function format(d) {
    return $.each(d.detalhes, function (index) {

    })
}

$(document).ready(function() {    

    var titulosTable = $('#grid_titulos').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/titulo/getTitulos',
            type: 'POST',
        },
        "language": {
            "processing": "<img style='position:fixed; top:60%; left:50%;margin-top:-8px;margin-left:-8px;' src='https://beta.sigacbr.com.br/js/loading.gif'>"
        },
        "columns": [
            { 
                "class"             :"details-control",
                "orderable"         : false,
                "data"              : null,
                "defaultContent"    : ""
            }, 
            {"data": "natureza"},
            {"data": "cliFor"},
            {"data": "prefixo"},
            {"data": "numero"},
            {"data": "emissao"},
            {"data": "formaPagamento"},
            {"data": "qtdParcelas"},
            {"data": "valorTotal"},
        ],
        "columnDefs": [
            {
                "orderable": false,
                "targets": "no-orderable"
            }
        ],
    });

    $('#grid_titulos tfoot th.searchable').each(function() {
        var title = $('#grid_titulos thead th').eq($(this).index()).text();
        $(this).html('<input style="width:100%" type="text" placeholder="Buscar ' + title + '" />');
    });

    titulosTable.columns().eq(0).each(function(colIdx) {
        $('input', titulosTable.column(colIdx).footer()).on('change', function() {
            titulosTable.column(colIdx).search(this.value).draw();
        })
    })
    
    var detailRows = [];

    $('#grid_titulos tbody').on('click', 'tr td:first-child', function () {

        var tr = $(this).closest('tr');
        var row = titulosTable.row(tr);
        var idx = $.inArray(tr.attr('id'), detailRows);

        if (row.child.isShown())
        {
            tr.removeClass('details');
            row.child.hide();
            detailRows.splice(idx, 1);
        }

        else
        {

            tr.addClass('details');
            row.child(format(row.data())).show();

            if (idx === -1)
            {
                detailRows.push(tr.attr('id'));
            }
        }

    });

    titulosTable.on('draw', function () {

        $.each(detailRows, function (i, id) {
            $('#' + id + ' td:first-child').trigger('click');
        });

    });


    $('#btn-filter').on('click', function () {

        titulosTable.draw();
        return false;
    });
    
});