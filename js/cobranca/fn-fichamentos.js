$(function(){

	var gridFichamentos = $('#grid_fichamentos').DataTable({

		"processing": true,
        
        "serverSide": true,
        
        "ajax": {
            url: '/cobranca/listarFichamentos/',
            type: 'POST'
        },

        "language": {
            "processing": "<img style='position:fixed; top:60%; left:50%;margin-top:-8px;margin-left:-8px;' src='https://s1.sigacbr.com.br/js/loading.gif'>"
        },

        "columnDefs": [{
            "orderable": false,
            "targets": "no-orderable"
        }],

        "columns": [        
            {"data": "cliente"},
            {"data": "cpf"},
            {"data": "valor"},
            {"data": "seq_parcela"},
            {"data": "data_fichamento"},
            {"data": "status_fichamento"},
            {"data": "btn_remove"},
        ],
	});

    $(document).on('click','.btn-rmv-fich',function(){

        $('#id_status_fichamento').val($(this).data('fichamento'));

        $('#modal_form_remove_fichamento').modal('show');

        return false;
    });

    $.validator.setDefaults({
        errorElement: "span", 
        errorClass: 'help-block',
        
        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group
        },
            
        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group
        },

        success: function (label, element) {
            label.addClass('help-block valid');
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        }
    });

  //Não é possível enviar arquivos por ajax, foi necessário colocar a requisição no "action" do formulário..
  $('#form-remove-fichamento').ajaxForm({
    
    beforeSubmit    : function(){
      //validando o arquivo que está sendo enviado
      if( $('#ComprovanteFile2').val() != '')
      {
          if ( window.File && window.FileReader && window.FileList && window.Blob && $('#form-remove-fichamento').valid() )
          {
            var fsize = $('#ComprovanteFile2')[0].files[0].size;
            var ftype = $('#ComprovanteFile2')[0].files[0].type;
                    
            switch( ftype )
            {
              case 'image/png': 
              case 'image/gif':
              case 'image/jpeg': 
              case 'image/pjpeg':
              case 'application/pdf':
              break;
              default:
                alert("Tipo de arquivo nÃ£o permitido!");
              return false;
            }
            if( fsize > 5242880 )
            {
              alert("Arquivo muito grande!");
              return false
            }
          }

          else
          {
            alert("Revise o formulÃ¡rio!");
          }
      }
    },
    //em caso de sucesso, notificar o usuário e ocultar o modal de fichamento
    success         : function( response, textStatus, xhr, form ) {

      var retorno = $.parseJSON(response);
            
      $('#modal_form_remove_fichamento').modal('hide');
      $('#form-remove-fichamento').trigger("reset");

      $.each(retorno.msgConfig.pnotify, function (notificacao, conteudoNotificacao) {
        $.pnotify({
          title: conteudoNotificacao.titulo,
          text: conteudoNotificacao.texto,
          type: conteudoNotificacao.tipo
        });
      });

      gridFichamentos.draw();
    }
  });

    /*$('#form-remove-fichamento').validate({

        submitHandler : function()
        {
            var request = $.ajax({

                url   :'/cobranca/removerFichamento/',
                type  :'POST', 
                data  : $('#form-remove-fichamento').serialize()

            });

            request.done(function(drt){

                $('#modal_form_remove_fichamento').modal('hide');

                var retorno = $.parseJSON(drt);
                
                console.log(retorno);

                $.each(retorno.msgConfig.pnotify,function(notificacao,conteudoNotificacao){
                    $.pnotify({
                      title   : conteudoNotificacao.titulo,
                      text    : conteudoNotificacao.texto,
                      type    : conteudoNotificacao.tipo
                    });
                });

                gridFichamentos.draw();

                $("#form-remove-fichamento")[0].reset();
            });
        }

    });*/
});