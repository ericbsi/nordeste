$(function(){

    $('#filiais_select').multiselect({

      buttonWidth: '200px',
      numberDisplayed:2,

      buttonText:function(options, select){

         if (options.length == 0) {
            return 'Selecionar Filiais <b class="caret"></b>'
         }
         else if (options.length == 1) {
            return '1 Filial Selecionada <b class="caret"></b>'
         }
         else{
            return options.length + ' Filiais Selecionadas <b class="caret"></b>'
         }
      },

    });

	var tableRecebimentos = $('#grid_recebimentos').DataTable({
		
        "processing": true,
        
        "serverSide": true,
        
        "ajax": {
            url: '/empresa/getRecebimentos/',
            type: 'POST',
            "data": function (d) {
                d.filiais    = $("#filiais_select").val(),
                d.data_de    = $("#data_de").val(),
                d.data_ate	 = $("#data_ate").val()
            },
        },
        
        "language": {
            "processing": "<img style='position:fixed; top:60%; left:50%;margin-top:-8px;margin-left:-8px;' src='https://beta.sigacbr.com.br/js/loading.gif'>"
        },
        
        "columnDefs": [{
            "orderable": false,
            "targets": "no-orderable"
        }],
        
        "columns": [
            {"data": "cliente"},
            {"data": "cpf"},
            {"data": "filial"},
            {"data": "seq_parcela"},
            {"data": "vencimento_da_parcela"},
            {"data": "data_da_baixa"},
            {"data": "valor_parcela"},
            {"data": "valor_pago"},
        ],
        "drawCallback" : function(settings) {
            $('#tfoot-total-atraso').html( settings.json.customReturn.totalAtrasoFilter + ' (de ' + settings.json.customReturn.totalAtraso +')' );
		    }
	});

	$('#btn-filter').on('click',function(){
		tableRecebimentos.draw();
		return false;
	});
});