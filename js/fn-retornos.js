$(function(){

	var tableRecebimentos = $('#grid_retornos').DataTable({
        "processing": true,
        "serverSide": true,
        
        "ajax": {
            url: '/empresa/getRetornos/',
            type: 'POST',
            "data": function (d) {
                d.data_de    = $("#data_de").val(),
                d.data_ate	 = $("#data_ate").val()
            },
        },
        
        "language": {
            "processing": "<img style='position:fixed; top:60%; left:50%;margin-top:-8px;margin-left:-8px;' src='https://beta.sigacbr.com.br/js/loading.gif'>"
        },
        
        "columnDefs": [{
            "orderable": false,
            "targets": "no-orderable"
        }],
        
        "columns": [
            {"data": "titulo"},
            {"data": "ocorrencia"},
            {"data": "data_importacao"},
            {"data": "data_ocorrencia"},
            {"data": "valor_pago"},
            {"data": "valor_mora"},
            //{"data": "origem_do_pagamento"},
            {"data": "motivo_da_rejeicao"},
            
        ],
        /*"drawCallback" : function(settings) {
            $(document).myfn(settings.json);
		}*/
	});

	$('#btn-filter').on('click',function(){
		tableRecebimentos.draw();
		return false;
	});
    $('#btn-clear-confirmados').on('click',function(){
        
        $.ajax({
            
            url : '/financeiro/clearRetornosRegistrados/',
            
            type: 'GET',
            
            beforeSend : function(){

                $('.panel').block({
                    overlayCSS: {
                                backgroundColor: '#fff'
                    },
                    message: '<img src="https://beta.sigacbr.com.br/js/loading.gif" /> Aguarde...',
                    css: {
                        border: 'none',
                        color: '#333',
                        background: 'none'
                    }
                });

                window.setTimeout(function () {
                        $('.panel').unblock();
                }, 1000);

            }
        })
        .done(function(drt){
            tableRecebimentos.draw();    
        });
    
        return false;
    });
    
});