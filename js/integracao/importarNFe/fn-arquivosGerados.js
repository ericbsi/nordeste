$(function()
{
    
    var propostasArquivos;
    
    var tableArquivos = $('#gridArquivos').DataTable(
    {

        "processing"    : true  ,
        "serverSide"    : true  ,
        "ajax"          : 
        {
            url     : '/integracao/listarArquivosCNAB/' ,
            type    : 'POST'                            ,
            "data"  : function (d) 
            {
                d.arquivosTransmitidos = $("#btnFiltroArquivosTrasmitidos").val();
            }
        },

        "columns": 
        [
            {
                "className"         : 'details-control' ,
                "orderable"         : false             ,
                "data"              : null              ,
                "defaultContent"    : ''
            },
            {
                "data"              : "dataArquivo"
            },
            {
                "data"              : "financeira"
            },
            {
                "data"              : "arquivo"
            },
            {
                "data"              : "status"
            },
            {
                "data"              : "qtdPropostas"
            },
            {
                "data"              : "nomeUsuario"
            },
            {
                "data"              : "valorFinanciado"
            },
            {
                "data"              : "valorRepasse"
            },
            {
                "data"              : "btnAcao"
            },
        ],

    });
    
    //$("#gridArquivos_filter").hide();
    
    $(".select2").select2();

    // Add event listener for opening and closing details
    $('#gridArquivos tbody').on('click', 'td.details-control', function () 
    {

        var tr          = $(this).closest('tr')             ;
        var row         = tableArquivos.row(tr)             ;
        var rowsTam     = tableArquivos.rows()[0].length    ;
        var rowData     = row.data()                        ;
        var elemento    = $(this)                           ;


//        elementoQtdFiliais = $('.qtdFiliais').get(row.index() + 1);
        idArquivo = rowData.idArquivo;

        if (row.child.isShown()) {

            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {

            for (i = 0; i < rowsTam; i++)
            {

                if (tableArquivos.row(i).child.isShown())
                {
                    tableArquivos.row(i).child.hide();
                }
            }

            $('td.details-control').closest('tr').removeClass('shown');

            formatSubTable(row.data(), tr, row);
            
//            if (idServico == 2) 
//            {
                
                propostasArquivos = $('#gridPropostasArquivo').dataTable(
                {
                    "processing": true,
                    "serverSide": true,
                    "ajax": 
                    {
                        url     : '/integracao/getPropostasArquivo' ,
                        type    : 'POST'                            ,
                        data    : function (d) 
                        {
                            d.idArquivo = idArquivo;
                        }

                    },
                    "columns": 
                    [
                        {
                            "orderable" : false,
                            "data"      : "codigoProposta"
                        },
                        {
                            "orderable" : false,
                            "data"      : "filial"
                        },
                        {
                            "orderable" : false,
                            "data"      : "dataProposta"
                        },
                        {
                            "orderable" : false,
                            "data"      : "cpf"
                        },
                        {
                            "orderable" : false,
                            "data"      : "nomeCliente"
                        },
                        {
                            "orderable" : false,
                            "data"      : "valorFinanciado"
                        },
                        {
                            "orderable" : false,
                            "data"      : "valorRepasse"
                        },
                    ],
                    "columnDefs": 
                    [
                        {
                            "orderable": false,
                            "targets": "no-orderable"
                        },
                        {
                            "orderable": false,
                            "targets": "no-orderable"
                        },
                        {
                            "orderable": false,
                            "targets": "no-orderable"
                        },
                        {
                            "orderable": false,
                            "targets": "no-orderable"
                        },
                        {
                            "orderable": false,
                            "targets": "no-orderable"
                        },
                        {
                            "orderable": false,
                            "targets": "no-orderable"
                        }
                    ],
                    "drawCallback": function (settings) 
                    {
                        
                        $("#gridPropostasArquivo_length").hide();
                        $("#gridPropostasArquivo_filter").hide();
                        
                        $("#vlrTotFinPropostasArq"  ).html(settings.json.vlrTotFin  );
                        $("#vlrTotRepPropostasArq"  ).html(settings.json.vlrTotRep  );
                    
                    }
                });

/*            } 
            else 
            {
                
                filiaisAdmin = $('#filiaisAdmin').dataTable(
                        {
                            "processing": true,
                            "serverSide": true,
                            "ajax": {
                                url: '/servico/getFiliaisAdmin',
                                type: 'POST',
                                data: function (d) {
                                    d.idServico = idServico
                                }

                            },
                            "columns": [
                                {
                                    "data": "checkFilial",
                                    "orderable": false,
                                    "className": "checkFilial"
                                },
                                {
                                    "orderable": false,
                                    "data": "filial"
                                },
                                {
                                    "orderable": false,
                                    "data": "selectFin"
                                }
                            ],
                            "columnDefs": [
                                {
                                    "orderable": false,
                                    "targets": "no-orderable"
                                },
                                {
                                    "orderable": false,
                                    "targets": "no-orderable"
                                }
                            ]
                        });
            }*/
            
        }
    });
    
    $(document).on("click",".btnFTP",function()
    {
        
        var idArquivo   = $(this).val() ;
        var elemento    = $(this)       ;
        
        $(this).removeClass ("btn-green"                                                );
        $(this).addClass    ("btn-white"                                                );
        $(this).attr        ("disabled"                                 , "disabled"    );
        $(this).html        ("<img src='/images/loading.gif'></img>"                    );
        
        $.ajax
        (
            {
                type    : "POST"                    ,
                url     : "/integracao/comunicaFTP" ,
                data    :
                {   
                    "idArquivo" : idArquivo
                }
            }
        )
        .done
        (
            function (dRt)
            {
                
                var retorno = $.parseJSON(dRt);
                
                $.pnotify
                (
                    {
                        title   : retorno.title ,
                        text    : retorno.msg   ,
                        type    : retorno.type
                    }
                );
                
                if(!retorno.hasErrors)
                {
                    
                    tableArquivos.draw();
                    
                }
                else
                {
                    elemento.removeClass    ("btn-white"                        );
                    elemento.addClass       ("btn-green"                        );
                    elemento.removeAttr     ("disabled"                         );
                    elemento.html           ("<i class='clip-arrow-right'></i>" );
                }
                
            }
                    
        );
        
    });
    
    $(document).on("click",".btnBordero",function()
    {
        
        var idArquivo   = $(this).val() ;
        var elemento    = $(this)       ;
        
        $(this).removeClass ("btn-yellow"                                               );
        $(this).addClass    ("btn-white"                                                );
        $(this).attr        ("disabled"                                 , "disabled"    );
        $(this).html        ("<img src='/images/loading.gif'></img>"                    );
        
        $.ajax
        (
            {
                type    : "POST"                            ,
                url     : "/integracao/gerarBorderosCNAB"   ,
                data    :
                {   
                    "idArquivo" : idArquivo
                }
            }
        )
        .done
        (
            function (dRt)
            {
                
                var retorno = $.parseJSON(dRt);
                
                $.pnotify
                (
                    {
                        title   : retorno.title ,
                        text    : retorno.msg   ,
                        type    : retorno.type
                    }
                );
                
                if(!retorno.hasErrors)
                {
                    
                    tableArquivos.draw();
                    
                }
                else
                {
                    elemento.removeClass    ("btn-white"                    );
                    elemento.addClass       ("btn-yellow"                   );
                    elemento.removeAttr     ("disabled"                     );
                    elemento.html           ("<i class='clip-data'></i>"    );
                }
                
            }
                    
        );
        
    });
    
});


function formatSubTable(d, tr, row) {

    tr.addClass('shown');

    // `d` is the original data object for the row
    var data = '<div class="row">' +
            '   <div  class="col-sm-12">' +
            '       <table id="gridPropostasArquivo" class="table table-striped table-bordered table-hover table-full-width dataTable">' +
            '           <thead>' +
            '              <tr>' +
            '                   <th class="no-orderable">' +
            '                       Código' + 
            '                   </th>' +
            '                   <th class="no-orderable">' +
            '                       Filial' + 
            '                   </th>' +
            '                   <th class="no-orderable" >' +
            '                       Data' +
            '                   </th>' +
            '                   <th class="no-orderable" >' +
            '                       CPF' +
            '                   </th>' +
            '                   <th class="no-orderable" >' +
            '                       Cliente' +
            '                   </th>' +
            '                   <th class="no-orderable" >' +
            '                       Valor Financiado' +
            '                   </th>' +
            '                   <th class="no-orderable" >' +
            '                       Valor Repasse' +
            '                   </th>' +
            '              </tr>' +
            '           </thead>' +
            '           <tbody>' +
            '           </tbody>' +
            '           <tfoot>' +
            '              <tr>' +
            '                   <th colspan="5"></th>' +
            '                   <th>' +
            '                       R$ <b id="vlrTotFinPropostasArq">0,00</b>' +
            '                   </th>' +
            '                   <th>' +
            '                       R$ <b id="vlrTotRepPropostasArq">0,00</b>' +
            '                   </th>' +
            '              </tr>' +
            '           </tfoot>' +
            '       </table>' +
            '   </div>' +
            '</div>';

    row.child(data).show();
}