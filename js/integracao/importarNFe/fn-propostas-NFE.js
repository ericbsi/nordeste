$(function(){

    var tablePropostas = $('#grid_propostas').DataTable(
    {

        "processing"    : true  ,
        "serverSide"    : true  ,
        "ajax"          : 
        {
            url     : '/integracao/listarPropostasNFe/',
            type    : 'POST',
            "data"  : function (d) 
            {
                d.codigo_filter = $("#codigo_filter"    ).val() ,
                d.nome_filter 	= $("#nome_filter"      ).val()
            }
        },

        "columns": 
        [
            {
                "data"  : "btn"
            },
            {
                "data"  : "codigo"
            },
            {
                "data"  : "cliente"
            },
            {
                "data"  : "crediarista"
            },
            {
                "data"  : "valor"
            },
            {
                "data"  : "valor_entrada"
            },
            {
                "data"  : "seguro"
            },
            {
                "data"  : "val_financiado"
            },
            {
                "data"  : "qtd_parcelas"
            },
            {
                "data"  : "valor_final"
            },
            {
                "data"  : "btn_status"
            },
        ],

    });

    $(document).on('change','.input_filter', function()
    {
        tablePropostas.draw();
    });


    $(document).on('click', '.btn-modal-xml', function(){
        $('#proposta-id').attr('value', $(this).data('proposta-id'));
    });

    $('#form-add-xml-nf').ajaxForm({

        beforeSubmit    : function(){

            if ( window.File && window.FileReader && window.FileList && window.Blob && $('#anexoXML').valid() )
            {
                var fsize = $('#anexoXML')[0].files[0].size;
                var ftype = $('#anexoXML')[0].files[0].type;
                
                switch( ftype )
                {                  
                    case 'text/xml':
                    break;
                    default:
                        alert("Tipo de arquivo não permitido!");
                    return false;
                }

                if( fsize > 5242880 )
                {
                    alert("Arquivo muito grande!");
                    return false
                }
            }

            else
            {
                alert("Revise o formulário!");
            }
        },

        success         : function( response, textStatus, xhr, form ) {

            var retorno = $.parseJSON(response);

            $.pnotify({
                    title    : 'Notificação'    ,
                    text     : retorno.msg      ,
                    type     : retorno.type
            });

            if( retorno.hasErrors != 1 )
            {
            
                tablePropostas.draw();

                $('#modalAnexo'         ).modal     ('hide'     );
                $('#form-add-xml-nf'    ).trigger   ("reset"    );
            
            }

        },

    });

});