function bloquear_ctrl_j(){

    if (window.event.ctrlKey && window.event.keyCode == 74)
    {
        event.keyCode = 0;  
        event.returnValue = false;  
    }  
}

$(function(){
    
    var focusCodigo = 0;
    
    $(document).ready(function()
    {
        myCheckboxIsChecked2    ();
        myCheckboxIsChecked     ();
    });
    
    var marcados        = [];
    var marcadosLecca   = [];
    
    var qtdFiltrados        ;
    var qtdFiltradosLecca   ;
    
    //var filtroMarcadasLecca ;
    
    var tablePropostasNFe = $('#grid_nfe').DataTable(
    {

        "processing"    : true  ,
        "serverSide"    : true  ,
        "ajax"          : 
        {
            url     : '/integracao/listarNFeCNAB/'  ,
            type    : 'POST'                        ,
            "data"  : function (d) 
            {
                
                d.codigo_filter     = $("#codigo_filter"        ).val() ;
                d.filial_filter     = $("#filial_filter"        ).val() ;
                d.nome_filter       = $("#nome_filter"          ).val() ;
                d.cpf_filter        = $("#cpf_filter"           ).val() ;
                d.data_filter       = $("#data_filter"          ).val() ;
                d.nfserie_filter    = $("#nfserie_filter"       ).val() ;
                d.comNFe            = $("#btnFiltroComNFe"      ).val() ;
                d.semNFe            = $("#btnFiltroSemNFe"      ).val() ;
                d.proNFe            = $("#btnFiltroProNFe"      ).val() ;
                d.semChv            = $("#btnFiltroSemChv"      ).val() ;
                d.selecionaTudo     = $("#btnSelecionaTudo"     ).val() ;
                d.comJuros          = $("#btnFiltroComJuros"    ).val() ;
                d.semJuros          = $("#btnFiltroSemJuros"    ).val() ;
                d.notasMarcadas     = marcados                      ;
//                d.soHoje        = $("#soHoje").hasClass("clip-square")
            }
        },

        "columns": 
        [
            {
                "data"      : "check"   ,
                "className" : "thCheck"
            },
            {
                "data"  : "modalidade"
            },
            {
                "data"  : "codigoProposta"
            },
            {
                "data"  : "nomeFilial"
            },
            {
                "data"  : "nomeCliente"
            },
            {
                "data"  : "cpfCliente"
            },
            {
                "data"  : "dataProposta"
            },
            {
                "data"  : "dataNFe"
            },
            {
                "data"  : "serieNota"
            },
            {
                "data"  : "chaveNFe"
            },
            {
                "data"  : "valorFinanciado"
            },
            {
                "data"  : "parcelamento"
            },
            {
                "data"  : "valorFinal"
            },
            {
                "data"  : "valorRepasse"
            }
        ],

    });
    
    var tablePropostasLecca = $('#grid_nfe_lecca').DataTable(
    {

        "processing"    : true  ,
        "serverSide"    : true  ,
        "ajax"          : 
        {
            url     : '/integracao/listarPropostasLecca/'   ,
            type    : 'POST'                                ,
            "data"  : function (d) 
            {
                
                d.codigo_filter     = $("#codigo_filterLecca"       ).val() ;
                d.filial_filter     = $("#filial_filterLecca"       ).val() ;
                d.nome_filter       = $("#nome_filterLecca"         ).val() ;
                d.cpf_filter        = $("#cpf_filterLecca"          ).val() ;
                d.data_filter       = $("#data_filterLecca"         ).val() ;
                d.proLecca          = $("#btnFiltroProLecca"        ).val() ;
                d.comJuros          = $("#btnFiltroComJurosLecca"   ).val() ;
                d.semJuros          = $("#btnFiltroSemJurosLecca"   ).val() ;
                d.filtroMarcadas    = $("#btnFiltroMarcadasLecca"   ).val() ;
                d.propostasMarcadas = marcadosLecca                         ;
            }
        },
        "order"     :   
        [
            [
                "2"     ,
                "asc"
            ]
        ],
        "columns"   : 
        [
            {
                "data"      : "check"   ,
                "className" : "thCheck no-orderable"
            },
            {
                "data"  : "modalidade"
            },
            {
                "data"  : "codigoProposta"
            },
            {
                "data"  : "nomeFilial"
            },
            {
                "data"  : "nomeCliente"
            },
            {
                "data"  : "cpfCliente"
            },
            {
                "data"  : "dataProposta"
            },
            {
                "data"  : "valorFinanciado"
            },
            {
                "data"  : "parcelamento"
            },
            {
                "data"  : "valorRepasse"
            }
        ],
        "columnDefs": 
        [
            { 
                "orderable" : false, 
                "targets"   : 0 
            }
        ],
        "drawCallback": function (settings) 
        {
            
            $("#vlrTotFinPropostas" ).html(settings.json.vlrTotFin  );
            $("#vlrTotRepPropostas" ).html(settings.json.vlrTotRep  );
            
            $("#vlrFilFinPropostas" ).html(settings.json.vlrFilFin  );
            $("#vlrFilRepPropostas" ).html(settings.json.vlrFilRep  );

        }

    });
    
    tablePropostasNFe.on
    (   
        "draw"          ,
        function()
        {

            marcados        = tablePropostasNFe.data().ajax.json().marcados ;
            qtdFiltrados    = tablePropostasNFe.page.info().recordsDisplay  ;
                        
            if($("#btnFiltroComNFe").hasClass("active"))
            {
                
                $("#btnSelecionaTudo").removeAttr("style");
                
            }
            else
            {
                
                $("#btnSelecionaTudo").attr("style","display : none");
                
            }
            
            $("#valorTotalFinanciado"   ).html(tablePropostasNFe.data().ajax.json().valorTotal         );
            $("#valorTotalFinal"        ).html(tablePropostasNFe.data().ajax.json().valorTotalFinal    );
            $("#valorTotalRepasse"      ).html(tablePropostasNFe.data().ajax.json().valorTotalRepasse  );
        
            habilitaGerar();
            
        }
    );
    
    tablePropostasLecca.on
    (   
        "draw"          ,
        function()
        {

//            marcadosLecca       = tablePropostasLecca.data().ajax.json().marcados ;
            qtdFiltradosLecca   = tablePropostasLecca.page.info().recordsDisplay  ;
            
            if(qtdFiltradosLecca == 1 && tablePropostasLecca.data().ajax.json().bipProposta !== "")
            {
                
                if($.inArray(tablePropostasLecca.data().ajax.json().bipProposta,marcadosLecca) < 0)
                {
                    
                    console.log(marcadosLecca);
                    console.log(tablePropostasLecca.data().ajax.json().bipProposta);
                    
                    marcadosLecca.push(tablePropostasLecca.data().ajax.json().bipProposta);
                }
                
                if(focusCodigo)
                {
                    $("#codigo_filterLecca").val    (""             );
                    $("#codigo_filterLecca").attr   ("value"   ,""  );
                    $("#codigo_filterLecca").attr   ("text"    ,""  );
                    $("#codigo_filterLecca").focus  (               );
                }
                else
                {
                    console.log("voti");
                }
                
            }
        
            habilitaGerarLecca();
            
        }
    );

    var myCheckbox  = $("#myCheckbox"   ).bootstrapSwitch('state',true, true);
    
    var myCheckbox2 = $("#myCheckbox2"  ).bootstrapSwitch('state',true, true);

    $(document).on('change','.input_filter', function()
    {
        tablePropostasNFe.draw();
    });

    $(document).on('click','.checkNF', function()
    {
        
        var elemento = $(this);
        
        if($(this).hasClass("clip-square"))
        {
            $(this).removeClass ("clip-square"                  );
            $(this).addClass    ("clip-checkbox-unchecked-2"    );
            
            if($(this).attr("id") == "iSelecionarTudo")
            {

                $("#btnSelecionaTudo").attr("value","0");

                tablePropostasNFe.draw();

            }
            else
            {
                
                if($.inArray($(this).attr("value"),marcados) >= 0)
                {
                    
                    marcados =  $.grep(marcados, function(value) 
                                {
                                    return value !== elemento.attr("value");
                                });
                }
        
                habilitaGerar();
                
            }
            
        }
        else
        {
            
            if($(this).attr("id") == "iSelecionarTudo")
            {

                $("#btnFiltroComNFe").addClass      ("active"           );
                $("#btnFiltroComNFe").val           ("1"                );
                $("#btnFiltroComNFe").removeClass   ("btn-blue");

                $("#btnFiltroSemNFe").removeClass   ("active"           );
                $("#btnFiltroSemNFe").val           ("0"                );
                $("#btnFiltroSemNFe").addClass      ("btn-yellow"       );

                $("#btnFiltroProNFe").removeClass   ("active"           );
                $("#btnFiltroProNFe").val           ("0"                );
                $("#btnFiltroProNFe").addClass      ("btn-green"        );

                $("#btnFiltroSemChv").removeClass   ("active"           );
                $("#btnFiltroSemChv").val           ("0"                );
                $("#btnFiltroSemChv").addClass      ("btn-red"          );
                
                $("#btnSelecionaTudo").attr("value","1");
                
                tablePropostasNFe.draw();

            }
            else
            {
                
                if($.inArray($(this).val(),marcados) < 0)
                {
                    marcados.push(elemento.attr("value"));
                }
        
                habilitaGerar();
                
            }
            
            $(this).addClass    ("clip-square"                  );
            $(this).removeClass ("clip-checkbox-unchecked-2"    );
        }
        
    });
    
    $(document).on('change', '.input_filterLecca', function ()
    {
        
        if($(this).attr("id") == "codigo_filterLecca")
        {
            focusCodigo = 1;
        }
        else
        {
            focusCodigo = 0;
        }
        
        tablePropostasLecca.draw();
    });

    $(document).on('click','.checkProposta', function()
    {
        
        var elemento = $(this);
        
        if($(this).hasClass("clip-square"))
        {
                
            if($.inArray($(this).attr("value"),marcadosLecca) >= 0)
            {

                marcadosLecca = $.grep(marcadosLecca, function(value) 
                                {
                                    return value !== elemento.attr("value");
                                });
            }

            habilitaGerarLecca();
            
            $(this).removeClass ("clip-square"                  );
            $(this).addClass    ("clip-checkbox-unchecked-2"    );
            
        }
        else
        {
                
            if($.inArray($(this).val(),marcadosLecca) < 0)
            {
                marcadosLecca.push(elemento.attr("value"));
            }

            habilitaGerarLecca();
            
            $(this).addClass    ("clip-square"                  );
            $(this).removeClass ("clip-checkbox-unchecked-2"    );
            
        }
        
    });
    
    $(document).on('click','.btnForcarXML', function()
    {
        var elemento = $(this);
        
        $(this).removeClass ("btn-yellow"                                               );
        $(this).addClass    ("btn-white"                                                );
        $(this).attr        ("disabled"                                 , "disabled"    );
        $(this).html        ("<img src='/images/loading.gif'></img>"                    );
        
        $.ajax
        (
            {
                type    : "POST"                    ,
                url     : "/integracao/buscarNFeWS" ,
                data    :
                {   
                    "idProposta" : elemento.val()
                }
            }
        )
        .done
        (
            function (dRt)
            {
                
                var retorno = $.parseJSON(dRt);
                
                $.pnotify
                (
                    {
                        title   : retorno.title ,
                        text    : retorno.msg   ,
                        type    : retorno.type
                    }
                );
                
                if(!retorno.hasErrors)
                {
                    
                    tablePropostasNFe.draw();
                    
                }
                else
                {
                    elemento.removeClass    ("btn-white"                        );
                    elemento.addClass       ("btn-yellow"                       );
                    elemento.removeAttr     ("disabled"                         );
                    elemento.html           ("<i class='clip-file-xml'></i>"    );
                }
                
            }
                    
        );

    });
    
//    $("#btnFiltroComNFe").on("click",function()
//    {

    $(document).on("click",".filtroNFe",function()
    {
        
        if($(this).hasClass("active"))
        {
            
            $(this).removeClass ("active"           );
            $(this).val         ("0"                );
            
            if($(this).attr("id") == "btnFiltroComNFe")
            {
                $(this).addClass("btn-blue");
            }
            else if($(this).attr("id") == "btnFiltroSemNFe")
            {
                $(this).addClass("btn-yellow");
            }
            else if($(this).attr("id") == "btnFiltroSemChv")
            {
                $(this).addClass("btn-red");
            }
            else if($(this).attr("id") == "btnFiltroProNFe")
            {
                $(this).addClass("btn-green");
            }
            else if($(this).attr("id") == "btnFiltroComJuros")
            {
                $(this).addClass("btn-beige");
            }
            else
            {
                $(this).addClass("btn-dark-beige");
            }
            
        }
        else
        {
            
            $(this).addClass    ("active"           );
            $(this).val         ("1"                );
            
            if($(this).attr("id") == "btnFiltroComNFe")
            {
                $(this).removeClass("btn-blue");
            }
            else if($(this).attr("id") == "btnFiltroSemNFe")
            {
                $(this).removeClass("btn-yellow");
            }
            else if($(this).attr("id") == "btnFiltroSemChv")
            {
                $(this).removeClass("btn-red");
            }
            else if($(this).attr("id") == "btnFiltroProNFe")
            {
                $(this).removeClass("btn-green");
            }
            else if($(this).attr("id") == "btnFiltroComJuros")
            {
                $(this).removeClass("btn-beige");
            }
            else
            {
                $(this).removeClass("btn-dark-beige");
            }
            
        }
        
        tablePropostasNFe.draw();
        
    });

    $(document).on("click",".filtroLecca",function()
    {
        
        if($(this).hasClass("active"))
        {
            
            $(this).removeClass ("active"           );
            $(this).val         ("0"                );
            
            if($(this).attr("id") == "btnFiltroProLecca")
            {
                $(this).addClass("btn-green");
            }
            else if($(this).attr("id") == "btnFiltroComJurosLecca")
            {
                $(this).addClass("btn-beige");
            }
            else
            {
                $(this).addClass("btn-dark-beige");
            }
            
        }
        else
        {
            
            $(this).addClass    ("active"           );
            $(this).val         ("1"                );
            
            if($(this).attr("id") == "btnFiltroProLecca")
            {
                $(this).removeClass("btn-green");
            }
            else if($(this).attr("id") == "btnFiltroComJurosLecca")
            {
                $(this).removeClass("btn-beige");
            }
            else
            {
                $(this).removeClass("btn-dark-beige");
            }
            
        }
        
        tablePropostasLecca.draw();
        
    });
    
    $("#botaoGerar").on("click", function()
    {
        
        $.ajax  
        (
            {

                type    : "POST"                    ,
                url     : "/integracao/gerarCNAB"   ,
                data    : 
                {
                    codigo_filter   : $("#codigo_filter"        ).val() ,
                    filial_filter   : $("#filial_filter"        ).val() ,
                    nome_filter     : $("#nome_filter"          ).val() ,
                    data_filter     : $("#data_filter"          ).val() ,
                    nfserie_filter  : $("#nfserie_filter"       ).val() ,
                    selecionaTudo   : $("#btnSelecionaTudo"     ).val() ,
                    comJuros        : $("#btnFiltroComJuros"    ).val() ,
                    semJuros        : $("#btnFiltroSemJuros"    ).val() ,
                    notasMarcadas   : marcados  //getMarcados($('.checkNF'))
                },
            }
        )
        .done
        (
            function (dRt)
            {
                
                var retorno = $.parseJSON(dRt);
                
                $.pnotify
                (
                    {
                        title   : retorno.title ,
                        text    : retorno.msg   ,
                        type    : retorno.type
                    }
                );
                
                if(!retorno.hasErrors)
                {
                    
                    var link        = document.createElement("a")   ;
                    link.download   = retorno.nomeArquivo           ;
                    link.href       = retorno.url                   ;
                    link.click()                                    ;
                    
                    tablePropostasNFe.draw();
                    
//                    document.location = 'data:Application/octet-stream,' + encodeURIComponent(retorno.cnab);
                    
//                    window.location = retorno.url;
//                    console.log(retorno.url);
                }
                
            }
        );

    });
    
    $("#botaoGerarLecca").on("click", function()
    {
        
        $.ajax  
        (
            {

                type    : "POST"                    ,
                url     : "/integracao/gerarArquivoLecca"   ,
                data    : 
                {
                    codigo_filter       : $("#codigo_filterLecca"       ).val() ,
                    filial_filter       : $("#filial_filterLecca"       ).val() ,
                    nome_filter         : $("#nome_filterLecca"         ).val() ,
                    data_filter         : $("#data_filterLecca"         ).val() ,
                    comJuros            : $("#btnFiltroComJurosLecca"   ).val() ,
                    semJuros            : $("#btnFiltroSemJurosLecca"   ).val() ,
                    propostasMarcadas   : marcadosLecca
                },
            }
        )
        .done
        (
            function (dRt)
            {
                
                var retorno = $.parseJSON(dRt);
                
                $.pnotify
                (
                    {
                        title   : retorno.title ,
                        text    : retorno.msg   ,
                        type    : retorno.type
                    }
                );
                
                if(!retorno.hasErrors)
                {
                    
                    marcadosLecca = [];
                    
                    var link        = document.createElement("a")   ;
                    link.download   = retorno.nomeArquivo           ;
                    link.href       = retorno.url                   ;
                    link.click()                                    ;
                    
                    tablePropostasLecca.draw();
                    
                }
                
            }
        );

    });
    
    $("#botaoForceXML").on("click",function()
    {
        
        var elemento = $(this);
        
        $(this).removeClass ("btn-orange"                                                                   );
        $(this).addClass    ("btn-white"                                                                    );
        $(this).attr        ("disabled"                                                     , "disabled"    );
        $(this).html        ("<img src='/images/loading.gif'></img> <b>Buscando NFe's</b>"                  );
        
        $.ajax
        (
            {
                type    : "POST"                    ,
                url     : "/integracao/buscarNFeWS"   
            }
        )
        .done
        (
            function (dRt)
            {
                
                var retorno = $.parseJSON(dRt);
                
                $.pnotify
                (
                    {
                        title   : retorno.title ,
                        text    : retorno.msg   ,
                        type    : retorno.type
                    }
                );
                
                if(!retorno.hasErrors)
                {
                    
                    tablePropostasNFe.draw();
                    
                }
            
                elemento.removeClass    ("btn-white"                                        );
                elemento.addClass       ("btn-orange"                                       );
                elemento.removeAttr     ("disabled"                                         );
                elemento.html           ("<i class='clip-date'></i> <b>Buscar NFe's</b>"    );
                
            }
                    
        );
        
        
    });
    
    $("#btnFiltroMarcadasLecca").on("click",function()
    {
        
        if($("#iFiltroMarcadas").hasClass("clip-checkbox-partial"))
        {
            
            //filtroMarcadasLecca = "1";
            
            $("#btnFiltroMarcadasLecca" ).attr          ("value"                    , "1"   );
            $("#iFiltroMarcadas"        ).addClass      ("clip-square"                      ); 
            $("#iFiltroMarcadas"        ).removeClass   ("clip-checkbox-partial"            );
            
        }
        else if ($("#iFiltroMarcadas").hasClass("clip-square"))
        {
            
            //filtroMarcadasLecca = "0";
            
            $("#btnFiltroMarcadasLecca" ).attr          ("value"                        , "0"   );
            $("#iFiltroMarcadas"        ).addClass      ("clip-checkbox-unchecked-2"            ); 
            $("#iFiltroMarcadas"        ).removeClass   ("clip-square"                          );
            
        }
        else
        {
            
//            filtroMarcadasLecca = "2";
            
            $("#btnFiltroMarcadasLecca" ).attr          ("value"                        , "2"   );
            $("#iFiltroMarcadas"        ).addClass      ("clip-checkbox-partial"                ); 
            $("#iFiltroMarcadas"        ).removeClass   ("clip-checkbox-unchecked-2"            );
            
        }
        
        tablePropostasLecca.draw();
        
    });
    
    myCheckbox.on('switchChange.bootstrapSwitch', function(event, state) 
    {
        
        myCheckboxIsChecked();
        
    });
    
    myCheckbox2.on('switchChange.bootstrapSwitch', function(event, state) 
    {
        
        myCheckboxIsChecked2();
        
    });
    
    function habilitaGerar()
    {
        
        if (marcados.length > 0)
        {
            
            if(marcados.length !== qtdFiltrados)
            {
                $("#iSelecionarTudo"    ).removeClass   ("clip-square"                          );
                $("#iSelecionarTudo"    ).removeClass   ("clip-checkbox-unchecked-2"            );
                $("#iSelecionarTudo"    ).addClass      ("clip-checkbox-partial"                );
                
                $("#btnSelecionaTudo"   ).attr          ("value"                        ,"2"    );
            }
            else
            {
                $("#iSelecionarTudo"    ).addClass      ("clip-square"                          );
                $("#iSelecionarTudo"    ).removeClass   ("clip-checkbox-unchecked-2"            );
                $("#iSelecionarTudo"    ).removeClass   ("clip-checkbox-partial"                );
                
                $("#btnSelecionaTudo"   ).attr          ("value"                        ,"1"    );
            }
            
            $("#botaoGerar").removeAttr("disabled");
        }
        else
        {
            
            $("#iSelecionarTudo"    ).removeClass   ("clip-square"                          );
            $("#iSelecionarTudo"    ).addClass      ("clip-checkbox-unchecked-2"            );
            $("#iSelecionarTudo"    ).removeClass   ("clip-checkbox-partial"                );

            $("#btnSelecionaTudo"   ).attr          ("value"                        ,"0"    );
                
            $("#botaoGerar").attr("disabled","disabled");
        }
        
    }
    
    function habilitaGerarLecca()
    {
        
        if (marcadosLecca.length > 0)
        {
            
            $("#botaoGerarLecca").removeAttr("disabled");
        }
        else
        {                
            $("#botaoGerarLecca").attr("disabled","disabled" );
        }
        
        $("#qtdMarcadas").text(marcadosLecca.length);
        
        $.ajax  
        (
            {

                type    : "POST"                                    ,
                url     : "/integracao/getValoresSelecionadosLecca" ,
                data    : 
                {
                    propostasMarcadas   : marcadosLecca
                },
            }
        )
        .done
        (
            function (dRt)
            {
                
                var retorno = $.parseJSON(dRt);
                
                if(!retorno.hasErrors)
                {
                    
                    $("#valorFinanciado"    ).text(retorno.valorFinanciado  );
                    $("#valorRepasse"       ).text(retorno.valorRepasse     );
                    
                }
                else
                {
                    $.pnotify
                    (
                        {
                            title   : "Erro"        ,
                            text    : retorno.msg   ,
                            type    : "error"
                        }
                    );
                }
                
            }
        );
        
    }

/*    function getMarcados(elementos)
    {

        var retorno = [];

        $.each(elementos, function (indice, variavel)
        {
            
            if($(variavel).attr("id") !== "iSelecionarTudo" ) 
            {
                retorno.push($(variavel).attr('value'));
            }

        })

        return retorno;

    }*/
    
    
    function myCheckboxIsChecked()
    {
        
        if ($('#myCheckbox').prop('checked')) 
        {
            
            $("#codigo_filterLecca" ).focus (       );
            
            $("#divFIDC"            ).hide  ("slow" );
            $("#divLecca"           ).show  ("slow" );
        }
        else
        {
            $("#divFIDC"    ).show("slow");
            $("#divLecca"   ).hide("slow");
        }
        
    }
    
    function myCheckboxIsChecked2()
    {
        
        if ($('#myCheckbox2').prop('checked')) 
        {
            
            $("#conteudo"       ).show  ("slow" );
            $("#divArquivos"    ).hide  ("slow" );
        }
        else
        {
            $("#conteudo"       ).hide("slow");
            $("#divArquivos"    ).show("slow");
        }
        
    }

});