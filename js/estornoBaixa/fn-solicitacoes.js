$(document).ready(function ()
{
    var estornoTable = $('#grid_estorno').DataTable(
    {
        "sDom"          : 'T<"clear">lfrtip'            ,
        "tableTools"    :
        {
            "sSwfPath"  : "/swf/copy_csv_xls_pdf.swf"
        },
        "processing"    : true                          ,
        "serverSide"    : true                          ,
        "ajax"          :
        {
            url         : '/estornoBaixa/gridEstornos'  ,
            type        : 'POST'                        
        },
        "columns":
        [
/*            {
                "className"         : 'details-control' ,
                "orderable"         : false             ,
                "data"              : null              ,
                "defaultContent"    : ''
            },*/
            {
                "data"  : "proposta"
            },
            {
                "data"  : "dataProposta"
            },
            {
                "data"  : "dataPagamento"
            },
            {
                "data"  : "dataSolicitacao"
            },
            {
                "data"  : "dataRetorno"
            },
            {
                "data"  : "cliente"
            },
            {
                "data"  : "cpf"
            },
            {
                "data"  : "dadosBancarios"
            },
            {
                "data"  : "comprovantePG"
            },
            {
                "data"  : "comprovanteEB"
            },
            {
                "data"  : "valor"
            },
            {
                "data"  : "btnAprovar"
            }
        ]
    });
    
    $(document).on('click','.btnAprovar',function()
    {   
        
        var idEstorno = $(this).val();

        $.ajax(
        {
            type  : "POST"                  ,
            url   : "/estornoBaixa/aprovar" ,
            data  : 
            {
                "idEstorno" : idEstorno,
            },
        }).done(function (dRt) 
        {

            var retorno = $.parseJSON(dRt);
            
            $.pnotify(
            {
               title : 'Notificação'      ,
               text  : retorno.msg        ,
               type  : retorno.type
            });
            
            if(!retorno.hasErrors)
            {
                estornoTable.draw();
            }
            
        });
        
    });
        
});