$(document).ready(function ()
{
    
    var idEstorno   = 0;
    
    var estornoParcelaTable = $('#grid_estornoParcelas').DataTable(
    {
        "processing"    : true  ,
        "serverSide"    : true  ,
        "paging"        : false ,
        "bFilter"       : false ,
        "bInfo"         : false ,
        "ajax"          :
        {
            url     : '/estornoBaixa/gridEstornosParcelas'  ,
            type    : 'POST'                        
        },
        "columns":
        [
            {
                "data"  : "proposta"
            },
            {
                "data"  : "dataProposta"
            },
            {
                "data"  : "vencimento"
            },
            {
                "data"  : "dataBaixa"
            },
            {
                "data"  : "dataSolicitacao"
            },
            {
                "data"  : "solicitante"
            },
            {
                "data"  : "responsavel"
            },
            {
                "data"  : "cliente"
            },
            {
                "data"  : "cpf"
            },
            {
                "data"  : "valorParcela"
            },
            {
                "data"  : "valorBaixa"
            },
            {
                "data"  : "btnMensagens"
            },
            {
                "data"  : "btnAprovar"
            }
        ]
    });
    
    var mensagensTable = $('#gridMensagens').DataTable(
    {
        "processing"    : true  ,
        "serverSide"    : true  ,
        "paging"        : false ,
        "bFilter"       : false ,
        "bInfo"         : false ,
        "ajax"          :
        {
            url     :   '/estornoBaixa/gridMensagens' ,
            type    :   'POST'                        ,
            data    :   function(d)
                        {
                            d.idEstorno = idEstorno;
                        }
        },
        "columns":
        [
            {
                "data"  : "dataStatus"
            },
            {
                "data"  : "descricaoStatus"
            },
            {
                "data"  : "usuario"
            },
            {
                "data"  : "mensagem"
            }
        ]
    });
    
    $(document).on('click', '.btnMensagens', function()
    {
        
        $("#divEstornos"    ).hide("slow");
        
        $("#divMensagens"   ).show("slow");
        
        idEstorno = $(this).val();
        
        console.log(idEstorno)
        
        mensagensTable.draw();
        
    });
    
    $("#btnVoltarDivMsg").on("click", function()
    {
        
        $("#divEstornos"    ).show("slow");
        
        $("#divMensagens"   ).hide("slow");
        
        idEstorno = 0;
        
        estornoParcelaTable.draw();
        
    });
    
    $(document).on("click", ".btnMudarStatus", function()
    {
        
        var idEstorno   = $(this).val   (                   );
        var idStatus    = $(this).attr  ("data-id-status"   );
        var status      = $(this).attr  ("data-status"      );
        
        $("#btnConfirmar"   ).val   (                   idEstorno   );
        $("#btnConfirmar"   ).attr  ("data-id-status",  idStatus    );
        $("#tipoStatus"     ).text  (                   status      );
        
        $("#divEstornos"    ).hide  ("slow"                         );

        $("#divEstornoMsg"  ).show  ("slow"                         );
        
    });
    
    $('#btnConfirmar').on("click",function()
    {   
        
        var idEstorno   = $(this            ).val   (                   );
        var idStatus    = $(this            ).attr  ("data-id-status"   );
        var observacao  = $("#observacao"   ).val   (                   );

        $.ajax(
        {
            type  : "POST"                                  ,
            url   : "/estornoBaixa/confirmarStatusEstorno"  ,
            data  : 
            {
                "idEstorno"     : idEstorno     ,
                "idStatus"      : idStatus      ,
                "observacao"    : observacao
            },
        }).done(function (dRt) 
        {

            var retorno = $.parseJSON(dRt);
            
            $.pnotify(
            {
               title : 'Notificação'      ,
               text  : retorno.msg        ,
               type  : retorno.type
            });
            
            if(!retorno.hasErrors)
            {
        
                $("#divEstornos"    ).show("slow");

                $("#divEstornoMsg"  ).hide("slow");
                
                estornoParcelaTable.draw();
            }
            
        });
        
    });

    $(document).on("click",".btnVoltar", function()
    {
        
        $("#divEstornoMsg"  ).hide("slow" );
        $("#divEstornos"    ).show("slow" );
        
    });
    
    $(document).on("click",".btnVoltarEstorno", function()
    {
        
        var idEstorno = $(this).val();

        $.ajax(
        {
            type  : "POST"                          ,
            url   : "/estornoBaixa/voltarEstorno"   ,
            data  : 
            {
                "idEstorno"     : idEstorno     
            },
        }).done(function (dRt) 
        {

            var retorno = $.parseJSON(dRt);
            
            $.pnotify(
            {
               title : 'Notificação'      ,
               text  : retorno.msg        ,
               type  : retorno.type
            });
            
            if(!retorno.hasErrors)
            {
                estornoParcelaTable.draw();
            }
            
        });
        
    });
    

});