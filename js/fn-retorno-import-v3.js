$(function(){

    $('#bancos_select, #bancos_select2').multiselect({
      buttonWidth                     : '230px',
      enableFiltering                 : true,
      enableCaseInsensitiveFiltering  : true,
    });
    
	$('#upload-retorno-form').ajaxForm({

        beforeSubmit    : function()
        {
            $('#import-resume').hide();
            $('#import-resume-error').hide();
            $('#resume-error-inner').html('');

            if ( !window.File && !window.FileReader && !window.FileList && !window.Blob )
            {
                alert("Por favor, seu navegador não suporta esta tarefa. Contate o Suporte.");            
            }

            $('.panel').block({
                overlayCSS: {
                    backgroundColor: '#fff'
                },
                message: '<img src="https://beta.sigacbr.com.br/js/loading.gif" /> Aguarde...',
                css: {
                    border: 'none',
                    color: '#333',
                    background: 'none'
                }
            });
            window.setTimeout(function () {
                $('.panel').unblock();
            }, 1000);
        },

        uploadProgress  : function(event, position, total, percentComplete)
        {
            $('#progressbox').show();
            $('#progressbar').width(percentComplete + '%') //update progressbar percent complete
            $('#statustxt').html(percentComplete + '%'); //update status text

            if( percentComplete > 50 )
            {
                $('#statustxt').css('color','#000'); //change status text to white after 50%
            }
        },

        success         : function( response, textStatus, xhr, form )
        {
            var retorno = $.parseJSON(response);

            if( retorno.hasErrors )
            {
                $('#import-resume-error').show();

                $('#static').modal('show');

                $.each(retorno.msgs, function(k, v)
                {
                    $('#modal-mens').html('');
                    $('#modal-mens').append('<div class="alert alert-danger"><i class="fa fa-times-circle"></i><strong> <span id="err2"></span>'+v+'</strong></div>');
                });
            }
            else
            {
                $('#forcar_importacao').attr('value', '0');
                $('#liq2').text(retorno.liquidados      );
                $('#regSuc2').text(retorno.registrados  );
                $('#err2').text(retorno.comErro         );
                $('#import-resume').show().fadeOut(6000);
                $('#upload-retorno-form').resetForm();
            }

            $('#progressbox').fadeOut(3000);
        },

        error           : function(xhr, textStatus, errorThrown)
        {
            $('#progressbox').fadeOut(3000);
        }
    });
    
    $('#btn-forcar-importacao').on('click', function(){
        $('#forcar_importacao').attr('value', '0');
        $('#forcar_importacao_obs').attr('value', $('#obs').val());
        $('#static').modal('hide');
        $('#upload-retorno-form').submit();
    });
});