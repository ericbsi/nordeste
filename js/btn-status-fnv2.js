$(document).ready(function () {

    function updateTablePropostas() {

        $('#table-wrapper').load('/proposta/ultimasPropostas/', function () {
            setTimeout(updateTablePropostas, 25000);
        });
    }

    updateTablePropostas();
})

$(document).on('click', '.label-warning', function () {

    $('#input_proposta_id').attr('value', $(this).attr('subid'));

});


var grid_analises = $('#table_id').DataTable({
    "processing": true,
    "serverSide": true,
    "stateSave" : true,
    "ajax": {
        url: '/proposta/ultimasPropostas/',
        type: 'POST',
        "data": function (d) {
            d.codigo_filter = $("#codigo_filter").val()
            //d.nome_filter   = $("#nome_filter").val()
        }
    },
    "columns": [
        {"data": "codigo"},
        {"data": "cliente"},
        {"data": "dadosCli"},
        {"data": "financeira"},
        {"data": "filial"},
        {"data": "politicaCredito"},
        {"data": "valor"},
        {"data": "valorSeguro"},
        {"data": "qtd_parcelas"},
        {"data": "btn"},
        {"data": "data_cadastro"},
        {"data": "tempoEspera"},
    ],
    "drawCallback": function (settings) {
        setTimeout(function () {
            grid_analises.draw();
        }, 20000)
    }
});

$(document).on('change','.input_filter', function(){
    grid_analises.draw();
});