$(function(){

    var gridItens = $('#grid_itens_caracteristica').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/valoresCaracteristica/getItensCaracteristica/',
            type: 'POST',
            "data": function (d) {
                d.caracteristicaId = $("#caracteristicaId").val()
            }
        },
        "language": {
            "processing": "<img style='position:fixed; top:60%; left:50%;margin-top:-8px;margin-left:-8px;' src='https://beta.sigacbr.com.br/js/loading.gif'>"
        },
        "columnDefs": [{
            "orderable": false,
            "targets": "no-orderable"
        }],
        "columns": [
            {"data": "valor"},
        ],
    });

	jQuery.fn.clearFormInputs = function(form){
        return form.find("input[type=text]").val("");
    }

    $.validator.setDefaults({
        errorElement: "span", 
        errorClass: 'help-block',
        
        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
        },
            
        unhighlight: function (element) { 
            $(element).closest('.form-group').removeClass('has-error');
        },

        success: function (label, element) {
            label.addClass('help-block valid');            
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        }
    });

	$('#form-add-item-carac-produto').validate({

		submitHandler : function(){

			$("#cadastro_item_carac_produto_msg_return").hide().empty();

            var formData = $('#form-add-item-carac-produto').serialize();

			$.ajax({
				url  : '/valoresCaracteristica/add/',
				type : 'POST',
				data: formData,

				beforeSend: function() {

                    $('.panel').block({
                        overlayCSS: {
                            backgroundColor: '#fff'
                        },
                        message: '<img src="https://beta.sigacbr.com.br/js/loading.gif" /> Salvando item...',
                        css: {
                            border: 'none',
                            color: '#333',
                            background: 'none'
                        }
                    });
                    window.setTimeout(function() {
                        $('.panel').unblock();
                    }, 1000);
                }
			})
			.done(function(dRt){

				var retorno = $.parseJSON(dRt);
				//console.log(retorno);

                if( !$('#item_carac_produto_checkbox_continuar').is(':checked') )
                {
                    $.pnotify({
                        title    : 'Notificação',
                        text     : retorno.msg,
                        type     : retorno.pntfyClass
                    });

                    $('#modal_form_new_item_carac_produto').modal('hide');
                }
                else
                {
                    $('#cadastro_item_carac_produto_msg_return').prepend('<p>'+retorno.msg+'</p>').fadeIn(100).addClass('alert-success').fadeOut(9000);
                }

                $('#form-add-item-carac-produto').clearFormInputs($('#form-add-item-carac-produto'));
                
                gridItens.draw();
                
			});

		}
	});
});