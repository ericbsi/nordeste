$(document).ready(function () {
    $('#gerar_relatorio_csv').on('click', function(){
        //document.location.href = '/reports/relatorioNordeste?nome_arquivo=parte1&offset=0';
        $.bloquearInterface('<h4>Aguarde... Os arquivos estao sendo gerados! [Gerando 1 de 5]</h4>');
        for (var i = 1; i <= 5; i++) {

            if (i === 1) {
                inicio = 0;
            }
            if (i === 2) {
                inicio = 70000;
            }
            if (i === 3) {
                inicio = 140000;
            }
            if (i === 4) {
                inicio = 210000;
            }
            if (i === 5) {
                inicio = 280000;
            }
            
            $.ajax(
                    {
                        type: "POST",
                        url: "/reports/relatorioNordeste",
                        data: {
                            nome_arquivo: "parte" + i,
                            offset: inicio
                        },
                        success: function(data){
                            $.desbloquearInterface();
                            var parte = document.querySelectorAll('a.down_rel').length;
                            var resp = data;
                            var a = $("<a />", {
                                class: "down_rel",
                                href: URL.createObjectURL(new Blob([resp], {
                                    type: "text/csv"
                                })),
                                "download": "relatorio_"+(parte+1)+".csv"
                            });
                            $("body").append(a);
                            a[0].click();
                        },
                        error: function(error){
                            console.log(error);
                        }
                    }).done(function(){
                        var atual = document.querySelectorAll('a.down_rel').length;
                        if(atual < 5){
                            $.bloquearInterface('<h4>Aguarde... Os arquivos estao sendo gerados! [Gerando '+(atual+1)+' de 5]</h4>');
                        }else{
                            $.desbloquearInterface();
                        }
                    });
        }
    });
});