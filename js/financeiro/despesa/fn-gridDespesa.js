$(document).ready(function ()
{
    
    var idDespesa       ;
    var parcelaTable    ;
    
    var filtroDivergencia = [0,0,0,0];

    $('.select2').select2();

    $('#btnSalvar').on('click', function () {

        $.ajax({
            type: "POST",
            url: "/financeiro/salvarDespesa/",
            data: {
                "descricao": $('#descricao').val(),
                "fornecedor": $('#fornecedor').val(),
                "natureza": $('#natureza').val(),
                "dataDespesa": $('#dataDespesa').val(),
                "serieNF": $('#serieNF').val(),
                "docNF": $('#docNF').val(),
                "valor": $('#valor').val(),
                "rateio": $('#rateio').val(),
                "carencia": $('#carencia').val(),
                "intervalo": $('#intervalo').val()
            }
        }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            $.pnotify({
                title: 'Notificação',
                text: retorno.msg,
                type: retorno.pntfyClass
            });

            if (!(retorno.hasErrors)) {

                $('#descricao').val('');
                $('#fornecedor').val(0).change();
                $('#natureza').val(0).change();
                $('#dataDespesa').val('');
                $('#serieNF').val('');
                $('#docNF').val('');
                $('#valor').val('0');
                $('#rateio').val(0);
                $('#carencia').val(0);
                $('#intervalo').val(0);

                despesaTable.draw();
            }

        })

    });

    var despesaTable = $('#grid_despesas').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax":
                {
                    url     :   '/financeiro/listarDespesas'  ,
                    type    :   'POST'                        ,
                    data    :   function(d){
                                    d.filtroDescricao   = $('#filtroDespesa'    ).val() ,
                                    d.filtroFornecedor  = $('#filtroFornecedor' ).val() ,
                                    d.filtroData        = $('#filtroData'       ).val() ,
                                    d.filtroNatureza    = $('#filtroNatureza'   ).val() ,
                                    d.filtroSerieNF     = $('#filtroSerie'      ).val() ,
                                    d.filtroDocNF       = $('#filtroDocumento'  ).val()
                                }
                },
        "columns": [
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            {
                "data": "descricao",
                "className": "descricao",
            },
            {
                "data": "fornecedor",
                "className": "fornecedor",
            },
            {
                "data": "natureza",
                "className": "natureza",
            },
            {
                "data": "dataDespesa",
                "className": "dataDespesa",
            },
            {
                "data": "serieNF",
                "className": "serieNF",
            },
            {
                "data": "docNF",
                "className": "docNF",
            },
            {"data": "btnRemover"},
        ],
    });

    var gridParcelasTable = $('#grid_parcelas').DataTable({
        "processing"  : true,
        "serverSide"  : true,
        "ajax"        : {
                            url     :   '/financeiro/getParcelasDespesa'    ,
                            type    :   'POST'                              ,
                            data    :   function(d) {
                                                        d.filtroDivergencia = filtroDivergencia,
                                                        d.filtroDespesa2 = $('#filtroDespesa2').val(),
                                                        d.filtroVencimento = $('#filtroVencimento').val(),
                                                        d.filtroVencimento2 = $('#filtroVencimento2').val(),
                                                        d.filtroDataBaixa = $('#filtroDataBaixa').val(),
                                                        d.filtroDataBaixa2 = $('#filtroDataBaixa2').val()
                                                    }

                        },
        "columns":  [
                        { 
                            "data"      : "descricaoDespesa"      
                        },
                        { 
                            "data"      : "parcela"      
                        },
                        { 
                            "data"      : "vencimento"          ,
                            "className" : "vencimentoParcela"
                        },
                        { 
                            "data"      : "valor"               ,
                            "className" : "valorParcela"
                        },
                        { 
                            "data"      : "dataBaixa"           ,
                            "className" : "dataBaixaParcela"
                        },
                        { 
                            "data"      : "valorJuros"          ,
                            "className" : "valorJurosParcela"
                        },
                        { 
                            "data"      : "valorDesconto"       ,
                            "className" : "valorDescontoParcela"
                        },
                        { 
                            "data"      : "valorBaixa"          ,
                            "className" : "valorBaixaParcela"
                        },
                        { 
                            "data"      : "btnBaixar"
                        }
                    ]
    });
    
    $(document).on('click','.checkFiltro',function()
    {
        var checkBoxes  = $('.checkFiltro')                     ;
        var indice      = parseInt($(this).attr('value')) - 1   ;
        
        if($(this).hasClass('clip-checkbox-unchecked-2'))
        {
            $(this).removeClass ('clip-checkbox-unchecked-2'    );
            $(this).addClass    ('clip-square'                  );
            
            filtroDivergencia[indice] = 1;
            
            
        }
        else
        {
            $(this).removeClass ('clip-square'                  );
            $(this).addClass    ('clip-checkbox-unchecked-2'    );
            
            filtroDivergencia[indice] = 0;
            
            
        }
        
        gridParcelasTable.draw();
        
        console.log(filtroDivergencia);
        
    });
    
    /*$('.filtro').on('change',function()
    {
        despesaTable.draw();
    })*/
    
    $(document).on('change','.inputBaixa',function()
    {
        
        var tr          = $(this).closest('tr')         ;
        var row         = gridParcelasTable.row(tr)     ;
        var rowData     = row.data()                    ;
        
        var idParcela  = $(this).attr('data-idparcela');
        
        var elementoVlrPago = $("input.form.form-control.inputBaixa.valorPago[data-idparcela='"     + idParcela + "']");
        var elementoVlrMora = $("input.form.form-control.inputBaixa.valorJuros[data-idparcela='"    + idParcela + "']");
        var elementoVlrDesc = $("input.form.form-control.inputBaixa.valorDesconto[data-idparcela='" + idParcela + "']");
        
        var valorBaixa      = 0;
        var valorMora       = 0;
        var valorDesconto   = 0;
        
        if($(this).val() < 0)
        {
            $(this).val(0);

            $.pnotify({
                title   : 'Notificação'                 ,
                text    : "Somentes valores positivos"  ,
                type    : "info"
            });
        }
        
        if ($(this).is(elementoVlrPago))
        {
            
            if($(this).val() > rowData.valorParcela)
            {
                valorMora = ($(this).val() - rowData.valorParcela)  ; //valor diferença pra cima
                elementoVlrMora.val(valorMora   )                   ;
                elementoVlrDesc.val(0           )                   ;
            }
            else if($(this).val() < rowData.valorParcela)
            {
                valorDesconto = (rowData.valorParcela - $(this).val())  ; //valor diferença pra cima
                elementoVlrDesc.val(valorDesconto   )                   ;
                elementoVlrMora.val(0               )                   ;
            }
            else
            {
                elementoVlrDesc.val(0);
                elementoVlrMora.val(0);
            }
            
        }
        else
        {
            valorBaixa = ((parseInt(rowData.valorParcela) - parseInt(elementoVlrDesc.val())) + parseInt(elementoVlrMora.val()));
            elementoVlrPago.val(valorBaixa);
        }
        
    });
    
    // Add event listener for opening and closing details
    $('#grid_despesas tbody').on('click', 'td.details-control', function () {

        var tr          = $(this).closest('tr')         ;
        var row         = despesaTable.row(tr)          ;
        var rowsTam     = despesaTable.rows()[0].length ;
        var rowData     = row.data()                    ;
        
        idDespesa   = rowData.idDespesa             ;
        
        console.log(rowData     );
        console.log(idDespesa   );

        if (row.child.isShown()) {

            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {

            for (i = 0; i < rowsTam; i++)
            {

                if (despesaTable.row(i).child.isShown()) {
                    despesaTable.row(i).child.hide();
                }
            }

            $('td.details-control').closest('tr').removeClass('shown');

            formatSubTable(row.data(), tr, row);
            
            parcelaTable = $('#parcelas').dataTable(
            {
                "processing"  : true,
                "serverSide"  : true,
                "ajax"        : {
                                    url     :   '/financeiro/getParcelasDespesa'    ,
                                    type    :   'POST'                              ,
                                    data    :   function(d) {
                                                                d.idDespesa = idDespesa
                                                            }
                  
                                },
                "columns":  [
                                { 
                                    "data"      : "parcela"      
                                },
                                { 
                                    "data"      : "vencimento"          ,
                                    "className" : "vencimentoParcela"
                                },
                                { 
                                    "data"      : "valor"               ,
                                    "className" : "valorParcela"
                                },
                                { 
                                    "data"      : "dataBaixa"           ,
                                    "className" : "dataBaixaParcela"
                                },
                                { 
                                    "data"      : "valorBaixa"          ,
                                    "className" : "valorBaixaParcela"
                                },
                                { 
                                    "data"      : "btnBaixar"
                                }
                            ]
            });

        }
    });
    
    $(document).on('click','.btnBaixa',function()
    {
        
        var elementoVlrPago = $("input.form.form-control.valorPago[data-idparcela='16944']");
                
        var tipo        = $(this).attr('data-tipo') ;
        var idEntidade  = $(this).val()             ;
        
        var elementoVlrPago = $("input.form.form-control.inputBaixa.valorPago[data-idparcela='"     + idEntidade + "']");
        var elementoVlrMora = $("input.form.form-control.inputBaixa.valorJuros[data-idparcela='"    + idEntidade + "']");
        var elementoVlrDesc = $("input.form.form-control.inputBaixa.valorDesconto[data-idparcela='" + idEntidade + "']");
        var elementoDatBaix = $("input.form.form-control.inputBaixa.dataBaixa[data-idparcela='"     + idEntidade + "']");
        
        $.ajax  
        ({
            type    :   "POST",
            url     :   "/financeiro/baixarParcelaDespesa/",
            data    :   {
                            "tipo"          : tipo                  ,
                            "idEntidade"    : idEntidade            ,
                            "dataBaixa"     : elementoDatBaix.val() ,
                            "valorDesconto" : elementoVlrDesc.val() ,
                            "valorJuros"    : elementoVlrMora.val() ,
                            "valorPago"     : elementoVlrPago.val()
                        }
        }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            $.pnotify({
                title: 'Notificação',
                text: retorno.msg,
                type: retorno.pntfyClass
            });
            
            if(!retorno.hasErrors)
            {
                gridParcelasTable.draw();
            }
            
        });
        
    });
    
    $(document).on('click','.btnRemover',function()
    {
        
        var idDespesa = $(this).val();
        
        $.ajax  
        ({
            type    :   "POST",
            url     :   "/financeiro/excluirDespesa/",
            data    :   {
                            "idDespesa" : idDespesa
                        }
        }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            $.pnotify({
                title   : retorno.title ,
                text    : retorno.msg   ,
                type    : retorno.type
            });
            
            if(!retorno.hasErrors)
            {
                despesaTable.draw();
            }
            
        });
        
    });

    //////////////////////////////////////////////////////////////////////////
    // funcao que irá capturar o click duplo na célula de senha e irá       //
    // habilitar para edição da mesma. caso a mesma sofra alguma alteração, //
    // salve o conteúdo no banco                                            //
    //////////////////////////////////////////////////////////////////////////
    // Autor : André Willams // Data : 19-11-2015 ////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    $(document).on('dblclick', '.vencimentoParcela', function () {

        var conteudoOriginal = $(this).text(); //resgate o conteúdo atual da célula
        
        var tr          = $(this).closest('tr')     ;
        var row         = gridParcelasTable.row(tr) ;
        var rowData     = row.data()                ;
        var idParcela   = rowData.idParcela         ;
        var elemento    = $(this)                   ;

        //transforme o elemento em um input
        $(this).html("<input id='mudaVencimento' class='form-control' type='date' value='" + conteudoOriginal + "' />");

        $(this).children().first().focus(); //atribua o foco ao elemento criado

        $(this).children().first().keypress(function (e) { //quando alguma tecla for pressionada

            if (e.which == 13) { //caso a tecla pressionada seja o ENTER, foi confirmada a alteração

                var novoConteudo = $(this).val(); //pegue o novo conteúdo

                //chame o ajax de alteração
                $.ajax({
                    type: "POST"                                ,
                    url : "/financeiro/mudarVencimentoParcela"  ,
                    data: 
                    {
                        "idParcela"     : idParcela     ,
                        "vencimento"    : novoConteudo  
                    },
                }).done(function (dRt) {

                    var retorno = $.parseJSON(dRt);

                    $.pnotify({
                        title   : 'Notificação' ,
                        text    : retorno.msg   ,
                        type    : retorno.type
                    });
                    
                    if(!retorno.hasErrors)
                    {
                        elemento.text(retorno.novoVencimento);
                    }

                })

            }
        });

        //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
        $(this).children().first().blur(function () {

            //devolva o conteúdo original
            $(this).parent().text(conteudoOriginal);

        });

    });
    
    $(function(){
        $("#grid_despesas input").keyup(function(){       
            despesaTable.draw();
        });
    });

    $(function(){
        $("#grid_parcelas input").keyup(function(){
            gridParcelasTable.draw();
        });
        $("#grid_parcelas input").on('change', function(){
            gridParcelasTable.draw();
        });
    });
});

/*$(document).on('change','#grid_despesas input', function()
{
   
});

$(function(){
    $("#grid_despesas input").keyup(function(){       
        var index = $(this).parent().index();
        var nth = "#grid_despesas td:nth-child("+(index+1).toString()+")";
        var valor = $(this).val().toUpperCase();
        $("#grid_despesas tbody tr").show();
        $(nth).each(function(){
            if($(this).text().toUpperCase().indexOf(valor) < 0){
                $(this).parent().hide();
            }
        });
    });
 
    $("#grid_despesas input").blur(function(){
        $(this).val("");
    });
});*/

function formatSubTable(d, tr, row) {

    tr.addClass('shown');

    // `d` is the original data object for the row
    var data =  '<h5><i class="clip-list"></i> Parcelas</h5>' +
                '<div class="row">' +
                '   <div  class="col-sm-12">' +
                '       <table id="parcelas" class="table table-striped table-bordered table-hover table-full-width dataTable">' +
                '           <thead>' +
                '               <tr>' +
                '                   <th class="no-orderable">' +
                '                       Parcela' +
                '                   </th>' +
                '                   <th class="no-orderable">' +
                '                       Vencimento' +
                '                   </th>' +
                '                   <th class="no-orderable">' +
                '                       Valor' +
                '                   </th>' +
                '                   <th class="no-orderable">' +
                '                       Data Baixa' +
                '                   </th>' +
                '                   <th class="no-orderable">' +
                '                       Valor Baixa' +
                '                   </th>' +
                '                   <th class="no-orderable" width="3%">' +
                '                   </th>' +
                '               </tr>' +
                '           </thead>' +
                '           <tbody>' +
                '           </tbody>' +
                '       </table>' +
                '   </div>' +
                '</div>';

    row.child(data).show();
}