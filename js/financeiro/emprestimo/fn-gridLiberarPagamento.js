$(document).ready(function()
{
    var tf_semear = 0;
    
    var liberarTable = $('#grid_liberar').DataTable(
    {
        "sDom": 'T<"clear">lfrtip',
        "tableTools":
                {
                    "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
                },
        "processing": true,
        "serverSide": true,
        "ajax":
                {
                    url     :   '/emprestimo/gridLiberarPagamento'  ,
                    type    :   'POST'                              ,
                    data: function(d){
//                        semear : tf_semear
                        d.semear = $('#filter_semear').prop('checked');
                    }
                },
        "columns":
                [
                    {
                        "className"         : 'details-control' ,
                        "orderable"         : false             ,
                        "data"              : null              ,
                        "defaultContent"    : ''
                    },
                    {
                        "data"  : "proposta"
                    },
                    {
                        "data"  : "nomeLoja"
                    },
                    {
                        "data"  : "nomeCrediarista"
                    },
                    {
                        "data"  : "dataProposta"
                    },
                    {
                        "data"  : "cliente"
                    },
                    {
                        "data"  : "cpf"
                    },
                    {
                        "data"  : "dadosBancarios"
                    },
                    {
                        "data"  : "valor"
                    },
                    {
                        "data"  : "btnAprovar"
                    }
                ],
    });

    // Add event listener for opening and closing details
    $('#grid_liberar tbody').on('click', 'td.details-control', function () 
    {

        var tr      = $(this).closest('tr')         ;
        var row     = liberarTable.row(tr)          ;
        var rowsTam = liberarTable.rows()[0].length ;
        var rowData = row.data()                    ;

        if (row.child.isShown()) {

           // This row is already open - close it
           row.child.hide();
           tr.removeClass('shown');
        }
        else 
        {

            for (i = 0; i < rowsTam; i++)
            {

                if (liberarTable.row(i).child.isShown()) 
                {
                    liberarTable.row(i).child.hide();
                }
            }

            $('td.details-control').closest('tr').removeClass('shown');

            tr.addClass('shown');
                
/*            console.log(rowData.anexos);
            console.log(rowData.anexos.length);

            if(rowData.anexos.length > 0)
            {*/
//                formatSubTable(row.data(), tr, row, rowData.linksContrato, rowData.idsAnexo);
                formatSubTable(row.data(), tr, row, rowData.anexos);
//            }

        }
    });

    $(document).on('click'  , '.btnAprovar'         , function ()
    {

        $.ajax(
        {
            type    : "POST"                            ,
            url     : "/emprestimo/aprovarLiberacao"    ,
            data    : 
            {
                "idParcela" : $(this).val()
            },
        }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            $.pnotify(
            {
                title   : 'Notificação'         ,
                text    : retorno.msg           ,
                type    : retorno.style
            });

            if (!retorno.hasErrors)
            {
                liberarTable.draw();
            }

        });
        
    });
    
    $('#filter_semear').on('click', function()
    {
        
        console.log($('#filter_semear').prop('checked'));
        liberarTable.draw();
    });
    
    liberarTable.on("draw",function()
    {
        console.log($('#filter_semear').prop('checked'));
    })
    
    $(document).on('click'  , '.btnExcluirContrato' , function ()
    {

        $.ajax(
        {
            type    : "POST"                            ,
            url     : "/emprestimo/excluirContratoAnexo"    ,
            data    : 
            {
                "idAnexo" : $(this).val()
            },
        }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            $.pnotify(
            {
                title   : 'Notificação'         ,
                text    : retorno.msg           ,
                type    : retorno.style
            });

            if (!retorno.hasErrors)
            {
                liberarTable.draw();
            }

        });
        
    });
    
})

//function formatSubTable(d, tr, row, linksContrato, idsAnexo) 
function formatSubTable(d, tr, row, anexos) 
{

    var data = '';
    
    console.log(anexos);
    
    // `d` is the original data object for the row
    data += '<div class="panel panel-default">' +
            '   <div class="panel-heading">' +
            '       <i class="fa fa-external-link-square"></i>' +
            '       Anexo Contrato' +
            '       <div class="panel-tools">' +
            '           <a class="btn btn-xs btn-link panel-collapse collapses" href="#">' +
            '           </a>' +
            '       </div>' +
            '   </div>' +
            '   <div class="panel-body expand">';

    $.each(anexos.contrato, function(index,anexo)
    {
      
        data += '       <div class="row">' +
                '           <p align="center">' +
                '               <a href="' + anexo.relative_path + '" target="blank" title="Clique na imagem para ampliar">' +
                '                   <img src="' + anexo.relative_path + '" alt="Clique na imagem..."  height="600" width="400">' +
                '               </a>' +
                '           </p>' +
                '       </div>' +
                '       <div class="row">' +
                '           <p align="center">' +
                '               <button class="btn btn-red btnExcluirContrato" value="' + anexo.id + '">' +
                '                   <i class="clip-cancel-circle-2"></i> Excluir Anexo' +
                '               </button>' +
                '           </p>' +
                '       </div>';
        
    });
    
    data +=
        '   </div>' +
        '</div>';
    
    // `d` is the original data object for the row
    data += '<div class="panel panel-default">' +
            '   <div class="panel-heading">' +
            '       <i class="fa fa-external-link-square"></i>' +
            '       Anexos Proposta' +
            '       <div class="panel-tools">' +
            '           <a class="btn btn-xs btn-link panel-collapse collapses" href="#">' +
            '           </a>' +
            '       </div>' +
            '   </div>' +
            '   <div class="panel-body expand">';

    $.each(anexos.proposta, function(index,anexo)
    {
      
        data += '       <div class="row">' +
                '           <p align="center">' +
                '               <a href="' + anexo.relative_path + '" target="blank" title="Clique na imagem para ampliar">' +
                '                   <img src="' + anexo.relative_path + '" alt="Clique na imagem..."  height="600" width="400">' +
                '               </a>' +
                '           </p>' +
                '       </div>' +
                '       <div class="row">' +
                '           <p align="center">' +
                '               <button class="btn btn-red btnExcluirContrato" value="' + anexo.id + '">' +
                '                   <i class="clip-cancel-circle-2"></i> Excluir Anexo' +
                '               </button>' +
                '           </p>' +
                '       </div>';
        
    });
    
    data +=
        '   </div>' +
        '</div>';

    row.child(data).show();
      
}