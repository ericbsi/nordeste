$(document).ready(function ()
{   
    /*Formulario de anexo de comprovante de pagamento*/
    $('#form-add-att').ajaxForm({
        
        beforeSubmit: function () 
        {

            if ($('#ComprovanteFile').val() != '')
            {
                if (window.File && window.FileReader && window.FileList && window.Blob && $('#form-add-att').valid())
                {
                    var fsize = $('#ComprovanteFile')[0].files[0].size;
                    var ftype = $('#ComprovanteFile')[0].files[0].type;

                    switch (ftype)
                    {
                        case 'image/png'        :
                        case 'image/gif'        :
                        case 'image/jpeg'       :
                        case 'image/pjpeg'      :
                        case 'application/pdf'  :
                           break;
                        default:
                           alert("Tipo de arquivo não permitido!");
                           return false;
                    }

                    if (fsize > 5242880)
                    {
                        alert("Arquivo muito grande!");
                        return false
                    }
                }

                else
                {
                   alert("Revise o formulário!");
                }
            }
        },
        success: function (response, textStatus, xhr, form) 
        {
            var retorno = $.parseJSON(response);
            
            $.pnotify(
            {
               title : 'Notificação'      ,
               text  : retorno.msg        ,
               type  : retorno.pntfyClass
            });

            if (!retorno.hasErrors)
            {
                pagarTable.draw();
                $('#modal_form_new_att').modal('hide');
            }
                
        }
    });

    $('#observacao').on('change',function()
    {
        
        var obs = $(this).val();
        
        $('#observacaoHdn').attr('value',obs);
        
    })

    $('#dataEstorno').on('change',function()
    {
        
        var dataEstorno = $(this).val();
        
        $('#dataEstornoHdn').attr('value',dataEstorno);
        
    })

    $('#comprovanteEstorno').on('change',function()
    {
        
        var comprovanteEstorno = $(this).val();
        
        $('#comprovanteHdn').attr('value',comprovanteEstorno);
        
    })
    
    $("#voltarTablePagos").on("click",function()
    {
        
        $('#divTablePagos'              ).show('slow'   )   ;
        $('#divTablePagar'              ).show('slow'   )   ;
        
        $('#infoEstorno'                ).hide('slow'   )   ;
        $('#confirmaDesfazLiberacao'    ).hide('slow'   )   ;
        
        $('#comprovanteEstorno'         ).attr('value',"");
        $('#observacao'                 ).attr('value',"");
        
    });

    var pagarTable = $('#grid_pagar').DataTable(
    {
        "sDom"          : 'T<"clear">lfrtip'            ,
        "tableTools"    :
        {
            "sSwfPath"  : "/swf/copy_csv_xls_pdf.swf"
        },
        "processing"    : true                          ,
        "serverSide"    : true                          ,
        "ajax"          :
        {
            url         : '/emprestimo/gridEmprestimos' ,
            type        : 'POST'                        ,
            data        : function(d)
            {
                d.pago = 0;
            }
        },
        "columns":
        [
            {
                "className"         : 'details-control' ,
                "orderable"         : false             ,
                "data"              : null              ,
                "defaultContent"    : ''
            },
            {
                "data"  : "proposta"
            },
            {
                "data"  : "dataProposta"
            },
            {
                "data"  : "dataLiberacao"
            },
            {
                "data"  : "cliente"
            },
            {
                "data"  : "cpf"
            },
            {
                "data"  : "dadosBancarios"
            },
            {
                "data"  : "valor"
            },
            {
                "data"  : "alerta"
            },
            {
                "data"  : "btnEstornar"
            }
        ]
    });
    
    var pagosTable = $('#grid_pagos').DataTable(
    {
        "sDom"          : 'T<"clear">lfrtip'                ,
        "tableTools"    :
        {
            "sSwfPath"  : "/swf/copy_csv_xls_pdf.swf"
        },
        "processing"    : true                              ,
        "serverSide"    : true                              ,
        "ajax"          :
        {
            url         :   '/emprestimo/gridEmprestimos'   ,
            type        :   'POST'                          ,
            data        :   function(d)
            {
                d.pago = 1;
            }
        },
        "columns":
        [
            {
                "className"         : 'details-control-2'   ,
                "orderable"         : false                 ,
                "data"              : null                  ,
                "defaultContent"    : ''
            },
            {
                "data"  : "proposta"
            },
            {
                "data"  : "dataProposta"
            },
            {
                "data"  : "dataLiberacao"
            },
/*                    {
                "data"  : "dataPagamento"
            },*/
            {
                "data"  : "cliente"
            },
            {
                "data"  : "cpf"
            },
            {
                "data"  : "dadosBancarios"
            },
            {
                "data"  : "valor"
            },
            {
                "data"  : "alerta"
            },
            {
                "data"  : "btnEstornar"
            }
        ]
    });

    // Add event listener for opening and closing details
    $('#grid_pagar tbody').on('click', 'td.details-control', function () 
    {

        var tr      = $(this).closest('tr')         ;
        var row     = pagarTable.row(tr)            ;
        var rowsTam = pagarTable.rows()[0].length   ;
        var rowData = row.data()                    ;

        if (row.child.isShown()) {

           // This row is already open - close it
           row.child.hide();
           tr.removeClass('shown');
        }
        else 
        {

           for (i = 0; i < rowsTam; i++)
           {

              if (pagarTable.row(i).child.isShown()) 
              {
                 pagarTable.row(i).child.hide();
              }
           }

           $('td.details-control').closest('tr').removeClass('shown');

           // Open this row
           /*row.child(format(row.data())).show();
            tr.addClass('shown');*/

           formatSubTable(row.data(), tr, row, rowData.valorNumero);

        }
    });

    // Add event listener for opening and closing details
    $('#grid_pagos tbody').on('click', 'td.details-control-2', function () 
    {

        var tr      = $(this).closest('tr')         ;
        var row     = pagosTable.row(tr)            ;
        var rowsTam = pagosTable.rows()[0].length   ;
        var rowData = row.data()                    ;

        if (row.child.isShown()) {

           // This row is already open - close it
           row.child.hide();
           tr.removeClass('shown');
        }
        else 
        {

           for (i = 0; i < rowsTam; i++)
           {

              if (pagarTable.row(i).child.isShown()) 
              {
                 pagarTable.row(i).child.hide();
              }
           }

           $('td.details-control-2').closest('tr').removeClass('shown');

           formatSubTablePagos(rowData, tr, row);

        }
    });

    // Add event listener for opening and closing details
    $(document).on('click', '.btnEstorno', function () 
    {

/*        console.log($(this));
        console.log($(this).parent());
        console.log($(this).parent().closest('tr'));*/
        
        var tr          = $(this).parent().closest('tr')    ;
        var row         = pagosTable.row(tr)                ;
        var rowsTam     = pagosTable.rows()[0].length       ;
        var rowData     = row.data()                        ;
        var elemento    = $(this)                           ;
        
        $('#divTablePagos'  ).hide('slow');
        $('#divTablePagar'  ).hide('slow');
        
        $('#infoEstorno'    ).show('slow');
        
        $('#idBaixa'                ).attr('value',rowData.idBaixa          );
        $('#dataBaixa'              ).attr('value',rowData.dataBaixa        );
        $('#comprovantePagamento'   ).attr('value',rowData.comprovantePG    );
        
    });

    // Add event listener for opening and closing details
    $(document).on('click', '.btnDesfaz', function () 
    {

/*        console.log($(this));
        console.log($(this).parent());
        console.log($(this).parent().closest('tr'));*/
        
        var tr          = $(this).parent().closest('tr')    ;
        var row         = pagarTable.row(tr)                ;
        var rowsTam     = pagarTable.rows()[0].length       ;
        var rowData     = row.data()                        ;
        var elemento    = $(this)                           ;
        
        $('#divTablePagos'              ).hide('slow');
        $('#divTablePagar'              ).hide('slow');
        
        $('#confirmaDesfazLiberacao'    ).show('slow');
        
        $('#retornarLiberar'            ).attr('value',$(this).val());
        
    });
    
    $('#salvarEstorno').on('click', function ()
    {
        

        $.ajax(
        {
            type  : "POST"                  ,
            url   : "/emprestimo/estornar"  ,
            data  : 
            {
                "idBaixa"               : $('#idBaixa'              ).val(),
                "dataEstorno"           : $('#dataEstorno'          ).val() ,
                "dataRetorno"           : $('#dataRetorno'          ).val() ,
                "comprovanteEstorno"    : $('#comprovanteEstorno'   ).val() ,
                "observacao"            : $('#observacao'           ).val() ,
            },
        }).done(function (dRt) 
        {

            var retorno = $.parseJSON(dRt);


            $.pnotify(
            {
               title : 'Notificação'      ,
               text  : retorno.msg        ,
               type  : retorno.type
            });

            if (!retorno.hasErrors)
            {
                pagosTable.draw();
        
                $('#divTablePagos'  ).show('slow'   )   ;
                $('#divTablePagar'  ).show('slow'   )   ;

                $('#infoEstorno'    ).hide('slow'   )   ;

                $('#comprovanteEstorno' ).attr('value',"");
                $('#observacao'         ).attr('value',"");


            }
        });

    });
    
    $('#retornarLiberar').on('click', function ()
    {
        
        var idParcela = $(this).val();

        $.ajax(
        {
            type  : "POST"                          ,
            url   : "/emprestimo/retornarLiberar"   ,
            data  : 
            {
                "idParcela"     : idParcela                     ,
                "observacao"    : $('#observacaoDesfaz' ).val() ,
            },
        }).done(function (dRt) 
        {

            var retorno = $.parseJSON(dRt);

            $.pnotify(
            {
               title : 'Notificação'      ,
               text  : retorno.msg        ,
               type  : retorno.style
            });

            if (!retorno.hasErrors)
            {
                pagarTable.draw();
        
                $('#divTablePagos'              ).show('slow'       );
                $('#divTablePagar'              ).show('slow'       );

                $('#confirmaDesfazLiberacao'    ).hide('slow'       );
                
                $('#observacaoDesfaz'           ).attr('value',""   );


            }
        });

    });

    $(document).on('click', '.btnPagar', function ()
    {
        var idParcela;

        if (confirm('Deseja realmente efetuar este Pagamento?'))
        {

            idParcela = $(this).val();

/*            var post = $.ajax(
            {
                type  : "POST"              ,
                url   : "/emprestimo/pagar" ,
                data  : 
                {
                    "idParcela"     : idParcela                     ,
                    "contaDebito"   : $('#contaDebito'      ).val() ,
                    "valorPago"     : $('#valorPago'        ).val() ,
                    "comprovante"   : $('#comprovante'      ).val() ,
                    "dataPagamento" : $('#dataPagamento'    ).val() ,
                    "contaCredito"  : $('#contaCredito'     ).val() ,
                    "observacao"    : $('#observacao'       ).val() ,
                },
            })

            post.done(function (dRt) 
            {

                var retorno = $.parseJSON(dRt);


                $.pnotify(
                {
                   title : 'Notificação'      ,
                   text  : retorno.msg        ,
                   type  : retorno.pntfyClass
                });

                if (!retorno.hasErrors)
                {
                    pagarTable.draw();*/

                    //$('#DadosPagamentoId').val(retorno.DPid);
                    
                    $("#idParcelaHdn"       ).val(idParcela                     );
                    $("#contaDebitoHdn"     ).val($('#contaDebito'      ).val() );
                    $("#valorPagoHdn"       ).val($('#valorPago'        ).val() );
                    $("#comprovanteHdn"     ).val($('#comprovante'      ).val() );
                    $("#dataPagamentoHdn"   ).val($('#dataPagamento'    ).val() );
                    $("#contaCreditoHdn"    ).val($('#contaCredito'     ).val() );
                    $("#observacaoHdn"      ).val($('#observacao'       ).val() );

                    /*Sobe tela para anexar*/
                    $('#modal_form_new_att').modal('show');

/*                }
            });*/
        }

    });

    $(document).on('blur', '.formBaixa', function ()
    {
        habilitaBaixa();
    });

    $(document).on('change', '.formBaixa', function ()
    {
        habilitaBaixa();
    });

    function habilitaBaixa()
    {

        if  (   ($.trim($('#contaDebito'    ).val()) !== "0") &&
                ($.trim($('#valorPago'      ).val()) !== "" ) &&
                ($.trim($('#comprovante'    ).val()) !== "" ) &&
                ($.trim($('#dataPagamento'  ).val()) !== "" ) &&
                ($.trim($('#contaCredito'   ).val()) !== "0")
            )
        {
           $('#btnPagar').removeAttr('disabled');
        }
        else
        {
            $('#btnPagar').attr('disabled', 'disabled');
        }
    }

})


function formatSubTable(d, tr, row, valorBaixa) {

   $.ajax(
            {
                type    : "POST"                            ,
                url     : "/emprestimo/getDadosBancarios"   ,
                data    : 
                {
                    "idParcela" : d.idParcela   ,
                    "idDB"      : d.idDB
                },
           }).done(function (dRt)
   {

      var retorno = $.parseJSON(dRt);

      tr.addClass('shown');

      // `d` is the original data object for the row
      var data = '<table class="table table-striped table-hover table-full-width dataTable">' +
              '   <thead>' +
              '      <tr>' +
              '         <th></th>' +
              '         <th colspan="4"><h4><p align="center">Informações da Baixa</p></h4></th>' +
              '         <th>' +
              '            <p align="right">' +
              '               <button id="btnPagar" disabled class="btn btn-green btnPagar" value="' + d.idParcela + '">' +
              '                  <i class="clip-banknote"> Baixar</i>' +
              '               </button>' +
              '            </p>' +
              '         </th>' +
              '      </tr>' +
              '      <tr>' +
              '         <td colspan="6"><b>Dados Bancários</b></td>' +
              '      </tr>' +
              '   </thead>' +
              '   <tbody>' +
              '      <tr>' +
              '         <th>Conta Débito:</th>' +
              '         <th colspan="5">' +
              '            <select id="contaDebito" class="form-control select2 formBaixa">' + retorno.empresa + '</select>' +
              '         </th>' +
              '      </tr>' +
              '      <tr>' +
              '         <td>' +
              '            Valor' +
              '         </td>' +
              '         <td><input class="formBaixa" id="valorPago" type="number" value=' + valorBaixa + '></td>' +
              '         <td>' +
              '            Nº Comprovante' +
              '         </td>' +
              '         <td><input class="formBaixa" id="comprovante" type="text"></td>' +
              '         <td>' +
              '            Data' +
              '         </td>' +
              '         <td><input class="formBaixa" id="dataPagamento" type="date" value="' + retorno.dataBaixa + '"></td>' +
              '      </tr>' +
              '      <tr>' +
              '         <th colspan="6"> </th>' +
              '      </tr>' +
              '      <tr>' +
              '         <th>Conta Crédito:</th>' +
              '         <th colspan="5">' +
              '            <select id="contaCredito" class="form-control select2 formBaixa">' + retorno.cliente + '</select>' +
              '         </th>' +
              '      </tr>' +
              '      <tr>' +
              '         <th>Observação:</th>' +
              '         <th colspan="5">' +
              '            <input class="formBaixa" id="observacao" type="text" style="width : 100%!important">' +
              '         </th>' +
              '      </tr>' +
              '   </tbody>' +
              '</table>';

      row.child(data).show();


   });
}

function formatSubTablePagos(d, tr, row) 
{

   $.ajax(
            {
                type    : "POST"                                ,
                url     : "/emprestimo/getDadosBancariosPagos"  ,
                data    : 
                {
                    "idParcela" : d.idParcela   
                },
           }).done(function (dRt)
   {

      var retorno = $.parseJSON(dRt);

      tr.addClass('shown');

      // `d` is the original data object for the row
      var data = '<table class="table table-striped table-hover table-full-width dataTable">' +
              '   <thead>' +
              '      <tr>' +
              '         <th></th>' +
              '         <th colspan="4"><h4><p align="center">Informações da Baixa</p></h4></th>' +
              '         <th>' +
              '         </th>' +
              '      </tr>' +
              '      <tr>' +
              '         <td colspan="6"><b>Dados Bancários</b></td>' +
              '      </tr>' +
              '   </thead>' +
              '   <tbody>' +
              '      <tr>' +
              '         <th>Conta Débito:</th>' +
              '         <th colspan="5">' +
              '            ' + retorno.empresa + 
              '         </th>' +
              '      </tr>' +
              '      <tr>' +
              '         <td>' +
              '            Valor' +
              '         </td>' +
              '         <td>' +
              '             ' + retorno.valorBaixa +
              '         <td>' +
              '            Nº Comprovante' +
              '         </td>' +
              '         <td>' +
              '             <a href="' + d.anexo + '" target="blank">' +
              '             ' +retorno.comprovante +
              '             </a>' +
              '         </td>' +
              '         <td>' +
              '            Data' +
              '         </td>' +
              '         <td>' + 
              '             ' + retorno.dataBaixa + 
              '         </td>' +
              '      </tr>' +
              '      <tr>' +
              '         <th colspan="6"> </th>' +
              '      </tr>' +
              '      <tr>' +
              '         <th>Conta Crédito:</th>' +
              '         <th colspan="5">' +
              '            ' + retorno.cliente + 
              '         </th>' +
              '      </tr>' +
              '      <tr>' +
              '         <th>Observação:</th>' +
              '         <th colspan="5">' +
              '            ' + retorno.observacao +
              '         </th>' +
              '      </tr>' +
              '   </tbody>' +
              '</table>';

      row.child(data).show();


   });
}