$(document).ready(function () {

    var tableArquivosGerados = $('#grid_gerados').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax":
        {
            url: '/baixa/arquivosGerados/',
            type: 'POST'
        },
        "columns":
        [
            {
                "data": "dt_geracao"
            },
            {
                "data": "qtd_titulos"
            },
            {
                "data": "usuario"
            },
            {
                "data": "download"
            }
        ]

    });
    
    var tableArquivosGeradosDePara = $('#grid_dePara').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax":
        {
            url: '/baixa/arquivosImportados/',
            type: 'POST'
        },
        "columns":
        [
            {
                "data": "dt_geracao"
            },
            {
                "data": "qtd_titulos"
            },
            {
                "data": "usuario"
            },
            {
                "data": "download"
            }
        ]

    });
    
    $("#botaoGerar").on("click", function () 
    {

        $.ajax(
        {
            type    : "POST",
            url     : "/baixa/exportarLinhasCNAB"
        }).done(function (dRt) 
        {

            var retorno = $.parseJSON(dRt);

            $.pnotify
                    (
                        {
                            title   : retorno.title,
                            text    : retorno.msg,
                            type    : retorno.type
                        }
                    );

            if (!retorno.hasErrors) 
            {
                var link = document.createElement("a");
                link.download = retorno.nomeArquivo;
                link.href = retorno.url;
                link.click();
            }
            
        });

        tableArquivosGerados.fnReloadAjax();
        tableArquivosGeradosDePara.fnReloadAjax();
        
    });

    $('#form_dp').ajaxForm(
    {
        
        beforeSubmit: function () 
        {
            $.bloquearInterface('<h4>Aguarde... O arquivo está sendo importado</h4>');
        },
        
        //em caso de sucesso
        success: function (response) 
        {

            var data = $.parseJSON(response);

            $.pnotify(
            {
                title: "Informações",
                text: data.msg,
                type: data.tipo
            });
            
            $("#anexo_importacao").empty();
            
            $.desbloquearInterface();
            
            tableArquivosGeradosDePara.fnReloadAjax();
            
            $("#anexo_importacao").replaceWith($("#anexo_importacao").val("").clone(true));
            
        }
    });

});