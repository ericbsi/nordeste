$(document).ready(function ()
{
   /*Formulario de anexo de documento*/
   $('#form-add-att').ajaxForm({
      beforeSubmit: function () {

         if ($('#ComprovanteFile').val() != '')
         {
            if (window.File && window.FileReader && window.FileList && window.Blob && $('#form-add-att').valid())
            {
               var fsize = $('#ComprovanteFile')[0].files[0].size;
               var ftype = $('#ComprovanteFile')[0].files[0].type;

               switch (ftype)
               {
                  case 'image/png':
                  case 'image/gif':
                  case 'image/jpeg':
                  case 'image/pjpeg':
                  case 'application/pdf':
                     break;
                  default:
                     alert("Tipo de arquivo não permitido!");
                     return false;
               }

               if (fsize > 5242880)
               {
                  alert("Arquivo muito grande!");
                  return false
               }
            }

            else
            {
               alert("Revise o formulário!");
            }
         }
      },
      success: function (response, textStatus, xhr, form) {
         //var retorno = $.parseJSON(response);
         $('#modal_form_new_att').modal('hide');
      }
   });

   var contasPagarTable = $('#grid_pagar').DataTable({
      "processing": true,
      "serverSide": true,
      "ajax":
              {
                 url: '/financeiro/gridContasPagar',
                 type: 'POST'
              },
      "columns": [
         {
            "data": "grupo"
         },
         {
            "data": "valorAberto"
         },
         {
            "data": "valorBordero"
         },
         {
            "data": "valorLote"
         },
         {
            "data": "valorTotal"
         }
      ]
   });

   var borderosTable = $('#grid_borderos').DataTable({
      "processing": true,
      "serverSide": true,
      "ajax":
              {
                 "url"  : '/financeiro/gridBorderos',
                 "type" : 'POST',
                 "data" : function (d) {
                    d.borderosSelecionados   =  getBorderosSelecionados()  ,
                    d.buscaParceiro          =  $('#buscaParceiro').val()  ,
                    d.buscaData              =  $('#buscaData'    ).val()  ,
                    d.buscaCodigo            =  $('#buscaCodigo'  ).val()  
                 }

              },
      "columns": [
         {
            "data": "checkbox",
            "class": "tdCheckBordero"
         },
         {
            "data": "parceiro"
         },
         {
            "data": "bordero"
         },
         {
            "data": "documentacao"
         },
         {
            "data": "dataCriacao"
         },
         {
            "data": "responsavel"
         },
         {
            "data": "valor"
         }
      ]
   });

   var lotesTable = $('#grid_lotes').DataTable({
      "processing": true,
      "serverSide": true,
      "ajax":
      {
        "url": '/financeiro/gridLotes',
        "type": 'POST'

      },
      "columnDefs":
              [
                 {
                    "orderable": false,
                    "targets": "no-orderable"
                 }
              ],
      "columns":
      [
        {
          "className": 'details-control',
          "orderable": false,
          "data": null,
          "defaultContent": ''
        },
        {
          "data": "nucleo"
        },
        {
          "data": "codigo"
        },
        {
          "data": "dataCadastro"
        },
        {
          "data": "dBancarios"
        },
        {
          "data": "cgc"
        },
        {
          "data": "valor"
        },
        {
          "className": "tdRemoveLote",
          "data": 'btnRemoveLote'
        }
      ],
      "drawCallback" : function(settings) {
        $('#th-total').html( settings.json.customReturn.total );
      }
      
   });

   function getBorderosSelecionados()
   {
      var retorno = [-1];

      var borderos = $("[name='BorderosIds[]']");

      for (i = 0; i < borderos.length; i++)
      {
         retorno.push(parseInt(borderos[i]['value']));
      }

      return retorno;
   }

   $(document).on('change', '.check_bordero', function () {

      var ipthdnId = 'iptn_hdn_parcela_' + $(this).val();

      if (this.checked) {

         if ($('#' + ipthdnId).length == 0)
         {
            $('#tdGerarLotes').append('<input id="' + ipthdnId + '" type="hidden" name="BorderosIds[]" value="' + $(this).val() + '">');
         }
      }

      else
      {
         if ($('#' + ipthdnId).length != 0)
         {
            $('#' + ipthdnId).remove();
         }
      }

      if ($("[name='BorderosIds[]']").length == 0)
      {
         $("#btn-bordero-selects").prop("disabled", true);
      }
      else
      {
         $("#btn-bordero-selects").prop("disabled", false);
      }

   });

   $('#btn-bordero-selects').on('click', function ()
   {
      var borderos = $("[name='BorderosIds[]']");

      if (borderos.length)
      {

         $.ajax({
            type: "POST",
            url: "/financeiro/gerarLotes",
            data: {
               "borderosSelecionados": getBorderosSelecionados()
            },
         }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            $.pnotify({
               title: 'Notificação',
               text: retorno.msg,
               type: retorno.pntfyClass
            });

            if (!retorno.hasErrors)
            {

               $("[name='BorderosIds[]']").remove();

               borderosTable.draw();
               contasPagarTable.draw();
               lotesTable.draw();
            }

         });

      }

   })

   $(document).on('click', '.btnPagar', function ()
   {
      var idLote;

      if (confirm('Deseja realmente baixar este Lote de Pagamento?'))
      {

         idLote = $(this).val();

         var post = $.ajax({
            type  : "POST",
            url   : "/lotePagamento/pagar",
            data  : 
            {
               "idLote"          : idLote                      ,
               "contaDebito"     : $('#contaDebito'   ).val()  ,
               "valorPago"       : $('#valorPago'     ).val()  ,
               "comprovante"     : $('#comprovante'   ).val()  ,
               "dataPagamento"   : $('#dataPagamento' ).val()  ,
               "contaCredito"    : $('#contaCredito'  ).val()  ,
               "observacao"      : $('#observacao'    ).val()  ,
            },
         })

         post.done(function (dRt) 
         {

            var retorno = $.parseJSON(dRt);


            $.pnotify(
            {
               title : 'Notificação'      ,
               text  : retorno.msg        ,
               type  : retorno.pntfyClass
            });

            if (!retorno.hasErrors)
            {
               borderosTable.draw();
               contasPagarTable.draw();
               lotesTable.draw();

               $('#DadosPagamentoId').val(retorno.DPid);

               /*Sobe tela para anexar*/
               $('#modal_form_new_att').modal('show');

            }
         });
      }

   });

   // Add event listener for opening and closing details
   $('#grid_lotes tbody').on('click', 'td.tdRemoveLote', function ()
   {
      var tr = $(this).closest('tr');
      var row = lotesTable.row(tr);
      var rowData = row.data();
      var idLote = rowData.idLote;

      if (confirm('Deseja realmente excluir este lote?'))
      {
         $.ajax(
                 {
                    type: "POST",
                    url: "/financeiro/excluirLote",
                    data: {
                       "idLote": idLote
                    },
                 }).done(function (dRt)
         {

            var retorno = $.parseJSON(dRt);

            $.pnotify(
                    {
                       title: 'Notificação',
                       text: retorno.msg,
                       type: retorno.pntfyClass
                    });

            if (!retorno.hasErrors)
            {
               borderosTable.draw();
               contasPagarTable.draw();
               lotesTable.draw();
            }

         });
      }

   });

   // Add event listener for opening and closing details
   $('#grid_lotes tbody').on('click', 'td.details-control', function () {

      var tr = $(this).closest('tr');
      var row = lotesTable.row(tr);
      var rowsTam = lotesTable.rows()[0].length;
      var rowData = row.data();
      
      console.log(rowData);

      if (row.child.isShown()) {

         // This row is already open - close it
         row.child.hide();
         tr.removeClass('shown');
      }
      else {

         for (i = 0; i < rowsTam; i++)
         {

            if (lotesTable.row(i).child.isShown()) {
               lotesTable.row(i).child.hide();
            }
         }

         $('td.details-control').closest('tr').removeClass('shown');

         // Open this row
         /*row.child(format(row.data())).show();
          tr.addClass('shown');*/

         formatSubTable(row.data(), tr, row, rowData.valorNumero);

      }
   });

   $(document).on('blur', '.formBaixa', function ()
   {
      habilitaBaixa();
   });

   $(document).on('change', '.formBaixa', function ()
   {
      habilitaBaixa();
   });
   
   $(document).on('change','.filtroBordero',function ()
   {
      borderosTable.draw();
   })

   function habilitaBaixa()
   {

      if (($.trim($('#contaDebito').val()) !== "0") &&
              ($.trim($('#valorPago').val()) !== "") &&
              ($.trim($('#comprovante').val()) !== "") &&
              ($.trim($('#dataPagamento').val()) !== "") &&
              ($.trim($('#contaCredito').val()) !== "0")
              )
      {
         $('#btnPagar').removeAttr('disabled');
      }
      else
      {
         $('#btnPagar').attr('disabled', 'disabled');
      }
   }

}
)

function formatSubTable(d, tr, row, valorBaixa) {

   $.ajax(
           {
              type: "POST",
              url: "/lotePagamento/getDadosBancarios",
              data: {
                 "idLote": d.idLote
              },
           }).done(function (dRt)
   {

      var retorno = $.parseJSON(dRt);

      tr.addClass('shown');
      
      console.log(valorBaixa);

      // `d` is the original data object for the row
      var data = '<table class="table table-striped table-hover table-full-width dataTable">' +
              '   <thead>' +
              '      <tr>' +
              '         <th></th>' +
              '         <th colspan="4"><h4><p align="center">Informações da Baixa</p></h4></th>' +
              '         <th>' +
              '            <p align="right">' +
              '               <button id="btnPagar" disabled class="btn btn-green btnPagar" value="' + d.idLote + '">' +
              '                  <i class="clip-banknote"> Baixar</i>' +
              '               </button>' +
              '            </p>' +
              '         </th>' +
              '      </tr>' +
              '      <tr>' +
              '         <td colspan="6"><b>Dados Bancários</b></td>' +
              '      </tr>' +
              '   </thead>' +
              '   <tbody>' +
              '      <tr>' +
              '         <th>Conta Débito:</th>' +
              '         <th colspan="5">' +
              '            <select id="contaDebito" class="form-control select2 formBaixa">' + retorno.empresa + '</select>' +
              '         </th>' +
              '      </tr>' +
              '      <tr>' +
              '         <td>' +
              '            Valor' +
              '         </td>' +
              '         <td><input class="formBaixa" id="valorPago" type="number" value=' + valorBaixa + '></td>' +
              '         <td>' +
              '            Nº Comprovante' +
              '         </td>' +
              '         <td><input class="formBaixa" id="comprovante" type="text"></td>' +
              '         <td>' +
              '            Data' +
              '         </td>' +
              '         <td><input class="formBaixa" id="dataPagamento" type="date" value="' + retorno.dataBaixa + '"></td>' +
              '      </tr>' +
              '      <tr>' +
              '         <th colspan="6"> </th>' +
              '      </tr>' +
              '      <tr>' +
              '         <th>Conta Crédito:</th>' +
              '         <th colspan="5">' +
              '            <select id="contaCredito" class="form-control select2 formBaixa">' + retorno.parceiro + '</select>' +
              '         </th>' +
              '      </tr>' +
              '      <tr>' +
              '         <th>Observação:</th>' +
              '         <th colspan="5">' +
              '            <input class="formBaixa" id="observacao" type="text" style="width : 100%!important">' +
              '         </th>' +
              '      </tr>' +
              '   </tbody>' +
              '</table>';

      row.child(data).show();


   });
}