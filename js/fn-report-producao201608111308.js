$(function () {


    $.fn.totais = function (json) {

        return this.each(function () {

            $('#tfoot-total-ini').html(json.customReturn.countValTotalIni);
            $('#tfoot-total-entradas').html(json.customReturn.countEntTot);
            $('#tfoot-total-seguro').html(json.customReturn.countSegTot);
            $('#tfoot-total').html(json.customReturn.totalFin);
            $('#tfoot-total-geral').html(json.customReturn.totalGeral);
        });
    }

    $('#toggle-filter').on('click', function () {
        if ($('#toggle-filter').hasClass('btn-success')) {
            $('#toggle-filter').removeClass('btn-success');
            $('#toggle-filter').addClass('btn-danger');
            $('#toggle-filter').text('Ocultar Opções de Filtragem');
        } else {
            $('#toggle-filter').addClass('btn-success');
            $('#toggle-filter').text('Mostrar Opções de Filtragem');
        }
        $('.filtragem').toggle('slow');
    });

    var selectGrupoFiliais = $('#grupos_select').multiselect({
        buttonWidth: '200px',
        numberDisplayed: 2,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        includeSelectAllOption: true,
        selectAllText: ' Selecionar tudo',
        buttonText: function (options, select) {

            if (options.length == 0) {
                return 'Selecionar Grupos <b class="caret"></b>'
            } else if (options.length == 1) {
                return '1 Grupo Selecionado <b class="caret"></b>'
            } else {
                return options.length + ' Grupos Selecionados <b class="caret"></b>'
            }
        },
    });

    var selectNucleoFiliais = $('#nucleos_select').multiselect({
        buttonWidth: '200px',
        numberDisplayed: 2,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        includeSelectAllOption: true,
        selectAllText: ' Selecionar tudo',
        buttonText: function (options, select) {

            if (options.length == 0) {
                return 'Selecionar Nucleos <b class="caret"></b>'
            } else if (options.length == 1) {
                return '1 Nucleo Selecionado <b class="caret"></b>'
            } else {
                return options.length + ' Nucleos Selecionados <b class="caret"></b>'
            }
        },
    });

    var selectFiliais = $('#filiais_select').multiselect({
        buttonWidth: '200px',
        numberDisplayed: 2,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        includeSelectAllOption: true,
        selectAllText: ' Selecionar tudo',
        buttonText: function (options, select) {

            if (options.length == 0) {
                return 'Selecionar Filiais <b class="caret"></b>'
            } else if (options.length == 1) {
                return '1 Filial Selecionada <b class="caret"></b>'
            } else {
                return options.length + ' Filiais Selecionadas <b class="caret"></b>'
            }
        },
    });

    var selectServico = $('#servicos_select').multiselect(
    {
        buttonWidth: '100%',
        numberDisplayed: 2,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        includeSelectAllOption: true,
        selectAllText: ' Selecionar tudo',
        buttonText: function (options, select) {

            if (options.length == 0) {
                return 'Selecionar Serviços <b class="caret"></b>'
            } else if (options.length == 1) {
                return '1 Serviço selecionado <b class="caret"></b>'
            } else {
                return options.length + ' Serviços selecionados <b class="caret"></b>'
            }
        },
    });



    var selectTipoFin = $('#tipo_fin_select').multiselect({
        buttonWidth: '100%',
        numberDisplayed: 2,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        includeSelectAllOption: true,
        selectAllText: ' Selecionar tudo',
        buttonText: function (options, select) {

            if (options.length == 0) {
                return 'Selecionar Tipos Financiamentos <b class="caret"></b>'
            } else if (options.length == 1) {
                return '1 Tipo de Financiamento selecionado <b class="caret"></b>'
            } else {
                return options.length + ' Tipos de Financiamento selecionados <b class="caret"></b>'
            }
        },
    });

    //quando algum núcleo for selecionado, atualizar os outros dois campos de grupo e filiais
    selectNucleoFiliais.on('change', function ()
    {

        idsNucleo = $(this).val();

        $.ajax(
                {
                    type: "POST",
                    url: "/grupoFiliais/getGrupoFilial/",
                    "data": {
                        idNucleos: idsNucleo
                    }
                }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            if (!(retorno.hasErrors))
            {
                selectGrupoFiliais.val(retorno.dataGrupos);
                selectFiliais.val(retorno.dataFiliais);

                selectGrupoFiliais.multiselect("refresh");
                selectFiliais.multiselect("refresh");

            } else
            {
                console.log(retorno);
            }

        });

    });
    //quando algum grupo for selecionado, atualizar os outros dois campos de filiais e núcleos
    selectGrupoFiliais.on('change', function ()
    {

        console.log($(this).val());

        idsGrupo = $(this).val();

        $.ajax(
                {
                    type: "POST",
                    url: "/grupoFiliais/getNucleoFilial/",
                    "data":
                            {
                                idGrupos: idsGrupo
                            }
                }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            if (!(retorno.hasErrors))
            {
                selectNucleoFiliais.val(retorno.dataNucleos);
                selectFiliais.val(retorno.dataFiliais);

                selectNucleoFiliais.multiselect("refresh");
                selectFiliais.multiselect("refresh");

            } else
            {
                console.log(retorno);
            }

        });

    });
    //quando alguma filial for selecionada, atualizar os outros dois campos de grupo e núcleo
    selectFiliais.on('change', function ()
    {

        idsFilial = $(this).val();

        $.ajax(
                {
                    type: "POST",
                    url: "/grupoFiliais/getFilial/",
                    "data": {
                        idFiliais: idsFilial
                    }
                }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);
            console.log(retorno);

            if (!(retorno.hasErrors))
            {
                selectNucleoFiliais.val(retorno.dataNucleos);
                selectGrupoFiliais.val(retorno.dataGrupos);

                selectNucleoFiliais.multiselect("refresh");
                selectGrupoFiliais.multiselect("refresh");

            } else
            {
            }

        })

    });


    var gridProducao = $('#grid_producao').DataTable(
    {

        "processing"    : true,
        "serverSide"    : true,
        "ajax"          :
        {
            url     : '/reports/reportProducao/',
            type    : 'POST',
            "data"  : function (d)
            {
                d.dataDe    = $("#data_de"          ).val(),
                d.dataAte   = $("#data_ate"         ).val(),
                d.filiais   = $("#filiais_select"   ).val(),
                d.valorDe   = $("#valor_de"         ).val(),
                d.valorAte  = $("#valor_ate"        ).val(),
                d.tipoFin   = $("#tipo_fin_select"  ).val(),
                d.servicos  = $("#servicos_select"  ).val()
            },
        },
        "columnDefs":
        [
            {
                "orderable" : false             ,
                "targets"   : "no-orderable"
            }
        ],
        "columns":
        [
            {
                "data": "codigo"
            },
            {
                "data": "financeira"
            },
            {
                "data": "servico"
            },
            {
                "data": "emissao"
            },
            {
                "data": "filial"
            },
            {
                "data": "cliente"
            },
            {
                "data": "cpf"
            },
            {
                "data": "val_inicial"
            },
            {
                "data": "val_entrada"
            },
            {
                "data": "seguro"
            },
            {
                "data": "carencia"
            },
            {
                "data": "val_financiado"
            },
            {
                "data": "condi_parce"
            },
            {
                "data": "val_total"
            },
        ],
        "drawCallback": function (settings) {
            $(document).totais(settings.json);
        }

    });

    $('#btn-filter').on('click', function () {
        gridProducao.draw();
        return false;
    });

    $('#btn-print-capa').on('click', function () {

        if ($("#data_de").val().length <= 0 || $("#data_ate").val().length <= 0)
        {
            alert("Por favor, selecione um intervalo de datas");
        } else
        {
            $('#iptHdnDataDe').attr('value', $("#data_de").val());
            $('#iptHdnDataAte').attr('value', $("#data_ate").val());
            $('#form-print-capa').submit();
        }

        return false;
    });

    $('#btn-print-rel').on('click', function () {

        if ($("#filiais_select").val() == null || $("#data_de").val().length <= 0 || $("#data_ate").val().length <= 0)
        {
            alert("Por favor, complete o filtro de pesquisa.");
        } else
        {
            $('#iptHdnData_De').attr('value', $("#data_de").val());
            $('#iptHdnData_Ate').attr('value', $("#data_ate").val());
            $('#iptHdnFiliais').attr('value', $("#filiais_select").val());
            $('#form-print-all').submit();
        }
        return false;
    })

});
