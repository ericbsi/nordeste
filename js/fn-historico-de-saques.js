$(function(){

	var tableHistorico = $('#tableHistorico').DataTable({

		"processing": true,
        "serverSide": true,
        "ajax": {
            url: '/saqueFacil/ListarHistoricoDeIntencoes/',
            type: 'POST',
        },

        "columns": [
            {"data": "cliente"},
            {"data": "cpf"},
            {"data": "valorInicial"},
            {"data": "parcelamento"},
            {"data": "valorSaque"},
            {"data": "dataCadastro"},
            {"data": "btnPrinter"},
        ],

	});

})