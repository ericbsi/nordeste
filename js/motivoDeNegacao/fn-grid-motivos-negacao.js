$(document).ready(function()
{
    
    var tableMotivo = $('#grid_motivo').DataTable(
    {
        "processing": true,
        "serverSide": true,
        "ajax": 
        {
            type    : 'POST'                        ,
            url     : '/generico/getMotivosDeNegacao/'  ,
        },
        "columns": [
            {"data": "descricao"},
        ]
    });
    
    
     $('#novoMotivo').on('click', function () {
        $.ajax({
            type: 'POST',
            url: '/generico/salvarMotivos',
            data: {
                descricao: $('#descricao').val()
            }
        }).done(function (dRt) {

            var jsonReturn = $.parseJSON(dRt);
            $.pnotify({
                title: "Informações",
                text: jsonReturn.msg,
                type: jsonReturn.type
            });
            
            if(!jsonReturn.error){
                tableMotivo.draw();
                $('#descricao').val('');
            }
        });
    });
    
   
    
    
});