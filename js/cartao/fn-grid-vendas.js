$(document).ready(function () {

    var vendasTable = $('#grid_vendas').DataTable({
        "processing": true,
        //"serverSide": true,
        "ajax":
                {
                    url: '/cartao/listarvendas',
                    type: 'POST',
                },
        "columns": [
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            {
                "data": "codigo",
            },
            {
                "data": "cliente",
            },
            {
                "data": "cpf",
            },
            {
                "data": "valor",
            },
            {
                "data": "qtd_parcelas",
            },
        ],
    });

    $('#check_select_all').on('change',function(){
        this.checked ? $(this).checkAll() : $(this).unCheckAll();
    });

    jQuery.fn.checkAll = function (){
        return this.each(function(){
            $(".check_print").prop('checked', true);
        })
    }

    jQuery.fn.unCheckAll = function (){
        return this.each(function(){    
            $(".check_print").prop('checked', false);
        })

    }

    $('#grid_vendas tbody').on('click', 'td.details-control', function () {

        var tr = $(this).closest('tr');
        var row = vendasTable.row(tr);
        var rowsTam = vendasTable.rows()[0].length;

        if (row.child.isShown()) {

            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
            
        }
        else 
        {

            for (i = 0; i < rowsTam; i++)
            {

                if (vendasTable.row(i).child.isShown()) {
                    vendasTable.row(i).child.hide();
                }
            }

            $('td.details-control').closest('tr').removeClass('shown');

            // Open this row
            /*row.child(format(row.data())).show();
             tr.addClass('shown');*/

            formatSubTable(row.data(), tr, row);

        }
        
    });

    function formatSubTable(d, tr, row) {
    $.ajax(
            {
                type: "POST",
                url: "/cartao/titulos",
                data: {
                    "cod_venda" : d.codigo,
                    "parc"      : Math.round((d.valor/d.qtd_parcelas)*100)/100 //arredondando o número para duas casas decimais
                },
            }).done(function (dRt)
    {

        var retorno = $.parseJSON(dRt);
        tr.addClass('shown');
        // `d` is the original data object for the row
        var data = '<table class="table table-striped table-hover table-full-width dataTable">' +
                '   <thead>' +
                '       <tr>' +
                '           <th width="3px">' +
                '               <label class="checkbox-inline" style="float:left">' +
                '                   <input id="check_select_all" type="checkbox">' +
                '               </label>' +
                '           </th>' +
                '           <th>Seq</th>' +
                '           <th>Valor</th>' +
                '           <th>Vencimento</th>' +
                '       </tr>' +
                '   </thead>' +
                '   <tbody>';
        if(retorno != null){
            $.each(retorno.parcela, function (indice, i)
            {
                data +=
                        '      <tr>' +
                        '         <td>' +
                        '           <label class="checkbox-inline" style="float:left"> ' +
                        '               <input class="check_print" type="checkbox">' +
                        '           </label>' +
                        '         </td>' + 
                        '         <td>' + retorno.parcela[indice].seq + 'º' +
                        '         </td>' +
                        '         <td>' + retorno.parcela[indice].valor +
                        '         </td>' +
                        '         <td>' + retorno.parcela[indice].vencimento +
                        '         </td>' +
                        '      </tr>';
            });
            data +=
            '       <tr> <td colspan="5">' +
            '           <div class="row">' +
            '               <p align="center">' +
            '                   <button class="btn btn-red btnIprimir" value="Imprimir Titulos">' +
            '                       <i class="clip-note"></i> Imprimir Títulos' +
            '                   </button>' +
            '               </p>' +
            '           </div>' +
            '       </td> </tr>'
            '   </tbody>' +
            '</table>';
        }
            row.child(data).show();
       });
    }
});