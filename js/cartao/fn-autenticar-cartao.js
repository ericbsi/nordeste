function bloquear_ctrl_j(){

    if (window.event.ctrlKey && window.event.keyCode == 74)
    {
        event.keyCode = 0;  
        event.returnValue = false;  
    }  
}

$('#cpf').focus();

$('#form-cpf').on('submit',function(){
        return false;
});

$('#form-cpf').validate({
	    submitHandler : function(){
        return false;
    }
});

$('#form-cartao').on('submit',function(){
        return false;
});

$('#form-cartao').validate({
	    submitHandler : function(){
        return false;
    }
});

$('#btn-gravar-venda').on('click', function(){

	//console.log($('#idCliente').val());
	//console.log($('#idCartao').val());
	//console.log($('#valor-compra').val());
	//console.log($('#qtd-parcelas').val());
	var request = $.ajax({
		type : 'POST',
		url  : '/cartao/gravarvenda',
		data : {
			idCliente 	: $('#idCliente').val(),
			idCartao  	: $('#idCartao').val(),
			carencia  	: '30',
			valorVenda	: $('#valor-compra').val(),
			qtdParcelas	: $('#qtd-parcelas option:selected').val()
		},
		beforeSend : function(){
			$.bloquearInterface('<h4>Enviando informações...</h4>');
		}
	});

	request.done(function(dRt){
		var retorno = $.parseJSON(dRt);
		$.desbloquearInterface();
		if(retorno.hasErrors){
			$.pnotify({
    			title: 'Erros foram encontrados!',
    			text: retorno.mensagem.msg,
    			type: 'error'
			});
		}else{
			/*$.pnotify({
    			title: 'Venda realizada com sucesso!',
    			text: retorno.mensagem.msg,
    			type: 'success'
			});*/
			$('#sucess-return').modal('show');
		}
	});
});

$('#cvc').on('blur', function(){
	if($('#cvc').valid()){
		var request = $.ajax({
			type : 'POST',
			url  : '/cliente/getCvc',
			data : {
				num_cartao : $('#num-cartao').val(),
				cvc_dig	   : $(this).val()
			},
			beforeSend : function(){
				$.bloquearInterface('<h4>Buscando informações...</h4>');
			}
		});

		request.done(function(dRt){
			var retorno = $.parseJSON(dRt);
			$.desbloquearInterface();
			if(retorno.hasError){
				$.pnotify({
    					title: 'Erros foram encontrados!',
    					text: retorno.msg,
    					type: 'error'
				});
			}else{
				$.pnotify({
    				title: 'Dados Corretos!',
    				text: retorno.msg,
    				type: 'success'
				});
			}
		});
	}
});

$('#num-cartao').on('blur', function(){
	if ($('#num-cartao').valid()) {
		var request = $.ajax({
			type : 'POST',
			url	 : '/cliente/getCartao',
			data : {
				num_cartao 	: $(this).val(),
				cpf 		: $('#cpf').val()
			},
			beforeSend : function(){
            	$.bloquearInterface('<h4>Buscando informações...</h4>');
        	}
		});

		request.done(function(dRt){
			var retorno = $.parseJSON(dRt);
			$.desbloquearInterface();
			if(retorno.hasError){
				$.pnotify({
    					title: 'Erros foram encontrados!',
    					text: retorno.msg,
    					type: 'error'
				});
			}else{
				$('#idCliente').val(retorno.idClt);
				$('#idCartao').val(retorno.idCartao);
				$.pnotify({
    				title: 'Dados Corretos!',
    				text: 'O cartão informado está associado ao cliente.',
    				type: 'success'
				});
			}
		});
	};
});

$('#cpf').on('blur',function(){
    if( $('#cpf').valid() )
    {           
    	var request  = $.ajax({

            type : 'POST',
            url  : '/cliente/situacaoCadastralCPF',
            data : {
                cpf : $(this).val()
            },
                        
            beforeSend : function()
            {
                $.bloquearInterface('<h4>Buscando informações...</h4>');
            }
        });

        request.done(function(dRt){
        	var retorno = $.parseJSON(dRt);
        	$.desbloquearInterface();
        	if (retorno.cadastrado) {
        		$.pnotify({
    				title: 'Cliente Cadastrado',
    				text: 'O cliente possui cadastro no sistema!',
    				type: 'success'
				});
        	}else{
        		$.pnotify({
    				title: 'Cliente não Cadastrado',
    				text: 'O cliente não possui cadastro no sistema!',
    				type: 'error'
				});
        	}
        });
    }
});