$(document).on('click', '.label-warning', function () {

    $('#input_proposta_id').attr('value', $(this).attr('subid'));

});


var grid_analises = $('#table_id').DataTable({
    "processing": true,
    "serverSide": true,
    "stateSave" : true,
    "ajax": {
        url: '/proposta/ultimasPropostasNegadasReanalise/',
        type: 'POST',
        "data": function (d) {
            d.codigo_filter = $("#codigo_filter").val()
            d.nome_filter   = $("#nome_filter").val()
        }
    },
    "columns": [
        {"data": "modalidade"},
        {"data": "codigo"},
        {"data": "cliente"},
        {"data": "dadosCli"},
        {"data": "financeira"},
        {"data": "filial"},
        {"data": "politicaCredito"},
        {"data": "valor"},
        {"data": "valorSeguro"},
        {"data": "qtd_parcelas"},
        {"data": "btn"},
        {"data": "data_cadastro"},
        {"data": "tempoEspera"},
    ],
    "drawCallback": function (settings) {
        setTimeout(function () {
            grid_analises.draw(false);
        }, 20000)
    }
});

$(document).on('change','.input_filter', function(){
    grid_analises.draw();
});


$('#form-liberar-proposta').validate({

    submitHandler   : function (){

        var post    = $.ajax({

            url     : '/proposta/enviarParaFila/',
            type    : 'POST',
            data    : $('#form-liberar-proposta').serialize()

        });

        post.done( function( dRt ){
                
            var retorno = $.parseJSON(dRt);

            $('#modal-enviar').modal('hide');
                
            grid_analises.draw();
                
            $.pnotify({
                title   : retorno.titulo,
                text    : retorno.texto,
                type    : retorno.tipo
            });
        } );

        return false;
    }
});

$(document).on('click','.show-modal',function(){
    $('#propostaid').val( $(this).data('propid') );
});