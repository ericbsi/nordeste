$(document).ready(function()
{
   var tablePendencias = $('#grid_relatorio_pendencias').DataTable(
    {
        "processing": true,
        "serverSide": true,
        "ajax": 
        {
            type    : 'POST'                        ,
            url     : '/pagamento/getRelatorioPendencias',
            data: function(d){
                d.codigo = $('#codigo').val();
                d.financeira = $('#selectSFilter').val();
                d.de = $('#data_de').val();
                d.ate = $('#data_ate').val();
                d.filiais = $('#filiais').val();        
            }
        },
        "columns": [
            {"data": "codigo"},
            {"data": "numero"},
            {"data": "nome"},
            {"data": "valor_financiado"},
            {"data": "valor_repasse"},
            {"data": "obs"},
            {"data": "atraso"},
        ]
    });
    
    $('#filtrar').on('click', function() {
        tablePendencias.draw();
         $('#codigo').val('');
    });
    $('.select2').select2();
});

