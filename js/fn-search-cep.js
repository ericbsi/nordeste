$(function(){

	$('#cep_cliente').on('blur',function(){
		$.ajax({
			type :  'POST', 
	        url  : '/analiseDeCredito/buscarCEP/',
	        data : {
               'cep' : $('#cep_cliente').val(),
            },

            beforeSend : function(){

            	$('.panel').block({
	                overlayCSS: {
	                    backgroundColor: '#fff'
	                },
	                message: '<img src="https://beta.sigacbr.com.br/js/loading.gif" /> Buscando CEP...',
	                css: {
	                    border: 'none',
	                    color: '#333',
	                    background: 'none'
	                }
            	});
            	window.setTimeout(function () {
                	$('.panel').unblock();
            	}, 1000);
            }
		})
		.done( function( data ){

			var jsonReturn = $.parseJSON(data);
			
			$('#cidade_cliente').val(jsonReturn.cidade);

			if( jsonReturn.logradouro != null && $.trim(jsonReturn.logradouro) != '' ){
				$('#logradouro_cliente').val(jsonReturn.logradouro);
			}

			if( jsonReturn.bairro != null && $.trim(jsonReturn.bairro) != '' ){
				$('#bairro_cliente').val(jsonReturn.bairro);
			}

			$('#EnderecoCliente_uf').select2().select2('val',jsonReturn.uf);
		} )
	})

	$('#cep_conjuge').on('blur',function(){

		$.ajax({
			type :  'POST', 
	        url  : '/analiseDeCredito/buscarCEP/',
	        data : {
               'cep' : $('#cep_conjuge').val(),
            },

            beforeSend : function(){

            	$('.panel').block({
	                overlayCSS: {
	                    backgroundColor: '#fff'
	                },
	                message: '<img src="https://beta.sigacbr.com.br/js/loading.gif" /> Buscando CEP...',
	                css: {
	                    border: 'none',
	                    color: '#333',
	                    background: 'none'
	                }
            	});
            	window.setTimeout(function () {
                $('.panel').unblock();
            	}, 1000);
            }

		})

		.done( function( data ){

			var jsonReturn = $.parseJSON(data);
			
			$('#cidade_conjuge').val(jsonReturn.cidade);

			if( jsonReturn.logradouro != null && $.trim(jsonReturn.logradouro) != '' ){
				$('#logradouro_conjuge').val(jsonReturn.logradouro);
			}

			if( jsonReturn.bairro != null && $.trim(jsonReturn.bairro) != '' ){
				$('#bairro_conjuge').val(jsonReturn.bairro);
			}

			$('#EnderecoConjuge_uf').select2().select2('val',jsonReturn.uf);

		});
	});

	$('#cep_dados_profissionais').on('blur',function(){

		$.ajax({
			type :  'POST', 
	        url  : '/analiseDeCredito/buscarCEP/',
	        data : {
               'cep' : $('#cep_dados_profissionais').val(),
            },

            beforeSend : function(){

            	$('.panel').block({
	                overlayCSS: {
	                    backgroundColor: '#fff'
	                },
	                message: '<img src="https://beta.sigacbr.com.br/js/loading.gif" /> Buscando CEP...',
	                css: {
	                    border: 'none',
	                    color: '#333',
	                    background: 'none'
	                }
            	});
            	window.setTimeout(function () {
                $('.panel').unblock();
            	}, 1000);
            }

		})

		.done( function( data ){

			var jsonReturn = $.parseJSON(data);
			
			$('#cidade_dados_profissionais').val(jsonReturn.cidade);

			if( jsonReturn.logradouro != null && $.trim(jsonReturn.logradouro) != '' ){
				$('#logradouro_dados_profissionais').val(jsonReturn.logradouro);
			}

			if( jsonReturn.bairro != null && $.trim(jsonReturn.bairro) != '' ){
				$('#bairro_dados_profissionais').val(jsonReturn.bairro);
			}

			$('#EnderecoProfiss_uf').select2().select2('val',jsonReturn.uf);
		});
	});

	$('#cep_dados_profissionais_conjuge').on('blur',function(){

		$.ajax({
				type :  'POST', 
		        url  : '/analiseDeCredito/buscarCEP/',
		        data : {
	               'cep' : $('#cep_dados_profissionais_conjuge').val(),
	            },

	            beforeSend : function(){

	            	$('.panel').block({
		                overlayCSS: {
		                    backgroundColor: '#fff'
		                },
		                message: '<img src="https://beta.sigacbr.com.br/js/loading.gif" /> Buscando CEP...',
		                css: {
		                    border: 'none',
		                    color: '#333',
		                    background: 'none'
		                }
	            	});
	            	window.setTimeout(function () {
	                $('.panel').unblock();
	            	}, 1000);
	            }

		})
		.done( function( data ){

			var jsonReturn = $.parseJSON(data);
				
			$('#cidade_dados_profissionais_conjuge').val(jsonReturn.cidade);

			if( jsonReturn.logradouro != null && $.trim(jsonReturn.logradouro) != '' ){
				$('#logradouro_dados_profissionais_conjuge').val(jsonReturn.logradouro);
			}

			if( jsonReturn.bairro != null && $.trim(jsonReturn.bairro) != '' ){
				$('#bairro_dados_profissionais_conjuge').val(jsonReturn.bairro);
			}
				
			$('#EnderecoProfissConjuge_uf').select2().select2('val',jsonReturn.uf);
		});
	});
});