$(function(){
    
    $.validator.setDefaults({
        
        errorElement: "span", 
        errorClass: 'help-block',
        
        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            $( $(element).data('icon-valid-bind') ).removeClass('ico-validate-success').addClass('ico-validate-error');
        },
            
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },

        success: function (label, element) {
            label.addClass('help-block valid');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            $( $(element).data('icon-valid-bind') ).removeClass('ico-validate-error').addClass('ico-validate-success');
        },
    });

    $('#nome').focus();

    jQuery.fn.deslizar          = function ( target )
    {
        return this.each(function (){
            $('html,body').animate({scrollTop:$("#"+target).offset().top}, 'slow');
        });
    }
    
    $.limparForm                = function(form)
    {
        $(form)[0].reset();
        $(form + " .select2").select2("val", "");    
        $(form).closest('.form-group').removeClass('has-success').find('.symbol').removeClass('ok').addClass('required');
    }

    $.mostrar                   = function(element)
    {
        if( !$('#'+element).is(':visible') )
        {
            $('#'+element).show();
        }
    }

    $("#form-proposta").validate({
        ignore                      : null
    });
    
    /*Enviando proposta*/
    $('#btn-enviar-proposta').on('click',function(){
        
        if( $('#form-proposta').valid() )
        {
            var post = $.ajax({

                url     : '/consignadoShow/enviar/',
                type    : 'POST',
                data    : $('#form-proposta').serialize(),

                beforeSend : function()
                {
                    $.bloquearInterface('<h4>Enviando informações...</h4>');
                }

            });

            post.done(function(dRt){

                var retorno = $.parseJSON(dRt); 
                        
                if( retorno.hasErrors == 0 )
                {
                    $.desbloquearInterface();
                    $('#sucess-return').modal('show');
                }

            });
        }
        else{
            alert("Todos os dados são obrigatórios! ");
        }

        return false;
    });

    $(document).on('click','.btn-show-form',function(){

        $.limparForm('#'+$(this).data('form-show'));
        $.mostrar($(this).data('form-show'));
        $(document).deslizar($(this).data('form-show'));
        $('#'+$(this).data('form-input-init')).focus();
        $('#tabForm a[href="'+$(this).data('go-to-tab')+'"]').tab('show');
        return false;

    });
        
    $('#tabForm a[data-toggle="tab"]').on('shown.bs.tab', function (e){
        $("#"+e.target.dataset.formBind).focus();
    });

    /*
    $('#form-anexos').ajaxForm({

        beforeSubmit    : function(){
            
            $('#Cliente_id_fup').val( $('#cliente_id').val() );

            if ( window.File && window.FileReader && window.FileList && window.Blob && $('#form-anexos').valid() )
            {
                var fsize = $('#FileInput')[0].files[0].size;
                var ftype = $('#FileInput')[0].files[0].type;
                
                switch( ftype )
                {
                    case 'image/png': 
                    case 'image/gif':
                    case 'image/jpeg': 
                    case 'image/pjpeg':
                    case 'application/pdf':
                    break;
                    default:
                        $('#cadastro_anexo_msg_return').html("Tipo de arquivo não permitido!").show();
                        $('#cadastro_anexo_msg_return').fadeOut(4000);
                    return false;
                }

                if( fsize > 5242880 )
                {
                    $('#cadastro_anexo_msg_return').html("Arquivo muito grande!").show();
                    $('#cadastro_anexo_msg_return').fadeOut(4000);
                    return false
                }
            }

            else
            {
                alert("Revise o formulário!");
            }

        },

        uploadProgress  : function(event, position, total, percentComplete){

            $('#progressbox').show();
            $('#progressbar').width(percentComplete + '%') //update progressbar percent complete
            $('#statustxt').html(percentComplete + '%'); //update status text

            if( percentComplete > 50 )
            {
                $('#statustxt').css('color','#000'); //change status text to white after 50%
            }
        },

        success         : function( response, textStatus, xhr, form ) {

            var retorno = $.parseJSON(response);

            $.pnotify({
                title    : 'Ação realizada com sucesso',
                text     : 'O Arquivo foi enviado com sucesso!',
                type     : 'success',
            });
                    
            $('#progressbox').fadeOut(3000);

            tableAnexos.draw();

        },

        error           : function(xhr, textStatus, errorThrown) {
            
        },

        resetForm       :true
    });
    */
});