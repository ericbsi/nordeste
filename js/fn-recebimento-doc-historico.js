$(function(){

    var selectGrupoFiliais = $('#grupos_select').multiselect({

      buttonWidth: '200px',
      numberDisplayed:2,
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      includeSelectAllOption: true,
      selectAllText: ' Selecionar tudo',
      buttonText:function(options, select){

         if (options.length == 0) {
            return 'Selecionar Grupos <b class="caret"></b>'
         }
         else if (options.length == 1) {
            return '1 Grupo Selecionado <b class="caret"></b>'
         }
         else{
            return options.length + ' Grupos Selecionados <b class="caret"></b>'
         }
      },

   });

   var selectNucleoFiliais = $('#nucleos_select').multiselect({

      buttonWidth: '200px',
      numberDisplayed:2,
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      includeSelectAllOption: true,
      selectAllText: ' Selecionar tudo',
      buttonText:function(options, select){

         if (options.length == 0) {
            return 'Selecionar Nucleos <b class="caret"></b>'
         }
         else if (options.length == 1) {
            return '1 Nucleo Selecionado <b class="caret"></b>'
         }
         else{
            return options.length + ' Nucleos Selecionados <b class="caret"></b>'
         }
      },

   });

  var selectFiliais = $('#filiais_select').multiselect({

      buttonWidth: '200px',
      numberDisplayed:2,
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
      includeSelectAllOption: true,
      selectAllText: ' Selecionar tudo',
      buttonText:function(options, select){

         if (options.length == 0) {
            return 'Selecionar Filiais <b class="caret"></b>'
         }
         else if (options.length == 1) {
            return '1 Filial Selecionada <b class="caret"></b>'
         }
         else{
            return options.length + ' Filiais Selecionadas <b class="caret"></b>'
         }
      },

   });
  //quando algum núcleo for selecionado, atualizar os outros dois campos de grupo e filiais
    selectNucleoFiliais.on('change', function()
    {
        
        idsNucleo = $(this).val();
        
        $.ajax(
        {
            type    : "POST"                                    ,
            url     : "/grupoFiliais/getGrupoFilial/"   ,
            "data": {
                idNucleos : idsNucleo
            }
        }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            if (!(retorno.hasErrors)) 
            {
                selectGrupoFiliais  .val(retorno.dataGrupos     );
                selectFiliais       .val(retorno.dataFiliais    );
                
                selectGrupoFiliais  .multiselect("refresh");
                selectFiliais       .multiselect("refresh");
                
            }
            else
            {
                console.log(retorno);
            }

        });
        
    });
    //quando algum grupo for selecionado, atualizar os outros dois campos de filiais e núcleos
    selectGrupoFiliais.on('change', function()
    {
        
        console.log($(this).val());
        
        idsGrupo = $(this).val();
        
        $.ajax(
        {
            type    : "POST"                                    ,
            url     : "/grupoFiliais/getNucleoFilial/"   ,
            "data"  : 
            {
                idGrupos : idsGrupo
            }
        }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            if (!(retorno.hasErrors)) 
            {
                selectNucleoFiliais .val(retorno.dataNucleos );
                selectFiliais       .val(retorno.dataFiliais );
                
                selectNucleoFiliais .multiselect("refresh");
                selectFiliais       .multiselect("refresh");
                
            }
            else
            {
                console.log(retorno);
            }

        });
        
    });
    //quando alguma filial for selecionada, atualizar os outros dois campos de grupo e núcleo
    selectFiliais.on('change', function()
    {
        
        idsFilial = $(this).val();
        
        $.ajax(
        {
            type    : "POST"                             ,
            url     : "/grupoFiliais/getFilial/" ,
            "data":  {
                idFiliais : idsFilial
            }
        }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);
                console.log(retorno);

            if (!(retorno.hasErrors)) 
            {
                selectNucleoFiliais .val(retorno.dataNucleos    );
                selectGrupoFiliais  .val(retorno.dataGrupos     );
                
                selectNucleoFiliais .multiselect("refresh");
                selectGrupoFiliais  .multiselect("refresh");
                
            }
            else
            {
            }

        })
        
    });

    $('#form-filter').on('submit', function(){
      return false;
    });

    var tableRecebimentosDeContratos = $('#grid_historico_recebimentos_contratos').DataTable({
      
          "processing": true,
          "serverSide": true,
          
          "ajax": {
              url: '/recebimentoDeDocumentacao/getRecebimentos/',
              type: 'POST',
              "data" : function(d)
              {
                d.filial      = $('#filiais_select').val(),
                d.data_de    = $("#data_de").val(),
                d.data_ate   = $("#data_ate").val()
              }
          },
          
          "language": {
              "processing": "Carregando...",
          },
          
          "columnDefs": [{
              "orderable": false,
              "targets": "no-orderable"
          }],
          
          "columns": [
              {"data" :"codigo"},
              {"data" :"data_criacao"},
              {"data" :"operador"},
              {"data" :"parceiro"},
              {"data" :"count_aprovadas"},
              {"data" :"count_pendentes"},
              {"data" :"total_aprovado"},
              {"data" :"total_pendente"},
              {"data" :"repasse_aprovado"},
              {"data" :"repasse_pendente"},
              {"data" :"btn_print"}
          ],
          "drawCallback" : function(settings) {
            $('#th-total').html( settings.json.customReturn.totalRepasse );
          }
    });

    $('#btn-filter').on('click',function(){
      	tableRecebimentosDeContratos.draw();
      	return false;
    });
});