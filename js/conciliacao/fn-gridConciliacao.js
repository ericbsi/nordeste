$(document).ready(function ()
{ 
    
    var filtroDivergencia = [0,0,0,0,0];

    var conciliacaoTable = $('#gridConciliacao').DataTable(
    {
        "sDom"          : 'T<"clear">lfrtip'            ,
        "oTableTools"   : 
        {
            "aButtons": 
            [
                {
                    "sExtends"      : "copy"    ,
                    "sButtonText"   : "Copiar"  ,
                    oSelectorOpts   : 
                    {
                        page    : 'all'
                    }
                }
            ]
        },
        "processing"    : true                          ,
        "serverSide"    : true                          ,
        "ajax":
                {
                    url     :   '/conciliacao/listar'   ,
                    type    :   'POST'                  ,
                    data    :   function(d){
                                    d.filtroContrato        = $('#filtroContrato'       ).val() ,
                                    d.filtroCodigo          = $('#filtroCodigo'         ).val() ,
                                    d.filtroLoja            = $('#filtroLoja'           ).val() ,
                                    d.filtroDataSigacDe     = $('#filtroDataSigacDe'    ).val() ,
                                    d.filtroDataSigacAte    = $('#filtroDataSigacAte'   ).val() ,
                                    d.filtroCGCOmni         = $('#filtroCGCOmni'        ).val() ,
                                    d.filtroCGCSigac        = $('#filtroCGCSigac'       ).val() ,
                                    d.filtroNomeOmni        = $('#filtroNomeOmni'       ).val() ,
                                    d.filtroNomeSigac       = $('#filtroNomeSigac'      ).val() ,
                                    d.filtroDivergencia     = filtroDivergencia         
                                }
                },
        "columns": [
            {
                "data": "contrato",
                "className": "contrato",
            },
            {
                "data": "codigo",
                "className": "codigo",
            },
            {
                "data": "loja",
                "className": "loja",
            },
            {
                "data": "dataSigac",
                "className": "dataSigac",
            },
            {
                "data": "cgcOmni",
                "className": "cgcOmni",
            },
            {
                "data": "cgcSigac",
                "className": "cgcSigac",
            },
            {
                "data": "nomeOmni",
                "className": "nomeOmni",
            },
            {
                "data": "nomeSigac",
                "className": "nomeSigac",
            },
            {
                "data": "valorOmni",
                "className": "valorOmni",
            },
            {
                "data": "valorSigac",
                "className": "valorSigac",
            },
        ],
        "drawCallback" : function(settings) 
        {
            
            /*$('#filtroDataSigacDe'  ).val(settings.json.filtroDataDe    );
            $('#filtroDataSigacAte' ).val(settings.json.filtroDataAte   );*/
            
            $('#thTotalOmniFiltro'  ).html('R$ ' + settings.json.valorFiltroOmni   );
            $('#thTotalSigacFiltro' ).html('R$ ' + settings.json.valorFiltroSigac  );
            $('#thTotalOmni'        ).html('R$ ' + settings.json.valorTotalOmni    );
            $('#thTotalSigac'       ).html('R$ ' + settings.json.valorTotalSigac   );
	}
    });
    
    $('.filtro').on('change',function()
    {
        conciliacaoTable.draw();
    });
    
    $(document).on('click','.checkFiltro',function()
    {
        var checkBoxes              = $('.checkFiltro')                     ;
        var checkBoxesMarcados      = []                                    ;
        var checkBoxesNaoMarcados   = []                                    ;
        var indice                  = parseInt($(this).attr('value')) - 1   ;
        
        if($(this).hasClass('clip-checkbox-unchecked-2') || $(this).hasClass('clip-checkbox-partial'))
        {
            $(this).removeClass ('clip-checkbox-unchecked-2'    );
            $(this).addClass    ('clip-square'                  );
            
            if($(this).attr('value') == "0")
            {
                
                $.each(checkBoxes,function(index,variavel)
                {
                    
                    if(index == 0)
                    {
                        return;
                    }
                    
                    $(variavel).removeClass ('clip-checkbox-unchecked-2'    );
                    $(variavel).addClass    ('clip-square'                  );
                    
                    filtroDivergencia[index-1] = 1;
                    
                });
                
            }
            else
            {
                filtroDivergencia[indice] = 1;
            }
            
        }
        else
        {
            $(this).removeClass ('clip-square'                  );
            $(this).addClass    ('clip-checkbox-unchecked-2'    );
            
            if($(this).attr('value') == "0")
            {
                
                $.each(checkBoxes,function(index,variavel)
                {
                    
                    if(index == 0)
                    {
                        return;
                    }
                    
                    $(variavel).removeClass ('clip-square'                  );
                    $(variavel).addClass    ('clip-checkbox-unchecked-2'    );
                    
                    filtroDivergencia[index-1] = 0;
                    
                });
                
            }
            else
            {
                filtroDivergencia[indice] = 0;
            }
            
        }
        
        checkBoxesMarcados      = $('.clip-square'                  );
        checkBoxesNaoMarcados   = $('.clip-checkbox-unchecked-2'    );
        
        if  (
                (
                    (checkBoxes.length == checkBoxesMarcados.length) || 
                    ($(checkBoxes[0]).hasClass('clip-checkbox-partial') && checkBoxesMarcados.length == (checkBoxes.length-1))
                )
            )
        {
            $(checkBoxes[0]).removeClass   ('clip-checkbox-unchecked-2'    );
            $(checkBoxes[0]).removeClass   ('clip-checkbox-partial'        );
            $(checkBoxes[0]).addClass      ('clip-square'                  );
        }
        else if (
                    (checkBoxes.length == checkBoxesNaoMarcados.length) ||
                    ($(checkBoxes[0]).hasClass('clip-checkbox-partial') && checkBoxesNaoMarcados.length == (checkBoxes.length-1))
                )
        {
            $(checkBoxes[0]).removeClass   ('clip-square'                  );
            $(checkBoxes[0]).removeClass   ('clip-checkbox-partial'        );
            $(checkBoxes[0]).addClass      ('clip-checkbox-unchecked-2'    );
        }
        else
        {
            $(checkBoxes[0]).removeClass   ('clip-checkbox-unchecked-2'    );
            $(checkBoxes[0]).removeClass   ('clip-square'                  );
            $(checkBoxes[0]).addClass      ('clip-checkbox-partial'        );
        }
        
        conciliacaoTable.draw();
        
    });
    
    $('#upload-arquivo-form').ajaxForm(
    {
        beforeSubmit: function () 
        {

            if (!window.File && !window.FileReader && !window.FileList && !window.Blob)
            {
                alert("Por favor, seu navegador não suporta esta tarefa. Contate o Suporte.");
            }

            $('#painelArquivo').block({
                overlayCSS: {
                    backgroundColor: '#fff'
                },
                message: '<img src="https://credshow.sigacbr.com.br/js/loading.gif" /> Aguarde...',
                css: {
                    border: 'none',
                    color: '#333',
                    background: 'none'
                }
            });
            window.setTimeout(function () {
                $('#painelArquivo').unblock();
            }, 1000);

        },
        uploadProgress: function (event, position, total, percentComplete) 
        {

            $('#progressbox').show  (                       );
            $('#progressbar').width (percentComplete + '%'  ); //update progressbar percent complete
            $('#statustxt'  ).html  (percentComplete + '%'  ); //update status text

            if (percentComplete > 50)
            {
                $('#statustxt').css('color', '#000'); //change status text to white after 50%
            }
        },
        success: function (response, textStatus, xhr, form) 
        {

            var retorno = $.parseJSON(response);

            $.pnotify({
               title    : 'Notificação' ,
               text     : retorno.msgs  ,
               type     : retorno.type
            });

            console.log(retorno);

            $('#progressbox').fadeOut(3000);
            
            conciliacaoTable.draw();
            
        },
        error: function (xhr, textStatus, errorThrown) 
        {
            console.log("erro no formAjax");
            $('#progressbox').fadeOut(3000);
        },
        resetForm: false
    });
    
});