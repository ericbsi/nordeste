$(function () {

   $('.select2').select2();
   
   $(document).on('click', '#back_table', function(){
      $('.tabela_prop').show('slow');
      $('.estorno').hide('slow');
   });
   
   var tablePropostas = $('#grid_propostas').DataTable({
      "dom": 'T<"clear">lfrtip',
      "tableTools":
              {
                 "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
              },
      "processing": true,
      "serverSide": true,
      "ajax": {
         url: '/empresa/GCP/',
         type: 'POST',
         "data": function (d) {
            d.codigo_filter      = ''                             ,
            d.nome_filter        = ''                             ,
            d.filtroGrupoFilial  = $('#filtroGrupoFilial').val()
         }
      },
      "columns": [
         {"data": "contrato"},
         {"data": "codigo"},
         {"data": "data"},
         {"data": "cliente"},
         {"data": "repasse"},
         {"data": "parceiro"},
         {"data": "button"}
      ],
      "drawCallback": function (settings) {

         var api = this.api(), data;

         $(api.column(4).footer()).html(settings.json.customReturn.total);
      }
   });
   
   $(document).on('click', '#confirm_baixa', function(){
      $('#modal_confirm').modal('show');
      var prop_id = $(this).parent().find('input').val();
      $('#prop_id').val(prop_id);
   });

   $('#cbaixa_btn').on('click', function(){
      var prop_id = $('#prop_id').val();
      $.ajax({
                    type: 'POST',
                    url: '/empresa/baixaCP/',
                    data: {
                        "prop_id": prop_id
                    }
                }).done(function (dRt) {
                    var retorno = $.parseJSON(dRt);

                    $.pnotify({
                        title: "Informações",
                        text: retorno.msg,
                        type: 'success'
                    });
                });

      tablePropostas.draw();
      $('#modal_confirm').modal('hide');
   });

   $('#filtroGrupoFilial').on('change', function()
   {
      tablePropostas.draw();
   });
   
});