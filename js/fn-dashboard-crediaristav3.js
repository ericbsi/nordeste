$('#btn-video').on('click', function(){
   $('#video_div').toggle('slow');
   if($(this).text() == 'Ocultar Vídeo'){
       $(this).text('Comunicação Credshow');
   }else{
       $(this).text('Ocultar Vídeo');
   }
   $('#video_cred').get(0).pause();
});

var tablePropostas = $('#table_id').DataTable({
   "processing": true,
   "serverSide": true,
   "ajax": {
      url: '/proposta/propostasCrediarista',
      type: 'POST'      
   },
   "language": {
      "processing": "<img style='position:fixed; top:60%; left:50%;margin-top:-8px;margin-left:-8px;' src='https://credshow.sigacbr.com.br/js/loading.gif'>"
   },
   "columnDefs": [{
      "orderable": false,
      "targets": "no-orderable"
   }],
   "columns": [
      {"data": "btn_det"},
      {"data": "codigo"},
      {"data": "cliente"},
      {"data": "financeira"},
      {"data": "val_inicial"},
      {"data": "val_entrada"},
      {"data": "val_seguro"},
      {"data": "val_fin"},
      {"data": "parcelamento"},
      {"data": "val_final"},
      {"data": "msg_novas"},
      {"data": "btn"}
   ],
   "drawCallback" : function(settings){
      setTimeout(function(){
         tablePropostas.draw();
      },15000)
   }
});