$(document).ready(function () 
{
    
    $.pnotify.defaults.history = false;
    
    var selectRole = $("#modulo").select2   (
                                                {
                                                    data        :   [
                                                                        {
                                                                            id      : 0                 ,
                                                                            text    : 'Carregando..'
                                                                        }
                                                                    ],
                                                    placeholder : "Selecione um módulo..."              ,
                                                }
                                            );
    
    var selectFilial = $("#filiais").select2(
                                                {
                                                    data        :   [
                                                                        {
                                                                            id      : 0                 ,
                                                                            text    : 'Carregando..'
                                                                        }
                                                                    ],
                                                    placeholder : "Selecione uma filial..."             ,
                                                }
                                            );
    
    $("#username").on("blur", function()
    {
        
        var username = $(this).val();
        
        //chame o ajax de alteração
        $.ajax(
        {
            type    : "POST"                    ,
            url     : "/login/validarUsuario"   ,
            data: 
            {
                "username"  : username ,
            },
        }).done(function (dRt) 
        {

            var retorno = $.parseJSON(dRt);

            $.pnotify(
            {
                title   : 'Notificação'         ,
                text    : retorno.msg           ,
                type    : retorno.type
            });
            
            if(!retorno.hasErrors)
            {
                $("#senhaUsuario"   ).removeAttr("style"    );
                
                $("#password"       ).focus(                );
            }
            else
            {
                $("#senhaUsuario"   ).attr  ("style","display:none" );
            }
            
            $("#selectRole"     ).attr  ("style","display:none" );
            $("#selectFiliais"  ).attr  ("style","display:none" );
            $("#btnLogar"       ).attr  ("style","display:none" );
            
            $("#senhaUsuario"   ).val   ("");
            
            $("#selectRole"     ).select2("val","");
            $("#selectFiliais"  ).select2("val","");
                        
        })
        
    });
    
    $("#password").on("blur", function()
    {
        
        var username    = $("#username" ).val();
        var senha       = $(this        ).val();
        
        //chame o ajax de alteração
        $.ajax(
        {
            type    : "POST"                ,
            url     : "/login/validarSenha" ,
            data: 
            {
                "username"  : username ,
                "senha"     : senha    
            },
        }).done(function (dRt) 
        {

            var retorno = $.parseJSON(dRt);

            $.pnotify(
            {
                title   : 'Notificação'         ,
                text    : retorno.msg           ,
                type    : retorno.type
            });
            
            if(!retorno.hasErrors)
            {
                
                $.ajax(
                {
                    type    : "POST"                    ,
                    url     : "/login/getRoleUsuario"   ,
                    data: 
                    {
                        "username"  : username 
                    },
                }).done(function (dRt) 
                {
                    
                    var retornoData = $.parseJSON(dRt);
                    
                    if(!retornoData.hasErrors)
                    {                        
                        selectRole.select2  (
                                                {
                                                    data        : retornoData.data          ,
                                                    placeholder : "Selecione um módulo..." 
                                                }
                                            );
                        
                        $("#selectRole" ).removeAttr("style"            );
                        
                    }
                    else
                    {
                        $("#selectRole" ).attr  ("style","display:none" );
                        
                    }
                    
                });
            }
            else
            {                
                $("#selectRole"     ).attr("style","display:none");
            }

            $("#selectFiliais"  ).attr  ("style","display:none");
            $("#btnLogar"       ).attr  ("style","display:none");
                
        });
            
            
        $("#selectRole"     ).val   ("0"                    );
        $("#selectFiliais"  ).val   ("0"                    );
        
    });
    
    $("#modulo").on("change", function()
    {
        
        var username = $("#username").val();
        var idModulo = $(this       ).val();
        
        //chame o ajax de alteração
        $.ajax(
        {
            type    : "POST"                        ,
            url     : "/login/getFiliaisUsuario"    ,
            data: 
            {
                "username"  : username ,
                "idModulo"  : idModulo 
            },
        }).done(function (dRt) 
        {

            var retorno = $.parseJSON(dRt);

            $.pnotify(
            {
                title   : 'Notificação'         ,
                text    : retorno.msg           ,
                type    : retorno.type
            });
            
            if(!retorno.hasErrors)
            {
                
                selectFilial.select2(
                                        {
                                            data        : retorno.data              ,
                                            placeholder : "Selecione uma filial..." 
                                        }
                                    );
                                    
                $("#selectFiliais"  ).removeAttr("style"            );
                
            }
            else
            {
                $("#selectFiliais"  ).attr  ("style","display:none" );
                $("#selectFiliais"  ).val   (""                     );
            }
            
            $("#btnLogar"       ).attr("style","display:none"   );

        })
    });
    
    $("#selectFiliais").on("change", function()
    {
        $("#btnLogar").removeAttr("style");
    });
    
});