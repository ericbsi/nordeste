function format(d) {
    return $.each(d.detalhe, function (index) {

    })
}

$(function () {
    $(document).on('click', '.btn-add-parcela-cobranca', function (e) {
        return false;
    });
});

$(function () {

    $('#filiais_select').multiselect({
        buttonWidth: '300px',
        numberDisplayed: 2,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        buttonText: function (options, select) {

            if (options.length == 0) {
                return 'Selecionar Parceiros <b class="caret"></b>'
            } else if (options.length == 1) {
                return '1 Parceiro Selecionado <b class="caret"></b>'
            } else {
                return options.length + ' Parceiros Selecionados <b class="caret"></b>'
            }
        },
    });

    var tableAtrasos = $('#grid_atrasos').DataTable({
        "processing": true,
        "serverSide": true,
        "cache": true,
        "ajax": {
            url: '/administradorDeParceiros/getProducaoParceiros/',
            type: 'POST',
            "data": function (d) {
                d.filiais = $("#filiais_select").val(),
                        d.data_de = $("#data_de").val(),
                        d.data_ate = $("#data_ate").val()
            },
        },
        "language": {
            "processing": "<img style='position:fixed; top:60%; left:50%;margin-top:-8px;margin-left:-8px;' src='https://beta.sigacbr.com.br/js/loading.gif'>"
        },
        "columnDefs": [{
                "orderable": false,
                "targets": "no-orderable"
            }],
        "columns": [
            {
                "class": "details-control",
                "orderable": false,
                "data": "btn_status", //null,
                "defaultContent": ""
            },
//            {"data": "btn_status"},
            {"data": "valor"},
            {"data": "porcentagem"},
            {"data": "btn_analitico"},
        ],
        /*"drawCallback" : function(settings) {
         $('#tfoot-total-atraso').html( settings.json.customReturn.valorTotal );
         }*/
    });

    var detailRows = [];

    $('#grid_atrasos tbody').on('click', 'tr td:first-child', function () {

        var tr = $(this).closest('tr');
        var row = tableAtrasos.row(tr);
        var idx = $.inArray(tr.attr('id'), detailRows);

        if (row.child.isShown()) {
            tr.removeClass('details');
            row.child.hide();
            detailRows.splice(idx, 1);
        } else {

            tr.addClass('details');
            row.child(format(row.data())).show();

            if (idx === -1) {
                detailRows.push(tr.attr('id'));
            }
        }

    });

    tableAtrasos.on('draw', function () {

        $.each(detailRows, function (i, id) {
            $('#' + id + ' td:first-child').trigger('click');
        });

    });

    $('#btn-filter').on('click', function () {
        tableAtrasos.draw();
        return false;
    });

});