$(function(){
    
    var marcados = [];
    
    var qtdFiltrados;
    
    /*var financeirasSelect = $("#selectAdmFinanceira").multiselect(
    {

    	buttonWidth                     : '300px'   ,
     	numberDisplayed 		: 2         ,
      	enableFiltering                 : true      ,
      	enableCaseInsensitiveFiltering  : true      ,

      	buttonText:function(options, select)
        {
            
            if (options.length == 0)
            {
                return 'Selecionar Financeiras <b class="caret"></b>'
            }
            else if (options.length == 1)
            {
                return options.context.selectedOptions[0].label + ' selecionada <b class="caret"></b>'
            }
            else
            {
                return options.length + ' Financeiras selecionadas <b class="caret"></b>'
            }

      	}
        
    });*/
    
    var tablePropostas = $('#grid_propostaFormalizacao').DataTable(
    {

        "processing"    : true  ,
        "serverSide"    : true  ,
        "ajax"          : 
        {
            url     : '/formalizacao/listarPropostas/'  ,
            type    : 'POST'                            ,
            "data"  : function (d) 
            {
                
                d.codigo_filter = $("#codigo_filter"        ).val() ;
                d.cpf_filter    = $("#cpf_filter"           ).val() ;
                d.nome_filter   = $("#nome_filter"          ).val() ;
                d.data_filter   = $("#data_filter"          ).val() ;
                d.selecionaTudo = $("#btnSelecionaTudo"     ).val() ;
                d.comJuros      = $("#btnFiltroComJuros"    ).val() ;
                d.semJuros      = $("#btnFiltroSemJuros"    ).val() ;
                d.notasMarcadas = marcados                          ;
//                d.soHoje        = $("#soHoje").hasClass("clip-square")
            }
        },

        "columns": 
        [
            {
                "data"      : "check"   ,
                "className" : "thCheck"
            },
            {
                "data"  : "modalidade"
            },
            {
                "data"  : "codigoProposta"
            },
            {
                "data"  : "nomeFilial"
            },
            {
                "data"  : "cpf"
            },
            {
                "data"  : "nomeCliente"
            },
            {
                "data"  : "dataProposta"
            },
            {
                "data"  : "valorFinanciado"
            },
            {
                "data"  : "parcelamento"
            },
            {
                "data"  : "valorFinal"
            },
            {
                "className"         : 'details-control' ,
                "orderable"         : false             ,
                "data"              : null              ,
                "defaultContent"    : ''
            }
        ],

    });
    
/*    tablePropostasNFe.on
    (   
        "draw"          ,
        function()
        {

            marcados        = tablePropostasNFe.data().ajax.json().marcados ;
            qtdFiltrados    = tablePropostasNFe.page.info().recordsDisplay  ;
                        
            if($("#btnFiltroComNFe").hasClass("active"))
            {
                
                $("#btnSelecionaTudo").removeAttr("style");
                
            }
            else
            {
                
                $("#btnSelecionaTudo").attr("style","display : none");
                
            }
        
            habilitaGerar();
            
        }
    );*/

    $(document).on('change','.input_filter', function()
    {
        tablePropostas.draw();
    });

    $(document).on('click','.checkNF', function()
    {
        
        var elemento = $(this);
        
        if($(this).hasClass("clip-square"))
        {
            $(this).removeClass ("clip-square"                  );
            $(this).addClass    ("clip-checkbox-unchecked-2"    );
            
            if($(this).attr("id") == "iSelecionarTudo")
            {

                $("#btnSelecionaTudo").attr("value","0");

                tablePropostas.draw();

            }
            else
            {
                
                if($.inArray($(this).attr("value"),marcados) >= 0)
                {
                    
                    marcados =  $.grep(marcados, function(value) 
                                {
                                    return value !== elemento.attr("value");
                                });
                }
        
                habilitaGerar();
                
            }
            
        }
        else
        {
            
            if($(this).attr("id") == "iSelecionarTudo")
            {

                $("#btnFiltroComNFe").addClass      ("active"           );
                $("#btnFiltroComNFe").val           ("1"                );
                $("#btnFiltroComNFe").removeClass   ("btn-blue");

                $("#btnFiltroSemNFe").removeClass   ("active"           );
                $("#btnFiltroSemNFe").val           ("0"                );
                $("#btnFiltroSemNFe").addClass      ("btn-yellow"       );

                $("#btnFiltroProNFe").removeClass   ("active"           );
                $("#btnFiltroProNFe").val           ("0"                );
                $("#btnFiltroProNFe").addClass      ("btn-green"        );

                $("#btnFiltroSemChv").removeClass   ("active"           );
                $("#btnFiltroSemChv").val           ("0"                );
                $("#btnFiltroSemChv").addClass      ("btn-red"          );
                
                $("#btnSelecionaTudo").attr("value","1");
                
                tablePropostas.draw();

            }
            else
            {
                
                if($.inArray($(this).val(),marcados) < 0)
                {
                    marcados.push(elemento.attr("value"));
                }
        
                habilitaGerar();
                
            }
            
            $(this).addClass    ("clip-square"                  );
            $(this).removeClass ("clip-checkbox-unchecked-2"    );
        }
        
    });
    
    $("#botaoGerar").on("click", function()
    {
        
        $.ajax  
        (
            {

                type    : "POST"                    ,
                url     : "/integracao/gerarCNAB"   ,
                data    : function(d)
                {
                    d.codigo_filter = $("#codigo_filter"    ).val() ;
                    d.nome_filter   = $("#nome_filter"      ).val() ;
                    d.data_filter   = $("#data_filter"      ).val() ;
                    d.comNFe        = $("#btnFiltroComNFe"  ).val() ;
                    d.semNFe        = $("#btnFiltroSemNFe"  ).val() ;
                    d.proNFe        = $("#btnFiltroProNFe"  ).val() ;
                    d.selecionaTudo = $("#btnSelecionaTudo" ).val() ;
                    d.notasMarcadas = marcados  //getMarcados($('.checkNF'))
                },
            }
        )
        .done
        (
            function (dRt)
            {
                
                var retorno = $.parseJSON(dRt);
                
                $.pnotify
                (
                    {
                        title   : retorno.title ,
                        text    : retorno.msg   ,
                        type    : retorno.type
                    }
                );
                
                if(!retorno.hasErrors)
                {
                    
                    var link        = document.createElement("a")   ;
                    link.download   = retorno.nomeArquivo           ;
                    link.href       = retorno.url                   ;
                    link.click()                                    ;
                    
                    tablePropostasNFe.draw();
                    
//                    document.location = 'data:Application/octet-stream,' + encodeURIComponent(retorno.cnab);
                    
//                    window.location = retorno.url;
//                    console.log(retorno.url);
                }
                
            }
        );

    });
    
/*    function habilitaGerar()
    {
        
        if (marcados.length > 0)
        {
            
            if(marcados.length !== qtdFiltrados)
            {
                $("#iSelecionarTudo"    ).removeClass   ("clip-square"                          );
                $("#iSelecionarTudo"    ).removeClass   ("clip-checkbox-unchecked-2"            );
                $("#iSelecionarTudo"    ).addClass      ("clip-checkbox-partial"                );
                
                $("#btnSelecionaTudo"   ).attr          ("value"                        ,"2"    );
            }
            else
            {
                $("#iSelecionarTudo"    ).addClass      ("clip-square"                          );
                $("#iSelecionarTudo"    ).removeClass   ("clip-checkbox-unchecked-2"            );
                $("#iSelecionarTudo"    ).removeClass   ("clip-checkbox-partial"                );
                
                $("#btnSelecionaTudo"   ).attr          ("value"                        ,"1"    );
            }
            
            $("#botaoGerar").removeAttr("disabled");
        }
        else
        {
            
            $("#iSelecionarTudo"    ).removeClass   ("clip-square"                          );
            $("#iSelecionarTudo"    ).addClass      ("clip-checkbox-unchecked-2"            );
            $("#iSelecionarTudo"    ).removeClass   ("clip-checkbox-partial"                );

            $("#btnSelecionaTudo"   ).attr          ("value"                        ,"0"    );
                
            $("#botaoGerar").attr("disabled","disabled");
        }
        
    }*/
    
    // Add event listener for opening and closing details
    $('#grid_propostaFormalizacao tbody').on('click', 'td.details-control', function () 
    {

        var tr          = $(this).closest('tr')             ;
        var row         = tablePropostas.row(tr)            ;
        var rowsTam     = tablePropostas.rows()[0].length   ;
        var rowData     = row.data()                        ;
        var elemento    = $(this)                           ;
        
        idProposta      = rowData.idProposta                ;

        if (row.child.isShown()) {

            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
            
        }
        else 
        {

            for (i = 0; i < rowsTam; i++)
            {

                if (tablePropostas.row(i).child.isShown()) 
                {
                    tablePropostas.row(i).child.hide();
                }
            }

            $('td.details-control').closest('tr').removeClass('shown');

            formatSubTableAnexos(row);
            
            tableAnexos = $('#grid_anexos_contratos').dataTable(
            {
               "processing"     : true,
                "serverSide"    : true,
                "ajax"          : 
                {
                    url         :   '/proposta/getAnexoContrato'    ,
                    type        :   'POST'                          ,
                    data        :   function(d) 
                    {
                        d.Entidade      = "Proposta"    ;
                        d.Entidade_id   = idProposta    ;
                    }

                },
                "columns"   : 
                [
                    {"data" : "descricao"   },
                    {"data" : "ext"         },
                    {"data" : "data_envio"  },
                    {"data" : "btn"         },
                ]

            });
            
            tableNFe = $('#grid_anexos_nfe').dataTable(
            {
               "processing"     : true,
                "serverSide"    : true,
                "ajax"          : 
                {
                    url         :   '/proposta/getNFe'  ,
                    type        :   'POST'              ,
                    data        :   function(d) 
                    {
                        d.idProposta    = idProposta    ;
                    }

                },
                "columns"   : 
                [
                    {"data" : "serie"       },
                    {"data" : "documento"   },
                    {"data" : "data"        },
                    {"data" : "chave"       },
                    {"data" : "observacao"  },
                    {"data" : "btn"         },
                ]

            });
            
            
            $(document).on("click",".btnAnexarNF",function()
            {
                
                $('#idNF'       ).attr  ('value'    , $(this).val() );
                
                $("#modalAnexo" ).modal ("show"                     );

            });
            

            $('#form-add-danfe').ajaxForm({

                beforeSubmit    : function(){

                    if ( window.File && window.FileReader && window.FileList && window.Blob && $('#anexoDanfe').valid() )
                    {
                        var fsize = $('#anexoDanfe')[0].files[0].size;
                        var ftype = $('#anexoDanfe')[0].files[0].type;

                        switch( ftype )
                        {                  
                            case 'text/xml':
                            case 'image/gif':
                            case 'image/jpeg': 
                            case 'image/pjpeg':
                            case 'application/pdf':
                            break;
                            default:
                                alert("Tipo de arquivo não permitido!");
                            return false;
                        }

                        if( fsize > 5242880 )
                        {
                            alert("Arquivo muito grande!");
                            return false
                        }
                    }

                    else
                    {
                        alert("Revise o formulário!");
                    }
                },

                success         : function( response, textStatus, xhr, form ) {

                    var retorno = $.parseJSON(response);

                    $.pnotify({
                            title    : 'Notificação'        ,
                            text     : retorno.msg          ,
                            type     : retorno.pntfyClass
                    });

                    if( retorno.hasErrors != 1 )
                    {

                        $('#grid_anexos_nfe').DataTable().draw();

                        $('#modalAnexo'     ).modal     ('hide'     );
                        $('#form-add-danfe' ).trigger   ("reset"    );

                    }

                },

            });
            
        }
        
    });
    
    $(document).on("click",".filtroModalidade",function()
    {
        
        if($(this).hasClass("active"))
        {
            
            $(this).removeClass ("active"           );
            $(this).val         ("0"                );
            
            if($(this).attr("id") == "btnFiltroComJuros")
            {
                $(this).addClass("btn-beige");
            }
            else
            {
                $(this).addClass("btn-dark-beige");
            }
            
        }
        else
        {
            
            $(this).addClass    ("active"           );
            $(this).val         ("1"                );
            
            if($(this).attr("id") == "btnFiltroComJuros")
            {
                $(this).removeClass("btn-beige");
            }
            else
            {
                $(this).removeClass("btn-dark-beige");
            }
            
        }
        
        tablePropostas.draw();
        
    });
    
    
    function formatSubTableAnexos(row)
    {
        
        // `d` is the original data object for the row
        var data =  '<div class= "row">'+
                    '   <div class= "col-md-12">'+
                    '       <div class= "panel panel-default">'+
                    '           <div class= "panel-heading">'+
                    '               <i class= "clip-clip">'+
                    '               </i>'+
                    '               <b>Anexos Contrato</b>'+
                    '           </div>'+
                    '           <div class= "panel-body">'+
                    '               <table id="grid_anexos_contratos" class="table table-striped table-hover table-full-width dataTable table-bordered">' +
                    '                   <thead>' +
                    '                       <tr>' +
                    '                           <th>Descrição</th>' +
                    '                           <th>Extensão</th>' +
                    '                           <th>Data de Envio</th>' +
                    '                           <th width="3px"></th>' +
                    '                       </tr>' +
                    '                   </thead>' +
                    '                   <tbody>' +
                    '                   </tbody>' +
                    '               </table>' +
                    '           </div>' +
                    '       </div>' +
                    '   </div>' +
                    '</div>';
            
        data +=     '<div class= "row">'+
                    '   <div class= "col-md-12">'+
                    '       <div class= "panel panel-default">'+
                    '           <div class= "panel-heading">'+
                    '               <i class= "clip-file-2">'+
                    '               </i>'+
                    '               <b>Notas Fiscais</b>'+
                    '           </div>'+
                    '           <div class= "panel-body">'+
                    '               <table id="grid_anexos_nfe" class="table table-striped table-hover table-full-width dataTable table-bordered">' +
                    '                   <thead>' +
                    '                       <tr>' +
                    '                           <th>Série</th>' +
                    '                           <th>Documento</th>' +
                    '                           <th>Data</th>' +
                    '                           <th>Chave Eletrônica</th>' +
                    '                           <th>Observação</th>' +
                    '                           <th width="3px"></th>' +
                    '                       </tr>' +
                    '                   </thead>' +
                    '                   <tbody>' +
                    '                   </tbody>' +
                    '               </table>' +
                    '           </div>' +
                    '       </div>' +
                    '   </div>' +
                    '</div>';

        row.child(data).show();
        
    }

/*    function getMarcados(elementos)
    {

        var retorno = [];

        $.each(elementos, function (indice, variavel)
        {
            
            if($(variavel).attr("id") !== "iSelecionarTudo" ) 
            {
                retorno.push($(variavel).attr('value'));
            }

        })

        return retorno;

    }*/
    

});