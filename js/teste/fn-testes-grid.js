$(document).ready(function ()
{

    $(function () {

        $('.select2').select2();

    });

    var cpfCliente = "";

    var selectContatos;

    var selectContatosSeq;
    
    var gridAtendimentos;

    $(document).on('change', '#check_select_all', function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
        if ($(this).prop('checked') === true) {
            $('#atendimento_sel').show();
        } else {
            $('#atendimento_sel').hide();
        }
    });

    $(document).on('click', '.check_parc', function () {
        if ($('#check_select_all').prop('checked') === true) {
            $('#check_select_all').not(this).prop('checked', this.checked);
        }
    });

    $(document).on('change', '.check_parc', function () {
        $('#atendimento_sel').hide();
        $("input[class='check_parc']").each(function () {
            if ($(this).prop('checked') === true) {
                $('#atendimento_sel').show();
            }
        });
    });

    $('#toggle-filter').on('click', function () {
        if ($('#toggle-filter').hasClass('btn-success')) {
            $('#toggle-filter').removeClass('btn-success');
            $('#toggle-filter').addClass('btn-danger');
            $('#toggle-filter').text('Ocultar Opções de Filtragem');
        } else {
            $('#toggle-filter').addClass('btn-success');
            $('#toggle-filter').text('Mostrar Opções de Filtragem');
        }
        $('.filtragem').toggle('slow');
    });

    $(document).on('submit', '#form-atend', function () {
        return false;
    });

    $(document).on('submit', '#form-seq', function () {
        return false;
    });

    $(document).on('submit', '#form-chamado', function () {
        return false;
    });

    var ids = new Array();

    $(document).on('click', '#atendimento_sel', function () {

        $("input[name='parc_id[]']:checked").each(function () {
            ids.push($(this).val());
            $(this).closest('tr').hide();
        });
        
        if (ids.length > 0) {
            $('.tableAtend').hide('slow');
            $('.form-atendimento').show('slow');

            var now = new Date();
            var hora = now.toTimeString().substring(0, 8);

            $('#inputHorai').val(hora);

            $('input:checkbox').hide('slow');
            $('#atendimento_sel').hide();
        } else {
            $.pnotify({
                title: "Parcelas não foram marcadas!",
                text: "Marque pelo menos uma parcela para iniciar o atendimento.",
                type: "error"
            });
        }
    });

    var time = setInterval(function () {
        timer();
    }, 1000);

    function timer() {
        var d = new Date();
        var t = d.toTimeString().substring(0, 8);
        if (document.getElementById("temporizador") !== null) {
            document.getElementById("temporizador").value = t;
        }
    }

    var time2 = setInterval(function () {
        timer2();
    }, 1000);

    function timer2() {
        var d = new Date();
        var t = d.toTimeString().substring(0, 8);
        if (document.getElementById("horaf") !== null) {
            document.getElementById("horaf").value = t;
        }
    }
    //formatação do capo de seleção de filiais
    var selectFiliais = $('#selectFiliais').multiselect(
            {
                buttonWidth: '300px',
                numberDisplayed: 2,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                buttonText: function (options, select)
                {

                    if (options.length == 0) {
                        return 'Selecionar Parceiros <b class="caret"></b>';
                    } else if (options.length == 1) {
                        return '1 Parceiro Selecionado <b class="caret"></b>';
                    } else {
                        return options.length + ' Parceiros Selecionados <b class="caret"></b>';
                    }

                }
            });
    //formatação do campo de seleção de nucleos
    var selectNucleoFiliais = $('#nucleoFiliaisSelect').multiselect(
            {
                buttonWidth: '300px',
                numberDisplayed: 2,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                buttonText: function (options, select)
                {

                    if (options.length == 0) {
                        return 'Selecionar Núcleo de Filiais <b class="caret"></b>';
                    } else if (options.length == 1) {
                        return '1 Núcleo Selecionado <b class="caret"></b>';
                    } else {
                        return options.length + ' Núcleos Selecionados <b class="caret"></b>';
                    }

                }
            });
    //formatação do campo de seleção de grupos
    var selectGrupoFiliais = $('#selectGrupoFiliais').multiselect(
            {
                buttonWidth: '300px',
                numberDisplayed: 2,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                buttonText: function (options, select)
                {

                    if (options.length == 0) {
                        return 'Selecionar Grupo de Filiais <b class="caret"></b>';
                    } else if (options.length == 1) {
                        return '1 Grupo Selecionado <b class="caret"></b>';
                    } else {
                        return options.length + ' Grupo Selecionados <b class="caret"></b>';
                    }

                }
            });

    //quando algum núcleo for selecionado, atualizar os outros dois campos de grupo e filiais
    selectNucleoFiliais.on('change', function ()
    {

        idsNucleo = $(this).val();

        $.ajax(
                {
                    type: "POST",
                    url: "/grupoFiliais/getGrupoFilial/",
                    "data": {
                        idNucleos: idsNucleo
                    }
                }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            if (!(retorno.hasErrors))
            {
                selectGrupoFiliais.val(retorno.dataGrupos);
                selectFiliais.val(retorno.dataFiliais);

                selectGrupoFiliais.multiselect("refresh");
                selectFiliais.multiselect("refresh");

            } else
            {
                console.log(retorno);
            }

        });

    });
    //quando algum grupo for selecionado, atualizar os outros dois campos de filiais e núcleos
    selectGrupoFiliais.on('change', function ()
    {

        console.log($(this).val());

        idsGrupo = $(this).val();

        $.ajax(
                {
                    type: "POST",
                    url: "/grupoFiliais/getNucleoFilial/",
                    "data":
                            {
                                idGrupos: idsGrupo
                            }
                }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            if (!(retorno.hasErrors))
            {
                selectNucleoFiliais.val(retorno.dataNucleos);
                selectFiliais.val(retorno.dataFiliais);

                selectNucleoFiliais.multiselect("refresh");
                selectFiliais.multiselect("refresh");

            } else
            {
                console.log(retorno);
            }

        });

    });
    //quando alguma filial for selecionada, atualizar os outros dois campos de grupo e núcleo
    selectFiliais.on('change', function ()
    {

        idsFilial = $(this).val();

        $.ajax(
                {
                    type: "POST",
                    url: "/grupoFiliais/getFilial/",
                    "data": {
                        idFiliais: idsFilial
                    }
                }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);
            console.log(retorno);

            if (!(retorno.hasErrors))
            {
                selectNucleoFiliais.val(retorno.dataNucleos);
                selectGrupoFiliais.val(retorno.dataGrupos);

                selectNucleoFiliais.multiselect("refresh");
                selectGrupoFiliais.multiselect("refresh");

            } else
            {
            }

        });

    });
    //ao clicar no botão filtrar redesenhar a tabela de cobranças..
    $('#btn-filter').on('click', function () {
        gridCobranca.draw();
        return false;
    });

    //Dados da tabela de inadiplências (Módulo Cobra's)
    var gridCobranca = $('#gridCobranca').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax":
                {
                    url: '/cobranca/listarAtrasados',
                    type: 'POST',
                    "data": function (d) {
                        d.filiais = $("#selectFiliais").val(),
                        d.data_de = $("#data_de").val(),
                        d.data_ate = $("#data_ate").val(),
                        d.aghoje = $('#iaghoje').val(),
                        d.mtodos = $('#imtodos').val(),
                        d.csc = $('#ifcsc').val(),
                        d.agend = $('#iagend').val(),
                        d.fich = $('#ifichados').val(),
                        d.cpf = $('#cpf_cliente').val(),
                        d.fdic = $('#iffdic').val(),
                        d.qtd_atrasos_de = $('#qtd_atrasos_de').val(),
                        d.qtd_atrasos_ate = $('#qtd_atrasos_ate').val();
                    }
                },
        "columnDefs": [{
                "orderable": false,
                "targets": "no-orderable"
            }],
        "columns": [
            {
                "class": "details-control",
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            {"data": "cpf"},
            {"data": "fichamento"},
            {"data": "nome_cliente"},
            {"data": "qtd_atrasos"},
            {"data": "maior_atraso"},
            {"data": "agenda"}
        ]
    });

    //subtable para cada atraso listado na grid de cobranças
    $('#gridCobranca tbody').on('click', 'td.details-control', function () {

        var tr = $(this).closest('tr');
        var row = gridCobranca.row(tr);
        var rowsTam = gridCobranca.rows()[0].length;

        if (row.child.isShown()) {

            row.child.hide('slow');
            tr.removeClass('shown');
            ids = new Array();

        } else
        {

            for (i = 0; i < rowsTam; i++)
            {

                if (gridCobranca.row(i).child.isShown()) {
                    gridCobranca.row(i).child.hide('slow');
                }
            }

            $('td.details-control').closest('tr').removeClass('shown');

            cpfCliente = row.data().numeroCPF;

//            console.log(row.data());

            formatSubTable(row.data(), tr, row);
                        
            $("#gridParcelas tr:nth-child(odd)").addClass("danger");
            $("#gridAtendimentos tr:nth-child(odd)").addClass("danger");
        }

    });

    $(document).on('click', '#btn_ncontato', function () {
            $.ajax({
                type: 'GET',
                url: '/cobranca/addContato',
                data: {
                    "id_cliente": $('#cliente_ncontato').val(),
                    "num_ncontato": $('#num_ncontato').val()
                }

            }).done(function (dRt) {
                var jsonReturn = $.parseJSON(dRt);

                $.pnotify({
                    title: 'Informações',
                    text: jsonReturn.msg,
                    type: jsonReturn.type
                });

                if (!jsonReturn.hasErrors) {
                    $.ajax({
                        type: 'GET',
                        url: '/cobranca/contatosClientes',
                        data: {
                            "id_cliente": $('#cliente_ncontato').val()
                        }

                    }).done(function (dRt2) {
                        var jsonReturn = $.parseJSON(dRt2);
                        selectContatos.select2({
                            data: jsonReturn
                        });

                        selectContatosSeq.select2({
                            data: jsonReturn
                        });
                    });
                }
            });
            $('#num_ncontato').val("");
            $('#modal_ncontato').modal('hide');
        });
    
    $(document).on('click', '#mtodos', function () {
        if ($(this).hasClass('fa fa-square')) {
            $(this).removeClass('fa fa-square');
            $(this).addClass('fa fa-square-o');
            $('#imtodos').val(0);
            gridCobranca.draw();
        } else {
            $(this).removeClass('fa fa-square-o');
            $(this).addClass('fa fa-square');
            $('#imtodos').val(1);
            gridCobranca.draw();
        }
    });
    
    $(document).on('click', '#fichados', function () {
        if ($(this).hasClass('fa fa-square')) {
            $(this).removeClass('fa fa-square');
            $(this).addClass('fa fa-square-o');
            $('#ifichados').val(0);
            gridCobranca.draw();
        } else {
            $(this).removeClass('fa fa-square-o');
            $(this).addClass('fa fa-square');
            $('#ifichados').val(1);
            gridCobranca.draw();
        }
    });

    $(document).on('click', '#fdic', function () {
        if ($(this).hasClass('fa fa-square')) {
            $(this).removeClass('fa fa-square');
            $(this).addClass('fa fa-square-o');
            $('#iffdic').val(0);
            gridCobranca.draw();
        } else {
            $(this).removeClass('fa fa-square-o');
            $(this).addClass('fa fa-square');
            $('#iffdic').val(1);
            gridCobranca.draw();
        }
    });
    
    $(document).on('click', '#aghoje', function () {
        if ($(this).hasClass('fa fa-square')) {
            $(this).removeClass('fa fa-square');
            $(this).addClass('fa fa-square-o');
            $('#iaghoje').val(0);
            gridCobranca.draw();
        } else {
            $(this).removeClass('fa fa-square-o');
            $(this).addClass('fa fa-square');
            $('#iaghoje').val(1);
            gridCobranca.draw();
        }
    });

    $(document).on('click', '#agend', function () {
        if ($(this).hasClass('fa fa-square')) {
            $(this).removeClass('fa fa-square');
            $(this).addClass('fa fa-square-o');
            $('#iagend').val(0);
            gridCobranca.draw();
        } else {
            $(this).removeClass('fa fa-square-o');
            $(this).addClass('fa fa-square');
            $('#iagend').val(1);
            gridCobranca.draw();
        }
    });

    $('#voltar-cha').on('click', function () {
        $('.tableAtend').show('slow');
        $('.form-chamado').hide('slow');
        return false;
    });

    $(document).on('click', '.btn-add-fich', function () {
        
        $('#modal_form_new_fichamento'  ).modal ('show'         );
        $('#Parcela_id'                 ).val   ($(this).val()  );
        
        return false;
    });

    $(document).on('click', '.btn-rmv-fich', function () 
    {
        
        $('#id_status_fichamento'           ).val   ($(this).val()  );
        $('#modal_form_remove_fichamento'   ).modal ('show'         );

        return false;
    });

    $(document).on('click', '#csc', function () {
        if ($(this).hasClass('fa fa-square')) {
            $(this).removeClass('fa fa-square');
            $(this).addClass('fa fa-square-o');
            $('#ifcsc').val(0);
            gridCobranca.draw();
        } else {
            $(this).removeClass('fa fa-square-o');
            $(this).addClass('fa fa-square');
            $('#ifcsc').val(1);
            gridCobranca.draw();
        }
    });

    //desabilitar o botão de confirmar fichamento, este só será desabilitado quando o comprovante de fichamento for anexado..
    $('#btn-send-fich').attr('disabled', true);

/*    //habilitando o botão de confirmar fichamento: houve uma mudança no campo e o valor do campo é diferente de vazio..
    $('#ComprovanteFile2').on('change', function () {
        if ($('#ComprovanteFile2').val() != '') {
            $('#btn-send-fich').attr('disabled', false);
        }
    });*/
    
    //habilitando o botão de confirmar fichamento: houve uma mudança no campo e o valor do campo é diferente de vazio..
    $(document).on('change', '.fichar', function () 
    {
        
        var qtdPreenchido = 0;
        
        $.each($('.fichar'), function(i,variavel)
        {
        
            if ($(variavel).val() !== '' && $(variavel).val() > "0") 
            {
                qtdPreenchido++;
            }
            
        });
        
        if($('.fichar').size() == qtdPreenchido+1)
        {
            $('#btn-send-fich').attr('disabled', false);
        }
        else
        {
            /*console.log($('.fichar'));
            console.log($('.fichar').size());
            console.log(qtdPreenchido);*/
        }
        
    });
    
    $('#btn-send-fich').on('click',function()
    {
        
        $.ajax(
        {
            type    : 'POST'                        ,
            url     : '/cobranca/negativarCliente'  ,
            data    : 
            {
                "parcelaId"         : $('#Parcela_id'                   ).val(),
                "orgaoNegacao"      : $('#select_orgao_negacao'         ).val(),
                "observacao"        : $('#textarea_observacaoInclui'    ).val(),
                "senha"             : $('#senha'                        ).val()
            }

        }).done(function (dRt) 
        {
            var jsonReturn = $.parseJSON(dRt);

            $.pnotify(
            {
                title   : jsonReturn.titulo   ,
                text    : jsonReturn.texto    ,
                type    : jsonReturn.tipo
            });

            if (!jsonReturn.hasErrors) 
            {
                
                $('#modal_form_new_fichamento'  ).modal     ('hide'     );
                $('#form-add-fichamento'        ).trigger   ("reset"    );
        
                btnAddFichamento = $('button[value=' + $('#Parcela_id').val() + ']');
                
                console.log("Linha 978: " + $('#Parcela_id').val());
                
                if(btnAddFichamento.hasClass('btn-add-fich'))
                {
                    
                    console.log(btnAddFichamento);
                    
                    btnAddFichamento.attr("disabled","disabled");
                    
                    console.log(btnAddFichamento);
                }
                else
                {
                    console.log(btnAddFichamento);        
                }
                
            }
        });
            
    });
    
    $('#btn-remove-fich').on('click',function()
    {
        
        $.ajax(
        {
            type    : 'POST'                    ,
            url     : '/cobranca/removerFich'   ,
            data    : 
            {
                "fichamentoId"  : $('#id_status_fichamento'         ).val() ,
                "observacao"    : $('#textarea_observacaoRemove'    ).val() ,
                "senha"         : $('#senha_remove'                 ).val() ,
                "idMotivo"      : $('#selectMotivosExclusao'        ).val()
            }

        }).done(function (dRt) 
        {
            var jsonReturn = $.parseJSON(dRt);

            $.pnotify(
            {
                title   : jsonReturn.titulo   ,
                text    : jsonReturn.texto    ,
                type    : jsonReturn.tipo
            });

            if (!jsonReturn.hasErrors) 
            {
                
                $('#modal_form_remove_fichamento').modal('hide');
        
                btnRemoveFichamento = $('button[value=' + $('#id_status_fichamento').val() + ']'); //verificar
                
                if(btnRemoveFichamento.hasClass('btn-remove-fich'))
                {
                    btnRemoveFichamento.attr("disabled","disabled");
                }
                
            }
        });
            
    });

    //Não é possível enviar arquivos por ajax, foi necessário colocar a requisição no "action" do formulário..
    $('#form-add-fichamento').ajaxForm({
        beforeSubmit: function () {
            //validando o arquivo que está sendo enviado
            if ($('#ComprovanteFile2').val() != '')
            {
                if (window.File && window.FileReader && window.FileList && window.Blob && $('#form-add-fichamento').valid())
                {
                    var fsize = $('#ComprovanteFile2')[0].files[0].size;
                    var ftype = $('#ComprovanteFile2')[0].files[0].type;

                    switch (ftype)
                    {
                        case 'image/png':
                        case 'image/gif':
                        case 'image/jpeg':
                        case 'image/pjpeg':
                        case 'application/pdf':
                            break;
                        default:
                            alert("Tipo de arquivo nÃ£o permitido!");
                            return false;
                    }
                    if (fsize > 5242880)
                    {
                        alert("Arquivo muito grande!");
                        return false
                    }
                } else
                {
                    alert("Revise o formulÃ¡rio!");
                }
            }
        },
        //em caso de sucesso, notificar o usuário e ocultar o modal de fichamento
        success: function (response, textStatus, xhr, form) {

            var retorno = $.parseJSON(response);

            $('#modal_form_new_fichamento').modal('hide');
            $('#form-add-fichamento').trigger("reset");

            $.each(retorno.msgConfig.pnotify, function (notificacao, conteudoNotificacao) {
                $.pnotify({
                    title: conteudoNotificacao.titulo,
                    text: conteudoNotificacao.texto,
                    type: conteudoNotificacao.tipo
                });
            });
        }
    });

    $('#form-remove-fichamento').ajaxForm({
        beforeSubmit: function () {
            //validando o arquivo que está sendo enviado
            if ($('#ComprovanteFile').val() != '')
            {
                if (window.File && window.FileReader && window.FileList && window.Blob && $('#form-remove-fichamento').valid())
                {
                    var fsize = $('#ComprovanteFile')[0].files[0].size;
                    var ftype = $('#ComprovanteFile')[0].files[0].type;

                    switch (ftype)
                    {
                        case 'image/png':
                        case 'image/gif':
                        case 'image/jpeg':
                        case 'image/pjpeg':
                        case 'application/pdf':
                            break;
                        default:
                            alert("Tipo de arquivo não permitido!");
                            return false;
                    }
                    if (fsize > 5242880)
                    {
                        alert("Arquivo muito grande!");
                        return false;
                    }
                } else
                {
                    alert("Revise o formulário!");
                }
            }
        },
        //em caso de sucesso, notificar o usuário e ocultar o modal de fichamento
        success: function (response, textStatus, xhr, form) {

            var retorno = $.parseJSON(response);

            $('#modal_form_remove_fichamento').modal('hide');
            $('#form-remove-fichamento').trigger("reset");

            $.each(retorno.msgConfig.pnotify, function (notificacao, conteudoNotificacao) {
                $.pnotify({
                    title: conteudoNotificacao.titulo,
                    text: conteudoNotificacao.texto,
                    type: conteudoNotificacao.tipo
                });
            });
        }
    });

    $(document).on('click', '#volta_seq', function () {
        $('.tableAtend').show('slow');
        $('.tableSeqs').hide('slow');
    });

    $(document).on('click', '#add_seq', function () {
        $('.form-seq').show('slow');
        var now = new Date();
        var hora = now.toTimeString().substring(0, 8);

        $('#horai').val(hora);
        $('.tableSeqs').hide('slow');
    });

    $(document).on('click', '#btn-salvar-seq', function () {
        $.ajax({
            type: 'GET',
            url: '/cobranca/novaSeq/',
            data: {
                "id_chamado": $('#chamado_seq').val(),
                "inicio": $('#horai').val(),
                "fim": $('#horaf').val(),
                "obs": $('#obs_seq').val(),
                "id_contato": $('#selectContatoSeq').val(),
                "dt_agendamento": $('#agendamento_seq').val()
            }
        }).done(function (dRt) {
            var retorno = $.parseJSON(dRt);
            if (!retorno.hasError) {
                $('.tableAtend').show('slow');
                $('.form-seq').hide('slow');
            }

            $.pnotify({
                title: "Informações",
                text: retorno.msg,
                type: retorno.type
            });
        });
    });

    $(document).on('click', '#btn-cancel-seq', function () {
        $('.form-seq').hide('slow');
        $('.tableSeqs').show('slow');
    });

    /*ValidaÃ§Ã£o formulÃ¡rio*/
    $.validator.setDefaults({
        errorElement: "span",
        errorClass: 'help-block',
        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group
        },
        success: function (label, element) {
            label.addClass('help-block valid');
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        }
    });

    $(document).on('click', '#btn-salvar-atend', function () {
        $.ajax({
            type: 'POST',
            url: '/cobranca/novoAtendimento/',
            data: {
                "parc_ids": ids,
                "inicio": $('#inputHorai').val(),
                "fim": $('#temporizador').val(),
                "obs": $('#obs').val(),
                "id_contato": $('#selectContato').val(),
                "dt_agendamento": $('#agendamento').val()
            }
        }).done(function (dRt) {
            var retorno = $.parseJSON(dRt);
            ids = new Array();
            $.pnotify({
                title: "Informações",
                text: retorno.msg,
                type: retorno.type
            });

            if (!retorno.hasError) {
                $('.tableAtend').show('slow');
                $('.form-atendimento').hide('slow');
                $('input:checkbox').show('slow');
                
                gridAtendimentos.draw();
                
                $('#obs'                ).val("");
                $('#selectContatoSeq'   ).select2("val","");
                
//                console.log("teste");
            }
        });
    });

    $(document).on('click', '#novo_contato', function () {
        $('#modal_ncontato').modal('show');
    });

});

function formatSubTable(d, tr, row) {

//        cpfCliente = d.cpf.substring(30, 41);

    var data = '<div class="col-sm-6">' +
            '       <table id="gridParcelas" class="table table-striped table-hover table-full-width dataTable">' +
            '           <thead>' +
            '               <tr>' +
            '                   <th colspan="5" style="text-align:center;">Parcelas em Atraso</th>' +
            '               </tr>' +
            '               <tr>' +
            '                   <th width="3px">' +
            '                       <label class="checkbox-inline" style="float:left">' +
            '                           <input id="check_select_all" type="checkbox">' +
            '                       </label>' +
            '                   </th>' +
            '                   <th>Seq</th>' +
            '                   <th>Valor</th>' +
            '                   <th>Vencimento</th>' +
            '                   <th width="150px">' +
            '                       <button id="atendimento_sel" class="btn btn-dark-grey btn-sm">Iniciar Atendimento</button>' +
            '                   </th>' +
            '               </tr>' +
            '           </thead>' +
            '           <tbody>' +
            '           </tbody>' +
            '       </table>' +
            '   </div>' +
            '   <div class="col-sm-6 tableSeqs" style="display:none">' +
            '       <table id="gridSeqs" class="table table-striped table-hover table-full-width dataTable">' +
            '           <thead>' +
            '               <tr>' +
            '                   <th colspan="4" style="text-align:center;">Sequencias</th>' +
            '               </tr>' +
            '               <tr>' +
            '                   <th>Inicio</th>' +
            '                   <th>Final</th>' +
            '                   <th>Usuario</th>' +
            '                   <th>Obs.</th>' +
            '               </tr>' +
            '           </thead>' +
            '           <tbody>' +
            '           </tbody>' +
            '       </table>' +
            '       <input type="hidden" id="chamado_seq">' +
            '       <button id="add_seq" class="btn btn-dark-grey">Adicionar</button>' +
            '       <button id="volta_seq" class="btn btn-grey">Voltar</button>' +
            '   </div>' +
            '   <div class="col-sm-6 tableAtend">' +
            '       <table id="gridAtendimentos" class="table table-striped table-hover table-full-width dataTable">' +
            '           <thead>' +
            '               <tr>' +
            '                   <th colspan="6" style="text-align:center;">Atendimentos</th>' +
            '               </tr>' +
            '               <tr>' +
            '                   <th>Inicio</th>' +
            '                   <th>Ult. Contato</th>' +
            '                   <th>Prox. Contato</th>' +
            '                   <th>Qtd.</th>' +
            '                   <th width="80px">Chamados</th>' +
            '                   <th width="70px">Adicionar</th>' +
            '               </tr>' +
            '           </thead>' +
            '           <tbody>' +
            '           </tbody>' +
            '       </table>' +
            '   </div>' +
            '   <div class="col-sm-6 form-atendimento" style="display:none;">' +
            '       <form id="form-atend" class="form-horizontal">' +
            '           <div class="form-group">' +
            '               <label for="inputObservação" class="col-sm-2 control-label">Observação</label>' +
            '               <div class="col-sm-10">' +
            '                   <textarea id="obs" placeholder="Digite aqui suas observações" class="form-control" rows="3"></textarea>' +
            '               </div>' +
            '           </div>' +
            '           <div class="form-group">' +
            '               <label for="inputHorai" class="col-sm-2 control-label">Hora Inicio</label>' +
            '               <div class="col-sm-4">' +
            '                   <input disabled="true" type="time" class="form-control" id="inputHorai">' +
            '               </div>' +
            '               <label for="temporizador" class="col-sm-2 control-label">Hora Final</label>' +
            '               <div class="col-sm-4">' +
            '                   <input disabled="true" id="temporizador" type="time" class="form-control">' +
            '               </div>' +
            '           </div>' +
            '           <div class="form-group">' +
            '               <label for="inputTel" class="col-sm-2 control-label">Contato</label>' +
            '               <div class="col-sm-3">' +
            '                   <input style="width:100%;"placeholder="Selecione" type="hidden" id="selectContato" class="search-select select2">' +
            '               </div>' +
            '               <div class="col-sm-1" style="margin-left: -15px; margin-top: 2.5px;">' +
            '                    <button id="novo_contato"class="btn btn-success"><i class="fa fa-plus"></i></button>' +
            '                </div>' +
            '               <label for="agendamento" class="col-sm-2 control-label" style="margin-left: 15px;">Agendamento</label>' +
            '               <div class="col-sm-4">' +
            '                   <input id="agendamento" type="date" class="form-control">' +
            '               </div>' +
            '           </div>' +
            '           <div class="form-group">' +
            '               <div class="col-sm-offset-2 col-sm-10">' +
            '                   <button id="btn-salvar-atend" class="btn btn-dark-grey">Salvar</button>' +
            '                   <button id="btn-negociar" class="btn btn-info">Negociar</button>' +
            '               </div>' +
            '           </div>' +
            '       </form>' +
            '   </div>' +
            '   <div class="col-sm-6 form-chamado" style="display:none;">' +
            '       <form id="form-chamado" class="">' +
            '           <div class="form-group">' +
            '               <label for="agend_cha" class="control-label">Agendamento</label>' +
            '               <input id="agend_cha" type="date" class="form-control" style="width:50%;">' +
            '           </div>' +
            '           <div class="form-group">' +
            '               <button id="salvar-cha" class="btn btn-dark-grey">Salvar Chamado</button>' +
            '               <button id="voltar-cha" class="btn btn-grey">Cancelar</button>' +
            '           </div>' +
            '       </form>' +
            '   </div>' +
            '   <div class="col-sm-6 form-seq" style="display:none;">' +
            '       <form id="form-seq" class="form-horizontal">' +
            '           <div class="form-group">' +
            '               <label for="obs_seq" class="col-sm-2 control-label">Observação</label>' +
            '               <div class="col-sm-10">' +
            '                   <textarea id="obs_seq" placeholder="Digite aqui suas observações" class="form-control" rows="3"></textarea>' +
            '               </div>' +
            '           </div>' +
            '           <div class="form-group">' +
            '               <label for="horai" class="col-sm-2 control-label">Hora Inicio</label>' +
            '               <div class="col-sm-4">' +
            '                   <input disabled="true" type="time" class="form-control" id="horai">' +
            '               </div>' +
            '               <label for="horaf" class="col-sm-2 control-label">Hora Final</label>' +
            '               <div class="col-sm-4">' +
            '                   <input disabled="true" id="horaf" type="time" class="form-control">' +
            '               </div>' +
            '           </div>' +
            '           <div class="form-group">' +
            '               <label for="tel" class="col-sm-2 control-label">Contato</label>' +
            '               <div class="col-sm-3">' +
            '                   <input style="width:100%;"placeholder="Selecione" type="hidden" id="selectContatoSeq" class="search-select select2">' +
            '               </div>' +
            '               <div class="col-sm-1" style="margin-left: -15px; margin-top: 2.5px;">' +
            '                    <button id="novo_contato"class="btn btn-success"><i class="fa fa-plus"></i></button>' +
            '                </div>' +
            '               <label for="agendamento_seq" class="col-sm-2 control-label" style="margin-left: 15px;">Agendamento</label>' +
            '               <div class="col-sm-4">' +
            '                   <input id="agendamento_seq" type="date" class="form-control">' +
            '               </div>' +
            '           </div>' +
            '           <div class="form-group">' +
            '               <div class="col-sm-offset-2 col-sm-10">' +
            '                   <button id="btn-salvar-seq" class="btn btn-dark-grey">Salvar Sequência</button>' +
            '                   <button id="btn-cancel-seq" class="btn btn-grey">Cancelar</button>' +
            '               </div>' +
            '           </div>' +
            '       </form>' +
            '   </div>' +
            '   <input type="hidden" id="cliente_id">';

    row.child(data).show('slow');
    $('#atendimento_sel').hide();
    tr.addClass('shown');
    //passando o id do cliente para o campo correspondente, o campo será usado para listar os contatos do cliente..
    
    gridAtendimentos = $('#gridAtendimentos').DataTable(
    {
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/cobranca/listarAtendimentos',
            type: 'POST',
            data: {
                "cpf_cliente": d.numeroCPF
            }
        },
        "columnDefs": [{
                "orderable": false,
                "targets": "no-orderable"
            }],
        "columns": [
            {"data": "inicio"},
            {"data": "ult_inter"},
            {"data": "prox_inter"},
            {"data": "qtd"},
            {"data": "cha"},
            {"data": "novo_cha"}
        ]
    });
            
    $.ajax({
        type: 'POST',
        url: '/cobranca/getCliente/',
        data: {
            "cpf_cliente": d.cpf.substring(30, 41)
        }
    }).done(function (dRt) {

        var retorno = $.parseJSON(dRt);
//            console.log(retorno.id);
        $('#cliente_ncontato').val(retorno.id);
        $('#cliente_id').val(retorno.id);
        $.ajax({
            type: 'GET',
            url: '/cobranca/contatosClientes',
            data: {
                "id_cliente": retorno.id
            }

        }).done(function (dRt2) {
            var jsonReturn = $.parseJSON(dRt2);
            selectContatos.select2({
                data: jsonReturn
            });

            var jsonReturn = $.parseJSON(dRt2);
            selectContatosSeq.select2({
                data: jsonReturn
            });
        });
    });

    var gridParcelas = $('#gridParcelas').DataTable({
        "processing": true,
        "ajax":
                {
                    url: '/cobranca/listarParcelas',
                    type: 'POST',
                    data: {
                        "cpf_cliente": d.cpf.substring(30, 41)
                    }
                },
        "columnDefs": [{
                "orderable": false,
                "targets": "no-orderable"
            }],
        "columns": [
            {"data": "check"},
            {"data": "seq"},
            {"data": "valor"},
            {"data": "vencimento"},
            {"data": "button"}
        ]
    });

    $('#gridAtendimentos tbody').on('click', '#novo_cha', function () {
        $('.tableAtend').hide('slow');
        $('.form-chamado').show('slow');

        var id_atend = $(this).parent().find('input').val();
        $(document).on('click', '#salvar-cha', function () {
            $.ajax({
                type: 'GET',
                url: '/cobranca/novoChamado/',
                data: {
                    "id_atend": id_atend,
                    "data": $('#agend_cha').val()
                }
            }).done(function (dRt) {
                var retorno = $.parseJSON(dRt);
                if (!retorno.hasErrors) {
//                        gridAtendimentos.destroy();
//                        gridAtendimentos.draw();
                    $('.tableAtend').show('slow');
                    $('.form-chamado').hide('slow');
                }

                $.pnotify({
                    title: "Informações",
                    text: retorno.msg,
                    type: retorno.type
                });
            });
        });
    });

    $('#gridAtendimentos tbody').on('click', '#parc_atend', function () {

        var tr = $(this).closest('tr');
        var row = gridAtendimentos.row(tr);
        var rowsTam = gridAtendimentos.rows()[0].length;
        //.parent (o pai, uma td); .find() procurando o input existente na td..
        var atend = $(this).parent().find('input').val();

        if (row.child.isShown()) {

            row.child.hide('slow');
            tr.removeClass('shown');

        } else
        {

            for (i = 0; i < rowsTam; i++)
            {

                if (gridAtendimentos.row(i).child.isShown()) {
                    gridAtendimentos.row(i).child.hide('slow');
                }
            }

            formatSubTableAtend2(row.data(), tr, row, atend);
        }
    });

    function formatSubTableAtend2(d, tr, row, atend) {
        data = '<table id="gridParcAtend" class="table table-striped table-bordered table-hover table-full-width dataTable">' +
                '    <thead>' +
                '        <tr>' +
                '            <th>Seq.</th>' +
                '            <th>Valor</th>' +
                '            <th>Vencimento</th>' +
                '            <th width="10px"></th>' +
                '            <th width="10px"></th>' +
                '        </tr>' +
                '    </thead>' +
                '    <tbody></tbody>' +
                '</table>';
        row.child(data).show('slow');

        var gridParcs = $('#gridParcAtend').DataTable({
            "processing": true,
            "ajax": {
                url: '/cobranca/listarParcAtend',
                type: 'GET',
                data: {
                    "id_atend": atend
                }
            },
            "columnDefs": [{
                    "orderable": false,
                    "targets": "no-orderable"
                }],
            "columns": [
                {"data": "seq"},
                {"data": "valor"},
                {"data": "venc"},
                {"data": "button"},
                {"data": "retirar"}
            ]
        });

    }

    $('#gridAtendimentos tbody').on('click', '#btn_chamado', function () {

        var tr = $(this).closest('tr');
        var row = gridAtendimentos.row(tr);
        var rowsTam = gridAtendimentos.rows()[0].length;
        //.parent (o pai, uma td); .find() procurando o input existente na td..
        var atend = $(this).parent().find('input').val();

        if (row.child.isShown()) {

            row.child.hide('slow');
            tr.removeClass('shown');

        } else
        {

            for (i = 0; i < rowsTam; i++)
            {

                if (gridAtendimentos.row(i).child.isShown()) {
                    gridAtendimentos.row(i).child.hide('slow');
                }
            }

            formatSubTableAtend1(row.data(), tr, row, atend);
        }
    });

    function formatSubTableAtend1(d, tr, row, atend) {
        $.ajax({
            type: 'GET',
            url: '/cobranca/listarChamados/',
            data: {
                "id_atend": atend
            }
        }).done(function (dRt) {
            var retorno = $.parseJSON(dRt);
            data = '<div style="text-align:center;">';
            $.each(retorno.data, function (i, value) {
                data += '<p> ' + value.ordem + 'º Chamado em ' + value.data + " " + value.button + '</p>';
            });

            data += '</div>';
            row.child(data).show('slow');
        });
    }

    $(document).on('click', '.btn_seqs', function () {
        $('.tableAtend').hide('slow');
        $('.tableSeqs').show('slow');
        $('.tableSeqs').removeAttr('style');
        $("#gridSeqs tr:nth-child(odd)").addClass("danger");
        var id_chamado = $(this).parent().find('input').val();
        $('#chamado_seq').val(id_chamado);
        var gridSeqs = $('#gridSeqs').DataTable({
            "processing": true,
            "ajax": {
                url: '/cobranca/listarSeqs',
                type: 'GET',
                data: {
                    "id_chamado": id_chamado
                }
            },
            "columnDefs": [{
                    "orderable": false,
                    "targets": "no-orderable"
                }],
            "columns": [
                {"data": "inicio"},
                {"data": "fim"},
                {"data": "user"},
                {"data": "obs"}
            ]
        });
        gridSeqs.destroy();
        gridSeqs.draw();
        $('.tableSeqs').removeAttr('style');
    });

    selectContatos = $("#selectContato").select2({
        data: [{id: 0, text: 'Carregando..'}],
        placeholder: "Selecione um contato..."
    });

    selectContatosSeq = $("#selectContatoSeq").select2({
        data: [{id: 0, text: 'Carregando..'}],
        placeholder: "Selecione um contato..."
    });
}

/*function formatSubTable(d, tr, row) {

    tr.addClass('shown');

    // `d` is the original data object for the row
    var data = '<h5><i class="clip-list"></i> Filiais</h5>' +
            '<div class="row">' +
            '   <div  class="col-sm-12">' +
            '       <table id="filiaisAdmin" class="table table-striped table-bordered table-hover table-full-width dataTable">' +
            '           <thead>' +
            '              <tr>' +
            '                   <th class="no-orderable" width="20px" id="thCheckFilial">' +
            '                       <i id="checkFiliais" class="clip-checkbox-partial"></i>' +
            '                   </th>' +
            '                   <th class="no-orderable"            >Filial</th>' +
            '              </tr>' +
            '           </thead>' +
            '           <tbody>' +
            '           </tbody>' +
            '       </table>' +
            '   </div>' +
            '</div>';

    row.child(data).show();
}*/