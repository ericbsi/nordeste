$(document).ready(function () {

   $('#grid_usuarios tfoot th.searchable').each(function ()
   {
      var title = $('#grid_usuarios thead th').eq($(this).index()).text();
      $(this).html('<input style="width:100%" type="text" placeholder="Buscar ' + title + '" />');
   }
   );

   var usuariosTable = $('#grid_usuarios').DataTable({
      "processing": true,
      "serverSide": true,
      "ajax":
              {
                 url: '/usuario/getUsuarios',
                 type: 'POST',
                 "data": function (d)
                 {
                    d.tipoUserLogado = $('#tipo_de_usuario_logado_id').val()
                 }
              },
      "columns": [
         {
            "data"      : "btn_habdesab",
            "className" : "btn_habdesab"
         },
         {
            "data"      : "nome_utilizador",
            "className" : "utilizador"
         },
         {
            "data"      : "username",
            "className" : "username"
         },
         {"data": "tipo"},
         {
            "data": "empresa_filial",
            "className" : "tdFilial"
         },
         {
            "data": "senha",
            "className": "tdSenha",
         },
         {"data": "last_view"},
         {"data": "saldoPontos"},
      ],
   });


    usuariosTable.columns().eq( 0 ).each( function ( colIdx ) {
        $( 'input', usuariosTable.column( colIdx ).footer() ).on( 'change', function () {
            usuariosTable.column( colIdx ).search( this.value ).draw();
        })
    })

    //////////////////////////////////////////////////////////////////////////
    // funcao que irá capturar o click duplo na célula de senha e irá       //
    // habilitar para edição da mesma. caso a mesma sofra alguma alteração, //
    // salve o conteúdo no banco                                            //
    //////////////////////////////////////////////////////////////////////////
    // Autor : André Willams // Data : 12-05-2015 ////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    $(document).on('dblclick', '.tdSenha', function () {

        //elemento atual - elemento pai - primeira celula (id) - conteudo
        var nomeUsuario = $(this).context.parentElement.cells[2].textContent; //pegue o id da referência

        console.log(nomeUsuario);

        var conteudoOriginal = $(this).text(); //resgate o conteúdo atual da célula

        var elemento = $(this);

        //transforme o elemento em um input
        $(this).html("<input class='form-control' type='password' value='' />");

        $(this).children().first().focus(); //atribua o foco ao elemento criado

        $(this).children().first().keypress(function (e) { //quando alguma tecla for pressionada

            if (e.which == 13) { //caso a tecla pressionada seja o ENTER, foi confirmada a alteração

                var novoConteudo = $(this).val(); //pegue o novo conteúdo

                //chame o ajax de alteração
                $.ajax({
                    type: "POST",
                    url: "/usuario/mudarSenha/",
                    data: {
                        "nomeUsuario": nomeUsuario,
                        "senha": novoConteudo,
                    },
                }).done(function (dRt)
                {

                    var retorno = $.parseJSON(dRt);

                    console.log(retorno);

                    $.pnotify(
                            {
                                text: retorno.msg,
                                type: retorno.pntfyClass
                            }
                    );

                });

                $(this).parent().text(conteudoOriginal);

            }
        });

        //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
        $(this).children().first().blur(function () {

            //devolva o conteúdo original
            $(this).parent().text(conteudoOriginal);

        });

    });
    
    //////////////////////////////////////////////////////////////////////////
    // funcao que irá capturar o click duplo na célula de senha e irá       //
    // habilitar para edição da mesma. caso a mesma sofra alguma alteração, //
    // salve o conteúdo no banco                                            //
    //////////////////////////////////////////////////////////////////////////
    // Autor : André Willams // Data : 12-05-2015 ////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    $(document).on('dblclick', '.utilizador', function () {

        //elemento atual - elemento pai - primeira celula (id) - conteudo      
//        var nomeUsuario = $(this).context.parentElement.cells[1].textContent; //pegue o id da referência
        
        var tr         = $(this).closest('tr');
        var row        = usuariosTable.row(tr);
        var rowData    = row.data();
        var idUsuario  = rowData.usuarioId;

        var conteudoOriginal = $(this).text(); //resgate o conteúdo atual da célula

        var elemento = $(this);

        //transforme o elemento em um input
        $(this).html("<input class='form-control' type='text' value='" + conteudoOriginal + "' />");

        $(this).children().first().focus(); //atribua o foco ao elemento criado

        $(this).children().first().keypress(function (e) { //quando alguma tecla for pressionada

            if (e.which == 13) { //caso a tecla pressionada seja o ENTER, foi confirmada a alteração

                var novoConteudo = $(this).val(); //pegue o novo conteúdo

                //chame o ajax de alteração
                $.ajax({
                    type    : "POST"                        ,
                    url     : "/usuario/mudarUtilizador/"   ,
                    data:   {
                                "idUsuario"     : idUsuario     ,
                                "utilizador"    : novoConteudo  ,
                            },
                }).done(function (dRt)
                {

                    var retorno = $.parseJSON(dRt);

                    $.pnotify(
                            {
                                text: retorno.msg,
                                type: retorno.pntfyClass
                            }
                    );
                
                    if(retorno.hasErrors)
                    {
                        elemento.text(conteudoOriginal);
                    }
                    else
                    {
                        elemento.text(novoConteudo);
                    }

                });

            }
        });

        //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
        $(this).children().first().blur(function () {

            //devolva o conteúdo original
            $(this).parent().text(conteudoOriginal);

        });

    });
    
    //////////////////////////////////////////////////////////////////////////
    // funcao que irá capturar o click duplo na célula de senha e irá       //
    // habilitar para edição da mesma. caso a mesma sofra alguma alteração, //
    // salve o conteúdo no banco                                            //
    //////////////////////////////////////////////////////////////////////////
    // Autor : André Willams // Data : 12-05-2015 ////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    $(document).on('dblclick', '.username', function () {

        //elemento atual - elemento pai - primeira celula (id) - conteudo      
        //var nomeUsuario = $(this).context.parentElement.cells[1].textContent; //pegue o id da referência

        var conteudoOriginal = $(this).text(); //resgate o conteúdo atual da célula

        var elemento = $(this);
        
        var tr         = $(this).closest('tr');
        var row        = usuariosTable.row(tr);
        var rowData    = row.data();
        var idUsuario  = rowData.usuarioId;

        //transforme o elemento em um input
        $(this).html("<input class='form-control' type='text' value='" + conteudoOriginal + "' />");

        $(this).children().first().focus(); //atribua o foco ao elemento criado

        $(this).children().first().keypress(function (e) { //quando alguma tecla for pressionada

            if (e.which == 13) { //caso a tecla pressionada seja o ENTER, foi confirmada a alteração

                var novoConteudo = $(this).val(); //pegue o novo conteúdo

                //chame o ajax de alteração
                $.ajax({
                    type    : "POST"                    ,
                    url     : "/usuario/mudarUsername/" ,
                    data:   {
                                "username"  : novoConteudo  ,
                                "idUsuario" : idUsuario     ,
                            },
                }).done(function (dRt)
                {

                    var retorno = $.parseJSON(dRt);

                    $.pnotify(
                            {
                                text: retorno.msg,
                                type: retorno.pntfyClass
                            }
                    );
                
                    if(retorno.hasErrors)
                    {
                        elemento.text(conteudoOriginal);
                    }
                    else
                    {
                        elemento.text(novoConteudo);
                    }

                });

            }
        });

        //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
        $(this).children().first().blur(function () {

            //devolva o conteúdo original
            $(this).parent().text(conteudoOriginal);

        });

    });

   //////////////////////////////////////////////////////////////////////////
   // funcao que irá capturar o click duplo na célula da filial e irá      //
   // habilitar para edição da mesma. caso a mesma sofra alguma alteração, //
   // salve o conteúdo no banco                                            //
   //////////////////////////////////////////////////////////////////////////
   // Autor : André Willams // Data : 04-08-2015 ////////////////////////////
   //////////////////////////////////////////////////////////////////////////
   $(document).on('dblclick', '.tdFilial', function () {

      var conteudoOriginal = $(this).text(); //resgate o conteúdo atual da célula
      
      var tr         = $(this).closest('tr');
      var row        = usuariosTable.row(tr);
      var rowData    = row.data();
      var idUsuario  = rowData.usuarioId;

      var elemento = $(this);

      $.ajax
              (
                      {
                         type: "POST",
                         url: "/usuario/listarFiliaisSelect",
                         data: {
                            "idUsuario"   : idUsuario ,
                         },
                      }
              ).done(function (dRt)
      {

         var retorno = $.parseJSON(dRt);

         elemento.html(retorno.html);

         elemento.children().first().focus(); //atribua o foco ao elemento criado

         elemento.on
                 ('change', function ()
                 {

                    index     = elemento.children()[0].selectedIndex;
                    idFilial  = elemento.children().val();

                    $.ajax
                            (
                                    {
                                       type: "POST",
                                       url: "/usuario/mudarFilialSelect",
                                       data: {
                                          "idFilial"  : idFilial  ,
                                          "idUsuario" : idUsuario
                                       }
                                    }
                            ).done(function (dRt)
                    {

                       var retorno = $.parseJSON(dRt);

                       $.pnotify({
                          title: 'Notificação',
                          text: retorno.msg,
                          type: retorno.pntfyClass
                       });

                       if (!retorno.hasErrors)
                       {
                          elemento.text(elemento.children()[0][index].text);
                       }

                    }
                    );


                 }
                 );

         //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
         elemento.children().first().blur(function () {

            //devolva o conteúdo original
            elemento.text(conteudoOriginal);

         });

      });

   });


  //////////////////////////////////////////////////////////////////////////
  // funcao que irá capturar o click no botão de habilitar e desabilitar  //
  //usuário                                                               //
  //////////////////////////////////////////////////////////////////////////
  // Autor : Eric Vieira // Data : 11-04-2016 ////////////////////////////
  //////////////////////////////////////////////////////////////////////////
  $(document).on('click', '.btn-hab', function(){

      var post = $.ajax({
        type: "POST",
        url: "/usuario/habilitarDesabilitar/",
        data: {
          "username"      : $(this).data('username'),
          "habilitado"    : $(this).data('hab'),
        },
      });

      post.done(function(retorno){
        usuariosTable.draw();
      });
    });
});