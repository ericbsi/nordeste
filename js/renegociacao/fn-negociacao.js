$(function () {

    var propostasTable = $('#grid_contratos').DataTable({
                "processing": true,
                "serverSide": true,
                "paging": true,
                "bLengthChange": false,
                "ajax":
                        {
                            url: '/renegociacao/contratosCPF',
                            type: 'POST',
                            "data" : function(d){
                               d.cpf_cliente = $('#cpf_cliente').val();
                            }
                        },
                "columns": [
                    {
                        "orderable": false,
                        "data": "codigo"
                    },
                    {
                        "orderable": false,
                        "data": "cliente"
                    },
                    {
                        "orderable": false,
                        "data": "parcelas"
                    },
                    {
                        "orderable": false,
                        "data": "negociar"
                    }
                ]
            });
            
     $('#btn_pesquisar').on('click', function(){
         propostasTable.ajax.reload();
     });
});