$(function()
{
    
    var marcadosGerarRange      = [];
    var marcadosImprimirBoletos = [];
    
    var idProposta;
    
    var idEstorno = 0;

    var propostasTable = $('#gridPropostas').DataTable(
    {
        "processing"    : true  ,
        "serverSide"    : true  ,
        "paging"        : true  ,
        "bLengthChange" : false ,
        "oLanguage": 
        {
            "sSearch"   : "CPF: "
        },
        "ajax":
                {
                    url: '/renegociacao/listarPropostasAjuste',
                    type: 'POST'
                },
        "columns": [
            {
                "orderable" : false             ,
                "data"      : "codigo"
            },
            {
                "orderable" : false             ,
                "data"      : "dataProposta"
            },
            {
                "orderable" : false             ,
                "data"      : "nomeFilial"
            },
            {
                "orderable" : false             ,
                "data"      : "cliente"
            },
            {
                "orderable" : false             ,
                "data"      : "cpf"
            },
            {
                "orderable" : false             ,
                "data"      : "valor"
            },
            {
                "orderable" : false             ,
                "data"      : "entrada"
            },
            {
                "orderable" : false             ,
                "data"      : "qtdParcelas"
            },
            {
                "orderable" : false             ,
                "data"      : "vlrParcela"
            }
        ]
    });

    var parcelasTable = $('#gridParcelas').DataTable(
    {
        "processing"    : true  ,
        "serverSide"    : true  ,
        "paging"        : false ,
        "bLengthChange" : false ,
        "bInfo"         : false ,
        "bFilter"       : false ,
        "oLanguage": 
        {
            "sSearch"   : "CPF: "
        },
        "ajax":
        {
            url     : '/renegociacao/listarParcelasAjuste'  ,
            type    : 'POST'                                ,
            data    : function(d)
            {
                d.idProposta = idProposta;
            }
        },
        "columns": 
        [
            {
                "orderable" : false             ,
                "data"      : "checkParcela"
            },
            /*{
                "orderable" : false             ,
                "data"      : "seq"
            },*/
            {
                "orderable" : false             ,
                "data"      : "valor"
            },
            {
                "orderable" : false             ,
                "data"      : "vencimento"      ,
                "className" : "tdVencimento"      
            },
            {
                "orderable" : false             ,
                "data"      : "btnImprimirBoleto"
            },
            {
                "orderable" : false             ,
                "data"      : "btnGerarRange"
            },
            {
                "orderable" : false             ,
                "data"      : "btnBaixarParcela"
            },
            {
                "orderable" : false             ,
                "data"      : "btnEstornarBaixa"
            },
        ]
    });
    
    var mensagensTable = $('#gridMensagens').DataTable(
    {
        "processing"    : true  ,
        "serverSide"    : true  ,
        "paging"        : false ,
        "bFilter"       : false ,
        "bInfo"         : false ,
        "ajax"          :
        {
            url     :   '/estornoBaixa/gridMensagens' ,
            type    :   'POST'                        ,
            data    :   function(d)
                        {
                            d.idEstorno = idEstorno;
                        }
        },
        "columns":
        [
            {
                "data"  : "dataStatus"
            },
            {
                "data"  : "descricaoStatus"
            },
            {
                "data"  : "usuario"
            },
            {
                "data"  : "mensagem"
            }
        ]
    });
    
    parcelasTable.on("draw",function()
    {
        
        $("#divTitulos").resize();
        
    });
    
    $('#gridPropostas tbody').on('dblclick', 'tr', function () 
    {
        
        idProposta = null;
        
        if($(this).hasClass("selecionada"))
        {
            
            $(this).removeClass("selecionada");
            
            $(this).css("background-color","inherit");
            
            $("#divPropostas"   ).removeClass   ("col-sm-8"     );
            $("#divPropostas"   ).addClass      ("col-sm-12"    );
            
            $("#divTitulos"     ).hide          ("slow"         );

            $("#divBaixa"       ).hide          ("slow"         );
            
        }
        else
        {
            
            $('#gridPropostas tr').each(function(index, elemento)
            {

                $(elemento).css("background-color","inherit");
                
                $(elemento).removeClass("selecionada");

            });
            
//            console.log(propostasTable.row($(this)));
            
            idProposta = propostasTable.row($(this)).data().idProposta;
            
//            console.log(idProposta);
            
            $(this).addClass("selecionada");
            
            $(this).css("background-color","gray");
            
            $("#divPropostas"   ).removeClass   ("col-sm-12"    );
            $("#divPropostas"   ).addClass      ("col-sm-8"     );
            
            $("#divTitulos"     ).removeClass   ("col-sm-3"     );
            $("#divTitulos"     ).addClass      ("col-sm-4"     );
            
            $("#divTitulos"     ).show          ("slow"         );

            $("#divBaixa"       ).hide          ("slow"         );
            
        }
        
        parcelasTable.draw();
        
        $('#gridPropostas').css("width","100%");
        
    });

    $(document).on('click','.checkParcela', function()
    {
        
        var elemento = $(this);
        
        if($(this).hasClass("clip-square"))
        {
            $(this).removeClass ("clip-square"                  );
            $(this).addClass    ("clip-checkbox-unchecked-2"    );
            
            if($(this).attr("id") == "iSelecionarTudo")
            {
                
                

            }
            else
            {
                
                if($(this).hasClass("gerarRange"))
                {
                    
                    marcadosGerarRange =  $.grep(marcadosGerarRange, function(value) 
                                {
                                    return value !== elemento.attr("value");
                                });
                    
                }
                else
                {
                    
                    marcadosImprimirBoletos =  $.grep(marcadosImprimirBoletos, function(value) 
                                {
                                    return value !== elemento.attr("value");
                                });
                }
        
//                habilitaGerar();
                
            }
            
        }
        else
        {
            
            if($(this).attr("id") == "iSelecionarTudo")
            {
                
                $("#btnSelecionaTudo").attr("value","0");
                
//                tablePropostas.draw();

            }
            else
            {
                
                if($(this).hasClass("gerarRange"))
                {
                
                    if($.inArray($(this).val(),marcadosGerarRange) < 0)
                    {
                        marcadosGerarRange.push(elemento.attr("value"));
                    }
                    
                }
                else
                {
                
                    if($.inArray($(this).val(),marcadosImprimirBoletos) < 0)
                    {
                        marcadosImprimirBoletos.push(elemento.attr("value"));
                    }
                }
        
//                habilitaGerar();
                
            }
            
            $(this).addClass    ("clip-square"                  );
            $(this).removeClass ("clip-checkbox-unchecked-2"    );
        }
        
    });
    
    $(document).on("click",".gerarRangeBtn",function()
    {
        
        var idsParcelas = [] ;
        
        if($(this).attr("id") == "btnGerarRanges")
        {
            idsParcelas = marcadosGerarRange;
        }
        else
        {
            idsParcelas = [$(this).val()];
        }
        
        $.ajax  
        (
            {

                type    : "POST"                                ,
                url     : "/renegociacao/gerarRangesParcelas"   ,
                data    : 
                {
                    "idsParcelas" : idsParcelas
                }
            }
        ).done(function (dRt)
            {
                
                var retorno = $.parseJSON(dRt);
                
                $.pnotify
                (
                    {
                        title   : retorno.title ,
                        text    : retorno.msg   ,
                        type    : retorno.type
                    }
                );
                
                if(!retorno.hasErrors)
                {
                    
                    marcadosGerarRange = [];
                    
                    parcelasTable.draw();
                    
                }
                
            }
                    
        );

        
    });
    
    $(document).on("click",".baixarParcelaBtn",function()
    {
        
        var idParcela   = $(this).val() ;
        
        /*var idsParcelas = [] ;
        
        if($(this).attr("id") == "btnGerarRanges")
        {
            idsParcelas = marcadosGerarRange;
        }
        else
        {
            idsParcelas = [$(this).val()];
        }*/
        
        if($(this).hasClass("active"))
        {
            
            $(this              ).removeClass   ("active"               );
            $(this              ).removeClass   ("btn-red"              );
            $(this              ).addClass      ("btn-orange"           );

/*          $("#divPropostas"   ).removeClass   ("col-sm-6"             );
            $("#divPropostas"   ).addClass      ("col-sm-8"             );*/

            $("#divPropostas"   ).show          ("slow"                 );

//          $("#divTitulos"     ).removeClass   ("col-sm-3"             );
//          $("#divTitulos"     ).addClass      ("col-sm-4"             );
            $("#divTitulos"     ).removeClass   ("col-sm-9"             );
            $("#divTitulos"     ).addClass      ("col-sm-4"             );

            $("#divBaixa"       ).hide          ("slow"                 );

            $('#gridPropostas'  ).css           ("width"        ,"100%" );
            
        }
        else
        {
            
            $('.baixarParcelaBtn').each(function(index, elemento)
            {
                $(elemento).removeClass ("active"       );
                $(elemento).removeClass ("btn-red"      );
                $(elemento).addClass    ("btn-orange"   );
            });
            
            $(this              ).addClass      ("active"               );
            $(this              ).removeClass   ("btn-orange"           );
            $(this              ).addClass      ("btn-red"              );

/*          $("#divPropostas"   ).removeClass   ("col-sm-8"             );
            $("#divPropostas"   ).addClass      ("col-sm-6"             );*/

            $("#divPropostas"   ).hide          ("slow"                 );

//          $("#divTitulos"     ).removeClass   ("col-sm-4"             );
//          $("#divTitulos"     ).addClass      ("col-sm-3"             );

            $("#divTitulos"     ).removeClass   ("col-sm-4"             );
            $("#divTitulos"     ).addClass      ("col-sm-9"             );

            $("#divBaixa"       ).show          ("slow"                 );

            $('#gridPropostas'  ).css           ("width"        ,"100%" );
            
            $.ajax
            (
                {
                    type    : "POST"                            ,
                    url     : "/renegociacao/getValorParcela"   ,
                    data    : 
                    {
                        "idParcela" : idParcela
                    }
                }
            ).done(function (dRt)
                {
                    var retorno = $.parseJSON(dRt);
                    
                    if(retorno.hasErrors)
                    {
                        
                        $.pnotify
                        (
                            {
                                title   : retorno.title ,
                                text    : retorno.msg   ,
                                type    : retorno.type
                            }
                        );
                
                    }
                    else
                    {
                        
//                        console.log(retorno);
                        
                        $("#valorPago"      ).val(retorno.valorParcela  );
                        $("#idParcelaHdn"   ).val(idParcela             );
                        
                    }
            
                }
            );
            
        }

        
    });
    
    $(document).on("click",".estornarBaixaBtn",function()
    {
        
        var idParcela   = $(this).val() ;
        
        if($(this).hasClass("active"))
        {
            
            $(this              ).removeClass   ("active"                       );
            $(this              ).removeClass   ("btn-light-grey"               );
            $(this              ).addClass      ("btn-dark-grey"                );

/*          $("#divPropostas"   ).removeClass   ("col-sm-6"                     );
            $("#divPropostas"   ).addClass      ("col-sm-8"                     );*/

            $("#divPropostas"   ).show          ("slow"                         );

//          $("#divTitulos"     ).removeClass   ("col-sm-3"                     );
//          $("#divTitulos"     ).addClass      ("col-sm-4"                     );
            $("#divTitulos"     ).removeClass   ("col-sm-9"                     );
            $("#divTitulos"     ).addClass      ("col-sm-4"                     );

            $("#divEstorno"     ).hide          ("slow"                         );

            $('#gridPropostas'  ).css           ("width"            , "100%"    );
            
        }
        else
        {
            
            $('.estornarBaixaBtn').each(function(index, elemento)
            {
                $(elemento).removeClass ("active"                               );
                $(elemento).removeClass ("btn-light-grey"                       );
                $(elemento).addClass    ("btn-dark-grey"                        );
            });
            
            $(this                  ).addClass      ("active"                       );
            $(this                  ).removeClass   ("btn-dark-grey"                );
            $(this                  ).addClass      ("btn-light-grey"               );

/*          $("#divPropostas"       ).removeClass   ("col-sm-8"                     );
            $("#divPropostas"       ).addClass      ("col-sm-6"                     );*/

            $("#divPropostas"       ).hide          ("slow"                         );

//          $("#divTitulos"         ).removeClass   ("col-sm-4"                     );
//          $("#divTitulos"         ).addClass      ("col-sm-3"                     );

            $("#divTitulos"         ).removeClass   ("col-sm-4"                     );
            $("#divTitulos"         ).addClass      ("col-sm-9"                     );
            
            $("#observacao"         ).val           (""                             );

            $("#divEstorno"         ).show          ("slow"                         );

            $('#gridPropostas'      ).css           ("width"            , "100%"    );
            
            $("#btnConfirmaEstorno" ).val           (idParcela                      );
            
        }

        
    });
    
    /*$("#btnFinalizarBaixa").on("click",function()
    {
        
        $.ajax  
        (
            {

                type    : "POST"                                ,
                url     : "/renegociacao/gerarRangesParcelas"   ,
                data    : 
                {
                    "idParcela" : idParcela
                }
            }
        ).done(function (dRt)
            {
                
                var retorno = $.parseJSON(dRt);
                
                $.pnotify
                (
                    {
                        title   : retorno.title ,
                        text    : retorno.msg   ,
                        type    : retorno.type
                    }
                );
                
                if(!retorno.hasErrors)
                {
                    parcelasTable.draw();   
                }
                
            }
                    
        );
        
    });*/

   //////////////////////////////////////////////////////////////////////////
    // funcao que irá capturar o click duplo na célula de vencimento e irá  //
    // habilitar para edição da mesma. caso a mesma sofra alguma alteração, //
    // salve o conteúdo no banco                                            //
    //////////////////////////////////////////////////////////////////////////
    // Autor : André Willams // Data : 14-12-2016 ////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    $(document).on('dblclick', '.tdVencimento', function ()
    {

        var conteudoOriginal    = $.trim($(this).text()); //resgate o conteúdo atual da célula
        
        var tr         = $(this).closest('tr');
        var row        = parcelasTable.row(tr);
        var rowData    = row.data();
        
        dataFormatada = conteudoOriginal.substr(6,4) + "-" + conteudoOriginal.substr(3,2) + "-" + conteudoOriginal.substr(0,2);

//        var idNucleo            = $(this).context.parentElement.cells[1].textContent; //pegue o id do Nucleo

        var elemento            = $(this);

        if(rowData.alteraVencimento == "1")
        {

            //transforme o elemento em um input
            $(this).html("<input id='mudaVencimento' class='form-control' type='date' value='" + dataFormatada + "' />");

            $(this).children().first().focus(); //atribua o foco ao elemento criado

            $(this).children().first().keypress(function (e)
            { //quando alguma tecla for pressionada

                if (e.which == 13) 
                { //caso a tecla pressionada seja o ENTER, foi confirmada a alteração

                    var novoConteudo = $.trim($(this).val()); //pegue o novo conteúdo

                    //chame o ajax de alteração
                    $.ajax(
                    {
                        type    : "POST"                            ,
                        url     : "/renegociacao/mudarVencimento"   ,
                        data    : 
                        {
                            "idParcela"     : rowData.idParcela ,
                            "vencimento"    : novoConteudo
                        },
                    }).done(function (dRt) 
                    {

                        var retorno = $.parseJSON(dRt);

                        $.pnotify(
                        {
                            title   : retorno.title ,
                            text    : retorno.msg   ,
                            type    : retorno.type
                        });

                        if (!retorno.hasErrors)
                        {

                            dataFormatada = novoConteudo.substr(8,2) + "/" + novoConteudo.substr(5,2) + "/" + novoConteudo.substr(0,4);

                            elemento.text(dataFormatada);
                        }

                    })

                }
            });

            //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
            $(this).children().first().blur(function () 
            {

                //devolva o conteúdo original
                $(this).parent().text(conteudoOriginal);

            });
            
        }

    });

    $("[type=file]").on("change", function () 
    {
        var file = ""; //this.files[0].name;
        var dflt = $(this).attr("placeholder");
        
        if(typeof this.files[0].name !== 'undefined')
        {
            file = this.files[0].name;
        }
        
        if ($(this).val() != "") 
        {
            $(this).next().text(file);
        } 
        else 
        {
            $(this).next().text(dflt);
        }
    });
    
    $('#form_importacao').ajaxForm(
    {
        
        beforeSubmit: function () 
        {
            $.bloquearInterface('<h4>Aguarde... A baixa está sendo efetuada!</h4>');
        },
        
        //em caso de sucesso
        success: function (response) 
        {

            var data = $.parseJSON(response);
                        
            $.pnotify
            (
                {
                    title   : "Sucesso"     ,
                    text    : data.msg      ,
                    type    : data.tipo
                }
            );

            $("#anexo_importacao"   ).empty (                       );
            $("#anexo_label"        ).text  ('Selecionar Arquivo'   );

            $.desbloquearInterface();

/*          $("#divPropostas"   ).removeClass   ("col-sm-6" );
            $("#divPropostas"   ).addClass      ("col-sm-8" );*/
            
            $("#divBaixa"       ).hide          ("slow"     );

//          $("#divTitulos"     ).removeClass   ("col-sm-3" );
            $("#divTitulos"     ).removeClass   ("col-sm-9" );
            $("#divTitulos"     ).addClass      ("col-sm-4" );

            $("#divBaixa"       ).hide          ("slow"     );
            
            propostasTable.draw();
            parcelasTable.draw();
            
        },
        error : function(response)
        {
            
            var data = $.parseJSON(response);
                        
            $.pnotify
            (
                {
                    title   : "Erro"    ,
                    text    : data.msg  ,
                    type    : data.tipo
                }
            );
    
            $.desbloquearInterface();
            
        }
    });
    
    $("#btnConfirmaEstorno").on("click",function()
    {
        
        var idParcela   = $(this            ).val();
        var observacao  = $("#observacao"   ).val();
        
        $.ajax
        (
            {
                type    : "POST"                                ,
                url     : "/renegociacao/gerarEstornoParcela"   ,
                data    : 
                {
                    "idParcela"     : idParcela ,
                    "observacao"    : observacao
                }
            }
        ).done(function (dRt)
            {
                var retorno = $.parseJSON(dRt);

                $.pnotify
                (
                    {
                        title   : retorno.title ,
                        text    : retorno.msg   ,
                        type    : retorno.type
                    }
                );

                if(!retorno.hasErrors)
                {
        
                    $("#divEstorno"     ).hide          ("slow"     );

                    $("#divTitulos"     ).removeClass   ("col-sm-9" );
                    $("#divTitulos"     ).addClass      ("col-sm-4" );

                    $("#divBaixa"       ).hide          ("slow"     );
                    $("#divPropostas"   ).show          ("slow"     );
                    
                    parcelasTable.draw();

                }

            }
        );
        
    });
    
    $(document).on("click",".btnVoltar", function()
    {
        
        parcelasTable.draw();
        
        $("#" + $(this).attr("data-div-responsavel")).hide("slow");

        $("#divTitulos"     ).removeClass   ("col-sm-9" );
        $("#divTitulos"     ).addClass      ("col-sm-4" );

        $("#divBaixa"       ).hide          ("slow"     );
        $("#divPropostas"   ).show          ("slow"     );
        
    });
    
    $("#observacao").on("blur", function()
    {
        
        if($(this).val().trim() !== "")
        {

            $("#btnConfirmaEstorno").removeAttr ("disabled"             );

        }
        else
        {
           
            $("#btnConfirmaEstorno").attr       ("disabled", "disabled" );
            
        }
           
    });
    
    $(document).on('click', '.btnMensagens', function()
    {
           
        $(this              ).addClass  ("active"   );

        $("#divPropostas"   ).hide      ("slow"     );
        $("#divTitulos"     ).hide      ("slow"     );
        
        $("#divMensagens"   ).show      ("slow"     );
        
        idEstorno = $(this).val();
        
        mensagensTable.draw();
        
    });
    
    $("#btnVoltarDivMsg").on("click", function()
    {

        $("#divTitulos"     ).show("slow");
        $("#divPropostas"   ).show("slow");
        
        $("#divMensagens"   ).hide("slow");
        
        idEstorno = 0;
        
        parcelasTable.draw();
        
    });
    
});