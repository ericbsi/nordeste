$(document).ready(function(){

    $('#filiais_select').multiselect({

      buttonWidth: '225px',
      numberDisplayed:2,

      buttonText:function(options, select){

         if (options.length == 0) {
            return 'Selecionar Filiais <b class="caret"></b>'
         }
         else if (options.length == 1) {
            return '1 Filial Selecionada <b class="caret"></b>'
         }
         else{
            return options.length + ' Filiais Selecionadas <b class="caret"></b>'
         }
      },

    });

    $('#financeiras_select').multiselect({

      buttonWidth: '225px',
      numberDisplayed:2,

      buttonText:function(options, select){

         if (options.length == 0) {
            return 'Selecionar Filiais <b class="caret"></b>'
         }
         else if (options.length == 1) {
            return '1 Filial Selecionada <b class="caret"></b>'
         }
         else{
            return options.length + ' Financeiras Selecionadas <b class="caret"></b>'
         }
      },

    });


    $('#modalidades_select').multiselect({

      buttonWidth: '225px',
      numberDisplayed:2,

      buttonText:function(options, select){

         if (options.length == 0) {
            return 'Selecionar modalidade <b class="caret"></b>'
         }
         else if (options.length == 1) {
            return '1 modalidade selecionada <b class="caret"></b>'
         }
         else{
            return options.length + ' Financeiras Selecionadas <b class="caret"></b>'
         }
      },

    });

    $('#btn_modal_form_new_tabela').on('click',function(){
        $("#cadastro_msg_return").hide().empty();
    })

    var gridTabelas = $('#grid_tabelas').DataTable({
        
        "processing"    : true,
        "serverSide"    : true,        
        "ajax"          : {
            url         : '/tabelaCotacao/getTabelas',
            type        : 'POST',
        },
        "columns":[
            { 
                "data"      : "descricao" ,
                "className" : "tdEditable",
            },
            { "data": "modalidade"},
            { 
                "data": "parcela_inicio",
                "className" : "tdEditable"
            },
            {
                "data": "parcela_fim",
                "className" : "tdEditable"
            },
            { 
                "data": "taxa",
                "className" : "tdEditable"
            },
            { 
                "data": "taxa_anual",
                "className" : "tdEditable"
            },
            { 
                "data": "btn"
            }
        ],
        "columnDefs" : [
            {
                "orderable" : false,
                "targets"   : "no-orderable"
            }            
        ],
    });
    
    /*Validação formulário*/
    $.validator.setDefaults({
        errorElement: "span", 
        errorClass: 'help-block',
        
        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group
        },
            
        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group
        },

        success: function (label, element) {
            label.addClass('help-block valid');
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        }
    })

    $("#form-add-tabela").validate({

        ignore: ':hidden:not(".multipleselect")',

        rules       : {
            'TabelaCotacao[parcela_inicio]' :{
                number : true
            },
            'TabelaCotacao[parcela_fim]' :{
                number : true
            },
            'TabelaCotacao[taxa]' :{
                number : true
            },
            'TabelaCotacao[taxa_atual]' :{
                number : true
            }
        },
        
        messages    : {
            'TabelaCotacao[parcela_inicio]' :{
                number : 'Apenas números'
            },
            'TabelaCotacao[parcela_fim]' :{
                number : 'Apenas números'
            },
            'Filiais[]'  : {
                required : "É preciso selecionar, no mínimo, uma filial."
            },
            'Financeiras[]'  : {
                required : "É preciso selecionar, no mínimo, uma financeira."
            }
        },

        submitHandler   :   function(){

            $("#cadastro_msg_return").hide().empty();

            var formData = $('#form-add-tabela').serialize();

            $.ajax({
                
                type        : "POST",
                url         : "/tabelaCotacao/add/",
                data        : formData,

                beforeSend  : function(){

                    $('.panel').block({
                        overlayCSS: {
                            backgroundColor: '#fff'
                        },
                        message: '<img src="https://beta.sigacbr.com.br/js/loading.gif" /> Salvando tabela...',
                        css: {
                            border: 'none',
                            color: '#333',
                            background: 'none'
                        }
                    });
                    window.setTimeout(function () {
                        $('.panel').unblock();
                    }, 1000);
                }

            })

            .done(function(dRt){

                var retorno = $.parseJSON( dRt );

                if( !$('#checkbox_continuar').is(':checked') )
                {
                    $('#modal_form_new_tabela').modal('hide');
                    $('#form-add-tabela').trigger("reset");

                    $.pnotify({
                        title    : 'Notificação',
                        text     : retorno.msg,
                        type     : retorno.pntfyClass
                    });
                }

                else
                {
                    $("#cadastro_msg_return").prepend('<p>'+retorno.msg+'</p>').fadeIn(100).addClass('alert-success').fadeOut(9000);
                }
                
                $('.has-success').each(function(index){
                    $(this).removeClass('has-success');
                })

                $('.ok').each(function(index){
                    $(this).removeClass('ok');
                })
                
                $('#form-add-tabela').trigger("reset");
                gridTabelas.draw();
            })

            return false;
        }
    });

    //////////////////////////////////////////////////////////////////////////
    // funcao que irá capturar o click duplo na célula de senha e irá       //
    // habilitar para edição da mesma. caso a mesma sofra alguma alteração, //
    // salve o conteúdo no banco                                            //
    //////////////////////////////////////////////////////////////////////////
    // Autor : André Willams // Data : 06-07-2015 ////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    $(document).on('dblclick', '.tdDescricao', function () {

        var conteudoOriginal = $(this).text(); //resgate o conteúdo atual da célula
        
        var tr               = $(this).closest('tr') ;
        var row              = gridTabelas.row(tr)  ;
        var rowData          = row.data()            ;
        var idTabela         = rowData.idTabela      ;
        var elemento = $(this);

        //transforme o elemento em um input
        $(this).html("<input id='mudaDescricao' class='form-control' type='text' value='" + conteudoOriginal + "' />");

        $(this).children().first().focus(); //atribua o foco ao elemento criado

        $(this).children().first().keypress(function (e) { //quando alguma tecla for pressionada


            if ( e.which == 13 ) { //caso a tecla pressionada seja o ENTER, foi confirmada a alteração
                var novoConteudo = $(this).val(); //pegue o novo conteúdo  

                //chame o ajax de alteração
                $.ajax({
                    type: "POST",
                    url: "/tabelaCotacao/mudarDescricao",
                    data: {
                        "idTabela"   : idTabela       ,
                        "descricao" : novoConteudo  
                    },
                }).done(function (dRt) {

                    var retorno = $.parseJSON(dRt);

                    $.pnotify({
                        title: 'Notificação',
                        text: retorno.msg,
                        type: retorno.pntfyClass
                    });
                    
                    console.log(retorno);
                    
                    if(!retorno.hasErrors)
                    {
                        elemento.text(novoConteudo);
                    }

                })

            }
        });

        //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
        $(this).children().first().blur(function () {

            //devolva o conteúdo original
            $(this).parent().text(conteudoOriginal);

        });

    });
    
    $(document).on('dblclick', '.tdEditable', function () {

        var conteudoOriginal    = $(this).text(); //resgate o conteúdo atual da célula
        
        var tr                  = $(this).closest('tr') ;
        var a                   = $(this).children().first();
        var row                 = gridTabelas.row(tr)   ;
        var rowData             = row.data()            ;
        var idTabela            = rowData.idTabela      ;
        var elemento            = $(this);

        $(this).html("<input id='mudaDescricao' class='form-control' type='text' value='" + conteudoOriginal + "' />");

        $(this).children().first().focus();

        $(this).children().first().keypress(function (e) {

            if (e.which == 13 )
            {
                var novoConteudo = $(this).val();

                var request = $.ajax({

                    type    : "GET",
                    url     : a.attr('data-url'),
                    data    : a.attr('data-query-string') + '&novoValor='+novoConteudo

                });

                request.done(function(dRt){
                    gridTabelas.draw(false);
                });
            }
        });

        $(this).children().first().blur(function () {
            $(this).parent().text(conteudoOriginal);
        });
    });
})