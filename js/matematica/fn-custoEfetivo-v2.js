function calculaJurosCET(vnum_mes, vjuranox_fin, vjur_mes, parcela, data_contrato, proxData, vvlr_prin, difAno)
{
    
    var strFormula  = new Array()                   ;
    var b           = 0                             ;
    var vjur        = 0.5                           ;
    var vjur_fin    = vjuranox_fin.replace(".", "") ;
    vjur_fin        = vjur_fin.replace(",", ".");
    var vjur1       = 1;
    var vjur2       = 0;
    var vjur_fin1   = 1;
    var vjur_fin2   = 0;
    vjur_mes        = vjur_mes.replace(".", "");
    vjur_mes        = vjur_mes.replace(",", ".");
    vjur_mes        = parseFloat(vjur_mes) / 100;
    var primeiraParcela = parcela;
    igual = 0;
    denovo = 0;
    vjur_fin_old = 0;
    vjur_dia = ((Math.pow(1 + (vjur_mes), (1 / 30))) - 1) / 100;
    vjur_dia = vjur_dia * 100;
    vvlr_prin_old = vvlr_prin;

    while ((Math.abs(vvlr_prin - b)) > 0.00001){
        vvlr_tot = 0;
        parcela = primeiraParcela;
        data_niver_cntr = getAniversarioContrato(data_contrato, primeiraParcela);
        vdif_dc = calculaDiferencaEntreDatas(data_contrato, primeiraParcela);
        //if((vdif_dc[0] == 28) || (vdif_dc[0] == 29))
        //vdif_dc[0] = 31;
        mes_contrato = data_contrato.substring(3, 5);
        ano_contrato = data_contrato.substring(6, 10);
        adif_dc = new Array();
        adiff_dc = 0;
        vdiff_data = 0;
        i_acerto = 0;
        vvlr_novo = 0;
        dias_padrao = quantidadeDias(mes_contrato, ano_contrato);


        if (vdif_dc[0] > dias_padrao) // se mais de 31 dias
        {
            //				alert(vdif_dc[0] + ' > ' + dias_padrao + ' em ' + mes_contrato + '/' + ano_contrato);
            i_acerto = 1;
            adif_dc1 = calculaDiferencaEntreDatas(data_contrato, data_niver_cntr);
            adif_dc2 = calculaDiferencaEntreDatas(data_contrato, primeiraParcela);
            qtdMesesDif = contaMeses(data_niver_cntr, primeiraParcela);
            qtdMesesDif = qtdMesesDif - 1;
            adif_dc = qtdMesesDif + (adif_dc1[0] / dias_padrao);
            idx = 0;
            if (vvlr_prin == vvlr_prin_old)
            {
                adiff_dc = adif_dc;
                v1 = (1 + vjur_dia);
                v2 = Math.abs(adif_dc1[0]);
                vvlr_desc1 = Math.pow(v1, v2);
                vvlr_desc = vvlr_prin * (vvlr_desc1);
                vvlr_desc = Arredonda(vvlr_desc, 2);
                vvlr_novo = vvlr_desc;
                str_vlr_desc = String(vvlr_desc).replace(".", ",");
                idx++;
            }
            vvlr_tot_old = vvlr_tot;
            //alert('qtdMesesDif: ' + qtdMesesDif + '\nvjur_mes: ' + vjur_mes + '\nvjur_dia: ' + vjur_dia +  '\nvvlr_prin_old: ' + vvlr_prin_old + '\nvvlr_prin: ' + vvlr_prin +'\nadif_dc: ' + adif_dc + '\nadif_dc1: ' + adif_dc1 + '\ndias_padrao: ' + dias_padrao);
        }
        i_acerto = 0;
        for (init = 0; init < (parseInt(vnum_mes)); init++)
        {
            
            if (init != 0)
            {
                proxData = verProximaData(primeiraParcela, parcela);
                vdif_data = calculaDiferencaEntreDatas(parcela, proxData);
                //alert('!= 0: \nvdif_data: ' + vdif_data[0]);
            } else
            {
                proxData = parcela;
                vdif_data = vdif_dc;
                //alert('eh 0: \nvdif_dc[0]: ' + vdif_dc[0]);
            }
            
            if ((vvlr_tot != 0) && (init == 0))
            {
                vvlr_desc = vvlr_tot;
                vvlr_desc = Arredonda(vvlr_desc, 2);
                str_vlr_desc = String(vvlr_desc).replace(".", ",");
                strFormula[(init + i_acerto)] = milhar(String(str_vlr_desc), String(str_vlr_desc));
                parcela = proxData;
                vdiff_data = vdif_data[0];
                
            } else
            {
                
                //if((vdif_data[0] == 28) || (vdif_data[0] == 29))
                //vdif_data[0] = 31;
                vdiff_data += vdif_data[0];
                v1 = (1 + vjur_fin / 100);
                v2 = Math.abs(vdiff_data) / (365);
                vvlr_desc1 = Math.pow(v1, v2);
                vvlr_desc = vvlr_pres / vvlr_desc1;
                
/*                console.log('v1 ' + v1);
                console.log('v2 ' + v2);
                console.log('vvlr_desc ' + vvlr_desc);
                console.log('vvlr_desc1 ' + vvlr_desc1);*/
                
                vvlr_desc = Arredonda(vvlr_desc, 2);
                
                vvlr_tot = vvlr_tot + vvlr_desc;
                str_vlr_desc = String(vvlr_desc).replace(".", ",");
                strFormula[(init + i_acerto)] = milhar(String(str_vlr_desc), String(str_vlr_desc));
                parcela = proxData;
                
//                console.log(milhar(String(str_vlr_desc)));
//                console.log(str_vlr_desc);
            }
            //				alert('<30 \ndif_data: ' + vdif_data[0] + '\nvdiff_data: ' + vdiff_data + '\nv1: ' + v1 + '\nv2: ' + v2 + '\nvjur_fin: ' + vjur_fin + '\nvjurfin1: ' + vjur_fin1 + '\nvjurfin2: ' + vjur_fin2 +'\nvvlr_desc1: ' + vvlr_desc1 +'\nvnum_mes: ' + vnum_mes + '\ninit: ' + init + '\nprimeiraParcela: ' + primeiraParcela + '\nparcela: ' + parcela + '\nproxData: ' + proxData + '\ndifAno: ' + difAno[0] + '\nvvlr_tot: ' + vvlr_tot + '\nvvlr_prin: ' + vvlr_prin +'\nformula: ' + strFormula[(init + i_acerto)]);
        }
        
        console.log('vjur_fin1 ' + vjur_fin1);
        console.log('vjur_fin2 ' + vjur_fin2);
        console.log('vjur_fin '  + vjur_fin);
        
        b = vvlr_tot;
        
        if (b < vvlr_prin)
        {
            vjur_fin1 = vjur_fin;
        }
        else
        {
            vjur_fin2 = vjur_fin;
        }
        
        vjur_fin = (parseFloat(vjur_fin1) + parseFloat(vjur_fin2)) / 2;
        
        if (vjur_fin_old == vjur_fin)
        {
            igual++;
        }
        else
        {
            igual = 0;
        }
        
        vjur_fin_old = vjur_fin;
        
        //alert(vjur_fin);
        if (igual == 10)
        {
            if (denovo == 0)
            {
                vjur_fin1 = vjuranox_fin.replace(".", "");
                vjur_fin1 = vjur_fin1.replace(",", ".");
                vjur_fin1 = vjur_fin1 * 2;
                denovo = 1;
                igual = 0;
            } else
                break;
        }
    }
    vjuranox_fin = (Math.pow((1 + parseFloat(vjur_fin)), 12) - 1);
    vjuranox_fin = String(Arredonda(vjuranox_fin, 5));
    vjur_fin = vjur_fin;
    vjur_fin = String(Arredonda(vjur_fin, 5));

    return new Array(strFormula, vvlr_tot, vjuranox_fin, vjur_fin);
}

function poezero(valor, tNum) 
{
    tValor = valor.length;
    vFinal = valor;
    zeros = '';
    for (i = 0; i < (tNum - tValor); i++)
    {
        zeros += '0';
    }
    vFinal = zeros + vFinal;
    return(vFinal);
}

////////////////////////////////////////////////////////
// verifica se dezena tem dois digitos.. e se nao tiver
// retorna valor realemente em dezena (PARA CORRIGIR 
// PROBLEMA EM CENTAVOS)
////////////////////////////////////////////////////////

function dezena(c) 
{
    
    tam_c = String(c).length;
    valor = c               ;
    
    if (tam_c == 1) 
    {
        valor = String(valor) + "0";
    }
    
    return valor;
    
}


///////////////////////////////////////////////
// Coloca ponto (.) para separar milhar
///////////////////////////////////////////////

function milhar(c, r) 
{
    
    c2 = c;
    aGrupo = new Array();
    ajuda = 0;
    
    if (c2.indexOf(".") != -1) 
    {
        
        aGrupo[5] = c.substring(c.indexOf(",") + 1, c.length);
        
        if (String(aGrupo[5]).length == 1) 
        {
            ajuda = 1;
        }
        
        aGrupo[5] = String(aGrupo[5].substring(0, 2));
        aGrupo[5] = dezena(aGrupo[5]);
        ct = c.substring(0, c.indexOf(","));
        
    } 
    else 
    {
        
        if (c2.indexOf(",") != -1) {
            aGrupo[5] = c.substring(c.indexOf(",") + 1, c.length);
            if (String(aGrupo[5]).length == 1) {
                ajuda = 1;
            }
            aGrupo[5] = String(aGrupo[5].substring(0, 2));
            aGrupo[5] = dezena(aGrupo[5]);
            ct = c.substring(0, c.indexOf(","));
        } else {
            aGrupo[5] = "00";
            ct = c;
        }
        tt = "";
        for (f = 0; f < (13 - ct.length); f++) {
            tt += "0";
        }
        tt += ct;
    }
    aGrupo[1] = tt.substring(1, 4);
    aGrupo[2] = tt.substring(4, 7);
    aGrupo[3] = tt.substring(7, 10);
    aGrupo[4] = tt.substring(10, 13);
    aGrupo[5] = "," + aGrupo[5];

    tt = String(aGrupo[1]) + '.' + String(aGrupo[2]) + '.' + String(aGrupo[3]) + '.' + String(aGrupo[4]) + String(aGrupo[5]);

    tam_c = c.length + ajuda;
    tam_tt = tt.length;

    if ((r.indexOf(",") == -1)) {
        difer = (tam_tt - tam_c);
    } else if ((r.indexOf(",") != -1)) {
        difer = (tam_tt - tam_c) + 3;
    }

    tu = tt.substring(difer, tam_tt);
    if (tt.substring(0, 3) > 0) {
        tu = tt.substring(difer - 6, tam_tt);
    } else

    if ((tt.substring(0, 3) == 0) && (tt.substring(4, 7)) > 0) {
        tu = tt.substring(difer - 5, tam_tt);
    } else

    if ((tt.substring(4, 7) == 0) && (tt.substring(8, 11) > 0)) {
        tu = tt.substring(difer - 4, tam_tt);
    } else

    if (tt.substring(8, 11) == 0) {
        tu = tt.substring(difer - 3, tam_tt);
    }

    correto = tu;
    return(correto);
}

///////////////////////////////////////////////
// Transforma o numero passado em Numero Romano 
///////////////////////////////////////////////

function romanos(valor) {
    vlr = parseInt(valor);
    switch (vlr) {
        case 1:
            var roma = "I";
            break;
        case 2:
            var roma = "II";
            break;
        case 3:
            var roma = "III";
            break;
        case 4:
            var roma = "IV";
            break;
        case 5:
            var roma = "V";
            break;
        case 6:
            var roma = "VI";
            break;
        case 7:
            var roma = "VII";
            break;
        case 8:
            var roma = "VIII";
            break;
        case 9:
            var roma = "IX";
            break;
        case 10:
            var roma = "X";
            break;
        case 11:
            var roma = "XI";
            break;
        case 12:
            var roma = "XII";
            break;
        case 13:
            var roma = "XIII";
            break;
        case 14:
            var roma = "XIV";
            break;
        case 15:
            var roma = "XV";
            break;
        case 16:
            var roma = "XVI";
            break;
        case 17:
            var roma = "XVII";
            break;
        case 18:
            var roma = "XVIII";
            break;
        case 19:
            var roma = "XIX";
            break;
        case 20:
            var roma = "XX";
            break;
        case 21:
            var roma = "XXI";
            break;
        case 22:
            var roma = "XXII";
            break;
        case 23:
            var roma = "XXIII";
            break;
        case 24:
            var roma = "XXIV";
            break;
        case 25:
            var roma = "XXV";
            break;
        case 26:
            var roma = "XXVI";
            break;
        case 27:
            var roma = "XXVII";
            break;
        case 28:
            var roma = "XXVIII";
            break;
        case 29:
            var roma = "XXIX";
            break;
        case 30:
            var roma = "XXX";
            break;
    }
    return(roma);
}


///////////////////////////////////////////////
// Fun��o que escreve valor por extenso
///////////////////////////////////////////////

function extenso(c)
{
    if (c == "" || c < 0 || c >= 10000000)
    {
        return(-1);
    } else
    {
        if (c == 0)
        {
            return(" ZERO ");
        } else
        {
            aUnid = new Array();
            aDezena = new Array();
            aCentena = new Array();
            aGrupo = new Array();
            aTexto = new Array();

            aUnid[1] = "hum ";
            aUnid[2] = "dois ";
            aUnid[3] = "tr�s ";
            aUnid[4] = "quatro ";
            aUnid[5] = "cinco ";
            aUnid[6] = "seis ";
            aUnid[7] = "sete ";
            aUnid[8] = "oito ";
            aUnid[9] = "nove ";
            aUnid[10] = "dez ";
            aUnid[11] = "onze ";
            aUnid[12] = "doze ";
            aUnid[13] = "treze ";
            aUnid[14] = "quatorze ";
            aUnid[15] = "quinze ";
            aUnid[16] = "dezesseis ";
            aUnid[17] = "dezessete ";
            aUnid[18] = "dezoito ";
            aUnid[19] = "dezenove ";

            aDezena[1] = "dez ";
            aDezena[2] = "vinte ";
            aDezena[3] = "trinta ";
            aDezena[4] = "quarenta ";
            aDezena[5] = "cinquenta ";
            aDezena[6] = "sessenta ";
            aDezena[7] = "setenta ";
            aDezena[8] = "oitenta ";
            aDezena[9] = "noventa ";

            aCentena[1] = "cento ";
            aCentena[2] = "duzentos ";
            aCentena[3] = "trezentos ";
            aCentena[4] = "quatrocentos ";
            aCentena[5] = "quinhentos ";
            aCentena[6] = "seiscentos ";
            aCentena[7] = "setecentos ";
            aCentena[8] = "oitocentos ";
            aCentena[9] = "novecentos ";

            if (c.indexOf(".") != -1)
            {
                aGrupo[4] = c.substring(c.indexOf(".") + 1, c.length);
                aGrupo[4] = aGrupo[4].substring(0, 2);
                aGrupo[4] = dezena(aGrupo[4]);
                ct = c.substring(0, c.indexOf("."));
            } else
            {
                if (c.indexOf(",") != -1)
                {
                    aGrupo[4] = c.substring(c.indexOf(",") + 1, c.length);
                    aGrupo[4] = aGrupo[4].substring(0, 2);
                    aGrupo[4] = dezena(aGrupo[4]);
                    ct = c.substring(0, c.indexOf(","));
                } else
                {
                    aGrupo[4] = "00";
                    ct = c;
                }
                tt = "";
                for (f = 0; f < (10 - ct.length); f++)
                {
                    tt += "0";
                }
                tt += ct;
            }
            aGrupo[1] = tt.substring(1, 4);
            aGrupo[2] = tt.substring(4, 7);
            aGrupo[3] = tt.substring(7, 10);
            aGrupo[4] = "0" + aGrupo[4];

            for (f = 1; f < 5; f++)
            {
                cParte = aGrupo[f];
                if (parseFloat(cParte) < 10)
                {
                    nTamanho = 1;
                } else
                {
                    if (parseFloat(cParte) < 100)
                    {
                        nTamanho = 2;
                    } else
                    {
                        if (parseFloat(cParte) < 1000)
                        {
                            nTamanho = 3;
                        } else
                        {
                            nTamanho = 0;
                        }
                    }
                }
                aTexto[f] = "";
                if (nTamanho == 3)
                {
                    if (cParte.substring(1, 3) != "00")
                    {
                        aTexto[f] += aCentena[cParte.substring(0, 1)] + "e ";
                        nTamanho = 2;
                    } else
                    {
                        if (cParte.substring(0, 1) == "1")
                        {
                            aTexto[f] += "cem ";
                        } else
                        {
                            aTexto[f] += aCentena[cParte.substring(0, 1)];
                        }
                    }
                }
                if (nTamanho == 2)
                {
                    if (parseFloat(cParte.substring(1, 3)) < 10)
                    {
                        aTexto[f] += aUnid[cParte.substring(2, 3)];
                    } else
                    {
                        if (parseFloat(cParte.substring(1, 3)) < 20)
                        {
                            aTexto[f] += aUnid[cParte.substring(1, 3)];
                        } else
                        {
                            aTexto[f] += aDezena[cParte.substring(1, 2)];
                            if (cParte.substring(2, 3) != "0")
                            {
                                aTexto[f] += "e ";
                                nTamanho = 1;
                            }
                        }
                    }
                }
                if (nTamanho == 1)
                {
                    aTexto[f] += aUnid[cParte.substring(2, 3)];
                }
            }
            if (parseFloat(aGrupo[1] + aGrupo[2] + aGrupo[3]) == 0 && parseFloat(aGrupo[4]) != 0)
            {
                cFinal = aTexto[4];
                if (parseFloat(aGrupo[4]) == 1)
                {
                    cFinal += "centavo";
                } else
                {
                    cFinal += "centavos";
                }
            } else
            {
                if (parseFloat(aGrupo[1]) != 0)
                {
                    cFinal = aTexto[1];
                    if (parseFloat(aGrupo[1]) > 1)
                    {
                        cFinal += "milhoes";
                    } else
                    {
                        cFinal += "milh�o";
                    }
                    if (parseFloat(aGrupo[2] + aGrupo[3]) == 0)
                    {
                        cFinal += " de ";
                    } else
                    {
                        cFinal += ", ";
                    }
                } else
                {
                    cFinal = "";
                }
                if (parseFloat(aGrupo[2]) != 0)
                {
                    cFinal += aTexto[2] + "mil";
                    if (parseFloat(aGrupo[3]) != 0)
                    {
                        cFinal += ", ";
                    }
                }
                if (parseFloat(aGrupo[3]) != 0)
                {
                    cFinal += aTexto[3];
                }
                if (parseFloat(aGrupo[1] + aGrupo[2] + aGrupo[3]) == 1)
                {
                    cFinal += "real";
                } else
                {
                    cFinal += "reais";
                }
                if (parseFloat(aGrupo[4]) != 0)
                {
                    cFinal += " e " + aTexto[4];
                    if (parseFloat(aGrupo[4]) == 1)
                    {
                        cFinal += "centavo";
                    } else
                    {
                        cFinal += "centavos";
                    }
                }
            }
            return(cFinal);
        }
    }
}


///////////////////////////////////////////////
// Arredonda valores para a quantidade de casas
// necessarias na pagina
///////////////////////////////////////////////

function Arredonda(valor, casas) {

    var novo = Math.round(valor * Math.pow(10, casas)) / Math.pow(10, casas);

    return(novo);

}

///////////////////////////////////////////////
// verifica se campo "numero de infra��es" foi 
// preenchido. Pois � por ele que sabemos quantas 
// infra��es s�o possiveis de serem preenchidas e 
// o que deve ser feito na programa��o.
///////////////////////////////////////////////

function ver_infra() {
    if (form1.numinfra.value == 1)
    {
        form1.codinfra2.value = '';
        form1.codinfra3.value = '';
        form1.codinfra4.value = '';
    } else
    if (form1.numinfra.value == 2) {
        form1.codinfra3.value = '';
        form1.codinfra4.value = '';
    } else
    if (form1.numinfra.value == 3) {
        form1.codinfra4.value = '';
    } else
    if (form1.numinfra.value == 4) {
    }
}

///////////////////////////////////////////////
// verifica quantidade de virgulas em algum valor
///////////////////////////////////////////////

function ver_virg(valor) {
    tamanho = valor.length;
    qtd_v = 0;
    for (i = 0; i <= tamanho; i++) {
        j = i + 1;
        if (valor.substring(i, j) == ',') {
            qtd_v++;
        }
    }
    return(qtd_v)
}

///////////////////////////////////////////////
// verifica quantidade de pontos em algum valor
///////////////////////////////////////////////

function ver_ponto(valor) {
    tamanho = valor.length;
    qtd_p = 0;
    for (i = 0; i <= tamanho; i++) {
        j = i + 1;
        if (valor.substring(i, j) == '.') {
            qtd_p++;
        }
    }
    return(qtd_p)
}

///////////////////////////////////////////////
// Fun��o que retorna ano completo (19## ou 20##)
///////////////////////////////////////////////

function milenio(valor) {
    tvalor = valor.length;
    if (tvalor == 2) {
        if ((valor >= 94) && (valor < 100)) {
            vFinal = "19" + valor;
        } else
        {
            vFinal = "20" + valor;
        }
    } else
    {
        vFinal = valor;
    }
    return(vFinal);
}

function checaData(dia, mes, ano)
{

    if ((isNaN(dia)) || (isNaN(mes)) || (isNaN(ano)))
    {
        return -1;
    } else
    {
        if ((mes == 1) || (mes == 3) || (mes == 5) || (mes == 7) || (mes == 8) || (mes == 10) || (mes == 12))
        {
            if (dia > 31)
            {
                return -1;
            }
        } else
        {
            if (dia > 30)
            {
                return -1;
            }
        }
        if (mes == 2)
        {
            if (dia >= 29)
            {
                if ((ano % 4) != 0)
                {
                    return -1;
                }
            }
        }
    }
}

///////////////////////////////////////////////
// verifica dados do formulario
///////////////////////////////////////////////

function verifica_dados()
{
    var erros = 0; //variavel para saber se ter� erros no form
    erroMsg = '';

    vnum_mes = $("#vnum_mes").val();
    vvlr_pres = $("#vvlr_pres").val();
    vvlr_prin = $("#vvlr_prin").val();
    //vvlr_acess		= document.form1.vvlr_acess.value;
    // COMPATIBILIDADE FIREFOX
    data_contrato = $("#data_contrato").val();
    parcela = $("#parcela_1").val();

    parcela_verifica_dia = parcela.substring(0, 2);
    parcela_verifica_mes = parcela.substring(3, 5);
    parcela_verifica_ano = parcela.substring(6, 10);
    if (checaData(parcela_verifica_dia, parcela_verifica_mes, parcela_verifica_ano))
    {
        erros++;
        erroMsg += 'Favor preencher corretamente a Data da Primeira Parcela\n';
    }
    contrato_verifica_dia = data_contrato.substring(0, 2);
    contrato_verifica_mes = data_contrato.substring(3, 5);
    contrato_verifica_ano = data_contrato.substring(6, 10);
    if (checaData(contrato_verifica_dia, contrato_verifica_mes, contrato_verifica_ano))
    {

        erros++;
        erroMsg += 'Favor preencher corretamente a data do contrato\n';
    }
    if (erros == '0')
    {
        //alert('ok');
        impressao()
    } else
    {
        alert(erroMsg);
        return(false);
    }
}

///////////////////////////////////////////////
// Preenche campo de juros no formul�rio (apenas para efeito visual)
///////////////////////////////////////////////

function descobreUltimaParcela(primeiraParcela, parcela, vnum_mes)
{
    pp_dia = primeiraParcela.substring(0, 2);
    pp_mes = primeiraParcela.substring(3, 5);
    pp_ano = primeiraParcela.substring(6, 10);
    p_dia = parcela.substring(0, 2);
    p_mes = parcela.substring(3, 5);
    p_ano = parcela.substring(6, 10);
    up_dia = pp_dia;
    up_mes = parseInt(p_mes) + (parseInt(vnum_mes) - 1);
    up_ano = p_ano;

    if (up_mes > 12)
    {
        up_mes = up_mes - 12;
        up_ano = parseInt(up_ano) + 1;
    }
    if ((up_mes == 4) || (up_mes == 6) || (up_mes == 9) || (up_mes == 11))
    {
        if (up_dia > 30)
        {
            up_dia = 30;
        } else
        {
            up_dia = pp_dia;
        }
    }
    if (up_mes == 2)
    {
        if (up_dia > 28)
        {
            if (up_ano % 4 != 0)
            {
                up_dia = 28;
            } else
            {
                up_dia = 29;
            }
        }
    }
    ultDiaStr = String(up_dia);
    ultMesStr = String(up_mes);
    if (String(up_dia).length < 2)
        ultDiaStr = "0" + String(up_dia);
    if (String(up_mes).length < 2)
        ultMesStr = "0" + String(up_mes);
    vdata = ultDiaStr + '/' + ultMesStr + '/' + up_ano;

    return vdata;
}

function mostra_juros()
{
    erro = 0;
    vnum_mes = $("#vnum_mes").val();
    vvlr_pres = $("#vvlr_pres").val();
    vvlr_prin = $("#vvlr_prin").val();
    //vvlr_acess 		= form1.vvlr_acess.value;
    // COMPATIBILIDADE FIREFOX
    data_contrato = $("#data_contrato").val();
    parcela = $("#parcela_1").val();

    vvlr_pres = vvlr_pres.replace(".", "");
    vvlr_pres = vvlr_pres.replace(",", ".");

    vvlr_prin = vvlr_prin.replace(".", "");
    vvlr_prin = vvlr_prin.replace(",", ".");

    //vvlr_acess = vvlr_acess.replace(".","");
    //vvlr_acess = vvlr_acess.replace(",",".");

    //if((isNaN(vnum_mes)) || (isNaN(vvlr_pres)) || (isNaN(vvlr_prin)) || (isNaN(vvlr_acess)))
    if ((isNaN(vnum_mes)) || (isNaN(vvlr_pres)) || (isNaN(vvlr_prin)))
    {
        erro++;
    }
    if (erro == 0)
    {
        //if( (vnum_mes != '') && (vvlr_pres != '') && (vvlr_prin != '') && (vvlr_acess != '')){
        if ((vnum_mes != '') && (vvlr_pres != '') && (vvlr_prin != '')) {

            if ((vvlr_pres * vnum_mes) < vvlr_prin) {
                alert('Total de Presta��es menor que valor Principal!');
                return -1;
            }
            if (parseInt(vvlr_pres) > parseInt(vvlr_prin)) {
                alert('Juros ao m�s superior a 100%!');
                return -1;
            }

            //var v2 = parseInt(vvlr_prin) + parseInt(vvlr_acess);
            var v2 = parseInt(vvlr_prin);
            var a = 0;
            var b = 0;
            var p = vvlr_pres;
            var vdif_dc = new Array();
            var primeiraParcela = parcela;
            var vvlr_tot = 0;
            var ultimaParcela = descobreUltimaParcela(primeiraParcela, parcela, vnum_mes);

            //parcela		= primeiraParcela;
            vdif_dc[0] = calculaDiferencaEntreDatas(data_contrato, ultimaParcela);
            var n = (vnum_mes) * (-1);
            //var n=(parseInt(vdif_dc[0][0]) / 30)*(-1);
            //alert(vdif_dc[0][0] + '\n' + n + '\n' + ultimaParcela);
            var vjur1 = 1;
            var vjur2 = 0;
            var vjur_fin1 = 1;
            var vjur_fin2 = 0;
            var vjur = 0.5;
            var vjuranox = 0;
            var vjur_fin = 0.5;
            var vjuranox_fin = 0;
            //alert(v2);
            while (Math.abs(v2 - a) > 0.00001)
            {
                a = p * ((1 - (Math.pow((1 + vjur), n))) / vjur);
                if (a < v2)
                    vjur1 = vjur;
                else
                    vjur2 = vjur;
                vjur = (vjur1 + vjur2) / 2;
            }
            vjuranox = (Math.pow((1 + parseFloat(vjur)), 12) - 1) * 100;
            vjuranox = String(Arredonda(vjuranox, 5));
            vjur = vjur * 100;
            vjur = String(Arredonda(vjur, 5));

            while (Math.abs(vvlr_prin - b) > 0.00001)
            {
                b = p * ((1 - (Math.pow((1 + vjur_fin), n))) / vjur_fin);
                if (b < vvlr_prin)
                    vjur_fin1 = vjur_fin;
                else
                    vjur_fin2 = vjur_fin;
                vjur_fin = (vjur_fin1 + vjur_fin2) / 2;
                //alert('b: ' + b + '\nprinc: ' + v2 + '\nv2-b: ' + (v2-b));
            }
            vjuranox_fin = (Math.pow((1 + parseFloat(vjur_fin)), 12) - 1) * 100;
            vjuranox_fin = String(Arredonda(vjuranox_fin, 5));
            vjur_fin = vjur_fin * 100;
            vjur_fin = String(Arredonda(vjur_fin, 5));

            vvlr_pres = String(Arredonda(vvlr_pres, 2));
            vvlr_pres = vvlr_pres.replace(".", ",");
            vvlr_prin = String(Arredonda(vvlr_prin, 2));
            vvlr_prin = vvlr_prin.replace(".", ",");
            //vvlr_acess = String(Arredonda(vvlr_acess,2));
            //vvlr_acess = vvlr_acess.replace(".",",");

            //alert('vjur: ' + vjur + '\nvjurano: ' + vjuranox + '\nvjur2: ' + vjur_fin + '\nvjurano2: ' + vjuranox_fin);
//            document.getElementById('vjur').value = vjur.replace(".", ",");
//            document.getElementById('vjuranox').value = vjuranox.replace(".", ",");
            $("#vjur").val(vjur.replace(".", ","));
            $("#vjuranox").val(vjuranox.replace(".", ","));
            $("#vjur_fin").val(vjur_fin.replace(".", ","));
            $("#vjuranox_fin").val(vjuranox_fin.replace(".", ","));
//            console.log(vjur.replace(".", ","));
//            console.log(vjuranox.replace(".", ","));
            // COMPATIBILIDADE FIREFOX
            /*				document.form1.vjur_fin.value=vjur_fin.replace(".",",");
             document.form1.vjuranox_fin.value=vjuranox_fin.replace(".",",");
             document.form1.vvlr_prin.value = milhar(vvlr_prin,vvlr_prin);
             document.form1.vvlr_pres.value = milhar(vvlr_pres,vvlr_pres);
             */
            //PARA IE
//            document.getElementById('vjur_fin').value = vjur_fin.replace(".", ",");
//            document.getElementById('vjuranox_fin').value = vjuranox_fin.replace(".", ",");
//            console.log(vjur_fin.replace(".", ","));
//            console.log(vjuranox_fin.replace(".", ","));
            //document.getElementById('vvlr_prin').value 		= milhar(String(vvlr_prin),String(vvlr_prin));
            //document.getElementById('vvlr_pres').value 		= milhar(String(vvlr_pres),String(vvlr_pres));

            //document.getElementById('vvlr_acess').value = milhar(vvlr_acess,vvlr_acess);


            //			document.getElementById('meses').innerHTML = htmlCamposMeses;
        }
        //document.getElementById('resultado').innerHTML = milhar(vvlr_acess,vvlr_acess);
        //document.form1.dia_venc.focus();
    }
}

function mascara(valor, tipo)
{
    var val = valor.value;
    if (tipo == 'data')
    {
        if (val.length == 2)
            val += 'http://www.procon.sp.gov.br/';
        if (val.length == 5)
            val += 'http://www.procon.sp.gov.br/';
        valor.value = val;
    }
}

///////////////////////////////////////////////
// Procura pelas virgulas de uma variavel e se naum achar adiciona ,00
///////////////////////////////////////////////

function proc_virg(valor) {
    virg = 0;
    tamanho = valor.length;
    numero = '';

    for (k = 0; k <= tamanho - 1; k++) {
        l = k + 1;
        if (valor.substring(k, l) == ',') {
            virg++;
        } else
        if (valor.substring(k, l) == '.') {
            virg++;
        }

        numero = numero + valor.substring(k, l);
    }
    if (virg == 0) {
        numero = numero + ',00';
    }
    return(numero);
}

function calculaDiferencaEntreDatas(dataInicio, dataFim)
{
    initDia = dataInicio.substring(0, 2);
    initMes = dataInicio.substring(3, 5);
    initAno = dataInicio.substring(6, 10);

    fimDia = dataFim.substring(0, 2);
    fimMes = dataFim.substring(3, 5);
    fimAno = dataFim.substring(6, 10);
    vdi_v = new Date(initMes + '/' + initDia + '/' + initAno);
    vdf_v = new Date(fimMes + '/' + fimDia + '/' + fimAno);
    vdi = Date.parse(vdi_v);
    vdf = Date.parse(vdf_v);

    vdif_data = (((((vdf - vdi) / 1000) / 60) / 60) / 24);
    vdif_data2 = vdf - vdi;

    return new Array(vdif_data, vdif_data2);
}

function verProximaData(dataPrimeiraParcela, dataUltimaParcela)
{
    dppDia = dataPrimeiraParcela.substring(0, 2);
    dppMes = dataPrimeiraParcela.substring(3, 5);
    dppAno = dataPrimeiraParcela.substring(6, 10);
    dupDia = dataUltimaParcela.substring(0, 2);
    dupMes = dataUltimaParcela.substring(3, 5);
    if (dupMes.substring(0, 1) == '0')
    {
        dupMes = dupMes.substring(1, 2);
    }
    dupAno = dataUltimaParcela.substring(6, 10);
    proxDia = parseInt(dupDia);
    proxMes = parseInt(dupMes) + 1;
    proxAno = parseInt(dupAno);

    //alert('dppmes: ' + dppMes + '\ndupmes: ' + dupMes + '\nparseInt(dupMes): ' + parseInt(dupMes) + '\nproxMes: ' + proxMes);
    if (dupMes == 12)
    {
        proxMes = 1;
        proxAno = proxAno + 1;
    }
    if ((dupMes == 4) || (dupMes == 6) || (dupMes == 9) || (dupMes == 11))
    {
        if (dupDia >= 30)
            proxDia = dppDia;
    } else
    {
        if ((dupMes == 2) || (dupDia > 31))
        {
            proxDia = dppDia;
        }
    }

    if (proxMes == 2)
    {
        if (proxDia > 28)
        {
            if ((proxAno % 4) != 0)
                proxDia = 28;
            else
                proxDia = 29;
        }
    }
    if ((proxMes == 4) || (proxMes == 6) || (proxMes == 9) || (proxMes == 11))
    {
        if (proxDia >= 31)
            proxDia = 30;
    }
    proxDiaStr = String(proxDia);
    proxMesStr = String(proxMes);
    if (String(proxDia).length < 2)
        proxDiaStr = "0" + String(proxDia);
    if (String(proxMes).length < 2)
        proxMesStr = "0" + String(proxMes);
    vdata = proxDiaStr + '/' + proxMesStr + '/' + proxAno;
    //alert('dppmes: ' + dppMes + '\ndupmes: ' + dupMes + '\nparseInt(dupMes): ' + parseInt(dupMes) + '\nproxMes: ' + proxMes);
    return vdata;
}

function getUltimoDiaAno(data)
{
    diDia = data.substring(0, 2);
    diMes = data.substring(3, 5);
    diAno = data.substring(6, 10);
    dfDia = diDia;
    dfMes = diMes;
    vdfAno = parseInt(diAno);
    dfAno = vdfAno + 1;
    if ((dfMes == 4) || (dfMes == 6) || (dfMes == 9) || (dfMes == 11))
    {
        if (dfDia >= 31)
            dfDia = 30;
        else
        {
            if (dfMes == 2)
            {
                if (dfDia > 28)
                {
                    if ((dfAno % 4) != 0)
                        dfDia = 28;
                    else
                        dfDia = 29;
                }
            } else
                dfDia = 31;
        }
    }
    ultDiaStr = String(dfDia);
    ultMesStr = String(dfMes);
    if (String(dfDia).length < 2)
        ultDiaStr = "0" + String(dfDia);
    if (String(dfMes).length < 2)
        ultMesStr = "0" + String(dfMes);
    vdata = ultDiaStr + '/' + ultMesStr + '/' + dfAno;
    return vdata;
}


function quantidadeDias(mes, ano)
{
    if ((mes == 2) || (mes == 4) || (mes == 6) || (mes == 9) || (mes == 11))
    {
        if (mes == 2)
        {
            if ((ano % 4) != 0)
                dias = 28;
            else
                dias = 29;
        } else
        {
            dias = 30;
        }
    } else
    {
        dias = 31;
    }

    return dias;
}
function getAniversarioContrato(data_contrato, primeiraParcela)
{
    dia_dc = data_contrato.substring(0, 2);
    dia_pp = primeiraParcela.substring(0, 2);
    mes_dc = data_contrato.substring(3, 5);
    ano_dt = data_contrato.substring(6, 10);
    if (mes_dc.substring(0, 1) == '0')
        mes_dc = mes_dc.substring(1, 2);
    mes_dt = parseInt(mes_dc);
    if (parseInt(dia_dc) > parseInt(dia_pp))
    {
        mes_dt++;
    }
    if (parseInt(mes_dt) > 12)
    {
        ano_dt = parseInt(ano_dt) + 1;
        mes_dt = parseInt(mes_dt) - 12;
    }
    dt = dia_pp + '/' + poezero(String(mes_dt), 2) + '/' + milenio(ano_dt);
    //alert('mes_dc: ' + mes_dc + '\nmes_dt: ' + mes_dt + '\nano_dt: ' + ano_dt + '\ndt:' + dt );
    return dt;
}

function contaMeses(data1, data2)
{
    mes1 = data1.substring(3, 5);
    mes2 = data2.substring(3, 5);

    if (mes1.substring(0, 1) == '0')
        mes1 = mes1.substring(1, 2);
    if (mes2.substring(0, 1) == '0')
        mes2 = mes2.substring(1, 2);

    if (parseInt(mes1) > parseInt(mes2))
    {
        //alert(parseInt(mes1) + ' > ' + parseInt(mes2));
        mes1 = parseInt(mes1) - 12;
    }
    dif = parseInt(mes2) - parseInt(mes1);
    return dif;
}

///////////////////////////////////////////////
// Tela para impress�o do resultado do form
///////////////////////////////////////////////

function impressao()
{
    ////////////////////////////////////////////////////////////////////////
    // atribui��o de valores do formulario para variaveis locais e declara��o de variaveis
    ////////////////////////////////////////////////////////////////////////

    vnum_mes = $("#vnum_mes").val();
    vvlr_pres = $("#vvlr_pres").val();
    vvlr_prin = $("#vvlr_prin").val();
//    vnum_mes = document.form1.vnum_mes.value;
//    vvlr_pres = document.form1.vvlr_pres.value;
//    vvlr_prin = document.form1.vvlr_prin.value;
    //vvlr_acess		= document.form1.vvlr_acess.value;

    vvlr_pres = vvlr_pres.replace(".", "");
    vvlr_pres = vvlr_pres.replace(",", ".");
    // COMPATIBILIDADE FIREFOX
    /*		var vjur			= document.form1.vjur.value;
     var vjuranox		= document.form1.vjuranox.value;
     var vjur_fin		= document.form1.vjur_fin.value;
     var vjuranox_fin	= document.form1.vjuranox_fin.value;
     data_contrato		= document.form1.data_contrato.value;
     parcela				= document.form1.parcela_1.value;
     */
    // PARA IE
    var vjur = $("#vjur").val();
    var vjuranox = $("#vjuranox").val();
    var vjur_fin = $("#vjur_fin").val();
    var vjuranox_fin = $("#vjuranox_fin").val();
    data_contrato = $("#data_contrato").val();
    parcela = $("#parcela_1").val();
    vjur_fin = vjuranox_fin.replace(".", "");
    vjur_fin = vjuranox_fin.replace(",", ".");
    ////////////////////////////////////////////////////////////////////////
    // Se n�o tiver sido calculado juros, realiza o calculo agora
    ////////////////////////////////////////////////////////////////////////

    if (vjur == "")
        mostra_juros();

    ////////////////////////////////////////////////////////
    // L�gica e c�lculos das datas da tabela
    ////////////////////////////////////////////////////////
    vvlr_tot = 0;
    vet = new Array();
    vdif_data = new Array();

    primeiraParcela = parcela;
    ultDiaAno = getUltimoDiaAno(data_contrato);
    difAno = calculaDiferencaEntreDatas(data_contrato, ultDiaAno);
    strFormula = new Array();

    ////////////////////////////////

    vdif_dc = calculaDiferencaEntreDatas(data_contrato, parcela);
    mes_contrato = data_contrato.substring(3, 5);
    ano_contrato = data_contrato.substring(6, 10);
    adif_dc = new Array();
    adiff_dc = 0;
    vdiff_data = 0;
    i_acerto = 0;
    if (vdif_dc[0] > 30) // se mais de 30 dias
    {
        i_acerto = 1;
        adif_dc[0] = vdif_dc[0] - 30;
        adif_dc[1] = 30;
        if (adif_dc[0] > 30) // se mais de 60 dias
        {
            i_acerto = 2;
            adif_dc[2] = adif_dc[0] - 30;
            adif_dc[3] = 0;
            adif_dc[4] = 30;
        }
        idx = 0;
        for (i = 0; i < adif_dc.length; i = i + i_acerto)
        {
            adiff_dc += adif_dc[i];
            v1 = (1 + vjur_fin / 100);
            v2 = Math.abs(adiff_dc) / (365);
            vvlr_desc1 = Math.pow(v1, v2);
            vvlr_desc = vvlr_pres / vvlr_desc1;
            vvlr_desc = Arredonda(vvlr_desc, 2);
            vvlr_tot = vvlr_tot + vvlr_desc;
            str_vlr_desc = String(vvlr_desc).replace(".", ",");
            //strFormula[idx] = milhar(String(str_vlr_desc),String(str_vlr_desc));
            //				alert('>30_1 \ndif_dc: ' + vdif_dc[i] + '\nvdiff_dc: ' + adiff_dc + '\ndifAno: ' + difAno[0] + '\nidx: ' + idx + '\nv1: ' + v1 + '\nv2: ' + v2 + '\nvvlr_desc1: ' + vvlr_desc1 +'\nvvlr_desc: ' + vvlr_desc +'\nvvlr_tot: ' + vvlr_tot + '\nvjur_fin: ' + vjur_fin + '\nformula: ' + strFormula[idx]);
            idx++;
        }
        vvlr_tot = ((vvlr_tot) / (i_acerto + 1));
        //			alert('>30 \ndif_dc: ' + vdif_dc[i] + '\nvdiff_dc: ' + adiff_dc + '\ndifAno: ' + difAno[0] + '\nidx: ' + idx + '\nv1: ' + v1 + '\nv2: ' + v2 + '\nvvlr_desc1: ' + vvlr_desc1 +'\nvvlr_desc: ' + vvlr_desc +'\nvvlr_tot: ' + vvlr_tot + '\nvjur_fin: ' + vjur_fin + '\nformula: ' + strFormula[idx]);
    }
    i_acerto = 0;
    for (init = 0; init < (parseInt(vnum_mes)); init++)
    {
        if (init != 0)
        {
            proxData = verProximaData(primeiraParcela, parcela);
            vdif_data = calculaDiferencaEntreDatas(parcela, proxData);
        } else
        {
            proxData = parcela;
            vdif_data = vdif_dc;
        }
        if ((vvlr_tot != 0) && (init == 0))
        {
            vvlr_desc = vvlr_tot;
            vvlr_desc = Arredonda(vvlr_desc, 2);
            str_vlr_desc = String(vvlr_desc).replace(".", ",");
            strFormula[(init + i_acerto)] = milhar(String(str_vlr_desc), String(str_vlr_desc));
            parcela = proxData;
        } else
        {
            if ((vdif_data[0] == 28) || (vdif_data[0] == 29))
                vdif_data[0] = 31;
            vdiff_data += vdif_data[0];
            v1 = (1 + vjur_fin / 100);
            v2 = Math.abs(vdiff_data) / (difAno[0]);
            vvlr_desc1 = Math.pow(v1, v2);
            vvlr_desc = vvlr_pres / vvlr_desc1;
            vvlr_desc = Arredonda(vvlr_desc, 2);
            vvlr_tot = vvlr_tot + vvlr_desc;
            str_vlr_desc = String(vvlr_desc).replace(".", ",");
            strFormula[(init + i_acerto)] = milhar(String(str_vlr_desc), String(str_vlr_desc));
            parcela = proxData;
        }
        //			alert('<30 \ndif_data: ' + vdif_data[0] + '\nvdiff_data: ' + vdiff_data + '\ndifAno: ' + difAno[0] + '\nv1: ' + v1 + '\nv2: ' + v2 + '\nvvlr_desc1: ' + vvlr_desc1 +'\nvvlr_desc: ' + vvlr_desc +'\nvvlr_tot: ' + vvlr_tot + '\nvjur_fin: ' + vjur_fin + '\nformula: ' + strFormula[(init + i_acerto)]);
    }

    ////////////////////////////////
    /*		for(init=0; init < (parseInt(vnum_mes)); init++) 
     {
     proxData = verProximaData(primeiraParcela,parcela);
     vdif_data = calculaDiferencaEntreDatas(parcela,proxData);
     vjur_fin 	= vjuranox_fin.replace(".","");
     vjur_fin 	= vjuranox_fin.replace(",",".");
     v1=(1+vjur_fin/100);
     v2=Math.abs(vdif_data[0])/(difAno[0]);
     vvlr_desc1 	= Math.pow(v1,v2);	
     vvlr_desc 	= vvlr_pres / vvlr_desc1;
     vvlr_desc 	= Arredonda(vvlr_desc,2);
     vvlr_tot 	= vvlr_tot + vvlr_desc;
     str_vlr_desc 	= String(vvlr_desc).replace(".",",");
     strFormula[init] = milhar(String(str_vlr_desc),String(str_vlr_desc));
     
     }*/
    totalCount = strFormula.length;
    htmlResult = '<br>';
    htmlResult += '<table border=0 cellspacing=0 cellspadding=0 width=520 />';
    htmlResult += '<tr />';
    htmlResult += '<td />';
    htmlRstTot = '';
    for (i = 0; i < totalCount; i++)
    {
        if (i != totalCount - 1)
            htmlRstTot += strFormula[i] + ' + ';
        else
            htmlRstTot += strFormula[i];
    }
    str_vlr_prin = vvlr_prin;
    vvlr_prin = vvlr_prin.replace(".", "");
    vvlr_prin = vvlr_prin.replace(",", ".");
    vResultado = parseFloat(vvlr_tot) - parseFloat(vvlr_prin); // Resultado de tudo menos o valor principal
    str_Result = String(Arredonda(vResultado, 2)).replace(".", ",");
    /////////////////////////////////////////////////////////////////////
    // PARTE DO SISTEMA QUE DESCOBRE OS JUROS DO CET
    /////////////////////////////////////////////////////////////////////
    jurosCET = calculaJurosCET(vnum_mes, vjuranox_fin, vjur, primeiraParcela, data_contrato, proxData, vvlr_prin, difAno);
    ///////////////////////////////////////////////////////////////////////////////////

    //alert(jurosCET[0]);
    totalCount2 = jurosCET[0].length;
    htmlRstTot2 = '';
    for (i = 0; i < totalCount; i++)
    {
        if (i != totalCount2 - 1)
            htmlRstTot2 += jurosCET[0][i] + ' + ';
        else
            htmlRstTot2 += jurosCET[0][i];
    }
    vlr_total2 = jurosCET[1];
    vResultado2 = parseFloat(vlr_total2) - parseFloat(vvlr_prin); // Resultado de tudo menos o valor principal
    str_Result2 = String(Arredonda(vResultado2, 2)).replace(".", ",");

    juros_CET = jurosCET[3];
    vjuranox = vjuranox.replace(".", "");
    vjuranox = vjuranox.replace(",", ".");
    vjuranox = parseFloat(vjuranox);
    vjuranox_fin = vjuranox_fin.replace(".", "");
    vjuranox_fin = vjuranox_fin.replace(",", ".");
    vjuranox_fin = parseFloat(vjuranox_fin);
    //		juros_CET = juros_CET.replace(".","");
    //		juros_CET = juros_CET.replace(",",".");
    vjur_fin = (Math.pow(1 + (parseFloat(juros_CET) / 100), (1 / 12)) - 1) * 100;
    //	alert('vjur_fin: ' + vjur_fin +'\njCET: ' +juros_CET);
    //vjur_fin 	= ((Math.root(12,((juros_CET)))+1)/100);


    //document.form1.vjur_fin.value		= String(Arredonda(vjur_fin,5)).replace(".",",");
    //document.form1.vjuranox_fin.value	= String(Arredonda(juros_CET,5)).replace(".",",");
    /*if(navigator.appName == "Microsoft Internet Explorer")
     {
     document.getElementById('vjur_fin').value		= String(Arredonda(vjur_fin,5)).replace(".",",");
     document.getElementById('vjuranox_fin').value	= String(Arredonda(juros_CET,5)).replace(".",",");
     }
     else*/
    {
        $("#vjur_fin").val(String(Arredonda(vjur_fin, 5)).replace(".", ","));
        $("#vjuranox_fin").val(String(Arredonda(juros_CET, 5)).replace(".", ","));
    }

    //		htmlResult += '<b>Custo Efetivo Total Aproximado <font color="#880000"> ('+ String(Arredonda(vjuranox_fin,2)).replace(".",",") +' %)</font></b>';
    //		htmlResult += '<BR>';		
    //htmlResult += htmlRstTot + ' - ' + str_vlr_prin + ' = ' + milhar(str_Result,str_Result);
    htmlResult += '<BR><BR>';
    htmlResult += '<b>Custo Efetivo Total <font color="#880000"> (' + String(Arredonda(juros_CET, 2)).replace(".", ",") + ' %)</font></b> ';
    htmlResult += '<BR>';
    htmlResult += '<BR><BR>';
    htmlResult += '<b>Custo Efetivo Mensal <font color="#880000"> (' + String(Arredonda(vjur_fin, 2)).replace(".", ",") + ' %)</font></b> ';
    htmlResult += '<BR>';
    //htmlResult += htmlRstTot2 + ' - ' + str_vlr_prin + ' = ' + milhar(str_Result2,str_Result2);
    htmlResult += '<BR><BR>';
    //		htmlResult += 'As opera&ccedil;&otilde;es acima mostram que uma taxa de <b>' + String(Arredonda(vjuranox,2)).replace(".",",") + '% </b>';
    //		htmlResult += ' ao ano, corresponde a um CET (Custo Efetivo Total) de <b>' + String(Arredonda(juros_CET,2)).replace(".",",") + '% </b>ao ano, considerando acess&oacute;rios inclu&iacute;dos no valor de <b> R$ ' + vvlr_acess + '</b>.';
    //		htmlResult += '<BR><BR>';
    htmlResult += '</td>';
    htmlResult += '</tr>';

    $(".vjur_fin_font"      ).html(String(Arredonda(vjur_fin    , 2)).replace(".", ","));
    $(".vjuranox_fin_font"  ).html(String(Arredonda(juros_CET   , 2)).replace(".", ","));
            
    $('#resultado').html(htmlResult);
}

$(document).ready(function ()
{
    
    if($("#modalidade").val() == "1")
    {
        mostra_juros();

        verifica_dados();
    }
    
    
    /////////////////////////////////////////////////////////////////////
    // PARTE DO SISTEMA QUE DESCOBRE OS JUROS DO CET
    /////////////////////////////////////////////////////////////////////
//    jurosCET = calculaJurosCET(vnum_mes, vjuranox_fin, vjur, primeiraParcela, data_contrato, proxData, vvlr_prin, difAno);
    
//    jurosCET = calculaJurosCET('12', '0', '0', '07/09/2016', '02/08/2016', '', '1000', '', '125.22');

    //console.log(jurosCET);
    
});