
$(function () {

    $('#filiais_select').multiselect({
        buttonWidth: '300px',
        numberDisplayed: 2,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        buttonText: function (options, select) {

            if (options.length == 0) {
                return 'Selecionar Parceiros <b class="caret"></b>'
            } else if (options.length == 1) {
                return '1 Parceiro Selecionado <b class="caret"></b>'
            } else {
                return options.length + ' Parceiros Selecionados <b class="caret"></b>'
            }
        },
    });

    $('#crediaristas_select').multiselect({
        buttonWidth: '300px',
        numberDisplayed: 2,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true
    });

    var dados = [
        {label: "ACNP", value: "ACNP"},
        {label: "test", value: "test"}
    ];

    var requestCrediaristas = $.ajax({

        url         : '/administradorDeParceiros/getCrediaristas/',
        type        : 'POST',
        data        : {
            filiais : $("#filiais_select").val()
        }

    });

    requestCrediaristas.done(function(drt){

        var retorno = $.parseJSON(drt);

        console.log( retorno );
    });

    //$("#crediaristas_select").multiselect('dataprovider', dados);

    var tableAtrasos = $('#grid_seguros').DataTable({
        "processing": true,
        "serverSide": true,
        "cache": true,
        "ajax": {
            url: '/seguro/seguroFiliais/',
            type: 'POST',
            "data": function (d) {
                d.filiais   = $("#filiais_select").val(),
                d.data_de   = $("#data_de").val(),
                d.data_ate  = $("#data_ate").val()
            },
        },
        "language": {
            "processing": "<img style='position:fixed; top:60%; left:50%;margin-top:-8px;margin-left:-8px;' src='https://beta.sigacbr.com.br/js/loading.gif'>"
        },
        "columnDefs": [{
                "orderable": false,
                "targets": "no-orderable"
            }],
        "columns": [
            {"data": "codigo"},
            {"data": "filial"},
            {"data": "data"},
            {"data": "crediarista"},
            {"data": "valor_da_proposta"},
            {"data": "valor_do_seguro"},
            {"data": "valor_financiado"},
        ],
        "drawCallback" : function(settings){
            $('#totalSolicitado').html('R$ ' + settings.json.customReturn.totalSolicitado );
            $('#totalSeguro').html('R$ ' + settings.json.customReturn.totalSeguro );
            $('#totalFinanciado').html('R$ ' + settings.json.customReturn.totalFinanciado );
        }
    });

    $('#btn-filter').on('click', function () {
        tableAtrasos.draw();
        return false;
    });

});