$(document).ready(function () {

    $(document).on('click', '.btn_remove_par',function(){
        
        var tr         = $(this).closest('tr');
        var row        = parametrosTable.row(tr);
        var rowData    = row.data();
        var id  = rowData.id;

        $.ajax({
            type: "POST",
            url: "/listaNegra/excluirParametro/",
            data: {
                "id_par" : id,
            },
        }).done(function (dRt){

            var retorno = $.parseJSON(dRt);

            if(!retorno.hasErrors){
                location.reload(true);
            }else{
                $.pnotify(
                    {   
                        title: 'Informações',
                        text: retorno.msg,
                        type: retorno.pntfyClass
                    }
                );
            }
        });
    });

    $(document).on('dblclick', '.tdValor', function(){

        var nomeParametro = $(this).context.parentElement.cells[0].textContent;
        var conteudoOriginal = $(this).text();
        
        var elemento = $(this);
        $(this).html("<input class='form-control' type='texte' value='' />");
        $(this).children().first().focus();

        $(this).children().first().keypress(function (e) {

            if (e.which == 13) {

                var novoValor = $(this).val();

                $.ajax({
                    type: "POST",
                    url: "/listaNegra/mudarValor/",
                    data: {
                        "nomeParametro" : nomeParametro,
                        "valor"         : novoValor,
                    },
                }).done(function (dRt)
                {

                    var retorno = $.parseJSON(dRt);

                    elemento.text(novoValor);

                    $.pnotify(
                            {   
                                title: 'Informações',
                                text: retorno.msg,
                                type: retorno.pntfyClass
                            }
                    );

                });

                $(this).parent().text(conteudoOriginal);

            }
        });

        //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
        $(this).children().first().blur(function () {

            //devolva o conteúdo original
            $(this).parent().text(conteudoOriginal);

        });

    });

    var parametrosTable = $('#grid_parametros').DataTable({
        //"processing": true,
        //"serverSide": true,
        "ajax":
                {
                    url: '/listaNegra/listarparametros',
                    type: 'POST',
                },
        "columns": [
            {
                "data": "parametro"
            },
            {
                "data": "valor",
                "className": "tdValor"
            },
            {
                "data": "tipo",
            },
            {
                "data": "data_cadastro",
            },
            {
                "data": "descricao",
            },
            {
                "data": "empty",
            },
        ],
    });

    $('#btnAddParametro').on('click', function(){
        $.ajax({
            type: 'POST',
            url: '/listaNegra/incluirconfig',
            data: {
                "config_par"        : $('#config_parametro').val()  ,
                "config_val"        : $('#config_valor').val()      ,
                "config_tipo"       : $('#config_tipo').val()       ,
                "config_data"       : $('#config_data').val()       ,
                "config_descricao"  : $('#config_descricao').val()  ,
            },
        }).done(function(dRt){
            var retorno = $.parseJSON(dRt);

            parametrosTable.draw();
            
            if(retorno.hasError){
                $.pnotify({
                    title   : 'Erros foram encontrados',
                    text    : retorno.msg,
                    type    : 'error'
                });
            }else{



                $.pnotify({
                    title   : 'Concluído',
                    text    : retorno.msg,
                    type    : 'success'
                });
            }
        });
    });
});