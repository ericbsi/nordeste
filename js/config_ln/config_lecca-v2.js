$(document).ready(function () {

    $.ajax({
        url: '/listaNegra/nucleos/',
        type: 'post'
    }).done(function (dRt) {
        var jsonReturn = $.parseJSON(dRt);

        $('#selectNucleoFiliais').select2({
            data: jsonReturn,
            placeholder: "Núcleo..."
        });
    });


    $("#cod_lojista").mask("99999");
    $("#cod_tabJuros").mask("999");
    $("#cod_tab15").mask("999");
    $("#cod_tab30").mask("999");
    $("#cod_tab45").mask("999");
    $("#cod_tabJuros90").mask("999");
    $("#cod_tabPromo").mask("999");

    $('#selectNucleoFiliais').on('change', function () {
        $('.parametos').show('slow');

        $.ajax({
            url: '/listaNegra/getParametrosLecca/',
            type: 'post',
            data: {
                nucleo: $(this).val()
            }
        }).done(function (dRt) {
            var retorno = $.parseJSON(dRt);
            $("#cod_lojista").val(retorno.lojista);
            $("#cod_tabJuros").val(retorno.tjuros);
            $("#cod_tab15").val(retorno.t15);
            $("#cod_tab30").val(retorno.t30);
            $("#cod_tab45").val(retorno.t45);
            $("#cod_tabJuros90").val(retorno.t90);
            $("#cod_tabPromo").val(retorno.tpr);
        });
    });

    $('#salvar_config').on('click', function () {
        $.ajax({
            url: '/listaNegra/salvarConfigLecca/',
            type: 'post',
            data: {
                nucleo : $("#selectNucleoFiliais").val(),
                clojista : $("#cod_lojista").val(),
                tjuros : $("#cod_tabJuros").val(),
                tjuros90 : $("#cod_tabJuros90").val(),
                tpromo : $("#cod_tabPromo").val(),
                t15 : $("#cod_tab15").val(),
                t30 : $("#cod_tab30").val(),
                t45 : $("#cod_tab45").val()
            }
        }).done(function (dRt) {
            var retorno = $.parseJSON(dRt);

            $.pnotify(
                    {
                        title: 'Informações',
                        text: retorno.msg,
                        type: retorno.tipo
                    }
            );
        });
    });

});