 $(document).ready(function() {

   $('#atividade_primaria_select').multiselect({
      
      buttonWidth: '520px',

      onChange:function(element, checked){

         var request = $.ajax({
            type  : "GET",
            url   : "/filial/changeAtividadePrincipal/",
            data  : { id:element[0]['value'], filialId: $('#filialId').val() }
         })

         request.done(function(response){
            console.log (response)
         })

      }
   });

   $('#atividades_secundarias_select').multiselect({

      buttonWidth: '520px',   
      numberDisplayed:1,
      /*includeSelectAllOption: true,
      selectAllText: 'Selecionar todos',*/
      
      buttonText:function(options, select){

         if (options.length == 0) {
            return 'Selecionar Atividades <b class="caret"></b>'
         }
         else if (options.length == 1) {
            return '1 Atividade Selecionada <b class="caret"></b>'
         }
         else{
            return options.length + ' Atividades Selecionadas <b class="caret"></b>'
         }
      },

      onChange:function(element, checked){

         var request = $.ajax({
            type  : "GET",
            url   : "/filial/changeAtividadeSecundaria/",
            data  : { id:element[0]['value'], check:checked, filialId: $('#filialId').val() }
         });

         /*request.done( function(response){
            console.log (response)
         })*/
      }
   });

  });