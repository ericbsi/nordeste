$(function () {

   $.fn.totais = function (json) {

      return this.each(function () {

         $('#tfoot-total').html(json.customReturn.totalFin);
         
      });
   }

    $('#filiais_select').multiselect({
        buttonWidth: '200px',
        numberDisplayed: 2,
        buttonText: function (options, select) {

            if (options.length == 0) {
                return 'Selecionar Filiais <b class="caret"></b>'
            }
            else if (options.length == 1) {
                return '1 Filial Selecionada <b class="caret"></b>'
            }
            else {
                return options.length + ' Filiais Selecionadas <b class="caret"></b>'
            }
        },
    });

   var gridProducao = $('#grid_producao').DataTable({
      "sDom": 'T<"clear">lfrtip',
      "tableTools":
              {
                 "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
              },
      "processing": true,
      "serverSide": true,
      "ajax": {
         url: '/reports/reportProducaoOmni/',
         type: 'POST',
         "data": function (d) {
            d.dataDe    = $("#data_de"          ).val(),
            d.dataAte   = $("#data_ate"         ).val(),
            d.filiais   = $("#filiais_select"   ).val()
         },
      },
      "columnDefs": [{
            "orderable": false,
            "targets": "no-orderable"
         }],
      "columns": [
         {"data": "emissao"         },
         {"data": "codigo"          },
         {"data": "cliente"         },
         {"data": "val_financiado"  },
         {"data": "nomeLoja"        }
      ],
      "drawCallback": function (settings) {
         $(document).totais(settings.json);
      }

   });

   $('#btn-filter').on('click', function () {
      gridProducao.draw();
      return false;
   });

});