$(function(){

	var oTable = $('#sample_1').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sServerMethod": "GET",
        "sAjaxSource": "https://sigac/financeiro/arquivosRemessas",
        "iDisplayLength": 2,
        "aaSorting": [[1, 'asc']],
        "aLengthMenu": [
            [2, 4, 6, -1],
            [2, 4, 6, "Todos"]
        ],

        "bStateSave": true,
        "iCookieDuration": 60*60*24,
        "aoColumnDefs": [{
            "aTargets": [0]
        }],


        "oLanguage": {
             "sProcessing": "<img src='https://beta.sigacbr.com.br/js/loading.gif'>",
            "sLengthMenu": "Exibir _MENU_ Registros",
            "sInfo":"Exibindo _START_ a _END_ de _TOTAL_ registros",
            "sSearch": "",
            "oPaginate": {
                "sPrevious": "",
                "sNext": ""
            }
        },
    });

	$('#sample_1_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Pesquisa avançada");    
    $('#sample_1_wrapper .dataTables_length select').addClass("m-wrap small");
    $('#sample_1_wrapper .dataTables_length select').select2();
    $('#sample_1_column_toggler input[type="checkbox"]').change(function () {

        var iCol = parseInt($(this).attr("data-column"));
        var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
        oTable.fnSetColumnVis(iCol, (bVis ? false : true));

    });

    $('#btn-create-remessa').on('click',function(){

        $.ajax({
            type  : "GET",            
            url   : "https://sigac/boleto/gerarRemessa",

            beforeSend  : function(){
                $('#span-loading').html('<img id="img-loading" src="https://beta.sigacbr.com.br/js/loading.gif">')
            },

            success : function ( data ) {

                if ( $('#btn-create-remessa').is('disabled') != true ) {
                    $('#btn-create-remessa').attr('disabled','disabled')                    
                    $('#msg-remessa-gerada').removeClass('alert-warning');
                    $('#msg-remessa-gerada').addClass('alert-success');
                    $('#msg-remessa-gerada').html('<i id="ico_ok" class="fa fa-check-circle"></i> <strong>Feito!<strong> Arquivo de remessa já gerado hoje.');
                    $('#span-loading').html('')
                    oTable.fnDraw();
                }
            }
        })
    })

    /*function updateTable(){
        oTable.fnDraw();
        setTimeout(updateTable, 2000);
    }

    updateTable();*/

})