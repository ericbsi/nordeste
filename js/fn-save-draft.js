$(document).on('click','.next-step',function(){
	
	$.ajax({

		type 	: "POST",
        url 	: "/analiseDeCredito/saveDraft/",
        data 	: $('.draft_sent').serialize(),

        beforeSend : function(){

			$('.panel').block({
		        
		        overlayCSS: {
		        	backgroundColor: '#fff'
		        },
		        
		        message: '<img src="https://beta.sigacbr.com.br/js/loading.gif" /> Salvando rascunho...',
				css: {
		            border: 'none',
		            color: '#333',
					background: 'none'
		        }
		    });
        }
        
	}).done( function( data ){
		$('.panel').unblock();
	})

	//alert ( $('.draft_sent').serialize() )

})