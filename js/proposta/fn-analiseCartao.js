$(document).ready(function(){
    
    var inputIdProposta = $('#input_proposta_id'    );
    var inputValor      = $('#inputLimiteAprovado'  );
    var selectDia       = $('#selectDiaVencimento'  );
    var btnLimite       = $('#btnAprovarLimite'     );
    
    var urlAdmin    = "https://nordeste.sigacbr.com.br/proposta/admin";
    
    if (window.location.host == "nordeste")
    {
        urlAdmin = "http://nordeste/proposta/admin"   ;
    }
    
    $('.select2').select2();
    
    $('.limiteCartao').on('change', function ()
    {

        habilitaBtnLimite();

    });
    
    $('.limiteCartao').on('blur', function ()
    {

        habilitaBtnLimite();

    });
    
    $('.limiteCartao').on('keyup', function ()
    {

        habilitaBtnLimite();

    });
    
    $('.limiteCartao').on('click', function ()
    {

        habilitaBtnLimite();

    });
    
    function habilitaBtnLimite()
    {

        if (inputValor.val() !== "" && inputValor.val() > 0)
        {
            btnLimite.removeAttr("disabled");
        } else
        {
            btnLimite.attr("disabled", "disbled");
        }
        
    }
    
    btnLimite.on('click', function()
    {
        
        console.log(inputValor.val());
        console.log(inputValor.attr("value"));
        
        $.ajax({

            type                    : 'POST',
            url                     : '/cartao/aprovarProposta/',
            data                    : 
            {
                'idProposta'    : inputIdProposta.val() ,
                'valorLimite'   : inputValor.val()      
            }
        })
        .done( function( dRt ){
            
            var retorno = $.parseJSON(dRt);
    
            console.log(retorno);
            
            $.pnotify({
                title   : retorno.mensagem.title    ,
                text    : retorno.mensagem.msg      ,
                type    : retorno.mensagem.type
            });
            
            if(!retorno.hasErrors)
            {
                
                window.setTimeout(function () {
                    window.location = urlAdmin;
                }, 1500);
                
            }
            
        });

    });
    
});