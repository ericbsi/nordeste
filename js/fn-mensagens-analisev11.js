$(document).ready(function ()
{

    $(document).on('click', '.btn-proposta-more-details', function () {
        $('#iframe_mais_detalhes_proposta').attr('src', $(this).attr('modal-iframe-uri'));
    });
    
    $(document).on('click', '.btn-proposta-conversa', function () {
        $('#iframe_mais_conversa_proposta').attr('src', $(this).attr('modal-iframe-uri'));
    });
    
    var tablePropostas = $('#tableHistorico').DataTable(
            {
                "processing": true,
                "serverSide": true,
                "ajax":
                        {
                            url: '/cliente/historicoPropostas/',
                            type: 'POST',
                            "data": function (d)
                            {
                                d.cpf = $("#input_cpf_historico").val()
                            }
                        },
                "columns":
                        [
                            {"data": "conversa"},
                            {"data": "codigo"},
                            {"data": "data_cadastro"},
                            {"data": "parceiro"},
                            {"data": "crediarista"},
                            {"data": "valor"},
                            {"data": "entrada"},
                            {"data": "seguro"},
                            {"data": "valor_final"},
                            {"data": "qtd_parc"},
                            {"data": "status"},
                            {"data": "detalhes"}
                        ]
            });

    $('#btn_cliente_alert').on('click', function ()
    {
        $('#modal_cliente_alert').modal('hide');
    })

    var tableAnexos = $('#grid_mensagens').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/proposta/getMensagens/',
            type: 'POST',
            "data": {
                propostaId: $("#input_proposta_id").val()
            }
        },
        "columns": [
            //{"data": "assunto"},
            {"data": "conteudo"},
            //{"data": "remetente"},
            //{"data": "lida"},
            {"data": "dataHora"},
            //{"data": "btn_more"},
            //{"data": "btn_edit"},
            //{"data": "btn_del"},
        ],
        "drawCallback": function (settings) {
            setTimeout(function () {
                tableAnexos.draw();
            }, 30000);
        }
    });

    var tableDetalhesFinanProp = $('#table-detalhes-financeiros').DataTable({
        "processing": true,
        "serverSide": true,
        "info": false,
        "ajax": {
            url: '/proposta/getDetalhesFinanceiros/',
            type: 'POST',
            "data": function (d) {
                d.propostaId = $("#input_proposta_id").val()
            }
        },
        "columns": [
            {"data": "val_inicial"},
            {"data": "val_entrada", "class": "td-entrada"},
            {"data": "seguro"},
            {"data": "carencia"},
            {"data": "val_financiado"},
            {"data": "parcelamento"},
            {"data": "val_final"},
            {"data": "data_primeira_parcela"},
            {"data": "data_ultima_parcela"},
            {"data": "btn"},
        ],
        "columnDefs": [{
                "orderable": false,
                "targets": "no-orderable"
            }],
    });
    
    var grid_anexos2       = $('#grid_anexos').DataTable({

        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/cliente/getAnexos/',
            type: 'POST',
            "data": function (d) {
                d.clienteId = $('#cliente_id').val();
                d.canDelete = '0';
            }
        },

        "columns": [
            {"data": "descricao"},
            {"data": "ext"},
            {"data": "data_envio"},
            {"data": "btn"},
        ],
    });
    
    var grid_anexos = $('#grid_anexos_contratos').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/cliente/getAnexos/',
            type: 'POST',
            "data": function (d) {
                d.clienteId = $('#cliente_id').val(),
                        d.canDelete = '0'
            }
        },
        "columns": [
            {"data": "descricao"},
            {"data": "ext"},
            {"data": "data_envio"},
            {"data": "btn"},
        ],
    });

    $(document).on('click', '.btn-update-detalhes-financeiros', function () {
        tableDetalhesFinanProp.draw();
        return false;
    })

    $(document).on('click', '.td-entrada', function () {
        $('#modal-edit-entrada').modal('show');
    });

    $('#btn-update-entrada').on('click', function () {

        $.ajax({
            url: "/proposta/alterarEntrada/",
            type: "POST",
            data: {
                'propostaId': $('#propId').val(),
                'entrada': $('#entrada').val()
            },
            success: function (data) {
                $('#modal-edit-entrada').modal('hide');
                tableDetalhesFinanProp.draw();
            }

        });
        return false;
    });

    $("#table-prestacoes tbody tr .td-select-parcela").on('click', function () {

        if (!$("#resumo-solicitacao").is(":visible")) {
            $("#resumo-solicitacao").show();
        }

        $(".td-select-parcela-selected").each(function () {

            $(this).removeClass("td-select-parcela-selected")

        })

        if ($(this).hasClass("td-select-parcela-selected")) {

            $(this).removeClass("td-select-parcela-selected");

        } else {

            $(this).addClass("td-select-parcela-selected")
        }


        $.ajax({
            type: "POST",
            url: "/proposta/alterarCondicoes/",
            data: {
                'propostaId': $('#propId').val(),
                'cotacao_id': $(this).attr('cotacao-id'),
                'carencia': $(this).attr('carencia'),
                'numero_parcelas': $(this).attr('numero-parcelas'),
                'val_parcelas': $(this).attr('valor-parcela')
            },
            success: function (data) {
                tableDetalhesFinanProp.draw();
            }
        })
    })

    $('.limited').inputlimiter({
        remText: 'Você possui apenas %n caracteres%s restantes...',
        remFullText: 'Número máximo excedido!',
        limitText: 'Número máximo permitido: %n.'
    });

    consultarCliente();

    jQuery.fn.checkPropostaStatusChange = function () {

        return $.ajax({
            type: 'POST',
            url: '/proposta/statusChangeListener/',
            data: {
                'statusPropostaId': $('#input_proposta_status_id').val(),
                'PropostaId': $('#input_proposta_id').val()
            }
        })
                .done(function (dRt) {

                    var retorno = $.parseJSON(dRt);

                    $('#input_proposta_status_id').val(retorno.statusAtualId)

                    if (retorno.statusAtualId != 2)
                    {
                        if ($('#li-show-contrato:visible').length != 0)
                        {
                            $('#li-show-contrato').hide();
                            $('#panel_contrato').hide();
                        }

                        if ($('#li-show-boletos:visible').length != 0)
                        {
                            $('#li-show-boletos').hide();
                            $('#panel_boletos').hide();
                        }
                    } else
                    {
                        if ($('#li-show-contrato:visible').length == 0)
                        {
                            $('#li-show-contrato').show();
                            $('#panel_contrato').show();
                        }

                        if ($('#li-show-boletos:visible').length == 0)
                        {
                            $('#li-show-boletos').show();
                            $('#panel_boletos').show();
                        }
                    }

                    if (retorno.statusHasChanged == 1)
                    {

                        $.pnotify({
                            title: 'Mudança de Status',
                            text: 'O status da proposta foi alterado.',
                            type: 'success'
                        });

                        $('#span-status').removeClass($('#span-status').attr('class')).addClass(retorno.statusAttrs.cssClass);
                        $('#span-status').text(retorno.statusAttrs.status);
                    }
                })
    }

    jQuery.fn.checkAll = function () {

        return this.each(function () {

            $(".check_print").each(function () {

                var inputHdn = $('#iptn_hdn_parcela_' + $(this).val());

                if (inputHdn.length == 0)
                {

                    $('#form_titulos_segunda_via').append('<input id="iptn_hdn_parcela_' + $(this).val() + '" type="hidden" name="ParcelasIds[]" value="' + $(this).val() + '">');
                }
            })

            $(".check_print").prop('checked', true);
        })
    }

    jQuery.fn.unCheckAll = function () {

        return this.each(function () {

            $(".check_print").each(function () {

                var inputHdnId = 'iptn_hdn_parcela_' + $(this).val();

                if ($('#' + inputHdnId).length != 0)
                {
                    $('#' + inputHdnId).remove();
                }
            })

            $(".check_print").prop('checked', false);
        })

    }

    setInterval(function () {
        $(document).checkPropostaStatusChange();
    }, 15000);

    /*Regras de validação*/
    $.validator.setDefaults({
        errorElement: "span",
        errorClass: 'help-block',
        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group
        },
        success: function (label, element) {
            label.addClass('help-block valid');
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        }
    });

    $('#msg_int').on('click', function () {
        $('#msg_interna').val(1);
    });

    $('#msg_pad').on('click', function () {
        $('#msg_interna').val(0);
    });

    $('#form-send-msg').validate({
        submitHandler: function ()
        {

            var formData = $('#form-send-msg').serialize();

            $.ajax({
                type: 'POST',
                url: '/mensagem/add/',
                data: formData
            })

                    .done(function (dRt) {

                        var retorno = $.parseJSON(dRt);

                        $.pnotify({
                            title: 'Notificação',
                            text: retorno.msgReturn,
                            type: retorno.classNotify
                        });

                        tableAnexos.draw();
                        $('#modal_form_new_msg').modal('hide');
                    });
        }
    });

    $(document).on('click', '.btn-ler-msg', function () {

        $.ajax({
            type: 'POST',
            url: '/mensagem/read/',
            data: {
                'mensagemId': $(this).data('mensagem-id'),
                'usuarioId': $('#ipt_hdn_user_id').val()
            }

        })
                .done(function (dRt) {

                    var retorno = $.parseJSON(dRt);

                    $('#modal_msg_more').modal('show');

                    $('#textarea_mensagem_conteudo_ler').val(retorno.conteudo.value)

                    /*console.log(retorno.conteudo.value);*/
                })
        return false;
    });

    $('.check_print').on('change', function () {

        var ipthdnId = 'iptn_hdn_parcela_' + $(this).val();

        if (this.checked) {

            if ($('#' + ipthdnId).length == 0)
            {
                $('#form_titulos_segunda_via').append('<input id="' + ipthdnId + '" type="hidden" name="ParcelasIds[]" value="' + $(this).val() + '">');
            }
        } else
        {
            if ($('#' + ipthdnId).length != 0)
            {
                $('#' + ipthdnId).remove();
            }
        }
    });

    $('#check_select_all').on('change', function () {
        this.checked ? $(this).checkAll() : $(this).unCheckAll();
    });

    $('#btn-print-selects').on('click', function () {

        if ($('.check_print:checked').length == 0)
        {
            alert('Nenhuma parcela foi selecionada!')
        } else
        {
            $('#modal-confirm-print').modal('show')
        }

    });

    $('#form-add-soli-cancel').validate({
        submitHandler: function () {

            $.ajax({
                type: 'POST',
                url: '/solicitacaoDeCancelamento/add/',
                data: $('#form-add-soli-cancel').serialize()

            })
                    .done(function (drt) {

                        var retorno = $.parseJSON(drt);

                        $('#modal_form_new_soli_cancel').modal('hide');
                        $('#form-add-soli-cancel').trigger("reset");

                        $.pnotify({
                            title: 'Notificação',
                            text: retorno.msg,
                            type: retorno.pntfyClass
                        });

                        $('.has-success').each(function (index) {
                            $(this).removeClass('has-success');
                        });

                        $('.ok').each(function (index) {
                            $(this).removeClass('ok');
                        });

                        if (retorno.hasErrors != 1)
                        {
                            $('#btn-soli').removeClass('btn-pinterest').addClass('btn-warning').text('Uma solicitação de cancelamento está aguardando aprovação.');
                            $('#btn-soli').attr('href', '#');
                            $('#btn-soli').attr('data-toggle', '#');
                        }

                    });
        }

    });

    $('#form-anexos').ajaxForm({
        beforeSubmit: function () {

            //$('#Cliente_id_fup').val( $('#cliente_id').val() );

            if (window.File && window.FileReader && window.FileList && window.Blob && $('#form-anexos').valid())
            {
                var fsize = $('#FileInput')[0].files[0].size;
                var ftype = $('#FileInput')[0].files[0].type;

                switch (ftype)
                {
                    case 'image/png':
                    case 'image/gif':
                    case 'image/jpeg':
                    case 'image/pjpeg':
                    case 'application/pdf':
                        break;
                    default:
                        $('#cadastro_anexo_msg_return').html("Tipo de arquivo não permitido!").show();
                        $('#cadastro_anexo_msg_return').fadeOut(4000);
                        return false;
                }

                if (fsize > 5242880)
                {
                    $('#cadastro_anexo_msg_return').html("Arquivo muito grande!").show();
                    $('#cadastro_anexo_msg_return').fadeOut(4000);
                    return false
                }
            } else
            {
                alert("Revise o formulário!");
            }

        },
        uploadProgress: function (event, position, total, percentComplete) {

            $('#progressbox').show();
            $('#progressbar').width(percentComplete + '%') //update progressbar percent complete
            $('#statustxt').html(percentComplete + '%'); //update status text

            if (percentComplete > 50)
            {
                $('#statustxt').css('color', '#000'); //change status text to white after 50%
            }
        },
        success: function (response, textStatus, xhr, form) {

            var retorno = $.parseJSON(response);

            $.pnotify({
                title: 'Ação realizada com sucesso',
                text: 'O Arquivo foi enviado com sucesso!',
                type: 'success',
            });

            $('#progressbox').fadeOut(3000);

            grid_anexos.draw();

        },
        error: function (xhr, textStatus, errorThrown) {

        },
        resetForm: true
    });


    $('.editable').editable({
        success: function () {
            tableDetalhesFinanProp.draw();
        }

    });

    $.fn.editable.defaults.mode = 'popup';

    $('.editable').on('shown', function () {
        $(this).data('editable').input.$input.maskMoney({
            decimal: ',',
            thousands: '.'
        });
    });


    $('#btnConsultarCliente').on('click', function ()
    {
        consultarCliente();
    })

    function consultarCliente()
    {

        var idProposta = $('#idProposta').val();
        var cpfConsultar = $('#cpfConsultar').text();

        var elemento = $('#btnConsultarCliente');

        elemento.attr('disabled', 'disabled');

        if (idProposta == "1")
        {

            $.ajax(
                    {
                        type: 'POST',
                        url: '/proposta/consultarCliente/',
                        data:
                                {
                                    cgc: cpfConsultar
                                }

                    }).done(function (dRt)
            {

                var retorno = $.parseJSON(dRt);

                console.log(retorno);


                $('#restricao').text(retorno.restricao);
                $('#protocolo').text(retorno.protocolo);
                $('#operador').text(retorno.operador);
                $('#nomeConsumidor').text(retorno.nomeConsumidor);
                $('#cpfConsumidor').text(retorno.cpfConsumidor);
                $('#nascimentoConsumidor').text(retorno.nascimentoConsumidor);
                $('#maeConsumidor').text(retorno.maeConsumidor);

                $('#qtdTelefoneVinculado').text(retorno.qtdTelefoneVinculado);
                $('#ultTelefoneVinculado').text(retorno.ultTelefoneVinculado);
                $('#vlrTelefoneVinculado').text(retorno.vlrTelefoneVinculado);

                $('#logradouroConsumidor').text(retorno.logradouroConsumidor);
                $('#numEndConsumidor').text(retorno.numEndConsumidor);
                $('#bairroConsumidor').text(retorno.bairroConsumidor);
                $('#comEndConsumidor').text(retorno.comEndConsumidor);
                $('#cidadeConsumidor').text(retorno.cidadeConsumidor);
                $('#ufConsumidor').text(retorno.ufConsumidor);
                $('#cepConsumidor').text(retorno.cepConsumidor);

                $('#qtdRegistroSPC').text(retorno.qtdRegistroSPC);
                $('#ultRegistroSPC').text(retorno.ultRegistroSPC);
                $('#vlrRegistroSPC').text(retorno.vlrRegistroSPC);

                $('#qtdRegistroSerasa').text(retorno.qtdRegistroSerasa);
                $('#ultRegistroSerasa').text(retorno.ultRegistroSerasa);
                $('#vlrRegistroSerasa').text(retorno.vlrRegistroSerasa);

                $('#qtdPoderJudiciario').text(retorno.qtdPoderJudiciario);
                $('#ultPoderJudiciario').text(retorno.ultPoderJudiciario);
                $('#vlrPoderJudiciario').text(retorno.vlrPoderJudiciario);

                $('#qtdConsultaRealizada').text(retorno.qtdConsultaRealizada);
                $('#ultConsultaRealizada').text(retorno.ultConsultaRealizada);
                $('#vlrConsultaRealizada').text(retorno.vlrConsultaRealizada);

                $('#qtdAlertaDocumentos').text(retorno.qtdAlertaDocumentos);
                $('#ultAlertaDocumentos').text(retorno.ultAlertaDocumentos);
                $('#vlrAlertaDocumentos').text(retorno.vlrAlertaDocumentos);

                if ($.trim(retorno.HTMLSPC) !== '')
                {
                    $('#HTMLSPC').html(retorno.HTMLSPC);
                }

                if ($.trim(retorno.HTMLConsulta) !== '')
                {
                    $('#HTMLConsulta').html(retorno.HTMLConsulta);
                }

                if ($.trim(retorno.HTMLSerasa) !== '')
                {
                    $('#HTMLSerasa').html(retorno.HTMLSerasa);
                }

                elemento.removeAttr('disabled');

            });

        }

    }

    $(document).on('click', '#consulta_score_btn', function () {
        $.ajax(
        {
            type: 'POST',
            url: '/cliente/consultarScore/',
            data:
            {
                "cpf_cl": $("#cpf_cliente_score").val(),
                "id_cl": $("#id_cliente_score").val()
            }

        }).done(function (dRt){

            var retorno = $.parseJSON(dRt);

            $('#score_consulta').show('slow');
            $('#score_consulta').addClass(retorno.data.css);
            $('#consulta_classe').text(retorno.data.classe);
            $('#consulta_risco').text(retorno.data.risco);
            $('#consulta_socore').text(retorno.data.score);
            $('#consulta_rest').text(retorno.data.rest);
        });
    });

    var gridDadosSociais        = $('#grid_dados_sociais').DataTable({

        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/cliente/getDadosSociais/',
            type: 'POST',
            "data": function (d) {
                d.clienteId = $("#cliente_id").val()
            }
        },
        "columns": [{
            "data": "nome_da_mae"
        }, {
            "data": "nome_do_pai"
        }, {
            "data": "numero_de_dependentes"
        }, {
            "data": "titular_cpf"
        }, {
            "data": "estado_civil"
        }, {
            "data": "conjCompRenda"
        }, {
            "data": "btn"
        }, ],

        "sorting": [
            [0]
        ],
        "columnDefs": [{
            "orderable": false,
            "targets": "no-orderable"
        }],
    });

    $(document).on('click', '.btn-load-form', function () {
        $(this).loadForm($(this).data('url-request'), $(this).data('entity-id'));
        return false;
    })

    $(document).on('click', '.btn-send', function () {
        $(this).sendForm($(this).data('url-request'), $(this).data('form-id'), $(this).data('table-redraw'), $(this).data('div-return-id'), $(this).data('checkbox-continuar'), $(this).data('modal-id'));
    })

    $(document).on('click', '.btn-add, .btn-update', function () {
        
        var ipt = $('#'+$(this).data('input-controller-action'));
        ipt.attr('value',$(this).data('controller-action'));
        return false;
    })

    $(document).on('click','.btn-remove-anexo', function(){

        var anexoId = $(this).data('anexo-id');
        $('#ipt-hdn-id-anexo').attr('value', anexoId);
        return false;

    })
    
    /*Formulários de edição*/
    jQuery.fn.loadForm          = function ( url, entityId ) {

        return this.each(function () {
            $.ajax({

                type: "GET",
                
                url: url,
                
                data: {
                    entity_id: entityId
                },

                beforeSend  : function(){

                    $('.panel2').block({
                        overlayCSS: {
                                backgroundColor: '#fff'
                        },
                        message: '<img src="/images/loading.gif" /> Carregando informações...',
                        css: {
                                border: 'none',
                                color: '#333',
                                background: 'none'
                        }
                    });
                        
                    window.setTimeout(function () {
                        $('.panel2').unblock();
                    }, 1000);        
                },
            })

            .done(function (dRt) {

                var paramsReturn = $.parseJSON(dRt);

                $.each(paramsReturn.fields, function (key, value) {

                    if (value.type != 'select')
                    {
                        $('#' + value.input_bind).val(value.value)
                    }

                    else
                    {   
                        if( value.value != null ){
                            $('#' + value.input_bind).select2("val", value.value);
                        }
                    }
                });

                $('#' + paramsReturn.modal_id).find('.btn-send').attr('data-url-request', paramsReturn.form_params.action);

                $('#' + paramsReturn.modal_id).modal('show');
            });
        });
    };

    /*Send form*/
    jQuery.fn.sendForm          = function( url, form, tableRedraw, divReturn, checkboxContinuar, modal_id ){

        return this.each(function(){

            $('#'+form).validate({

                submitHandler : function(){

                    $('#'+divReturn).hide().empty();
                    
                    var formData = $('#'+form).serialize();

                    $.ajax({
                        
                        type    : "POST",
                        url     : url,
                        data    : formData,

                        beforeSend  : function(){

                            $('.panel').block({
                                overlayCSS: {
                                    backgroundColor: '#fff'
                                },
                                message: '<img src="https://beta.sigacbr.com.br/js/loading.gif" /> Salvando informações...',
                                css: {
                                    border: 'none',
                                    color: '#333',
                                    background: 'none'
                                }
                            });
                            
                            window.setTimeout(function () {
                                $('.panel').unblock();
                            }, 1000);
                        },
                    })
                    .done(function(dRt){

                        //console.log(dRt);

                        var retorno = $.parseJSON(dRt);

                        if( !$('#'+checkboxContinuar).is(':checked') )
                        {
                            $('#'+modal_id).modal('hide');
                        }
                        
                        $('.has-success').each(function(index){
                            $(this).removeClass('has-success');
                        })

                        $('.ok').each(function(index){
                            $(this).removeClass('ok');
                        })

                        $('#'+tableRedraw).DataTable().draw();

                        $('#'+divReturn).prepend('<p>'+retorno.msgReturn+'</p>').fadeIn(100).addClass('alert-success').fadeOut(9000);

                        $.pnotify.defaults.history = false;

                        if( !$('#'+checkboxContinuar).is(':checked') )
                        {
                            $.pnotify({
                                title    : 'Notificação',
                                text     : retorno.msgReturn,
                                type     : 'success'
                            });
                        }

                        $('#'+form).trigger("reset");
                    })
                }
            });            
        })
    };
    
    $('.search-select').select2();
    
    var tableFamiliares         = $('#grid_familiares').DataTable({

        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/cliente/getFamiliares/',
            type: 'POST',
            "data": function (d) {
                d.clienteId = $("#cliente_id").val()
            }
        },
        "sorting": [
            [0]
        ],
        "columns": [
            {"data": "nome"},
            {"data": "relacao"},
            {"data": "btn"},
        ],
    });
    
    var tableTelefones          = $('#grid_telefones').DataTable({

        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/cliente/getTelefones/',
            type: 'POST',
            "data": function (d) {
                d.clienteId = $("#cliente_id").val()
            }
        },
        "sorting": [
            [0]
        ],
        "columns": [{
            "data": "numero"
        }, {
            "data": "tipo"
        }, {
            "data": "ramal"
        }, {
            "data": "btn"
        }, ],
    });
    
    var tableDadosProfissionais = $('#grid_dados_profissionais_cliente').DataTable({
        "scrollX": true,
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/cliente/getDadosProfissionais/',
            type: 'POST',
            "data": function (d) {
                d.clienteId = $("#cliente_id").val()
            }
        },
        "autoWidth": false,
        "sorting": [
            [0]
        ],
        "columnDefs": [{
            "orderable": false,
            "targets": "no-orderable",
        }],

        "columns": [
            {
                "data" : "principal",
            },
            {
                "data" : "empresa",
            },
            {
                "data": "cpf_cnpj"
            },
            {
                "data": "data_de_admissao"
            },
            {
                "data": "ocupacao"
            }, 
            {
                "data": "classe_profissional"
            },
            {
                "data": "renda_liquida"
            }, 
            {
                "data": "mes_ano_renda"
            }, 
            {
                "data": "profissao"
            }, 
            {
                "data": "aposentado"
            }, 
            {
                "data": "pensionista"
            }, 
            {
                "data": "tipo_de_comprovante"
            }, 
            {
                "data": "numero_do_beneficio"
            },
            {
                "data": "orgao_pagador"
            }, 
            {
                "data": "cep"
            },
            {
                "data": "logradouro"
            }, 
            {
                "data": "bairro"
            }, 
            {
                "data": "cidade"
            }, 
            {
                "data": "estado"
            }, 
            {
                "data": "telefone"
            }, 
            {
                "data": "ramal"
            }, 
            {
                "data": "email"
            }, 
            {
                "data": "btn"
            },
        ],
    });
    
    var tableDadosBancarios     = $('#grid_dados_bancarios_cliente').DataTable({

        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/cliente/getDadosBancarios/',
            type: 'POST',
            "data": function (d) {
                d.clienteId = $("#cliente_id").val()
            }
        },
        "sorting": [
            [0]
        ],
        "columns": [{
            "data": "banco"
        }, {
            "data": "agencia"
        }, {
            "data": "conta"
        }, {
            "data": "tipo"
        }, {
            "data": "operacao"
        }, {
            "data": "btn"
        }, ],
    });
    
    var tableEnderecos          = $('#grid_enderecos_cliente').DataTable({

        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/cliente/getEnderecos/',
            type: 'POST',
            "data": function (d) {
                d.clienteId = $("#cliente_id").val()
            }
        },
        "sorting": [
            [0]
        ],
        "columns": [{
            "data": "logradouro"
        }, {
            "data": "bairro"
        }, {
            "data": "numero"
        }, {
            "data": "cidade"
        }, {
            "data": "cep"
        }, {
            "data": "uf"
        }, {
            "data": "complemento"
        },{
            "data": "cob"
        },{
            "data": "btn"
        }],
    });
    
    var tableReferencias        = $('#grid_referencias_cliente').DataTable({

        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/cliente/getReferencias/',
            type: 'POST',
            "data": function (d) {
                d.clienteId = $("#cliente_id").val()
            }
        },
        "sorting": [
            [0]
        ],
        "columns": [
            {"data": "parentesco"},
            {"data": "nome"},
            {"data": "numero"},
            {"data": "tipo"},
            {"data": "btn"}
        ],
    });
    
     $(document).on('change','.negacao', function () {
        console.log($('#MotivoDeNegacao').val());
        console.log($('#obs').val());
        if ($('#MotivoDeNegacao').val() > '0' && $('#obs').val() !== '') {
            $('#recusar').removeAttr('disabled');
        } else {
            $('#recusar').attr('disabled','disabled');
        }
    });

})