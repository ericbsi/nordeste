$(document).ready(function () {

    var naturezasTable = $('#grid_naturezas').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax":
                {
                    url     :   '/naturezaTitulo/listar'    ,
                    type    :   'POST'                      ,
                    data    :   function(d)
                                {
                                    d.filtroPai         = $('#filtroPai'        ).val();
                                    d.filtroCodigo      = $('#filtroCodigo'     ).val();
                                    d.filtroDescricao   = $('#filtroDescricao'  ).val();
                                }
                },
        "columns": [
            {
                "data"      : "paiNatureza",
                "className" : "paiNatureza",
            },
            {
                "data"      : "codigoNatureza",
                "className" : "codigoNatureza",
            },
            {
                "data"      : "descricaoNatureza",
                "className" : "descricaoNatureza",
            },
            {   
                "data"      : "btnRemover"
            },
        ],
    });

    $('#btnAddNatureza').on('click', function () {

        $.ajax({
            type: "POST",
            url: "/naturezaTitulo/salvar/",
            data: {
                "paiNatureza"       : $('#paiNatureza'          ).val() ,
                "codigoNatureza"    : $('#codigoNatureza'       ).val() ,
                "descricaoNatureza" : $('#descricaoNatureza'    ).val() 
            }
        }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            $.pnotify({
                title: 'Notificação',
                text: retorno.msg,
                type: retorno.pntfyClass
            });

            if (!(retorno.hasErrors)) {
                
                $('#codigoNatureza'     ).val('')           ;
                $('#descricaoNatureza'  ).val('')           ;
                $('#paiNatureza'        ).val(0 ).change()  
                
                naturezasTable.draw();
            }

        })

    });

    $('.select2').select2();
    
    $('.filtroNatureza').on('change', function()
    {
       naturezasTable.draw();
    });
            
    $('.form-control').on('change',function()
    {
        
        if($.trim($('#codigoNatureza').val()) == "")
        {
            $('#btnAddNatureza').attr('disabled','disabled');
            return;
        }
        
        if($.trim($('#descricaoNatureza').val()) == "")
        {
            $('#btnAddNatureza').attr('disabled','disabled');
            return;
        }
        
        if($.trim($('#paiNatureza').val()) == "0")
        {
            $('#btnAddNatureza').attr('disabled','disabled');
            return;
        }
        
        $('#btnAddNatureza').removeAttr('disabled');
    });

    //////////////////////////////////////////////////////////////////////////
    // funcao que irá capturar o click duplo na célula de senha e irá       //
    // habilitar para edição da mesma. caso a mesma sofra alguma alteração, //
    // salve o conteúdo no banco                                            //
    //////////////////////////////////////////////////////////////////////////
    // Autor : André Willams // Data : 06-07-2015 ////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    $(document).on('dblclick', '.tdNome', function () 
    {

        var conteudoOriginal = $(this).text(); //resgate o conteúdo atual da célula

        var idGrupo = $(this).context.parentElement.cells[1].textContent; //pegue o id do Grupo

        //transforme o elemento em um input
        $(this).html("<input id='mudaNome' class='form-control' type='text' value='" + conteudoOriginal + "' />");

        $(this).children().first().focus(); //atribua o foco ao elemento criado

        $(this).children().first().keypress(function (e) { //quando alguma tecla for pressionada

            if (e.which == 13) { //caso a tecla pressionada seja o ENTER, foi confirmada a alteração

                var novoConteudo = $(this).val(); //pegue o novo conteúdo

                //chame o ajax de alteração
                $.ajax({
                    type: "POST",
                    url: "/grupoFiliais/mudarNome",
                    data: {
                        "idGrupo": idGrupo,
                        "nomeGrupo": novoConteudo
                    },
                }).done(function (dRt) {

                    var retorno = $.parseJSON(dRt);

                    $.pnotify({
                        title: 'Notificação',
                        text: retorno.msg,
                        type: retorno.pntfyClass
                    });

                })

                $(this).parent().text(novoConteudo);

            }
        });

        //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
        $(this).children().first().blur(function () {

            //devolva o conteúdo original
            $(this).parent().text(conteudoOriginal);

        });

    });

})