$(function(){

  var tableRecebimentos = $('#grid_recebidos').DataTable({
    "processing"    : true,
    "serverSide"    : true,
    "ajax"          :{
      url           : '/interno/listarRelatorio/',
      type          : 'POST',
      "data"  : function (d){
        d.nucleo      = $('#selectNucleoFiliais').val(),
        d.status      = $('#selectStatus').val(),
        d.financeira  = $('#selectFinanceiras').val(),
        d.filial      = $('#selectFiliais').val(),
        d.de          = $('#data_de').val(),
        d.ate         = $('#data_ate').val()
      },
    },
    "columns": [
        {"data": "detalhar", "class": "detalhar"},
        {"data": "filial"},
        {"data": "financeira"},
        {"data": "data"},
        {"data": "valor"},
        {"data": "anexo"}
    ],
    "drawCallback": function (settings) {
        $('#th-valor-total').html( "R$ "+ settings.json.customReturn.total );
    }
  });

  $('#filtrar_recebimentos').on('click', function(){
      tableRecebimentos.draw();
  });

  $.ajax({
      url: '/interno/filiais/',
      type: 'post'
  }).done(function (dRt) {
      var jsonReturn = $.parseJSON(dRt);

      $('#selectFiliais').select2({
          data: jsonReturn,
          placeholder: "Filial..."
      });
  });


  $.ajax({
      url: '/interno/nucleos/',
      type: 'post'
  }).done(function (dRt) {
      var jsonReturn = $.parseJSON(dRt);

      $('#selectNucleoFiliais').select2({
          data: jsonReturn,
          placeholder: "Núcleo..."
      });
  });

  $.ajax({
      url: '/interno/status/',
      type: 'post'
  }).done(function (dRt) {
      var jsonReturn = $.parseJSON(dRt);

      $('#selectStatus').select2({
          data: jsonReturn,
          placeholder: "Status..."
      });
  });

  $.ajax({
      url: '/interno/financeiras/',
      type: 'post'
  }).done(function (dRt) {
      var jsonReturn = $.parseJSON(dRt);

      $('#selectFinanceiras').select2({
          data: jsonReturn,
          placeholder: "Financeira..."
      });
  });

  $('#selectNucleoFiliais').on('change', function(){

    var nuc = $(this).val();

    $.ajax({
      url: '/interno/filiais/',
      type: 'post',
      data:{nucleo : nuc}

    }).done(function (dRt) {
      var jsonReturn = $.parseJSON(dRt);

      $('#selectFiliais').select2({
          data: jsonReturn,
          placeholder: "Filial..."
      });
    });
  });

  $('#grid_recebidos tbody').on('click', 'td.detalhar', function () {
      var tr = $(this).closest('tr');
      var row = tableRecebimentos.row(tr);
      var rowsTam = tableRecebimentos.rows()[0].length;
      var dt = $(this).parent().find('button').val();

      if ($(this).parent().find('button.detail').hasClass('btn-success')) {
          $(this).parent().find('button.detail').removeClass('btn-success');
          $(this).parent().find('button.detail').addClass('btn-danger');
      } else {
          $(this).parent().find('button.detail').addClass('btn-success');
          $(this).parent().find('button.detail').removeClass('btn-danger');
      }

      if (row.child.isShown()) {
          row.child.hide();
      } else {
          for (i = 0; i < rowsTam; i++)
          {

              if (tableRecebimentos.row(i).child.isShown()) {
                  tableRecebimentos.row(i).child.hide();
              }
          }

          SubParcelas(row.data(), tr, row, dt);
      }

      var tableRecDet = $('#grid_rec_detalhado').DataTable({
          "processing": true,
          "serverSide": false,
          "paging": false,
          "bFilter": false,
          "bInfo": false,
          "ajax": {
              type: 'POST',
              url: '/interno/detalharParcelas/',
              data: function (d) {
                  d.id_rec = dt;
              }
          },
          "columns": [
              {"data": "contrato"},
              {"data": "cliente"},
              {"data": "seq"},
              {"data": "venc"},
              {"data": "valor"},
              {"data": "valor_pago"}
          ]
      });
  });

  function SubParcelas(d, tr, row, dt) {
      var data = '<div class="col-sm-12" style="text-align: center;">' +
              '       <span style="font-size: 18px;">Recebimento Detalhado</span><br />' +
              '       <hr /> ' +
              '       <table id="grid_rec_detalhado" ' +
              '           class="table table-striped table-bordered table-hover table-full-width dataTable"' +
              '           style="width: 100%; margin-bottom: 25px!important;">' +
              '           <thead>' +
              '               <tr>' +
              '                   <th width="18%">Contrato</th>' +
              '                   <th>Cliente</th>' +
              '                   <th width="1%">Seq.</th>' +
              '                   <th width="5%">Vencimento</th>' +
              '                   <th width="10%">Valor</th>' +
              '                   <th width="10%">Valor Pago</th>' +
              '               </tr>' +
              '           </thead>' +
              '           <tbody>' +
              '           </tbody>' +
              '       </table>' +
              '   </div>';

      row.child(data).show();
  }

});
