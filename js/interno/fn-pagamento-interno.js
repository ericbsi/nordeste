$(document).ready(function () {

    var tableParcelas = $('#grid_parcelas').DataTable({
        "processing": true,
        "serverSide": false,
        "paging": false,
        "bFilter": false,
        "bInfo": false,
        "ajax": {
            type: 'POST',
            url: '/interno/listarParcelas/',
            data: function (d) {
                d.contrato = $('#contrato').val();
                d.cpf = $('#cpf_cliente').val();
            }
        },
        "columns": [
            {"data": "contrato"       },
            {"data": "cliente"        },
            {"data": "seq"            },
            {"data": "venc"           },
            {"data": "valor"          },
            {"data": "dias_atraso"    },
            {"data": "valor_atraso"   },
            {"data": "valor_receber"  },
            {"data": "comprovante"    }
        ]
    });

    var tableRecebimentos = $('#grid_recebidos').DataTable({
        "processing": true,
        "serverSide": false,
        "paging": false,
        "bFilter": false,
        "bInfo": false,
        "ajax": {
            type: 'POST',
            url: '/interno/listarRecebimentos/'
        },
        "columns": [
            {"data": "detalhar", "class":"detalhar"},
            {"data": "financeira"},
            {"data": "data"},
            {"data": "valor"},
            {"data": "boleto"},
            {"data": "anexar", class:"anexar_cpr"}
        ]
    });

    $('#aberto_btn').on('click', function(){
        $('.emAberto').show('slow');
        $('.recebidos').hide();
    });

    $('#recebido_btn').on('click', function () {
        $('.emAberto').hide();
        $('.recebidos').show('slow');
    });

    $('#filtrar_parcelas').on('click', function () {
        tableParcelas.ajax.reload();
    });

    $('#grid_recebidos tbody').on('click', 'td.detalhar', function () {
        var tr = $(this).closest('tr');
        var row = tableRecebimentos.row(tr);
        var rowsTam = tableRecebimentos.rows()[0].length;
        var dt = $(this).parent().find('button').val();

        if ($(this).parent().find('button.detail').hasClass('btn-success')) {
            $(this).parent().find('button.detail').removeClass('btn-success');
            $(this).parent().find('button.detail').addClass('btn-danger');
        } else {
            $(this).parent().find('button.detail').addClass('btn-success');
            $(this).parent().find('button.detail').removeClass('btn-danger');
        }

        if (row.child.isShown()) {
            row.child.hide();
        } else {
            for (i = 0; i < rowsTam; i++)
            {

                if (tableRecebimentos.row(i).child.isShown()) {
                    tableRecebimentos.row(i).child.hide();
                }
            }

            SubParcelas(row.data(), tr, row, dt);
        }

        var tableRecDet = $('#grid_rec_detalhado').DataTable({
            "processing": true,
            "serverSide": false,
            "paging": false,
            "bFilter": false,
            "bInfo": false,
            "ajax": {
                type: 'POST',
                url: '/interno/detalharParcelas/',
                data: function (d) {
                    d.id_rec = dt;
                }
            },
            "columns": [
                {"data": "contrato"},
                {"data": "cliente"},
                {"data": "seq"},
                {"data": "venc"},
                {"data": "valor"},
                {"data": "valor_pago"}
            ]
        });
    });

    function SubParcelas(d, tr, row, dt) {
        var data = '<div class="col-sm-12" style="text-align: center;">' +
                '       <span style="font-size: 18px;">Recebimento Detalhado</span><br />' +
                '       <hr /> ' +
                '       <table id="grid_rec_detalhado" '+
                '           class="table table-striped table-bordered table-hover table-full-width dataTable"'+
                '           style="width: 100%; margin-bottom: 25px!important;">'+
                '           <thead>'+
                '               <tr>'+
                '                   <th width="18%">Contrato</th>'+
                '                   <th>Cliente</th>'+
                '                   <th width="1%">Seq.</th>'+
                '                   <th width="5%">Vencimento</th>'+
                '                   <th width="10%">Valor</th>'+
                '                   <th width="10%">Valor Pago</th>'+
                '               </tr>'+
                '           </thead>'+
                '           <tbody>'+
                '           </tbody>'+
                '       </table>'+
                '   </div>';

        row.child(data).show();
    }

    $('#grid_recebidos tbody').on('click', 'td.anexar_cpr', function () {
        var tr = $(this).closest('tr');
        var row = tableRecebimentos.row(tr);
        var rowsTam = tableRecebimentos.rows()[0].length;
        var dt = $(this).parent().find('button').val();

        if ($(this).parent().find('button.anx').hasClass('btn-warning')) {
            $(this).parent().find('button.anx').removeClass('btn-warning');
            $(this).parent().find('button.anx').addClass('btn-success');
        } else {
            $(this).parent().find('button.anx').addClass('btn-warning');
            $(this).parent().find('button.anx').removeClass('btn-success');
        }

        if (row.child.isShown()) {
            row.child.hide();
        } else {
            for (i = 0; i < rowsTam; i++)
            {

                if (tableRecebimentos.row(i).child.isShown()) {
                    tableRecebimentos.row(i).child.hide();
                }
            }

            if($(this).parent().find('a').length === 0){
                SubAnexo(row.data(), tr, row, dt);
            }
        }

    });

    function SubAnexo(d, tr, row, dt) {
        var data = '<div class="col-sm-12" style="text-align: center;">' +
                '       <span style="font-size: 18px;">Anexar Comprovante de Pagamento</span><br />' +
                '       <label style="color: red">Depois de anexado o comprovante, sera dado baixa neste recebimento. Tenha certeza de que trata-se do recebimento correto.</label>' +
                '       <hr /> ' +
                '       <form id="form_anexo_comprovante" action="https://credshow.sigacbr.com.br/interno/anexarComprovante" method="post" enctype="multipart/form-data" class="form-horizontal"> ' +
                '           <input type="hidden" name="id_ri" value="' + dt + '">' +
                '           <div class="form-group">' +
                '               <div class="row">' +
                '                   <div class="col-md-3">' +
                '                   </div>' +
                '                   <div class="col-md-3">' +
                '                       <input type="file" name="anexo_comprovante" class="form-control"> ' +
                '                   </div>' +
                '                   <div class="col-md-3">' +
                '                       <button type="submit" style="width: 100%;" class="btn btn-default">Enviar Comprovante</button> ' +
                '                   </div>' +
                '                   <div class="col-md-3">' +
                '                   </div>' +
                '               </div>' +
                '           </div>' +
                '       </form>' +
                '   </div>';

        row.child(data).show();

        $('#form_anexo_comprovante').ajaxForm({
            beforeSubmit: function () {
                $.bloquearInterface('<h4>Aguarde... Processando Informações</h4>');
            },
            //em caso de sucesso
            success: function (response) {
                var retorno = $.parseJSON(response);
                var titulo = "Sucesso!";

                if(retorno.hasError){
                   titulo = "Algo deu errado :(";
                }

                $.desbloquearInterface();

                swal(
                        titulo,
                        retorno.msg,
                        retorno.tipo
                );

                $('#form_anexo_comrpovante').each(function () {
                    this.reset();
                });

                tableRecebimentos.ajax.reload();
            }
        });
    }

    var self;

    $(document).on('submit', '.comprovante_pgto', function (e) {
        e.preventDefault();
        self = this;
        swal({
            title: 'Tem certeza?',
            text: "Será dado baixa nesta parcela, tenha certeza de que se trata da parcela correta!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Sim, imprimir!',
            cancelButtonText: 'Não, cancelar!'
        }).then(function () {
            $.ajax({
                type: "POST",
                url: "/interno/baixaParcela",
                data: {
                    'id_parcela': $(self).find("input[name='parcelaid']").val()
                }
            }).done(function (dRt) {
                retorno = $.parseJSON(dRt);
                if(!retorno.hasError){
                    swal(
                            'Sucesso',
                            'Comprovante já disponibilizado! A parcela já consta como paga em nosso sistema :)',
                            'success'
                    );

                    tableParcelas.ajax.reload();
                    tableRecebimentos.ajax.reload();
                   
                    self.submit();
                }else{
                    swal(
                            'Algo deu errado :(',
                            retorno.msg,
                            'error'
                    );
                }
            });

        }, function (dismiss) {
            // dismiss can be 'cancel', 'overlay',
            // 'close', and 'timer'
            if (dismiss === 'cancel') {
                swal(
                        'Cancelado',
                        'Operação cancelada com sucesso! :)',
                        'error'
                        );
                return false;
            }
        });
    });
});
