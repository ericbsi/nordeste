$(function(){
	
	$('#cpfcliente').focus(); // cursor setado no campo de pesquisa assim que a página é carregada

	$.validator.setDefaults({
        
        errorElement: "span", 
        errorClass: 'help-block',
        
        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            $( $(element).data('icon-valid-bind') ).removeClass('ico-validate-success').addClass('ico-validate-error');
        },
            
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },

        success: function (label, element) {
            label.addClass('help-block valid');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            $( $(element).data('icon-valid-bind') ).removeClass('ico-validate-error').addClass('ico-validate-success');
        },
    });

	$('#form-boletos').validate();

    var tableBoletos = $('#grid_parcelas').DataTable({
        "columnDefs" : [
          {
            "orderable" : false,
            "targets"   : "no-orderable"
          }            
        ]
    });

});