
$(function(){

	var gridIndicadoresDiaMes = $('#grid_indicadores_dia_mes').DataTable({
		
		"processing"    	:true,
        "serverSide"    	:true,
        "scrollY" 			:"500px",
        "scrollCollapse" 	:true,
        "paging" 			:false,

        "ajax"          	: {
            url         	: '/administradorDeParceiros/getRanking',
            type        	: 'POST',
        },
        "columns":[
            { "data": "filial" },
            { "data": "totalDia" },
            { "data": "totalMes" },
        ],

		"columnDefs" 	: [
	      {
	        "orderable" : false,
	        "targets"   : "no-orderable"
	      }
	    ],
	    
	    "drawCallback" : function( settings ){
	    	
	    	var api 	=  	this.api(), data;
	    	$( api.column( 1 ).footer() ).html('R$ '+settings.json.customReturn.totalGeralDia);
	    	$( api.column( 2 ).footer() ).html('R$ '+settings.json.customReturn.totalGeralMes);
        }
	});
});
