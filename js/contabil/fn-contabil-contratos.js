$(function(){
	
	$('#nucleos_select').multiselect({

    	buttonWidth 						: '300px',
     	numberDisplayed 					: 2,
      	enableFiltering                  	: true,
      	enableCaseInsensitiveFiltering   	: true,
      	//includeSelectAllOption 				: true,

      	buttonText:function(options, select){

        	if (options.length == 0)
        	{
            	return 'Selecionar Núcleos <b class="caret"></b>'
        	}
        	else if (options.length == 1)
        	{
            	return '1 Núcleo Selecionado <b class="caret"></b>'
        	}
        	else
        	{
            	return options.length + ' Núcleos Selecionados <b class="caret"></b>'
         	}

      	}
  	});


	$.validator.setDefaults({
	    errorElement: "span",
	    errorClass: 'help-block',
	    highlight: function (element) {
	      $(element).closest('.help-block').removeClass('valid');
	      $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
	    },
	    unhighlight: function (element) {
	      $(element).closest('.form-group').removeClass('has-error');
	    },
	    success: function (label, element) {
	      label.addClass('help-block valid');
	      $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
	    }
  	});


	$('#form-filter').validate({

		submitHandler 	: function()
		{	
			var post 	= $.ajax({
				type 	: 'POST',
				url 	: '/contabil/listarContratos/',
				data 	: $('#form-filter').serialize(),
			});

			post.done(function(dRt){

				var retorno = $.parseJSON(dRt);

				console.log(retorno);

			});
		}
	});

});