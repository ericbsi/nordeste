$(function () {

    var filiaisAdmin;
    var idUsuario;
    var elementoQtdPar;

    var adminsTable = $('#grid_admins').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/usuario/getAdministradores',
            type: 'POST',
        },
        "columns": [
            {
                "data": "utilizador",
                "className": "utilizador"
            },
            {
                "data": "username",
                "className": "username"
            },
            {
                "data": "senha",
                "className": "tdSenha"
            },
            {
                "data": "qtd_parceiros",
                "className": "qtdParceiros"
            },
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            }
        ],
        "columnDefs": [
            {
                "orderable": false,
                "targets": "no-orderable"
            }
        ],
    });

    $('#parceiros_select').multiselect({
        buttonWidth: '730px',
        numberDisplayed: 2,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        buttonText: function (options, select) {

            if (options.length == 0) {
                return 'Selecionar modalidade <b class="caret"></b>'
            }
            else if (options.length == 1) {
                return '1 modalidade selecionada <b class="caret"></b>'
            }
            else {
                return options.length + ' Financeiras Selecionadas <b class="caret"></b>'
            }
        },
    });

    $.validator.setDefaults({
        errorElement: "span",
        errorClass: 'help-block',
        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group
        },
        success: function (label, element) {
            label.addClass('help-block valid');
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        }
    });

    $('#form-add-admin').validate({
        rules: {
            'Admin[username]': {
                remote: {
                    url: '/usuario/checkUsername/',
                    type: 'POST',
                    data: {
                        username: function () {
                            return $('#Usuario_username').val()
                        }
                    },
                }
            },
            'Admin[password]': {
                required: true
            },
            password_again: {
                required: true,
                equalTo: "#Usuario_password"
            },
        },
        messages: {
            'Admin[username]': {
                remote: "Nome de usuário já cadastrado"
            },
            password_again: {
                equalTo: "Senhas não coincidem!"
            },
        },
        submitHandler: function () {

            var posting = $.ajax({
                url: '/usuario/cadastrarAdmin/',
                type: 'POST',
                data: $('#form-add-admin').serialize(),
            });

            posting.done(function (response) {

                var resp = $.parseJSON(response);

                if (!resp.hasErrors)
                {
                    adminsTable.draw();
                    $("#modal_form_new_admin").modal('hide');
                }

            });

            return false;

        }
    });

    // Add event listener for opening and closing details
    $('#grid_admins tbody').on('click', 'td.details-control', function () {

        var tr = $(this).closest('tr');
        var row = adminsTable.row(tr);
        var rowsTam = adminsTable.rows()[0].length;
        var rowData = row.data();
        var elemento = $(this);

        elementoQtdPar = $('.qtdParceiros').get(row.index() + 1);

        idUsuario = rowData.idUsuario;

        if (row.child.isShown()) {

            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        
        else {

            for (i = 0; i < rowsTam; i++)
            {

                if (adminsTable.row(i).child.isShown()) {
                    adminsTable.row(i).child.hide();
                }
            }

            $('td.details-control').closest('tr').removeClass('shown');

            formatSubTable(row.data(), tr, row);

            filiaisAdmin = $('#filiaisAdmin').dataTable(
            {
                "processing": true,
                "serverSide": true,
                "ajax": {
                    url: '/usuario/getFiliaisAdmin',
                    type: 'POST',
                    data: function (d) {
                        d.idUsuario = idUsuario
                    }

                },
                "columns": [
                    {
                        "data": "checkFilial",
                        "orderable": false,
                        "className": "checkFilial"
                    },
                    {
                        "orderable": false,
                        "data": "filial"
                    }
                ],
                "columnDefs": [
                    {
                        "orderable": false,
                        "targets": "no-orderable"
                    },
                    {
                        "orderable": false,
                        "targets": "no-orderable"
                    }
                ]
            });

        }
    });

    $(document).on('click', '#checkFiliais', function ()
    {

        var className = $(this).attr('class');
        var elemento = $(this);

        $.ajax({
            type: "POST",
            url: "/usuario/marcarTodasFiliaisAdmin/",
            data: {
                'idUsuario': idUsuario,
                'classeCheck': className
            }
        }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            $.pnotify(
                    {
                        title: retorno.title,
                        text: retorno.msg,
                        type: retorno.pntfyClass
                    }
            );

            if (!retorno.hasErrors)
            {

                if (elemento.hasClass('clip-checkbox-partial'))
                {

                    elemento.removeClass('clip-checkbox-partial');
                    elemento.addClass('clip-checkbox-unchecked-2');
                }
                else if (elemento.hasClass('clip-checkbox-unchecked-2'))
                {
                    elemento.removeClass('clip-checkbox-unchecked-2');
                    elemento.addClass('clip-checkbox-checked-2');
                }
                else
                {
                    elemento.removeClass('clip-checkbox-checked-2');
                    elemento.addClass('clip-checkbox-unchecked-2');
                }

                filiaisAdmin.
                        dataTable().
                        fnDraw();

                elementoQtdPar.textContent = retorno.qtdAdmin;

            }

        });

    });

    $(document).on('click', '.iCheckFilial', function ()
    {
        var tr = $(this).closest('tr');
        var row = filiaisAdmin.api().row(tr);
        var rowData = row.data();
        var idFilial = rowData.filialID;

        var className = $(this).attr('class');
        var elemento = $(this);

        $.ajax({
            type: "POST",
            url: "/usuario/marcarFilialAdmin/",
            data: {
                'idUsuario': idUsuario,
                'idFilial': idFilial,
                'classeCheck': className
            }
        }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            $.pnotify(
                    {
                        title: retorno.title,
                        text: retorno.msg,
                        type: retorno.pntfyClass
                    }
            );

            if (!retorno.hasErrors)
            {

                if (elemento.hasClass('clip-checkbox-unchecked-2'))
                {
                    elemento.removeClass('clip-checkbox-unchecked-2');
                    elemento.addClass('clip-checkbox-checked-2');
                }
                else
                {
                    elemento.removeClass('clip-checkbox-checked-2');
                    elemento.addClass('clip-checkbox-unchecked-2');
                }

                elementoQtdPar.textContent = retorno.qtdAdmin;

                if (retorno.qtdAdmin == retorno.qtdGeral)
                {
                    $('#checkFiliais').removeClass('clip-checkbox-partial');
                    $('#checkFiliais').removeClass('clip-checkbox-unchecked-2');
                    $('#checkFiliais').addClass('clip-checkbox-checked-2');
                }
                else if (retorno.qtdAdmin == 0)
                {
                    $('#checkFiliais').removeClass('clip-checkbox-checked-2');
                    $('#checkFiliais').removeClass('clip-checkbox-partial');
                    $('#checkFiliais').addClass('clip-checkbox-unchecked-2');
                }
                else
                {
                    $('#checkFiliais').removeClass('clip-checkbox-checked-2');
                    $('#checkFiliais').removeClass('clip-checkbox-unchecked-2');
                    $('#checkFiliais').addClass('clip-checkbox-partial');
                }

            }

        });

    })

    $('#btnSalvar').on('click', function () {

        $.ajax({
            type: "POST",
            url: "/usuario/salvarAdmin/",
            data: {
                "nomeUtilizador": $('#inputNomeUtilizador').val(),
                "username": $('#inputUsername').val(),
                "senha": $('#inputSenha').val(),
                "parceiros": $('#parceiros_select').val(),
            }
        }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            $.pnotify({
                title: 'Notificação',
                text: retorno.msg,
                type: retorno.type
            });

            if (!(retorno.hasErrors))
            {

                //$('#parceiros_select'       ).val(0).change();
                $('#inputNomeUtilizador').val('');
                $('#inputUsername').val('');
                $('#inputSenha').val('');

                console.log($('#parceiros_select'));

                $('#parceiros_select option').each(function (index, option)
                {
                    $(option).remove();
                });

                console.log($('#parceiros_select'));

                adminsTable.draw();

            }

        })

    });

});

function formatSubTable(d, tr, row) {

    tr.addClass('shown');

    // `d` is the original data object for the row
    var data = '<h5><i class="clip-list"></i> Parceiros</h5>' +
            '<div class="row">' +
            '   <div  class="col-sm-12">' +
            '       <table id="filiaisAdmin" class="table table-striped table-bordered table-hover table-full-width dataTable">' +
            '           <thead>' +
            '              <tr>' +
            '                   <th class="no-orderable" width="20px" id="thCheckFilial">' +
            '                       <i id="checkFiliais" class="clip-checkbox-partial"></i>' +
            '                   </th>' +
            '                   <th class="no-orderable"            >Filial</th>' +
            '              </tr>' +
            '           </thead>' +
            '           <tbody>' +
            '           </tbody>' +
            '       </table>' +
            '   </div>' +
            '</div>';

    row.child(data).show();
}