$(document).ready(function () {

    var fornecedorTable = $('#grid_fornecedores').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax":
                {
                    url     :   '/fornecedor/listar'  ,
                    type    :   'POST'                ,
                    data    :   function(d)
                                {
                                    d.filtroRazaoSocial     = $('#filtroRazaoSocial'    ).val();
                                    d.filtroNomeFantasia    = $('#filtroNomeFantasia'   ).val();
                                    //d.filtroCGC             = $('#filtroCGC'            ).val();
                                    d.filtroTipo            = $('#filtroTipo'           ).val();
                                    /*d.filtroUF              = $('#filtroUF'             ).val();
                                    d.filtroCidade          = $('#filtroCidade'         ).val();*/
                                }
                },
        "columns": [
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            {
                "data": "razaoSocial",
                "className": "razaoSocial",
            },
            {
                "data": "nomeFantasia",
                "className": "nomeFantasia",
            },
            {
                "data": "cgc",
                "className": "cgc",
            },
            {
                "data": "tipo",
                "className": "tipo",
            },
            {
                "data": "uf",
                "className": "uf",
            },
            {
                "data": "cidade",
                "className": "cidade",
            },
            {"data": "btnRemover"},
        ],
    });
    
    $('.filtroFornecedor').on('change', function()
    {
        fornecedorTable.draw();
    });

    $('.ufSelect').on('change', function ()
    {
        if ($.trim($(this).attr('id')) == "ufCliente")
        {
            getCidades($(this).val(), $('#cidadeCliente'));
        }
        else if ($.trim($(this).attr('id')) == "ufEndereco")
        {
            getCidades($(this).val(), $('#cidadeEndereco'));
        }
    });

    $('#btnAddFornecedor').on('click', function () {

        $.ajax({
            type: "POST",
            url: "/fornecedor/salvar/",
            data: {
                "razaoSocial"           : $('#razaoSocial'          ).val() ,
                "nomeFantasia"          : $('#nomeFantasia'         ).val() ,
                "tipoCGC"               : $('#tipoCGC'              ).val() ,
                "cgc"                   : $('#cgc'                  ).val() ,
                "ufCliente"             : $('#ufCliente'            ).val() ,
                "cidadeCliente"         : $('#cidadeCliente'        ).val() ,
                "numeroContato"         : $('#numeroContato'        ).val() ,
                "tipoTelefoneContato"   : $('#tipoTelefoneContato'  ).val() ,
                "ramalContato"          : $('#ramalContato'         ).val() ,
                "cepEndereco"           : $('#cepEndereco'          ).val() ,
                "logradouroEndereco"    : $('#logradouroEndereco'   ).val() ,
                "bairroEndereco"        : $('#bairroEndereco'       ).val() ,
                "numeroEndereco"        : $('#numeroEndereco'       ).val() ,
                "complementoEndereco"   : $('#complementoEndereco'  ).val() ,
                "ufEndereco"            : $('#ufEndereco'           ).val() ,
                "cidadeEndereco"        : $('#cidadeEndereco'       ).val() ,
                "tipoEndereco"          : $('#tipoEndereco'         ).val()
            }
        }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            $.pnotify({
                title: 'Notificação',
                text: retorno.msg,
                type: retorno.pntfyClass
            });

            if (!(retorno.hasErrors)) {
                
                $('#razaoSocial'            ).val('')           ;
                $('#nomeFantasia'           ).val('')           ;
                $('#tipoCGC'                ).val(0 ).change()  ;
                $('#cgc'                    ).val('')           ;
                $('#ufCliente'              ).val(0 ).change()  ;
                $('#cidadeCliente'          ).val(0 ).change()  ;
                $('#numeroContato'          ).val('')           ;
                $('#tipoTelefoneContato'    ).val(0 ).change()  ;
                $('#ramalContato'           ).val('')           ;
                $('#cepEndereco'            ).val('')           ;
                $('#logradouroEndereco'     ).val('')           ;
                $('#bairroEndereco'         ).val('')           ;
                $('#numeroEndereco'         ).val('')           ;
                $('#complementoEndereco'    ).val('')           ;
                $('#ufEndereco'             ).val(0 ).change()  ;
                $('#cidadeEndereco'         ).val(0 ).change()  ;
                $('#tipoEndereco'           ).val(0 ).change()  ;
                
                fornecedorTable.draw();
            }

        })

    });

    $('.select2').select2()

    $('.selectCidades').select2(
            {
                data: [
                    {
                        id: 0,
                        text: 'Carregando..'
                    }
                ],
                placeholder: "Cidade...",
            });
            
    $('.form-control').on('change',function()
    {
        
        if($.trim($('#razaoSocial').val()) == "")
        {
            $('#btnAddFornecedor').attr('disabled','disabled');
            return;
        }
        
        if($.trim($('#nomeFantasia').val()) == "")
        {
            $('#btnAddFornecedor').attr('disabled','disabled');
            return;
        }
        
        if($.trim($('#cgc').val()) == "")
        {
            $('#btnAddFornecedor').attr('disabled','disabled');
            return;
        }
        
        if($.trim($('#ufCliente').val()) == "0")
        {
            $('#btnAddFornecedor').attr('disabled','disabled');
            return;
        }
        
        if($.trim($('#cidadeCliente').val()) == "0")
        {
            $('#btnAddFornecedor').attr('disabled','disabled');
            return;
        }
        
        if($.trim($('#numeroContato').val()) == "")
        {
            $('#btnAddFornecedor').attr('disabled','disabled');
            return;
        }
        
        if($.trim($('#tipoTelefoneContato').val()) == "0")
        {
            $('#btnAddFornecedor').attr('disabled','disabled');
            return;
        }
        
        if($.trim($('#cepEndereco').val()) == "")
        {
            $('#btnAddFornecedor').attr('disabled','disabled');
            return;
        }
        
        if($.trim($('#logradouroEndereco').val()) == "")
        {
            $('#btnAddFornecedor').attr('disabled','disabled');
            return;
        }
        
        if($.trim($('#bairroEndereco').val()) == "")
        {
            $('#btnAddFornecedor').attr('disabled','disabled');
            return;
        }
        
        if($.trim($('#numeroEndereco').val()) == "")
        {
            $('#btnAddFornecedor').attr('disabled','disabled');
            return;
        }
        
        if($.trim($('#ufEndereco').val()) == "0")
        {
            $('#btnAddFornecedor').attr('disabled','disabled');
            return;
        }
        
        if($.trim($('#cidadeEndereco').val()) == "0")
        {
            $('#btnAddFornecedor').attr('disabled','disabled');
            return;
        }
        
        if($.trim($('#tipoEndereco').val()) == "0")
        {
            $('#btnAddFornecedor').attr('disabled','disabled');
            return;
        }
        
        $('#btnAddFornecedor').removeAttr('disabled');
    });

    function getCidades(estado, elemento)
    {

        $.ajax({
            type: 'GET',
            url: '/cidade/listCidades/',
            data: {
                uf: estado
            },
        }).done(function (dRt) {

            var jsonReturn = $.parseJSON(dRt);

            elemento.select2({
                data: jsonReturn
            });

        });

    }

    //////////////////////////////////////////////////////////////////////////
    // funcao que irá capturar o click duplo na célula de senha e irá       //
    // habilitar para edição da mesma. caso a mesma sofra alguma alteração, //
    // salve o conteúdo no banco                                            //
    //////////////////////////////////////////////////////////////////////////
    // Autor : André Willams // Data : 06-07-2015 ////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    $(document).on('dblclick', '.tdNome', function () {

        var conteudoOriginal = $(this).text(); //resgate o conteúdo atual da célula

        var idGrupo = $(this).context.parentElement.cells[1].textContent; //pegue o id do Grupo

        //transforme o elemento em um input
        $(this).html("<input id='mudaNome' class='form-control' type='text' value='" + conteudoOriginal + "' />");

        $(this).children().first().focus(); //atribua o foco ao elemento criado

        $(this).children().first().keypress(function (e) { //quando alguma tecla for pressionada

            if (e.which == 13) { //caso a tecla pressionada seja o ENTER, foi confirmada a alteração

                var novoConteudo = $(this).val(); //pegue o novo conteúdo

                //chame o ajax de alteração
                $.ajax({
                    type: "POST",
                    url: "/grupoFiliais/mudarNome",
                    data: {
                        "idGrupo": idGrupo,
                        "nomeGrupo": novoConteudo
                    },
                }).done(function (dRt) {

                    var retorno = $.parseJSON(dRt);

                    $.pnotify({
                        title: 'Notificação',
                        text: retorno.msg,
                        type: retorno.pntfyClass
                    });

                })

                $(this).parent().text(novoConteudo);

            }
        });

        //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
        $(this).children().first().blur(function () {

            //devolva o conteúdo original
            $(this).parent().text(conteudoOriginal);

        });

    });

})