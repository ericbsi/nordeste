$(document).ready(function(){

	var form1 = $('#empresaForm');

	var errorHandler1 = $('.errorHandler', form1);
        
    var successHandler1 = $('.successHandler', form1);

	$('#usuarioForm').validate({

		errorElement: "span",
    errorClass: 'help-block',
    errorPlacement: function (error, element) { // render error placement for each input type
        	if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
            } else if (element.attr("name") == "dd" || element.attr("name") == "mm" || element.attr("name") == "yyyy") {
                    error.insertAfter($(element).closest('.form-group').children('div'));
            } else {
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
            }
    },
       
    ignore: "",

    rules : {
          
          'Usuario[username]' : {
             remote : {
                url: '/usuario/checkUsername/',
                type: 'POST',
                data :{
                    username : function () {
                        return $('#Usuario_username').val()
                    }
                },
             }
          },

          'Usuario[password]' :{
            required: true
          },

          password_again: {
            required: true,
            equalTo: "#Usuario_password"
          },

          '#s2id_autogen2':{
            required :true
          }
    },
    
    messages: {
          
          'Usuario[username]'  : {
            remote : "Nome de usuário já cadastrado"
          },

          password_again: {
            equalTo : "Insira a senha novamente"
          }, 
    },

    invalidHandler: function (event, validator) { //display error alert on form submit
      successHandler1.hide();
      errorHandler1.show();
    },

    highlight: function (element) {
      $(element).closest('.help-block').removeClass('valid');
      // display OK icon
      $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
      // add the Bootstrap error class to the control group
    },
                
    unhighlight: function (element) {
      // revert the change done by hightlight
      $(element).closest('.form-group').removeClass('has-error');
      // set error class to the control group
    },

    success: function (label, element) {
      label.addClass('help-block valid');
      // mark the current input as valid and display OK icon
      $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
    },
    
	});

});