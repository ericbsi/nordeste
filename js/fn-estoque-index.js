$(function(){

	$('.select2').select2();

    $('.multipleselect').multiselect({

        buttonWidth: '200px',
        numberDisplayed:2,

        buttonText:function(options, select){

             if (options.length == 0) {
                return 'Filtrar <b class="caret"></b>'
             }

             else if (options.length == 1) {
                return '1 Selecionado(a) <b class="caret"></b>'
             }

             else{
                return options.length + ' Selecionados(as) <b class="caret"></b>'
             }

        },
    });

    var gridEstoques = $('#grid_estoques').DataTable({
		"processing": true,
        "serverSide": true,
        "ajax": {
            url: '/estoque/getEstoquesEmpresa/',
            type: 'POST',
            "data": function (d) {
                d.empresaId = $("#empresaId").val(),
                d.filiais   = $("#filiais_select").val(),
                d.locais    = $("#select_locais").val(),
                d.vende     = $("#select_vende").val()
            },
        },
        "language": {
            "processing": "<img style='position:fixed; top:60%; left:50%;margin-top:-8px;margin-left:-8px;' src='https://beta.sigacbr.com.br/js/loading.gif'>"
        },
        "columnDefs": [{
            "orderable": false,
            "targets": "no-orderable"
        }],
        "columns": [
            {"data": "filial"},
            {"data": "local"},
            {"data": "vende"},
        ],
        /*"drawCallback" : function(settings) {
            console.log(settings.json);
        }*/
	});

    $(document).on('change','.select_redraw_table', function(){
        gridEstoques.draw();
    })

    /*Validação formulário*/
    $.validator.setDefaults({
        errorElement: "span", 
        errorClass: 'help-block',
        
        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group
        },
            
        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group
        },

        success: function (label, element) {
            label.addClass('help-block valid');
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        }
    })

    $("#form-add-estoque").validate({

        submitHandler   :   function(){

            $("#cadastro_msg_return").hide().empty();

            var formData = $('#form-add-estoque').serialize();

            $.ajax({
                
                type        : "POST",
                url         : "/estoque/add/",
                data        : formData,

                beforeSend  : function(){

                    $('.panel').block({
                        overlayCSS: {
                            backgroundColor: '#fff'
                        },
                        message: '<img src="https://beta.sigacbr.com.br/js/loading.gif" /> Salvando estoque...',
                        css: {
                            border: 'none',
                            color: '#333',
                            background: 'none'
                        }
                    });
                    window.setTimeout(function () {
                        $('.panel').unblock();
                    }, 1000);
                }

            })

            .done(function(dRt){

                var retorno = $.parseJSON( dRt );

                if( !$('#checkbox_continuar').is(':checked') )
                {
                    $('#modal_form_new_estoque').modal('hide');
                    $('#form-add-estoque').trigger("reset");

                    $.pnotify({
                        title    : 'Notificação',
                        text     : retorno.msg,
                        type     : retorno.pntfyClass
                    });
                }

                else
                {
                    $("#cadastro_msg_return").prepend('<p>'+retorno.msg+'</p>').fadeIn(100).addClass('alert-success').fadeOut(9000);
                }
                
                $('.has-success').each(function(index){
                    $(this).removeClass('has-success');
                })

                $('.ok').each(function(index){
                    $(this).removeClass('ok');
                })
                
                $('#form-add-estoque').trigger("reset");

                gridEstoques.draw();
            })

            return false;
        }
    });
});