/*
 $(document).ready(function () {
 
 function updateTablePropostas() {
 
 $('#table-wrapper').load('/proposta/ultimasPropostas/', function () {
 setTimeout(updateTablePropostas, 25000);
 });
 }
 
 updateTablePropostas();
 })
 */


$(document).on('click', '.label-warning', function () {

    $('#input_proposta_id').attr('value', $(this).attr('subid'));

});


var grid_analises = $('#table_id').DataTable({
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "ajax": {
        url: '/proposta/ultimasPropostas/',
        type: 'POST',
        "data": function (d) {
                    d.codigo_filter = $("#codigo_filter").val(),
                    d.nome_filter = $("#nome_filter").val(),
                    d.semear = $('#filter_semear').prop('checked');
                    d.prestige = $('#filter_prestige').prop('checked');
        }
    },
    "columns": [
        {"data": "modalidade", "orderable":false},
        {"data": "codigo", "orderable":false},
        {"data": "cliente", "orderable":false},
        {"data": "dadosCli", "orderable":false},
        {"data": "financeira", "orderable":false},
        {"data": "filial", "orderable":false},
        {"data": "politicaCredito", "orderable":false},
        {"data": "valor", "orderable":false},
        {"data": "valorSeguro", "orderable":false},
        {"data": "qtd_parcelas", "orderable":false},
        {"data": "btn", "orderable":false},
        {"data": "data_cadastro", "orderable":false},
        {"data": "tempoEspera", "orderable":false}
    ],
    "drawCallback": function (settings) {
        setTimeout(function () {
            grid_analises.draw(false);
        }, 20000)
            
        if (settings.json.retorno != null) {
              
            $.pnotify(
            {
                title   : 'Notificação'                            ,
                text    : settings.json.retorno.messages           ,
                type    : settings.json.retorno.type
            });
                
                $('#filter_prestige').on('click', function (){
                    $("#filter_prestige").change(function(e){
                        if(!($(this).is(':checked')))       
                            $(this).attr("checked", false);                     
                    });
                });              
        }       
    }    
});

$('#filter_semear').on('click', function ()
{

    //console.log($('#filter_semear').prop('checked'));
    grid_analises.draw();
});
$('#filter_prestige').on('click', function ()
{
    //console.log($('#filter_prestige').prop('checked'));
    grid_analises.draw();
});

$(document).on('change', '.input_filter', function () {
    grid_analises.draw();
});

$('#form-liberar-proposta').validate({
    submitHandler: function () {

        var post = $.ajax({
            url: '/proposta/enviarParaFila/',
            type: 'POST',
            data: $('#form-liberar-proposta').serialize()

        });

        post.done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            $('#modal-enviar').modal('hide');

            grid_analises.draw();

            $.pnotify({
                title: retorno.titulo,
                text: retorno.texto,
                type: retorno.tipo
            });
        });

        return false;
    }
});
