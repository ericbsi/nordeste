$(document).ready(function (){
    
    var filiaisTable = $('#gridFiliais').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax":
                {
                    url: '/servico/getFiliais',
                    type: 'POST',
                    data: function(d){
                        d.filiais = $('#selectFiliais').val();
                    }
                },
        "columns": [
            {
                "orderable": false,
                "data": "filial"
            },
            {
                "orderable": false,
                "class": "details-control",
                "data": null,
                "defaultContent": ''
            }
        ]
    });
    
    var selectGF = $('#selectGrupoFiliais').multiselect(
            {
                buttonWidth: '300px',
                numberDisplayed: 2,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                buttonText: function (options, select)
                {

                    if (options.length == 0) {
                        return 'Selecionar Grupos <b class="caret"></b>';
                    } else if (options.length == 1) {
                        return '1 Grupo Selecionado <b class="caret"></b>';
                    } else {
                        return options.length + ' Grupos Selecionados <b class="caret"></b>';
                    }

                }
            });
            
    var selectNF = $('#selectNucleoFiliais').multiselect(
            {
                buttonWidth: '300px',
                numberDisplayed: 2,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                buttonText: function (options, select)
                {

                    if (options.length == 0) {
                        return 'Selecionar Nucleos <b class="caret"></b>';
                    } else if (options.length == 1) {
                        return '1 Nucleo Selecionado <b class="caret"></b>';
                    } else {
                        return options.length + ' Nucleos Selecionados <b class="caret"></b>';
                    }

                }
            });
    
    var selectF = $('#selectFiliais').multiselect(
            {
                buttonWidth: '300px',
                numberDisplayed: 2,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                buttonText: function (options, select)
                {

                    if (options.length == 0) {
                        return 'Selecionar Filiais <b class="caret"></b>';
                    } else if (options.length == 1) {
                        return '1 Filial Selecionada <b class="caret"></b>';
                    } else {
                        return options.length + ' Filiais Selecionadas <b class="caret"></b>';
                    }

                }
            });
            
    selectNF.on('change', function()
    {
        
        $.ajax(
        {
            type    : "POST"                                    ,
            url     : "/grupoFiliais/getGrupoFilial/"   ,
            
            data: 
            {
                "idNucleos"  : $(this).val(),
            }
        }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            if (!(retorno.hasErrors)) 
            {
                selectGF  .val(retorno.dataGrupos     );
                selectF       .val(retorno.dataFiliais    );
                
                selectGF  .multiselect("refresh");
                selectF       .multiselect("refresh");
                
            }
            else
            {
                console.log(retorno);
            }
            
            filiaisTable.draw();
            
        });
        
    });
    
    selectGF.on('change', function()
    {
        
        $.ajax(
        {
            type    : "POST"                                    ,
            url     : "/grupoFiliais/getNucleoFilial/"   ,
            
            data: 
            {
                "idGrupos"  : $(this).val(),
            }
        }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            if (!(retorno.hasErrors)) 
            {
                selectNF .val(retorno.dataNucleos );
                selectF       .val(retorno.dataFiliais );
                
                selectNF .multiselect("refresh");
                selectF       .multiselect("refresh");
                
            }
            else
            {
                console.log(retorno);
            }
            
            filiaisTable.draw();
            
        });
        
    });
    
    selectF.on('change', function()
    {
        
        $.ajax(
        {
            type    : "POST"                            ,
            url     : "/grupoFiliais/getFilial/" ,
            
            data: 
            {
                "idFiliais"  : $(this).val(),
            }
        }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            if (!(retorno.hasErrors)) 
            {
                selectNF .val(retorno.dataNucleos    );
                selectGF  .val(retorno.dataGrupos     );
                
                selectNF .multiselect("refresh");
                selectGF  .multiselect("refresh");
                
            }
            else
            {
                console.log(retorno);
            }

        });
        
        filiaisTable.draw();
        
    });
});