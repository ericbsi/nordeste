$(document).ready(function ()
{

    var filiaisAdmin;
    var idServico;
    var elementoQtdFiliais;

    var gridServicos = $('#gridServicos').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax":
                {
                    url: '/servico/getServicos',
                    type: 'POST',
                },
        "columns":
                [
                    {
                        "data": "descricao",
                        "className": "descricao"
                    },
                    {
                        "data": "qtdFiliais",
                        "className": "qtdFiliais"
                    },
                    {
                        "className": 'details-control',
                        "orderable": false,
                        "data": null,
                        "defaultContent": ''
                    }
                ],
        "columnDefs":
                [
                    {
                        "orderable": false,
                        "targets": "no-orderable"
                    }
                ],
    });

    // Add event listener for opening and closing details
    $('#gridServicos tbody').on('click', 'td.details-control', function () {

        var tr = $(this).closest('tr');
        var row = gridServicos.row(tr);
        var rowsTam = gridServicos.rows()[0].length;
        var rowData = row.data();
        var elemento = $(this);


        elementoQtdFiliais = $('.qtdFiliais').get(row.index() + 1);
        idServico = rowData.idServico;

        if (row.child.isShown()) {

            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {

            for (i = 0; i < rowsTam; i++)
            {

                if (gridServicos.row(i).child.isShown())
                {
                    gridServicos.row(i).child.hide();
                }
            }

            $('td.details-control').closest('tr').removeClass('shown');

            formatSubTable(row.data(), tr, row);
            if (idServico !== 1) 
            {
            
                filiaisAdmin = $('#filiaisAdmin').dataTable(
                        {
                            "processing": true,
                            "serverSide": true,
                            "ajax": {
                                url: '/servico/getFiliaisAdmin',
                                type: 'POST',
                                data: function (d) {
                                    d.idServico = idServico
                                }

                            },
                            "columns": [
                                {
                                    "data": "checkFilial",
                                    "orderable": false,
                                    "className": "checkFilial"
                                },
                                {
                                    "orderable": false,
                                    "data": "filial"
                                }
                            ],
                            "columnDefs": [
                                {
                                    "orderable": false,
                                    "targets": "no-orderable"
                                },
                                {
                                    "orderable": false,
                                    "targets": "no-orderable"
                                }
                            ]
                        });
                        

            } 
            else 
            {
                
                filiaisAdmin = $('#filiaisAdmin').dataTable(
                        {
                            "processing": true,
                            "serverSide": true,
                            "ajax": {
                                url: '/servico/getFiliaisAdmin',
                                type: 'POST',
                                data: function (d) {
                                    d.idServico = idServico
                                }

                            },
                            "columns": [
                                {
                                    "data": "checkFilial",
                                    "orderable": false,
                                    "className": "checkFilial"
                                },
                                {
                                    "orderable": false,
                                    "data": "filial"
                                },
                                {
                                    "orderable": false,
                                    "data": "selectFin"
                                }
                            ],
                            "columnDefs": [
                                {
                                    "orderable": false,
                                    "targets": "no-orderable"
                                },
                                {
                                    "orderable": false,
                                    "targets": "no-orderable"
                                }
                            ]
                        });
            }
        }
    });

    $(document).on('click', '#checkFiliais', function ()
    {

        var className = $(this).attr('class');
        var elemento = $(this);

        $.ajax(
                {
                    type: "POST",
                    url: "/servico/marcarTodasFiliaisAdmin/",
                    data:
                            {
                                'idServico': idServico,
                                'classeCheck': className
                            }
                }).done(function (dRt)
        {

            var retorno = $.parseJSON(dRt);

            $.pnotify(
                    {
                        title: retorno.title,
                        text: retorno.msg,
                        type: retorno.pntfyClass
                    });

            if (!retorno.hasErrors)
            {

                if (elemento.hasClass('clip-checkbox-partial'))
                {

                    elemento.removeClass('clip-checkbox-partial');
                    elemento.addClass('clip-checkbox-unchecked-2');
                } else if (elemento.hasClass('clip-checkbox-unchecked-2'))
                {
                    elemento.removeClass('clip-checkbox-unchecked-2');
                    elemento.addClass('clip-checkbox-checked-2');
                } else
                {
                    elemento.removeClass('clip-checkbox-checked-2');
                    elemento.addClass('clip-checkbox-unchecked-2');
                }

                filiaisAdmin.
                        dataTable().
                        fnDraw();

                elementoQtdFiliais.textContent = retorno.qtdAdmin;

            }

        });

    });

    $(document).on('change', '#selectFin', function () {
        var tr = $(this).closest('tr');
        var row = filiaisAdmin.api().row(tr);
        var rowData = row.data();
        var idFilial = rowData.filialID;

        $.ajax(
                {
                    type: "POST",
                    url: "/servico/atulizarFinanceira/",
                    data:
                            {
                                'idFin': $(this).val(),
                                'idFil': idFilial
                            }
                }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            $.pnotify(
                    {
                        title: 'Informações',
                        text: retorno.msg,
                        type: retorno.type
                    }
            );
        });
    });

    $(document).on('click', '.iCheckFilial', function ()
    {
        var tr = $(this).closest('tr');
        var row = filiaisAdmin.api().row(tr);
        var rowData = row.data();
        var idFilial = rowData.filialID;

        var className = $(this).attr('class');
        var elemento = $(this);

        $.ajax(
                {
                    type: "POST",
                    url: "/servico/marcarFilialAdmin/",
                    data:
                            {
                                'idServico': idServico,
                                'idFilial': idFilial,
                                'classeCheck': className
                            }
                }).done(function (dRt) {

            var retorno = $.parseJSON(dRt);

            $.pnotify(
                    {
                        title: retorno.title,
                        text: retorno.msg,
                        type: retorno.pntfyClass
                    }
            );

            if (!retorno.hasErrors)
            {

                if (elemento.hasClass('clip-checkbox-unchecked-2'))
                {
                    elemento.removeClass('clip-checkbox-unchecked-2');
                    elemento.addClass('clip-checkbox-checked-2');
                } else
                {
                    elemento.removeClass('clip-checkbox-checked-2');
                    elemento.addClass('clip-checkbox-unchecked-2');
                }

                elementoQtdFiliais.textContent = retorno.qtdAdmin;

                if (retorno.qtdAdmin == retorno.qtdGeral)
                {
                    $('#checkFiliais').removeClass('clip-checkbox-partial');
                    $('#checkFiliais').removeClass('clip-checkbox-unchecked-2');
                    $('#checkFiliais').addClass('clip-checkbox-checked-2');
                } else if (retorno.qtdAdmin == 0)
                {
                    $('#checkFiliais').removeClass('clip-checkbox-checked-2');
                    $('#checkFiliais').removeClass('clip-checkbox-partial');
                    $('#checkFiliais').addClass('clip-checkbox-unchecked-2');
                } else
                {
                    $('#checkFiliais').removeClass('clip-checkbox-checked-2');
                    $('#checkFiliais').removeClass('clip-checkbox-unchecked-2');
                    $('#checkFiliais').addClass('clip-checkbox-partial');
                }

            }

        });

    })

    $('#btnSalvar').on('click', function ()
    {

        $.ajax(
                {
                    type: "POST",
                    url: "/servico/salvarServico/",
                    data:
                            {
                                'nomeServico': $("#inputDescricao").val()
                            }
                }).done(function (dRt)
        {

            var retorno = $.parseJSON(dRt);

            $.pnotify(
                    {
                        title: retorno.title,
                        text: retorno.msg,
                        type: retorno.pntfyClass
                    });

            if (!retorno.hasErrors)
            {
                gridServicos.draw();
            }

        });

    });

});

function formatSubTable(d, tr, row) {

    tr.addClass('shown');

    // `d` is the original data object for the row
    var data = '<h5><i class="clip-list"></i> Filiais</h5>' +
            '<div class="row">' +
            '   <div  class="col-sm-12">' +
            '       <table id="filiaisAdmin" class="table table-striped table-bordered table-hover table-full-width dataTable">' +
            '           <thead>' +
            '              <tr>' +
            '                   <th class="no-orderable" width="20px" id="thCheckFilial">' +
            '                       <i id="checkFiliais" class="clip-checkbox-partial"></i>' +
            '                   </th>' +
            '                   <th class="no-orderable"            >Filial</th>';
    if (d.idServico == 1) {
        data += '<th class="no-orderable"            >Financeira</th>';
    }
    data += '              </tr>' +
            '           </thead>' +
            '           <tbody>' +
            '           </tbody>' +
            '       </table>' +
            '   </div>' +
            '</div>';

    row.child(data).show();
}