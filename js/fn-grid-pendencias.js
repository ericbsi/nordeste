/*Função para evitar que o keyup envie uma requisição ajax para cada digito escrito no input do codigo*/
var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();

/*Bloqueia o Ctrl J no (apenas para google chrome)*/
function bloquear_ctrl_j(){

    if (window.event.ctrlKey && window.event.keyCode == 74)
    {
        event.keyCode = 0;  
        event.returnValue = false;  
    }
}

$(function(){

    $.validator.setDefaults({
        
        errorElement: "span", 
        errorClass: 'help-block',
        
        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            $( $(element).data('icon-valid-bind') ).removeClass('ico-validate-success').addClass('ico-validate-error');
        },
            
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },

        success: function (label, element) {
            label.addClass('help-block valid');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            $( $(element).data('icon-valid-bind') ).removeClass('ico-validate-error').addClass('ico-validate-success');
        },
    });

	$('#codigoProposta').focus();

    $('#form-codigo-proposta').on('submit',function(){
        return false;
    });
    
	$('#codigoProposta').keyup(function(){
        
        if( $('#codigoProposta').valid() ){

    	    delay(function(){
    	    	
                var parceirosIds  = [];

                $('input.itens_pgto_aprovados_arr').each(function() {
                    parceirosIds.push( $(this).val() );
                });

                $.ajax({
                    url     : '/proposta/getPropostaCodigo/',
                    type    : 'POST',
                    data    : {
                        codigo              : $('#codigoProposta').val(),
                        parceiroId          : $('#parceiroId').val(),
                        itensPgtoAprovados  : parceirosIds,
                        totalAtualGrid      : $('#totalAtualGrid').val(),
                        totalAtualGridFinan : $('#totalAtualGridFinan').val(),
                        porImagem           : 0
                    }
                }).done(function(dRt){

                    var retorno = $.parseJSON(dRt);
                    
                    if( retorno.encontrado )
                    {
                        $('#idPropostaHdn').val(retorno.gridAprovado.id);
                        $('#codigoPropostaHdn').val(retorno.gridAprovado.codigo);
                        $('#cpfClienteHdn').val(retorno.gridAprovado.cpf);
                        $('#nomeClienteHdn').val(retorno.gridAprovado.nome);
                        $('#valorFinanciadoHdn').val(retorno.gridAprovado.valFin);
                        $('#valorRepasseHdn').val(retorno.gridAprovado.valRep);
                        $('#valorFinanciadoDoubleHdn').val(retorno.valFinDouble);
                        $('#valorRepasseDoubleHdn').val(retorno.valRepDouble);

                        $('#totalAtualGridFinan').attr('value',     retorno.totalFinanciado );
                        $('#totalAtualGrid').attr('value',          retorno.totalParcial );

                        $('#count_repasse').html( "R$ " + retorno.totalParcialText );
                        $('#count_finan').html( "R$ " + retorno.totalFinanText );

                        $('#form-propostas-pgto-aprovado').append('<input type="hidden" value="' + retorno.gridAprovado.id + '" id="item_'+retorno.gridAprovado.id+'_id" name="itens_pgto_aprovados[]" class="itens_pgto_aprovados_arr">');

                        $('#modal_form_obs').modal('show');

                        $('#obs_content').focus();

                        if($("#" + 'filial_'+retorno.idFilial).length == 0) {
                            $('#form-propostas-pgto-aprovado').append('<input type="hidden" value="' + retorno.idFilial + '" id="filial_' + retorno.idFilial + '" name="filiaisProcesso[]" class="filiais_processo_arr">');
                        }
                    }
                    else
                    {
                        $.pnotify({
                            title   : retorno.pnotify.titulo,
                            text    : retorno.pnotify.texto,
                            type    : retorno.pnotify.tipo
                        });
                    }

                    $('#codigoProposta').val('');
                });
    	    }, 1000 );
        }

        else
        {
            $('#codigoProposta').val("");
        }
	});

    var gridPgtosAprovados = $('#grid_pgtos_aprovados').DataTable({
        "drawCallback" : function(settings)
        {   
            if( $('#grid_pgtos_aprovados tbody tr[role="row"]').length < 1  )
            {
                $('#btn-concluir-grid').hide();
            }
            else
            {
                $('#btn-concluir-grid').show();
            }
        }
    });

    $('#form-add-obs').validate({

        submitHandler :  function(){

            gridPgtosAprovados.row.add([                
                $('#codigoPropostaHdn').val(),
                $('#cpfClienteHdn').val(),
                $('#nomeClienteHdn').val(),
                $('#valorFinanciadoHdn').val(),
                $('#valorRepasseHdn').val(),
                $('#obs_content').val(),
                '<a data-obs-item="item_'+$('#idPropostaHdn').val()+'_obs" data-id-prop="item_'+$('#idPropostaHdn').val()+'_id" data-valor-fin="'+$('#valorFinanciadoDoubleHdn').val()+'" data-valor-repasse="'+$('#valorRepasseDoubleHdn').val()+'" href="#" class="btn btn-xs btn-bricky btn-remover-item"><i class="fa fa-times fa fa-white"></i></a>'
            ]).draw();
            
            $('#form-propostas-pgto-aprovado').append('<input type="hidden" value="'+$('#obs_content').val()+'" id="item_'+$('#idPropostaHdn').val()+'_obs" name="itens_pgto_aprovados_obs[]" class="itens_pgto_aprovados_obs_arr">');

            $('#obs_content').val("");
            $('#modal_form_obs').modal('hide');
            $('#codigoProposta').focus();
            return false;
        }
    });

    $(document).on('click', '.btn-remover-item',function(){

        var valorRepasseAtual = parseFloat( $('#totalAtualGrid').val() );
        var valorRepassePropo = $(this).data('valor-repasse');

        var valorFinanciAtual = parseFloat( $('#totalAtualGridFinan').val() );
        var valorFinanciPropo = $(this).data('valor-fin');
        
        $('#'+$(this).data('id-prop')).remove();
        $('#'+$(this).data('obs-item')).remove();
        gridPgtosAprovados.row( $(this).parent().parent().remove() ).remove().draw();

        if( $('#grid_pgtos_aprovados tbody tr[role="row"]').length < 1  )
        {
            $('#totalAtualGrid').attr('value', 0);
            $('#totalAtualGridFinan').attr('value', 0);
            $('#count_repasse').html("R$ " + number_format( 0 ,2, ',', '.' ) );
            $('#count_finan').html("R$ " + number_format( 0 ,2, ',', '.' ) );
        }
        else
        {
            $('#totalAtualGrid').attr('value', valorRepasseAtual-valorRepassePropo);
            $('#count_repasse').html("R$ " + number_format( valorRepasseAtual-valorRepassePropo ,2, ',', '.' ) );

            $('#totalAtualGridFinan').attr('value', valorFinanciAtual-valorFinanciPropo);
            $('#count_finan').html("R$ " + number_format( valorFinanciAtual-valorFinanciPropo ,2, ',', '.' ) );
        }
        
        return false;
    });

    $('#modal_form_obs').on('shown.bs.modal', function (e){
        $('#obs_content').focus();
    });

    $('#btn-concluir-grid').on('click',function(){

        $('#modal_confirm').modal('show');

        if( $('#grid_pgtos_aprovados tbody tr[role="row"]').length == 1 )
        {
            $('#qtd_itens').text( $('#grid_pgtos_aprovados tbody tr[role="row"]').length  + ' Item será marcado como pendente');
        }
        else
        {
            $('#qtd_itens').text( $('#grid_pgtos_aprovados tbody tr[role="row"]').length  + ' Itens serão marcados como pendentes');
        }

        return false;
    });
    
    $('#btn-concluir').on('click',function(){
        $(this).attr('disabled', 'disabled');
        $('#btn_action').attr('value', 'concluir');
        $('#form-propostas-pgto-aprovado').submit();
    });

    $('#btn-concluir-aut-pgto').on('click',function(){
        $('#btn_action').attr('value', 'aut_pgto');
        $('#form-propostas-pgto-aprovado').submit();
    });

});