/*Função para evitar que o keyup envie uma requisição ajax para cada digito escrito no input do codigo*/
var delay = (function () {
   var timer = 0;
   return function (callback, ms) {
      clearTimeout(timer);
      timer = setTimeout(callback, ms);
   };
})();

/*Bloqueia o Ctrl J no (apenas para google chrome)*/
function bloquear_ctrl_j() {

   if (window.event.ctrlKey && window.event.keyCode == 74)
   {
      event.keyCode = 0;
      event.returnValue = false;
   }
}

$(function () {

   var gridPgtosAprovados = $('#grid_pgtos_aprovados').DataTable({
      "drawCallback": function (settings)
      {
         if ($('#grid_pgtos_aprovados tbody tr[role="row"]').length < 1)
         {
            $('#btn-concluir-grid').hide();
         }
         else
         {
            $('#btn-concluir-grid').show();
         }
      }
   });

   $.validator.setDefaults({
      errorElement: "span",
      errorClass: 'help-block',
      highlight: function (element) {
         $(element).closest('.help-block').removeClass('valid');
         $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
         $($(element).data('icon-valid-bind')).removeClass('ico-validate-success').addClass('ico-validate-error');
      },
      unhighlight: function (element) {
         $(element).closest('.form-group').removeClass('has-error');
      },
      success: function (label, element) {
         label.addClass('help-block valid');
         $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
         $($(element).data('icon-valid-bind')).removeClass('ico-validate-error').addClass('ico-validate-success');
      },
   });

   $('#codigoProposta').focus();

   $('#form-codigo-proposta').on('submit', function () {
      return false;
   });

   $('#codigoProposta').keyup(function () {

      if ($('#codigoProposta').valid()) {
         delay(function () {

            var parceirosIds = [];

            $('input.itens_pgto_aprovados_arr').each(function () {
               parceirosIds.push($(this).val());
            });

            var request = $.ajax({
               url: '/proposta/getPropostaCodigo/',
               type: 'POST',
               data: {
                  codigo: $('#codigoProposta').val(),
                  parceiroId: $('#parceiroId').val(),
                  itensPgtoAprovados: parceirosIds,
                  totalAtualGrid: $('#totalAtualGrid').val(),
                  totalAtualGridFinan: $('#totalAtualGridFinan').val()
               }
            });

            request.done(function (dRt) {

               var retorno = $.parseJSON(dRt);
               var ok      = true            ;

               if( typeof retorno.pnotify != 'undefined' )
               {
                  $.pnotify({
                     title: retorno.pnotify.titulo,
                     text: retorno.pnotify.texto,
                     type: retorno.pnotify.tipo
                  });
               }


               if (typeof retorno.pendente != 'undefined') {

                  if( retorno.pendente == 1 )
                  {
                     $('#modal_retirar_pendencia').modal('show');
                  }
               }

               
               if (retorno.encontrado)
               {
                  if (typeof retorno.pnotify !== "undefined" && retorno.pnotify.tipo === "alert") {
                     
                     //exibe a mensagem de pergunta sobre a retirada da pendencia
                     if (confirm(retorno.pnotify.texto))
                     {
                        
                        //caso o usuario clique em ok, chame um ajax para tirar a pendencia
                        $.ajax(
                           {
                              url: '/proposta/tiraPendencia/', 
                              type: 'POST',
                              data: {
                                 codigo: $('#codigoProposta').val(),
                              }
                        }
                        ).done(function (dRt) 
                        {
                           ret   = $.parseJSON(dRt); //trate o retorno
                           ok    = ret.retorno     ; //atribua a variavel de controle
                        });
                  
                     }
                     else
                     {
                        ok = false;

                        $.pnotify({
                           title : retorno.pnotify.titulo,
                           text  : "Proposta pendente"   ,
                           type  : "error"
                        });
                     }
                  }
                  
                  if (ok)
                  {
                     if($("#" + 'filial_'+retorno.idFilial).length == 0) {
                        $('#form-propostas-pgto-aprovado').append('<input type="hidden" value="' + retorno.idFilial + '" id="filial_' + retorno.idFilial + '" name="filiaisProcesso[]" class="filiais_processo_arr">');
                     }

                     gridPgtosAprovados.row.add([
                        retorno.gridAprovado.codigo,
                        retorno.gridAprovado.cpf,
                        retorno.gridAprovado.filial,
                        retorno.gridAprovado.nome,
                        retorno.gridAprovado.valFin,
                        retorno.gridAprovado.valRep,
                        retorno.gridAprovado.btnRem,
                     ]).draw();

                     $('#form-propostas-pgto-aprovado').append('<input type="hidden" value="' + retorno.gridAprovado.id + '" id="item_' + retorno.gridAprovado.id + '_id" name="itens_pgto_aprovados[]" class="itens_pgto_aprovados_arr">');
                     $('#totalAtualGrid').attr('value', retorno.totalParcial);
                     $('#totalAtualGridFinan').attr('value', retorno.totalFinanciado);

                     $('#count_repasse').html("R$ " + retorno.totalParcialText);
                     $('#count_finan').html("R$ " + retorno.totalFinanText);
                  }
               }

               if( typeof retorno.incluido != 'undefined' )
               {
                  $('#codigoProposta').val("");
               }
               
               $('#codigoProposta').focus();
            });
         }, 1000);
      }

      else
      {
         $('#codigoProposta').val("");
      }

   });
   
   $('#btn-retirar-pendencia').on('click',function(){
      $.ajax({
         url: '/proposta/tiraPendencia/', 
         type: 'POST',
         data: {
            codigo: $('#codigoProposta').val(),
         }
      })
      .done(function (dRt){

         var retorno   = $.parseJSON(dRt);         

         $('#modal_retirar_pendencia').modal('hide');
         

         $.pnotify({
            title: "Pendência retirada",
            text: "Por favor, bipe novamente para incluir no borderô",
            type: "success"
         });

         $('#codigoProposta').focus();
         $('#codigoProposta').val('');

      });

      return false;
   });

   $(document).on('click', '.btn-remover-item', function () {

      var valorRepasseAtual = parseFloat($('#totalAtualGrid').val());
      var valorRepassePropo = $(this).data('valor-repasse');

      var valorFinanciAtual = parseFloat($('#totalAtualGridFinan').val());
      var valorFinanciPropo = $(this).data('valor-fin');

      $('#' + $(this).data('id-prop')).remove();
      gridPgtosAprovados.row($(this).parent().parent().remove()).remove().draw();

      if ($('#grid_pgtos_aprovados tbody tr[role="row"]').length < 1)
      {
         $('#totalAtualGrid').attr('value', 0);
         $('#totalAtualGridFinan').attr('value', 0);
         $('#count_repasse').html("R$ " + number_format(0, 2, ',', '.'));
         $('#count_finan').html("R$ " + number_format(0, 2, ',', '.'));
      }
      else
      {
         $('#totalAtualGrid').attr('value', valorRepasseAtual - valorRepassePropo);
         $('#count_repasse').html("R$ " + number_format(valorRepasseAtual - valorRepassePropo, 2, ',', '.'));

         $('#totalAtualGridFinan').attr('value', valorFinanciAtual - valorFinanciPropo);
         $('#count_finan').html("R$ " + number_format(valorFinanciAtual - valorFinanciPropo, 2, ',', '.'));
      }

      return false;
   });

   $('#btn-concluir-grid').on('click', function () {

      $('#modal_confirm').modal('show');

      if ($('#grid_pgtos_aprovados tbody tr[role="row"]').length == 1)
      {
         $('#qtd_itens').text($('#grid_pgtos_aprovados tbody tr[role="row"]').length + ' Item será encaminhado para pagamento');
      }
      else
      {
         $('#qtd_itens').text($('#grid_pgtos_aprovados tbody tr[role="row"]').length + ' Itens serão encaminhados para pagamento');
      }

      return false;
   });

   $('#btn-concluir').on('click', function () {
      $('#btn_action').attr('value', 'concluir');
      $('#form-propostas-pgto-aprovado').submit();
   });

   $('#btn-concluir-reg-pendencia').on('click', function () {
      $('#btn_action').attr('value', 'registrar_pendencias');
      $('#form-propostas-pgto-aprovado').submit();
   });
   
});