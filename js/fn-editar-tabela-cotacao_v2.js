$(document).ready(function(){

    $('#filiais_select').multiselect({

      buttonWidth       : '200px',
      numberDisplayed   : 2,
      enableFiltering   : true,
      enableCaseInsensitiveFiltering: true,

      buttonText:function(options, select){

         if (options.length == 0) {
            return 'Selecionar Parceiros <b class="caret"></b>'
         }
         else if (options.length == 1) {
            return '1 Parceiro Selecionado <b class="caret"></b>'
         }
         else{
            return options.length + ' Parceiros Selecionados <b class="caret"></b>'
         }
      },

      onChange:function(element, checked){
            
        var request = $.ajax({
            type  : "POST",
            url   : "/tabelaCotacao/changeParceiroRelation/",
            data  : { parceiroId:element[0]['value'], check:checked, tabelaId: $('#TabelaId').val() }
        });

        request.done(function(dRt){
            //console.log( $.parseJSON( dRt ) );
        });

      }


    });

    $('#carencias_select').multiselect({
        buttonWidth         : '270px',
        enableFiltering     : true,
    });

    $('#btn_modal_form_new_fator').on('click',function(){
        $("#cadastro_msg_return").hide().empty();
    })

    var gridFatores = $('#grid_fatores').DataTable(
    {
        
        "processing"    : true,
        "serverSide"    : true,
        "ajax"          : 
        {
            url         : '/fator/getFatoresTabela/',
            type        : 'POST'                    ,
            "data"      : function (d) 
            {
                d.TabelaId = $("#TabelaId").val()
            }
        },
        "columns":
        [
            { 
                "data"  : "carencia" 
            },
            { 
                "data"  : "fator" 
            },
            { 
                "data"  : "fatorIOF"    ,
                "class" : "tdFatorIOF"
            },
            { 
                "data"  : "retencao" 
            },
            { 
                "data"  : "parcela" 
            }
        ],
        "columnDefs" : [
            {
                "orderable" : false,
                "targets"   : "no-orderable"
            }            
        ],    
    });
    
    /*Validação formulário*/
    $.validator.setDefaults({
        errorElement: "span", 
        errorClass: 'help-block',
        
        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group
        },
            
        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group
        },

        success: function (label, element) {
            label.addClass('help-block valid');
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        }
    })

    $("#form-add-fator").validate({

        ignore: ':hidden:not(".multipleselect")',

        rules       : {
            'Fator[fator]' :{
                number : true
            },
            'Fator[carencia]' :{
                number : true
            },
            'Fator[parcela]' :{
                number : true
            }
        },
        
        messages    : {
            'Fator[carencia]' :{
                required: 'Selecione uma carência'
            },
        },

        submitHandler   :   function(){

            $("#cadastro_msg_return").hide().empty();

            var formData = $('#form-add-fator').serialize();

            $.ajax({
                
                type        : "POST",
                url         : "/fator/add/",
                data        : formData,

                beforeSend  : function(){

                    $('.panel').block({
                        overlayCSS: {
                            backgroundColor: '#fff'
                        },
                        message: '<img src="https://beta.sigacbr.com.br/js/loading.gif" /> Salvando fator...',
                        css: {
                            border: 'none',
                            color: '#333',
                            background: 'none'
                        }
                    });
                    window.setTimeout(function () {
                        $('.panel').unblock();
                    }, 1000);
                }

            })

            .done(function(dRt){

                var retorno = $.parseJSON( dRt );

                if( !$('#checkbox_continuar').is(':checked') )
                {
                    $('#modal_form_new_fator').modal('hide');
                    $('#form-add-fator').trigger("reset");

                    $.pnotify({
                        title    : 'Notificação',
                        text     : retorno.msg,
                        type     : retorno.pntfyClass
                    });
                }

                else
                {
                    $("#cadastro_msg_return").prepend('<p>'+retorno.msg+'</p>').fadeIn(100).addClass('alert-success').fadeOut(9000);
                }
                
                $('.has-success').each(function(index){
                    $(this).removeClass('has-success');
                })

                $('.ok').each(function(index){
                    $(this).removeClass('ok');
                })
                
                $('#form-add-fator').trigger("reset");

                gridFatores.draw();
                
                
                //$('#carencias_select').multiselect("refresh");
            })

            return false;
        }
    });
    

    //////////////////////////////////////////////////////////////////////////
    // funcao que irá capturar o click duplo na célula de fatorIOF e irá    //
    // habilitar para edição da mesma. caso a mesma sofra alguma alteração, //
    // salve o conteúdo no banco                                            //
    //////////////////////////////////////////////////////////////////////////
    // Autor : André Willams // Data : 11-07-2015 ////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    $(document).on('dblclick', '.tdFatorIOF', function () 
    {

        var conteudoOriginal    = $(this).text()        ; //resgate o conteúdo atual da célula
        
        var tr                  = $(this).closest('tr') ;
        var row                 = gridFatores.row(tr)   ;
        var rowData             = row.data()            ;
        var idFator             = rowData.idFator       ;
        
        var elemento            = $(this)               ;

        //transforme o elemento em um input
        $(this).html("<input id='mudaFatorIOF' class='form-control' type='text' value='" + conteudoOriginal + "' />");

        $(this).children().first().focus(); //atribua o foco ao elemento criado

        $(this).children().first().keypress(function (e) 
        { //quando alguma tecla for pressionada

            if (e.which == 13) 
            { //caso a tecla pressionada seja o ENTER, foi confirmada a alteração

                var novoConteudo = $(this).val(); //pegue o novo conteúdo

                //chame o ajax de alteração
                $.ajax(
                {
                    type    : "POST"                    ,
                    url     : "/fator/mudarFatorIOF"    ,
                    data    : 
                    {
                        "idFator"   : idFator       ,
                        "fatorIOF"  : novoConteudo  
                    },
                }).done(function (dRt) 
                {

                    var retorno = $.parseJSON(dRt);

                    $.pnotify(
                    {
                        title   : 'Notificação'         ,
                        text    : retorno.msg           ,
                        type    : retorno.pntfyClass
                    });

                    if(!retorno.hasErrors)
                    {
                        elemento.text(novoConteudo);
                    }
                    
                })

            }
        });

        //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
        $(this).children().first().blur(function () 
        {

            //devolva o conteúdo original
            $(this).parent().text(conteudoOriginal);
            

        });

    });
    
});