$(document).ready(function () {
    $('.form-inline').on('submit', function(){
        return false;
    });
    
    $('.form-horizontal').on('submit', function(){
        return false;
    });
    
    var tableParceiros = $('#grid_parceiros').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            type: 'POST',
            url: '/generico/listarParceiros/',
            data:function(d){
                d.statusFiltro = $('#selectSFilter').val()
            }
        },
        "columns": [
            {"data": "nome"},
            {"data": "cidade"},
            {"data": "email"},
            {"data": "tel"},
            {"data": "status"},
            {"data": "obs"}
        ]
    });
    
    $('#selectSFilter').on('change', function(){
        tableParceiros.draw();
    });
    
    $(document).on('change', '.selectSt', function(){
        var lojaID = $(this).parent().find('input').val();
        $.ajax({
            type: 'POST',
            url: '/generico/salvarStatus',
            data: {
                idStatus: $(this).val(),
                idLoja: lojaID
            }
        }).done(function (dRt) {

            var jsonReturn = $.parseJSON(dRt);
            $.pnotify({
                title: "Informações",
                text: jsonReturn.msg,
                type: jsonReturn.type
            });
            
            if(!jsonReturn.error){
                tableParceiros.draw();
            }
        });
    });
    
    $('#cadastrar_ploja').on('click', function () {
        $.ajax({
            type: 'POST',
            url: '/generico/novaLoja',
            data: {
                nomeLoja: $('#nomeParceiro').val(),
                cidLoja: $('#cidLoja').val(),
                emailLoja: $('#emailLoja').val(),
                telLoja: $('#telLoja').val()
            }
        }).done(function (dRt) {

            var jsonReturn = $.parseJSON(dRt);
            $.pnotify({
                title: "Informações",
                text: jsonReturn.msg,
                type: jsonReturn.type
            });
            
            if(!jsonReturn.error){
                tableParceiros.draw();
                
                $('#nomeParceiro').val('');
                $('#cidLoja').val('');
                $('#emailLoja').val('');
                $('#telLoja').val('');
                
                $('#cadastro-ploja').modal('hide');
            }
        });
    });
    
    var idToObs;
    
    $(document).on('click', '#edit_obs', function(){
        $('#obs-ploja').modal('show');
        $('#area_obs').val($(this).parent().find('label').text());
        idToObs = $(this).parent().find('input').val();
    });
    
    $('#altobs_ploja').on('click', function(){
        $.ajax({
            type: 'POST',
            url: '/generico/EditObs',
            data: {
                alt_obs: $('#area_obs').val(),
                plID: idToObs
            }
        }).done(function (dRt) {

            var jsonReturn = $.parseJSON(dRt);
            $.pnotify({
                title: "Informações",
                text: jsonReturn.msg,
                type: jsonReturn.type
            });
            
            if(!jsonReturn.error){
                tableParceiros.draw();
                $('#area_obs').val('');
                $('#obs-ploja').modal('hide');
            }
        });
    });
    
    $('#novaLoja').on('click', function () {
        if($('#nomeParceiro').val() != ''){
            $('#cadastro-ploja').modal('show');
        }else{
            alert("Digite o nome da loja...");
        }
    });
    
    $('#novoStatus').on('click', function () {
        $.ajax({
            type: 'POST',
            url: '/generico/novoStatus',
            data: {
                nomeStatus: $('#nomeStatus').val()
            }
        }).done(function (dRt) {

            var jsonReturn = $.parseJSON(dRt);
            $.pnotify({
                title: "Informações",
                text: jsonReturn.msg,
                type: jsonReturn.type
            });
            
            if(!jsonReturn.error){
                tableParceiros.draw();
                $('#nomeStatus').val('');
            }
        });
    });
});