$(function(){

	$.bloquearInterface = function(mensagem){
		$.blockUI({
	    	message: mensagem,
	        css: { 
	            borderColor: '#d6e9c6', 
	            padding: '15px', 
	            backgroundColor: '#dff0d8', 
	            '-webkit-border-radius': '4px', 
	            '-moz-border-radius': '4px', 
	            opacity: .5, 
	        	color: '#3c763d' 
	        } 
	    });
    }

    $.desbloquearInterface = function(){
        $.unblockUI();    	
    }
})