function format ( d ) {
    return $.each(d.analises, function(index){
                
    })    
}

$(function(){

	var grid_analises = $('#grid_analises').DataTable({
		
		"processing": true,
        "serverSide": true,
        
        "ajax": {
            url: '/crediarista/listAnalises/',
            type: 'POST'            
        },

        "columns":[
            { 
                "class"             :"details-control",
                "orderable"         : false,
                "data"              : null,
                "defaultContent"    : ""
            },
        	{ "data"                : "codigo"},
        	{ "data"                : "cliente"},
        	{ "data"                : "valor"},
        	{ "data"                : "entrada"},
        	{ "data"                : "data"},
        ]

	});

    var detailRows = [];

    $('#grid_analises tbody').on('click', 'tr td:first-child', function(){

        var tr      = $(this).closest('tr');
        var row     = grid_analises.row( tr );
        var idx     = $.inArray( tr.attr('id'), detailRows );

        if ( row.child.isShown() ) 
        {
            tr.removeClass( 'details' );
            row.child.hide();
            detailRows.splice( idx, 1 );
        }

        else 
        {
            
            tr.addClass( 'details' );
            row.child( format( row.data() ) ).show();
            
            if ( idx === -1 )
            {
                detailRows.push( tr.attr('id') );
            }
        }

    });

    grid_analises.on('draw', function(){

         $.each(detailRows, function ( i, id ) {
            $('#'+id+' td:first-child').trigger( 'click' );
         });

    });

});


// Load the Visualization API and the piechart package.
google.load('visualization', '1.0', {'packages':['corechart']});

// Set a callback to run when the Google Visualization API is loaded.
google.setOnLoadCallback(drawChart);

jQuery.fn.getAnalisesChartInfo = function(){

    return $.ajax({

        url     : '/proposta/propostasCrediaristaCharts/',
        type    : 'POST',
        async:    false,
        data    :{
            userId:$('#userId').val()
        }

    }).done(function( dRt ){        
        
    })    
}


function drawChart()
{
    var data    = new google.visualization.DataTable();
    var reqXhr  = $(document).getAnalisesChartInfo();
    
    reqXhr      = $.parseJSON(reqXhr.responseText);

    console.log(reqXhr);

    data.addColumn('string', 'Topping');
    data.addColumn('number', 'Slices');
    data.addRows([
        ['Em Análise', reqXhr.em_analise],
        ['Cancelados', reqXhr.cancelados],
        ['Aguardando análise', reqXhr.aguardando_analise],
        ['Aprovado', reqXhr.aprovados],
    ]);

    // Set chart options
    var options = {
        'title':'Porcentagem dos Status da Propostas',
        'width':400,
        'height':300
    };

    // Instantiate and draw our chart, passing in some options.
    var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
    chart.draw(data, options);
}