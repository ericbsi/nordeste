$(document).ready(function () {

   var nucleosTable = $('#grid_nucleo').DataTable({
      "processing": true,
      "serverSide": true,
      "ajax":
              {
                 url: '/grupoFiliais/listarNucleos',
                 type: 'POST',
              },
      "columns": [
         {
            "data"      : "filiais"             ,
            "className" : "tableFiliais"        ,
         },
         {
            "data"      : "idNucleo"            ,
         },
         {
            "data"      : "nomeGrupo"           ,
            "className" : "tdGrupo"             ,
         },
         {
            "data"      : "nome"                ,
            "className" : "tdNome"              ,
         },
         {
            "data"      : "banco"               ,
            "className" : "tdBanco"             ,
         },
         {
            "data"      : "agencia"             ,
            "className" : "tdAgencia"           ,
         },
         {
            "data"      : "conta"               ,
            "className" : "tdConta"             ,
         },
         {
            "data"      : "operacao"            ,
            "className" : "tdOperacao"          ,
         },
         {
            "data"      : "cgcDadosBancarios"   ,
            "className" : "tdCGC"               ,
         },
         {"data": "btn"},
      ],
   });

   $('.grupoFilial').select2();

   $('.bancoNucleo').select2();

   $('#btnSalvar').on('click', function () {

      $.ajax({
         type: "POST",
         url: "/grupoFiliais/salvarNucleo/",
         data: {
            "grupoFilial"     : $('#grupoFilial'      ).val()  ,
            "nomeNucleo"      : $('#inputNomeNucleo'  ).val()  ,
            "bancoNucleo"     : $('#bancoNucleo'      ).val()  ,
            "agenciaNucleo"   : $('#inputAgencia'     ).val()  ,
            "contaNucleo"     : $('#inputConta'       ).val()  ,
            "operacaoNucleo"  : $('#inputOperacao'    ).val()
         }
      }).done(function (dRt) {

         var retorno = $.parseJSON(dRt);

         $.pnotify({
            title: 'Notificação',
            text: retorno.msg,
            type: retorno.pntfyClass
         });

         if (!(retorno.hasErrors)) {
            $('#grupoFilial').val(0).change();
            $('#bancoNucleo').val(0).change();
            $('#inputNomeNucleo').val('');
            $('#inputAgencia').val('');
            $('#inputConta').val('');
            $('#inputOperacao').val('');
            nucleosTable.draw();
         }

      })

   });

   //////////////////////////////////////////////////////////////////////////
   // funcao que irá capturar o click duplo na célula de senha e irá       //
   // habilitar para edição da mesma. caso a mesma sofra alguma alteração, //
   // salve o conteúdo no banco                                            //
   //////////////////////////////////////////////////////////////////////////
   // Autor : André Willams // Data : 06-07-2015 ////////////////////////////
   //////////////////////////////////////////////////////////////////////////
   $(document).on('dblclick', '.tdNome', function () {

      var conteudoOriginal = $(this).text(); //resgate o conteúdo atual da célula

      var idNucleo = $(this).context.parentElement.cells[1].textContent; //pegue o id do Nucleo

      var elemento = $(this);

      //transforme o elemento em um input
      $(this).html("<input id='mudaNome' class='form-control' type='text' value='" + conteudoOriginal + "' />");

      $(this).children().first().focus(); //atribua o foco ao elemento criado

      $(this).children().first().keypress(function (e) { //quando alguma tecla for pressionada

         if (e.which == 13) { //caso a tecla pressionada seja o ENTER, foi confirmada a alteração

            var novoConteudo = $(this).val(); //pegue o novo conteúdo

            //chame o ajax de alteração
            $.ajax({
               type: "POST",
               url: "/grupoFiliais/mudarNomeNucleo",
               data: {
                  "idNucleo": idNucleo,
                  "nomeNucleo": novoConteudo
               },
            }).done(function (dRt) {

               var retorno = $.parseJSON(dRt);

               $.pnotify({
                  title: 'Notificação',
                  text: retorno.msg,
                  type: retorno.pntfyClass
               });

               if (!retorno.hasErrors)
               {
                  elemento.text(novoConteudo);
               }

            })

         }
      });

      //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
      $(this).children().first().blur(function () {

         //devolva o conteúdo original
         $(this).parent().text(conteudoOriginal);

      });

   });

   //////////////////////////////////////////////////////////////////////////
   // funcao que irá capturar o click duplo na célula de senha e irá       //
   // habilitar para edição da mesma. caso a mesma sofra alguma alteração, //
   // salve o conteúdo no banco                                            //
   //////////////////////////////////////////////////////////////////////////
   // Autor : André Willams // Data : 06-07-2015 ////////////////////////////
   //////////////////////////////////////////////////////////////////////////
   $(document).on('dblclick', '.tdAgencia', function () {

      var conteudoOriginal = $(this).text(); //resgate o conteúdo atual da célula

      var idNucleo = $(this).context.parentElement.cells[1].textContent; //pegue o id do Nucleo

      var elemento = $(this);

      //transforme o elemento em um input
      $(this).html("<input id='mudaAgencia' class='form-control' type='text' value='" + conteudoOriginal + "' />");

      $(this).children().first().focus(); //atribua o foco ao elemento criado

      $(this).children().first().keypress(function (e) { //quando alguma tecla for pressionada

         if (e.which == 13) { //caso a tecla pressionada seja o ENTER, foi confirmada a alteração

            var novoConteudo = $(this).val(); //pegue o novo conteúdo

            //chame o ajax de alteração
            $.ajax({
               type: "POST",
               url: "/grupoFiliais/mudarAgenciaNucleo",
               data: {
                  "idNucleo"        : idNucleo     ,
                  "agenciaNucleo"   : novoConteudo
               },
            }).done(function (dRt) {

               var retorno = $.parseJSON(dRt);

               $.pnotify({
                  title: 'Notificação',
                  text: retorno.msg,
                  type: retorno.pntfyClass
               });

               if (!retorno.hasErrors)
               {
                  elemento.text(novoConteudo);
               }

            })

         }
      });

      //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
      $(this).children().first().blur(function () {

         //devolva o conteúdo original
         $(this).parent().text(conteudoOriginal);

      });

   });

   //////////////////////////////////////////////////////////////////////////
   // funcao que irá capturar o click duplo na célula de senha e irá       //
   // habilitar para edição da mesma. caso a mesma sofra alguma alteração, //
   // salve o conteúdo no banco                                            //
   //////////////////////////////////////////////////////////////////////////
   // Autor : André Willams // Data : 06-07-2015 ////////////////////////////
   //////////////////////////////////////////////////////////////////////////
   $(document).on('dblclick', '.tdConta', function () {

      var conteudoOriginal = $(this).text(); //resgate o conteúdo atual da célula

      var idNucleo = $(this).context.parentElement.cells[1].textContent; //pegue o id do Nucleo

      var elemento = $(this);

      //transforme o elemento em um input
      $(this).html("<input id='mudaConta' class='form-control' type='text' value='" + conteudoOriginal + "' />");

      $(this).children().first().focus(); //atribua o foco ao elemento criado

      $(this).children().first().keypress(function (e) { //quando alguma tecla for pressionada

         if (e.which == 13) { //caso a tecla pressionada seja o ENTER, foi confirmada a alteração

            var novoConteudo = $(this).val(); //pegue o novo conteúdo

            //chame o ajax de alteração
            $.ajax({
               type: "POST",
               url: "/grupoFiliais/mudarContaNucleo",
               data: {
                  "idNucleo"     : idNucleo     ,
                  "contaNucleo"  : novoConteudo
               },
            }).done(function (dRt) {

               var retorno = $.parseJSON(dRt);

               $.pnotify({
                  title: 'Notificação',
                  text: retorno.msg,
                  type: retorno.pntfyClass
               });

               if (!retorno.hasErrors)
               {
                  elemento.text(novoConteudo);
               }

            })

         }
      });

      //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
      $(this).children().first().blur(function () {

         //devolva o conteúdo original
         $(this).parent().text(conteudoOriginal);

      });

   });

   //////////////////////////////////////////////////////////////////////////
   // funcao que irá capturar o click duplo na célula de senha e irá       //
   // habilitar para edição da mesma. caso a mesma sofra alguma alteração, //
   // salve o conteúdo no banco                                            //
   //////////////////////////////////////////////////////////////////////////
   // Autor : André Willams // Data : 06-07-2015 ////////////////////////////
   //////////////////////////////////////////////////////////////////////////
   $(document).on('dblclick', '.tdOperacao', function () {

      var conteudoOriginal = $(this).text(); //resgate o conteúdo atual da célula

      var idNucleo = $(this).context.parentElement.cells[1].textContent; //pegue o id do Nucleo

      var elemento = $(this);

      //transforme o elemento em um input
      $(this).html("<input id='mudaOperacao' class='form-control' type='text' value='" + conteudoOriginal + "' />");

      $(this).children().first().focus(); //atribua o foco ao elemento criado

      $(this).children().first().keypress(function (e) { //quando alguma tecla for pressionada

         if (e.which == 13) { //caso a tecla pressionada seja o ENTER, foi confirmada a alteração

            var novoConteudo = $(this).val(); //pegue o novo conteúdo

            //chame o ajax de alteração
            $.ajax({
               type: "POST",
               url: "/grupoFiliais/mudarOperacaoNucleo",
               data: {
                  "idNucleo"        : idNucleo     ,
                  "operacaoNucleo"  : novoConteudo
               },
            }).done(function (dRt) {

               var retorno = $.parseJSON(dRt);

               $.pnotify({
                  title: 'Notificação',
                  text: retorno.msg,
                  type: retorno.pntfyClass
               });

               if (!retorno.hasErrors)
               {
                  elemento.text(novoConteudo);
               }

            })

         }
      });

      //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
      $(this).children().first().blur(function () {

         //devolva o conteúdo original
         $(this).parent().text(conteudoOriginal);

      });

   });

   //////////////////////////////////////////////////////////////////////////
   // funcao que irá capturar o click duplo na célula de cgc e irá         //
   // habilitar para edição da mesma. caso a mesma sofra alguma alteração, //
   // salve o conteúdo no banco                                            //
   //////////////////////////////////////////////////////////////////////////
   // Autor : André Willams // Data : 25-11-2015 ////////////////////////////
   //////////////////////////////////////////////////////////////////////////
   $(document).on('dblclick', '.tdCGC', function () {

      var conteudoOriginal = $(this).text(); //resgate o conteúdo atual da célula

      var idNucleo = $(this).context.parentElement.cells[1].textContent; //pegue o id do Nucleo

      var elemento = $(this);

      //transforme o elemento em um input
      $(this).html("<input id='mudaCGC' class='form-control' type='text' value='" + conteudoOriginal + "' />");

      $(this).children().first().focus(); //atribua o foco ao elemento criado

      $(this).children().first().keypress(function (e) { //quando alguma tecla for pressionada

         if (e.which == 13) { //caso a tecla pressionada seja o ENTER, foi confirmada a alteração

            var novoConteudo = $(this).val(); //pegue o novo conteúdo

            //chame o ajax de alteração
            $.ajax({
               type: "POST",
               url: "/grupoFiliais/mudarCGCNucleoDB",
               data: {
                  "idNucleo"    : idNucleo     ,
                  "cgcNucleoDB" : novoConteudo
               },
            }).done(function (dRt) {

               var retorno = $.parseJSON(dRt);

               $.pnotify({
                  title : 'Notificação'         ,
                  text  : retorno.msg           ,
                  type  : retorno.pntfyClass
               });

               if (!retorno.hasErrors)
               {
                  elemento.text(novoConteudo);
               }

            })

         }
      });

      //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
      $(this).children().first().blur(function () {

         //devolva o conteúdo original
         $(this).parent().text(conteudoOriginal);

      });

   });

   //////////////////////////////////////////////////////////////////////////
   // funcao que irá capturar o click duplo na célula de senha e irá       //
   // habilitar para edição da mesma. caso a mesma sofra alguma alteração, //
   // salve o conteúdo no banco                                            //
   //////////////////////////////////////////////////////////////////////////
   // Autor : André Willams // Data : 06-07-2015 ////////////////////////////
   //////////////////////////////////////////////////////////////////////////
   $(document).on('dblclick', '.tdGrupo', function () {

      var conteudoOriginal = $(this).text(); //resgate o conteúdo atual da célula

      var idNucleo = $(this).context.parentElement.cells[1].textContent; //pegue o id do Nucleo

      var elemento = $(this);

      $.ajax
              (
                  {
                     type: "POST",
                     url: "/grupoFiliais/listarSelect",
                     data: {
                        "idNucleo": idNucleo,
                     },
                  }
              ).done(function (dRt)
      {

         var retorno = $.parseJSON(dRt);

         elemento.html(retorno.html);

         elemento.children().first().focus(); //atribua o foco ao elemento criado

         elemento.on
                 ('change', function ()
                 {

                    index = elemento.children()[0].selectedIndex;
                    idGrupo = elemento.children().val();

                    $.ajax
                            (
                                    {
                                       type: "POST",
                                       url: "/grupoFiliais/mudarGrupoNucleo",
                                       data: {
                                          "idNucleo": idNucleo,
                                          "idGrupo": idGrupo
                                       }
                                    }
                            ).done(function (dRt)
                    {

                       var retorno = $.parseJSON(dRt);

                       $.pnotify({
                          title: 'Notificação',
                          text: retorno.msg,
                          type: retorno.pntfyClass
                       });

                       if (!retorno.hasErrors)
                       {
                          elemento.text(elemento.children()[0][index].text);
                       }

                    }
                    );


                 }
                 );

         //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
         elemento.children().first().blur(function () {

            //devolva o conteúdo original
            elemento.text(conteudoOriginal);

         });

      }

      );

   });

   //////////////////////////////////////////////////////////////////////////
   // funcao que irá capturar o click duplo na célula de senha e irá       //
   // habilitar para edição da mesma. caso a mesma sofra alguma alteração, //
   // salve o conteúdo no banco                                            //
   //////////////////////////////////////////////////////////////////////////
   // Autor : André Willams // Data : 06-07-2015 ////////////////////////////
   //////////////////////////////////////////////////////////////////////////
   $(document).on('dblclick', '.tdBanco', function () {

      var conteudoOriginal = $(this).text(); //resgate o conteúdo atual da célula

      var idNucleo = $(this).context.parentElement.cells[1].textContent; //pegue o id do Nucleo

      var elemento = $(this);

      $.ajax
              (
                  {
                     type: "POST",
                     url: "/grupoFiliais/listarSelectBanco",
                     data: {
                        "idNucleo": idNucleo,
                     },
                  }
              ).done(function (dRt)
      {

         var retorno = $.parseJSON(dRt);

         elemento.html(retorno.html);

         elemento.children().first().focus(); //atribua o foco ao elemento criado

         elemento.on
                 ('change', function ()
                 {

                    index     = elemento.children()[0].selectedIndex;
                    idBanco   = elemento.children().val();

                    $.ajax
                            (
                                    {
                                       type: "POST",
                                       url: "/grupoFiliais/mudarBancoNucleo",
                                       data: {
                                          "idNucleo"  : idNucleo,
                                          "idBanco"   : idBanco
                                       }
                                    }
                            ).done(function (dRt)
                    {

                       var retorno = $.parseJSON(dRt);

                       $.pnotify({
                          title: 'Notificação',
                          text: retorno.msg,
                          type: retorno.pntfyClass
                       });

                       if (!retorno.hasErrors)
                       {
                          elemento.text(elemento.children()[0][index].text);
                       }

                    }
                    );


                 }
                 );

         //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
         elemento.children().first().blur(function () {

            //devolva o conteúdo original
            elemento.text(conteudoOriginal);

         });

      }

      );

   });

});