function bloquear_ctrl_j(){

    if (window.event.ctrlKey && window.event.keyCode == 74)
    {
        event.keyCode = 0;  
        event.returnValue = false;  
    }  
}

$(function ()
{   
    var marcadosLecca       = [];
    var marcadosRecebimento = [];
    
    $(document).ready(function()
    {
        myCheckboxIsChecked();
    });
    
    var qtdFiltradosLecca       ;
    var qtdFiltradosRecebidos   ;
    
    var focusCodigo = 0;
    
    var tableMalotes = $('#grid_malotes').DataTable(
            {
                "processing": true,
                "serverSide": true,
                "ajax":
                        {
                            url: '/recebimentoDeDocumentacao/listarMalotes/',
                            type: 'POST',
                            "data": function ()
                            {
                                
                            }
                        },
                "columns":
                        [
                            {
                                "data": "codigo",
                            },
                            {
                                "data": "dataProposta"
                            },
                            {
                                "data": "usuario"
                            },
                            {
                                "data": "btn_printer"
                            },
                        ],
            });

    var tableRecebimento = $('#grid_recebimento').DataTable(
            {
                "processing": true,
                "serverSide": true,
                "ajax":
                        {
                            url: '/recebimentoDeDocumentacao/listarItemBordero/',
                            type: 'POST',
                            "data": function (d)
                            {
                                d.codigo_filter = $("#codigo_filterLecca").val();
                                d.filial_filter = $("#filial_filterLecca").val();
                                d.nome_filter = $("#nome_filterLecca").val();
                                d.cpf_filter = $("#cpf_filterLecca").val();
                                d.data_filter = $("#data_filterLecca").val();
                                d.proLecca = $("#btnFiltroProLecca").val();
                                d.comJuros = $("#btnFiltroComJurosLecca").val();
                                d.semJuros = $("#btnFiltroSemJurosLecca").val();
                                d.propostasMarcadas = marcadosLecca;
                            }
                        },
                "columns":
                        [
                            {
                                "data": "check",
                            },
                            {
                                "data": "codigoProposta"
                            },
                            {
                                "data": "nomeFilial"
                            },
                            {
                                "data": "nomeCliente"
                            },
                            {
                                "data": "cpf"
                            },
                            {
                                "data": "dataProposta"
                            },
                            {
                                "data": "valorFinanciado"
                            },
                            {
                                "data": "valorRepasse"
                            },
                            {
                                "data": "pago"
                            },
                        ],
            });

    $("#codigo_filterLecca").focus  (               );

    tableRecebimento.on
    (
        "draw",
        function ()
        {

//            marcadosLecca       = tablePropostasLecca.data().ajax.json().marcados ;
            qtdFiltradosLecca = tableRecebimento.page.info().recordsDisplay;

            if (qtdFiltradosLecca == 1 && tableRecebimento.data().ajax.json().bipProposta !== "")
            {

                if ($.inArray(tableRecebimento.data().ajax.json().bipProposta, marcadosLecca) < 0)
                {
                    marcadosLecca.push(tableRecebimento.data().ajax.json().bipProposta);
                    
                    if(focusCodigo)
                    {
                        $("#codigo_filterLecca").val    (""             );
                        $("#codigo_filterLecca").attr   ("value"   ,""  );
                        $("#codigo_filterLecca").attr   ("text"    ,""  );
                        $("#codigo_filterLecca").focus  (               );
                    }
                    else
                    {
                        console.log("voti");
                    }
                    
                }

            }

            habilitaGerarLecca();

        }
    );
    
    var myCheckbox  = $("#myCheckbox").bootstrapSwitch('state',true, true);

    var tableRecebido = $('#grid_recebido').DataTable(
    {
        "processing": true,
        "serverSide": true,
        "ajax":
                {
                    url: '/recebimentoDeDocumentacao/listarRecebimentos/',
                    type: 'POST',
                    "data": function (d)
                    {
                        d.codigoFiltro          = $("#codigo_filtroMalote"  ).val() ;
                        d.filialFiltro          = $("#filial_filtroMalote"  ).val() ;
                        d.usuarioFiltro         = $("#usuario_filtroMalote" ).val() ;
                        d.dataFiltro            = $("#data_filtroMalote"    ).val() ;
                        d.recebimentosMarcados  = marcadosRecebimento               ;
                    }
                },
        "columns":
        [
            {
                "data": "check",
            },
            {
                "data": "codigo"
            },
            {
                "data": "nomeFilial"
            },
            {
                "data": "nomeUsuario"
            },
            {
                "data": "dataRecebimento"
            },
            {
                "data": "btnImprimir"
            },
        ],
    });

    tableRecebido.on
    (
        "draw",
        function ()
        {

            qtdFiltradosRecebidos = tableRecebido.page.info().recordsDisplay;

/*            if (qtdFiltradosRecebidos == 1 && tableRecebido.data().ajax.json().bipProposta !== "")
            {

                if ($.inArray(tableRecebido.data().ajax.json().bipProposta, marcadosRmarcadosRecebimento) <= 0)
                {
                    marcadosmarcadosRecebimento.push(tableRecebido.data().ajax.json().bipProposta);
                }

            }*/

            habilitaGerarRecebido();

        }
    );

    $(".select2").select2();

    $(document).on('change', '.input_filterLecca', function ()
    {
        
        if($(this).attr("id") == "codigo_filterLecca")
        {
            focusCodigo = 1;
        }
        else
        {
            focusCodigo = 0;
        }
        
        tableRecebimento.draw();
    });

    $(document).on('change', '.input_filtroMalote', function ()
    {
        tableRecebido.draw();
    });


    function habilitaGerarLecca()
    {

        if (marcadosLecca.length > 0)
        {

            $("#botaoGerarLecca").removeAttr("disabled");
        } else
        {
            $("#botaoGerarLecca").attr("disabled", "disabled");
        }

        $("#qtdMarcadas").text(marcadosLecca.length);

        $.ajax
                (
                        {
                            type: "POST",
                            url: "/integracao/getValoresSelecionadosLecca",
                            data:
                                    {
                                        propostasMarcadas: marcadosLecca
                                    },
                        }
                )
                .done
                (
                        function (dRt)
                        {

                            var retorno = $.parseJSON(dRt);

                            if (!retorno.hasErrors)
                            {

                                $("#valorFinanciado").text(retorno.valorFinanciado);
                                $("#valorRepasse").text(retorno.valorRepasse);

                            } else
                            {
                                $.pnotify
                                        (
                                                {
                                                    title: "Erro",
                                                    text: retorno.msg,
                                                    type: "error"
                                                }
                                        );
                            }

                        }
                );

    }

    function habilitaGerarRecebido()
    {

        if (marcadosRecebimento.length > 0)
        {

            $("#botaoGerarMalote").removeAttr("disabled");
        } else
        {
            $("#botaoGerarMalote").attr("disabled", "disabled");
        }

//        $("#qtdMarcadas").text(marcadosLecca.length);

/*        $.ajax
                (
                        {
                            type: "POST",
                            url: "/integracao/getValoresSelecionadosLecca",
                            data:
                                    {
                                        propostasMarcadas: marcadosLecca
                                    },
                        }
                )
                .done
                (
                        function (dRt)
                        {

                            var retorno = $.parseJSON(dRt);

                            if (!retorno.hasErrors)
                            {

                                $("#valorFinanciado").text(retorno.valorFinanciado);
                                $("#valorRepasse").text(retorno.valorRepasse);

                            } else
                            {
                                $.pnotify
                                        (
                                                {
                                                    title: "Erro",
                                                    text: retorno.msg,
                                                    type: "error"
                                                }
                                        );
                            }

                        }
                );*/

    }

    $(document).on('click', '.checkProposta', function ()
    {

        var elemento = $(this);

        if ($(this).hasClass("clip-square"))
        {

            if ($.inArray($(this).attr("value"), marcadosLecca) >= 0)
            {

                marcadosLecca = $.grep(marcadosLecca, function (value)
                {
                    return value !== elemento.attr("value");
                });
            }

            habilitaGerarLecca();

            $(this).removeClass("clip-square");
            $(this).addClass("clip-checkbox-unchecked-2");

        } else
        {

            if ($.inArray($(this).val(), marcadosLecca) < 0)
            {
                marcadosLecca.push(elemento.attr("value"));
            }

            habilitaGerarLecca();

            $(this).addClass("clip-square");
            $(this).removeClass("clip-checkbox-unchecked-2");

        }

    });
    
    myCheckbox.on('switchChange.bootstrapSwitch', function(event, state) 
    {
        
        myCheckboxIsChecked();
        
    });
    
    function myCheckboxIsChecked()
    {
        
        if ($('#myCheckbox').prop('checked')) 
        {      
            $("#divMalotes"            ).hide  ("slow" );
            $("#divReceber"           ).show  ("slow" );
        }
        else
        {
            $("#divMalotes"    ).show("slow");
            $("#divReceber"   ).hide("slow");
        }
        
    }
    $(document).on('click', '.checkRecebimento', function ()
    {

        var elemento = $(this);

        if ($(this).hasClass("clip-square"))
        {

            if ($.inArray($(this).attr("value"), marcadosRecebimento) >= 0)
            {

                marcadosRecebimento = $.grep(marcadosRecebimento, function (value)
                {
                    return value !== elemento.attr("value");
                });
            }

            habilitaGerarRecebido();

            $(this).removeClass("clip-square");
            $(this).addClass("clip-checkbox-unchecked-2");

        } else
        {

            if ($.inArray($(this).val(), marcadosRecebimento) < 0)
            {
                marcadosRecebimento.push(elemento.attr("value"));
            }

            habilitaGerarRecebido();

            $(this).addClass("clip-square");
            $(this).removeClass("clip-checkbox-unchecked-2");

        }

    });

    $("#botaoGerarLecca").on('click', function () {
        
        $.ajax({
           type: "POST",
           url: "/recebimentoDeDocumentacao/processarRecebimento",
           data:
           {
               propostasMarcadas: marcadosLecca
           },
        }).done(function(r){
            
            var jsonReturn = $.parseJSON(r)
            
            $.pnotify
            (
                {
                    title   : jsonReturn.title  ,
                    text    : jsonReturn.msg    ,
                    type    : jsonReturn.type
                }
            );
    
            if(!jsonReturn.hasErrors)
            {
                    
                marcadosLecca = [];
                tableRecebimento.draw();
                tableRecebido.draw();
                
            }
            
        });
    });

    $("#botaoGerarMalote").on('click', function () {
        
        $.ajax(
        {
        
            type: "POST",
            url: "/recebimentoDeDocumentacao/gerarMalote",
            data:
            {
                recebimentosMarcados : marcadosRecebimento
            },
        }).done(function(r){
            
            var jsonReturn = $.parseJSON(r)
            
            $.pnotify
            (
                {
                    title   : jsonReturn.title  ,
                    text    : jsonReturn.msg    ,
                    type    : jsonReturn.type
                }
            );
    
            if(!jsonReturn.hasErrors)
            {
                    
                marcadosLecca = [];
                tableRecebido.draw();
                
            }
            
        });
    });

});

