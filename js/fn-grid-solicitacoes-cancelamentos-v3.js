function format ( d ) {
    return $.each(d.moreDetails, function(index){
                
    })    
}

$(function(){

	var tableSolicitacoes = $('#grid_solicitacoes').DataTable({

		"processing": true,
        "serverSide": true,
        "ajax": {
            url: '/solicitacaoDeCancelamento/listar/',
            type: 'POST',
            "data": function (d) {
                d.er = $("input[name=task]").val()
            }
        },
        "language": {
            "processing": "<img style='position:fixed; top:60%; left:50%;margin-top:-8px;margin-left:-8px;' src='https://s1.sigacbr.com.br/js/loading.gif'>"
        },

		"columnDefs": [{
            "orderable": false,
            "targets": "no-orderable"
        }],

        "columns": [
        	{ 
                "class"             :"details-control",
                "orderable"         : false,
                "data"              : null,
                "defaultContent"    : ""
            },
            {"data": "financeira"},
            {"data": "proposta"},
            {"data": "dataProposta"},
            {"data": "cliente"},
            {"data": "cpf_cliente"},
            {"data": "solicitante"},
            {"data": "filial"},
            {"data": "parcelamento"},
            {"data": "valorTotal"},
            {"data": "banco"},
            {"data": "info"},
            {"data": "btn"}
        ],

	});

	var detailRows = [];

	$('#grid_solicitacoes tbody').on('click', 'tr td:first-child', function(){

        var tr      = $(this).closest('tr');
        var row     = tableSolicitacoes.row( tr );
        var idx     = $.inArray( tr.attr('id'), detailRows );

        if ( row.child.isShown() ) 
        {
            tr.removeClass( 'details' );
            row.child.hide();
            detailRows.splice( idx, 1 );
        }

        else 
        {
            
            tr.addClass( 'details' );
            row.child( format( row.data() ) ).show();
            
            if ( idx === -1 )
            {
                detailRows.push( tr.attr('id') );
            }
        }

    });

	tableSolicitacoes.on('draw', function(){

         $.each(detailRows, function ( i, id ) {
            $('#'+id+' td:first-child').trigger( 'click' );
         });

    });

    $(document).on('click','.btn-accept-soli',function(){
        $('#resposta_analista').val('');
        $('#resposta_analista').hide();
        $('#btn-confirm-option').text('Cancelar Proposta');
        $('#main-msg').text('Deseja cancelar esta proposta?');
        $('#ipt_hdn_action').attr('value','accept');
        $('#ipt_hdn_soli_id').attr('value',$(this).data('soli-id'));

        $(this).data('soli-id');
        return false;
    });

    $(document).on('click','.btn-deny-soli',function(){
        $('#resposta_analista').val('');
        $('#resposta_analista').show();
        $('#btn-confirm-option').text('Recusar Cancelamento');
        $('#main-msg').text('Deseja recusar o cancelamento desta proposta? Descreva o motivo:');
        $('#ipt_hdn_action').attr('value','decline');
        $('#ipt_hdn_soli_id').attr('value',$(this).data('soli-id'));

        $(this).data('soli-id');
        return false;
    });

    $('#btn-confirm-option').on('click',function(){

        $.ajax({
            url     : '/solicitacaoDeCancelamento/change/',
            type    : 'POST',
            data    : $('#form-efetivar').serialize(),
        })
        .done(function(drt){
            
            var retorno = $.parseJSON(drt);
            //console.log( retorno );
            $.pnotify({
                title    : 'Notificação',
                text     : retorno.msg,
                type     : retorno.pntfyClass
            });

            tableSolicitacoes.draw();
            
            $('#confirm-modal').modal('hide');
        });

        return false;
    });
});