$(document).ready(function () {
    
    var gridPropostas = $('#sample_1').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax":
                {
                    url: '/cliente/listarAnalisesCliente',
                    type: 'POST',
                    "data" : function (d) {
                        d.cpf = $('#cpfCliente').val()
                    }
                },
        "columnDefs": [{
                "orderable": false,
                "targets": "no-orderable"
            }],
        "columns": [
            {"data": "codigo"},
            {"data": "data_cadastro"},
            {"data": "valor"},
            {"data": "valor_final"},
            {"data": "qtd_parc"},
            {"data": "status"},
            {"data": "button"}
        ]
    });
    
    $.fn.editable.defaults.mode = 'inline';

    $('.search-select').select2();

    $(document).on('click', '.btn-proposta-more-details', function () {
        $('#iframe_mais_detalhes_proposta').attr('src', $(this).attr('modal-iframe-uri'));
    })

    var gridDadosSociais        = $('#grid_dados_sociais').DataTable({

        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/cliente/getDadosSociais/',
            type: 'POST',
            "data": function (d) {
                d.clienteId = $("#cliente_id").val()
            }
        },
        "columns": [{
            "data": "nome_da_mae"
        }, {
            "data": "nome_do_pai"
        }, {
            "data": "numero_de_dependentes"
        }, {
            "data": "titular_cpf"
        }, {
            "data": "estado_civil"
        }, {
            "data": "conjCompRenda"
        }, {
            "data": "btn"
        }, ],

        "sorting": [
            [0]
        ],
        "columnDefs": [{
            "orderable": false,
            "targets": "no-orderable"
        }],
    });

    var tableEnderecos          = $('#grid_enderecos_cliente').DataTable({

        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/cliente/getEnderecos/',
            type: 'POST',
            "data": function (d) {
                d.clienteId = $("#cliente_id").val()
            }
        },
        "sorting": [
            [0]
        ],
        "columns": [{
            "data": "logradouro"
        }, {
            "data": "bairro"
        }, {
            "data": "numero"
        }, {
            "data": "cidade"
        }, {
            "data": "cep"
        }, {
            "data": "uf"
        }, {
            "data": "complemento"
        },{
            "data": "cob"
        },{
            "data": "btn"
        }],
    });

    var tableReferencias        = $('#grid_referencias_cliente').DataTable({

        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/cliente/getReferencias/',
            type: 'POST',
            "data": function (d) {
                d.clienteId = $("#cliente_id").val()
            }
        },
        "sorting": [
            [0]
        ],
        "columns": [
            {"data": "parentesco"},
            {"data": "nome"},
            {"data": "numero"},
            {"data": "tipo"},
            {"data": "btn"}
        ],
    });

    var tableFamiliares         = $('#grid_familiares').DataTable({

        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/cliente/getFamiliares/',
            type: 'POST',
            "data": function (d) {
                d.clienteId = $("#cliente_id").val()
            }
        },
        "sorting": [
            [0]
        ],
        "columns": [
            {"data": "nome"},
            {"data": "relacao"},
            {"data": "btn"},
        ],
    });

    /*var tableDocumentos         = $('#grid_documentos_cliente').DataTable({

        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/cliente/getDocumentos/',
            type: 'POST',
            "data": function (d) {
                d.clienteId = $("#cliente_id").val()
            }
        },
        "sorting": [
            [0]
        ],
        "columns": [{
            "data": "numero"
        }, {
            "data": "tipo"
        }, {
            "data": "orgao_emissor"
        }, {
            "data": "uf_emissor"
        }, {
            "data": "data_emissao"
        }, {
            "data": "btn"
        },],
    });*/

    var tableDadosBancarios     = $('#grid_dados_bancarios_cliente').DataTable({

        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/cliente/getDadosBancarios/',
            type: 'POST',
            "data": function (d) {
                d.clienteId = $("#cliente_id").val()
            }
        },
        "sorting": [
            [0]
        ],
        "columns": [{
            "data": "banco"
        }, {
            "data": "agencia"
        }, {
            "data": "conta"
        }, {
            "data": "tipo"
        }, {
            "data": "operacao"
        }, {
            "data": "btn"
        }, ],
    });

    var tableDadosProfissionais = $('#grid_dados_profissionais_cliente').DataTable({
        "scrollX": true,
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/cliente/getDadosProfissionais/',
            type: 'POST',
            "data": function (d) {
                d.clienteId = $("#cliente_id").val()
            }
        },
        "autoWidth": false,
        "sorting": [
            [0]
        ],
        "columnDefs": [{
            "orderable": false,
            "targets": "no-orderable",
        }],

        "columns": [
            {
                "data" : "principal",
            },
            {
                "data" : "empresa",
            },
            {
                "data": "cpf_cnpj"
            },
            {
                "data": "data_de_admissao"
            },
            {
                "data": "ocupacao"
            }, 
            {
                "data": "classe_profissional"
            },
            {
                "data": "renda_liquida"
            }, 
            {
                "data": "mes_ano_renda"
            }, 
            {
                "data": "profissao"
            }, 
            {
                "data": "aposentado"
            }, 
            {
                "data": "pensionista"
            }, 
            {
                "data": "tipo_de_comprovante"
            }, 
            {
                "data": "numero_do_beneficio"
            },
            {
                "data": "orgao_pagador"
            }, 
            {
                "data": "cep"
            },
            {
                "data": "logradouro"
            }, 
            {
                "data": "bairro"
            }, 
            {
                "data": "cidade"
            }, 
            {
                "data": "estado"
            }, 
            {
                "data": "telefone"
            }, 
            {
                "data": "ramal"
            }, 
            {
                "data": "email"
            }, 
            {
                "data": "btn"
            },
        ],
    });

    var tableTelefones          = $('#grid_telefones').DataTable({

        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/cliente/getTelefones/',
            type: 'POST',
            "data": function (d) {
                d.clienteId = $("#cliente_id").val()
            }
        },
        "sorting": [
            [0]
        ],
        "columns": [{
            "data": "numero"
        }, {
            "data": "tipo"
        }, {
            "data": "ramal"
        }, {
            "data": "btn"
        }, ],
    });

    var tableEmails             = $('#grid_emails').DataTable({

        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/cliente/getEmails/',
            type: 'POST',
            "data": function (d) {
                d.clienteId = $("#cliente_id").val()
            }
        },

        "columns": [
            {"data": "email"}, 
            {"data": "btn"},
        ],
    });

    var tableAnexos             = $('#grid_anexos').DataTable({

        "processing": true,
        "serverSide": true,
        "ajax": {
            url: '/cliente/getAnexos/',
            type: 'POST',
            "data": function (d) {
                d.clienteId = $("#cliente_id").val(),
                d.canDelete = '1'
            }
        },

        "columns": [
            {"data": "descricao"},
            {"data": "ext"},
            {"data": "data_envio"},
            {"data": "btn"},
        ],
    });

    /**/
    $.validator.setDefaults({
        errorElement: "span",
        errorClass: 'help-block',

        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            // display OK icon
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            // add the Bootstrap error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('has-error');
            // set error class to the control group
        },

        success: function (label, element) {
            label.addClass('help-block valid');
            // mark the current input as valid and display OK icon
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
        }
    })
    
    /*Formulários de edição*/
    jQuery.fn.loadForm          = function ( url, entityId ) {

        return this.each(function () {
            $.ajax({

                type: "GET",
                
                url: url,
                
                data: {
                    entity_id: entityId
                },

                beforeSend  : function(){

                    $('.panel2').block({
                        overlayCSS: {
                                backgroundColor: '#fff'
                        },
                        message: '<img src="https://sigac/js/loading.gif" /> Carregando informações...',
                        css: {
                                border: 'none',
                                color: '#333',
                                background: 'none'
                        }
                    });
                        
                    window.setTimeout(function () {
                        $('.panel2').unblock();
                    }, 1000);        
                },
            })

            .done(function (dRt) {

                var paramsReturn = $.parseJSON(dRt);

                $.each(paramsReturn.fields, function (key, value) {

                    if (value.type != 'select')
                    {
                        $('#' + value.input_bind).val(value.value)
                    }

                    else
                    {   
                        if( value.value != null ){
                            $('#' + value.input_bind).select2("val", value.value);
                        }
                    }
                });

                $('#' + paramsReturn.modal_id).find('.btn-send').attr('data-url-request', paramsReturn.form_params.action);

                $('#' + paramsReturn.modal_id).modal('show');
            });
        });
    };

    /*Send form*/
    jQuery.fn.sendForm          = function( url, form, tableRedraw, divReturn, checkboxContinuar, modal_id ){

        return this.each(function(){

            $('#'+form).validate({

                submitHandler : function(){

                    $('#'+divReturn).hide().empty();
                    
                    var formData = $('#'+form).serialize();

                    $.ajax({
                        
                        type    : "POST",
                        url     : url,
                        data    : formData,

                        beforeSend  : function(){

                            $('.panel').block({
                                overlayCSS: {
                                    backgroundColor: '#fff'
                                },
                                message: '<img src="https://beta.sigacbr.com.br/js/loading.gif" /> Salvando informações...',
                                css: {
                                    border: 'none',
                                    color: '#333',
                                    background: 'none'
                                }
                            });
                            
                            window.setTimeout(function () {
                                $('.panel').unblock();
                            }, 1000);
                        },
                    })
                    .done(function(dRt){

                        //console.log(dRt);

                        var retorno = $.parseJSON(dRt);

                        if( !$('#'+checkboxContinuar).is(':checked') )
                        {
                            $('#'+modal_id).modal('hide');
                        }
                        
                        $('.has-success').each(function(index){
                            $(this).removeClass('has-success');
                        })

                        $('.ok').each(function(index){
                            $(this).removeClass('ok');
                        })

                        $('#'+tableRedraw).DataTable().draw();

                        $('#'+divReturn).prepend('<p>'+retorno.msgReturn+'</p>').fadeIn(100).addClass('alert-success').fadeOut(9000);

                        $.pnotify.defaults.history = false;

                        if( !$('#'+checkboxContinuar).is(':checked') )
                        {
                            $.pnotify({
                                title    : 'Notificação',
                                text     : retorno.msgReturn,
                                type     : 'success'
                            });
                        }

                        $('#'+form).trigger("reset");
                    })
                }
            });            
        })
    };

    $(document).on('click', '.btn-load-form', function () {
        $(this).loadForm($(this).data('url-request'), $(this).data('entity-id'));
        return false;
    })

    $(document).on('click', '.btn-send', function () {
        $(this).sendForm($(this).data('url-request'), $(this).data('form-id'), $(this).data('table-redraw'), $(this).data('div-return-id'), $(this).data('checkbox-continuar'), $(this).data('modal-id'));
    })

    $(document).on('click', '.btn-add, .btn-update', function () {
        
        var ipt = $('#'+$(this).data('input-controller-action'));
        ipt.attr('value',$(this).data('controller-action'));
        return false;
    })

    $(document).on('click','.btn-remove-anexo', function(){

        var anexoId = $(this).data('anexo-id');
        $('#ipt-hdn-id-anexo').attr('value', anexoId);
        return false;

    })

    var mySelect = $("#selectCidades").select2({
        data:[{id:0,text:'Carregando..'}],
        placeholder: "Selecione uma cidade...",
    });

    $.ajax({

        type  :  'GET',
        url   :  '/cidade/listCidades/',        
    })

    .done(function(dRt){
        var jsonReturn = $.parseJSON(dRt);
        mySelect.select2({
            data:jsonReturn 
        })
    })
    
    $('#Pessoa_naturalidade').on('change',function(){
        $.ajax({

            type  :  'GET',
            url   :  '/cidade/listCidades/',
            data  :  {
                uf : $(this).val()
            },
            beforeSend : function(){

                $('.panel').block({
                    overlayCSS: {
                        backgroundColor: '#fff'
                    },
                    message: '<img src="https://beta.sigacbr.com.br/js/loading.gif" /> Listando cidades do ' + $('#Pessoa_naturalidade').val(),
                    css: {
                        border: 'none',
                        color: '#333',
                        background: 'none'
                    }
                });
                window.setTimeout(function () {
                    $('.panel').unblock();
                }, 1000);
            }
        })

        .done(function(dRt){

            var jsonReturn = $.parseJSON(dRt);

            mySelect.select2({
                data:jsonReturn
            });
        })
    });

    $('#MyUploadForm').ajaxForm({

        beforeSubmit    : function(){
            
            if ( window.File && window.FileReader && window.FileList && window.Blob )
            {
                var fsize = $('#FileInput')[0].files[0].size;
                var ftype = $('#FileInput')[0].files[0].type;
                
                switch( ftype )
                {
                    case 'image/png': 
                    case 'image/gif':
                    case 'image/jpeg': 
                    case 'image/pjpeg':
                    case 'application/pdf':
                    break;
                    default:
                        $('#cadastro_anexo_msg_return').html("Tipo de arquivo não permitido!").show();
                        $('#cadastro_anexo_msg_return').fadeOut(4000);
                    return false;
                }

                if( fsize > 5242880 )
                {
                    $('#cadastro_anexo_msg_return').html("Arquivo muito grande!").show();
                    $('#cadastro_anexo_msg_return').fadeOut(4000);
                    return false
                }
            }

            else
            {
                alert("Por favor, seu navegador não suporta esta tarefa. Contate o Suporte.");
            }

        },

        uploadProgress  : function(event, position, total, percentComplete){

            $('#progressbox').show();
            $('#progressbar').width(percentComplete + '%') //update progressbar percent complete
            $('#statustxt').html(percentComplete + '%'); //update status text

            if( percentComplete > 50 )
            {
                $('#statustxt').css('color','#000'); //change status text to white after 50%
            }
        },

        success         : function( response, textStatus, xhr, form ) {

            var retorno = $.parseJSON(response);

            
            if( !$('#anexo_checkbox_continuar').is(':checked') )
            {
                $.pnotify({
                    title    : 'Notificação',
                    text     : retorno.msgReturn,
                    type     : retorno.classNotify,
                });

                $('#modal_form_new_anexo').modal('hide');
            }
            
            tableAnexos.draw();
            $('#progressbox').fadeOut(3000);
        },

        error           : function(xhr, textStatus, errorThrown) {
            console.log("in ajaxForm error");
        },

        resetForm       :true
    });
    
    $('#form-remove-anexo').ajaxForm({

        success         : function( response, textStatus, xhr, form ) {

            var retorno = $.parseJSON(response);

            $.pnotify({
                title    : 'Notificação',
                text     : retorno.msgReturn,
                type     : retorno.classNotify,
            });

            $('#modal_confirm_delete_anexo').modal('hide');
            tableAnexos.draw();
        },
    });


    $('#grid_documentos_cliente').dataTable({
          "aaSorting": [[ 1, "desc" ]],
          "lengthMenu": [[50, 250, 500, 1000], [50, 250, 500, 1000]],
          "iDisplayLength": 250
    });

    $('.editable').editable();

    /*$('#grid_documentos_cliente .editable a').editable({
        type: 'text',
        name: 'Type',
        title: 'Type',
        source: [
          {value: 0, text: 'Nothing'},
          {value: 1, text: 'Everything'},
          {value: 2, text: 'Something'},
          {value: 3, text: 'That Thing'},
          {value: 4, text: 'This Thing'},
          {value: 5, text: 'Things'}
        ]
    });*/

    $('.dob').editable();
})