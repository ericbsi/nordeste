$(document).ready(function(){

	$('#add-upload-field').on('click',function(){

		var form = $('#div-add-up');

		$('<div class="row">'
			+'<a style="float: none" class="closeup fileupload-exists" href="#">x</a>'
			+'<div class="col-md-9">'
				+'<div class="form-group">'
					+'<a style="float: none" class="close fileupload-exists" href="#">×</a>'
					+'<div class="col-sm-9">'
						+'<div data-provides="fileupload" class="fileupload fileupload-new">'
							+'<span class="btn btn-file btn-light-grey">'
								+'<i class="fa fa-folder-open-o"></i>'
								+'<span class="fileupload-new">Selecionar arquivo</span>'
								+'<span class="fileupload-exists">Mudar</span>'
								+'<input type="file" name="arquivos[]" required>'
							+'</span>'
							+'<span class="fileupload-preview"></span>'
						+'</div>'
						+'<p class="help-block">'
							+'<input required type="text" class="form-control" name="descricoes[]">'
						+'</p>'
					+'</div>'
				+'</div>'
			+'</div>'
		+'</div>').appendTo(form)
		
		return false;


	})

	$(document).on('click','.closeup',function(){
		$(this).parent().remove();
		return false;
	})

})