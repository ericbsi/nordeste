$(document).ready(function ()
{
    
    //////////////////////////////////////////////////////////////////////////
    // funcao que irá capturar o click duplo na célula de senha e irá       //
    // habilitar para edição da mesma. caso a mesma sofra alguma alteração, //
    // salve o conteúdo no banco                                            //
    //////////////////////////////////////////////////////////////////////////
    // Autor : André Willams // Data : 12-05-2015 ////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    $(document).on('dblclick', '.tdSenha', function () {

//                      elemento atual - elemento pai - primeira celula (id) - conteudo      
        var nomeUsuario = $(this).context.parentElement.cells[1].textContent; //pegue o id da referência

        var conteudoOriginal = $(this).text(); //resgate o conteúdo atual da célula

        var elemento = $(this);

        //transforme o elemento em um input
        $(this).html("<input class='form-control' type='password' value='' />");

        $(this).children().first().focus(); //atribua o foco ao elemento criado

        $(this).children().first().keypress(function (e) { //quando alguma tecla for pressionada

            if (e.which == 13) { //caso a tecla pressionada seja o ENTER, foi confirmada a alteração

                var novoConteudo = $(this).val(); //pegue o novo conteúdo

                //chame o ajax de alteração
                $.ajax({
                    type: "POST",
                    url: "/usuario/mudarSenha/",
                    data: {
                        "nomeUsuario": nomeUsuario,
                        "senha": novoConteudo,
                    },
                }).done(function (dRt)
                {

                    var retorno = $.parseJSON(dRt);

                    $.pnotify(
                            {
                                text: retorno.msg,
                                type: retorno.pntfyClass
                            }
                    );

                });

                $(this).parent().text(conteudoOriginal);

            }
        });

        //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
        $(this).children().first().blur(function () {

            //devolva o conteúdo original
            $(this).parent().text(conteudoOriginal);

        });

    });
    
    //////////////////////////////////////////////////////////////////////////
    // funcao que irá capturar o click duplo na célula de senha e irá       //
    // habilitar para edição da mesma. caso a mesma sofra alguma alteração, //
    // salve o conteúdo no banco                                            //
    //////////////////////////////////////////////////////////////////////////
    // Autor : André Willams // Data : 12-05-2015 ////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    $(document).on('dblclick', '.utilizador', function () {

//                      elemento atual - elemento pai - primeira celula (id) - conteudo      
        var nomeUsuario = $(this).context.parentElement.cells[1].textContent; //pegue o id da referência

        var conteudoOriginal = $(this).text(); //resgate o conteúdo atual da célula

        var elemento = $(this);

        //transforme o elemento em um input
        $(this).html("<input class='form-control' type='text' value='" + conteudoOriginal + "' />");

        $(this).children().first().focus(); //atribua o foco ao elemento criado

        $(this).children().first().keypress(function (e) { //quando alguma tecla for pressionada

            if (e.which == 13) { //caso a tecla pressionada seja o ENTER, foi confirmada a alteração

                var novoConteudo = $(this).val(); //pegue o novo conteúdo

                //chame o ajax de alteração
                $.ajax({
                    type    : "POST"                        ,
                    url     : "/usuario/mudarUtilizador/"   ,
                    data:   {
                                "nomeUsuario"   : nomeUsuario   ,
                                "utilizador"    : novoConteudo  ,
                            },
                }).done(function (dRt)
                {

                    var retorno = $.parseJSON(dRt);

                    $.pnotify(
                            {
                                text: retorno.msg,
                                type: retorno.pntfyClass
                            }
                    );
                
                    if(retorno.hasErrors)
                    {
                        elemento.text(conteudoOriginal);
                    }
                    else
                    {
                        elemento.text(novoConteudo);
                    }

                });

            }
        });

        //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
        $(this).children().first().blur(function () {

            //devolva o conteúdo original
            $(this).parent().text(conteudoOriginal);

        });

    });
    
    //////////////////////////////////////////////////////////////////////////
    // funcao que irá capturar o click duplo na célula de senha e irá       //
    // habilitar para edição da mesma. caso a mesma sofra alguma alteração, //
    // salve o conteúdo no banco                                            //
    //////////////////////////////////////////////////////////////////////////
    // Autor : André Willams // Data : 12-05-2015 ////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    $(document).on('dblclick', '.username', function () {

//                      elemento atual - elemento pai - primeira celula (id) - conteudo      
        var nomeUsuario = $(this).context.parentElement.cells[1].textContent; //pegue o id da referência

        var conteudoOriginal = $(this).text(); //resgate o conteúdo atual da célula

        var elemento = $(this);

        //transforme o elemento em um input
        $(this).html("<input class='form-control' type='text' value='" + conteudoOriginal + "' />");

        $(this).children().first().focus(); //atribua o foco ao elemento criado

        $(this).children().first().keypress(function (e) { //quando alguma tecla for pressionada

            if (e.which == 13) { //caso a tecla pressionada seja o ENTER, foi confirmada a alteração

                var novoConteudo = $(this).val(); //pegue o novo conteúdo

                //chame o ajax de alteração
                $.ajax({
                    type    : "POST"                        ,
                    url     : "/usuario/mudarUsername/"   ,
                    data:   {
                                "nomeUsuario"   : nomeUsuario   ,
                                "username"      : novoConteudo  ,
                            },
                }).done(function (dRt)
                {

                    var retorno = $.parseJSON(dRt);

                    $.pnotify(
                            {
                                text: retorno.msg,
                                type: retorno.pntfyClass
                            }
                    );
                
                    if(retorno.hasErrors)
                    {
                        elemento.text(conteudoOriginal);
                    }
                    else
                    {
                        elemento.text(novoConteudo);
                    }

                });

            }
        });

        //caso o usuário clique fora, significa que o mesmo está desprezando a alteração
        $(this).children().first().blur(function () {

            //devolva o conteúdo original
            $(this).parent().text(conteudoOriginal);

        });

    });
    
})