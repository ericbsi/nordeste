$(function(){

    $.validator.setDefaults({
        
        errorElement: "span", 
        errorClass: 'help-block',
        
        highlight: function (element) {
            $(element).closest('.help-block').removeClass('valid');
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            $( $(element).data('icon-valid-bind') ).removeClass('ico-validate-success').addClass('ico-validate-error');
        },
            
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },

        success: function (label, element) {
            label.addClass('help-block valid');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            $( $(element).data('icon-valid-bind') ).removeClass('ico-validate-error').addClass('ico-validate-success');
        },
    });

    var grid_analises = $('#table_id').DataTable({
        "processing": true,
        "serverSide": true,
        "stateSave" : true,
        "ajax": {
            url: '/proposta/reaproveitarPropostas/',
            type: 'POST',
            "data": function (d) {
                d.codigo_filter = $("#codigo_filter").val()
            }
        },
        "columns": [
            {"data": "codigo"},
            {"data": "cliente"},
            {"data": "filial"},
            {"data": "valor"},
            {"data": "valorSeguro"},
            {"data": "qtd_parcelas"},
            {"data": "btn"},
            {"data": "data_cadastro"},
        ],
        "drawCallback": function (settings) {
            setTimeout(function () {
                grid_analises.draw();
            }, 20000)
        }
    });

    $(document).ready(function () {

        function updateTablePropostas() {

            $('#table-wrapper').load('/proposta/reaproveitarPropostas/', function () {
                setTimeout(updateTablePropostas, 25000);
            });
        }

        updateTablePropostas();
    })

    $(document).on('click', '.label-warning', function () {

        $('#input_proposta_id').attr('value', $(this).attr('subid'));

    });

    $('#form-liberar-proposta').validate({
        submitHandler   : function (){

            var post    = $.ajax({

                url     : '/proposta/enviarParaFila/',
                type    : 'POST',
                data    : $('#form-liberar-proposta').serialize()

            });

            post.done( function( dRt ){
                
                var retorno = $.parseJSON(dRt);

                $('#modal-enviar').modal('hide');
                
                grid_analises.draw();
                
                $.pnotify({
                    title   : retorno.titulo,
                    text    : retorno.texto,
                    type    : retorno.tipo
                });
            } );

            return false;
        }
    });

    $(document).on('click','.show-modal',function(){

        $('#propostaid').val( $(this).data('propid') );

    });

    $(document).on('change','.input_filter', function(){
        grid_analises.draw();
    });

});
