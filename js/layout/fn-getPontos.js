$(document).ready(function ()
{
    
//    console.log(window.location);
    
    var urlGetPontos    = "https://s1.sigacbr.com.br/integracao/getPontos"      ;
    var urlLogin        = "https://s1.sigacbr.com.br/integracao/loginPontos"    ;
    
    if (window.location.host == "nordeste")
    {
        urlGetPontos    = "http://sigac/integracao/getPontos"   ;
        urlLogin        = "http://sigac/integracao/loginPontos" ;
    }

    $.ajax({
        type    : "POST",
        url     : urlGetPontos,
        data:
                {
                    nomeLogin: $('#nomeLogin').val()
                }
    }).done(function (dRt)
    {

        var retorno = $.parseJSON(dRt);

        console.log(retorno);
        console.log(parseInt($('#pontosUsuario').text()));

        console.log($('#nomeLogin').val());

        $('#pontosUsuario').text(
                parseInt(
                        retorno.pontos + parseFloat($('#pontosUsuario').text())
                        )
                );
        
        $('#form-login').attr('action',urlLogin);
        
        if(parseInt(retorno.msgNaoLidas) > 0)
        {
            $('#qtdMsgNaoLidas').text(retorno.msgNaoLidas + ' novas');
        }
        else
        {
            $('#qtdMsgNaoLidas').text('0');
        }   
        
    });

});