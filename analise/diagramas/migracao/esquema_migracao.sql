CREATE TABLE IF NOT EXISTS `EmpresaOrigem`(
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(150) NOT NULL,
  `data_cadastro` DATETIME NOT NULL,
  `habilitado` TINYINT(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `SistemaOrigem` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  `data_cadastro` DATETIME NOT NULL,
  `habilitado` TINYINT(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `FilialOrigem` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(150) NOT NULL,
  `data_cadastro` DATETIME NOT NULL,
  `habilitado` TINYINT(1) NOT NULL DEFAULT 1,
  `EmpresaOrigem_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_FilialOrigem_EmpresaOrigem1_idx` (`EmpresaOrigem_id` ASC),
  CONSTRAINT `fk_FilialOrigem_EmpresaOrigem1`
    FOREIGN KEY (`EmpresaOrigem_id`)
    REFERENCES `EmpresaOrigem` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `FilialOrigem_has_SistemaOrigem` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `FilialOrigem_id` INT NOT NULL,
  `SistemaOrigem_id` INT NOT NULL,
  `data_cadastro` DATETIME NOT NULL,
  `habilitado` TINYINT(1) NOT NULL,
  INDEX `fk_FilialOrigem_has_SistemaOrigem_SistemaOrigem1_idx` (`SistemaOrigem_id` ASC),
  INDEX `fk_FilialOrigem_has_SistemaOrigem_FilialOrigem1_idx` (`FilialOrigem_id` ASC),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_FilialOrigem_has_SistemaOrigem_FilialOrigem1`
    FOREIGN KEY (`FilialOrigem_id`)
    REFERENCES `FilialOrigem` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_FilialOrigem_has_SistemaOrigem_SistemaOrigem1`
    FOREIGN KEY (`SistemaOrigem_id`)
    REFERENCES `SistemaOrigem` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `Migracao`(
  `id` INT NOT NULL AUTO_INCREMENT,
  `data` DATETIME NOT NULL,
  `usuarioId` INT NOT NULL,
  `data_cadastro` DATETIME NOT NULL,
  `habilitado` TINYINT(1) NOT NULL,
  `destino` INT NOT NULL,
  `origem` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Migracao_Filial1_idx` (`destino` ASC),
  INDEX `fk_Migracao_FilialOrigem_has_SistemaOrigem1_idx` (`origem` ASC),
  CONSTRAINT `fk_Migracao_Filial1`
    FOREIGN KEY (`destino`)
    REFERENCES `Filial` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Migracao_FilialOrigem_has_SistemaOrigem1`
    FOREIGN KEY (`origem`)
    REFERENCES `FilialOrigem_has_SistemaOrigem` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `ItensMigrados`(
  `id` INT NOT NULL AUTO_INCREMENT,
  `tabela` VARCHAR(150) NOT NULL,
  `registro` INT NOT NULL,
  `obs` TEXT NOT NULL,
  `data_cadastro` DATETIME NOT NULL,
  `habilitado` TINYINT(1) NOT NULL,
  `Migracao_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_ItensMigrados_Migracao1_idx` (`Migracao_id` ASC),
  CONSTRAINT `fk_ItensMigrados_Migracao1`
    FOREIGN KEY (`Migracao_id`)
    REFERENCES `Migracao` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `ItensBuscados`(
  `id` INT NOT NULL,
  `tabela` VARCHAR(45) NOT NULL,
  `registro` VARCHAR(100) NOT NULL,
  `data_cadastro` DATETIME NOT NULL,
  `habilitado` TINYINT(1) NOT NULL DEFAULT 1,
  `sucesso` TINYINT(1) NOT NULL,
  `obs` TEXT NOT NULL,
  `Migracao_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_ItensBuscados_Migracao1_idx` (`Migracao_id` ASC),
  CONSTRAINT `fk_ItensBuscados_Migracao1`
    FOREIGN KEY (`Migracao_id`)
    REFERENCES `Migracao` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `ClientesCamaleao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CPF` text,
  `CODIGO` text,
  `RG` text,
  `ORGAO` text,
  `EXPEDICAO` datetime DEFAULT NULL,
  `RAZAO_SOCIAL` text,
  `RUA` text,
  `BAIRRO` text,
  `CIDADE` text,
  `UF` text,
  `CEP` text,
  `NASCIMENTO` datetime DEFAULT NULL,
  `FONE` int(11) DEFAULT NULL,
  `CELULAR` text,
  `INCLUSAO` datetime DEFAULT NULL,
  `MAE` text,
  `PAI` text,
  `DATA_IMPORTACAO` datetime DEFAULT NULL,
  `DATA_EFETIVACAO` datetime DEFAULT NULL,
  `EFETIVADO` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=245195 ;