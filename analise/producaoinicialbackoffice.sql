SELECT F.id as 'FILIAL', truncate(coalesce(sum(P.valor-P.valor_entrada), 0),2) AS 'VALOR'
FROM Filial as F
        LEFT OUTER JOIN(
	Analise_de_Credito as AC
    JOIN Proposta AS P ON (P.Analise_de_Credito_id = AC.id AND P.habilitado and P.Status_Proposta_id = 2 and P.titulos_gerados)
) ON (F.id = AC.Filial_id and AC.data_cadastro between '2015-01-19' and '2015-01-19')
where F.id IN(19,37,22,21,23,24,25,20)
group by F.id 
order by truncate(coalesce(sum(P.valor), 0),2) DESC

/*Query do ranking inicial*/