<!DOCTYPE html>
<html lang="en" class="no-js">
   <head>
      <title>Credshow Operadora de Crédito S/A</title>
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="apple-mobile-web-app-status-bar-style" content="black">
      <meta content="" name="description" />
      <meta content="" name="author" />
      <!-- start: MAIN CSS -->
      <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap.min.css" rel="stylesheet" media="screen">
      <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.css" rel="stylesheet" media="screen">
      <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/login/font-awesome.min.css">
      <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/style.css">
      <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/main.css">
      <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/main-responsive.css">
      <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap-colorpalette.css">
      <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/perfect-scrollbar.css">
      <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/theme_light.css" id="skin_color">
      <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/select2.css">
      <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/summernote.css">
      <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/DT_bootstrap.css">
      <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet' type='text/css'>
      <link href='https://fonts.googleapis.com/css?family=Raleway:300,200,100' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap-modal-bs3patch.css">
      <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap-modal.css">
      <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery.pnotify.default.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery.pnotify.default.icons.css" />
      <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.ico" />
      <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/ladda-themeless.min.css">
      <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap-switch.css">
      <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/social-buttons-3.css">
   </head>
   <!-- end: HEAD -->
   <!-- start: BODY -->
   <body class="navigation-small">
      <!-- start: HEADER -->
      <div class="navbar navbar-inverse navbar-fixed-top">
         <!-- start: TOP NAVIGATION CONTAINER -->
         <div class="container">
            <div class="navbar-header">
               <!-- start: RESPONSIVE MENU TOGGLER -->
               <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
               <span class="clip-list-2"></span>
               </button>
               <!-- end: RESPONSIVE MENU TOGGLER -->
               <!-- start: LOGO -->
               
               <a class="navbar-brand" href="#">

                  <img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/credtresanos.png">
               </a>                  
               <!-- end: LOGO -->
            </div>
            <div class="navbar-tools">
               <!-- start: TOP NAVIGATION MENU -->
               
               <!-- end: TOP NAVIGATION MENU -->
            </div>
         </div>
         <!-- end: TOP NAVIGATION CONTAINER -->
      </div>
      <!-- end: HEADER -->
      <!-- start: MAIN CONTAINER -->
      <div class="main-container">
         <div class="navbar-content">
            <!-- start: SIDEBAR -->
            <div class="main-navigation navbar-collapse collapse">
               <!-- start: MAIN MENU TOGGLER BUTTON -->
               <div class="navigation-toggler">
                  <i class="clip-chevron-left"></i>
                  <i class="clip-chevron-right"></i>
               </div>
               <!-- end: MAIN MENU TOGGLER BUTTON -->
               <!-- start: MAIN NAVIGATION MENU -->
               
               <!-- end: MAIN NAVIGATION MENU -->
            </div>
            <!-- end: SIDEBAR -->
         </div>
         
         <div class="main-content">
            <div class="container">
               <?php echo $content ?>
            </div>
         </div>
         
      </div>
      
      <div class="footer clearfix">
         
      </div>
         
   </body>
   
</html>