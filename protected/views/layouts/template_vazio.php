<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>

        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap.min.css" rel="stylesheet" media="screen">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/login/font-awesome.min.css">
        
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/main.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/style.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/main-responsive.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap-colorpalette.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/perfect-scrollbar.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/select2.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap-modal-bs3patch.css">
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap-modal.css">
        
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.min.js"></script>
        

    </head>
    <body>

        <?php echo $content ?>

    </body>

</html>
