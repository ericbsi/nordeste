<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>

        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/contratov2.css" rel="stylesheet" media="screen,print">

        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-barcode.min.js"></script>
    </head>
    <body>
        <?php echo $content ?>        
    </body>
</html>
