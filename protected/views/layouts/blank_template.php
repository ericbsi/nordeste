<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title></title>
      <meta name="description" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap.min.css" rel="stylesheet" media="screen">
      <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/login/font-awesome.min.css">
      <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/style.css">
      <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/main.css">
      <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/main-responsive.css">
      <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap-colorpalette.css">
      <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/perfect-scrollbar.css">
      <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/theme_light.css" id="skin_color">
      <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/select2.css">
      <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/DT_bootstrap.css">
      <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap-modal-bs3patch.css">
      <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap-modal.css">      

   </head>
   <body>
      <?php echo $content; ?>
   </body> 

      <?php if (isset($this->pageIsModal) && $this->pageIsModal) { ?>
      
         <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/7e745f75/jquery.js"></script>
         <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.inputlimiter.1.3.1.min.js"></script>
         <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.autosize.min.js"></script>
         <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/spin.min.js"></script>
         <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ladda.min.js"></script>
         <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap-switch.min.js"></script>
         <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ui-buttons.js"></script>
         <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/modal-crud-fn.js"></script>
         
         <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/form-elements.js"></script>
         <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/select2.js"></script>
         
         <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.maskedinput.js"></script>
         <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.maskMoney.js"></script>
         <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap-datepicker.js"></script>
         <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap-timepicker.min.js"></script>
         <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/daterangepicker.js"></script>
         <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap-colorpicker.js"></script>
         <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap-colorpalette.js"></script>
         <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.tagsinput.js"></script>
         <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/summernote.min.js"></script>
         <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ckeditor/ckeditor.js"></script>
         <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ckeditor/jquery.js"></script>
         <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap-modal.js"></script>
         <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap-modalmanager.js"></script>
         <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ui-modals.js"></script>
         
         <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.dataTables.min.js"></script>
         <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/DT_bootstrap.js"></script>
         <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/table-data.js"></script>
      
      
      
         <script type="text/javascript">
      
            jQuery(function($) {
               FormElements.init();
               //TableData.init();
               UIModals.init();
            });
      
         </script>
   <?php } ?>

</html>