<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="span-19">
	<div id="content">
		<?php echo $content; ?>
	</div><!-- content -->
</div>

<div class="span-5 last">
	<div id="sidebar">		
			
		<?php  

			foreach (Yii::app()->session['usuario']->fetch_menus()  as $menus) {
				
				foreach ($menus as $menu) {
					
					$this->renderPartial($menu);
				}

			}

		?>

	</div>
</div>

<?php $this->endContent();  ?>