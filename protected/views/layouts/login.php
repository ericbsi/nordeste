<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="robots" content="noindex" />
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.png">
      <title> Nordeste | Credshow Operadora de Crédito S/A - Login </title>
      <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet' type='text/css'>
      <link href='https://fonts.googleapis.com/css?family=Raleway:300,200,100' rel='stylesheet' type='text/css'>

      <!-- Bootstrap core CSS -->
      <!--<link rel="stylesheet" type="text/css" href="<?php //echo Yii::app()->request->baseUrl; ?>/css/main_sigax.css" />-->
      <!--<link rel="stylesheet" type="text/css" href="<?php //echo Yii::app()->request->baseUrl; ?>/css/bootstrap.css" />-->
      <!--<link rel="stylesheet" type="text/css" href="<?php //echo Yii::app()->request->baseUrl; ?>/css/login/font-awesome.min.css" />-->

      <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/select2.css">
      <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main_sigax.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/login/font-awesome.min.css" />

   </head>
   <body class="texture">
      <div id="cl-wrapper" class="login-container">
         <div class="middle-login">
            <div class="block-flat">
               <div class="header">
                   <img width="200"class="logo-img" src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/logocredshow.png">
               </div>
               <div>
                  <?php echo $content?>
               </div>
            </div>
            <div class="text-center out-links"><a href="#">&copy; Credshow Operadora de Crédito S/A</a></div>
         </div>
      </div>

   </body>
</html>