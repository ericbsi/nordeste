<?php /* Main sigac-marex template */
  header("Cache-Control: max-age=60, must-revalidate"); 
?>
<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie8 no-js" lang="en">
   <![endif]-->
   <!--[if IE 9]>
   <html class="ie9 no-js" lang="en">
      <![endif]-->
      <!--[if !IE]><!-->
      <html lang="en" class="no-js">
         <!--<![endif]-->
         <head>
            <title>Credshow Operadora de Crédito S/A</title>
            <meta charset="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
            <meta name="apple-mobile-web-app-capable" content="yes">
            <meta name="apple-mobile-web-app-status-bar-style" content="black">
            <meta content="" name="description" />
            <meta content="" name="author" />
            <!-- start: MAIN CSS -->
            <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap.min.css" rel="stylesheet" media="screen">
            <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.css" rel="stylesheet" media="screen">
            <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/login/font-awesome.min.css">
            <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/style.css">
            <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/main.css">
            <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/main-responsive.css">
            <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap-colorpalette.css">
            <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/perfect-scrollbar.css">
            <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/theme_light.css" id="skin_color">
            <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/select2.css">
            <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/summernote.css">
            <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/DT_bootstrap.css">
            <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet' type='text/css'>
            <link href='https://fonts.googleapis.com/css?family=Raleway:300,200,100' rel='stylesheet' type='text/css'>
            <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap-modal-bs3patch.css">
            <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap-modal.css">

            <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery.pnotify.default.css" />
            <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery.pnotify.default.icons.css" />
            
            
            <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.ico" />

            <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/ladda-themeless.min.css">
            <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap-switch.css">
            <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/social-buttons-3.css">
         </head>
         <!-- end: HEAD -->
         <!-- start: BODY -->
         <body class="navigation-small footer-fixed">
            <!-- start: HEADER -->
            <div class="navbar navbar-inverse navbar-fixed-top">
               <!-- start: TOP NAVIGATION CONTAINER -->
               <div class="container">
                  <div class="navbar-header">
                     <!-- start: RESPONSIVE MENU TOGGLER -->
                     <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                     <span class="clip-list-2"></span>
                     </button>
                     <!-- end: RESPONSIVE MENU TOGGLER -->
                     <!-- start: LOGO -->
                     <a class="navbar-brand" href="#">
                        <!--SIGAC <span class="azul-cred-brand">GMM</span>-->
                         <img width="200"src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/credlogoinner.png">
                     </a>                  
                     <!-- end: LOGO -->
                  </div>
                  <div class="navbar-tools">
                     
                     <ul class="nav navbar-right">
                     <?php if ( Yii::app()->session['usuario']->tipo != 'super' ) {?>
                        <li class="dropdown">
                           <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                           <span class="badge empresa-filial-badge">
                              <?php if ( Yii::app()->session['usuario']->tipo == 'financas' || Yii::app()->session['usuario']->tipo == 'empresa_admin' || Yii::app()->session['usuario']->tipo == 'analista_de_credito' || Yii::app()->session['usuario']->tipo == 'financeiro' || Yii::app()->session['usuario']->tipo == 'estoque') { ?>
                                 Empresa: <?php echo Yii::app()->session['usuario']->nomeEmpresa() ?>
                              <?php } elseif ( Yii::app()->session['usuario']->tipo == 'crediarista' || Yii::app()->session['usuario']->tipo == 'caixa' || Yii::app()->session['usuario']->tipo == 'vendedor') { ?>
                                 Parceiro: <?php echo substr(Yii::app()->session['usuario']->nomeFilial(), 0,15) ?> -
                                 <?php echo Yii::app()->session['usuario']->returnFilial()->getEndereco()->cidade ?> / <?php echo Yii::app()->session['usuario']->returnFilial()->getEndereco()->uf ?>
                              <?php } elseif( Yii::app()->session['usuario']->tipo == 'parceiros_admin' ) { ?>
                                 Gestor de Filiais
                              <?php } elseif( Yii::app()->session['usuario']->tipo == 'emprestimo' ) { ?>
                                 Empréstimo Pessoal
                              <?php } else { ?>
                                    <?php //echo Yii::app()->session['usuario']->nomeEmpresa() ?>
                              <?php }?>
                           </span>
                           </a>                          
                        </li>
                        <?php } ?>
                        <li class="dropdown current-user">
                           <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="username">
                               <?php echo Yii::app()->session['usuario']->roleLabel() ?>: 
                               <?php echo Yii::app()->session['usuario']->nome_utilizador ?>
                            </span>
                           </a>
                            <i class="clip-chevron-down"></i>
                           <ul class="dropdown-menu">
                             <!-- <li>
                                 <a href="pages_user_profile.html">
                                 <i class="clip-user-2"></i>
                                 &nbsp;My Profile
                                 </a>
                              </li>
                              <li>
                                 <a href="pages_calendar.html">
                                 <i class="clip-calendar"></i>
                                 &nbsp;My Calendar
                                 </a>
                              <li>
                                 <a href="pages_messages.html">
                                 <i class="clip-bubble-4"></i>
                                 &nbsp;My Messages (3)
                                 </a>
                              </li>
                              <li class="divider"></li>
                              <li>
                                 <a href="utility_lock_screen.html"><i class="clip-locked"></i>
                                 &nbsp;Lock Screen </a>
                              </li>-->
                              <li>
                                 <a href="<?php echo Yii::app()->request->baseUrl;?>/usuario/changePassword">
                                 <i class="clip-key"></i>
                                 &nbsp;Mudar senha
                                 </a>
                              </li>
                              <li>
                                 <a href="<?php echo Yii::app()->request->baseUrl;?>/site/logout">
                                 <i class="clip-exit"></i>
                                 &nbsp;Sair do sistema
                                 </a>
                              </li>
                           </ul>
                        </li>
                        <!-- end: USER DROPDOWN -->
                     </ul>
                     <!-- end: TOP NAVIGATION MENU -->
                  </div>
               </div>
               <!-- end: TOP NAVIGATION CONTAINER -->
            </div>
            <!-- end: HEADER -->
            <!-- start: MAIN CONTAINER -->
            <div class="main-container">
               <div class="navbar-content">
              
                  <!-- start: SIDEBAR -->
                  <div class="main-navigation navbar-collapse collapse">
                     
                     <!-- start: MAIN MENU TOGGLER BUTTON -->
                     <div class="navigation-toggler">
                        <i class="clip-chevron-left"></i>
                        <i class="clip-chevron-right"></i>
                     </div>
                     <!-- end: MAIN MENU TOGGLER BUTTON -->
                     
                     <!-- start: MAIN NAVIGATION MENU -->
                     <?php  $this->renderPartial( Yii::app()->session['usuario']->getRole()->menu_padrao ); ?>
                     <!-- end: MAIN NAVIGATION MENU -->
                  </div>
                  <!-- end: SIDEBAR -->

               </div>
               <!-- start: PAGE -->
               <div class="main-content">
                  <!-- start: PANEL CONFIGURATION MODAL FORM -->
                  <div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
                     <div class="modal-dialog">
                        <div class="modal-content">
                           <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                              &times;
                              </button>
                              <h4 class="modal-title">Panel Configuration</h4>
                           </div>
                           <div class="modal-body">
                              Here will be a configuration form
                           </div>
                           <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">
                              Close
                              </button>
                              <button type="button" class="btn btn-primary">
                              Save changes
                              </button>
                           </div>
                        </div>
                        <!-- /.modal-content -->
                     </div>
                     <!-- /.modal-dialog -->
                  </div>
                  <!-- /.modal -->
                  <!-- end: SPANEL CONFIGURATION MODAL FORM -->
                  <div class="container">
                     
                     <!-- start: PAGE CONTENT -->
                     <?php echo $content; ?>
                     <!-- end: PAGE CONTENT-->
                  </div>
               </div>
               <!-- end: PAGE -->
            </div>
            <!-- end: MAIN CONTAINER -->
            <!-- start: FOOTER -->
            <div class="footer clearfix">
                <div class="footer-inner">
                   <div class="container">

                       <?php if (Yii::app()->session['usuario']->getRole()->id == 5) { ?>

                            <form id="form-login" action="" method="post">
                                <!--<a href="#" id="btnFidelidade">-->
                                <button type="submit" 
                                        class="btn btn-default btn-squared" 
                                        style="height: 30px;padding: 0px;vertical-align: top">
                                    <!--
                                    <h4>
                                        <img src="<?php //echo Yii::app()->baseUrl?>/images/meus_pontos.png" height="28" width="100" /> 
                                         <span id="pontosUsuario" style="color:#3D9400"> 
                                            //echo Yii::app()->session['usuario']->getSaldoPontos();
                                         </span> pontos.
                                         <span id="qtdMsgNaoLidas" class="badge badge-warning">0</span>
                                    </h4>
                                    -->
                                </button>
                                <input 
                                    type="hidden" 
                                    id="nomeLogin" 
                                    name="LoginFora[username]" 
                                    value="<?php echo Yii::app()->session['usuario']->username; ?>" /> 
                            </form>
                            <!--</a>-->

                       <?php } ?>
                   </div>
               </div>
               <div class="footer-items">
                  <span class="go-top"><i class="clip-chevron-up"></i></span>
               </div>
            </div>

            <!-- end: FOOTER -->
            <!-- start: MAIN JAVASCRIPTS -->            
            
            <!--<script src="<?php //echo Yii::app()->request->baseUrl; ?>/js/jquery-ui-1.10.2.custom.min.js"></script>-->
            <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>
            <!--<script src="<?php //echo Yii::app()->request->baseUrl; ?>/js/jquery.blockUI.js"></script>-->
            <!--<script src="<?php //echo Yii::app()->request->baseUrl; ?>/js/jquery.icheck.min.js"></script>-->
            <!--<script src="<?php //echo Yii::app()->request->baseUrl; ?>/js/jquery.mousewheel.js"></script>-->
            <!--<script src="<?php //echo Yii::app()->request->baseUrl; ?>/js/perfect-scrollbar.js"></script>-->
            <!--<script src="<?php //echo Yii::app()->request->baseUrl; ?>/js/less-1.5.0.min.js"></script>-->
            <!--<script src="<?php //echo Yii::app()->request->baseUrl; ?>/js/jquery.cookie.js"></script>-->
            <!--<script src="<?php //echo Yii::app()->request->baseUrl; ?>/js/bootstrap-colorpalette.js"></script>-->
            <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/layout/fn-getPontos.js"></script>
            <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/main.js"></script>
            <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.pnotify.min.js"></script>
            
            <script>
               jQuery(document).ready(function() {
                  Main.init();
                  $.pnotify.defaults.history = false;
               });
            </script>

         </body>
         
      </html>