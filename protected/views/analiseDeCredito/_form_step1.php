<!-- start: PAGE HEADER -->
<div class="row">
   <div class="col-sm-12">
      <!-- start: PAGE TITLE & BREADCRUMB -->
      <ol class="breadcrumb">
         <li><a href="#">Análises</a></li>
         <li class="active">Criar</li>
      </ol>
      <div class="page-header">
         <h1>Análise de Crédito</h1>
      </div>
      <!-- end: PAGE TITLE & BREADCRUMB -->
   </div>
</div>
<!-- end: PAGE HEADER -->
<div class="row">
   <div class="col-sm-12">
      <div class="panel panel-default">
         <div class="panel-heading">
            <i class="fa fa-external-link-square"></i>
            CONSULTAR CPF            
            <div class="panel-tools">
               <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
               </a>            
            </div>
         </div>
         <div class="panel-body">
            <form method="post" action="" id="analise-de-credito-form" class="form-horizontal">
               <div class="form-group">
                  <label class="col-sm-2 control-label">CPF</label>
                  <span class="input-help">
                  <div class="col-sm-4">
                     <span class="input-help">
                     <input type="text" name="CPF" class="form-control" id="CPF" placeholder="CPF">
                     <i style="background:#D9534F" class="help-button popovers" title="" data-content="Apenas números" data-placement="right" data-trigger="hover" data-rel="popover" data-original-title="Informação"></i> </span>
                  </div>
               </div>
               <!--<div class="form-group">
                  <label class="col-sm-2 control-label">Conteúdo da imagem</label>
                  <span class="input-help">
                  <div class="col-sm-4">
                     <span class="input-help">
                     <input id="Captha" class="form-control tooltips" type="text" data-placement="top" title="" placeholder="Conteúdo da imagem" data-rel="tooltip" data-original-title="">
                     <i style="background:#D9534F" class="help-button popovers" title="" data-content="Certifique-nos de que és um ser humano." data-placement="right" data-trigger="hover" data-rel="popover" data-original-title="Informação"></i> </span>
                  </div>
               </div>-->
               <!--<div class="form-group">
                  <label class="col-sm-2 control-label"></label>                           
                  <div class="col-sm-4">
                     <img id="img-captcha-receita" src="<?php echo Yii::app()->request->baseUrl.'/images/captchas/captchas_'.Yii::app()->session['usuario']->id.'/'.$imgName ?>" />
                     <a id="img-captcha-update" href="#">
                     <i style="font-size:25px;margin:0 0 0 30px;" class="fa fa-refresh"></i>
                     </a>
                  </div>
               </div>-->
               <!--Submitbutton-->
               <div class="form-group">
                  <label class="col-sm-2 control-label"></label>
                  <div class="col-md-5">
                     <button id="btn-submit-receita-form" data-style="expand-right" class="btn btn-teal ladda-button">
                        <span class="ladda-label"> Consultar </span>
                        <i class="fa fa-arrow-circle-right"></i>
                        <span class="ladda-spinner"></span>
                        <span class="ladda-spinner"></span>
                        <div class="ladda-progress" style="width: 0px;"></div>
                     </button>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>

<div id="responsive" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
   <div class="modal-header" style="background:#dff0d8!important;">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      <h4 style="color: #468847;" class="alert-heading"><i class="clip-user"></i> Cliente: <span id="span-clinome"></span></h4>
      <h4 style="color: #468847;" class="alert-heading"><i class="fa fa-credit-card"></i> CPF: <span id="span-clicpf"></span></h4>
      <h4 style="color: #468847;" class="alert-heading"><i class="fa fa-check-circle"></i> Situação: <span id="span-situacao"></span></h4>
      <h4 style="color: #468847;" class="alert-heading"><i class="fa fa-check-circle"></i> Cadastrado: <span class="span-isclient"></span></h4>
      <!--<form target="_blank" class="form-horizontal" method="post" action="<?php //echo Yii::app()->request->baseUrl; ?>/cliente/editar">
         <input id="btn-editar-cliente" style="display:none" type="submit" class="btn btn-primary" value="Alterar informações do cliente">
         <input type="hidden" name="idCliente" class="form-control" id="input-hdn-pessoa-id">
      </form>-->
   </div>
   <div class="modal-body" style="background: #ECF0F1">
      <form id="form-do-analise" class="form-horizontal" method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/analiseDeCredito/step1">
         <div class="form-group">
            <label class="col-sm-4 control-label">
            Renda líquida: <span class="symbol required"></span>
            </label>
            <div class="col-sm-3">
               <input required="required" type="text" class="form-control grana" name="renda_liquida" readonly="readonly" id="renda_liquida">
            </div>
         </div>
         <div class="form-group">
            <label class="col-sm-4 control-label">
            Valor solicitado: <span class="symbol required"></span>
            </label>
            <div class="col-sm-3">
               <input required="required" type="text" class="form-control grana" name="valor_soli">
            </div>
            <input type="hidden" name="cpf" class="form-control" id="CPFsend">
            <input type="hidden" name="nome" class="form-control" id="NOMEsend">
            <input type="hidden" name="situacao" class="form-control" id="SITUACAOsend">
            <input type="hidden" name="input_hdn_pessoa_id" class="form-control" id="input-hdn-pessoa-id2">
            <!--Nova implementação da consulta-->
            <input type="hidden" name="tokenReceita_0" id="tokenReceita_0" value="<?php echo $token[0] ?>">
            <input type="hidden" name="tokenReceita_1" id="tokenReceita_1" value="<?php echo $token[1] ?>">
            <input type="hidden" name="oldCaptcha" id="oldCaptcha" value="<?php echo $imgName ?>">
         </div>
         <div class="form-group">
            <label class="col-sm-4 control-label">
            Entrada: <span class="symbol required"></span>
            </label>
            <div class="col-sm-3">
               <input type="text" class="form-control grana" name="valor_entrada">
            </div>
         </div>
         <div class="alert alert-success">
            <i class="fa fa-check-circle"></i>
            Clique em  <strong>'Simular'</strong> Para iniciar a simulação.
         </div>
   </div>
   <div class="modal-footer">
   <button type="button" data-dismiss="modal" class="btn btn-light-grey">
   Cancelar
   </button>
   <input type="submit" class="btn btn-green" value="Simular">
   </div>
   </form>
</div>

<div id="modal_suspense" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
   <div class="modal-header" style="background:#b33426!important;">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      <h4 style="color: #FFF;" class="alert-heading"><i class="clip-user"></i> Cliente: <span id="span-err-clinome"></span></h4>
      <h4 style="color: #FFF;" class="alert-heading"><i class="fa fa-credit-card"></i> CPF: <span id="span-err-clicpf"></span></h4>
      <h4 style="color: #FFF;" class="alert-heading"><i class="fa fa-times-circle"></i> Situação: <span id="span-err-situacao"></span></h4>
   </div>
   <div class="modal-footer" style="background: #f2dede">
      <button type="button" data-dismiss="modal" class="btn btn-bricky">
      Fechar
      </button>
   </div>
</div>

<div class="modal fade" id="modalResultError" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-content">
      <div id="modal-return-wrapper" class="alert alert-block alert-danger fade in">
         <h4 class="alert-heading"><i style="font-size:20px;" class="clip-notification-2"></i> Erro: <span id="span-situacao"></span></h4>
         <p>
            CPF e/ou Captcha inválidos. Por favor, verifique as informações antes 
            de enviá-las.               
         </p>
         <br>
         <button  class="btn btn-bricky" data-dismiss="modal">
         Fechar
         </button>
      </div>
   </div>
</div>

<div data-width="700" class="modal fade" id="modalCadastroIncompleto" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-content">
      <div id="modal-return-wrapper" class="alert alert-block alert-warning fade in">
         <h4 class="alert-heading"><i style="font-size:20px;" class="clip-notification-2"></i> Atenção: <span id="span-situacao"></span></h4>
         <p>
            O cadastro do cliente está incompleto. Por isso, não é possível prosseguir com a análise. Veja mais detalhes abaixo:
         </p>
         
         <br>

         <div id="cadIncObs">
            
         </div>

         <br>
         <button  class="btn btn-warning" data-dismiss="modal">
         Fechar
         </button>
         <br>
         <form target="_blank" class="form-horizontal" method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/cliente/editar">
            <br>
            <button  class="btn btn-warning" type="submit">Editar informações</button>
            <input type="hidden" name="idCliente" class="form-control" id="input-hdn-pessoa-id3">
         </form>
      </div>
   </div>
</div>

<div data-width="700" class="modal fade" id="modalNaoCadastrado" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-content">
      <div id="modal-return-wrapper" class="alert alert-block alert-warning fade in">
         <h4 class="alert-heading"><i style="font-size:20px;" class="clip-notification-2"></i> Atenção: <span id="span-situacao"></span></h4>
         <p>
            O cliente não está cadastrado. Para prosseguir, cadastre-o.
         </p>
            
         <br>
         <p><button  class="btn btn-warning" data-dismiss="modal">Fechar</button></p>
         <br>
         <form action="/cliente/listarClientes/" method="POST">
            <input type="hidden" value="1" name="showModal">
            <input type="hidden" name="cliCPF" id="cliCpf">
            <input type="hidden" name="cliNOME" id="cliNome">
            <button class="btn btn-warning">Cadastrar Cliente</button>
         </form>
      </div>
   </div>
</div>

<style>
   .alert {margin-bottom: 0!important;}
   .modal-footer {margin-top: 0!important;}
</style>

<script type="text/javascript">
   function updateCaptcha(){
   
         $.ajax({
   
            type  :  'POST',
            url   :  '/analiseDeCredito/updateCaptcha/',
   
            data : {
               'oldCaptcha' :  $('#oldCaptcha').val()
            },
   
            beforeSend      : function(){
               $('#img-captcha-receita').attr('src','https://beta.sigacbr.com.br/js/loading.gif');
            }
   
         }).done( function( dataR ){
   
            var jsonReturn = $.parseJSON( dataR );
   
            $('#img-captcha-receita').attr('src',jsonReturn.newImgUrl);
   
            $('#tokenReceita_0').val( jsonReturn.token.token_0 );
            $('#tokenReceita_1').val( jsonReturn.token.token_1 );
            $('#oldCaptcha').val( jsonReturn.imgName );
         } )
   
   }
   
   $(document).ready(function(){
                  
         $('#modalResultError').on('hidden.bs.modal', function () {
   
               updateCaptcha();
         })
         
         $('#modalCadastroIncompleto').on('hidden.bs.modal',function(){
            $('#cadIncObs').html('');
         });

         $('#modal_suspense').on('hidden.bs.modal', function(){
            updateCaptcha();
         })
   
         $('#responsive').on('hidden.bs.modal', function () {
               updateCaptcha();
         })
   
            
         $('#img-captcha-update').on('click',function(){
   
            updateCaptcha();
            return false;
   
         })
   
         $('#btn-submit-receita-form').on('click', function(){ 
   
            $.ajax({
   
               type : "POST",
   
               url : "/analiseDeCredito/getCpfReceita/",
   
               data : {
                  'cpf'       : $('#CPF').val(),
                  'captcha'   : $('#Captha').val(),
                  'token_0'   : $('#tokenReceita_0').val(),
                  'token_1'   : $('#tokenReceita_1').val(),
               },
   
               beforeSend : function(){
   
                  $('.panel').block({
                      overlayCSS: {
                          backgroundColor: '#fff'
                      },
                      message: '<img src="https://beta.sigacbr.com.br/js/loading.gif" /> Buscando CPF...',
                      css: {
                          border: 'none',
                          color: '#333',
                          background: 'none'
                      }
                  });
   
                  window.setTimeout(function () {
                   $('.panel').unblock();
                  }, 1000);
               }
   
            })
   
            .done( function( data ){
                              
               var jsonReturn = $.parseJSON( data );

               console.log(jsonReturn)
               
               if( jsonReturn.inputCount == 0 )
               {
                  $('#modalResultError').modal();
               }

               else
               {
                  
                  if( jsonReturn.isClient == 0 )
                  {
                     $('#modalNaoCadastrado').modal('show');
                     $('#cliNome').val(jsonReturn.nome);
                     $('#cliCpf').val(jsonReturn.cpf);
                  }
                  
                  else
                  {
                     
                     if ( jsonReturn.statusReturn == 1 )
                     {
                        if ( jsonReturn.situacao == "REGULAR" )
                        {
                           $('#form-do-analise').attr('action','/analiseDeCredito/analiseCliente');

                           $('#span-situacao').html(jsonReturn.situacao)
                           $('#span-clinome').html(jsonReturn.nome)
                           $('#span-clicpf').html(jsonReturn.cpf)
                           $('#NOMEsend').val(jsonReturn.nome)
                           $('#CPFsend').val(jsonReturn.cpf)
                           $('#SITUACAOsend').val(jsonReturn.situacao)
                           $('#renda_liquida').val( jsonReturn.rendaLiquida );
                           $('#responsive').modal()


                           $('#input-hdn-pessoa-id').attr('value',jsonReturn.clienteId);
                           $('#input-hdn-pessoa-id2').attr('value',jsonReturn.clienteId);
                           $('.span-isclient').html('Sim');
                           $('#btn-editar-cliente').show();
                           
                           $('#renda_liquida').val( jsonReturn.rendaLiquida );
                        }
      
                        else if( jsonReturn.situacao == "SUSPENSA" )
                        {
                           $('#span-err-situacao').html(jsonReturn.situacao)
                           $('#span-err-clinome').html(jsonReturn.nome)
                           $('#span-err-clicpf').html(jsonReturn.cpf)
                           $('#modal_suspense').modal()
                        }
                     }
                     
                     else if ( jsonReturn.statusReturn == 2 )
                     {

                        $('#input-hdn-pessoa-id3').val(jsonReturn.clienteId);
                        $('#cadIncObs').html('');

                        $.each( jsonReturn.observacoes, function(key, val) {
                           $('#cadIncObs').append('<p> - '+val+'</p>');
                        })

                        $('#modalCadastroIncompleto').modal('show');
                     }
                  }
               }

               
            } )
   
            return false;
   
         })
   })
   
</script>

<!--
               if (jsonReturn.isClient == '0')
               {
                  $('#form-do-analise').attr('action','/analiseDeCredito/step1');
                  $('.span-isclient').html('Não');
                  $('#btn-editar-cliente').hide();                  
               }
   
               else
               {
                  $('#form-do-analise').attr('action','/analiseDeCredito/analiseCliente');
                  $('#input-hdn-pessoa-id').attr('value',jsonReturn.clienteId);
                  $('#input-hdn-pessoa-id2').attr('value',jsonReturn.clienteId);
                  $('.span-isclient').html('Sim');
                  $('#btn-editar-cliente').show();
                  $('#renda_liquida').val( jsonReturn.rendaLiquida );
               }
-->