<?php
/* @var $this AnaliseDeCreditoController */
/* @var $model AnaliseDeCredito */

$this->breadcrumbs=array(
	'Analise De Creditos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List AnaliseDeCredito', 'url'=>array('index')),
	array('label'=>'Create AnaliseDeCredito', 'url'=>array('create')),
	array('label'=>'Update AnaliseDeCredito', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AnaliseDeCredito', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AnaliseDeCredito', 'url'=>array('admin')),
);
?>

<h1>View AnaliseDeCredito #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'Cliente_id',
		'habilitado',
		'data_cadastro',
		'numero_do_pedido',
		'descricao_do_bem',
		'vendedor',
		'telefone_loja',
		'promotor',
		'Procedencia_compra_id',
		'mercadoria_retirada_na_hora',
		'celular_pre_pago',
		'observacao',
		'alerta',
		'valor',
		'entrada',
		'Usuario_id',
	),
)); ?>
