<?php  ?>
<div class="row">
   <div class="col-md-12">
      <div class="alert alert-info">
         <div class="row">
            <div class="col-sm-4">
               <div class="core-box">
                  <div class="heading">
                     <i class="clip-user-6 circle-icon circle-green"></i>
                     <h2>Informações do cliente</h2>
                  </div>
                  <div class="content">
                     <table class="table table-condensed table-hover" >
                        <thead>
                        </thead>
                        <tbody>
                           <tr>
                              <td>Nome: </td>
                              <td>
                                 <?php echo $rascunho->nome_completo ?>
                              </td>
                           </tr>
                           <tr>
                              <td>CPF: </td>
                              <td>                                       
                                 <?php echo $rascunho->cpf ?>
                              </td>
                           </tr>
                           <tr>
                              <td>Renda Líquida: </td>
                              <td>                                       
                                 <?php echo "R$ ". number_format($rascunho->renda_liquida, 2, ",", ".") ?>
                              </td>
                           </tr>
                           <tr>
                              <td>Cadastrado: </td>
                              <td>
                                 <?php echo  (Documento::model()->cadastrado($rascunho->cpf) ? "Sim" : "Não"); ?>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <div class="col-sm-4">
               <div class="core-box">
                  <div class="heading">
                     <i class=" clip-file-2 circle-icon circle-green"></i>
                     <h2>Informações da solicitação</h2>
                  </div>
                  <div class="content">
                     <table class="table table-condensed table-hover">
                        <thead>
                        </thead>
                        <tbody>
                           <tr>
                              <td>Valor solicitado: </td>
                              <td>
                                 <?php echo "R$ " . number_format($rascunho->valor_solicitado,2,",",".") ?>
                              </td>
                           </tr>
                           <?php if ($rascunho->valor_entrada > 0) { ?>
                           <tr>
                              <td>Entrada: </td>
                              <td>
                                 <?php echo "R$ " . number_format($rascunho->valor_entrada, 2, ",", ".") ?>
                              </td>
                           </tr>
                           <?php } ?>
                           
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <div class="col-sm-4" id="resumo-solicitacao">
               <div class="core-box">
                  <div class="heading">
                     <i class=" circle-icon circle-green fa fa-list"></i>
                     <h2>Resumo</h2>
                  </div>                  
                  <div class="content">
                     <table class="table table-condensed table-hover">
                        <thead>
                        </thead>
                        <tbody>
                           <tr>
                              <td>Valor financiado: </td>
                              <td>
                                 <?php echo "R$ " . number_format($rascunho->valor_financiado, 2, ",", ".") ?>
                           </td>
                           </tr>
                           <tr>
                              <td>Número de parcelas: </td>
                              <td>
                                 <?php echo $rascunho->num_parcelas ?>
                              </td>
                           </tr>
                           <tr>
                              <td>Valor da parcela: </td>
                              <td>
                                 <?php echo "R$ ". number_format($rascunho->val_parcelas, 2, ",",".") ?>
                              </td>
                           </tr>
                            <tr>
                              <td>Taxa (%): </td>
                              <td>
                                 <?php echo number_format($rascunho->taxa , 2, ",", "."); ?>
                              </td>
                           </tr>
                           <tr>
                              <td>Valor total: </td>
                              <td >
                                 <?php echo "R$ ". number_format($rascunho->valor_total , 2, ",", "."); ?>
                              </td>
                           </tr>
                           <tr>
                              <td>Carência: </td>
                              <td>
                                 <?php echo $rascunho->carencia; ?>
                              </td>
                           </tr>
                           <tr>
                              <td>Data da 1° parcela: </td>
                              <td>
                                 <?php echo $rascunho->data_primeira_parcela; ?>
                              </td>
                           </tr>
                           <tr>
                              <td>Data da última parcela: </td>
                              <td>
                                 <?php echo $rascunho->data_ultima_parcela; ?>
                              </td>
                           </tr>
                           <tr>
                              <td>Cotação: </td>
                              <td>
                                 <?php echo $cotacao->descricao; ?>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-sm-12">
      <div class="panel panel-default">       
         <div class="panel-heading">
            <i class="fa fa-external-link-square"></i>
               ANÁLISE DE CRÉDITO
            <div class="panel-tools">
               <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
               </a>                  
            </div>
         </div>
         
         <div class="panel-body">

            <form enctype="multipart/form-data" id="form" class="smart-wizard" role="form" method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/analiseDeCredito/doAnalise">
               
               <div id="wizard" class="swMain">
                  
                  <ul>
                        <li>
                           <a href="#step-1">
                              <div class="stepNumber">1</div>
                              <span class="stepDesc">Dados Pessoais</span>
                           </a>
                        </li>
                        <li>
                           <a href="#step-2">
                              <div class="stepNumber">2</div>
                              <span class="stepDesc">Endereço</span>
                           </a>
                        </li>
                        <li>
                           <a href="#step-3">
                              <div class="stepNumber">3</div>
                              <span class="stepDesc">Contato</span>
                           </a>
                        </li>
                        <li>
                           <a href="#step-4">
                              <div class="stepNumber">4</div>
                              <span class="stepDesc">Cônjuge</span>
                           </a>
                        </li>
                        <li>
                           <a href="#step-5">
                              <div class="stepNumber">5</div>
                              <span class="stepDesc">Dados bancários</span>
                           </a>
                        </li>
                        <li>
                           <a href="#step-6">
                              <div class="stepNumber">6</div>
                              <span class="stepDesc">Dados profissionais</span>
                           </a>
                        </li>
                        <li>
                           <a href="#step-7">
                              <div class="stepNumber">7</div>
                              <span class="stepDesc">Referência</span>
                           </a>
                        </li>
                        <li>
                           <a href="#step-8">
                              <div class="stepNumber">8</div>
                              <span class="stepDesc">Dados adicionais da compra</span>
                           </a>
                        </li>
                        <li>
                           <a href="#step-9">
                              <div class="stepNumber">9</div>
                              <span class="stepDesc">Anexos</span>
                           </a>
                        </li>
                        <li>
                           <a href="#step-10">
                              <div class="stepNumber">10</div>
                              <span class="stepDesc">Finalizar</span>
                           </a>
                        </li>
                  </ul>
                  
                  <div class="progress progress-striped active progress-sm">
                     <div aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar progress-bar-success step-bar">
                        <span class="sr-only"> 0% Complete (success)</span>
                     </div>
                  </div>

                  <div id="step-1">
                     <h2 class="StepTitle">Dados Pessoais</h2>
                     <div class="row">
                        <div class="row-centralize">
                           <div class="col-md-6">
                              
                              <div class="form-group">
                                 <label class="control-label">
                                    Nome completo <span class="symbol required"></span>
                                 </label>
                                 <input name="Pessoa[nome]" type="text" class="form-control" value="<?php echo $rascunho->nome_completo ?>" readonly>
                              </div>
                              
                              <div class="row">
                                 <div class="col-md-7">
                                    <div class="form-group">
                                       <label class="control-label">
                                          CPF <span class="symbol required"></span>
                                       </label>
                                       <input name="CPFCliente[numero]" type="text" class="form-control" value="<?php echo $rascunho->cpf ?>" readonly>
                                    </div>
                                 </div>
                                 <div class="col-md-5">
                                    <div class="form-group">
                                       <label class="control-label">
                                          É titular do cpf <span class="symbol required"></span>
                                       </label>
                                       <select name="Cadastro[titular_do_cpf]" id="Cadastro_titular_do_cpf" class="form-control search-select">
                                          <?php foreach ( TTable::model()->findAll() as $tt ): ?>
                                             <option <?php if( $rascunho->titular_do_cpf == $tt->id ){ echo "selected"; } ?> value="<?php echo $tt->id ?>" ><?php echo $tt->flag ?></option>   
                                          <?php endforeach ?>
                                       </select>
                                    </div>
                                 </div>
                              </div>
                              
                              <div class="row">
                                 <div class="col-md-5">
                                    <div class="form-group">
                                       <label class="control-label">
                                          Nascimento <span class="symbol required"></span>
                                       </label>
                                       <input value="<?php echo $rascunho->nascimento ?>" name="Pessoa[nascimento]" id="nascimento" class="form-control input-mask-date" type="text">
                                    </div>
                                 </div>
                                 <div class="col-md-7">
                                    <div class="form-group">
                                       <label class="control-label">Sexo <span class="symbol required"></span></label>
                                       <select name="Pessoa[sexo]" id="Pessoa_sexo" class="form-control search-select">
                                          <?php foreach ( Sexo::model()->findAll() as $sexo ): ?>
                                             <option <?php if( $rascunho->sexo == $sexo->sigla ){ echo "selected"; } ?> value="<?php echo $sexo->sigla ?>" ><?php echo $sexo->sexo ?></option>
                                          <?php endforeach ?>
                                       </select>
                                    </div>
                                 </div>
                              </div>

                              <div class="row">                                 
                                 <div class="col-md-5">
                                    <div class="form-group">
                                       <label class="control-label">Nacionalidade <span class="symbol required"></span></label>
                                       <select id="select-nacionalidade" name="Pessoa[nacionalidade]" class="form-control search-select">
                                          <option <?php if($rascunho->nacionalidade == "Brasileira") { echo "selected"; } ?> value="Brasileira">Brasileira</option>
                                          <option <?php if($rascunho->nacionalidade == "Extrangeira") { echo "selected"; } ?> value="Extrangeira">Extrangeira</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="col-md-7" id="select-estados-wrapper" style="<?php if($rascunho->nacionalidade == "Extrangeira") { echo "display:none;"; } ?>">
                                    <div class="form-group">
                                       <label class="control-label">UF <span class="symbol required"></span></label>
                                       <select id="naturalidade_br" name="naturalidade_br" class="form-control search-select">
                                          <?php foreach (Estados::model()->findAll() as $uf): ?>
                                             <option <?php if($rascunho->naturalidade_cliente == $uf->sigla){echo "selected";} ?> value="<?php echo $uf->sigla ?>"><?php echo $uf->sigla ?></option>
                                          <?php endforeach ?>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="col-md-7" id="select-paises-wrapper" style="<?php if($rascunho->nacionalidade == "Brasileira") { echo "display:none;"; } ?>">
                                    <div class="form-group">
                                       <label class="control-label">País <span class="symbol required"></span></label>
                                       <select id="naturalidade_ext" name="naturalidade_ext" class="form-control search-select">
                                          <?php foreach (Pais::model()->findAll() as $pais): ?>
                                             <option <?php if($rascunho->naturalidade_ext == $pais->paisNome){echo "selected";} ?> value="<?php echo $pais->paisNome ?>"><?php echo $pais->paisNome ?></option>
                                          <?php endforeach ?>
                                       </select>
                                       <!--naturalidade_ext-->
                                    </div>
                                 </div>
                              </div>
                              <div class="form-group">
                                <label class="control-label">Nome do pai</label>
                                <input value="<?php echo $rascunho->nome_do_pai ?>" name="Cadastro[nome_do_pai]" type="text" class="form-control">
                              </div>
                              <div class="form-group">
                                 <label class="control-label">
                                   Nome da mãe <span class="symbol required"></span>
                                 </label>
                                 <input value="<?php echo $rascunho->nome_da_mae ?>" name="Cadastro[nome_da_mae]" type="text" class="form-control">
                              </div>
                              <div class="form-group">
                                 <label class="control-label">N° de dependentes</label>
                                 <input value="<?php echo $rascunho->numero_de_dependentes ?>" name="Cadastro[numero_de_dependentes]" type="text" class="form-control">
                              </div>
                           </div>

                           <div class="col-md-6">
                              <div class="row">
                                 <div class="col-md-5">
                                    <div class="form-group">
                                       <label class="control-label">Estado civil</label>
                                       <select id="select-estado-civil" name="Pessoa[estado_civil]" class="form-control search-select">
                                          <option value="Solteiro" <?php if($rascunho->estado_civil == "Solteiro"){echo "selected";} ?>>Solteiro</option>
                                          <option value="Casado" <?php if($rascunho->estado_civil == "Casado"){echo "selected";} ?> >Casado</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="col-md-7">
                                    <div class="form-group">
                                       <label class="control-label">Cônjuge compõe renda</label>
                                       <select class="form-control search-select" name="Cadastro[conjugue_compoe_renda]" id="conjugue_compoe_renda">
                                          <?php foreach (TTable::model()->findAll() as $sim_nao): ?>
                                             <option value="<?php echo $sim_nao->id ?>" <?php if($rascunho->conjugue_compoe_renda == $sim_nao->id){echo "selected";} ?> ><?php echo $sim_nao->flag ?></option>
                                          <?php endforeach ?>
                                       </select>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 
                                 <div class="col-md-5">
                                    <div class="form-group">
                                       <label class="control-label">Tipo de documento</label>
                                       <select name="DocumentoCliente[Tipo_documento_id]" id="Tipo_documento_id" class="form-control search-select">
                                          <?php foreach (TipoDocumento::model()->findAll(array('condition'=>'id != 1')) as $td): ?>
                                             <option value="<?php echo $td->id ?>" <?php if($td->tipo == $rascunho->tipo_de_documento){echo "selected";} ?>><?php echo $td->tipo ?></option>
                                          <?php endforeach ?>
                                       </select>
                                    </div>
                                 </div>

                                 <div class="col-md-7">
                                    <div class="form-group">
                                       <label class="control-label">Número do documento <span class="symbol required"></span></label>
                                       <input value="<?php echo $rascunho->numero_do_documento ?>" name="DocumentoCliente[numero]" type="text" class="form-control">
                                    </div>
                                 </div>

                              </div>
                              <div class="row">
                                 <div class="col-md-12">
                                    <div class="form-group">
                                       <label class="control-label">Órgão emissor</label>
                                       <input type="text" name="DocumentoCliente[orgao_emissor]" class="form-control" value="<?php echo $rascunho->orgao_emissor_documento ?>">
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6" >
                                    <div class="form-group">
                                       <label class="control-label">UF Emissor</label>
                                       <select name="DocumentoCliente[uf_emissor]" class="form-control search-select" id="uf_emissor">
                                          <?php foreach (Estados::model()->findAll() as $uf): ?>
                                             <option value="<?php echo $uf->sigla ?>" <?php if($rascunho->uf_emissor == $uf->sigla){echo "selected";} ?> ><?php echo $uf->nome ?></option>
                                          <?php endforeach ?>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Data de emissão</label>
                                       <input value="<?php echo $rascunho->data_emissao_documento ?>" class="form-control input-mask-date" type="text" name="DocumentoCliente[data_emissao]">
                                    </div>
                                 </div>
                              </div>
                           </div>
                           
                           <div class="col-md-3">
                           </div>
                        </div>
                        <div class="wizard-step-btns-wrapper">
                           <div class="col-md-4" style="float:right">
                              <div class="form-group">
                                 <button class="btn btn-blue next-step btn-block">
                                 Avançar <i class="fa fa-arrow-circle-right"></i>
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>

                  <div id="step-2">
                     <h2 class="StepTitle">Endereço</h2>
                     <div class="row">
                        <div class="row-centralize">
                           <div class="col-md-6">
                              <div class="row">
                                 <div class="col-md-3">
                                    <div class="form-group">
                                       <label class="control-label">
                                          CEP <span class="symbol required"></span>
                                       </label>
                                       <input value="<?php echo $rascunho->cep_pessoal ?>" class="form-control cep" type="text" name="EnderecoCliente[cep]" id="cep_cliente">
                                    </div>
                                 </div>
                                 <div class="col-md-9">
                                    <div class="form-group">
                                       <label class="control-label">
                                          Logradouro <span class="symbol required"></span>
                                       </label>
                                       <input value="<?php echo $rascunho->logradouro ?>" class="form-control" type="text" name="EnderecoCliente[logradouro]" id="logradouro_cliente">
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Cidade <span class="symbol required"></span></label>
                                       <input value="<?php echo $rascunho->cidade ?>" class="form-control" type="text" name="EnderecoCliente[cidade]" id="cidade_cliente">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Bairro <span class="symbol required"></span></label>
                                       <input value="<?php echo $rascunho->bairro ?>" class="form-control" type="text" name="EnderecoCliente[bairro]" id="bairro_cliente">
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">
                                          Tempo de residência (mes/ano) <span class="symbol required"></span>
                                       </label>
                                       <input value="<?php echo $rascunho->tempo_de_residencia ?>" class="form-control" type="text" name="EnderecoCliente[tempo_de_residencia]" id="tempo_de_residencia">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Tipo de moradia <span class="symbol required"></span></label>
                                       <select name="EnderecoCliente[Tipo_Moradia_id]" id="Tipo_Moradia_id" class="form-control search-select">
                                          <?php foreach (TipoMoradia::model()->findAll() as $tipoDeMoradia): ?>
                                             <option value="<?php echo $tipoDeMoradia->id ?>" <?php if($rascunho->tipo_moradia_id == $tipoDeMoradia->id){echo "selected";} ?>><?php echo $tipoDeMoradia->tipo ?></option>
                                          <?php endforeach ?>
                                       </select>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="row">
                                 <div class="col-md-3">
                                    <div class="form-group">
                                       <label class="control-label">Número <span class="symbol required"></span></label>
                                       <input value="<?php echo $rascunho->numero ?>" class="form-control" type="text" name="EnderecoCliente[numero]">
                                    </div>
                                 </div>
                                 <div class="col-md-9">
                                    <div class="form-group">
                                       <label class="control-label">Complemento</label>
                                       <input value="<?php echo $rascunho->complemento ?>" class="form-control" type="text" name="EnderecoCliente[complemento]">
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">UF <span class="symbol required"></span></label>
                                       <select name="EnderecoCliente[uf]" class="form-control search-select" id="">
                                          <?php foreach (Estados::model()->findAll() as $estado): ?>
                                             <option value="<?php echo $estado->sigla ?>" <?php if($rascunho->uf_endereco_pessoal == $estado->sigla){echo "selected";} ?> ><?php echo $estado->sigla ?></option>
                                          <?php endforeach ?>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Tipo de endereço <span class="symbol required"></span></label>
                                       <select name="EnderecoCliente[Tipo_Endereco_id]" class="form-control search-select" id="">
                                          <?php foreach (TipoEndereco::model()->findAll() as $te): ?>
                                             <option value="<?php echo $te->id ?>" <?php if($rascunho->tipo_endereco_pessoal == $te->id){echo "selected";} ?> ><?php echo $te->tipo ?></option>
                                          <?php endforeach ?>
                                       </select>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-6">
                              </div>
                           </div>
                        </div>
                        <div class="wizard-step-btns-wrapper">
                           <div class="col-md-4" style="float:left">
                              <div class="form-group">
                                 <button class="btn btn-blue back-step btn-block">
                                   <i class="fa fa-arrow-circle-left"></i> Voltar
                                 </button>
                              </div>
                           </div>
                           <div class="col-md-4" style="float:right">
                              <div class="form-group">
                                 <button class="btn btn-blue next-step btn-block">
                                   Avançar <i class="fa fa-arrow-circle-right"></i>
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>

                  <div id="step-3">
                     <h2 class="StepTitle">Contato</h2>
                     
                     <div class="row">
                        <div class="row-centralize">
                           <div class="col-md-6">
                              <div class="row">
                                 <div class="col-md-4">
                                    <div class="form-group">
                                       <label class="control-label">Telefone 1 <span class="symbol required"></span></label>
                                       <input value="<?php echo $rascunho->telefone1_numero ?>" class="form-control telefone" type="text" name="TelefoneCliente[numero]">
                                    </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                       <label class="control-label">Tipo de telefone</label>
                                       <select name="TelefoneCliente[Tipo_Telefone_id]" id="" class="form-control search-select">
                                          <?php foreach (TipoTelefone::model()->findAll() as $tf): ?>
                                             <option value="<?php echo $tf->id ?>" <?php if($rascunho->telefone1_tipo == $tf->id){echo "selected";} ?>><?php echo $tf->tipo ?></option>
                                          <?php endforeach ?>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                       <label class="control-label">Ramal </label>
                                       <input value="<?php echo $rascunho->telefone1_ramal ?>" class="form-control" type="text" name="TelefoneCliente[ramal]">
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-4">
                                    <div class="form-group">
                                       <label class="control-label">Telefone 2 <span class="symbol required"></span></label>
                                       <input value="<?php echo $rascunho->telefone2_numero ?>" class="form-control telefone" type="text" name="TelefoneCliente2[numero]">
                                    </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                       <label class="control-label">Tipo de telefone</label>
                                       <select name="TelefoneCliente2[Tipo_Telefone_id]" id="" class="form-control search-select">
                                          <?php foreach (TipoTelefone::model()->findAll() as $tf): ?>
                                             <option value="<?php echo $tf->id ?>" <?php if($rascunho->telefone2_tipo == $tf->id){echo "selected";} ?>><?php echo $tf->tipo ?></option>
                                          <?php endforeach ?>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                       <label class="control-label">Ramal </label>
                                       <input value="<?php echo $rascunho->telefone2_ramal ?>" class="form-control" type="text" name="TelefoneCliente2[ramal]">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <label class="control-label">Email</label>
                                    <input value="<?php echo $rascunho->email_cliente ?>" class="form-control" id="email" type="text" name="EmailCliente[email]">
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <label class="control-label">Facebook</label>
                                    <input value="<?php echo $rascunho->cliente_facebook ?>" class="form-control" type="text" name="ContatoCliente[facebook]">
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-8">
                                 <div class="form-group">
                                    <label class="control-label">Skype</label>
                                    <input value="<?php echo $rascunho->cliente_skype ?>" id="skype" class="form-control" type="text" name="ContatoCliente[skype]">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="wizard-step-btns-wrapper">
                           <div class="col-md-4" style="float:left">
                              <div class="form-group">
                                 <button class="btn btn-blue back-step btn-block">
                                    <i class="fa fa-arrow-circle-left"></i> Voltar
                                 </button>
                              </div>
                           </div>
                           <div class="col-md-4" style="float:right">
                              <div class="form-group">
                                 <button class="btn btn-blue next-step btn-block">
                                    Avançar <i class="fa fa-arrow-circle-right"></i>
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>

                  <div id="step-4">
                     <div class="row">
                        <div class="row-centralize">
                           <!--Dados conjuge esquerda-->
                           <div class="col-md-6">
                              <div class="row">
                                 <div class="col-md-12">
                                    <h1 style="font-size:20px;">Dados profissionais do Conjuge</h1>
                                    <hr>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-12">
                                    <div class="form-group"> 
                                       <label class="control-label">Nome completo</label>
                                       <input value="<?php echo $rascunho->nome_completo_conjuge?>" name="Conjuge[nome]" type="text" class="draft_sent form-control">
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-12">
                                    <div class="form-group">
                                       <label class="control-label">CPF do Cônjuge</label>
                                       <input value="<?php echo $rascunho->cpf_do_conjuge?>" name="CPF_Conjuge[numero]" type="text" class="draft_sent form-control">
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-5">
                                    <div class="form-group">
                                       <label class="control-label">Tipo de documento</label>
                                       <select name="Doc_Conjuge[Tipo_documento_id]" class="form-control search-select" id="">
                                          <?php foreach (TipoDocumento::model()->findAll('id <> 1') as $tdoc): ?>
                                             <option <?php if($rascunho->tipo_de_documento_conjuge == $tdoc->id){echo "selected";} ?> value="<?php echo $tdoc->id ?>"><?php echo $tdoc->tipo ?></option>
                                          <?php endforeach ?>
                                       </select>      
                                    </div>
                                 </div>
                                 <div class="col-md-7">
                                    <div class="form-group">
                                       <label class="control-label">Número do documento</label>
                                       <input value="<?php echo $rascunho->numero_do_documento_conjuge ?>" name="Doc_Conjuge[numero]" type="text" class="draft_sent form-control">
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-12">
                                    <div class="form-group">
                                       <label class="control-label">Órgão emissor</label>
                                       <input value="<?php echo $rascunho->orgao_emissor_documento_conjuge ?>" type="text" name="Doc_Conjuge[orgao_emissor]" class="draft_sent form-control">
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">UF Emissor </label>
                                       <select name="Doc_Conjuge[uf_emissor]" class="form-control search-select" id="">
                                          <?php foreach (Estados::model()->findAll() as $uf): ?>
                                             <option <?php if($rascunho->uf_emissor_documento_conjuge == $uf->sigla){echo "selected";} ?> value="<?php echo $uf->sigla ?>"><?php echo $uf->nome ?></option>
                                          <?php endforeach ?>
                                       </select>      
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Data de emissão</label>
                                       <input value="<?php echo $rascunho->data_emissao_documento_conjuge ?>" id="data_doc_emissao" class="draft_sent form-control input-mask-date" type="text" name="Doc_Conjuge[data_emissao]">
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-5">
                                    <div class="form-group">
                                       <label class="control-label">Nascimento</label>
                                       <input value="<?php echo $rascunho->conjuge_nascimento?>" class="draft_sent form-control input-mask-date" type="text" name="Conjuge[nascimento]">
                                    </div>
                                 </div>
                                 <div class="col-md-7">
                                    <div class="form-group">
                                       <label class="control-label">Sexo </label>
                                       <select name="Conjuge[sexo]" class="form-control search-select" id="">
                                           <?php foreach (Sexo::model()->findAll() as $sexo): ?>
                                              <option <?php if($sexo->sigla == $rascunho->sexo_conjuge){echo"selected";} ?> value="<?php echo $sexo->sigla ?>"><?php echo $sexo->sexo ?></option>
                                           <?php endforeach ?>
                                        </select>       
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-5">
                                    <div class="form-group">
                                       <label class="control-label">Nacionalidade</label>
                                       <select id="select-nacionalidade-con" name="Conjuge[nacionalidade]" class="draft_sent form-control search-select">
                                          <option <?php if( $rascunho->nacionalidade_conjuge == "Brasileira" ) {echo"selected";} ?> value="Brasileira">Brasileira</option>
                                          <option <?php if( $rascunho->nacionalidade_conjuge == "Extrangeira" ) {echo"selected";} ?> value="Extrangeira">Extrangeira</option>
                                       </select>
                                    </div>
                                 </div>

                                 <div class="col-md-7" id="select-estados-wrapper-con" style="<?php if( $rascunho->nacionalidade_conjuge == "Extrangeira" ) {echo"display:none;";} ?>">
                                    <div class="form-group">
                                       <label class="control-label">Estados</label>
                                       <select name="naturalidade_con_br" class="form-control search-select" id="naturalidade_con_br">
                                          <?php foreach (Estados::model()->findAll() as $estado): ?>
                                             <option <?php if( $rascunho->naturalidade_conjuge == $estado->sigla ) {echo"selected";} ?> value="<?php echo $estado->sigla ?>"><?php echo $estado->nome ?></option>
                                          <?php endforeach ?>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="col-md-7" id="select-paises-wrapper-con" style="<?php if( $rascunho->nacionalidade_conjuge == "Brasileira" ) {echo"display:none;";} ?>">
                                    <div class="form-group">
                                       <label class="control-label">País</label>
                                       <select name="naturalidade_con_ext" class="form-control search-select" id="naturalidade_con_ext">
                                          <?php foreach (Pais::model()->findAll() as $pais): ?>
                                             <option <?php if( $rascunho->naturalidade_conjuge_ext == $pais->paisNome ) {echo"selected";} ?> value="<?php echo $pais->paisNome ?>"><?php echo $pais->paisNome ?></option>
                                          <?php endforeach ?>
                                       </select>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-12">
                                    <h1 style="font-size:20px;">Dados profissionais do Conjuge</h1>
                                    <hr>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Renda Líquida R$</label>
                                       <input value="<?php echo $rascunho->renda_liquida_conjuge ?>" class="draft_sent form-control dinheiro" type="text" name="DadosProfissionaisConjuge[renda_liquida]">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Mês / Ano Renda</label>
                                       <input value="<?php echo $rascunho->mes_ano_renda_conjuge ?>"  class="draft_sent form-control mes_ano_renda" type="text" name="DadosProfissionaisConjuge[mes_ano_renda]">
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Ocupação atual</label>
                                       <select name="DadosProfissionaisConjuge[Tipo_Ocupacao_id]" class="form-control search-select" id="">
                                          <?php foreach (TipoOcupacao::model()->findAll() as $to): ?>
                                             <option <?php if($rascunho->ocupacao_atual_conjuge == $to->id){echo "selected";} ?> value="<?php echo $to->id ?>"><?php echo $to->tipo ?></option>
                                          <?php endforeach ?>
                                       </select>      
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Profissão</label>
                                       <select name="DadosProfissionaisConjuge[profissao]" class="form-control search-select" id="">
                                          <?php foreach (Profissao::model()->findAll() as $profi): ?>
                                             <option <?php if($rascunho->profissao_conjuge){echo "selected";} ?> value="<?php echo $profi->profissao ?>"><?php echo $profi->profissao ?></option>
                                          <?php endforeach ?>
                                       </select>      
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Tipo de comprovante</label>
                                       <select name="DadosProfissionaisConjuge[TipoDeComprovante]" class="form-control search-select" id="">
                                          <?php foreach (TipoDeComprovante::model()->findAll() as $tipC): ?>
                                             <option <?php if($rascunho->tipo_de_comprovante_conjuge == $tipC->tipo){echo "selected";} ?> value="<?php echo $tipC->tipo ?>"><?php echo $tipC->tipo ?></option>
                                          <?php endforeach ?>
                                       </select>      
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!--Dados conjuge direita-->
                           <div class="col-md-6">
                              <div class="row">
                                 <h1 style="font-size:20px; padding-left:15px">Endereço do Conjuge</h1>
                                 <div class="col-md-12">
                                    <hr>
                                    <div class="form-group">
                                       <label class="control-label">Usar mesmo endereço do cliente?</label>
                                       <select name="MesmoEndereco" class="form-control search-select" id="MesmoEndereco">
                                          <?php foreach (TTable::model()->findAll() as $sim_nao): ?>
                                             <option <?php if($rascunho->mesmo_endereco_conjuge == $sim_nao->id){echo "selected";} ?> value="<?php echo $sim_nao->id ?>"><?php echo $sim_nao->flag ?></option>
                                          <?php endforeach ?>
                                       </select>      
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-3">
                                    <div class="form-group">
                                       <label class="control-label">CEP</label>
                                       <input value="<?php echo $rascunho->cep_conjuge?>" id="cep_conjuge" class="draft_sent form-control cep" type="text" name="EnderecoConjuge[cep]">
                                    </div>
                                 </div>
                                 <div class="col-md-9">
                                    <div class="form-group">
                                       <label class="control-label">Logradouro</label>
                                       <input value="<?php echo $rascunho->logradouro_conjuge?>" id="logradouro_conjuge" class="draft_sent form-control" type="text" name="EnderecoConjuge[logradouro]">
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Cidade</label>
                                       <input value="<?php echo $rascunho->cidade_conjuge ?>" id="cidade_conjuge" class="draft_sent form-control" type="text" name="EnderecoConjuge[cidade]">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Bairro</label>
                                       <input value="<?php echo $rascunho->bairro_conjuge?>" id="bairro_conjuge" class="draft_sent form-control" type="text" name="EnderecoConjuge[bairro]">
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-3">
                                    <div class="form-group">
                                       <label class="control-label">Número</label>
                                       <input value="<?php echo $rascunho->numero_endereco_conjuge ?>" class="draft_sent form-control" type="text" name="EnderecoConjuge[numero]">
                                    </div>
                                 </div>
                                 <div class="col-md-9">
                                    <div class="form-group">
                                       <label class="control-label">Complemento</label>
                                       <input value="<?php echo $rascunho->complemento_endereco_conjuge ?>" class="draft_sent form-control" type="text" name="EnderecoConjuge[complemento]">
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">UF</label>
                                       <select name="EnderecoConjuge[uf]" class="form-control search-select" id="">
                                          <?php foreach (Estados::model()->findAll() as $es): ?>
                                             <option <?php if($es->sigla == $rascunho->uf_endereco_conjuge){echo "selected";} ?> value="<?php echo $es->sigla ?>"><?php echo $es->nome ?></option>
                                          <?php endforeach ?>
                                       </select>      
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Tipo de endereço</label>
                                       <select name="EnderecoConjuge[Tipo_Endereco_id]" class="form-control search-select" id="">
                                          <?php foreach (TipoEndereco::model()->findAll() as $te): ?>
                                             <option <?php if( $te->id == $rascunho->tipo_de_endereco_conjuge ){echo "selected";} ?> value="<?php echo $te->id ?>"><?php echo $te->tipo ?></option>
                                          <?php endforeach ?>
                                       </select>      
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-12">
                                    <h1 style="font-size:20px;">Contato do Conjuge</h1>
                                    <hr>
                                 </div>      
                              </div>
                              <div class="row">
                                 <div class="col-md-4">
                                    <div class="form-group">
                                       <label class="control-label">Telefone</label>
                                       <input value="<?php echo $rascunho->telefone_conjuge?>" class="draft_sent form-control telefone" type="text" name="TelefoneConjuge[numero]">
                                    </div>
                                 </div>
                                 <div class="col-md-8">
                                    <div class="form-group">
                                       <label class="control-label">Tipo de telefone</label>
                                       <select name="TelefoneConjuge[Tipo_Telefone_id]" class="form-control search-select" id="">
                                          <?php foreach ( TipoTelefone::model()->findAll() as $tt ): ?>
                                             <option <?php if( $rascunho->tipo_de_telefone_conjuge == $tt->id ){echo "selected";} ?> value="<?php echo $tt->id ?>"><?php echo $tt->tipo ?></option>
                                          <?php endforeach ?>
                                       </select>      
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Email</label>
                                       <input value="<?php echo $rascunho->email_conjuge ?>" class="draft_sent form-control" type="text" name="EmailConjuge[email]">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="wizard-step-btns-wrapper">
                              <div class="col-md-4" style="float:left">
                                 <div class="form-group">
                                    <button class="btn btn-blue back-step btn-block">
                                       <i class="fa fa-arrow-circle-left"></i> Voltar
                                    </button>
                                 </div>
                              </div>
                              <div class="col-md-4" style="float:right">
                                 <div class="form-group">
                                    <button class="btn btn-blue next-step btn-block">
                                       Avançar <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                 </div>
                              </div>
                        </div>
                     </div>
                  </div>

                  <div id="step-5">
                     <h2 class="StepTitle">Dados bancários</h2>
                     <div class="row">
                        <div class="row-centralize">
                           <div class="col-md-6">
                              <div class="row">
                                 <div class="col-md-5">
                                    <div class="form-group">
                                       <label class="control-label">N° conta</label>
                                       <input value="<?php echo $rascunho->numero_conta ?>" class="form-control" type="text" name="DadosBancarios[numero]">
                                    </div>
                                 </div>
                                 <div class="col-md-5">
                                    <div class="form-group">
                                       <label class="control-label">Tipo de conta</label>
                                       <select name="DadosBancarios[Tipo_Conta_Bancaria_id]" id="" class="form-control search-select">
                                          <?php foreach (TipoContaBancaria::model()->findAll() as $tcb): ?>
                                             <option value="<?php echo $tcb->id ?>" <?php if($rascunho->tipo_de_conta == $tcb->id){echo "selected";} ?> ><?php echo $tcb->tipo ?></option>
                                          <?php endforeach ?>
                                       </select>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="row">
                                 <div class="col-md-5">
                                    <div class="form-group">
                                       <label class="control-label">
                                          Agência <span class="symbol required"></span>
                                       </label>
                                       <input value="<?php echo $rascunho->agencia_conta; ?>" class="draft_sent form-control" type="text" name="DadosBancarios[agencia]">
                                    </div>
                                 </div>
                                 <div class="col-md-5">
                                    <div class="form-group">
                                       <label class="control-label">Banco</label>
                                       <select name="DadosBancarios[Banco_id]" class="draft_sent form-control search-select" id="">
                                          <?php foreach (Banco::model()->findAll() as $banco): ?>
                                             <option value="<?php echo $banco->id ?>" <?php if($rascunho->banco_conta == $banco->id){echo "selected";} ?>><?php echo $banco->nome ?></option>
                                          <?php endforeach ?>
                                       </select>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-3">
                                    <div class="form-group">
                                       <label class="control-label">Data de abertura</label>
                                       <input value="<?php echo $rascunho->data_de_abertura_conta ?>" class="draft_sent form-control ipt_date" type="text" name="DadosBancarios[data_abertura]">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="wizard-step-btns-wrapper">
                           <div class="col-md-4" style="float:left">
                              <div class="form-group">
                                 <button class="btn btn-blue back-step btn-block">
                                    <i class="fa fa-arrow-circle-left"></i> Voltar
                                 </button>
                              </div>
                           </div>
                           <div class="col-md-4" style="float:right">
                              <div class="form-group">
                                 <button class="btn btn-blue next-step btn-block">
                                    Avançar <i class="fa fa-arrow-circle-right"></i>
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>

                  <div id="step-6">
                     <h2 class="StepTitle">Dados Profissionais</h2>
                     <div class="row">
                        <div class="row-centralize">
                           <div class="col-md-6">
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Classe Profissional</label>
                                       <select name="DadosProfissionais[Classe_Profissional_id]" class="form-control search-select" id="">
                                          <?php foreach (ClasseProfissional::model()->findAll() as $cp): ?>
                                             <option <?php if( $rascunho->classe_profissional == $cp->id ) {echo"selected";} ?> value="<?php echo $cp->id ?>"><?php echo $cp->classe ?></option>
                                          <?php endforeach ?>
                                       </select>      
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">CNPJ/CPF</label>
                                       <input value="<?php echo $rascunho->cpf_cnpj_profissional ?>" class="draft_sent form-control" type="text" name="DadosProfissionais[cnpj_cpf]">
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Empresa</label>
                                       <input value="<?php echo $rascunho->empresa_profissional ?>" class="draft_sent form-control" type="text" name="DadosProfissionais[empresa]">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Ocupação atual</label>
                                       <select name="DadosProfissionais[Tipo_Ocupacao_id]" class="form-control search-select" id="">
                                          <?php foreach (TipoOcupacao::model()->findAll() as $to): ?>
                                             <option <?php if( $to->id == $rascunho->ocupacao_atual ){echo "selected";} ?> value="<?php echo $to->id ?>"><?php echo $to->tipo ?></option>
                                          <?php endforeach ?>
                                       </select>      
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Data de admissão</label>
                                       <input value="<?php echo $rascunho->data_admissao ?>" class="draft_sent form-control ipt_date" type="text" name="DadosProfissionais[data_admissao]">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Profissão</label>
                                       <select name="DadosProfissionais[profissao]" class="form-control search-select" id="">
                                          <?php foreach (Profissao::model()->findAll() as $profissao): ?>
                                             <option <?php if( $rascunho->profissao == $profissao->profissao ) {echo"selected";} ?> value="<?php echo $profissao->profissao ?>"><?php echo $profissao->profissao ?></option>
                                          <?php endforeach ?>
                                       </select>      
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Renda Líquida R$</label>
                                       <input value="<?php echo $rascunho->renda_liquida ?>" value="<?php echo $rascunho->renda_liquida ?>" class="draft_sent form-control dinheiro" type="text" name="DadosProfissionais[renda_liquida]">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Mês / Ano Renda</label>
                                       <input value="<?php echo $rascunho->mes_ano_renda ?>" class="draft_sent form-control mes_ano_renda" type="text" name="DadosProfissionais[mes_ano_renda]">
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Órgão Pagador</label>
                                       <select name="DadosProfissionais[orgao_pagador]" class="form-control search-select" id="">
                                          <?php foreach (OrgaoPagador::model()->findAll() as $op): ?>
                                             <option <?php if( $rascunho->orgao_pagador == $op->nome ) {echo"selected";} ?> value="<?php echo $op->nome ?>"><?php echo $op->nome ?></option>
                                          <?php endforeach ?>
                                       </select>      
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">N° do benefício</label>
                                       <input value="<?php echo $rascunho->numero_do_beneficio ?>" class="draft_sent form-control" type="text" name="DadosProfissionais[numero_do_beneficio]">
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Tipo de comprovante</label>
                                       <select name="DadosProfissionais[TipoDeComprovante]" class="form-control search-select" id="">
                                          <?php foreach (TipoDeComprovante::model()->findAll() as $tdc): ?>
                                             <option <?php if( $rascunho->tipo_de_comprovante == $tdc->tipo ) {echo"selected";} ?> value="<?php echo $tdc->tipo ?>"><?php echo $tdc->tipo ?></option>
                                          <?php endforeach ?>
                                       </select>      
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!--Endereço/Contato Profissional-->
                        <div class="col-md-6">
                           <div class="row">
                              <div class="col-md-3">
                                 <div class="form-group">
                                    <label class="control-label">CEP</label>
                                    <input value="<?php echo $rascunho->cep_endereco_profissional ?>" id="cep_dados_profissionais" class="draft_sent form-control cep" type="text" name="EnderecoProfiss[cep]">
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <label class="control-label">Logradouro</label>
                                    <input value="<?php echo $rascunho->logradouro_endereco_profissional?>" id="logradouro_dados_profissionais" class="draft_sent form-control" type="text" name="EnderecoProfiss[logradouro]">
                                 </div>
                              </div>
                              <div class="col-md-3">
                                 <div class="form-group">
                                    <label class="control-label">Número</label>
                                    <input value="<?php echo $rascunho->numero_endereco_profissional ?>"  class="draft_sent form-control" type="text" name="EnderecoProfiss[numero]">
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-4">
                                 <div class="form-group">
                                   <label class="control-label">Cidade</label>
                                    <input value="<?php echo $rascunho->cidade_endereco_profissional ?>" id="cidade_dados_profissionais" class="draft_sent form-control" type="text" name="EnderecoProfiss[cidade]">
                                 </div>
                              </div>
                              <div class="col-md-8">
                                 <div class="form-group">
                                    <label class="control-label">Bairro</label>
                                    <input value="<?php echo $rascunho->bairro_endereco_profissional ?>" id="bairro_dados_profissionais" class="draft_sent form-control" type="text" name="EnderecoProfiss[bairro]">
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-4">
                                 <div class="form-group">
                                    <label class="control-label">UF</label>
                                    <select name="EnderecoProfiss[uf]" class="form-control search-select" id="">
                                       <?php foreach (Estados::model()->findAll() as $uf): ?>
                                          <option <?php if( $rascunho->uf_endereco_profissional == $uf->sigla ) {echo"selected";} ?> value="<?php echo $uf->sigla ?>"><?php echo $uf->nome ?></option>
                                       <?php endforeach ?>
                                    </select>
                                 </div>
                              </div>
                              <div class="col-md-8">
                                 <div class="form-group">
                                    <label class="control-label">Complemento</label>
                                    <input value="<?php echo $rascunho->complemento_endereco_profissional ?>" class="draft_sent form-control" type="text" name="EnderecoProfiss[complemento]">
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-4">
                                 <div class="form-group">
                                    <label class="control-label">Telefone</label>
                                    <input value="<?php echo $rascunho->telefone_profissional ?>" class="draft_sent form-control telefone" type="text" name="TelefoneProfiss[numero]">
                                 </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="form-group">
                                    <label class="control-label">Ramal</label>
                                    <input value="<?php echo $rascunho->ramal_telefone_profissional ?>" class="draft_sent form-control" type="text" name="TelefoneProfiss[ramal]">
                                 </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="form-group">
                                    <label class="control-label">Tipo de telefone</label>
                                    <select name="TelefoneProfiss[Tipo_Telefone_id]" class="form-control search-select" id="">
                                       <?php foreach (TipoTelefone::model()->findAll() as $tt): ?>
                                          <option <?php if( $rascunho->tipo_telefone_profissional == $tt->id ) {echo"selected";} ?> value="<?php echo $tt->id ?>"><?php echo $tt->tipo ?></option>
                                       <?php endforeach ?>
                                    </select>      
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-4">
                                 <div class="form-group">
                                    <label class="control-label">Email</label>
                                    <input value="<?php echo $rascunho->email_telefone_profissional ?>" class="draft_sent form-control" type="text" name="EmailProfiss[email]">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="wizard-step-btns-wrapper">
                           <div class="col-md-4" style="float:left">
                              <div class="form-group">
                                 <button class="btn btn-blue back-step btn-block">
                                    <i class="fa fa-arrow-circle-left"></i> Voltar
                                 </button>
                              </div>
                           </div>
                           <div class="col-md-4" style="float:right">
                              <div class="form-group">
                                 <button class="btn btn-blue next-step btn-block">
                                    Avançar <i class="fa fa-arrow-circle-right"></i>
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>

                  <div id="step-7">
                     <h2 class="StepTitle">Referência</h2>

                     <div class="row">
                        <div class="row-centralize">
                           <div class="col-md-6">
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Nome <span class="symbol required"></span></label>
                                       <input value="<?php echo $rascunho->referencia1_nome ?>" class="form-control" type="text" name="Referencia1[nome]">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Tipo de referência</label>
                                       <select name="Referencia1[Tipo_Referencia_id]" class="form-control search-select" id="">
                                          <?php foreach (TipoReferencia::model()->findAll() as $tr): ?>
                                             <option <?php if($rascunho->referencia1_tipo == $tr->id){echo "selected";} ?> value="<?php echo $tr->id ?>"><?php echo $tr->tipo ?></option>
                                          <?php endforeach ?>
                                       </select>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Telefone <span class="symbol required"></span></label>
                                       <input value="<?php echo $rascunho->referencia1_telefone_numero ?>" class="draft_sent form-control telefone" type="text" name="TelefoneRef1[numero]">
                                    </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                       <label class="control-label">Ramal</label>
                                       <input value="<?php echo $rascunho->referencia1_telefone_ramal ?>" class="draft_sent form-control" type="text" name="TelefoneRef1[ramal]">
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Nome <span class="symbol required"></span></label>
                                       <input value="<?php echo $rascunho->referencia2_nome?>" class="draft_sent form-control" type="text" name="Referencia2[nome]">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Tipo de referência</label>
                                       <select name="Referencia2[Tipo_Referencia_id]" class="form-control search-select" id="">
                                          <?php foreach (TipoReferencia::model()->findAll() as $tr): ?>
                                             <option <?php if($rascunho->referencia2_tipo == $tr->id){echo "selected";} ?> value="<?php echo $tr->id ?>"><?php echo $tr->tipo ?></option>
                                          <?php endforeach ?>
                                       </select>      
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Telefone <span class="symbol required"></span></label>
                                       <input value="<?php echo $rascunho->referencia2_telefone_numero ?>" class="draft_sent form-control telefone" type="text" name="TelefoneRef2[numero]">
                                    </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                       <label class="control-label">Ramal</label>
                                       <input value="<?php echo $rascunho->referencia2_telefone_ramal ?>" class="draft_sent form-control" type="text" name="TelefoneRef2[ramal]">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="wizard-step-btns-wrapper">
                           <div class="col-md-4" style="float:left">
                              <div class="form-group">
                                 <button class="btn btn-blue back-step btn-block">
                                    <i class="fa fa-arrow-circle-left"></i> Voltar
                                 </button>
                              </div>
                           </div>
                           <div class="col-md-4" style="float:right">
                              <div class="form-group">
                                 <button class="btn btn-blue next-step btn-block">
                                    Avançar <i class="fa fa-arrow-circle-right"></i>
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>

                  <div id="step-8">
                     
                     <h2 class="StepTitle">Resumo</h2>
                     <div class="row">
                        
                        <div class="row-centralize">
                           <div class="col-md-6">
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Número do pedido</label>
                                       <input value="<?php echo $rascunho->numero_do_pedido?>" class="draft_sent form-control" type="text" name="AnaliseDeCredito[numero_do_pedido]">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Vendedor</label>
                                       <input value="<?php echo $rascunho->vendedor?>" class="draft_sent form-control" type="text" name="AnaliseDeCredito[vendedor]">
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Promotor</label>
                                       <input value="<?php echo $rascunho->promotor?>" class="draft_sent form-control" type="text" name="AnaliseDeCredito[promotor]">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Procedência da compra</label>
                                       <select name="AnaliseDeCredito[Procedencia_compra_id]" class="form-control search-select" id="">
                                          <?php foreach (ProcedenciaCompra::model()->findAll() as $pc): ?>
                                             <option <?php if($rascunho->procedencia_da_compra == $pc->id){echo "selected";} ?> value="<?php echo $pc->id ?>"><?php echo $pc->procedencia ?></option>
                                          <?php endforeach ?>
                                       </select>      
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Valor</label>
                                       <input value="<?php echo $rascunho->valor ?>" class="draft_sent form-control dinheiro" type="text" name="AnaliseDeCredito[valor]">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Entrada</label>
                                       <input value="<?php echo $rascunho->entrada ?>" class="draft_sent form-control" type="text" name="AnaliseDeCredito[entrada]">
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Alerta</label>
                                       <select name="AnaliseDeCredito[alerta]" class="form-control search-select" id="">
                                          <?php foreach (Alerta::model()->findAll() as $alerta): ?>
                                             <option <?php if($alerta->alerta == $rascunho->alerta){echo "selected";} ?> value="<?php echo $alerta->alerta ?>"><?php echo $alerta->alerta ?></option>
                                          <?php endforeach ?>
                                       </select>      
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Mercadoria retirada na hora</label>
                                       <select name="AnaliseDeCredito[mercadoria_retirada_na_hora]" class="form-control search-select" id="">
                                          <?php foreach (TTable::model()->findAll() as $sim_nao): ?>
                                             <option <?php if( $rascunho->mercadoria_retirada_na_hora == $sim_nao->id ){echo "selected";} ?> value="<?php echo $sim_nao->id ?>"><?php echo $sim_nao->flag ?></option>
                                          <?php endforeach ?>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">É celular pré-pago</label>
                                       <select name="AnaliseDeCredito[celular_pre_pago]" class="form-control search-select" id="">
                                          <?php foreach (TTable::model()->findAll() as $sim_nao): ?>
                                             <option <?php if( $rascunho->celular_pre_pago == $sim_nao->id ){echo "selected";} ?> value="<?php echo $sim_nao->id ?>"><?php echo $sim_nao->flag ?></option>
                                          <?php endforeach ?>
                                       </select>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-12">
                                    <div class="form-group">
                                       <label class="control-label">Classificação do bem</label>
                                       <?php echo CHtml::dropDownList('Bens','Bens',
                                          CHtml::listData(GrupoDeProduto::model()->findAll(),
                                          'id','nome' ), array('class'=>'draft_sent form-control search-select', 'multiple'=>'multiple', 'placeholder'=>'Selecione'));
                                       ?> 
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-12">
                                    <div class="form-group">
                                       <label class="control-label">Mercadoria</label>
                                       <input value="<?php echo $rascunho->mercadoria?>" class="draft_sent form-control" type="text" name="AnaliseDeCredito[mercadoria]">
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-12">
                                    <div class="form-group">
                                       <label class="control-label">Observação</label>
                                       <textarea value="<?php echo $rascunho->observacao?>" name="AnaliseDeCredito[observacao]" maxlength="100" id="form-field-23" class="draft_sent form-control limited"></textarea>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        
                        <div class="wizard-step-btns-wrapper">
                           <div class="col-md-4" style="float:left">
                              <div class="form-group">
                                 <button class="btn btn-blue back-step btn-block">
                                    <i class="fa fa-arrow-circle-left"></i> Voltar
                                 </button>
                              </div>
                           </div>
                           <div class="col-md-4" style="float:right">
                              <div class="form-group">
                                 <button class="btn btn-blue next-step btn-block">
                                 Avançar <i class="fa fa-arrow-circle-right"></i>
                                 </button>
                              </div>
                           </div>
                        </div>

                     </div>
                  </div>

                  <div id="step-9">
                     <div class="wizard-step-btns-wrapper">
                        <div class="col-md-4" style="float:left">
                           <div class="form-group">
                              <button class="btn btn-blue back-step btn-block">
                                 <i class="fa fa-arrow-circle-left"></i> Voltar
                              </button>
                           </div>
                        </div>
                        <div class="col-md-4" style="float:right">
                           <div class="form-group">
                              <button class="btn btn-blue next-step btn-block">
                              Avançar <i class="fa fa-arrow-circle-right"></i>
                              </button>
                           </div>
                        </div>
                     </div>
                  </div>

                  <div id="step-10">
                     <div class="alert alert-block alert-warning fade in">
                        <button data-dismiss="alert" class="close" type="button">
                           &times;
                        </button>
                        <h4 class="alert-heading"><i class="fa fa-exclamation-triangle"></i> Atenção!</h4>
                        <p>
                           Antes de submeter o formulário, certifique-se de que os dados informados estão corretos. Lembre-se que todos os dados solicitados são importantes para a análise, 
                           portanto, revise-os antes de concluir.
                        </p>
                        <p>
                           <input type="submit" class="btn btn-yellow" value="Estou ciente, quero enviar.">
                        </p>
                     </div>
                     <div class="wizard-step-btns-wrapper">
                        <div class="col-md-4" style="float:left">
                           <div class="form-group">
                              <button class="btn btn-blue back-step btn-block">
                                <i class="fa fa-arrow-circle-left"></i> Voltar
                              </button>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

            </form>

         </div>

      </div>
   </div>
</div>

<script type="text/javascript">
   $(function(){

      $.pnotify.defaults.history = false;

      $.pnotify({
         title    : 'Atenção',
         text     : <?php echo json_encode(Yii::app()->session['usuario']->nome_utilizador); ?> + ', por favor, lembre-se de anexar, na sessão de anexos, todas as cópias dos documentos informados neste cadastro. Elas são de extrema importância no processo de avaliação de crédito.',
         type     : 'error'
      });
   });
</script>
