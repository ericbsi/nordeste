<?php ?>
<div class="row">
   
   <div class="col-sm-12">
      <div class="page-header">
         <h1>Propostas da análise: <small style="font-size:18px"><?php echo $analise->codigo ?></small></h1>
      </div>
   </div>

</div>

<div class="row">
   <div class="col-sm-12">
	   <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
		   <thead>
		      <tr>
		         <th>Cód Proposta</th>
		         <th>Financeira</th>
		         <th>Cotação</th>
		         <th>Carência</th>
		         <th>N° parcelas</th>
		         <th>Valor parcela R$</th>
		         <th>Valor entrada R$</th>
		         <th>Valor final R$</th>
		         <th>Status</th>
		      </tr>
		   </thead>
		   <tbody>
		   	<?php foreach( $propsotasTG as $ptg ){ ?>
		      <tr>
		         <td><?php echo $ptg->codigo ?></td>
		         <td><?php echo $ptg->financeira->nome ?></td>
		         <td><?php echo $ptg->cotacao->taxa ?></td>
		         <td><?php echo $ptg->carencia ?></td>
		         <td><?php echo $ptg->qtd_parcelas ?></td>
		         <td><?php echo number_format($ptg->getValorParcela(), 2, ',', '.') ?></td>
		         <td><?php echo number_format($ptg->valor_entrada, 2, ',', '.') ?></td>
		         <td><?php echo number_format($ptg->valor_final, 2, ',', '.') ?></td>
		         <td>
                    <span class="<?php echo $ptg->statusProposta->cssClass ?>">
                    	<?php echo $ptg->statusProposta->status ?>
                 	</span>
                 </td>
		      </tr>
		    <?php } ?>
		    <?php foreach( $propsotasTNG as $ptng ){ ?>
		      <tr>
		         <td><?php echo $ptng->codigo ?></td>
		         <td><?php echo $ptng->financeira->nome ?></td>
		         <td><?php echo $ptng->cotacao->taxa ?></td>
		         <td><?php echo $ptng->carencia ?></td>
		         <td><?php echo $ptng->qtd_parcelas ?></td>
		         <td><?php echo number_format($ptng->getValorParcela(), 2, ',', '.') ?></td>
		         <td><?php echo number_format($ptng->valor_entrada, 2, ',', '.') ?></td>
		         <td><?php echo number_format($ptng->valor_final, 2, ',', '.') ?></td>
		         <td><?php echo $ptng->statusProposta->id ?></td>
		         <td>
                    <span class="<?php echo $ptng->statusProposta->cssClass ?>">
                    	<?php echo $ptng->statusProposta->status ?>
                 	</span>
                 </td>
		      </tr>
		    <?php } ?>
		   </tbody>
	   </table>
   </div>
</div>