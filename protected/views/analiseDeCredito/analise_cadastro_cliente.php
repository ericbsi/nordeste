<?php  ?>
<div class="row">
   <div class="col-md-12">
      <div class="alert alert-info">
         <div class="row">
            <div class="col-sm-4">
               <div class="core-box">
                  <div class="heading">
                     <i class="clip-user-6 circle-icon circle-green"></i>
                     <h2>Informações do cliente</h2>
                  </div>
                  <div class="content">
                     <table class="table table-condensed table-hover" >
                        <thead>
                        </thead>
                        <tbody>
                           <tr>
                              <td>Nome: </td>
                              <td>
                                 <?php echo $nome ?>
                              </td>
                           </tr>
                           <tr>
                              <td>CPF: </td>
                              <td>                                       
                                 <?php echo $cpf ?>
                              </td>
                           </tr>
                           <tr>
                              <td>Renda Líquida: </td>
                              <td>                                       
                                 <?php echo "R$ ". number_format($renda_liquida, 2, ",", ".") ?>
                              </td>
                           </tr>
                           <tr>
                              <td>Cadastrado: </td>
                              <td>
                                 <?php echo $isClient ?>                                          
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <div class="col-sm-4">
               <div class="core-box">
                  <div class="heading">
                     <i class=" clip-file-2 circle-icon circle-green"></i>
                     <h2>Informações da solicitação</h2>
                  </div>
                  <div class="content">
                     <table class="table table-condensed table-hover">
                        <thead>
                        </thead>
                        <tbody>
                           <tr>
                              <td>Valor solicitado: </td>
                              <td>
                                 <?php echo "R$ " . number_format($valor_compra,2,",",".") ?>
                              </td>
                           </tr>
                           <?php if ($valor_entrada > 0) {?>
                           <tr>
                              <td>Entrada: </td>
                              <td>
                                 <?php echo "R$ " . number_format($valor_entrada, 2, ",", ".") ?>         
                              </td>
                           </tr>
                           <?php } ?>
                           
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <div class="col-sm-4" id="resumo-solicitacao">
               <div class="core-box">
                  <div class="heading">
                     <i class=" circle-icon circle-green fa fa-list"></i>
                     <h2>Resumo</h2>
                  </div>                  
                  <div class="content">
                     <table class="table table-condensed table-hover">
                        <thead>
                        </thead>
                        <tbody>
                           <tr>
                              <td>Valor financiado: </td>
                              <td>
                                 <?php echo "R$ " . number_format($valor_financiado_hidden, 2, ",", ".") ?>
                           </td>
                           </tr>
                           <tr>
                              <td>Número de parcelas: </td>
                              <td>
                                 <?php echo $num_parcelas ?>
                              </td>
                           </tr>
                           <tr>
                              <td>Valor da parcela: </td>
                              <td>
                                 <?php echo "R$ ". number_format($val_parcelas, 2, ",",".") ?>
                              </td>
                           </tr>
                            <tr>
                              <td>Taxa (%): </td>
                              <td>
                                 <?php echo number_format($cotacao->taxa , 2, ",", "."); ?>
                              </td>
                           </tr>
                           <tr>
                              <td>Valor total: </td>
                              <td >
                                 <?php echo "R$ ". number_format($val_total_hidden , 2, ",", "."); ?>
                              </td>
                           </tr>
                           <tr>
                              <td>Carência: </td>
                              <td>
                                 <?php echo $carencia; ?>
                              </td>
                           </tr>
                           <tr>
                              <td>Data da 1° parcela: </td>
                              <td>
                                 <?php echo $data_pri_par_hidden; ?>
                              </td>
                           </tr>
                           <tr>
                              <td>Data da última parcela: </td>                              
                              <td>
                                 <?php echo $data_ult_par_hidden; ?>
                              </td>
                           </tr>
                           <tr>
                              <td>Cotação: </td>
                              <td>
                                 <?php echo $cotacao->descricao; ?>
                              </td>
                           </tr>
                        
                           <tr>
                              <td>Carência: </td>
                              <td>
                                 <?php echo $carencia; ?>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-sm-12">
      <div class="panel panel-default">
            
            <div class="panel-heading">
               <i class="fa fa-external-link-square"></i>
               ANÁLISE DE CRÉDITO
               <div class="panel-tools">
                  <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                  </a>
                  <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                  <i class="fa fa-wrench"></i>
                  </a>
                  <a class="btn btn-xs btn-link panel-refresh" href="#">
                  <i class="fa fa-refresh"></i>
                  </a>
                  <a class="btn btn-xs btn-link panel-expand" href="#">
                  <i class="fa fa-resize-full"></i>
                  </a>
                  <a class="btn btn-xs btn-link panel-close" href="#">
                  <i class="fa fa-times"></i>
                  </a>
               </div>
            </div>

            <div class="panel-body">
               <form id="form" class="smart-wizard" role="form" method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/analiseDeCredito/doAnaliseCliente">
                  <div id="wizard" class="swMain">
                     
                     <ul>
                        <li>
                           <a href="#step-1">
                              <div class="stepNumber">1</div>
                              <span class="stepDesc">Dados adicionais da compra</span>
                           </a>
                        </li>
                        
                        <li>
                           <a href="#step-2">
                              <div class="stepNumber">2</div>
                              <span class="stepDesc">Atenção</span>
                           </a>
                        </li>
                        <li>
                           <a href="#step-3">
                              <div class="stepNumber">3</div>
                              <span class="stepDesc">Finalizar</span>
                           </a>
                        </li>
                     </ul>
                     
                     <div class="progress progress-striped active progress-sm">
                        <div aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar progress-bar-success step-bar">
                           <span class="sr-only"> 0% Complete (success)</span>
                        </div>
                     </div>

                     <div id="step-1">
                        <h2 class="StepTitle">Resumo</h2>
                        <div class="row">
                           <div class="row-centralize">
                              <div class="col-md-6">
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Número do pedido</label>
                                          <input class="form-control" type="text" name="AnaliseDeCredito[numero_do_pedido]">
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Vendedor</label>
                                          <input class="form-control" type="text" name="AnaliseDeCredito[vendedor]"  required="required">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Promotor</label>
                                          <input class="form-control" type="text" name="AnaliseDeCredito[promotor]">
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Procedência da compra</label>
                                          <?php echo CHtml::dropDownList('AnaliseDeCredito[Procedencia_compra_id]','AnaliseDeCredito[Procedencia_compra_id]',
                                             CHtml::listData(ProcedenciaCompra::model()->findAll(),
                                             'id','procedencia' ), array('class'=>'form-control search-select', 'prompt'=>'Selecione:', 'required'=>true));
                                             ?> 
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Valor</label>
                                          <input readonly="readonly" required="required" value="<?php echo number_format($valor_financiado_hidden,2,',','.'); ?>" class="form-control dinheiro" type="text" name="AnaliseDeCredito[valor]">
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Entrada</label>
                                          <input <?php if( !empty($valor_entrada) ){ echo 'readonly="readonly"'; } ?> class="form-control dinheiro" type="text" name="AnaliseDeCredito[entrada]" value="<?php if( !empty($valor_entrada) ){ echo number_format($valor_entrada,2,',','.'); } ?>">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Alerta</label>
                                          <?php echo CHtml::dropDownList('AnaliseDeCredito[alerta]','AnaliseDeCredito[alerta]',
                                             CHtml::listData(Alerta::model()->findAll(),
                                             'alerta','alerta' ), array('class'=>'form-control search-select','prompt'=>'Selecione:','required'=>true)); 
                                             ?> 
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Mercadoria retirada na hora</label>
                                          <?php echo CHtml::dropDownList('AnaliseDeCredito[mercadoria_retirada_na_hora]','AnaliseDeCredito[mercadoria_retirada_na_hora]',
                                             CHtml::listData(TTable::model()->findAll(),
                                             'id','flag' ), array('class'=>'form-control search-select','prompt'=>'Selecione:','required'=>true)); 
                                             ?> 
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">É celular pré-pago</label>
                                          <?php echo CHtml::dropDownList('AnaliseDeCredito[celular_pre_pago]','AnaliseDeCredito[celular_pre_pago]',
                                             CHtml::listData(TTable::model()->findAll(),
                                             'id','flag' ), array('class'=>'form-control search-select','prompt'=>'Selecione:','required'=>true)); 
                                          ?> 
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="form-group">
                                          <label class="control-label">Classificação do bem</label>
                                          <?php echo CHtml::dropDownList('Bens','Bens',
                                             CHtml::listData(SubgrupoDeProduto::model()->findAll(),
                                             'id','descricao' ), array('class'=>'form-control search-select', 'multiple'=>'multiple', 'placeholder'=>'Selecione', 'required'=>'required')); ?> 
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="form-group">
                                          <label class="control-label">Mercadoria</label>
                                          <input required="required" class="form-control" type="text" name="AnaliseDeCredito[mercadoria]">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="form-group">
                                          <label class="control-label">Observação</label>
                                          <textarea required="required" name="AnaliseDeCredito[observacao]" maxlength="300" id="form-field-23" class="form-control limited"></textarea>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="wizard-step-btns-wrapper">
                              <div class="col-md-4" style="float:left">
                                  <div class="form-group">
                                    
                                  </div>
                              </div>
                              <div class="col-md-4" style="float:right">
                                  <div class="form-group">
                                    <button class="btn btn-blue next-step btn-block">
                                      Avançar <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                  </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  
                     <div id="step-2">
                        <div class="alert alert-block alert-warning fade in">
                           <button data-dismiss="alert" class="close" type="button">
                              &times;
                           </button>
                           <h4 class="alert-heading"><i class="fa fa-exclamation-triangle"></i> Atenção!</h4>
                           <p>
                              Antes de submeter o formulário, certifique-se de que os dados informados estão corretos. Lembre-se que todos os dados solicitados são importantes para a análise, 
                              portanto, revise-os antes de concluir.
                           </p>                           
                        </div>
                        <div class="wizard-step-btns-wrapper">
                              <div class="col-md-4" style="float:left">
                                  <div class="form-group">
                                    <button class="btn btn-blue back-step btn-block">
                                      <i class="fa fa-arrow-circle-left"></i> Voltar
                                    </button>
                                  </div>
                              </div>
                              <div class="col-md-4" style="float:right">
                                  <div class="form-group">
                                    <button class="btn btn-blue next-step btn-block">
                                      Avançar <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                  </div>
                              </div>
                        </div>
                     </div>

                     <div id="step-3">
                        <div class="alert alert-block alert-warning fade in">
                           <button data-dismiss="alert" class="close" type="button">
                              &times;
                           </button>
                           <p>
                              <input id="btn-enviar-proposta" type="submit" class="btn btn-yellow" value="Estou ciente, quero enviar.">
                           </p>
                           <p class="panel_load"></p>
                        </div>
                        <div class="wizard-step-btns-wrapper">
                              <div class="col-md-4" style="float:left">
                                  <div class="form-group">
                                    <button class="btn btn-blue back-step btn-block">
                                      <i class="fa fa-arrow-circle-left"></i> Voltar
                                    </button>
                                  </div>
                              </div>
                           </div>
                     </div>

                  </div>
                  </div>
                  </div>
                  
                  <input type="hidden" value="<?php echo $valor_compra?>" name="Proposta[valor]">
                  <input type="hidden" value="<?php echo $num_parcelas ?>" name="Proposta[qtd_parcelas]">
                  <input type="hidden" value="<?php echo $carencia ?>" name="Proposta[carencia]">
                  <input type="hidden" value="<?php echo $val_parcelas?>" name="Proposta[valor_parcela]">
                  <input type="hidden" value="<?php echo $valor_entrada ?>" name="Proposta[valor_entrada]">
                  <input type="hidden" value="<?php echo $val_total_hidden ?>" name="Proposta[valor_final]">
                  <input type="hidden" value="<?php echo $cotacao->id ?>" name="Proposta[Tabela_id]">
                  <input type="hidden" value="<?php echo $cotacao->id ?>" name="Proposta_id">
                  <input type="hidden" value="<?php echo $clienteId ?>" name="clienteId">

               </form>
            </div>
      </div>
   </div>
</div>
<style type="text/css">
   .panel_load
   {
      border: none;
      background: transparent;
   }
</style>