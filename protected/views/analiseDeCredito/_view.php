<?php
/* @var $this AnaliseDeCreditoController */
/* @var $data AnaliseDeCredito */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Cliente_id')); ?>:</b>
	<?php echo CHtml::encode($data->Cliente_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('habilitado')); ?>:</b>
	<?php echo CHtml::encode($data->habilitado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_cadastro')); ?>:</b>
	<?php echo CHtml::encode($data->data_cadastro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numero_do_pedido')); ?>:</b>
	<?php echo CHtml::encode($data->numero_do_pedido); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descricao_do_bem')); ?>:</b>
	<?php echo CHtml::encode($data->descricao_do_bem); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vendedor')); ?>:</b>
	<?php echo CHtml::encode($data->vendedor); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('telefone_loja')); ?>:</b>
	<?php echo CHtml::encode($data->telefone_loja); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('promotor')); ?>:</b>
	<?php echo CHtml::encode($data->promotor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Procedencia_compra_id')); ?>:</b>
	<?php echo CHtml::encode($data->Procedencia_compra_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mercadoria_retirada_na_hora')); ?>:</b>
	<?php echo CHtml::encode($data->mercadoria_retirada_na_hora); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('celular_pre_pago')); ?>:</b>
	<?php echo CHtml::encode($data->celular_pre_pago); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('observacao')); ?>:</b>
	<?php echo CHtml::encode($data->observacao); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alerta')); ?>:</b>
	<?php echo CHtml::encode($data->alerta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('valor')); ?>:</b>
	<?php echo CHtml::encode($data->valor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('entrada')); ?>:</b>
	<?php echo CHtml::encode($data->entrada); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Usuario_id')); ?>:</b>
	<?php echo CHtml::encode($data->Usuario_id); ?>
	<br />

	*/ ?>

</div>