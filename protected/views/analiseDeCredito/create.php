<?php
/* @var $this AnaliseDeCreditoController */
/* @var $model AnaliseDeCredito */

$this->breadcrumbs=array(
	'Analise De Creditos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AnaliseDeCredito', 'url'=>array('index')),
	array('label'=>'Manage AnaliseDeCredito', 'url'=>array('admin')),
);
?>

<h1>Create AnaliseDeCredito</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>