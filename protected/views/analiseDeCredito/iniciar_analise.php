<div class="row">
   <div class="col-sm-12">
      
      <ol class="breadcrumb">
         <li><a href="#">Análises</a></li>
         <li class="active">Criar</li>
      </ol>
      <div class="page-header">
         <h1 id="initial_place">Análise de Crédito</h1>
      </div>
      
   </div>
</div>

<div class="row" style="margin-bottom:50px;">
   
   <!--Formulário inicial-->
   <div class="col-sm-12">
      <form action="#" id="form-informacoes-basicas">
         <div class="row">
            <div class="col-md-12" id="msgsReturn"></div>
         </div>
         <div class="row">
            <div class="col-md-12" id="actions" style="display:none;">
               <div class="form-group" style="display: block;">
                  <div class="col-md-12">
                     <div class="btn-group">
                        <button type="button" class="btn btn-light-grey btn-lg">
                           <i class="fa fa-cogs"></i> Opções
                        </button>
                        <button type="button" class="btn btn-light-grey btn-lg dropdown-toggle" data-toggle="dropdown">
                           <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                           <li><a data-form-bind="form-dados-profissionais" data-form-focus="#" class="bind-form-add" href="#">Cadastrar Dados Profissionais</a></li>
                           <li><a data-form-bind="form-contato" data-form-focus="#" class="bind-form-add" href="#">Cadastrar Telefone</a></li>
                           <li><a data-form-bind="form-endereco" data-form-focus="#" class="bind-form-add" href="#">Cadastrar Endereço</a></li>
                           <li><a data-form-bind="form-referencia" data-form-focus="#" class="bind-form-add" href="#">Cadastrar Referência</a></li>
                           <li><a href="#">Compor Renda com Cônjuge</a></li>
                           <li><a href="#">Anexar Arquivo</a></li>
                           <li><a href="#">Solicitar Alteração no cadastro</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-12">
                     <h4 style="color:#666666;font-family: 'Raleway', sans-serif;font-weight:200">Dados do Cliente</h4>
                     <hr>
                  </div>
               </div>
            </div>
         </div>
         
         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-3">
                     <label class="control-label">CPF : <span class="symbol required"></span></label>
                     <input required name="CPF[numero]" type="text" placeholder="CPF" id="cpf" class="form-control cpf">
                     <input name="Cliente[id]" type="hidden" id="cliente_id" value="0">
                     <input name="ControllerAction" type="hidden" id="ControllerAction" value="">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-5">
                     <label class="control-label">Nome completo : <span class="symbol required"></span></label>
                     <input required name="Pessoa[nome]" type="text" placeholder="Nome completo" id="cliente_nome" class="form-control">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">Sexo : <span class="symbol required"></span></label>
                     <?php echo CHtml::dropDownList('Pessoa[sexo]','Pessoa[sexo]',
                        CHtml::listData(Sexo::model()->findAll(),
                        'sigla','sexo' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:','required'=>true)); 
                     ?>  
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">Nascimento : <span class="symbol required"></span></label>
                     <input required name="Pessoa[nascimento]" type="text" placeholder="Data de nascimento" id="nascimento" class="form-control dateBR">
                  </div>
               </div>
              
            </div>
         </div>
         
         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-3">
                     <label class="control-label">RG : <span class="symbol required"></span></label>
                     <input required name="RG[numero]" type="text" placeholder="RG" id="rg" class="form-control">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-3">
                     <label class="control-label">Órgão Emissor : <span class="symbol required"></span></label>
                     <input required name="RG[orgao_emissor]" type="text" placeholder="Órgão emissor" id="RG_orgao_emissor" class="form-control">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-3">
                     <label class="control-label">UF : <span class="symbol required"></span></label>
                     <?php echo CHtml::dropDownList('RG[uf_emissor]','DocumentoCliente[uf_emissor]',
                        CHtml::listData(Estados::model()->findAll(),
                           'sigla','nome' ), array('class'=>'form-control search-select select2', 'prompt'=>'Selecione:','required'=>true)
                        );
                     ?>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-3">
                     <label class="control-label">Data de Emissão : <span class="symbol required"></span></label>
                     <input required name="RG[data_emissao]" type="text" placeholder="Data de emissão" id="RG_data_emissao" class="form-control dateBR">
                  </div>
               </div>
            </div>
         </div>
         
         <div class="row">
            <div class="col-md-12">
               
               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">Nacionalidade : <span class="symbol required"></span></label>
                     <select required="true" id="select-nacionalidade" name="Pessoa[nacionalidade]" class="draft_sent form-control search-select select2">
                        <option value="">Selecione:</option>
                        <option value="Brasileira">Brasileira</option>
                        <option value="Estrangeira">Estrangeira</option>
                     </select>
                  </div>
               </div>

               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">UF : <span class="symbol required"></span></label>
                     <?php echo CHtml::dropDownList('Pessoa[naturalidade]','Pessoa[naturalidade]',
                           CHtml::listData(Estados::model()->findAll(),
                           'sigla','nome' ), array('class'=>'form-control search-select select2', 'prompt'=>'Selecione:','required'=>true));
                     ?>
                  </div>
               </div>

               <div class="form-group">
                  <div class="col-sm-3">
                     <label class="control-label">Cidade : <span class="symbol required"></span></label>
                     <input required="true" name="Pessoa[naturalidade_cidade]" placeholder="Selecione" type="hidden" id="selectCidades" class="form-control search-select">
                  </div>
               </div>

               <div class="form-group">
                  <div class="col-sm-3">
                     <label class="control-label">Estado Civil : <span class="symbol required"></span></label>
                     <?php echo CHtml::dropDownList('Pessoa[Estado_Civil_id]','Pessoa[Estado_Civil_id]',
                        CHtml::listData(EstadoCivil::model()->findAll(),
                        'id','estado' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:','required'=>true)); 
                     ?>  
                  </div>
               </div>

               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">Cônjuge compõe renda?<span class="symbol required"></span></label>
                     <select required="true" id="Cadastro_conjugue_compoe_renda" name="Cadastro[conjugue_compoe_renda]" class="draft_sent form-control search-select select2">
                        <option value="">Selecione:</option>
                        <?php foreach( TTable::model()->findAll() as $tt ) { ?>
                           <option value="<?php echo $tt->id ?>"><?php echo $tt->flag ?></option>
                        <?php } ?>
                     </select>
                  </div>
               </div>

            </div>
         </div>

         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-5">
                     <label class="control-label">Nome da mãe : <span class="symbol required"></span></label>
                     <input id="Cadastro_nome_da_mae" required name="Cadastro[nome_da_mae]" type="text" placeholder="Nome da mãe" class="form-control">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-5">
                     <label class="">Nome do pai : </label>
                     <input id="Cadastro_nome_do_pai" name="Cadastro[nome_do_pai]" type="text" placeholder="Nome do pai" class="form-control">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">Número de dependentes : <span class="symbol required"></span></label>
                     <input id="Cadastro_numero_de_dependentes" required name="Cadastro[numero_de_dependentes]" type="text" placeholder="Número de dependentes" class="form-control">
                  </div>
               </div>
            </div>
         </div>

         <div class="row">
            <p></p>
         </div>

         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-9">
                     
                  </div>
                  <div class="col-sm-3">
                     <button id="btn-submit-informacoes-basicas" class="btn btn-yellow btn-block" type="submit">
                        Salvar <i class="fa fa-arrow-circle-right"></i>
                     </button>
                  </div>
               </div>
            </div>
         </div>
         
      </form>
   </div>

   <!--Formulário Conjuge-->
   <div class="col-sm-12">
      <form action="#" id="form-conjuge" style="display:none">
         
         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-12">
                     <h4 style="color:#666666;font-family: 'Raleway', sans-serif;font-weight:200">Dados do Cônjuge</h4>
                     <hr>
                  </div>
               </div>
            </div>
         </div>

         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-3">
                     <label class="control-label">CPF : <span class="symbol required"></span></label>
                     <input required name="CPFConjuge[numero]" type="text" placeholder="CPF do Cônjuge" id="cpf_conjuge" class="form-control cpf">
                     <input name="ControllerAction" type="hidden" id="ControllerConjugeAction" value="new">
                     <input name="Conjuge_id" type="hidden" id="Conjuge_id">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-5">
                     <label class="control-label">Nome completo : <span class="symbol required"></span></label>
                     <input required name="Conjuge[nome]" type="text" placeholder="Nome completo Cônjuge" id="conjuge_nome" class="form-control">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">Sexo : <span class="symbol required"></span></label>
                     <?php echo CHtml::dropDownList('Conjuge[sexo]','Conjuge[sexo]',
                        CHtml::listData(Sexo::model()->findAll(),
                        'sigla','sexo' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:','required'=>true)); 
                     ?>  
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">Nascimento : <span class="symbol required"></span></label>
                     <input required name="Conjuge[nascimento]" type="text" placeholder="Data de nascimento" id="conjuge_nascimento" class="form-control dateBR">
                  </div>
               </div>
              
            </div>
         </div>

         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-3">
                     <label class="control-label">RG : <span class="symbol required"></span></label>
                     <input required name="RGConjuge[numero]" type="text" placeholder="RG" id="rg_conjuge" class="form-control">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-3">
                     <label class="control-label">Órgão Emissor : <span class="symbol required"></span></label>
                     <input required name="RGConjuge[orgao_emissor]" type="text" placeholder="Órgão emissor" id="RG_conjuge_orgao_emissor" class="form-control">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-3">
                     <label class="control-label">UF : <span class="symbol required"></span></label>
                     <?php echo CHtml::dropDownList('RGConjuge[uf_emissor]','RGConjuge[uf_emissor]',
                        CHtml::listData(Estados::model()->findAll(),
                           'sigla','nome' ), array('class'=>'form-control search-select select2', 'prompt'=>'Selecione:','required'=>true)
                        );
                     ?>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-3">
                     <label class="control-label">Data de Emissão : <span class="symbol required"></span></label>
                     <input required name="RGConjuge[data_emissao]" type="text" placeholder="Data de emissão" id="RG_conjuge_data_emissao" class="form-control dateBR">
                  </div>
               </div>
            </div>
         </div>
         
         <div class="row">
            <div class="col-md-12">
               
               <div class="form-group">
                  <div class="col-sm-3">
                     <label class="control-label">Nacionalidade : <span class="symbol required"></span></label>
                     <select required="true" id="select-nacionalidade-conjuge" name="Conjuge[nacionalidade]" class="draft_sent form-control search-select select2">
                        <option value="">Selecione:</option>
                        <option value="Brasileira">Brasileira</option>
                        <option value="Estrangeira">Estrangeira</option>
                     </select>
                  </div>
               </div>

               <div class="form-group">
                  <div class="col-sm-3">
                     <label class="control-label">UF : <span class="symbol required"></span></label>
                     <?php echo CHtml::dropDownList('Conjuge[naturalidade]','Conjuge[naturalidade]',
                           CHtml::listData(Estados::model()->findAll(),
                           'sigla','nome' ), array('class'=>'form-control search-select select2', 'prompt'=>'Selecione:','required'=>true));
                     ?>
                  </div>
               </div>

               <div class="form-group">
                  <div class="col-sm-3">
                     <label class="control-label">Cidade : <span class="symbol required"></span></label>
                     <input required="true" name="Conjuge[naturalidade_cidade]" placeholder="Selecione" type="hidden" id="selectCidades2" class="form-control search-select">
                  </div>
               </div>

               <div class="form-group">
                  <div class="col-sm-3">
                     <label class="control-label">Estado Civil : <span class="symbol required"></span></label>
                     <?php echo CHtml::dropDownList('Conjuge[Estado_Civil_id]','Conjuge[Estado_Civil_id]',
                        CHtml::listData(EstadoCivil::model()->findAll(),
                        'id','estado' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:','required'=>true)); 
                     ?>  
                  </div>
               </div>
            
            </div>
         </div>

          <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-12">
                     <p></p>
                  </div>
               </div>
            </div>
         </div>

         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-12">
                     <h4 style="color:#666666;font-family: 'Raleway', sans-serif;font-weight:200">Dados Profissionais do Cônjuge</h4>
                     <hr>
                  </div>
               </div>
            </div>
         </div>

         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">CNPJ/CPF: <span class="symbol required"></span></label>
                     <input placeholder="CPF / CNPJ" id="dp_conjuge_cnpj_cpf" class="form-control" type="text" name="DadosProfissionaisConjuge[cnpj_cpf]">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-3">
                     <label class="control-label">Classe Profissional : <span class="symbol required"></span></label>
                     <?php echo CHtml::dropDownList('DadosProfissionaisConjuge[Classe_Profissional_id]','DadosProfissionaisConjuge[Classe_Profissional_id]',
                        CHtml::listData(ClasseProfissional::model()->findAll(),
                        'id','classe' ), array('class'=>'form-control search-select select2', 'prompt'=>'Selecione:', 'required'=>true)); 
                     ?>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-3">
                     <label class="control-label">Empresa</label>
                     <input placeholder="Empresa" id="dp_conj_empresa" class="form-control" type="text" name="DadosProfissionaisConjuge[empresa]">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">Ocupação atual</label>
                     <?php echo CHtml::dropDownList('DadosProfissionaisConjuge[Tipo_Ocupacao_id]','DadosProfissionaisConjuge[Tipo_Ocupacao_id]',
                        CHtml::listData(TipoOcupacao::model()->findAll(),
                        'id','tipo' ), array('class'=>'form-control search-select select2', 'prompt'=>'Selecione:', 'required'=>true)); 
                     ?>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">Data de admissão</label>
                     <input placeholder="Data de Admissão" required="required" id="dp_con_data_admissao" class="form-control dateBR" type="text" name="DadosProfissionaisConjuge[data_admissao]">
                  </div>
               </div>
            </div>
         </div>

         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-3">
                     <label class="control-label">Profissão</label>
                     <?php echo CHtml::dropDownList('DadosProfissionaisConjuge[profissao]','DadosProfissionaisConjuge[profissao]',
                        CHtml::listData(Profissao::model()->findAll(),
                        'profissao','profissao' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:')); 
                     ?> 
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">Renda Líquida R$</label>
                     <input id="dp_conj_renda_liquida" required class="form-control dinheiro currency" type="text" name="DadosProfissionaisConjuge[renda_liquida]">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">Mês / Ano Renda</label>
                     <input id="dp_conj_mes_ano_renda" class="form-control date-picker-mes-ano" type="text" name="DadosProfissionaisConjuge[mes_ano_renda]">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">Órgão Pagador</label>
                     <?php echo CHtml::dropDownList('DadosProfissionaisConjuge[orgao_pagador]','DadosProfissionaisConjuge[orgao_pagador]',
                        CHtml::listData(OrgaoPagador::model()->findAll(),
                        'nome','nome' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:')); 
                     ?>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-3">
                     <label class="control-label">N° do benefício</label>
                     <input id="dp_conj_numero_do_beneficio" class="form-control" type="text" name="DadosProfissionaisConjuge[numero_do_beneficio]">
                  </div>
               </div>
            </div>
         </div>

         <div class="row">
             <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-3">
                     <label class="control-label">Tipo de comprovante</label>
                     <?php echo CHtml::dropDownList('DadosProfissionaisConjuge[TipoDeComprovante]','DadosProfissionaisConjuge[TipoDeComprovante]',
                        CHtml::listData(TipoDeComprovante::model()->findAll(),
                        'tipo','tipo' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:')); 
                     ?>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">CEP</label>
                     <input required="required" id="cep_dados_profissionais_conjuge" class="form-control cep" type="text" name="EnderecoProfissConjuge[cep]">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-5">
                     <label class="control-label">Logradouro</label>
                     <input required="required" id="logradouro_dados_profissionais_conjuge" class="form-control" type="text" name="EnderecoProfissConjuge[logradouro]">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">Número</label>
                     <input required="required" id="numero_endereco_dados_profissionais_conjuge"  class="form-control" type="text" name="EnderecoProfissConjuge[numero]">
                  </div>
               </div>
            </div>
         </div>

         <div class="row">
             <div class="col-md-12">
               <div class="form-group">
                  <div class="col-md-3">
                     <label class="control-label">Cidade</label>
                     <input required="required" id="cidade_dados_profissionais_conjuge" class="form-control" type="text" name="EnderecoProfissConjuge[cidade]">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-3">
                     <label class="control-label">Bairro</label>
                     <input required="required" id="bairro_dados_profissionais_conjuge" class="form-control" type="text" name="EnderecoProfissConjuge[bairro]">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-3">
                     <label class="control-label">UF</label>
                     <?php echo CHtml::dropDownList('EnderecoProfissConjuge[uf]','EnderecoProfissConjuge[uf]',
                        CHtml::listData(Estados::model()->findAll(),
                        'sigla','nome' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:', 'required' => true)
                     );?>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-3">
                     <label class="control-label">Complemento</label>
                     <input id="dp_conj_endereco_complemento" class="form-control" type="text" name="EnderecoProfissConjuge[complemento]">
                  </div>
               </div>
             </div>
         </div>

         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-md-4">
                     <label class="control-label">Telefone</label>
                     <input id="dp_conj_telefone_numero" class="form-control telefone" type="text" name="TelefoneProfissConjuge[numero]" required="required">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-4">
                     <label class="control-label">Ramal</label>
                     <input id="dp_conj_telefone_ramal" class="form-control" type="text" name="TelefoneProfissConjuge[ramal]">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-4">
                     <label class="control-label">Tipo de telefone</label>
                     <?php echo CHtml::dropDownList('TelefoneProfissConjuge[Tipo_Telefone_id]','TelefoneProfissConjuge[Tipo_Telefone_id]',
                        CHtml::listData(TipoTelefone::model()->findAll(),
                        'id','tipo' ), array('class'=>'form-control search-select select2', 'prompt'=>'Selecione:', 'required' => true));
                     ?>
                  </div>
               </div>
            </div>
         </div>

         <div class="row">
            <p></p>
         </div>
         
         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-9">
                     
                  </div>
                  <div class="col-sm-3">
                     <button disabled="disabled" class="btn btn-yellow btn-block" type="submit" id="btn-submit-dados-conjuge">
                        Salvar <i class="fa fa-arrow-circle-right"></i>
                     </button>
                  </div>
               </div>
            </div>
         </div>

      </form>
   </div>

   <!--Formulário dados profissionais-->
   <div class="col-md-12">
      <form style="display:none" action="#" id="form-dados-profissionais">
         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-12">
                     <h4 style="color:#666666;font-family: 'Raleway', sans-serif;font-weight:200">Dados Profissionais do Cliente</h4>
                     <hr>
                  </div>
               </div>
            </div>
         </div>

         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">CNPJ/CPF:</label>
                     <input placeholder="CPF / CNPJ" id="dp_cnpj_cpf" class="form-control" type="text" name="DadosProfissionais[cnpj_cpf]">
                     <input name="ControllerAction" type="hidden" id="ControllerDPAction" value="new">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-3">
                     <label class="control-label">Classe Profissional : <span class="symbol required"></span></label>
                     <?php echo CHtml::dropDownList('DadosProfissionais[Classe_Profissional_id]','DadosProfissionais[Classe_Profissional_id]',
                        CHtml::listData(ClasseProfissional::model()->findAll(),
                        'id','classe' ), array('class'=>'form-control search-select select2', 'prompt'=>'Selecione:', 'required'=>true)); 
                     ?>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-3">
                     <label class="control-label">Empresa</label>
                     <input placeholder="Empresa" id="dp_empresa" class="form-control" type="text" name="DadosProfissionais[empresa]">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">Ocupação atual</label>
                     <?php echo CHtml::dropDownList('DadosProfissionais[Tipo_Ocupacao_id]','DadosProfissionais[Tipo_Ocupacao_id]',
                        CHtml::listData(TipoOcupacao::model()->findAll(),
                        'id','tipo' ), array('class'=>'form-control search-select select2', 'prompt'=>'Selecione:', 'required'=>true)); 
                     ?>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">Data de admissão</label>
                     <input placeholder="Data de Admissão" required="required" id="dp_data_admissao" class="form-control dateBR" type="text" name="DadosProfissionais[data_admissao]">
                  </div>
               </div>
            </div>
         </div>

         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-3">
                     <label class="control-label">Profissão</label>
                     <?php echo CHtml::dropDownList('DadosProfissionais[profissao]','DadosProfissionais[profissao]',
                        CHtml::listData(Profissao::model()->findAll(),
                        'profissao','profissao' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:')); 
                     ?> 
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">Renda Líquida R$</label>
                     <input id="dp_renda_liquida" required class="form-control dinheiro currency" type="text" name="DadosProfissionais[renda_liquida]">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">Mês / Ano Renda</label>
                     <input id="dp_mes_ano_renda" class="form-control mes_ano_renda" type="text" name="DadosProfissionais[mes_ano_renda]">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">Órgão Pagador</label>
                     <?php echo CHtml::dropDownList('DadosProfissionais[orgao_pagador]','DadosProfissionais[orgao_pagador]',
                        CHtml::listData(OrgaoPagador::model()->findAll(),
                        'nome','nome' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:')); 
                     ?>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-3">
                     <label class="control-label">N° do benefício</label>
                     <input id="dp_numero_do_beneficio" class="form-control" type="text" name="DadosProfissionais[numero_do_beneficio]">
                  </div>
               </div>
            </div>
         </div>

         <div class="row">
             <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-3">
                     <label class="control-label">Tipo de comprovante</label>
                     <?php echo CHtml::dropDownList('DadosProfissionais[TipoDeComprovante]','DadosProfissionais[TipoDeComprovante]',
                        CHtml::listData(TipoDeComprovante::model()->findAll(),
                        'tipo','tipo' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:')); 
                     ?>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">CEP</label>
                     <input required="required" id="cep_dados_profissionais" class="form-control cep" type="text" name="EnderecoProfiss[cep]">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-5">
                     <label class="control-label">Logradouro</label>
                     <input required="required" id="logradouro_dados_profissionais" class="form-control" type="text" name="EnderecoProfiss[logradouro]">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">Número</label>
                     <input required="required" id="numero_endereco_dados_profissionais"  class="form-control" type="text" name="EnderecoProfiss[numero]">
                  </div>
               </div>
            </div>
         </div>

         <div class="row">
             <div class="col-md-12">
               <div class="form-group">
                  <div class="col-md-3">
                     <label class="control-label">Cidade</label>
                     <input required="required" id="cidade_dados_profissionais" class="form-control" type="text" name="EnderecoProfiss[cidade]">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-3">
                     <label class="control-label">Bairro</label>
                     <input required="required" id="bairro_dados_profissionais" class="form-control" type="text" name="EnderecoProfiss[bairro]">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-3">
                     <label class="control-label">UF</label>
                     <?php echo CHtml::dropDownList('EnderecoProfiss[uf]','EnderecoProfiss[uf]',
                        CHtml::listData(Estados::model()->findAll(),
                        'sigla','nome' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:', 'required' => true)
                     );?>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-3">
                     <label class="control-label">Complemento</label>
                     <input id="dp_endereco_complemento" class="form-control" type="text" name="EnderecoProfiss[complemento]">
                  </div>
               </div>
             </div>
         </div>

         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-md-3">
                     <label class="control-label">Telefone</label>
                     <input id="dp_telefone_numero" class="form-control telefone" type="text" name="TelefoneProfiss[numero]" required="required">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-2">
                     <label class="control-label">Ramal</label>
                     <input id="dp_telefone_ramal" class="form-control" type="text" name="TelefoneProfiss[ramal]">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-md-3">
                     <label class="control-label">Tipo de telefone</label>
                     <?php echo CHtml::dropDownList('TelefoneProfiss[Tipo_Telefone_id]','TelefoneProfiss[Tipo_Telefone_id]',
                        CHtml::listData(TipoTelefone::model()->findAll(),
                        'id','tipo' ), array('class'=>'form-control search-select select2', 'prompt'=>'Selecione:', 'required' => true));
                     ?>
                  </div>
               </div>
            </div>
         </div>

         <div class="row">
            <p></p>
         </div>

         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-9">
                     
                  </div>
                  <div class="col-sm-3">
                     <button class="btn btn-yellow btn-block" type="submit">
                        Salvar <i class="fa fa-arrow-circle-right"></i>
                     </button>
                  </div>
               </div>
            </div>
         </div>
      </form>
   </div>

   <!--Formulário de endereço-->
   <div class="col-md-12">
      <form style="display:none" action="#" id="form-endereco">
         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-12">
                     <h4 style="color:#666666;font-family: 'Raleway', sans-serif;font-weight:200">Endereço</h4>
                     <hr>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-2">
                     <div class="form-group">
                        <label class="control-label">CEP <span class="symbol required"></span></label>
                        <input required class="form-control cep" type="text" name="EnderecoCliente[cep]" id="cep_cliente">
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-3">
                     <div class="form-group">
                        <label class="control-label">Logradouro <span class="symbol required"></span></label>
                        <input required class="form-control" type="text" name="EnderecoCliente[logradouro]" id="logradouro_cliente">
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-3">
                     <div class="form-group">
                        <label class="control-label">Cidade <span class="symbol required"></span></label>
                        <input required class="form-control" type="text" name="EnderecoCliente[cidade]" id="cidade_cliente">
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-2">
                     <div class="form-group">
                        <label class="control-label">Bairro <span class="symbol required"></span></label>
                        <input required class="form-control" type="text" name="EnderecoCliente[bairro]" id="bairro_cliente">
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-2">
                     <div class="form-group">
                        <label class="control-label">Número <span class="symbol required"></span></label>
                        <input required id="numero_end_cliente" class="form-control" type="text" name="EnderecoCliente[numero]">
                     </div>
                  </div>
               </div>
            </div>
         </div>

         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-3">
                     <div class="form-group">
                        <label class="control-label">Tipo de moradia <span class="symbol required"></span></label>
                        <select required id="EnderecoCliente_Tipo_Moradia_id" name="EnderecoCliente[Tipo_Moradia_id]" class="form-control search-select select2">
                           <option value="">Selecione:</option>
                           <?php  foreach( TipoMoradia::model()->findAll() as $tipoMoradia ){ ?>
                              <option value="<?php echo $tipoMoradia->id ?>"><?php echo $tipoMoradia->tipo ?></option>
                           <?php } ?>
                        </select>
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-3">
                     <div class="form-group">
                        <label class="control-label">Complemento</label>
                        <input id="complemento_end_cliente" class="form-control" type="text" name="EnderecoCliente[complemento]">
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-3">
                     <label class="control-label">UF <span class="symbol required"></span></label>
                     <select required id="EnderecoCliente_uf" name="EnderecoCliente[uf]" class="form-control search-select select2">
                        <option value="">Selecione:</option>
                        <?php foreach( Estados::model()->findAll() as $uf ) { ?>
                           <option value="<?php echo $uf->sigla ?>"><?php echo $uf->sigla ?></option>
                        <?php } ?>
                     </select>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-3">
                     <label class="control-label">Tipo de endereço <span class="symbol required"></span></label>
                     <select required id="EnderecoCliente_Tipo_Endereco_id" name="EnderecoCliente[Tipo_Endereco_id]" class="form-control search-select select2">
                        <option value="">Selecione:</option>
                        <?php  foreach( TipoEndereco::model()->findAll() as $TipoEndereco ){ ?>
                           <option value="<?php echo $TipoEndereco->id ?>"><?php echo $TipoEndereco->tipo ?></option>
                        <?php } ?>
                     </select>
                  </div>
               </div>
            </div>
         </div>

         <div class="row">
            <p></p>
         </div>
         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-9">
                     
                  </div>
                  <div class="col-sm-3">
                     <button class="btn btn-yellow btn-block" type="submit">
                        Salvar <i class="fa fa-arrow-circle-right"></i>
                     </button>
                  </div>
               </div>
            </div>
         </div>

      </form>
   </div>

   <!--Formulário de contato-->
   <div class="col-md-12">
      <form style="display:none" action="#" id="form-contato">
         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-12">
                     <strong><h4 style="color:#666666;font-family: 'Raleway', sans-serif;font-weight:200">Contato</h4></strong>
                     <hr>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-4">
                     <label class="control-label">N° do telefone<span class="symbol required"></span></label>
                     <input required class="form-control telefone" type="text" name="TelefoneCliente[numero]" id="telefone_numero">
                     <input name="ControllerAction" type="hidden" id="ControllerTelefoneAction" value="new">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-4">
                     <label class="control-label">Tipo de telefone</label>
                     <?php echo CHtml::dropDownList('TelefoneCliente[Tipo_Telefone_id]','TelefoneCliente[Tipo_Telefone_id]',
                        CHtml::listData(TipoTelefone::model()->findAll(),
                        'id','tipo' ), array('class'=>'form-control search-select select2','prompt' => 'Selecione:','required' => true )); 
                     ?>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-4">
                     <label class="control-label">Ramal </label>
                     <input class="form-control" type="text" name="TelefoneCliente[ramal]" id="">
                  </div>
               </div>
            </div>
         </div>
           <div class="row">
            <p></p>
         </div>
         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-9">
                     
                  </div>
                  <div class="col-sm-3">
                     <button class="btn btn-yellow btn-block" type="submit">
                        Salvar <i class="fa fa-arrow-circle-right"></i>
                     </button>
                  </div>
               </div>
            </div>
         </div>
      </form>
   </div>

   <!--Formulário de referencias-->
   <div class="col-md-12">
      <form style="display:none" action="#" id="form-referencia">
         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-12">
                     <strong><h4 style="color:#666666;font-family: 'Raleway', sans-serif;font-weight:200">Referência</h4></strong>
                     <hr>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">Parentesco <span class="symbol required"></span></label>
                     <input id="referencia_parentesco" required class="form-control" type="text" name="Referencia[parentesco]">
                     <input name="ControllerAction" type="hidden" id="ControllerReferenciaAction" value="new">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-4">
                     <label class="control-label">Nome <span class="symbol required"></span></label>
                     <input id="referencia_nome" required class="form-control" type="text" name="Referencia[nome]">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-4">
                     <label class="control-label">N° do telefone <span class="symbol required"></span></label>
                     <input id="telefone_referencia_numero" required class="telefone form-control" type="text" name="TelefoneReferencia[numero]">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">Tipo <span class="symbol required"></span></label>
                     <?php echo CHtml::dropDownList('Referencia[Tipo_Referencia_id]','Referencia[Tipo_Referencia_id]',
                        CHtml::listData(TipoReferencia::model()->findAll(),'id','tipo'),
                        array('class'=>'form-control search-select select2','prompt'=>'Selecione:','required'=>true));
                     ?>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <p></p>
         </div>
         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-9">
                     
                  </div>
                  <div class="col-sm-3">
                     <button class="btn btn-yellow btn-block" type="submit">
                        Salvar <i class="fa fa-arrow-circle-right"></i>
                     </button>
                  </div>
               </div>
            </div>
         </div>
      </form>
   </div>

   <!--Formulário da proposta-->
   <div class="col-md-12">
      <form style="display:block" action="#" id="form-proposta">
         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-12">
                     <strong><h4 style="color:#666666;font-family: 'Raleway', sans-serif;font-weight:200">Detalhes da proposta</h4></strong>
                     <hr>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">Valor da compra <span class="symbol required"></span></label>
                     <input id="" required class="form-control dinheiro currency2 triggerSimularInput" type="text" name="Proposta[valor]">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">Valor de entrada <span class="symbol required"></span></label>
                     <input id="" required class="form-control dinheiro currency triggerSimularInput" type="text" name="Proposta[entrada]">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-4">
                     <label class="control-label">Tabela <span class="symbol required"></span></label>
                     <?php echo CHtml::dropDownList('Proposta[Tabela_id]','Proposta[Tabela_id]', CHtml::listData( TabelaCotacao::model()->findAll( Cotacao::model()->filialCotacoes( Yii::app()->session['usuario']->returnFilial() ) ),
                        'id','descricao' ), array( 'class'=>'form-control search-select select2', 'prompt'=>'Selecione:','required'=>true,'id'=>'Cotacao_id' ));
                     ?>
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">Carência <span class="symbol required"></span></label>
                     <!--<input id="" required class="form-control" type="text" name="Proposta[carencia]">-->
                     <input required="true" name="Proposta[carencia]" placeholder="Selecione" type="hidden" id="selectCarencias" class="form-control search-select triggerSimularSelect">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">Qtd. Parcelas <span class="symbol required"></span></label>
                     <input required="true" name="Proposta[qtd_parcelas]" placeholder="Selecione" type="hidden" id="selectQtdParcelas" class="form-control search-select triggerSimularSelect">
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">               
               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">Valor Financiado </span></label>
                     <input style="border:none" readonly id="valor_financiado"  class="form-control" type="text" name="valor_final">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-3">
                     <label class="control-label">Valor Final </span></label>
                     <input style="border:none" readonly id="valor_final"  class="form-control" type="text" name="valor_final">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-3">
                     <label class="control-label">Parcelamento </label>
                     <input style="border:none" readonly id="parcelamento"  class="form-control" type="text" name="condicoes_parcelamento">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">Primeira Parcela</label>
                     <input style="border:none" readonly id="data_primeira_parcela"  class="form-control" type="text" name="data_primeira_parcela">
                  </div>
               </div>
               <div class="form-group">
                  <div class="col-sm-2">
                     <label class="control-label">Última Parcela </label>
                     <input style="border:none" readonly id="data_ultima_parcela"  class="form-control" type="text" name="data_ultima_parcela">
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <p></p>
         </div>
         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <div class="col-sm-9">
                     
                  </div>
                  <div class="col-sm-3">
                     <button class="btn btn-yellow btn-block" type="submit">
                        Salvar <i class="fa fa-arrow-circle-right"></i>
                     </button>
                  </div>
               </div>
            </div>
         </div>
      </form>
   </div>

</div>

<style type="text/css">
   .btn-show-form{
      cursor: pointer;
   }   
</style>
<!--06984486424-->
<!--08267742450-->
<!--08663332408-->
<!--59076305-->