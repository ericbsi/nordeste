<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
		<div class="panel-heading">
					<i class="fa fa-external-link-square"></i>
						
					<div class="panel-tools">
					<a class="btn btn-xs btn-link panel-collapse collapses" href="#">
					</a>
					<a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
						<i class="fa fa-wrench"></i>
					</a>
					<a class="btn btn-xs btn-link panel-refresh" href="#">
						<i class="fa fa-refresh"></i>
					</a>
					<a class="btn btn-xs btn-link panel-expand" href="#">
						<i class="fa fa-resize-full"></i>
					</a>
					<a class="btn btn-xs btn-link panel-close" href="#">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
	<?php 

	$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'proposta-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width dataTable',
	'columns'=>array(
		'valor_entrada',
		'qtd_parcelas',
		'valor_parcela',
		'carencia',
		'valor',
		'valor_final',
		array(
			'class'=>'CButtonColumn',
			'template'=>''
		),
	),
));
	
	?>
		</div>
	</div>
</div>
