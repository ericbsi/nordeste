<script type="text/javascript">
   
   function resizeIframe( obj ){

      var isFirefox = typeof InstallTrigger !== 'undefined';
      var plusFf = 0;

      if (isFirefox) {plusFf = 100};

      {
         obj.style.height = 0;
      };
   
      {
         obj.style.height = plusFf + obj.contentWindow.document.body.scrollHeight + 'px';
      }
   }

</script>

<div class="row">
   <div class="col-sm-12">
      
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
            Crediarista
            </a>
         </li>
         <li class="active">
            Histórico de análises
         </li>
      </ol>
      <div class="page-header">
         <h1>Histórico de análises /<small>Crediarista: <?php echo Yii::app()->session['usuario']->nome_utilizador ?> </small></h1>
      </div>

   </div>

</div>

<div class="row">
   <div class="col-md-12">
      <!-- start: DYNAMIC TABLE PANEL -->
      <div class="panel panel-default">
         <div class="panel-heading">
            <i class="fa fa-external-link-square"></i>
            Histórico de análises
            <div class="panel-tools">
               <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
               </a>
            </div>
         </div>
         <div class="panel-body">
            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
               <thead>
                  <tr>
                     <th class="hidden-xs">Código</th>
                     <th>Cliente</th>
                     <th>Data</th>
                     <th>Valor R$</th>
                     <th>Entrada R$</th>
                     <th>Valor financiado R$</th>
                     <th></th>
                     <th></th>
                  </tr>
               </thead>
               <tbody>  

                  <?php foreach($analises as $analise) { ?>
                     <tr>
                        <td><?php echo $analise->codigo ?></td>
                        <td><?php echo $analise->cliente->pessoa->nome ?></td>
                        <td><?php echo $analise->data_cadastro_br ?></td>
                        <td><?php echo number_format($analise->valor, 2, ',','.') ?></td>
                        <td><?php echo number_format($analise->entrada, 2, ',','.') ?></td>
                        <td><?php echo number_format(($analise->valor-$analise->entrada), 2, ',','.') ?></td>
                        <td class="center">
                           <div class="visible-md visible-lg hidden-sm hidden-xs">
                               <a modal-iframe-uri="<?php echo Yii::app()->request->baseUrl; ?>/analiseDeCredito/maisDetalhes?id=<?php echo $analise->id; ?>" href="#responsive" data-toggle="modal" class="btn btn-xs btn-teal tooltips btn-mais-detalhes" data-placement="top" data-original-title="Mais detalhes"><i class="fa fa-edit"></i></a>
                           </div>
                        </td>
                        <td class="center">
                           <div class="visible-md visible-lg hidden-sm hidden-xs">
                              <a modal-iframe-uri="<?php echo Yii::app()->request->baseUrl; ?>/analiseDeCredito/verPropostas?id=<?php echo $analise->id; ?>" href="#responsive" data-toggle="modal" class="btn btn-xs btn-green tooltips btn-ver-propostas" data-placement="top" data-original-title="Ver propostas"><i class="clip-zoom-in"></i></a>
                           </div>
                        </td>
                     </tr>
                  <?php } ?>
               </tbody>
            </table>
         </div>
      </div>
      <!-- end: DYNAMIC TABLE PANEL -->
   </div>
</div>

<div id="responsive" class="modal fade" tabindex="-1" data-width="1100" style="display: none;">
      
   <div class="modal-body">
      <iframe class="overflownone" style="backgroun:#FFF!important" width="100%" src="" scrolling="no" onload='javascript:resizeIframe(this);' id="iframe_mais_detalhes_analise" frameborder="0"></iframe>
   </div>

   <div class="modal-footer">
      <button type="button" data-dismiss="modal" class="btn btn-light-grey">Fechar</button>
   </div>

</div>