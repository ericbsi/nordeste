<?php  ?>
<div class="row">
   <div class="col-md-12">
      <div class="alert alert-info">
         <div class="row">
            <div class="col-sm-4">
               <div class="core-box">
                  <div class="heading">
                     <i class="clip-user-6 circle-icon circle-green"></i>
                     <h2>Informações do cliente</h2>
                  </div>
                  <div class="content">
                     <table class="table table-condensed table-hover" >
                        <thead>
                        </thead>
                        <tbody>
                           <tr>
                              <td>Nome: </td>
                              <td>
                                 <?php echo $pessoa->nome ?>
                              </td>
                           </tr>
                           <tr>
                              <td>CPF: </td>
                              <td>                                       
                                 <?php echo $cpf->numero ?>
                              </td>
                           </tr>
                           <tr>
                              <td>Situação cadastral do CPF: </td>
                              <td>                                       
                                 <?php echo $situacao_cadastral ?>
                              </td>
                           </tr>
                           <tr>
                              <td>Renda Líquida: </td>
                              <td>                                       
                                 <?php echo "R$ " . number_format($renda_liquida,2,",","."); ?>
                              </td>
                           </tr>
                           <tr>
                              <td>Cadastrado: </td>
                              <td>
                                 <?php echo $isClient ?>                                          
                              </td>
                           </tr>

                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <div class="col-sm-4">
               <div class="core-box">
                  <div class="heading">
                     <i class=" clip-file-2 circle-icon circle-green"></i>
                     <h2>Informações da solicitação</h2>
                  </div>
                  <div class="content">
                     <table class="table table-condensed table-hover">
                        <thead>
                        </thead>
                        <tbody>
                           <tr>
                              <td>Valor solicitado: </td>
                              <td>
                                 <?php echo "R$ " . number_format( $valor_soli, 2, ",", "." ) ?>         
                              </td>
                           </tr>
                           <?php if ($valor_entrada > 0 || $valor_entrada != null){ ?>
                           <tr>
                              <td>Entrada: </td>
                              <td>
                                 <?php echo "R$ " . number_format($valor_entrada, 2, ",", ".") ?>
                              </td>
                           </tr>
                        <?php } ?>
                           <tr>
                              <td>
                                 <a data-original-title="30% do valor da renda líquida" class="tooltips" href="#">
                                    <i class="clip-question"></i>
                                 </a>
                                 Parcela máxima sugerida: <?php echo "R$ " . number_format($parcela_sugerida, 2, ",", "."); ?>
                              </td>
                              <td>                                       
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <div class="col-sm-4" id="resumo-solicitacao" style="display:none">
               <div class="core-box">
                  <div class="heading">
                     <i class=" circle-icon circle-green fa fa-list"></i>
                     <h2>Resumo</h2>
                  </div>                  
                  <div class="content">
                     <table class="table table-condensed table-hover">
                        <thead>
                        </thead>
                        <tbody>
                           <tr>
                              <td>Valor financiado: </td>
                              <td id="td_val_financiado">
                              </td>
                           </tr>
                           <tr>
                              <td>Número de parcelas: </td>
                              <td id="td_num_parcelas">
                              </td>
                           </tr>
                           <tr>
                              <td>Valor da parcela: </td>
                              <td id="td_val_parcelas">
                              </td>
                           </tr>
                           <tr>
                              <td>Taxa (%): </td>
                              <td id="td_cotacao_taxa">
                              </td>
                           </tr>
                           <tr>
                              <td>Valor total: </td>
                              <td id="td_val_total">
                              </td>
                           </tr>
                           <tr>
                              <td>Carência: </td>
                              <td id="td_carencia">
                              </td>
                           </tr>
                           <tr>
                              <td>Data da 1° parcela: </td>
                              <td id="td_pri_parcela">
                              </td>
                           </tr>
                           <tr>
                              <td>Data da última parcela: </td>
                              <td id="td_ultima_parcela">
                              </td>
                           </tr>
                           <tr>
                              <td>Cotação: </td>
                              <td id="td_cotacao">
                              </td>
                           </tr>
                        </tbody>
                     </table>

                     <form method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/analiseDeCredito/analiseResume">

                        <!--Informações do cliente-->
                        <input type="hidden" value="<?php echo $pessoa->nome ?>" name="pessoa_nome">
                        <input type="hidden" value="<?php echo $cpf->numero ?>" name="cpf">
                        <input type="hidden" value="<?php echo $renda_liquida ?>" name="renda_liquida">
                        <input type="hidden" value="<?php echo $isClient ?>" name="isClient">
                        <input type="hidden" value="<?php echo $situacao_cadastral ?>" name="situacao_cadastral">

                        <!--Informações da solicitação-->
                        <input type="hidden" value="<?php echo $valor_soli ?>" name="valor_compra"/>
                        <input type="hidden" value="<?php echo $valor_entrada ?>" name="valor_entrada"/>

                        <!--Resumo-->
                        <input type="hidden" value="" name="num_parcelas_hidden" id="num_parcelas_hidden">
                        <input type="hidden" value="" name="val_parcelas_hidden" id="val_parcelas_hidden">
                        <input type="hidden" value="" name="val_total_hidden" id="val_total_hidden">
                        <input type="hidden" value="" name="data_pri_par_hidden" id="data_pri_par_hidden">
                        <input type="hidden" value="" name="data_ult_par_hidden" id="data_ult_par_hidden">
                        <input type="hidden" value="" name="cotacao_id_hidden" id="cotacao_id_hidden">
                        <input type="hidden" value="" name="carencia_hidden" id="carencia_hidden">
                        <input type="hidden" value="" name="valor_financiado_hidden" id="valor_financiado_hidden">
                        
                        <button style="position:absolute; right:15px; bottom:0" class="btn btn-info ladda-button">
                              <span class="ladda-label"> Prosseguir </span>
                                 <i class="fa fa-arrow-circle-right"></i>
                                 <span class="ladda-spinner"></span>
                              <span class="ladda-spinner"></span>
                        </button>
                     </form>

                  </div>
               </div>

            </div>
         </div>
      </div>      
      <div class="row">
         <div class="col-md-12">
            <div class="panel panel-default">
               <div class="panel-heading">
                  <i class="fa fa-external-link-square"></i>
                  Cotações
                  <div class="panel-tools">
                     <a href="#" class="btn btn-xs btn-link panel-collapse collapses">
                     </a>
                  </div>
               </div>
               <div class="panel-body">
                  <form method="post" action="#" id="empresaForm" role="form" class="form-horizontal">
                     <div class="row">
                        <input type="hidden" value="<?php echo $valor_soli ?>" name="valor_solicitado" id="valor_solicitado">
                        <input type="hidden" value="<?php echo $valor_entrada ?>" name="valor_entrada" id="valor_entrada">
                        <label for="s2id_autogen1" class="col-sm-2 control-label required">Selecione uma cotação:</label>
                        <div class="col-sm-3">
                           <?php echo CHtml::dropDownList('cotacoes','id', CHtml::listData( Cotacao::model()->findAll( Cotacao::model()->findByFilial( Yii::app()->session['usuario']->returnFilial() ) ),
                                 'id','descricao' ), array( 'class'=>'form-control search-select', 'id'=>'Cotacao_id' ));
                              ?>
                        </div>
                        <div class="row">
                           <div class="col-md-4">
                              <button id="btn-load-parcelas" data-style="zoom-out" class="btn btn-info ladda-button">
                              <span class="ladda-label"> Simular </span>
                              <span class="ladda-spinner"></span>
                              <span class="ladda-spinner"></span>
                              </button>
                           </div>
                        </div>
                        <input type="hidden" value="<?php echo $valor_soli?>" name="valor_soli" id="valor_soli">
                  </form>
                  </div>
               </div>
            </div>
         </div>
         <div id="parcelas-wrapper" class="col-md-12">
         </div>
      </div>
   </div>
</div>