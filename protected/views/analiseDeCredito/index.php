<?php
/* @var $this AnaliseDeCreditoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Analise De Creditos',
);

$this->menu=array(
	array('label'=>'Create AnaliseDeCredito', 'url'=>array('create')),
	array('label'=>'Manage AnaliseDeCredito', 'url'=>array('admin')),
);
?>

<h1>Analise De Creditos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
