<div class="row">
	<div class="col-md-12">

		<div class="row">
                  <div class="col-sm-12">                   
                     <!-- start: PAGE TITLE & BREADCRUMB -->
                     <ol class="breadcrumb">
                        <li>
                           <i class="clip-pencil"></i>
                           <a href="#">
                              Análises
                           </a>
                        </li>
                        <li class="active">
                              Minhas análises - Pendentes
                        </li>
                        
                     </ol>

                     <div class="page-header">
                        <h1>Propostas</h1>
                     </div>
                     <!-- end: PAGE TITLE & BREADCRUMB -->
                  </div>
         </div>

		<div class="panel panel-default">
		<div class="panel-heading">
					<i class="fa fa-external-link-square"></i>
					<div class="panel-tools">
					<a class="btn btn-xs btn-link panel-collapse collapses" href="#">
					</a>
				</div>
			</div>

			<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'analise-de-credito-grid',
				'dataProvider'=>$model->search( Yii::app()->session['usuario']->id ),
				'itemsCssClass' => 'table table-bordered responsive dataTable',
				'summaryText'=>'Exibindo {start} de {end} resultados',
				'filter'=>$model,
				'columns' => array(
					'numero_do_pedido',
					'data_cadastro_br',
					'valor',
					'alerta',
					'Usuario_id'
					/*
					'vendedor',
					'telefone_loja',
					'promotor',
					'Procedencia_compra_id',
					'mercadoria_retirada_na_hora',
					'celular_pre_pago',
					'observacao',
					'alerta',
					'valor',
					'entrada',
					'Usuario_id',
					*/
				),
			)); ?>
		</div>
	</div>	
</div>