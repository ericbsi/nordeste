<?php  ?>
<div class="row">
   <div class="col-md-12">
      <div class="alert alert-info">
         <div class="row">
            <div class="col-sm-4">
               <div class="core-box">
                  <div class="heading">
                     <i class="clip-user-6 circle-icon circle-green"></i>
                     <h2>Informações do cliente</h2>
                  </div>
                  <div class="content">
                     <table class="table table-condensed table-hover" >
                        <thead>
                        </thead>
                        <tbody>
                           <tr>
                              <td>Nome: </td>
                              <td>
                                 <?php echo $nome ?>
                              </td>
                           </tr>
                           <tr>
                              <td>CPF: </td>
                              <td>                                       
                                 <?php echo $cpf ?>
                              </td>
                           </tr>
                           <tr>
                              <td>Renda Líquida: </td>
                              <td>                                       
                                 <?php echo "R$ ". number_format($renda_liquida, 2, ",", ".") ?>
                              </td>
                           </tr>
                           <tr>
                              <td>Cadastrado: </td>
                              <td>
                                 <?php echo $isClient ?>                                          
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <div class="col-sm-4">
               <div class="core-box">
                  <div class="heading">
                     <i class=" clip-file-2 circle-icon circle-green"></i>
                     <h2>Informações da solicitação</h2>
                  </div>
                  <div class="content">
                     <table class="table table-condensed table-hover">
                        <thead>
                        </thead>
                        <tbody>
                           <tr>
                              <td>Valor solicitado: </td>
                              <td>
                                 <?php echo "R$ " . number_format($valor_compra,2,",",".") ?>
                              </td>
                           </tr>
                           <?php if ($valor_entrada > 0) {?>
                           <tr>
                              <td>Entrada: </td>
                              <td>
                                 <?php echo "R$ " . number_format($valor_entrada, 2, ",", ".") ?>         
                              </td>
                           </tr>
                           <?php } ?>
                           
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <div class="col-sm-4" id="resumo-solicitacao">
               <div class="core-box">
                  <div class="heading">
                     <i class=" circle-icon circle-green fa fa-list"></i>
                     <h2>Resumo</h2>
                  </div>                  
                  <div class="content">
                     <table class="table table-condensed table-hover">
                        <thead>
                        </thead>
                        <tbody>
                           <tr>
                              <td>Valor financiado: </td>
                              <td>
                                 <?php echo "R$ " . number_format($valor_financiado_hidden, 2, ",", ".") ?>
                           </td>
                           </tr>
                           <tr>
                              <td>Número de parcelas: </td>
                              <td>
                                 <?php echo $num_parcelas ?>
                              </td>
                           </tr>
                           <tr>
                              <td>Valor da parcela: </td>
                              <td>
                                 <?php echo "R$ ". number_format($val_parcelas, 2, ",",".") ?>
                              </td>
                           </tr>
                            <tr>
                              <td>Taxa (%): </td>
                              <td>
                                 <?php echo number_format($cotacao->taxa , 2, ",", "."); ?>
                              </td>
                           </tr>
                           <tr>
                              <td>Valor total: </td>
                              <td >
                                 <?php echo "R$ ". number_format($val_total_hidden , 2, ",", "."); ?>
                              </td>
                           </tr>
                           <tr>
                              <td>Carência: </td>
                              <td>
                                 <?php echo $carencia; ?>
                              </td>
                           </tr>
                           <tr>
                              <td>Data da 1° parcela: </td>
                              <td>
                                 <?php echo $data_pri_par_hidden; ?>
                              </td>
                           </tr>
                           <tr>
                              <td>Data da última parcela: </td>                              
                              <td>
                                 <?php echo $data_ult_par_hidden; ?>
                              </td>
                           </tr>
                           <tr>
                              <td>Cotação: </td>
                              <td>
                                 <?php echo $cotacao->descricao; ?>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-sm-12">
      <div class="panel panel-default">
            
            <div class="panel-heading">
               <i class="fa fa-external-link-square"></i>
               ANÁLISE DE CRÉDITO
               <div class="panel-tools">
                  <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                  </a>                  
               </div>
            </div>

            <div class="panel-body">
               <form enctype="multipart/form-data" id="form" class="smart-wizard" role="form" method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/analiseDeCredito/doAnalise">
                  <div id="wizard" class="swMain">
                     <!--Cabeçalho do wizard-->
                     <ul>
                        <li>
                           <a href="#step-1">
                              <div class="stepNumber">1</div>
                              <span class="stepDesc">Dados Pessoais</span>
                           </a>
                        </li>
                        <li>
                           <a href="#step-2">
                              <div class="stepNumber">2</div>
                              <span class="stepDesc">Endereço</span>
                           </a>
                        </li>
                        <li>
                           <a href="#step-3">
                              <div class="stepNumber">3</div>
                              <span class="stepDesc">Contato</span>
                           </a>
                        </li>
                        <li>
                           <a href="#step-4">
                              <div class="stepNumber">4</div>
                              <span class="stepDesc">Cônjuge</span>
                           </a>
                        </li>
                        <li>
                           <a href="#step-5">
                              <div class="stepNumber">5</div>
                              <span class="stepDesc">Dados bancários</span>
                           </a>
                        </li>
                        <li>
                           <a href="#step-6">
                              <div class="stepNumber">6</div>
                              <span class="stepDesc">Dados profissionais</span>
                           </a>
                        </li>
                        <li>
                           <a href="#step-7">
                              <div class="stepNumber">7</div>
                              <span class="stepDesc">Referência</span>
                           </a>
                        </li>
                        <li>
                           <a href="#step-8">
                              <div class="stepNumber">8</div>
                              <span class="stepDesc">Dados adicionais da compra</span>
                           </a>
                        </li>
                        <li>
                           <a href="#step-9">
                              <div class="stepNumber">9</div>
                              <span class="stepDesc">Anexos</span>
                           </a>
                        </li>
                        <li>
                           <a href="#step-10">
                              <div class="stepNumber">10</div>
                              <span class="stepDesc">Finalizar</span>
                           </a>
                        </li>
                     </ul>
                     
                     <div class="progress progress-striped active progress-sm">
                        <div aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar progress-bar-success step-bar">
                           <span class="sr-only"> 0% Complete (success)</span>
                        </div>
                     </div>
                  
                     <div id="step-1">
                        <h2 class="StepTitle">Dados Pessoais</h2>
                        <div class="row">
                           <div class="row-centralize">
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <label class="control-label">
                                    Nome completo <span class="symbol required"></span>
                                    </label>
                                    <input name="Pessoa[nome]" type="text" class="form-control draft_sent" value="<?php echo $nome ?>" readonly>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-7">
                                       <div class="form-group">
                                          <label class="control-label">
                                             CPF <span class="symbol required"></span>
                                          </label>
                                          <input name="CPFCliente[numero]" type="text" class="form-control draft_sent" value="<?php echo $cpf ?>" readonly>
                                       </div>
                                    </div>
                                    <div class="col-md-5">
                                       <div class="form-group">
                                          <label class="control-label">
                                             É titular do cpf <span class="symbol required"></span>
                                          </label>
                                          <?php echo CHtml::dropDownList('Cadastro[titular_do_cpf]','Cadastro[titular_do_cpf]',
                                             CHtml::listData(TTable::model()->findAll(),
                                             'id','flag' ), array('class'=>'draft_sent form-control search-select')); 
                                          ?> 
                                       </div>
                                    </div>
                                 </div>                     
                                 <div class="row">
                                    <div class="col-md-5">
                                       <div class="form-group">
                                          <label class="control-label">
                                             Nascimento <span class="symbol required"></span>
                                          </label>
                                          <input name="Pessoa[nascimento]" id="nascimento" class="draft_sent form-control input-mask-date" type="text">
                                       </div>
                                    </div>
                                    <div class="col-md-7">
                                       <div class="form-group">
                                          <label class="control-label">Sexo <span class="symbol required"></span></label>
                                          <?php echo CHtml::dropDownList('Pessoa[sexo]','Pessoa[sexo]',
                                             CHtml::listData(Sexo::model()->findAll(),
                                             'sigla','sexo' ), array('class'=>'draft_sent form-control search-select')); 
                                             ?>  
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-5">
                                       <div class="form-group">
                                          <label class="control-label">Nacionalidade <span class="symbol required"></span></label>
                                          <select id="select-nacionalidade" name="Pessoa[nacionalidade]" class="draft_sent form-control search-select">
                                             <option value="Brasileira">Brasileira</option>
                                             <option value="Extrangeira">Extrangeira</option>
                                          </select>
                                       </div>
                                    </div>
                                    
                                    <div class="col-md-7" id="select-estados-wrapper">
                                       <div class="form-group">
                                          <label class="control-label">UF <span class="symbol required"></span></label>
                                          <?php echo CHtml::dropDownList('naturalidade_br','naturalidade_br',
                                             CHtml::listData(Estados::model()->findAll(),
                                             'sigla','nome' ), array('class'=>'draft_sent form-control search-select')
                                             );
                                           ?>
                                       </div>
                                    </div>

                                    <div class="col-md-7" id="select-paises-wrapper" style="display:none">
                                       <div class="form-group">
                                          <label class="control-label">País <span class="symbol required"></span></label>
                                          <?php echo CHtml::dropDownList('naturalidade_ext','naturalidade_ext',
                                             CHtml::listData(Pais::model()->findAll(),
                                             'paisNome','paisNome' ), array('class'=>'form-control search-select draft_sent')
                                             );
                                          ?>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label">Nome do pai</label>
                                    <input name="Cadastro[nome_do_pai]" type="text" class="draft_sent form-control">
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label">
                                       Nome da mãe <span class="symbol required"></span>
                                    </label>
                                    <input name="Cadastro[nome_da_mae]" type="text" class="draft_sent form-control">
                                 </div>
                                 <div class="form-group">
                                    <label class="control-label">
                                       N° de dependentes
                                    </label>
                                    <input name="Cadastro[numero_de_dependentes]" type="text" class="draft_sent form-control">
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="row">
                                    <div class="col-md-5">
                                       <div class="form-group">
                                          <label class="control-label">Estado civil</label>
                                          <select id="select-estado-civil" name="Pessoa[estado_civil]" class="draft_sent form-control search-select">
                                             <option value="Solteiro">Solteiro</option>
                                             <option value="Casado">Casado</option>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="col-md-7">
                                       <div class="form-group">
                                          <label class="control-label">Cônjuge compõe renda</label>
                                          <?php echo CHtml::dropDownList('Cadastro[conjugue_compoe_renda]','Cadastro[conjugue_compoe_renda]',
                                             CHtml::listData(TTable::model()->findAll(),
                                             'id','flag' ), array('class'=>'draft_sent form-control search-select')); 
                                             ?> 
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-5">
                                       <div class="form-group">
                                          <label class="control-label">Tipo de documento</label>
                                          <?php echo CHtml::dropDownList('DocumentoCliente[Tipo_documento_id]','DocumentoCliente[Tipo_documento_id]',
                                             CHtml::listData(TipoDocumento::model()->findAll(array('condition'=>'id != 1')),
                                             'id','tipo' ), array('class'=>'draft_sent form-control search-select')
                                             );?>
                                       </div>
                                    </div>
                                    <div class="col-md-7">
                                       <div class="form-group">
                                          <label class="control-label">Número do documento <span class="symbol required"></span></label>
                                          <input name="DocumentoCliente[numero]" type="text" class="draft_sent form-control">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="form-group">
                                          <label class="control-label">Órgão emissor</label>
                                          <input type="text" name="DocumentoCliente[orgao_emissor]" class="draft_sent form-control">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6" >
                                       <div class="form-group">
                                          <label class="control-label">UF Emissor</label>
                                          <?php echo CHtml::dropDownList('DocumentoCliente[uf_emissor]','DocumentoCliente[uf_emissor]',
                                             CHtml::listData(Estados::model()->findAll(),
                                             'sigla','nome' ), array('class'=>'draft_sent form-control search-select')
                                             );
                                             ?>
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Data de emissão</label>
                                          <input class="draft_sent form-control input-mask-date" type="text" name="DocumentoCliente[data_emissao]">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-3">
                              </div>
                           </div>
                           <div class="wizard-step-btns-wrapper">
                              <div class="col-md-4" style="float:right">
                                  <div class="form-group">
                                    <button class="btn btn-blue next-step btn-block">
                                      Avançar <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                  </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div id="step-2">
                        <h2 class="StepTitle">Endereço</h2>
                        <div class="row">
                           <div class="row-centralize">
                              <div class="col-md-6">
                                 <div class="row">
                                    <div class="col-md-3">
                                       <div class="form-group">
                                          <label class="control-label">
                                             CEP <span class="symbol required"></span>
                                          </label>
                                          <input class="draft_sent form-control cep" type="text" name="EnderecoCliente[cep]" id="cep_cliente">
                                       </div>
                                    </div>
                                    <div class="col-md-9">
                                       <div class="form-group">
                                          <label class="control-label">
                                             Logradouro <span class="symbol required"></span>
                                          </label>
                                          <input class="draft_sent form-control" type="text" name="EnderecoCliente[logradouro]" id="logradouro_cliente">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Cidade <span class="symbol required"></span></label>
                                          <input class="draft_sent form-control" type="text" name="EnderecoCliente[cidade]" id="cidade_cliente">
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Bairro <span class="symbol required"></span></label>
                                          <input class="draft_sent form-control" type="text" name="EnderecoCliente[bairro]" id="bairro_cliente">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">
                                             Tempo de residência (mes/ano) <span class="symbol required"></span>
                                          </label>
                                          <input class="draft_sent form-control" type="text" name="EnderecoCliente[tempo_de_residencia]" id="tempo_de_residencia">
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Tipo de moradia <span class="symbol required"></span></label>
                                          <?php echo CHtml::dropDownList('EnderecoCliente[Tipo_Moradia_id]','EnderecoCliente[Tipo_Moradia_id]',
                                             CHtml::listData(TipoMoradia::model()->findAll(),
                                             'id','tipo' ), array('class'=>'draft_sent form-control search-select'));?>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="row">
                                    <div class="col-md-3">
                                       <div class="form-group">
                                          <label class="control-label">Número <span class="symbol required"></span></label>
                                          <input class="draft_sent form-control" type="text" name="EnderecoCliente[numero]">
                                       </div>
                                    </div>
                                    <div class="col-md-9">
                                       <div class="form-group">
                                          <label class="control-label">Complemento</label>
                                          <input class="draft_sent form-control" type="text" name="EnderecoCliente[complemento]">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">UF <span class="symbol required"></span></label>
                                          <?php echo CHtml::dropDownList('EnderecoCliente[uf]','EnderecoCliente[uf]',
                                             CHtml::listData(Estados::model()->findAll(),
                                             'sigla','nome' ), array('class'=>'draft_sent form-control search-select')
                                             );
                                             ?>
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Tipo de endereço <span class="symbol required"></span></label>
                                          <?php echo CHtml::dropDownList('EnderecoCliente[Tipo_Endereco_id]','EnderecoCliente[Tipo_Endereco_id]',
                                             CHtml::listData(TipoEndereco::model()->findAll(),
                                             'id','tipo' ), array('class'=>'draft_sent form-control search-select'));?>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                 </div>
                              </div>
                           </div>
                           <div class="wizard-step-btns-wrapper">
                              <div class="col-md-4" style="float:left">
                                  <div class="form-group">
                                    <button class="btn btn-blue back-step btn-block">
                                      <i class="fa fa-arrow-circle-left"></i> Voltar
                                    </button>
                                  </div>
                              </div>
                              <div class="col-md-4" style="float:right">
                                  <div class="form-group">
                                    <button class="btn btn-blue next-step btn-block">
                                      Avançar <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                  </div>
                              </div>
                           </div>
                        </div><!--Fim-->
                     </div>
                  
                     <div id="step-3">
                        <h2 class="StepTitle">Contato</h2>
                        <div class="row">
                           <div class="row-centralize">
                              <div class="col-md-6">
                                 <div class="row">
                                    <div class="col-md-4">
                                       <div class="form-group">
                                          <label class="control-label">Telefone 1 <span class="symbol required"></span></label>
                                          <input class="draft_sent form-control telefone" type="text" name="TelefoneCliente[numero]">
                                       </div>
                                    </div>
                                    <div class="col-md-4">
                                       <div class="form-group">
                                          <label class="control-label">Tipo de telefone</label>
                                          <?php echo CHtml::dropDownList('TelefoneCliente[Tipo_Telefone_id]','TelefoneCliente[Tipo_Telefone_id]',
                                             CHtml::listData(TipoTelefone::model()->findAll(),
                                             'id','tipo' ), array('class'=>'draft_sent form-control search-select'
                                             )); ?> 
                                       </div>
                                    </div>
                                    <div class="col-md-4">
                                       <div class="form-group">
                                          <label class="control-label">Ramal </label>
                                          <input class="draft_sent form-control" type="text" name="TelefoneCliente[ramal]">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-4">
                                       <div class="form-group">
                                          <label class="control-label">Telefone 2 <span class="symbol required"></span></label>
                                          <input class="draft_sent form-control telefone" type="text" name="TelefoneCliente2[numero]">
                                       </div>
                                    </div>
                                    <div class="col-md-4">
                                       <div class="form-group">
                                          <label class="control-label">Tipo de telefone</label>
                                          <?php echo CHtml::dropDownList('TelefoneCliente2[Tipo_Telefone_id]','TelefoneCliente2[Tipo_Telefone_id]',
                                             CHtml::listData(TipoTelefone::model()->findAll(),
                                             'id','tipo' ), array('class'=>'draft_sent form-control search-select'
                                             )); ?> 
                                       </div>
                                    </div>
                                    <div class="col-md-4">
                                       <div class="form-group">
                                          <label class="control-label">Ramal </label>
                                          <input class="draft_sent form-control" type="text" name="TelefoneCliente2[ramal]">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Email</label>
                                          <input class="draft_sent form-control" id="email" type="text" name="EmailCliente[email]">
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Facebook</label>
                                          <input class="draft_sent form-control" type="text" name="ContatoCliente[facebook]">
                                       </div>
                                    </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-8">
                                    <div class="form-group">
                                       <label class="control-label">Skype</label>
                                       <input id="skype" class="draft_sent form-control" type="text" name="ContatoCliente[skype]">
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="wizard-step-btns-wrapper">
                              <div class="col-md-4" style="float:left">
                                  <div class="form-group">
                                    <button class="btn btn-blue back-step btn-block">
                                      <i class="fa fa-arrow-circle-left"></i> Voltar
                                    </button>
                                  </div>
                              </div>
                              <div class="col-md-4" style="float:right">
                                  <div class="form-group">
                                    <button class="btn btn-blue next-step btn-block">
                                      Avançar <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                  </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  
                     <div id="step-4">
                        <div class="row">
                           <div class="row-centralize">
                              <!--Dados conjuge esquerda-->
                              <div class="col-md-6">
                                 <div class="row">
                                    <div class="col-md-12">
                                       <h1 style="font-size:20px;">Dados profissionais do Conjuge</h1>
                                       <hr>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="form-group"> 
                                          <label class="control-label">Nome completo</label>
                                          <input name="Conjuge[nome]" type="text" class="draft_sent form-control">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="form-group">
                                          <label class="control-label">CPF do Cônjuge</label>
                                          <input name="CPF_Conjuge[numero]" type="text" class="draft_sent form-control">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-5">
                                       <div class="form-group">
                                          <label class="control-label">Tipo de documento</label>
                                          <?php echo CHtml::dropDownList('Doc_Conjuge[Tipo_documento_id]','Doc_Conjuge[Tipo_documento_id]',
                                             CHtml::listData(TipoDocumento::model()->findAll('id <> 1'),
                                             'id','tipo' ), array('class'=>'draft_sent form-control search-select')
                                             );?>                                          
                                       </div>
                                    </div>
                                    <div class="col-md-7">
                                       <div class="form-group">
                                          <label class="control-label">Número do documento</label>
                                          <input name="Doc_Conjuge[numero]" type="text" class="draft_sent form-control">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="form-group">
                                          <label class="control-label">Órgão emissor</label>
                                          <input type="text" name="Doc_Conjuge[orgao_emissor]" class="draft_sent form-control">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">UF Emissor </label>
                                          <?php echo CHtml::dropDownList('Doc_Conjuge[uf_emissor]','Doc_Conjuge[uf_emissor]',
                                             CHtml::listData(Estados::model()->findAll(),
                                             'sigla','nome' ), array('class'=>'draft_sent form-control search-select')
                                             );
                                             ?>
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Data de emissão</label>
                                          <input id="data_doc_emissao" class="draft_sent form-control input-mask-date" type="text" name="Doc_Conjuge[data_emissao]">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-5">
                                       <div class="form-group">
                                          <label class="control-label">Nascimento</label>
                                          <input class="draft_sent form-control input-mask-date" type="text" name="Conjuge[nascimento]">
                                       </div>
                                    </div>
                                    <div class="col-md-7">
                                       <div class="form-group">
                                          <label class="control-label">Sexo </label>
                                          <?php echo CHtml::dropDownList('Conjuge[sexo]','Conjuge[sexo]',
                                             CHtml::listData(Sexo::model()->findAll(),
                                             'sigla','sexo' ), array('class'=>'draft_sent form-control search-select')); 
                                             ?>  
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-5">
                                       <div class="form-group">
                                          <label class="control-label">Nacionalidade</label>
                                          <select id="select-nacionalidade-con" name="Conjuge[nacionalidade]" class="draft_sent form-control search-select">
                                             <option value="Brasileira">Brasileira</option>
                                             <option value="Extrangeira">Extrangeira</option>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="col-md-7" id="select-estados-wrapper-con">
                                       <div class="form-group">
                                          <label class="control-label">Estados</label>
                                          <?php echo CHtml::dropDownList('naturalidade_con_br','naturalidade_con_br',
                                             CHtml::listData(Estados::model()->findAll(),
                                             'sigla','nome' ), array('class'=>'draft_sent form-control search-select')
                                             );
                                             ?> 
                                       </div>
                                    </div>
                                    <div class="col-md-7" id="select-paises-wrapper-con" style="display:none">
                                       <div class="form-group">
                                          <label class="control-label">País</label>
                                          <?php echo CHtml::dropDownList('naturalidade_con_ext','naturalidade_con_ext',
                                             CHtml::listData(Pais::model()->findAll(),
                                             'paisNome','paisNome' ), array('class'=>'draft_sent form-control search-select')
                                             );
                                          ?>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <h1 style="font-size:20px;">Dados profissionais do Conjuge</h1>
                                       <hr>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Renda Líquida R$</label>
                                          <input class="draft_sent form-control dinheiro" type="text" name="DadosProfissionaisConjuge[renda_liquida]">
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Mês / Ano Renda</label>
                                          <input  class="draft_sent form-control mes_ano_renda" type="text" name="DadosProfissionaisConjuge[mes_ano_renda]">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Ocupação atual</label>
                                          <?php echo CHtml::dropDownList('DadosProfissionaisConjuge[Tipo_Ocupacao_id]','DadosProfissionaisConjuge[Tipo_Ocupacao_id]',
                                             CHtml::listData(TipoOcupacao::model()->findAll(),
                                             'id','tipo' ), array('class'=>'draft_sent form-control search-select')); 
                                             ?> 
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Profissão</label>
                                          <?php echo CHtml::dropDownList('DadosProfissionaisConjuge[profissao]','DadosProfissionaisConjuge[profissao]',
                                             CHtml::listData(Profissao::model()->findAll(),
                                             'profissao','profissao' ), array('class'=>'draft_sent form-control search-select')); 
                                             ?> 
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Tipo de comprovante</label>
                                          <?php echo CHtml::dropDownList('DadosProfissionaisConjuge[TipoDeComprovante]','DadosProfissionaisConjuge[TipoDeComprovante]',
                                             CHtml::listData(TipoDeComprovante::model()->findAll(),
                                             'tipo','tipo' ), array('class'=>'draft_sent form-control search-select')); 
                                          ?>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!--Dados conjuge direita-->
                              <div class="col-md-6">
                                 <div class="row">
                                    <h1 style="font-size:20px; padding-left:15px">Endereço do Conjuge</h1>
                                    <div class="col-md-12">
                                       <hr>
                                       <div class="form-group">
                                          <label class="control-label">Usar mesmo endereço do cliente?</label>
                                          <?php echo CHtml::dropDownList('MesmoEndereco','MesmoEndereco',
                                             CHtml::listData(TTable::model()->findAll(),
                                             'id','flag' ), array('class'=>'draft_sent form-control search-select')); 
                                             ?> 
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-3">
                                       <div class="form-group">
                                          <label class="control-label">CEP</label>
                                          <input id="cep_conjuge" class="draft_sent form-control cep" type="text" name="EnderecoConjuge[cep]">
                                       </div>
                                    </div>
                                    <div class="col-md-9">
                                       <div class="form-group">
                                          <label class="control-label">Logradouro</label>
                                          <input id="logradouro_conjuge" class="draft_sent form-control" type="text" name="EnderecoConjuge[logradouro]">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Cidade</label>
                                          <input id="cidade_conjuge" class="draft_sent form-control" type="text" name="EnderecoConjuge[cidade]">
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Bairro</label>
                                          <input id="bairro_conjuge" class="draft_sent form-control" type="text" name="EnderecoConjuge[bairro]">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-3">
                                       <div class="form-group">
                                          <label class="control-label">Número</label>
                                          <input class="draft_sent form-control" type="text" name="EnderecoConjuge[numero]">
                                       </div>
                                    </div>
                                    <div class="col-md-9">
                                       <div class="form-group">
                                          <label class="control-label">Complemento</label>
                                          <input class="draft_sent form-control" type="text" name="EnderecoConjuge[complemento]">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">UF</label>
                                          <?php echo CHtml::dropDownList('EnderecoConjuge[uf]','EnderecoConjuge[uf]',
                                             CHtml::listData(Estados::model()->findAll(),
                                             'sigla','nome' ), array('class'=>'draft_sent form-control search-select')
                                             );
                                          ?>
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Tipo de endereço</label>
                                          <?php echo CHtml::dropDownList('EnderecoConjuge[Tipo_Endereco_id]','EnderecoConjuge[Tipo_Endereco_id]',
                                             CHtml::listData(TipoEndereco::model()->findAll(),
                                             'id','tipo' ), array('class'=>'draft_sent form-control search-select'));
                                          ?>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <h1 style="font-size:20px;">Contato do Conjuge</h1>
                                       <hr>
                                    </div>      
                                 </div>
                                 <div class="row">
                                    <div class="col-md-4">
                                       <div class="form-group">
                                          <label class="control-label">Telefone</label>
                                          <input class="draft_sent form-control telefone" type="text" name="TelefoneConjuge[numero]">
                                       </div>
                                    </div>
                                    <div class="col-md-8">
                                       <div class="form-group">
                                          <label class="control-label">Tipo de telefone</label>
                                          <?php echo CHtml::dropDownList('TelefoneConjuge[Tipo_Telefone_id]','TelefoneConjuge[Tipo_Telefone_id]',
                                             CHtml::listData(TipoTelefone::model()->findAll(),
                                             'id','tipo' ), array('class'=>'draft_sent form-control search-select')); 
                                             ?> 
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Email</label>
                                          <input class="draft_sent form-control" type="text" name="EmailConjuge[email]">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="wizard-step-btns-wrapper">
                              <div class="col-md-4" style="float:left">
                                  <div class="form-group">
                                    <button class="btn btn-blue back-step btn-block">
                                      <i class="fa fa-arrow-circle-left"></i> Voltar
                                    </button>
                                  </div>
                              </div>
                              <div class="col-md-4" style="float:right">
                                  <div class="form-group">
                                    <button class="btn btn-blue next-step btn-block">
                                      Avançar <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                  </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  
                     <div id="step-5">
                        <h2 class="StepTitle">Dados bancários</h2>
                        <div class="row">
                           <div class="row-centralize">
                              <div class="col-md-6">
                                 <div class="row">
                                    <div class="col-md-5">
                                       <div class="form-group">
                                          <label class="control-label">N° conta</label>
                                          <input class="draft_sent form-control" type="text" name="DadosBancarios[numero]">
                                       </div>
                                    </div>
                                    <div class="col-md-5">
                                       <div class="form-group">
                                          <label class="control-label">Tipo de conta</label>
                                          <?php echo CHtml::dropDownList('DadosBancarios[Tipo_Conta_Bancaria_id]','DadosBancarios[Tipo_Conta_Bancaria_id]',
                                             CHtml::listData(TipoContaBancaria::model()->findAll(),
                                             'id','tipo' ), array('class'=>'draft_sent form-control search-select')); 
                                             ?> 
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="row">
                                    <div class="col-md-5">
                                       <div class="form-group">
                                          <label class="control-label">
                                             Agência <span class="symbol required"></span>
                                          </label>
                                          <input class="draft_sent form-control" type="text" name="DadosBancarios[agencia]">
                                       </div>
                                    </div>
                                    <div class="col-md-5">
                                       <div class="form-group">
                                          <label class="control-label">Banco</label>
                                          <?php echo CHtml::dropDownList('DadosBancarios[Banco_id]','DadosBancarios[Banco_id]',
                                             CHtml::listData(Banco::model()->findAll(),'id','nome'),
                                             array('class'=>'draft_sent form-control search-select'));?>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-3">
                                       <div class="form-group">
                                          <label class="control-label">Data de abertura</label>
                                          <input class="draft_sent form-control ipt_date" type="text" name="DadosBancarios[data_abertura]">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="wizard-step-btns-wrapper">
                              <div class="col-md-4" style="float:left">
                                  <div class="form-group">
                                    <button class="btn btn-blue back-step btn-block">
                                      <i class="fa fa-arrow-circle-left"></i> Voltar
                                    </button>
                                  </div>
                              </div>
                              <div class="col-md-4" style="float:right">
                                  <div class="form-group">
                                    <button class="btn btn-blue next-step btn-block">
                                      Avançar <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                  </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  
                     <div id="step-6">
                        <h2 class="StepTitle">Dados Profissionais</h2>
                        <div class="row">
                           <div class="row-centralize">
                              <div class="col-md-6">
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Classe Profissional</label>
                                          <?php echo CHtml::dropDownList('DadosProfissionais[Classe_Profissional_id]','DadosProfissionais[Classe_Profissional_id]',
                                             CHtml::listData(ClasseProfissional::model()->findAll(),
                                             'id','classe' ), array('class'=>'draft_sent form-control search-select')); 
                                             ?>
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">CNPJ/CPF</label>
                                          <input class="draft_sent form-control" type="text" name="DadosProfissionais[cnpj_cpf]">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Empresa</label>
                                          <input class="draft_sent form-control" type="text" name="DadosProfissionais[empresa]">
                                       </div>
                                    </div>   
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Ocupação atual</label>
                                          <?php echo CHtml::dropDownList('DadosProfissionais[Tipo_Ocupacao_id]','DadosProfissionais[Tipo_Ocupacao_id]',
                                             CHtml::listData(TipoOcupacao::model()->findAll(),
                                             'id','tipo' ), array('class'=>'draft_sent form-control search-select')); 
                                             ?> 
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Data de admissão</label>
                                          <input class="draft_sent form-control ipt_date" type="text" name="DadosProfissionais[data_admissao]">
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Profissão</label>
                                          <?php echo CHtml::dropDownList('DadosProfissionais[profissao]','DadosProfissionais[profissao]',
                                             CHtml::listData(Profissao::model()->findAll(),
                                             'profissao','profissao' ), array('class'=>'draft_sent form-control search-select')); 
                                             ?> 
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Renda Líquida R$</label>
                                          <input value="<?php echo $renda_liquida ?>" class="draft_sent form-control dinheiro" type="text" name="DadosProfissionais[renda_liquida]">
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Mês / Ano Renda</label>
                                          <input class="draft_sent form-control mes_ano_renda" type="text" name="DadosProfissionais[mes_ano_renda]">
                                       </div>
                                    </div>
                                 </div>
                                
                                 <!--<div class="row">
                                    
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Pensionista</label>
                                          <?php echo CHtml::dropDownList('DadosProfissionais[pensionista]','DadosProfissionais[pensionista]',
                                             CHtml::listData(TTable::model()->findAll(),
                                             'id','flag' ), array('class'=>'form-control search-select')); 
                                             ?> 
                                       </div>
                                    </div>
                                 </div>-->
                                 
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Órgão Pagador</label>
                                          <?php echo CHtml::dropDownList('DadosProfissionais[orgao_pagador]','DadosProfissionais[orgao_pagador]',
                                             CHtml::listData(OrgaoPagador::model()->findAll(),
                                             'nome','nome' ), array('class'=>'draft_sent form-control search-select')); 
                                             ?> 
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">N° do benefício</label>
                                          <input class="draft_sent form-control" type="text" name="DadosProfissionais[numero_do_beneficio]">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Tipo de comprovante</label>
                                          <?php echo CHtml::dropDownList('DadosProfissionais[TipoDeComprovante]','DadosProfissionais[TipoDeComprovante]',
                                             CHtml::listData(TipoDeComprovante::model()->findAll(),
                                             'tipo','tipo' ), array('class'=>'draft_sent form-control search-select')); 
                                             ?> 
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!--Endereço/Contato Profissional-->
                           <div class="col-md-6">
                              <div class="row">
                                 <div class="col-md-3">
                                    <div class="form-group">
                                       <label class="control-label">CEP</label>
                                       <input id="cep_dados_profissionais" class="draft_sent form-control cep" type="text" name="EnderecoProfiss[cep]">
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Logradouro</label>
                                       <input id="logradouro_dados_profissionais" class="draft_sent form-control" type="text" name="EnderecoProfiss[logradouro]">
                                    </div>
                                 </div>
                                 <div class="col-md-2">
                                    <div class="form-group">
                                       <label class="control-label">Número</label>
                                       <input  class="draft_sent form-control" type="text" name="EnderecoProfiss[numero]">
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-4">
                                    <div class="form-group">
                                       <label class="control-label">Cidade</label>
                                       <input id="cidade_dados_profissionais" class="draft_sent form-control" type="text" name="EnderecoProfiss[cidade]">
                                    </div>
                                 </div>
                                 <div class="col-md-8">
                                    <div class="form-group">
                                       <label class="control-label">Bairro</label>
                                       <input id="bairro_dados_profissionais" class="draft_sent form-control" type="text" name="EnderecoProfiss[bairro]">
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-4">
                                    <div class="form-group">
                                       <label class="control-label">UF</label>
                                       <?php echo CHtml::dropDownList('EnderecoProfiss[uf]','EnderecoProfiss[uf]',
                                          CHtml::listData(Estados::model()->findAll(),
                                          'sigla','nome' ), array('class'=>'draft_sent form-control search-select')
                                          );
                                        ?>
                                    </div>
                                 </div>
                                 <div class="col-md-8">
                                    <div class="form-group">
                                       <label class="control-label">Complemento</label>
                                       <input class="draft_sent form-control" type="text" name="EnderecoProfiss[complemento]">
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-4">
                                    <div class="form-group">
                                       <label class="control-label">Telefone</label>
                                       <input class="draft_sent form-control telefone" type="text" name="TelefoneProfiss[numero]">
                                    </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                       <label class="control-label">Ramal</label>
                                       <input class="draft_sent form-control" type="text" name="TelefoneProfiss[ramal]">
                                    </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="form-group">
                                       <label class="control-label">Tipo de telefone</label>
                                       <?php echo CHtml::dropDownList('TelefoneProfiss[Tipo_Telefone_id]','TelefoneProfiss[Tipo_Telefone_id]',
                                          CHtml::listData(TipoTelefone::model()->findAll(),
                                          'id','tipo' ), array('class'=>'draft_sent form-control search-select')); 
                                          ?> 
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-4">
                                    <div class="form-group">
                                       <label class="control-label">Email</label>
                                       <input class="draft_sent form-control" type="text" name="EmailProfiss[email]">
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="wizard-step-btns-wrapper">
                              <div class="col-md-4" style="float:left">
                                  <div class="form-group">
                                    <button class="btn btn-blue back-step btn-block">
                                      <i class="fa fa-arrow-circle-left"></i> Voltar
                                    </button>
                                  </div>
                              </div>
                              <div class="col-md-4" style="float:right">
                                  <div class="form-group">
                                    <button class="btn btn-blue next-step btn-block">
                                      Avançar <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                  </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  
                     <div id="step-7">
                        <h2 class="StepTitle">Referência</h2>
                        <div class="row">
                           <div class="row-centralize">
                              <div class="col-md-6">
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Nome <span class="symbol required"></span></label>
                                          <input class="draft_sent form-control" type="text" name="Referencia1[nome]">
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Tipo de referência</label>
                                          <?php echo CHtml::dropDownList('Referencia1[Tipo_Referencia_id]','Referencia1[Tipo_Referencia_id]',
                                             CHtml::listData(TipoReferencia::model()->findAll(),
                                             'id','tipo' ), array('class'=>'draft_sent form-control search-select')); 
                                             ?>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Telefone <span class="symbol required"></span></label>
                                          <input class="draft_sent form-control telefone" type="text" name="TelefoneRef1[numero]">
                                       </div>
                                    </div>
                                    <div class="col-md-4">
                                       <div class="form-group">
                                          <label class="control-label">Ramal</label>
                                          <input class="draft_sent form-control" type="text" name="TelefoneRef1[ramal]">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Nome <span class="symbol required"></span></label>
                                          <input class="draft_sent form-control" type="text" name="Referencia2[nome]">
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Tipo de referência</label>
                                          <?php echo CHtml::dropDownList('Referencia2[Tipo_Referencia_id]','Referencia2[Tipo_Referencia_id]',
                                             CHtml::listData(TipoReferencia::model()->findAll(),
                                             'id','tipo' ), array('class'=>'draft_sent form-control search-select')); 
                                             ?>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Telefone <span class="symbol required"></span></label>
                                          <input class="draft_sent form-control telefone" type="text" name="TelefoneRef2[numero]">
                                       </div>
                                    </div>
                                    <div class="col-md-4">
                                       <div class="form-group">
                                          <label class="control-label">Ramal</label>
                                          <input class="draft_sent form-control" type="text" name="TelefoneRef2[ramal]">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="wizard-step-btns-wrapper">
                              <div class="col-md-4" style="float:left">
                                  <div class="form-group">
                                    <button class="btn btn-blue back-step btn-block">
                                      <i class="fa fa-arrow-circle-left"></i> Voltar
                                    </button>
                                  </div>
                              </div>
                              <div class="col-md-4" style="float:right">
                                  <div class="form-group">
                                    <button class="btn btn-blue next-step btn-block">
                                      Avançar <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                  </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  
                     <div id="step-8">
                        <h2 class="StepTitle">Resumo</h2>
                        <div class="row">
                           <div class="row-centralize">
                              <div class="col-md-6">
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Número do pedido</label>
                                          <input class="draft_sent form-control" type="text" name="AnaliseDeCredito[numero_do_pedido]">
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Vendedor</label>
                                          <input class="draft_sent form-control" type="text" name="AnaliseDeCredito[vendedor]">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Promotor</label>
                                          <input class="draft_sent form-control" type="text" name="AnaliseDeCredito[promotor]">
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Procedência da compra</label>
                                          <?php echo CHtml::dropDownList('AnaliseDeCredito[Procedencia_compra_id]','AnaliseDeCredito[Procedencia_compra_id]',
                                             CHtml::listData(ProcedenciaCompra::model()->findAll(),
                                             'id','procedencia' ), array('class'=>'draft_sent form-control search-select')); 
                                             ?> 
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Valor</label>
                                          <input class="draft_sent form-control dinheiro" type="text" name="AnaliseDeCredito[valor]">
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Entrada</label>
                                          <input class="draft_sent form-control" type="text" name="AnaliseDeCredito[entrada]">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Alerta</label>
                                          <?php echo CHtml::dropDownList('AnaliseDeCredito[alerta]','AnaliseDeCredito[alerta]',
                                             CHtml::listData(Alerta::model()->findAll(),
                                             'alerta','alerta' ), array('class'=>'draft_sent form-control search-select')); 
                                             ?> 
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Mercadoria retirada na hora</label>
                                          <?php echo CHtml::dropDownList('AnaliseDeCredito[mercadoria_retirada_na_hora]','AnaliseDeCredito[mercadoria_retirada_na_hora]',
                                             CHtml::listData(TTable::model()->findAll(),
                                             'id','flag' ), array('class'=>'draft_sent form-control search-select')); 
                                             ?> 
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">É celular pré-pago</label>
                                          <?php echo CHtml::dropDownList('AnaliseDeCredito[celular_pre_pago]','AnaliseDeCredito[celular_pre_pago]',
                                             CHtml::listData(TTable::model()->findAll(),
                                             'id','flag' ), array('class'=>'draft_sent form-control search-select')); 
                                             ?> 
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="form-group">
                                          <label class="control-label">Classificação do bem</label>
                                          <?php echo CHtml::dropDownList('Bens','Bens',
                                             CHtml::listData(GrupoDeProduto::model()->findAll(),
                                             'id','nome' ), array('class'=>'draft_sent form-control search-select', 'multiple'=>'multiple', 'placeholder'=>'Selecione')); ?> 
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="form-group">
                                          <label class="control-label">Mercadoria</label>
                                          <input class="draft_sent form-control" type="text" name="AnaliseDeCredito[mercadoria]">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="form-group">
                                          <label class="control-label">Observação</label>
                                          <textarea name="AnaliseDeCredito[observacao]" maxlength="100" id="form-field-23" class="draft_sent form-control limited"></textarea>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="wizard-step-btns-wrapper">
                              <div class="col-md-4" style="float:left">
                                  <div class="form-group">
                                    <button class="btn btn-blue back-step btn-block">
                                      <i class="fa fa-arrow-circle-left"></i> Voltar
                                    </button>
                                  </div>
                              </div>
                              <div class="col-md-4" style="float:right">
                                  <div class="form-group">
                                    <button class="btn btn-blue next-step btn-block">
                                      Avançar <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                  </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  
                     <div id="step-9">
                        <h2 class="StepTitle">Anexos</h2>

                        <div class="row">

                           <div class="row-centralize">
                        
                              <div id="div-add-up" class="col-md-12">
                        
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="form-group">
                                          <div class="col-sm-4">
                                             <a id="add-upload-field" class="btn btn-primary" href="#"><i class="fa fa-plus"></i> Adicionar</a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 
                                 <br>

                                 <div class="row">
                                    <div class="col-md-9">
                                       <div class="form-group">
                                          <div class="col-sm-9">
                                             <div data-provides="fileupload" class="fileupload fileupload-new">
                                                <span class="btn btn-file btn-light-grey">
                                                   <i class="fa fa-folder-open-o"></i>
                                                   <span class="fileupload-new">Selecionar arquivo</span>
                                                   <span class="fileupload-exists">Mudar</span>
                                                   <input type="file" name="arquivos[]" required>
                                                </span>
                                                <span class="fileupload-preview"></span>
                                             </div>
                                         
                                             <p class="help-block">
                                                <input required type="text" class="form-control" name="descricoes[]">
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>

                              <!--<div id="div-add-up" class="col-md-12">
                                 <div class="row">
                                    <div class="col-md-3">
                                       <div class="form-group">
                                          <button class="btn btn-blue next-step btn-block">
                                          Avançar <i class="fa fa-arrow-circle-right"></i>
                                          </button>
                                       </div>
                                    </div>
                                 </div>
                              </div>-->
                           </div>
                           <div class="wizard-step-btns-wrapper">
                              <div class="col-md-4" style="float:left">
                                  <div class="form-group">
                                    <button class="btn btn-blue back-step btn-block">
                                      <i class="fa fa-arrow-circle-left"></i> Voltar
                                    </button>
                                  </div>
                              </div>
                              <div class="col-md-4" style="float:right">
                                  <div class="form-group">
                                    <button class="btn btn-blue next-step btn-block">
                                      Avançar <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                  </div>
                              </div>
                           </div>
                        </div>

                     </div>
                     
                     <div id="step-10">
                        <div class="alert alert-block alert-warning fade in">
                           <button data-dismiss="alert" class="close" type="button">
                              &times;
                           </button>
                           <h4 class="alert-heading"><i class="fa fa-exclamation-triangle"></i> Atenção!</h4>
                           <p>
                              Antes de submeter o formulário, certifique-se de que os dados informados estão corretos. Lembre-se que todos os dados solicitados são importantes para a análise, 
                              portanto, revise-os antes de concluir.
                           </p>
                           <p>
                              <input type="submit" class="btn btn-yellow" value="Estou ciente, quero enviar.">
                           </p>
                        </div>
                        <div class="wizard-step-btns-wrapper">
                           <div class="col-md-4" style="float:left">
                              <div class="form-group">
                                 <button class="btn btn-blue back-step btn-block">
                                   <i class="fa fa-arrow-circle-left"></i> Voltar
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>
                    
                  </div>
                  </div>
                  </div>
                  <!--Hidden fields-->
                  <input class="draft_sent" type="hidden" value="<?php echo $renda_liquida?>" name="cliente_renda_liquida">
                  <input class="draft_sent" type="hidden" value="<?php echo $valor_compra?>" name="valor_solicitado_analise">
                  <input class="draft_sent" type="hidden" value="<?php echo $valor_financiado_hidden?>" name="valor_financiado">
                  <input class="draft_sent" type="hidden" value="<?php echo $data_pri_par_hidden?>" name="data_pri_par">
                  <input class="draft_sent" type="hidden" value="<?php echo $data_ult_par_hidden?>" name="data_ult_par">

                  <input type="hidden" value="<?php echo $valor_compra?>" name="Proposta[valor]">
                  <input class="draft_sent" type="hidden" value="<?php echo $num_parcelas ?>" name="Proposta[qtd_parcelas]">
                  <input class="draft_sent" type="hidden" value="<?php echo $carencia ?>" name="Proposta[carencia]">
                  <input class="draft_sent" type="hidden" value="<?php echo $val_parcelas?>" name="Proposta[valor_parcela]">
                  <input class="draft_sent" type="hidden" value="<?php echo $valor_entrada ?>" name="Proposta[valor_entrada]">
                  <input class="draft_sent" type="hidden" value="<?php echo $val_total_hidden ?>" name="Proposta[valor_final]">
                  <input class="draft_sent" type="hidden" value="<?php echo $cotacao->id ?>" name="Proposta[Cotacao_id]">
                  <input type="hidden" value="<?php echo $cotacao->id ?>" name="Proposta_id">
                  <input type="hidden" class="draft_sent" value="<?php echo $rascunho->id ?>" name="Rascunho_id">

               </form>
            </div>
      </div>
   </div>
</div>

<script type="text/javascript">
   $(function(){

      $.pnotify.defaults.history = false;

      $.pnotify({
         title    : 'Atenção',
         text     : <?php echo json_encode(Yii::app()->session['usuario']->nome_utilizador); ?> + ', por favor, lembre-se de anexar, na sessão de anexos, todas as cópias dos documentos informados neste cadastro. Elas são de extrema importância no processo de avaliação de crédito.',
         type     : 'error'
      });
   });
</script>