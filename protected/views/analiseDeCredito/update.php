<?php
/* @var $this AnaliseDeCreditoController */
/* @var $model AnaliseDeCredito */

$this->breadcrumbs=array(
	'Analise De Creditos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AnaliseDeCredito', 'url'=>array('index')),
	array('label'=>'Create AnaliseDeCredito', 'url'=>array('create')),
	array('label'=>'View AnaliseDeCredito', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AnaliseDeCredito', 'url'=>array('admin')),
);
?>

<h1>Update AnaliseDeCredito <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>