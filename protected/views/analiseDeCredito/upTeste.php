<?php  ?>
<div class="row">
   <div class="col-sm-12">
      <!-- start: FILE UPLOAD PANEL -->
      <div class="panel panel-default">
         <div class="panel-heading">
            <i class="fa fa-external-link-square"></i>
            File upload
            <div class="panel-tools">
               <a class="btn btn-xs btn-link panel-close" href="#">
                  <i class="fa fa-times"></i>
               </a>
            </div>
         </div>
         <div class="panel-body">
            <form method="POST" id="myform" class="form-horizontal" action="<?php echo Yii::app()->request->baseUrl; ?>/analiseDeCredito/uploadTeste" enctype="multipart/form-data">
               
               <div class="form-group">
                  <div class="col-sm-4">
                     <a id="add-upload-field" class="btn btn-primary" href="#"><i class="fa fa-plus"></i> Adicionar</a>
                     <input name="sendbtn" type="submit" value="Enviar" class="btn btn-bricky">
                  </div>
               </div>

               <div class="form-group">
                  <div class="col-sm-6">
                     <div data-provides="fileupload" class="fileupload fileupload-new">
                        <span class="btn btn-file btn-light-grey">
                           <i class="fa fa-folder-open-o"></i> 
                           <span class="fileupload-new">Selecionar arquivo</span>
                           <span class="fileupload-exists">Mudar</span>
                           <input type="file" name="arquivos[]" required>
                        </span>
                        <span class="fileupload-preview"></span>
                     </div>
                     <p class="help-block">
                        <input required type="text" class="form-control" name="descricoes[]">
                     </p>
                  </div>
               </div>
               
            </form>
         </div>
      </div>
   </div>
</div>