<?php $util = new Util; ?>

<div class="row">
   
   <div class="col-sm-12">
      <div class="page-header">
         <h1>Detalhes da análise: <small style="font-size:18px"><?php echo $analise->codigo ?></small></h1>
      </div>
   </div>

</div>
<div class="row">
   <div class="col-sm-12">
	   <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
		   <thead>
		      <tr>
		         <th>Filial</th>
		         <th>N° pedido</th>
		         <th>Vendedor</th>
		         <th>Promotor</th>
		         <th>Procedência da compra</th>
		         <th>Mercadoria retirada na hora</th>
		         <th>Alerta</th>
		         <th>Obs.</th>
		      </tr>
		   </thead>
		   <tbody>
		      <tr>
		         <td><?php echo $analise->filial->nome_fantasia . ' / '. $analise->filial->getEndereco()->cidade . '-' . $analise->filial->getEndereco()->uf; ?></td>
		         <td><?php echo $analise->numero_do_pedido ?></td>
		         <td><?php echo $analise->vendedor ?></td>
		         <td><?php echo $analise->promotor ?></td>
		         <td><?php echo $analise->procedenciaCompra->procedencia ?></td>
		         <td>
		         	<?php if ($analise->mercadoria_retirada_na_hora) {
		         			echo "Sim";
		         		} 
		         		else{
		         			echo "Não";
		         		}
		         	?>
		         </td>
		         <td><?php echo $analise->alerta ?></td>
		         <td><?php echo $analise->observacao ?></td>
		      </tr>
		   </tbody>
	   </table>
   </div>
</div>