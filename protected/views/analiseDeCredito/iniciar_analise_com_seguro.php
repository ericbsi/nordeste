<div class="row"  id="initial_place">
   <div class="col-sm-12">
      <ol class="breadcrumb">
         <li><a href="#">Análises</a></li>
         <li class="active">Criar</li>
      </ol>
      <div class="page-header">
          <?php if($emprestimo){ ?>
            <h1>Empréstimo</h1>
          <?php } else { ?>
            <h1>Análise de Crédito</h1>
          <?php } ?>
          <input id="emprestimo_tf" 
               type="hidden"
               <?php if($emprestimo) {?>
                  value="1"
               <?php } ?>
               <?php if(!$emprestimo) {?>
                  value="0"
               <?php } ?>
         >
         <input id="emprestimo_max" 
               type="hidden"
               <?php if($emprestimo && !$semear) {
                  $config_max = ConfigLN::model()->find("habilitado AND parametro = 'valor_max_emprestimo'");
               ?>
                  value="<?php echo $config_max->valor; ?>"
               <?php } ?>
          >

          <input id="emprestimo_min" 
               type="hidden"
               <?php if($emprestimo && !$semear) {
                  $config_min = ConfigLN::model()->find("habilitado AND parametro = 'valor_min_emprestimo'");
               ?>
                  value="<?php echo $config_min->valor; ?>"
               <?php } ?>
          >
      </div>
   </div>
</div>
<div class="row">
   <div class="col-sm-12" id="msgsReturn">
   </div>
</div>
<div class="row">
   <br>
</div>

<form id="form-cpf">
   <div class="row">
      <div class="form-group">
        <div class="col-md-4">
            <label class="control-label">CPF do Cliente : <span class="symbol required"></span></label>
            <span class="input-icon">
               <input onkeydown="bloquear_ctrl_j()" data-icon-valid-bind="#ico-cpf-valid-bind" required type="text" placeholder="" class="form-control cpf input-lg number" id="cpf" style="padding-left:35px">
               <i id="ico-cpf-valid-bind" class="fa fa-credit-card" style="font-size:20px;line-height:46px"></i>
            </span>
         </div>      
      </div>
   </div>
</form>

<div class="row">
   <p></p>
   <p></p>
   <p></p>
</div>
<div class="row" style="background-color : <?php if($emprestimo) { echo "#EEFFEE"; } else { echo "#EEFFFF"; }?>" >
   <div class="col-sm-12">
      <div class="tabbable tabs-left">
         <ul id="tabForm" class="nav nav-tabs tab-green">
            <li class="active" id="li-aba-detalhes-proposta">
               <a href="#tab4_detalhes_proposta" data-toggle="tab" data-form-bind="valor_da_compra">
               <i class="fa fa-keyboard-o"></i> Dados da Proposta
               </a>
            </li>
            <li class="" id="li-aba-dados-cliente">
               <a href="#tab4_dados_do_cliente" data-toggle="tab" data-form-bind="cliente_nome">
               <i class="pink fa fa-user"></i> Dados do Cliente
               </a>
            </li>
            <li class="" id="li-aba-endereco">
               <a href="#tab4_endereco" data-toggle="tab" data-form-bind="cep_cliente">
                  <i class="blue fa fa-home"></i> Endereço
               </a>
            </li>
            <li class="" id="li-aba-contato">
               <a href="#tab4_contato" data-toggle="tab" data-form-bind="telefone1_numero">
               <i class="fa fa-phone"></i> Telefones
               </a>
            </li>
            <li class="" id="li-aba-dados-profissionais">
               <a href="#tab4_dados_profissionais" data-toggle="tab" data-form-bind="dp_cnpj_cpf">
               <i class="blue fa fa-briefcase"></i> Dados Profissionais
               </a>
            </li>
            <li class="" id="li-aba-dados-conjuge">
               <a href="#tab4_dados_conjuge" data-toggle="tab" data-form-bind="cpf_conjuge">
               <i class="pink fa fa-male"></i>
               <i style="margin-left:-13px!important;" class="pink fa fa-female"></i>Dados do Cônjuge
               </a>
            </li>
            <li class="" id="li-aba-referencias">
               <a href="#tab4_referencias" data-toggle="tab" data-form-bind="referencia1_parentesco">
               <i class="fa fa-group"></i> Referências
               </a>
            </li>
            <li class="" id="li-aba-anexos">
               <a href="#tab4_anexos" data-toggle="tab" data-form-bind="anexo">
               <i class="fa fa-paperclip"></i> Anexos
               </a>
            </li>
         </ul>
         <div class="tab-content">
            <div class="tab-pane active" id="tab4_detalhes_proposta">
               <div class="col-md-12">
                  <form action="#" id="form-proposta">
                    <input type="hidden" name="emprestimo" value="<?php echo $emprestimo; ?>">
                    <input type="hidden" id="semear_utilizado" name="semear" value="<?php echo $semear; ?>">
                    <?php if($semear) {?>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="control-label">Cliente da casa? : <span class="symbol required"></span></label>
                                    <select required="true" id="Cadastro_cliente_da_casa2" name="Cadastro[cliente_da_casa]" class="form-control search-select select2">
                                        <option value="">Selecione:</option>
                                        <?php foreach( TTable::model()->findAll("flag = " . "'NÃO'") as $tt ) { ?>
                                        <option value="<?php echo $tt->id ?>"><?php echo $tt->flag ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-2">
                                 <label class="control-label">Valor <span class="symbol required"></span></label>
                                 <input id="valor_da_compra" required class="form-control dinheiro currency2 triggerSimularInput" type="text" name="Proposta[valor]">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-<?php if($emprestimo){echo "0";}else{echo "2";}?>">
                                 <?php if(!$emprestimo){ ?><label class="control-label">Valor de entrada <span class="symbol required"></span></label><?php } ?>
                                 <input id="valor_de_entrada" 
                                        required 
                                        class="form-control dinheiro currency triggerSimularInput" 
                                        name="Proposta[valor_entrada]"
                                        <?php if($emprestimo) {?>
                                            value="0"
                                            disabled="disabled"
                                            type="hidden"
                                        <?php } else {?>
                                            type="text"
                                        <?php } ?>
                                        >
                              </div>
                           </div>
                           <div class="form-group">
                              <?php if(!$semear){?>
                              <div class="col-sm-3">
                                    <label class="control-label">Tabela <span class="symbol required"></span></label>
                                    <?php echo CHtml::dropDownList('Proposta[Tabela_id]','Proposta[Tabela_id]', CHtml::listData( TabelaCotacao::model()->findAll( Cotacao::model()->filialCotacoes( Yii::app()->session['usuario']->returnFilial($emprestimo) ) ),
                                    'id','descricao' ), array( 'class'=>'form-control search-select select2', 'prompt'=>'Selecione:','required'=>true,'id'=>'Cotacao_id' ));
                                    ?>
                              </div>
                              <?php } else { ?>
                               <div class="col-sm-3" id="cotacao_display">
                                 <label class="control-label">Tabela <span class="symbol required"></span></label>
                                 <input required="true" name="Proposta[Tabela_id]" placeholder="Selecione" type="hidden" id="Cotacao_id" class="form-control search-select triggerSimularSelect">
                               </div>
                              <?php } ?>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">Carência <span class="symbol required"></span></label>
                                 <input required="true" name="Proposta[carencia]" placeholder="Selecione" type="hidden" id="selectCarencias" class="form-control search-select triggerSimularSelect">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-2">
                                 <label class="control-label">
                                     Seguro? 
                                     <span class="symbol required"></span>
                                 </label>
                                 <select required="required" name="Proposta[segurada]" class="form-control search-select select2 triggerSimularSelect">
                                    <?php if(!$emprestimo || $semear) { ?>
                                        <option value="0">NÃO</option>
                                    <?php } ?>
                                    <option value="1">SIM</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group" id="opcoes-parcelas">
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">Valor financiado</label>
                                 <input readonly="readonly" id="valor_financiado"class="form-control" type="text" name="">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">Valor final</label>
                                 <input readonly="readonly" id="valor_financiado_final"class="form-control" type="text" name="">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">Primeira parcela</label>
                                 <input readonly="readonly" id="data_pri_parc"class="form-control" type="text" name="">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">Última Parcela</label>
                                 <input readonly="readonly" id="data_ult_parc"class="form-control" type="text" name="">
                              </div>
                           </div>
                        </div>
                     </div>
                     <?php if ($emprestimo) {?>
                        <div class="row">
                           <div class="col-md-12">
                              <div class="form-group">
                                 <div class="col-sm-2">
                                    <label class="control-label">Vendedor <span class="symbol required"></span></label>
                                    
                                    <!--<?php echo CHtml::dropDownList('AnaliseDeCredito[vendedor]','AnaliseDeCredito[vendedor]',
                                       CHtml::listData(Vendedor::model()->listVendedores(Yii::app()->session['usuario']->getFilial()->id),
                                       'id','nome' ), array('class'=>'form-control search-select select2', 'prompt'=>'Selecione:', 'required'=>true));
                                    ?>
                                    
                                    <input class="form-control" type="text" name="AnaliseDeCredito[vendedor]"  required="required">-->
                                    <input required="true" name="AnaliseDeCredito[vendedor]" type="hidden" id="selectVendedor" class="form-control search-select">
                                    <button type="submit" id="novo-vend" class="col-sm-12 btn btn-green" style="margin-top:5px;"><i class="fa fa-plus"> Cadastrar</i></button>
                                 </div>
                              </div>
                           </div>
                        </div>
                     <?php } ?>
                      <?php if(!$emprestimo) {?>
                        <div class="row">
                           <div class="col-md-12">
                              <div class="form-group">
                                 <div class="col-sm-2">
                                    <label class="control-label">Vendedor <span class="symbol required"></span></label>
                                    
                                    <!--<?php echo CHtml::dropDownList('AnaliseDeCredito[vendedor]','AnaliseDeCredito[vendedor]',
                                       CHtml::listData(Vendedor::model()->listVendedores(Yii::app()->session['usuario']->getFilial()->id),
                                       'id','nome' ), array('class'=>'form-control search-select select2', 'prompt'=>'Selecione:', 'required'=>true));
                                    ?>
                                    
                                    <input class="form-control" type="text" name="AnaliseDeCredito[vendedor]"  required="required">-->
                                    <input required="true" name="AnaliseDeCredito[vendedor]" type="hidden" id="selectVendedor" class="form-control search-select">
                                    <button type="submit" id="novo-vend" class="col-sm-12 btn btn-green" style="margin-top:5px;"><i class="fa fa-plus"> Cadastrar</i></button>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-2">
                                    <label class="control-label">Procedência da compra <span class="symbol required"></span></label>
                                    <?php echo CHtml::dropDownList('AnaliseDeCredito[Procedencia_compra_id]','AnaliseDeCredito[Procedencia_compra_id]',
                                       CHtml::listData(ProcedenciaCompra::model()->findAll(),
                                       'id','procedencia' ), array('class'=>'form-control search-select select2', 'prompt'=>'Selecione:', 'required'=>true));
                                    ?> 
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-2">
                                    <label class="control-label">Mercadoria retirada na hora <span class="symbol required"></span></label>
                                    <?php echo CHtml::dropDownList('AnaliseDeCredito[mercadoria_retirada_na_hora]','AnaliseDeCredito[mercadoria_retirada_na_hora]',
                                       CHtml::listData(TTable::model()->findAll(),
                                       'id','flag' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:','required'=>true)); 
                                    ?> 
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-2">
                                    <label class="control-label">É celular pré-pago <span class="symbol required"></span></label>
                                    <?php echo CHtml::dropDownList('AnaliseDeCredito[celular_pre_pago]','AnaliseDeCredito[celular_pre_pago]',
                                       CHtml::listData(TTable::model()->findAll(),
                                       'id','flag' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:','required'=>true)); 
                                    ?> 
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-4">
                                    <label class="control-label">Mercadoria <span class="symbol required"></span></label>
                                    <input required="required" class="form-control" type="text" name="AnaliseDeCredito[mercadoria]">
                                 </div>
                              </div>
                              <!--
                              <div class="form-group">
                                 <div class="col-sm-4">
                                    <label class="control-label">Alerta <span class="symbol required"></span></label>
                                    <?php echo CHtml::dropDownList('AnaliseDeCredito[alerta]','AnaliseDeCredito[alerta]',
                                       CHtml::listData(Alerta::model()->findAll(),
                                       'alerta','alerta' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:','required'=>true)); 
                                    ?> 
                                 </div>
                              </div>
                              -->
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-12">
                              <!--
                              <div class="form-group">
                                 <div class="col-sm-3">
                                    <label class="control-label">Mercadoria retirada na hora <span class="symbol required"></span></label>
                                    <?php echo CHtml::dropDownList('AnaliseDeCredito[mercadoria_retirada_na_hora]','AnaliseDeCredito[mercadoria_retirada_na_hora]',
                                       CHtml::listData(TTable::model()->findAll(),
                                       'id','flag' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:','required'=>true)); 
                                    ?> 
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-3">
                                    <label class="control-label">É celular pré-pago <span class="symbol required"></span></label>
                                    <?php echo CHtml::dropDownList('AnaliseDeCredito[celular_pre_pago]','AnaliseDeCredito[celular_pre_pago]',
                                       CHtml::listData(TTable::model()->findAll(),
                                       'id','flag' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:','required'=>true)); 
                                    ?> 
                                 </div>
                              </div>
                              -->



                           </div>
                        </div>
                        <br>
                        <div class="row">
                           <div class="col-md-12">
                              <div class="form-group">
                                 <div class="col-sm-12">
                                    <label class="control-label">Classificação do bem <span class="symbol required"></span></label>
                                    <?php echo CHtml::dropDownList('Bens','Bens',
                                       CHtml::listData(SubgrupoDeProduto::model()->findAll(),
                                       'id','descricao' ), array('class'=>'form-control search-select select2', 'multiple'=>'multiple', 'placeholder'=>'Selecione', 'required'=>'required')); 
                                    ?> 
                                 </div>
                              </div>
                           </div>
                        </div>
                      <?php } ?>
                     <br>

                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-12">
                                 <label class="control-label">Observação <span class="symbol required"></span></label>
                                 <textarea required="required" name="AnaliseDeCredito[observacao]" maxlength="300" id="form-field-23" class="form-control limited"></textarea>
                                 <input type="hidden" name="Proposta[qtd_parcelas]" id="Proposta_qtd_parcelas" value="0">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-9">
                              </div>
                              <div class="col-sm-3">
                                 <a href="#" id="btn-go-to-informacoes-basicas" class="btn btn-yellow btn-block">
                                 Avançar <i class="fa fa-arrow-circle-right"></i>
                                 </a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
               <div class="col-md-12">
                  
               </div>
            </div>
            <div class="tab-pane" id="tab4_dados_do_cliente">
               <!--Formulário inicial-->
               <div class="col-sm-12">
                  <form style="display:none" action="#" id="form-informacoes-basicas" data-tab-bind="li-dados-cliente">
                     <div class="row">
                        <div class="col-md-12" ></div>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-6">
                                 <label class="control-label">Nome completo : <span class="symbol required"></span></label>
                                 <input required name="Pessoa[nome]" type="text" placeholder="Nome completo" id="cliente_nome" class="form-control">
                                 <input name="CPF_antigo"   type="hidden" id="cpf_antigo" value="0">
                                 <input name="Cliente[id]"  type="hidden" id="cliente_id" value="0">
                                 <input name="ControllerAction" type="hidden" id="ControllerAction" value="">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">Sexo : <span class="symbol required"></span></label>
                                 <?php echo CHtml::dropDownList('Pessoa[sexo]','Pessoa[sexo]',
                                    CHtml::listData(Sexo::model()->findAll(),
                                    'sigla','sexo' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:','required'=>true)); 
                                    ?>  
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">Nascimento : <span class="symbol required"></span></label>
                                 <input required name="Pessoa[nascimento]" type="text" placeholder="Data de nascimento" id="nascimento" class="form-control dateBR">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">RG : <span class="symbol required"></span></label>
                                 <input required name="RG[numero]" type="text" placeholder="RG" id="rg" class="form-control">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">Órgão Emissor : <span class="symbol required"></span></label>
                                 <input required name="RG[orgao_emissor]" type="text" placeholder="Órgão emissor" id="RG_orgao_emissor" class="form-control">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">UF : <span class="symbol required"></span></label>
                                 <?php echo CHtml::dropDownList('RG[uf_emissor]','DocumentoCliente[uf_emissor]',
                                    CHtml::listData(Estados::model()->findAll(),
                                       'sigla','nome' ), array('class'=>'form-control search-select select2', 'prompt'=>'Selecione:','required'=>true)
                                    );
                                    ?>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">Data de Emissão : <span class="symbol required"></span></label>
                                 <input required name="RG[data_emissao]" type="text" placeholder="Data de emissão" id="RG_data_emissao" class="form-control dateBR">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-2">
                                 <label class="control-label">Nacionalidade : <span class="symbol required"></span></label>
                                 <select required="true" id="select-nacionalidade" name="Pessoa[nacionalidade]" class="draft_sent form-control search-select select2">
                                    <option value="">Selecione:</option>
                                    <option value="Brasileira">Brasileira</option>
                                    <option value="Estrangeira">Estrangeira</option>
                                 </select>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-2">
                                 <label class="control-label">UF : <span class="symbol required"></span></label>
                                 <?php echo CHtml::dropDownList('Pessoa[naturalidade]','Pessoa[naturalidade]',
                                    CHtml::listData(Estados::model()->findAll(),
                                    'sigla','nome' ), array('class'=>'form-control search-select select2', 'prompt'=>'Selecione:','required'=>true));
                                    ?>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">Cidade : <span class="symbol required"></span></label>
                                 <input required="true" name="Pessoa[naturalidade_cidade]" placeholder="Selecione" type="hidden" id="selectCidades" class="form-control search-select">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-2">
                                 <label class="control-label">Estado Civil : <span class="symbol required"></span></label>
                                 <?php echo CHtml::dropDownList('Pessoa[Estado_Civil_id]','Pessoa[Estado_Civil_id]',
                                    CHtml::listData(EstadoCivil::model()->findAll(),
                                    'id','estado' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:','required'=>true)); 
                                    ?>  
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">Cônjuge compõe renda?<span class="symbol required"></span></label>
                                 <select required="true" id="Cadastro_conjugue_compoe_renda" name="Cadastro[conjugue_compoe_renda]" class="draft_sent form-control search-select select2">
                                    <option value="">Selecione:</option>
                                    <?php foreach( TTable::model()->findAll() as $tt ) { ?>
                                    <option value="<?php echo $tt->id ?>"><?php echo $tt->flag ?></option>
                                    <?php } ?>
                                 </select>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">Nome da mãe : <span class="symbol required"></span></label>
                                 <input id="Cadastro_nome_da_mae" required name="Cadastro[nome_da_mae]" type="text" placeholder="Nome da mãe" class="form-control">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="">Nome do pai : </label>
                                 <input id="Cadastro_nome_do_pai" name="Cadastro[nome_do_pai]" type="text" placeholder="Nome do pai" class="form-control">
                                 <input name="DadosBancarios[numero]" type="hidden" value="0">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">Número de dependentes : <span class="symbol required"></span></label>
                                 <input id="Cadastro_numero_de_dependentes" required name="Cadastro[numero_de_dependentes]" type="text" placeholder="Número de dependentes" class="form-control number">
                              </div>
                           </div>
                           <div class="form-group">
                               <div <?php if($semear){echo "style='visibility:hidden;'";} ?>class="col-sm-3">
                                 <label class="control-label">Cliente da casa? : <span class="symbol required"></span></label>
                                 <select required="true" id="Cadastro_cliente_da_casa" name="Cadastro[cliente_da_casa]" class="form-control search-select select2">
                                    <option value="">Selecione:</option>
                                    <?php foreach( TTable::model()->findAll() as $tt ) { ?>
                                    <option value="<?php echo $tt->id ?>"><?php echo $tt->flag ?></option>
                                    <?php } ?>
                                 </select>
                              </div>
                           </div>
                        </div>
                     </div>

                     <?php if( $emprestimo ): ?>
                        <br>
                        <div class="row">
                           <div class="col-md-12">
                              <div class="form-group">
                                 <div class="col-sm-2">
                                    <label class="control-label">Banco: <span class="symbol required"></span></label>
                                    <select required="true" id="DadosBancarios_Banco_id" name="DadosBancarios[Banco_id]" class="form-control search-select select2">
                                       <?php foreach (Banco::model()->findAll('id NOT IN(6,10,8)') as $banco) { ?>
                                          <option value="<?= $banco->id ?>"><?= strtoupper($banco->nome) ?></option>
                                       <?php } ?>
                                    </select>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-2">
                                    <label class="control-label">Agência: <span class="symbol required"></span></label>
                                    <input id="conta_agencia" required name="DadosBancarios[agencia]" type="text" placeholder="Agência" class="form-control">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-2">
                                    <label class="control-label">Conta: <span class="symbol required"></span></label>
                                    <input id="conta_numero" required name="DadosBancarios[numero]" type="text" placeholder="Número da Conta" class="form-control">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-3">
                                    <label class="control-label">Operação: <span class="symbol"></span></label>
                                    <input id="conta_operacao"  name="DadosBancarios[operacao]" type="text" placeholder="Operação" class="form-control">
                                    <!--<input id="dados_bancarios_id"  name="DadosBancariosId" type="hidden"   class="form-control">-->
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-3">
                                    <label class="control-label">Tipo de conta: <span class="symbol required"></span></label>
                                    <select id="DadosBancarios_Tipo_Conta_Bancaria_id" name="DadosBancarios[Tipo_Conta_Bancaria_id]" class="form-control search-select select2 required">
                                       <?php foreach (TipoContaBancaria::model()->findAll() as $TipoContaBancaria) { ?>
                                          <option value="<?= $TipoContaBancaria->id ?>"><?= strtoupper($TipoContaBancaria->tipo) ?></option>
                                       <?php } ?>
                                    </select>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <br>
                     <?php endif; ?>

                     <div class="row">
                        <p></p>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-9" style="padding-left:0!important;">
                                 <div class="col-sm-3">
                                    <a data-go-to-step="#tab4_detalhes_proposta" class="btn btn-yellow btn-block btn-step-back">
                                    <i class="fa fa-arrow-circle-left"></i> Voltar  </a>
                                 </div>
                              </div>
                              <div class="col-sm-3">
                                 <button id="btn-submit-informacoes-basicas" class="btn btn-yellow btn-block" type="submit">
                                 Avançar <i class="fa fa-arrow-circle-right"></i>
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
            <div class="tab-pane" id="tab4_endereco">
               <!--Formulário de endereço-->
               <div class="col-md-12">
                  <form style="display:none" action="#" id="form-endereco">
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-2">
                                 <div class="form-group">
                                    <label class="control-label">CEP <span class="symbol required"></span></label>
                                    <input required class="form-control cep cepmask" type="text" name="EnderecoCliente[cep]" id="cep_cliente">
                                    <input name="ControllerAction" type="hidden" id="ControllerEnderecoAction" value="new">
                                    <input name="EnderecoId" type="hidden" id="endereco_id">
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <div class="form-group">
                                    <label class="control-label">Logradouro <span class="symbol required"></span></label>
                                    <input required class="form-control" type="text" name="EnderecoCliente[logradouro]" id="logradouro_cliente">
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <div class="form-group">
                                    <label class="control-label">Cidade <span class="symbol required"></span></label>
                                    <input required class="form-control" type="text" name="EnderecoCliente[cidade]" id="cidade_cliente">
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-2">
                                 <div class="form-group">
                                    <label class="control-label">Bairro <span class="symbol required"></span></label>
                                    <input required class="form-control" type="text" name="EnderecoCliente[bairro]" id="bairro_cliente">
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-2">
                                 <div class="form-group">
                                    <label class="control-label">Número <span class="symbol required"></span></label>
                                    <input required id="numero_end_cliente" class="form-control" type="text" name="EnderecoCliente[numero]">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <div class="form-group">
                                    <label class="control-label">Tipo de moradia <span class="symbol required"></span></label>
                                    <select required id="EnderecoCliente_Tipo_Moradia_id" name="EnderecoCliente[Tipo_Moradia_id]" class="form-control search-select select2">
                                       <option value="">Selecione:</option>
                                       <?php  foreach( TipoMoradia::model()->findAll('id = 3') as $tipoMoradia ){ ?>
                                          <option value="<?php echo $tipoMoradia->id ?>"><?php echo $tipoMoradia->tipo ?></option>
                                       <?php } ?>
                                    </select>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <div class="form-group">
                                    <label class="control-label">Complemento</label>
                                    <input id="complemento_end_cliente" class="form-control" type="text" name="EnderecoCliente[complemento]">
                                 </div>
                              </div>
                           </div>
                           <div class="form-group">
                              <input name="EnderecoCliente[Tipo_Endereco_id]" class="form-control" type="hidden" value="1">
                              <div class="col-sm-3">
                                 <label class="control-label">UF <span class="symbol required"></span></label>
                                 <select required id="EnderecoCliente_uf" name="EnderecoCliente[uf]" class="form-control search-select select2">
                                    <option value="">Selecione:</option>
                                    <?php foreach( Estados::model()->findAll() as $uf ) { ?>
                                       <option value="<?php echo $uf->sigla ?>"><?php echo $uf->sigla ?></option>
                                    <?php } ?>
                                 </select>
                              </div>
                           </div>
                           
                        </div>
                     </div>

                     <div class="row">
                        <p></p>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-9" style="padding-left:0!important;">
                                 <div class="col-sm-3">
                                    <a data-go-to-step="#tab4_dados_do_cliente" class="btn btn-yellow btn-block btn-step-back">
                                    <i class="fa fa-arrow-circle-left"></i> Voltar  </a>
                                 </div>
                              </div>
                              <div class="col-sm-3">
                                 <button class="btn btn-yellow btn-block" type="submit">
                                    Avançar <i class="fa fa-arrow-circle-right"></i>
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>

                  </form>
               </div>
            </div>
            <div class="tab-pane" id="tab4_contato">
               <!--Formulário de contato-->
               <div class="col-md-12">
                  <form style="display:none" action="#" id="form-contato">
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-4">
                                 <label class="control-label">N° do telefone <span class="symbol required"></span></label>
                                 <input required class="form-control telefone number" type="text" name="TelefoneCliente[numero]" id="telefone1_numero">
                                 <input name="ControllerAction" type="hidden" id="ControllerTelefoneAction" value="new">
                                 <input name="telefone1_id" type="hidden" id="telefone1_id" value="0">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-4">
                                 <label class="control-label">Tipo de telefone <span class="symbol required"></span></label>
                                 <?php echo CHtml::dropDownList('TelefoneCliente[Tipo_Telefone_id]','TelefoneCliente[Tipo_Telefone_id]',
                                    CHtml::listData(TipoTelefone::model()->findAll(),
                                    'id','tipo' ), array('class'=>'form-control search-select select2','prompt' => 'Selecione:','required' => true, 'id'=>'Tipo_Telefone1_id' )); 
                                 ?>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-4">
                                 <label class="control-label">Ramal </label>
                                 <input class="form-control" type="text" name="TelefoneCliente[ramal]" id="telefone1_ramal">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-4">
                                 <label class="control-label">N° do telefone </label>
                                 <input required class="form-control telefone number" type="text" name="TelefoneCliente2[numero]" id="telefone2_numero">
                                 <input name="telefone2_id" type="hidden" id="telefone2_id"  value="0">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-4">
                                 <label class="control-label">Tipo de telefone</label>
                                 <?php echo CHtml::dropDownList('TelefoneCliente2[Tipo_Telefone_id]','TelefoneCliente2[Tipo_Telefone_id]',
                                    CHtml::listData(TipoTelefone::model()->findAll(),
                                    'id','tipo' ), array('class'=>'form-control search-select select2','prompt' => 'Selecione:', 'required' => true, 'id' => 'Tipo_Telefone2_id' )); 
                                 ?>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-4">
                                 <label class="control-label">Ramal </label>
                                 <input class="form-control" type="text" name="TelefoneCliente2[ramal]" id="telefone2_ramal">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-4">
                                 <label class="control-label">N° do telefone </label>
                                 <input class="form-control telefone number" type="text" name="TelefoneCliente3[numero]" id="telefone3_numero">
                                 <input name="telefone3_id" type="hidden" id="telefone3_id"  value="0">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-4">
                                 <label class="control-label">Tipo de telefone</label>
                                 <?php echo CHtml::dropDownList('TelefoneCliente3[Tipo_Telefone_id]','TelefoneCliente3[Tipo_Telefone_id]',
                                    CHtml::listData(TipoTelefone::model()->findAll(),
                                    'id','tipo' ), array('class'=>'form-control search-select select2','prompt' => 'Selecione:', 'id' => 'Tipo_Telefone3_id' ));
                                 ?>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-4">
                                 <label class="control-label">Ramal </label>
                                 <input class="form-control" type="text" name="TelefoneCliente3[ramal]" id="telefone3_ramal">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <p></p>
                     </div>

                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-9" style="padding-left:0!important;">
                                 <div class="col-sm-3">
                                    <a data-go-to-step="#tab4_endereco" class="btn btn-yellow btn-block btn-step-back">
                                    <i class="fa fa-arrow-circle-left"></i> Voltar  </a>
                                 </div>
                              </div>
                              <div class="col-sm-3">
                                 <button class="btn btn-yellow btn-block" type="submit">
                                    Avançar <i class="fa fa-arrow-circle-right"></i>
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
            <div class="tab-pane" id="tab4_dados_profissionais">
               <div class="col-md-12">
                  <form style="display:none" action="#" id="form-dados-profissionais">
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-2">
                                 <label class="control-label">CNPJ/CPF:</label>
                                 <input placeholder="CPF / CNPJ" id="dp_cnpj_cpf" class="form-control" type="text" name="DadosProfissionais[cnpj_cpf]">
                                 <input name="ControllerAction" type="hidden" id="ControllerDPAction" value="new">
                                 <input name="dadosProfissionaisId" type="hidden" id="dados_profissionais_id">
                              </div>
                           </div>

                           <!--
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">Classe Profissional : <span class="symbol required"></span></label>
                                 <?php echo CHtml::dropDownList('DadosProfissionais[Classe_Profissional_id]','DadosProfissionais[Classe_Profissional_id]',
                                    CHtml::listData(ClasseProfissional::model()->findAll(),
                                    'id','classe' ), array('class'=>'form-control search-select select2', 'prompt'=>'Selecione:', 'required'=>true)); 
                                 ?>
                              </div>
                           </div>
                           -->
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">Empresa <span class="symbol required"></span></label>
                                 <input required="required" placeholder="Empresa" id="dp_empresa" class="form-control" type="text" name="DadosProfissionais[empresa]">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-4">
                                 <label class="control-label">Profissão</label>
                                 <?php echo CHtml::dropDownList('DadosProfissionais[profissao]','DadosProfissionais[profissao]',
                                    CHtml::listData(Profissao::model()->findAll('id=723'),
                                    'profissao','profissao' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:')); 
                                 ?> 
                              </div>
                           </div>
                           <!--
                           <div class="form-group">
                              <div class="col-sm-2">
                                 <label class="control-label">Ocupação atual <span class="symbol required"></span></label>
                                 <?php echo CHtml::dropDownList('DadosProfissionais[Tipo_Ocupacao_id]','DadosProfissionais[Tipo_Ocupacao_id]',
                                    CHtml::listData(TipoOcupacao::model()->findAll(),
                                    'id','tipo' ), array('class'=>'form-control search-select select2', 'prompt'=>'Selecione:', 'required'=>true)); 
                                 ?>
                              </div>
                           </div>
                           -->
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">Renda Líquida <span class="symbol required"></span></label>
                                 <input id="dp_renda_liquida" required class="form-control dinheiro currency" type="text" name="DadosProfissionais[renda_liquida]">
                                 <!--<input placeholder="Data de Admissão" required="required" id="dp_data_admissao" class="form-control dateBR" type="text" name="DadosProfissionais[data_admissao]">-->
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="row">
                        <div class="col-md-12">
                           <!--
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">Profissão</label>
                                 <?php echo CHtml::dropDownList('DadosProfissionais[profissao]','DadosProfissionais[profissao]',
                                    CHtml::listData(Profissao::model()->findAll('id=723'),
                                    'profissao','profissao' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:')); 
                                 ?> 
                              </div>
                           </div>
                           -->
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">Meses de Serviço <span class="symbol required"></span></label>
                                 <input min="0" id="dp_meses_de_servico" required class="form-control" type="number" name="DadosProfissionais[tempo_de_servico_meses]">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">Anos de Serviço <span class="symbol required"></span></label>
                                 <input min="0" id="dp_anos_de_servico" required class="form-control" type="number" name="DadosProfissionais[tempo_de_servico_anos]">
                                 <!--<input style="width:13%" class="form-control" type="number" name="DadosProfissionais[tempo_de_servico_meses]">
                                 <input style="width:13%" class="form-control" type="number" name="DadosProfissionais[tempo_de_servico_anos]">-->
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">Órgão Pagador</label>
                                 <?php echo CHtml::dropDownList('DadosProfissionais[orgao_pagador]','DadosProfissionais[orgao_pagador]',
                                    CHtml::listData(OrgaoPagador::model()->findAll(),
                                    'nome','nome' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:')); 
                                 ?>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">N° do benefício</label>
                                 <input id="dp_numero_do_beneficio" class="form-control" type="text" name="DadosProfissionais[numero_do_beneficio]">
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="row">
                         <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">Tipo de comprovante</label>
                                 <?php echo CHtml::dropDownList('DadosProfissionais[tipo_de_comprovante]','DadosProfissionais[tipo_de_comprovante]',
                                    CHtml::listData(TipoDeComprovante::model()->findAll(),
                                    'tipo','tipo' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:')); 
                                 ?>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-2">
                                 <label class="control-label">CEP</label>
                                 <input id="cep_dados_profissionais" class="form-control cep" type="text" name="EnderecoProfiss[cep]">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-5">
                                 <label class="control-label">Logradouro <span class="symbol required"></span></label>
                                 <input required="required" id="logradouro_dados_profissionais" class="form-control" type="text" name="EnderecoProfiss[logradouro]">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-2">
                                 <label class="control-label">Número <span class="symbol required"></span></label>
                                 <input required="required" id="numero_endereco_dados_profissionais"  class="form-control" type="text" name="EnderecoProfiss[numero]">
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="row">
                         <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-md-3">
                                 <label class="control-label">Cidade <span class="symbol required"></span></label>
                                 <input required="required" id="cidade_dados_profissionais" class="form-control" type="text" name="EnderecoProfiss[cidade]">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-md-3">
                                 <label class="control-label">Bairro <span class="symbol required"></span></label>
                                 <input required="required" id="bairro_dados_profissionais" class="form-control" type="text" name="EnderecoProfiss[bairro]">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-md-3">
                                 <label class="control-label">UF <span class="symbol required"></span></label>
                                 <?php echo CHtml::dropDownList('EnderecoProfiss[uf]','EnderecoProfiss[uf]',
                                    CHtml::listData(Estados::model()->findAll(),
                                    'sigla','nome' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:', 'required' => true)
                                 );?>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-md-3">
                                 <label class="control-label">Complemento</label>
                                 <input id="dp_endereco_complemento" class="form-control" type="text" name="EnderecoProfiss[complemento]">
                              </div>
                           </div>
                         </div>
                     </div>

                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-md-3">
                                 <label class="control-label">Telefone <span class="symbol required"></span></label>
                                 <input id="dp_telefone_numero" class="form-control telefone" type="text" name="TelefoneProfiss[numero]" required="required">
                                 <input id="dp_telefone_id" class="form-control" type="hidden" name="TelefoneProfiss[id]">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-md-2">
                                 <label class="control-label">Ramal</label>
                                 <input id="dp_telefone_ramal" class="form-control" type="text" name="TelefoneProfiss[ramal]">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-md-3">
                                 <label class="control-label">Tipo de telefone <span class="symbol required"></span></label>
                                 <?php echo CHtml::dropDownList('TelefoneProfiss[Tipo_Telefone_id]','TelefoneProfiss[Tipo_Telefone_id]',
                                    CHtml::listData(TipoTelefone::model()->findAll(),
                                    'id','tipo' ), array('class'=>'form-control search-select select2', 'prompt'=>'Selecione:', 'required' => true));
                                 ?>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="row">
                        <p></p>
                     </div>

                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-9" style="padding-left:0!important;">
                                 <div class="col-sm-3">
                                    <a data-go-to-step="#tab4_contato" class="btn btn-yellow btn-block btn-step-back">
                                    <i class="fa fa-arrow-circle-left"></i> Voltar  </a>
                                 </div>
                              </div>
                              <div class="col-sm-3">
                                 <button class="btn btn-yellow btn-block" type="submit">
                                    Avançar <i class="fa fa-arrow-circle-right"></i>
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
            <div class="tab-pane" id="tab4_dados_conjuge">
               <!--Formulário Conjuge-->
               <div class="col-sm-12">
                     <form style="display:none" action="#" id="form-conjuge">
                        
                        <div class="row">
                           <div class="col-md-12">
                              <div class="form-group">
                                 <div class="col-sm-3">
                                    <label class="control-label">CPF : <span class="symbol required"></span></label>
                                    <input required name="CPFConjuge[numero]" type="text" placeholder="CPF do Cônjuge" id="cpf_conjuge" class="form-control cpf">
                                    <input type="hidden" id="cpf_conjuge_hidden">
                                    <input name="ControllerAction" type="hidden" id="ControllerConjugeAction" value="new">
                                    <input name="Conjuge_id" type="hidden" id="Conjuge_id">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-4">
                                    <label class="control-label">Nome completo : <span class="symbol required"></span></label>
                                    <input required name="Conjuge[nome]" type="text" placeholder="Nome completo Cônjuge" id="conjuge_nome" class="form-control">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-3">
                                    <label class="control-label">Sexo : <span class="symbol required"></span></label>
                                    <?php echo CHtml::dropDownList('Conjuge[sexo]','Conjuge[sexo]',
                                       CHtml::listData(Sexo::model()->findAll(),
                                       'sigla','sexo' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:','required'=>true)); 
                                    ?>  
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-2">
                                    <label class="control-label">Nascimento : <span class="symbol required"></span></label>
                                    <input required name="Conjuge[nascimento]" type="text" placeholder="Data de nascimento" id="conjuge_nascimento" class="form-control dateBR">
                                 </div>
                              </div>
                             
                           </div>
                        </div>

                        <div class="row">
                           <div class="col-md-12">
                              <div class="form-group">
                                 <div class="col-sm-3">
                                    <label class="control-label">RG : <span class="symbol required"></span></label>
                                    <input required name="RGConjuge[numero]" type="text" placeholder="RG" id="rg_conjuge" class="form-control">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-3">
                                    <label class="control-label">Órgão Emissor : <span class="symbol required"></span></label>
                                    <input required name="RGConjuge[orgao_emissor]" type="text" placeholder="Órgão emissor" id="RG_conjuge_orgao_emissor" class="form-control">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-3">
                                    <label class="control-label">UF : <span class="symbol required"></span></label>
                                    <?php echo CHtml::dropDownList('RGConjuge[uf_emissor]','RGConjuge[uf_emissor]',
                                       CHtml::listData(Estados::model()->findAll(),
                                          'sigla','nome' ), array('class'=>'form-control search-select select2', 'prompt'=>'Selecione:','required'=>true)
                                       );
                                    ?>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-3">
                                    <label class="control-label">Data de Emissão : <span class="symbol required"></span></label>
                                    <input required name="RGConjuge[data_emissao]" type="text" placeholder="Data de emissão" id="RG_conjuge_data_emissao" class="form-control dateBR">
                                 </div>
                              </div>
                           </div>
                        </div>
                        
                        <div class="row">
                           <div class="col-md-12">
                              
                              <div class="form-group">
                                 <div class="col-sm-2">
                                    <label class="control-label">Nacionalidade : <span class="symbol required"></span></label>
                                    <select required="true" id="select-nacionalidade-conjuge" name="Conjuge[nacionalidade]" class="draft_sent form-control search-select select2">
                                       <option value="">Selecione:</option>
                                       <option value="Brasileira">Brasileira</option>
                                       <option value="Estrangeira">Estrangeira</option>
                                    </select>
                                 </div>
                              </div>

                              <div class="form-group">
                                 <div class="col-sm-2">
                                    <label class="control-label">UF : <span class="symbol required"></span></label>
                                    <?php echo CHtml::dropDownList('Conjuge[naturalidade]','Conjuge[naturalidade]',
                                          CHtml::listData(Estados::model()->findAll(),
                                          'sigla','nome' ), array('class'=>'form-control search-select select2', 'prompt'=>'Selecione:','required'=>true));
                                    ?>
                                 </div>
                              </div>

                              <div class="form-group">
                                 <div class="col-sm-2">
                                    <label class="control-label">Cidade : <span class="symbol required"></span></label>
                                    <input required="true" name="Conjuge[naturalidade_cidade]" placeholder="Selecione" type="hidden" id="selectCidades2" class="form-control search-select">
                                 </div>
                              </div>

                              <div class="form-group">
                                 <div class="col-sm-2">
                                    <label class="control-label">Estado Civil : <span class="symbol required"></span></label>
                                    <?php echo CHtml::dropDownList('Conjuge[Estado_Civil_id]','Conjuge[Estado_Civil_id]',
                                       CHtml::listData(EstadoCivil::model()->findAll(),
                                       'id','estado' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:','required'=>true)); 
                                    ?>  
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-4">
                                    <label class="control-label">Nome da Mãe : <span class="symbol required"></span></label>
                                    <input required name="CadastroConjuge[nome_da_mae]" type="text" placeholder="Nome da Mãe" id="Cadastro_conjuge_nome_da_mae" class="form-control">
                                 </div>
                              </div>
                           
                           </div>
                        </div>

                         <div class="row">
                           <div class="col-md-12">
                              <div class="form-group">
                                 <div class="col-sm-12">
                                    <p></p>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="row">
                           <div class="col-md-12">
                              <div class="form-group">
                                 <div class="col-sm-12">
                                    <h4 style="color:#666666;font-family: 'Raleway', sans-serif;font-weight:200">Dados Profissionais do Cônjuge</h4>
                                    <hr>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="row">
                           <div class="col-md-12">
                              <div class="form-group">
                                 <div class="col-sm-2">
                                    <label class="control-label">CNPJ/CPF: </label>
                                    <input placeholder="CPF / CNPJ" id="dp_conjuge_cnpj_cpf" class="form-control" type="text" name="DadosProfissionaisConjuge[cnpj_cpf]">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-3">
                                    <label class="control-label">Classe Profissional : <span class="symbol required"></span></label>
                                    <?php echo CHtml::dropDownList('DadosProfissionaisConjuge[Classe_Profissional_id]','DadosProfissionaisConjuge[Classe_Profissional_id]',
                                       CHtml::listData(ClasseProfissional::model()->findAll(),
                                       'id','classe' ), array('class'=>'form-control search-select select2', 'prompt'=>'Selecione:', 'required'=>true)); 
                                    ?>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-3">
                                    <label class="control-label">Empresa</label>
                                    <input placeholder="Empresa" id="dp_conj_empresa" class="form-control" type="text" name="DadosProfissionaisConjuge[empresa]">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-2">
                                    <label class="control-label">Ocupação atual</label>
                                    <?php echo CHtml::dropDownList('DadosProfissionaisConjuge[Tipo_Ocupacao_id]','DadosProfissionaisConjuge[Tipo_Ocupacao_id]',
                                       CHtml::listData(TipoOcupacao::model()->findAll(),
                                       'id','tipo' ), array('class'=>'form-control search-select select2', 'prompt'=>'Selecione:', 'required'=>true)); 
                                    ?>
                                 </div>
                              </div>
                              
                              <!--<div class="form-group">
                                 <div class="col-sm-2">
                                    <label class="control-label">Data de admissão</label>
                                    <input placeholder="Data de Admissão" required="required" id="dp_con_data_admissao" class="form-control dateBR" type="text" name="DadosProfissionaisConjuge[data_admissao]">
                                 </div>
                              </div>-->
                              
                              <div class="form-group">
                                 <div class="col-sm-2">
                                    <label class="control-label">Renda Líquida R$</label>
                                    <input id="dp_conj_renda_liquida" required class="form-control dinheiro currency" type="text" name="DadosProfissionaisConjuge[renda_liquida]">
                                 </div>
                              </div>

                           </div>
                        </div>

                        <div class="row">
                           <div class="col-md-12">
                              <div class="form-group">
                                 <div class="col-sm-3">
                                    <label class="control-label">Profissão</label>
                                    <?php echo CHtml::dropDownList('DadosProfissionaisConjuge[profissao]','DadosProfissionaisConjuge[profissao]',
                                       CHtml::listData(Profissao::model()->findAll('id = 723'),
                                       'profissao','profissao' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:')); 
                                    ?> 
                                 </div>
                              </div>

                              <div class="form-group">
                                 <div class="col-sm-2">
                                    <label class="control-label">Meses de Serviço <span class="symbol required"></span></label>
                                    <input id="dp_conj_meses_de_servico" required class="form-control" type="number" name="DadosProfissionaisConjuge[tempo_de_servico_meses]">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-2">
                                    <label class="control-label">Anos de Serviço <span class="symbol required"></span></label>
                                    <input id="dp_conj_anos_de_servico" required class="form-control" type="number" name="DadosProfissionaisConjuge[tempo_de_servico_anos]">
                                 </div>
                              </div>
                              
                              <!--
                              <div class="form-group">
                                 <div class="col-sm-2">
                                    <label class="control-label">Mês / Ano Renda</label>
                                    <input id="dp_conj_mes_ano_renda" class="form-control mes_ano_renda" type="text" name="DadosProfissionaisConjuge[mes_ano_renda]">
                                 </div>
                              </div>
                              -->
                              <div class="form-group">
                                 <div class="col-sm-2">
                                    <label class="control-label">Órgão Pagador</label>
                                    <?php echo CHtml::dropDownList('DadosProfissionaisConjuge[orgao_pagador]','DadosProfissionaisConjuge[orgao_pagador]',
                                       CHtml::listData(OrgaoPagador::model()->findAll(),
                                       'nome','nome' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:')); 
                                    ?>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-3">
                                    <label class="control-label">N° do benefício</label>
                                    <input id="dp_conj_numero_do_beneficio" class="form-control" type="text" name="DadosProfissionaisConjuge[numero_do_beneficio]">
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                 <div class="col-sm-3">
                                    <label class="control-label">Tipo de comprovante</label>
                                    <?php echo CHtml::dropDownList('DadosProfissionaisConjuge[TipoDeComprovante]','DadosProfissionaisConjuge[TipoDeComprovante]',
                                       CHtml::listData(TipoDeComprovante::model()->findAll(),
                                       'tipo','tipo' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:')); 
                                    ?>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-2">
                                    <label class="control-label">CEP</label>
                                    <input required="required" id="cep_dados_profissionais_conjuge" class="form-control cep" type="text" name="EnderecoProfissConjuge[cep]">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-5">
                                    <label class="control-label">Logradouro</label>
                                    <input required="required" id="logradouro_dados_profissionais_conjuge" class="form-control" type="text" name="EnderecoProfissConjuge[logradouro]">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-sm-2">
                                    <label class="control-label">Número</label>
                                    <input required="required" id="numero_endereco_dados_profissionais_conjuge"  class="form-control" type="text" name="EnderecoProfissConjuge[numero]">
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                 <div class="col-md-3">
                                    <label class="control-label">Cidade</label>
                                    <input required="required" id="cidade_dados_profissionais_conjuge" class="form-control" type="text" name="EnderecoProfissConjuge[cidade]">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-md-3">
                                    <label class="control-label">Bairro</label>
                                    <input required="required" id="bairro_dados_profissionais_conjuge" class="form-control" type="text" name="EnderecoProfissConjuge[bairro]">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-md-3">
                                    <label class="control-label">UF</label>
                                    <?php echo CHtml::dropDownList('EnderecoProfissConjuge[uf]','EnderecoProfissConjuge[uf]',
                                       CHtml::listData(Estados::model()->findAll(),
                                       'sigla','nome' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:', 'required' => true)
                                    );?>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-md-3">
                                    <label class="control-label">Complemento</label>
                                    <input id="dp_conj_endereco_complemento" class="form-control" type="text" name="EnderecoProfissConjuge[complemento]">
                                 </div>
                              </div>
                            </div>
                        </div>

                        <div class="row">
                           <div class="col-md-12">
                              <div class="form-group">
                                 <div class="col-md-4">
                                    <label class="control-label">Telefone</label>
                                    <input id="dp_conj_telefone_numero" class="form-control telefone" type="text" name="TelefoneProfissConjuge[numero]" required="required">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-md-4">
                                    <label class="control-label">Ramal</label>
                                    <input id="dp_conj_telefone_ramal" class="form-control" type="text" name="TelefoneProfissConjuge[ramal]">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-md-4">
                                    <label class="control-label">Tipo de telefone</label>
                                    <?php echo CHtml::dropDownList('TelefoneProfissConjuge[Tipo_Telefone_id]','TelefoneProfissConjuge[Tipo_Telefone_id]',
                                       CHtml::listData(TipoTelefone::model()->findAll(),
                                       'id','tipo' ), array('class'=>'form-control search-select select2', 'prompt'=>'Selecione:', 'required' => true));
                                    ?>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="row">
                           <p></p>
                        </div>
                        
                        <div class="row">
                           <div class="col-md-12">
                              <div class="form-group">
                                 <div class="col-sm-9" style="padding-left:0!important;">
                                    <div class="col-sm-3">
                                       <a data-go-to-step="#tab4_dados_profissionais" class="btn btn-yellow btn-block btn-step-back">
                                       <i class="fa fa-arrow-circle-left"></i> Voltar  </a>
                                    </div>
                                 </div>
                                 <div class="col-sm-3">
                                    <button disabled="disabled" class="btn btn-yellow btn-block" type="submit" id="btn-submit-dados-conjuge">
                                       Avançar <i class="fa fa-arrow-circle-right"></i>
                                    </button>
                                 </div>
                              </div>
                           </div>
                        </div>

                     </form>
               </div>
            </div>
            <div style="" class="tab-pane" id="tab4_referencias">
               <!--Formulário de referencias-->
               <div class="col-md-12">
                  <form style="display:none" action="#" id="form-referencia">
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-2">
                                 <label class="control-label">Parentesco <span class="symbol required"></span></label>
                                 <input id="referencia1_parentesco" required class="form-control" type="text" name="Referencia[parentesco]">
                                 <input name="ControllerAction" type="hidden" id="ControllerReferenciaAction" value="new">
                                 <input name="ref1Id" type="hidden" id="ref1Id" value="0">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-4">
                                 <label class="control-label">Nome <span class="symbol required"></span></label>
                                 <input id="referencia1_nome" required class="form-control" type="text" name="Referencia[nome]">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-4">
                                 <label class="control-label">N° do telefone <span class="symbol required"></span></label>
                                 <input id="telefone_referencia1_numero" required class="telefone form-control number" type="text" name="TelefoneReferencia[numero]">
                                 <input name="t1_id" type="hidden" id="t1_id" value="0">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-2">
                                 <label class="control-label">Tipo <span class="symbol required"></span></label>
                                 <?php echo CHtml::dropDownList('Referencia[Tipo_Referencia_id]','Referencia1_Tipo_Referencia_id',
                                    CHtml::listData(TipoReferencia::model()->findAll(),'id','tipo'),
                                    array('class'=>'form-control search-select select2','prompt'=>'Selecione:','required'=>true, 'id'=>'Referencia1_Tipo_Referencia_id'));
                                 ?>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-2">
                                 <label class="control-label">Parentesco </label>
                                 <input id="referencia2_parentesco"  class="form-control" type="text" name="Referencia2[parentesco]">
                                 <input name="ref2Id" type="hidden" id="ref2Id" value="0">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-4">
                                 <label class="control-label">Nome </label>
                                 <input id="referencia2_nome"  class="form-control" type="text" name="Referencia2[nome]">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-4">
                                 <label class="control-label">N° do telefone </label>
                                 <input id="telefone_referencia2_numero" class="telefone form-control number" type="text" name="TelefoneReferencia2[numero]">
                                 <input name="t2_id" type="hidden" id="t2_id" value="0">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-2">
                                 <label class="control-label">Tipo </label>
                                 <?php echo CHtml::dropDownList('Referencia2[Tipo_Referencia_id]','Referencia2[Tipo_Referencia_id]',
                                    CHtml::listData(TipoReferencia::model()->findAll(),'id','tipo'),
                                    array('class'=>'form-control search-select select2','prompt'=>'Selecione:'));
                                 ?>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-2">
                                 <label class="control-label">Parentesco </label>
                                 <input id="referencia3_parentesco"  class="form-control" type="text" name="Referencia3[parentesco]">
                                 <input name="ref3Id" type="hidden" id="ref3Id" value="0">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-4">
                                 <label class="control-label">Nome </label>
                                 <input id="referencia3_nome"  class="form-control" type="text" name="Referencia3[nome]">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-4">
                                 <label class="control-label">N° do telefone </label>
                                 <input id="telefone_referencia3_numero" class="telefone form-control number" type="text" name="TelefoneReferencia3[numero]">
                                 <input name="t3_id" type="hidden" id="t3_id" value="0">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-2">
                                 <label class="control-label">Tipo </label>
                                 <?php echo CHtml::dropDownList('Referencia3[Tipo_Referencia_id]','Referencia3[Tipo_Referencia_id]',
                                    CHtml::listData(TipoReferencia::model()->findAll(),'id','tipo'),
                                    array('class'=>'form-control search-select select2','prompt'=>'Selecione:'));
                                 ?>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <p></p>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-9" style="padding-left:0!important;">
                                 <div class="col-sm-3">
                                    <a data-go-to-step="#tab4_dados_conjuge" class="btn btn-yellow btn-block btn-step-back">
                                    <i class="fa fa-arrow-circle-left"></i> Voltar  </a>
                                 </div>
                              </div>
                              <div class="col-sm-3">
                                 <button class="btn btn-yellow btn-block" type="submit">
                                    Salvar <i class="fa fa-arrow-circle-right"></i>
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
            <div style="" class="tab-pane" id="tab4_anexos">
               <!--Formulário de anexos-->
               <!--<div class="col-md-3"></div>-->
               <div class="col-md-12" style="/*margin:0 auto!important;margin:10% 20%!important;position:absolute*/">
                  <div class="row">
                     <table id="grid_anexos" class="table table-striped table-bordered table-hover table-full-width dataTable">
                        <thead>
                           <tr role="row">
                              <th>Descrição</th>
                              <th>Extensão</th>
                              <th>Data de envio</th>
                              <th></th>
                           </tr>
                        </thead>
                     </table>
                  </div>   
                  <form action="/anexos/add/" method="POST" enctype="multipart/form-data" style="display:none" id="form-anexos">
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-4">
                                 <label class="control-label">Arquivo <span class="symbol required"></span></label>
                                 <input data-icon="false" class="control-label filestyle" required="required" name="FileInput" id="FileInput" type="file" />
                                 <input type="hidden" name="Cliente_id" id="Cliente_id_fup" value="0">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-4">
                                 <label class="control-label">Descrição <span class="symbol required"></span></label>
                                 <input required class="form-control" type="text" name="Anexo[descricao]">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div id="progressbox">
                              <div id="progressbar"></div >
                              <div id="statustxt">0%</div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-2">
                                 <button type="submit" class="btn btn-blue">Enviar Arquivo</button>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-9" style="padding-left:0!important;">
                                 <div class="col-sm-3">
                                    <a data-go-to-step="#tab4_referencias" class="btn btn-yellow btn-block btn-step-back">
                                    <i class="fa fa-arrow-circle-left"></i> Voltar  </a>
                                 </div>
                              </div>
                              <div class="col-sm-3">
                              </div>
                           </div>
                        </div>
                     </div>
                  </form>
                  <!--<button id="btn-show-modal-photo" class="btn btn-teal btn-block">
                        <i class="fa fa-camera"></i>
                  </button>-->
               </div>
               <!--<div class="col-md-3"></div>-->
            </div>
         </div>
      </div>
   </div>
</div>

<div class="row" style="margin-bottom:40px;">
   <div class="col-md-3"></div>
   <div class="col-md-6">
      <div class="form-group">
         <button id="btn-enviar-proposta" class="btn btn-yellow btn-block">
            Enviar Proposta <i class="fa fa-arrow-circle-right"></i>
         </button>
      </div> 
   </div>
   <div class="col-md-3">        
   </div>
</div>

<div data-width="80%" id="cadastro-vend" class="modal fade">
   <div class="modal-body" style="background:#dff0d8!important">
      <h4 style="color:#5cb85c;text-align:center"> Informe os dados do vendedor </h4>
         <form id="form-vendedor">
            <div class="form-group col-xs-6">
               <label for="cpf-vend">CPF</label>
               <input type="text" class="form-control cpf" id="cpf-vend" placeholder="CPF">
            </div>
            <div class="form-group col-xs-6">
               <label for="nome-vend">Nome</label>
               <input type="text" class="form-control" id="nome-vend" placeholder="Nome">
            </div>
               <input id="filial_vend" type="hidden" value="<?php echo Yii::app()->session['usuario']->getFilial()->id ?>">
            <div style="text-align:right"class="form-group">
               <button id="btn-vendedor" style="margin-right:15px" class="btn btn-green">Cadastrar</button>
            </div>
         </form>
   </div>
</div>

<div data-width="690" id="sucess-return" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
   <div class="modal-body" style="background:#dff0d8!important">
      <h4 style="color:#5cb85c;text-align:center"><i class="fa fa-check-circle" ></i> Proposta enviada com sucesso!</h4>
   </div>
   <div class="modal-footer" style="margin-top:0!important;text-align:center">
      <!--
      <form method="POST" action="/proposta/more/" style="display:inline">
         <button type="submit" class="btn btn-success">
            Ir para a página da proposta <span id="span-codigo-proposta"></span>
            <input type="hidden" name="id" id="ipt-hdn-id-proposta">
         </button>   
      </form>
      -->
      <form action="/proposta/minhasPropostas/" method="POST" style="display:inline">
         <button type="submit" class="btn btn-success">
            Página Inicial
         </button>   
      </form>
      <!--
      <form action="/analiseDeCredito/iniciarAnalise/" method="POST" style="display:inline">
         <button type="submit" class="btn btn-success">
            Nova Proposta
         </button>   
      </form>
      -->
   </div>
</div>

<div data-width="690" id="cadastro-precisa-atualizar" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
   <div class="modal-body" style="background:#dff0d8!important">
      <h4 style="color:#5cb85c;text-align:center">O Cadastro do cliente precisa ser atualizado!</h4>
   </div>
   <div class="modal-footer" style="margin-top:0!important;text-align:center">
      <button type="submit" class="btn btn-success" id="btn-go-to-dados-pessoais-update">
         Atualize os dados do cliente! <span id="span-codigo-proposta"></span>
         <input type="hidden" name="id" id="ipt-hdn-id-proposta">
      </button>
   </div>
</div>

<div data-width="690" id="photo-modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
   <div class="modal-header">
      
   </div>
   <div class="modal-body">
   
      <canvas style="display:none" id="canvas" width="640" height="480"></canvas>

        <div id='container'>

            <div class='select'>
                <label for='videoSource'>Video source: </label><select id='videoSource'></select>
            </div>

            <video id="video" width="500" height="320" muted autoplay></video>

        </div>
        <button id="snap">Tirar foto</button>

        <p id="pngHolder"></p>

   </div>
   <div class="modal-footer">
      <button type="button" data-dismiss="modal" class="btn btn-default">Fechar</button>   
   </div>
</div>

<style type="text/css">
   .tab-pane{
      min-height:263px!important
   }
   #grid_anexos_filter, #grid_anexos_length{
      display: none;
   }
   .div-condicoes-parcelamento
   {      
      margin-top: 10px;
      background-color: #F9F9F9;
      border-color: #DDDDDD;
      border-radius: 0 0 0 0 !important;
      color: #999999;
      padding: 10px 15px;
      border-color: #DDDDDD transparent #DDDDDD #999999;
      border-left: 2px solid #999999;
      font-weight: bold!important;
   }
   .div-condicoes-parcelamento p
   {
      margin: 0!important;
   }
   .div-condicoes-parcelamento p:hover
   {
      color: #000000;
      cursor: pointer;
   }
   .tooltip-inner {
      min-width   : 200px;
      min-height  : 60px;
   }

   .div-condicoes-parcelamento
   {      
      margin-top: 10px;
      border-radius: 0 0 0 0 !important;
      padding: 10px 15px;
   }
   .selectDefault
   {
      background-color: #F9F9F9;
      border-color: #DDDDDD;
      border-color: #DDDDDD transparent #DDDDDD #999999;
      border-left: 2px solid #999999;
      color: #999999;
   }
   .selectedIn
   {  
      background-color: #0fbc6e;
      border-color: #0b8c52;
      border-color: #0b8c52 transparent #0b8c52 #0b8c52;
      border-left: 2px solid #0b8c52;
      color: #000000;
   }
   .selectedOut
   {
      
      background-color: #F9F9F9;
      text-decoration: line-through;
   }
   /* progress bar style */
   #progressbox {
      border: 1px solid #4cae4c;
      padding: 1px; 
      position:relative;
      width:315px;
      border-radius: 3px;
      margin: 10px;
      margin-left: 0;
      display:none;
      text-align:left;
   }
   #progressbar {
      height:20px;
      border-radius: 3px;
      background-color: #4cae4c;
      width:1%;
   }
   #statustxt {
      top:3px;
      left:50%;
      position:absolute;
      display:inline-block;
      color: #FFF;
      font-size: 11px;
      font-family: arial
   }
</style>
