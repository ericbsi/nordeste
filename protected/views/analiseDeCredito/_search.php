<?php
/* @var $this AnaliseDeCreditoController */
/* @var $model AnaliseDeCredito */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Cliente_id'); ?>
		<?php echo $form->textField($model,'Cliente_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'habilitado'); ?>
		<?php echo $form->textField($model,'habilitado'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'data_cadastro'); ?>
		<?php echo $form->textField($model,'data_cadastro'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'numero_do_pedido'); ?>
		<?php echo $form->textField($model,'numero_do_pedido',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'descricao_do_bem'); ?>
		<?php echo $form->textArea($model,'descricao_do_bem',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vendedor'); ?>
		<?php echo $form->textField($model,'vendedor',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'telefone_loja'); ?>
		<?php echo $form->textField($model,'telefone_loja',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'promotor'); ?>
		<?php echo $form->textField($model,'promotor',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Procedencia_compra_id'); ?>
		<?php echo $form->textField($model,'Procedencia_compra_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mercadoria_retirada_na_hora'); ?>
		<?php echo $form->textField($model,'mercadoria_retirada_na_hora'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'celular_pre_pago'); ?>
		<?php echo $form->textField($model,'celular_pre_pago'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'observacao'); ?>
		<?php echo $form->textArea($model,'observacao',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'alerta'); ?>
		<?php echo $form->textField($model,'alerta',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'valor'); ?>
		<?php echo $form->textField($model,'valor'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'entrada'); ?>
		<?php echo $form->textField($model,'entrada'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Usuario_id'); ?>
		<?php echo $form->textField($model,'Usuario_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->