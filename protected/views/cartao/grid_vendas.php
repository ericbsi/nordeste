<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/grupoFiliais/index">
                    Cartão Credshow
                </a>
            </li>
            <li class="active">
                Vendas
            </li>
        </ol>
        <div class="page-header">
            <h1>
                Vendas Realizadas
        </div>
    </div>
</div>
<p>

</p>

<div class="row">
    <div class="col-sm-12">
        <a href="/cartao/autenticarcartao">
            <button id="nova_veda" class="btn btn-green btn-primary">
                <i class="fa fa-plus"></i>
                Nova Venda
            </button>
        </a>
    </div>
</div>
<br /><br />
<div class="row" style="padding-bottom:40px">
    <div class="col-sm-12">

        <table id="grid_vendas" 
               class="table table-striped table-bordered table-hover table-full-width dataTable">

            <thead>
                    <tr>
                        <th class="no-orderable"></th>
                        <th class="no-orderable"></th>
                        <th class="no-orderable">
                            <input 
                                class="form-control" 
                                id="inputNomeCliente" 
                                type="text" 
                                name="nomeCliente" 
                                value="" 
                                placeholder="Digite o nome do cliente..." />
                        </th>
                        <th class="no-orderable">
                            <button id="btnProc" 
                                    type="button" 
                                    class="btn btn-success">
                                <i class="fa fa-search"></i>
                            </button>
                        </th>
                    </tr>
                <tr>
                    <th style="width : 05%!important">
                    </th>
                    <th style="width : 25%!important">
                        Código
                    </th>
                    <th style="width : 35%!important">
                        Cliente
                    </th>
                    <th style="width : 15%!important">
                        CPF do Cliente
                    </th>
                    <th style="width : 8%!important">
                        Valor da Compra
                    </th>
                    <th style="width : 8%!important">
                        Qtd. Parcelas
                    </th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<style type="text/css">
   td.details-control {
      background: url('../../images/details_open.png') no-repeat center center;
      cursor: pointer;
   }
   tr.shown td.details-control {
      background: url('../../images/details_close.png') no-repeat center center;
   }
</style>

<style type="text/css">
    #grid_grupo_length{
        display: none!important;
    }
    .panel{
        background: transparent!important;
        border:none!important;
    }
</style>