<div class="row"  id="initial_place">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li><a href="#">Cartão Credshow</a></li>
            <li class="active">Autenticar Cartão</li>
        </ol>
        <div class="page-header">
            <h1>Autenticação de Cartão</h1>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12" id="msgsReturn">
    </div>
</div>
<div class="row">
    <br>
</div>

<form id="form-cpf">
    <div class="row">
        <div class="form-group">
            <div class="col-md-6">
                <label class="control-label">CPF do Cliente : <span class="symbol required"></span></label>
                <span class="input-icon">
                    <input onkeydown="bloquear_ctrl_j()" data-icon-valid-bind="#ico-cpf-valid-bind" required type="text" placeholder="" class="form-control cpf input-lg number" id="cpf" style="padding-left:35px">
                    <i id="ico-cpf-valid-bind" class="fa fa-credit-card" style="font-size:20px;line-height:46px"></i>
                </span>
            </div>
            <div class="col-md-6">
                <label class="control-label">Valor da Compra : <span class="symbol required"></span></label>
                <span>
                    <input onkeydown="bloquear_ctrl_j()" data-icon-valid-bind="#ico-cpf-valid-bind" required type="text" placeholder="" class="form-control currency2 valor-compra input-lg number" id="valor-compra" style="padding-left:6px">
                </span>
            </div>        
        </div>
    </div>
    <div class="row">
        <input type="hidden" id="idCliente">
    </div>
</form>
<br />
<form id="form-cartao">
	<div class="row">
        <div class="form-group">
            <div class="col-md-6">
                <label class="control-label">Número do Cartão : <span class="symbol required"></span></label>
                <span class="input-icon">
                    <input onkeydown="bloquear_ctrl_j()" required type="text" placeholder="" class="form-control num-cartao input-lg number" id="num-cartao" style="padding-left:35px">
                    <i id="ico-cpf-valid-bind" class="fa fa-credit-card" style="font-size:20px;line-height:46px"></i>
                </span>
                <span>
                    <input onkeydown="bloquear_ctrl_j()" required type="text" placeholder="CVC" class="form-control cvc input-lg number" id="cvc" style="padding-left:6px">
                </span>
            </div>
            <div class="col-md-2">
                <label class="control-label">Quantidade de Parcelas: <span class="symbol required"></span></label>                                 
                <select id="qtd-parcelas" class="form form-control search-select select2">
                    <option value="1">
                        1
                    </option>
                    <option value="2">
                        2
                    </option>
                    <option value="3">
                        3
                    </option>
                    <option value="4">
                        4
                    </option>
                    <option value="5">
                        5
                    </option>
                    <option value="6">
                        6
                    </option>
                    <option value="7">
                        7
                    </option>
                    <option value="8">
                        8
                    </option>
                    <option value="9">
                        9
                    </option>
                    <option value="10">
                        10
                    </option>    
                </select>
            </div>      
        </div>
    </div>
    <div class="row">
        <input type="hidden" id="idCartao">
    </div>
</form>
<br /><br />
<div class="row">
    <div class="col-md-12">
        <button id="btn-gravar-venda" class="btn btn-yellow btn-block" type="submit">
            Autenticar e Verificar Limite <i class="fa fa-arrow-circle-right"></i>
        </button>
    </div>
</div>
<div data-width="690" id="sucess-return" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
   <div class="modal-body" style="background:#dff0d8!important">
      <h4 style="color:#5cb85c;text-align:center"><i class="fa fa-check-circle" ></i> Venda realizada com sucesso!</h4>
   </div>
   <div class="modal-footer" style="margin-top:0!important;text-align:center">
      <!--
      <form method="POST" action="/proposta/more/" style="display:inline">
         <button type="submit" class="btn btn-success">
            Ir para a página da proposta <span id="span-codigo-proposta"></span>
            <input type="hidden" name="id" id="ipt-hdn-id-proposta">
         </button>   
      </form>
      -->
      <form action="/cartao/getvendas/" method="POST" style="display:inline">
         <button type="submit" class="btn btn-success">
            Página Inicial
         </button>   
      </form>
      <!--
      <form action="/analiseDeCredito/iniciarAnalise/" method="POST" style="display:inline">
         <button type="submit" class="btn btn-success">
            Nova Proposta
         </button>   
      </form>
      -->
   </div>
</div>