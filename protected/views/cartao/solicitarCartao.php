<div class="row">
    <div class="col-sm-12">
        <div class="page-header">
            <h2 style="color: darkgreen"><i class="fa fa-credit-card"></i> Solicitar Cartão</h2>
        </div>
    </div>
</div>

<div id="divCPF" class="row">
    <div class="form-group">
        <div class="col-md-4">
            <label class="control-label">
                Digite o CPF do Cliente:
                <span class="symbol required"></span>
            </label>
            <span class="input-icon">
                <input 
                    required 
                    type="text" 
                    placeholder="" 
                    class="form-control cpf input-lg cpfmask" 
                    id="cpf" 
                    style="padding-left:35px">
                <i 
                    id="ico-cpf-valid-bind" 
                    class="fa fa-credit-card" 
                    style="font-size:20px;line-height:46px">
                </i>
            </span>
        </div>      
    </div>
</div>

<br>

<div id="divCadastro" class="row panel2">
    <div class="col-sm-12">
        <div class="tabbable">
            <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="painelCliente">
                <li id="liDados" class="active">
                    <a data-toggle="tab" href="#painel_dados">
                        Visão Geral
                    </a>
                </li>
                <li id="liContato">
                    <a data-toggle="tab" href="#painel_contato">
                        Contato
                    </a>
                </li>
                <li id="liEndereco">
                    <a data-toggle="tab" href="#painel_endereco">
                        Endereço
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="painel_dados" class="tab-pane in active">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label class="control-label">
                                        Nome completo : <span class="symbol required"></span>
                                    </label>
                                    <input 
                                        type="text" 
                                        placeholder="Nome completo" 
                                        id="cliente_nome" 
                                        class="form-control"/>
                                    <span for="cliente_nome" class="help-block valid">                                        </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="control-label">
                                        Sexo : <span class="symbol required" aria-required="true"></span>
                                    </label>
                                    <select 
                                        class="form-control search-select select2 select2-offscreen" 
                                        required="required" 
                                        id="Pessoa_sexo" 
                                        tabindex="-1" 
                                        aria-required="true">
                                        <option value="">Selecione:</option>
                                        <option value="F">Feminino</option>
                                        <option value="M">Masculino</option>
                                    </select>  
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="control-label">
                                        Nascimento : 
                                        <span class="symbol required" aria-required="true"></span>
                                    </label>
                                    <input 
                                        required="" 
                                        type="date" 
                                        placeholder="Data de nascimento" 
                                        id="nascimento" 
                                        class="form-control" 
                                        aria-required="true">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="control-label">
                                        RG : 
                                        <span class="symbol required"></span>
                                    </label>
                                    <input 
                                        required 
                                        type="text" 
                                        placeholder="RG" 
                                        id="rg" 
                                        class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="control-label">
                                        Órgão Emissor : <span class="symbol required"></span>
                                    </label>
                                    <input 
                                        required 
                                        type="text" 
                                        placeholder="Órgão emissor" 
                                        id="RG_orgao_emissor" 
                                        class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="control-label">
                                        UF Emissor: 
                                        <span class="symbol required"></span>
                                    </label>
                                    <select id="ufEmissor" class="form-control search-select select2 ufSelect">
                                        <option value="0">
                                            Selecione...
                                        </option>
                                        <?php foreach (Estados::model()->findAll() as $e) { ?>
                                            <option value="<?php echo $e->sigla; ?>">
                                                <?php echo $e->nome; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="control-label">
                                        Data de Emissão : <span class="symbol required"></span>
                                    </label>
                                    <input 
                                        required 
                                        type="date" 
                                        placeholder="Data de emissão" 
                                        id="RG_data_emissao" 
                                        class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="control-label">
                                        Nacionalidade : 
                                        <span class="symbol required"></span>
                                    </label>
                                    <select 
                                        required="true" 
                                        id="select-nacionalidade" 
                                        class="draft_sent form-control search-select select2">
                                        <option value="0">
                                            Selecione:
                                        </option>
                                        <option value="Brasileira">
                                            Brasileira
                                        </option>
                                        <option value="Estrangeira">
                                            Estrangeira
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="control-label">
                                        UF : 
                                        <span class="symbol required"></span>
                                    </label>
                                    <select 
                                        id="ufCliente"
                                        class="form form-control search-select select2 ufSelect">
                                        <option value="0">
                                            Selecione...
                                        </option>
                                        <?php foreach (Estados::model()->findAll() as $e) { ?>
                                            <option value="<?php echo $e->sigla; ?>">
                                                <?php echo $e->nome; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="control-label">
                                        Cidade : 
                                        <span class="symbol required"></span>
                                    </label>
                                 <input 
                                     required="true" 
                                     placeholder="Selecione" 
                                     type="hidden" 
                                     id="cidadeCliente" 
                                     class="form-control search-select selectCidades" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label class="control-label">
                                        Estado Civil : 
                                        <span class="symbol required"></span>
                                    </label>
                                    <select 
                                        id="selectEstadoCivil" 
                                        class="form-control search-select select2">
                                        <option value="0">
                                            Selecione...
                                        </option>
                                        <?php foreach (EstadoCivil::model()->findAll() as $ec) { ?>
                                            <option value="<?php echo $ec->id; ?>">
                                                <?php echo $ec->estado; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-sm-9" style="padding-left:0!important;">
                                    <div class="col-sm-3">
                                        <a class="btn btn-red btn-block btn-step-back">
                                            <i class="fa fa-eraser"></i> 
                                            Limpar Formulário
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <a 
                                        id="btnAvancarContato" 
                                        class="btn btn-blue btn-block" 
                                        type="submit" 
                                        data-toggle="tab" 
                                        href="#">
                                        Avançar 
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="painel_contato" class="tab-pane">
                    <div class="row">
                        <div class="col-md-12">

                            <table id="grid_contato" 
                                   class="table table-striped table-bordered table-hover table-full-width dataTable">
                                <thead>
                                    <tr>
                                        <th>
                                            <input 
                                                type="text" 
                                                class="form form-control"
                                                id="numeroContato" 
                                                placeholder="Digite o número..."/>
                                        </th>
                                        <th>
                                            <select
                                                class="form form-control search-select select2"
                                                id="tipoTelefoneContato" >
                                                <option value="0">
                                                    Tipo de Telefone...
                                                </option>
                                                <?php foreach (TipoTelefone::model()->findAll('habilitado') as $tipoTelefone) {?>
                                                    <option value="<?php echo $tipoTelefone->id; ?>">
                                                        <?php echo $tipoTelefone->tipo; ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </th>
                                        <th>
                                            <input 
                                                type="text" 
                                                class="form form-control"
                                                id="ramalContato" 
                                                placeholder="Digite o ramal..."/>
                                        </th>
                                        <th>
                                            <button 
                                                type="button" 
                                                id="btnAddContato"
                                                class="btn btn-green">
                                                <i class="fa fa-plus-circle"></i>
                                            </button>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>
                                            Número
                                        </th>
                                        <th>
                                            Tipo de Telefone
                                        </th>
                                        <th>
                                            Ramal
                                        </th>
                                        <th width="3px">
                                        </th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-sm-9" style="padding-left:0!important;">
                                    <div class="col-sm-3">
                                        <a class="btn btn-orange btn-block btn-step-back" 
                                           data-toggle="tab" href="#painel_dados"
                                            id="btnVoltarDados" >
                                            <i class="fa fa-arrow-circle-left"></i> 
                                            Voltar
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <a 
                                        id="btnAvancarEndereco" 
                                        class="btn btn-blue btn-block" 
                                        data-toggle="tab" 
                                        href="#painel_endereco">
                                        Avançar 
                                        <i class="fa fa-arrow-circle-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="painel_endereco" class="tab-pane">
                    <div class="row">
                        <div class="col-md-12">

                            <table id="grid_endereco" 
                                   class="table table-striped table-bordered table-hover table-full-width dataTable"
                                   style="font-size: 10px!important">
                                <thead style="font-size: 10px!important">
                                    <tr>
                                        <th>
                                            <button class="btn btn-beige btnEntrega inclusao">
                                                <i class="fa fa-thumbs-down fa-white">
                                                    Não
                                                </i>
                                            </button>
                                        </th>
                                        <th>
                                            <input 
                                                type="text" 
                                                class="form form-control"
                                                id="cepEndereco" 
                                                placeholder="Digite o CEP..."/>
                                        </th>
                                        <th>
                                            <input 
                                                type="text" 
                                                class="form form-control"
                                                id="logradouroEndereco" 
                                                placeholder="Digite o logradouro"/>
                                        </th>
                                        <th>
                                            <input 
                                                type="text" 
                                                class="form form-control"
                                                id="bairroEndereco" 
                                                placeholder="Digite o bairro..."/>
                                        </th>
                                        <th>
                                            <input 
                                                type="text" 
                                                class="form form-control"
                                                id="numeroEndereco" 
                                                placeholder="Digite o número..."/>
                                        </th>
                                        <th>
                                            <input 
                                                type="text" 
                                                class="form form-control"
                                                id="complementoEndereco" 
                                                placeholder="Digite o complemento..."/>
                                        </th>
                                        <th>
                                            <select 
                                                id="ufEndereco" 
                                                class="form form-control search-select select2 ufSelect">
                                                <option value="0">
                                                    Uf...
                                                </option>
                                                <?php foreach (Estados::model()->findAll() as $uf) { ?>
                                                    <option value="<?php echo $uf->sigla; ?>">
                                                        <?php echo $uf->sigla; ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </th>
                                        <th>
                                            <input 
                                                required="true" 
                                                placeholder="Cidade..." 
                                                type="hidden" 
                                                id="cidadeEndereco" 
                                                class="form-control search-select selectCidades" />
                                        </th>
                                        <th>
                                            <select id="tipoMoradiaEndereco" class="form form-control search-select select2">
                                                <option value="0">
                                                    Tipo Moradia...
                                                </option>
                                                <?php foreach (TipoMoradia::model()->findAll() as $tm) { ?>
                                                    <option value="<?php echo $tm->id; ?>">
                                                        <?php echo $tm->tipo ; ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </th>
                                        <th>
                                            <select id="tipoEndereco" 
                                                    class="form form-control search-select select2">
                                                <option value="0">
                                                    Tipo...
                                                </option>
                                                <?php foreach (TipoEndereco::model()->findAll() as $te) { ?>
                                                    <option value="<?php echo $te->id; ?>">
                                                        <?php echo $te->tipo; ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </th>
                                        <th>
                                            <button 
                                                type="button" 
                                                id="btnAddEndereco"
                                                class="btn btn-green">
                                                <i class="fa fa-plus-circle"></i>
                                            </button>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>
                                            Entrega
                                        </th>
                                        <th>
                                            CEP
                                        </th>
                                        <th>
                                            Logradouro
                                        </th>
                                        <th>
                                            Bairro
                                        </th>
                                        <th>
                                            N°
                                        </th>
                                        <th>
                                            Complemento
                                        </th>
                                        <th>
                                            UF
                                        </th>
                                        <th>
                                            Cidade
                                        </th>
                                        <th>
                                            Tipo de Moradia
                                        </th>
                                        <th>
                                            Tipo
                                        </th>
                                        <th width="3px">
                                        </th>
                                    </tr>
                                </thead>
                                <tbody style="font-size: 10px!important"></tbody>
                            </table>
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-sm-9" style="padding-left:0!important;">
                                    <div class="col-sm-3">
                                        <a class="btn btn-orange btn-block btn-step-back" 
                                           data-toggle="tab" href="#painel_contato"
                                            id="btnVoltarContato" >
                                            <i class="fa fa-arrow-circle-left"></i> 
                                            Voltar
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row" style="margin-bottom:40px;">
   <div class="col-md-5"></div>
   <div class="col-md-2">
      <div class="form-group">
          <button id="btnAtualiza" class="btn btn-block" >
          </button>
      </div> 
   </div>
   <div class="col-md-5">        
   </div>
</div>

<style> 
    #grid_endereco_length   , #grid_endereco_filter , #grid_endereco_info   , #grid_endereco_paginate, 
    #grid_contato_length    , #grid_contato_filter  , #grid_contato_info    , #grid_contato_paginate
    {
        display: none
    }
</style>