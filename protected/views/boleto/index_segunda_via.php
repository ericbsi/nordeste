<?php ob_start(); ?>
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>
<html>
<head>
<title><?php //echo $dadosboleto["identificacao"]; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
	<!--
	.cp {
		font: bold 6.6pt Arial;
		color: black
	}
	<!--
	.ti {
		font: 8px Arial, Helvetica, sans-serif
	}
	<!--
	.ld {
		font: bold 11px Arial;
		color: #000000
	}
	<!--
	.ct {
		font:8px "Arial Narrow";
		color: #000033
	}
	<!--
	.cn {
		font: 9px Arial;
		color: black
	}
	<!--
	.bc {
		font: bold 11px Arial;
		color: #000000
	}
	<!--
	.ld2 {
		font: bold 12px Arial;
		color: #000000
	}
	-->
</style>

<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/boletoMarex.css" rel="stylesheet" media="all">
</head>
<body text="#000000" bgColor="#ffffff" topMargin="0" rightMargin="0">
<?php

	for ($i = 0; $i < sizeof($boletoConfig) ; $i++){

		if( $proposta->Banco_id == 5 )
		{
			$this->renderPartial('/boleto/renderBoletoMarex', array(
				'boletoConfig'	=>	$boletoConfig[$i],
				'proposta'		=>	$proposta,
				'iCount'		=>  $i,
			));
		}

		else if( $proposta->Banco_id == 7 )
		{
			$this->renderPartial('/boleto/renderBoletoSantander', array(
				'boletoConfig'	=>	$boletoConfig[$i],
				'proposta'		=>	$proposta,
				'iCount'		=>  $i,
			));
		}

		else if( $proposta->Banco_id == 3 )
		{
			$this->renderPartial('/boleto/renderBoletoBradesco', array(
				'boletoConfig'	=>	$boletoConfig[$i],
				'proposta'		=>	$proposta,
				'iCount'		=>  $i,
			));
		}

		else if( $proposta->Banco_id == 10 )
		{
			$this->renderPartial('/boleto/renderBoletoNaoBancario', array('boletoConfig'=>$boletoConfig[$i],'proposta'=>$proposta,'iCount'=> $i));
		}
	}
?>

</body>
</html>
