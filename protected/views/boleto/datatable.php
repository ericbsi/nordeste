<!doctype html>
<html class="no-js">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title></title>
      <meta name="description" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/jquery.dataTables-1.10.0.css" rel="stylesheet" media="screen">
      <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/dataTables.tableTools.css" rel="stylesheet" media="screen">

   </head>
   <body>
      <table id="table_id" class="display">
         <thead>
            <tr>
               <th>Id</th>
               <th>Campo</th>
               <th>Tipo</th>
               <th>Significado</th>
            </tr>
         </thead>
         <tbody>

            
         </tbody>
      </table>

      <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.min.js"></script>
      <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.dataTables-1.10.0.js"></script>
      <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/dataTables.tableTools.js"></script>
      
      <script>
            $("#table_id").dataTable({
               "dom": 'T<"clear">lfrtip',
               "tableTools": {
                  "sSwfPath": "/swf/copy_csv_xls_pdf.swf"
               },
               iDisplayLength : 5,
               serverSide     : true, 
               ajax           : {
                  url         : '/boleto/SSDT',
                  type        : 'POST'
               }

            });      
      </script>

   </body>
</html>