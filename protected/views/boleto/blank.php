<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/cliente/listarClientes">
                    Estoque
                </a>
            </li>
            <li class="active">
                Produtos
            </li>
        </ol>
        <div class="page-header">
            <h1>Produtos</h1>
        </div>
    </div>
</div>
<div class="row">

    <div class="col-sm-12">
        <table id="grid_produtos" class="table table-striped table-bordered table-hover table-full-width dataTable">
            <thead>
                <tr>
                    <th class="searchable">Código</th>
                    <th class="searchable">Descrição</th>
                    <th class="searchable">Ref.</th>
                    <th class="searchable">Fornecedor.</th>
                    <th class="searchable">Qtd em estoque</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>0001</td>
                    <td>Descrição</td>
                    <td>Referencia</td>
                    <td>Fornecedor</td>
                    <td>10</td>
                </tr>
                <tr>
                    <td>0002</td>
                    <td>Descrição</td>
                    <td>Referencia</td>
                    <td>Fornecedor</td>
                    <td>10</td>
                </tr>
                <tr>
                    <td>0003</td>
                    <td>Descrição</td>
                    <td>Referencia</td>
                    <td>Fornecedor</td>
                    <td>10</td>
                </tr>
                <tr>
                    <td>0004</td>
                    <td>Descrição</td>
                    <td>Referencia</td>
                    <td>Fornecedor</td>
                    <td>10</td>
                </tr>
                <tr>
                    <td>0005</td>
                    <td>Descrição</td>
                    <td>Referencia</td>
                    <td>Fornecedor</td>
                    <td>10</td>
                </tr>
                <tr>
                    <td>0006</td>
                    <td>Descrição</td>
                    <td>Referencia</td>
                    <td>Fornecedor</td>
                    <td>10</td>
                </tr>
                <tr>
                    <td>0007</td>
                    <td>Descrição</td>
                    <td>Referencia</td>
                    <td>Fornecedor</td>
                    <td>10</td>
                </tr>
                <tr>
                    <td>0008</td>
                    <td>Descrição</td>
                    <td>Referencia</td>
                    <td>Fornecedor</td>
                    <td>10</td>
                </tr>
                <tr>
                    <td>0009</td>
                    <td>Descrição</td>
                    <td>Referencia</td>
                    <td>Fornecedor</td>
                    <td>10</td>
                </tr>
                <tr>
                    <td>0010</td>
                    <td>Descrição</td>
                    <td>Referencia</td>
                    <td>Fornecedor</td>
                    <td>10</td>
                </tr>
                <tr>
                    <td>00011</td>
                    <td>Descrição</td>
                    <td>Referencia</td>
                    <td>Fornecedor</td>
                    <td>10</td>
                </tr>
                <tr>
                    <td>0012</td>
                    <td>Descrição</td>
                    <td>Referencia</td>
                    <td>Fornecedor</td>
                    <td>10</td>
                </tr>
                <tr>
                    <td>0013</td>
                    <td>Descrição</td>
                    <td>Referencia</td>
                    <td>Fornecedor</td>
                    <td>10</td>
                </tr>
                <tr>
                    <td>0014</td>
                    <td>Descrição</td>
                    <td>Referencia</td>
                    <td>Fornecedor</td>
                    <td>10</td>
                </tr>
                <tr>
                    <td>0015</td>
                    <td>Descrição</td>
                    <td>Referencia</td>
                    <td>Fornecedor</td>
                    <td>10</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">

    $(function() {

        $('#grid_produtos thead th.searchable').each(function() {

            $(this).html($(this).html() + '<input style="width:100%" type="text" placeholder="Buscar" />');
        });

        var clientesTable = $('#grid_produtos').DataTable();

        clientesTable.columns().eq(0).each(function(colIdx) {
            $('input', clientesTable.column(colIdx).header()).on('change', function() {
                clientesTable.column(colIdx).search(this.value).draw();
            })
        })
    })

</script>
<style type="text/css">
    #grid_produtos_length, #grid_produtos_filter{display: none}
</style>
>>>>>>> cee9cdb440358f10e611b3a0607221fe430f6bc2
