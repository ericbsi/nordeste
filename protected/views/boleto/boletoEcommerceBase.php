<?php ob_start(); ?>
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>
<html>
<head>
<title><?php //echo $dadosboleto["identificacao"]; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type=text/css>
	<!--
	.cp {
		font: bold 10px Arial;
		color: black
	}
	<!--
	.ti {
		font: 9px Arial, Helvetica, sans-serif
	}
	<!--
	.ld {
		font: bold 15px Arial;
		color: #000000
	}
	<!--
	.ct {
		FONT: 9px "Arial Narrow";
		COLOR: #000033
	}
	<!--
	.cn {
		FONT: 9px Arial;
		COLOR: black
	}
	<!--
	.bc {
		font: bold 20px Arial;
		color: #000000
	}
	<!--
	.ld2 {
		font: bold 12px Arial;
		color: #000000
	}
	-->
</style>

<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/boletoMarex.css" rel="stylesheet" media="all">

</head>
<body text="#000000" bgColor="#ffffff" topMargin="0" rightMargin="0">

<?php 
	
	for ( $i = 0; $i < sizeof( $boletoConfig ) ; $i++ ) {

		$this->renderPartial('/boleto/renderBoletoEcommerce', array(
			'boletoConfig'	=> $boletoConfig[$i],
			'venda'			=> $venda,
			'iCount'		=> $i,
		));			
	}
?>

</body>
</html>
