<?php ?>
<button id="mybtn" type="button" class="btn btn-primary">
Teste
</button>

<div id="retorno"></div>

<script type="text/javascript">
	
	$('#mybtn').on('click',function(){
	
		var venda = {
			
			"codigoExterno" : "0001",
			"numeroDeParcelas" : "1",
			"dataDaVenda" : "05042014",
		    "valorCobrado":"232.50",

			"boleto":{
		      "diasDePrazoParaPagamento":"5",
		      "taxaBoleto":"1.00",
		      "dataVenc":"15042014",
		    },
		
		    "cliente":{
		      "nome":"Eric Vinicius Vieira Ferreira",
		      "cpf":"08267742450",
		      "endereco":{
		         "logradouro":"Rua Djalma Maranhao",
		         "numero":"229",
		         "bairro":"Nova Descoberta",
		         "cidade":"Natal",
		         "estado":"RN",
		         "cep":"59075290"
		      },
		      "contato":{
		         "email":"eu@ericvinicius.com.br",
		         "telefone":"8496208270"
		      }
		    },
		
		    "itens":[[
			   	  {
			      	"descricao":"Produto A",
			      	"quantidade":"1"
			      },
			      {
			         "descricao":"Produto B",
			         "quantidade":"1"
			      },
		      	  {
		         	"descricao":"Produto C",
		         	"quantidade":"1"
		      	   }

		   		]
		    ]
		}

		$.ajax({
			type	: "POST",
			url		: "http://sigac/boleto/externalClient",
			data 	: {ven:venda},

			success : function(data){
				//$('#retorno').append(data)
				console.log(data)
			}
		})

	})

</script>