<?php

  $santander        = new Bradesco( $boletoConfig['vencimento'], number_format($boletoConfig['valor_corri'],2,",",""), $boletoConfig['range'], Null, $recebimento);
  $util             = new Util;
  $arrBolSeq        = array(2,5,8,11,14,17,20,23);

  $configBanco      = Banco::model()->configuracoesBanco();
  $imagem           = Yii::app()->getBaseUrl(true) . "/images/boleto/parcelas/parcela_".$boletoConfig['seq_parc'].".png";

  $meses            = $util->getMeses();

  $boletoConfigC     = 'credshow_bradesco';

  // if( in_array($recebimento->Financeira_id, [10,11]) )
  // {
  //   $boletoConfigC   = 'credshow_bradesco_fidic';
  // }
?>

<div id="boleto-all" style="background-image: url('<?php echo $imagem ?>');">

  <div id="boleto-left">
    <div id="boleto-left-top">
      <div class="bloco border-right no-border-left" style="width:55px; height:15px;padding-top:5px;padding-left:5px;">
        <img class="logobra" src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/logobradesco2.png">
      </div>
      <div class="bloco border-right" style="font-weight: bold;width:34px; height:16px;padding-left:6px;padding-top:4px">
        <span style="font-size:10px;"><?php echo $santander->geraCodigoBanco($boletoConfig['codigobanco'])  ?></span>
      </div>
      <div class="bloco" style="padding-left:6px;padding-top:3px;">
        <span style="font-size:4.5pt; text-align:center!important;">
          &nbsp;&nbsp;&nbsp;&nbsp;RECIBO <br>DO SACADO
        </span>
      </div>
    </div>

    <div id="boleto-left-body">
      <div class="bloco border-bottom h17" style="width:100%;">
        <div class="bloco border-right h16" style="width:60px;padding-top:2px;">
          <span style="font-size:4.8pt; text-align:center!important;padding-left:4px">PARCELA/PLANO:</span><br>
          <span style="font-size:5pt; text-align:center!important;padding-left:20px"><?php echo $boletoConfig["instrucoes5"] ?></span>
        </div>
        <div class="bloco h16" style="width:60px;padding-top:2px">
          <span style="font-size:4.8pt; text-align:center!important;padding-left:5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VENCIMENTO:</span><br>
          <span style="font-size:5pt; text-align:center!important;padding-left:25px;"><?php echo $util->bd_date_to_view($boletoConfig["vencimento"]) ?></span>
        </div>
      </div>

      <div class="bloco border-bottom h16" style="width:100%;">
        <div class="bloco h16" style="width:100%;padding-top:2px">
          <span style="font-size:4pt; text-align:center!important;padding-left:3px;">AGENCIA/CÓDIGO DO BENEFICIÁRIO:</span><br>
          <span style="font-size:5pt; text-align:center!important;float:right;margin-right:2px;"> <?php echo $configBanco[$boletoConfigC]['ag_cod_boleto']; ?> </span>
        </div>
      </div>

      <div class="bloco border-bottom h15" style="width:100%;">
        <div class="bloco border-right h14" style="width:80px;padding-top:2px">
          <span style="font-size:4.8pt; text-align:center!important;padding-left:4px">ESPÉCIE/MOEDA:</span><br>
        </div>
        <div class="bloco h16" style="width:60px;padding-top:2px">
          <span style="font-size:4.8pt; text-align:center!important;padding-left:5px;">&nbsp;&nbsp;&nbsp;QUANTIDADE:</span><br>
        </div>
      </div>

      <div class="bloco border-bottom h18" style="width:100%;">
        <div class="bloco h16" style="width:98%;padding-top:2px;padding-left:2px;">
          <span style="font-size:4.8pt; text-align:center!important;padding-left:0px;">VALOR DO DOCUMENTO:</span><br>
          <span style="font-size:5pt; text-align:center!important;float:right;margin-right:2px;"><?php echo $boletoConfig["valor_boleto"]?></span>
        </div>
      </div>

      <div class="bloco border-bottom h15" style="width:100%;">
        <div class="bloco h16" style="width:98%;padding-top:2px;padding-left:2px;">
          <span style="font-size:4.8pt; text-align:center!important;padding-left:0px;">(-)DESCONTO/ABATIMENTO:</span><br>
          <span style="font-size:5pt; text-align:center!important;float:right;"></span>
        </div>
      </div>

      <div class="bloco border-bottom h15" style="width:100%;">
        <div class="bloco h16" style="width:98%;padding-top:2px;padding-left:2px;">
          <span style="font-size:4.8pt; text-align:center!important;padding-left:0px;">(-)OUTRAS DEDUÇÕES:</span><br>
          <span style="font-size:5pt; text-align:center!important;float:right;"></span>
        </div>
      </div>

      <div class="bloco border-bottom h18" style="width:100%;">
        <div class="bloco h16" style="width:100%;padding-top:2px;padding-left:2px;">
          <span style="font-size:4.8pt; text-align:center!important;padding-left:0px;">(+)MORA/MULTA:</span><br>
          <span style="font-size:5pt; text-align:center!important;float:right;margin-right:5px;">
            <?php if( isset($boletoConfig['mora']) && $boletoConfig['mora'] != null && !empty($boletoConfig['mora']) ){
                  echo number_format($boletoConfig['mora'],2,',','');
                }
            ?>
          </span>
        </div>
      </div>

      <div class="bloco border-bottom h15" style="width:100%;">
        <div class="bloco h16" style="width:100%;padding-top:2px;padding-left:2px;">
          <span style="font-size:4.8pt; text-align:center!important;padding-left:0px;">(+)OUTROS ACRÉSCIMOS:</span><br>
          <span style="font-size:5pt; text-align:center!important;float:right;"></span>
        </div>
      </div>

      <div class="bloco border-bottom h18" style="width:100%;">
        <div class="bloco h16" style="width:100%;padding-top:2px;padding-left:2px;">
          <span style="font-size:4.8pt; text-align:center!important;padding-left:0px;">(=)VALOR COBRADO:</span><br>
          <span style="font-size:5pt; text-align:center!important;float:right;"></span>
        </div>
      </div>

      <div class="bloco border-bottom h18" style="width:100%;">
        <div class="bloco h16" style="width:95%;padding-top:2px;padding-left:2px;">
          <span style="font-size:4.8pt; text-align:center!important;padding-left:0px;">CARTEIRA/NOSSO NÚMERO:</span><br>
          <span style="font-size:5pt; text-align:center!important;float:right;">002 / <?php echo substr($santander->getNossoNumero(), 0,-1) . '-' . substr($santander->getNossoNumero(), -1) ?></span>
        </div>
      </div>

      <div class="bloco border-bottom h15" style="width:100%;">
        <div class="bloco h16" style="width:95%;padding-top:2px;padding-left:2px;">
          <span style="font-size:4.8pt; text-align:center!important;padding-left:0px;">NÚMERO DO DOCUMENTO:</span><br>
          <span style="font-size:5pt; text-align:center!important;float:right;"></span>
        </div>
      </div>

      <div class="bloco border-bottom h25" style="width:100%;">
        <div class="bloco h18" style="width:95%;padding-top:2px;padding-left:2px;">
          <span style="font-size:4.8pt; text-align:center!important;padding-left:0px;"><small>PAGADOR</small>:<?php echo strtoupper($boletoConfig["sacado"]) ?></span><br>

          <span style="font-size:4.8pt; margin-top:2px!important; float:left; text-align:center!important;">
            CPF/CNPJ:<?php echo $boletoConfig["cpfsacado"]; ?>
          </span>
        </div>
      </div>

      <div class="bloco h16" style="width:100%;">
        <div class="bloco h16" style="width:100%;padding-top:2px">
          <span style="font-size:4.8pt; text-align:center!important;padding-left:0px;">AUTENTICAR NO VERSO:</span><br>
          <span style="font-size:5pt; text-align:center!important;float:right;"></span>
        </div>
      </div>
    </div>
  </div>

  <div id="boleto-center">
    <div id="boleto-center-top">
      <div class="bloco border-right" style="width:60px; height:15px;padding-top:5px;padding-left:10px;">
        <img class="logobra" src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/logobradesco2.png">
      </div>
      <div class="bloco border-right" style="font-weight: bold;width:45px; height:16px;padding-left:16px;padding-top:4px">
        <span style="font-size:10px;"><?php echo $santander->geraCodigoBanco($boletoConfig['codigobanco'])  ?></span>
      </div>
      <div class="bloco" style="padding-top:3px;padding-left:70px;">
        <span style="font-size:11px;font-weight: bold">
          <?php echo $santander->monta_linha_digitavel() ?>
        </span>
      </div>
    </div>
    <div id="boleto-center-body">

      <div id="boleto-center-body-left">

        <div class="bloco border-bottom h20" style="width:100%;">

          <div class="bloco h17" style="width:100%;padding-top:3px;padding-left:2px;">
            <span style="font-size:4.8pt;">LOCAL DE PAGAMENTO:</span><br>
            <!--<span style="font-size:7pt;">PAGAR PREFERENCIALMENTE NA LOJA EM QUE A COMPRA FOI REALIZADA</span>-->
            <span style="font-size:7pt;">Pagável preferencialmente na Rede Bradesco ou Bradesco Expresso</span>
          </div>

        </div>

        <div class="bloco border-bottom h19" style="width:100%;height:30px!important;">

          <div class="bloco h18 border-right" style="width:60%;padding-top:1px;padding-left:2px;height:30px!important;">
            <span style="font-size:4.8pt;">BENEFICIÁRIO:</span><br>
            <span style="font-size:5pt;"><?php echo $configBanco[$boletoConfigC]['r_s'] . ', <br>'. $configBanco[$boletoConfigC]['endereco'] ?></span>
          </div>
          <div class="bloco h16" style="width:35%;padding-top:3px;padding-left:2px;">
            <span style="font-size:5pt;"><?php echo 'CNPJ: '. $configBanco[$boletoConfigC]['cnpj'] ?></span>
          </div>
        </div>

        <div class="bloco border-bottom h20" style="width:100%;">

          <div class="bloco h18 border-right" style="width:20%;padding-top:2px;padding-left:2px;">
            <span style="font-size:4.8pt;">DATA DO DOCUMENTO:</span><br>
            <span style="font-size:7pt;"><?php echo date('d/m/Y'); ?></span>
          </div>

          <div class="bloco h18 border-right" style="width:30%;padding-top:2px;padding-left:2px;">
            <span style="font-size:4.8pt;">N° DOCUMENTO:</span><br>
            <span style="font-size:7pt;"><?php echo substr($santander->getNossoNumero(), 0,-1) . '-' . substr($santander->getNossoNumero(), -1) ?></span>
          </div>

          <div class="bloco h18 border-right" style="width:15%;padding-top:2px;padding-left:2px;">
            <span style="font-size:4.8pt;">ESPÉCIE DOC.:</span><br>
            <span style="font-size:7pt;">DM</span>
          </div>

          <div class="bloco h18 border-right" style="width:10%;padding-top:2px;padding-left:2px;">
            <span style="font-size:4.8pt;">ACEITE:</span><br>
            <span style="font-size:7pt;">NÃO</span>
          </div>

          <div class="bloco h18" style="width:20%;padding-top:2px;padding-left:2px;">
            <span style="font-size:4.8pt;">DATA PROCESSAMENTO:</span><br>
            <span style="font-size:7pt;"><?php echo date('d/m/y'); ?></span>
          </div>

        </div>

        <div class="bloco border-bottom h20" style="width:100%;">

          <div class="bloco h18 border-right" style="width:20%;padding-top:2px;padding-left:2px;">
            <span style="font-size:4.8pt;">USO DO BANCO:</span><br>
            <span style="font-size:7pt;"></span>
          </div>

          <div class="bloco h18 border-right" style="width:20%;padding-top:2px;padding-left:2px;">
            <span style="font-size:4.8pt;">CARTEIRA:</span><br>
            <span style="font-size:7pt;">002</span>
          </div>

          <div class="bloco h18 border-right" style="width:15%;padding-top:2px;padding-left:2px;">
            <span style="font-size:4.8pt;">ESPÉCIE:</span><br>
            <span style="font-size:7pt;">REAL</span>
          </div>

          <div class="bloco h18 border-right" style="width:10%;padding-top:2px;padding-left:2px;">
            <span style="font-size:4pt;">QUANTIDADE:</span><br>
            <span style="font-size:6pt;"></span>
          </div>

          <div class="bloco h18" style="padding-top:2px;padding-left:2px;">
            <span style="font-size:4.8pt;">VALOR:</span><br>
            <span style="font-size:7pt;"></span>
          </div>

        </div>

        <div class="bloco border-bottom h77" style="width:99%;padding-left:5px;padding-top:5px; padding-bottom: 5px;">
          <span style="font-size:4pt;">INSTRUÇÕES (TEXTO DE RESPONSABILIDADE DO CEDENTE):</span><br>
          <span style="font-size:6.5pt;"><?php echo $boletoConfig["instrucoes1"]; ?></span><br>
          <span style="font-size:6.5pt;"><?php echo $boletoConfig["instrucoes2"]; ?></span><br>
          <span style="font-size:6.5pt;"><?php echo $boletoConfig["instrucoes4"]; ?></span><br>
          <span style="font-size:6.5pt;"><?php echo $boletoConfig["instrucoes6"]; ?></span><br>
          <span style="font-size:6.5pt;"><?php echo $boletoConfig["instrucoes7"]; ?></span><br>

        </div>

        <div class="bloco" style="font-size: 7pt; text-align:center;color:white;width:99%;padding-left:5px;padding-top:2px;background-color:black;">
            <span>
              <?php
                setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                date_default_timezone_set('America/Sao_Paulo');
                //echo mb_strtoupper('REF. VENCIMENTO ' . strftime('%B/%Y', strtotime($boletoConfig['vencimento'])), 'UTF-8');
                echo mb_strtoupper('REF. VENCIMENTO ' . $meses[ intval( substr($boletoConfig['vencimento'], 5,2)-1 ) ][1], 'UTF-8') . '/' . substr($boletoConfig['vencimento'], 0,4);
              ?>
            </span>
        </div>

        <div class="bloco h30" style="width:99%;padding-left:5px;padding-top:5px;">
          <span style="font-size:4pt;">PAGADOR:</span><br>
          <span style="font-size:5pt;"><?php echo strtoupper($boletoConfig["sacado"])?> - CPF/CNPJ:<?php echo $boletoConfig["cpfsacado"]; ?></span><br>
          <span style="font-size:5pt;"><?php echo strtoupper( $boletoConfig["endereco1"] ) .', '. strtoupper($boletoConfig["bairro"] ) . ' - '. strtoupper($boletoConfig["endereco2"] ) . ' / ' .strtoupper($boletoConfig["uf"] ); ?> </span><br>
        </div>

        <div class="bloco" style="width:100%;padding-left:5px;padding-top:2px;">
          <?php $santander->fbarcode(); ?>
        </div>

      </div>

      <div id="boleto-center-body-right" class="border-left" style="height:156px;">

        <div class="bloco border-bottom h20" style="width:100%;">
          <div class="bloco h16" style="width:100%;padding-top:2px">
            <span style="font-size:4.8pt; text-align:center!important;padding-left:2px;">VENCIMENTO:</span><br>
            <span style="font-size:6pt; text-align:center!important;float:right;"><?php echo $util->bd_date_to_view($boletoConfig["vencimento"]) ?></span>
          </div>
        </div>

        <div class="bloco border-bottom h20" style="width:100%;">
          <div class="bloco h16" style="width:100%;padding-top:2px">
            <span style="font-size:4pt; text-align:center!important;padding-left:3px;">AGENCIA/CÓDIGO DO BENEFICIÁRIO:</span><br>
            <span style="font-size:4.8pt; text-align:center!important;float:right;"><?php echo $configBanco[$boletoConfigC]['ag_cod_boleto']; ?></span>
          </div>
        </div>

        <div class="bloco border-bottom h19" style="width:100%;">
          <div class="bloco h16" style="width:100%;padding-top:2px">
            <span style="font-size:4.8pt; text-align:center!important;padding-left:3px;">NOSSO NÚMERO:</span><br>
            <span style="font-size:6pt; text-align:center!important;float:right;"><?php echo substr($santander->getNossoNumero(), 0,-1) . '-' . substr($santander->getNossoNumero(), -1) ?></span>
          </div>
        </div>

        <div class="bloco border-bottom h20" style="width:100%;">
          <div class="bloco h16" style="width:100%;padding-top:2px">
            <span style="font-size:4.8pt; text-align:center!important;padding-left:3px;">(=)  VALOR DOCUMENTO:</span><br>
            <span style="font-size:6pt; text-align:center!important;float:right;"><?php echo $boletoConfig['valor_boleto']; ?></span>
          </div>
        </div>

        <div class="bloco border-bottom h17" style="width:100%;">
          <div class="bloco h16" style="width:100%;padding-top:2px">
            <span style="font-size:4.8pt; text-align:center!important;padding-left:3px;">(-)  DESCONTO / ABATIMENTOS:</span><br>
          </div>
        </div>

        <div class="bloco border-bottom h17" style="width:100%;">
          <div class="bloco h16" style="width:100%;padding-top:2px">
            <span style="font-size:4.8pt; text-align:center!important;padding-left:3px;">(-)  OUTRAS DEDUÇÕES:</span><br>
          </div>
        </div>

        <div class="bloco border-bottom h19" style="width:100%;">
          <div class="bloco h16" style="width:100%;padding-top:2px">
            <span style="font-size:4.8pt; text-align:center!important;padding-left:3px;">(+)  MORA / MULTA:</span><br>
            <span style="font-size:4.8pt; text-align:center!important;padding-left:3px;">
              <?php if( isset($boletoConfig['mora']) && $boletoConfig['mora'] != null && !empty($boletoConfig['mora']) ){
                  echo number_format($boletoConfig['mora'],2,',','');
                }
              ?>
            </span>
          </div>
        </div>

        <div class="bloco border-bottom h16" style="width:100%;">
          <div class="bloco h16" style="width:100%;padding-top:2px">
            <span style="font-size:4.8pt; text-align:center!important;padding-left:3px;">(+)  OUTROS ACRÉSCIMOS:</span><br>
          </div>
        </div>

        <div class="bloco border-bottom h16 no-border-bottom" style="width:100%;">
          <div class="bloco h16" style="width:100%;padding-top:2px">
            <span style="font-size:4.8pt; text-align:center!important;padding-left:3px;">(=)  VALOR COBRADO:</span><br>
            <span style="font-size:5pt; text-align:center!important;float:right;"></span>
          </div>
        </div>

      </div>
    </div>
  </div>

</div>

<?php if ( ( $iCount+1 ) % 3 == 0 ){ ?>
  <div class="page-break"></div>
<?php }else{ ?>
  <p >&nbsp;</p>
<?php } ?>
<style type="text/css">
  #boleto-all {
    background-repeat: no-repeat;
    background-position: 80px -30px;
    background-size: 350px 250px;
  }

  .logobra {
    width: 50px;
    height: 14px;
  }

  @media all {
    .page-break { display: none; }
  }

  @media print {
   .page-break { display: block; page-break-before: always; }
  }

  @media print {
   * {-webkit-print-color-adjust:exact;}
  }

</style>
