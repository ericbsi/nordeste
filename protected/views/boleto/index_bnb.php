<?php ob_start(); ?>
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>
<html>
<head>
<title><?php //echo $dadosboleto["identificacao"]; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
	<!--
	.cp {
		font: bold 6.6pt Arial;
		color: black
	}
	<!--
	.ti {
		font: 8px Arial, Helvetica, sans-serif
	}
	<!--
	.ld {
		font: bold 11px Arial;
		color: #000000
	}
	<!--
	.ct {
		font:8px "Arial Narrow";
		color: #000033
	}
	<!--
	.cn {
		font: 9px Arial;
		color: black
	}
	<!--
	.bc {
		font: bold 11px Arial;
		color: #000000
	}
	<!--
	.ld2 {
		font: bold 12px Arial;
		color: #000000
	}
	-->
</style>

	<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/boletoMarex.css" rel="stylesheet" media="all">
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.min.js"></script>
</head>
<body text="#000000" bgColor="#ffffff" topMargin="0" rightMargin="0">
<?php

    $erroTituloEmprestimo = false;

    $ehEmprestimo       = false;
    $retornoEmprestimo  = false;

    if($ehEmprestimo)
    {
      $erroTituloEmprestimo = $retornoEmprestimo["hasErrors"];
    }

    if($erroTituloEmprestimo)
    {
      echo 'Título:   ' . $retornoEmprestimo["title"] . '<br>';
      echo 'Mensagem: ' . $retornoEmprestimo["text" ] . '<br>';
    }
    else
    {
		for ($i = 0; $i < sizeof($boletoConfig)-2 ; $i++){

			$this->renderPartial('renderBoletoNaoBancario', array(
				'boletoConfig'	=>	$boletoConfig[$i],
				'proposta'		=>	$proposta,
				'iCount'		=>  $i,
			));
		}
    }

?>

</body>
</html>
