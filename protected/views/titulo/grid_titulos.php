<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/produto/listarTitulos">
                    Títulos
                </a>
            </li>
            <li class="active">
                Listar
            </li>
        </ol>
    </div>
</div>
<br>
<br>
<!--<p>
    <a class="btn btn-success" href="#modal_form_titulo" data-toggle="modal" id="btn_modal_form_new_titulo">
        <i class="fa fa-plus"> Título</i>
    </a>
</p>-->
<p>
</p>

<div class="row">
    <div class="col-sm-12">
        <table id="grid_titulos" 
               class="table table-striped table-bordered table-hover table-full-width dataTable">
            <thead>
                <tr>
                    <th class="no-orderable"></th>
                    <th class="no-orderable">Natureza</th>
                    <th class="no-orderable">Pessoa</th>
                    <th class="no-orderable">Prefixo</th>
                    <th class="no-orderable">Número</th>
                    <th class="no-orderable">Emissão</th>
                    <th class="no-orderable">Forma de Pagamento</th>
                    <th class="no-orderable">Qtd Parcelas</th>
                    <th class="no-orderable" style="width : 150px!important">Total</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th></th>
                    <th class="searchable">Natureza</th>
                    <th class="searchable">Pessoa</th>
                    <th class="searchable">Prefixo</th>
                    <th class="searchable">Número</th>
                    <th class="searchable">Emissão</th>
                    <th class="searchable">Forma de Pagamento</th>
                    <th class="searchable">Qtd Parcelas</th>
                    <th>Total</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

<style type="text/css">
    td.details-control {
        padding:7px 13px!important;
        background: url('../../images/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.details td.details-control {
        background: url('../../images/details_close.png') no-repeat center center;
    }
</style>