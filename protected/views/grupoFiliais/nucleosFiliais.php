<div class="row">
   <div class="col-sm-12">
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/grupoFiliais/nucleosFiliais">
               Grupos de Filiais
            </a>
         </li>
         <li class="active">
            Núcleos
         </li>
      </ol>
      <div class="page-header">
         <h1>
            Núcleos de Filiais
      </div>
   </div>
</div>
<p>

</p>
<div class="row">
   <div class="col-sm-12">

      <table id="grid_nucleo" 
             class="table table-striped table-bordered table-hover table-full-width dataTable">

         <thead>
            <tr>
               <th class="no-orderable"></th>
               <th class="no-orderable"></th>
               <th class="no-orderable">
                  <select class="grupoFilial" id="grupoFilial" name="NucleoFilial[GrupoFilial_id]">
                     <option value="0" selected="">
                        Selecione um Grupo
                     </option>
                     <?php 
                        foreach (GrupoFiliais::model()->findAll() as $grupo)
                        { 
                     ?>
                        <option value="<?php echo $grupo->id ?>" >
                           <?php echo $grupo->nome_fantasia ?>
                        </option>
                     <?php 
                        
                        } 
                     ?>
                  </select>
               </th>
               <th class="no-orderable">
                  <input 
                     class="form-control" 
                     id="inputNomeNucleo" 
                     type="text" 
                     name="NucleoFilial[nome]" 
                     value="" 
                     placeholder="Digite o nome do Núcleo..."/>
               </th>
               <th class="no-orderable">
                  <select class="bancoNucleo" id="bancoNucleo" name="NucleoFilial[Banco_id]">
                     <option value="0" selected="">
                        Selecione um Banco
                     </option>
                     <?php 
                        foreach (Banco::model()->findAll() as $banco)
                        { 
                     ?>
                        <option value="<?php echo $banco->id ?>" >
                           <?php echo $banco->codigo . ' - ' . $banco->nome  ?>
                        </option>
                     <?php 
                        
                        } 
                     ?>
                  </select>
               </th>
               <th class="no-orderable">
                  <input 
                     class="form-control" 
                     id="inputAgencia" 
                     type="text" 
                     name="NucleoFilial[agencia]" 
                     value="" 
                     placeholder="Digite o nº da Agência..."/>
               </th>
               <th class="no-orderable">
                  <input 
                     class="form-control" 
                     id="inputConta" 
                     type="text" 
                     name="NucleoFilial[conta]" 
                     value="" 
                     placeholder="Digite o nº da Conta..."/>
               </th>
               <th class="no-orderable">
                  <input 
                     class="form-control" 
                     id="inputOperacao" 
                     type="text" 
                     name="NucleoFilial[operacao]" 
                     value="" 
                     placeholder="Digite o cod Operação..."/>
               </th>
               <th class="no-orderable">
                  <input 
                     class="form-control" 
                     id="inputCGCDadosBancarios" 
                     type="text" 
                     name="NucleoFilial[cgcDadosBancarios]" 
                     value="" 
                     placeholder="Digite o CGC..."/>
               </th>
               <th class="no-orderable">
                  <button id="btnSalvar" 
                          type="button" 
                          class="btn btn-success">
                     <i class="fa fa-save"></i>
                  </button>
               </th>
            </tr>
            <tr>
               <th style="width : 05%!important">
               </th>
               <th style="width : 05%!important">
                  ID
               </th>
               <th style="width : 15%!important">
                  Grupo
               </th>
               <th style="width : 32%!important">
                  Nome
               </th>
               <th style="width : 10%!important">
                  Banco
               </th>
               <th style="width : 10%!important">
                  Agência
               </th>
               <th style="width : 10%!important">
                  Conta
               </th>
               <th style="width : 05%!important">
                  Operação
               </th>
               <th style="width : 05%!important">
                  CGC Conta
               </th>
               <th style="width : 03%!important"></th>
            </tr>
         </thead>
         <tbody>
         </tbody>
      </table>
   </div>
</div>

<style type="text/css">
   #grid_nucleo_length{
      display: none!important;
   }
   #grupoFilial{
      width: 200px;
   }
   .panel{
      background: transparent!important;
      border:none!important;
   }
</style>