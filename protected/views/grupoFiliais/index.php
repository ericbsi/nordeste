<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/grupoFiliais/index">
                    Grupo de Filiais
                </a>
            </li>
            <li class="active">
                Index
            </li>
        </ol>
        <div class="page-header">
            <h1>
                Grupo de Filiais
        </div>
    </div>
</div>
<p>

</p>
<div class="row">
    <div class="col-sm-12">

        <table id="grid_grupo" 
               class="table table-striped table-bordered table-hover table-full-width dataTable">

            <thead>
                    <tr>
                        <th class="no-orderable"></th>
                        <th class="no-orderable"></th>
                        <th class="no-orderable">
                            <input 
                                class="form-control" 
                                id="inputNomeGrupo" 
                                type="text" 
                                name="nomeGrupo" 
                                value="" 
                                placeholder="Digite o nome do Grupo..." />
                        </th>
                        <th class="no-orderable">
                            <button id="btnSalvar" 
                                    type="button" 
                                    class="btn btn-success">
                                <i class="fa fa-save"></i>
                            </button>
                        </th>
                    </tr>
                <tr>
                    <th style="width : 05%!important">
                    </th>
                    <th style="width : 05%!important">
                        ID
                    </th>
                    <th style="width : 87%!important">
                        Nome
                    </th>
                    <th style="width : 03%!important">
<!--                        <p>
                            <a class="btn btn-success" 
                               data-toggle="modal" 
                               id="btn_novo_grupo">
                                <i class="fa fa-plus"></i>
                            </a>
                        </p>-->
                    </th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<style type="text/css">
    #grid_grupo_length{
        display: none!important;
    }
    .panel{
        background: transparent!important;
        border:none!important;
    }
</style>