
<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type=text/css>
<!--
.cp {
	font: bold 10px Arial;
	color: black
}
<!--
.ti {
	font: 9px Arial, Helvetica, sans-serif
}
<!--
.ld {
	font: bold 15px Arial;
	color: #000000
}
<!--
.ct {
	FONT: 9px "Arial Narrow";
	COLOR: #000033
}
<!--
.cn {
	FONT: 9px Arial;
	COLOR: black
}
<!--
.bc {
	font: bold 20px Arial;
	color: #000000
}
<!--
.ld2 {
	font: bold 12px Arial;
	color: #000000
}
-->
</style>

<?php 

include("modulo_11.php");

// ------------------------- DADOS DINÂMICOS DO SEU CLIENTE PARA A GERAÇÃO DO BOLETO (FIXO OU VIA GET) -------------------- //
// Os valores abaixo podem ser colocados manualmente ou ajustados p/ formulário c/ POST, GET ou de BD (MySql,Postgre,etc)	//

// DADOS DO BOLETO PARA O SEU CLIENTE
$dias_de_prazo_para_pagamento = 5;
$taxa_boleto = 5.00;
$data_venc = date("d/m/Y", time() + ($dias_de_prazo_para_pagamento * 86400));  // Prazo de X dias OU informe data: "13/04/2006"; 
$valor_cobrado = "150,00"; // Valor - REGRA: Sem pontos na milhar e tanto faz com "." ou "," ou com 1 ou 2 ou sem casa decimal
$valor_cobrado = str_replace(",", ".",$proposta->valor);
$valor_boleto = number_format($valor_cobrado+$taxa_boleto, 2, ',', '');

// Quantidade de pedidos
$qtdePedidos = 1;

//ATENÇÃO NESTE NUMERO. UM ERRO NO NUMERO DO BOLETO CAUSARÁ ERROS EM TODO O BOLETO
//COLOQUE AQUI O NÚMERO ÚNICO SEQUENCIAL QUE O BANCO LHE FORNECE
//NO MEU CASO, O BANCO ME FORNECEU O SEGUINTE NUMERO
//EX: 2074200002 Á 2074299999
//CADA BOLETO PRECISA SER SEQUENCIAL, E NUNCA PODE REPETIR
//ENTAO, UM CODIGO DE BOLETO VALIDO SERIA EX: 2074200003
//SOLICITE AO GERENTE O NUMERO SEQUENCIAL, E COM ELE GERE AS SEQUENCIAS 
//PARA CADA BOLETO

$dadosboleto["numero_documento"] = 2074200003; // --- ALTERE ISSO PARA O NUMERO SEQUENCIAL DO SEU BOLETO, DE ACORDO COM A SUA FAIXA DE NUMEROS
$dadosboleto["numero_documento"] .= modulo_11( $dadosboleto["numero_documento"],7);

//CALCULO DO DIGITO VERIFICADOR DO NUMERO DO DOCUMENTO

$dadosboleto["data_vencimento"] = $data_venc; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
$dadosboleto["data_documento"] = date("d/m/Y"); // Data de emissão do Boleto
$dadosboleto["data_processamento"] = date("d/m/Y"); // Data de processamento do boleto (opcional)
$dadosboleto["valor_boleto"] = $valor_boleto; 	// Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgula

// DADOS DO SEU CLIENTE
$dadosboleto["sacado"] = 'LUCAS PEPERAIO'; //NOME DO CLIENTE
$dadosboleto["cpfsacado"] = '111.111.111-11'; //CPF DO CLIENTE
$dadosboleto["endereco1"] = 'RUA AMAPA, 525' . 'COMPLEMENTO SE HOUVER'; //NÃO ALTERE A ORDEM, PRECISA SER ASSIM PARA SER HOMOLOGADO
$dadosboleto["bairro"] = 'BAIRRO';
$dadosboleto["endereco2"] = 'CEP CIDADE';
$dadosboleto["estado"] = 'ESTADO';

// INFORMACOES PARA O CLIENTE
$dadosboleto["demonstrativo1"] = "Pagamento de Compra na Loja BLA BLA BLA";
$dadosboleto["demonstrativo2"] = "PRODUTO <br>Taxa bancária - R$ ".number_format($taxa_boleto, 2, ',', '');
$dadosboleto["demonstrativo3"] = "http://projetos.lucaspeperaio.com.br";
$dadosboleto["instrucoes1"] = "- Sr. Caixa, cobrar multa de 2% após o vencimento";
$dadosboleto["instrucoes2"] = "- Receber até 10 dias após o vencimento";
$dadosboleto["instrucoes3"] = "- Em caso de dúvidas entre em contato conosco: contato@seusite.com.br";
$dadosboleto["instrucoes4"] = "PARA MAIS INSTRUCOES, PREENCHA AQUI";

// DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
$dadosboleto["quantidade"] = "1"; //ALTERE APENAS A QUANTIDADE
$dadosboleto["valor_unitario"] = ""; //NAO ALTERE
$dadosboleto["aceite"] = "NÃO";	//NÃO ALTERE
$dadosboleto["especie"] = "REAL"; //NAO ALTERE
$dadosboleto["especie_doc"] = "PD"; //NAO ALTERE


// ---------------------- DADOS FIXOS DE CONFIGURAÇÃO DO SEU BOLETO --------------- //
// DADOS PERSONALIZADOS - HSBC
$dadosboleto["agencia"] = "0000"; //AGENCIA HSBC
$dadosboleto["codigo_cedente"] = "0012345"; // Código do Cedente (Somente 7 digitos)
$dadosboleto["carteira"] = "00";  // Código da Carteira (NÃO ALTERE)
$dadosboleto["nosso_numero"] = "0043214"; //NUMERO DO BANCO, CONSULTE COM O GERENTE

// SEUS DADOS
$dadosboleto["identificacao"] = "NOME DA EMPRESA";
$dadosboleto["cpf_cnpj"] = "0000000/0000-00";
$dadosboleto["endereco"] = "Avenida dos bla bla bla, 700 - Sala 2";
$dadosboleto["cidade_uf"] = "CIDADE / ESTADO";
$dadosboleto["cedente"] = "RAZAO SOCIAL";

// NÃO ALTERAR!
include("funcoes_hsbc.php"); 
include("layout_hsbc.php");
?>