<?php  ?>
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap.min.css" rel="stylesheet" media="screen">
<form>
   <div class="row">
        <div class="col-md-8">
	        <div class="form-group">
	        	<label class="control-label">Telefone</label>
            <input id="numero" class="form-control" type="text" name="numero" value="<?php echo $entity->numero; ?>">
            <input id="tipoInput" class="form-control" type="hidden" name="tipo" value="<?php echo "telefone"; ?>">
	        	<input id="idInput" class="form-control" type="hidden" name="id" value="<?php echo $entity->id; ?>">
	        </div>
          <div class="form-group">
            <label class="control-label">Tipo</label>
            <select name="tipo" id="tipoTelefone" class="form-control">
                <?php foreach( TipoTelefone::model()->findAll() as $tipoTelefone ) {?>
                    <option <?php if ( $entity->Tipo_Telefone_id == $tipoTelefone->id ) { echo " selected "; } ?> value="<?php echo $tipoTelefone->id ?>"><?php echo $tipoTelefone->tipo; ?></option>
                <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label class="control-label">Ramal</label>
            <input id="ramal" class="form-control" type="text" name="ramal" value="<?php echo $entity->ramal; ?>">
          </div>
	   		<div class="form-group">
      			<button id="sbmt" type="submit" class="btn btn-success save-event"><i class="fa fa-check"></i> Salvar</button>
	   		</div>     
        </div>
   </div>
</form>

<script type="text/javascript">

      $('#sbmt').on('click',function(){            

             $.ajax({

                  type :  'POST', 
                  url  : '/cliente/changeClienteContatoAttribute/',
                  data : {
                        'numero' : $('#numero').val(),
                        'ramal' : $('#ramal').val(),
                        'id' : $('#idInput').val(),
                        'tipo' : $("#tipoTelefone option:selected").val(),
                  },

                  success : function ( data ){
                        window.parent.$('#stack3').modal('hide')
                        window.parent.$("#telefone_" + $('#idInput').val() + "_numero_return").html( $('#numero').val() )
                        window.parent.$("#telefone_" + $('#idInput').val() + "_tipo_return").html( $("#tipoTelefone option:selected").text() )
                        window.parent.$("#telefone_" + $('#idInput').val() + "_ramal_return").html( $('#ramal').val() )
                  }
            })

            return false;            
      })
</script>