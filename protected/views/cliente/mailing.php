<div class="row">
   <div class="col-sm-12">
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
               Clientes
            </a>
         </li>
         <li class="active">
            Lista
         </li>
      </ol>
      <div class="page-header">
         <h1>
            Clientes
      </div>
   </div>
</div>
<p>
<div class="row">
   <div class="col-sm-12">
      <table id="grid_clientes" class="table table-striped table-bordered table-hover table-full-width dataTable">
         <thead>
            <tr>
               <th>Nome do cliente</th>
               <th>CPF</th>
               <th>Sexo</th>
               <th>Nascimento</th>
               <th>Contatos</th>
            </tr>
         </thead>
         <tbody>
            <?php $util = new Util; ?>
            <?php foreach ( Cliente::model()->findAll(['order' => 'id desc','limit' => 20000,'offset' => 34000]) as $cliente ): ?>
               <?php if( $cliente->pessoa->contato != NULL && $cliente->pessoa->contato->getLasTelefone() != NULL 
               && trim($cliente->pessoa->contato->getLasTelefone()->numero) != '' && $cliente->pessoa->contato->getLasTelefone()->numero != NULL && $cliente->pessoa->getCPF() != NULL ): ?>
                  <tr>
                     <td><?php echo strtoupper($cliente->pessoa->nome); ?></td>
                     <td><?php echo $cliente->pessoa->getCPF()->numero; ?></td>
                     <td><?php echo strtoupper($cliente->pessoa->sexo); ?></td>
                     <td><?php echo $util->bd_date_to_view($cliente->pessoa->nascimento); ?></td>
                     <td><?php echo str_replace([' ','-','(',')','/'], ['','','','',''], $cliente->pessoa->contato->getLasTelefone()->numero); ?></td>
                  </tr>
               <?php endif; ?>
            <?php endforeach ?>

         </tbody>
      </table>
   </div>
</div>
<style type="text/css">
   #grid_clientes_filter, #grid_clientes_length{
      display: none;
   }
</style>