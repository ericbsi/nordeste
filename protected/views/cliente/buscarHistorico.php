<div class="row">
   <div class="col-sm-12">
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/cliente/listarClientes">
            Clientes
            </a>
         </li>
         <li class="active">
            Buscar
         </li>
      </ol>
      <div class="page-header">
         <h1>
         Buscar Cliente
      </div>
   </div>
</div>
<div class="row">
	<div class="alert alert-warning">
		<i class="fa fa-info-circle"></i>
		<strong>Atenção!</strong> Caso não consiga imprimir o boleto, entre em contato através do telefone: <strong>84 2040 0800</strong>, ou envie um email para <strong>boleto@credshow.com.br</strong>.
	</div>
   	<div class="col-sm-12">
		<form id="form-cpf" method="post" action="/cliente/historicoIntegrado/" target="_blank">
		   <div class="row">
		      <div class="form-group">
		        <div class="col-md-2">
		            <label class="control-label">CPF do Cliente : <span class="symbol required" ></span></label>
		            <span class="input-icon">
		               <input required="required" type="text" class="form-control cpf input-lg cpfmask" id="cpfcliente" name="cpf">
		            </span>
		         </div>
		      </div>
		   </div>
		   <div class="row">
		      <div class="form-group">
		        <div class="col-md-2">
		            <label class="control-label"></label>
		            <span class="input-icon">
		               <button type="submit" class="btn btn-success btn-squared btn-lg">Buscar</button>
		            </span>
		         </div>
		      </div>
		   </div>
		</form>
   	</div>
</div>