<?php
      
     $sexos = Sexo::model()->findAll(); 

?>

<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap.min.css" rel="stylesheet" media="screen">

<div class="col-md-6">
   <h4>Sexo</h4>
   <form method="POST" action="">
      <p>
         <select id="form-field-select-3" class="form-control search-select" name="sexo">
            <?php foreach ( $sexos as $sexo ) { ?>
                  <option  <?php if( $cliente->pessoa->sexo == $sexo->sigla ) { echo "selected"; } ?> value="<?php echo $sexo->sigla; ?>">
                        <?php echo $sexo->sexo; ?>
                  </option>
            <?php  } ?>
         </select>
         <input type="hidden" value="sexo" id="attribute">
         <input type="hidden" value="<?php echo $cliente->pessoa->id ?>" id="pessoa_id">
      </p>
      <p>
            <button id="sbmt" class="btn btn-yellow btn-block" type="submit">
                  Salvar <i class="fa fa-arrow-circle-right"></i>
            </button>
      </p>
   </form>
</div>

<script type="text/javascript">
      $('#sbmt').on('click',function(){

            $.ajax({

                  type :  'POST', 
                  url  : '/cliente/changeClienteAttribute/',
                  data : {
                        'attribute_value' : $('#form-field-select-3').val(),
                        'attribute' : $('#attribute').val(),
                        'pessoa_id' : $('#pessoa_id').val(),
                  },

                  success : function (data){
                        window.parent.$('#stack1').modal('hide')
                        window.parent.$('#sexo_pessoa').html(data);
                  }
            })

            return false;            
      })
</script>