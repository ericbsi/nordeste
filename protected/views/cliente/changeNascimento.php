<?php 
   
   $util = new Util;

?>

<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap.min.css" rel="stylesheet" media="screen">

<div class="col-md-6">
   <h4>Nascimento</h4>
   <form method="POST" action="">
      <p>
         <input type="text" id="nascimento" value="<?php if( $cliente->pessoa->nascimento == NULL ){ echo "00/00/0000"; }else{ echo $util->bd_date_to_view($cliente->pessoa->nascimento); } ?>">
         <input type="hidden" value="nascimento" id="attribute">
         <input type="hidden" value="<?php echo $cliente->pessoa->id ?>" id="pessoa_id">
      </p>
      <p>
            <button id="sbmt" class="btn btn-yellow btn-block" type="submit">
                  Salvar <i class="fa fa-arrow-circle-right"></i>
            </button>
      </p>
   </form>
</div>

<script type="text/javascript">
      $('#sbmt').on('click',function(){

            $.ajax({

                  type :  'POST', 
                  url  : '/cliente/changeClienteAttribute/',
                  data : {
                        'attribute_value' : $('#nascimento').val(),
                        'attribute' : $('#attribute').val(),
                        'pessoa_id' : $('#pessoa_id').val(),
                  },

                  success : function (data){
                        window.parent.$('#stack1').modal('hide')
                        window.parent.$('#nascimento_pessoa').html(data);
                  }
            })

            return false;            
      })
</script>