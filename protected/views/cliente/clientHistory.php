<?php $numerosDePagamentos = $cliente->numerosDePagamentos();
 $util = new Util(); ?>

<script type="text/javascript">
   
   function resizeIframe( obj ){

      var isFirefox = typeof InstallTrigger !== 'undefined';
      var plusFf = 0;

      if (isFirefox) {plusFf = 100};

      {
         obj.style.height = 0;
      };
   
      {
         obj.style.height = plusFf + obj.contentWindow.document.body.scrollHeight + 'px';
      }
   }

</script>

<div class="row">
   <div class="col-sm-12">
      <!-- start: PAGE TITLE & BREADCRUMB -->
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
            Cliente
            </a>
         </li>
         <li class="active">
            Histórico
         </li>
      </ol>
      <div class="page-header">
         <h1>Histórico /<small>Cliente: <?php echo $cliente->pessoa->nome ?> - CPF: <?php echo $cliente->pessoa->getCPF()->numero; ?></small></h1>
      </div>

   </div>
</div>

<div class="row">
   <div class="col-md-12">
      <!-- start: DYNAMIC TABLE PANEL -->
      <div class="panel panel-default">
         <div class="panel-heading">
            <i class="fa fa-external-link-square"></i>
            Histórico de propostas
            <div class="panel-tools">
               <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
               </a>
               <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
               <i class="fa fa-wrench"></i>
               </a>
               <a class="btn btn-xs btn-link panel-refresh" href="#">
               <i class="fa fa-refresh"></i>
               </a>
               <a class="btn btn-xs btn-link panel-expand" href="#">
               <i class="fa fa-resize-full"></i>
               </a>
               <a class="btn btn-xs btn-link panel-close" href="#">
               <i class="fa fa-times"></i>
               </a>
            </div>
         </div>
         <div class="panel-body">
            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
               <thead>
                  <tr>
                     <th class="hidden-xs">Código</th>
                     <th>Data</th>
                     <th>Valor financiado R$</th>
                     <th>Valor final a pagar R$</th>
                     <th>N° parcelas</th>
                     <th>Status</th>
                     <th></th>
                  </tr>
               </thead>
               <tbody>

                  <?php foreach( $analises as $analise ) { ?>
                  	<?php foreach( $analise->getPropostasAnalise(array(2),1) as $proposta ) { ?>
	                     <tr>
	                        <td class="hidden-xs">
	                        	<?php echo $proposta->codigo ?>
	                        </td>
	                        <td>
	                        	<?php echo substr($proposta->data_cadastro_br, 0, 10) ?>
	                        </td>
	                        <td class="hidden-xs">
	                        	<?php echo number_format($proposta->valor, 2, ',', '.') ?>
	                        </td>
                           <td class="hidden-xs">
                              <?php echo number_format($proposta->valor_final, 2, ',', '.') ?>
                           </td>
	                        <td>
	                        	<?php echo $proposta->qtd_parcelas ?>
	                        </td>
                           <td>
                              <span class="<?php echo $proposta->statusProposta->cssClass ?>">
                              <?php echo $proposta->statusProposta->status ?>
                              </span>
                           </td>
	                        <td class="center">
                              <?php if( $proposta->statusProposta->id == 2 ) { ?>
	                           <div class="visible-md visible-lg hidden-sm hidden-xs">	                              
	                             <a data-toggle="modal" href="#responsive" class="btn btn-xs btn-green tooltips btn-proposta-more-details" data-placement="top" data-original-title="Mais detalhes" modal-iframe-uri="<?php echo Yii::app()->request->baseUrl; ?>/proposta/maisDetalhes?id=<?php echo $proposta->id; ?>">
                                 <i class="clip-plus-circle"></i>
                                </a>
	                           </div>
                              <?php } ?>
	                        </td>
	                     </tr>
                     <?php } ?>
                  <?php } ?>
               </tbody>
            </table>
         </div>
      </div>
      <!-- end: DYNAMIC TABLE PANEL -->
   </div>
</div>

<div class="row">
   <div class="col-sm-12">
      <div class="alert alert-info" style="text-align:center">
         <strong>Total a pagar: R$ <?php echo number_format($numerosDePagamentos['emAberto'],2 ,',','.'); ?> </strong>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-sm-4">
      <div class="alert alert-success">
         Total pago até hoje: <strong>R$ <?php echo number_format($numerosDePagamentos['pago'],2 ,',','.'); ?></strong>
      </div>
   </div>
   <div class="col-sm-4">
      <div class="alert alert-warning">
         Vencendo hoje:<strong>R$ <?php echo number_format($numerosDePagamentos['vencendoHoje'],2 ,',','.'); ?></strong>
      </div>
   </div>
   <div class="col-sm-4">
      <div class="alert alert-danger">
         Em atraso:<strong>R$ <?php echo number_format($numerosDePagamentos['atrasado'],2 ,',','.'); ?></strong>
      </div>
   </div>
</div>

<div id="responsive" class="modal fade" tabindex="-1" data-width="1100" style="display: none;">
      
   <div class="modal-body">
      <iframe class="overflownone" style="backgroun:#FFF!important" width="100%" src="" scrolling="no" onload='javascript:resizeIframe(this);' id="iframe_mais_detalhes_proposta" frameborder="0"></iframe>
   </div>

   <div class="modal-footer">
      <button type="button" data-dismiss="modal" class="btn btn-light-grey">
      Fechar
      </button>      
   </div>

</div>


