<div class="form">

	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'cliente-form',	
		'enableAjaxValidation'=>false,
	)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($cadastroHasFilial,'Filial_id'); ?>
		<?php echo $form->dropDownList($cadastroHasFilial,'Filial_id', 
			CHtml::listData(Filial::model()->findAll(Yii::app()->session['usuario']->filiaisId('criteria')),
			'id','nome_fantasia' ));
		?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($pessoa,'nome'); ?>
		<?php echo $form->textField($pessoa,'nome'); ?>
		<?php echo $form->error($pessoa,'nome'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($pessoa,'nascimento'); ?>
		<?php echo $form->textField($pessoa,'nascimento'); ?>
		<?php echo $form->error($pessoa,'nascimento'); ?>
	</div>

	<div class="row">
		<label for=​"Documento_numero">CPF:</label>​
		<?php echo $form->textField($cpf,'numero'); ?>
		<?php echo $form->error($cpf,'numero'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($email,'email'); ?>
		<?php echo $form->textField($email,'email'); ?>
		<?php echo $form->error($email,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($telefone,'numero'); ?>
		<?php echo $form->textField($telefone,'numero'); ?>
		<?php echo $form->error($telefone,'numero'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($telefone,'Tipo_Telefone_id'); ?>
		<?php echo $form->dropDownList($telefone,'Tipo_Telefone_id', 
			CHtml::listData(TipoTelefone::model()->findAll(),
			'id','tipo' )); 
		?>
	</div>

	<!--ENDEREÇO-->
	<div class="row">
		<?php echo $form->labelEx($endereco,'logradouro'); ?>
		<?php echo $form->textField($endereco,'logradouro',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($endereco,'logradouro'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($endereco,'numero'); ?>
		<?php echo $form->textField($endereco,'numero'); ?>
		<?php echo $form->error($endereco,'numero'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($endereco,'complemento'); ?>
		<?php echo $form->textField($endereco,'complemento'); ?>
		<?php echo $form->error($endereco,'complemento'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($endereco,'cidade'); ?>
		<?php echo $form->textField($endereco,'cidade'); ?>
		<?php echo $form->error($endereco,'cidade'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($endereco,'bairro'); ?>
		<?php echo $form->textField($endereco,'bairro'); ?>
		<?php echo $form->error($endereco,'bairro'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($endereco,'uf'); ?>
		<?php echo $form->dropDownList($endereco,'uf', 
			CHtml::listData(Estados::model()->findAll(),
			'sigla','nome' )); 
		?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($endereco,'cep'); ?>
		<?php echo $form->textField($endereco,'cep'); ?>
		<?php echo $form->error($endereco,'cep'); ?>
	</div>
	
    <div class="row">
		<?php echo $form->labelEx($endereco,'Tipo_Endereco_id'); ?>
		<?php echo $form->dropDownList($endereco,'Tipo_Endereco_id', 
			CHtml::listData(TipoEndereco::model()->findAll(),
			'id','tipo' )); 
		?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Salvar' : 'Salvar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->