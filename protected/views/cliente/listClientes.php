<?php $util = new Util(); ?>
<div class="row">
   <div class="col-sm-12">
      <!-- start: PAGE TITLE & BREADCRUMB -->
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
            Clientes
            </a>
         </li>
         <li class="active">
            Listar
         </li>
      </ol>
      <div class="page-header">
         <h1>Clientes /<small>Filial: <?php echo substr(Yii::app()->session['usuario']->nomeFilial(), 0,10) ?> -
         <?php echo Yii::app()->session['usuario']->returnFilial()->getEndereco()->cidade ?> / <?php echo Yii::app()->session['usuario']->returnFilial()->getEndereco()->uf ?>:</small></h1>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-md-12">
      <!-- start: DYNAMIC TABLE PANEL -->
      <div class="panel panel-default">
         <div class="panel-heading">
            <i class="fa fa-external-link-square"></i>
            <div class="panel-tools">
               <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
               </a>                                          
            </div>
         </div>
         <div class="panel-body">
            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
               <thead>
                  <tr>
                     <th class="hidden-xs">Nome</th>
                     <th>CPF</th>
                     <th class="hidden-xs">Nascimento</th>
                     <th></th>
                  </tr>
               </thead>
               <tbody>
                  <?php foreach($clientes as $cliente) { ?>
                     <tr>
                        <td class="hidden-xs"><?php echo $cliente->pessoa->nome ?></td>
                        <td><?php echo $cliente->pessoa->getCPF()->numero ?></td>
                        <td class="hidden-xs">
                           <?php   
                                 if ( $cliente->pessoa->nascimento != NULL )
                                    echo $util->bd_date_to_view($cliente->pessoa->nascimento);
                                 else
                                    echo "__/__/__";
                           ?>
                        </td>
                        <td class="center">
                           <div class="visible-md visible-lg hidden-sm hidden-xs">
                              <form action="<?php echo Yii::app()->request->baseUrl;?>/cliente/editar" method="POST">
                                 <button class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Editar"><i class="fa fa-edit"></i></button>
                                 <input type="hidden" value="<?php echo $cliente->id ?>" name="idCliente">
                              </form>
                           </div>
                        </td>
                     </tr>
                  <?php } ?>
               </tbody>
            </table>
         </div>
      </div>
      <!-- end: DYNAMIC TABLE PANEL -->
   </div>
</div>