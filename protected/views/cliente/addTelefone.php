<?php  ?>
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap.min.css" rel="stylesheet" media="screen">
<form>
   <div class="row">
        <div class="col-md-8">
	        <div class="form-group">
	        	<label class="control-label">Telefone</label>
            <input id="numero" class="form-control" type="text" name="numero" value="">
            <input id="tipoInput" class="form-control" type="hidden" name="tipo" value="<?php echo "telefone"; ?>">
            <input id="idContato" class="form-control" type="hidden" name="contato_id" value="<?php echo $contato_id; ?>">
	        </div>
          <div class="form-group">
            <label class="control-label">Tipo</label>
            <select name="tipo_telefone" id="tipoTelefone" class="form-control">
                <?php foreach( TipoTelefone::model()->findAll() as $tipoTelefone ) {?>
                    <option <?php if ( $entity->Tipo_Telefone_id == $tipoTelefone->id ) { echo " selected "; } ?> value="<?php echo $tipoTelefone->id ?>"><?php echo $tipoTelefone->tipo; ?></option>
                <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label class="control-label">Ramal</label>
            <input id="ramal" class="form-control" type="text" name="ramal" value="<?php echo $entity->ramal; ?>">
          </div>
	   		<div class="form-group">
      			<button id="sbmt" type="submit" class="btn btn-success save-event"><i class="fa fa-check"></i> Salvar</button>
	   		</div>     
        </div>
   </div>
</form>

<script type="text/javascript">

      $('#sbmt').on('click',function(){            

          $.ajax({

              type :  'POST',
              url  : '/cliente/addContatoElement/',
              
              data : {
                'numero' : $('#numero').val(),
                'tipo_telefone' : $("#tipoTelefone option:selected").val(),
                'ramal' : $('#ramal').val(),
                'tipo'  : $('#tipoInput').val(),
                'contato_id'  : $('#idContato').val(),
              },

              success : function ( data ){

                  var jsonReturn = $.parseJSON(data);

                  var newRow  =   '<tr class="odd">';
                      newRow +=   ' <td id="telefone_'+jsonReturn.telefoneId+'_numero_return">'+jsonReturn.telefoneNumero+'</td>';
                      newRow +=   ' <td id="telefone_'+jsonReturn.telefoneId+'_tipo_return">'+jsonReturn.telefoneTipo+'</td>';
                      newRow +=   ' <td id="telefone_'+jsonReturn.telefoneId+'_ramal_return">'+jsonReturn.telefoneRamal+'</td>';
                      newRow +=   ' <td>';
                      newRow +=   '  <a tr-id="telefone_row_'+jsonReturn.telefoneId+'" modal-iframe-uri="'+<?php echo json_encode(Yii::app()->request->baseUrl); ?>+'/cliente/renderFormChangeContato?tipo=telefone&view=changeTelefone&id='+jsonReturn.telefoneId+'"'+' data-toggle="modal" href="#stack3">';
                      newRow +=   '   <i tr-id="telefone_row_'+jsonReturn.telefoneId+'" modal-iframe-uri="'+<?php echo json_encode(Yii::app()->request->baseUrl); ?>+'/cliente/renderFormChangeContato?tipo=telefone&view=changeTelefone&id='+jsonReturn.telefoneId+'"'+' class="fa fa-pencil edit-user-info edit-customer-contact-info"></i>';
                      newRow +=   '  </a>';
                      newRow +=   '  <a data-toggle="modal" href="#">';
                      newRow +=   '   <i class="fa fa-trash-o edit-user-info"></i>';
                      newRow +=   '  </a>';
                      newRow +=   ' </td>';
                      newRow +=   '</tr>';

                      window.parent.$('#addFormEl').modal('hide')
                      window.parent.$('#table-telefones tbody tr').last().after(newRow);

              }

          })

          return false;

      })
</script>