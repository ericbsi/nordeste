<?php  ?>
<meta charset="utf-8" />
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/login/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/style.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/main.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/main-responsive.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap-colorpalette.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/perfect-scrollbar.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/select2.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/summernote.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/DT_bootstrap.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap-modal-bs3patch.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap-modal.css">

<form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/cliente/updateEnderecoProfissional">
   <div class="row">
      <div class="col-sm-12">
         <div class="panel" style="border:none!important;">
            <div class="col-md-6">
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label">Classe Profissional</label>
                         <select id="" name="" class="form-control search-select">
                              <?php foreach( ClasseProfissional::model()->findAll() as $classeProfissional ) { ?>
                                 <option <?php if( $DadosProfissionais->Classe_Profissional_id == $classeProfissional->id ){ echo "selected"; } ?> value="<?php echo $classeProfissional->id ?>"><?php echo $classeProfissional->classe ?></option>
                              <?php } ?>
                         </select>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label">CNPJ/CPF</label>
                        <input class="form-control" type="text" name="DadosProfissionais[cnpj_cpf]" value="<?php echo $DadosProfissionais->cnpj_cpf ?>">
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label">Empresa</label>
                        <input class="form-control" type="text" name="DadosProfissionais[empresa]" value="<?php echo $DadosProfissionais->empresa ?>">
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label">Ocupação atual</label>
                        <select id="" name="" class="form-control search-select">
                              <?php foreach( TipoOcupacao::model()->findAll() as $tipoOcupacao ) { ?>
                                 <option <?php if( $DadosProfissionais->Tipo_Ocupacao_id == $tipoOcupacao->id ){ echo "selected"; } ?> value="<?php echo $classeProfissional->id ?>"><?php echo $tipoOcupacao->tipo ?></option>
                              <?php } ?>
                         </select>
                     </div>
                  </div>
               </div>
               <?php $util = new Util; ?>
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label">Data de admissão</label>
                        <input class="form-control ipt_date" type="text" name="DadosProfissionais[data_admissao]" value="<?php echo $util->bd_date_to_view($DadosProfissionais->data_admissao) ?>">
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label">Profissão</label>
                           <select id="select_profissoes" name="DadosProfissionais[profissao]" class="form-control search-select">
                              <?php foreach( Profissao::model()->findAll() as $profissao ) { ?>
                                 <option <?php if( $DadosProfissionais->profissao == $profissao->profissao ){ echo "selected"; } ?> value="<?php echo $classeProfissional->id ?>"><?php echo $profissao->profissao ?></option>
                              <?php } ?>
                           </select>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label">Renda Líquida R$</label>
                        <input value="<?php echo number_format($DadosProfissionais->renda_liquida, 2, '.', '') ?>" class="form-control dinheiro" type="text" name="DadosProfissionais[renda_liquida]">
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label">Mês / Ano Renda</label>
                        <input value="<?php echo $DadosProfissionais->mes_ano_renda ?>" class="form-control mes_ano_renda" type="text" name="DadosProfissionais[mes_ano_renda]">
                     </div>
                  </div>
               </div>
               <!--<div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label">Pensionista</label>
                        <?php echo CHtml::dropDownList('DadosProfissionais[pensionista]','DadosProfissionais[pensionista]',
                     CHtml::listData(TTable::model()->findAll(),
                     'id','flag' ), array('class'=>'form-control search-select')); 
                     ?> 
                     </div>
                  </div>
                  </div>-->
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label">Órgão Pagador</label>
                        <?php echo CHtml::dropDownList('DadosProfissionais[orgao_pagador]','DadosProfissionais[orgao_pagador]',
                           CHtml::listData(OrgaoPagador::model()->findAll(),
                              'nome','nome' ), array('class'=>'form-control search-select')); 
                              ?> 
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label">N° do benefício</label>
                        <input class="form-control" type="text" name="DadosProfissionais[numero_do_beneficio]">
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label">Tipo de comprovante</label>
                        <?php echo CHtml::dropDownList('DadosProfissionais[TipoDeComprovante]','DadosProfissionais[TipoDeComprovante]',
                           CHtml::listData(TipoDeComprovante::model()->findAll(),
                              'tipo','tipo' ), array('class'=>'form-control search-select')); 
                              ?> 
                     </div>
                  </div>
               </div>
            </div>
            <!--Endereço/Contato Profissional-->
            <div class="col-md-6">
               <div class="row">
                  <div class="col-md-3">
                     <div class="form-group">
                        <label class="control-label">CEP</label>
                        <input value="<?php echo $enderecoProfissao->cep ?>" id="cep_dados_profissionais" class="form-control cep" type="text" name="EnderecoProfiss[cep]">
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label">Logradouro</label>
                        <input value="<?php echo $enderecoProfissao->logradouro ?>" id="logradouro_dados_profissionais" class="form-control" type="text" name="EnderecoProfiss[logradouro]">
                     </div>
                  </div>
                  <div class="col-md-2">
                     <div class="form-group">
                        <label class="control-label">Número</label>
                        <input  class="form-control" type="text" name="EnderecoProfiss[numero]" value="<?php echo $enderecoProfissao->numero ?>">
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-4">
                     <div class="form-group">
                        <label class="control-label">Cidade</label>
                        <input value="<?php echo $enderecoProfissao->cidade ?>" id="cidade_dados_profissionais" class="form-control" type="text" name="EnderecoProfiss[cidade]">
                     </div>
                  </div>
                  <div class="col-md-8">
                     <div class="form-group">
                        <label class="control-label">Bairro</label>
                        <input value="<?php echo $enderecoProfissao->bairro ?>" id="bairro_dados_profissionais" class="form-control" type="text" name="EnderecoProfiss[bairro]">
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-4">
                     <div class="form-group">
                        <label class="control-label">UF</label>
                        <select id="select_profissoes" name="EnderecoProfiss[uf]" class="form-control search-select">
                           <?php foreach( Estados::model()->findAll() as $uf ) { ?>
                              <option <?php if( $enderecoProfissao->uf == $uf->sigla ){ echo "selected"; } ?> value="<?php echo $enderecoProfissao->uf ?>"><?php echo $enderecoProfissao->uf ?></option>
                           <?php } ?>
                        </select>
                     </div>
                  </div>
                  <div class="col-md-8">
                     <div class="form-group">
                        <label class="control-label">Complemento</label>
                        <input class="form-control" type="text" name="EnderecoProfiss[complemento]" value="<?php echo $enderecoProfissao->complemento ?>">
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-4">
                     <div class="form-group">
                        <label class="control-label">Telefone</label>
                        <input class="form-control telefone" type="text" name="TelefoneProfiss[numero]">
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label class="control-label">Ramal</label>
                        <input class="form-control" type="text" name="TelefoneProfiss[ramal]">
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label class="control-label">Tipo de telefone</label>
                        <?php echo CHtml::dropDownList('TelefoneProfiss[Tipo_Telefone_id]','TelefoneProfiss[Tipo_Telefone_id]',
                           CHtml::listData(TipoTelefone::model()->findAll(),
                              'id','tipo' ), array('class'=>'form-control search-select')); 
                              ?> 
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-4">
                     <div class="form-group">
                        <label class="control-label">Email</label>
                        <input class="form-control" type="text" name="EmailProfiss[email]">
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-6" style="float:right">
                     <div class="form-group">
                     <label class="control-label">&nbsp;</label>
                        <input type="submit" value="Atualizar" class="btn btn-blue btn-block">                     
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</form>