<?php

$this->breadcrumbs=array(
	'Clientes'=>array('index'),
	'Administrar',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#cliente-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Clientes</h1>


<?php echo CHtml::link('Busca Avançada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'cliente-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(		
		array(
            'header'=>'Nome',
            'value'=>'$data->pessoa->nome',
            'type'=>'text',
        ),
        array(
            'header'=>'Nascimento',
            'value'=>'$data->pessoa->nascimento',
            'type'=>'date',
        ),
		
		array(
            'header'=>'CPF',
            'value'=>'$data->pessoa->returnCPF()',
            'type'=>'date',
        ),

		array(
			'class'=>'CButtonColumn',
			'template'=>'{update}'
		),		
	),
)); ?>
