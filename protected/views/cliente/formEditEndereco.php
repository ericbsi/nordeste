<?php  ?>
<meta charset="utf-8" />
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/login/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/style.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/main.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/main-responsive.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap-colorpalette.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/perfect-scrollbar.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/select2.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/summernote.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/DT_bootstrap.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap-modal-bs3patch.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap-modal.css">
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl ?>/js/form-editar-endereco-cliente.js"></script>
<form id="formEditarEndereco" method="POST">
  
   <div class="row">
      
      <div class="col-sm-12">
         
         <div class="panel" style="border:none!important;">
            
            <div class="col-md-6">
               <div class="row">
                  <div class="col-md-3">
                     <div class="form-group">
                        <label class="control-label">
                        CEP <span class="symbol required"></span>
                        </label>
                        <input class="form-control cep" type="text" name="EnderecoCliente[cep]" id="cep_cliente" value="<?php echo $Endereco->cep ?>">
                        <input type="hidden" value="<?php echo $Endereco->id ?>" name="EnderecoCliente[id]" >
                     </div>
                  </div>
                  <div class="col-md-9">
                     <div class="form-group">
                        <label class="control-label">
                        Logradouro <span class="symbol required"></span>
                        </label>
                        <input value="<?php echo $Endereco->logradouro ?>" class="form-control" type="text" name="EnderecoCliente[logradouro]" id="logradouro_cliente">
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label">Cidade <span class="symbol required"></span></label>
                        <input value="<?php echo $Endereco->cidade ?>" class="form-control" type="text" name="EnderecoCliente[cidade]" id="cidade_cliente">
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label">Bairro <span class="symbol required"></span></label>
                        <input value="<?php echo $Endereco->bairro ?>" class="form-control" type="text" name="EnderecoCliente[bairro]" id="bairro_cliente">
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label">
                        Tempo de residência (mes/ano) <span class="symbol required"></span>
                        </label>
                        <input value="<?php echo $Endereco->tempo_de_residencia ?>" class="form-control" type="text" name="EnderecoCliente[tempo_de_residencia]" id="tempo_de_residencia">
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label">Tipo de moradia <span class="symbol required"></span></label>
                        <select name="EnderecoCliente[Tipo_Moradia_id]" class="form-control search-select">
                           <?php  foreach( TipoMoradia::model()->findAll() as $tipoMoradia ){ ?>
                              <option <?php if( $Endereco->Tipo_Moradia_id == $tipoMoradia->id ) { echo "selected"; } ?> value="<?php echo $tipoMoradia->id ?>"><?php echo $tipoMoradia->tipo ?></option>
                           <?php } ?>
                        </select>
                     </div>
                  </div>
               </div>
            </div>
            
            <div class="col-md-6">
               <div class="row">
                  <div class="col-md-3">
                     <div class="form-group">
                        <label class="control-label">Número <span class="symbol required"></span></label>
                        <input id="numero_end_cliente" value="<?php echo $Endereco->numero ?>" class="form-control" type="text" name="EnderecoCliente[numero]">
                     </div>
                  </div>
                  <div class="col-md-9">
                     <div class="form-group">
                        <label class="control-label">Complemento</label>
                        <input id="complemento_end_cliente" value="<?php echo $Endereco->complemento ?>" class="form-control" type="text" name="EnderecoCliente[complemento]">
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label">UF <span class="symbol required"></span></label>
                        <select id="EnderecoCliente_uf" name="EnderecoCliente[uf]" class="form-control search-select">
                           <?php foreach( Estados::model()->findAll() as $uf ) { ?>
                              <option <?php if( $Endereco->uf == $uf->sigla ){ echo "selected"; } ?> value="<?php echo $uf->sigla ?>"><?php echo $uf->sigla ?></option>
                           <?php } ?>
                        </select>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label">Tipo de endereço <span class="symbol required"></span></label>
                        <select name="EnderecoCliente[Tipo_Endereco_id]" class="form-control search-select">
                              <?php  foreach( TipoEndereco::model()->findAll() as $TipoEndereco ){ ?>
                                 <option <?php if( $Endereco->Tipo_Endereco_id == $TipoEndereco->id ) { echo "selected"; } ?> value="<?php echo $TipoEndereco->id ?>"><?php echo $TipoEndereco->tipo ?></option>
                              <?php } ?>
                        </select>

                     </div>
                  </div>
               </div>
               <div class="col-md-6">
               </div>
            </div>

         </div>

         <div class="wizard-step-btns-wrapper">
            
            <div class="col-md-4" style="float:left">
            </div>
            
            <div class="col-md-4" style="float:right">
               <div class="form-group">
                  <button id="submit-form-editar-endereco-cliente" class="btn btn-blue next-step btn-block">Atualizar <i class="fa fa-arrow-circle-right"></i></button>
               </div>
            </div>

         </div>

      </div>

   </div>

</form>