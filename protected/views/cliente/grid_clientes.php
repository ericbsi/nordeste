<div class="row">
   <div class="col-sm-12">
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/cliente/listarClientes">
            Clientes
            </a>
         </li>
         <li class="active">
            Listar
         </li>
      </ol>
      <div class="page-header">
         <h1>
         Clientes
      </div>
   </div>
</div>

<p>
   <a class="btn btn-success" href="#modal_form_new_client" data-toggle="modal" id="btn_modal_form_new_client">
   Cadastrar Cliente <i class="fa fa-plus"></i>
   </a>
</p>

<p>
   
</p>

<div class="row">
   <div class="col-sm-12">
      <table id="grid_clientes" class="table table-striped table-bordered table-hover table-full-width dataTable">
         <thead>
            <tr>
               <th>Nome</th>
               <th class="no-orderable">CPF</th>
               <th class="no-orderable">Sexo</th>
               <th class="no-orderable">Nascimento</th>
               <th class="no-orderable"></th>
            </tr>
         </thead>
         <tfoot>
            <tr>
               <th class="searchable">Nome</th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
            </tr>
         </tfoot>
      </table>
   </div>
</div>
<div id="modal_form_new_client" class="modal fade" tabindex="-1" data-width="960" style="display: none;">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
      &times;
      </button>
      <h4 class="modal-title">Cadastrar Cliente</h4>
   </div>
   <form id="form-add-client">
      <div class="modal-body">
         <div class="row">
            <div class="row-centralize">
               <div class="col-md-6">
                  <div class="row">
                     <div class="col-md-11">
                        <div class="form-group">
                           <label class="control-label">
                              Nome completo <span class="symbol required"></span>
                           </label>
                           <input value="<?php if( isset($nome) && !empty($nome) ) { echo $nome; } ?>" required name="Pessoa[nome]" type="text" class="form-control">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-5">
                        <div class="form-group">
                           <label class="control-label">
                           Nascimento <span class="symbol required"></span>
                           </label>
                           <input required name="Pessoa[nascimento]" id="nascimento" class="dateBR form-control input-mask-date" type="text">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Sexo <span class="symbol required"></span></label>
                           <?php echo CHtml::dropDownList('Pessoa[sexo]','Pessoa[sexo]',
                              CHtml::listData(Sexo::model()->findAll(),
                              'sigla','sexo' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:','required'=>true)); 
                              ?>  
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-5">
                        <div class="form-group">
                           <label class="control-label">
                           CPF <span class="symbol required"></span>
                           </label>
                           <input value="<?php if( isset($cpf) && !empty($cpf) ) { echo $cpf; } ?>" required id="cpf_numero" name="CPFCliente[numero]" type="text" class="form-control cpf">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Estado Civil <span class="symbol required"></span></label>
                           <?php echo CHtml::dropDownList('Pessoa[Estado_Civil_id]','Pessoa[Estado_Civil_id]',
                              CHtml::listData(EstadoCivil::model()->findAll(),
                              'id','estado' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:','required'=>true)); 
                              ?>  
                        </div>
                     </div>
                  </div><!---->
                  <div class="row">
                     <div class="col-md-5">
                        <div class="form-group">
                           <label class="control-label">
                           RG <span class="symbol required"></span>
                           </label>
                           <input required id="rg_numero" name="DocumentoCliente[numero]" type="text" class="form-control">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">
                           Órgão Emissor <span class="symbol required"></span>
                           </label>
                           <input required type="text" name="DocumentoCliente[orgao_emissor]" class="form-control">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-5">
                        <div class="form-group">
                           <label class="control-label">
                           UF Emissor <span class="symbol required"></span>
                           </label>
                           <?php echo CHtml::dropDownList('DocumentoCliente[uf_emissor]','DocumentoCliente[uf_emissor]',
                              CHtml::listData(Estados::model()->findAll(),
                                 'sigla','nome' ), array('class'=>'form-control search-select select2', 'prompt'=>'Selecione:','required'=>true)
                              );
                           ?>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">
                           Data de emissão <span class="symbol required"></span>
                           </label>
                           <input required class="dateBR form-control input-mask-date" type="text" name="DocumentoCliente[data_emissao]">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="row">
                     <div class="col-md-5">
                        <div class="form-group">
                           <label class="control-label">Nacionalidade <span class="symbol required"></span></label>
                           <select required="true" id="select-nacionalidade" name="Pessoa[nacionalidade]" class="draft_sent form-control search-select select2">
                              <option value="">Selecione:</option>
                              <option value="Brasileiro(a)">Brasileira</option>
                              <option value="Estrangeiro(a)">Estrangeira</option>
                           </select>
                        </div>
                     </div>
                                       
                     <div class="col-md-7" id="select-estados-wrapper">
                        <div class="form-group">
                           <label class="control-label">UF <span class="symbol required"></span></label>
                           <?php echo CHtml::dropDownList('Pessoa[naturalidade]','Pessoa[naturalidade]',
                              CHtml::listData(Estados::model()->findAll(),
                              'sigla','nome' ), array('class'=>'form-control search-select select2', 'prompt'=>'Selecione:','required'=>true));
                           ?>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-12" id="select-estados-wrapper">
                        <div class="form-group">
                           <label class="control-label">Cidade <span class="symbol required"></span></label>
                           <input required="true" name="Pessoa[naturalidade_cidade]" placeholder="Selecione" type="hidden" id="selectCidades" class="form-control search-select">
                        </div>
                     </div>
                     <div class="col-md-12" id="select-estados-wrapper">
                        <div class="form-group">
                           <label class="control-label">Nome da mãe <span class="symbol required"></span></label>
                           <input required class="form-control" type="text" name="Cadastro[nome_da_mae]">
                        </div>
                     </div>
                     <div class="col-md-12" id="select-estados-wrapper">
                        <div class="form-group">
                           <label class="control-label">Nome do pai <span class="symbol required"></span></label>
                           <input class="form-control" type="text" name="Cadastro[nome_do_pai]">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal-footer">
         <label class="checkbox-inline" style="float:left">
            <input id="checkbox_continuar" type="checkbox" value="">
            Continuar Cadastrando
         </label>
         <div class="panel"></div>
         <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
         <button type="submit" class="btn btn-blue">Salvar</button>
         <div class="row">
         </div>
         <br>
         <div id="cadastro_msg_return" class="alert" style="text-align:left">
         </div>
      </div>
   </form>
</div>
<style type="text/css">
   #grid_clientes_filter, #grid_clientes_length{
      display: none!important;
   }
   .panel{
      background: transparent!important;
      border:none!important;
   }
</style>