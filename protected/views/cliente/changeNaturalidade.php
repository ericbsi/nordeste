<?php 
?>

<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap.min.css" rel="stylesheet" media="screen">

<div class="col-md-6">
  
   <h4>Naturalidade</h4>
  
   <form method="POST" action="">
      
      <p>
         
         <?php if ( $cliente->pessoa->nacionalidade == 'Brasileira' ) { $estados = Estados::model()->findAll(); ?>
            <select id="form-field-select-3" class="form-control search-select" name="sexo">

               <?php foreach ($estados as $estado) { ?>
                  <option <?php if( $cliente->pessoa->naturalidade == $estado->sigla ) { echo "selected"; } ?> value="<?php echo $estado->sigla; ?>">
                     <?php echo $estado->nome; ?>
                  </option>
               <?php } ?>
            </select>

         <?php } else { $paises = Pais::model()->findAll(); ?>

            <select id="form-field-select-3" class="form-control search-select" name="sexo">

               <?php foreach( $paises as $pais ){ ?>

                  <option <?php if( $cliente->pessoa->naturalidade == $pais->paisNome ){ echo "selected"; } ?> value="<?php echo $pais->paisNome; ?>">
                     <?php echo $pais->paisNome; ?>
                  </option>

               <?php } ?>

            </select>

         <?php } ?>

         <input type="hidden" value="naturalidade" id="attribute">
         <input type="hidden" value="<?php echo $cliente->pessoa->id ?>" id="pessoa_id">

      </p>

      <p>
            <button id="sbmt" class="btn btn-yellow btn-block" type="submit">
                  Salvar <i class="fa fa-arrow-circle-right"></i>
            </button>
      </p>

   </form>

</div>

<script type="text/javascript">
      $('#sbmt').on('click',function(){

            $.ajax({

                  type :  'POST', 
                  url  : '/cliente/changeClienteAttribute/',
                  data : {
                        'attribute_value' : $('#form-field-select-3').val(),
                        'attribute' : $('#attribute').val(),
                        'pessoa_id' : $('#pessoa_id').val(),
                  },

                  success : function (data){
                        window.parent.$('#stack1').modal('hide')
                        window.parent.$('#naturalidade_pessoa').html(data);
                  }
            })

            return false;
      })

</script>