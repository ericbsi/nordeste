<?php  ?>
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap.min.css" rel="stylesheet" media="screen">
<form method="POST" action="">
   <div class="row">
        <div class="col-md-8">
	        <div class="form-group">
	        	<label class="control-label">E-mail</label>
            <input id="emailInput" class="form-control" type="text" name="email" value="">
            <input id="tipoInput" class="form-control" type="hidden" name="tipo" value="<?php echo "email"; ?>">
            <input id="idContato" class="form-control" type="hidden" name="contato_id" value="<?php echo $contato_id; ?>">
	        </div>
	   		<div class="form-group">
      			<button id="sbmt" type="submit" class="btn btn-success save-event"><i class="fa fa-check"></i>Adicionar</button>
	   		</div>     
        </div>
   </div>
</form>

<script type="text/javascript">

      $('#sbmt').on('click',function(){            

            $.ajax({

                  type :  'POST',
                  url  : '/cliente/addContatoElement/',
                  data : {
                        'email' : $('#emailInput').val(),
                        'tipo'  : $('#tipoInput').val(),
                        'contato_id'  : $('#idContato').val(),
                  },

                  success : function ( data ){

                        var jsonReturn = $.parseJSON(data);
                        var newRow  =   '<tr class="odd" id="email_row_'+jsonReturn.emailId+'">';
                            newRow +=   ' <td id="email_'+jsonReturn.emailId+'_return">'+jsonReturn.email+'</td>';
                            newRow +=   ' <td>';
                            newRow +=   '  <a tr-id="email_row_'+jsonReturn.emailId+'" modal-iframe-uri="'+<?php echo json_encode(Yii::app()->request->baseUrl); ?>+'/cliente/renderFormChangeContato?tipo=email&view=changeEmail&id='+jsonReturn.emailId+'"'+' data-toggle="modal" href="#stack3">';
                            newRow +=   '   <i tr-id="email_row_'+jsonReturn.emailId+'" modal-iframe-uri="'+<?php echo json_encode(Yii::app()->request->baseUrl); ?>+'/cliente/renderFormChangeContato?tipo=email&view=changeEmail&id='+jsonReturn.emailId+'"'+' class="fa fa-pencil edit-user-info edit-customer-contact-info"></i>';
                            newRow +=   '  </a>';
                            newRow +=   '  <a data-toggle="modal" href="#">';
                            newRow +=   '   <i class="fa fa-trash-o edit-user-info"></i>';
                            newRow +=   '  </a>';
                            newRow +=   ' </td>';
                            newRow +=   '</tr>';
                        window.parent.$('#addFormEl').modal('hide')
                        window.parent.$('#table-emails tbody tr').last().after(newRow);
                  }
            })

            return false;            
      })
</script>