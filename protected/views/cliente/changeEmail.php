<?php  ?>
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap.min.css" rel="stylesheet" media="screen">
<form>
   <div class="row">
        <div class="col-md-8">
	        <div class="form-group">
	        	<label class="control-label">E-mail</label>
            <input id="emailInput" class="form-control" type="text" name="email" value="<?php echo $entity->getAttribute('email'); ?>">
            <input id="tipoInput" class="form-control" type="hidden" name="tipo" value="<?php echo "email"; ?>">
	        	<input id="idInput" class="form-control" type="hidden" name="id" value="<?php echo $entity->getAttribute('id'); ?>">
	        </div>
	   		<div class="form-group">
	   			  <!--<button type="button" data-dismiss="modal" class="btn btn-light-grey">Fechar</button>-->
      			<button id="sbmt" type="submit" class="btn btn-success save-event"><i class="fa fa-check"></i> Salvar</button>
	   		</div>     
        </div>
   </div>
</form>

<script type="text/javascript">
      $('#sbmt').on('click',function(){            

             $.ajax({

                  type :  'POST', 
                  url  : '/cliente/changeClienteContatoAttribute/',
                  data : {
                        'email' : $('#emailInput').val(),
                        'id' : $('#idInput').val(),
                        'tipo' : $('#tipoInput').val(),
                  },

                  success : function ( data ){
                        window.parent.$('#stack3').modal('hide')
                        window.parent.$('#email_' + $('#idInput').val() + '_return').html( $('#emailInput').val() );
                  }
            })

            return false;            
      })
</script>