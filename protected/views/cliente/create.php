<?php

$this->breadcrumbs=array(
	'Clientes'=>array('index'),
	'Cadastrar',
);
?>

<?php 
	$this->renderPartial(
		'_form', array(
			'model'=>$model,
			'pessoa'=>$pessoa,
			'cpf'=>$cpf,
			'endereco'=>$endereco,
			'telefone'=>$telefone,
			'email'=>$email,
			'endereco'=>$endereco,
			'cadastroHasFilial'=>$cadastroHasFilial,
	)); 
?>