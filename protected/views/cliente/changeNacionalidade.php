<?php
     $sexos = Sexo::model()->findAll();
     $nacionalidades = array('Brasileira', 'Extrangeira');
?>

<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap.min.css" rel="stylesheet" media="screen">

<div class="col-md-6">
   <h4>Nacionalidade</h4>
   <form method="POST" action="">
      <p>
         <select id="form-field-select-3" class="form-control search-select" name="sexo">

            <?php for ( $i=0; $i < count( $nacionalidades ); $i++ ) { ?>
               
               <option  <?php if( $cliente->pessoa->nacionalidade == $nacionalidades[$i] ) { echo "selected"; } ?> value="<?php echo $nacionalidades[$i]; ?>">
                     <?php echo $nacionalidades[$i]; ?>
               </option> 
         
            <?php } ?>
         </select>
         <input type="hidden" value="nacionalidade" id="attribute">
         <input type="hidden" value="<?php echo $cliente->pessoa->id ?>" id="pessoa_id">
      </p>
      <p>
            <button id="sbmt" class="btn btn-yellow btn-block" type="submit">
                  Salvar <i class="fa fa-arrow-circle-right"></i>
            </button>
      </p>
   </form>
</div>

<!--<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.pnotify.min.js"></script>-->

<script type="text/javascript">
      $('#sbmt').on('click',function(){

            $.ajax({

                  type :  'POST', 
                  url  : '/cliente/changeClienteAttribute/',
                  data : {
                        'attribute_value' : $('#form-field-select-3').val(),
                        'attribute' : $('#attribute').val(),
                        'pessoa_id' : $('#pessoa_id').val(),
                  },

                  success : function (data){
                        window.parent.$('#stack1').modal('hide')
                        window.parent.$('#nacionalidade_pessoa').html(data);
                  }
            })

            return false;            
      })

      

</script>