<div class="row">
   <div class="col-sm-12">
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/cliente/listarClientes">
            Clientes
            </a>
         </li>
         <li class="active">
            Buscar
         </li>
      </ol>
      <div class="page-header">
         <h1>
         Histórico de Compras - Credshow
      </div>
   </div>
</div>
<div class="row">
   <div class="col-sm-12">
		<?php if( isset( $propostas ) && count( $propostas ) > 0 ): ?>
			<table id="grid_propostas" class="table table-striped table-bordered table-hover table-full-width dataTable">
			    <thead>
			    	<tr>
				       <th>Cód</th>
		               <th>Data</th>
		               <th>Valor Inicial</th>
		               <th>Entrada</th>
		               <th>Qtd Parcelas</th>
		               <th>Imprimir</th>
			        </tr>
			    </thead>
			    <tbody>
			    	
			    	<?php foreach( $propostas as $proposta ): ?>
			    		<tr>
			    			<td><?php echo $proposta['codigo'] ?></td>
			    			<td><?php echo $proposta['data'] ?></td>
			    			<td><?php echo $proposta['valor_soli'] ?></td>
			    			<td><?php echo $proposta['entrada'] ?></td>
			    			<td><?php echo $proposta['parcelas'] ?></td>
			    			<td>
			    				<form target="_blank" method="post" action="<?php echo $proposta['urlPrint']; ?>">	
			    					<input type="hidden" name="id" value="<?php echo $proposta['id'] ?>">	
			    					<button style="padding:6px;font-size:8px;" type="submit" class="btn btn-primary">
			    						<i class="clip-search"></i>
			    					</button>
			    				</form>
			    			</td>
			    		</tr>
			    	<?php endforeach; ?>

				</tbody>
			</table>
		<?php endif; ?>
   </div>
</div>
<style type="text/css">
	#grid_propostas_filter, #grid_propostas_length{
		display: none;
	}
</style>