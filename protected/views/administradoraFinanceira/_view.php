<?php
/* @var $this AdministradoraFinanceiraController */
/* @var $data AdministradoraFinanceira */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nome')); ?>:</b>
	<?php echo CHtml::encode($data->nome); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Forma_de_Pagamento_id')); ?>:</b>
	<?php echo CHtml::encode($data->Forma_de_Pagamento_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TipoAdministradoraFinanceira_id')); ?>:</b>
	<?php echo CHtml::encode($data->TipoAdministradoraFinanceira_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('habilitado')); ?>:</b>
	<?php echo CHtml::encode($data->habilitado); ?>
	<br />


</div>