<?php
/* @var $this AdministradoraFinanceiraController */
/* @var $model AdministradoraFinanceira */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'administradora-financeira-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nome'); ?>
		<?php echo $form->textField($model,'nome',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'nome'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Forma_de_Pagamento_id'); ?>
		<?php echo $form->dropDownList($model,'Forma_de_Pagamento_id',$this->getAllFormaDePagamento()); ?>
		<?php echo $form->error($model,'Forma_de_Pagamento_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'TipoAdministradoraFinanceira_id'); ?>
		<?php echo $form->dropDownList($model,'TipoAdministradoraFinanceira_id',$this->getAllTipoAdministradoraFinanceira()); ?>
		<?php echo $form->error($model,'TipoAdministradoraFinanceira_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'habilitado'); ?>
		<?php echo $form->dropDownList($model,'habilitado',array('1' => 'Sim', '0' => 'Não')); ?>
		<?php echo $form->error($model,'habilitado'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->