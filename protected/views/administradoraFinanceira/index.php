<?php
/* @var $this AdministradoraFinanceiraController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Administradora Financeiras',
);

$this->menu=array(
	array('label'=>'Create AdministradoraFinanceira', 'url'=>array('create')),
	array('label'=>'Manage AdministradoraFinanceira', 'url'=>array('admin')),
);
?>

<h1>Administradora Financeiras</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
