<?php
/* @var $this AdministradoraFinanceiraController */
/* @var $model AdministradoraFinanceira */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nome'); ?>
		<?php echo $form->textField($model,'nome',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Forma_de_Pagamento_id'); ?>
		<?php echo $form->textField($model,'Forma_de_Pagamento_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'TipoAdministradoraFinanceira_id'); ?>
		<?php echo $form->textField($model,'TipoAdministradoraFinanceira_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'habilitado'); ?>
		<?php echo $form->textField($model,'habilitado'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->