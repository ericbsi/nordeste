<?php
/* @var $this AdministradoraFinanceiraController */
/* @var $model AdministradoraFinanceira */

$this->breadcrumbs=array(
	'Administradora Financeiras'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List AdministradoraFinanceira', 'url'=>array('index')),
	array('label'=>'Create AdministradoraFinanceira', 'url'=>array('create')),
	array('label'=>'Update AdministradoraFinanceira', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AdministradoraFinanceira', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AdministradoraFinanceira', 'url'=>array('admin')),
);
?>

<h1>View AdministradoraFinanceira #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nome',
		'Forma_de_Pagamento_id',
		'TipoAdministradoraFinanceira_id',
		'habilitado',
	),
)); ?>
