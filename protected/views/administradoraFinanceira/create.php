<?php
/* @var $this AdministradoraFinanceiraController */
/* @var $model AdministradoraFinanceira */

$this->breadcrumbs=array(
	'Administradora Financeiras'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AdministradoraFinanceira', 'url'=>array('index')),
	array('label'=>'Manage AdministradoraFinanceira', 'url'=>array('admin')),
);
?>

<h1>Create AdministradoraFinanceira</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>