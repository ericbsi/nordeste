<?php
/* @var $this AdministradoraFinanceiraController */
/* @var $model AdministradoraFinanceira */

$this->breadcrumbs=array(
	'Administradora Financeiras'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AdministradoraFinanceira', 'url'=>array('index')),
	array('label'=>'Create AdministradoraFinanceira', 'url'=>array('create')),
	array('label'=>'View AdministradoraFinanceira', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AdministradoraFinanceira', 'url'=>array('admin')),
);
?>

<h1>Update AdministradoraFinanceira <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>