<?php  ?>

<div class="row">
   <div class="col-sm-12">
      
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
               Cobranças
            </a>
         </li>
         <li class="active">
            Fichamentos
         </li>
      </ol>
      <div class="page-header">
         <h1>
         	Fichamentos cadastrados
         </h1>
      </div>
   </div>
</div>

<div class="row" style="margin-bottom:50px">
   <div class="col-sm-12">
      <table id="grid_fichamentos" class="table table-striped table-bordered table-hover table-full-width dataTable">
         <thead>
            <tr>
	            <th width="300" class="no-orderable">Cliente</th>
               <th class="no-orderable">CPF</th>
	           	<th class="no-orderable">Valor</th>
	            <th class="no-orderable">Parcela</th>               
               <th class="no-orderable">Data do fichamento</th>
               <th class="no-orderable">Status</th>
	            <th width="30" class="no-orderable"></th>
            </tr>
         </thead>
         <tbody>
            
            

         </tbody>
      </table>
   </div>
</div>

<div id="modal_form_remove_fichamento" class="modal fade" tabindex="-1" data-width="560" style="display: none;">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
      &times;
      </button>
      <h4 class="modal-title">Remover fichamento</h4>
   </div>
   <form action="/cobranca/removerFichamento/" id="form-remove-fichamento" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
         <div class="row">
            <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Agente:</label>
                           <input id="ipt_Usuario_id" readonly="readonly" value="<?php echo Yii::app()->session['usuario']->nome_utilizador ?>" type="text" name="Usuario_nome" class="form-control">
                           <input value="<?php echo Yii::app()->session['usuario']->id ?>" type="hidden" name="FichamentoHasStatusFichamento[Usuario_id]">
                           <input id="id_status_fichamento" type="hidden" name="FichamentoAtual[id]">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Data da Remoção:</label>
                           <input readonly="readonly" required="required" value="<?php echo date('d/m/Y') ?>" type="text" class="form-control">
                           <input value="<?php echo date('Y-m-d') ?>" type="hidden" name="FichamentoHasStatusFichamento[data_cadastro]">                           
                        </div>
                     </div>
                  </div>
            </div>
         </div>
         <div class="row">
            <div class="row-centralize">
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label class="control-label">Observacao:</label>
                           <textarea id="textarea_observacao" name="FichamentoHasStatusFichamento[observacao]" required="required" maxlength="300" class="form-control limited" style="width:518px!important;height:116px;"></textarea>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">                        
              <div class="row-centralize">
                 <div class="col-md-12">
                    <div class="row">
                       <div class="col-md-12">
                          <div class="form-group">
                             <label class="control-label">Comprovante:</label>
                             <input data-icon="false" class="control-label filestyle" required="required" name="ComprovanteFile2" id="ComprovanteFile2" type="file" />
                          </div>
                       </div>
                    </div>
                 </div>
              </div>
           </div>
         <div class="row">
            <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label class="control-label">Senha:</label>
                           <input required="required" value="" type="password" name="Operacao[senha]" class="form-control">
                        </div>
                     </div>
                  </div>
            </div>
         </div>
      </div>
      <div class="modal-footer">
         <div style="background:transparent;border:none;" class="panel"></div>
         <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
         <button type="submit" class="btn btn-blue btn-send" id="btn-remove-fich">Confirmar</button>
      </div>
   </form>
</div>

<style type="text/css">
	.dropdown-menu {
	    max-height: 250px;
	    overflow-y: auto;
	    overflow-x: hidden;
	}
	#grid_fichamentos_filter, #grid_fichamentos_length{
		display: none;
	}
	tfoot th {
      font-weight: bold!important;
      color: #d9534f;
      font-size: 11px;
   }
   td.details-control {
		padding:7px 13px!important;
	    background: url('../../images/details_open.png') no-repeat center center;
	    cursor: pointer;
	}
	tr.details td.details-control {
	    background: url('../../images/details_close.png') no-repeat center center;
	}
</style>