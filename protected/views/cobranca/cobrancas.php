<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="#">
                    Cobranças
                </a>
            </li>
            <li class="active">
                Inicial
            </li>
        </ol>
    </div>
</div>
<br />
<div class="row">
    <div class="col-sm-12">
        <button id="toggle-filter" class="col-sm-12 btn btn-success">Mostrar Opções de Filtragem</button>
    </div>
</div>
<br />
<div class="row">
    <div class="col-sm-2">
      <input placeholder="Ir para página" id="pagina" type="number" name="pagina" class="form-control">
    </div>
    <div class="col-sm-2">
        <button id="go-to-page" class="col-sm-12 btn btn-success">Ir</button>
    </div>
</div>
<br>
<div class="filtragem">
    <div class="row">
        <div class="col-sm-10">
            <form>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">
                                Grupo de Filiais <span class="symbol required"></span>
                            </label>

                            <select required="required" multiple="multiple" id="selectGrupoFiliais" class="form-control search-select multipleselect" >
                                <?php foreach (GrupoFiliais::model()->listarParaCobranca() as $gf) { ?>
                                    <option value="<?php echo $gf->id ?>"><?php echo strtoupper($gf->nome_fantasia) ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">
                                Nucleo Filiais <span class="symbol required"></span>
                            </label>

                            <select required="required" multiple="multiple" id="nucleoFiliaisSelect" class="form-control search-select multipleselect" >
                                <?php foreach (NucleoFiliais::model()->listarParaCobranca() as $nf) { ?>
                                    <option value="<?php echo $nf->id ?>"><?php echo strtoupper($nf->nome) ?></option>
                                <?php } ?>
                            </select>

                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">
                                Filiais <span class="symbol required"></span>
                            </label>

                            <select required="required" multiple="multiple" id="selectFiliais" class="form-control search-select multipleselect" >
                                <?php foreach (Filial::model()->listarParaCobranca() as $ap) { ?>
                                    <option value="<?php echo $ap->id ?>"><?php echo strtoupper($ap->getConcat()) ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label">
                                De
                            </label>
                            <input type="date" name="data_de" class="form-control date-picker" data-date-viewmode="years" data-date-format="dd/mm/yyyy" id="data_de">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label">
                                Até
                            </label>
                            <input type="date" name="data_ate" class="form-control date-picker" data-date-viewmode="years" data-date-format="dd/mm/yyyy" id="data_ate">
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label">
                                Qtds atrasos de:
                            </label>
                            <input value="0" type="text" name="qtd_atrasos_de" class="form-control date-picker" id="qtd_atrasos_de">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label">
                                Qtds atrasos até:
                            </label>
                            <input value="0" type="text" name="qtd_atrasos_ate" class="form-control date-picker" id="qtd_atrasos_ate">
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label">CPF</label>
                            <input class="form-control" type="text" id="cpf_cliente">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="control-label">&nbsp;</label>
                            <button id="btn-filter" class="btn btn-blue next-step btn-block">Filtrar</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="row">
    <form class="form-horizontal">
        <div class="form-group">
            <div class="col-sm-10">
                <div class="checkbox">
                    <label>
                        <input id="imtodos"type="hidden" value="0">
                        <i id="mtodos" class="fa fa-square-o"></i> Mostrar todos
                    </label>
                    <label class="col-sm-offset-1">
                        <input id="iagend" type="hidden" value="0">
                        <i id="agend" class="fa fa-square-o" value="0"></i> Agendamentos
                    </label>
                    <label class="col-sm-offset-1">
                        <input id="iaghoje" type="hidden" value="0">
                        <i id="aghoje" class="fa fa-square-o" value="0"></i> Agendamentos de hoje
                    </label>
                    <label class="col-sm-offset-1">
                        <input id="ifichados" type="hidden" value="0">
                        <i id="fichados" class="fa fa-square-o" value="0"></i> Fichados
                    </label>

                    <label class="col-sm-offset-1">
                        <input id="ifcsc" type="hidden" value="0">
                        <i id="csc" class="fa fa-square-o" value="0"></i> CSC
                    </label>

                    <label class="col-sm-offset-1">
                        <input id="iffdic" type="hidden" value="0">
                        <i id="fdic" class="fa fa-square-o" value="0"></i> FDIC
                    </label>

                </div>
            </div>
        </div>
    </form>
</div>
<div class="row">
    <div class="col-sm-12">
        <table id="gridCobranca" class="table table-striped table-bordered table-hover table-full-width dataTable">
            <thead>
                <tr>
                    <th></th>
                    <th class="no-orderable">
                        CPF
                    </th>
                    <th width="10px" class="no-orderable">
                        Situação
                    </th>
                    <th class="no-orderable">
                        Nome
                    </th>
                    <th width="90px" class="no-orderable">
                        Qtd Atrasos
                    </th>
                    <th width="90px" class="no-orderable">
                        Maior Atraso
                    </th>
                    <th width="10px" class="no-orderable"></th>
                </tr>
            </thead>

            <tbody>
            </tbody>

        </table>
    </div>
</div>
<br /> <br />
<div id="modal_form_remove_fichamento" class="modal fade" tabindex="-1" data-width="560" style="display: none; position: fixed; margin-top: -325px;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Remover fichamento</h4>
    </div>
<!--    <form action="/cobranca/removerFich/" id="form-remove-fichamento" method="POST" enctype="multipart/form-data">-->
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Agente:</label>
                                <input id="ipt_Usuario_id" readonly="readonly" value="<?php echo Yii::app()->session['usuario']->nome_utilizador ?>" type="text" name="Usuario_nome" class="form-control">
                                <input id="id_status_fichamento" type="hidden" name="FichamentoAtual[id]">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Data da Remoção:</label>
                                <input readonly="readonly" required="required" value="<?php echo date('d/m/Y') ?>" type="text" class="form-control">
                                <input value="<?php echo date('Y-m-d') ?>" type="hidden" name="FichamentoHasStatusFichamento[data_cadastro]">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="row-centralize">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Observacao:</label>
                                    <textarea id="textarea_observacaoRemove" name="FichamentoHasStatusFichamento[observacao]" required="required" maxlength="300" class="form-control limited" style="height:116px;"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<!--            <div class="row">
                <div class="row-centralize">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Comprovante:</label>
                                    <input data-icon="false" class="control-label filestyle" required="required" name="ComprovanteFile" id="ComprovanteFile" type="file" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Motivo da Exclusão:</label>
                                <select id="selectMotivosExclusao" required="required" name="Operacao[motivo]" class="form-control search-select select2">
                                    <?php foreach(MotivoExclusao::model()->findAll("habilitado") as $motivo) { ?>
                                        <option value="<?php echo $motivo->id; ?>">
                                            <?php echo $motivo->descricao; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Senha:</label>
                                <input id="senha_remove" required="required" value="" type="password" name="Operacao[senha]" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div style="background:transparent;border:none;" class="panel"></div>
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
            <button type="submit" class="btn btn-blue btn-send" id="btn-remove-fich">Confirmar</button>
        </div>
    <!--</form>-->
</div>
<div id="modal_form_new_fichamento" class="modal fade" tabindex="-1" data-width="560" style="display: none; position: fixed; margin-top: -325px;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Negativar cliente</h4>
    </div>
<!--    <form action="/cobranca/negativarCliente/" method="POST" id="form-add-fichamento" enctype="multipart/form-data">-->
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Agente:</label>
                                <input id="ipt_Usuario_id" readonly="readonly" value="<?php echo Yii::app()->session['usuario']->nome_utilizador ?>" type="text" name="Usuario_nome" class="form-control">
                                <input value="<?php echo Yii::app()->session['usuario']->id ?>" type="hidden" name="FichamentoHasStatusFichamento[Usuario_id]">
                                <input id="Parcela_id" type="hidden" name="Fichamento[Parcela]" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Data:</label>
                                <input readonly="readonly" required="required" value="<?php echo date('d/m/Y') ?>" type="text" name="Fichamento[data_cadastro]" class="form-control date-picker">
                                <input value="<?php echo date('Y-m-d') ?>" type="hidden" name="FichamentoHasStatusFichamento[data_cadastro]" id="dataFichamento">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Órgão:</label>
                                <select id="select_orgao_negacao" required="true" name="Fichamento[OrgaoNegacao_id]" class="draft_sent form-control search-select select2 fichar">
                                    <option value="">Selecione:</option>
                                    <?php foreach (OrgaoNegacao::model()->findAll('habilitado') as $orgao) {
                                        ?>
                                        <option value="<?php echo $orgao->id ?>"><?php echo $orgao->descricao ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="row-centralize">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Observacao:</label>
                                    <textarea id="textarea_observacaoInclui" name="FichamentoHasStatusFichamento[observacao]" required="required" maxlength="300" class="form-control limited fichar" style="height:116px;"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<!--            <div class="row">
                <div class="row-centralize">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Comprovante:</label>
                                    <input data-icon="false" class="control-label filestyle" required="required" name="ComprovanteFile2" id="ComprovanteFile2" type="file" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Senha:</label>
                                <input id="senha" required="required" value="" type="password" name="Operacao[senha]" class="form-control fichar">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div style="background:transparent;border:none;" class="panel"></div>
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
            <button id="btn-send-fich" type="submit" class="btn btn-blue btn-send">Confirmar</button>
        </div>
<!--    </form>-->
</div>

<div id="modal_ncontato" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Novo Contato</h4>
        </div>
        <div class="modal-body">
            <form>
                <div class="form-group">
                    <label for="num_ncontato">Digite o número</label>
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-phone"></i></div>
                        <input type="text" class="form-control" id="num_ncontato" placeholder="DDD + Número">
                        <input type="hidden" class="form-control" id="cliente_ncontato">
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button id="btn_ncontato" type="button" class="btn btn-primary">Adicionar</button>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal -->
<style>
    .filtragem {
        display: none;
    }
    .dropdown-menu {
        max-height: 250px;
        overflow-y: auto;
        overflow-x: hidden;
    }
    .select2-drop{
        width: 400px!important;
    }
    #gridCobranca_length, #gridCobranca_filter{
        display: none;
    }
    #gridSeqs_length, #gridSeqs_filter{
        display: none;
    }

    #gridParcAtend_length, #gridParcAtend_filter, #gridParcAtend_paginate, #gridParcAtend_info{
        display: none;
    }

    #gridParcelas_length, #gridParcelas_filter{
        display: none;
    }

    #gridAtendimentos_length, #gridAtendimentos_filter{
        display: none;
    }

    td.details-control {
        background: url('../../images/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('../../images/details_close.png') no-repeat center center;
    }
    .buttonpulsate {

        -webkit-border-radius: 10px;
        border-radius: 10px;
        border: none;

        cursor: pointer;
        display: inline-block;
        font-family: Arial;
        font-size: 20px;
        padding: 5px 10px;
        text-align: center;
        text-decoration: none;
    }
    @-webkit-keyframes glowing {
        0% { background-color: #929292; -webkit-box-shadow: 0 0 3px #5c5c5c; }
        50% { background-color: #929292; -webkit-box-shadow: 0 0 60px #5c5c5c; }
        100% { background-color: #929292; -webkit-box-shadow: 0 0 3px #5c5c5c; }
    }

    @-moz-keyframes glowing {
        0% { background-color: #929292; -moz-box-shadow: 0 0 3px #5c5c5c; }
        50% { background-color: #929292; -moz-box-shadow: 0 0 10px #5c5c5c; }
        100% { background-color: #929292; -moz-box-shadow: 0 0 3px #5c5c5c; }
    }

    @-o-keyframes glowing {
        0% { background-color: #929292; box-shadow: 0 0 3px #5c5c5c; }
        50% { background-color: #929292; box-shadow: 0 0 10px #5c5c5c; }
        100% { background-color: #929292; box-shadow: 0 0 3px #5c5c5c; }
    }

    @keyframes glowing {
        0% { background-color: #929292; box-shadow: 0 0 3px #5c5c5c; }
        50% { background-color: #929292; box-shadow: 0 0 10px #5c5c5c; }
        100% { background-color: #929292; box-shadow: 0 0 3px #5c5c5c; }
    }

    .buttonpulsate {
        -webkit-animation: glowing 1200ms infinite;
        -moz-animation: glowing 1200ms infinite;
        -o-animation: glowing 1200ms infinite;
        animation: glowing 1200ms infinite;
    }
</style>
