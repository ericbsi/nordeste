<?php 
   $util = new Util;
?>
<div class="row">
   <div class="col-sm-12">
      
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="/cobranca/">
               Cobranças
            </a>
         </li>
         <li class="active">
            Parcela
         </li>
      </ol>
      <div class="page-header">
         <h1 style="font-size:20px">
         	COBRANÇAS DA PARCELA N° <?php echo $parcela->seq ?>, proposta <?php echo $parcela->titulo->proposta->codigo ?>
         </h1>
         <h1 style="font-size:20px">
            VENCIMENTO DA PARCELA <?php echo $util->bd_date_to_view( $parcela->vencimento ) ?>
         </h1>
         <h1 style="font-size:20px">
            CLIENTE: <?php echo $parcela->titulo->proposta->analiseDeCredito->cliente->pessoa->nome ?>
         </h1>
         <h1 style="font-size:20px">
            LOCAL DA COMPRA: <?php echo $parcela->titulo->proposta->analiseDeCredito->filial->getConcat() ?>
         </h1>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-sm-12">
      <p>
         <form style="display:inline" action="/cobranca/atrasos/" method="POST">
            <button type="submit" class="btn btn-success">Voltar <i class="fa fa-backward"></i></button>
            <input type="hidden" value="<?php echo $cpfPesquisa ?>" name="cpfPesquisa">
         </form>
         <a id="btn_modal_form_new_cobranca" data-toggle="modal" href="#modal_form_new_cobranca" class="btn btn-success">Registrar cobrança <i class="fa fa-plus"></i></a>
      </p>
   </div>
</div>
<div class="row">
   <div class="col-sm-12">
      <table id="grid_cobrancas" class="table table-striped table-bordered table-hover table-full-width dataTable">
         <thead>
            <tr>
               <th style="width:150px" class="no-orderable">Data da cobrança</th>
               <th class="no-orderable">Agente</th>
               <th class="no-orderable">Observação</th>
               <th class="no-orderable">Método</th>
               <th class="no-orderable"></th>
               <!--<th class="no-orderable"></th>-->
            </tr>            
         </thead>
         <tbody>
            
         </tbody>
      </table>
   </div>
</div>

<div id="modal_form_new_cobranca" class="modal fade" tabindex="-1" data-width="560" style="display: none;">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
      &times;
      </button>
      <h4 class="modal-title">Registrar cobrança</h4>
   </div>
   <form id="form-add-cobranca">
      <div class="modal-body">
         <div class="row">
            <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label class="control-label">Agente:</label>
                           <input id="ipt_Usuario_id" readonly="readonly" value="<?php echo Yii::app()->session['usuario']->nome_utilizador ?>" type="text" name="Usuario_nome" class="form-control">
                           <input value="<?php echo Yii::app()->session['usuario']->id ?>" type="hidden" name="Cobranca[Usuario_id]" class="form-control">
                           <input id="Parcela_id" value="<?php echo $parcela->id ?>" type="hidden" name="Cobranca[Parcela_id]" class="form-control">
                           <input id="Cobranca_id" type="hidden" name="Cobranca_id" class="form-control">
                        </div>
                     </div>
                  </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Data:</label>
                           <input id="ipt_data_cobranca" required="required" value="<?php echo date('d/m/Y') ?>" type="text" name="Cobranca[data_cobranca]" class="form-control date-picker" data-date-viewmode="years" data-date-format="dd/mm/yyyy">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Método:</label>
                           <select id="select_Metodo_cobranca_id" required="true" name="Cobranca[Metodo_cobranca_id]" class="draft_sent form-control search-select select2">
                              <option value="">Selecione:</option>
                              <?php foreach( MetodoCobranca::model()->findAll('habilitado') as $mc ){ ?>
                                 <option value="<?php echo $mc->id ?>"><?php echo $mc->descricao ?></option>
                              <?php } ?>
                           </select>
                        </div>
                     </div>
                  </div>
            </div>
         </div>
         <div class="row">                        
            <div class="row-centralize">
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label class="control-label">Observacao:</label>
                           <textarea id="textarea_observacao" name="Cobranca[observacao]" required="required" maxlength="300" class="form-control limited" style="width:518px!important;height:116px;"></textarea>
                           <input id="ipt_action_type" name="action_type" type="hidden" />
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal-footer">
         <label class="checkbox-inline" style="float:left">
            <input id="checkbox_continuar" type="checkbox" value="">
            Continuar Cadastrando
         </label>
         <div style="background:transparent;border:none;" class="panel"></div>
         <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
         <button type="submit" class="btn btn-blue btn-send">Salvar</button>
         <div class="row">
         </div>
         <br>
         <div id="cadastro_msg_return" class="alert" style="text-align:left">
         </div>
      </div>
   </form>
</div>

<style type="text/css">
	#grid_cobrancas_length, #grid_cobrancas_filter{
		display: none;
	}
	td.details-control {
		padding:7px 13px!important;
	    background: url('../../images/details_open.png') no-repeat center center;
	    cursor: pointer;
	}
	tr.details td.details-control {
	    background: url('../../images/details_close.png') no-repeat center center;
	}
</style>
