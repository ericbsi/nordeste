<?php  ?>
<div class="row">
   <div class="col-sm-12">
      
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
               Cobranças
            </a>
         </li>
         <li class="active">
            Atrasos
         </li>
      </ol>
      <div class="page-header">
         <h1>
         	Parcelas em atraso
         </h1>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-sm-9">
      <div class="row">
         <form id="form-filter" action="">
            
            <div class="col-md-3">
               <div class="form-group">
                  <label class="control-label">
                     Filiais <span class="symbol required"></span>
                  </label>
                  <select required="required" multiple="multiple" id="filiais_select" class="form-control multipleselect" name="Filiais[]">
                     <?php foreach ( Yii::app()->session['usuario']->getEmpresa()->listFiliais() as $f ) { ?>
                        <option value="<?php echo $f->id ?>"><?php echo strtoupper($f->getConcat()) ?></option>
                     <?php } ?>
                  </select>  
               </div>
            </div>
            <!--
            <div class="col-md-3">
               <div class="form-group">
                  <label class="control-label">
                     Filiais <span class="symbol required"></span>
                  </label>
                  <select required="required" multiple="multiple" id="analistas_select" class="form-control multipleselect" name="analistas[]">
                      <option value="<?php echo Yii::app()->session['usuario']->id ?>">Minhas Análises</option>
                      <option value="1">Todas</option>
                  </select>
               </div>
            </div>
            -->
            <div class="col-md-3" style="width:10%">
               <div class="form-group">
                  <label class="control-label">&nbsp;</label>
                  <button id="btn-filter" class="btn btn-blue next-step btn-block">Filtrar</button>
               </div>
            </div>

         </form>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-sm-12">
      <table id="grid_atrasos" class="table table-striped table-bordered table-hover table-full-width dataTable">
         <thead>
            <tr>
				  <th class="no-orderable"></th>
              <th class="no-orderable">Título</th>
              <th class="no-orderable">Proposta</th>
              <th class="no-orderable">Filial</th>
              <th class="no-orderable">Cliente</th>
              <th class="no-orderable">CPF</th>
              <th class="no-orderable">Nascimento</th>
              <th class="no-orderable">Parcela</th>
              <th class="no-orderable">Valor</th>
              <th class="no-orderable">Status</th>
              <th class="no-orderable">Analista</th>
              <th class="no-orderable"></th>
            </tr>
         </thead>
         <tbody></tbody>
      </table>
   </div>
</div>

<style type="text/css">
	#grid_atrasos_length, #grid_atrasos_filter{
		display: none;
	}
	td.details-control {
		padding:7px 13px!important;
	    background: url('../../images/details_open.png') no-repeat center center;
	    cursor: pointer;
	}
	tr.details td.details-control {
	    background: url('../../images/details_close.png') no-repeat center center;
	}
</style>
