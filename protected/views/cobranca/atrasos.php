<?php  ?>

<div class="row">
   <div class="col-sm-12">
      
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
               Cobranças
            </a>
         </li>
         <li class="active">
            Atrasos
         </li>
      </ol>
      <div class="page-header">
         <h1>
         	Parcelas em atraso
         </h1>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-sm-9">
      <div class="row">
         <form>
            
            <div class="col-md-3">
               <div class="form-group">
                  <label class="control-label">
                     Parceiros
                  </label>
                  <select required="required" multiple="multiple" id="filiais_select" class="form-control multipleselect" name="Filiais[]">
                     <?php foreach ( Yii::app()->session['usuario']->getEmpresa()->listFiliais() as $f ) { ?>
                        <option value="<?php echo $f->id ?>"><?php echo strtoupper($f->getConcat()) ?></option>
                     <?php } ?>
                  </select>  
               </div>
            </div>
            <div class="col-md-3">
               <div class="form-group">
                  <label class="control-label">
                     Analistas
                  </label>
                  <select required="required" multiple="multiple" id="analistas_select" class="form-control multipleselect" name="Filiais[]">
                     <?php foreach ( Usuario::model()->findAll('tipo_id = 4 AND habilitado') as $analista ) { ?>
                        <option value="<?php echo $analista->id ?>"><?php echo strtoupper($analista->nome_utilizador) ?></option>
                     <?php } ?>
                  </select>  
               </div>
            </div>
            <div class="col-md-3">
               <div class="form-group">
                  <label class="control-label">
                     CPF
                  </label>
                  <input value="<?php if( isset( $cpfPesquisa ) && $cpfPesquisa != 0 ){echo $cpfPesquisa; } ?>" required="" name="Cliente[cpf]" type="text" class="form-control" aria-required="true" id="cpfcliente">
               </div>
            </div>
            
            <div class="col-md-3" style="width:10%">
               <div class="form-group">
                  <label class="control-label">&nbsp;</label>
                  <button id="btn-filter" class="btn btn-blue next-step btn-block">Filtrar</button>
               </div>
            </div>

         </form>
      </div>
   </div>
</div>

<div class="row" style="margin-bottom:50px">
   <div class="col-sm-12">
      <table id="grid_atrasos" class="table table-striped table-bordered table-hover table-full-width dataTable">
         <thead>
            <tr>
               <th width="20" class="no-orderable"></th>
               <th width="280" class="no-orderable">Parceiro</th>
               <th class="no-orderable">Cliente</th>
               <th width="80" class="no-orderable">CPF</th>
               <th width="80" class="no-orderable">Nascimento</th>
               <th class="no-orderable">Parcela</th>
               <th width="60" class="no-orderable">Valor</th>
               <th width="80" class="no-orderable">Data da compra</th>
               <th class="no-orderable">Vencimento</th>
               <th class="no-orderable">Status</th>
               <th class="no-orderable">Analista</th>
               <th class="no-orderable"></th>
               <th class="no-orderable"></th>
               <th class="no-orderable"></th>
            </tr>
         </thead>
         <tbody></tbody>
         <tfoot>
            <tr>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th id="tfoot-total-atraso"></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
            </tr>
         </tfoot>
      </table>
   </div>
</div>

<div id="modal_form_new_fichamento" class="modal fade" tabindex="-1" data-width="560" style="display: none;">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
      &times;
      </button>
      <h4 class="modal-title">Negativar cliente</h4>
   </div>
   <form action="/cobranca/negativarCliente/" method="POST" id="form-add-fichamento" enctype="multipart/form-data">
      <div class="modal-body">
         <div class="row">
            <div class="col-md-12">
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <label class="control-label">Agente:</label>
                        <input id="ipt_Usuario_id" readonly="readonly" value="<?php echo Yii::app()->session['usuario']->nome_utilizador ?>" type="text" name="Usuario_nome" class="form-control">
                        <input value="<?php echo Yii::app()->session['usuario']->id ?>" type="hidden" name="FichamentoHasStatusFichamento[Usuario_id]">
                        <input id="Parcela_id" type="hidden" name="Fichamento[Parcela]" class="form-control">
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label">Data:</label>
                        <input readonly="readonly" required="required" value="<?php echo date('d/m/Y') ?>" type="text" name="Fichamento[data_cadastro]" class="form-control date-picker">
                        <input value="<?php echo date('Y-m-d') ?>" type="hidden" name="FichamentoHasStatusFichamento[data_cadastro]">
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label">Órgão:</label>
                        <select id="select_orgao_negacao" required="true" name="Fichamento[OrgaoNegacao_id]" class="draft_sent form-control search-select select2">
                           <option value="">Selecione:</option>
                           <?php foreach (OrgaoNegacao::model()->findAll('habilitado') as $orgao)
                              { ?>
                           <option value="<?php echo $orgao->id ?>"><?php echo $orgao->descricao ?></option>
                           <?php } ?>
                        </select>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="row-centralize">
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label class="control-label">Observacao:</label>
                           <textarea id="textarea_observacao" name="FichamentoHasStatusFichamento[observacao]" required="required" maxlength="300" class="form-control limited" style="width:518px!important;height:116px;"></textarea>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">                        
              <div class="row-centralize">
                 <div class="col-md-12">
                    <div class="row">
                       <div class="col-md-12">
                          <div class="form-group">
                             <label class="control-label">Comprovante:</label>
                             <input data-icon="false" class="control-label filestyle" required="required" name="ComprovanteFile2" id="ComprovanteFile2" type="file" />
                          </div>
                       </div>
                    </div>
                 </div>
              </div>
           </div>
         <div class="row">
            <div class="col-md-12">
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <label class="control-label">Senha:</label>
                        <input required="required" value="" type="password" name="Operacao[senha]" class="form-control">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal-footer">
         <div style="background:transparent;border:none;" class="panel"></div>
         <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
         <button id="btn-send-fich" type="submit" class="btn btn-blue btn-send">Confirmar</button>
      </div>
   </form>
</div>

<div id="modal_form_new_auditoria" class="modal fade" tabindex="-1" data-width="560" style="display: none;">
     
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
        &times;
        </button>
        <h4 class="modal-title">Encaminhar para Auditoria</h4>
     </div>

     <form action="/auditoria/novo/" method="POST" enctype="multipart/form-data" id="form-add-auditoria">
        <div class="modal-body">
           <div class="row">
              <div class="col-md-12">
                    <div class="row">
                       <div class="col-md-12">
                           <div class="form-group">
                              <label class="control-label">Agente:</label>
                              <input id="ipt_Usuario_id" readonly="readonly" value="<?php echo Yii::app()->session['usuario']->nome_utilizador ?>" type="text" name="Usuario_nome" class="form-control">
                              <input value="<?php echo Yii::app()->session['usuario']->id ?>" type="hidden" name="Auditoria[Agente]">
                              <input type="hidden" name="Auditoria[Entidade_Relacionamento]" value="Parcela" class="form-control">
                              <input id="ER_id"    type="hidden" name="Auditoria[Entidade_Relacionamento_id]" class="form-control">
                           </div>
                       </div>
                    </div>
              </div>
           </div>
           <div class="row">
              <div class="col-md-12">
                    <div class="row">
                       <div class="col-md-12">
                          <div class="form-group">
                              <label class="control-label">Data:</label>
                              <input readonly="readonly" required="required" value="<?php echo date('d/m/Y') ?>" type="text" name="data" class="form-control date-picker">
                              <input value="<?php echo date('Y-m-d') ?>" type="hidden" name="Auditoria[data_criacao]">
                           </div>
                       </div>                     
                    </div>
              </div>
           </div>
           <div class="row">
              <div class="row-centralize">
                 <div class="col-md-12">
                    <div class="row">
                       <div class="col-md-12">
                           <div class="form-group">
                              <label class="control-label">Observacao:</label>
                              <textarea id="" name="Auditoria[Observacao]" required="required" maxlength="300" class="form-control limited" style="width:518px!important;height:116px;"></textarea>
                           </div>
                       </div>
                    </div>
                 </div>
              </div>
           </div>
           <div class="row">
              <div class="row-centralize">
                 <div class="col-md-12">
                    <div class="row">
                       <div class="col-md-12">
                          <div class="form-group">
                             <label class="control-label">Comprovante:</label>
                             <input data-icon="false" class="control-label filestyle" name="ComprovanteFile" id="ComprovanteFile" type="file" />
                          </div>
                       </div>
                    </div>
                 </div>
              </div>
           </div>
        </div>
        <div class="modal-footer">
           <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
           <button type="submit" class="btn btn-blue btn-send">Salvar</button>
        </div>
     </form>
</div>

<style type="text/css">
	.dropdown-menu {
	    max-height: 250px;
	    overflow-y: auto;
	    overflow-x: hidden;
	}
	#grid_atrasos_filter, #grid_atrasos_length{
		display: none;
	}
	tfoot th {
      font-weight: bold!important;
      color: #d9534f;
      font-size: 11px;
   }
   td.details-control {
		padding:7px 13px!important;
	    background: url('../../images/details_open.png') no-repeat center center;
	    cursor: pointer;
	}
	tr.details td.details-control {
	    background: url('../../images/details_close.png') no-repeat center center;
	}
</style>

