<?php  ?>
<div class="row">
   <div class="col-sm-12">
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/produto/listarProdutos">
            Produtos
            </a>
         </li>
         <li class="active">
            Editar Características de Produto
         </li>
      </ol>
      <div class="page-header">
         <h1>
         Editar Característica de Produto : <?php echo $caracteristica->descricao ?>
      </div>
   </div>
</div>
<p>
   <a class="btn btn-success" href="#modal_form_new_item_carac_produto" data-toggle="modal" id="btn_modal_form_new_produto">
   Adicionar item da Característica de Produto <i class="fa fa-plus"></i>
   </a>
</p>
<p>
</p>
<div class="row">
   <div class="col-sm-12">
      <table id="grid_itens_caracteristica" class="table table-striped table-bordered table-hover table-full-width dataTable">
         <thead>
            <tr>
               <th class="no-orderable">Valor</th>
            </tr>
         </thead>
      </table>
   </div>
</div>

<!--Modal novo item da característica-->
<div id="modal_form_new_item_carac_produto" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
      &times;
      </button>
      <h4 class="modal-title">Cadastrar Item de <?php echo $caracteristica->descricao ?></h4>
   </div>
   <form id="form-add-item-carac-produto">
      <div class="modal-body">
         <div class="row">
            <div class="row-centralize">
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label class="control-label">Valor <span class="symbol required"></span></label>
                           <input required name="ValoresCaracteristica[valor]" type="text" class="form-control">
                           <input id="caracteristicaId" name="ValoresCaracteristica[Caracteristica_id]" type="hidden" value="<?php echo $caracteristica->id ?>">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal-footer">
         <label class="checkbox-inline" style="float:left">
         <input id="item_carac_produto_checkbox_continuar" type="checkbox" value="">Continuar Cadastrando</label>
         <div style="background:#ECF0F1; border:none" class="panel"></div>
         <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
         <button type="submit" class="btn btn-blue btn-send">Salvar</button>
         <div class="row">
         </div>
         <br>
         <div id="cadastro_item_carac_produto_msg_return" class="alert" style="text-align:left; display:none"></div>
      </div>
   </form>
</div>

<style type="text/css">
   #grid_itens_caracteristica_length, #grid_itens_caracteristica_filter{display: none}
   .panel{
      background: transparent!important;
      border:none!important;
   }
   .col-md-3{
      padding-left: 0!important;
   }
</style>