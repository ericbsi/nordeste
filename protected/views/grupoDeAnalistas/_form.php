<div class="row">
	<div class="col-md-12">
         <div class="row">
                  <div class="col-sm-12">                   
                     <!-- start: PAGE TITLE & BREADCRUMB -->
                     <ol class="breadcrumb">
                        <li>
                           <i class="clip-pencil"></i>
                           <a href="#">
                              Grupos de Analistas
                           </a>
                        </li>
                        <li class="active">
                              Criar
                        </li>
                        
                     </ol>

                     <div class="page-header">
                        <h1>Cadastrar Grupo de Analistas</h1>
                     </div>
                     <!-- end: PAGE TITLE & BREADCRUMB -->
                  </div>
         </div>
			
         <div class="panel panel-default">
            <div class="panel-heading">
               <i class="fa fa-external-link-square"></i>
               
               <div class="panel-tools">
                  <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                  </a>
               </div>
            </div>
            <div class="panel-body">            
               <hr>
   	           <?php $form=$this->beginWidget('CActiveForm', array(
   					'id'=>'grupoAnalistasForm',			
   					'enableAjaxValidation'=>false,
   					'htmlOptions'=>array(
   						'role'=>'form'
   					)
   				)); ?>

                  <div class="row">

                     <div class="col-md-6">
                        <div class="form-group">
                        	<?php echo $form->labelEx($model,'nome', array('class'=>'control-label')); ?>
   						      <?php echo $form->textField($model,'nome',array('class'=>'form-control', 'required'=>true,'placeholder'=>'Nome do grupo')); ?>
                        </div>

                        <?php
                           $empresa = Empresa::model()->find("nome_fantasia = " . "'".Yii::app()->session['usuario']->nomeEmpresa()."'");
                        ?>

                        <div class="form-group">
                           <label class="control-label">Filiais</label>
                               <select multiple="multiple" id="filiais_select" class="form-control" name="Filiais[]">
                                 <?php foreach ( $empresa->listFiliais() as $f ) { ?>
                                    <option value="<?php echo $f->id ?>"><?php echo $f->getConcat() ?></option>
                                 <?php } ?>
                               </select>  

                              <?php /*echo CHtml::dropDownList('Filiais','Filiais',
                                 CHtml::listData($empresa->listFiliais(),
                                 'id','concat' ), array('class'=>'form-control search-select', 'multiple'=>'multiple', 'placeholder'=>'Selecione')); 
                              */?> 
                        </div>
                     </div>

                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Observação</label>
                           <textarea name="GrupoDeAnalistas[descricao]" maxlength="100" id="form-field-23" class="form-control limited"></textarea>
                        </div>
                    </div>
                  </div>

                  <div class="row">
                     <div class="col-md-12">
                        <div>
                           <span class="symbol required"></span> CAMPOS OBRIGATÓRIOS
                           <hr>
                        </div>
                     </div>
                  </div>

                  <div class="row">
                     <div class="col-md-8">
                     </div>
                     <div class="col-md-4">
                        <input type="submit" value="Cadastrar" class="btn btn-teal btn-block">
                     </div>
                  </div>
    			<?php $this->endWidget(); ?>
            </div>
         </div>
	</div>
</div>