<?php
/* @var $this GrupoDeAnalistasController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Grupo De Analistases',
);

$this->menu=array(
	array('label'=>'Create GrupoDeAnalistas', 'url'=>array('create')),
	array('label'=>'Manage GrupoDeAnalistas', 'url'=>array('admin')),
);
?>

<h1>Grupo De Analistases</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
