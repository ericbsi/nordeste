<?php
/* @var $this GrupoDeAnalistasController */
/* @var $model GrupoDeAnalistas */

$this->breadcrumbs=array(
	'Grupo De Analistases'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List GrupoDeAnalistas', 'url'=>array('index')),
	array('label'=>'Create GrupoDeAnalistas', 'url'=>array('create')),
	array('label'=>'Update GrupoDeAnalistas', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete GrupoDeAnalistas', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage GrupoDeAnalistas', 'url'=>array('admin')),
);
?>

<h1>View GrupoDeAnalistas #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nome',
		'descricao',
		'habilitado',
		'data_cadastro',
	),
)); ?>
