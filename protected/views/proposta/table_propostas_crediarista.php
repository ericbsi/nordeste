<table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
   <thead>
      <tr>
         <th>Código</th>
         <th class="hidden-xs">Cliente</th>
         <th>Financeira</th>
         <th class="hidden-xs">Valor financiado</th>
         <th>N° parcelas</th>
         <th class="hidden-xs">Valor da parcela</th>
         <th>Taxa</th>
         <th class="hidden-xs">Valor final</th>
         <th>Status</th>
      </tr>
   </thead>
   <tbody>

      <?php foreach ( Yii::app()->session['usuario']->listCrediaristaPropostas( array(3,4,1,2) ) as $proposta ) { ?>

         <tr>
            <td><?php echo $proposta->codigo ?></td>
            <td class="hidden-xs"><?php echo substr($proposta->analiseDeCredito->cliente->pessoa->nome, 0, 20) . '...'; ?></td>
            <td><?php echo $proposta->financeira->nome ?></td>
            <td class="hidden-xs"><?php echo "R$ " . number_format($proposta->valor - $proposta->valor_entrada, 2, ",", ".") ?></td>
            <td><?php echo $proposta->qtd_parcelas ?></td>
            <td class="hidden-xs"><?php echo "R$ " . number_format($proposta->getValorParcela(), 2, ",", ".") ?></td>
            <td><?php echo number_format($proposta->cotacao->taxa, 2, ",", "") . "%" ?></td>
            <td class="hidden-xs"><?php echo "R$ " . number_format($proposta->valor_final, 2, ",", ".") ?></td>
            <td>
               <?php if( $proposta->statusProposta->id == 2 && !$proposta->titulos_gerados ) { ?>
               <a href="<?php echo Yii::app()->getBaseUrl(true) ?>/proposta/concluir?c=<?php echo $proposta->codigo ?>">
               <?php } ?>
               <span class="<?php echo $proposta->statusProposta->cssClass ?>">
                  <?php
                        if ( $proposta->statusProposta->id == 2 && $proposta->titulos_gerados )
                           echo "Finalizada";
                        else
                           echo $proposta->statusProposta->status;
                  ?>
               </span>
               <?php if( $proposta->statusProposta->id == 2 && !$proposta->titulos_gerados ) { ?>
               </a>
               <?php } ?>
            </td>
         </tr>

      <?php } ?>

   </tbody>
</table>