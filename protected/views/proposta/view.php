<?php
/* @var $this PropostaController */
/* @var $model Proposta */

$this->breadcrumbs=array(
	'Propostas'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Proposta', 'url'=>array('index')),
	array('label'=>'Create Proposta', 'url'=>array('create')),
	array('label'=>'Update Proposta', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Proposta', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Proposta', 'url'=>array('admin')),
);
?>

<h1>View Proposta #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'Analise_de_Credito_id',
		'Financeira_id',
		'Cotacao_id',
		'habilitado',
		'data_cadastro',
		'valor',
		'qtd_parcelas',
		'carencia',
		'valor_parcela',
		'valor_entrada',
		'valor_final',
		'Status_Proposta_id',
	),
)); ?>
