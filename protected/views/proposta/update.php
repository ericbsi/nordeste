<?php
/* @var $this PropostaController */
/* @var $model Proposta */

$this->breadcrumbs=array(
	'Propostas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Proposta', 'url'=>array('index')),
	array('label'=>'Create Proposta', 'url'=>array('create')),
	array('label'=>'View Proposta', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Proposta', 'url'=>array('admin')),
);
?>

<h1>Update Proposta <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>