<?php
$util = new Util;
$cpf = $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero;
$numerosDePagamentos = $proposta->analiseDeCredito->cliente->pagamentosCliente($cpf);

/* if ($proposta->Status_Proposta_id == "1" && $proposta->hasOmniConfig() == NULL) {

  $scores = new ConsultaCliente;
  $scores->consultarScore($proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero, 1, $proposta->analiseDeCredito->cliente->id);


  $classe = $scores->classe;
  $risco = 'Desconhecido';
  $cssClass = "alert-danger";

  if ($scores->probabilidade > 0.10 && $scores->probabilidade <= 10.20) {
  $risco = 'Baixo';
  $cssClass = "alert-success";
  }
  if ($scores->probabilidade > 10.30 && $scores->probabilidade <= 20.7) {
  $risco = 'Médio';
  $cssClass = "alert-warning";
  }
  if ($scores->probabilidade > 20.7 && $scores->probabilidade <= 98.8) {
  $risco = 'Alto';
  $cssClass = "alert-danger";
  }

  $fscore = $scores->score;

  $rest = 'Desconhecido';
  if ($scores->restricao == 'true') {
  $rest = 'SIM';
  } else {
  $rest = 'NÃO';
  }
  } */
?>
<script type="text/javascript">
    function resizeIframe(obj) {

        var isFirefox = typeof InstallTrigger !== 'undefined';

        var plusFf = 0;

        if (isFirefox) {
            plusFf = 100
        }
        ;

        {
            obj.style.height = 0;
        }
        ;

        {
            obj.style.height = plusFf + obj.contentWindow.document.body.scrollHeight + 'px';
        }
    }
</script>
<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="#">
                    Proposta
                </a>
            </li>
            <li class="active">
                Resumo
            </li>
        </ol>
        <div class="page-header">
            <h1>Proposta</h1>

            <div class="row">

                <form target="_blank" action="/cliente/analisarCadastro" method="post">  
                    <button style="display:inline" data-original-title="Editar" data-placement="top" class="btn btn btn-primary">
                        Dados Cliente
                    </button>
                    <input type="hidden" value="<?php echo $proposta->analiseDeCredito->cliente->id ?>" name="idCliente">
                </form>

            </div>

            <br>

            <div class="row">

                <?php if ($proposta->hasOmniConfig() == NULL && ($proposta->Status_Proposta_id == "4" || $proposta->Status_Proposta_id == "1")): ?>
                    <?php if (!$proposta->ehCartao()) { ?>
                        <div class="col-md-3">
                            <a type="button" class="btn btn-success"    data-toggle="modal" href="#modal-aprove-confirm"        style="width : 100%">
                                Aprovar            
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a type="button" class="btn btn-warning"    data-toggle="modal" href="#modal-cancelar-confirm"      style="width : 100%">
                                Cancelar análise   
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a type="button" class="btn btn-dark-grey"  data-toggle="modal" href="#modal-desistencia-confirm"   style="width : 100%">
                                Cliente desistiu   
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a type="button" class="btn btn-danger"     data-toggle="modal" href="#modal-recusar-confirm"       style="width : 100%">
                                Negar              
                            </a>
                        </div>
                    <?php } else { ?>
                        <div class="col-md-4">
                            <a type="button" class="btn btn-warning"    data-toggle="modal"  href="#modal-cancelar-confirm"     style="width : 100%">
                                Cancelar análise   
                            </a>
                        </div>
                        <div class="col-md-4">
                            <a type="button" class="btn btn-dark-grey"  data-toggle="modal"  href="#modal-desistencia-confirm"  style="width : 100%">
                                Cliente desistiu   
                            </a>
                        </div>
                        <div class="col-md-4">
                            <a type="button" class="btn btn-danger"     data-toggle="modal"  href="#modal-recusar-confirm"      style="width : 100%">
                                Negar              
                            </a>
                        </div>
                    <?php } ?>

                <?php endif; ?>

            </div>

        </div>
        <!-- end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<?php if (AnaliseDeCredito::model()->qtdAnalisesCliente($proposta->analiseDeCredito->Cliente_id) >= ConfigLN::model()->valorDoParametro("passagens_cpf")) { ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#modal_cliente_alert").modal('show');
        });
    </script>
<?php } ?>
<?php if ($proposta->Status_Proposta_id == "1" && $proposta->hasOmniConfig() == NULL) { ?>
    <button style="width: 100%;" class="btn btn-success" id="consulta_score_btn">Consultar Score</button>
    <input type="hidden" id="cpf_cliente_score" value="<?php echo $cpf; ?>">
    <input type="hidden" id="id_cliente_score" value="<?php echo $proposta->analiseDeCredito->cliente->id; ?>">
    <br /><br />
    <div id="score_consulta" class="row alert" style="display: none; text-align: center; margin: 1px; padding-top: 25px;">
        <h4>
            <i class="fa clip-user"></i> Classe: <span id="consulta_classe"></span> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp 
            <i class="fa clip-notification"></i> Risco: <span id="consulta_risco"></span> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp 
            <i class="clip-bars"></i> Score: <span id="consulta_socore"></span> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp 
            <i class="fa clip-locked"></i> Restrição: <span id="consulta_rest"></span>
        </h4>
    </div>
<?php } ?>

<div class="row">
    <div class="col-sm-12">
        <div class="tabbable">
            <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">

                <li class="active">
                    <a data-toggle="tab" href="#panel_projects">
                        Detalhes da proposta
                    </a>
                </li>

                <li>
                    <a data-toggle="tab" href="#panel_consulta">
                        Consulta Cliente
                    </a>
                </li>

                <li>
                    <a data-toggle="tab" href="#panel_historico">
                        Histórico
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_overview">Visão Geral</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_documentos">Documentos</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_familiares">Familiares</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_edit_account">Contato</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_dados_profissionais">Dados profissionais</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_dados_bancarios">Dados bancários</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_endereco">Endereço</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_referencias">Referências</a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_anexos">Anexos</a>
                </li>

                <?php if (in_array($proposta->Status_Proposta_id, [2, 7])) { ?>

                    <li>
                        <a data-toggle="tab" href="#panel_contrato_proposta">
                            Anexos Contrato
                        </a>
                    </li>

                <?php } ?>

            </ul>          
            <div class="tab-content">
                <div id="panel_projects" class="tab-pane in active">
                    <div class="row">
                        <div class="col-sm-4 col-md-3">
                            <div class="user-left">
                                <table class="table table-condensed table-hover">
                                    <tbody>
                                        <tr>
                                            <td>Parceiro</td>
                                            <td>
                                                <?php echo strtoupper($proposta->analiseDeCredito->filial->getConcat()); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Crediarista</td>
                                            <td>
                                                <?php echo strtoupper($proposta->analiseDeCredito->usuario->username); ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Mais detalhes</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Cliente</td>
                                            <td>
                                                <?php echo strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>CPF</td>
                                            <td id="cpfConsultar">
                                                <?php echo $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Idade</td>
                                            <td id="cpfConsultar">
                                                <?php echo substr(date('d/m/Y'), 6, 7) - substr($proposta->analiseDeCredito->cliente->pessoa->nascimento, 0, 4) . ' anos' ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Vendedor</td>
                                            <td>
                                                <?php echo strtoupper($proposta->analiseDeCredito->vendedor); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Promotor</td>
                                            <td>
                                                <?php echo strtoupper($proposta->analiseDeCredito->promotor); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Alerta</td>
                                            <td>
                                                <?php echo strtoupper($proposta->analiseDeCredito->alerta); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Procedência</td>
                                            <td><?php echo strtoupper($proposta->analiseDeCredito->procedenciaCompra->procedencia); ?></td>
                                        </tr>
                                    </tbody>
                                </table>                       
                            </div>
                        </div>

                        <div class="col-sm-8 col-md-9">

                            <?php if ($proposta->ehCartao()) { ?>
                                <h1>
                                    <font color="orange">
                                    <i class="fa fa-credit-card">
                                    </i>
                                    Proposta de Cartão de Crédito
                                    </font>
                                </h1>

                                <br>

                                <div class="row"> 
                                    <div class="col-md-2">   
                                        <label>
                                            Limite a ser aprovado:
                                        </label>
                                    </div>
                                    <div class="col-md-2">   
                                        <input type="number" id="inputLimiteAprovado" class="form form-control limiteCartao" value="<?php echo $proposta->valor_final; ?>" />
                                    </div>
                                    <div class="col-md-1">
                                        <button disabled class="btn btn-success" id="btnAprovarLimite">
                                            Aprovar
                                        </button>
                                    </div>
                                    <div class="col-md-3">
                                    </div>
                                </div>

                            <?php } else { ?>
                                <div class="col-sm-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <i class="clip-user-5"></i>
                                            Dados financeiros
                                            <div class="panel-tools">
                                                <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="panel-body panel-scroll ps-container" style="height:120px">
                                            <table id="table-detalhes-financeiros" class="table table-striped table-bordered table-hover table-full-width dataTable">
                                                <thead>
                                                    <tr>
                                                        <th width="80" class="no-orderable">Inicial</th>
                                                        <th width="60" class="no-orderable">Entrada</th>
                                                        <th width="50" class="no-orderable">Seguro</th>
                                                        <th width="70" class="no-orderable">Carência</th>
                                                        <th width="70" class="no-orderable">Financiado</th>
                                                        <th width="70" class="no-orderable">Parcelamento</th>
                                                        <th class="no-orderable">Valor final</th>
                                                        <th class="no-orderable">1° parc</th>
                                                        <th class="no-orderable">Última parc</th>
                                                        <th class="no-orderable"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                            <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px; width: 651px;">
                                                <div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div>
                                            </div>
                                            <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px; height: 120px;">
                                                <div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <i class="clip-user-5"></i>
                                            Itens da análise
                                            <div class="panel-tools">
                                                <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="panel-body panel-scroll ps-container" style="height:120px">
                                            <table class="table table-striped table-hover" id="sample-table-1">
                                                <thead>
                                                    <tr>
                                                        <th>Item</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($proposta->analiseDeCredito->listSubgruposDaAnalise() as $item) { ?>
                                                        <tr>                                       
                                                            <td>
                                                                <?php echo strtoupper($item->descricao); ?>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                            <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px; width: 651px;">
                                                <div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div>
                                            </div>
                                            <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px; height: 120px;">
                                                <div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <i class="clip-user-5"></i>
                                            Descrição dos itens
                                            <div class="panel-tools">
                                                <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="panel-body panel-scroll ps-container" style="height:120px">
                                            <table class="table table-striped table-hover" id="sample-table-1">
                                                <thead>
                                                    <tr>
                                                        <th>Descrição</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>                                       
                                                        <td>
                                                            <?php echo $util->cleanStr($proposta->analiseDeCredito->mercadoria, 'upp'); ?>
                                                        </td>
                                                    </tr>                                    
                                                </tbody>
                                            </table>
                                            <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px; width: 651px;">
                                                <div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div>
                                            </div>
                                            <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px; height: 120px;">
                                                <div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="row">

                        <div style="margin-left:10px;margin-bottom:10px">
                            <h3>Mensagens:</h3>
                            <a id="msg_pad" type="button" class="btn btn-primary" data-toggle="modal"  href="#modal_form_new_msg">
                                Enviar Mensagem
                                <i class="fa fa-envelope-o"></i>
                            </a>
                            <a id="msg_int" type="button" class="btn btn-blue" data-toggle="modal"  href="#modal_form_new_msg">
                                Mensagem Interna
                                <i class="fa fa-envelope-o"></i>
                            </a>
                        </div>

                        <div class="" style="width:97%">
                            <table style="margin-left:10px;" id="grid_mensagens" class="table table-striped table-bordered table-hover table-full-width dataTable">
                                <thead>
                                    <tr role="row">
                                        <th>Conteúdo</th>
                                        <th>Data de envio</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="panel_consulta" class="tab-pane">
                    <div class="row">
                        <div class="col-sm-4 col-md-3">
                            <div class="user-left">
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr colspan="2">
                                            <td>
                                                <button class="btn btn-purple" id="btnConsultarCliente">
                                                    Consultar
                                                    <i class="fa fa-eye"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th colspan="2">Dados da Consulta</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Restrição?</td>
                                            <td id="restricao">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Protocolo</td>
                                            <td id="protocolo">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Operador</td>
                                            <td id="operador">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Dados do Cliente</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Nome Completo</td>
                                            <td id="nomeConsumidor">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>CPF</td>
                                            <td id="cpfConsumidor">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Data Nascimento</td>
                                            <td id="nascimentoConsumidor">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Nome da Mãe</td>
                                            <td id="maeConsumidor">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>  
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Endereço</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Logradouro</td>
                                            <td id="logradouroConsumidor">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Número</td>
                                            <td id="numEndConsumidor">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Bairro</td>
                                            <td id="bairroConsumidor">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Complemento</td>
                                            <td id="comEndConsumidor">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Cidade</td>
                                            <td id="cidadeConsumidor">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>UF</td>
                                            <td id="ufConsumidor">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>CEP</td>
                                            <td id="cepConsumidor">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>                       
                            </div>
                        </div>
                        <div class="col-sm-8 col-md-9">
                            <div class="col-sm-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <i class="clip-user-5"></i>
                                        Resultado
                                        <div class="panel-tools">
                                            <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="panel-body panel-scroll ps-container" style="height:220px">
                                        <table class="table table-striped table-bordered table-hover table-full-width dataTable">
                                            <thead>
                                                <tr>
                                                    <td>Ocorrência</td>
                                                    <td>Quantidade</td>
                                                    <td>Última Ocorrência</td>
                                                    <td>Valor</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Telefones vinculados dcto. telefone consultado</td>
                                                    <td id="qtdTelefoneVinculado"></td>
                                                    <td id="ultTelefoneVinculado"></td>
                                                    <td id="vlrTelefoneVinculado"></td>
                                                </tr>
                                                <tr>
                                                    <td>Registro de SPC</td>
                                                    <td id="qtdRegistroSPC"></td>
                                                    <td id="ultRegistroSPC"></td>
                                                    <td id="vlrRegistroSPC"></td>
                                                </tr>
                                                <tr>
                                                    <td>Registro de Serasa</td>
                                                    <td id="qtdRegistroSerasa"></td>
                                                    <td id="ultRegistroSerasa"></td>
                                                    <td id="vlrRegistroSerasa"></td>
                                                </tr>
                                                <tr>
                                                    <td>Informação do Poder Judiciário</td>
                                                    <td id="qtdPoderJudiciario"></td>
                                                    <td id="ultPoderJudiciario"></td>
                                                    <td id="vlrPoderJudiciario"></td>
                                                </tr>
                                                <tr>
                                                    <td>Consulta Realizada</td>
                                                    <td id="qtdConsultaRealizada"></td>
                                                    <td id="ultConsultaRealizada"></td>
                                                    <td id="vlrConsultaRealizada"></td>
                                                </tr>
                                                <tr>
                                                    <td>Alerta de Documentos</td>
                                                    <td id="qtdAlertaDocumentos"></td>
                                                    <td id="ultAlertaDocumentos"></td>
                                                    <td id="vlrAlertaDocumentos"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px; width: 651px;">
                                            <div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div>
                                        </div>
                                        <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px; height: 120px;">
                                            <div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <i class="clip-user-5"></i>
                                        Registro de inadimplência SPC
                                        <div class="panel-tools">
                                            <a class="btn btn-xs btn-link panel-collapse expand" href="#">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="panel-body panel-scroll ps-container collapse" style="height:220px">
                                        <table class="table table-striped table-bordered table-hover table-full-width dataTable">
                                            <thead>
                                                <tr>
                                                    <th>Data Inclusão</th>
                                                    <th>Data Vencimento</th>
                                                    <th>Contrato</th>
                                                    <th>Comprador/Fiador/Avalista</th>
                                                    <th>Valor</th>
                                                    <th>Associado/Credor</th>
                                                    <th>Cidade/UF Origem</th>
                                                    <th>Origem</th>
                                                </tr>
                                            </thead>
                                            <tbody id="HTMLSPC">
                                            </tbody>
                                        </table>
                                        <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px; width: 651px;">
                                            <div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div>
                                        </div>
                                        <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px; height: 120px;">
                                            <div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <i class="clip-user-5"></i>
                                        Registro de inadimplência Serasa
                                        <div class="panel-tools">
                                            <a class="btn btn-xs btn-link panel-collapse expand" href="#">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="panel-body panel-scroll ps-container collapse" style="height:220px">
                                        <table class="table table-striped table-bordered table-hover table-full-width dataTable">
                                            <thead>
                                                <tr>
                                                    <th>Data Inclusão</th>
                                                    <th>Data Vencimento</th>
                                                    <th>Contrato</th>
                                                    <th>Avalista</th>
                                                    <th>Valor</th>
                                                    <th>Origem</th>
                                                    <th>Título</th>
                                                    <th>Cidade/UF Origem</th>
                                                </tr>
                                            </thead>
                                            <tbody id="HTMLSerasa">
                                            </tbody>
                                        </table>
                                        <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px; width: 651px;">
                                            <div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div>
                                        </div>
                                        <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px; height: 120px;">
                                            <div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <i class="clip-user-5"></i>
                                        Consultas Realizadas
                                        <div class="panel-tools">
                                            <a class="btn btn-xs btn-link panel-collapse expand" href="#">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="panel-body panel-scroll ps-container collapse" style="height:220px">
                                        <table class="table table-striped table-bordered table-hover table-full-width dataTable">
                                            <thead>
                                                <tr>
                                                    <th>Data da Consulta</th>
                                                    <th>Associado/Empresa Consultante</th>
                                                    <th>Cidade/UF Origem</th>
                                                    <th>Origem</th>
                                                </tr>
                                            </thead>
                                            <tbody id="HTMLConsulta">
                                            </tbody>
                                        </table>
                                        <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px; width: 651px;">
                                            <div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div>
                                        </div>
                                        <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px; height: 120px;">
                                            <div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div id="panel_historico" class="tab-pane">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-info" style="text-align:center">
                                <strong>Total a pagar: R$ <?php echo number_format($numerosDePagamentos['emAberto'], 2, ',', '.'); ?> </strong>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="alert alert-success">
                                Total pago até hoje: <strong>R$ <?php echo number_format($numerosDePagamentos['pago'], 2, ',', '.'); ?></strong>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="alert alert-warning">
                                Total a vencer:<strong>R$ <?php echo number_format($numerosDePagamentos['emAberto'] - $numerosDePagamentos['atrasado'], 2, ',', '.'); ?></strong>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="alert alert-danger">
                                Total em atraso:<strong>R$ <?php echo number_format($numerosDePagamentos['atrasado'], 2, ',', '.'); ?></strong>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" id="input_cpf_historico" value="<?php echo $cpf; ?>">

                            <table class="table table-striped table-bordered table-hover table-full-width" id="tableHistorico">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th class="hidden-xs">Código</th>
                                        <th>Data</th>
                                        <th>Parceiro</th>
                                        <th>Crediarista</th>
                                        <th>Vlr R$</th>
                                        <th>Entrada R$</th>
                                        <th>Seguro</th>
                                        <th>Vlr fin R$</th>
                                        <th>Parcelas</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>          
                </div>




                <input type="hidden" value="<?php echo $cliente->id ?>" id="cliente_id">
                <div id="panel_overview" class="tab-pane">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="user-left">
                                <div class="center">
                                    <h4>
                                        <a class="editable" href="#" id="nome" data-type="text" data-pk="<?php echo $cliente->pessoa->id ?>" data-url="/cliente/pessoaEditAttr" data-title="Nome do Cliente"><?php echo $cliente->pessoa->nome ?></a>
                                    </h4>
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="user-image">
                                            <div class="fileupload-new thumbnail">
                                                <?php if ($cliente->pessoa->sexo == 'M') { ?>
                                                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/male_avatar.jpg" alt="">
                                                <?php } else { ?>
                                                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/avatar_female.jpg" alt="">
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Dados Pessoais</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Sexo:</td>
                                            <td id="sexo_pessoa"><?php echo $cliente->pessoa->sexo ?></td>
                                            <td>
                                                <a class="btn-edit-att" modal-iframe-uri="<?php echo Yii::app()->request->baseUrl; ?>/cliente/renderFormChangeClienteAttribute?view=changeSexo&cliente_id=<?php echo $cliente->id; ?>" data-toggle="modal" href="#stack1">
                                                    <i class="fa fa-pencil edit-user-info"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Nascimento:</td>
                                            <td id="nascimento_pessoa">
                                                <?php
                                                $nascimento = ( $cliente->pessoa->nascimento != NULL ) ? $util->bd_date_to_view($cliente->pessoa->nascimento) : "__/__/__";
                                                echo $nascimento;
                                                ?>
                                            </td>
                                            <td>
                                                <a class="btn-edit-att" modal-iframe-uri="<?php echo Yii::app()->request->baseUrl; ?>/cliente/renderFormChangeClienteAttribute?view=changeNascimento&cliente_id=<?php echo $cliente->id; ?>" data-toggle="modal" href="#stack1">
                                                    <i class="fa fa-pencil edit-user-info"></i>
                                                </a>                                    
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Nacionalidade:</td>
                                            <td id="nacionalidade_pessoa">
                                                <?php
                                                $nacionalidade = ( $cliente->pessoa->nacionalidade != NULL ) ? $cliente->pessoa->nacionalidade : "__________";
                                                echo $nacionalidade;
                                                ?>
                                            </td>
                                            <td>
                                                <a class="btn-edit-att" modal-iframe-uri="<?php echo Yii::app()->request->baseUrl; ?>/cliente/renderFormChangeClienteAttribute?view=changeNacionalidade&cliente_id=<?php echo $cliente->id; ?>" data-toggle="modal" href="#stack1">
                                                    <i class="fa fa-pencil edit-user-info"></i>
                                                </a>                                    
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Naturalidade:</td>
                                            <td id="naturalidade_pessoa">
                                                <?php $naturalDe = ($cliente->pessoa->getCidadeNaturalidade() != NULL) ? $cliente->pessoa->getCidadeNaturalidade()->nome . ', ' . $cliente->pessoa->getCidadeNaturalidade()->uf : 'Não informado.' ?>
                                                <?php echo $naturalDe; ?>
                                            </td>
                                            <!--<td>
                                               <a class="btn-edit-att" modal-iframe-uri="<?php //echo Yii::app()->request->baseUrl;  ?>/cliente/renderFormChangeClienteAttribute?view=changeNaturalidade&cliente_id=<?php echo $cliente->id; ?>" data-toggle="modal" href="#stack1">
                                               <i class="fa fa-pencil edit-user-info"></i>
                                               </a>                                    
                                            </td>-->
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-sm-7 col-md-8">
                            <div class="row">
                                <div class="col-sm-10">
                                    <table id="grid_dados_sociais" class="table table-striped table-bordered table-hover table-full-width dataTable">
                                        <thead>
                                            <tr>
                                                <th class="no-orderable">Nome da mãe</th>
                                                <th class="no-orderable">Nome do pai</th>
                                                <th class="no-orderable">N° dependentes</th>
                                                <th class="no-orderable">É titular do cpf?</th>
                                                <th class="no-orderable">Estado civil</th>
                                                <th class="no-orderable">Cônjuge compõe renda?</th>
                                                <th class="no-orderable"></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="panel_documentos" class="tab-pane">
                    <div class="user-left">
                        <h3>Documentos</h3>
                        <!--<table id="grid_documentos_cliente" class="table table-striped table-bordered table-hover table-full-width dataTable">
                           <thead>
                              <tr role="row">
                                 <th>Número</th>
                                 <th>Tipo</th>
                                 <th>Órgão Emissor</th>
                                 <th>UF Emissor</th>
                                 <th>Data Emissão</th>
                                 <th></th>
                              </tr>
                           </thead>
                        </table>-->
                        <table id="grid_documentos_cliente" class="table table-striped table-bordered table-hover table-full-width dataTable">
                            <thead>
                                <tr>
                                    <th>Número</th>
                                    <th>Tipo</th>
                                    <th>Órgão Emissor</th>
                                    <th>UF Emissor</th>
                                    <th>Data de Emissão</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($cliente->pessoa->pessoaHasDocumentos as $phd):
                                    $data_emissao = ( $phd->documento->data_emissao != NULL ? $util->bd_date_to_view($phd->documento->data_emissao) : NULL );
                                    ?>
                                    <tr>
                                        <td>
                                            <a data-numerocpf="<?php echo $phd->documento->numero; ?>" class="<?php if ($cliente->hasFichamento() && $phd->documento->Tipo_documento_id == 1) {
                                        echo "remover-cpf";
                                    } ?>" data-docid="<?php echo $phd->documento->id; ?>" style="<?php if ($cliente->hasFichamento() && $phd->documento->Tipo_documento_id == 1) {
                                        echo "color: #d9534f; font-weight: bold;";
                                    } ?>" data-url="/documento/editAttr/" class="<?php if ($phd->documento->tipoDocumento->id != 1) {
                                        echo "dob";
                                    } ?>" href="#" id="numero" data-type="text" data-pk="<?php echo $phd->documento->id ?>"  data-title="Número do documento"><?php echo $phd->documento->numero ?></a>
                                        </td>
                                        <td> 
                                            <a><?php echo $phd->documento->tipoDocumento->tipo ?></a>
                                        </td>
                                        <td>
                                            <a data-url="/documento/editAttr/" class="<?php if ($phd->documento->tipoDocumento->id != 1) {
                                        echo "dob";
                                    } ?>" href="#" id="orgao_emissor" data-type="text" data-pk="<?php echo $phd->documento->id ?>"  data-title="Órgão Emissor"><?php echo $phd->documento->orgao_emissor ?></a>
                                        </td>
                                        <td>
                                            <a data-url="/documento/editAttr/" class="<?php if ($phd->documento->tipoDocumento->id != 1) {
                                    echo "dob";
                                } ?>" href="#" id="uf_emissor" data-type="text" data-pk="<?php echo $phd->documento->id ?>"  data-title="Órgão Emissor"><?php echo $phd->documento->uf_emissor ?></a>
                                        </td>
                                        <td>
                                            <a data-url="/documento/editAttr/" class="<?php if ($phd->documento->tipoDocumento->id != 1) {
                                    echo "dob";
                                } ?>" href="#" id="data_emissao" data-type="text" data-format="YYYY-MM-DD" data-viewformat="DD/MM/YYYY" data-template="D / MMM / YYYY" data-pk="<?php echo $phd->documento->id ?>"  data-title="Selecione uma nova data"><?php echo $data_emissao; ?></a>
                                        </td>
                                    </tr>
<?php endforeach; ?>
                            </tbody>
                        </table>

                        <p class="panel2"></p>
                        <p style="margin-top:40px">
                            <a data-input-controller-action="controller_documento_action" data-controller-action="add" class="btn btn-success btn-add" href="#modal_form_new_doc" data-toggle="modal">
                                Adicionar Documento <i class="fa fa-plus"></i>
                            </a>
                        </p>
                    </div>
                </div>
                <div id="panel_familiares" class="tab-pane">
                    <div class="user-left">
                        <h3>Familiares</h3>
                        <table id="grid_familiares" class="table table-striped table-bordered table-hover table-full-width dataTable">
                            <thead>
                                <tr role="row">
                                    <th>Nome</th>                           
                                    <th>Relação</th>
                                    <th></th>
                                </tr>
                            </thead>
                        </table>
                        <p class="panel2"></p>
                        <p style="margin-top:40px">
                            <a data-input-controller-action="controller_familiar_action" data-controller-action="add" class="btn btn-success btn-add" href="#modal_form_new_familiar" data-toggle="modal">
                                Adicionar Familiar <i class="fa fa-plus"></i>
                            </a>
                        </p>
                    </div>
                </div>
                <div id="panel_edit_account" class="tab-pane">
                    <div class="user-left">
                        <h3>Telefones</h3>
                        <table id="grid_telefones" class="table table-striped table-bordered table-hover table-full-width dataTable">
                            <thead>
                                <tr role="row">
                                    <th>Número</th>
                                    <th>Tipo</th>
                                    <th>Ramal</th>
                                    <th></th>
                                </tr>
                            </thead>
                        </table>
                        <p style="margin-top:40px">
                            <a data-input-controller-action="controller_telefone_action" data-controller-action="add" class="btn btn-success btn-add" href="#modal_form_new_telefone" data-toggle="modal">
                                Adicionar Telefone <i class="fa fa-plus"></i>
                            </a>
                        </p>
                        <h3>Emails</h3>
                        <table id="grid_emails" class="table table-striped table-bordered table-hover table-full-width dataTable">
                            <thead>
                                <tr role="row">
                                    <th>E-mail</th>
                                    <th></th>
                                </tr>
                            </thead>
                        </table>
                        <p style="margin-top:40px">
                            <a data-input-controller-action="controller_email_action" data-controller-action="add" class="btn btn-add btn-success" href="#modal_form_new_email" data-toggle="modal">
                                Adicionar Email <i class="fa fa-plus"></i>
                            </a>
                        </p>
                    </div>
                </div>
                <div id="panel_dados_profissionais" class="tab-pane">
                    <div class="row">
                        <div class="user-left">
                            <div class="col-sm-12">
                                <table style="width:1700px!important;" id="grid_dados_profissionais_cliente" class="table table-striped table-bordered table-hover table-full-width dataTable">
                                    <thead>
                                        <tr>
                                            <th class="no-orderable">Principal?</th>
                                            <th class="no-orderable">Empresa</th>
                                            <th class="no-orderable">CNPJ/CPF</th>
                                            <th class="no-orderable">Data de admissão</th>
                                            <th class="no-orderable">Ocupação</th>
                                            <th class="no-orderable">Classe profissional</th>
                                            <th class="no-orderable">Renda Líquida</th>
                                            <th class="no-orderable">Mes/Ano renda</th>
                                            <th class="no-orderable">Profissão</th>
                                            <th class="no-orderable">Aposentado</th>
                                            <th class="no-orderable">Pensionista</th>
                                            <th class="no-orderable">Tipo de comprovante</th>
                                            <th class="no-orderable">N° do benefício</th>
                                            <th class="no-orderable">Órgão Pagador</th>
                                            <th class="no-orderable">CEP</th>
                                            <th class="no-orderable">Logradouro</th>
                                            <th class="no-orderable">Cidade</th>
                                            <th class="no-orderable">Bairro</th>
                                            <th class="no-orderable">UF</th>
                                            <th class="no-orderable">Telefone</th>
                                            <th class="no-orderable">Ramal</th>
                                            <th class="no-orderable">Email</th>
                                            <th class="no-orderable"></th>
                                        </tr>
                                    </thead>
                                </table>
                                <p style="margin-top:40px">
                                    <a data-input-controller-action="controller_dp_action" data-controller-action="add" data-form-bind="form-add-dado-profissional-client" data-form-action="/dadosProfissionais/add/" class="btn btn-success btn-add" href="#modal_form_new_dados_profissionais" data-toggle="modal">
                                        Adicionar dado profissional <i class="fa fa-plus"></i>
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="panel_dados_bancarios" class="tab-pane">
                    <div class="row">
                        <div class="user-left">
                            <div class="col-sm-12">
                                <table id="grid_dados_bancarios_cliente" class="table table-striped table-bordered table-hover table-full-width dataTable">
                                    <thead>
                                        <tr>
                                            <th>Banco</th>
                                            <th>Ag</th>
                                            <th>Conta</th>
                                            <th>Tipo</th>
                                            <th>Op</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                                <p style="margin-top:40px">
                                    <a data-input-controller-action="controller_db_action" data-controller-action="add" class="btn btn-add btn-success" href="#modal_form_new_conta_bancaria" data-toggle="modal">
                                        Adicionar conta <i class="fa fa-plus"></i>
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="panel_endereco" class="tab-pane">
                    <div class="row">
                        <div class="user-left">
                            <div class="col-sm-12">
                                <table id="grid_enderecos_cliente" class="table table-striped table-bordered table-hover table-full-width dataTable">
                                    <thead>
                                        <tr>
                                            <th>Logradouro</th>
                                            <th>Bairro</th>
                                            <th>N°</th>
                                            <th>Cidade</th>
                                            <th>CEP</th>
                                            <th>UF</th>
                                            <th>Complemento</th>
                                            <th>End. de Cobrança?</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                                <p style="margin-top:40px">
                                    <a data-input-controller-action="controller_endereco_action" data-controller-action="add" class="btn-add btn btn-success" href="#modal_form_new_endereco" data-toggle="modal" id="btn_modal_form_new_client">
                                        Adicionar endereço <i class="fa fa-plus"></i>
                                    </a>                           
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="panel_referencias" class="tab-pane">
                    <div class="row">
                        <div class="user-left">
                            <div class="col-sm-12">
                                <table id="grid_referencias_cliente" class="table table-striped table-bordered table-hover table-full-width dataTable">
                                    <thead>
                                        <tr>
                                            <th>Parentesco</th>
                                            <th>Nome</th>
                                            <th>Telefone</th>
                                            <th>Tipo</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                                <p style="margin-top:40px">
                                    <a data-input-controller-action="controller_referencia_action" data-controller-action="add" class="btn-add btn btn-success" href="#modal_form_new_referencia" data-toggle="modal" id="btn_modal_form_new_referencia">
                                        Adicionar referência <i class="fa fa-plus"></i>
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="panel_anexos" class="tab-pane">
                    <div class="user-left">
                        <h3>Anexos</h3>
                        <table id="grid_anexos" class="table table-striped table-bordered table-hover table-full-width dataTable">
                            <thead>
                                <tr role="row">
                                    <th>Descrição</th>
                                    <th>Extensão</th>
                                    <th>Data de envio</th>
                                    <th></th>
                                </tr>
                            </thead>
                        </table>
                        <p class="panel2"></p>
                        <p style="margin-top:40px">
                            <a data-input-controller-action="controller_anexos_action" data-controller-action="add" class="btn btn-success btn-add" href="#modal_form_new_anexo" data-toggle="modal">
                                Adicionar Anexo <i class="fa fa-plus"></i>
                            </a>
                        </p>
                    </div>
                </div>
<?php if (Yii::app()->session['usuario']->tipo_id == 4) { ?>
                    <div id="panel_history" class="tab-pane">
                        <div class="row">
                            <div class="col-md-12">

                                <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                                    <thead>
                                        <tr>
                                            <th class="hidden-xs">Código</th>
                                            <th>Data</th>
                                            <th>Valor financiado R$</th>
                                            <th>Valor final a pagar R$</th>
                                            <th>N° parcelas</th>
                                            <th>Status</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="alert alert-info" style="text-align:center">
                                    <strong>Total a pagar: R$ <?php echo number_format($numerosDePagamentos['emAberto'], 2, ',', '.'); ?> </strong>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="alert alert-success">
                                    Total pago até hoje: <strong>R$ <?php echo number_format($numerosDePagamentos['pago'], 2, ',', '.'); ?></strong>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="alert alert-warning">
                                    Vencendo hoje:<strong>R$ <?php echo number_format($numerosDePagamentos['vencendoHoje'], 2, ',', '.'); ?></strong>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="alert alert-danger">
                                    Em atraso:<strong>R$ <?php echo number_format($numerosDePagamentos['atrasado'], 2, ',', '.'); ?></strong>
                                </div>
                            </div>
                        </div>
                    </div>
<?php } ?>
            </div>

            <!--Modal formulario editar cadastro-->
            <div id="modal_form_new_cadastro" class="modal fade" tabindex="-1" data-width="850" style="display: none;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Editar cadastro</h4>
                </div>
                <form id="form-add-cadastro">
                    <div class="modal-body">
                        <div class="row">
                            <div class="row-centralize">

                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Nome da mãe<span class="symbol required"></span></label>
                                                <input required class="form-control" type="text" name="Cadastro[nome_da_mae]" id="nome_da_mae">
                                                <input type="hidden" name="Cliente_id" value="<?php echo $cliente->id ?>">
                                                <input type="hidden" name="cadastro_id" id="cadastro_id">
                                                <input value="update" id="controller_cadastro_action" type="hidden" name="controller_action">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Nome do pai<span class="symbol required"></span></label>
                                                <input class="form-control" type="text" name="Cadastro[nome_do_pai]" id="nome_do_pai">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">N° de dependentes<span class="symbol required"></span></label>
                                                <input required class="form-control" type="text" name="Cadastro[numero_de_dependentes]" id="numero_de_dependentes">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Cônjuge compõe renda?<span class="symbol required"></span></label>
                                                <select required id="conjugue_compoe_renda" name="Cadastro[conjugue_compoe_renda]" class="form-control search-select">
                                                    <option value="">Selecione:</option>
                                                    <?php foreach (TTable::model()->findAll() as $tt) { ?>
                                                        <option value="<?php echo $tt->id ?>"><?php echo $tt->flag ?></option>
<?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">É titular do cpf?<span class="symbol required"></span></label>
                                                <select required id="titular_do_cpf" name="Cadastro[titular_do_cpf]" class="form-control search-select">
                                                    <option value="">Selecione:</option>
<?php foreach (TTable::model()->findAll() as $tt) { ?>
                                                        <option value="<?php echo $tt->id ?>"><?php echo $tt->flag ?></option>
<?php } ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Estado civil:<span class="symbol required"></span></label>
                                                <select required id="Estado_Civil_id" name="Estado_Civil" class="form-control search-select">
                                                    <option value="">Selecione:</option>
<?php foreach (EstadoCivil::model()->findAll() as $ec) { ?>
                                                        <option value="<?php echo $ec->id ?>"><?php echo $ec->estado ?></option>
<?php } ?>
                                                </select>
                                            </div>
                                        </div>


                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">

                        <div class="panel"></div>
                        <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
                        <button data-modal-id="modal_form_new_cadastro" data-checkbox-continuar="ds_checkbox_continuar" data-div-return-id="cadastro_ds_msg_return" data-table-redraw="grid_dados_sociais" data-url-request="/cadastro/update/" data-form-id="form-add-cadastro" type="submit" class="btn btn-blue btn-send">Salvar</button>
                        <div class="row">
                        </div>
                        <br>
                        <div id="cadastro_ds_msg_return" class="alert" style="text-align:left; display:none"></div>
                    </div>
                </form>
            </div>
            <!--Formulário familiares-->
            <div id="modal_form_new_familiar" class="modal fade" tabindex="-1" data-width="960" style="display: none;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Cadastrar Familiar</h4>
                </div>
                <form id="form-add-familiar">
                    <div class="modal-body">
                        <div class="row">
                            <div class="row-centralize">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-11">
                                            <div class="form-group">
                                                <label class="control-label">
                                                    Nome completo <span class="symbol required"></span>
                                                </label>
                                                <input required name="Pessoa[nome]" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="control-label">
                                                    Nascimento <span class="symbol required"></span>
                                                </label>
                                                <input required name="Pessoa[nascimento]" id="nascimento" class="dateBR form-control input-mask-date" type="text">
                                                <input id="controller_familiar_action" type="hidden" name="controller_action">
                                                <input type="hidden" name="Cliente_id" value="<?php echo $cliente->id ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Sexo <span class="symbol required"></span></label>
                                                <?php
                                                echo CHtml::dropDownList('Pessoa[sexo]', 'Pessoa[sexo]', CHtml::listData(Sexo::model()->findAll(), 'sigla', 'sexo'), array('class' => 'form-control search-select select2', 'prompt' => 'Selecione:', 'required' => true));
                                                ?>  
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Parentesco<span class="symbol required"></span></label>
<?php
echo CHtml::dropDownList('Parentesco_id', 'Parentesco_id', CHtml::listData(Parentesco::model()->findAll('nome = "Cônjuge"'), 'id', 'nome'), array('class' => 'form-control search-select select2', 'prompt' => 'Selecione:', 'required' => true));
?>  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="control-label">Nacionalidade <span class="symbol required"></span></label>
                                                <select required="true" id="select-nacionalidade" name="Pessoa[nacionalidade]" class="draft_sent form-control search-select select2">
                                                    <option value="">Selecione:</option>
                                                    <option value="Brasileira">Brasileira</option>
                                                    <option value="Extrangeira">Extrangeira</option>
                                                </select>

                                            </div>
                                        </div>

                                        <div class="col-md-7" id="select-estados-wrapper">
                                            <div class="form-group">
                                                <label class="control-label">UF <span class="symbol required"></span></label>
<?php
echo CHtml::dropDownList('Pessoa[naturalidade]', 'Pessoa[naturalidade]', CHtml::listData(Estados::model()->findAll(), 'sigla', 'nome'), array('class' => 'form-control search-select select2', 'prompt' => 'Selecione:', 'required' => true));
?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" id="select-estados-wrapper">
                                            <div class="form-group">
                                                <label class="control-label">Cidade <span class="symbol required"></span></label>
                                                <input required="true" name="Pessoa[naturalidade_cidade]" placeholder="Selecione" type="hidden" id="selectCidades" class="select2" style="display:block">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <label class="checkbox-inline" style="float:left">
                            <input id="checkbox_fam_continuar" type="checkbox" value="">
                            Continuar Cadastrando
                        </label>
                        <div class="panel"></div>
                        <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
                        <button data-modal-id="modal_form_new_familiar" data-checkbox-continuar="checkbox_fam_continuar" data-div-return-id="cadastro_fam_msg_return" data-table-redraw="grid_familiares" data-url-request="/familiar/update/" data-form-id="form-add-familiar" type="submit" class="btn btn-blue btn-send">Salvar</button>
                        <div class="row">
                        </div>
                        <br>
                        <div id="cadastro_fam_msg_return" class="alert" style="text-align:left">
                        </div>
                    </div>
                </form>
            </div> 

            <!--Formulario telefones-->
            <div id="modal_form_new_telefone" class="modal fade" tabindex="-1" data-width="700" style="display: none;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Adicionar telefone</h4>
                </div>
                <form id="form-add-telefone">
                    <div class="modal-body">
                        <div class="row">
                            <div class="row-centralize">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Telefone 1 <span class="symbol required"></span></label>
                                                <input required class="form-control telefone" type="text" name="TelefoneCliente[numero]" id="telefone_numero">
                                                <input type="hidden" name="Cliente_id" value="<?php echo $cliente->id ?>">
                                                <input type="hidden" name="telefone_id" id="telefone_id">
                                                <input id="controller_telefone_action" type="hidden" name="controller_action">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Tipo de telefone</label>
<?php
echo CHtml::dropDownList('TelefoneCliente[Tipo_Telefone_id]', 'TelefoneCliente[Tipo_Telefone_id]', CHtml::listData(TipoTelefone::model()->findAll(), 'id', 'tipo'), array('class' => 'form-control search-select', 'prompt' => 'Selecione:', 'required' => true));
?>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Ramal </label>
                                                <input class="form-control" type="text" name="TelefoneCliente[ramal]" id="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <label class="checkbox-inline" style="float:left">
                            <input id="telefone_checkbox_continuar" type="checkbox" value="">
                            Continuar Cadastrando
                        </label>
                        <div class="panel"></div>
                        <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
                        <button data-modal-id="modal_form_new_telefone" data-checkbox-continuar="telefone_checkbox_continuar" data-div-return-id="cadastro_telefone_msg_return" data-table-redraw="grid_telefones" data-url-request="/telefone/update/" data-form-id="form-add-telefone" type="submit" class="btn btn-blue btn-send">Salvar</button>
                        <div class="row">
                        </div>
                        <br>
                        <div id="cadastro_telefone_msg_return" class="alert" style="text-align:left; display:none"></div>
                    </div>
                </form>
            </div>

            <!--Formulario dados profissionais-->
            <div id="modal_form_new_dados_profissionais" class="modal fade" tabindex="-1" data-width="960" style="display: none;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Adicionar Dado Profissional</h4>
                </div>
                <form id="form-add-dado-profissional-client">
                    <div class="modal-body">
                        <div class="row">
                            <div class="row-centralize">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Classe Profissional</label>
<?php
echo CHtml::dropDownList('DadosProfissionais[Classe_Profissional_id]', 'DadosProfissionais[Classe_Profissional_id]', CHtml::listData(ClasseProfissional::model()->findAll(), 'id', 'classe'), array('class' => 'form-control search-select', 'prompt' => 'Selecione:', 'required' => true));
?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">CNPJ/CPF</label>
                                                <input id="dp_cnpj_cpf" class="form-control" type="text" name="DadosProfissionais[cnpj_cpf]">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Empresa</label>
                                                <input id="dp_empresa" class="form-control" type="text" name="DadosProfissionais[empresa]">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Ocupação atual</label>
<?php
echo CHtml::dropDownList('DadosProfissionais[Tipo_Ocupacao_id]', 'DadosProfissionais[Tipo_Ocupacao_id]', CHtml::listData(TipoOcupacao::model()->findAll(), 'id', 'tipo'), array('class' => 'form-control search-select', 'prompt' => 'Selecione:', 'required' => true));
?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Data de admissão</label>
                                                <input required="required" id="dp_data_admissao" data-date-format="dd/mm/yyyy" data-date-viewmode="years" class="form-control date-picker" type="text" name="DadosProfissionais[data_admissao]">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Profissão</label>
<?php
echo CHtml::dropDownList('DadosProfissionais[profissao]', 'DadosProfissionais[profissao]', CHtml::listData(Profissao::model()->findAll(), 'profissao', 'profissao'), array('class' => 'form-control search-select', 'prompt' => 'Selecione:'));
?> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Renda Líquida R$</label>
                                                <input id="dp_renda_liquida" required class="form-control dinheiro currency" type="text" name="DadosProfissionais[renda_liquida]">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Mês / Ano Renda</label>
                                                <input id="dp_mes_ano_renda" class="form-control date-picker-mes-ano" type="text" name="DadosProfissionais[mes_ano_renda]">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Órgão Pagador</label>
<?php
echo CHtml::dropDownList('DadosProfissionais[orgao_pagador]', 'DadosProfissionais[orgao_pagador]', CHtml::listData(OrgaoPagador::model()->findAll(), 'nome', 'nome'), array('class' => 'form-control search-select', 'prompt' => 'Selecione:'));
?> 
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">N° do benefício</label>
                                                <input id="dp_numero_do_beneficio" class="form-control" type="text" name="DadosProfissionais[numero_do_beneficio]">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Tipo de comprovante</label>
<?php
echo CHtml::dropDownList('DadosProfissionais[TipoDeComprovante]', 'DadosProfissionais[TipoDeComprovante]', CHtml::listData(TipoDeComprovante::model()->findAll(), 'tipo', 'tipo'), array('class' => 'form-control search-select', 'prompt' => 'Selecione:'));
?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">CEP</label>
                                            <input required="required" id="cep_dados_profissionais" class="form-control cep" type="text" name="EnderecoProfiss[cep]">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Logradouro</label>
                                            <input required="required" id="logradouro_dados_profissionais" class="form-control" type="text" name="EnderecoProfiss[logradouro]">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="control-label">Número</label>
                                            <input required="required" id="numero_endereco_dados_profissionais"  class="form-control" type="text" name="EnderecoProfiss[numero]">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Cidade</label>
                                            <input required="required" id="cidade_dados_profissionais" class="form-control" type="text" name="EnderecoProfiss[cidade]">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="control-label">Bairro</label>
                                            <input required="required" id="bairro_dados_profissionais" class="form-control" type="text" name="EnderecoProfiss[bairro]">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">UF</label>
<?php
echo CHtml::dropDownList('EnderecoProfiss[uf]', 'EnderecoProfiss[uf]', CHtml::listData(Estados::model()->findAll(), 'sigla', 'nome'), array('class' => 'form-control search-select', 'prompt' => 'Selecione:', 'required' => true)
);
?>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="control-label">Complemento</label>
                                            <input id="dp_endereco_complemento" class="form-control" type="text" name="EnderecoProfiss[complemento]">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Telefone</label>
                                            <input id="dp_telefone_numero" class="form-control telefone" type="text" name="TelefoneProfiss[numero]" required="required">
                                            <input id="dp_telefone_id" type="hidden" name="TelefoneProfiss[id]">
                                            <input id="controller_dp_action" type="hidden" name="controller_action">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Ramal</label>
                                            <input id="dp_telefone_ramal" class="form-control" type="text" name="TelefoneProfiss[ramal]">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Tipo de telefone</label>
<?php
echo CHtml::dropDownList('TelefoneProfiss[Tipo_Telefone_id]', 'TelefoneProfiss[Tipo_Telefone_id]', CHtml::listData(TipoTelefone::model()->findAll(), 'id', 'tipo'), array('class' => 'form-control search-select', 'prompt' => 'Selecione:', 'required' => true));
?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Email</label>
                                            <input id="dp_email" class="form-control" type="text" name="EmailProfiss[email]">
                                            <input type="hidden" name="Cliente_id" value="<?php echo $cliente->id ?>">
                                            <input type="hidden" name="email_id" id="dp_email_id">
                                            <input type="hidden" name="dados_profissionais_id" id="dados_profissionais_id">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Usar como principal? <span class="symbol required"></span></label>
                                            <select required id="DadosProfissionais_principal" name="DadosProfissionais[principal]" class="form-control search-select">
                                                <option value="">Selecione:</option>
<?php foreach (TTable::model()->findAll() as $tt) { ?>
                                                    <option value="<?php echo $tt->id ?>"><?php echo $tt->flag ?></option>
<?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <label class="checkbox-inline" style="float:left">
                            <input id="dp_checkbox_continuar" type="checkbox" value="">
                            Continuar Cadastrando
                        </label>
                        <div class="panel"></div>
                        <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
                        <button data-modal-id="modal_form_new_dados_profissionais" data-checkbox-continuar="dp_checkbox_continuar" data-div-return-id="cadastro_dadoprofissional_msg_return" data-table-redraw="grid_dados_profissionais_cliente" data-url-request="/dadosProfissionais/update/" data-form-id="form-add-dado-profissional-client" type="submit" class="btn btn-blue btn-send">Salvar</button>
                        <div class="row">
                        </div>
                        <br>
                        <div id="cadastro_dadoprofissional_msg_return" class="alert" style="text-align:left; display:none"></div>
                    </div>
                </form>
            </div>

            <!--Formulario dados bancarios-->
            <div id="modal_form_new_conta_bancaria" class="modal fade" tabindex="-1" data-width="960" style="display: none;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Adicionar Conta bancária</h4>
                </div>
                <form id="form-add-dado-bancario-client">
                    <div class="modal-body">
                        <div class="row">
                            <div class="row-centralize">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="control-label">N° conta</label>
                                                <input id="conta_numero" required class="form-control" type="text" name="DadosBancarios[numero]">
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="control-label">Tipo de conta</label>
<?php
echo CHtml::dropDownList('DadosBancarios[Tipo_Conta_Bancaria_id]', 'DadosBancarios[Tipo_Conta_Bancaria_id]', CHtml::listData(TipoContaBancaria::model()->findAll(), 'id', 'tipo'), array('class' => 'form-control search-select', 'prompt' => 'Selecione:', 'required' => true));
?> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="control-label">
                                                    Agência <span class="symbol required"></span>
                                                </label>
                                                <input id="conta_agencia" required class="form-control" type="text" name="DadosBancarios[agencia]">
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="control-label">Banco</label>
<?php
echo CHtml::dropDownList('DadosBancarios[Banco_id]', 'DadosBancarios[Banco_id]', CHtml::listData(Banco::model()->findAll(), 'id', 'nome'), array('class' => 'form-control search-select', 'prompt' => 'Selecione:', 'required' => true));
?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="control-label">Operação</label>
                                                <input id="conta_operacao" class="form-control" type="text" name="DadosBancarios[operacao]">
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="control-label">Data de abertura</label>
                                                <input id="conta_data_abertura" class="dateBR form-control input-mask-date" type="text" name="DadosBancarios[data_abertura]">
                                                <input  type="hidden" name="Cliente_id" value="<?php echo $cliente->id ?>">
                                                <input  id="dados_bancarios_id" type="hidden" name="dados_bancarios_id">
                                                <input id="controller_db_action" type="hidden" name="controller_action">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <label class="checkbox-inline" style="float:left">
                            <input id="db_checkbox_continuar" type="checkbox" value="">
                            Continuar Cadastrando
                        </label>
                        <div class="panel"></div>
                        <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
                        <button data-modal-id="modal_form_new_conta_bancaria" data-checkbox-continuar="db_checkbox_continuar" data-div-return-id="cadastro_dadobancario_msg_return" data-table-redraw="grid_dados_bancarios_cliente" data-url-request="/dadosBancarios/update/" data-form-id="form-add-dado-bancario-client" type="submit" class="btn btn-blue btn-send">Salvar</button>
                        <div class="row">
                        </div>
                        <br>
                        <div id="cadastro_dadobancario_msg_return" class="alert" style="text-align:left; display:none"></div>
                    </div>
                </form>
            </div>

            <!--Formulario endereços-->
            <div id="modal_form_new_endereco" class="modal fade" tabindex="-1" data-width="960" style="display: none;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Adicionar endereço</h4>
                </div>
                <form id="form-add-endereco-client">
                    <div class="modal-body">
                        <div class="row">
                            <div class="row-centralize">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">
                                                    CEP <span class="symbol required"></span>
                                                </label>
                                                <input required class="form-control cep" type="text" name="EnderecoCliente[cep]" id="cep_cliente">
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <label class="control-label">Logradouro <span class="symbol required"></span></label>
                                                <input required class="form-control" type="text" name="EnderecoCliente[logradouro]" id="logradouro_cliente">
                                                <input type="hidden" name="Cliente_id" value="<?php echo $cliente->id ?>">
                                                <input type="hidden" name="endereco_id" id="endereco_id" >
                                                <input id="controller_endereco_action" type="hidden" name="controller_action">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Cidade <span class="symbol required"></span></label>
                                                <input required class="form-control" type="text" name="EnderecoCliente[cidade]" id="cidade_cliente">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Bairro <span class="symbol required"></span></label>
                                                <input required class="form-control" type="text" name="EnderecoCliente[bairro]" id="bairro_cliente">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Tempo de residência (mes/ano) <span class="symbol required"></span></label>
                                                <input required class="date-picker-mes-ano form-control" type="text" name="EnderecoCliente[tempo_de_residencia]" id="tempo_de_residencia">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Tipo de moradia <span class="symbol required"></span></label>
                                                <select required id="EnderecoCliente_Tipo_Moradia_id" name="EnderecoCliente[Tipo_Moradia_id]" class="form-control search-select">
                                                    <option value="">Selecione:</option>
<?php foreach (TipoMoradia::model()->findAll() as $tipoMoradia) { ?>
                                                        <option value="<?php echo $tipoMoradia->id ?>"><?php echo $tipoMoradia->tipo ?></option>
<?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!---->
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Número <span class="symbol required"></span></label>
                                                <input required id="numero_end_cliente" class="form-control" type="text" name="EnderecoCliente[numero]">
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <label class="control-label">Complemento</label>
                                                <input id="complemento_end_cliente" class="form-control" type="text" name="EnderecoCliente[complemento]">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">UF <span class="symbol required"></span></label>
                                                <select required id="EnderecoCliente_uf" name="EnderecoCliente[uf]" class="form-control search-select">
                                                    <option value="">Selecione:</option>
<?php foreach (Estados::model()->findAll() as $uf) { ?>
                                                        <option value="<?php echo $uf->sigla ?>"><?php echo $uf->sigla ?></option>
<?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Tipo de endereço <span class="symbol required"></span></label>
                                                <select required id="EnderecoCliente_Tipo_Endereco_id" name="EnderecoCliente[Tipo_Endereco_id]" class="form-control search-select">
                                                    <option value="">Selecione:</option>
<?php foreach (TipoEndereco::model()->findAll() as $TipoEndereco) { ?>
                                                        <option value="<?php echo $TipoEndereco->id ?>"><?php echo $TipoEndereco->tipo ?></option>
<?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Endereço de cobrança? <span class="symbol required"></span></label>
                                                <select required id="EnderecoCliente_endereco_de_cobranca" name="EnderecoCliente[endereco_de_cobranca]" class="form-control search-select">
                                                    <option value="">Selecione:</option>
<?php foreach (TTable::model()->findAll() as $tt) { ?>
                                                        <option value="<?php echo $tt->id ?>"><?php echo $tt->flag ?></option>
<?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    </div>
                                </div>
                                <!---->
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <label class="checkbox-inline" style="float:left">
                            <input id="end_checkbox_continuar" type="checkbox" value="">
                            Continuar Cadastrando
                        </label>
                        <div class="panel"></div>
                        <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
                        <button  data-modal-id="modal_form_new_endereco" data-checkbox-continuar="end_checkbox_continuar" data-div-return-id="cadastro_endereco_msg_return" data-table-redraw="grid_enderecos_cliente" data-url-request="/endereco/update/" data-form-id="form-add-endereco-client" type="submit" class="btn btn-blue btn-send">Salvar</button>
                        <div class="row">
                        </div>
                        <br>
                        <div id="cadastro_endereco_msg_return" class="alert" style="text-align:left">
                        </div>
                    </div>
                </form>
            </div> 

            <!--Formulario referencias-->
            <div id="modal_form_new_referencia" class="modal fade" tabindex="-1" data-width="1000" style="display: none;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Adicionar Referência</h4>
                </div>
                <form id="form-add-referencia-client">
                    <div class="modal-body">
                        <div class="row">
                            <div class="row-centralize">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="control-label">Parentesco</label>
                                                <input id="referencia_parentesco" required class="form-control" type="text" name="Referencia[parentesco]">
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <label class="control-label">Nome</label>
                                                <input id="referencia_nome" required class="form-control" type="text" name="Referencia[nome]">
                                                <input value="<?php echo $cliente->id ?>" type="hidden" name="Referencia[Cliente_id]">
                                                <input type="hidden" name="Cliente_id" value="<?php echo $cliente->id ?>">
                                                <input type="hidden" name="referencia_id" id="referencia_cliente_id">
                                                <input id="controller_referencia_action" type="hidden" name="controller_action">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <label class="control-label">N° Telefone</label>
                                                <input id="telefone_referencia_numero" required class="telefone form-control" type="text" name="TelefoneReferencia[numero]">
                                                <input type="hidden" name="telefone_referencia_id" id="telefone_referencia_id">
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="control-label">Tipo</label>
<?php
echo CHtml::dropDownList('Referencia[Tipo_Referencia_id]', 'Referencia[Tipo_Referencia_id]', CHtml::listData(TipoReferencia::model()->findAll(), 'id', 'tipo'), array('class' => 'form-control search-select', 'prompt' => 'Selecione:', 'required' => true));
?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <label class="checkbox-inline" style="float:left">
                            <input id="referencia_checkbox_continuar" type="checkbox" value="">
                            Continuar Cadastrando
                        </label>
                        <div class="panel"></div>
                        <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
                        <button data-modal-id="modal_form_new_referencia" data-checkbox-continuar="referencia_checkbox_continuar" data-div-return-id="cadastro_referencia_msg_return" data-table-redraw="grid_referencias_cliente" data-url-request="/referencia/update/" data-form-id="form-add-referencia-client" type="submit" class="btn btn-blue btn-send">Salvar</button>
                        <div class="row">
                        </div>
                        <br>
                        <div id="cadastro_referencia_msg_return" class="alert" style="text-align:left; display:none"></div>
                    </div>
                </form>
            </div>

            <!--Formulario documentos-->
            <div id="modal_form_new_doc" class="modal fade" tabindex="-1" data-width="800" style="display: none;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Adicionar documento</h4>
                </div>
                <form id="form-add-doc">
                    <div class="modal-body">
                        <div class="row">
                            <div class="row-centralize">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label class="control-label">Tipo de documento</label>
                                                <?php
                                                echo CHtml::dropDownList('Documento[Tipo_documento_id]', 'Documento[Tipo_documento_id]', CHtml::listData(TipoDocumento::model()->findAll('id <> 1'), 'id', 'tipo'), array('class' => 'form-control search-select', 'prompt' => 'Selecione:', 'required' => true));
                                                ?> 
                                            </div>
                                        </div>                     
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <label class="control-label">N° documento <span class="symbol required"></span></label>
                                                <input id="documento_numero" required class="form-control" type="text" name="Documento[numero]">
                                                <input type="hidden" name="Cliente_id" value="<?php echo $cliente->id ?>">
                                                <input id="documento_id" type="hidden" name="documento_id">
                                                <input id="controller_documento_action" type="hidden" name="controller_action">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">
                                                    Data emissão
                                                </label>
                                                <input id="documento_data_emissao" class="form-control dateBR" type="text" name="Documento[data_emissao]">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Órgão emissor</label>
                                                <input id="documento_orgao_emissor" class="form-control" type="text" name="Documento[orgao_emissor]">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Uf emissor</label>
                                                <?php
                                                echo CHtml::dropDownList('Documento[uf_emissor]', 'Documento[uf_emissor]', CHtml::listData(Estados::model()->findAll(), 'sigla', 'nome'), array('class' => 'form-control search-select'));
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <label class="checkbox-inline" style="float:left">
                            <input id="doc_checkbox_continuar" type="checkbox" value="">
                            Continuar Cadastrando
                        </label>
                        <div class="panel"></div>
                        <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
                        <button data-modal-id="modal_form_new_doc" data-checkbox-continuar="doc_checkbox_continuar" data-div-return-id="cadastro_doc_msg_return" data-table-redraw="grid_documentos_cliente" data-url-request="/documento/update/" data-form-id="form-add-doc" type="submit" class="btn-new btn btn-blue btn-send">Salvar</button>
                        <div class="row">
                        </div>
                        <br>
                        <div id="cadastro_doc_msg_return" class="alert" style="text-align:left; display:none"></div>
                    </div>
                </form>
            </div>

            <!--Formulario anexos-->
            <div id="modal_form_new_anexo" class="modal fade" tabindex="-1" data-width="700" style="display: none;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Anexar arquivo</h4>
                </div>
                <form action="/anexos/add/" method="post" enctype="multipart/form-data" id="MyUploadForm">
                    <div class="modal-body">
                        <div class="row">
                            <div class="row-centralize">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Arquivo <span class="symbol required"></span></label>
                                                <input class="control-label" required="required" name="FileInput" id="FileInput" type="file" />
                                                <input type="hidden" name="Cliente_id" value="<?php echo $cliente->id ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <label class="control-label">Descrição</label>
                                                <input required class="form-control" type="text" name="Anexo[descricao]">
                                                <!--
                                                 O valor de ['origem'] precisa ser enviado, para a action de adicionar anexos
                                                 tomar a decisão de upar no server local, ou no bucket s3.
                                                 É apenas um workaround temporário, para a migração ser feita no decorrer dos dias,
                                                 pois várias telas trabalham com anexo de arquivos - Eric
                                                -->
                                                <input type="hidden" name="origem" value="tela_proposta_resume">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <label class="checkbox-inline" style="float:left">
                            <input id="anexo_checkbox_continuar" type="checkbox" value="">
                            Continuar Cadastrando
                        </label>
                        <div class="panel"></div>
                        <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
                        <button data-checkbox-continuar="" data-div-return-id="" data-table-redraw="" data-url-request="" data-form-id="" type="submit" class="btn btn-blue">Salvar</button>
                        <div class="row">
                        </div>
                        <div id="progressbox" >
                            <div id="progressbar"></div >
                            <div id="statustxt">0%</div>
                        </div>
                        <br>
                        <div id="cadastro_anexo_msg_return" class="alert" style="text-align:left; display:none"></div>
                    </div>
                </form>
            </div>

            <!--Modal confirm delete anexo-->
            <div id="modal_confirm_delete_anexo" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
                <div class="modal-body">
                    <p>
                        Deseja realmente excluir este arquivo?
                    </p>
                </div>
                <div class="modal-footer">
                    <form action="/anexos/remove/" method="POST" id="form-remove-anexo">   
                        <button type="button" data-dismiss="modal" class="btn btn-default">
                            Cancelar
                        </button>
                        <input type="hidden" id="ipt-hdn-id-anexo" name="ipt_id_anexo">
                        <button type="submit" class="btn btn-primary btn-excluir-anexo">
                            Excluir
                        </button>
                    </form>
                </div>
            </div>
            <!--Modal confirm delete anexo-->
            <div id="modal_confirm_delete_anexo" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
                <div class="modal-body">
                    <p>
                        Deseja realmente excluir este arquivo?
                    </p>
                </div>
                <div class="modal-footer">
                    <form action="/anexos/remove/" method="POST" id="form-remove-anexo">   
                        <button type="button" data-dismiss="modal" class="btn btn-default">
                            Cancelar
                        </button>
                        <input type="hidden" id="ipt-hdn-id-anexo" name="ipt_id_anexo">
                        <button type="submit" class="btn btn-primary btn-excluir-anexo">
                            Excluir
                        </button>
                    </form>
                </div>
            </div>
            <!--Formulario emails-->
            <div id="modal_form_new_email" class="modal fade" tabindex="-1" data-width="560" style="display: none;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title">Adicionar email</h4>
                </div>
                <form id="form-add-email">
                    <div class="modal-body">
                        <div class="row">
                            <div class="row-centralize">
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="control-label">Email</label>
                                                <input id="email_email" required class="form-control email" type="text" name="EmailCliente[email]">
                                                <input type="hidden" name="Cliente_id" value="<?php echo $cliente->id ?>">
                                                <input type="hidden" name="email_id" id="email_cliente_id">
                                                <input id="controller_email_action" type="hidden" name="controller_action">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <label class="checkbox-inline" style="float:left">
                            <input id="email_checkbox_continuar" type="checkbox" value="">
                            Continuar Cadastrando
                        </label>
                        <div class="panel"></div>
                        <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
                        <button data-modal-id="modal_form_new_email" data-checkbox-continuar="email_checkbox_continuar" data-div-return-id="cadastro_email_msg_return" data-table-redraw="grid_emails" data-url-request="/email/update/" data-form-id="form-add-email" type="submit" class="btn btn-blue btn-send">Salvar</button>
                        <div class="row">
                        </div>
                        <br>
                        <div id="cadastro_email_msg_return" class="alert" style="text-align:left; display:none"></div>
                    </div>
                </form>
            </div>
            <?php if (in_array($proposta->Status_Proposta_id, [2, 7])) { ?>

                <input type="hidden" name="Entidade"    id="Entidade_fup"       value="<?php
                /*if (in_array($proposta->Financeira_id, [10, 11])) 
                {
                    echo "Proposta";
                } 
                else 
                {
                    echo "Emprestimo";
                }*/
                if (isset($isEmprestimo)) 
                {
                    echo "Emprestimo";
                } 
                else 
                {
                    echo "Proposta";
                }
                ?>">
                <input type="hidden" name="Entidade_id" id="Entidade_id_fup"    
                       value="<?php
                                    /*if (in_array($proposta->Financeira_id, [10, 11])) 
                                    {
                                        echo $proposta->id;
                                    } 
                                    else 
                                    {
                                        echo $isEmprestimo->id;
                                    }*/
                                    if (isset($isEmprestimo)) 
                                    {
                                        echo $isEmprestimo->id;
                                    } 
                                    else 
                                    {
                                        echo $proposta->id;
                                    }
                                ?>">

                <div id="panel_contrato_proposta" class="tab-pane">

                    <div class="row">

                        <div class="col-md-12">

                            <table id="grid_anexos_contratos" class="table table-striped table-bordered table-hover table-full-width dataTable">
                                <thead>
                                    <tr role="row">
                                        <th>Descrição</th>
                                        <th>Extensão</th>
                                        <th>Data de envio</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>

                        </div>

                    </div>   

                </div>

<?php } ?>

        </div>
    </div>
</div>

<div id="modal-aprove-confirm" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <div class="modal-header" style="background:#dff0d8!important;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 style="color: #468847;" class="alert-heading">
            Deseja aprovar esta proposta?
        </h4>
    </div>

    <div class="modal-body" style="background: #ECF0F1">

        <form class="form-horizontal" method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/proposta/aprovar">
            <div class="form-group">
                <div class="col-sm-3">
                    <input id="input_proposta_id" type="hidden" name="id" value="<?php echo $proposta->id ?>">
                    <input type="hidden" name="primeira_parcela" value="<?php echo $data_primeira_parcela ?>">
                    <input type="hidden" name="ultima_parcela" value="<?php echo $data_ultima_parcela ?>">
                </div>
            </div>                              
            <div class="alert alert-success">
                <i class="fa fa-check-circle"></i>
                Clique em  <strong>'Aprovar'</strong> para prosseguir.
            </div>              
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                    Cancelar
                </button>
                <input type="submit" class="btn btn-green" value="Aprovar">
            </div>
        </form>

    </div>     
</div>

<div id="responsive" class="modal fade" tabindex="-1" data-width="1100" style="display: none;">
    <div class="modal-body">
        <iframe class="overflownone" 
                style="backgroun:#FFF!important" 
                width="100%" 
                src="" scrolling="no" 
                onload='javascript:resizeIframe(this);' 
                id="iframe_mais_detalhes_proposta" 
                frameborder="0"></iframe>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-light-grey">
            Fechar
        </button>      
    </div>
</div>

<div id="conversa" class="modal fade" tabindex="-1" data-width="1100" style="display: none;">
    <div class="modal-body">

        <iframe class="overflownone" style="backgroun:#FFF!important" 
                width="100%" 
                src="" 
                scrolling="no" 
                onload='javascript:resizeIframe(this);' 
                id="iframe_mais_conversa_proposta" 
                frameborder="0"></iframe>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-light-grey">
            Fechar
        </button>      
    </div>
</div>    

<!--Modal confirmação cancelamento de análise (Libera a análise)-->
<div id="modal-cancelar-confirm" class="modal fade" tabindex="-1" data-width="500" style="display: none;">

    <div class="modal-header" style="background:#fcf8e3!important;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 style="color: #c09853;" class="alert-heading">
            Deseja cancelar a análise?
        </h4>
    </div>

    <div class="modal-body" style="background:#fcf8e3!important">

        <form class="form-horizontal" method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/proposta/cancelarAnalise">
            <div class="form-group">
                <div class="col-sm-3">
                    <input type="hidden" name="proposta_id" value="<?php echo $proposta->id ?>">
                </div>
            </div>                              
            <div class="alert alert-warning">
                <i class="fa fa-check-circle"></i>
                Clique em  <strong>'Aprovar'</strong> para prosseguir.
            </div>              
            <div class="modal-footer" style="background:#fcf8e3!important">
                <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                    Cancelar
                </button>
                <input type="submit" class="btn btn-warning" value="Cancelar análise">
            </div>
        </form>

    </div>     
</div>

<div id="modal-recusar-confirm" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <div class="modal-header" style="background:#F6D8D8!important;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 style="color: #DA5251;" class="alert-heading">
            Deseja recusar esta proposta?
        </h4>
    </div>

    <div class="modal-body" style="background:#F6D8D8!important;">

        <form class="form-horizontal" method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/proposta/recusar">
            <div class="form-group">
                <div class="col-sm-3">
                    <input type="hidden" name="proposta_id" value="<?php echo $proposta->id ?>">
                </div>
            </div>                              
            <div class="alert alert-error">
                <i class="fa fa-check-circle"></i>
                Clique em  <strong>'Recusar'</strong> para prosseguir.
            </div>
            <div>
                <label class="alert alert-error fa fa-check-circle"> Selecione o motivo de negação:</label>
<?php echo CHtml::dropDownList('MotivoDeNegacao', 'MotivoDeNegacao', CHtml::listData(MotivoDeNegacao::model()->findAll(), 'id', 'descricao'), array('class' => 'search-select select2 form-control negacao', 'prompt' => 'Selecione', 'id' => 'MotivoDeNegacao')); ?>
                <br/><br/> <label class="alert alert-error fa fa-check-circle"> Observação:</label>
                <br/><textarea style="width:459px; height: 100px; border-radius: 0px!important;" type="text" class="form-control negacao" id="obs" name="obs"></textarea> 
            </div>
            <div class="modal-footer" style="background:#F6D8D8!important;">
                <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                    Cancelar
                </button>
                <input disabled="" type="submit" class="btn btn-danger" id="recusar" value="Recusar">
            </div>
        </form>

    </div>     
</div>

<div id="modal_form_new_msg" class="modal fade" tabindex="-1" data-width="560" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Enviar mensagem</h4>
    </div>
    <form id="form-send-msg">
        <div class="modal-body">
            <div class="row">
                <div class="row-centralize">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label class="control-label">Mensagem:</label>
                                    <textarea name="Mensagem[conteudo]" required="required" maxlength="300" class="form-control limited" style="width:518px!important;height:116px;"></textarea>
                                    <input type="hidden" value="<?php echo Yii::app()->session['usuario']->id ?>" name="Usuario_id" id="ipt_hdn_user_id">
                                    <input type="hidden" value="<?php echo $dialogo->id ?>" name="Dialogo_id">
                                    <input type="hidden" value="0" id="msg_interna" name="interna">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div style="background:transparent;border:none;" class="panel"></div>
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
            <button data-form-id="form-add-email" type="submit" class="btn btn-blue btn-send">Enviar</button>         
        </div>
    </form>
</div>

<div id="modal_msg_more" class="modal fade" tabindex="-1" data-width="560" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Ler mensagem</h4>
    </div>
    <form>
        <div class="modal-body">
            <div class="row">
                <div class="row-centralize">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label class="control-label">Mensagem:</label>
                                    <textarea id="textarea_mensagem_conteudo_ler" readonly="readonly" maxlength="300" class="form-control limited" style="width:518px!important;height:116px;"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div style="background:transparent;border:none;" class="panel"></div>
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">Fechar</button>
        </div>
    </form>
</div>

<div id="modal_cliente_alert" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-header" style="background:#fcf8e3!important;text-align:center;">
        <h2 style="color: #c09853;" class="alert-heading">
            Atenção
        </h2>
    </div>
    <div class="modal-body" style="text-align:center;">
        <h4 style="color: #c09853;"><i class="fa fa-exclamation-triangle"></i> O cliente relacionado a esta proposta possui <?php echo AnaliseDeCredito::model()->qtdAnalisesCliente($proposta->analiseDeCredito->Cliente_id) ?> passagens no dia de hoje!</h4>
    </div>
    <div class="modal-footer" style="background: white!important">
        <button id="btn_cliente_alert" type="button" class="btn btn-warning">Confirmar Leitura</button>
    </div>
</div>

<!--Modal confirmação a desistencia do cliente(Cancela a análise)-->
<div id="modal-desistencia-confirm" class="modal fade" tabindex="-1" data-width="500" style="display: none;">

    <div class="modal-header" style="background:#fcf8e3!important;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 style="color: #c09853;" class="alert-heading">
            Cliente desistiu?
        </h4>
    </div>

    <div class="modal-body" style="background:#fcf8e3!important">

        <form class="form-horizontal" method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/proposta/desistencia">
            <div class="form-group">
                <div class="col-sm-3">
                    <input type="hidden" name="proposta_id" value="<?php echo $proposta->id ?>">
                </div>
            </div>                              
            <div class="alert alert-warning">
                <i class="fa fa-check-circle"></i>
                Clique em  <strong>'Cliente desistiu'</strong> para prosseguir.
            </div>              
            <div class="modal-footer" style="background:#fcf8e3!important">
                <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                    Cancelar
                </button>
                <input type="submit" class="btn btn-warning" value="Cliente desistiu">
            </div>
        </form>

    </div>     
</div>


<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/social-buttons-3.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap-fileupload.min.css">
<style type="text/css">
    #table-detalhes-financeiros_length  , #table-detalhes-financeiros_filter    , #table-detalhes-financeiros_paginate  , 
    #grid_mensagens_length              , #grid_mensagens_filter                , #tableHistorico_length                , #tableHistorico_paginate          , #tableHistorico_filter        , 
    #tableHistorico_info                , #grid_anexos_contratos_length         , #grid_anexos_contratos_filter         , #grid_anexos_contratos_paginate   , #grid_anexos_contratos_info, 
    #grid_dados_sociais_length          , #grid_dados_sociais_info              , #grid_dados_sociais_filter            , #grid_dados_sociais_paginate      , #grid_familiares_length,
    #grid_familiares_filter             , #grid_familiares_info                 , #grid_familiares_paginate             , #grid_telefones_length, 
    #grid_telefones_filter              , #grid_telefones_info                  , #grid_telefones_paginate              , #grid_dados_profissionais_cliente_length,
    #grid_dados_profissionais_cliente_filter                                    , #grid_dados_profissionais_cliente_info,       #grid_dados_profissionais_cliente_info, 
    #grid_dados_profissionais_cliente_previous                                  , #grid_dados_profissionais_cliente_paginate,
    #grid_dados_bancarios_cliente_length                                        , #grid_dados_bancarios_cliente_filter                                   , #grid_dados_bancarios_cliente_info,
    #grid_dados_bancarios_cliente_paginate                                      , #grid_dados_bancarios_cliente_paginate                                 , #grid_enderecos_cliente_length,
    #grid_enderecos_cliente_filter                                              , #grid_enderecos_cliente_filter                                         , #grid_enderecos_cliente_info,
    #grid_enderecos_cliente_previous                                            , #grid_enderecos_cliente_paginate                                       , #grid_referencias_cliente_length,
    #grid_referencias_cliente_filter                                            , #grid_referencias_cliente_info                                         , #grid_referencias_cliente_paginate,
    #grid_anexos_length                                                         , #grid_anexos_filter                                                    , #grid_anexos_info        , #grid_anexos_paginate

    {
        display: none;
    }

    #selectSFilter, #StatusPre{
        width: 100%!important;
    }
</style>