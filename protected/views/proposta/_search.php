<?php
/* @var $this PropostaController */
/* @var $model Proposta */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Analise_de_Credito_id'); ?>
		<?php echo $form->textField($model,'Analise_de_Credito_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Financeira_id'); ?>
		<?php echo $form->textField($model,'Financeira_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Cotacao_id'); ?>
		<?php echo $form->textField($model,'Cotacao_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'habilitado'); ?>
		<?php echo $form->textField($model,'habilitado'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'data_cadastro'); ?>
		<?php echo $form->textField($model,'data_cadastro'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'valor'); ?>
		<?php echo $form->textField($model,'valor'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'qtd_parcelas'); ?>
		<?php echo $form->textField($model,'qtd_parcelas'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'carencia'); ?>
		<?php echo $form->textField($model,'carencia'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'valor_parcela'); ?>
		<?php echo $form->textField($model,'valor_parcela'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'valor_entrada'); ?>
		<?php echo $form->textField($model,'valor_entrada'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'valor_final'); ?>
		<?php echo $form->textField($model,'valor_final'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Status_Proposta_id'); ?>
		<?php echo $form->textField($model,'Status_Proposta_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->