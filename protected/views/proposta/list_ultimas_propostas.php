<div class="row">
   <div class="col-md-12">
      <div class="row">
                  <div class="col-sm-12">                   
                     <!-- start: PAGE TITLE & BREADCRUMB -->
                     <ol class="breadcrumb">
                        <li>
                           <i class="clip-pencil"></i>
                           <a href="#">
                              Propostas
                           </a>
                        </li>
                        <li class="active">
                           Últimas Propostas
                        </li>
                        
                     </ol>

                     <div class="page-header">
                        <h1>Últimas Propostas</h1>
                     </div>
                     <!-- end: PAGE TITLE & BREADCRUMB -->
                  </div>
         </div>
      <!-- start: DYNAMIC TABLE PANEL -->
      <div class="panel panel-default">
         <div class="panel-heading">
            <i class="fa fa-external-link-square"></i>
            
            <div class="panel-tools">
               <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
               </a>
            </div>
         </div>
         
         <div class="panel-body" id="table-wrapper">
         </div>
      </div>
      <!-- end: DYNAMIC TABLE PANEL -->
   </div>
</div>
<div id="responsive" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <div class="modal-header" style="background:#dff0d8!important;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 style="color: #468847;" class="alert-heading">
          Deseja iniciar esta análise ?
        </h4>
    </div>

    <div class="modal-body" style="background: #ECF0F1">

        <form class="form-horizontal" method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/proposta/analise">
               <div class="form-group">
                  <div class="col-sm-3">
                     <input id="input_proposta_id" type="hidden" name="id">
                  </div>             
               </div>                              
          <div class="alert alert-success">
                 <i class="fa fa-check-circle"></i>
                 Clique em  <strong>'Analisar'</strong> Para iniciar a análise.
          </div>              
            <div class="modal-footer">
               <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                  Cancelar
               </button>
               <input type="submit" class="btn btn-green" value="Analisar">
            </div>
        </form>
    </div>     
</div>

<style type="text/css">
   span.label{
      padding: 0.3em 0.5em !important;
   }
   .modal_toggle:hover, .modal_toggle:focus{
      text-decoration: none;
   }
</style>

<script type="text/javascript">
  $(document).ready(function(){
    
    <?php if( isset( $pnotify ) ){ ?>

      $.pnotify({
        title: <?php echo json_encode($pnotify['title']); ?>,
        type: <?php echo json_encode($pnotify['type']); ?>,
        text: <?php echo json_encode($pnotify['text']); ?>
      });

    <?php } ?>
  })
</script>