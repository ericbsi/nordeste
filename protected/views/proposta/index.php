<?php
/* @var $this PropostaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Propostas',
);

$this->menu=array(
	array('label'=>'Create Proposta', 'url'=>array('create')),
	array('label'=>'Manage Proposta', 'url'=>array('admin')),
);
?>

<h1>Propostas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
