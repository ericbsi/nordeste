<?php
/* @var $this PropostaController */
/* @var $model Proposta */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'proposta-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'Analise_de_Credito_id'); ?>
		<?php echo $form->textField($model,'Analise_de_Credito_id'); ?>
		<?php echo $form->error($model,'Analise_de_Credito_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Financeira_id'); ?>
		<?php echo $form->textField($model,'Financeira_id'); ?>
		<?php echo $form->error($model,'Financeira_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Cotacao_id'); ?>
		<?php echo $form->textField($model,'Cotacao_id'); ?>
		<?php echo $form->error($model,'Cotacao_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'habilitado'); ?>
		<?php echo $form->textField($model,'habilitado'); ?>
		<?php echo $form->error($model,'habilitado'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'data_cadastro'); ?>
		<?php echo $form->textField($model,'data_cadastro'); ?>
		<?php echo $form->error($model,'data_cadastro'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'valor'); ?>
		<?php echo $form->textField($model,'valor'); ?>
		<?php echo $form->error($model,'valor'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'qtd_parcelas'); ?>
		<?php echo $form->textField($model,'qtd_parcelas'); ?>
		<?php echo $form->error($model,'qtd_parcelas'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'carencia'); ?>
		<?php echo $form->textField($model,'carencia'); ?>
		<?php echo $form->error($model,'carencia'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'valor_parcela'); ?>
		<?php echo $form->textField($model,'valor_parcela'); ?>
		<?php echo $form->error($model,'valor_parcela'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'valor_entrada'); ?>
		<?php echo $form->textField($model,'valor_entrada'); ?>
		<?php echo $form->error($model,'valor_entrada'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'valor_final'); ?>
		<?php echo $form->textField($model,'valor_final'); ?>
		<?php echo $form->error($model,'valor_final'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Status_Proposta_id'); ?>
		<?php echo $form->textField($model,'Status_Proposta_id'); ?>
		<?php echo $form->error($model,'Status_Proposta_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->