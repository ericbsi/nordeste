<?php
$util = new Util;
?>

<script type="text/javascript">
    function resizeIframe(obj) {

        var isFirefox = typeof InstallTrigger !== 'undefined';

        var plusFf = 0;

        if (isFirefox) {
            plusFf = 100
        }
        ;

        {
            obj.style.height = 0;
        }
        ;

        {
            obj.style.height = plusFf + obj.contentWindow.document.body.scrollHeight + 'px';
        }
    }
</script>
<div class="row">
    <div class="col-sm-12">

        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="#">
                    Proposta
                </a>
            </li>
            <li class="active">
                Resumo
            </li>
        </ol>
        <div class="page-header">
            <h1>Proposta <?php echo $proposta->codigo; ?></h1>

            <input type="hidden" value="<?php echo $proposta->analiseDeCredito->cliente->id ?>" name="idCliente" id="cliente_id">

            <br>
            <?php //$isEmprestimo = $proposta->getEmprestimo(); ?>
            <?php $emprestimoCancela = $proposta->emprestimoAceitaCancelamento(); ?>
            <?php //if(!$proposta->ehCartao() && $isEmprestimo == null) {?>
            <?php if (!$proposta->ehCartao() && $emprestimoCancela) { ?>

                <?php if (SolicitacaoDeCancelamento::model()->solicitacaoExistente($proposta->id) == null) { ?>

                    <a class="btn btn-pinterest" data-toggle="modal" href="#modal_form_new_soli_cancel" id="btn-soli">
                        Solicitar Cancelamento
                        <i class="clip-cancel-circle"></i>
                    </a>

                <?php } else { ?>         

                    <a class="btn btn-warning">
                        Uma solicitação de cancelamento está aguardando aprovação.
                    </a>

                <?php } ?>

            <?php } else { ?> 
                <a disabled="true" class="btn btn-pinterest" data-toggle="modal" href="#modal_form_new_soli_cancel" id="btn-soli">
                    Solicitar Cancelamento
                    <i class="clip-cancel-circle"></i>
                </a>
            <?php } ?>
            <?php if ($proposta->Financeira_id == 7 && ($proposta->Status_Proposta_id == 9 || $proposta->Status_Proposta_id == 3)) { ?>
                <a class="btn btn-info" id="btn-devol" href="#modal_dev_prop">
                    Devolver Proposta
                    <i class="clip-cancel-circle"></i>
                </a>
            <?php } ?>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="tabbable">
            <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
                <li class="active">
                    <a data-toggle="tab" href="#panel_projects">Detalhes da proposta</a>
                </li>
                <li id="li-show-contrato" style="<?php if (($proposta->Status_Proposta_id != 2 && $proposta->Status_Proposta_id != 7) || $proposta->ehCartao()) {
                echo "display:none";
            } ?>">
                    <a data-toggle="tab" href="#panel_contrato">Contrato e boletos</a>
                </li>
                <li id="li-show-anexos">
                    <a data-toggle="tab" href="#panel_anexos">Anexos do Cliente</a>
                </li>
                <?php
                $isEmprestimo = $proposta->getEmprestimo();
                if ($proposta->Status_Proposta_id == 2 && ($isEmprestimo != NULL || in_array($proposta->Financeira_id, [10, 11]))):
                    ?>
                    <li id="li-show-anexar-contrato">
                        <a data-toggle="tab" href="#panel_contrato_proposta">Anexar Contrato</a>
                    </li>
<?php endif; ?>

            </ul>
            <div class="tab-content">
                <div id="panel_projects" class="tab-pane in active">
                    <div class="row">

                        <div class="col-sm-4 col-md-3">
                            <div class="user-left">
                                <div class="center">
                                    <hr>
                                    <h5>
                                        Filial: <?php echo strtoupper($proposta->analiseDeCredito->filial->getConcat()); ?>
                                    </h5>
                                    <hr>
                                    <h5>
                                        Crediarista: <?php echo strtoupper($proposta->analiseDeCredito->usuario->username); ?>
                                    </h5>

                                    <hr>

                                    <?php
                                    if ($proposta->ehCartao()) {

                                        $valorAprovado = "-";

                                        $cartaoHasProposta = CartaoHasProposta::model()->find("habilitado AND Proposta_id");

                                        if ($cartaoHasProposta !== null) {
                                            $cartaoHasLimite = CartaoHasLimite::model()->find("habilitado AND Cartao_has_Proposta_id = $cartaoHasProposta->id");

                                            if ($cartaoHasLimite !== null) {
                                                $valorAprovado = number_format($cartaoHasLimite->limite->valor, 2, ",", ".");
                                            }
                                        }
                                        ?>
                                        <h5>
                                            Valor Aprovado: R$ <?php echo $valorAprovado; ?>
                                        </h5>
                                    <?php } else { ?>
                                        <h5>
                                            Valor Inicial: <a href="#" id="valor" data-type="text" data-pk="<?php echo $proposta->id ?>" data-url="/proposta/alterarValorInicial/" data-title="Nome do Cliente" data-inputclass="currency" class="<?php if ($proposta->Status_Proposta_id == 1 || $proposta->Status_Proposta_id == 4 || $proposta->Status_Proposta_id == 9) {
                                            echo "editable";
                                        } ?>"><?php echo number_format($proposta->valor, 2, ",", "."); ?></a>
                                        </h5>
<?php } ?>

                                    <hr>

                                    <h5>
                                        <?php
                                        if (($proposta->Status_Proposta_id == 3 && $proposta->analise_solicitada) && $proposta->analiseDeCredito->cliente->getCadastro()->cliente_da_casa) {
                                            echo '<span class="btn label label-warning">Aguardando Analise</span>';
                                        } else {
                                            ?>

                                            <span id="span-status" class="<?php echo $proposta->statusProposta->cssClass ?>">
                                            <?php echo $proposta->statusProposta->status ?>
                                            </span>
<?php } ?>
                                    </h5>
                                    <hr>
                                </div>
                                <table class="table table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Mais detalhes</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Vendedor</td>
                                            <td>
<?php echo strtoupper($proposta->analiseDeCredito->vendedor); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Cliente</td>
                                            <td>
<?php echo strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>CPF</td>
                                            <td>
<?php echo $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Data da Proposta</td>
                                            <td>
<?php echo $util->bd_date_to_view(substr($proposta->data_cadastro, 0, 10)); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Alerta</td>
                                            <td>
<?php echo strtoupper($proposta->analiseDeCredito->alerta); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Procedência</td>
                                            <td><?php echo strtoupper($proposta->analiseDeCredito->procedenciaCompra->procedencia); ?></td>
                                        </tr>
                                    </tbody>
                                </table>                       
                            </div>
                        </div>
<?php if ($proposta->ehCartao()) { ?>
                            <p style="text-align: center; font-size: 36px; color: orange"> 
                                Proposta de Cartão de Crédito
                            </p>
<?php } else { ?>
                            <div class="col-sm-8 col-md-9">
                                <div class="col-sm-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <i class="clip-user-5"></i>
                                            Dados financeiros
                                            <div class="panel-tools">
                                                <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="panel-body panel-scroll ps-container" style="height:120px">
                                            <table id="table-detalhes-financeiros" class="table table-striped table-bordered table-hover table-full-width dataTable">
                                                <thead>
                                                    <tr>
                                                        <th class="no-orderable"></th>
                                                        <th width="80" class="no-orderable">Inicial</th>
                                                        <th width="60" class="no-orderable">Entrada</th>
                                                        <th width="50" class="no-orderable">Seguro</th>
                                                        <th width="70" class="no-orderable">Carência</th>
                                                        <th width="70" class="no-orderable">Financiado</th>
                                                        <th width="70" class="no-orderable">Parcelamento</th>
                                                        <th class="no-orderable">Valor final</th>
                                                        <th class="no-orderable">1° parc</th>
                                                        <th class="no-orderable">Última parc</th>
                                                        <th class="no-orderable"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                            <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px; width: 651px;">
                                                <div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div>
                                            </div>
                                            <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px; height: 120px;">
                                                <div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <i class="clip-user-5"></i>
                                            Itens da análise
                                            <div class="panel-tools">
                                                <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="panel-body panel-scroll ps-container" style="height:120px">
                                            <table class="table table-striped table-hover" id="sample-table-1">
                                                <thead>
                                                    <tr>
                                                        <th>Item</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                            <?php foreach ($proposta->analiseDeCredito->listSubgruposDaAnalise() as $item) { ?>
                                                        <tr>                                       
                                                            <td>
                                                        <?php echo strtoupper($item->descricao); ?>
                                                            </td>
                                                        </tr>
    <?php } ?>
                                                </tbody>
                                            </table>
                                            <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px; width: 651px;">
                                                <div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div>
                                            </div>
                                            <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px; height: 120px;">
                                                <div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div>
                                            </div>
                                        </div>
                                    </div>
    <?php if (($proposta->Status_Proposta_id == 2 || $proposta->Status_Proposta_id == 7) && $proposta->titulos_gerados && $proposta->hasOmniConfig() == NULL) { ?>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <i class="clip-user-5"></i>
                                                Títulos / Boletos
                                                <div class="panel-tools">
                                                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="panel-body panel-scroll ps-container" style="height:350px">
                                                <table class="table table-striped table-hover" id="sample-table-1">
                                                    <thead>
                                                        <tr>
                                                            <td>
                                                                <label class="checkbox-inline" style="float:left">
                                                                    <input id="check_select_all" type="checkbox">
                                                                </label>
                                                            </td>
                                                            <th>Seq</th>
                                                            <th>Valor</th>
                                                            <th>Vencimento</th>
                                                            <th>Status</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($proposta->getParcelas() as $parcela) { ?>
                                                            <?php
                                                            if ($parcela->valor_atual < $parcela->valor) {
                                                                $statusPgto = $parcela->statusPagamento();
                                                                ?>
                                                                <tr>
                                                                    <td>
                                                                        <label class="checkbox-inline" style="float:left">
                                                                            <input class="check_print" type="checkbox" value="<?php echo $parcela->id; ?>">
                                                                        </label>
                                                                    </td>
                                                                    <td><?php echo $parcela->seq . ' °'; ?></td>
                                                                    <td><?php echo 'R$ ' . number_format($proposta->getValorParcela(), 2, ',', '.'); ?></td>
                                                                    <td><?php echo $util->bd_date_to_view($parcela->vencimento); ?></td>
                                                                    <td>
                                                                        <span class="<?php echo $statusPgto['span_class'] ?>"><?php echo $statusPgto['mensagem'] ?></span>
                                                                    </td>
                                                                </tr>
            <?php } ?>
        <?php } ?>
                                                    </tbody>
                                                </table>

                                                <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px; width: 651px;">
                                                    <div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div>
                                                </div>
                                                <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px; height: 120px;">
                                                    <div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div>
                                                </div>

                                            </div>
                                            <button class="btn btn-google-plus" id="btn-print-selects" style="margin:0 0 8px 8px">
                                                <i class="clip-note"></i> |
                                                Imprimir boletos selecionados
                                            </button>

                                        </div>
    <?php } ?>
                                </div>
                                <div class="col-sm-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <i class="clip-user-5"></i>
                                            Descrição dos itens
                                            <div class="panel-tools">
                                                <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="panel-body panel-scroll ps-container" style="height:120px">
                                            <table class="table table-striped table-hover" id="sample-table-1">
                                                <thead>
                                                    <tr>
                                                        <th>Descrição</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>                                       
                                                        <td>
    <?php echo $util->cleanStr($proposta->analiseDeCredito->mercadoria, 'upp'); ?>
                                                        </td>
                                                    </tr>                                    
                                                </tbody>
                                            </table>
                                            <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px; width: 651px;">
                                                <div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div>
                                            </div>
                                            <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px; height: 120px;">
                                                <div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
<?php } ?>

                    </div>
                    <div class="row">
                        <div style="margin-left:10px;margin-bottom:10px;">                      
                            <h3>Mensagens do(a) Analista:</h3>
                            <a type="button" class="btn btn-primary <?php if ($proposta->Status_Proposta_id == 3) {
    echo" disabled ";
} ?> " data-toggle="modal"  href="#modal_form_new_msg">
                                Enviar mensagem
                                <i class="fa fa-envelope-o"></i>
                            </a>
                        </div>
                        <div class="" style="width:97%">
                            <table style="margin-left:10px;" id="grid_mensagens" class="table table-striped table-bordered table-hover table-full-width dataTable">
                                <thead>
                                    <tr role="row">
                                        <th width="300">Conteúdo</th>
                                        <th width="40">Data</th>
                                    </tr>
                                </thead>
                            </table>                      
                        </div>
                    </div>
                </div>
                <div style="<?php if (($proposta->Status_Proposta_id != 2 && $proposta->Status_Proposta_id != 7) || $proposta->ehCartao()) {
    echo "display:none";
} ?>" id="panel_contrato" class="tab-pane">
                    <div class="row">
                        <div class="col-sm-5 col-md-4">
                            <div class="user-left">
                                <div class="row">
                                    <div class="col-sm-5 col-md-4">
<?php echo $proposta->renderImpressaoContrato(); ?>
                                    </div>
                                </div>
                                <br>
<?php if ($proposta->segurada && $proposta->Financeira_id != 11) { ?>
                                    <div class="row">
                                        <div class="col-sm-5 col-md-4">

                                            <form target="_blank" action="/contrato/seguro/" method="POST">
                                                <button type="submit" class="btn btn-google-plus">
                                                    <i class="clip-note"></i> |
                                                    Abrir contrato do seguro
                                                </button>
                                                <input type="hidden" value="<?php echo $proposta->id ?>" name="propostaId">
                                            </form>

                                        </div>
                                    </div>
<?php } ?>
                                <br>
                                <div class="row">
                                    <div class="col-sm-5 col-md-4">
<?php if (($proposta->hasOmniConfig() != NULL) || ( $proposta->hasOmniConfig() == NULL && !$proposta->titulos_gerados )): ?>
    <?php echo $proposta->renderImpressaoBoleto(); ?>
<?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="panel_anexos" class="tab-pane">
                    <div class="col-md-12" style="/*margin:0 auto!important;margin:10% 20%!important;position:absolute*/">
                        <div class="row">
                            <form action="/anexos/add/" method="POST" enctype="multipart/form-data" id="form-anexos">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <label class="control-label">Arquivo <span class="symbol required"></span></label>
                                                <input data-icon="false" class="control-label filestyle" required="required" name="FileInput" id="FileInput" type="file" />
                                                <input type="hidden" name="Cliente_id" id="Cliente_id_fup" value="<?php echo $proposta->analiseDeCredito->cliente->id ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <label class="control-label">Descrição <span class="symbol required"></span></label>
                                                <input required class="form-control" type="text" name="Anexo[descricao]">
                                                <!--
                                                  O valor de ['origem'] precisa ser enviado, para a action de adicionar anexos
                                                  tomar a decisão de upar no server local, ou no bucket s3.
                                                  É apenas um workaround temporário, para a migração ser feita no decorrer dos dias,
                                                  pois várias telas trabalham com anexo de arquivos - Eric
                                                -->
                                                <input type="hidden" name="origem" value="tela_proposta_more">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="progressbox">
                                            <div id="progressbar"></div >
                                            <div id="statustxt">0%</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-sm-2">
                                                <button type="submit" class="btn btn-blue">Enviar Arquivo</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>   
                    </div>
                    <div>
                        <br>
                        <br>
                    </div>
                    <div class="col-md-12" style="/*margin:0 auto!important;margin:10% 20%!important;position:absolute*/">
                        <div class="row">
                            <table id="grid_anexos" class="table table-striped table-bordered table-hover table-full-width dataTable">
                                <thead>
                                    <tr role="row">
                                        <th>Descrição</th>
                                        <th>Extensão</th>
                                        <th>Data de envio</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>   
                    </div>   
                </div>
<?php if ($proposta->Status_Proposta_id == 2 && ($isEmprestimo != NULL || in_array($proposta->Financeira_id, [10, 11]))): ?>
                    <div id="panel_contrato_proposta" class="tab-pane">
                        <div class="col-md-12" style="/*margin:0 auto!important;margin:10% 20%!important;position:absolute*/">
                            <div class="row">
                                <form action="/proposta/addAnexoContrato/" method="POST" enctype="multipart/form-data" id="form_contrato_proposta">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-sm-4">

                                                    <label class="control-label">
                                                        Arquivo <span class="symbol required"></span>
                                                    </label>

                                                    <input type="file"   name="FileInputContrato"    id="FileInputContrato"  data-icon="false" class="control-label filestyle" required="required"  />

                                                    <input type="hidden" name="Entidade"             id="Entidade_fup"       value="<?php if (in_array($proposta->Financeira_id, [10, 11])) {
        echo "Proposta";
    } else {
        echo "Emprestimo";
    } ?>">
                                                    <input type="hidden" name="Entidade_id"          id="Entidade_id_fup"    value="<?php if (in_array($proposta->Financeira_id, [10, 11])) {
        echo $proposta->id;
    } else {
        echo $isEmprestimo->id;
    } ?>">

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-4">
                                                    <label class="control-label">Descrição <span class="symbol required"></span></label>
                                                    <input required class="form-control" type="text" name="Anexo[descricao]">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="progressbox_contrato">
                                                <div id="progressbar_contrato"></div >
                                                <div id="statustxt_contrato">0%</div>
                                                <div id="progressbox_contrato">
                                                    <div id="progressbar_contrato"></div >
                                                    <div id="statustxt_contrato">0%</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-sm-2">
                                                        <button type="submit" class="btn btn-blue">Enviar Arquivo</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </form>
                            </div>   
                        </div>
                        <div>
                            <br>
                            <br>
                        </div>
                        <div class="col-md-12" style="/*margin:0 auto!important;margin:10% 20%!important;position:absolute*/">
                            <div class="row">
                                <table id="grid_anexos_contratos" class="table table-striped table-bordered table-hover table-full-width dataTable">
                                    <thead>
                                        <tr role="row">
                                            <th>Descrição</th>
                                            <th>Extensão</th>
                                            <th>Data de envio</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>   
                    </div>
<?php endif; ?>
            </div>
        </div>
    </div>
</div>

<div id="modal-edit-entrada" class="modal fade" data-width="600" tabindex="-1" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Alterar entrada</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <form id="form-filter" action="">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Entrada</label>
                                <input type="text" name="entrada" class="form-control currency" id="entrada" value="<?php echo number_format($proposta->valor_entrada, 2, ',', '.') ?>">
                                <input id="propId" type="hidden" value="<?php echo $proposta->id ?>" name="propostaId">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">&nbsp;</label>
                                <button id="btn-update-entrada" class="btn btn-blue next-step btn-block">Atualizar entrada</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-default">Fechar</button>
    </div>
</div>

<div id="modal-edit-condicoes-proposta" class="modal fade" data-width="1200" tabindex="-1" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Alterar condições</h4>
    </div>
    <div class="modal-body">

        <!--<table class="table table-bordered table-hover" id="table-prestacoes">
            <thead>
               <tr>
                  <th style="width:150px">Prestações/Carência</th>
                  <th style="width:80px" class="th-carencia">30</th>
                  <th style="width:80px" class="th-carencia">35</th>
                  <th style="width:80px" class="th-carencia">40</th>
                  <th style="width:80px" class="th-carencia">45</th>
                  <th style="width:80px" class="th-carencia">50</th>
                  <th style="width:80px" class="th-carencia">55</th>
                  <th style="width:80px" class="th-carencia">60</th>
               </tr>
            </thead>
            <tbody>
        <?php
        if ($proposta->ehCartao()) {
            $valor_solicitado = $proposta->valor_final;
        } else {

            for ($i = $proposta->tabelaCotacao->parcela_inicio; $i <= $proposta->tabelaCotacao->parcela_fim; $i++) {

                $valor_solicitado = $proposta->valor - $proposta->valor_entrada;
                ?>
                           <tr>
                              <td class="td-prestacao">
                <?php echo $i ?>
                              </td>

                <?php $arrReturnValParcela = Fator::model()->getValor($valor_solicitado, $i, 30, $proposta->tabelaCotacao->id); ?>
                              <td valor-parcela="<?php echo $arrReturnValParcela[0]; ?>" class="td-select-parcela" carencia="30" cotacao-id="<?php echo $proposta->tabelaCotacao->id ?>" numero-parcelas="<?php echo $i ?>">
        <?php
        echo "R$ " . $arrReturnValParcela[1];
        ?>
                              </td>

                <?php $arrReturnValParcela = Fator::model()->getValor($valor_solicitado, $i, 35, $proposta->tabelaCotacao->id); ?>
                              <td valor-parcela="<?php echo $arrReturnValParcela[0]; ?>" class="td-select-parcela" carencia="35" cotacao-id="<?php echo $proposta->tabelaCotacao->id ?>" numero-parcelas="<?php echo $i ?>">
        <?php
        echo "R$ " . $arrReturnValParcela[1];
        ?>
                              </td>

                <?php $arrReturnValParcela = Fator::model()->getValor($valor_solicitado, $i, 40, $proposta->tabelaCotacao->id); ?>
                              <td valor-parcela="<?php echo $arrReturnValParcela[0]; ?>" class="td-select-parcela" carencia="40" cotacao-id="<?php echo $proposta->tabelaCotacao->id ?>" numero-parcelas="<?php echo $i ?>">
        <?php
        echo "R$ " . $arrReturnValParcela[1];
        ?>
                              </td>

                <?php $arrReturnValParcela = Fator::model()->getValor($valor_solicitado, $i, 45, $proposta->tabelaCotacao->id); ?>
                              <td valor-parcela="<?php echo $arrReturnValParcela[0]; ?>" class="<?php if ($i > 5) {
            echo "td-select-parcela";
        } else {
            echo "td-none";
        } ?>" carencia="45" cotacao-id="<?php echo $proposta->tabelaCotacao->id ?>" numero-parcelas="<?php echo $i ?>">
                <?php
                echo "R$ " . $arrReturnValParcela[1];
                ?>
                              </td>

                <?php $arrReturnValParcela = Fator::model()->getValor($valor_solicitado, $i, 50, $proposta->tabelaCotacao->id); ?>
                              <td valor-parcela="<?php echo $arrReturnValParcela[0]; ?>" class="<?php if ($i > 5) {
            echo "td-select-parcela";
        } else {
            echo "td-none";
        } ?>" carencia="50" cotacao-id="<?php echo $proposta->tabelaCotacao->id ?>" numero-parcelas="<?php echo $i ?>">
        <?php
        echo "R$ " . $arrReturnValParcela[1];
        ?>
                              </td>

        <?php $arrReturnValParcela = Fator::model()->getValor($valor_solicitado, $i, 55, $proposta->tabelaCotacao->id); ?>
                              <td valor-parcela="<?php echo $arrReturnValParcela[0]; ?>" class="<?php if ($i > 5) {
            echo "td-select-parcela";
        } else {
            echo "td-none";
        } ?>" carencia="55" cotacao-id="<?php echo $proposta->tabelaCotacao->id ?>" numero-parcelas="<?php echo $i ?>">
        <?php
        echo "R$ " . $arrReturnValParcela[1];
        ?>
                              </td>

        <?php $arrReturnValParcela = Fator::model()->getValor($valor_solicitado, $i, 60, $proposta->tabelaCotacao->id); ?>
                              <td valor-parcela="<?php echo $arrReturnValParcela[0]; ?>" class="<?php if ($i > 5) {
            echo "td-select-parcela";
        } else {
            echo "td-none";
        } ?>" carencia="60" cotacao-id="<?php echo $proposta->tabelaCotacao->id ?>" numero-parcelas="<?php echo $i ?>">
        <?php
        echo "R$ " . $arrReturnValParcela[1];
        ?>
                              </td>                  

                           </tr>
    <?php } ?>
<?php } ?>
            </tbody>
        </table>-->

<?php if (!$proposta->ehCartao()) { ?>
            <iframe scrolling="no" class="overflownone" style="backgroun:#FFF!important" width="100%" frameborder="0" onload='javascript:resizeIframe(this);' src="<?php echo Yii::app()->request->baseUrl; ?>/cotacao/renderTableEditar?proposta_id=<?php echo $proposta->id; ?>"></iframe>
<?php } ?>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-default">Fechar</button>
    </div>
</div>

<div id="modal-confirm-print" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <div class="modal-header" style="background:#dff0d8!important;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 style="color: #468847;" class="alert-heading">
            Deseja reimprimir o(s) título(s) selecionado(s)?
        </h4>
    </div>

    <div class="modal-body" style="background: #ECF0F1">
        <form target="_blank" action="/boleto/segundaVia/" method="POST" id="form_titulos_segunda_via">
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
                <input type="submit" class="btn btn-green" value="Imprimir">
            </div>
        </form>
    </div>     
</div>

<div id="modal-aprove-confirm" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <div class="modal-header" style="background:#dff0d8!important;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 style="color: #468847;" class="alert-heading">
            Deseja aprovar esta proposta?
        </h4>
    </div>

    <div class="modal-body" style="background: #ECF0F1">

        <form class="form-horizontal" method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/proposta/aprovar">
            <div class="form-group">
                <div class="col-sm-3">
                    <input id="input_proposta_id" type="hidden" name="id" value="<?php echo $proposta->id ?>">
                    <input id="input_proposta_status_id" type="hidden" name="id" value="<?php echo $proposta->Status_Proposta_id ?>">
                    <input type="hidden" name="primeira_parcela" value="<?php echo $data_primeira_parcela ?>">
                    <input type="hidden" name="ultima_parcela" value="<?php echo $data_ultima_parcela ?>">
                </div>
            </div>                              
            <div class="alert alert-success">
                <i class="fa fa-check-circle"></i>
                Clique em  <strong>'Aprovar'</strong> para prosseguir.
            </div>              
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                    Cancelar
                </button>
                <input type="submit" class="btn btn-green" value="Aprovar">
            </div>
        </form>

    </div>     
</div>

<div id="modal-cancelar-confirm" class="modal fade" tabindex="-1" data-width="500" style="display: none;">

    <div class="modal-header" style="background:#fcf8e3!important;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 style="color: #c09853;" class="alert-heading">
            Deseja cancelar a análise?
        </h4>
    </div>

    <div class="modal-body" style="background:#fcf8e3!important">

        <form class="form-horizontal" method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/proposta/cancelarAnalise">
            <div class="form-group">
                <div class="col-sm-3">
                    <input type="hidden" name="proposta_id" value="<?php echo $proposta->id ?>">
                </div>
            </div>                              
            <div class="alert alert-warning">
                <i class="fa fa-check-circle"></i>
                Clique em  <strong>'Aprovar'</strong> para prosseguir.
            </div>              
            <div class="modal-footer" style="background:#fcf8e3!important">
                <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                    Cancelar
                </button>
                <input type="submit" class="btn btn-warning" value="Cancelar análise">
            </div>
        </form>

    </div>     
</div>

<div id="modal-recusar-confirm" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <div class="modal-header" style="background:#F6D8D8!important;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 style="color: #DA5251;" class="alert-heading">
            Deseja recusar esta proposta?
        </h4>
    </div>

    <div class="modal-body" style="background:#F6D8D8!important;">

        <form class="form-horizontal" method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/proposta/recusar">
            <div class="form-group">
                <div class="col-sm-3">
                    <input type="hidden" name="proposta_id" value="<?php echo $proposta->id ?>">
                </div>
            </div>                              
            <div class="alert alert-error">
                <i class="fa fa-check-circle"></i>
                Clique em  <strong>'Recusar'</strong> para prosseguir.
            </div>              
            <div class="modal-footer" style="background:#F6D8D8!important;">
                <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                    Cancelar
                </button>
                <input type="submit" class="btn btn-danger" value="Recusar">
            </div>
        </form>

    </div>     
</div>

<div id="modal_form_new_msg" class="modal fade" tabindex="-1" data-width="560" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Enviar mensagem</h4>
    </div>
    <form id="form-send-msg">
        <div class="modal-body">
            <div class="row">
                <div class="row-centralize">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label class="control-label">Mensagem:</label>
                                    <textarea name="Mensagem[conteudo]" required="required" maxlength="300" class="form-control limited" style="width:518px!important;height:116px;"></textarea>
                                    <input type="hidden" value="<?php echo Yii::app()->session['usuario']->id ?>" name="Usuario_id" id="ipt_hdn_user_id">
                                    <input type="hidden" value="<?php if( isset( $dialogo ) ){echo $dialogo->id;} ?>" name="Dialogo_id">
                                    <input type="hidden" value="0" id="msg_interna" name="interna">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div style="background:transparent;border:none;" class="panel"></div>
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
            <button data-form-id="form-add-email" type="submit" class="btn btn-blue btn-send">Enviar</button>         
        </div>
    </form>
</div>

<div id="modal_dev_prop" class="modal fade" tabindex="-1" data-width="560" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Devolver Proposta</h4>
    </div>
    <form id="form-dev-prop">
        <div class="modal-body">
            <div class="row">
                <div class="row-centralize">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label class="control-label">Mensagem:</label>
                                    <textarea name="Mensagem[conteudo]" required="required" maxlength="300" class="form-control limited" style="width:518px!important;height:116px;"></textarea>
                                    <input type="hidden" value="<?php echo Yii::app()->session['usuario']->id ?>" name="Usuario_id" id="ipt_hdn_user_id">
                                    <input type="hidden" value="<?php if( isset( $dialogo ) ){ echo $dialogo->id; } ?>" name="Dialogo_id">
                                    <input type="hidden" value="<?php echo $proposta->id ?>" name="id_prop">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div style="background:transparent;border:none;" class="panel"></div>
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
            <button type="submit" class="btn btn-blue btn-send">Enviar</button>         
        </div>
    </form>
</div>

<div id="modal_msg_more" class="modal fade" tabindex="-1" data-width="560" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Ler mensagem</h4>
    </div>
    <form>
        <div class="modal-body">
            <div class="row">
                <div class="row-centralize">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label class="control-label">Mensagem:</label>
                                    <textarea id="textarea_mensagem_conteudo_ler" readonly="readonly" maxlength="300" class="form-control limited" style="width:518px!important;height:116px;"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div style="background:transparent;border:none;" class="panel"></div>
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">Fechar</button>
        </div>
    </form>
</div>
<div id="modal_comprovante_soli" class="modal fade" tabindex="-1" data-width="560" data-backdrop="static" style="display: none;">
    <div class="modal-header">
        <h4 class="modal-title">Imprimir Comprovante de Solicitação</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">Para finalizar esta solicitação se faz necessário imprimir o <strong>Comprovante de Solicitação de Cancelamento do Cliente</strong> e entregar ao cliente solicitante.</label>
                </div>
            </div>
        </div> 
    </div>
    <div class="modal-footer">
        <form action="/solicitacaoDeCancelamento/comprovanteCancelamento" method="POST" target="_blank">
            <input value="<?php echo Yii::app()->session['usuario']->id ?>" type="hidden" name="Solicitacao[Solicitante]">
            <input value="<?php echo $proposta->id ?>" type="hidden" name="Solicitacao[Proposta_id]">
            <button id="btn-imprimir-comp" class="btn btn-red"><i class="fa fa-print"></i> IMPRIMIR </button>
        </form>
    </div>
</div>
<?php if (SolicitacaoDeCancelamento::model()->solicitacaoExistente($proposta->id) == null) { ?>
    <div id="modal_form_new_soli_cancel" class="modal fade" tabindex="-1" data-width="560" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">Solicitar cancelamento</h4>
        </div>
        <form action="/solicitacaoDeCancelamento/add/" method="POST" enctype="multipart/form-data" id="form-add-soli-cancel">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Solicitante:</label>
                                    <input readonly="readonly" value="<?php echo Yii::app()->session['usuario']->nome_utilizador ?>" type="text" name="Usuario_nome" class="form-control">
                                    <input value="<?php echo Yii::app()->session['usuario']->id ?>" type="hidden" name="Solicitacao[Solicitante]">
                                    <input id="Parcela_id" value="<?php echo $proposta->id ?>" type="hidden" name="Solicitacao[Proposta_id]">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Data:</label>
                                    <input readonly="readonly" required="required" value="<?php echo date('d/m/Y') ?>" type="text" name="Solicitacao[data_solicitacao]" class="form-control">
                                </div>
                            </div>                     
                        </div>
                    </div>
                </div>
                <div class="row">                        
                    <div class="row-centralize">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Motivo:</label>
                                        <textarea id="textarea_observacao" name="Solicitacao[motivo]" required="required" maxlength="300" class="form-control limited" style="width:518px!important;height:116px;"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">                        
                    <div class="row-centralize">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12" style="text-align: center!important">
                                    <span>Faça download do formulário de solicitação, preencha, e envie em anexo a esta solicitação. Se preferir, cancele esta
                                    e volte em seguida com o formulário já preenchido e digitalizado para ser anexado</span>
                                    <div class="form-group">
                                        <a target="_blank" href="<?php echo Yii::app()->baseUrl ?>/arquivos/form_solicitacao.pdf" class="btn btn-primary">Download do Formulário</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Comprovante:</label>
                                        <input data-icon="false" class="control-label filestyle" required="required" name="ComprovanteFile" id="ComprovanteFile" type="file" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
                <button type="submit" class="btn btn-blue btn-send" id="btn-salvar-soli">Salvar</button>
                <input id="comprovante-impresso" type="hidden" value="0" />
            </div>
        </form>
    </div>
<?php } ?>
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/social-buttons-3.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap-fileupload.min.css">
<style type="text/css">
    #grid_anexos_contratos_length, #grid_anexos_contratos_filter ,#grid_anexos_filter,#grid_anexos_length,#table-detalhes-financeiros_paginate, #grid_mensagens_length, #grid_mensagens_filter, #table-detalhes-financeiros_length, #table-detalhes-financeiros_filter{
        display: none;
    }
    .td-none{
        color: #ccc;
        text-decoration: line-through
    }
    .td-entrada{
        cursor: pointer;  
    }
    .td-entrada:hover{
        text-decoration: underline;
        color: #357ebd;  
    }

    /* progress bar style */
    #progressbox, #progressbox_contrato {
        border: 1px solid #4cae4c;
        padding: 1px; 
        position:relative;
        width:315px;
        border-radius: 3px;
        margin: 10px;
        margin-left: 0;
        display:none;
        text-align:left;
    }
    #progressbar, #progressbar_contrato {
        height:20px;
        border-radius: 3px;
        background-color: #4cae4c;
        width:1%;
    }
    #statustxt, #statustxt_contrato {
        top:3px;
        left:50%;
        position:absolute;
        display:inline-block;
        color: #FFF;
        font-size: 11px;
        font-family: arial
    }
</style>