<?php  $util = new Util; ?>
<div class="row">
   <div class="col-sm-12">
      <!-- start: PAGE TITLE & BREADCRUMB -->
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
            Proposta
            </a>
         </li>
         <li class="active">
            Resumo
         </li>
      </ol>
      <div class="page-header">
         <h1>Proposta: <strong style="font-family:Arial;font-size:26px;"><?php echo $proposta->codigo ?></strong> </h1>
         <h1>
            Cliente: <strong style="font-family:Arial;font-size:26px;"><?php echo $proposta->analiseDeCredito->cliente->pessoa->nome ?></strong>
            <form style="display:inline" action="/cliente/analisarCadastro/" method="POST" target="_blank">
               <button target="_blank" class="btn btn-primary" style="padding:2px 5px!important; font-size:11px!important;"><i class=" clip-search"></i></button>
               <input type="hidden" name="idCliente" value="<?php echo $proposta->analiseDeCredito->cliente->id ?>">
            </form>
         </h1>
      </div>
      <!-- end: PAGE TITLE & BREADCRUMB -->
   </div>
</div>

<div class="row">
   <div class="col-sm-12">
      <div class="tabbable">
         
         <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
            <li class="active"><a data-toggle="tab" href="#panel_projects">Detalhes da proposta</a></li>
            <li id="tab-contrato"><a data-toggle="tab" href="#panel_contrato">Contrato</a></li>
            <li id="tab-boletos"><a data-toggle="tab" href="#panel_boletos">Boletos</a></li>
         </ul>
         
         <div class="tab-content">
            
            <div id="panel_projects" class="tab-pane in active">
               <div class="row">
                  <div class="col-sm-5 col-md-4">
                     <div class="user-left">
                        <div class="center">
                           <h5>
                              Código: <?php echo $proposta->codigo; ?>
                           </h5>
                           <hr>
                           <h5>
                              Filial: <?php echo $proposta->analiseDeCredito->filial->nome_fantasia; ?>
                           </h5>
                           <hr>
                           <h5>
                              Crediarista: <?php echo $proposta->analiseDeCredito->usuario->username; ?>
                           </h5>
                           <hr>
                           <h5>
                              Valor Inicial: <?php echo "R$ " . number_format($proposta->valor,2,",",""); ?>
                           </h5>
                           <hr>
                           <h5>
                              Valor de entrada: <?php echo "R$ " . number_format($proposta->valor_entrada,2,",",""); ?>
                           </h5>
                           <hr>
                           <h5>
                              Valor do seguro: <?php echo "R$ " . number_format($proposta->calcularValorDoSeguro(),2,",",""); ?>
                           </h5>
                           <hr>
                           <h5>
                              Valor financiado: <?php echo "R$ " . number_format(($proposta->getValorFinanciado()),2,",",""); ?>
                           </h5>
                           
                        </div>
                        <table class="table table-condensed table-hover">
                           <thead>
                              <tr>
                                 <th colspan="3">Mais detalhes</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>Vendedor</td>
                                 <td>
                                    <?php echo $proposta->analiseDeCredito->vendedor; ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>Promotor</td>
                                 <td>
                                    <?php echo $proposta->analiseDeCredito->promotor; ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>Alerta</td>
                                 <td>
                                    <?php echo $proposta->analiseDeCredito->alerta; ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>Procedência</td>
                                 <td><?php echo $proposta->analiseDeCredito->procedenciaCompra->procedencia; ?></td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
                  
                  <div class="col-sm-7 col-md-8">

                     <div class="col-sm-12">
                        <div class="panel panel-default">
                           <div class="panel-heading">
                              <i class="clip-user-5"></i>
                              Dados financeiros
                              <div class="panel-tools">
                                 <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                 </a>
                              </div>
                           </div>
                           <div class="panel-body panel-scroll ps-container" style="height:120px">
                              <table class="table table-striped table-hover" id="sample-table-1">
                                 <thead>
                                    <tr>
                                       <th>Valor da parcela</th>
                                       <th>N° de parcelas</th>
                                       <th>Carência</th>                                       
                                       <th>1° parc</th>
                                       <th>Última parc</th>
                                       <th>Taxa %</th>
                                       <th>Valor final</th>
                                       <!--<th></th>-->
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>
                                          <?php echo number_format($proposta->getValorParcela(),2,",",""); ?>
                                       </td>
                                       <td>
                                          <?php echo $proposta->qtd_parcelas; ?>
                                       </td>
                                       <td>
                                          <?php echo $proposta->carencia; ?>
                                       </td>                                       
                                       <td>
                                          <?php echo $proposta->getDataPrimeiraParcela(); ?>
                                       </td>
                                       <td>
                                          <?php
                                             echo $proposta->getDataUltimaParcela();
                                          ?>
                                       </td>
                                       <td>
                                          <a data-original-title="<?php echo $proposta->tabelaCotacao->descricao; ?>" class="tooltips" href="#">
                                             <?php echo $proposta->tabelaCotacao->taxa; ?>
                                          </a>                                          
                                       </td>
                                       <td>
                                          <?php echo number_format($proposta->getValorFinal(),2,",",""); ?>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                              <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px; width: 651px;">
                                 <div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div>
                              </div>
                              <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px; height: 120px;">
                                 <div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div>
                              </div>
                           </div>
                        </div>
                        <!--
                        <div class="panel panel-default">
                           <div class="panel-heading">
                              <i class="clip-user-5"></i>
                              <?php echo $proposta->qtd_parcelas; ?> Parcelas
                              <div class="panel-tools">
                                 <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                 </a>
                              </div>
                           </div>
                           <div class="panel-body panel-scroll ps-container" style="height:200px">
                               <table class="table table-striped table-hover" id="sample-table-1">
                                    <thead>
                                       <tr>
                                          <th>Sequência</th>
                                          <th>Valror</th>
                                          <th>Vencimento</th>
                                          <th></th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <?php for ($i = 0; $i < $proposta->qtd_parcelas; $i++) { ?>
                                       <tr>
                                          <td><?php echo $i+1 . '°' ?></td>
                                          <td>
                                             <?php echo $proposta->getValorParcela() ?>
                                          </td>
                                          <td>
                                             <?php echo $proposta->getVencimentoParcela( $i+1 ) ?>
                                          </td>
                                          <td>
                                             <a class="btn btn-red" href="#"><i class="clip-file-pdf"></i></a>
                                          </td>
                                       </tr>
                                       <?php } ?>
                                    </tbody>
                               </table>
                           </div>
                        </div>
                        -->
                     </div>

                     <div class="col-sm-12">
                        <div class="panel panel-default">
                           <div class="panel-heading">
                              <i class="clip-user-5"></i>
                              Itens da análise
                              <div class="panel-tools">
                                 <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                 </a>
                              </div>
                           </div>
                           <div class="panel-body panel-scroll ps-container" style="height:120px">
                              <table class="table table-striped table-hover" id="sample-table-1">
                                 <thead>
                                    <tr>
                                       <th>Código</th>
                                       <th>Item</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php foreach( $proposta->analiseDeCredito->listItensDaAnalise() as $item ){ ?>
                                    <tr>
                                       <td>
                                          <?php echo $item->codigo; ?>
                                       </td>
                                       <td>
                                          <?php echo $item->nome; ?>
                                       </td>
                                    </tr>
                                    <?php } ?>
                                 </tbody>
                              </table>
                              <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px; width: 651px;">
                                 <div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div>
                              </div>
                              <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px; height: 120px;">
                                 <div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div>
                              </div>
                           </div>
                        </div>

                        <?php if ( !$util->strIsNull( $proposta->analiseDeCredito->observacao ) ) { ?>

                        <div class="panel panel-default">
                           <div class="panel-heading">
                              <i class="clip-user-5"></i>
                              Observação
                              <div class="panel-tools">
                                 <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                 </a>
                              </div>
                           </div>
                           <div class="panel-body panel-scroll ps-container" style="height:120px">
                              <p>
                                 <?php echo $proposta->analiseDeCredito->observacao ?>
                              </p>
                           </div>
                        </div>
                        <?php } ?>
                     </div>

                  </div>
               </div>
            </div>

            <div id="panel_contrato" class="tab-pane">
               <div class="alert alert-block alert-warning">
                  Ao Gerar o <strong>contrato</strong>, você, <strong><?php echo Yii::app()->session['usuario']->nome_utilizador ?></strong>, estará
                  aceitando os nossos <a href="#"><strong>termos de contrato</strong></a>
               </div>
               
               <!--<a id="btn-open-contrato" href="#" class="btn btn-google-plus">
                  <i class="fa fa-thumbs-up"></i>
                  | Estou ciente. Desejo abrir e imprimir o contrato
               </a>-->

               <form target="_blank" action="/contrato/" method="POST">
                  <button type="submit" class="btn btn-google-plus">
                     <i class="fa fa-thumbs-up"></i> |
                     Estou ciente. Desejo abrir e imprimir o contrato
                  </button>
                  <input type="hidden" value="<?php echo $proposta->id ?>" name="propostaId">
               </form>
            </div>
            
            <div id="panel_boletos" class="tab-pane">
               <form target="_blank" method="post" action="<?php echo Yii::app()->getBaseUrl(true) ?>/boleto">
                  <input type="hidden" name="c" value="<?php echo $proposta->codigo ?>" />
                  <input type="hidden" name="tg" value="<?php echo $proposta->titulos_gerados ?>" />
                  <button class="btn btn-google-plus">
                     <i class="clip-file-pdf"></i>
                     | Imprimir boletos 
                  </button>
               </form>
            </div>

         </div>
      </div>
   </div>
</div>

<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/social-buttons-3.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap-fileupload.min.css">

<style type="text/css">
      #tab-boletos{
         /*display: none;*/
      }
</style>