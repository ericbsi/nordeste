<table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
   <thead>
      <tr>
         <th>Código</th>
         <th class="hidden-xs">Cliente</th>
         <th>Financeira</th>
         <th class="hidden-xs">Filial</th>
         <th>Valor Finan</th>
         <th class="hidden-xs">N° parcelas</th>
         <th>Status</th>
         <th class="hidden-xs">Data cadastro</th>
      </tr>
   </thead>
   <tbody>
      <?php foreach($propostas as $prop) { ?>
      <tr>
         <td><?php echo $prop->codigo; ?></td>
         <td class="hidden-xs"><?php echo substr($prop->analiseDeCredito->cliente->pessoa->nome, 0, 20) . '...'; ?></td>
         <td><?php echo $prop->financeira->nome ?></td>
         <td class="hidden-xs"><?php echo $prop->financeira->nome ?></td>
         <td><?php echo "R$ " . number_format($prop->valor - $prop->valor_entrada, 2, ",", ".") ?></td>
         <td class="hidden-xs"><?php echo $prop->qtd_parcelas ?></td>
         <td>
            <a subid="<?php echo $prop->id ?>" class="modal_toggle label-warning" data-toggle="<?php echo $prop->statusProposta->data_toggle?>" href="#responsive">
            <span class="label label-sm <?php echo $prop->statusProposta->cssClass ?>">
               <?php echo $prop->statusProposta->status ?>
            </span>
            <a>
         </td>
         <td class="hidden-xs"><?php echo $prop->data_cadastro_br ?></td>
      </tr>
      <?php } ?>
   </tbody>
</table>
