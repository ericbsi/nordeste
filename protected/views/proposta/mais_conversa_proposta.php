<?php 

$util = new Util; 

if($banco == 1){
    $sql =  "SELECT * FROM beta.Dialogo as Dn
            INNER JOIN beta.Mensagem as Mn ON Mn.Dialogo_id = Dn.id
            WHERE Dn.Entidade_Relacionamento_id = " . $proposta->id;
}else{
    $sql =  "SELECT * FROM nordeste2.Dialogo as Dn
            INNER JOIN nordeste2.Mensagem as Mn ON Mn.Dialogo_id = Dn.id
            WHERE Dn.Entidade_Relacionamento_id = " . $proposta->id;
}

$mensagens = Yii::app()->db->createCommand($sql)->queryAll();

?>

<script type="text/javascript">

   function resizeIframe(obj) {

      var isFirefox = typeof InstallTrigger !== 'undefined';
      var plusFf = 0;

      if (isFirefox) {
         plusFf = 100
      }
      ;

      {
         obj.style.height = 0;
      }
      ;
      {
         obj.style.height = plusFf + obj.contentWindow.document.body.scrollHeight + 'px';
      }
   }

</script>


<div class="row">

   <div class="col-sm-12">

      <div class="page-header">
         <h1>Conversa da proposta: <small style="font-size:18px"><?php echo $proposta->codigo ?></small></h1>
      </div>

   </div>

</div>
<div class="row">
   <div class="col-sm-12">
      <h4>Mensagens</h4>
      <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
         <thead>
            <tr>
               <th colspan="2">Mensagem</th>
            </tr>
         </thead>
         <tbody>
            <?php
            foreach ($mensagens as $m) {
                if($banco == 1){
                    $sql_user = "SELECT * FROM beta.Usuario WHERE id = " . $m['Usuario_id'];
                }else {
                    $sql_user = "SELECT * FROM nordeste2.Usuario WHERE id = " . $m['Usuario_id'];
                }
                $user = Yii::app()->db->createCommand($sql_user)->queryRow();
            ?>
               <tr>
                  <td width="03%">De:</td>
                  <td width="97%"><?php echo $user['nome_utilizador'] ; ?></td>
               </tr>
               <tr rowspan="2">
                  <td colspan="2"><?php echo $m['conteudo']                 ; ?></td>
               </tr>
            <?php } ?>
         </tbody>
      </table>
   </div>
</div>

<div id="responsive" class="modal fade" tabindex="-1" data-width="1000" style="display: none;">

   <div class="modal-body">
      <iframe class="overflownone" style="backgroun:#FFF!important" width="100%" src="" scrolling="no" onload='javascript:resizeIframe(this);' id="iframe_show_baixas" frameborder="0"></iframe>
   </div>

   <div class="modal-footer">
      <button type="button" data-dismiss="modal" class="btn btn-light-grey">
         Fechar
      </button>      
   </div>

</div>

<script>
   $(document).ready(function () {

      $('.btn-parcela-show-baixas').on('click', function () {
         $('#iframe_show_baixas').attr('src', $(this).attr('modal-iframe-uri'));
      })
   })
</script>
