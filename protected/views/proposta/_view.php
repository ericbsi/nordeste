<?php
/* @var $this PropostaController */
/* @var $data Proposta */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Analise_de_Credito_id')); ?>:</b>
	<?php echo CHtml::encode($data->Analise_de_Credito_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Financeira_id')); ?>:</b>
	<?php echo CHtml::encode($data->Financeira_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Cotacao_id')); ?>:</b>
	<?php echo CHtml::encode($data->Cotacao_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('habilitado')); ?>:</b>
	<?php echo CHtml::encode($data->habilitado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_cadastro')); ?>:</b>
	<?php echo CHtml::encode($data->data_cadastro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('valor')); ?>:</b>
	<?php echo CHtml::encode($data->valor); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('qtd_parcelas')); ?>:</b>
	<?php echo CHtml::encode($data->qtd_parcelas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('carencia')); ?>:</b>
	<?php echo CHtml::encode($data->carencia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('valor_parcela')); ?>:</b>
	<?php echo CHtml::encode($data->getValorParcela()); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('valor_entrada')); ?>:</b>
	<?php echo CHtml::encode($data->valor_entrada); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('valor_final')); ?>:</b>
	<?php echo CHtml::encode($data->valor_final); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Status_Proposta_id')); ?>:</b>
	<?php echo CHtml::encode($data->Status_Proposta_id); ?>
	<br />

	*/ ?>

</div>