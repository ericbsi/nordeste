<div class="row">
	<div class="col-md-12">

		<div class="row">
                  <div class="col-sm-12">                   
                     <!-- start: PAGE TITLE & BREADCRUMB -->
                     <ol class="breadcrumb">
                        <li>
                           <i class="clip-pencil"></i>
                           <a href="#">
                              Propostas
                           </a>
                        </li>
                        <li class="active">
                              Aguardando análise
                        </li>
                        
                     </ol>

                     <div class="page-header">
                        <h1>Aguardando análise</h1>
                     </div>
                     <!-- end: PAGE TITLE & BREADCRUMB -->
                  </div>
         </div>

		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="fa fa-external-link-square"></i>
					<div class="panel-tools">
						<a class="btn btn-xs btn-link panel-collapse collapses" href="#">
						</a>
					</div>
			</div>
			<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'proposta-grid',
				//'dataProvider'=>Yii::app()->session['usuario']->listPropostas(),
				'dataProvider'=>$model->search(),
				//'filter'=>$model,
				'enableSorting' => false,
				'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width dataTable',
				
				'columns'=>array(

					array(
						'header'=>'Cliente',
						'value'=> 'strtolower($data->analiseDeCredito->cliente->pessoa->nome)',
					),

					array(
						'header'=>'CPF',
						'value'=> '$data->analiseDeCredito->cliente->pessoa->returnCPF()',
					),
					array(
						'type'=>'raw',
						'header' => 'Valor',
						'value' => '$data->monetaryFormater($data->valor)'
					),					
					array(
						'type'=>'raw',
						'header' => 'Valor Entrada',
						'value' => '$data->monetaryFormater($data->valor_entrada)'
					),
					
					array(
						'type'=>'raw',
						'header' => 'Valor Financiado',
						'value' => '$data->monetaryFormater($data->valor-$data->valor_entrada)'
					),

					array(
						'type'=>'raw',
						'header' => 'Valor Parcela',
						'value' => '$data->monetaryFormater($data->getValorParcela())'
					),
					'qtd_parcelas',
					'carencia',
					array(
						'type'=>'raw',
						'header' => 'Valor Final',
						'value' => '$data->monetaryFormater($data->valor_final)'
					),

					
					array(
						'type'=>'raw',
						'header' => 'Status da proposta',
						'value' => 'CHtml::link($data->statusProposta->status, "#responsive", array("subid"=>$data->id, "class"=>$data->statusProposta->cssClass, "data-toggle"=>$data->statusProposta->data_toggle))'
					),
				),

			)); ?>
		</div>
		
	</div>	
</div>

<script type="text/javascript">
	$(document).ready(function(){
		
		<?php if( isset( $pnotify ) ){ ?>

			$.pnotify({
				title: <?php echo json_encode($pnotify['title']); ?>,
				type: <?php echo json_encode($pnotify['type']); ?>,
				text: <?php echo json_encode($pnotify['text']); ?>
			});

		<?php } ?>
	})
</script>