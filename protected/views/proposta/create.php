<?php
/* @var $this PropostaController */
/* @var $model Proposta */

$this->breadcrumbs=array(
	'Propostas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Proposta', 'url'=>array('index')),
	array('label'=>'Manage Proposta', 'url'=>array('admin')),
);
?>

<h1>Create Proposta</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>