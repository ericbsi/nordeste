<?php  $util = new Util; ?>

<div class="row">
   <div class="col-sm-12">
      <!-- start: PAGE TITLE & BREADCRUMB -->
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
            Proposta
            </a>
         </li>
         <li class="active">
            Resumo
         </li>
      </ol>
      <div class="page-header">
         <h1>Proposta</h1>
         <a class="btn btn-green" href="#"><i class="glyphicon glyphicon-thumbs-up"></i></a>
         <a class="btn btn-primary" href="#"><i class="fa fa-share"></i></a>
         <a class="btn btn-red" href="#"><i class="glyphicon glyphicon-thumbs-down"></i></a>
      </div>
      <!-- end: PAGE TITLE & BREADCRUMB -->
   </div>
</div>

<div class="row">
   <div class="col-sm-12">
      <div class="tabbable">
         <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
            <li class="active">
               <a data-toggle="tab" href="#dados_do_cliente">Dados do Cliente</a>
            </li>
            <?php if ( $proposta->analiseDeCredito->cliente->pessoa->estado_civil == "Casado" ) {
               ?>
            <li>
               <a data-toggle="tab" href="#panel_edit_account">Dados Cônjuge</a>
            </li>
            <?php } ?>
            <li>
               <a data-toggle="tab" href="#panel_projects">Detalhes da proposta</a>
            </li>
         </ul>
         <div class="tab-content">
            <div id="dados_do_cliente" class="tab-pane in active">
               <div class="row">
                  <div class="col-sm-5 col-md-4">
                     <div class="user-left">
                        <div class="center">
                           <h5><?php echo $proposta->analiseDeCredito->cliente->pessoa->nome; ?></h5>
                           <hr>
                           <h5>
                              CPF: <?php echo $proposta->analiseDeCredito->cliente->pessoa->returnCPF(); ?>
                           </h5>
                           <hr>
                           <h5>
                              Nacionalidade: <?php echo $proposta->analiseDeCredito->cliente->pessoa->nacionalidade; ?>
                           </h5>
                           <hr>
                           <h5>
                              Estado Civil: <?php echo $proposta->analiseDeCredito->cliente->pessoa->estado_civil; ?>
                           </h5>
                           <hr>
                        </div>
                        <table class="table table-condensed table-hover">
                           <thead>
                              <tr>
                                 <th colspan="3">Contato</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>E-mail</td>
                                 <td>
                                 </td>
                              </tr>
                              <tr>
                                 <td>Telefone:</td>
                                 <td>
                                    <?php
                                       $proposta->analiseDeCredito->cliente->pessoa->listarTelefones();
                                       
                                       foreach ($proposta->analiseDeCredito->cliente->pessoa->listarTelefones() as $telefone) {
                                       echo $telefone->numero;
                                       }
                                       ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>Facebook:</td>
                                 <td><?php echo $proposta->analiseDeCredito->cliente->pessoa->contato->facebook ?></td>
                              </tr>
                              <tr>
                                 <td>Skype:</td>
                                 <td><?php echo $proposta->analiseDeCredito->cliente->pessoa->contato->skype ?></td>
                              </tr>
                              <tr>
                                 <td>Twtter:</td>
                                 <td><?php echo $proposta->analiseDeCredito->cliente->pessoa->contato->twitter ?></td>
                              </tr>
                           </tbody>
                        </table>
                        <table class="table table-condensed table-hover">
                           <thead>
                              <tr>
                                 <th colspan="3">Endereço</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>CEP</td>
                                 <td>
                                    <?php echo  $proposta->analiseDeCredito->cliente->pessoa->getEndereco()->cep; ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>Logradouro , n°:</td>
                                 <td>
                                    <?php echo  $proposta->analiseDeCredito->cliente->pessoa->getEndereco()->logradouro; ?>, 
                                    <?php echo  $proposta->analiseDeCredito->cliente->pessoa->getEndereco()->numero; ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>Bairro:</td>
                                 <td>
                                    <?php echo  $proposta->analiseDeCredito->cliente->pessoa->getEndereco()->bairro; ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>Complemento:</td>
                                 <td>
                                    <?php echo  $proposta->analiseDeCredito->cliente->pessoa->getEndereco()->complemento; ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>Cidade:</td>
                                 <td>
                                    <?php echo  $proposta->analiseDeCredito->cliente->pessoa->getEndereco()->cidade; ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>Estado:</td>
                                 <td>
                                    <?php echo  $proposta->analiseDeCredito->cliente->pessoa->getEndereco()->uf; ?>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <div class="col-sm-7 col-md-8">
                     <div class="col-sm-12">
                        <div class="panel panel-default">
                           <div class="panel-heading">
                              <i class="clip-user-5"></i>
                              Dados Profissionais
                              <div class="panel-tools">
                                 <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                 </a>
                              </div>
                           </div>
                           <div class="panel-body panel-scroll" style="height:120px">
                              <table class="table table-striped table-hover" id="sample-table-1">
                                 <thead>
                                    <tr>
                                       <th>Empresa</th>
                                       <th>Ocupação Atual</th>
                                       <th>Data de admissão</th>
                                       <th>Renda Líquida</th>
                                       <th></th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>
                                          <?php echo $proposta->analiseDeCredito->cliente->pessoa->getDadosProfissionais()->empresa;?>
                                       </td>
                                       <td>                                      
                                          <?php echo $proposta->analiseDeCredito->cliente->pessoa->getDadosProfissionais()->tipoOcupacao->tipo;?>
                                       </td>
                                       <td>
                                          <?php echo $util->bd_date_to_view($proposta->analiseDeCredito->cliente->pessoa->getDadosProfissionais()->data_admissao); ?>
                                       </td>
                                       <td>
                                          <?php echo $proposta->analiseDeCredito->cliente->pessoa->getDadosProfissionais()->renda_liquida;?>
                                       </td>
                                       <td class="center">
                                          <div>
                                             <div class="btn-group">
                                                <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                                                <i class="fa fa-cog"></i> <span class="caret"></span>
                                                </a>
                                                <ul role="menu" class="dropdown-menu pull-right">
                                                   <li role="presentation">
                                                      <a role="menuitem" tabindex="-1" href="#">
                                                      <i class="fa fa-edit"></i> Mais de talhes
                                                      </a>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-12">
                        <div class="panel panel-default">
                           <div class="panel-heading">
                              <i class="clip-users-2"></i>
                              Dados Bancários
                              <div class="panel-tools">
                                 <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                 </a>
                              </div>
                           </div>
                           <div class="panel-body panel-scroll" style="height:80px">
                              <table class="table table-striped table-hover" id="sample-table-1">
                                 <thead>
                                    <tr>
                                       <th>Banco</th>
                                       <th>Agência</th>
                                       <th>Conta</th>
                                       <th>Operação</th>
                                       <th>Tipo de Conta</th>
                                       <th>Data de abertura</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>
                                          <?php echo $proposta->analiseDeCredito->cliente->pessoa->getDadosBancarios()->banco->nome; ?>
                                       </td>
                                       <td>
                                          <?php echo $proposta->analiseDeCredito->cliente->pessoa->getDadosBancarios()->agencia; ?>
                                       </td>
                                       <td>
                                          <?php echo $proposta->analiseDeCredito->cliente->pessoa->getDadosBancarios()->numero; ?>
                                       </td>
                                       <td>
                                          <?php echo $proposta->analiseDeCredito->cliente->pessoa->getDadosBancarios()->operacao; ?>
                                       </td>
                                       <td>
                                          <?php echo $proposta->analiseDeCredito->cliente->pessoa->getDadosBancarios()->tipoContaBancaria->tipo; ?>
                                       </td>
                                       <td>
                                          <?php echo $proposta->analiseDeCredito->cliente->pessoa->getDadosBancarios()->data_abertura; ?>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-12">
                        <div class="panel panel-default">
                           <div class="panel-heading">
                              <i class="clip-users-2"></i>
                              Referências
                              <div class="panel-tools">
                                 <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                 </a>
                              </div>
                           </div>
                           <div class="panel-body panel-scroll" style="height:110px">
                              <table class="table table-striped table-hover" id="sample-table-1">
                                 <thead>
                                    <tr>
                                       <th>Nome</th>
                                       <th class="hidden-xs">Telefone</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php foreach ($proposta->analiseDeCredito->cliente->listReferencias() as $referencia) { ?>
                                    <tr>
                                       <td><?php echo $referencia->nome ?></td>
                                       <td class="hidden-xs">                                   
                                          <?php echo $referencia->getTelefone()->numero; ?>                                   
                                       </td>
                                    </tr>
                                    <?php } ?>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php if ( $proposta->analiseDeCredito->cliente->pessoa->estado_civil == "Casado" ) { ?>
            <div id="panel_edit_account" class="tab-pane in active">
               <div class="row">
                  <div class="col-sm-5 col-md-4">
                     <div class="user-left">
                        <div class="center">
                           <?php $conjuge = $proposta->analiseDeCredito->cliente->getConjuge(); ?>

                           <h5><?php echo $conjuge->pessoa->nome; ?></h5>
                           <hr>

                           <?php foreach ($conjuge->getDocs() as $doc) { ?>
                           <h5>
                              <?php echo $doc->tipoDocumento->tipo; ?>: <?php echo $doc->numero ?>
                           </h5>
                           <hr>
                           <?php } ?>

                           <h5>
                              Nacionalidade: <?php echo $conjuge->pessoa->nacionalidade; ?>
                           </h5>
                           <hr>
                        </div>
                        <table class="table table-condensed table-hover">
                           <thead>
                              <tr>
                                 <th colspan="3">Contato</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>E-mail</td>
                                 <td>
                                 </td>
                              </tr>
                              <tr>
                                 <td>Telefone:</td>
                                 <td>
                                    <?php

                                       $conjuge->pessoa->listarTelefones();
                                       
                                       foreach ( $conjuge->pessoa->listarTelefones() as $tel ) {
                                             echo $tel->numero;
                                       }

                                       ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>Facebook:</td>
                                 <td><?php echo $conjuge->pessoa->contato->facebook ?></td>
                              </tr>
                              <tr>
                                 <td>Skype:</td>
                                 <td><?php echo $conjuge->cliente->pessoa->contato->skype ?></td>
                              </tr>
                              <tr>
                                 <td>Twtter:</td>
                                 <td><?php echo $conjuge->pessoa->contato->twitter ?></td>
                              </tr>
                           </tbody>
                        </table>
                        <table class="table table-condensed table-hover">
                           <thead>
                              <tr>
                                 <th colspan="3">Endereço</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>CEP</td>
                                 <td>
                                    <?php echo  $conjuge->pessoa->getEndereco()->cep; ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>Logradouro , n°:</td>
                                 <td>
                                    <?php echo $conjuge->pessoa->getEndereco()->logradouro; ?>, 
                                    <?php echo $conjuge->pessoa->getEndereco()->numero; ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>Bairro:</td>
                                 <td>
                                    <?php echo $conjuge->pessoa->getEndereco()->bairro; ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>Complemento:</td>
                                 <td>
                                    <?php echo $conjuge->pessoa->getEndereco()->complemento; ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>Cidade:</td>
                                 <td>
                                    <?php echo $conjuge->pessoa->getEndereco()->cidade; ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>Estado:</td>
                                 <td>
                                    <?php echo $conjuge->pessoa->getEndereco()->uf; ?>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <div class="col-sm-7 col-md-8">
                     <div class="col-sm-12">
                        <div class="panel panel-default">
                           <div class="panel-heading">
                              <i class="clip-user-5"></i>
                              Dados Profissionais
                              <div class="panel-tools">
                                 <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                 </a>
                              </div>
                           </div>
                           <div class="panel-body panel-scroll" style="height:120px">
                              <table class="table table-striped table-hover" id="sample-table-1">
                                 <thead>
                                    <tr>
                                       <th>Empresa</th>
                                       <th>Ocupação Atual</th>
                                       <?php if( !$util->strIsNull( $conjuge->pessoa->getDadosProfissionais()->data_admissao ) ){ ?>
                                       <th>Data de admissão</th>
                                       <?php } ?>
                                       <th>Renda Líquida</th>
                                       <th>Profissão</th>
                                       <th></th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>
                                          <?php echo $conjuge->pessoa->getDadosProfissionais()->empresa;?>
                                       </td>
                                       <td>                                      
                                          <?php echo $conjuge->pessoa->getDadosProfissionais()->tipoOcupacao->tipo;?>
                                       </td>
                                       <?php if( !$util->strIsNull( $conjuge->pessoa->getDadosProfissionais()->data_admissao ) ){ ?>
                                       <td>
                                          <?php echo $util->bd_date_to_view($conjuge->pessoa->getDadosProfissionais()->data_admissao); ?>
                                       </td>
                                       <?php } ?>
                                       <td>
                                          <?php echo $conjuge->pessoa->getDadosProfissionais()->renda_liquida;?>
                                       </td>
                                       <td>
                                          <?php echo $conjuge->pessoa->getDadosProfissionais()->profissao;?>
                                       </td>
                                       <td class="center">
                                          <div>
                                             <div class="btn-group">
                                                <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                                                <i class="fa fa-cog"></i> <span class="caret"></span>
                                                </a>
                                                <ul role="menu" class="dropdown-menu pull-right">
                                                   <li role="presentation">
                                                      <a role="menuitem" tabindex="-1" href="#">
                                                      <i class="fa fa-edit"></i> Mais de talhes
                                                      </a>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php } ?>
            <div id="panel_projects" class="tab-pane">
               <div class="row">
                  <div class="col-sm-5 col-md-4">
                     <div class="user-left">
                        <div class="center">
                           <h5>
                              Número do pedido: <?php echo $proposta->analiseDeCredito->numero_do_pedido; ?>
                           </h5>
                           <hr>
                           <h5>
                              Filial: <?php echo $proposta->analiseDeCredito->filial->nome_fantasia; ?>
                           </h5>
                           <hr>
                           <h5>
                              Crediarista: <?php echo $proposta->analiseDeCredito->usuario->username; ?>
                           </h5>
                           <hr>
                           <h5>
                              Valor solicitado: <?php echo number_format($proposta->valor,2,",",""); ?>
                           </h5>
                           <hr>
                        </div>
                        <table class="table table-condensed table-hover">
                           <thead>
                              <tr>
                                 <th colspan="3">Mais detalhes</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>Vendedor</td>
                                 <td>
                                    <?php echo $proposta->analiseDeCredito->vendedor; ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>Promotor</td>
                                 <td>
                                    <?php echo $proposta->analiseDeCredito->promotor; ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>Alerta</td>
                                 <td>
                                    <?php echo $proposta->analiseDeCredito->alerta; ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>Procedência</td>
                                 <td><?php echo $proposta->analiseDeCredito->procedenciaCompra->procedencia; ?></td>
                              </tr>
                           </tbody>
                        </table>
                        <!--<table class="table table-condensed table-hover">
                           <thead>
                              <tr>
                                 <th colspan="3">Endereço</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>CEP</td>
                                 <td>
                                    <?php echo  $proposta->analiseDeCredito->cliente->pessoa->getEndereco()->cep; ?>
                                 </td>                                 
                              </tr>
                              <tr>
                                 <td>Logradouro , n°:</td>
                                 <td>
                           <?php echo  $proposta->analiseDeCredito->cliente->pessoa->getEndereco()->logradouro; ?>, 
                           <?php echo  $proposta->analiseDeCredito->cliente->pessoa->getEndereco()->numero; ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>Bairro:</td>
                                 <td>
                                    <?php echo  $proposta->analiseDeCredito->cliente->pessoa->getEndereco()->bairro; ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>Complemento:</td>
                                 <td>
                                    <?php echo  $proposta->analiseDeCredito->cliente->pessoa->getEndereco()->complemento; ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>Cidade:</td>
                                 <td>
                                    <?php echo  $proposta->analiseDeCredito->cliente->pessoa->getEndereco()->cidade; ?>
                                 </td>
                              </tr>
                              <tr>
                                 <td>Estado:</td>
                                 <td>
                                    <?php echo  $proposta->analiseDeCredito->cliente->pessoa->getEndereco()->uf; ?>
                                 </td>
                              </tr>
                           </tbody>
                           </table>-->
                     </div>
                  </div>
                  
                  <div class="col-sm-7 col-md-8">

                     <div class="col-sm-12">
                        <div class="panel panel-default">
                           <div class="panel-heading">
                              <i class="clip-user-5"></i>
                              Dados financeiros
                              <div class="panel-tools">
                                 <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                 </a>
                              </div>
                           </div>
                           <div class="panel-body panel-scroll ps-container" style="height:120px">
                              <table class="table table-striped table-hover" id="sample-table-1">
                                 <thead>
                                    <tr>
                                       <th>Carência</th>
                                       
                                       <?php if ( $proposta->valor_entrada > 0 ) { ?>
                                       <th>Val entr</th>
                                       <?php } ?>
                                       <th>N° de parcelas</th>
                                       <th>Valor da parcela</th>
                                       <th>Valor final</th>
                                       <th>1° parc</th>
                                       <th>Última parc</th>
                                       <th>Taxa %</th>
                                       <!--<th></th>-->
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>
                                          <?php echo $proposta->carencia; ?>
                                       </td>
                                       <?php if ( $proposta->valor_entrada > 0 ) { ?>
                                       <td>
                                          <?php echo number_format($proposta->valor_entrada,2,",",""); ?>
                                       </td>
                                       <?php } ?>
                                       <td>
                                          <?php echo $proposta->qtd_parcelas; ?>
                                       </td>
                                       <td>
                                          <?php echo number_format($proposta->getValorParcela(),2,",",""); ?>
                                       </td>
                                       <td>
                                          <?php echo number_format($proposta->valor_final,2,",",""); ?>
                                       </td>
                                       <td>
                                          <?php echo $data_primeira_parcela; ?>
                                       </td>
                                       <td>
                                          <?php echo $data_ultima_parcela; ?>
                                       </td>
                                       <td>
                                          <a data-original-title="<?php echo $proposta->cotacao->descricao; ?>" class="tooltips" href="#">
                                             <?php echo $proposta->cotacao->taxa; ?>
                                          </a>                                          
                                       </td>
                                      
                                       <!--<td class="center">
                                          <div>
                                             <div class="btn-group">
                                                <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                                                <i class="fa fa-cog"></i> <span class="caret"></span>
                                                </a>
                                                <ul role="menu" class="dropdown-menu pull-right">
                                                   <li role="presentation">
                                                      <a role="menuitem" tabindex="-1" href="#">
                                                      <i class="fa fa-edit"></i> Mais de talhes
                                                      </a>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </td>-->

                                    </tr>
                                 </tbody>
                              </table>
                              <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px; width: 651px;">
                                 <div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div>
                              </div>
                              <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px; height: 120px;">
                                 <div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="col-sm-12">
                        <div class="panel panel-default">
                           <div class="panel-heading">
                              <i class="clip-user-5"></i>
                              Itens da análise
                              <div class="panel-tools">
                                 <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                 </a>
                              </div>
                           </div>
                           <div class="panel-body panel-scroll ps-container" style="height:120px">
                              <table class="table table-striped table-hover" id="sample-table-1">
                                 <thead>
                                    <tr>
                                       <th>Código</th>
                                       <th>Item</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php foreach( $proposta->analiseDeCredito->listItensDaAnalise() as $item ){ ?>
                                    <tr>
                                       <td>
                                          <?php echo $item->codigo; ?>
                                       </td>
                                       <td>
                                          <?php echo $item->nome; ?>
                                       </td>
                                    </tr>
                                    <?php } ?>
                                 </tbody>
                              </table>
                              <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px; width: 651px;">
                                 <div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div>
                              </div>
                              <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px; height: 120px;">
                                 <div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div>
                              </div>
                           </div>
                        </div>

                        <?php if ( !$util->strIsNull( $proposta->analiseDeCredito->observacao ) ) { ?>

                        <div class="panel panel-default">
                           <div class="panel-heading">
                              <i class="clip-user-5"></i>
                              Observação
                              <div class="panel-tools">
                                 <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                 </a>
                              </div>
                           </div>
                           <div class="panel-body panel-scroll ps-container" style="height:120px">
                              <p>
                                 <?php echo $proposta->analiseDeCredito->observacao ?>
                              </p>
                           </div>
                        </div>

                        <?php } ?>
                     </div>

                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>



<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/social-buttons-3.css">
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sigax_template/bootstrap-fileupload.min.css">