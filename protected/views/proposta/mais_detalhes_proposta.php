<?php
$util = new Util;
$parcelas = [];
if ($banco == 1 && isset($titulo['id'])) {
    $sql = "SELECT Pas.*, Bs.id AS 'b_id', Bs.data_da_ocorrencia FROM beta.Titulo 	AS Ts

                INNER JOIN beta.Parcela 				AS Pas 	ON Pas.Titulo_id = Ts.id 	AND Pas.habilitado
                LEFT  JOIN beta.Baixa 					AS Bs 	ON Bs.Parcela_id = Pas.id	AND Bs.baixado

                WHERE Ts.id = " . $titulo['id'] . " AND Ts.habilitado";
    $parcelas = Yii::app()->db->createCommand($sql)->queryAll();
} else {
    if (isset($titulo['id'])) {
        $sql = "SELECT Pas.*, Bs.id AS 'b_id', Bs.data_da_ocorrencia FROM nordeste2.Titulo 	AS Ts

                    INNER JOIN nordeste2.Parcela 				AS Pas 	ON Pas.Titulo_id = Ts.id 	AND Pas.habilitado
                    LEFT  JOIN nordeste2.Baixa 				AS Bs 	ON Bs.Parcela_id = Pas.id	AND Bs.baixado

                    WHERE Ts.id = " . $titulo['id'] . " AND Ts.habilitado";
        $parcelas = Yii::app()->db->createCommand($sql)->queryAll();
    }
}
?>

<script type="text/javascript">

    function resizeIframe(obj) {

        var isFirefox = typeof InstallTrigger !== 'undefined';
        var plusFf = 0;

        if (isFirefox) {
            plusFf = 100
        }
        ;

        {
            obj.style.height = 0;
        }
        ;
        {
            obj.style.height = plusFf + obj.contentWindow.document.body.scrollHeight + 'px';
        }
    }

</script>


<div class="row">

    <div class="col-sm-12">

        <div class="page-header">
            <h1>Detalhes da proposta: <small style="font-size:18px"><?php echo $proposta['codigo'] ?></small></h1>
        </div>

    </div>

</div>
<div class="row">
    <div class="col-sm-12">
        <h4>Parcelas</h4>
        <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
            <thead>
                <tr>
                    <th>Seq</th>
                    <th>Vencimento</th>
                    <th>Valor</th>
                    <th>Valor pago</th>
                    <th>Informações</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($parcelas as $parcela) { ?>
                    <tr>
                        <td><?php echo $parcela['seq'] . '°' ?></td>
                        <td><?php echo $util->bd_date_to_view($parcela['vencimento']) ?></td>
                        <td><?php echo "R$ " . number_format($parcela['valor'], 2, ',', '.') ?></td>
                        <td><?php echo "R$ " . number_format($parcela['valor_atual'], 2, ',', '.') ?></td>
                        <td><?php
                            $hoje = date('Y-m-d');
                            $vencimento = $parcela['vencimento'];

                            $arrReturn = array();

                            /* Não existe baixa */
                            if ($parcela['b_id'] == NULL) {

                                $diffDias = abs( floor( ( strtotime( $vencimento ) - strtotime( $hoje ) ) / ( 60 * 60 * 24 ) ) );
                                $arrReturn['numDias'] = $diffDias;

                                /* Não está vencida, e não está no dia do pagamento */
                                if (strtotime($vencimento) > strtotime($hoje)) {
                                    $arrReturn['status'] = 'Em dia';
                                    $arrReturn['span_class'] = 'btn label label-success';
                                    $arrReturn['mensagem'] = "A parcela está em dia. Restando " . $arrReturn['numDias'] . " dias para o vencimento.";
                                }

                                /* Está no dia do pagamento */ else if (strtotime($vencimento) == strtotime($hoje)) {
                                    $arrReturn['status'] = 'Em dia';
                                    $arrReturn['span_class'] = 'btn label label-warning';
                                    $arrReturn['mensagem'] = 'Estamos no dia do vencimento da parcela';
                                }

                                /* Está vencido */ else {
                                    $arrReturn['status'] = 'Atrasado';
                                    $arrReturn['span_class'] = 'btn label label-danger';
                                    $arrReturn['mensagem'] = "Atrasada há " . $arrReturn['numDias'] . " dia(s)";
                                }
                            }

                            /* Existe baixa */ else {
                                $diasPagamento = floor(( strtotime($vencimento) - strtotime($parcela['data_da_ocorrencia']) ) / ( 60 * 60 * 24 ));

                                $arrReturn['span_class'] = 'btn label label-success';
                                $arrReturn['status'] = 'Paga em dia';

                                if ($diasPagamento < 0) {
                                    $arrReturn['status'] = 'Paga em atraso';
                                    $arrReturn['span_class'] = 'btn label btn-soundcloud';
                                    $arrReturn['mensagem'] = "A parcela foi paga " . abs($diasPagamento) . " dia(s) após o vencimento. ";
                                } else if ($diasPagamento == 0) {
                                    $arrReturn['mensagem'] = "A parcela foi paga no dia do vencimento";
                                } else {
                                    $arrReturn['mensagem'] = "A parcela foi paga " . abs($diasPagamento) . " dia(s) antes do vencimento.";
                                }
                            }
                            echo "<span data-parcelaid='" .$parcela['id']."' class='".$arrReturn['span_class']."'>".$arrReturn['mensagem']."</span>";
                            ?></td>
                        <td>
                            <div class="visible-md visible-lg hidden-sm hidden-xs">	                              
			        <a data-toggle="modal" href="#responsive" class="btn btn-xs tooltips btn-parcela-show-baixas <?php if ( $parcela['b_id'] == NULL ) {echo " disabled btn-light-grey"; } else{ echo " btn-green "; } ?>" data-placement="top" data-original-title="Mais detalhes" modal-iframe-uri="<?php echo Yii::app()->request->baseUrl; ?>/proposta/listarBaixasParcela?id=<?php echo $parcela['id'] ?>&banco=<?php echo $banco ?>">
			            <i class="clip-download-3"></i>
			        </a>
			    </div>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<div id="responsive" class="modal fade" tabindex="-1" data-width="1000" style="display: none;">

    <div class="modal-body">
        <iframe class="overflownone" style="backgroun:#FFF!important" width="100%" src="" scrolling="no" onload='javascript:resizeIframe(this);' id="iframe_show_baixas" frameborder="0"></iframe>
    </div>

    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-light-grey">
            Fechar
        </button>      
    </div>

</div>

<script>
    $(document).ready(function () {

        $('.btn-parcela-show-baixas').on('click', function () {
            $('#iframe_show_baixas').attr('src', $(this).attr('modal-iframe-uri'));
        })
    })
</script>
<style type="text/css">
    .btn-soundcloud {
        color: #fff;
        background-color: #f7931e;
        border-color: rgba(0,0,0,0.2);
    }
</style>
