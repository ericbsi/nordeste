<div class="row"  id="initial_place">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li><a href="#">Lista Negra</a></li>
            <li class="active">Configurações</li>
        </ol>
        <div class="page-header">
            <h1>Configurações de Parâmetros</h1>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12" id="msgsReturn">
    </div>
</div>
<div class="row">
    <br>
</div>

<div class="row">
    <div class="col-md-12">
        <form class="form-horizontal">
            <div class="form-group">
                <label for="config_parametro" class="col-sm-2 control-label">Parâmetro</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="config_parametro" placeholder="Parâmetro">
                </div>
                <label for="config_valor" class="col-sm-1 control-label">Valor</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="config_valor" placeholder="Valor">
                </div>
            </div>
            <div class="form-group">
                <label for="config_tipo" class="col-sm-2 control-label">Tipo</label>
                <div class="col-sm-4">
                    <select id="config_tipo" class="form-control" placeholder="Selecione">
                        <option value="" disabled selected>...</option>
                        <option value="string">String</option>
                        <option value="inteiro">Inteiro</option>
                        <option value="float">Float</option>
                    </select>
                </div>
                <label for="config_data" class="col-sm-1 control-label">Data</label>
                <div class="col-sm-4">
                    <input type="date" class="form-control" id="config_data" placeholder="Data">
                </div>
             </div>
             <div class="form-group">
                <label for="" class="col-sm-2 control-label">Descrição</label>
                <div class="col-sm-9">
                    <textarea id="config_descricao" name="" class="form-control" cols="10" rows="5"></textarea>
                </div>
            </div>
             <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label"></label>
                <div class="col-sm-9">
                    <button 
                        align="center"
                        type="button" 
                        id="btnAddParametro"
                        class="col-sm-12 btn btn-green">
                            Incluir Parâmetro
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<br />
<br />
<br />
<div class="row" style="padding-bottom:40px">
    <div class="col-sm-12">

        <table id="grid_parametros" 
               class="table table-striped table-bordered table-hover table-full-width dataTable">

            <thead>
                <tr>
                    <th style="width : 20%!important">
                        Parâmetro
                    </th>
                    <th style="width : 20%!important">
                        Valor
                    </th>
                    <th style="width : 20%!important">
                        Tipo
                    </th>
                    <th style="width : 20%!important">
                        Data de Cadastro
                    </th>
                    <th style="width : 10%!important">
                        Descrição
                    </th>
                    <th style="width : 6%!important">
                    </th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<style type="text/css">
    #grid_parametros_filter, #grid_parametros_length{
        display: none;
    }
</style>