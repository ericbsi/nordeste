<div class="row"  id="initial_place">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li><a href="#">Lecca</a></li>
            <li class="active">Configurações</li>
        </ol>
        <div class="page-header">
            <h1>Configurações de Parâmetros</h1>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12" id="msgsReturn">
    </div>
</div>
<div class="row">
    <br>
</div>

<div class="row">
    <div class="col-md-12">
        <input style="width: 100%!important;" type="hidden" class="select2 search-select" id="selectNucleoFiliais">
    </div>
</div>
<br />
<br />
<br />
<br />
<div class="parametos" style="display:none;">
    <div class="row" style="padding-bottom:40px">
        <div class="col-md-12">
            <label>Cod. Lojista</label>
            <input id="cod_lojista" type="text" class="codLoj form-control" placeholder="Cod. Logista"> 
        </div>
    </div>
    <div class="row" style="padding-bottom:40px">
        <div class="col-md-4">
            <label>Tab. com Juros (30 a 60 dias)</label>
            <input id="cod_tabJuros" type="text" class="codTab form-control" placeholder="Cod. Tabela com Juros (30 a 60 Dias)">
        </div>
        <div class="col-md-4">
            <label>Tab. com Juros (90 dias)</label>
            <input id="cod_tabJuros90" type="text" class="codTab form-control" placeholder="Cod. Tabela com Juros (90 Dias)">
        </div>
        <div class="col-md-4">
            <label>Tab. Promocional</label>
            <input id="cod_tabPromo" type="text" class="codTab form-control" placeholder="Cod. Tabela Promocional">
        </div>
    </div>
    <div class="row" style="padding-bottom:40px">
        <div class="col-md-4">
            <label>Tab. sem Juros (15 Dias)</label>
            <input id="cod_tab15" type="text" class="codTab form-control" placeholder="Cod. Tabela Sem Juros (15 Dias)">
        </div>
        <div class="col-md-4">
            <label>Tab. sem Juros (30 Dias)</label>
            <input id="cod_tab30" type="text" class="codTab form-control" placeholder="Cod. Tabela Sem Juros (30 Dias)">
        </div>
        <div class="col-md-4">
            <label>Tab. sem Juros (45 Dias)</label>
            <input id="cod_tab45" type="text" class="codTab form-control" placeholder="Cod. Tabela Sem Juros (45 Dias)">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <button id="salvar_config" style="width: 100%" class="btn btn-success">Salvar Configurações</button>
        </div>
    </div>
</div>