<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/fornecedor/all/">
                    Fornecedores
                </a>
            </li>
            <li class="active">
                Administrar
            </li>
        </ol>
    </div>
</div>

<p></p>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">

            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i>
                Novo Fornecedor
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse expand" href="#">
                    </a>
                </div>
            </div>

            <div class="panel-body collapse">
                <div class="tabbable">
                    <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="painelFornecedor">
                        <li id="liDados" class="active">
                            <a data-toggle="tab" href="#painel_dados">
                                Visão Geral
                            </a>
                        </li>
                        <li id="liContato">
                            <a data-toggle="tab" href="#painel_contato">
                                Contato
                            </a>
                        </li>
                        <li id="liEndereco">
                            <a data-toggle="tab" href="#painel_endereco">
                                Endereço
                            </a>
                        </li>
                        <li>
                        <buton id="btnAddFornecedor" class="btn btn-green" disabled>
                            <i class="fa fa-save"></i> Salvar
                        </buton>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="painel_dados" class="tab-pane in active">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-7">
                                            <label class="control-label">
                                                Razão Social : 
                                                <span class="symbol required"></span>
                                            </label>
                                            <input 
                                                required
                                                type="text" 
                                                placeholder="Razão Social" 
                                                id="razaoSocial" 
                                                class="form form-control" />
                                            <span for="razaoSocial" class="help-block valid"></span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-5">
                                            <label class="control-label">
                                                Nome Fantasia : 
                                                <span class="symbol required" aria-required="true"></span>
                                            </label>
                                            <input 
                                                required
                                                type="text" 
                                                placeholder="Nome Fantasia" 
                                                id="nomeFantasia" 
                                                class="form form-control" />
                                            <span for="nomeFantasia" class="help-block valid"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="control-label">
                                                Tipo CGC : 
                                                <span class="symbol required"></span>
                                            </label>
                                            <select 
                                                id="tipoCGC"
                                                class="form form-control search-select select2 ufSelect">
                                                <option value="0">
                                                    Selecione...
                                                </option>
                                                <?php foreach (TipoDocumento::model()->findAll('id IN (1,9)') as $tipo) { ?>
                                                    <option value="<?php echo $tipo->id; ?>">
                                                        <?php echo $tipo->tipo; ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                            <span for="tipoCGC" class="help-block valid"></span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="control-label">
                                                CGC : 
                                                <span class="symbol required" aria-required="true"></span>
                                            </label>
                                            <input 
                                                required
                                                type="text" 
                                                placeholder="CGC" 
                                                id="cgc" 
                                                class="form form-control" />
                                            <span for="cgc" class="help-block valid"></span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="control-label">
                                                UF : 
                                                <span class="symbol required"></span>
                                            </label>
                                            <select 
                                                id="ufCliente"
                                                class="form form-control search-select select2 ufSelect">
                                                <option value="0">
                                                    Selecione...
                                                </option>
                                                <?php foreach (Estados::model()->findAll() as $e) { ?>
                                                    <option value="<?php echo $e->sigla; ?>">
                                                        <?php echo $e->nome; ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                            <span for="ufCliente" class="help-block valid"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="control-label">
                                                Cidade : 
                                                <span class="symbol required"></span>
                                            </label>
                                            <input 
                                                required="true" 
                                                placeholder="Selecione" 
                                                type="hidden" 
                                                id="cidadeCliente" 
                                                class="form-control search-select selectCidades" />
                                            <span for="cidadeCliente" class="help-block valid"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="painel_contato" class="tab-pane">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <label class="control-label">
                                                Telefone : 
                                                <span class="symbol required"></span>
                                            </label>
                                            <input 
                                                type="text" 
                                                class="form form-control"
                                                id="numeroContato" 
                                                placeholder="Digite o número..."/>
                                            <span for="numeroContato" class="help-block valid"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <label class="control-label">
                                                Tipo de Telefone : 
                                                <span class="symbol required"></span>
                                            </label>
                                            <select
                                                class="form form-control search-select select2"
                                                id="tipoTelefoneContato" >
                                                <option value="0">
                                                    Tipo de Telefone...
                                                </option>
                                                <?php foreach (TipoTelefone::model()->findAll('habilitado') as $tipoTelefone) {?>
                                                    <option value="<?php echo $tipoTelefone->id; ?>">
                                                        <?php echo $tipoTelefone->tipo; ?>
                                                    </option>
                                                <?php } ?>
                                                <span for="tipoTelefoneContato" class="help-block valid"></span>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <label class="control-label">
                                                Ramal : 
                                                <span class="symbol required"></span>
                                            </label>
                                            <input 
                                                type="text" 
                                                class="form form-control"
                                                id="ramalContato" 
                                                placeholder="Digite o ramal..."/>
                                            <span for="ramalContato" class="help-block valid"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="painel_endereco" class="tab-pane">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-2">
                                            <label class="control-label">
                                                CEP : 
                                                <span class="symbol required"></span>
                                            </label>

                                            <input 
                                                type="text" 
                                                class="form form-control"
                                                id="cepEndereco" 
                                                placeholder="Digite o CEP..."/>
                                            <span for="cepEndereco" class="help-block valid"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-6">
                                            <label class="control-label">
                                                Logradouro : 
                                                <span class="symbol required"></span>
                                            </label>

                                            <input 
                                                type="text" 
                                                class="form form-control"
                                                id="logradouroEndereco" 
                                                placeholder="Digite o logradouro"/>
                                            <span for="logradouroEndereco" class="help-block valid"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="control-label">
                                                Bairro : 
                                                <span class="symbol required"></span>
                                            </label>

                                            <input 
                                                type="text" 
                                                class="form form-control"
                                                id="bairroEndereco" 
                                                placeholder="Digite o bairro..."/>
                                            <span for="bairroEndereco" class="help-block valid"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-1">
                                            <label class="control-label">
                                                Nº : 
                                                <span class="symbol required"></span>
                                            </label>

                                            <input 
                                                type="text" 
                                                class="form form-control"
                                                id="numeroEndereco" 
                                                placeholder="Digite o número..."/>
                                            <span for="numeroEndereco" class="help-block valid"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-7">
                                            <label class="control-label">
                                                Complemento 
                                                <span class="symbol required"></span>
                                            </label>

                                            <input 
                                                type="text" 
                                                class="form form-control"
                                                id="complementoEndereco" 
                                                placeholder="Digite o complemento..."/>
                                            <span for="complementoEndereco" class="help-block valid"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-1">
                                            <label class="control-label">
                                                UF 
                                                <span class="symbol required"></span>
                                            </label>

                                            <select 
                                                id="ufEndereco" 
                                                class="form form-control search-select select2 ufSelect">
                                                <option value="0">
                                                    Uf...
                                                </option>
                                                <?php foreach (Estados::model()->findAll() as $uf) { ?>
                                                    <option value="<?php echo $uf->sigla; ?>">
                                                        <?php echo $uf->sigla; ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                            <span for="ufEndereco" class="help-block valid"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-2">
                                            <label class="control-label">
                                                Cidade 
                                                <span class="symbol required"></span>
                                            </label>

                                            <input 
                                                required="true" 
                                                placeholder="Cidade..." 
                                                type="hidden" 
                                                id="cidadeEndereco" 
                                                class="form form-control search-select selectCidades" />
                                            <span for="cidadeEndereco" class="help-block valid"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-2">
                                            <label class="control-label">
                                                Tipo Endereço 
                                                <span class="symbol required"></span>
                                            </label>

                                            <select id="tipoEndereco" 
                                                    class="form form-control search-select select2">
                                                <option value="0">
                                                    Tipo...
                                                </option>
                                                <?php foreach (TipoEndereco::model()->findAll() as $te) { ?>
                                                    <option value="<?php echo $te->id; ?>">
                                                        <?php echo $te->tipo; ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                            <span for="tipoEndereco" class="help-block valid"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">

            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i>
                Fornecedores
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                    </a>
                </div>
            </div>

            <div class="panel-body">
                <table id="grid_fornecedores" class="table table-striped table-bordered table-hover table-full-width dataTable">
                    <thead>
                        <tr>
                            <th>

                            </th>
                            <th>
                                <input 
                                    type="text" 
                                    placeholder="Filtre Razão Social..." 
                                    id="filtroRazaoSocial" 
                                    class="form form-control filtroFornecedor" />
                            </th>
                            <th>
                                <input 
                                    type="text" 
                                    placeholder="Filtre Nome Fantasia..." 
                                    id="filtroNomeFantasia" 
                                    class="form form-control filtroFornecedor" />
                            </th>
                            <th>
                                <!--<input 
                                    type="text" 
                                    placeholder="Filtre CGC..." 
                                    id="filtroCGC" 
                                    class="form form-control filtroFornecedor" />-->
                            </th>
                            <th>
                                <select class="select select2 search-select filtroFornecedor" id="filtroTipo">
                                    <option value="0">
                                        Filtre Tipo...
                                    </option>
                                    <?php foreach (TipoFornecedor::model()->findAll('habilitado') as $tipo) { ?>
                                        <option value="<?php echo $tipo->id; ?>">
                                            <?php echo $tipo->descricao; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </th>
                            <th>
                                <!--<input 
                                    type="text" 
                                    placeholder="Filtre UF..." 
                                    id="filtroUF" 
                                    class="form form-control filtroFornecedor" />-->
                            </th>
                            <th>
                                <!--<input 
                                    type="text" 
                                    placeholder="Filtre Cidade..." 
                                    id="filtroCidade" 
                                    class="form form-control filtroFornecedor" />-->
                            </th>
                            <th width="3%">
                            </th>
                        </tr>
                        <tr>
                            <th></th>
                            <th class="no-orderable">Razão Social</th>
                            <th class="no-orderable">Nome Fantasia</th>
                            <th class="no-orderable">CNPJ</th>
                            <th class="no-orderable">Tipo</th>
                            <th class="no-orderable">UF</th>
                            <th class="no-orderable">Cidade</th>
                            <th class="no-orderable"></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<br>
<br>

<style type="text/css">
    #grid_fornecedores_filter, #grid_fornecedores_length{
        display: none;
    }
    td.details-control {
        background: url('../../images/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('../../images/details_close.png') no-repeat center center;
    }
</style>