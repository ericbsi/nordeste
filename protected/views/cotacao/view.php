<?php
/* @var $this CotacaoController */
/* @var $model Cotacao */

$this->breadcrumbs=array(
	'Cotacaos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Cotacao', 'url'=>array('index')),
	array('label'=>'Create Cotacao', 'url'=>array('create')),
	array('label'=>'Update Cotacao', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Cotacao', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Cotacao', 'url'=>array('admin')),
);
?>

<h1>View Cotacao #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'taxa',
		'descricao',
		'Filial_id',
		'data_cadastro',
		'habilitado',
		'parcela_inicio',
		'parcela_fim',
	),
)); ?>
