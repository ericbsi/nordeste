<div class="col-md-12">
<div class="panel panel-default">
   <div class="panel-heading">
      <i class="fa fa-external-link-square"></i>
      Condições: <?php echo $tabela->descricao . " / " . $tabela->taxa . "%"?>
      <div class="panel-tools">
         <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
         </a>             
      </div>
   </div>
   <div class="panel-body">
      <div class="table-responsive">
         <table class="table table-bordered table-hover" id="table-prestacoes">
            <thead>
               <tr>
                  <th style="width:150px">Prestações/Carência</th>
                  <th class="th-carencia">30</th>
                  <th class="th-carencia">35</th>
                  <th class="th-carencia">40</th>
                  <th class="th-carencia">45</th>
                  <th class="th-carencia">50</th>
                  <th class="th-carencia">55</th>
                  <th class="th-carencia">60</th>
               </tr>
            </thead>
            <tbody>
               <?php for ($i = $tabela->parcela_inicio; $i <= $tabela->parcela_fim ; $i++) { ?>
               <tr>
                  <td class="td-prestacao">
                     <?php echo $i ?>
                  </td>
                  
                  <?php $arrReturnValParcela = Fator::model()->getValor($valor_solicitado, $i,30, $tabela->id ); ?>
                  <td valor-parcela="<?php echo $arrReturnValParcela[0]; ?>" class="td-select-parcela" carencia="30" cotacao-id="<?php echo $tabela->id ?>" numero-parcelas="<?php echo $i ?>">
                     <?php
                        echo "R$ ".$arrReturnValParcela[1];
                     ?>
                  </td>
                  
                  <?php $arrReturnValParcela = Fator::model()->getValor($valor_solicitado, $i,35, $tabela->id ); ?>
                  <td valor-parcela="<?php echo $arrReturnValParcela[0]; ?>" class="td-select-parcela" carencia="35" cotacao-id="<?php echo $tabela->id ?>" numero-parcelas="<?php echo $i ?>">
                     <?php
                        echo "R$ ".$arrReturnValParcela[1];
                     ?>
                  </td>

                  <?php $arrReturnValParcela = Fator::model()->getValor($valor_solicitado, $i,40, $tabela->id ); ?>
                  <td valor-parcela="<?php echo $arrReturnValParcela[0]; ?>" class="td-select-parcela" carencia="40" cotacao-id="<?php echo $tabela->id ?>" numero-parcelas="<?php echo $i ?>">
                     <?php
                        echo "R$ ".$arrReturnValParcela[1];
                     ?>
                  </td>

                  <?php $arrReturnValParcela = Fator::model()->getValor($valor_solicitado, $i,45, $tabela->id ); ?>
                  <td valor-parcela="<?php echo $arrReturnValParcela[0]; ?>" class="<?php if($i > 5){ echo "td-select-parcela"; } else{ echo "td-none"; } ?>" carencia="45" cotacao-id="<?php echo $tabela->id ?>" numero-parcelas="<?php echo $i ?>">
                     <?php
                        echo "R$ ".$arrReturnValParcela[1];
                     ?>
                  </td>

                  <?php $arrReturnValParcela = Fator::model()->getValor($valor_solicitado, $i,50, $tabela->id ); ?>
                  <td valor-parcela="<?php echo $arrReturnValParcela[0]; ?>" class="<?php if($i > 5){ echo "td-select-parcela"; } else{ echo "td-none"; } ?>" carencia="50" cotacao-id="<?php echo $tabela->id ?>" numero-parcelas="<?php echo $i ?>">
                     <?php
                        echo "R$ ".$arrReturnValParcela[1];
                     ?>
                  </td>

                  <?php $arrReturnValParcela = Fator::model()->getValor($valor_solicitado, $i,55, $tabela->id ); ?>
                  <td valor-parcela="<?php echo $arrReturnValParcela[0]; ?>" class="<?php if($i > 5){ echo "td-select-parcela"; } else{ echo "td-none"; } ?>" carencia="55" cotacao-id="<?php echo $tabela->id ?>" numero-parcelas="<?php echo $i ?>">
                     <?php
                        echo "R$ ".$arrReturnValParcela[1];
                     ?>
                  </td>

                  <?php $arrReturnValParcela = Fator::model()->getValor($valor_solicitado, $i,60, $tabela->id ); ?>
                  <td valor-parcela="<?php echo $arrReturnValParcela[0]; ?>" class="<?php if($i > 5){ echo "td-select-parcela"; } else{ echo "td-none"; } ?>" carencia="60" cotacao-id="<?php echo $tabela->id ?>" numero-parcelas="<?php echo $i ?>">
                     <?php
                        echo "R$ ".$arrReturnValParcela[1];
                     ?>
                  </td>                  

               </tr>
               <?php }  ?>
            </tbody>
         </table>
      </div>
   </div>
</div>
<form>
   <input type="hidden" value="<?php echo number_format($valor_solicitado, 2, ",", ".") ?>" name="valor_financiado" id="val_fin">
   <input type="hidden" value="<?php echo $valor_solicitado ?>" name="valor_financiado2" id="val_fin2">
   <input type="hidden" value="<?php echo $proposta->id ?>" name="proposta_id" id="propId">
</form>

<script>
   $(document).ready(function(){
      
      $("#table-prestacoes tbody tr .td-select-parcela").on('click', function(){

         if ( !$("#resumo-solicitacao").is(":visible") ) {
            $("#resumo-solicitacao").show();
         }

         $( ".td-select-parcela-selected" ).each(function(){

            $(this).removeClass("td-select-parcela-selected")            

         })

         if ( $(this).hasClass("td-select-parcela-selected") ) {

            $(this).removeClass("td-select-parcela-selected");

         }

         else{

            $(this).addClass("td-select-parcela-selected")
         }
         
         
         $.ajax({

            type : "POST",
            url : "/proposta/alterarCondicoes/",
         
            data : {
               'propostaId'         : $('#propId').val(),
               'cotacao_id'         : $(this).attr('cotacao-id'),
               'carencia'           : $(this).attr('carencia'),
               'numero_parcelas'    : $(this).attr('numero-parcelas'),
               'val_parcelas'       : $(this).attr('valor-parcela')
            },

            success : function (data){
               
               window.parent.$('#table-detalhes-financeiros').DataTable().draw();
            }
         })
    })

   })
</script>

<style type="text/css">
   .td-none{
      color: #ccc;
      text-decoration: line-through
   }
</style>