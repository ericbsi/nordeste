<?php
/* @var $this CotacaoController */
/* @var $data Cotacao */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('taxa')); ?>:</b>
	<?php echo CHtml::encode($data->taxa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dias_carencia')); ?>:</b>
	<?php echo CHtml::encode($data->dias_carencia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descricao')); ?>:</b>
	<?php echo CHtml::encode($data->descricao); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Filial_id')); ?>:</b>
	<?php echo CHtml::encode($data->Filial_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_cadastro')); ?>:</b>
	<?php echo CHtml::encode($data->data_cadastro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('habilitado')); ?>:</b>
	<?php echo CHtml::encode($data->habilitado); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('valor_de')); ?>:</b>
	<?php echo CHtml::encode($data->valor_de); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('valor_ate')); ?>:</b>
	<?php echo CHtml::encode($data->valor_ate); ?>
	<br />

	*/ ?>

</div>