<div class="row">
   <div class="col-sm-12">
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/cliente/listarClientes">
            Cotações
            </a>
         </li>
         <li class="active">
            Editar Tabela
         </li>
      </ol>
      <div class="page-header">
         <h1>
         Editar tabela : <?php echo $tabela->descricao ?>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-sm-12">

   </div>
</div>
<div class="row">
   
   <div class="col-md-2">
      <p>
         <a class="btn btn-success" href="#modal_form_new_fator" data-toggle="modal" id="btn_modal_form_new_fator">
            Inserir fator <i class="fa fa-plus"></i>
         </a>
      </p>
   </div>
   <div class="col-md-4">
      <div class="form-group">
         <label class="control-label">
            Parceiros: 
         </label>
         <select multiple="multiple" id="filiais_select" class="form-control multipleselect">
         
            <?php foreach (Filial::model()->findAll("habilitado") as $f): ?>
               <option <?php if( $tabela->hasFilial( $f ) ){ echo "selected"; } ?> value="<?php echo $f->id ?>"><?php echo $f->getConcat() ?></option>
            <?php endforeach ?>

         </select>
      </div>
   </div>   
</div>
<p>
   
</p>
<div class="row">
   <div class="col-sm-12">
      <table id="grid_fatores" class="table table-striped table-bordered table-hover table-full-width dataTable">
         <thead>
            <tr>
               <th class="no-orderable">Carência</th>
               <th class="no-orderable">Fator</th>
               <th class="no-orderable">Fator IOF</th>
               <th class="no-orderable">Retenção</th>
               <th class="no-orderable">Sequencial da Parcela</th>
            </tr>
         </thead>
      </table>
   </div>
</div>


<div id="modal_form_new_fator" class="modal fade" tabindex="-1" data-width="620" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Cadastrar Fator</h4>
    </div>
    <form id="form-add-fator">
        <div class="modal-body">
            <div class="row">
                <div class="row-centralize">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">
                                        Fator <span class="symbol required"></span>
                                    </label>
                                    <input required name="Fator[fator]" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">
                                        Fator IOF<span class="symbol required"></span>
                                    </label>
                                    <input required name="Fator[fatorIOF]" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">
                                        Retenção
                                    </label>
                                    <input name="Fator[porcentagem_retencao]" type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Carência <span class="symbol required"></span></label>
                                    <select required="required" id="carencias_select" class="form-control multipleselect" name="Fator[carencia]">
                                        <option value="">Selecione</option>
                                        <?php foreach (Carencia::model()->findAll("habilitado") as $carencia) {?>
                                            <option value="<?php echo $carencia->id; ?>">
                                                <?php echo $carencia->valor; ?>
                                            </option>
                                        <?php } ?>
                                    </select>  
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">
                                        Parcela <span class="symbol required"></span>
                                    </label>
                                    <input required name="Fator[parcela]" id="parcela" class="form-control" type="number">
                                    <input name="TabelaId" id="TabelaId" class="form-control" type="hidden" value="<?php echo $tabela->id ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <label class="checkbox-inline" style="float:left">
                <input id="checkbox_continuar" type="checkbox" value="">
                Continuar Cadastrando
            </label>
            <div class="panel"></div>
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
            <button type="submit" class="btn btn-blue">Salvar</button>
            <div class="row">
            </div>
            <br>
            <div id="cadastro_msg_return" class="alert" style="text-align:left">
            </div>
        </div>
    </form>
</div>
<style type="text/css">
   #grid_fatores_filter, #grid_fatores_length{
      display: none!important;
   }
   .panel{
      background: transparent!important;
      border:none!important;
   }
   .dropdown-menu {
      max-height: 200px;
      overflow-y: auto;
      overflow-x: hidden;
   }
</style>