<div class="row">
	<div class="col-md-12">
         
			<div class="panel panel-default">         
         <div class="panel-body">            
            <hr>
	           <?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'cotacoesForm',			
					'enableAjaxValidation'=>false,
					'htmlOptions'=>array(
						'role'=>'form'
					)
				)); ?>

               <div class="row">

                  <div class="col-md-6">
                     <div class="form-group">
                     	<div class="form-group">
                           <?php echo $form->labelEx($model,'taxa', array('class'=>'control-label')); ?>
                           <?php echo $form->textField($model,'taxa',array('class'=>'form-control','required'=>true)); ?>
                        </div>
                        <div class="form-group">
                           <?php echo $form->labelEx($model,'parcela_inicio', array('class'=>'control-label')); ?>
                           <?php echo $form->textField($model,'parcela_inicio',array('class'=>'form-control','required'=>true)); ?>
                        </div>
                        <div class="form-group">
                           <?php echo $form->labelEx($model,'parcela_fim', array('class'=>'control-label')); ?>
                           <?php echo $form->textField($model,'parcela_fim',array('class'=>'form-control','required'=>true)); ?>
                        </div>
                     </div>
                  </div>

                  <?php

                        $empresa = Empresa::model()->find("nome_fantasia = " . "'".Yii::app()->session['usuario']->nomeEmpresa()."'");
                        $filiais = $empresa->listFiliais();

                   ?>

                  <div class="col-md-6">                     
                     <div class="form-group">
                        <label class="control-label">Filiais</label>
                        <?php echo CHtml::dropDownList('Filiais','Filiais',
                              CHtml::listData($filiais,'id','concat' ), array('multiple'=>'multiple', 'class'=>'form-control search-select', 'placeholder'=>'Selecione')); 
                        ?>
                     </div>
                     <div class="form-group">
                        <label class="control-label">Financeiras</label>
                        <?php echo CHtml::dropDownList('Financeiras','Financeiras',
                              CHtml::listData(Financeira::model()->findAll(),
                              'id','nome' ), array('multiple'=>'multiple', 'class'=>'form-control search-select', 'placeholder'=>'Selecione')); 
                        ?>
                     </div>
                     <div class="form-group">
                        <?php echo $form->labelEx($model,'descricao', array('class'=>'control-label')); ?>
                        <textarea name="Cotacao[descricao]" maxlength="100" class="form-control limited"></textarea>
                     </div>
                 </div>
               </div>

               <div class="row">
                  <div class="col-md-12">
                     <div>
                        <span class="symbol required"></span> CAMPOS OBRIGATÓRIOS
                        <hr>
                     </div>
                  </div>
               </div>

               <div class="row">
                  <div class="col-md-8">
                  </div>
                  <div class="col-md-4">
                     <input type="submit" value="Cadastrar" class="btn btn-teal btn-block">
                  </div>
               </div>
 			<?php $this->endWidget(); ?>
         </div>
      </div>
	</div>
</div>