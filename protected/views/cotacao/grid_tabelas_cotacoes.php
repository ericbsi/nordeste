<div class="row">
   <div class="col-sm-12">
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/cotacao/tabelas">
            Cotações
            </a>
         </li>
         <li class="active">
            Tabelas
         </li>
      </ol>
      <div class="page-header">
         <h1>
         Tabelas de Cotações
      </div>
   </div>
</div>
<p>
   <a class="btn btn-success" href="#modal_form_new_tabela" data-toggle="modal" id="btn_modal_form_new_tabela">
   Cadastrar Tabela <i class="fa fa-plus"></i>
   </a>
</p>
<p>
   
</p>
<div class="row">
   <div class="col-sm-12">
      <table id="grid_tabelas" class="table table-striped table-bordered table-hover table-full-width dataTable">
         <thead>
            <tr>
               <th class="no-orderable">Descrição</th>
               <th class="no-orderable">Modalidade</th>
               <th class="no-orderable">Parcela Início</th>
               <th class="no-orderable">Parcela Fim</th>
               <th class="no-orderable">Taxa %</th>
               <th class="no-orderable">Taxa Anual %</th>
               <th class="no-orderable"></th>
            </tr>
         </thead>
      </table>
   </div>
</div>
<div id="modal_form_new_tabela" class="modal fade" tabindex="-1" data-width="780" style="display: none;">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
      &times;
      </button>
      <h4 class="modal-title">Cadastrar Tabela</h4>
   </div>
   <form id="form-add-tabela">
      <div class="modal-body">
         <div class="row">
            <div class="row-centralize">
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label class="control-label">
                              Descrição <span class="symbol required"></span>
                           </label>
                           <input required name="TabelaCotacao[descricao]" type="text" class="form-control">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-3">
                        <div class="form-group">
                           <label class="control-label">
                           Parcela Início <span class="symbol required"></span>
                           </label>
                           <input required name="TabelaCotacao[parcela_inicio]" id="parcela_inicio" class="form-control" type="text">
                        </div>
                     </div>
                     <div class="col-md-3">
                        <div class="form-group">
                           <label class="control-label">
                           Parcela Fim <span class="symbol required"></span>
                           </label>
                           <input required name="TabelaCotacao[parcela_fim]" id="parcela_fim" class="form-control" type="text">
                        </div>
                     </div>
                     <div class="col-md-3">
                        <div class="form-group">
                           <label class="control-label">
                           Taxa Mensal <span class="symbol required"></span>
                           </label>
                           <input required name="TabelaCotacao[taxa]" id="taxa" class="form-control" type="text">
                        </div>
                     </div>
                     <div class="col-md-3">
                        <div class="form-group">
                           <label class="control-label">
                           Taxa Anual <span class="symbol required"></span>
                           </label>
                           <input required name="TabelaCotacao[taxa_anual]" id="taxa_anual" class="form-control" type="text">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-4">
                        <div class="form-group">
                           <label class="control-label">
                              Filiais <span class="symbol required"></span>
                           </label>
                           <select required="required" multiple="multiple" id="filiais_select" class="form-control multipleselect" name="Filiais[]">
                              <?php foreach ( Yii::app()->session['usuario']->getEmpresa()->listFiliais() as $f ) { ?>
                                 <option value="<?php echo $f->id ?>"><?php echo $f->getConcat() ?></option>
                              <?php } ?>
                           </select>  
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label class="control-label">
                              Financeiras <span class="symbol required"></span>
                           </label>
                           <select required="required" multiple="multiple" id="financeiras_select" class="form-control multipleselect" name="Financeiras[]">
                              <?php foreach ( Financeira::model()->findAll() as $financeira ) { ?>
                                 <option value="<?php echo $financeira->id ?>"><?php echo $financeira->nome?></option>
                              <?php } ?>
                           </select>  
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label class="control-label">
                              Modalidades <span class="symbol required"></span>
                           </label>
                           <select required="required" id="modalidades_select" class="form-control multipleselect" name="TabelaCotacao[ModalidadeId]">
                              <?php foreach ( ModalidadeTabela::model()->findAll() as $modalidade ) { ?>
                                 <option value="<?php echo $modalidade->id ?>"><?php echo $modalidade->descricao ?></option>
                              <?php } ?>
                           </select>  
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal-footer">
         <label class="checkbox-inline" style="float:left">
            <input id="checkbox_continuar" type="checkbox" value="">
            Continuar Cadastrando
         </label>
         <div class="panel"></div>
         <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
         <button type="submit" class="btn btn-blue">Salvar</button>
         <div class="row">
         </div>
         <br>
         <div id="cadastro_msg_return" class="alert" style="text-align:left">
         </div>
      </div>
   </form>
</div>
<style type="text/css">
   #grid_tabelas_filter, #grid_tabelas_length{
      display: none!important;
   }
   .panel{
      background: transparent!important;
      border:none!important;
   }
</style>