<?php
/* @var $this CotacaoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cotacaos',
);

$this->menu=array(
	array('label'=>'Create Cotacao', 'url'=>array('create')),
	array('label'=>'Manage Cotacao', 'url'=>array('admin')),
);
?>

<h1>Cotacaos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
