<div class="row">
	<div class="col-md-12">

		<div class="row">
                  <div class="col-sm-12">                   
                     <!-- start: PAGE TITLE & BREADCRUMB -->
                     <ol class="breadcrumb">
                        <li>
                           <i class="clip-pencil"></i>
                           <a href="#">
                              Cotações
                           </a>
                        </li>
                        <li class="active">
                              Administrar
                        </li>
                        
                     </ol>

                     <div class="page-header">
                        <h1>Cotações cadastradas</h1>
                     </div>
                     <!-- end: PAGE TITLE & BREADCRUMB -->
                  </div>
         </div>

		<div class="panel panel-default">
		<div class="panel-heading">
					<i class="fa fa-external-link-square"></i>
					<div class="panel-tools">
					<a class="btn btn-xs btn-link panel-collapse collapses" href="#">
					</a>
				</div>
			</div>

			<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'fornecedores-grid',
				'dataProvider'=>Yii::app()->session['usuario']->listCotacoesEmpresa(),
				//'filter'=>$model,
				'itemsCssClass' => 'table table-bordered responsive dataTable',
				//'template'=>"{items}\n{summary}\n{pager}",
				'enablePagination'=>true,
				'summaryText'=>'Exibindo {start} de {end} resultados',
				'columns'=>array(
					'descricao',		
					array(
						'header'=>'Taxa %',
			            'value'=>'$data->taxaToView()',
					),

					array(
						'header'=>'Parcela Início',
			            'value'=>'$data->parcela_inicio',
			            'type'=>'number',
					),
					array(
						'header'=>'Parcela Fim',
			            'value'=>'$data->parcela_fim',
			            'type'=>'number',
					),
					array(
						'class'=>'CButtonColumn',
						'template' => '{update}',
						'buttons'=>array(
							'update'=>array(
								'options'=>array('title'=>'Atualizar dados', 'data-toggle'=>'modal', 'data-target'=>'#responsive'),
								'click'=> 'function(){$("#cru-frame").attr("src",$(this).attr("href")); return false;}',
							),	
						)
					),
				),
			)); ?>
		</div>
	</div>	
</div>

<div class="btn-group">
		<button data-toggle="dropdown" class="btn btn-danger dropdown-toggle">
			<i class="clip-settings"></i>
			Opções <span class="caret"></span>
		</button>
		<ul class="dropdown-menu" role="menu">
			<li>
				<a class="crud-modal-show" href="#" data-toggle="modal" data-target="#responsive">
					<i class="fa fa-plus"></i>
					Adicionar
				</a>
			</li>
		</ul>
</div>

<div id="responsive" class="modal fade" tabindex="-1" data-heigh="" data-width="1200" style="display: none;">
			
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
			&times;
		</button>
		<h4 class="modal-title">Cadastrar Cotação</h4>
	</div>

	<div class="modal-body">
		<div class="row">
			<iframe style="backgroun:#FFF!important" scrolling="no" frameBorder="0" class="overflownone" id="cru-frame" width="100%" height="400px"></iframe>
		</div>
	</div>
</div> <!--Fim modal-->


<?php $createUrl = $this->createUrl('create',array("asDialog"=>1,"gridId"=>'cotacao-grid')); ?>
<input type="hidden" id="urlcrud" value="<?php echo $createUrl ?>">
