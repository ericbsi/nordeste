<div class="col-md-12">
<div class="panel panel-default">
   <div class="panel-heading">
      <i class="fa fa-external-link-square"></i>
      Condições: <?php echo $tabela->descricao . " / " . $tabela->taxa . "%"?>
      <div class="panel-tools">
         <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
         </a>             
      </div>
   </div>
   <div class="panel-body">
      <div class="table-responsive">
         <table class="table table-bordered table-hover" id="table-prestacoes">
            <thead>
               <tr>
                  <th style="width:150px">Prestações/Carência</th>
                  <th class="th-carencia">15</th>
                  <th class="th-carencia">30</th>
                  <th class="th-carencia">35</th>
                  <th class="th-carencia">40</th>
                  <th class="th-carencia">45</th>
                  <th class="th-carencia">50</th>
                  <th class="th-carencia">55</th>
                  <th class="th-carencia">60</th>
                  <th class="th-carencia">90</th>
                  <th class="th-carencia">120</th>
               </tr>
            </thead>
            <tbody>
               <?php for ($i = $tabela->parcela_inicio; $i <= $tabela->parcela_fim ; $i++) { ?>
               <tr>
                  <td class="td-prestacao">
                     <?php echo $i ?>
                  </td>
                  
                  <?php $arrReturnValParcela = Fator::model()->getValor($valor_solicitado, $i,15, $tabela->id ); ?>
                  <td valor-parcela="<?php echo $arrReturnValParcela[0]; ?>" class="<?php if( is_null($arrReturnValParcela[1]) ){echo "td-none";} else{echo "td-select-parcela"; }?>" carencia="15" cotacao-id="<?php echo $tabela->id ?>" numero-parcelas="<?php echo $i ?>">
                     <?php
                        echo "R$ ".$arrReturnValParcela[1];
                     ?>
                  </td>

                  <?php $arrReturnValParcela = Fator::model()->getValor($valor_solicitado, $i,30, $tabela->id ); ?>
                  <td valor-parcela="<?php echo $arrReturnValParcela[0]; ?>" class="td-select-parcela" carencia="30" cotacao-id="<?php echo $tabela->id ?>" numero-parcelas="<?php echo $i ?>">
                     <?php
                        echo "R$ ".$arrReturnValParcela[1];
                     ?>
                  </td>
                  
                  <?php $arrReturnValParcela = Fator::model()->getValor($valor_solicitado, $i,35, $tabela->id ); ?>
                  <td valor-parcela="<?php echo $arrReturnValParcela[0]; ?>" class="<?php if( is_null($arrReturnValParcela[1]) ){echo "td-none";} else{echo "td-select-parcela"; }?>" carencia="35" cotacao-id="<?php echo $tabela->id ?>" numero-parcelas="<?php echo $i ?>">
                     <?php
                        echo "R$ ".$arrReturnValParcela[1];
                     ?>
                  </td>

                  <?php $arrReturnValParcela = Fator::model()->getValor($valor_solicitado, $i,40, $tabela->id ); ?>
                  <td valor-parcela="<?php echo $arrReturnValParcela[0]; ?>" class="<?php if( is_null($arrReturnValParcela[1]) ){echo "td-none";} else{echo "td-select-parcela"; }?>" carencia="40" cotacao-id="<?php echo $tabela->id ?>" numero-parcelas="<?php echo $i ?>">
                     <?php
                        echo "R$ ".$arrReturnValParcela[1];
                     ?>
                  </td>

                  <?php $arrReturnValParcela = Fator::model()->getValor($valor_solicitado, $i,45, $tabela->id ); ?>
                  <td valor-parcela="<?php echo $arrReturnValParcela[0]; ?>" class="<?php if($tabela->ModalidadeId != 1 ){ echo "td-select-parcela"; } else{ echo "td-none"; } ?>" carencia="45" cotacao-id="<?php echo $tabela->id ?>" numero-parcelas="<?php echo $i ?>">
                     <?php
                        echo "R$ ".$arrReturnValParcela[1];
                     ?>
                  </td>

                  <?php $arrReturnValParcela = Fator::model()->getValor($valor_solicitado, $i,50, $tabela->id ); ?>
                  <td valor-parcela="<?php echo $arrReturnValParcela[0]; ?>" class="<?php if( is_null($arrReturnValParcela[1]) ){echo "td-none";} else{echo "td-select-parcela"; }?>" carencia="50" cotacao-id="<?php echo $tabela->id ?>" numero-parcelas="<?php echo $i ?>">
                     <?php
                        echo "R$ ".$arrReturnValParcela[1];
                     ?>
                  </td>

                  <?php $arrReturnValParcela = Fator::model()->getValor($valor_solicitado, $i,55, $tabela->id ); ?>
                  <td valor-parcela="<?php echo $arrReturnValParcela[0]; ?>" class="<?php if( is_null($arrReturnValParcela[1]) ){echo "td-none";} else{echo "td-select-parcela"; }?>" carencia="55" cotacao-id="<?php echo $tabela->id ?>" numero-parcelas="<?php echo $i ?>">
                     <?php
                        echo "R$ ".$arrReturnValParcela[1];
                     ?>
                  </td>

                  <?php $arrReturnValParcela = Fator::model()->getValor($valor_solicitado, $i,60, $tabela->id ); ?>
                  <td valor-parcela="<?php echo $arrReturnValParcela[0]; ?>" class="<?php if( is_null($arrReturnValParcela[1]) ){echo "td-none";} else{echo "td-select-parcela"; }?>" carencia="60" cotacao-id="<?php echo $tabela->id ?>" numero-parcelas="<?php echo $i ?>">
                     <?php
                        echo "R$ ".$arrReturnValParcela[1];
                     ?>
                  </td>

                  <?php $arrReturnValParcela = Fator::model()->getValor($valor_solicitado, $i,90, $tabela->id ); ?>
                  <td valor-parcela="<?php echo $arrReturnValParcela[0]; ?>" class="<?php if( is_null($arrReturnValParcela[1]) ){echo "td-none";} else{echo "td-select-parcela"; }?>" carencia="90" cotacao-id="<?php echo $tabela->id ?>" numero-parcelas="<?php echo $i ?>">
                     <?php
                        echo "R$ ".$arrReturnValParcela[1];
                     ?>
                  </td>

                  <?php $arrReturnValParcela = Fator::model()->getValor($valor_solicitado, $i,120, $tabela->id ); ?>
                  <td valor-parcela="<?php echo $arrReturnValParcela[0]; ?>" class="<?php if( is_null($arrReturnValParcela[1]) ){echo "td-none";} else{echo "td-select-parcela"; }?>" carencia="120" cotacao-id="<?php echo $tabela->id ?>" numero-parcelas="<?php echo $i ?>">
                     <?php
                        echo "R$ ".$arrReturnValParcela[1];
                     ?>
                  </td>

               </tr>
               <?php }  ?>
            </tbody>
         </table>
      </div>
   </div>
</div>
<form>
   <input type="hidden" value="<?php echo number_format($valor_solicitado, 2, ",", ".") ?>" name="valor_financiado" id="val_fin">
   <input type="hidden" value="<?php echo $valor_solicitado ?>" name="valor_financiado2" id="val_fin2">
</form>

<script>

   $(document).ready(function(){
      $("#table-prestacoes tbody tr .td-select-parcela").on('click', function(){

         if ( !$("#resumo-solicitacao").is(":visible") ) {
            $("#resumo-solicitacao").show();
         }

         $( ".td-select-parcela-selected" ).each(function(){

            $(this).removeClass("td-select-parcela-selected")            

         })

         if ( $(this).hasClass("td-select-parcela-selected") ) {

            $(this).removeClass("td-select-parcela-selected");

         }

         else{

            $(this).addClass("td-select-parcela-selected")
         }
         
         /*enviando*/
         $.ajax({

            type : "POST",
            url : "/analiseDeCredito/analiseCheckout/",
         
            data : {
               'cotacao_id' : $(this).attr('cotacao-id'),
               'carencia' : $(this).attr('carencia'),
               'val_fin' : $('#val_fin2').val(),
               'numero_parcelas' : $(this).attr('numero-parcelas'),
               'valor_parcela' : $(this).attr('valor-parcela')
            },

            success : function ( checkout ){
               
               var jsonReturn = $.parseJSON( checkout );

               $('#td_val_financiado').html("R$ " + jsonReturn.valor_financiadoMASK)
               $('#td_num_parcelas').html(jsonReturn.numero_parcelas)
               $('#td_val_parcelas').html("R$ " + jsonReturn.valor_parcelaMASK)
               $('#td_val_total').html("R$ " + jsonReturn.valor_totalMASK)
               $('#td_pri_parcela').html(jsonReturn.data_primeira_parcela),
               $('#td_ultima_parcela').html(jsonReturn.data_ultima_parcela)
               $('#td_cotacao').html(jsonReturn.cotacao_descri)
               $('#td_cotacao_taxa').html(jsonReturn.taxa_MASK)
               $('#td_carencia').html(jsonReturn.carencia)

               
               $('#num_parcelas_hidden').val(jsonReturn.numero_parcelas)
               $('#val_parcelas_hidden').val(jsonReturn.valor_parcela)
               $('#val_total_hidden').val(jsonReturn.valor_total)
               $('#data_pri_par_hidden').val(jsonReturn.data_primeira_parcela)
               $('#data_ult_par_hidden').val(jsonReturn.data_ultima_parcela)
               $('#cotacao_id_hidden').val(jsonReturn.cotacao_id)
               $('#carencia_hidden').val(jsonReturn.carencia)

               $('#valor_financiado_hidden').val(jsonReturn.valor_financiado)

            },

            error : function ( msg ) {
               //alert("Algo errado: " + msg)
            }

         })
      })
   })
</script>

<style type="text/css">
   .td-none{
      color: #ccc;
      text-decoration: line-through
   }
</style>