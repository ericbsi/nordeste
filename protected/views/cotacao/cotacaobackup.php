<div class="row">
	<div class="col-md-12">
         <div class="row">
                  <div class="col-sm-12">                   
                     <!-- start: PAGE TITLE & BREADCRUMB -->
                     <ol class="breadcrumb">
                        <li>
                           <i class="clip-pencil"></i>
                           <a href="#">
                              Cotações
                           </a>
                        </li>
                        <li class="active">
                              Criar
                        </li>
                        
                     </ol>

                     <div class="page-header">
                        <h1>Cadastrar Cotação</h1>
                     </div>
                     <!-- end: PAGE TITLE & BREADCRUMB -->
                  </div>
         </div>
			<div class="panel panel-default">
         <div class="panel-heading">
            <i class="fa fa-external-link-square"></i>
            
            <div class="panel-tools">
               <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
               </a>
            </div>
         </div>
         <div class="panel-body">            
            <hr>
	           <?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'cotacoesForm',			
					'enableAjaxValidation'=>false,
					'htmlOptions'=>array(
						'role'=>'form'
					)
				)); ?>

               <div class="row">

                  <div class="col-md-6">
                     <div class="form-group">
                     	<div class="form-group">
                           <?php echo $form->labelEx($model,'taxa', array('class'=>'control-label')); ?>
                           <?php echo $form->textField($model,'taxa',array('class'=>'form-control','required'=>true)); ?>
                        </div>
                        <div class="form-group">
                           <?php echo $form->labelEx($model,'parcela_inicio', array('class'=>'control-label')); ?>
                           <?php echo $form->textField($model,'parcela_inicio',array('class'=>'form-control','required'=>true)); ?>
                        </div>
                        <div class="form-group">
                           <?php echo $form->labelEx($model,'parcela_fim', array('class'=>'control-label')); ?>
                           <?php echo $form->textField($model,'parcela_fim',array('class'=>'form-control','required'=>true)); ?>
                        </div>
                     </div>
                  </div>

                  <div class="col-md-6">                     
                     <div class="form-group">
                        <?php echo $form->labelEx($model,'Filial_id', array('class'=>'control-label')); ?>
                        <?php echo CHtml::dropDownList('Filiais','Filiais',
                              CHtml::listData(Filial::model()->findAll(),
                              'id','nome_fantasia' ), array('class'=>'form-control search-select', 'placeholder'=>'Selecione')); 
                        ?>
                     </div>
                     <div class="form-group">
                        <?php echo $form->labelEx($model,'descricao', array('class'=>'control-label')); ?>
                        <textarea name="Cotacao[descricao]" maxlength="100" class="form-control limited"></textarea>
                     </div>
                 </div>
               </div>

               <div class="row">
                  <div class="col-md-12">
                     <div>
                        <span class="symbol required"></span> CAMPOS OBRIGATÓRIOS
                        <hr>
                     </div>
                  </div>
               </div>

               <div class="row">
                  <div class="col-md-8">
                  </div>
                  <div class="col-md-4">
                     <input type="submit" value="Cadastrar" class="btn btn-teal btn-block">
                  </div>
               </div>
 			<?php $this->endWidget(); ?>
         </div>
      </div>
	</div>
</div>