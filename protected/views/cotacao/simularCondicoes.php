<?php  ?>
<div class="row">
   <div class="col-sm-12">
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/cliente/listarClientes">
            Cotação
            </a>
         </li>
         <li class="active">
            Simular
         </li>
      </ol>
      <div class="page-header">
         <h1>
         Simular condições
      </div>
   </div>
</div>
<div class="row">
   <div class="col-sm-9">
      <div class="row">
         <form method="post" id="form-simular" action="/cotacao/simularCondicoes/">
            <div class="col-md-3">
               <div class="form-group">
                  <label class="control-label">
                  Valor <span class="symbol required" aria-required="true"></span>
                  </label>                  
                  <input required="required" type="text" name="valor" class="form-control currency" id="valor" value="<?php if(isset($valor_solicitado)){ echo number_format($valor_solicitado,2,',','.'); } ?>">
                  <input type="hidden" name="valor_numeric" class="form-control currency" id="valor_numeric" value="<?php if(isset($valor_solicitado)){ echo $valor_solicitado; } ?>">
               </div>
            </div>
            <div class="col-md-4">
               <div class="form-group">
                  <label class="control-label">
                     Tabela <span class="symbol required" aria-required="true"></span>
                     <?php echo CHtml::dropDownList('cotacoes','id', CHtml::listData( TabelaCotacao::model()->findAll( Cotacao::model()->filialCotacoes( Yii::app()->session['usuario']->returnFilial() ) ),
                        'id','descricao' ), array( 'class'=>'form-control search-select select2', 'id'=>'Cotacao_id' ));
                     ?>
                     <!--<select required="required" id="tabelaSelect" class="form-control search-select select2" name="tabela" style="width:300px">
                        <option value="">Selecione:</option>
                        
                        <?php foreach( TabelaCotacao::model()->findAll() as $table ){ ?>
                        <option <?php if( isset($tabela) && $table->id == $tabela->id ){ echo "selected"; } ?> value="<?php echo $table->id ?>"><?php echo $table->descricao ?></option>
                        <?php } ?>
                        
                     </select>
                     -->
                  </label>
               </div>
            </div>
            <div class="col-md-3">
               <div class="form-group">
                  <label class="control-label">&nbsp;</label>
                  <button id="btn-filter" class="btn btn-blue next-step btn-block">Simular</button>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-sm-12">
      <?php if( isset($tabela) && isset($valor_solicitado) ){ ?>
      <table class="table table-bordered table-hover" id="table-prestacoes">
         <thead>
            <tr>
               <th style="width:150px">Prestações/Carência</th>
               <th class="th-carencia">30</th>
               <th class="th-carencia">35</th>
               <th class="th-carencia">40</th>
               <th class="th-carencia">45</th>
               <th class="th-carencia">50</th>
               <th class="th-carencia">55</th>
               <th class="th-carencia">60</th>
            </tr>
         </thead>
         <tbody>
            <?php for ($i = $tabela->parcela_inicio; $i <= $tabela->parcela_fim ; $i++) { ?>
            <tr>
               <td class="td-prestacao">
                  <?php echo $i ?>
               </td>
               <?php $arrReturnValParcela = Fator::model()->getValor($valor_solicitado, $i,30, $tabela->id ); ?>
               <td valor-parcela="<?php echo $arrReturnValParcela[0]; ?>" class="td-select-parcela" carencia="30" cotacao-id="<?php echo $tabela->id ?>" numero-parcelas="<?php echo $i ?>">
                  <?php
                     echo "R$ ".$arrReturnValParcela[1];
                     ?>
               </td>
               <?php $arrReturnValParcela = Fator::model()->getValor($valor_solicitado, $i,35, $tabela->id ); ?>
               <td valor-parcela="<?php echo $arrReturnValParcela[0]; ?>" class="td-select-parcela" carencia="35" cotacao-id="<?php echo $tabela->id ?>" numero-parcelas="<?php echo $i ?>">
                  <?php
                     echo "R$ ".$arrReturnValParcela[1];
                     ?>
               </td>
               <?php $arrReturnValParcela = Fator::model()->getValor($valor_solicitado, $i,40, $tabela->id ); ?>
               <td valor-parcela="<?php echo $arrReturnValParcela[0]; ?>" class="td-select-parcela" carencia="40" cotacao-id="<?php echo $tabela->id ?>" numero-parcelas="<?php echo $i ?>">
                  <?php
                     echo "R$ ".$arrReturnValParcela[1];
                     ?>
               </td>
               <?php $arrReturnValParcela = Fator::model()->getValor($valor_solicitado, $i,45, $tabela->id ); ?>
               <td valor-parcela="<?php echo $arrReturnValParcela[0]; ?>" class="<?php if($i > 5){ echo "td-select-parcela"; } else{ echo "td-none"; } ?>" carencia="45" cotacao-id="<?php echo $tabela->id ?>" numero-parcelas="<?php echo $i ?>">
                  <?php
                     echo "R$ ".$arrReturnValParcela[1];
                     ?>
               </td>
               <?php $arrReturnValParcela = Fator::model()->getValor($valor_solicitado, $i,50, $tabela->id ); ?>
               <td valor-parcela="<?php echo $arrReturnValParcela[0]; ?>" class="<?php if($i > 5){ echo "td-select-parcela"; } else{ echo "td-none"; } ?>" carencia="50" cotacao-id="<?php echo $tabela->id ?>" numero-parcelas="<?php echo $i ?>">
                  <?php
                     echo "R$ ".$arrReturnValParcela[1];
                     ?>
               </td>
               <?php $arrReturnValParcela = Fator::model()->getValor($valor_solicitado, $i,55, $tabela->id ); ?>
               <td valor-parcela="<?php echo $arrReturnValParcela[0]; ?>" class="<?php if($i > 5){ echo "td-select-parcela"; } else{ echo "td-none"; } ?>" carencia="55" cotacao-id="<?php echo $tabela->id ?>" numero-parcelas="<?php echo $i ?>">
                  <?php
                     echo "R$ ".$arrReturnValParcela[1];
                     ?>
               </td>
               <?php $arrReturnValParcela = Fator::model()->getValor($valor_solicitado, $i,60, $tabela->id ); ?>
               <td valor-parcela="<?php echo $arrReturnValParcela[0]; ?>" class="<?php if($i > 5){ echo "td-select-parcela"; } else{ echo "td-none"; } ?>" carencia="60" cotacao-id="<?php echo $tabela->id ?>" numero-parcelas="<?php echo $i ?>">
                  <?php
                     echo "R$ ".$arrReturnValParcela[1];
                     ?>
               </td>
            </tr>
            <?php }  ?>
         </tbody>
      </table>
      <?php } ?>
   </div>
</div>
<div id="responsive" class="modal fade" tabindex="-1" data-width="380" style="display: none;">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
      &times;
      </button>
      <h4 class="modal-title">Simulação</h4>
   </div>
   <div class="modal-body">
      <div class="row">
         <div class="col-sm-12" id="resumo-solicitacao" style="display:none">
            <div class="core-box">
               <div class="heading">
                  <i class=" circle-icon circle-green fa fa-list"></i>
                  <h2>Resumo</h2>
               </div>
               <div class="content">
                  <table class="table table-condensed table-hover">
                     <thead>
                     </thead>
                     <tbody>
                        <tr>
                           <td>Valor financiado: </td>
                           <td id="td_val_financiado">
                           </td>
                        </tr>
                        <tr>
                           <td>Número de parcelas: </td>
                           <td id="td_num_parcelas">
                           </td>
                        </tr>
                        <tr>
                           <td>Valor da parcela: </td>
                           <td id="td_val_parcelas">
                           </td>
                        </tr>
                        <tr>
                           <td>Taxa (%): </td>
                           <td id="td_cotacao_taxa">
                           </td>
                        </tr>
                        <tr>
                           <td>Valor total: </td>
                           <td id="td_val_total">
                           </td>
                        </tr>
                        <tr>
                           <td>Carência: </td>
                           <td id="td_carencia">
                           </td>
                        </tr>
                        <tr>
                           <td>Data da 1° parcela: </td>
                           <td id="td_pri_parcela">
                           </td>
                        </tr>
                        <tr>
                           <td>Data da última parcela: </td>
                           <td id="td_ultima_parcela">
                           </td>
                        </tr>
                        <tr>
                           <td>Cotação: </td>
                           <td id="td_cotacao">
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  <form method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/analiseDeCredito/analiseResumeCliente">
                     <!--Informações do cliente-->
                     <!--Informações da solicitação-->
                     <!--Resumo-->
                     <input type="hidden" value="" name="num_parcelas_hidden" id="num_parcelas_hidden">
                     <input type="hidden" value="" name="val_parcelas_hidden" id="val_parcelas_hidden">
                     <input type="hidden" value="" name="val_total_hidden" id="val_total_hidden">
                     <input type="hidden" value="" name="data_pri_par_hidden" id="data_pri_par_hidden">
                     <input type="hidden" value="" name="data_ult_par_hidden" id="data_ult_par_hidden">
                     <input type="hidden" value="" name="cotacao_id_hidden" id="cotacao_id_hidden">
                     <input type="hidden" value="" name="carencia_hidden" id="carencia_hidden">
                     <input type="hidden" value="" name="valor_financiado_hidden" id="valor_financiado_hidden">
                  </form>
               </div>
            </div>
         </div>
         <div class="col-md-6">
         </div>
      </div>
   </div>
   <div class="modal-footer">
      <button type="button" data-dismiss="modal" class="btn btn-light-grey">
      Fechar
      </button>
   </div>
</div>
<style type="text/css">
   .td-none{
   color: #ccc;
   text-decoration: line-through
   }
</style>