<div class="title-header">Financeiras Associadas</div>
<div id="cotacao-grid" class="grid-view">
   <table class="items" id="finassoc-table">
      <thead>
         <tr>
            <th id="cotacao-grid_c0">Financeira</th>
            <th id="cotacao-grid_c0">CNPJ</th>
            <th class="button-column" id="cotacao-grid_c6">Remover</th>
         </tr>
      </thead>
      <tbody>
         <?php foreach( $cotacoesAssociadas as $finAssoc ){ ?>
         <tr class="odd">
            <td><?php echo $finAssoc['nome'] ?></td>
            <td><?php echo $finAssoc['cnpj'] ?></td>
            <td class="button-column"><a cotacao-id="<?php echo $cotacao->id ?>" financeira-id="<?php echo $finAssoc['id'] ?>" class="bt-finremove"><img src="/assets/f6b6321/gridview/delete.png" /></a></td>
         </tr>      
         <?php } ?>
      </tbody>
   </table>
</div>


<div class="title-header">Financeiras não Associadas</div>
<div id="cotacao-grid" class="grid-view">
   <table class="items" id="finnaoassoc-table">
      <thead>
         <tr>
            <th id="cotacao-grid_c0">Financeira</th>
            <th id="cotacao-grid_c0">CNPJ</th>
            <th class="button-column" id="cotacao-grid_c6">Adicionar</th>
         </tr>
      </thead>
      <tbody>
         <?php foreach( $cotacoesNaoAssociadas as $finAssoc ){ ?>
         <tr class="odd">
            <td><?php echo $finAssoc['nome'] ?></td>
            <td><?php echo $finAssoc['cnpj'] ?></td>
            <td class="button-column"><a <a cotacao-id="<?php echo $cotacao->id ?>" financeira-id="<?php echo $finAssoc['id'] ?>" class="bt-add"><img width="20" height="15" src="/images/accept-button.jpg"/></a></td>
         </tr>      
         <?php } ?>
      </tbody>
   </table>
</div>
<form>
   <input type="hidden" value="" id="cotacao_id_input"/>
</form>
<style type="text/css">
   .title-header{
      background: url("http://sigac/assets/f6b6321/gridview/bg.gif") repeat-x scroll left top white;
      text-align: center;
      color: #FFF;
      padding: 3px 0;
      font-weight: bold;
   }
</style>

<script type="text/javascript">
   
   $(document).ready(function () {

    $('#finassoc-table .bt-finremove').live('click',function(){

/*
      $.ajax({

         type: "POST",
         url: "/cotacao/rmfinanceira/",

         data: {
           'cotacao_id': $(this).attr('cotacao-id') ),
           'financeira_id': $(this).attr('financeira-id') )
         },
         
         beforeSend: function(){
            console.log( "Enviando..." )
            console.log("cotacao-id = " + $(this).attr('cotacao-id') ) )
            console.log("financeira-id = " + $(this).attr('financeira-id') ) )
         }

         success: function( retorno ) {

            console.log( retorno )
            console.log("")
         }

      })*/
   
      var newRow = $(this).parent().parent().clone();
      $(newRow).children().children().children().attr('src', '/images/accept-button.jpg').width('20').height('15');
      $(newRow).children().children().addClass('bt-add').removeClass('bt-finremove');
      $("#finnaoassoc-table").append(newRow).html();
      $(this).parent().parent().remove();

    });
    
    
    $('#finnaoassoc-table .bt-add').live('click',function(){         
      
      var newRow2 = $(this).parent().parent().clone();
      $(newRow2).children().children().children().attr('src', 'http://sigac/assets/f6b6321/gridview/delete.png');
      $(newRow2).children().children().addClass('bt-finremove').removeClass('bt-add');
      $("#finassoc-table").append(newRow2).html();
      $(this).parent().parent().remove();

    })

   });
</script>