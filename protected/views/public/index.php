<?php $util = new Util; ?>
<div class="row">
   <div class="col-sm-12">
      <!-- start: PAGE TITLE & BREADCRUMB -->
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
            Acesso Público
            </a>
         </li>
         <li class="active">
            Segunda via de boletos
         </li>
      </ol>
      <div class="page-header">
         <h1>Segunda via de Boletos</h1>
      </div>
      <!-- end: PAGE TITLE & BREADCRUMB -->
   </div>
</div>
<div class="alert alert-info">
	<i class="fa fa-info-circle"></i>
	<strong>Atenção!</strong> Preencha o formulário abaixo para obter as informaçẽos sobre a compra.
</div>

<div class="alert alert-warning">
	<i class="fa fa-info-circle"></i>
	<strong>Atenção!</strong> Caso não consiga imprimir o boleto, entre em contato através do telefone: <strong>84 2040 0800</strong>, ou envie um email para <strong>boleto@credshow.com.br</strong>.
</div>

<div class="row">
   <div class="col-sm-12">
		<form id="form-boletos" method="post" action="/public/boletos/">
		   <div class="row">
		      <div class="form-group">
		        <div class="col-md-3">
		            <label class="control-label">CPF do Cliente : <span class="symbol required" ></span></label>
		            <span class="input-icon">
		               <input required="required" type="text" class="form-control cpf input-lg cpfmask" id="cpfcliente" name="cpf">
		            </span>
		         </div>
		         <div class="col-md-3">
		            <label class="control-label">Código da proposta : <span class="symbol required" ></span></label>
		            <span class="input-icon">
		               <input required="required" type="text" class="form-control number input-lg" name="codigo">
		            </span>
		         </div>
		      </div>
		   </div>
		   <div class="row">
		      <div class="form-group">
		        <div class="col-md-2">
		            <label class="control-label"></label>
		            <span class="input-icon">
		               <button type="submit" class="btn btn-success btn-squared btn-lg">Buscar</button>
		            </span>
		         </div>
		      </div>
		   </div>
		</form>
   </div>
</div>
<br>

<?php if( $proposta != NULL ){ ?>
<div class="row" style="margin-bottom:30px">
	<div class="col-sm-12">
		<div class="page-header">
	       <h1>Boletos</h1>
	    </div>

		<table id="grid_parcelas" class="table table-striped table-bordered table-hover table-full-width dataTable">

			<thead>
		        <tr>
		        	<th class="no-orderable" width="30">Parcela</th>
		            <th class="no-orderable">Valor</th>
		            <th class="no-orderable">Vencimento</th>
		            <th class="no-orderable">Status</th>
		            <th class="no-orderable" width="50">Imprimir</th>
		    	</tr>
			</thead>
			<tbody>
				<?php foreach( $proposta->getParcelas() as $parcela ): ?>
					<?php if( $parcela->valor_atual < $parcela->valor ): ?>
						<?php $statusPgto = $parcela->statusPagamento(); ?>
						<tr>
							<td><?= $parcela->seq ?></td>
							<td><?= number_format($parcela->valor, 2, ',', '.') ?></td>
							<td><?= $util->bd_date_to_view($parcela->vencimento); ?></td>
							<td>
                            	<span class="<?php echo $statusPgto['span_class'] ?>"><?= $statusPgto['mensagem'] ?></span>
							</td>
							<td>
								<form target="_blank" action="/public/segundaVia/" method="POST">
									<button class="btn btn-sm btn-google-plus">
                                    	<i class="clip-note"></i>
                             		</button>
                             		<input type="hidden" name="ParcelasIds[]" value="<?= $parcela->id ?>">
								</form>
							</td>
						</tr>
					<?php endif; ?>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<?php } else if(  isset($postback) && $postback == 1  ) { ?>
	<div class="alert alert-danger">
		<button data-dismiss="alert" class="close">×</button>
		<i class="fa fa-times-circle"></i>
		<strong>Não foi possível encontrar a proposta!</strong> Por favor, verifique se preencheu corretamente os dados acima.
	</div>
<?php } ?>

<style type="text/css">
	#grid_parcelas_length, #grid_parcelas_filter{
		display: none;
	}
	table.table thead .sorting_asc{
		background: none
	}
</style>