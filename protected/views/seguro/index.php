<?php ?>
<div class="row">
    <div class="col-sm-12">

        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="<?php echo Yii::app()->getBaseUrl(true) . '/' . Yii::app()->session['usuario']->getRole()->login_redirect; ?>">
                    Administrador de Filiais
                </a>
            </li>
            <li class="active">
                Relatórios
            </li>
        </ol>
        <div class="page-header">
            <h1>
                Produção
            </h1>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="row">
            <form>
                <div class="col-md-3" style="width:11%">
                    <div class="form-group">
                        <label class="control-label">
                            Data de <span class="symbol required" aria-required="true"></span>
                        </label>                  
                        <input value="" style="width:120px" type="text" name="data_de" class="form-control date-picker" data-date-viewmode="years" data-date-format="dd/mm/yyyy" id="data_de">
                    </div>
                </div>
                <div class="col-md-3" style="width:11%">
                    <div class="form-group">
                        <label class="control-label">Data até:</label>
                        <input value="" style="width:120px" type="text" name="data_ate" class="form-control date-picker" data-date-viewmode="years" data-date-format="dd/mm/yyyy" id="data_ate">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">
                            Filiais <span class="symbol required"></span>
                        </label>
                        <select required="required" multiple="multiple" id="filiais_select" class="form-control multipleselect" name="Filiais[]">
                            <?php foreach (Yii::app()->session['usuario']->adminParceiros as $f) { ?>
                                <option value="<?php echo $f->parceiro->id ?>"><?php echo strtoupper($f->parceiro->getConcat()) ?></option>
                            <?php } ?>
                        </select>  
                    </div>
                </div>
                <!--
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">
                            Crediarista <span class="symbol required"></span>
                        </label>
                        <select required="required" id="crediaristas_select" class="form-control multipleselect" name="Crediaristas[]">
                        
                        </select>  
                    </div>
                </div>
                -->
                <div class="col-md-3" style="width:16%">
                    <div class="form-group">
                        <label class="control-label">&nbsp;</label>
                        <button id="btn-filter" class="btn btn-blue next-step btn-block">Filtrar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="row" style="margin-bottom:50px">
    <div class="col-sm-12">
        <table id="grid_seguros" class="table table-striped table-bordered table-hover table-full-width dataTable">
            <thead>
                <tr>
                    <th width="50" class="no-orderable">Código</th>
                    <th width="300" class="no-orderable">Filial</th>
                    <th width="40" class="no-orderable">Data</th>
                    <th width="150" class="no-orderable">Crediarista</th>
                    <th width="80" class="no-orderable">R$ Solicitado</th>
                    <th width="80" class="no-orderable">R$ Seguro</th>
                    <th width="80" class="no-orderable">R$ Financiado</th>
                </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th id="totalSolicitado" width="80">R$ Solicitado</th>
                    <th id="totalSeguro" width="80">R$ Seguro</th>
                    <th id="totalFinanciado" width="80">R$ Financiado</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>


<style type="text/css">
	.dropdown-menu {
        max-height: 250px;
        overflow-y: auto;
        overflow-x: hidden;
    }
    #grid_seguros_filter, #grid_seguros_length{
        display: none;
    }
    tfoot th {
        font-weight: bold!important;
        color: #5cb85c;
        font-size: 11px;
    }
    td.details-control {
        padding:7px 13px!important;
        background: url('../../images/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.details td.details-control {
        background: url('../../images/details_close.png') no-repeat center center;
    }
</style>