<div class="row">
    
    <div class="col-sm-12" id="divPropostas">
        
        <div class="panel panel-default">
        
            <div class="panel-heading">
                <i class="clip-user-5"></i>
                Propostas
                
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                    </a>
                </div>
                
            </div>
            
            <div class="panel-body">
                
                <table class="table" id="gridPropostas">
                    
                    <thead>
                        
                        <tr>
                            
                            <th>
                                Código
                            </th>
                            
                            <th>
                                Data
                            </th>
                            
                            <th>
                                Filial
                            </th>
                            
                            <th>
                                Cliente
                            </th>
                            
                            <th>
                                CPF
                            </th>
                            
                            <th>
                                Valor
                            </th>
                            
                            <th>
                                Entrada
                            </th>
                            
                            <th>
                                Qtd Parcelas
                            </th>
                            
                            <th>
                                Vlr Parcela
                            </th>
                            
                        </tr>
                        
                    </thead>
                    
                    <tbody>
                    </tbody>
                    
                </table>

            </div>

        </div>

    </div>

    <div class="col-sm-4" id="divTitulos" style="display: none; height: auto;">
        <div class="panel panel-default" style="height: auto;">
            <div class="panel-heading">
                <i class="clip-user-5"></i>
                Títulos / Boletos
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                    </a>
                </div>
            </div>
            <div class="panel-body panel-scroll ps-container" style="height: auto;">
                <table class="table" id="gridParcelas">
                    <thead>
                        <tr>
                            <th>
                                <!--<button class="btn btn-blue" id="btnSelecionaTudo" value="0">
                                    <i class="clip-checkbox-unchecked checkParcela"></i>
                                </button>-->
                            </th>
                            <!--<th>Seq</th>-->
                            <th>Valor</th>
                            <th>Vencimento</th>
                            <th>
                                <button class="btn btn-green"                   id="btnImprimirParcelas"    style="display: none">
                                    <i class="clip-file-pdf"></i>
                                </button>
                                Boleto
                            </th>
                            <th>
                                <button class="btn btn-warning gerarRange"      id="btnGerarRanges"         style="display: none">
                                    <i class="clip-spinner"></i>
                                </button>
                                Gerar
                            </th>
                            <th>
                                <button class="btn btn-warning baixarParcela"   id="btnBaixarParcelas"      style="display: none">
                                    <i class="fa fa-credit-card"></i>
                                </button>
                                Baixar
                            </th>
                            <th>
                                <button class="btn btn-warning estornarBaixa"   id="btnEstornarBaixa"      style="display: none">
                                    <i class="fa fa-history"></i>
                                </button>
                                Estornar
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

            </div>

        </div>

    </div>
    
    <div class="col-sm-3" id="divBaixa" style="display: none; height: auto;">
        
        <div class="panel panel-default" style="height: auto;">
            
            <div class="panel-heading">
                
                <i class="clip-user-5"></i>
                Baixas
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                    </a>
                </div>
            </div>
            
            <div class="panel-body panel-scroll ps-container" style="height: auto;">
                                        
                <form action="/renegociacao/anexarComprovanteBaixa" method="post" id="form_importacao" class="form-horizontal" enctype="multipart/form-data">
                    
                    <input type="hidden" id="idParcelaHdn" value="0" name="idParcelaHdn" />
                    
                    <div class="row">

                        <div class="col-md-6">
                            <input type="date"  
                                   id="dataOcorrencia" 
                                   value="<?php echo date("Y-m-d");?>" 
                                   style="width : 100%" 
                                   max="<?php echo date("Y-m-d");?>"
                                   name="dataOcorrencia"
                                   />
                        </div>

                        <div class="col-md-6">
                            <input type="text"  
                                   id="valorPago"      
                                   value="0"                           
                                   style="width : 100%" 
                                   name="valorPago"
                                   />
                        </div>

                    </div>

                    <br>

                    <div class="row">
                    
                        <div class="row">
                        
                            <div class="col-md-12">
                                <input class="anexo_importacao" name="anexo_importacao" id="anexo_importacao" type="file" class="form-control">
                                <label id="anexo_label" for="anexo_importacao">
                                    <i class="fa fa-refresh" aria-hidden="true"></i> Selecionar Arquivo
                                </label>
                            </div>
                            
                        </div>
                        
                        
                        <div class="row">
                        
                            <div class="col-md-6">

                                <button style="width: 100%; border-radius: 3px" id="submit_arquivo" class="btn btn-success btn-sm" type="submit">
                                    <i class="fa fa-arrow-circle-down" aria-hidden="true"></i> Baixar
                                </button>

                            </div>
                        
                            <div class="col-md-6">

                                <button 
                                        style="width: 100%; border-radius: 3px" 
                                        id="submit_arquivo" 
                                        class="btn btn-warning btnVoltar btn-sm" 
                                        type="submit"
                                        data-div-responsavel="divBaixa"
                                >
                                    <i class="clip-cancel-circle" aria-hidden="true"></i> Voltar
                                </button>

                            </div>
                            
                        </div>
                    
                    </div>
                        
                </form>
                
            </div>
            
        </div>
        
    </div>
    
    <div class="col-sm-3" id="divEstorno" style="display: none; height: auto;">
        
        <div class="panel panel-default" style="height: auto;">
            
            <div class="panel-heading">
                <i class="clip-history"></i>
                Estornar
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                    </a>
                </div>
            </div>
            
            <div class="panel-body panel-scroll ps-container" style="height: auto;">
                    
                <!--<input type="hidden" id="idParcelaHdn" value="0" name="idParcelaHdn" />-->

                <div class="row" style="height: 50px">

                    <div class="col-md-12" style="height: 50px">
                        <input type=""  
                               id="observacao"  
                               style="width : 100%; height: 100%!important" 
                               placeholder="Insira uma observação. Ex.: Baixa estornada devido..."
                               />
                    </div>

                </div>

                <br>

                <div class="row">

                    <div class="col-md-6">

                        <button style="width: 100%; border-radius: 3px" id="btnConfirmaEstorno" disbled class="btn btn-success btn-sm">
                            <i class="fa fa-arrow-circle-down" aria-hidden="true"></i> Confirmar
                        </button>

                    </div>
                        
                    <div class="col-md-6">

                        <button 
                                style="width: 100%; border-radius: 3px" 
                                id="submit_arquivo" 
                                class="btn btn-warning btnVoltar btn-sm" 
                                type="submit"
                                data-div-responsavel="divEstorno"
                                >
                            
                            <i class="clip-cancel-circle" aria-hidden="true"></i> Voltar
                            
                        </button>

                    </div>

                </div>
                
            </div>
            
        </div>
        
    </div>
    
</div>

<div class="row" id="divMensagens" style="display: none">
    
    <div class="row">
        
        <div class="col-sm-12">
            
            <button class="btn btn-orange" id="btnVoltarDivMsg" style="width: 100%;">
                <i class="clip-undo"></i>
                <b>Voltar</b>
            </button>
            
        </div>
        
    </div>
    
    <br>
    
    <div class="row">

        <div class="col-sm-12">

            <table id="gridMensagens" class="table table-striped table-full-width dataTable">

                <thead>

                    <tr>

                        <th width="15%">
                            Data
                        </th>

                        <th width="15%">
                            Status
                        </th>

                        <th width="15%">
                            Usuário
                        </th>

                        <th width="55%">
                            Mensagem
                        </th>

                    </tr>

                </thead>

            </table>

        </div>
        
    </div>
    
</div>

<style>
    
    input#anexo_importacao 
    {
        position: absolute;
        filter: alpha(opacity=0);
        opacity: 0;
    }
    input#anexo_importacao + label 
    {
        border: 1px solid #CCC;
        border-radius: 3px;
        text-align: left;
        padding: 6px;
        width: 100%;
        height: 30px;
        left: 0;
        position: relative;
        text-align: center;
        background: #ffbd4a;
        color: #fff;
        border: none;
        cursor: pointer;
    }
</style>