<div class="col-md-12">
    <div class="row">
        <div class="col-md-12" style="text-align: center!important;">
            <h2 style="color: #a9a9a9;">Negociaçao de Contratos</h2>
        </div>
        <hr />
    </div>
    <div class="row">
        <div class="col-md-4">
            <input id="cpf_cliente" class="form-control input-lg inp" placeholder="CPF do Cliente">
        </div>
        <div class="col-md-4">
            <button id="btn_pesquisar" class="btn btn-lg btn-info">Pesquisar</button>
        </div>
    </div>
    <br /><br />
    <div class="row">
        <div class="col-md-12">
            <table class="table" id="grid_contratos">
                <thead>
                    <th>Código</th>
                    <th>Cliente</th>
                    <th>Parcelas</th>
                    <th>Negociar</th>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
    </div>
</div>
<style type="text/css">
    #grid_contratos_length,
    #grid_contratos_info,
    #grid_contratos_filter,
    #grid_contratos_paginate{
        display: none;
    }
    #grid_contratos{
        padding-bottom: 25px;
    }
</style>