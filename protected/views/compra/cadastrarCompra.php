<?php ?>
<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/compra/CadastrarCompra">
                    Compra
                </a>
            </li>
            <li class="active">
                Cadastrar
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <form id="form-add-compra" action="/compra/cadastrarCompra/" method="post">
        <div class="modal-body">
            <div class="row">
                <div class="row-centralize">
                    <div class="col-md-2">
                        <label class="control-label">
                            Data <span class="symbol required"></span>
                        </label>
                        <input id="Compra_Data" 
                               type="date" 
                               value="<?php echo date('d/m/Y') ?>" 
                               max="<?php echo date('d/m/Y') ?>" 
                               required 
                               name="Compra[data]" 
                               class="form-control form-compra">
                    </div>
                    <div class="col-md-10">
                        <label class="control-label">
                            Fornecedor 
                            <span class="symbol required"></span>
                        </label>
                        <select required="required" 
                                name="Compra[Fornecedor_id]" 
                                id="Compra_Fornecedor_id" 
                                class="form-control search-select select2 form-compra">
                            
                            <option value="0">Selecione:</option>
                            <?php foreach (Fornecedor::model()->findAll() as $fornecedor) { ?>
                                <option value="<?php echo $fornecedor->id ?>"><?php echo $fornecedor->pessoa->nome ?></option>
                            <?php } ?>                              
                        </select>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="row-centralize">
                    <div class="col-md-2">
                        <label class="control-label">Comprador <span class="symbol required"></span></label>
                        
                        <select required="required" 
                                id="Compra_Comprador_id" 
                                class="form-control search-select select2 form-compra">
                            
                            <option value="0">Selecione:</option>
                            <option value="1">Teste 1</option>
                            <option value="2">Teste 2</option>
                        </select>
                    </div>
                    <div class="col-md-10">
                       <div class="form-group">
                            <label class="control-label">Filial:</label>
                            <select id="Filial_id"
                                    name="Filial_id" 
                                    class="form-control search-select select2 form-compra"
                                    required
                                    >
                                <option value="0">Selecione:</option>
                                <?php foreach( Filial::model()->findAll() as $Filial ){ ?>

                                    <option value="<?php echo $Filial->id ?>"><?php echo $Filial->nome_fantasia ?> - <?php echo $Filial->getEndereco()->cidade ?>/ <?php echo $Filial->getEndereco()->uf ?></option>

                                <?php } ?>
                            </select>
                       </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="row-centralize">
                    <div class="col-md-3">
                        <label class="control-label">Valor Mercadoria</label>
                        <input id="valMer" value="0" readonly="true" class="form-control">
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">Valor Descontos</label>
                        <input id="valDes" value="0" readonly="true" class="form-control">
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">Valor Acrescentado</label>
                        <input id="valAcr" value="0" readonly="true" class="form-control">
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">Valor a Pagar</label>
                        <input id="valPag" value="0" readonly="true" class="form-control">
                    </div>
                    <div class="col-md-3">
                        <label class="control-label">Valor Final</label>
                        <input id="valFin" value="0" readonly="true" class="form-control">
                    </div>
                    <input type="hidden" id="valMerc" >
                </div>
            </div>
        </div>
    </form>
</div>
<div class="row">
    <br>
</div>
<div class="row">
    <form id="form-cmp-it">
        <div class="col-sm-12">
            <table id="grid_Compra_Item_da_Compra" hidden="true" class="table table-striped table-bordered table-hover table-full-width dataTable">
                <thead>
                    <tr>
                        <th>Item</th>
                        <th>Produto</th>
                        <th>Estoque</th>
                        <th>Quantidade</th>
                        <th>Preço de Compra</th>
                        <th>% Desconto</th>
                        <th>Preço de Total</th>
                        <td style="width:10px;">
                            <a style="padding:1px 5px; font-size:12px;" id="btn-modalItCompra-show" href="#modal_new_Compra_Item_da_Compra" data-toggle="modal" type="submit" class="btn btn-green">+</a>
                        </td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </form>
</div>
<div class="row">
    <br>
</div>
<div class="row">
    <div class="col-sm-12">
        <table id="grid_Compra_CP" hidden="true" class="table table-striped table-bordered table-hover table-full-width dataTable">
            <thead>
                <tr>
                    <th>Forma de Pagamento</th>
                    <th>Parcelas</th>
                    <th>Valor</th>
                    <td style="width:10px;">
                        <a style="padding:1px 5px; font-size:12px;" id="btn-modalPgCompra-show" data-toggle="modal" type="submit" class="btn btn-green">+</a>
                    </td>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <br>
    <br>
</div>
<div class="row">
    <div id="div-btn-salvar" hidden="true" class="col-md-1">
        <button id="btn-salvar-compra" type="submit" class="btn btn-blue">Salvar</button>
    </div>
</div>
<div id="modal_new_Compra_CP" class="modal fade" tabindex="-1" data-width="480" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Inserir Forma de Pagamento</h4>
    </div>
    <form id="form-add-FP">
        <div class="modal-body">
            <div class="row-centralize">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="control-label">
                            Forma de Pagamento <span class="symbol required"></span>
                        </label>
                        <input required="true" name="FP[id]" placeholder="Selecione" type="hidden" id="FP_id" class="form-control search-select">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="control-label">
                                    Qtd de Parcelas <span class="symbol required"></span>
                                </label>
                                <input required name="CP[parcelas]" id="parcelas" class="form-control" type="number">
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="control-label">
                                    Valor <span class="symbol required"></span>
                                </label>
                                <input required name="CP[valorFP]" id="valorFP" class="form-control" type="number">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-red">Cancelar</button>
            <button type="submit" id="btn-add-fp" class="btn btn-blue">Adicionar</button>
            <div class="row">
            </div>
            <br>
        </div>
    </form>
</div>

<div id="modal_new_Compra_Item_da_Compra" class="modal fade" tabindex="-1" data-width="720" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Inserir item</h4>
    </div>
    <form id="form-add-item">
        <div class="modal-body">
            <div class="row">
                <div class="row-centralize">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label class="control-label">
                                        Produto <span class="symbol required"></span>
                                    </label>
                                    <select class="form-control search-select select2" name="Item_da_Compra[Item_da_Compra_id]" id="Item_da_Compra_id">
                                        <option value="">Selecione: </option>
                                        <?php foreach (Produto::model()->findAll() as $item) { ?>
                                            <option value="<?php echo $item->id ?>"><?php echo $item->descricao ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">
                                        Estoque <span class="symbol required"></span>
                                    </label>
                                    
                                    <input required="true" 
                                           name="Item_da_Compra[Estoque_id]" 
                                           placeholder="Selecione" 
                                           type="hidden" 
                                           id="Estoque_id" 
                                           class="form-control search-select">
                                    
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">
                                        Quantidade <span class="symbol required"></span>
                                    </label>
                                    <input required name="Item_da_Compra[quantidade]" id="quantidade" class="form-control" type="number">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">
                                        Preço <span class="symbol required"></span>
                                    </label>
                                    <input required name="Item_da_Compra[precoTabela]" id="preco_compra" class="form-control moeda" type="number">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">
                                        % Desconto <span class="symbol required"></span>
                                    </label>
                                    <input required name="Item_da_Compra[desconto]" id="desconto" class="form-control" type="number">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">
                                        Preço Total
                                    </label>
                                    <input required name="Item_da_Compra[precoCompra]" value="0" disabled="true" id="precoTotal" class="form-control moeda" type="number">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-red">Cancelar</button>
            <button type="submit" id="btn-add-item" class="btn btn-blue">Adicionar</button>
            <div class="row">
            </div>
            <br>
        </div>
        
    </form>
</div>
<style>

</style>