<?php ?>
<div class="row">
   <div class="col-sm-12">
      <!-- start: PAGE TITLE & BREADCRUMB -->
      <ol class="breadcrumb">
         <li><a href="#">Estoque</a></li>
         <li class="active">Inventariar estoque</li>
      </ol>
      <div class="page-header">
	      <h1 style="font-size:33px;">Inventariar estoque <?php echo '<span style="color:#007AFF">'. $estoque->local->nome .'</span>,' ?> filial <?php echo '<span style="color:#007AFF">'. Yii::app()->session['usuario']->returnFilial()->getEndereco()->cidade .'</span>'; ?></h1>
      </div>
   </div>
</div>
<div class="row">

	<div class="col-sm-12">
		<div class="page-header no-border-bottom no-padding-bottom">
			<h3>Itens do estoque</h3>
		</div>
		<table id="table_id" class="table table-striped table-bordered table-hover table-full-width dataTable">
		    
		    <thead>
		    	<tr>
               		<th>Descrição</th>
               		<th>Saldo</th>
		        </tr>
		    </thead>

		    <tbody>

		    	<?php foreach( $estoque->listItensEstoque() as $itemEstoque ){ ?>
		    		<tr>
		    			<td><?php echo $itemEstoque->produto->descricao ?></td>
		    			<td><?php echo $itemEstoque->saldo ?></td>
		    		</tr>
		    	<?php } ?>
		            
			</tbody>

		</table>
	</div>

</div>