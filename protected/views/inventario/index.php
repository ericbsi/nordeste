<div class="row">
   <div class="col-sm-12">
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/estoque/">
            Início
            </a>
         </li>
         <li class="active">
            Inventário
         </li>
      </ol>
   </div>
</div>
<div id="modal_select_filial" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;" data-width="700">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
      &times;
      </button>
      <h4 class="modal-title">Selecione uma filial:</h4>
   </div>
   <form id="form-add-email" method="POST" action="/inventario/inventariarEstoque/">
      <div class="modal-body">
         <div class="row">
            <div class="row-centralize">
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label class="control-label">Filiais:</label>
                           <select name="Filial_id" class="form-control">
                              <?php foreach( $Filiais as $Filial ){ ?>

                                 <?php if( count( $Filial->listEstoques() ) > 0 ){ ?>

                                    <option value="<?php echo $Filial->id ?>"><?php echo $Filial->nome_fantasia ?> - <?php echo $Filial->getEndereco()->cidade ?>/ <?php echo $Filial->getEndereco()->uf ?></option>

                                 <?php } ?>

                              <?php } ?>
                           </select>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal-footer">         
         <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
         <button type="submit" class="btn btn-blue">Prosseguir</button>
         <div class="row">
         </div>
         <br>
      </div>
   </form>     
</div>
<script type="text/javascript">
   
   $(function(){

      $('#modal_select_filial').modal('show');

   })

</script>