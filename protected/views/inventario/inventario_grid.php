<div class="row">
   <div class="col-sm-12">
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">Estoque</a>
         </li>
         <li class="active">
            Inventariar estoque 
         </li>
      </ol>
      <div class="page-header">
         <h1>Estoques /<small> <?php echo $Filial->nome_fantasia ?> - <?php echo $Filial->getEndereco()->cidade ?> / <?php echo $Filial->getEndereco()->uf ?></small></h1>
      </div>
   </div>
</div>
<div class="row">
	<div class="col-sm-6">
    <form id="form-pass-estoque" method="POST" action="/inventario/inventariarEstoque/">
      <input name="Filial_id" type="hidden" value="<?php echo $Filial->id?>">
      <label>Selecione um estoque:</label>
  		<p>
  		   <select name="Estoque_id" class="form-control search-select" name="select_estoques_name" id="select_estoques_id">
            <option value="0">Selecione:</option>
  		   		<?php foreach( $Filial->listEstoques() as $estoque ) { ?>
  		   			<option <?php if($estoqueSel != NULL && $estoqueSel->id == $estoque->id ){ echo " selected "; } ?> value="<?php echo $estoque->id ?>">
  		   				<?php echo $estoque->local->nome ?>
  		   			</option>
  		   		<?php } ?>
  		   </select>
  		</p>
    </form>
	</div>
</div>

<div class="row">
	<p></p>
</div>

<input id="nome_estoque" value="<?php if( $estoqueSel != null ){ echo $estoqueSel->local->nome; } ?>" type="hidden">

<div class="row">
    <div class="col-sm-12">
        <table id="grid_itens_estoque" class="table table-striped table-bordered table-hover table-full-width dataTable">
            <thead>
              <tr>
                <th></th>
                <th style="width:500px;">Produto</th>
                <th style="width:120px;">Qtd em estoque</th>
                <th style="width:120px;">Qtd inventário</th>
                <th style="width:120px;">Diferença</th>
                <th style="width:240px;">Estoque</th>
                <th></th>
              </tr>
            </thead>
                 
            <tbody>
            <?php if( isset( $itensEstoque ) && $itensEstoque != NULL ){ ?>
              <?php foreach( $itensEstoque as $item ) {?>
                <tr id="<?php echo $item->produto->id ?>">
                  <td></td>
                  <td><?php echo $item->produto->descricao ?></td>
                  <td id="qtd_atual_item_<?php echo $item->produto->id?>_td"><?php echo $item->saldo ?></td>
                  <td id="qtd_inventariada_item_<?php echo $item->produto->id?>_td"><?php echo $item->saldo ?></td>
                  <td class="editable" id="qtd_diff_item_<?php echo $item->produto->id?>_td">0</td>
                  <td><?php echo $item->estoque->local->nome ?></td>
                  <td>
                    <a id="btn_item_<?php echo $item->produto->id ?>" data-item-td-qtd-diff="#qtd_diff_item_<?php echo $item->produto->id?>_td" data-item-td-qtd-inventariada="#qtd_inventariada_item_<?php echo $item->produto->id?>_td" data-item-td-qtd-atual="#qtd_atual_item_<?php echo $item->produto->id?>_td" data-item-id-input-id="#item_<?php echo $item->produto->id?>_id" data-item-qtd-input-id="#item_<?php echo $item->produto->id?>_qtd" class="btn btn-primary btn-add-item-ao-inventario" style="padding:2px 5px!important; font-size:11px!important;"><i class="clip-plus-circle"></i></a>
                    <a disabled href="#" class="btn btn-red" style="padding:2px 5px!important; font-size:11px!important;"><i class=" clip-close"></i></a>
                  </td>
                </tr>
              <?php } ?>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>


<div id="modal_edit_item_inventario" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;" data-width="500">
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
        &times;
        </button>
        <h4 class="modal-title">Inventariar item</h4>
     </div>
      <div class="modal-body">
         <div class="row">
            <div class="row-centralize">
               <div class="col-md-8">
                  <div class="row">
                     <div class="col-md-10">
                        <div class="form-group">
                           	<label class="control-label">Quantidade:</label>
                            <input required="required" class="form-control" id="qtd_novo_item_inventario" type="text">
                            <input id="td_item_qtd_atual" type="hidden">
                            <input id="td_item_qtd_inventariada" type="hidden">
                            <input id="td_item_qtd_diff" type="hidden">

                            <input id="input_hidden_diferenca_id" type="hidden">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
        <button id="changeItem" class="btn btn-blue">Adicionar</button>
        <div class="row">
        </div>
        <br>
      </div>
</div>

<div class="row">
  <div class="col-sm-12">
    <form id="formulario-itens-do-inventario" method="POST" action="/inventario/add/">
      
      <?php if( isset( $itensEstoque ) ){ ?>

        <?php foreach( $itensEstoque as $item ){ ?>
          <input type="hidden" value="<?php echo $item->produto->id ?>"    id="item_<?php echo $item->produto->id?>_id"   name="itens_estoque_ids[]">
          <input type="hidden" value="<?php echo $item->saldo ?>" id="item_<?php echo $item->produto->id?>_qtd"  name="itens_estoque_qtds[]">
          <input type="hidden" value="<?php echo $item->produto->id ?>" id="item_<?php echo $item->produto->id?>_no_inventario"  name="ids_produtos_em_inventario[]">
        <?php } ?>

      <?php } ?>
      <p>
        <button class="btn btn-success" type="submit" id="btn-concluir-inventario">
          Concluir inventário
        </button>
        <a id="" class="btn btn-success" href="#modal_add_produto" data-toggle="modal">Adicionar produto</a>
      </p>
    </form>
  </div>
</div>

<div id="modal_add_produto" class="modal fade" tabindex="-1" data-width="700" style="display: none;">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
      &times;
      </button>
      <h4 class="modal-title">Adicionar Produto ao inventário</h4>
   </div>   
   <form id="form-add-produto">
      <div class="modal-body">
         <div class="row">
            <div class="row-centralize">
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label class="control-label">
                              Produto <span class="symbol required"></span>
                           </label>
                           <input data-show-no-father="0" required="true" value="" required="required" name="Produtos" placeholder="Selecione um produto" type="hidden" id="selectProdutos" class="form-control search-select">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label class="control-label">Quantidade <span class="symbol required"></span></label>
                           <input required="required" class="form-control" type="text" name="qtd_new_product" id="qtd_new_product">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal-footer">
          <label class="checkbox-inline" style="float:left">
              <input id="checkbox_continuar" type="checkbox" value="">
              Continuar Cadastrando
          </label>
         <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
         <button id="btn-add-novo-produto-no-inventario" class="btn btn-blue">Salvar</button>
      </div>
   </form>
</div>

<div id="modal_confirm" class="modal fade" tabindex="-1" data-width="400" style="display: none;">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
      &times;
      </button>
      <h4 class="modal-title">Confirmar</h4>
   </div>   
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            <p>
                Deseja inventariar este(s) <span id="span_qtd_itens"></span> item(s)?
            </p>  
          </div>
        </div>
      </div>
      <div class="modal-footer">
         <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
         <button id="btn-confirmar-inventario" class="btn btn-blue">Concluir</button>
      </div>
</div>

<style type="text/css">
	#grid_itens_estoque_length, #example_filter, #example_length{
	 display: none;	
	}
  td.highlight {
    color: #3D8F3D;
  }
  #grid_itens_estoque_filter{
    float: left;
    text-align: left!important;    
  }
  #grid_itens_estoque_filter input{ 
    float: left;
    margin-left: 0!important;
    margin-top: 5px!important;   
    margin-bottom: 15px!important;
  }
</style>