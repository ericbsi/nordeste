<!--<select id="multiselect" multiple="multiple">
  <option value="cheese">Cheese</option>
  <option value="tomatoes">Tomatoes</option>
  <option value="mozarella">Mozzarella</option>
  <option value="mushrooms">Mushrooms</option>
  <option value="pepperoni">Pepperoni</option>
  <option value="onions">Onions</option>
</select>
 <script type="text/javascript">
  /*
  $(document).ready(function() {

    $('#multiselect').multiselect({
        onChange: function(element, checked) {
          alert(element[0]['value']);
        }
    });

  });*/

</script>-->
<!--<script type="text/javascript">
	$(document).ready(function(){
		$('#sample_1').dataTable({
			"aoColumnDefs": [{
                "aTargets": [0]
            }],
            "oLanguage": {
                "sLengthMenu": "Show _MENU_ Rows",
                "sSearch": "",
                "oPaginate": {
                    "sPrevious": "",
                    "sNext": ""
                }
            },
            "aaSorting": [
                [1, 'asc']
            ],
            "aLengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "iDisplayLength": 10,
		});
	})
</script>
-->

<?php
	//$paises = Pais::model()->findAll();
?>
<!--<table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
	<thead>
		<tr>
			<th>Id</th>
			<th class="hidden-xs">Nome</th>			
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>-->
<?php
  /*$hash = CPasswordHelper::hashPassword("123456");

  if (CPasswordHelper::verifyPassword("123456", $hash))
    echo "Bom";
  else
    echo "Ruim";
  */
?>

<?php if( Yii::app()->session['usuario']->primeira_senha){ ?>

<div id="static" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
   <div class="modal-body">
      <p>
       O sistema detectou que você ainda não alterou sua senha após o cadastro (ou após solicitar uma nova senha). 
       Por medidas de segurança, solicitamos que você altere a sua senha.
      </p>
   </div>
   <div class="modal-footer">
      <form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/usuario/changePassword">
         <button type="submit"  class="btn btn-primary">
            Mudar senha
         </button>
      </form>
   </div>      
</div>

<?php } ?>