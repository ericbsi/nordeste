<?php ?>
<div class="row">
   <div class="col-sm-12">
      <!-- start: PAGE TITLE & BREADCRUMB -->
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
            Configurações
            </a>
         </li>
         <li class="active">
            Trocar senha
         </li>         
      </ol>
      <div class="page-header">
         <h1>Troca de senha / <small><?php echo Yii::app()->session['usuario']->nome_utilizador ?></small></h1>
      </div>
      <!-- end: PAGE TITLE & BREADCRUMB -->
   </div>
</div>

<div class="alert alert-warning">
   <i class="fa fa-exclamation-triangle"></i>
   <strong>Aviso!</strong> O nosso sistema detectou que você ainda não modificou a sua primeira senha. Por motivos de segurança, pedimos que você escolha uma nova senha, com no mínimo seis caracteres e diferente da atual.
</div>

<div class="row">
   <div class="col-md-12">
      <!-- start: FORM VALIDATION 1 PANEL -->
      <div class="panel panel-default">         
         <div class="panel-body">
            
            <form role="form" id="form" method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/dashboard/changePassword">
               <div class="row">
                  <div class="col-md-12">

                     <?php if( !$retorno['empty'] ) {?>
                       <div class="errorHandler alert <?php echo $retorno['alert_class'] ?>">
                          <?php for ($i = 0; $i < count( $retorno['messages'] ); $i++) { ?>
                            <p> * <?php echo $retorno['messages'][$i] ?> </p>
                          <?php } ?>
                       </div>
                     <?php } ?>

                     <!--<div class="successHandler alert alert-success no-display">
                        <i class="fa fa-ok"></i> Your form validation is successful!
                     </div>-->

                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label class="control-label">
                        Senha atual <span class="symbol required"></span>
                        </label>
                        <input type="password" class="form-control" name="old_password" id="old_password">
                     </div>
                     <div class="form-group">
                        <label class="control-label">
                        Nova senha <span class="symbol required"></span>
                        </label>
                        <input type="password" class="form-control" name="new_password" id="new_password">
                     </div>
                     <div class="form-group">
                        <label class="control-label">
                        Repita a nova senha <span class="symbol required"></span>
                        </label>
                        <input type="password" class="form-control" id="password_again" name="password_again">
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-4">
                     <button class="btn btn-yellow btn-block" type="submit">
                     Atualizar <i class="fa fa-arrow-circle-right"></i>
                     </button>
                  </div>
               </div>
               <br>
               <div class="row">
                  <div class="col-md-12">
                    <div>
                      <span class="symbol required"></span> Campos obrigatórios
                      <hr>
                    </div>
                  </div>
                </div>
            </form>
         </div>
      </div>
      <!-- end: FORM VALIDATION 1 PANEL -->
   </div>
</div>