<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/naturezaTitulo/">
                    Natureza
                </a>
            </li>
            <li class="active">
                Administrar
            </li>
        </ol>
    </div>
</div>

<p></p>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">

            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i>
                Nova Natureza
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse expand" href="#">
                    </a>
                </div>
            </div>

            <div class="panel-body collapse">
                <div class="tabbable">
                    <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="painelNatureza">
                        <li id="liDados" class="active">
                            <a data-toggle="tab" href="#painel_dados">
                                Visão Geral
                            </a>
                        </li>
                        <li>
                            <buton id="btnAddNatureza" class="btn btn-green" disabled>
                                <i class="fa fa-save"></i> Salvar
                            </buton>
                        </li>
                    </ul>
                    
                    <div class="tab-content">
                        <div id="painel_dados" class="tab-pane in active">
                            <div class="row">
                                <div class="col-md-12">
                                    
                                    <div class="form-group">
                                        <div class="col-sm-5">
                                            <label class="control-label">
                                                Natureza Pai : 
                                                <span class="symbol required"></span>
                                            </label>
                                            <select 
                                                id="paiNatureza"
                                                class="form form-control search-select select2 ufSelect">
                                                <option value="0">
                                                    Selecione...
                                                </option>
                                                <?php foreach (NaturezaTitulo::model()->findAll('habilitado') as $nt) { ?>
                                                    <option value="<?php echo $nt->id; ?>">
                                                        <?php echo $nt->codigo . ' - '. $nt->descricao; ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                            <span for="paiNatureza" class="help-block valid"></span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="col-sm-2">
                                            <label class="control-label">
                                                Código : 
                                                <span class="symbol required"></span>
                                            </label>
                                            <input 
                                                required
                                                type="text" 
                                                placeholder="Código..." 
                                                id="codigoNatureza" 
                                                class="form form-control" />
                                            <span for="codigoNatureza" class="help-block valid"></span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-5">
                                            <label class="control-label">
                                                Descrição : 
                                                <span class="symbol required" aria-required="true"></span>
                                            </label>
                                            <input 
                                                required
                                                type="text" 
                                                placeholder="Descrição..." 
                                                id="descricaoNatureza" 
                                                class="form form-control" />
                                            <span for="descricaoNatureza" class="help-block valid"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">

            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i>
                Naturezas
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                    </a>
                </div>
            </div>

            <div class="panel-body">
                <table id="grid_naturezas" class="table table-striped table-bordered table-hover table-full-width dataTable">
                    <thead>
                        <tr>
                            <th>
                                <select class="select select2 search-select filtroNatureza" id="filtroPai">
                                    <option value="0">
                                        Filtre Pai...
                                    </option>
                                    <?php foreach (NaturezaTitulo::model()->findAll('habilitado') as $nt) { ?>
                                        <option value="<?php echo $nt->id; ?>">
                                            <?php echo $nt->codigo . ' - ' . $nt->descricao; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </th>
                            <th>
                                <input 
                                    type="text" 
                                    placeholder="Filtre Código..." 
                                    id="filtroCodigo" 
                                    class="form form-control filtroNatureza" />
                            </th>
                            <th>
                                <input 
                                    type="text" 
                                    placeholder="Filtre Descrição..." 
                                    id="filtroDescricao" 
                                    class="form form-control filtroNatureza" />
                            </th>
                            <th width="3%">
                            </th>
                        </tr>
                        <tr>
                            <th class="no-orderable">Natureza Pai</th>
                            <th class="no-orderable">Código</th>
                            <th class="no-orderable">Descrição</th>
                            <th class="no-orderable"></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<br>
<br>

<style type="text/css">
    #grid_naturezas_filter, #grid_naturezas_length{
        display: none;
    }
</style>