<?php  ?>
<div class="row">
   <div class="col-sm-12">
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
            Cobranças
            </a>
         </li>
         <li class="active">
            Auditorias
         </li>
      </ol>
      <div class="page-header">
         <h1>
            Auditorias - Parcelas atrasadas
         </h1>
      </div>
   </div>
</div>

<div class="row" style="margin-bottom:50px">
   <div class="col-sm-12">
      <table id="grid_parcelas_auditadas" class="table table-striped table-bordered table-hover table-full-width dataTable">
         <thead>
            <tr>
               <th width="15" class="no-orderable">Data Criação</th>
               <th width="20" class="no-orderable">Criado por</th>
               <th width="60" class="no-orderable">Cód. Proposta</th>
               <th width="150" class="no-orderable">Cliente</th>
               <th width="10" class="no-orderable">CPF</th>
               <th width="100" class="no-orderable">Status</th>
               <th width="20" class="no-orderable">Mais detalhes</th>
            </tr>
         </thead>
         <tbody></tbody>
         <!--
         <tfoot>
            <tr>
               <th></th>
               <th></th>
            </tr>
         </tfoot>
         -->
      </table>
   </div>
</div>

<style type="text/css">
   #grid_parcelas_auditadas_filter, #grid_parcelas_auditadas_length{
      display: none
   }
</style>