<?php 
   $util = new Util;
?>
<div class="row">
   <div class="col-sm-12">
      
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="/cobranca/">
               Cobranças
            </a>
         </li>
         <li class="active">
            Parcela
         </li>
      </ol>
      <div class="page-header">
         <h1 style="font-size:20px;font-weight: bold;">
         	AUDITORIA: <?php echo $auditoria->Entidade_Relacionamento ?>
         </h1>
         <h1 style="font-size:20px;font-weight: bold;">
            Criado por:  <?php echo strtoupper( $auditoria->agente->nome_utilizador ) ?>
         </h1>
         <h1 style="font-size:20px;font-weight: bold;">
            Data criação:  <?php echo $util->bd_date_to_view( substr($auditoria->data_criacao, 0,10) ) ?>
         </h1>
         <h1 style="font-size:20px;font-weight: bold;">
            Observação Inicial:  <?php echo strtoupper( $auditoria->Observacao ) ?>
         </h1>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-sm-12">
      <p>
        <a data-toggle="modal" href="#modal_form_finalizar_auditoria" id="btn-finalizar-auditoria" class="btn btn-facebook"> <i class="clip-checkmark"></i> Finalizar auditoria</a>
      </p>
   </div>
</div>
<div class="row">
   <div class="col-sm-12">
      <p>
         <form style="display:inline" action="/auditoria/parcelas/" method="POST">
            <button type="submit" class="btn btn-success">Voltar <i class="fa fa-backward"></i></button>
         </form>
         <a id="btn_modal_form_new_obs" data-toggle="modal" href="#modal_form_new_obs" class="btn btn-success">Adicionar observação <i class="fa fa-plus"></i></a>
      </p>
   </div>
</div>
<h1 style="font-size:20px;font-weight: bold;">
   OBSERVAÇÕES
</h1>
<div class="row">
   <div class="col-sm-12">
      <table id="grid_observacoes" class="table table-striped table-bordered table-hover table-full-width dataTable">
         <thead>
            <tr>
               <th style="width:50px" class="no-orderable">Escrita por:</th>
               <th style="width:50px" class="no-orderable">Data de criação:</th>
               <th style="width:300px" class="no-orderable">Conteúdo:</th>
            </tr>            
         </thead>
         <tbody>
            
         </tbody>
      </table>
   </div>
</div>

<h1 style="font-size:20px;font-weight: bold;">
   ANEXOS
</h1>
<div class="row">
   <div class="col-sm-12">
      <table id="grid_anexos" class="table table-striped table-bordered table-hover table-full-width dataTable">
         <thead>
            <tr>
               <th class="no-orderable">Descrição:</th>
               <th style="width:50px" class="no-orderable">Abrir:</th>
            </tr>            
         </thead>
         <tbody>
            
         </tbody>
      </table>
   </div>
</div>
<div class="row">
   <div class="col-sm-12">
      <p>
         <a id="btn_modal_form_new_att" data-toggle="modal" href="#modal_form_new_att" class="btn btn-success">Anexar documento <i class="fa fa-plus"></i></a>
      </p>
   </div>
</div>

<div id="modal_form_new_att" class="modal fade" tabindex="-1" data-width="560" style="display: none;">
     
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
        &times;
        </button>
        <h4 class="modal-title">Anexar documento</h4>
     </div>

     <form action="/auditoria/anexar/" method="POST" enctype="multipart/form-data" id="form-add-att">
        <div class="modal-body">
           <div class="row">
              <div class="col-md-12">
                    <div class="row">
                       <div class="col-md-12">
                           <div class="form-group">
                              <label class="control-label">Descrição:</label>
                              <input id=""  type="text" name="Anexo[descricao]" class="form-control">
                           </div>
                       </div>
                    </div>
              </div>
           </div>
           <div class="row">
              <div class="row-centralize">
                 <div class="col-md-12">
                    <div class="row">
                       <div class="col-md-12">
                          <div class="form-group">
                             <label class="control-label">Arquivo:</label>
                             <input required="required" data-icon="false" class="control-label filestyle" name="ComprovanteFile" id="ComprovanteFile" type="file" />
                             <input type="hidden" name="Auditoria_id" value="<?php echo $auditoria->id ?>">
                          </div>
                       </div>
                    </div>
                 </div>
              </div>
           </div>
        </div>
        <div class="modal-footer">
           <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
           <button type="submit" class="btn btn-blue btn-send">Salvar</button>
        </div>
     </form>
</div>

<div id="modal_form_new_obs" class="modal fade" tabindex="-1" data-width="560" style="display: none;">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
      &times;
      </button>
      <h4 class="modal-title">Adicionar observação</h4>
   </div>
   <form id="form-add-obs">
      <div class="modal-body">
         <div class="row">
            <div class="row-centralize">
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label class="control-label">Observacao:</label>
                           <textarea id="textarea_observacao" name="Observacao[conteudo]" required="required" maxlength="300" class="form-control limited" style="width:518px!important;height:116px;"></textarea>
                           <input id="Auditoria_id" type="hidden" name="Auditoria_id" value="<?php echo $auditoria->id ?>">

                           <!--Conteudo Mensagem-->
                           <input type="hidden" value="<?php echo date('Y-m-d'); ?>" name="Observacao[data_criacao]">
                           <input type="hidden" value="<?php echo Yii::app()->session['usuario']->id ?>" name="Observacao[Usuario_id]">

                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal-footer">
         
         <div style="background:transparent;border:none;" class="panel"></div>
         <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
         <button type="submit" class="btn btn-blue btn-send">Salvar</button>
      </div>
   </form>
</div>

<div id="modal_form_finalizar_auditoria" class="modal fade" tabindex="-1" data-width="560" style="display: none;">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
      &times;
      </button>
      <h4 class="modal-title">Deseja finalizar esta auditoria?</h4>
   </div>
   <form id="form-finalizar-audit" action="/auditoria/finalizar/" method="POST">
      <div class="modal-body">
         <div class="row">
            <div class="row-centralize">
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <!--Conteudo Mensagem-->
                           <input type="hidden" value="<?php echo $auditoria->id ?>" name="Auditoria[id]">
                           <input type="hidden" value="<?php echo Yii::app()->session['usuario']->id ?>" name="Auditoria[Finalizado_por]">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal-footer">
         <div style="background:transparent;border:none;" class="panel"></div>
         <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
         <button type="submit" class="btn btn-blue btn-send">Finalizar</button>
      </div>
   </form>
</div>

<style type="text/css">
	#grid_observacoes_length, #grid_observacoes_filter,
   #grid_anexos_filter, #grid_anexos_length
   {
		display: none;
	}
	td.details-control {
		padding:7px 13px!important;
	    background: url('../../images/details_open.png') no-repeat center center;
	    cursor: pointer;
	}
	tr.details td.details-control {
	    background: url('../../images/details_close.png') no-repeat center center;
	}
</style>
