<div class="row"  id="initial_place">
   <div class="col-sm-12">
      <ol class="breadcrumb">
         <li><a href="#">Saque Fácil</a></li>
         <li class="active"></li>
      </ol>
      <div class="page-header">
         <h1>Saque Fácil - Credshow</h1>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-sm-12">
      <div class="tabbable tabs-left">
         <ul id="tabForm" class="nav nav-tabs tab-green">
            <li class="active" id="li-aba-detalhes-proposta">
               <a href="#tab4_detalhes_proposta" data-toggle="tab" data-form-bind="valor_da_compra">
               <i class="fa fa-keyboard-o"></i> Dados da Proposta
               </a>
            </li>
            <li class="" id="li-aba-dados-cliente">
               <a href="#tab4_dados_do_cliente" data-toggle="tab" data-form-bind="cliente_nome">
               <i class="pink fa fa-user"></i> Dados do Cliente
               </a>
            </li>
            <li class="" id="li-aba-dados-cartao">
               <a href="#tab4_dados_do_cartao" data-toggle="tab" data-form-bind="cartao_numero">
               <i class="pink fa fa-credit-card"></i> Dados do Cartão
               </a>
            </li>
            
            <li class="" id="li-aba-dados-bancarios">
               <a href="#tab4_dados_bancarios" data-toggle="tab" data-form-bind="cartao_numero">
               <i class="pink fa fa-credit-card"></i> Dados bancários
               </a>
            </li>
            
         </ul>
         <div class="tab-content">
            <div class="tab-pane active" id="tab4_detalhes_proposta">
               <div class="col-md-12">
                  <form action="#" id="form-proposta">
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-4">
                                 <label class="control-label">Valor Solicitado <span class="symbol required"></span></label>
                                 <input required="true" placeholder="Valor solicitado" id="valor_solicitado" class="form-control dinheiro currency2 triggerSimularInput" type="text" name="Saque[valorSolicitado]">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-4">
                                 <label class="control-label">Parcelamento : <span class="symbol required"></span></label>
                                 <input required="true" name="Saque[qtdParcelas]" placeholder="Selecione" type="hidden" id="selectParcelamento" class="form-control search-select triggerSimularSelect">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-4">
                                 <label class="control-label">Valor do saque</label>
                                 <input required="true" readonly="readonly" id="valor_financiado"class="form-control" type="text" name="">
                              </div>
                           </div>
                           
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group" id="opcoes-parcelas">
                           </div>
                        </div>
                     </div>

                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-9">
                              </div>
                              <div class="col-sm-3">
                                 <a href="#" id="btn-go-to-informacoes-basicas" class="btn btn-yellow btn-block">
                                 Avançar <i class="fa fa-arrow-circle-right"></i>
                                 </a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
               <div class="col-md-12">
                  
               </div>
            </div>
            <div class="tab-pane" id="tab4_dados_do_cliente">
               <!--Formulário inicial-->
               <div class="col-sm-12">
                  <form action="#" id="form-dados-cliente" data-tab-bind="li-aba-dados-cartao">
                     <div class="row">
                        <div class="col-md-12" ></div>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           
                           <div class="form-group">
                              <div class="col-sm-6">
                                 <label class="control-label">Nome completo : <span class="symbol"></span></label>
                                 <input required name="Saque[nomeCliente]" type="text" placeholder="Nome completo" id="cliente_nome" class="form-control">
                              </div>
                           </div>
                                                      
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">Nascimento : <span class="symbol"></span></label>
                                 <input required name="Saque[nascimento]" type="text" placeholder="Data de nascimento" id="nascimento" class="form-control dateBR">
                              </div>
                           </div>

                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">CPF : <span class="symbol"></span></label>
                                 <input required name="Saque[cpf]" type="text" placeholder="CPF" id="cpf" class="form-control cpf">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <p></p>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-9" style="padding-left:0!important;">
                                 <div class="col-sm-3">
                                    <a data-go-to-step="#tab4_detalhes_proposta" class="btn btn-yellow btn-block btn-step-back">
                                    <i class="fa fa-arrow-circle-left"></i> Voltar  </a>
                                 </div>
                              </div>
                              <div class="col-sm-3">
                                 <button id="btn-submit-informacoes-basicas" class="btn btn-yellow btn-block" type="submit">
                                 Avançar <i class="fa fa-arrow-circle-right"></i>
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
            <div class="tab-pane" id="tab4_dados_do_cartao">
               
               <div class="col-sm-12">
                  <form action="#" id="form-dados-cartao" data-tab-bind="">
                     <div class="row">
                        <div class="col-md-12" ></div>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-4">
                                 <label class="control-label">Selecione a bandeira : <span class="symbol required"></span></label>
                                 <select required="true" id="select-bandeira" name="Saque[bandeiraCartao]" class="form-control search-select">
                                    <option value="">Selecione:</option>
                                    <option value="VISA">Visa</option>
                                    <option value="MASTER">Master</option>
                                    <option value="HIPER">Hiper</option>
                                 </select>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-4">
                                 <label class="control-label">Nº do cartão: <span class="symbol required"></span></label>
                                 <input id="numero_cartao" required name="Saque[numeroCartao]" type="text" placeholder="Número do cartão" class="form-control">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-4">
                                 <label class="control-label">Validade(mm/aa) : <span class="symbol required"></span></label>
                                 <input id="validade_cartao" required name="Saque[vencimentoCartao]" type="text" placeholder="Validade do cartão" class="form-control">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-5">
                                 <label class="control-label">Nome do Titular (idêntico ao cartão) : <span class="symbol required"></span></label>
                                 <input id="" required name="Saque[nomeTitular]" type="text" placeholder="Nome do titular" class="form-control">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">Cód. Segurança : <span class="symbol required"></span></label>
                                 <input id="" required name="Saque[codSeguranca]" type="text" placeholder="Código de segurança" class="form-control">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <p></p>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-9" style="padding-left:0!important;">
                                 <div class="col-sm-3">
                                    <a data-go-to-step="#tab4_dados_do_cliente" class="btn btn-yellow btn-block btn-step-back">
                                    <i class="fa fa-arrow-circle-left"></i> Voltar  </a>
                                 </div>
                              </div>
                              <div class="col-sm-3">
                                 <button id="" class="btn btn-yellow btn-block" type="submit">
                                 Avançar <i class="fa fa-arrow-circle-right"></i>
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
            
            <div class="tab-pane" id="tab4_dados_bancarios">
               
               <div class="col-sm-12">
                  <form action="#" id="form-dados-bancarios" data-tab-bind="">
                     <div class="row">
                        <div class="col-md-12" ></div>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-4">
                                 <label class="control-label">Banco: <span class="symbol required"></span></label>
                                 <select required="true" id="select-banco" name="Saque[banco]" class="form-control search-select">
                                    <?php foreach (Banco::model()->findAll() as $banco) { ?>
                                       <option value="<?php echo $banco->nome ?>"><?php echo $banco->nome ?></option>
                                    <?php } ?>
                                 </select>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-4">
                                 <label class="control-label">Agência: <span class="symbol required"></span></label>
                                 <input id="numero_cartao" required name="Saque[agencia]" type="text" placeholder="Número do cartão" class="form-control">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-4">
                                 <label class="control-label">Conta: <span class="symbol required"></span></label>
                                 <input id="numero_cartao" required name="Saque[conta]" type="text" placeholder="Número do cartão" class="form-control">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <p></p>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-9" style="padding-left:0!important;">
                                 <div class="col-sm-3">
                                    <a data-go-to-step="#tab4_dados_do_cartao" class="btn btn-yellow btn-block btn-step-back">
                                    <i class="fa fa-arrow-circle-left"></i> Voltar  </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
               
            </div>
            
         </div>
      </div>
   </div>
</div>

<div data-width="690" id="sucess-return" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
   <div class="modal-body" style="background:#dff0d8!important">
      <h4 style="color:#5cb85c;text-align:center"><i class="fa fa-check-circle" ></i> Intenção de saque registrada com sucesso!</h4>
   </div>
   <div class="modal-footer" style="margin-top:0!important;text-align:center">
      <form method="POST" action="/saqueFacil/comprovante/" style="display:inline">
         <button type="submit" class="btn btn-success">
            Imprimir comprovante de intenção <span id="span-codigo-proposta"></span>
            <input type="hidden" name="id" id="ipt-hdn-id-saque">
         </button>   
      </form>
      <form action="/proposta/minhasPropostas/" method="POST" style="display:inline">
         <button type="submit" class="btn btn-success">
            Página Inicial
         </button>   
      </form>
      <form action="/saqueFacil/" method="POST" style="display:inline">
         <button type="submit" class="btn btn-success">
            Nova Intenção
         </button>   
      </form>
   </div>
</div>

<div class="row" style="margin-bottom:40px;">
   <div class="col-md-3"></div>
   <div class="col-md-6">
      <div class="form-group">
         <button id="btn-enviar-proposta" class="btn btn-yellow btn-block">
            Enviar <i class="fa fa-arrow-circle-right"></i>
         </button>
      </div> 
   </div>
   <div class="col-md-3">        
   </div>
</div>