<div class="row">
   <div class="col-sm-12">
      
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
               Saque Fácil
            </a>
         </li>
         <li class="active">
            Histórico 
         </li>
      </ol>
      <div class="page-header">
         <h1>
         	Histórico de Intenções de saque
         </h1>
      </div>
   </div>
</div>

<div class="row" style="margin-bottom:50px">
   <div class="col-sm-12">
      <table id="tableHistorico" class="table table-striped table-bordered table-hover table-full-width dataTable">
         <thead>
            <tr>
	            <th width="300" class="no-orderable">Cliente</th>
               <th class="no-orderable">CPF</th>
               <th class="no-orderable">Valor Inicial</th>
               <th class="no-orderable">Parcelamento</th>
	           	<th class="no-orderable">Valor do saque</th>
               <th class="no-orderable">Data do cadastro</th>
               <th class="no-orderable">Imprimir</th>
            </tr>
         </thead>
         <tbody>
            
         </tbody>
      </table>
   </div>
</div>

<style type="text/css">
   #tableHistorico_filter, #tableHistorico_length{
      display: none;
   }
</style>