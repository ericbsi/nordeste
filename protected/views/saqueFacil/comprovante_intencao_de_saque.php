<?php

	$util 			= new Util;
?>
<form>
	<input type="hidden" id="proposta_codigo" value="">
</form>
<div id="main-center" style="padding-top:0!important;margin-top:0!important;">
	<header style="padding-top:0!important;margin-top:0!important;">
		<h1 style="width:300px;float:left;margin-top:0!important;">
			<img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/credtresanos.png">
		</h1>
		<h2 style="float:right;">COMPROVANTE DE INTENÇÃO DE SAQUE</h2>
	</header>
	<section style="text-align:center;margin-top:10px;float:left;width:100%">
		<h3 style="font-size:15px;">DADOS DO SOLICITANTE</h3>
	</section>
	<section id="contrato-section-one">
		<div style="float:left;width:40%">
			<p><span>Solicitante: </span><?php echo strtoupper($saque->nomeCliente); ?></p>
		</div>
		<div style="float:left;width:22%">
			<p><span>CPF: </span><?php echo $saque->cpf ?></p>
		</div>
		<div style="float:right;width:30%;">
			<p><span>Nascimento: </span><?php echo $util->bd_date_to_view($saque->nascimento); ?></p>
		</div>
		
	</section>
	
	<section style="text-align:center;margin-top:10px;float:left;width:100%">
		<h3 style="font-size:15px;">DADOS FINANCEIROS DA SOLICITAÇÃO</h3>
	</section>
	<section id="contrato-section-three">
		<div style="float:left;width:50%">
			<p><span>Valor solicitado: </span>R$ <?php echo number_format($saque->valorSolicitado, 2, ',','.'); ?></p>
			<p><span>Parcelamento: </span>R$ <?php echo $saque->getParcelamento(); ?></p>
			<p><span>Valor do saque: </span>R$ <?php echo $saque->getValorSaque(); ?></p>
			<p><span>Bandeira do cartão: </span> <?php echo $saque->bandeiraCartao ?> <img src="/<?php echo $saque->bandeiraCartao ?>.png"> </p>
			<p><span>Juros: </span>2.99% + 1.99% ao mês</p>
			<p><span>Nome na sua fatura: </span>CREDSHOW</p>
		</div>
	</section>

	<section id="section-mensagem-seguro" style="width:970px">
		<div style="width:90%;float:left">
			<p><strong>Esta proposta está sujeita a uma análise de crédito</strong></p>
		</div>
	</section>

	<section id="contrato-section-six">
		<div style="float:left">
			<p style="text-align:left"><?php echo strtoupper($saque->nomeCliente); ?></p>
			<p>___________________________________</p>
		</div>
	</section>
	
</div>
