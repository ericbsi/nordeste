<?php 
	$util 				= new Util;
	$campoCnabConfig 	= new CampoCnabConfig;
        $baixas = [];
        if($banco == 1){
            $sql = "SELECT * FROM beta.Baixa as B
                    INNER JOIN beta.Parcela as P ON B.Parcela_id = P.id AND P.habilitado
                    WHERE B.Parcela_id = ". $id_parc;
            $baixas = Yii::app()->db->createCommand($sql)->queryAll();
        }else{
            $sql = "SELECT * FROM nordeste2.Baixa as B
                    INNER JOIN nordeste2.Parcela as P ON B.Parcela_id = P.id AND P.habilitado
                    WHERE B.Parcela_id = ". $id_parc;
            $baixas = Yii::app()->db->createCommand($sql)->queryAll();
        }
?>
<div class="row">
	<div class="col-sm-12">
		<h4>Baixas</h4>
		<table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
		   <thead>
		      <tr>
		         <th>Valor da parcela</th>
		         <th>Data pagamento</th>
		         <th>Mora</th>
		         <!--<th>IOF</th>
		         <th>Multa</th>-->
		         <th>Desconto</th>
		         <th>Valor total pago</th>
		      </tr>
		   </thead>
		   <tbody>
		      <?php foreach( $baixas as $baixa ){ ?>
		      <tr>
		         <td><?php echo "R$ " . number_format($baixa['valor'],2,',','.'); ?></td>
		         <td><?php echo $util->bd_date_to_view($baixa['data_da_ocorrencia']);?></td>
		         <td><?php echo "R$ " . number_format($campoCnabConfig->cnabValorMonetarioToDouble($baixa['valor_mora']), 2, ',', '.'); ?></td>
		         <!--<td><?php //echo "0,00"; ?></td>-->
		         <!--<td><?php //echo "0,00"; ?></td>-->
		         <td><?php echo "0,00"; ?></td>
		         <td><?php echo number_format($baixa['valor_pago'],2,',','.'); ?></td>
		      </tr>
		      <?php } ?>
		   </tbody>
		</table>
	</div>
</div>