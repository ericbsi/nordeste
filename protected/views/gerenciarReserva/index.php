<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/GerenciarReserva/index">
                    Loja
                </a>
            </li>
            <li class="active">
                Gerenciar Reserva
            </li>
        </ol>
        <div class="page-header">
            <h1>Gerenciar Reserva</h1>
        </div>
    </div>
</div>

<div class="row" align="right">
    <div class="col-sm-12">
        <!--<a style="font-size:12px;" id="teste" onClick="document.location.reload(true)" class="btn btn-green"><b>+ Adicionar Reserva</b></a>-->
    </div>
</div>

<br/>
<br/>

<?php if (isset($query) && $query != NULL) { ?>

    <div class="row">

        <div class="col-sm-12">

            <table id="tableReservas" class="table display table-bordered table-hover dataTable">

                <thead>
                    <tr>
                        
                        <?php foreach ($cabec as $c) { ?>
                            <th class="searchable"><?php echo $c; ?></th>
                        <?php } ?>

                        <th style="vertical-align: top">REQUISITADO</th>
                        <th style="vertical-align: top">EFETIVAR</th>
                        <th style="vertical-align: top">RECUSAR</th>
                        <td style="width: 20px"></td>

                    </tr>
                </thead>

                <tbody>
                    <?php for ($cnt = 0; $cnt < sizeof($query); $cnt++) { ?>
                        <tr class="<?php
                                        if ($query[$cnt][8] == 1) {
                                            echo 'tr-aberta';
                                        }elseif ($query[$cnt][8] == 2){
                                            echo 'tr-parcial';
                                        }elseif ($query[$cnt][8] == 3){
                                            echo 'tr-efetivada';
                                        }elseif ($query[$cnt][8] == 4){
                                            echo 'tr-recusada';
                                        }
                                   ?>">
                            <td><?php echo $query[$cnt][0]; ?></td> <!--reserva-->
                            <td><?php echo $query[$cnt][1]; ?></td> <!--filial-->
                            <td><?php echo $query[$cnt][2]; ?></td> <!--data-->
                            <td><?php echo $query[$cnt][3]; ?></td> <!--cod produto-->
                            <td><?php echo $query[$cnt][4]; ?></td> <!--nome produto-->
                            
                            <td                         data-td-tipo="qtd" data-rese-cod="<?php echo $query[$cnt][0]; ?>">
                                <?php echo $query[$cnt][5]; ?>
                            </td> <!--qtd req-->
                            
                            <td class="<?php 
                                    if ($query[$cnt][8] == 1){
                                      echo 'td-qtd-gerencia';
                                    }
                                ?>"
                                data-td-tipo="efe" 
                                data-rese-cod="<?php echo $query[$cnt][0]; ?>" 
                                style="width: 20px">
                                <?php echo $query[$cnt][6]; ?> <!--qtd efe-->
                            </td>

                            <td class="<?php 
                                    if ($query[$cnt][8] == 1){
                                      echo 'td-qtd-gerencia';
                                    }
                                ?>"
                                data-td-tipo="rec"
                                data-rese-cod="<?php echo $query[$cnt][0]; ?>" 
                                style="width: 20px">
                                <?php echo $query[$cnt][7]; ?> <!--qtd recusada-->
                            </td>

                            <td>
                                <p hidden data-p-btn="<?php echo $query[$cnt][0]; ?>">
                                    <a style="font-size:10px;" class="btn btn-green btn-ger-reserva" data-rese-btn="<?php echo $query[$cnt][0]; ?>" >
                                        <b>Confirmar</b>
                                    </a>
                                </p>
                            </td>

                        </tr>

                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>

    <br/>
<?php } ?>
<style>
    .tr-aberta{
        background-color: lightgreen!important;
    }
    .tr-parcial{
        background-color: lightgoldenrodyellow!important;
    }
    .tr-efetivada{
        background-color: lightblue!important;
    }
    .tr-recusada{
        background-color: lightsalmon!important;
    }
</style>