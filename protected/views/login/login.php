<div class="content">
    
    <div class="form-group">
        <div class="col-sm-12">
            <div class="input-group">
                
                <span class="input-group-addon">
                    <i class="fa fa-user"></i>
                </span>
                
                <input class="form form-control" id="username" placeholder="Nome de usuário" />
                
            </div>
        </div>
    </div>
    
    <div class="form-group" id="senhaUsuario" style="display: none">
        <div class="col-sm-12">
            <div class="input-group">

                <span class="input-group-addon">
                    <i class="fa fa-lock"></i>
                </span>
                
                <input type="password" placeholder="Senha" id="password" class="form form-control" />

            </div>
        </div>
    </div>
    
    <div class="form-group" id="selectRole" style="display: none">
        <div class="col-sm-12">
            <div class="input-group">

                <span class="input-group-addon">
                    <i class="fa fa-tasks"></i>
                </span>
                
                <input type="hidden" id="modulo" class="form form-control search-select select2" />

            </div>
        </div>
    </div>
    
    <div class="form-group" id="selectFiliais" style="display: none">
        <div class="col-sm-12">
            <div class="input-group">

                <span class="input-group-addon">
                    <i class="fa fa-sitemap"></i>
                </span>
                
                <input type="hidden" id="filiais" class="form form-control search-select select2" />

            </div>
        </div>
    </div>
    
</div>

<div class="foot">
    <p align="center">
        <button style="display: none" id="btnLogar" class="btn btn-primary" type="submit">
            ENTRAR
        </button>
    </p>
</div>