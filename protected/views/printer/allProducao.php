<?php 

	$util = new Util; 

	$totalInicial 		= 0;
	$totalEntradas 		= 0;
	$totalFinanciado 	= 0;
	$totalFinal 		= 0;

?>

<table role="grid" style="margin:0!important;border-bottom:none!important">
	<thead>
		<tr>
			<th  width="1137" style="text-align:center">Relatório de Produção por filiais</th>
		</tr>
	</thead>	
</table>

<table role="grid" style="border-top:none!important">
	<thead>
		<tr>
			<th  width="339">Operador: <?php echo Yii::app()->session['usuario']->nome_utilizador; ?></th>
			<th  width="264">Data de emissão: <?php echo date('d/m/Y') ?></th>
			<th  width="264">Data de: <?php echo $parametros['dataDe']; ?></th>			
			<th  width="264">Data até: <?php echo $parametros['dataAte']; ?></th>
		</tr>
	</thead>	
</table>

<table role="grid">
	<thead>
		<tr>
			<th  width="120">Código</th>
			<th  width="70">Emissão</th>
			<th>Filial</th>
			<th  width="150">Cliente</th>
			<th  width="70">CPF</th>
			<th  width="100">R$ Inicial</th>
			<th  width="100">R$ Entrada</th>
			<th  width="100">R$ Financiado</th>
			<th  width="100">Parcelamento</th>
			<th  width="100">R$ Final</th>
		</tr>
	</thead>	
	<tbody>
		<?php foreach($parametros['propostas'] as $proposta):  ?>
			<tr>
				<td><?php echo $proposta->codigo; ?></td>
				<td><?php echo $util->bd_date_to_view(substr($proposta->data_cadastro, 0,10)); ?></td>
				<td><?php echo strtoupper($proposta->analiseDeCredito->filial->getEndereco()->cidade . ' / ' .$proposta->analiseDeCredito->filial->getEndereco()->uf); ?></td>
				<td><?php echo substr(strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome), 0,15); ?></td>
				<td><?php echo $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero; ?></td>
				<td><?php echo number_format($proposta->valor, 							2, ',','.'); ?></td>				
				<td><?php echo number_format($proposta->valor_entrada, 					2, ',','.'); ?></td>
				<td><?php echo number_format($proposta->valor-$proposta->valor_entrada, 2, ',','.'); ?></td>
				<td><?php echo $proposta->qtd_parcelas.' x ' .number_format($proposta->getValorParcela(),			2, ',','.'); ?></td>
				<td><?php echo number_format($proposta->valor_final, 					2, ',','.'); ?></td>				
			</tr>
		<?php 
			
			$totalInicial 		+= $proposta->valor;
			$totalEntradas 		+= $proposta->valor_entrada;
			$totalFinanciado 	+= $proposta->valor-$proposta->valor_entrada;
			$totalFinal 		+= $proposta->valor_final;

			endforeach;  
		?>
	</tbody>
	<tfoot>
		<tr>
			<th  width="120">Total: <?php echo count($parametros['propostas']); ?> propostas</th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th><?php echo 'R$ '. number_format($totalInicial, 		2, ',','.'); ?></th>
			<th><?php echo 'R$ '. number_format($totalEntradas, 	2, ',','.'); ?></th>
			<th><?php echo 'R$ '. number_format($totalFinanciado, 	2, ',','.'); ?></th>
			<th><?php echo 'R$ '. number_format($totalFinal, 		2, ',','.'); ?></th>			
			<th></th>
		</tr>
	</tfoot>
</table>