<?php 
	$util 				= new Util;
	$totalInicial 		= 0;
	$totalEntradas 		= 0;
	$totalFinanciado	= 0;
?>
<table role="grid" style="margin:0!important;border-bottom:none!important">
	<thead>
		<tr>
			<th  width="1012" style="text-align:center">Relatório de Propostas Negadas</th>
		</tr>
	</thead>	
</table>
<table role="grid" style="border-top:none!important;">
	<thead>
		<tr>
			<th  width="300">Operador: <?php echo strtoupper( Yii::app()->session['usuario']->nome_utilizador ); ?></th>
			<th  width="250">Data de emissão: <?php echo date('d/m/Y') ?></th>
			<th  width="250">Data de: <?php echo $params['dataDe']; ?></th>
			<th  width="206">Data até: <?php echo $params['dataAte']; ?></th>
		</tr>
	</thead>	
</table>
<table role="grid">
	<thead>
		<tr>
			<th width="1012px" style="text-align:center;">Filiais Filtradas: </th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td width="900px"><?php echo $params['nomesParceiros']; ?></td>
		</tr>
	</tbody>
</table>
<table width="1017" role="grid" style="margin-bottom:0!important">
	<thead>
		<tr>
			<th style="text-align:center" width="1012px">Propostas: </th>
		</tr>
	</thead>
</table>

<table width="1017" style="margin-top:0!important" role="grid">
	<thead>
		<tr>
			<th width="80px" style="text-align:left;">Data:</th>
			<th width="120px" style="text-align:left;">Cód:</th>
			<th width="200px" style="text-align:left;">Filial:</th>
			<th width="100px" style="text-align:left;">Cliente:</th>
			<th width="80px" style="text-align:left;">Inicial:</th>
			<th width="65px" style="text-align:left;">Entrada:</th>
			<th width="50px" style="text-align:left;">Carência:</th>
			<th width="80px" style="text-align:left;">Financiado:</th>
			<th width="40px" style="text-align:left;">Parcelas:</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach( $params['propostas'] as $row ){ 
			$proposta             = Proposta::model()->findByPk($row);
			$totalInicial 		 += $proposta->valor;
			$totalEntradas 		 += $proposta->valor_entrada;
			$totalFinanciado	 += $proposta->valor-$proposta->valor_entrada;
		?>
		<tr>
			<td><?php echo $util->bd_date_to_view(substr($proposta->data_cadastro, 0,10)); ?></td>
			<td><?php echo $proposta->codigo; ?></td>
			<td><?php echo strtoupper($proposta->analiseDeCredito->filial->getConcat()); ?></td>
			<td><?php echo strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome); ?></td>
			<td><?php echo 'R$ ' .number_format($proposta->valor,2,',','.'); ?></td>
			<td><?php echo 'R$ ' .number_format($proposta->valor_entrada,2,',','.'); ?></td>
			<td><?php echo $proposta->carencia; ?></td>
			<td><?php echo 'R$ ' .number_format($proposta->valor-$proposta->valor_entrada,2,',','.'); ?></td>
			<td><?php echo $proposta->qtd_parcelas; ?></td>
		</tr>
		<?php } ?>
	</tbody>
	<tfoot>
		<tr>
			<th></th>
			<th><?php echo count( $params['propostas'] ) . ' Propostas'; ?></th>
			<th></th>
			<th></th>
			<th><?php echo 'R$ '.number_format($totalInicial, 2,',','.'); ?></th>
			<th><?php echo 'R$ '.number_format($totalEntradas, 2,',','.'); ?></th>
			<th></th>
			<th><?php echo 'R$ '.number_format($totalFinanciado, 2,',','.'); ?></th>
			<th></th>
		</tr>
	</tfoot>
</table>