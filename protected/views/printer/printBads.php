<table role="grid" style="margin:0!important;border-bottom:none!important">
	<thead>
		<tr>
			<th  width="906" style="text-align:center">Impressão de Análise de Bads : <?php echo $printConfig['customReturn']['printerConfig']['labelSafra'] ?></th>
		</tr>
	</thead>	
</table>

<table role="grid" style="border-top:none!important">
	<thead>
		<tr>
			<th  width="452">Operador: <?php echo strtoupper(Yii::app()->session['usuario']->nome_utilizador); ?></th>
			<th  width="452">Data de emissão: <?php echo date('d/m/Y') ?></th>			
		</tr>
	</thead>	
</table>


<?php if( count( $printConfig['customReturn']['printerConfig']['analista'] ) > 0 && $printConfig['customReturn']['printerConfig']['analista'] != 0 ){

	$analista = Usuario::model()->findByPk( $printConfig['customReturn']['printerConfig']['analista'] );
?>

<table role="grid">
	<thead>
		<tr>
			<th style="text-align:center"  width="904">Analista: <?php echo strtoupper($analista->nome_utilizador); ?></th>
		</tr>
	</thead>	
</table>
<?php } ?>
<?php if( isset( $printConfig['customReturn']['printerConfig']['parceiros'][1] ) ){ ?>
<table role="grid" style="margin-bottom:none!important" width="910px">
	<thead>
		<tr>
			<th style="text-align:center" width="904">Parceiros</th>
		</tr>
	</thead>
	<tbody>
	<tr>
		<td>
	<?php
		for ( $i = 0; $i < count( $printConfig['customReturn']['printerConfig']['parceiros'] ); $i++ ){
				if( $printConfig['customReturn']['printerConfig']['parceiros'][$i] != '0' ){
					$parceiro = Filial::model()->findByPk( $printConfig['customReturn']['printerConfig']['parceiros'][$i] );
		?>
		<?php echo strtoupper($parceiro->getConcat()) ; if( $i != count( $printConfig['customReturn']['printerConfig']['parceiros'] )-1 ){echo ', ';} ?>
		<?php 
				}
			}		
		?>
		</td>
	</tr>
	</tbody>
</table>

<?php }else{ ?>
<table role="grid" style="margin-bottom:none!important" width="910px">
	<thead>
		<tr>
			<th style="text-align:center" width="904">Parceiros: Todos</th>
		</tr>
	</thead>
</table>
<?php } ?>

<table role="grid" width="910px">
	<thead>
		<tr>
			<th>Bad</th>
			<th>Período</th>
			<th>R$ Total</th>
			<th>% Pago</th>
			<th>% Bad</th>
		</tr>
	</thead>
	<tbody>
	<?php for ( $y = 0; $y < count( $printConfig['data'] ); $y++ ) { ?>
		<tr>
			<td><?php echo $printConfig['data'][$y]['bad'] ?></td>
			<td><?php echo $printConfig['data'][$y]['periodo'] ?></td>
			<td><?php echo $printConfig['data'][$y]['total'] ?></td>
			<td><?php echo $printConfig['data'][$y]['porcenPgto'] ?></td>
			<td><?php echo $printConfig['data'][$y]['porcenInadim'] ?></td>
		</tr>
	<?php } ?>
	</tbody>
	<tfoot>
		<tr>
			<th></th>
			<th><?php echo 'R$ ' . $printConfig['customReturn']['totalProducao']; ?></th>
			<th></th>
			<th></th>
			<th></th>
		</tr>
	</tfoot>
</table>

<?php //var_dump($printConfig['customReturn']); ?>