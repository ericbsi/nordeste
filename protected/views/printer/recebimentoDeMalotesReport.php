<?php 
	$util 				= new Util;
	$totalFinanciado 	= 0;
	$totalRepasse 		= 0;
?>
<table role="grid" style="margin:0!important;border-bottom:none!important">
    <thead>
        <tr>
            <th  width="920" style="text-align:left">DATA DE PROCESSAMENTO: <?php echo $util->bd_date_to_view(substr($maloteHasRecebimentoDeDocumentacao->recebimentoDeDocumentacao->dataCriacao, 0, 10)) . ' às ' . substr($maloteHasRecebimentoDeDocumentacao->recebimentoDeDocumentacao->dataCriacao, 11) ?> </th>
        </tr>
    </thead>
</table>
<table role="grid" style="margin:0!important;border-bottom:none!important">
    <thead>
        <tr>
            <th  width="920" style="text-align:left">CÓDIGO DO PROCESSO: <?php echo $maloteHasRecebimentoDeDocumentacao->recebimentoDeDocumentacao->codigo ?> </th>
        </tr>
    </thead>
</table>
<table role="grid" style="margin:0 0 0 0!important;border-bottom:none!important">
    <thead>
        <tr>
            <th  width="920" style="text-align:center">Propostas</th>
        </tr>
    </thead>
</table>
<table role="grid" style="margin:0!important;">
        <thead>
            <tr width>
                <th  width="80" style="text-align:left">DATA:</th>
                <th  width="151" style="text-align:left">CÓDIGO:</th>
                <th  width="70" style="text-align:left">CPF:</th>
                <th  width="200" style="text-align:left">NOME:</th>
                <th  width="139" style="text-align:left">VAL FIN:</th>
                <th  width="60" style="text-align:left">CARÊNCIA:</th>
                <th  width="60" style="text-align:left">PARCELAS:</th>
                <th  width="137" style="text-align:left">VAL REPASSE:</th>
            </tr>
        </thead>
        <tbody>
<?php foreach ($malote->maloteHasRecebimentoDeDocumentacaos as $maloteHasRecebimentoDeDocumentacao): ?>
	
    <?php //$totalFinanciado = 0; ?>
    <?php //$totalRepasse    = 0;?>
	
            <?php foreach ($maloteHasRecebimentoDeDocumentacao->recebimentoDeDocumentacao->recebimentoHasPropostas as $hasProposta): ?>
                	<?php 
                		$totalFinanciado 	+=  ($hasProposta->proposta->valor - $hasProposta->proposta->valor_entrada);
                		$totalRepasse 		+=  ($hasProposta->proposta->valorRepasse());
                		
                	?>
                    <tr>
                        <td><?php echo $util->bd_date_to_view(substr($hasProposta->proposta->data_cadastro, 0,10)) ?></td>
                        <td><?php echo $hasProposta->proposta->codigo ?></td>
                        <td><?php echo $hasProposta->proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero ?></td>
                        <td><?php echo substr(strtoupper($hasProposta->proposta->analiseDeCredito->cliente->pessoa->nome), 0,20) ?></td>
                        <td><?php echo "R$ " . number_format($hasProposta->proposta->valor - $hasProposta->proposta->valor_entrada, 2, ',', '.') ?></td>
                        <td><?php echo $hasProposta->proposta->carencia ?></td>
                        <td><?php echo $hasProposta->proposta->qtd_parcelas ?></td>
                        <td><?php echo "R$ " . number_format($hasProposta->proposta->valorRepasse(), 2, ',', '.') ?></td>
                    </tr>
                
            <?php endforeach; ?>
        </tbody>
<?php endforeach ?>
</table>
<table role="grid" style="margin:0!important ">
    <thead>
            <tr>
                <th width="80"></th>
                <th width="151" ></th>
                <th width="70"></th>
                <th width="200"></th>
                <th width="139">TOTAL: <?php echo "R$ " . number_format($totalFinanciado, 2, ',','.'); ?></th>
                <th width="60"></th>
                <th width="60"></th>
                <th width="137">TOTAL: <?php echo "R$ " . number_format($totalRepasse, 2, ',','.'); ?></th>
            </tr>
    </thead>
</table>
<table role="grid" style="margin:0!important">
	<thead>
		<tr>
			<th  width="460" style="text-align:left">ASS: ___________________________________</th>
			<th  width="460" style="text-align:left">DOC: ___________________________________, ASS: ___________________________________</th>
		</tr>
	</thead>
</table>
<style type="text/css">
	table tr th, table tr td{
		font-size: 0.400rem;
	}
	

    @media all{

    }
    
</style>