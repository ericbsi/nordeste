<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title></title>
    
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/foundation/vendor/modernizr.js"></script>

    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/foundation/foundation.css" >

  </head>
  <body>
    
    <?php echo $content ?>


    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/foundation/vendor/jquery.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/foundation/foundation.min.js"></script>
    
    <script>
      $(document).foundation();
    </script>

  </body>
</html>
