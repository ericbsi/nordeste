<?php
	$totalPropostas 		= 0;
	$valorTotalPropostas 	= 0;
?>
<table role="grid" style="margin:0!important;border-bottom:none!important">
	<thead>
		<tr>
			<th  width="906" style="text-align:center">Relatório de Produção por filiais</th>
		</tr>
	</thead>	
</table>
<table role="grid" style="border-top:none!important">
	<thead>
		<tr>
			<th  width="250">Operador: <?php echo Yii::app()->session['usuario']->nome_utilizador; ?></th>
			<th  width="250">Data de emissão: <?php echo date('d/m/Y') ?></th>
			<th  width="200">Data de: <?php echo $parametros['dataDe'] ?></th>
			<th  width="200">Data até: <?php echo $parametros['dataAte'] ?></th>
		</tr>
	</thead>	
</table>
<table role="grid">
	<thead>
		<tr>
			<th  width="500">Filial</th>
			<th  width="200">Quantidade</th>
			<th  width="200">Produção</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($parametros['resultado'] as $r): $totalPropostas += $r['numero_de_proposta']; $valorTotalPropostas += $r['valor_total']; ?>
			<tr>
				<td><?php echo strtoupper($r['Filial']) ?></td>				
				<td><?php echo $r['numero_de_proposta'] ?></td>
				<td><?php echo "R$ " . number_format($r['valor_total'],2,',','.') ?></td>				
			</tr>
		<?php endforeach;  ?>
	</tbody>
	<tfoot>
		<tr>
			<th></th>
			<th><?php echo $totalPropostas; ?></th>
			<th><?php echo "R$ " . number_format($valorTotalPropostas,2,',','.'); ?></th>
		</tr>
	</tfoot>
</table>