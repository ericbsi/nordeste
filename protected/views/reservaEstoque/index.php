<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/ReservaEstoque/index">
                    Loja
                </a>
            </li>
            <li class="active">
                Reserva Estoque
            </li>
        </ol>
        <div class="page-header">
            <h1>Reserva Estoque</h1>
        </div>
    </div>
</div>

<div class="row" align="right">
    <div class="col-sm-12">
        <!--<a style="font-size:12px;" id="teste" onClick="document.location.reload(true)" class="btn btn-green"><b>+ Adicionar Reserva</b></a>-->
    </div>
</div>

<br/>
<br/>

<?php if (isset($query) && $query != NULL) { ?>

<!--<form id="form_reservaestoque" method="post" action="/reservaEstoque/reservar/">-->

        <div class="row">

            <div class="col-sm-12">

                <table id="table_reservaestoque" class="table display table-striped table-bordered table-hover dataTable">

                    <thead>
                        <tr>
                            <!--<th><input type="checkbox" id="selectall" /></th>-->
                            <?php foreach ($cabec as $c) { ?>
                                <th class="searchable"><?php echo $c; ?></th>
                            <?php } ?>
                            
                            <th style="vertical-align: top">RESERVAR</th>
                            <td style="width: 20px"></td>

                        </tr>
                    </thead>

                    <tbody>
                        <?php for ($cnt = 0; $cnt < sizeof($query); $cnt++) { ?>
                            <tr>
                                <td><?php echo $query[$cnt][0]; ?></td>
                                <td><?php echo $query[$cnt][1]; ?></td>
                                <td><?php echo $query[$cnt][2]; ?></td>
                                <td><?php echo $query[$cnt][3]; ?></td>
                                <td><?php echo $query[$cnt][4]; ?></td>
                                <!--<td style="width: 20px"><input style="width: 50px" type="number" class="input-qtd-reserva" data-prod-qtd="<?php //echo $query[$cnt][0]; ?>" value="0"/></td>-->
                                <td class="td-qtd-reserva" data-prod-qtd="<?php echo $query[$cnt][0]; ?>" style="width: 20px">0</td>

                                <td>
                                    <p hidden data-p-btn="<?php echo $query[$cnt][0]; ?>">
                                        <a style="font-size:10px;" class="btn btn-green btn-add-reserva" data-prod-btn="<?php echo $query[$cnt][0]; ?>" >
                                            <b>Confirmar</b>
                                        </a>
                                    </p>
                                </td>
<!--                                <input type="hidden" name="lojaEstoque[]" value="25" />
                                <input type="hidden" name="produto[]"     value="<?php //echo $query[$cnt][0]; ?>" />-->
                            </tr>

                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>

        <br/>
<!--    </form>-->
<?php } ?>
