<div class="row">
   <div class="col-sm-12">
      
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
               Relatórios
            </a>
         </li>
         <li class="active">
            Histórico do Analista
         </li>
      </ol>
      <div class="page-header">
         <h1>
         	Meu Histórico
         </h1>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-sm-9">
      <div class="row">
         <form id="form-filter" action="">
            <div class="col-md-3" style="width:15%">
               <div class="form-group">
                  <label class="control-label">
                     Data de <span class="symbol required" aria-required="true"></span>
                  </label>                  
                  <input style="width:120px" type="text" name="data_de" class="form-control date-picker" data-date-viewmode="years" data-date-format="dd/mm/yyyy" id="data_de">
               </div>
            </div>
            <div class="col-md-3" style="width:15%">
               <div class="form-group">
                  <label class="control-label">Data até:</label>
                  <input style="width:120px" type="text" name="data_ate" class="form-control date-picker" data-date-viewmode="years" data-date-format="dd/mm/yyyy" id="data_ate">
               </div>
            </div>
             <div class="col-md-3" style="width:15%">
               <div class="form-group">
                  <label class="control-label">Apr/Neg:</label>
                  <select class="form-control" id="aprneg">
                      <option value="0">Todas</option>
                      <option value="1">Aprovadas</option>
                      <option value="2">Negadas</option>
                  </select>
               </div>
            </div>
            <!--<div class="col-md-3" style="width:15%">
               <div class="form-group">
                  <label class="control-label">N° parcelas de:</label>
                  <input style="width:120px" type="text" name="qtd_parcelas_de" class="form-control" id="qtd_parcelas_de">
               </div>
            </div>
            <div class="col-md-3" style="width:15%">
               <div class="form-group">
                  <label class="control-label">N° parcelas até:</label>
                  <input style="width:120px" type="text" name="qtd_parcelas_ate" class="form-control" id="qtd_parcelas_ate">
               </div>
            </div>
            <div class="col-md-3">
               <div class="form-group">
                  <label class="control-label">
                     Filiais <span class="symbol required"></span>
                  </label>
                  <select required="required" multiple="multiple" id="filiais_select" class="form-control multipleselect" name="Filiais[]">
                     <?php foreach ( Yii::app()->session['usuario']->getEmpresa()->listFiliais() as $f ) { ?>
                        <option value="<?php echo $f->id ?>"><?php echo $f->getConcat() ?></option>
                     <?php } ?>
                  </select>  
               </div>
            </div>-->
            <div class="col-md-3" style="width:15%">
               <div class="form-group">
                  <label class="control-label">&nbsp;</label>
                  <button id="btn-filter" class="btn btn-blue next-step btn-block">Filtrar</button>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>

<div class="row" >
   <div class="col-sm-12">
      <div class="col-sm-5" style="padding:0!important;">
         <div id="chart_div"></div>
      </div>
      <div class="col-sm-3" style="padding:0!important;">
         <div class="block-flat">
            <div class="content no-padding">
               <div class="overflow-hidden">
                  <i class="fa fa-thumbs-o-up fa-4x pull-left color-success"></i> 
                  <h3 class="no-margin">APROVADO</h3>
                  <p class="color-success">Valor total aprovado</p>
               </div>
               <h1 id="h1-total-aprovado" class="big-text no-margin"></h1>
            </div>
         </div>
      </div>
      <div class="col-sm-3" style="padding:0!important;margin-left:50px;">
         <div class="block-flat">
            <div class="content no-padding">
               <div class="overflow-hidden">
                  <i class="fa fa-thumbs-o-down fa-4x pull-left color-danger"></i> 
                  <h3 class="no-margin">REPROVADO</h3>
                  <p class="color-danger">Valor total reprovado</p>
               </div>
               <h1 id="h1-total-reprovado" class="big-text no-margin"></h1>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-sm-12">
      <table id="grid_financiamentos" class="table table-striped table-bordered table-hover table-full-width dataTable">
         <thead>
            <tr>
               <th style="width:70px!important" class="no-orderable">Cód</th>
               <th style="width:20px!important" class="no-orderable">Emissão</th>
               <th class="no-orderable">Filial</th>
               <th style="width:200px!important" class="no-orderable">Cliente</th>
               <th class="no-orderable">CPF</th>
               <th style="width:100px!important" class="no-orderable">R$ Inicial</th>
               <th style="width:80px!important" class="no-orderable">R$ Entrada</th>
               <th style="width:80px!important" class="no-orderable">R$ Seguro</th>
               <th style="width:80px!important" class="no-orderable">R$ Financiado</th>
               <th style="width:80px!important" class="no-orderable">Parcelamento</th>
               <th style="width:80px!important" class="no-orderable">R$ Final</th>
               <th class="no-orderable">Status</th>
            </tr>
            <tfoot>
               <!--<th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>               
               <th id="tfoot-total-inicial"></th>
               <th id="tfoot-total-ent"></th>
               <th id="tfoot-total-fin"></th>
               <th></th>
               <th id="tfoot-total-final"></th>
               <th></th>-->
            </tfoot>
         </thead>
      </table>
   </div>
</div>

<div class="row">
   <br>
   <br>
   <br>
</div>


<style>
   #grid_financiamentos_length, #grid_financiamentos_filter{
      display: none;
   }
   table tbody tr td{
      font-style: 10px!important;
   }
   td.details-control {
       background: url('../../images/details_open.png') no-repeat center center;
       cursor: pointer;
       padding: 0 25px!important;
   }
   tr.details td.details-control {
       background: url('../../images/details_close.png') no-repeat center center;
   }
   tfoot th {
      font-weight: bold!important;
      color: #3D9400;
      font-size: 11px;
   }

   .block-flat {
      margin-bottom: 40px;
      padding: 20px 10px 0px 20px;
      background: #F6F6F6;
      border-radius: 3px;
      -webkit-border-radius: 3px;
      border-left: 1px solid #efefef;
      border-right: 1px solid #efefef;
      border-bottom: 1px solid #e2e2e2;
      box-shadow: 0 1px 0 0 rgba(0, 0, 0, 0.04);
   }

   .overflow-hidden {
      overflow: hidden;
   }

   .fa.pull-left {
      margin-right: .3em;
   }

   .color-success {
      color: #5FBF5F;
   }

   .fa-4x {
      font-size: 4em;
   }

   .fa {
      display: inline-block;
      font-family: FontAwesome;
      font-style: normal;
      font-weight: normal;
      line-height: 1;
      -webkit-font-smoothing: antialiased;
      -moz-osx-font-smoothing: grayscale;
   }
   .pull-left {
      float: left !important;
   }

   h3, .h3{
      
      font-weight: 300!important;
      line-height: 1.1;
   }
   h3, .h3 {
      font-size: 24px!important;
   }

   .big-text {
      font-size: 30px;
      line-height: 40px !important;
      text-shadow: 2px 1px 2px rgba(0, 0, 0, 0.2);
      font-family: 'Open Sans';
      color: #555;
   }
   .color-danger {
      color: #ee5037;
   }
</style>