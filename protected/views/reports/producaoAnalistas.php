<?php $analistas = Yii::app()->session['usuario']->getEmpresa()->getAnalistas(); ?>
<div class="row">
   <div class="col-sm-12">
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
            Relatórios
            </a>
         </li>
         <li class="active">
            Produção dos Analistas
         </li>
      </ol>
      <div class="page-header">
         <h1>
            Analistas de Crédito
         </h1>
      </div>
   </div>
</div>

<div class="row" >
   <div class="col-sm-12">
      <?php if( count( $analistas ) > 0 ) : ?>
      <?php foreach( $analistas as $analista ) : ?>
      <div class="col-sm-3">
         <div class="block-flat">
            <div class="content no-padding">
               <div class="overflow-hidden">
                  <i class="fa fa-user fa-4x pull-left color-success"></i> 
                  <h3 class="no-margin nome-analista"><?php echo substr($analista->nome_utilizador, 0, 20); ?></h3>
                  <p class="color-success"><?php echo "Analista de Crédito" ?></p>
                  <div class="col-sm-2">
                  </div>
                  <div class="col-sm-8" style="padding:10px;">
                     <a data-nome-analista="<?php echo $analista->nome_utilizador ?>" data-analista-id="<?php echo $analista->id ?>" class="chart-perforamance" style="margin-left:5px" href="#"> <i class="fa fa-bar-chart-o"></i> </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php endforeach; ?>
      <?php endif; ?>
   </div>
</div>

<div class="row">
   <div class="col-sm-12">
      <div class="page-header">
         <h1>
            Desempenho temporal
         </h1>
      </div>
   </div>
</div>
<br>
<div class="row">
   <div class="col-sm-12" style="margin-left:15px;">
      <div class="row">
         <form id="form-filter" action="">
            <div class="col-md-3">
               <div class="form-group">
                  <label class="control-label">
                     Analistas <span class="symbol required"></span>
                  </label>
                  <select required="required" multiple="multiple" id="analistas_select" class="form-control multipleselect" name="Filiais[]">
                     <?php foreach( $analistas as $analista ) : ?>
                        <option value="<?php echo $analista->id ?>"><?php echo $analista->nome_utilizador ?></option>
                     <?php endforeach; ?>
                  </select>  
               </div>
            </div>
            <div class="col-md-3">
               <div class="form-group">
                  <label class="control-label">
                     Ano <span class="symbol required"></span>
                  </label>
                  <select id="selectAnos" name="anosAtivo" required="required" class="form-control multipleselect">
                     <option value="0"> Selecione: </option>
                     <?php foreach( $appConfig->getYearsAlive() as $ano ): ?>
                        <option value="<?php echo $ano ?>"> <?php echo $ano ?> </option>
                     <?php endforeach; ?>
                  </select>
               </div>
            </div>
         
            <div class="col-md-3" style="width:15%">
               <div class="form-group">
                  <label class="control-label">&nbsp;</label>
                  <button data-tipo-comparativo="2" type="button" class="btn btn-success btn-line-comparativo">
                     Aprovadas <i class="fa fa-thumbs-up"></i>
                  </button>
               </div>
            </div>
            <div class="col-md-3" style="width:15%">
               <div class="form-group">
                  <label class="control-label">&nbsp;</label>                  
                  <button data-tipo-comparativo="3" type="button" class="btn btn-pinterest btn-line-comparativo">
                     Reprovadas <i class="fa fa-thumbs-down"></i>
                  </button>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-md-12">
      <div class="row">
         <div id="chart_div"></div>
      </div>
   </div>
</div>

<div class="row">
   <br>
   <br>
   <br>
</div>


<input type="hidden" value="" name="" id="ipt_hdn_analista_id">

<div id="avanco-analista" class="modal fade" tabindex="-1" data-width="1200" style="display: none;">
   <div class="modal-header" style="padding:15px 15px 2px 15px!important">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4 class="modal-title">Desempenho do Analista: <strong><span id="nome_analista_bind"></span></strong> </h4>
      <div class="form-group">
         <label>Selecione um ano:</label>
         <select data-analista-id="" id="selectAnos" name="anosAtivo" class="select2">
            <option value="0"> Selecione: </option>
            <?php foreach( $appConfig->getYearsAlive() as $ano ): ?>
               <option value="<?php echo $ano ?>"> <?php echo $ano ?> </option>
            <?php endforeach; ?>
         </select>
      </div>
   </div>
   <div class="modal-body">
      <div class="row">
         <div class="col-md-1" style="width:6.2%">
            <br>
            <br>
         </div>
      </div>
      <div class="row">
         <div class="col-md-8">
            <div id="chart_div"></div>
         </div>
      </div>
   </div>
   <div class="modal-footer">
      <button data-tipo-comparativo="2" type="button" class="btn btn-success btn-line-comparativo">
         Aprovadas <i class="fa fa-thumbs-up"></i>
      </button>

      <button data-tipo-comparativo="3" type="button" class="btn btn-pinterest btn-line-comparativo">
         Reprovadas <i class="fa fa-thumbs-down"></i>
      </button>

      <button type="button" data-dismiss="modal" class="btn btn-light-grey">Fechar</button>         
   </div>
</div>

<div id="score-panel" class="modal fade" tabindex="-1" data-width="1150" style="display: none;">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
      &times;
      </button>
      <h4 class="modal-title">Score do Analista</h4>
   </div>
   <div class="modal-body">
      <div class="row">
         <div class="col-md-4">
            <div id="piechart"></div>
         </div>
         <div class="col-md-8">
            <div class="col-sm-5" style="padding:0!important;margin-left:70px;">
               <div class="block-flat">
                  <div class="content no-padding">
                     <div class="overflow-hidden">
                        <i class="fa fa-thumbs-o-up fa-4x pull-left color-success"></i> 
                        <h3 class="no-margin">APROVADO</h3>
                        <p class="color-success">Valor total aprovado</p>
                     </div>
                     <h1 id="h1-total-aprovado" class="big-text no-margin"></h1>
                  </div>
               </div>
            </div>
            <div class="col-sm-5" style="padding:0!important;margin-left:50px;">
               <div class="block-flat">
                  <div class="content no-padding">
                     <div class="overflow-hidden">
                        <i class="fa fa-thumbs-o-down fa-4x pull-left color-danger"></i> 
                        <h3 class="no-margin">REPROVADO</h3>
                        <p class="color-danger">Valor total reprovado</p>
                     </div>
                     <h1 id="h1-total-reprovado" class="big-text no-margin"></h1>
                  </div>
               </div>
            </div>

         </div>
      </div>
   </div>
   <div class="modal-footer">
      <button type="button" data-dismiss="modal" class="btn btn-light-grey">Fechar</button>      
   </div>
</div>

<style>
   #grid_financiamentos_length, #grid_financiamentos_filter{
      display: none;
   }
   table tbody tr td{
      font-style: 10px!important;
   }
   td.details-control {
      background: url('../../images/details_open.png') no-repeat center center;
      cursor: pointer;
      padding: 0 25px!important;
   }
   tr.details td.details-control {
      background: url('../../images/details_close.png') no-repeat center center;
   }
   tfoot th {
      font-weight: bold!important;
      color: #3D9400;
      font-size: 11px;
   }
   .block-flat {
      margin-bottom: 40px;
      padding: 20px 10px 0px 20px;
      background: #F6F6F6;
      border-radius: 3px;
      -webkit-border-radius: 3px;
      border-left: 1px solid #efefef;
      border-right: 1px solid #efefef;
      border-bottom: 1px solid #e2e2e2;
      box-shadow: 0 1px 0 0 rgba(0, 0, 0, 0.04);
   }
   .overflow-hidden {
      overflow: hidden;
   }
   .fa.pull-left {
      margin-right: .3em;
   }
   .color-success {
      color: #5FBF5F;
   }
   .fa-4x {
      font-size: 4em;
   }
   .fa {
      display: inline-block;
      font-family: FontAwesome;
      font-style: normal;
      font-weight: normal;
      line-height: 1;
      -webkit-font-smoothing: antialiased;
      -moz-osx-font-smoothing: grayscale;
   }
   .pull-left {
      float: left !important;
   }
   h3, .h3{
      font-weight: 300!important;
      line-height: 1.1;
      font-size: 18px!important;
   }
   .big-text {
      font-size: 30px;
      line-height: 40px !important;
      text-shadow: 2px 1px 2px rgba(0, 0, 0, 0.2);
      font-family: 'Open Sans';
      color: #555;
   }
   .color-danger {
      color: #ee5037;
   }
   .nome-analista, .fa-user{
      cursor: pointer;
   }
</style>
