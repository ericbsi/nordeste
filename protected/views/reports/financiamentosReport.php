<div class="row">
   <div class="col-sm-12">
      
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
               Relatórios
            </a>
         </li>
         <li class="active">
            Financiamentos
         </li>
      </ol>
      <div class="page-header">
         <h1>
         	Histórico de financiamentos
         </h1>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-sm-9">
      <div class="row">
         <form id="form-filter" action="">
            <div class="col-md-3" style="width:15%">
               <div class="form-group">
                  <label class="control-label">
                     Data de <span class="symbol required" aria-required="true"></span>
                  </label>                  
                  <input style="width:120px" type="text" name="data_de" class="form-control date-picker" data-date-viewmode="years" data-date-format="dd/mm/yyyy" id="data_de">
               </div>
            </div>
            <div class="col-md-3" style="width:15%">
               <div class="form-group">
                  <label class="control-label">Data até:</label>
                  <input style="width:120px" type="text" name="data_ate" class="form-control date-picker" data-date-viewmode="years" data-date-format="dd/mm/yyyy" id="data_ate">
               </div>
            </div>
            <div class="col-md-3" style="width:15%">
               <div class="form-group">
                  <label class="control-label">N° parcelas de:</label>
                  <input style="width:120px" type="text" name="qtd_parcelas_de" class="form-control" id="qtd_parcelas_de">
               </div>
            </div>
            <div class="col-md-3" style="width:15%">
               <div class="form-group">
                  <label class="control-label">N° parcelas até:</label>
                  <input style="width:120px" type="text" name="qtd_parcelas_ate" class="form-control" id="qtd_parcelas_ate">
               </div>
            </div>
            <div class="col-md-3">
               <div class="form-group">
                  <label class="control-label">
                     Parceiros <span class="symbol required"></span>
                  </label>
                  <select required="required" multiple="multiple" id="filiais_select" class="form-control multipleselect" name="Filiais[]">
                     <?php foreach ( Yii::app()->session['usuario']->getEmpresa()->listFiliais() as $f ) { ?>
                        <option value="<?php echo $f->id ?>"><?php echo $f->getConcat() ?></option>
                     <?php } ?>
                  </select>  
               </div>
            </div>
            <div class="col-md-3" style="width:15%">
               <div class="form-group">
                  <label class="control-label">&nbsp;</label>
                  <button id="btn-filter" class="btn btn-blue next-step btn-block">Filtrar</button>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-sm-12">
      <div class="col-sm-5" style="padding:0!important">
         <div id="chart_div"></div>
      </div>
      <div class="col-sm-3" style="padding:0!important">
         <div id="chart_div2"></div>
      </div>
      <div class="col-sm-3" style="padding:0!important">
         <div id="chart_div3"></div>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-sm-12">
      <table id="grid_financiamentos" class="table table-striped table-bordered table-hover table-full-width dataTable">
         <thead>
            <tr>
               <th style="width:70px!important" class="no-orderable">Cód</th>
               <th style="width:20px!important" class="no-orderable">Emissão</th>
               <th class="no-orderable">Parceiro</th>
               <th class="no-orderable">Cliente</th>
               <th class="no-orderable">CPF</th>
               <th style="width:100px!important" class="no-orderable">R$ Inicial</th>
               <th style="width:80px!important" class="no-orderable">R$ Entrada</th>
               <th style="width:80px!important" class="no-orderable">R$ Seguro</th>
               <th class="no-orderable">R$ Financiado</th>
               <th class="no-orderable">Parcelamento</th>
               <th class="no-orderable">R$ Final</th>
            </tr>
            <tfoot>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>               
               <th id="tfoot-total-inicial"></th>
               <th id="tfoot-total-ent"></th>
               <th id="tfoot-total-seg"></th>
               <th id="tfoot-total-fin"></th>
               <th></th>
               <th id="tfoot-total-final"></th>
            </tfoot>
         </thead>
      </table>
   </div>
</div>

<div class="row">
   <br>
   <br>
   <br>
</div>


<style>
   #grid_financiamentos_length, #grid_financiamentos_filter{
      display: none;
   }
   table tbody tr td{
      font-style: 10px!important;
   }
   td.details-control {
       background: url('../../images/details_open.png') no-repeat center center;
       cursor: pointer;
       padding: 0 25px!important;
   }
   tr.details td.details-control {
       background: url('../../images/details_close.png') no-repeat center center;
   }
   tfoot th {
      font-weight: bold!important;
      color: #3D9400;
      font-size: 11px;
   }
   .dropdown-menu {
      max-height: 250px;
      overflow-y: auto;
      overflow-x: hidden;
   }
</style>