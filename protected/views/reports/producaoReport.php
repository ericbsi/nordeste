<div class="row">
    <div class="col-sm-12">

        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="#">
                    Relatórios
                </a>
            </li>
            <li class="active">
                Produção
            </li>
        </ol>
        <div class="page-header">
            <h1>
                Relatório de produção
            </h1>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <button id="toggle-filter" class="col-sm-12 btn btn-success">Mostrar Opções de Filtragem</button>
    </div>
</div>
<br/><br/> 
<div class="row filtragem" style="display:none;">
    <div class="col-sm-12">
        
        <div class="row">

        </div>
        
        <div class="row">
            
            <div class="col-md-4">
                <label class="control-label">
                    Grupo de Filiais <span class="symbol required"></span>
                </label>
            </div>
        
            <div class="col-md-4">
                <label class="control-label">
                    Nucleo Filiais <span class="symbol required"></span>
                </label>
            </div>
            
            <div class="col-md-4">
                <label class="control-label">
                    Filiais <span class="symbol"></span>
                </label>
            </div>
            
        </div>
        
        <div class="row">
            
            <div class="col-md-4">
                <select required="required" multiple="multiple" id="grupos_select" class="form-control search-select multipleselect" >
                    <?php foreach (GrupoFiliais::model()->listarParaProducao() as $gf)
                    { ?>
                        <option value="<?php echo $gf->id ?>"><?php echo strtoupper($gf->nome_fantasia) ?></option>
                    <?php } ?>
                </select>  
            </div>
            
            <div class="col-md-4">
                <select required="required" multiple="multiple" id="nucleos_select" class="form-control search-select multipleselect" >
                    <?php foreach (NucleoFiliais::model()->listarParaProducao() as $nf)
                    { ?>
                        <option value="<?php echo $nf->id ?>"><?php echo strtoupper($nf->nome) ?></option>
                    <?php } ?>
                </select>  
            </div>
            
            <div class="col-md-4">
                <select required="required" multiple="multiple" id="filiais_select" class="form-control multipleselect" name="Filiais[]">
                    <?php foreach (Filial::model()->listarParaProducao() as $ap)
                    { ?>
                        <option value="<?php echo $ap->id ?>"><?php echo strtoupper($ap->getConcat()) ?></option>
                    <?php } ?>
                </select>  
            </div>
            
        </div>
        
        <div class="row">
            
<!--        <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">
                        Ambiente <span class="symbol"></span>
                    </label>
                    <select required="required" id="ambiente_select" multiple="multiple" class="form-control multipleselect" name="ambiente">
                        <option value="1">S-1</option>
                        <option value="2">S-2</option>
                    </select>  
                </div>
            </div>-->

            <div class="col-md-6">
                <label class="control-label">
                    Serviço <span class="symbol"></span>
                </label>
            </div>

            <div class="col-md-6">
                <label class="control-label">
                    Tipo de financiamento <span class="symbol"></span>
                </label>
            </div>
        </div>
        
        <div class="row">

            <div class="col-md-6">                    
                <select required="required" id="servicos_select" multiple="multiple" class="form-control" name="servicos">

                    <option value="5">
                        CDC
                    </option>
                    <option value="11">
                        CDC Lecca
                    </option>
                    <option value="10">
                        CSC
                    </option>

                </select>  
            </div>

            <div class="col-md-6" id="divTipoFin">
                <select required="required" id="tipo_fin_select" multiple="multiple" class="form-control" name="tipo_fin">

                    <option value="1">
                        Juros
                    </option>
                    <option value="2">
                        Retenção
                    </option>

                </select>  
            </div>
        </div>
        
        <div class="row">
            
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">
                        Data de <span class="symbol required" aria-required="true"></span>
                    </label>                  
                    <input type="text" name="data_de" class="form-control date-picker" data-date-viewmode="years" data-date-format="dd/mm/yyyy" id="data_de">
                </div>
            </div>
            
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Data até:</label>
                    <input type="text" name="data_ate" class="form-control date-picker" data-date-viewmode="years" data-date-format="dd/mm/yyyy" id="data_ate">
                </div>
            </div>
            
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">
                        Valor de <span class="symbol"></span>
                    </label>                  
                    <input type="text" name="valor_de" class="form-control dinheiro" id="valor_de">
                </div>
            </div>
            
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">
                        Valor até <span class="symbol" ></span>
                    </label>                  
                    <input type="text" name="valor_ate" class="form-control dinheiro" id="valor_ate">
                </div>
            </div>
            
        </div>
        
        <div class="row">
            <form id="form-filter" action="">
                <div class="col-md-4" style="width:33.3%">
                    <div class="form-group">
                        <label class="control-label">&nbsp;</label>
                        <button id="btn-filter" class="btn btn-blue next-step btn-block">Filtrar</button>                  
                    </div>
                </div>
                <div class="col-md-4" style="width:33.3%">
                    <div class="form-group">
                        <label class="control-label">&nbsp;</label>
                        <button id="btn-print-capa" class="btn btn-blue next-step btn-block">Imprimir capa</button>
                    </div>
                </div>
                <div class="col-md-4" style="width:33.3%">
                    <div class="form-group">
                        <label class="control-label">&nbsp;</label>
                        <button id="btn-print-rel" class="btn btn-blue next-step btn-block">Imprimir relatório</button>
                    </div>
                </div>
            </form>
        </div>
        
    </div>
</div>

<form method="POST" id="form-print-capa" action="/printer/printProducaoCapa/" target="_blank">
    <input type="hidden" name="hdnDataDe" id="iptHdnDataDe">
    <input type="hidden" name="hdnDataAte" id="iptHdnDataAte">
</form>
<form method="POST" id="form-print-all" action="/printer/printProducaoAll/" target="_blank">
    <input type="hidden" name="hdnDataDe"     id="iptHdnData_De">
    <input type="hidden" name="hdnDataAte"    id="iptHdnData_Ate">
    <input type="hidden" name="hdnFiliais[]"  id="iptHdnFiliais">
</form>

<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-5" style="padding:0!important">
            <div id="chart_div"></div>
        </div>
        <div class="col-sm-3" style="padding:0!important">
            <div id="chart_div2"></div>
        </div>
        <div class="col-sm-3" style="padding:0!important">
            <div id="chart_div3"></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <table id="grid_producao" class="table table-striped table-bordered table-hover table-full-width dataTable">
            <thead>
                <tr>
                    <th style="width:70px!important" class="no-orderable">
                        Cód
                    </th>
                    <th style="width:70px!important" class="no-orderable">
                        Financeira
                    </th>
                    <th style="width:70px!important" class="no-orderable">
                        Serviço
                    </th>
                    <th style="width:20px!important" class="no-orderable">
                        Emissão
                    </th>
                    <th class="no-orderable">
                        Filial
                    </th>
                    <th class="no-orderable">
                        Cliente
                    </th>
                    <th class="no-orderable">
                        CPF
                    </th>
                    <th style="width:100px!important" class="no-orderable">
                        R$ Inicial
                    </th>
                    <th style="width:80px!important" class="no-orderable">
                        R$ Entrada
                    </th>
                    <th style="width:80px!important" class="no-orderable">
                        R$ Seguro
                    </th>
                    <th style="width:80px!important" class="no-orderable">
                        Carência
                    </th>
                    <th class="no-orderable">
                        R$ Financiado
                    </th>
                    <th class="no-orderable">
                        Parcelamento
                    </th>
                    <th class="no-orderable">
                        R$ Final
                    </th>
                </tr>
            </thead>
            <tfoot>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th id="tfoot-total-ini"></th>
                <th id="tfoot-total-entradas"></th>
                <th id="tfoot-total-seguro"></th>
                <th></th>
                <th id="tfoot-total"></th>
                <th></th>
                <th id="tfoot-total-geral"></th>
            </tfoot>
        </table>
    </div>
</div>

<div class="row">
    <br>
    <br>
    <br>
</div>

<style>
    #grid_producao_length, #grid_producao_filter{
        display: none;
    }

    .dropdown-menu {
        max-height: 250px;
        overflow-y: auto;
        overflow-x: hidden;
    }

    td.details-control {
        background: url('../../images/details_open.png') no-repeat center center;
        cursor: pointer;
        padding: 0 25px!important;
    }
    tr.details td.details-control {
        background: url('../../images/details_close.png') no-repeat center center;
    }
    tfoot th {
        color: #3D9400;
        font-size: 10px;
    }

    #grid_producao tbody td{
        font-size:11px!important;
    }

</style>