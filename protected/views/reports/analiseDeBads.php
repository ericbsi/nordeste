<div class="row">
   <div class="col-sm-12">
      
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="<?php echo Yii::app()->getBaseUrl(true) .'/'. Yii::app()->session['usuario']->getRole()->login_redirect;?>">
               Administrador de Filiais
            </a>
         </li>
         <li class="active">
            Relatórios
         </li>
      </ol>
      <div class="page-header">
         <h1>
            Análise de Bads <span id="labelSafra"></span>
         </h1>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-sm-11">
      <div class="row">
         <form>
            <div class="col-md-2" style="width:13.7%!important">
               <div class="form-group">
                  <label class="control-label">
                     Mês <span class="symbol required"></span>
                  </label>
                  <select required="required" id="meses_select" class="form-control multipleselect" name="Meses[]">
                     <option value="01">Janeiro</option>
                     <option value="02">Fevereiro</option>
                     <option value="03">Março</option>
                     <option value="04">Abril</option>
                     <option value="05">Maio</option>
                     <option value="06">Junho</option>
                     <option value="07">Julho</option>
                     <option value="08">Agosto</option>
                     <option value="09">Setembro</option>
                     <option value="10">Outubro</option>
                     <option value="11">Novembro</option>
                     <option value="12">Dezembro</option>
                  </select>  
               </div>
            </div>
            <div class="col-md-2" style="width:13.7%!important">
               <div class="form-group">
                  <label class="control-label">
                     Ano <span class="symbol required"></span>
                  </label>
                  <select required="required" id="anos_select" class="form-control multipleselect" name="Anos[]">
                     <option value="2014">2014</option>
                     <option value="2015">2015</option>
                  </select>  
               </div>
            </div>
            <div class="col-md-3" style="width:19%!important;">
               <div class="form-group">
                  <label class="control-label">
                     Parceiro <span class="symbol required"></span>
                  </label>
                  <select multiple="multiple" required="required" id="parceiros_select" class="form-control multipleselect" name="Parceiros[]">
                     <?php foreach( Filial::model()->findAll() as $parceiro ) { ?>
                        <option value="<?php echo $parceiro->id ?>"><?php echo $parceiro->getConcat(); ?></option>
                     <?php } ?>
                  </select>  
               </div>
            </div>
            <div class="col-md-3" style="width:19%!important;">
               <div class="form-group">
                  <label class="control-label">
                     Analista <span class="symbol required"></span>
                  </label>
                  <select required="required" id="analistas_select" class="form-control multipleselect" name="Analistas">
                     <option value="0"> Selecione: </option>
                     <?php foreach( Usuario::model()->findAll('tipo_id = 4 AND habilitado') as $analista ) { ?>
                        <option value="<?php echo $analista->id ?>"><?php echo strtoupper( $analista->nome_utilizador ); ?></option>
                     <?php } ?>
                  </select>  
               </div>
            </div>
            <div class="col-md-3" style="width:14%">
               <div class="form-group">
                  <label class="control-label">&nbsp;</label>
                  <button id="btn-filter" class="btn btn-blue next-step btn-block">Filtrar</button>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-sm-3">
      <button id="btn-trigger-print" style="width:50px;height:40px" class="submit"><i style="font-size:23px" class="fa fa-print"></i></button>
   </div>

   <form id="form-print" action="/printer/printBads/" target="_blank" method="POST">
      <input type="hidden" name="mes" id="mesPrinter">
      <input type="hidden" name="ano" id="anoPrinter">
      <input type="hidden" name="analista" id="analistaPrinter" value="0">

      <input type="hidden" name="Parceiros[]" value="0">
   </form>

</div>
<br>
<div class="row" style="margin-bottom:50px">
   <div class="col-sm-12">
      <table id="grid_bads" class="table table-striped table-bordered table-hover table-full-width dataTable">
         <thead>
            <tr>
              <th width="20"  class="no-orderable">Bad</th>
              <th width="110" class="no-orderable">Período</th>
              <th width="110" class="no-orderable">R$ Total</th>
              <th width="370" class="no-orderable">% Pago</th>
              <th width="370" class="no-orderable">% Bad</th>
            </tr>
         </thead>
         <tbody></tbody>
         <tfoot>
            <tr>
               <th></th>
               <th></th>
               <th id="total-producao"></th>
               <th></th>
               <th></th>
            </tr>
         </tfoot>
      </table>
   </div>
</div>

<style type="text/css">
   .dropdown-menu {
       max-height: 250px;
       overflow-y: auto;
       overflow-x: hidden;
   }
   #grid_bads_filter, #grid_bads_length{
      display: none;
   }
   table.table thead .sorting_asc{
      background: none!important
   }
   tfoot tr th{
      font-size: 10px;
      color: #3D9400!important;
      font-weight: bold;
   }
</style>