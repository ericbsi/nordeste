<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/financeiro/">
                    Financeiro
                </a>
            </li>
            <li class="active">
                Liberar Pagamento
            </li>
        </ol>
    </div>
</div>

<div class="row">
    
    <div class="col-sm-12">

        <div class="panel panel-default">

            <div class="panel-body">

                <div class="row">

                    <div class="col-sm-3">
                        <form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/financeiro/contasPagar">
                            <button class="btn btn-icon btn-block">
                                <i class="fa fa-money"></i>
                                Contas a Pagar
                            </button>
                        </form>
                    </div>

                    <div class="col-sm-3">
                        <form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/financeiro/contasPagas">
                           <button class="btn btn-icon btn-block">
                                <i class="fa fa-barcode"></i>
                                Pagamentos Efetuados
                            </button>
                        </form>
                    </div>

                    <div class="col-sm-3">
                        <form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/emprestimo/pagarEmprestimo">
                            <button class="btn btn-icon btn-block">
                                <i class="clip-transfer"></i>
                                Pagar Empréstimos
                            </button>
                        </form>                            
                    </div>

                    <div class="col-sm-3">
                        <form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/emprestimo/liberarPagamento">
                            <button class="btn btn-icon btn-block"  disabled="disabled">
                                <i class="clip-transfer"></i>
                                Liberar Pagamento
                            </button>
                        </form>                            
                    </div>

                </div>
            </div>
        </div>

    </div>
    
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">

            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i>
                Liberar Pagamento
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse expand" href="#">
                    </a>
                </div>
            </div>

            <div class="panel-body collapse">
                <input type="checkbox" id="filter_semear" style="transform: scale(1.5); padding: 50px; margin: 10px;" />
                Propostas Semear
                <table id="grid_liberar" 
                       class="table table-striped table-bordered table-hover table-full-width dataTable">

                    <thead>
                        <tr>
                            <th width="3%!important">
                            </th>
                            <th>
                                Proposta
                            </th>
                            <th>
                                Loja
                            </th>
                            <th>
                                Crediarista
                            </th>
                            <th>
                                Data
                            </th>
                            <th>
                                Cliente
                            </th>
                            <th>
                                CPF
                            </th>
                            <th>
                                Dados Bancários
                            </th>
                            <th>
                                Valor
                            </th>
                            <th width="3%!important">
                            </th>
                        </tr>
                    </thead>

                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    #grid_liberar_length, #grid_liberar_filter
    {
       display: none!important;
    }
    td.details-control 
    {
        background: url('../../images/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control 
    {
        background: url('../../images/details_close.png') no-repeat center center;
    }
</style>