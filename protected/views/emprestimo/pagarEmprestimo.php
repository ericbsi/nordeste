<?php 

$tituloPagina   = "Pagamentos de Empréstimo"    ;
$colunaPagar    = "Pagar"                       ;
$vPagarReceber  = "Empréstimo a Pagar"          ;
$ehFinanceiro   = true                          ;

?>


<div class="row">
   <div class="col-sm-12">
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/financeiro/">
               Financeiro
            </a>
         </li>
         <li class="active">
            <?php echo $tituloPagina; ?>
         </li>
      </ol>
   </div>
</div>

<?php if($ehFinanceiro) {?>
    <div class="row">
        <div class="col-sm-12">

            <div class="panel panel-default">

                <div class="panel-body">

                    <div class="row">

                        <div class="col-sm-3">
                            <form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/financeiro/contasPagar">
                                <button class="btn btn-icon btn-block">
                                    <i class="fa fa-money"></i>
                                    Contas a Pagar
                                </button>
                            </form>
                        </div>

                        <div class="col-sm-3">
                            <form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/financeiro/contasPagas">
                               <button class="btn btn-icon btn-block">
                                    <i class="fa fa-barcode"></i>
                                    Pagamentos Efetuados
                                </button>
                            </form>
                        </div>

                        <div class="col-sm-3">
                            <form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/emprestimo/pagarEmprestimo">
                                <button class="btn btn-icon btn-block"  disabled="disabled">
                                    <i class="clip-transfer"></i>
                                    Pagar Empréstimos
                                </button>
                            </form>                            
                        </div>

                        <div class="col-sm-3">
                            <form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/emprestimo/liberarPagamento">
                                <button class="btn btn-icon btn-block">
                                    <i class="clip-transfer"></i>
                                    Liberar Pagamento
                                </button>
                            </form>                            
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
<?php }else{ ?>
    <br>
<?php } ?>
    
<br>

    <div class="row" id="divTablePagar">
        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i>
                    <?php echo $vPagarReceber; ?>
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-collapse expand" href="#">
                        </a>
                    </div>
                </div>

                <div class="panel-body collapse">
                    <table id="grid_pagar" 
                           class="table table-striped table-bordered table-hover table-full-width dataTable">

                        <thead>
                            <tr>
                                <th width="3%!important">
                                    Pagar
                                </th>
                                <th>
                                    Proposta
                                </th>
                                <th>
                                    Data Proposta
                                </th>
                                <th>
                                    Data Liberação
                                </th>
                                <th>
                                    Cliente
                                </th>
                                <th>
                                    CPF
                                </th>
                                <th>
                                    Dados Bancários
                                </th>
                                <th>
                                    Valor
                                </th>
                                <th>
                                    
                                </th>
                                <th>
                                    
                                </th>
                            </tr>
                        </thead>

                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

<br>

<div class="row" id="divTablePagos">
    <div class="col-sm-12">
        <div class="panel panel-default">

            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i>
                Empréstimos Pagos
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse expand" href="#">
                    </a>
                </div>
            </div>

            <div class="panel-body collapse">
                <table id="grid_pagos" 
                       class="table table-striped table-bordered table-hover table-full-width dataTable">

                    <thead>
                        <tr>
                            <th width="3%!important">
                                
                            </th>
                            <th>
                                Proposta
                            </th>
                            <th>
                                Data Proposta
                            </th>
                            <th>
                                Data Liberação
                            </th>
                            <th>
                                Cliente
                            </th>
                            <th>
                                CPF
                            </th>
                            <th>
                                Dados Bancários
                            </th>
                            <th>
                                Valor
                            </th>
                            <th>
                                
                            </th>
                            <th width="3%!important">
                                Estornar
                            </th>
                        </tr>
                    </thead>

                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div id="confirmaDesfazLiberacao" class="row" style="display: none; padding : 5px; color: #5c5c5c">
    
    <div class="panel panel-default">
        
        <br>

        <div class="row" style="text-align : center; padding: 5px">

            <div class="col-sm-1" >
                <button id="voltarTablePagos" class="btn btn-red">
                    <i class="fa fa-reply"></i>
                    Não
                </button>
            </div>

            <div class="col-sm-10 page-head" >
                <h4>
                    <i class="clip-file-2"></i> 
                    Deseja mesmo voltar o empréstimo para Liberação?
                </h4>
            </div>

            <div class="col-sm-1" >

                <button id="retornarLiberar" class="btn btn-green">

                    <i class="fa fa-save"></i> Sim

                </button>

            </div>

        </div>

        <br>

        <div class="row">

            <div class="col-sm-1">
            </div>

            <div class="col-sm-10" >
                <label class="control-label">
                    Observação:
                </label>
            </div>

            <div class="col-sm-1">
            </div>

        </div>

        <div class="row">

            <div class="col-sm-1">
            </div>

            <div class="col-sm-10" >
                <textarea id="observacaoDesfaz" class="form form-control" style="width: 100%; margin-right : -50px"></textarea>
            </div>

            <div class="col-sm-1" style="margin-left : 20px">
            </div>

        </div>

        <br>

    </div>
    
</div>

<div id="infoEstorno" class="row" style="display: none; padding : 5px; color: #5c5c5c">
    
    <div class="panel panel-default">
        
        <br>
        
        <div class="row">
        
            <div class="col-md-1">
            </div>

            <div class="col-md-10">

                <div class="row panel panel-default" style="text-align : center; padding: 5px">

                    <div class="col-sm-1">
                        <button id="voltarTablePagos" class="btn btn-light-grey">
                            <i class="fa fa-reply"></i>
                            Voltar
                        </button>
                    </div>

                    <div class="col-sm-10 page-head">
                        <h4>
                            <i class="clip-file-2"></i> 
                            Informações do Estorno
                        </h4>
                    </div>

                    <div class="col-sm-1">

                        <button id="salvarEstorno" class="btn btn-green">

                            <i class="fa fa-save"></i> Salvar

                        </button>
                        
                    </div>

                </div>
            </div>

            <div class="col-md-1">

            </div>
            
        </div>
            
        <br>

        <div class="row" style="padding:5px">

            <div class="col-sm-1">
            </div>

            <div class="col-sm-2" >
                <label class="control-label">
                    Data Estorno:
                </label>
            </div>

            <div class="col-sm-2" >
                <label class="control-label">
                    Data Retorno:
                </label>
            </div>

            <div class="col-sm-2">
                <label class="control-label">
                    Nº Comprovante Estorno:
                </label>
            </div>
            
            <div class="col-sm-2">
                <label class="control-label">
                    Nº Comprovante Baixa:
                </label>
            </div>
            
            <div class="col-sm-2">
                <label class="control-label">
                    Data Baixa:
                </label>
            </div>

        </div>

        <div class="row"  style="padding:5px">

            <div class="col-sm-1">
            </div>

            <div class="col-sm-2">

                <div class="input-group">
                    
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    
                    <input type="date" id="dataEstorno" class="form form-control" value="<?php echo date('Y-m-d'); ?>" />
                    
                </div>

            </div>

            <div class="col-sm-2">

                <div class="input-group">
                    
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    
                    <input type="date" id="dataRetorno" class="form form-control" value="<?php echo date('Y-m-d'); ?>" />
                    
                </div>

            </div>

            <div class="col-sm-2">

                <div class="input-group">
                    
                    <span class="input-group-addon">
                        <i class="clip-note"></i>
                    </span>
                    
                    <input type="text" id="comprovanteEstorno" class="form form-control" />
                    
                </div>

            </div>

            <div class="col-sm-2">

                <div class="input-group">
                    
                    <span class="input-group-addon">
                        <i class="fa fa-money"></i>
                    </span>
                    
                    <input type="text" id="comprovantePagamento" class="form form-control" disabled value="" />
                    
                </div>

            </div>
           
            <div class="col-sm-2">

                <div class="input-group">
                    
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>

                    <input class="form form-control" id="dataBaixa" type="date" value="" disabled />
                    
                </div>
                
            </div>

            <div class="col-sm-1">

            </div>
            
            <input type="hidden" id="idBaixa" value="" />

        </div>
        
        <br>
        
        <div class="row">
            
            <div class="col-sm-1">
            </div>
            
            <div class="col-sm-10" >
                <label class="control-label">
                    Observação:
                </label>
            </div>
            
            <div class="col-sm-1">
            </div>
            
        </div>
        
        <div class="row">
            
            <div class="col-sm-1">
            </div>
            
            <div class="col-sm-10" >
                <textarea id="observacao" class="form form-control" style="width: 100%; margin-right : -50px"></textarea>
            </div>
            
            <div class="col-sm-1" style="margin-left : 20px">
            </div>
            
        </div>
    
        <br>
        
    </div>
    
</div>

<div id="modal_form_new_att" 
    class="modal fade"
    data-backdrop="static"
    tabindex="-1" 
    data-width="560" 
    style="display: none;">
     
    <div class="modal-header">
        <h4 class="modal-title">Anexar comprovante</h4>
    </div>

    <form action="/emprestimo/pagar/" method="POST" enctype="multipart/form-data" id="form-add-att">
        <div class="modal-body">
            <div class="row">
                <div class="row-centralize">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Arquivo:</label>
                                    
                                    <input required="required" data-icon="false" class="control-label filestyle" name="ComprovanteFile" id="ComprovanteFile" type="file" />
                                    
                                    <!--<input type="hidden" name="DadosPagamentoId"    id="DadosPagamentoId"   value="">-->
                                    
                                    <input type="hidden" name="idParcelaHdn"        id="idParcelaHdn"       value="">
                                    <input type="hidden" name="contaDebitoHdn"      id="contaDebitoHdn"     value="">
                                    <input type="hidden" name="valorPagoHdn"        id="valorPagoHdn"       value="">
                                    <input type="hidden" name="comprovanteHdn"      id="comprovanteHdn"     value="">
                                    <input type="hidden" name="dataPagamentoHdn"    id="dataPagamentoHdn"   value="">
                                    <input type="hidden" name="contaCreditoHdn"     id="contaCreditoHdn"    value="">
                                    <input type="hidden" name="observacaoHdn"       id="observacaoHdn"      value="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-blue btn-send">Salvar</button>
        </div>
     </form>
</div>

<style type="text/css">
   #grid_pagar_length, #grid_pagos_length, #grid_pagos_filter
   {
      display: none!important;
   }
</style>

<?php if(Yii::app()->session['usuario']->tipo_id !== 11){ ?>
    <style type="text/css">
        td.details-control 
        {
           background: url('../../images/cash-register.png') no-repeat center center;
           cursor: pointer;
        }
        tr.shown td.details-control 
        {
           background: url('../../images/details_close.png') no-repeat center center;
        }
        td.details-control-2 
        {
           background: url('../../images/details_open.png') no-repeat center center;
           cursor: pointer;
        }
        tr.shown td.details-control-2 
        {
           background: url('../../images/details_close.png') no-repeat center center;
        }
        
        @-webkit-keyframes glowing 
        {
            0%      { background-color: #929292; -webkit-box-shadow: 0 0 3px  #5c5c5c; }
            50%     { background-color: #929292; -webkit-box-shadow: 0 0 60px #5c5c5c; }
            100%    { background-color: #929292; -webkit-box-shadow: 0 0 3px  #5c5c5c; }
        }

        @-moz-keyframes glowing 
        {
            0%      { background-color: #929292; -moz-box-shadow: 0 0 3px  #5c5c5c; }
            50%     { background-color: #929292; -moz-box-shadow: 0 0 10px #5c5c5c; }
            100%    { background-color: #929292; -moz-box-shadow: 0 0 3px  #5c5c5c; }
        }

        @-o-keyframes glowing 
        {
            0%      { background-color: #929292; box-shadow: 0 0 3px  #5c5c5c; }
            50%     { background-color: #929292; box-shadow: 0 0 10px #5c5c5c; }
            100%    { background-color: #929292; box-shadow: 0 0 3px  #5c5c5c; }
        }

        @keyframes glowing 
        {
            0%      { background-color: #929292; box-shadow: 0 0 3px  #5c5c5c; }
            50%     { background-color: #929292; box-shadow: 0 0 10px #5c5c5c; }
            100%    { background-color: #929292; box-shadow: 0 0 3px  #5c5c5c; }
        }

        .buttonpulsate 
        {
            -webkit-animation       : glowing 1200ms infinite   ;
            -moz-animation          : glowing 1200ms infinite   ;
            -o-animation            : glowing 1200ms infinite   ;
            animation               : glowing 1200ms infinite   ;
            -webkit-border-radius   : 10px                      ;
            border-radius           : 10px                      ;
            border                  : none                      ;

            cursor                  : pointer                   ;
            display                 : inline-block              ;
            font-family             : Arial                     ;
            font-size               : 20px                      ;
            padding                 : 5px 10px                  ;
            text-align              : center                    ;
            text-decoration         : none                      ;
        }
    </style>
<?php } ?>