<?php
/* @var $this MigracaoController */
/* @var $model Migracao */

$this->breadcrumbs=array(
	'Migracaos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Migracao', 'url'=>array('index')),
	array('label'=>'Create Migracao', 'url'=>array('create')),
	array('label'=>'View Migracao', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Migracao', 'url'=>array('admin')),
);
?>

<h1>Update Migracao <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>