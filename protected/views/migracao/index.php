<?php
/* @var $this MigracaoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Migracaos',
);

$this->menu=array(
	array('label'=>'Create Migracao', 'url'=>array('create')),
	array('label'=>'Manage Migracao', 'url'=>array('admin')),
);
?>

<h1>Migracaos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
