<?php
/* @var $this MigracaoController */
/* @var $model Migracao */

$this->breadcrumbs=array(
	'Migracaos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Migracao', 'url'=>array('index')),
	array('label'=>'Create Migracao', 'url'=>array('create')),
	array('label'=>'Update Migracao', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Migracao', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Migracao', 'url'=>array('admin')),
);
?>

<h1>View Migracao #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'data',
		'usuarioId',
		'data_cadastro',
		'habilitado',
		'destino',
		'origem',
	),
)); ?>
