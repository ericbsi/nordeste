<?php
/* @var $this MigracaoController */
/* @var $model Migracao */

$this->breadcrumbs=array(
	'Migracaos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Migracao', 'url'=>array('index')),
	array('label'=>'Manage Migracao', 'url'=>array('admin')),
);
?>

<h1>Create Migracao</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>