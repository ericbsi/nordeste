<?php
/* @var $this TabelaDePrecosController */
/* @var $model TabelaDePrecos */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dataDe'); ?>
		<?php echo $form->textField($model,'dataDe'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dataAte'); ?>
		<?php echo $form->textField($model,'dataAte'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'habilitado'); ?>
		<?php echo $form->textField($model,'habilitado'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->