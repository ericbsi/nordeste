<?php
/* @var $this TabelaDePrecosController */
/* @var $model TabelaDePrecos */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tabela-de-precos-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'dataDe'); ?>
		<?php echo $form->textField($model,'dataDe'); ?>
		<?php echo $form->error($model,'dataDe'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dataAte'); ?>
		<?php echo $form->textField($model,'dataAte'); ?>
		<?php echo $form->error($model,'dataAte'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'habilitado'); ?>
		<?php echo $form->textField($model,'habilitado'); ?>
		<?php echo $form->error($model,'habilitado'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->