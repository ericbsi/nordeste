<?php
/* @var $this TabelaDePrecosController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tabela De Precoses',
);

$this->menu=array(
	array('label'=>'Create TabelaDePrecos', 'url'=>array('create')),
	array('label'=>'Manage TabelaDePrecos', 'url'=>array('admin')),
);
?>

<h1>Tabela De Precoses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
