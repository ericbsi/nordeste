<?php ?>
<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/tabelaDePrecos/cadastrarTP">
                    Tabela de Preços
                </a>
            </li>
            <li class="active">
                Cadastrar
            </li>
        </ol>
        <div class="page-header">
            <h1>
                Tabela de Preços
        </div>
    </div>
</div>
<div class="row">
    <form id="form-add-tp" action="/tabelaDePrecos/cadastrarTP/" method="post">
        <div class="modal-body">
            <div class="row">
                <div class="row-centralize">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Data inicial <span class="symbol required"></span></label>
                                    <input required name="TabelaDePrecos[dataDe]" type="date" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6" id="select-estados-wrapper">
                                <div class="form-group">
                                    <label class="control-label">Data final <span class="symbol required"></span></label>
                                    <input required name="TabelaDePrecos[dataAte]" type="date" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="row">
    <div class="col-sm-12">
        <table id="grid_ItemTabelaDePrecos" class="table table-striped table-bordered table-hover table-full-width dataTable">
            <thead>
                <tr>
                    <th>Produto</th>
                    <th>Valor</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <br>
</div>
<div class="row">
    <div class="col-md-11">
        <a href="#modal_new_ItemTabelaDePrecos" data-toggle="modal" type="submit" class="btn btn-blue">Adicionar Item</a>
        <button id="btn-salvar-tp" type="submit" class="btn btn-blue">Salvar</button>
    </div>
</div>
<div id="modal_new_ItemTabelaDePrecos" class="modal fade" tabindex="-1" data-width="960" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Inserir Preço</h4>
    </div>
    <form id="form-add-item">
        <div class="modal-body">
            <div class="row">
                <div class="row-centralize">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label class="control-label">
                                        Produto <span class="symbol required"></span>
                                    </label>
                                    <select class="form-control search-select select2" name="ItemTabelaDePrecos[ItemDoEstoque_id]" id="ItemTabelaDePrecos_ItemDoEstoque_id">
                                        <option value="">Selecione: </option>
                                        <?php foreach (ItemDoEstoque::model()->findAll() as $item) { ?>
                                            <option value="<?php echo $item->id ?>"><?php echo $item->produto->descricao ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="control-label">
                                        Valor R$ <span class="symbol required"></span>
                                    </label>
                                    <input required name="ItemTabelaDePrecos[valor]" id="valor" class="form-control input-mask-date" type="number">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <!--<label class="checkbox-inline" style="float:left">
            <input id="checkbox_continuar" type="checkbox" value="">
            Continuar Cadastrando
            </label>-->
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
            <button type="submit" id="btn-add-item" class="btn btn-blue">Salvar</button>
            <div class="row">
            </div>
            <br>
        </div>
    </form>
</div>
