<?php
/* @var $this TabelaDePrecosController */
/* @var $model TabelaDePrecos */

$this->breadcrumbs=array(
	'Tabela De Precoses'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TabelaDePrecos', 'url'=>array('index')),
	array('label'=>'Create TabelaDePrecos', 'url'=>array('create')),
	array('label'=>'Update TabelaDePrecos', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TabelaDePrecos', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TabelaDePrecos', 'url'=>array('admin')),
);
?>

<h1>View TabelaDePrecos #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'dataDe',
		'dataAte',
		'habilitado',
	),
)); ?>
