<?php
/* @var $this TabelaDePrecosController */
/* @var $model TabelaDePrecos */

$this->breadcrumbs=array(
	'Tabela De Precoses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TabelaDePrecos', 'url'=>array('index')),
	array('label'=>'Manage TabelaDePrecos', 'url'=>array('admin')),
);
?>

<h1>Create TabelaDePrecos</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>