<?php
/* @var $this TabelaDePrecosController */
/* @var $data TabelaDePrecos */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dataDe')); ?>:</b>
	<?php echo CHtml::encode($data->dataDe); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dataAte')); ?>:</b>
	<?php echo CHtml::encode($data->dataAte); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('habilitado')); ?>:</b>
	<?php echo CHtml::encode($data->habilitado); ?>
	<br />


</div>