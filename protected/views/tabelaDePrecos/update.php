<?php
/* @var $this TabelaDePrecosController */
/* @var $model TabelaDePrecos */

$this->breadcrumbs=array(
	'Tabela De Precoses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TabelaDePrecos', 'url'=>array('index')),
	array('label'=>'Create TabelaDePrecos', 'url'=>array('create')),
	array('label'=>'View TabelaDePrecos', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TabelaDePrecos', 'url'=>array('admin')),
);
?>

<h1>Update TabelaDePrecos <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>