<?php
/* @var $this CondicaoDePagamentoController */
/* @var $data CondicaoDePagamento */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nome')); ?>:</b>
	<?php echo CHtml::encode($data->nome); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descricao')); ?>:</b>
	<?php echo CHtml::encode($data->descricao); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('habilitado')); ?>:</b>
	<?php echo CHtml::encode($data->habilitado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dataInicial')); ?>:</b>
	<?php echo CHtml::encode($data->dataInicial); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dataFinal')); ?>:</b>
	<?php echo CHtml::encode($data->dataFinal); ?>
	<br />


</div>