<?php
/* @var $this CondicaoDePagamentoController */
/* @var $model CondicaoDePagamento */

$this->breadcrumbs=array(
	'Condicao De Pagamentos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List CondicaoDePagamento', 'url'=>array('index')),
	array('label'=>'Create CondicaoDePagamento', 'url'=>array('create')),
	array('label'=>'View CondicaoDePagamento', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage CondicaoDePagamento', 'url'=>array('admin')),
);
?>

<h1>Update CondicaoDePagamento <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>