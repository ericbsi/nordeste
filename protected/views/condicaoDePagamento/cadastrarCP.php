<?php ?>
<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/condicaoDePagamento/Cadastrar Condição de Pagamento">
                    Condição de Pagamento
                </a>
            </li>
            <li class="active">
                Cadastrar
            </li>
        </ol>
        <div class="page-header">
            <h1>
                Condição de pagamento
        </div>
    </div>
</div>
<div class="row">
    <form id="form-add-cp" action="/condicaoDePagamento/cadastrarCP/" method="post">
        <div class="modal-body">
            <div class="row">
                <div class="row-centralize">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label class="control-label">
                                        Nome <span class="symbol required"></span>
                                    </label>
                                    <input required name="Condicao_de_Pagamento[nome]" type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label class="control-label">
                                        Descrição <span class="symbol required"></span>
                                    </label>
                                    <input required name="Condicao_de_Pagamento[descricao]" type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Data inicial <span class="symbol required"></span></label>
                                    <input required name="Condicao_de_Pagamento[dataInicial]" type="date" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6" id="select-estados-wrapper">
                                <div class="form-group">
                                    <label class="control-label">Data final <span class="symbol required"></span></label>
                                    <input required name="Condicao_de_Pagamento[dataFinal]" type="date" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="row">
    <div class="col-sm-12">
        <table id="grid_Forma_de_Pagamento_has_Condicao_de_Pagamento" class="table table-striped table-bordered table-hover table-full-width dataTable">
            <thead>
                <tr>
                    <th>Forma de pagamento</th>
                    <th>Qtd parcelas de</th>
                    <th>Qtd parcelas até</th>
                    <th>Carência</th>
                    <th>Intervalo</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <br>
</div>
<div class="row">
    <div class="col-md-11">
        <a href="#modal_new_Forma_de_Pagamento_has_Condicao_de_Pagamento" data-toggle="modal" type="submit" class="btn btn-blue">Adicionar forma de pagamento</a>
        <button id="btn-salvar-cp" type="submit" class="btn btn-blue">Salvar</button>
    </div>
</div>
<div id="modal_new_Forma_de_Pagamento_has_Condicao_de_Pagamento" class="modal fade" tabindex="-1" data-width="960" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Inserir forma</h4>
    </div>
    <form id="form-add-condicao">
        <div class="modal-body">
            <div class="row">
                <div class="row-centralize">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label class="control-label">
                                        Forma de pagamento <span class="symbol required"></span>
                                    </label>
                                    <select class="form-control search-select select2" name="Forma_de_Pagamento_has_Condicao_de_Pagamento[Forma_de_Pagamento_id]" id="Forma_de_Pagamento_has_Condicao_de_Pagamento_Forma_de_Pagamento_id">
                                        <option value="">Selecione: </option>
                                        <?php foreach (FormaDePagamento::model()->findAll() as $fp) { ?>
                                            <option value="<?php echo $fp->id ?>"><?php echo $fp->descricao ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="control-label">
                                        Parcelas de <span class="symbol required"></span>
                                    </label>
                                    <input required name="Forma_de_Pagamento_has_Condicao_de_Pagamento[qtdParcelasDe]" id="qtdParcelasDe" class="form-control input-mask-date" type="number">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">
                                        Parcelas até <span class="symbol required"></span>
                                    </label>
                                    <input required name="Forma_de_Pagamento_has_Condicao_de_Pagamento[qtdParcelasAte]" id="qtdParcelasAte" class="form-control input-mask-date" type="number">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Carência <span class="symbol required"></span></label>
                                    <input required name="Forma_de_Pagamento_has_Condicao_de_Pagamento[carencia]" id="fp_carencia" class="form-control" type="number">
                                </div>
                            </div>
                            <div class="col-md-6" id="select-estados-wrapper">
                                <div class="form-group">
                                    <label class="control-label">Intervalo <span class="symbol required"></span></label>
                                    <input required name="Forma_de_Pagamento_has_Condicao_de_Pagamento[intervalo]" id="fp_intervalo" class="form-control" type="number">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <!--<label class="checkbox-inline" style="float:left">
            <input id="checkbox_continuar" type="checkbox" value="">
            Continuar Cadastrando
            </label>-->
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
            <button type="submit" id="btn-add-forma" class="btn btn-blue">Salvar</button>
            <div class="row">
            </div>
            <br>
        </div>
    </form>
</div>
