<?php
/* @var $this CondicaoDePagamentoController */
/* @var $model CondicaoDePagamento */

$this->breadcrumbs=array(
	'Condicao De Pagamentos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CondicaoDePagamento', 'url'=>array('index')),
	array('label'=>'Manage CondicaoDePagamento', 'url'=>array('admin')),
);
?>

<h1>Create CondicaoDePagamento</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>