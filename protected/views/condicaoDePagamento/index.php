<?php
/* @var $this CondicaoDePagamentoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Condicao De Pagamentos',
);

$this->menu=array(
	array('label'=>'Create CondicaoDePagamento', 'url'=>array('create')),
	array('label'=>'Manage CondicaoDePagamento', 'url'=>array('admin')),
);
?>

<h1>Condicao De Pagamentos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
