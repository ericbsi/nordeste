<?php
/* @var $this CondicaoDePagamentoController */
/* @var $model CondicaoDePagamento */

$this->breadcrumbs=array(
	'Condicao De Pagamentos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List CondicaoDePagamento', 'url'=>array('index')),
	array('label'=>'Create CondicaoDePagamento', 'url'=>array('create')),
	array('label'=>'Update CondicaoDePagamento', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete CondicaoDePagamento', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CondicaoDePagamento', 'url'=>array('admin')),
);
?>

<h1>View CondicaoDePagamento #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nome',
		'descricao',
		'habilitado',
		'dataInicial',
		'dataFinal',
	),
)); ?>
