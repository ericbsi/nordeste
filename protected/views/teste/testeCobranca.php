<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="#">
                    Teste
                </a>
            </li>
            <li class="active">
                Cobrança
            </li>
        </ol>
    </div>
</div>
<br>
<div class="row">
    <div class="col-sm-12">
        <button id="toggle-filter" class="col-sm-12 btn btn-success">Mostrar/Ocultar Opções de Filtragem</button>
    </div>
</div>
<br />
<div class="row filtragem">
    <div class="col-sm-9">
        <form>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label">
                            Grupo de Filiais <span class="symbol required"></span>
                        </label>

                        <select required="required" multiple="multiple" id="selectGrupoFiliais" class="form-control search-select multipleselect" >
                            <?php foreach (GrupoFiliais::model()->listarParaCobranca() as $gf) { ?>
                                <option value="<?php echo $gf->id ?>"><?php echo strtoupper($gf->nome_fantasia) ?></option>
                            <?php } ?>
                        </select>  
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label">
                            Nucleo Filiais <span class="symbol required"></span>
                        </label>

                        <select required="required" multiple="multiple" id="nucleoFiliaisSelect" class="form-control search-select multipleselect" >
                            <?php foreach (NucleoFiliais::model()->listarParaCobranca() as $nf) { ?>
                                <option value="<?php echo $nf->id ?>"><?php echo strtoupper($nf->nome) ?></option>
                            <?php } ?>
                        </select>  

                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label">
                            Filiais <span class="symbol required"></span>
                        </label>

                        <select required="required" multiple="multiple" id="selectFiliais" class="form-control search-select multipleselect" >
                            <?php foreach (Filial::model()->listarParaCobranca() as $ap) { ?>
                                <option value="<?php echo $ap->id ?>"><?php echo strtoupper($ap->getConcat()) ?></option>
                            <?php } ?>
                        </select>  
                    </div>
                </div>
            </div>
            <div class="row filtragem">
                <div class="col-sm-5">
                    <div class="form-group">
                        <label class="control-label">
                            De:
                        </label>                  
                        <input type="date" name="data_de" class="form-control date-picker" data-date-viewmode="years" data-date-format="dd/mm/yyyy" id="data_de">
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group">
                        <label class="control-label">
                            Até:
                        </label>
                        <input type="date" name="data_ate" class="form-control date-picker" data-date-viewmode="years" data-date-format="dd/mm/yyyy" id="data_ate">
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="form-group">
                        <label class="control-label">&nbsp;</label>
                        <button id="btn-filter" class="btn btn-blue next-step btn-block">Filtrar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <table id="gridCobranca" class="table table-striped table-bordered table-hover table-full-width dataTable">
            <thead>
                <tr>
                    <th></th>
                    <th class="no-orderable">
                        CPF
                    </th>
                    <th class="no-orderable">
                        Nome
                    </th>
                    <th class="no-orderable">
                        Qtd Atrasos
                    </th>
                    <th class="no-orderable">
                        Maior Atraso
                    </th>
                </tr>
            </thead>

            <tbody>
            </tbody>

        </table>
    </div>
</div>
<br /> <br />
<style>
    .filtragem {
        display: none;
    }
    .dropdown-menu {
        max-height: 250px;
        overflow-y: auto;
        overflow-x: hidden;
    }
    #gridCobranca_length, #gridCobranca_filter{
        display: none;
    }

    #gridParcelas_length, #gridParcelas_filter{
        display: none;
    }

    td.details-control {
        background: url('../../images/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('../../images/details_close.png') no-repeat center center;
    }
</style>