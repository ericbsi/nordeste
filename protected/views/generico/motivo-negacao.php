<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/generico/">
                    Acompanhamento
                </a>
            </li>
            <li class="active">
                Parceiros
            </li>
        </ol>
        <div class="page-header">
            <h1>
                Motivo Negação
            </h1>
        </div>
    </div>
</div>
<div class="row" style="padding-bottom:40px; text-align: center;">
    <div class="col-sm-12">
        <form class="form-horizontal">
            <div class="form-inline">
                <label class="sr-only" for="nomeParceiro">Nome do parceiro</label>
                <input style="width:500px; border-radius: 10px!important;" type="text" class="form-control" id="descricao" placeholder="Descrição">
               <button style="width:100px; border-radius: 10px!important;" class="btn btn-green" id="novoMotivo">Cadastrar</button>
                
            </div>
        </form>
        <br>
    </div>
</div>

<div class="row" style="padding-bottom:40px; text-align: center;">
    <div class="col-sm-12">
        <table id="grid_motivo" 
               class="table table-striped table-bordered table-hover table-full-width dataTable"
               style="width: 100%;">
            <thead>
                <tr>
                    <th width="200px">
                        Descrição
                    </th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<div data-width="70%" id="cadastro-ploja" class="modal fade">
    <div class="modal-body" style="text-align:center;">
        <h4> Contatos do Parceiro </h4>
        <br />
        <form class="form-horizontal">
            <div class="form-group">
                <input type="text" 
                       class="form-control" 
                       id="cidLoja" 
                       placeholder="Digite a cidade"
                       style="text-align:center; border-radius: 20px!important;">
            </div>
            <div class="form-group">
                <input type="text" 
                       class="form-control" 
                       id="telLoja" 
                       placeholder="Digite o telefone"
                       style="text-align:center; border-radius: 20px!important;">
            </div>
            <div class="form-group">
                <input type="email" 
                       class="form-control" 
                       id="emailLoja" 
                       placeholder="Digite o email"
                       style="text-align:center; border-radius: 20px!important;">
            </div>
            <button id="cadastrar_ploja" class="btn btn-success">Cadastrar</button>
        </form>
        <br />
    </div>
</div>

<div data-width="50%" id="obs-ploja" class="modal fade">
    <div class="modal-body" style="text-align:center;">
        <h4> Observações </h4>
        <br />
        <form class="form-horizontal">
            <div class="form-group">
                <textarea id="area_obs" rows="5" cols="80"></textarea> 
            </div>
            <button id="altobs_ploja" class="btn btn-success">Atualizar</button>
        </form>
        <br />
    </div>
</div>
<style type="text/css">
    td.details-control {
        background: url('../../images/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('../../images/details_close.png') no-repeat center center;
    }
</style>

<style type="text/css">
    #grid_parceiros_length, #grid_parceiros_paginate,
    #grid_motivo_length, #grid_motivo_filter,
    #grid_motivo_info, #grid_motivo_paginate,
    #grid_motivo_previous,
    #grid_parceiros_info, #grid_parceiros_filter{
        display: none!important;
    }
    .panel{
        background: transparent!important;
        border:none!important;
    }
    #selectSFilter{
        border: 1px solid;
    }
</style>