<?php  ?>
<div class="row">
   <div class="col-sm-12">
      
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
               Financeiro
            </a>
         </li>
         <li class="active">
            Repasses
         </li>
      </ol>
      <div class="page-header">
         <h1>
         	Recebimentos - Credshow Operadora de Crédito S/A<small><?php //echo Yii::app()->session['usuario']->getEmpresa()->razao_social; ?></small>
         </h1>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-sm-9">
      <div class="row">
         <form id="form-filter" action="">
            <div class="col-md-3" style="width:15%">
               <div class="form-group">
                  <label class="control-label">
                     Data de <span class="symbol required" aria-required="true"></span>
                  </label>                  
                  <input style="width:120px" type="text" name="data_de" class="form-control date-picker" data-date-viewmode="years" data-date-format="dd/mm/yyyy" id="data_de">
               </div>
            </div>
            <div class="col-md-3" style="width:15%">
               <div class="form-group">
                  <label class="control-label">Data até:</label>
                  <input style="width:120px" type="text" name="data_ate" class="form-control date-picker" data-date-viewmode="years" data-date-format="dd/mm/yyyy" id="data_ate">
                  <input type="hidden" name="Filial_id" id="Filial_id" value="20">
               </div>
            </div>
            <div class="col-md-3">
               <div class="form-group">
                  <label class="control-label">
                     Parceiros <span class="symbol required"></span>
                  </label>
                  <select required="required" multiple="multiple" id="filiais_select" class="form-control multipleselect" name="Filiais[]">
                     <?php foreach ( Yii::app()->session['usuario']->getEmpresa()->listFiliais() as $f ) { ?>
                     	<?php if( !$f->isMatriz ){ ?>
                        	<option value="<?php echo $f->id ?>"><?php echo strtoupper($f->getConcat()) ?></option>
                        <?php } ?>
                     <?php } ?>
                  </select>  
               </div>
            </div>
            <div class="col-md-3" style="width:15%">
               <div class="form-group">
                  <label class="control-label">&nbsp;</label>
                  <button id="btn-filter" class="btn btn-blue next-step btn-block">Filtrar</button>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-sm-12">
      <table id="grid_repasses" class="table table-striped table-bordered table-hover table-full-width dataTable">
         <thead>
            <tr>
               <th class="no-orderable">Parceiro</th>
               <th class="no-orderable">Valor</th>
               <!--<th class="no-orderable">CPF</th>
               <th class="no-orderable">Filial</th>
               <th class="no-orderable">Parcela</th>
               <th style="width:50px" class="no-orderable">Vencimento da parcela</th>
               <th style="width:50px" class="no-orderable">Data da baixa</th>
               <th style="width:80px" class="no-orderable">R$ Valor da parcela</th>
               <th style="width:150px" class="no-orderable">R$ Valor pago</th>-->
            </tr>
         </thead>
         <tfoot>
               <th></th>
               <!--<th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th id="tfoot-total-atraso"></th>-->
         </tfoot>
      </table>
   </div>
</div>

<style type="text/css">
	#grid_repasses_length, #grid_repasses_filter{
		display: none;
	}
   tfoot th {
      font-weight: bold!important;
      color: #3D9400;
      font-size: 11px;
   }
</style>