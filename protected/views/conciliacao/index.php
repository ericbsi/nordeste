<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/conciliacao/">
                    Conciliação
                </a>
            </li>
            <li class="active">
                Conciliar
            </li>
        </ol>
    </div>
</div>

<p></p>

<div class="row" id="painelArquivo">
    <div class="col-sm-12">
        <div class="row">

            <form id="upload-arquivo-form" action="/conciliacao/importarArquivo/" method="post" enctype="multipart/form-data">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Arquivo de Conciliação:</label>
                        <input data-icon="false" 
                               class="control-label filestyle" 
                               required="required" 
                               name="FileInput" 
                               id="FileInput" 
                               type="file" 
                               accept=".csv"/>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <label class="control-label">&nbsp;</label>
                        <button class="btn btn-blue next-step btn-block">Importar</button>
                    </div>
                </div>
                
            
                <div class="col-sm-7">
                    <div id="progressbox" width="100%">
                        <div id="progressbar"></div >
                        <div id="statustxt">0%</div>
                    </div>
                    <div id="cadastro_anexo_msg_return" class="alert" style="text-align:left; display:none"></div>
                </div>
                
            </form>

        </div>
    </div>
</div>

<br>

<div class="row">
    <div class="col-sm-12"  style="text-align: left">

        <font color="gray">
        <b>Legendas: </b>
        </font>

        <i class="clip-circle-2" style="color: blue"></i>
        <font color="blue">
        <b>Cabeçalho Omni</b>
        </font>

        <i class="clip-circle-2" style="color: green"></i>
        <font color="green">
        <b>Cabeçalho Sigac</b>
        </font>

        <i class="clip-circle-2" style="color: red"></i>
        <font color="red">
        <b>Linhas com divergências</b>
        </font>

    </div>
</div>

<div class="row">

    <div class="col-sm-12" style="text-align: right">

        <font color="gray">
        <b>Divergências: </b>
        </font>

        <i class="clip-checkbox-unchecked-2 checkFiltro"
           style="color: limegreen"
           id="checkFiltroTodas"
           value="0"></i>
        <font color="limegreen">
        <b>Todas</b>
        </font>

        <i class="clip-checkbox-unchecked-2 checkFiltro"
           style="color: crimson"
           id="checkFiltroCGC"
           value="2"></i>
        <font color="crimson">
        <b>CGC</b>
        </font>

        <i class="clip-checkbox-unchecked-2 checkFiltro"
           style="color: blueviolet"
           id="checkFiltroNome"
           value="3"></i>
        <font color="blueviolet">
        <b>Nome</b>
        </font>

        <i class="clip-checkbox-unchecked-2 checkFiltro"
           style="color: chocolate"
           id="checkFiltroValor"
           value="4"></i>
        <font color="chocolate">
        <b>Valor</b>
        </font>

        <i class="clip-checkbox-unchecked-2 checkFiltro"
           style="color: tomato"
           id="checkFiltroValor"
           value="5"></i>
        <font color="tomato">
        <b>Sem Conciliação</b>
        </font>
    </div>
    
</div>

<div class="row">
    
    <div class="col-sm-12">
        
        <table id="gridConciliacao" 
               class="table table-striped table-bordered table-hover table-full-width dataTable">
            <thead>
                <tr>
                    <th>
                        <input 
                            style="color: blue"
                            class="filtro form form-control"
                            type="text" 
                            placeholder="Filtre Contrato..." 
                            id="filtroContrato" />
                    </th>
                    <th>
                        <input 
                            style="color: green"
                            class="filtro form form-control"
                            type="text" 
                            placeholder="Filtre Código..." 
                            id="filtroCodigo" />
                    </th>
                    <th>
                        <input 
                            style="color: green"
                            class="filtro form form-control"
                            type="text" 
                            placeholder="Filtre Loja..." 
                            id="filtroLoja" />
                    </th>
                    <th>
                        <input 
                            style="color: green"
                            class="filtro form form-control"
                            type="date" 
                            placeholder="Filtre Data de..." 
                            id="filtroDataSigacDe" 
                            value="" />
                    </th>
                    <th>
                        <input 
                            style="color: blue"
                            class="filtro form form-control"
                            type="text" 
                            placeholder="Filtre CGC..." 
                            id="filtroCGCOmni" />
                    </th>
                    <th>
                        <input 
                            style="color: green"
                            class="filtro form form-control"
                            type="text" 
                            placeholder="Filtre CGC..." 
                            id="filtroCGCSigac" />
                    </th>
                    <th>
                        <input 
                            style="color: blue"
                            class="filtro form form-control"
                            type="text" 
                            placeholder="Filtre Nome..." 
                            id="filtroNomeOmni" />
                    </th>
                    <th>
                        <input 
                            style="color: green"
                            class="filtro form form-control"
                            type="text" 
                            placeholder="Filtre Nome..." 
                            id="filtroNomeSigac" />
                    </th>
                    <th></th>
                    <th></th>
                </tr>
                <tr>
                    <th class="no-orderable" style="color : blue">
                        Contrato Proposta
                    </th>
                    <th class="no-orderable" style="color : green">
                        Código
                    </th>
                    <th class="no-orderable" style="color : green">
                        Loja
                    </th>
                    <th class="no-orderable" style="color : green">
                        Data 
                    </th>
                    <th class="no-orderable" style="color : blue">
                        CGC Cliente 
                    </th>
                    <th class="no-orderable" style="color : green">
                        CGC Cliente
                    </th>
                    <th class="no-orderable" style="color : blue">
                        Nome Cliente 
                    </th>
                    <th class="no-orderable" style="color : green">
                        Nome Cliente
                    </th>
                    <th class="no-orderable" style="color : blue">
                        Valor 
                    </th>
                    <th class="no-orderable" style="color : green">
                        Valor
                    </th>
                </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="3">
                    </th>
                    <th>
                        <input 
                            style="color: green"
                            class="filtro form form-control"
                            type="date" 
                            placeholder="Filtre Data ate..." 
                            id="filtroDataSigacAte"
                            value="" />
                    </th>
                    <th colspan="4" style="text-align: right">
                        Total Filtrado
                    </th>
                    <th>
                        <font id="thTotalOmniFiltro"  color="blue"></font>
                    </th>
                    <th>
                        <font id="thTotalSigacFiltro" color="green"></font>
                    </th>
                </tr>
                <tr>
                    <th colspan="8" style="text-align: right">
                        Total Geral
                    </th>
                    <th>
                        <font id="thTotalOmni"  color="blue"></font>
                    </th>
                    <th>
                        <font id="thTotalSigac" color="green"></font>
                    </th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

<br>
<br>

<style type="text/css">
    #gridConciliacao_filter, #gridConciliacao_length{
        display: none;
    }
    td.details-control {
        background: url('../../images/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('../../images/details_close.png') no-repeat center center;
    }
   #progressbox {
      border: 1px solid #4cae4c;
      padding: 1px; 
      position:relative;
      border-radius: 3px;
      margin: 10px;
      margin-left: 0;
      display:none;
      text-align:left;
   }
   #progressbar {
      height:20px;
      border-radius: 3px;
      background-color: #4cae4c;
      width:1%;
   }
   #statustxt {
      top:3px;
      left:50%;
      position:absolute;
      display:inline-block;
      color: #FFF;
      font-size: 11px;
      font-family: arial
   }
</style>