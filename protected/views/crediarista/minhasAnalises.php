<?php  ?>
<div class="row">
   <div class="col-sm-12">
      
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
               Análises
            </a>
         </li>
         <li class="active">
            Minhas análises
         </li>
      </ol>
      <div class="page-header">
         <h1>
         	Meu histórico de análises
         </h1>
      </div>
   </div>
</div>
<form>
	<input type="hidden" value="<?php echo Yii::app()->session['usuario']->id ?>" id="userId">
</form>
<div class="row">
	<div class="col-sm-12">
		<table id="grid_analises" class="table table-striped table-bordered table-hover table-full-width dataTable">
		    <thead>
		    	<tr>
			       <th></th>
			       <th>Cód</th>
	               <th>Cliente</th>
	               <th>Valor</th>
	               <th>Entrada</th>
	               <th>Data</th>
		        </tr>
		    </thead>
		    <tbody>
		    
			</tbody>
		</table>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<div id="chart_div"></div>
	</div>
</div>

<style type="text/css">
	#grid_analises_length, #grid_analises_filter{
		display: none;
	}
	td.details-control {
	    background: url('../../images/details_open.png') no-repeat center center;
	    cursor: pointer;
	}
	tr.details td.details-control {
	    background: url('../../images/details_close.png') no-repeat center center;
	}
</style>