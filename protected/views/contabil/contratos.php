<div class="row">
   <div class="col-sm-12">
      
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
               Contábil
            </a>
         </li>
         <li class="active">
            Relatórios
         </li>
      </ol>
      <div class="page-header">
         <h1>
         	Contratos
         </h1>
      </div>
   </div>
</div>


<div class="row">
   <div class="col-sm-11">
      <form action="#" id="form-filter">
         <div class="row">
            <div class="col-md-5">
               <div class="form-group">
                  <label class="control-label">
                     Núcleos:
                  </label>
                  <select required="required" multiple="multiple" id="nucleos_select" class="form-control multipleselect" name="Nucleos[]">
                     <?php foreach ( NucleoFiliais::model()->findAll() as $nucleo ) { ?>
                        <option value="<?= $nucleo->id ?>"><?= strtoupper($nucleo->nome) ?></option>
                     <?php } ?>
                  </select>  
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-3" style="width:10%">
               <div class="form-group">
                  <label class="control-label">&nbsp;</label>
                  <button id="btn-filter" class="btn btn-blue next-step btn-block">Filtrar</button>
               </div>
            </div>
         </div>
      </form>
   </div>
</div>

<style type="text/css">
   .dropdown-menu {
       max-height: 300px;
       overflow-y: auto;
       overflow-x: hidden;
   }
</style>