<?php  ?>
<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="#">
                    Formalização de Propostas
                </a>
            </li>
            <li class="active">
                Propostas
            </li>
        </ol>
    </div>
</div>

<br>

<div class="row">
    
    <div class="col-sm-8" >
        
        <button class="btn btn-red"     title="Contrato sem Danfe"          ><i class="clip-clip"></i></button>
        <button class="btn btn-purple"  title="Contrato e Danfes"           ><i class="clip-clip"></i></button>
        <button class="btn btn-green"   title="Contrato e Danfes Parciais"  ><i class="clip-clip"></i></button>
        <button class="btn btn-blue"    title="Danfes sem Contrato"         ><i class="clip-clip"></i></button>
        <button class="btn btn-info"    title="Danfes Parciais se Contrato" ><i class="clip-clip"></i></button>
        
    </div>
    
<!--    <div class="col-sm-6">
        
        <button class='btn btn-green filtroNFe' value="0"  id="btnFiltroProNFe">
            <i class='clip-spinner-4'></i> Processadas
        </button>
        
        <button class="btn filtroNFe active" value="1" id="btnFiltroComNFe">
            <i class="clip-checkbox-unchecked-2" ></i> Processar NFe's
        </button>
        
        <button class="btn filtroNFe active" value="1" id="btnFiltroSemNFe">
            <i class="clip-file-xml" ></i> Sem NFe
        </button>
        
        <button class="btn filtroNFe active" value="1" id="btnFiltroSemChv">
            <i class="clip-file-xml" ></i> Sem Chave
        </button>
        
    </div>-->
    
    <div class="col-sm-4" style="text-align: right; right: 1%; padding: 0px!important; margin: 0px!important;">
        
        <button class="btn filtroModalidade active" value="1" id="btnFiltroComJuros">
            
            <i class="clip-plus-circle-2"   ></i>  
            <i class="fa fa-dollar"         ></i> Com Juros
            
        </button>
        
        <button class="btn filtroModalidade active" value="1" id="btnFiltroSemJuros">
            
            <i class="clip-minus-circle-2"  ></i>  
            <i class="fa fa-dollar"         ></i> Sem Juros
            
        </button>
        
    </div>
    
</div>

<br>

<div class="row">
    <div class="col-sm-12">
        <table id="grid_propostaFormalizacao" class="table table-striped table-bordered table-hover table-full-width dataTable">
            <thead>
                <tr>
                    <th>
                        Todas
                    </th>
                    <th>
                    </th>
                    <th class="searchable">
                        <input class="input_filter form-control" id="codigo_filter" style="width:100%"  type="text" placeholder="Pesquisar" />
                    </th>
                    <th class="searchable">
                        <input class="input_filter form-control" id="filial_filter" style="width:100%"  type="text" placeholder="Pesquisar" />
                    </th>
                    <th class="searchable">
                        <input class="input_filter form-control" id="cpf_filter"                        type="text" placeholder="Pesquisar" />
                    </th>
                    <th class="searchable">
                        <input class="input_filter form-control" id="nome_filter"   style="width:100%"  type="text" placeholder="Pesquisar" />
                    </th>
                    <th class="searchable">
                        <input class="input_filter form-control" id="data_filter"   style="width:100%"  type="date" placeholder="Pesquisar" 
                               value="<?php echo date("Y-m-d"); ?>"
                        />
                    </th>
                    <th colspan="2">
                        
                    </th>
                    <th id="thBotao">
                        
                        <button class="btn btn-blue" disabled="disabled" id="botaoGerar" style="display: none">
                            Exportar
                        </button>
                        
                    </th>
                    <th width="3px">
                        
                    </th>
                </tr>
                <tr>
                    <th width="3px">       
                        <button class="btn btn-blue" id="btnSelecionaTudo" value="0" style="display : none">
                            <i class="clip-checkbox-unchecked-2 checkNF" id="iSelecionarTudo"></i>
                        </button>
                    </th>
                    <th>
                        Modalidade
                    </th>
                    <th>
                        Cód Proposta
                    </th>
                    <th>
                        Filial
                    </th>
                    <th>
                        CPF
                    </th>
                    <th>
                        Cliente
                    </th>
                    <th>
                        Data Proposta
                    </th>
                    <th>
                        R$ Financiado
                    </th>
                    <th>
                        Parcelamento
                    </th>
                    <th>
                        R$ final
                    </th>
                    <th width="3px">
                    </th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>


<div id="modalAnexo" class="modal fade" tabindex="-1" data-width="560" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Anexar XML -Nf</h4>
    </div>
    <form action="/formalizacao/anexarDanfe/" method="POST" enctype="multipart/form-data" id="form-add-danfe">
        <div class="modal-body">
            <div class="row">
                <div class="row-centralize">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Danfe:</label>
                                    <input data-icon="false" class="control-label filestyle" required="required" name="anexoDanfe" id="anexoDanfe" type="file" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
            <button type="submit" class="btn btn-blue btn-send" id="btn-salvar-soli">Salvar</button>
            <input name="idNF" id="idNF" type="hidden" />
        </div>
    </form>
</div>

<br>

<style type="text/css">
    
    #grid_propostaFormalizacao_length   , #grid_propostaFormalizacao_filter ,
    #grid_anexos_contratos_length       , #grid_anexos_contratos_filter     ,
    #grid_anexos_nfe_length             , #grid_anexos_nfe_filter
    {
        display: none;
    }
    .thChek
    {
        text-align: center!important;
    }
    td.details-control 
    {
        background: url('../../images/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.details td.details-control 
    {
        background: url('../../images/details_close.png') no-repeat center center;
    }
    
</style>