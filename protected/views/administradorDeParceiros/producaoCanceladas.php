<?php

	$util                   = new Util;
  $nomesParceiros         = array();
  $util 					        = new Util;
  $valorTotalInicial      = 0;
  $valorTotalEntradas     = 0;
	$valorTotalFinanciado   = 0;
  $htmlInputsHiddenProps  = '';

	for($i = 0; $i < sizeof($params['Parceiros']); $i++)
	{
		$parceiro 			   = Filial::model()->findByPk($params['Parceiros'][$i]);

		$nomesParceiros[] 	= strtoupper($parceiro->getConcat());
	}

   $tableRows              = Empresa::model()->producaoResultRows($params['Parceiros'], array($params['status']), $params['dataDe'], $params['dataAte']);

?>
<div class="row">
   <div class="col-sm-12">
      
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="<?php echo Yii::app()->getBaseUrl(true) .'/'. Yii::app()->session['usuario']->getRole()->login_redirect;?>">
               Administrador de Parceiros
            </a>
         </li>
         <li class="active">
            Relatórios
         </li>
      </ol>
      <div class="page-header">
         <h1>
         	<span class="label label-warning" style="font-size:70%!important">Propostas Canceladas <?php //echo $params['porcentagemTotalNegado'] ?></span> - <?php echo  ' de '.$params['dataDe'].' a '.$params['dataAte']?>
         </h1>

      </div>
   </div>
</div>

<div class="row">
   <div class="col-sm-12">
		<label>
			FILIAIS FILTRADAS:
		</label>
		<input readonly="true" id="tags_1" type="text" class="tags" value="<?php echo join(',',$nomesParceiros) ?>">
   </div>
</div>
<br>
<div class="row">
   <div class="col-sm-12">
      <button id="btn-trigger-print" style="width:50 px;height:40px" class="submit"><i style="font-size:23px" class="fa fa-print"></i></button>
   </div>
</div>
<div class="row" style="margin-bottom:50px;margin-top:40px;">
   <div class="col-sm-12">
      <table id="grid_canceladas" class="table table-striped table-bordered table-hover table-full-width dataTable">
         <thead>
            <tr>
              <th width="20"  class="no-orderable"></th>
              <th width="20"  class="no-orderable"></th>
              <th width="60" class="no-orderable">Data</th>
              <th width="140" class="no-orderable">Código</th>
              <th width="230" class="no-orderable">Filial</th>
              <th width="210" class="no-orderable">Cliente</th>
              <th width="90"  class="no-orderable">Inicial</th>
              <th width="75"  class="no-orderable">Entrada</th>
              <th width="60"  class="no-orderable">Carência</th>
              <th width="90"  class="no-orderable">Financiado</th>
              <th width="60"  class="no-orderable">Parcelas</th>
            </tr>
         </thead>
         <tbody>
            <?php foreach( $tableRows as $row ){ ?>
               <?php 
                  $proposta               = Proposta::model()->findByPk($row['id']);
                  $valorTotalInicial      += $proposta->valor;
                  $valorTotalEntradas     += $proposta->valor_entrada;
                  $valorTotalFinanciado   += ($proposta->valor-$proposta->valor_entrada);
                  $htmlInputsHiddenProps  .= '<input type="hidden" name="propostas[]" value="'.$proposta->id.'">';
               ?>
               <tr>
                  <td data-d-solicitacao="<?php echo $util->bd_date_to_view( substr($proposta->soliCancelamento->data_solicitacao, 0,10) ) . ' ÀS ' . substr($proposta->soliCancelamento->data_solicitacao,10) ?>" data-solicitante="<?php echo strtoupper($proposta->soliCancelamento->solicitante->nome_utilizador); ?>" data-motivo="<?php echo strtoupper($proposta->soliCancelamento->motivo); ?>" class="details-control sorting_1"></td>
                  <td>
                  	<span class="badge badge-warning">&nbsp;</span>
                  </td>
                  <td><?php echo $util->bd_date_to_view( substr($proposta->data_cadastro, 0,10) ); ?></td>
                  <td><?php echo $proposta->codigo ?></td>
                  <td><?php echo strtoupper($proposta->analiseDeCredito->filial->getConcat()) ?></td>
                  <td><?php echo strtoupper(substr($proposta->analiseDeCredito->cliente->pessoa->nome, 0,25)) ?></td>
                  <td><?php echo 'R$ ' . number_format($proposta->valor,2,',','.'); ?></td>
                  <td><?php echo 'R$ ' . number_format($proposta->valor_entrada,2,',','.'); ?></td>
                  <td><?php echo $proposta->carencia .' dias' ?></td>
                  <td><?php echo 'R$ ' . number_format($proposta->valor-$proposta->valor_entrada,2,',','.'); ?></td>
                  <td><?php echo $proposta->qtd_parcelas; ?></td>
               </tr>
            <?php } ?>
         </tbody>
         <tfoot>
            <tr>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th><?php echo 'R$ '.number_format($valorTotalInicial,2,',','.'); ?></th>
               <th><?php echo 'R$ '.number_format($valorTotalEntradas,2,',','.'); ?></th>
               <th></th>
               <th><?php echo 'R$ '.number_format($valorTotalFinanciado,2,',','.'); ?></th>
               <th></th>
            </tr>
         </tfoot>
      </table>
   </div>
</div>

<form id="print-grid" method="POST" target="_blank" action="/printer/printProducaoFiliais/">
  <?php echo $htmlInputsHiddenProps; ?>
  <input type="hidden" name="view" value="printProducaoCanceladas">
  <input name="nomesParceiros" type="hidden" value="<?php echo join(', ',$nomesParceiros) ?>">
  <input name="dataDe" type="hidden" value="<?php echo $params['dataDe']; ?>">
  <input name="dataAte" type="hidden" value="<?php echo $params['dataAte']; ?>">
</form>

<style>
div.tagsinput span.tag a {
   display: none!important;
}
#grid_canceladas_length, #grid_canceladas_filter{
   display: none
}
tfoot tr th{
   font-size: 10px;
   color: #3D9400!important;
   font-weight: bold;
}
td.details-control{
	padding:7px 13px!important;
    background: url('../../images/details_open.png') no-repeat center center;
    cursor: pointer;
}
tr.details td.details-control{
    background: url('../../images/details_close.png') no-repeat center center;
}
</style>