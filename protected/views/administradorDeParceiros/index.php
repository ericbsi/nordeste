<?php   

?>
<div class="row">
   <div class="col-sm-12">
      
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="<?php echo Yii::app()->getBaseUrl(true) .'/'. Yii::app()->session['usuario']->getRole()->login_redirect;?>">
               Administrador de Filiais
            </a>
         </li>
         <li class="active">
            Indicadores
         </li>
      </ol>
      <div class="page-header">
         <h1>
          Acompanhamento de Produção | Dia : <?php echo date('d/m/Y') ?>
         </h1>
      </div>
   </div>
</div>

<div class="row">
  <div class="col-sm-11 col-md-12" style="background:#FFF!important;">
      <table id="grid_indicadores_dia_mes" class="table table-striped table-bordered table-hover table-full-width dataTable">
        <thead>
            <tr>
              <th width="450" class="no-orderable">Filial</th>
              <th class="no-orderable">R$ Produção Dia</th>
              <th class="no-orderable">R$ Produção Mês</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
        <tfoot>
          <tr>
            <th></th>
            <th id="tfoot-total-atraso"></th>
            <th></th>
          </tr>
         </tfoot>
      </table>
  </div>
</div>
<?php if( Yii::app()->session['usuario']->primeira_senha){ ?>
<div id="static" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
   <div class="modal-body">
      <p>
         O sistema detectou que você ainda não alterou sua senha após o cadastro (ou após solicitar uma nova senha). 
         Por medidas de segurança, solicitamos que você altere a sua senha.
      </p>
   </div>
   <div class="modal-footer">
      <form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/usuario/changePassword">
         <button type="submit"  class="btn btn-primary">
         Mudar senha
         </button>
      </form>
   </div>
</div>
<?php } ?>
<style type="text/css">
  
#grid_indicadores_dia_mes_filter, #grid_indicadores_dia_mes_length{
  display: none;
}
tfoot tr th{
   font-size: 10px;
   color: #3D9400!important;
   font-weight: bold;
}
table.table thead .sorting_asc{
  background: none!important
}
</style>