<?php 
	
  $util                   = new Util;
  $nomesParceiros         = array();
  
	$htmlInputsHiddenProps  = '';
  
  $valorTotalInicial      = 0;
  $valorTotalEntradas     = 0;
  $valorTotalFinanciado   = 0;

  $totalInicialEfetivado  = 0;
  $totalEntradaEfetivado  = 0;
  $totalEfetivado         = 0;

  $totalInicialPendente   = 0;
  $totalEntradaPendente   = 0;
  $totalPendente          = 0;

  $totalInicialPgtoAut    = 0;
  $totalEntradaPgtoAut    = 0;
  $totalPgtoAut           = 0;

	for($i = 0; $i < sizeof($params['Parceiros']); $i++)
	{  

    $parceiro         = Filial::model()->findByPk($params['Parceiros'][$i]);

    $htmlInputsHiddenProps  .= '<input type="hidden" name="Parceiros[]" value="'.$parceiro->id.'">';

		$nomesParceiros[] = strtoupper($parceiro->getConcat());
	}

  $statusDaProposta   = array(2,7);

  $tableRows          = Empresa::model()->producaoResultRows($params['Parceiros'], $statusDaProposta, $params['dataDe'], $params['dataAte'], TRUE);

?>
<div class="row">
   <div class="col-sm-12">
      
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="<?php echo Yii::app()->getBaseUrl(true) .'/'. Yii::app()->session['usuario']->getRole()->login_redirect;?>">
               Administrador de Parceiros
            </a>
         </li>
         <li class="active">
            Relatórios
         </li>
      </ol>
      <div class="page-header">
         <h1>
         	<span class="label label-success" style="font-size:70%!important">Propostas Aprovadas: <?php echo $params['porcentagemTotalAprovado'] ?></span> - <?php echo  ' de '.$params['dataDe'].' a '.$params['dataAte']?>
         </h1>

      </div>
   </div>
</div>

<div class="row">
   <div class="col-sm-12">
		<label>
			FILIAIS FILTRADAS:
		</label>
		<input readonly="true" id="tags_1" type="text" class="tags" value="<?php echo join(',',$nomesParceiros) ?>">
   </div>
</div>
<br>
<div class="row">
   <div class="col-sm-3">
      <button id="btn-trigger-print" style="width:50 px;height:40px" class="submit"><i style="font-size:23px" class="fa fa-print"></i></button>
   </div>
</div>
<br>
<br>
<div class="row">
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-5" style="width:18%">
        <div class="form-group">
          <label style="display:block" class="control-label">Filtrar:</label>
          <select required="required" id="filtro_select" class="form-control multipleselect">
            <option value="">Todas</option>
            <option value="Aprovado">Aprovados</option>
            <option value="Pendente">Pendentes</option>
            <option value="Autorizado">Pagamento Autorizado</option>
          </select>  
        </div>
      </div>
      <div class="col-md-3" style="width:15%">
        <div class="form-group">
          <label class="control-label">&nbsp;</label>
          <button id="btn-apply-filter" class="btn btn-blue next-step btn-block">Filtrar</button>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row" style="margin-bottom:50px;margin-top:40px;">
   <div class="col-sm-12">
      <table id="grid_aprovadas" class="table table-striped table-bordered table-hover table-full-width dataTable">
         <thead>
            <tr>
              <th width="20" class="no-orderable"></th>
              <th width="60" class="no-orderable">Data</th>
              <th width="140" class="no-orderable">Código</th>
              <th width="230" class="no-orderable">Filial</th>
              <th width="210" class="no-orderable">Cliente</th>
              <th width="80" class="no-orderable">Inicial</th>
              <th width="70" class="no-orderable">Entrada</th>
              <th width="60" class="no-orderable">Carência</th>
              <th width="85" class="no-orderable">Financiado</th>
              <th width="55" class="no-orderable">Parcelas</th>
              <th class="no-orderable">Status</th>
            </tr>
         </thead>
         <tbody>
            <?php foreach( $tableRows as $row ){ ?>
               <?php

                  $proposta             = Proposta::model()->findByPk($row['id']);
                  
                  if( $proposta->habilitado && $proposta->titulos_gerados )
                  {

                    $valorTotalInicial          += $proposta->valor;
                    $valorTotalEntradas         += $proposta->valor_entrada;
                    $valorTotalFinanciado       += ($proposta->valor-$proposta->valor_entrada);

                    $htmlInputsHiddenProps      .= '<input type="hidden" name="propostas[]" value="'.$proposta->id.'">';

                    if( ItemPendente::model()->find('ItemPendente = ' . $proposta->id ) != NULL )
                    {
                      $totalInicialPendente     += $proposta->valor;
                      $totalEntradaPendente     += $proposta->valor_entrada;
                      $totalPendente            += ($proposta->valor-$proposta->valor_entrada);
                    }

                    else if( ItemDoBordero::model()->find( 'Proposta_id = ' . $proposta->id ) )
                    {
                        $totalInicialPgtoAut    += $proposta->valor;
                        $totalEntradaPgtoAut    += $proposta->valor_entrada;
                        $totalPgtoAut           += ($proposta->valor-$proposta->valor_entrada);
                    }

                    else
                    {
                      $totalInicialEfetivado    += $proposta->valor;
                      $totalEntradaEfetivado    += $proposta->valor_entrada;
                      $totalEfetivado           += ($proposta->valor-$proposta->valor_entrada);
                    }

                  }
               ?>
               <tr>
                  <td>
                    <span class="badge badge-success">&nbsp;</span>
                  </td>
                  <td><?php  echo $util->bd_date_to_view( substr($proposta->data_cadastro, 0,10) ); ?></td>
                  <td><?php  echo $proposta->codigo ?></td>
                  <td><?php  echo strtoupper($proposta->analiseDeCredito->filial->getConcat()) ?></td>
                  <td><?php  echo strtoupper(substr($proposta->analiseDeCredito->cliente->pessoa->nome, 0,25)) ?></td>
                  <td><?php  echo 'R$ ' . number_format($proposta->valor,2,',','.'); ?></td>
                  <td><?php  echo 'R$ ' . number_format($proposta->valor_entrada,2,',','.'); ?></td>
                  <td><?php  echo $proposta->carencia .' dias' ?></td>
                  <td><?php  echo 'R$ ' . number_format($proposta->valor-$proposta->valor_entrada,2,',','.'); ?></td>
                  <td><?php  echo $proposta->qtd_parcelas; ?></td>
                  <td> <?php echo $proposta->btnStatusAprovado(); ?> </td>
               </tr>
            <?php } ?>
         </tbody>
         <tfoot>
            <tr>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th><?php echo 'R$ '.number_format($valorTotalInicial,2,',','.'); ?></th>
               <th><?php echo 'R$ '.number_format($valorTotalEntradas,2,',','.'); ?></th>
               <th></th>
               <th><?php echo 'R$ '.number_format($valorTotalFinanciado,2,',','.'); ?></th>
               <th></th>
               <th></th>
            </tr>
         </tfoot>
      </table>
   </div>
</div>

<form id="print-grid" method="POST" target="_blank" action="/printer/printProducaoFiliais/">
  <input type="hidden" name="view" value="printProducaoAprovadas">
  <input name="nomesParceiros" type="hidden" value="<?php echo join(', ',$nomesParceiros) ?>">
  <input name="dataDe" type="hidden" value="<?php echo $params['dataDe']; ?>">
  <input name="dataAte" type="hidden" value="<?php echo $params['dataAte']; ?>">

  <input type="hidden" value="<?php echo number_format($valorTotalInicial,2,',','.'); ?>" id="totalInicialGeral">
  <input type="hidden" value="<?php echo number_format($valorTotalEntradas,2,',','.'); ?>" id="totalEntradaGeral">
  <input type="hidden" value="<?php echo number_format($valorTotalFinanciado,2,',','.'); ?>" id="totalGeral">

  <input type="hidden" value="<?php echo number_format($totalInicialEfetivado,2,',','.'); ?>" id="totalInicialEfetivado">
  <input type="hidden" value="<?php echo number_format($totalEntradaEfetivado,2,',','.'); ?>" id="totalEntradaEfetivado">
  <input type="hidden" value="<?php echo number_format($totalEfetivado,2,',','.'); ?>" id="totalEfetivado">

  <input type="hidden" value="<?php echo number_format($totalInicialPendente,2,',','.'); ?>" id="totalInicialPendente">
  <input type="hidden" value="<?php echo number_format($totalEntradaPendente,2,',','.'); ?>" id="totalEntradaPendente">
  <input type="hidden" value="<?php echo number_format($totalPendente,2,',','.'); ?>" id="totalPendente">

  <input type="hidden" value="<?php echo number_format($totalInicialPgtoAut,2,',','.'); ?>" id="totalInicialPgtoAut">
  <input type="hidden" value="<?php echo number_format($totalEntradaPgtoAut,2,',','.'); ?>" id="totalEntradaPgtoAut">
  <input type="hidden" value="<?php echo number_format($totalPgtoAut,2,',','.'); ?>"        id="totalPgtoAut">
  <input type="hidden" value="" name="statusFiltro" id="statusFiltro">
  
  <?php echo $htmlInputsHiddenProps; ?>

</form>

<style>
div.tagsinput span.tag a {
   display: none!important;
}
#grid_aprovadas_length, #grid_aprovadas_filter{
   display: none
}
#grid_aprovadas tfoot tr th{
   font-size: 10px;
   color: #3D9400!important;
   font-weight: bold;
}
</style>