<div class="row">
    <div class="col-sm-12">

        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="<?php echo Yii::app()->getBaseUrl(true) . '/' . Yii::app()->session['usuario']->getRole()->login_redirect; ?>">
                    Administrador de Filiais
                </a>
            </li>
            <li class="active">
                Relatórios
            </li>
        </ol>
        <div class="page-header">
            <h1>
                Produção
            </h1>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-9">
        <div class="row">
            <form>
                <div class="col-md-3" style="width:15%">
                    <div class="form-group">
                        <label class="control-label">
                            Data de <span class="symbol required" aria-required="true"></span>
                        </label>                  
                        <input value="<?php echo date('d/m/Y') ?>" style="width:120px" type="text" name="data_de" class="form-control date-picker" data-date-viewmode="years" data-date-format="dd/mm/yyyy" id="data_de">
                    </div>
                </div>
                <div class="col-md-3" style="width:15%">
                    <div class="form-group">
                        <label class="control-label">Data até:</label>
                        <input value="<?php echo date('d/m/Y') ?>" style="width:120px" type="text" name="data_ate" class="form-control date-picker" data-date-viewmode="years" data-date-format="dd/mm/yyyy" id="data_ate">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">
                            Filiais <span class="symbol required"></span>
                        </label>
                        <select required="required" multiple="multiple" id="filiais_select" class="form-control multipleselect" name="Filiais[]">
                            <?php foreach (Yii::app()->session['usuario']->adminParceiros as $f) { ?>
                                <option value="<?php echo $f->parceiro->id ?>"><?php echo strtoupper($f->parceiro->getConcat()) ?></option>
                            <?php } ?>
                        </select>  
                    </div>
                </div>
                <div class="col-md-3" style="width:16%">
                    <div class="form-group">
                        <label class="control-label">&nbsp;</label>
                        <button id="btn-filter" class="btn btn-blue next-step btn-block">Filtrar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="row" style="margin-bottom:50px">
    <div class="col-sm-12">
        <table id="grid_atrasos" class="table table-striped table-bordered table-hover table-full-width dataTable">
            <thead>
                <tr>
                  <!--<th></th>-->
                    <th width="190" class="no-orderable">Status</th>
                    <th width="300" class="no-orderable">Valor</th>
                    <th width="300" class="no-orderable">Porcentagem do total analisado</th>
                    <th width="100" class="no-orderable"></th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>

<style type="text/css">
    .dropdown-menu {
        max-height: 250px;
        overflow-y: auto;
        overflow-x: hidden;
    }
    #grid_atrasos_filter, #grid_atrasos_length{
        display: none;
    }
    tfoot th {
        font-weight: bold!important;
        color: #d9534f;
        font-size: 11px;
    }
    td.details-control {
        padding:7px 13px!important;
        background: url('../../images/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.details td.details-control {
        background: url('../../images/details_close.png') no-repeat center center;
    }
</style>