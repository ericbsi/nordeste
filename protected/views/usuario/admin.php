<div class="row">
   <div class="col-sm-12">
      <!-- start: PAGE TITLE & BREADCRUMB -->
      <ol class="breadcrumb">
         <li><a href="#">Usuários</a></li>
         <li class="active">Admininstrar</li>
      </ol>
      <div class="page-header">
         <h1>Usuários</h1>
	      <div class="btn-group">
				<button data-toggle="dropdown" class="btn btn-danger dropdown-toggle">
					<i class="clip-settings"></i>
					Opções <span class="caret"></span>
				</button>
				<ul class="dropdown-menu" role="menu">
					<li>
						<a class="crud-modal-show" href="<?php echo Yii::app()->request->baseUrl;?>/usuario/create/">
							<i class="fa fa-plus"></i>
							Adicionar
						</a>
					</li>
				</ul>
		   </div>
      </div>
      <!-- end: PAGE TITLE & BREADCRUMB -->
   </div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
		<div class="panel-heading">
					<i class="fa fa-external-link-square"></i>
					<div class="panel-tools">
					<a class="btn btn-xs btn-link panel-collapse collapses" href="#">
					</a>
					<a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
						<i class="fa fa-wrench"></i>
					</a>
					<a class="btn btn-xs btn-link panel-refresh" href="#">
						<i class="fa fa-refresh"></i>
					</a>
					<a class="btn btn-xs btn-link panel-expand" href="#">
						<i class="fa fa-resize-full"></i>
					</a>
					<a class="btn btn-xs btn-link panel-close" href="#">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
	<?php 

	if ( Yii::app()->session['usuario']->tipo != 'empresa_admin' && Yii::app()->session['usuario']->tipo != 'super' ) {
		$this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'usuario-grid',
			'dataProvider'=> Yii::app()->session['usuario']->searchUserFilialUsers(),
			'filter'=>$model,
			'itemsCssClass' => 'table table-bordered responsive dataTable',
			'summaryText'=>'Exibindo {start} de {end} resultados',
			'columns'=>array(
				'id',
				'username',
				'tipo',
				array(
					'class'=>'CButtonColumn',
					'template' => ''
				),
			),
		));
	}

	elseif ( Yii::app()->session['usuario']->tipo == 'super' ) {
		
		$this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'usuario-grid',
			'dataProvider'=> Yii::app()->session['usuario']->findAdminsEmpresas(),
			'filter'=>$model,
			'itemsCssClass' => 'table table-bordered responsive dataTable',
			'summaryText'=>'Exibindo {start} de {end} resultados',
			'columns'=>array(
				'nome_utilizador',
				'username',
				'tipo' =>array(
					'header' => 'Função no sistema',
					'name'=> 'tipo',
					'value'=> '$data->tipoId->label'
				),
				'Empresa' =>array(
					'header' => 'Empresa',
					'value'=> '$data->nomeEmpresa()'
				),
				'data_cadastro_br',
			),
		));
	}
	
	else{
	
		$this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'usuario-grid',
			'dataProvider'=> Yii::app()->session['usuario']->findAnalistas(),
			'filter'=>$model,
			'itemsCssClass' => 'table table-hover',
			'summaryText'=>'Exibindo {start} de {end} resultados',
			'columns'=>array(
				'nome_utilizador',
				'username',
				'tipo' =>array(
					'name'=> 'tipo',
					'value'=> '$data->tipoId->label',
				),
				/*'Empresa' =>array(
					'header' => 'Empresa / Filial',
					'value'=> '$data->nomeEmpresa()'
				),*/
				'data_cadastro_br',
			),
		));
	}
?>
		</div>
	</div>
</div>
