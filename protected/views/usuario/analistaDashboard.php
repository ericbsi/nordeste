<?php ?>

<div class="row">
    <div class="col-sm-12">

        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="#">
                    Propostas   
                </a>
            </li>
            <li class="active">
                Últimas propostas
            </li>
        </ol>
        <div class="page-header">
            <h1>Últimas propostas</h1>

            <a target="_blank" class="btn btn-orange" href="<?php echo Yii::app()->request->baseUrl; ?>/proposta/adminReanalises/"><i class="clip-stack-empty"></i>
                Reanalises / Propostas Negadas
            </a>

        </div>

    </div>
</div>

<div class="row" style="padding-bottom:40px;">

    <div class="col-sm-12">
        <input type="checkbox" id="filter_semear" style="transform: scale(1.5); padding: 50px; margin: 10px;" /> Propostas Semear
        <input  <?php $user = Yii::app()->session['usuario']; 
                if (!(($user->id == 51) || ($user->id == 663))) {echo 'disabled';} ?> 
                type="checkbox" id="filter_prestige" style="transform: scale(1.5); padding: 50px; margin: 10px;" 
        /> Propostas Prestige
        <table id="table_id" class="table table-striped table-bordered table-hover table-full-width dataTable" style="font-size: 11px!important;">
            <thead>
                <tr>
                    <th></th>
                    <th class="searchable">
                        <input class="input_filter form-control" id="codigo_filter" style="width:100%" type="text" placeholder="Pesquisar" />
                   </th>
                   <th class="searchable">
                        <input class="input_filter form-control" id="nome_filter" style="width:100%" type="text" placeholder="Pesquisar" />
                   </th>
                </tr>
                <tr>
                    <th></th>
                    <th>Cód</th>
                    <th>Cliente</th>
                    <th></th>
                    <th>Financeira</th>
                    <th>Parceiro</th>
                    <th></th>
                    <th>R$</th>
                    <th>Seguro</th>
                    <th>Parcelamento</th>
                    <th>Status</th>
                    <th>Data</th>
                    <th>Espera</th>
                </tr>
            </thead>
            <tbody>


            </tbody>
        </table>
    </div>

</div>

<div id="responsive" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <div class="modal-header" style="background:#dff0d8!important;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 style="color: #468847;" class="alert-heading">
            Deseja iniciar esta análise ?
        </h4>
    </div>

    <div class="modal-body" style="background: #ECF0F1">

        <form class="form-horizontal" method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/proposta/analise">
            <div class="form-group">
                <div class="col-sm-3">
                    <input id="input_proposta_id" type="hidden" name="id">
                </div>             
            </div>                              
            <div class="alert alert-success">
                <i class="fa fa-check-circle"></i>
                Clique em  <strong>'Analisar'</strong> Para iniciar a análise.
            </div>              
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                    Cancelar
                </button>
                <input type="submit" class="btn btn-green" value="Analisar">
            </div>
        </form>
    </div>     
</div>

<div id="modal-enviar" class="modal fade" tabindex="-1" data-width="560" style="display: none;">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
      &times;
      </button>
      <h4 class="modal-title">Enviar Proposta para a mesa?</h4>
   </div>
   <form id="form-liberar-proposta">
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label class="control-label">Senha:</label>
                           <input required="required" value="" type="password" name="Operacao[senha]" class="form-control">
                           <input id="propostaid" type="hidden" name="Operacao[Proposta_id]">
                        </div>
                     </div>
                  </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
         <div style="background:transparent;border:none;" class="panel"></div>
         <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
         <button type="submit" class="btn btn-blue btn-send">Enviar</button>
      </div>
   </form>
</div>

<style type="text/css">
    span.label{
        padding: 0.3em 0.5em !important;
    }
    .modal_toggle:hover, .modal_toggle:focus{
        text-decoration: none;
    }
</style>

<?php if (Yii::app()->session['usuario']->primeira_senha) { ?>

    <div id="static" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
        <div class="modal-body">
            <p>
                O sistema detectou que você ainda não alterou sua senha após o cadastro (ou após solicitar uma nova senha). 
                Por medidas de segurança, solicitamos que você altere a sua senha.
            </p>
        </div>
        <div class="modal-footer">
            <form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/usuario/changePassword">
                <button type="submit"  class="btn btn-primary">
                    Mudar senha
                </button>
            </form>
        </div>      
    </div>

<?php } ?>


<style type="text/css">
    #table_id_filter, #table_id_length{
        display: none
    }
    .buttonpulsate {

        -webkit-border-radius: 10px;
        border-radius: 10px;
        border: none;

        cursor: pointer;
        display: inline-block;
        font-family: Arial;
        font-size: 20px;
        padding: 5px 10px;
        text-align: center;
        text-decoration: none;
    }
    @-webkit-keyframes glowing {
        0% { background-color: #d9534f; -webkit-box-shadow: 0 0 3px #b33426; }
        50% { background-color: #d9534f; -webkit-box-shadow: 0 0 60px #b33426; }
        100% { background-color: #d9534f; -webkit-box-shadow: 0 0 3px #b33426; }
    }

    @-moz-keyframes glowing {
        0% { background-color: #d9534f; -moz-box-shadow: 0 0 3px #b33426; }
        50% { background-color: #d9534f; -moz-box-shadow: 0 0 10px #b33426; }
        100% { background-color: #d9534f; -moz-box-shadow: 0 0 3px #b33426; }
    }

    @-o-keyframes glowing {
        0% { background-color: #d9534f; box-shadow: 0 0 3px #b33426; }
        50% { background-color: #d9534f; box-shadow: 0 0 10px #b33426; }
        100% { background-color: #d9534f; box-shadow: 0 0 3px #b33426; }
    }

    @keyframes glowing {
        0% { background-color: #d9534f; box-shadow: 0 0 3px #b33426; }
        50% { background-color: #d9534f; box-shadow: 0 0 10px #b33426; }
        100% { background-color: #d9534f; box-shadow: 0 0 3px #b33426; }
    }

    .buttonpulsate {
        -webkit-animation: glowing 1200ms infinite;
        -moz-animation: glowing 1200ms infinite;
        -o-animation: glowing 1200ms infinite;
        animation: glowing 1200ms infinite;
    }
</style>