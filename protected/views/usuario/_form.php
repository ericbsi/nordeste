<?php ?>
<!--
   <div class="form">
   
      <?php $form=$this->beginWidget('CActiveForm', array(
      'id'=>'usuario-form',
      'enableAjaxValidation'=>false,
      )); ?>
   
   
      <?php echo $form->errorSummary($model); ?>
      
      <div class="row">       
      </div>
   
      <?php if ( Yii::app()->session['usuario']->tipo == 'super' ) { ?>
      <div class="row">    
      </div>
      
      <!--Se o usuario logado for empresa_admin, devemos exibir a lista de filiais
      <?php } else { ?>
         <div class="row">
            <?php echo $form->labelEx($model,'limite_analise'); ?>
            <?php echo $form->textField($model,'limite_analise'); ?>
            <?php echo $form->error($model,'limite_analise'); ?>
         </div>
         <div class="row">
            <?php echo $form->labelEx($filialHasUsuario,'Filial_id'); ?>
            <?php echo $form->dropDownList($filialHasUsuario,'Filial_id', 
      CHtml::listData(Filial::model()->findAll(Yii::app()->session['usuario']->filiaisId('criteria')),
      'id','nome_fantasia' ));
      ?>
         </div>
      <?php } ?>
   <?php $this->endWidget(); ?>
   
   </div><!-- form -->
<div class="row">
   <div class="col-md-12">
      <div class="row">
         <div class="col-sm-12">
            <!-- start: PAGE TITLE & BREADCRUMB -->
            <ol class="breadcrumb">
               <li>
                  <i class="clip-pencil"></i>
                  <a class="crud-modal-show" href="<?php echo Yii::app()->request->baseUrl;?>/usuario/admin/">
                  Usuários
                  </a>
               </li>
               <li class="active">
                  Cadastrar
               </li>
            </ol>
            <div class="page-header">
               <h1>Cadastrar usuário</h1>
            </div>
            <!-- end: PAGE TITLE & BREADCRUMB -->
         </div>
      </div>
      <div class="panel panel-default">
         <div class="panel-heading">
            <i class="fa fa-external-link-square"></i>
            <div class="panel-tools">
               <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
               </a>
            </div>
         </div>
         <div class="panel-body">
            <hr>
            <?php $form=$this->beginWidget('CActiveForm', array(
               'id'=>'usuarioForm',       
               'enableAjaxValidation'=>false,
               'htmlOptions'=>array(
                  'role'=>'form'
               )
               )); ?>
            <div class="row">
               <div class="col-md-6">
                  <div class="form-group">
                     <?php echo $form->labelEx($model,'nome_utilizador', array('class'=>'control-label')); ?>
                     <?php echo $form->textField($model,'nome_utilizador', array('class'=>'form-control', 'required'=>true,'placeholder'=>'Nome do utilizador')); ?>
                  </div>
                  <div class="form-group">
                     <?php echo $form->labelEx($model,'username', array('class'=>'control-label')); ?>
                     <?php echo $form->textField($model,'username', array('class'=>'form-control', 'required'=>true,'placeholder'=>'Username')); ?>
                  </div>
                  <div class="form-group">
                     <?php echo $form->labelEx($model,'password', array('class'=>'control-label')); ?> 
                     <?php echo $form->passwordField($model,'password',array('class'=>'form-control', 'placeholder'=>'Senha')); ?>
                  </div>
                  <div class="form-group">
                     <label class="control-label">Confirme a senha</label>
                     <input type="password" name="password_again" class="form-control" placeholder="Confirme a senha">
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <?php echo $form->labelEx($model,'tipo', array('class'=>'control-label')); ?> <span class="symbol required"></span>
                     <?php echo $form->dropDownList($model,'tipo', CHtml::listData(Role::model()->findAll(Yii::app()->session['usuario']->userTypeCreate()),
                        'papel','label' ), array('class'=>'form-control search-select')); 
                        ?>
                  </div>
                  <?php if ( Yii::app()->session['usuario']->tipo == 'super' ) { ?>
                  <div class="form-group">
                     <?php echo $form->labelEx($empresaHasUsuario,'Empresa_id',array('class'=>'control-label')); ?>
                     <?php echo $form->dropDownList($empresaHasUsuario,'Empresa_id', 
                        CHtml::listData(Empresa::model()->findAll(Yii::app()->session['usuario']->empresasId('criteria')),
                        'id','nome_fantasia' ), array('class'=>'form-control search-select'));
                        ?>
                  </div>
                  <?php } ?>
                  <div class="form-group" id="select_grupo_de_analista_wrapper" style="display:none!important">
                     <label class="control-label">Grupos de analistas</label>
                     <?php echo CHtml::dropDownList('Grupo_de_Analistas','Grupo_de_Analistas',
                        CHtml::listData(GrupoDeAnalistas::model()->findAll( 'Empresa_id = ' . Yii::app()->session['usuario']->getEmpresa()->id ),
                        'id','nome' ), array('class'=>'form-control search-select', 'placeholder'=>'Selecione')); 
                        //'id','nome' ), array('class'=>'form-control search-select', 'multiple'=>'multiple', 'placeholder'=>'Selecione')); 
                        ?> 
                  </div>
                  <div class="form-group" id="input_limite_de_credito" style="display:none!important">
                     <?php echo $form->labelEx($model,'limite_analise',array('class'=>'control-label')); ?>
                     <?php echo $form->textField($model,'limite_analise', array('class'=>'form-control','placeholder'=>'Limite de análise')); ?>
                  </div>
                  <div class="form-group" id="input_filial" style="display:none!important">
                     <label class="control-label">Filial</label>
                     <?php echo $form->dropDownList($filialHasUsuario,'Filial_id', CHtml::listData(Yii::app()->session['usuario']->getEmpresa()->listFiliais(),
                        'id','concat' ), array('class'=>'form-control search-select')); 
                         //Yii::app()->session['usuario']->filiaisId('criteria')
                        ?>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-12">
                  <div>
                     <span class="symbol required"></span> CAMPOS OBRIGATÓRIOS
                     <hr>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-8">
               </div>
               <div class="col-md-4">
                  <input type="submit" value="Cadastrar" class="btn btn-teal btn-block">
               </div>
            </div>
            <?php $this->endWidget(); ?>
         </div>
      </div>
   </div>
</div>

<style type="text/css">
   span.required{
   color: #E6674A;
   }
</style>
