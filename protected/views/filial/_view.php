<?php
/* @var $this FilialController */
/* @var $data Filial */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nome_fantasia')); ?>:</b>
	<?php echo CHtml::encode($data->nome_fantasia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cnpj')); ?>:</b>
	<?php echo CHtml::encode($data->cnpj); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_cadastro')); ?>:</b>
	<?php echo CHtml::encode($data->data_cadastro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('habilitado')); ?>:</b>
	<?php echo CHtml::encode($data->habilitado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Contato_id')); ?>:</b>
	<?php echo CHtml::encode($data->Contato_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Empresa_id')); ?>:</b>
	<?php echo CHtml::encode($data->Empresa_id); ?>
	<br />


</div>