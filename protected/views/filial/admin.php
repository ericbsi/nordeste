<!-- start: PAGE HEADER -->
<div class="row">
   <div class="col-sm-12">
      <!-- start: PAGE TITLE & BREADCRUMB -->
      <ol class="breadcrumb">
         <li><a href="#">Filiais</a></li>
         <li class="active">Admin</li>
      </ol>
      <div class="page-header">
         <h1>Filiais</h1>
      </div>
      <!-- end: PAGE TITLE & BREADCRUMB -->
   </div>
</div>
<!-- end: PAGE HEADER -->
<!--<div class="row">
   <div class="col-md-12">
      <div class="panel panel-default">
         <div class="panel-heading">
            <i class="fa fa-external-link-square"></i>
            Filiais
            <div class="panel-tools">
               <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
               </a>
               <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
               <i class="fa fa-wrench"></i>
               </a>
               <a class="btn btn-xs btn-link panel-refresh" href="#">
               <i class="fa fa-refresh"></i>
               </a>
               <a class="btn btn-xs btn-link panel-expand" href="#">
               <i class="fa fa-resize-full"></i>
               </a>
               <a class="btn btn-xs btn-link panel-close" href="#">
               <i class="fa fa-times"></i>
               </a>
            </div>
         </div>
         <?php $this->widget('zii.widgets.grid.CGridView', array(
      'id'=>'filial-grid',
            'dataProvider'=> $model->filialUserFilterSearch( Yii::app()->session['usuario']->id ),
      //'filter'=>$model,
      'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width dataTable',
      'columns'=>array(
         'nome_fantasia',
         'cnpj',
         'data_cadastro_br',
         array(
            'class'=>'CButtonColumn',
            'template'=>'{update}'
         ),
      ),
      )); ?>
      </div>
   </div>
   </div>-->
<?php
   $fs         = Yii::app()->session['usuario']->listFiliais();
   $empresa    = Yii::app()->session['usuario']->getEmpresa();
?>
<div class="row">
   <div class="col-md-12">
      <!-- start: DYNAMIC TABLE PANEL -->
      <div class="panel panel-default">
         <div class="panel-heading">
            <i class="fa fa-external-link-square"></i>
            Filiais
            <div class="panel-tools">
               <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
               </a>
               <a class="btn btn-xs btn-link panel-close" href="#">
               <i class="fa fa-times"></i>
               </a>
            </div>
         </div>
         <div class="panel-body">
            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
               <thead>
                  <tr>
                     <th>Núcleo</th>
                     <th>Nome Fantasia</th>
                     <th class="hidden-xs">CNPJ</th>
                     <th>Inscrição Estad.</th>
                     <th class="hidden-xs">Localização</th>
                     <th></th>
                     <th></th>
                  </tr>
               </thead>
               <tbody>
                  <?php foreach($empresa->listFiliais() as $filial){ 
                     ?>
                     <tr>
                        <input type="hidden" name="idFilial" value="<?php echo $filial->id  ?>" />
                        <td class="tdNucleo"><?php echo $filial->getNomeNucleo() ?></td>
                        <td><?php echo strtoupper($filial->nome_fantasia) ?></td>
                        <td class="hidden-xs"><?php echo $filial->cnpj ?></td>
                        <td><?php echo $filial->inscricao_estadual ?></td>
                        <td class="hidden-xs"><?php echo strtoupper($filial->getEndereco()->cidade).', '.strtoupper($filial->getEndereco()->uf); ?></td>
                        <td class="center">
                           <div class="visible-md visible-lg hidden-sm hidden-xs">
                              <form action="<?php echo Yii::app()->request->baseUrl;?>/filial/update" method="POST">
                                 <button class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Editar"><i class="fa fa-edit"></i></button>
                                 <input type="hidden" value="<?php echo $filial->id ?>" name="id">
                              </form>
                              <!--<a href="<?php echo Yii::app()->request->baseUrl;?>/filial/update/<?php echo $filial->id ?>" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Editar"><i class="fa fa-edit"></i></a>
                              <a href="#" class="btn btn-xs btn-green tooltips" data-placement="top" data-original-title="Share"><i class="fa fa-share"></i></a>
                              <a href="#" class="btn btn-xs btn-bricky tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>-->
                           </div>
                        </td>
                        <td>
                           <?php
                                if (isset($filial->filialHasPoliticaCredito->politicaCredito->descricao))
                                {
                                    echo $filial->filialHasPoliticaCredito->politicaCredito->descricao;
                                } else
                                {
                                    echo "POLÍTICA NÃO DEFINIDA";
                                }
                          ?>
                        </td>
                     </tr>
                  <?php } ?>
               </tbody>
            </table>
         </div>
      </div>
      <!-- end: DYNAMIC TABLE PANEL -->
   </div>
</div>