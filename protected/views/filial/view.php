<?php
$this->breadcrumbs=array(
	'Filials'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Filial', 'url'=>array('index')),
	array('label'=>'Create Filial', 'url'=>array('create')),
	array('label'=>'Update Filial', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Filial', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Filial', 'url'=>array('admin')),
);
?>

<h1>View Filial #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nome_fantasia',
		'cnpj',
		'data_cadastro',
		'habilitado',
		'Contato_id',
		'Empresa_id',
	),
)); ?>
