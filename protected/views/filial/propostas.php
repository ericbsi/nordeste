
<?php  ?>
<div class="row">
   <div class="col-sm-12">
      
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
               Propostas
            </a>
         </li>
         <li class="active">
            Por Filial
         </li>
      </ol>
      <div class="page-header">
         <h1 style="font-size:24px;">
         	Propostas filial <?php echo Yii::app()->session['usuario']->returnFilial()->getConcat(); ?>
         </h1>
      </div>
   </div>
</div>
<div class="row">
	<div class="col-sm-12">
		<table id="grid_propostas" class="table table-striped table-bordered table-hover table-full-width dataTable">
		    <thead>
		    	<tr>
			       <th></th>
			       <th class="searchable">
			       		<input class="input_filter form-control" id="codigo_filter" style="width:100%" type="text" placeholder="Pesquisar" />
			       </th>
			       <th class="searchable">
			       		<input class="input_filter form-control" id="nome_filter" style="width:100%" type="text" placeholder="Pesquisar" />
			       </th>
			       <th></th>
			       <th></th>
			       <th></th>
			       <th></th>
			       <th></th>
			       <th></th>
			       <th></th>
			       <th></th>
		        </tr>
		    	<tr>
			       <th style="width:20px;"></th>
			       <th style="width:160px;">Cód</th>
	               <th>Cliente</th>
	               <th style="width:150px;">Crediarista</th>
	               <th style="width:100px;">R$ Inicial</th>
	               <th style="width:80px;">Entrada</th>
	               <th style="width:80px;">Seguro</th>
	               <th style="width:80px;">R$ Financiado</th>
	               <th style="width:80px;">Parcelamento</th>
	               <th style="width:80px;">R$ final</th>
	               <th style="width:75px;">Status</th>
		        </tr>
		    </thead>
		    <tbody>
		    
			</tbody>
		</table>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<div id="chart_div"></div>
	</div>
</div>

<style type="text/css">
	#grid_propostas_length, #grid_propostas_filter{
		display: none;
	}
	td.details-control {
	    background: url('../../images/details_open.png') no-repeat center center;
	    cursor: pointer;
	}
	tr.details td.details-control {
	    background: url('../../images/details_close.png') no-repeat center center;
	}
</style>