<?php
$util = new Util;
$model->data_de_abertura = $util->bd_date_to_view($model->data_de_abertura);
?>
<div class="row">   
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">                   

                <ol class="breadcrumb">
                    <li>
                        <i class="clip-pencil"></i>
                        <a href="#">
                            Filial
                        </a>
                    </li>
                    <li class="active">
                        Atualizar
                    </li>

                </ol>

                <div class="page-header">
                    <h1>Cadastrar Filial</h1>
                </div>

            </div>
        </div>
        <div class="panel panel-default">         
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                    </a>	               
                </div>
            </div>
            <div class="panel-body">

                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'filial-form',
                    'enableAjaxValidation' => false,
                    'htmlOptions' => array(
                        'role' => 'form',
                        'class' => 'smart-wizard',
                        'id' => 'formFilial'
                    )
                        ));
                ?>

                <div id="wizard" class="swMain">

                    <ul>
                        <li>
                            <a href="#step-1">
                                <div class="stepNumber">1</div>
                                <span class="stepDesc">Dados da Filial</span>
                            </a>
                        </li>
                        <li>
                            <a href="#step-2">
                                <div class="stepNumber">2</div>
                                <span class="stepDesc">Endereço</span>
                            </a>
                        </li>
                        <li>
                            <a href="#step-3">
                                <div class="stepNumber">3</div>
                                <span class="stepDesc">Contato</span>
                            </a>
                        </li>
                        <li>
                            <a href="#step-4">
                                <div class="stepNumber">4</div>
                                <span class="stepDesc">Finalizar</span>
                            </a>
                        </li>
                    </ul>
                    <div class="progress progress-striped active progress-sm">
                        <div aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar progress-bar-success step-bar">
                            <span class="sr-only"> 0% Complete (success)</span>
                        </div>
                    </div>

                    <div id="step-1">
                        <h2 class="StepTitle">Dados da Filial</h2>
                        <div class="row">
                            <div class="row-centralize">
                                <input type="hidden" name="id" value="<?php echo $model->id ?>" id="filialId" />
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?php echo $form->labelEx($model, 'nome_fantasia', array('class' => 'control-label')); ?> <span class="symbol required"></span>
                                        <?php echo $form->textField($model, 'nome_fantasia', array('class' => 'form-control', 'placeholder' => 'Nome fantasia')); ?>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?php echo $form->labelEx($model, 'cnpj', array('class' => 'control-label')); ?> <span class="symbol required"></span>
                                                <?php echo $form->textField($model, 'cnpj', array('class' => 'form-control', 'placeholder' => 'CNPJ')); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?php echo $form->labelEx($model, 'inscricao_estadual', array('class' => 'control-label')); ?> <span class="symbol required"></span>
                                                <?php echo $form->textField($model, 'inscricao_estadual', array('class' => 'form-control', 'placeholder' => 'Inscrição Estadual')); ?>
                                            </div>
                                        </div>                              
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?php echo $form->labelEx($model, 'data_de_abertura', array('class' => 'control-label')); ?>
                                                <?php echo $form->textField($model, 'data_de_abertura', array('class' => 'form-control', 'placeholder' => 'Data de abertura')); ?>
                                                <label class="control-label">&nbsp;</label>                                    
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?php echo $form->labelEx($model, 'Empresa_id', array('class' => 'control-label')); ?>
                                                <?php
                                                echo CHtml::dropDownList('Filial[Empresa_id]', 'Filial[Empresa_id]', CHtml::listData(Empresa::model()->findAll(Yii::app()->session['usuario']->empresasId('criteria')), 'id', 'nome_fantasia'), array('class' => 'form-control search-select'));
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row">

                                        <div class="col-md-12">

                                            <div class="form-group">
                                                <label class="control-label" for="s2id_autogen1">Atividade Econômica Principal:</label>
                                                <select id="atividade_primaria_select" class="form-control" name="Unidade_de_negocio_id">
                                                    <?php foreach ($unidadesDeNegocio as $und) { ?>
                                                        <option <?php
                                                        if ($und->id == $model->Unidade_de_negocio_id) {
                                                            echo "selected ";
                                                        }
                                                        ?> value="<?php echo $und->id ?>"><?php echo $und->descricao ?></option>
                                                      <?php 
                                                      } ?>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label" for="s2id_autogen1">Atividades Econômicas Secundárias:</label>
                                                <select multiple="multiple" id="atividades_secundarias_select" class="form-control" name="UnidadesDeNegocio[]">
                                                    <?php foreach ($unidadesDeNegocio as $und) { ?>
                                                        <option <?php if ($model->hasAtividadeSecundaria($und->id)) {
                                                            echo "selected";
                                                        } ?> tipo="<?php echo $und->tipo ?>" value="<?php echo $und->id ?>"><?php echo $und->descricao ?></option>
                                                      <?php 
                                                      } ?>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                
                                                <?php /*
                                                   echo $form->labelEx(
                                                           $model, 
                                                           'PoliticaCredito_id', 
                                                           array('class' => 'control-label')
                                                           ); 
                                                ?>
                                                <?php
                                                   echo CHtml::dropDownList(
                                                           'Filial[PoliticaCredito_id]'               , 
                                                           'Filial[PoliticaCredito_id]'               , 
                                                           CHtml::listData(
                                                                   PoliticaCredito::model()->findAll(), 
                                                                   'id'                               , 
                                                                   'descricao'
                                                                   )                                  , 
                                                           array('class' => 'form-control search-select')
                                                           );
                                                */?>
                                                
                                                <label class="control-label" for="s2id_autogen1">
                                                    Política de Crédito:
                                                </label>
                                                
                                                <select id="PoliticaCredito_id" class="form-control" name="PoliticaCredito_id">
                                                    
                                                    <?php foreach (PoliticaCredito::model()->findAll("habilitado") as $politica) { ?>
                                                        
                                                        <option 
                                                            
                                                            <?php if (isset($model->filialHasPoliticaCredito->politicaCredito->id) && 
                                                                    $model->filialHasPoliticaCredito->politicaCredito->id == $politica->id) {
                                                                echo "selected";
                                                            } ?> value="<?php echo $politica->id ?>">
                                                            
                                                            <?php echo $politica->descricao ?>
                                                            
                                                        </option>
                                                            
                                                      <?php 
                                                    } ?>
                                                        
                                                </select>
                                                
                                            </div>
                                            
                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <button class="btn btn-blue next-step btn-block">
                                                    Avançar <i class="fa fa-arrow-circle-right"></i>
                                                </button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="step-2">
                        <h2 class="StepTitle">Endereço</h2>
                        <div class="row">
                            <div class="row-centralize">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <?php echo $form->labelEx($endereco, 'cep', array('class' => 'control-label')); ?> <span class="symbol required"></span>
                                                <?php echo $form->textField($endereco, 'cep', array('class' => 'required form-control', 'placeholder' => 'CEP')); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <?php echo $form->labelEx($endereco, 'logradouro', array('class' => 'control-label')); ?> <span class="symbol required"></span>
                                                <?php echo $form->textField($endereco, 'logradouro', array('class' => 'required form-control', 'placeholder' => 'Logradouro')); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?php echo $form->labelEx($endereco, 'cidade', array('class' => 'control-label')); ?>  <span class="symbol required"></span>
                                                <?php echo $form->textField($endereco, 'cidade', array('class' => 'required form-control', 'placeholder' => 'Cidade')); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?php echo $form->labelEx($endereco, 'bairro', array('class' => 'control-label')); ?> <span class="symbol required"></span>
                                                <?php echo $form->textField($endereco, 'bairro', array('class' => 'required form-control', 'placeholder' => 'Bairro')); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <?php echo $form->labelEx($endereco, 'numero', array('class' => 'control-label')); ?>  <span class="symbol required"></span>
                                                <?php echo $form->textField($endereco, 'numero', array('class' => 'required form-control required', 'placeholder' => 'Número')); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <?php echo $form->labelEx($endereco, 'complemento', array('class' => 'control-label')); ?>
                                                <?php echo $form->textField($endereco, 'complemento', array('class' => 'form-control', 'placeholder' => 'Complemento')); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">UF</label>
                                                <?php
                                                echo $form->dropDownList($endereco, 'uf', CHtml::listData(Estados::model()->findAll(), 'sigla', 'nome'), array('class' => 'form-control search-select'));
                                                ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Tipo de endereço</label>
                                                <?php
                                                echo $form->dropDownList($endereco, 'Tipo_Endereco_id', CHtml::listData(TipoEndereco::model()->findAll(), 'id', 'tipo'), array('class' => 'form-control search-select'));
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <button class="btn btn-blue next-step btn-block">
                                                Avançar <i class="fa fa-arrow-circle-right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="step-3">
                        <h2 class="StepTitle">Contato</h2>
                        <div class="row">
                            <div class="row-centralize">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?php echo $form->labelEx($telefone, 'Tipo_Telefone_id', array('class' => 'control-label')); ?>
                                                <?php
                                                echo $form->dropDownList($telefone, 'Tipo_Telefone_id', CHtml::listData(TipoTelefone::model()->findAll(), 'id', 'tipo'), array('class' => 'form-control search-select'));
                                                ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?php echo $form->labelEx($telefone, 'numero', array('class' => 'control-label')); ?>  <span class="symbol required"></span>
                                                <?php echo $form->textField($telefone, 'numero', array('class' => 'required form-control required', 'placeholder' => 'Numero')); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">                              
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?php echo $form->labelEx($email, 'email', array('class' => 'control-label')); ?> <span class="symbol required"></span>
                                            <?php echo $form->textField($email, 'email', array('class' => 'form-control', 'placeholder' => 'Email')); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">&nbsp;</label>
                                            <button class="btn btn-blue next-step btn-block">
                                                Avançar <i class="fa fa-arrow-circle-right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="step-4">
                        <div class="alert alert-block alert-warning fade in">
                            <button data-dismiss="alert" class="close" type="button">
                                &times;
                            </button>
                            <h4 class="alert-heading"><i class="fa fa-exclamation-triangle"></i> Atenção!</h4>
                            <p>
                                Antes de submeter o formulário, certifique-se de que os dados informados estão corretos. Lembre-se que todos os dados solicitados são importantes para o cadastro, 
                                portanto, revise-os antes de concluir.
                            </p>
                            <p>
                                <input type="submit" class="btn btn-yellow" value="Estou ciente, quero enviar.">
                            </p>
                        </div>
                    </div>
                </div>  

                <?php $this->endWidget(); ?>

            </div>
        </div>
    </div>
</div>