<?php
/* @var $this FormaDePagamentoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Forma De Pagamentos',
);

$this->menu=array(
	array('label'=>'Create FormaDePagamento', 'url'=>array('create')),
	array('label'=>'Manage FormaDePagamento', 'url'=>array('admin')),
);
?>

<h1>Forma De Pagamentos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
