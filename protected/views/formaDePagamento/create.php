<?php
/* @var $this FormaDePagamentoController */
/* @var $model FormaDePagamento */

$this->breadcrumbs=array(
	'Forma De Pagamentos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FormaDePagamento', 'url'=>array('index')),
	array('label'=>'Manage FormaDePagamento', 'url'=>array('admin')),
);
?>

<h1>Create FormaDePagamento</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>