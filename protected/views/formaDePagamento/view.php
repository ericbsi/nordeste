<?php
/* @var $this FormaDePagamentoController */
/* @var $model FormaDePagamento */

$this->breadcrumbs=array(
	'Forma De Pagamentos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List FormaDePagamento', 'url'=>array('index')),
	array('label'=>'Create FormaDePagamento', 'url'=>array('create')),
	array('label'=>'Update FormaDePagamento', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete FormaDePagamento', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage FormaDePagamento', 'url'=>array('admin')),
);
?>

<h1>View FormaDePagamento #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'descricao',
		'sigla',
		'habilitado',
	),
)); ?>
