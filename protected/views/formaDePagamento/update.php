<?php
/* @var $this FormaDePagamentoController */
/* @var $model FormaDePagamento */

$this->breadcrumbs=array(
	'Forma De Pagamentos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List FormaDePagamento', 'url'=>array('index')),
	array('label'=>'Create FormaDePagamento', 'url'=>array('create')),
	array('label'=>'View FormaDePagamento', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage FormaDePagamento', 'url'=>array('admin')),
);
?>

<h1>Update FormaDePagamento <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>