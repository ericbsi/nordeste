<div class="row">
   <div class="col-sm-12">
      <!-- start: PAGE TITLE & BREADCRUMB -->
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/categoriaProduto/">
            Estoque
            </a>
         </li>
         <li class="active">
            Categorias de Produtos
         </li>
      </ol>
      <div class="page-header">
         <h1>Categorias de produtos</h1>
      </div>
   </div>
</div>

<!--<div class="row">
   <div class="col-sm-12">
      <div class="col-md-12">
         <div class="row">
            <div class="col-md-3">
               <div class="form-group">
                  <label class="control-label">Filial</label>
                  <input required class="form-control" type="text" name="CategoriaProduto[nome]">
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
-->
<div class="row">
   <div class="col-sm-12">
      <table id="grid_categorias_produtos" class="table table-striped table-bordered table-hover table-full-width dataTable">
         <thead>
            <tr>
               <th style="width:190px">Código</th>
               <th style="width:450px">Nome</th>
               <th style="width:300px">Pai</th>
               <th style="width:25px">
                  <a style="padding:2px 6px!important; font-size:12px!important; color:#FFF!important" class="btn-add btn btn-success" href="#modal_form_new_categoria" data-toggle="modal">
                     <i class="fa fa-plus"></i>
                  </a>
               </th>
            </tr>
         </thead>
         <tfoot>
            <tr>
               <th class="searchable"></th>
               <th class="searchable"></th>
               <th></th>
               <th></th>
            </tr>
         </tfoot>
      </table>
   </div>
</div>
<div id="modal_form_new_categoria" class="modal fade" tabindex="-1" data-width="960" style="display: none;">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
      &times;
      </button>
      <h4 class="modal-title">Adicionar Categoria</h4>
   </div>
   <form id="form-add-categoria-produto">
      <div class="modal-body">
         <div class="row">
            <div class="row-centralize">
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Nome</label>
                           <input required class="form-control" type="text" name="CategoriaProduto[nome]">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Categoria pai</label>
                           <input value="" required="required" name="CategoriaProduto[idPai]" placeholder="Selecione uma categoria" type="hidden" id="selectCategorias" class="form-control search-select">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="row">
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal-footer">
         <label class="checkbox-inline" style="float:left">
         <input id="categorias_checkbox_continuar" type="checkbox" value="">
         Continuar Cadastrando
         </label>
         <div style="background:#ECF0F1; border:none" class="panel"></div>
         <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
         <button type="submit" class="btn btn-blue btn-send">Salvar</button>
         <div class="row">
         </div>
         <br>
         <div id="cadastro_categorias_msg_return" class="alert" style="text-align:left; display:none"></div>
      </div>
   </form>
</div>
<style type="text/css">
   #grid_categorias_produtos_length, #grid_categorias_produtos_filter{
      display: none;
   }
   .col-md-3{
      padding-left: 0!important;
   }
</style>