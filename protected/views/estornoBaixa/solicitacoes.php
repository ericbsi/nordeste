<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/estornoBaixa/index">
                    Estorno de Baixas
                </a>
            </li>
            <li class="active">
                Solicitações
            </li>
        </ol>
    </div>
</div>
<p>

</p>
<div class="row">
    <div class="col-sm-12">

        <table id="grid_estorno" 
               class="table table-striped table-bordered table-hover table-full-width dataTable">

            <thead>
                <tr>
                    <th>
                        Proposta
                    </th>
                    <th>
                        Data Proposta
                    </th>
                    <th>
                        Data Pagamento
                    </th>
                    <th>
                        Data Solicitação
                    </th>
                    <th>
                        Data Retorno
                    </th>
                    <th>
                        Cliente
                    </th>
                    <th>
                        CPF
                    </th>
                    <th>
                        Dados Bancários
                    </th>
                    <th>
                        Comprovante Pg
                    </th>
                    <th>
                        Comprovante Estorno
                    </th>
                    <th>
                        Valor
                    </th>
                    <th>

                    </th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<style type="text/css">
    #grid_estorno_length
    {
        display     : none!important        ;
    }
    .panel
    {
        background  : transparent!important ;
        border      : none!important        ;
    }
</style>