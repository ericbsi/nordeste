<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/estornoBaixa/solicitacoesParcelas">
                    Estorno de Baixas
                </a>
            </li>
            <li class="active">
                Solicitações
            </li>
        </ol>
    </div>
</div>
<p>

</p>
<div class="row" id="divEstornos">
    
    <div class="col-sm-12">

        <table id="grid_estornoParcelas" 
               class="table table-striped table-bordered table-hover table-full-width dataTable">

            <thead>
                <tr>
                    <th>
                        Proposta
                    </th>
                    <th>
                        Data Proposta
                    </th>
                    <th>
                        Vencimento
                    </th>
                    <th>
                        Data Baixa
                    </th>
                    <th>
                        Data Solicitação
                    </th>
                    <th>
                        Solicitante
                    </th>
                    <th>
                        Responsável
                    </th>
                    <th>
                        Cliente
                    </th>
                    <th>
                        CPF
                    </th>
                    <th>
                        Valor Parcela
                    </th>
                    <th>
                        Valor Baixa
                    </th>
                    <th>

                    </th>
                    <th>

                    </th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<div class="row" id="divMensagens" style="display: none">
    
    <div class="row">
        
        <div class="col-sm-12">
            
            <button class="btn btn-orange" id="btnVoltarDivMsg" style="width: 100%;">
                <i class="clip-undo"></i>
                <b>Voltar</b>
            </button>
            
        </div>
        
    </div>
    
    <br>
    
    <div class="row">

        <div class="col-sm-12">

            <table id="gridMensagens" class="table table-striped table-full-width dataTable">

                <thead>

                    <tr>

                        <th width="15%">
                            Data
                        </th>

                        <th width="15%">
                            Status
                        </th>

                        <th width="15%">
                            Usuário
                        </th>

                        <th width="55%">
                            Mensagem
                        </th>

                    </tr>

                </thead>

            </table>

        </div>
        
    </div>
    
</div>

<div class="row" id="divEstornoMsg" style="display: none;">

    <div class="col-sm-12">

        <div class="panel panel-default" style="height: auto;">

            <div class="panel-heading">
                <i class="clip-history"></i>
                <b id="tipoStatus"></b>
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                    </a>
                </div>
            </div>

            <div class="panel-body panel-scroll ps-container" style="height: auto;">

                <!--<input type="hidden" id="idParcelaHdn" value="0" name="idParcelaHdn" />-->

                <div class="row" style="height: 50px">

                    <div class="col-md-12" style="height: 50px">
                        <input type=""  
                               id="observacao"  
                               style="width : 100%; height: 100%!important" 
                               />
                    </div>

                </div>

                <br>

                <div class="row">

                    <div class="col-md-6">

                        <button style="width: 100%; border-radius: 3px" id="btnConfirmar" data-id-status="" value="" disbled class="btn btn-success btn-sm">
                            <i class="fa fa-arrow-circle-down" aria-hidden="true"></i> Confirmar
                        </button>

                    </div>

                    <div class="col-md-6">

                        <button 
                                style="width: 100%; border-radius: 3px" 
                                id="submit_arquivo" 
                                class="btn btn-warning btnVoltar btn-sm" 
                                type="submit"
                                data-div-responsavel="divEstorno"
                                >

                            <i class="clip-cancel-circle" aria-hidden="true"></i> Voltar

                        </button>

                    </div>

                </div>

            </div>

        </div>

    </div>
    
</div>

<style type="text/css">
    #grid_estorno_length
    {
        display     : none!important        ;
    }
    .panel
    {
        background  : transparent!important ;
        border      : none!important        ;
    }
</style>