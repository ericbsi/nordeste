<?php if( Yii::app()->session['usuario']->primeira_senha){ ?>

<div id="static" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
   <div class="modal-body">
      <p>
       O sistema detectou que você ainda não alterou sua senha após o cadastro (ou após solicitar uma nova senha). 
       Por medidas de segurança, solicitamos que você altere a sua senha.
      </p>
   </div>
   <div class="modal-footer">
      <form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/usuario/changePassword">
         <button type="submit"  class="btn btn-primary">
            Mudar senha
         </button>
      </form>
   </div>      
</div>

<?php } ?>