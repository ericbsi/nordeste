<div class="row">
   <div class="col-md-12">
      <!-- start: FORM VALIDATION 1 PANEL -->
         <div class="row">
                  <div class="col-sm-12">                   
                     <!-- start: PAGE TITLE & BREADCRUMB -->
                     <ol class="breadcrumb">
                        <li>
                           <i class="clip-pencil"></i>
                           <a href="#">
                              Empresa
                           </a>
                        </li>
                        <li class="active">
                              Criar
                        </li>
                        
                     </ol>

                     <div class="page-header">
                        <h1>Nova empresa</h1>
                     </div>
                     <!-- end: PAGE TITLE & BREADCRUMB -->
                  </div>
         </div>
      <div class="panel panel-default">
         <div class="panel-heading">
            <i class="fa fa-external-link-square"></i>            
            <div class="panel-tools">
               <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
               </a>
            </div>
         </div>
         <div class="panel-body">            
            <hr>
	           <?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'empresaForm',			
					'enableAjaxValidation'=>false,
					'htmlOptions'=>array(
						'role'=>'form'
					)
				)); ?>

               <div class="row">
                  
                  <div class="col-md-6">
                     <div class="form-group">
						    <?php echo $form->labelEx($model,'razao_social', array('class'=>'control-label')); ?> <span class="symbol required"></span>
		                <?php echo $form->textField($model,'razao_social', array('class'=>'form-control', 'required'=>true,'placeholder'=>'Razão Social')); ?>
                     </div>
                     <div class="form-group">
                     	<?php echo $form->labelEx($model,'nome_fantasia', array('class'=>'control-label')); ?> <span class="symbol required"></span>
                     	<?php echo $form->textField($model,'nome_fantasia', array('class'=>'form-control', 'placeholder'=>'Nome Fantasia')); ?>
                     </div>
                     
                     <!---->
                     <div class="row">
                        
                        <div class="col-md-6">
                           <div class="form-group">
                              <?php echo $form->labelEx($model,'cnpj', array('class'=>'control-label')); ?> <span class="symbol required"></span>
                              <?php echo $form->textField($model,'cnpj',array('class'=>'form-control', 'placeholder'=>'CNPJ')); ?>
                           </div>
                        </div>

                        <div class="col-md-6">
                           <div class="form-group">
                              <?php echo $form->labelEx($model,'inscricao_estadual', array('class'=>'control-label')); ?> <span class="symbol required"></span>
                              <?php echo $form->textField($model,'inscricao_estadual',array('class'=>'form-control', 'placeholder'=>'Inscrição Estadual')); ?>
                           </div>
                        </div>

                     </div>

                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <?php echo $form->labelEx($model,'Unidade_de_negocio_id', array('class'=>'control-label')); ?> <span class="symbol required"></span>
                              <?php echo $form->dropDownList($model,'Unidade_de_negocio_id', 
                                 CHtml::listData(UnidadeDeNegocio::model()->findAll(),
                                 'id','descricao' ), array('class'=>'form-control search-select')); 
                               ?>
                           </div>
                        </div>                        
                     </div>
                     <!---->

                  </div>
                  
                  <div class="col-md-6">

                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <?php echo $form->labelEx($dadosBancarios,'numero', array('class'=>'control-label')); ?> <span class="symbol required"></span>
                              <?php echo $form->textField($dadosBancarios,'numero',array('class'=>'form-control', 'placeholder'=>'Número da conta')); ?>
                           </div>
                           <div class="form-group">
                              <?php echo $form->labelEx($dadosBancarios,'agencia', array('class'=>'control-label')); ?> <span class="symbol required"></span>
                              <?php echo $form->textField($dadosBancarios,'agencia',array('class'=>'form-control', 'placeholder'=>'Agência')); ?>
                           </div>
                           <div class="form-group">
                              <?php echo $form->labelEx($dadosBancarios,'data_abertura', array('class'=>'control-label')); ?> <span class="symbol required"></span>
                              <?php echo $form->textField($dadosBancarios,'data_abertura',array('class'=>'form-control', 'placeholder'=>'Data de Abertura')); ?>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <?php echo $form->labelEx($dadosBancarios,'Banco_id', array('class'=>'control-label')); ?>
                              <?php echo $form->dropDownList($dadosBancarios,'Banco_id',
                                 CHtml::listData(Banco::model()->findAll(),
                                 'id','nome' ), array('class'=>'form-control search-select'));
                               ?>
                           </div>
                           <div class="form-group">
                              <?php echo $form->labelEx($dadosBancarios,'operacao', array('class'=>'control-label')); ?> <span class="symbol required"></span>
                              <?php echo $form->textField($dadosBancarios,'operacao',array('class'=>'form-control', 'placeholder'=>'Operação')); ?>
                           </div>
                           <div class="form-group">
                              <?php echo $form->labelEx($dadosBancarios,'Tipo_Conta_Bancaria_id', array('class'=>'control-label')); ?>
                              <?php echo $form->dropDownList($dadosBancarios,'Tipo_Conta_Bancaria_id',
                                 CHtml::listData(TipoContaBancaria::model()->findAll(),
                                 'id','tipo' ), array('class'=>'form-control search-select'));
                               ?>
                            </div>
                        </div>
                     </div>

                  </div>

               </div>

               <div class="row">
                  <div class="col-md-12">
                     <div>
                        <span class="symbol required"></span> CAMPOS OBRIGATÓRIOS
                        <hr>
                     </div>
                  </div>
               </div>

               <div class="row">
                  <div class="col-md-8">
                  </div>
                  <div class="col-md-4">
                     <input type="submit" value="Cadastrar" class="btn btn-teal btn-block">
                  </div>
               </div>

 			<?php $this->endWidget(); ?>
         </div>
      </div>
      <!-- end: FORM VALIDATION 1 PANEL -->
   </div>
</div>

