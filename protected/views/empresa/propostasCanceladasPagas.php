<?php ?>
<div class="row">
    <div class="col-sm-12">

        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="#">
                    Cancelamentos Pagos
                </a>
            </li>
        </ol>
        <div class="page-header">
            <h1 style="font-size:24px;">
                Propostas Canceladas - Repasses realizados
            </h1>
        </div>
    </div>
</div>
<div class="row tabela_prop">
    <div class="col-sm-12" style="padding-bottom:50px;">
        <table id="grid_propostas" class="table table-striped table-bordered table-hover table-full-width dataTable">
            <thead>
                <tr>
                    <th colspan="4"></th>
                    <th>
                        <select id="filtroGrupoFilial" class="form-control search-select select2">
                            <option value="0" selected="selected">Filtre por grupo...</option>
                            <?php foreach (GrupoFiliais::model()->findAll('habilitado') as $grupo) { ?>
                                <option value="<?php echo $grupo->id ?>"><?php echo $grupo->nome_fantasia ?></option>
                            <?php } ?>
                        </select>
                    </th>
                </tr>
                <tr>
                    <th>Nº Contrato</th>
                    <th width="">Cód</th>
                    <th width="">Data</th>
                    <th width="">Cliente</th>
                    <th width="">R$ Valor e Repasse</th>
                    <th>Parceiro</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>

            </tbody>
            <tfoot>
                <tr>
                    <th colspan="4" style="text-align:right;">Total: </th>
                    <th id="total-producao"></th>
                    <th></th>
                    <th></th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div id="chart_div"></div>
    </div>
</div>

<div class="row estorno" style="display: none;">
    <div class="col-sm-12">
        <h3 style="text-align:center;"><i class="clip-file-2"></i> Informações de Estorno</h3>
        <hr style="width:30%;" />
        <div class="row" style="padding:5px">

            <div class="col-sm-1">
            </div>

            <div class="col-sm-2" >
                <label class="control-label">
                    Data Estorno:
                </label>
            </div>

            <div class="col-sm-2" >
                <label class="control-label">
                    Data Retorno:
                </label>
            </div>

            <div class="col-sm-2">
                <label class="control-label">
                    Nº Comprovante Estorno:
                </label>
            </div>

            <div class="col-sm-2">
                <label class="control-label">
                    Nº Comprovante Baixa:
                </label>
            </div>

            <div class="col-sm-2">
                <label class="control-label">
                    Data Baixa:
                </label>
            </div>

        </div>

        <div class="row"  style="padding:5px">

            <div class="col-sm-1">
            </div>

            <div class="col-sm-2">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    <input type="date" id="dataEstorno" class="form form-control" value="<?php echo date('Y-m-d'); ?>" />
                </div>
            </div>

            <div class="col-sm-2">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    <input type="date" id="dataRetorno" class="form form-control" value="<?php echo date('Y-m-d'); ?>" />
                </div>
            </div>

            <div class="col-sm-2">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="clip-note"></i>
                    </span>
                    <input type="text" id="comprovanteEstorno" class="form form-control" />
                </div>
            </div>

            <div class="col-sm-2">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-money"></i>
                    </span>
                    <input type="text" id="comprovantePagamento" class="form form-control" disabled value="" />
                </div>
            </div>

            <div class="col-sm-2">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    <input class="form form-control" id="dataBaixa" type="date" value="" disabled />
                </div>
            </div>

            <div class="col-sm-1">
            </div>

            <input type="hidden" id="idBaixa" value="" />
        </div>

        <br>

        <div class="row">

            <div class="col-sm-1">
            </div>

            <div class="col-sm-10" >
                <label class="control-label">
                    Observação:
                </label>
            </div>

            <div class="col-sm-1">
            </div>

        </div>

        <div class="row">

            <div class="col-sm-1">
            </div>

            <div class="col-sm-10" >
                <textarea id="observacao" class="form form-control" style="width: 100%; margin-right : -50px"></textarea>
            </div>

            <div class="col-sm-1" style="margin-left : 20px">
            </div>

        </div>

        <div class="row" style="padding: 20px;">
            <div class="col-sm-1">
            </div>

            <button class="btn btn-default" id="back_table">Voltar</button>
            <button class="btn btn-success" id="save_estorno">Salvar</button>
        </div>
    </div>

</div>

<div class="modal fade" id="modal_confirm" tabindex="-1">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Confirmar Baixa</h4>
        </div>
        <div class="modal-body">
            <p>Tem certeza que esse valor foi abatido?</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-primary" id="cbaixa_btn">Confirmar</button>
            <input type="hidden" id="prop_id"></input>
        </div>
    </div>
</div>

<style type="text/css">
    #grid_propostas_length, #grid_propostas_filter, #grid_propostas_info, #grid_propostas_paginate{
        display: none;
    }
    td.details-control {
        background: url('../../images/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.details td.details-control {
        background: url('../../images/details_close.png') no-repeat center center;
    }
</style>