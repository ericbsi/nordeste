<?php

	$this->breadcrumbs=array(
		'Empresas'=>array('index'),
		'Criar',
	);

?>

<?php 
	$this->renderPartial('_form_wizard', 
		array(
			'model'=>$model, 
			'dadosBancarios'=>$dadosBancarios, 
			'endereco'=>$endereco,
			'telefone'=>$telefone,
			'email'=>$email,
		));
?>