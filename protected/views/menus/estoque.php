<?php  ?>
<ul class="main-navigation-menu">
   <li>
      <a href="<?php echo Yii::app()->getBaseUrl(true) .'/'. Yii::app()->session['usuario']->getRole()->login_redirect;?>"><i class="fa fa-home"></i>
      <span class="title"> Inicial </span>
      <span class="selected"></span>
      </a>
   </li>
   <li>
      <a href="javascript:void(0)">
      <i class="fa fa-sitemap"></i>
      <span class="title">Estoque</span><i class="icon-arrow"></i>
      <span></span>
      </a>
      <ul class="sub-menu">
         <li>
            <a href="/estoque/">
            <i class="fa fa-edit"></i>
            <span class="title">Novo estoque</span>
            </a>
         </li>
         <li>
            <a href="/inventario/">
            <i class="fa fa-edit"></i>
            <span class="title">Inventário</span>
            </a>
         </li>
      </ul>
   </li>   
   <li>
      <a href="javascript:;" class="active">
      <i class="fa fa-sitemap"></i>
      <span class="title"> Produtos </span>
      <i class="icon-arrow"></i>
      </a>
      <ul class="sub-menu">
         <li>
            <a href="javascript:;">
            Cadastros <i class="icon-arrow"></i>
            </a>
            <ul class="sub-menu">
               <li>
                  <a href="/caracteristicaProduto/">
                  Características
                  </a>
               </li>
               <li>
                  <a href="/categoriaProduto/">
                  Categorias
                  </a>
               </li>
               <li>
                  <a href="/fabricante/">
                  Fabricante
                  </a>
               </li>
               <li>
                  <a href="/fornecedor/">
                  Fornecedor
                  </a>
               </li>
               <li>
                  <a href="/produto/">
                  Produto
                  </a>
               </li>
            </ul>
         </li>
      </ul>
   </li>
</ul>