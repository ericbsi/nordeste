<?php  ?>
<ul class="main-navigation-menu">
   <li>
      <a href="<?php echo Yii::app()->getBaseUrl(true) .'/'. Yii::app()->session['usuario']->getRole()->login_redirect;?>"><i class="fa fa-home"></i>
         <span class="title"> Inicial </span>
         <span class="selected"></span>
      </a>
   </li>
   <li>
      <a href="#"><i class="clip-stack"></i>
         <span class="title"> Relatórios </span>
         <span class="selected"></span>
      </a>
      <ul class="sub-menu" style="display: none;">
         <li>
            <a href="/contabil/contratos/">
            <span class="title"> Contratos / Crediários </span>
            </a>
         </li>
      </ul>
   </li>
</ul>