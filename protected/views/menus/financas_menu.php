<?php $util = new Util; ?>
<ul class="main-navigation-menu">
   <li>
      <a href="javascript:void(0)"><i class="fa fa-home"></i>
      <span class="title"> Cnab </span><i class="icon-arrow"></i>
      <span class="selected"></span>
      </a>
      <ul class="sub-menu">
        <li>
            <a href="<?php echo Yii::app()->request->baseUrl;?>/financeiro/cnabRemessa">
               <i class="fa fa-edit"></i>
               <span class="title"> Gerar remessa </span>
            </a>
        </li>
        <li>
         <a href="<?php echo Yii::app()->request->baseUrl;?>/financeiro/retorno">
            <i class="clip-list"></i>
            <span class="title"> Importar retorno </span>
         </a>
        </li>
        <li>
         <a href="<?php echo Yii::app()->request->baseUrl;?>/reports/relatorioNordeste">
            <i class="fa fa-edit"></i>
            <span class="title">Gerar Relatorio - S2</span>
         </a>
        </li>
      </ul>
   </li>
   <li>
      <a href="/empresa/recebimentos/">
         <i class="clip-stats"></i>
         <span class="title">Recebimentos</span>
         <span class="selected"></span>
      </a>
   </li>
</ul>