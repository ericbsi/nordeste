<?php ?>
<ul class="main-navigation-menu">
    <li>
        <a href="<?php echo Yii::app()->request->baseUrl; ?>/proposta/minhasPropostas/"><i class="fa fa-home"></i>
            <span class="title"> Inicial </span>
            <span class="selected"></span>
        </a>
    </li>
    <li>
        <a href="/filial/propostas/">
            <i class="clip-stack"></i>
            <span class="title">Propostas Filial</span>
            <span class="selected"></span>
        </a>
    </li>
    <li>
        <a href="/crediarista/minhasAnalises/">
            <i class="clip-stack"></i>
            <span class="title">Minhas análises</span>
            <span class="selected"></span>
        </a>
    </li>
    <li class="">
        <a href="javascript:void(0)"><i class="clip-pencil"></i>
            <span class="title"> Relatórios </span><i class="icon-arrow"></i>
            <span class="selected"></span>
        </a>
        <ul class="sub-menu" style="display: none;">
            <li>
                <a href="/parceiro/relatorioDeProducao/">
                    <span class="title">Produção</span>
                </a>
            </li>
        </ul>
    </li>
</ul>