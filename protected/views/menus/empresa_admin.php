<?php ?>
<ul class="main-navigation-menu">
    <li>
        <a href="<?php echo Yii::app()->getBaseUrl(true) . '/' . Yii::app()->session['usuario']->getRole()->login_redirect; ?>"><i class="fa fa-home"></i>
            <span class="title"> Inicial </span>
            <span class="selected"></span>
        </a>
    </li>

    <?php if( Yii::app()->session['usuario']->id == 268 || Yii::app()->session['usuario']->id == 236 ): ?>
      <li>
        <a href="<?php echo Yii::app()->request->baseUrl; ?>/interno/relatorioPagamentos/"><i class="fa fa-money"></i>
          <span class="title"> Recebimento Interno </span><i class="icon-arrow"></i>
          <span class="selected"></span>
        </a>
      </li>
    <?php endif; ?>

    <?php if (in_array(Yii::app()->session['usuario']->id, [4, 333, 343, 236, 268])) { ?>

        <li>
            <a href="javascript:void(0)"><i class="fa fa-bar-chart-o"></i>
                <span class="title"> Cotações </span><i class="icon-arrow"></i>
                <span class="selected"></span>
            </a>
            <ul class="sub-menu">
                <!--<li>
                   <a href="<?php //echo Yii::app()->request->baseUrl;   ?>/cotacao/admin">
                      <i class="clip-list"></i>
                      <span class="title"> Administrar Cotações </span>
                   </a>
                   </li>-->
                <li>
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/cotacao/tabelas">
                        <i class="clip-list"></i>
                        <span class="title"> Administrar Tabelas </span>
                    </a>
                </li>
            </ul>
        </li>

    <?php } ?>
    <li>
        <a href="/pagamento/iniciarBordero/">
            <i class="fa fa-edit"></i>
            <span class="title"> Gerar Borderô </span>
        </a>
    </li>
    <li>
        <a href="javascript:void(0)">
            <i class="clip-cancel-circle"></i>
            <span class="title">Soli. Cancelamentos</span><i class="icon-arrow"></i>
            <span class="selected"></span>
        </a>
        <ul class="sub-menu">
            <li>
                <a href="/solicitacaoDeCancelamento/">
                    <i class="clip-info-2"></i>
                    <span class="title">Pendentes</span>
                </a>
            </li>
            <li>
                <a href="/solicitacaoDeCancelamento/concluidos/">
                    <i class="clip-info-2"></i>
                    <span class="title">Efetivados / Recusados</span>
                </a>
            </li>
        </ul>
    </li>
    <li>
        <a href="<?php echo Yii::app()->request->baseUrl; ?>/integracao/propostaXMLparaCNAB">
            <i class="fa fa-edit"></i>
            <span class="title"> Gerar remessa FIDC</span>
        </a>
    </li>
    <li>
        <a href="<?php echo Yii::app()->request->baseUrl; ?>/recebimentoDeDocumentacao/RecebimentoDocumentacao">
            <i class="fa fa-edit"></i>
            <span class="title"> Recebimento de Documentação </span>
        </a>
    </li>
      <li>
        <a href="<?php echo Yii::app()->request->baseUrl; ?>/interno/relatorioPagamentos/"><i class="fa fa-money"></i>
          <span class="title"> Recebimento Interno </span><i class="icon-arrow"></i>
          <span class="selected"></span>
        </a>
      </li>
    <li>
        <a href="<?php echo Yii::app()->request->baseUrl; ?>/recebimentoDeDocumentacao/Malotes">
            <i class="fa fa-edit"></i>
            <span class="title"> Listar Malotes </span>
        </a>
    </li>
    <li>
        <a href="<?php echo Yii::app()->request->baseUrl; ?>/integracao/validarNFE">
            <i class="fa fa-edit"></i>
            <span class="title"> Importar XML</span>
        </a>
    </li>
    <li>
        <a href="<?php echo Yii::app()->request->baseUrl; ?>/estornoBaixa">
            <i class="clip-rotate-2"></i>
            <span class="title">Estorno de Baixas</span>
        </a>
    </li>
    <li>
        <a href="<?php echo Yii::app()->request->baseUrl; ?>/estornoBaixa/parcelas">
            <i class="clip-rotate-2"></i>
            <span class="title">Estorno de Baixas de Clientes</span>
        </a>
    </li>
    <li>
        <a href="javascript:void(0)">
            <i class="fa fa-cogs"></i>
            <span class="title"> Configurações</span><i class="icon-arrow"></i>
            <span class="selected"></span>
        </a>
        <ul class="sub-menu">
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/listaNegra/config">
                    <i class="fa fa-cogs"></i>
                    <span class="title">Parâmetros de Sistema</span>
                </a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/listaNegra/configLecca">
                    <i class="fa fa-cogs"></i>
                    <span class="title">Parâmetros Lecca</span>
                </a>
            </li>
        </ul>
    </li>
    <li>
        <a href="javascript:void(0)">
            <i class="fa fa-sitemap"></i>
            <span class="title"> Filiais</span><i class="icon-arrow"></i>
            <span class="selected"></span>
        </a>
        <ul class="sub-menu">
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/filial/create">
                    <i class="fa fa-edit"></i>
                    <span class="title"> Cadastrar Filial </span>
                </a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/filial/admin">
                    <i class="clip-list"></i>
                    <span class="title"> Administrar Filiais </span>
                </a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/grupoFiliais/index">
                    <i class="clip-list"></i>
                    <span class="title"> Grupos de Filiais </span>
                </a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/grupoFiliais/nucleosFiliais">
                    <i class="clip-list"></i>
                    <span class="title"> Núcleos de Filiais </span>
                </a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/politicaCredito/index">
                    <i class="clip-list"></i>
                    <span class="title"> Políticas de Crédito </span>
                </a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/conciliacao/">
                    <i class="clip-list"></i>
                    <span class="title"> Conciliação </span>
                </a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/servico/">
                    <i class="clip-list"></i>
                    <span class="title"> Serviços </span>
                </a>
            </li>
        </ul>
    </li>
    <!---->

    <li class="">
        <a href="javascript:void(0)"><i class="clip-pencil"></i>
            <span class="title"> Relatórios </span><i class="icon-arrow"></i>
            <span class="selected"></span>
        </a>
        <ul class="sub-menu" style="display: none;">
            <!--<li>
               <a href="/reports/financiamentos/">
               <span class="title">Financiamentos</span>
               </a>
            </li>-->
            <li>
                <a href="/empresa/propostas/">
                    <span class="title"> Propostas </span>
                </a>
            </li>
            <?php if (in_array(Yii::app()->session['usuario']->id, [4, 333, 343, 236, 268])) { ?>
                <li>
                    <a href="/reports/producao/">
                        <span class="title">Produção</span>
                    </a>
                </li>

            <?php } ?>

            <li>
                <a href="/reports/producaoAnalistas/">
                    <span class="title">Produção Analistas</span>
                </a>
            </li>

            <?php if (in_array(Yii::app()->session['usuario']->id, [4, 333, 343, 268, 236, 268])) { ?>
                <li>
                    <a href="/reports/analisarBads/">
                        <span class="title">Bads</span>
                    </a>
                </li>

            <?php } ?>
            <li>
                <a href="/reports/receberVencimento/">
                    <span class="title">Receber por Vencimento</span>
                </a>
            </li>
            <li>
                <a href="/reports/jurosAtraso/">
                    <span class="title">Juros por Atraso</span>
                </a>
            </li>
            <li>
                <a href="/empresa/cancelamentosPagos/">
                    <span class="title">Cancelamentos Pagos</span>
                </a>
            </li>
            <li>
                <a href="/reports/totaisClientesRegiao/">
                    <span class="title">Totais de Clientes por Região</span>
                </a>
            </li>
        </ul>
    </li>

    <!---->
    <li>
        <a href="javascript:void(0)"><i class="clip-users"></i>
            <span class="title"> Usuarios </span><i class="icon-arrow"></i>
            <span class="selected"></span>
        </a>
        <ul class="sub-menu">
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/usuario/create/">
                    <i class="clip-user-plus"></i>
                    <span class="title"> Cadastrar Usuário </span>
                </a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/usuario/admin/">
                    <i class="clip-list"></i>
                    <span class="title"> Administrar Usuários </span>
                </a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/usuario/administradores/">
                    <i class="clip-list"></i>
                    <span class="title"> Administradores</span>
                </a>
            </li>
        </ul>
    </li>

    <?php if (in_array(Yii::app()->session['usuario']->id, [4, 333, 343, 236, 268, 237])) { ?>

        <li>
            <a href="javascript:void(0)"><i class="clip-users"></i>
                <span class="title"> Grupos de analistas </span><i class="icon-arrow"></i>
                <span class="selected"></span>
            </a>
            <ul class="sub-menu">
                <li>
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/GrupoDeAnalistas/create">
                        <i class="clip-list"></i>
                        <span class="title"> Criar Grupos </span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/GrupoDeAnalistas/admin">
                        <i class="clip-list"></i>
                        <span class="title"> Administrar Grupos </span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="/empresa/recebimentos/">
                <i class="clip-stats"></i>
                <span class="title">Recebimentos</span>
                <span class="selected"></span>
            </a>
        </li>
        <li>
            <a href="javascript:void(0)">
                <i class="clip-stack"></i>
                <span class="title"> Contratos</span><i class="icon-arrow"></i>
                <span class="selected"></span>
            </a>
            <ul class="sub-menu">
                <li>
                    <a href="/pagamento/">
                        <i class="fa fa-edit"></i>
                        <span class="title"> Registrar recebimento </span>
                    </a>
                </li>
                <li>
                    <a href="/recebimentoDeDocumentacao/historico/">
                        <i class="clip-list"></i>
                        <span class="title"> Histórico </span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:void(0)">
                <i class="clip-phone-4"></i>
                <span class="title">Cobranças</span><i class="icon-arrow"></i>
                <span class="selected"></span>
            </a>
            <ul class="sub-menu">
                <li>
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/cobranca/atrasos/">
                        <i class="clip-calendar"></i>
                        <span class="title">Atrasos</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/cobranca/fichamentos/">
                        <i class="clip-calendar"></i>
                        <span class="title">Fichamentos</span>
                    </a>
                </li>
            </ul>
        </li>
    <?php } ?>
</ul>
