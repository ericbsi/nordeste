<?php ?>
<ul class="main-navigation-menu" style="padding-bottom:120px!important">
    <?php if (!in_array(Yii::app()->session['usuario']->id, [973, 974, 1007, 1009, 1036, 1037, 1038, 1039, 1040, 334])): ?>
        <li>
            <a href="<?php echo Yii::app()->getBaseUrl(true) . '/' . Yii::app()->session['usuario']->getRole()->login_redirect; ?>"><i class="fa fa-home"></i>
                <span class="title"> Inicial </span>
                <span class="selected"></span>
            </a>
        </li>
        <li>
            <a href="javascript:void(0)">
                <i class="clip-users"></i>
                <span class="title">Clientes</span><i class="icon-arrow"></i>
                <span class="selected"></span>
            </a>
            <ul class="sub-menu">
                <li>
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/cliente/listarClientes">
                        <i class="fa fa-edit"></i>
                        <span class="title">Administrar clientes</span>
                    </a>
                </li>
            </ul>
        </li>
        <?php if (Yii::app()->session['usuario']->id == 541 || Yii::app()->session['usuario']->id == 13): ?>
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/estornoBaixa">
                    <i class="clip-rotate-2"></i>
                    <span class="title">Estorno de Baixas</span>
                </a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/estornoBaixa/parcelas">
                    <i class="clip-rotate-2"></i>
                    <span class="title">Estorno de Baixas de Clientes</span>
                </a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/interno/relatorioPagamentos/"><i class="fa fa-money"></i>
                    <span class="title"> Recebimento Interno </span><i class="icon-arrow"></i>
                    <span class="selected"></span>
                </a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/interno/relatorioPagamentos/"><i class="fa fa-money"></i>
                    <span class="title"> Recebimento Interno </span><i class="icon-arrow"></i>
                    <span class="selected"></span>
                </a>
            </li>
            <li>
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/reports/relatorioTitulos">
                            <i class="fa fa-edit"></i>
                            <span class="title">Gerar Relatorio - S2</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/reports/relatorioTitulosCorban">
                            <i class="fa fa-file-text-o"></i>
                            <span class="title">Relatorio CORBAN</span>
                        </a>
                    </li>
                    <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/financeiro/retorno">
                    <i class="clip-list"></i>
                    <span class="title"> Importar retorno </span>
                </a>
            </li>
        <?php endif; ?>
        <li>
            <a href="javascript:void(0)">
                <i class="clip-search"></i>
                <span class="title">Propostas</span><i class="icon-arrow"></i>
                <span class="selected"></span>
            </a>
            <ul class="sub-menu">
                <li>
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/proposta/admin">
                        <i class="fa fa-edit"></i>
                        <span class="title">Novas</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="">
            <a href="javascript:void(0)"><i class="clip-pencil"></i>
                <span class="title"> Relatórios </span><i class="icon-arrow"></i>
                <span class="selected"></span>
            </a>
            <ul class="sub-menu" style="display: none;">
                <!--<li>
                   <a href="/reports/financiamentos/">
                   <span class="title">Financiamentos</span>
                   </a>
                </li>-->
                <li>
                    <a href="/reports/producao/">
                        <span class="title">Produção</span>
                    </a>
                </li>
                <li>
                    <a href="/reports/minhaProducao/">
                        <span class="title">Minha Produção</span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:void(0)">
                <i class="clip-phone-4"></i>
                <span class="title">Cobranças</span><i class="icon-arrow"></i>

            </a>
            <ul class="sub-menu">
                <li>
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/cobranca/atrasos/">
                        <i class="clip-calendar"></i>
                        <span class="title">Atrasos</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/cobranca/fichamentos/">
                        <i class="clip-calendar"></i>
                        <span class="title">Fichamentos</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/auditoria/parcelas/">
                        <i class="fa fa-exclamation-triangle"></i>
                        <span class="title">Auditorias</span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/cobranca/cobrancas/">
                <i class="fa clip-map-2"></i>
                <span class="title">Cobranças Unificadas</span><i class="icon-arrow"></i>
                <span class="selected"></span>
            </a>
        </li>
        <li>
            <a href="/proposta/reaproveitarList/">
                <i class="clip-cancel-circle"></i>
                <span class="title">Reaproveitar Propostas</span><i class="icon-arrow"></i>
                <span class="selected"></span>
            </a>
        </li>
        <li>
            <a href="/proposta/liberarPropostas/">
                <i class="clip-cancel-circle"></i>
                <span class="title">Liberar Propostas</span><i class="icon-arrow"></i>
                <span class="selected"></span>
            </a>
        </li>
        <li>
            <a href="javascript:void(0)">
                <i class="clip-stack"></i>
                <span class="title"> Contratos</span><i class="icon-arrow"></i>
                <span class="selected"></span>
            </a>
            <ul class="sub-menu">
                <li>
                    <a href="/pagamento/iniciarBordero/">
                        <i class="fa fa-edit"></i>
                        <span class="title"> Registrar Recebimento </span>
                    </a>
                </li>
                <li>
                    <a href="/pagamento/registrarPendencias/">
                        <i class="fa fa-edit"></i>
                        <span class="title"> Registrar Pendências </span>
                    </a>
                </li>
                <li>
                    <a href="/pagamento/relatorioPendencias/">
                        <i class="fa fa-edit"></i>
                        <span class="title"> Relatório Pendências </span>
                    </a>
                </li>
                <li>
                    <a href="/recebimentoDeDocumentacao/historico/">
                        <i class="clip-list"></i>
                        <span class="title"> Histórico </span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="/generico/motivoNegacao">
                <i class="clip-list"></i>
                <span class="title">Cadastro Negativas</span>
            </a>
        </li>
        <li>
            <a href="javascript:void(0)">
                <i class="clip-cancel-circle"></i>
                <span class="title">Soli. Cancelamentos</span><i class="icon-arrow"></i>
                <span class="selected"></span>
            </a>
            <ul class="sub-menu">
                <li>
                    <a href="/solicitacaoDeCancelamento/">
                        <i class="clip-info-2"></i>
                        <span class="title">Pendentes</span>
                    </a>
                </li>
                <li>
                    <a href="/solicitacaoDeCancelamento/concluidos/">
                        <i class="clip-info-2"></i>
                        <span class="title">Efetivados / Recusados</span>
                    </a>
                </li>
            </ul>
        </li>
        <?php if (Yii::app()->session['usuario']->id == 545): ?>
            <li>
                <a href="/recebimentoDeDocumentacao/RecebimentoDocumentacao/">
                    <i class="clip-list"></i>
                    <span class="title">Recebimento Documentação</span>
                </a>
            </li>
        <?php endif; ?>
    <?php else: ?>
        <li>
            <a href="<?php echo Yii::app()->getBaseUrl(true) . '/' . Yii::app()->session['usuario']->getRole()->login_redirect; ?>"><i class="fa fa-home"></i>
                <span class="title"> Inicial </span>
                <span class="selected"></span>
            </a>
        </li>
        <?php if (Yii::app()->session['usuario']->id == 334): ?>
            <li>
                <a href="javascript:void(0)">
                    <i class="clip-users"></i>
                    <span class="title">Clientes</span><i class="icon-arrow"></i>
                    <span class="selected"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/cliente/listarClientes">
                            <i class="fa fa-edit"></i>
                            <span class="title">Administrar clientes</span>
                        </a>
                    </li>
                </ul>
            </li>
        <?php endif; ?>
    <?php endif; ?>
</ul>
