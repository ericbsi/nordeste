<?php $util = new Util; ?>
<ul class="main-navigation-menu">
    <li>
        <a href="<?php echo Yii::app()->request->baseUrl; ?>/financeiro/"><i class="fa fa-home"></i>
            <span class="title"> Financeiro </span><i class="icon-arrow"></i>
            <span class="selected"></span>
        </a>
    </li>
    <?php if( Yii::app()->session['usuario']->id == 684 ): ?>
      <li>
        <a href="<?php echo Yii::app()->request->baseUrl; ?>/interno/relatorioPagamentos/"><i class="fa fa-money"></i>
          <span class="title"> Recebimento Interno </span><i class="icon-arrow"></i>
          <span class="selected"></span>
        </a>
      </li>
    <?php endif; ?>
    <li>
        <a href="javascript:void(0)"><i class="fa fa-tasks"></i>
            <span class="title"> Cnab </span><i class="icon-arrow"></i>
            <span class="selected"></span>
        </a>
        <ul class="sub-menu">
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/integracao/propostaXMLparaCNAB">
                    <i class="fa fa-edit"></i>
                    <span class="title"> Gerar remessa FIDC</span>
                </a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/formalizacao/propostasFormalizacao">
                    <i class="fa fa-edit"></i>
                    <span class="title"> Formalizar FIDC</span>
                </a>
            </li>
            <!--
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/financeiro/cnabRemessa">
                    <i class="fa fa-edit"></i>
                    <span class="title"> Gerar remessa </span>
                </a>
            </li>
            -->
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/financeiro/remessas">
                    <i class="fa fa-edit"></i>
                    <span class="title"> Remessas </span>
                </a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/financeiro/retorno">
                    <i class="clip-list"></i>
                    <span class="title"> Importar retorno </span>
                </a>
            </li>
        </ul>
    </li>
    <li>
        <a href="javascript:void(0)"><i class="clip-database"></i>
            <span class="title"> Cadastros </span><i class="icon-arrow"></i>
            <span class="selected"></span>
        </a>
        <ul class="sub-menu">
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/dadosBancarios/contasEmpresa">
                    <i class="fa fa-archive"></i>
                    <span class="title"> Contas da Empresa </span>
                </a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/grupoFiliais/">
                    <i class="fa fa-archive"></i>
                    <span class="title"> Grupos Filiais </span>
                </a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/grupoFiliais/nucleosFiliais">
                    <i class="fa fa-archive"></i>
                    <span class="title"> Núcleos Filiais </span>
                </a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/filial/admin">
                    <i class="fa fa-archive"></i>
                    <span class="title"> Administrar Filiais </span>
                </a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/fornecedor/">
                    <i class="fa fa-archive"></i>
                    <span class="title"> Fornecedores </span>
                </a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/naturezaTitulo/">
                    <i class="fa fa-archive"></i>
                    <span class="title"> Naturezas </span>
                </a>
            </li>
        </ul>
    </li>
    <li>
        <a href="<?php echo Yii::app()->request->baseUrl; ?>/estornoBaixa">
            <i class="clip-rotate-2"></i>
            <span class="title">Estorno de Baixas</span>
        </a>
    </li>
    <li>
        <a href="<?php echo Yii::app()->request->baseUrl; ?>/estornoBaixa/parcelas">
            <i class="clip-rotate-2"></i>
            <span class="title">Estorno de Baixas de Clientes</span>
        </a>
    </li>
    <li>
        <a href="/empresa/recebimentos/">
            <i class="clip-stats"></i>
            <span class="title">Recebimentos</span>
            <span class="selected"></span>
        </a>
    </li>
    <li>
        <a href="/repasse/">
            <i class="fa fa-share"></i>
            <span class="title">Repasses</span>
            <span class="selected"></span>
        </a>
    </li>
    <li>
        <a href="/pagamento/iniciarBordero/">
            <i class="fa fa-edit"></i>
            <span class="title"> Gerar Borderô </span>
        </a>
    </li>
    <li>
        <a href="/recebimentoDeDocumentacao/RecebimentoDocumentacao/">
            <i class="fa fa-tasks"></i>
            <span class="title"> Receber Documentação </span>
        </a>
    </li>
    <li>
        <a href="/recebimentoDeDocumentacao/Malotes">
            <i class="clip-pencil"></i>
            <span class="title"> Listar Malotes </span>
        </a>
    </li>
    <li>
      <a href="javascript:void(0)">
      <i class="clip-cancel-circle"></i>
      <span class="title">Soli. Cancelamentos</span><i class="icon-arrow"></i>
      <span class="selected"></span>
      </a>
      <ul class="sub-menu">
         <li>
            <a href="/solicitacaoDeCancelamento/">
            <i class="clip-info-2"></i>
            <span class="title">Pendentes</span>
            </a>
         </li>
         <li>
            <a href="/solicitacaoDeCancelamento/concluidos/">
            <i class="clip-info-2"></i>
            <span class="title">Efetivados / Recusados</span>
            </a>
         </li>
      </ul>
   </li>
   <li>
        <a href="/financeiro/gerarLiquidacao/">
            <i class="fa fa-file-text-o" aria-hidden="true"></i>
            <span class="title"> Importaçao DEPARA </span>
        </a>
    </li>
    <li>
         <a href="<?php echo Yii::app()->request->baseUrl;?>/reports/relatorioTitulos">
            <i class="fa fa-edit"></i>
            <span class="title">Gerar Relatorio - S2</span>
         </a>
     </li>
     <li>
         <a href="<?php echo Yii::app()->request->baseUrl;?>/reports/relatorioTitulosCorban">
            <i class="fa fa-file-text-o"></i>
            <span class="title">Relatorio CORBAN</span>
         </a>
     </li>
</ul>
