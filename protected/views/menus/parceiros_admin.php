<?php  ?>
<ul class="main-navigation-menu">
   <li>
      <a href="<?php echo Yii::app()->getBaseUrl(true) .'/'. Yii::app()->session['usuario']->getRole()->login_redirect;?>"><i class="fa fa-home"></i>
      <span class="title"> Inicial </span>
      <span class="selected"></span>
      </a>
   </li>
   <li>
      <a href="javascript:void(0)"><i class="fa fa-bar-chart-o"></i>
      <span class="title"> Relatórios </span><i class="icon-arrow"></i>
      <span class="selected"></span>
      </a>
      <ul class="sub-menu">
<!--         <li>
            <a href="<?php echo Yii::app()->request->baseUrl;?>/administradorDeParceiros/producao/">
            <i class="clip-list"></i>
            <span class="title"> Produção </span>
            </a>
         </li>
         <li>
            <a href="<?php echo Yii::app()->request->baseUrl;?>/seguro/">
            <i class="clip-list"></i>
            <span class="title"> Seguro </span>
            </a>
         </li>-->
         <li>
            <a href="<?php echo Yii::app()->request->baseUrl;?>/financeiro/contasPagar">
            <i class="clip-list"></i>
            <span class="title"> Valores a Receber </span>
            </a>
         </li>
         <li>
            <a href="<?php echo Yii::app()->request->baseUrl;?>/financeiro/contasPagas">
            <i class="clip-list"></i>
            <span class="title"> Valores Pagos </span>
            </a>
         </li>
      </ul>
   </li>
</ul>