<?php ?>
<ul class="main-navigation-menu">
    <li>
        <a href="<?php echo Yii::app()->getBaseUrl(true) . '/' . Yii::app()->session['usuario']->getRole()->login_redirect; ?>"><i class="fa fa-home"></i>
            <span class="title"> Inicial </span>
            <span class="selected"></span>
        </a>
    </li>
    <li>
        <a href="javascript:void(0)">
            <i class="clip-users"></i>
            <span class="title">Clientes</span><i class="icon-arrow"></i>
            <span class="selected"></span>
        </a>
        <ul class="sub-menu">
            <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/cliente/listarClientes">
                    <i class="fa fa-edit"></i>
                    <span class="title">Administrar clientes</span>
                </a>
            </li>
        </ul>
</ul>