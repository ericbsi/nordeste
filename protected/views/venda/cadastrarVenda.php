<?php ?>
<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/venda/Cadastrar Venda">
                    Venda
                </a>
            </li>
            <li class="active">
                Cadastrar
            </li>
        </ol>
        <div class="page-header">
            <h1>Venda</h1>
        </div>
    </div>
</div>
<div class="row">
    <form id="form-add-venda" action="/venda/cadastrarVenda/" method="post">
        <div class="modal-body">
            <div class="row">
                <div class="row-centralize">
                    <div class="col-md-2">
                        <label class="control-label">
                            Data <span class="symbol required"></span>
                        </label>
                        <input value="<?php echo date('d/m/Y') ?>" readonly="true" type="text" required name="Venda[data]" type="text" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">Cliente <span class="symbol required"></span></label>
                        <select required="required" name="Venda[Cliente_id]" id="Venda_Cliente_id" class="form-control search-select select2">
                            <option value="0">Selecione:</option>
                            <?php foreach (Cliente::model()->findAll() as $cliente) { ?>
                                <option value="<?php echo $cliente->id ?>"><?php echo $cliente->pessoa->nome ?></option>
                            <?php } ?>                              
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label class="control-label">Vendedor <span class="symbol required"></span></label>
                        <select required="required" id="Venda_Vendedor_id" class="form-control search-select select2">
                            <option value="0">Selecione:</option>
                            <option value="1">Teste 1</option>
                            <option value="2">Teste 2</option>
                        </select>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="row-centralize">
                    <div class="col-md-3">
                        <label class="control-label">Valor Mercadoria</label>
                        <input id="valMer" value="0" readonly="true" class="form-control">
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">Valor Descontos</label>
                        <input id="valDes" value="0" readonly="true" class="form-control">
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">Valor Acrescentado</label>
                        <input id="valAcr" value="0" readonly="true" class="form-control">
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">Valor a Pagar</label>
                        <input id="valPag" value="0" readonly="true" class="form-control">
                    </div>
                    <div class="col-md-3">
                        <label class="control-label">Valor Final</label>
                        <input id="valFin" value="0" readonly="true" class="form-control">
                    </div>
                    <input type="hidden" id="valMerc" >
                </div>
            </div>
        </div>
        <input id="finaliza" type="hidden" name="finaliza" value="0">
    </form>
</div>
<div class="row">
    <br>
</div>
<div class="row">
    <form id="form-vd-it">
        <div class="col-sm-12">
            <table id="grid_Venda_Item_da_Venda" hidden="true" class="table table-striped table-bordered table-hover table-full-width dataTable">
                <thead>
                    <tr>
                        <th>Item</th>
                        <th>Produto</th>
                        <th>Quantidade</th>
                        <th>Preço de Tabela</th>
                        <th>% Desconto</th>
                        <th>Preço de Venda</th>
                        <td style="width:10px;">
                            <a style="padding:1px 5px; font-size:12px;" id="btn-modalItVenda-show" href="#modal_new_Venda_Item_da_Venda" data-toggle="modal" type="submit" class="btn btn-green">+</a>
                        </td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </form>
</div>
<div class="row">
    <br>
</div>
<div class="row">
    <div class="col-sm-12">
        <table id="grid_Venda_CP" hidden="true" class="table table-striped table-bordered table-hover table-full-width dataTable">
            <thead>
                <tr>
                    <th>Forma de Pagamento</th>
                    <th>Parcelas</th>
                    <th>Valor</th>
                    <td style="width:10px;">
                        <a style="padding:1px 5px; font-size:12px;" id="btn-modalPgVenda-show" data-toggle="modal" type="submit" class="btn btn-green">+</a>
                    </td>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <br>
    <br>
</div>
<div class="row">
    <div id="div-btn-salvar" hidden="true" class="col-md-1">
        <button id="btn-salvar-venda" type="submit" class="btn btn-blue">Salvar</button>
    </div>
    &nbsp;
    &nbsp;
    <div id="div-btn-efetivar" hidden="true" class="col-md-1">
        <button id="btn-efetivar-venda" type="submit" class="btn btn-blue">Efetivar</button>
    </div>
</div>
<div id="modal_new_Venda_CP" class="modal fade" tabindex="-1" data-width="480" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Inserir Forma de Pagamento</h4>
    </div>
    <form id="form-add-FP">
        <div class="modal-body">
            <div class="row-centralize">
                <div class="col-md-10">
                    <div class="form-group">
                        <label class="control-label">
                            Forma de Pagamento <span class="symbol required"></span>
                        </label>
                        <input required="true" name="FP[id]" placeholder="Selecione" type="hidden" id="FP_id" class="form-control search-select">
                        <!--<select class="form-control search-select select2" name="CP[FP_id]" id="FP_id">
                           <option value="">Selecione: </option>
                        <?php /* foreach (FormaDePagamentoHasCondicaoDePagamento::model()->searchUnique('') as $fp) { ?>
                          <option value="<?php echo $fp->id ?>">
                          <?php echo $fp->formaDePagamento->sigla . ' - ' . $fp->qtdParcelasDe . ' até ' . $fp->qtdParcelasAte . 'X' ?>
                          </option>
                          <?php } */ ?>
                           </select>-->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="control-label">
                                    Qtd de Parcelas <span class="symbol required"></span>
                                </label>
                                <input required name="CP[parcelas]" id="parcelas" class="form-control" type="number">
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="control-label">
                                    Valor <span class="symbol required"></span>
                                </label>
                                <input required name="CP[valorFP]" id="valorFP" class="form-control" type="number">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <!--<label class="checkbox-inline" style="float:left">
               <input id="checkbox_continuar" type="checkbox" value="">
               Continuar Cadastrando
               </label>-->
            <button type="button" data-dismiss="modal" class="btn btn-light-red">Cancelar</button>
            <button type="submit" id="btn-add-fp" class="btn btn-blue">Adicionar</button>
            <div class="row">
            </div>
            <br>
        </div>
    </form>
</div>
<div id="modal_new_Venda_Item_da_Venda" class="modal fade" tabindex="-1" data-width="960" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Inserir item</h4>
    </div>
    <form id="form-add-item">
        <div class="modal-body">
            <div class="row">
                <div class="row-centralize">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label class="control-label">
                                        Produto <span class="symbol required"></span>
                                    </label>
                                    <select class="form-control search-select select2" name="Item_da_Venda[Item_do_Estoque_id]" id="Item_da_Venda_id">
                                        <option value="">Selecione: </option>
                                        <?php foreach (ItemDoEstoque::model()->findAll(array("condition" => "saldo > 0")) as $item) { ?>
                                            <option value="<?php echo $item->id ?>"><?php echo $item->produto->descricao ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="control-label">
                                        Quantidade <span class="symbol required"></span>
                                    </label>
                                    <input required name="Item_da_Venda[quantidade]" id="quantidade" class="form-control" type="number">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="control-label">
                                        Preço de Tabela 
                                    </label>
                                    <input required name="Item_da_Venda[precoTabela]" disabled="true" id="preco_tabela" class="form-control moeda" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">
                                        % Desconto <span class="symbol required"></span>
                                    </label>
                                    <input required name="Item_da_Venda[desconto]" id="desconto" class="form-control" type="number">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="control-label">
                                        Preço de Venda
                                    </label>
                                    <input required name="Item_da_Venda[precoVenda]" value="0" disabled="true" id="precoVenda" class="form-control moeda" type="number">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <!--            <label class="checkbox-inline" style="float:left">
               <input id="checkbox_continuar" type="checkbox" value="">
               Continuar Cadastrando
               </label>-->
            <button type="button" data-dismiss="modal" class="btn btn-red">Cancelar</button>
            <button type="submit" id="btn-add-item" class="btn btn-blue">Adicionar</button>
            <div class="row">
            </div>
            <br>
        </div>
    </form>
</div>
<div id="modal_edit_Venda_Item_da_Venda" class="modal fade" tabindex="-1" data-width="960" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Editar item</h4>
    </div>
    <form id="form-edit-item">
        <div class="modal-body">
            <div class="row">
                <div class="row-centralize">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label class="control-label">
                                        Produto <span class="symbol required"></span>
                                    </label>
                                    <select class="form-control search-select select2" name="Item_da_Venda[Item_do_Estoque_id]" id="eItem_da_Venda_id">
                                        <option value="">Selecione: </option>
                                        <?php foreach (ItemDoEstoque::model()->findAll(array("condition" => "saldo > 0")) as $item) { ?>
                                            <option value="<?php echo $item->id ?>"><?php echo $item->produto->descricao ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="control-label">
                                        Quantidade <span class="symbol required"></span>
                                    </label>
                                    <input required name="Item_da_Venda[quantidade]" id="eQuantidade" class="form-control" type="number">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="control-label">
                                        Preço de Tabela 
                                    </label>
                                    <input required name="Item_da_Venda[precoTabela]" disabled="true" id="ePreco_tabela" class="form-control moeda" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">
                                        % Desconto <span class="symbol required"></span>
                                    </label>
                                    <input required name="Item_da_Venda[desconto]" id="eDesconto" class="form-control" type="number">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="control-label">
                                        Preço de Venda
                                    </label>
                                    <input required name="Item_da_Venda[precoVenda]" value="0" disabled="true" id="ePrecoVenda" class="form-control moeda" type="number">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <!--            <label class="checkbox-inline" style="float:left">
               <input id="checkbox_continuar" type="checkbox" value="">
               Continuar Cadastrando
               </label>-->
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
            <button type="submit" id="btn-edit-item" class="btn btn-blue">Salvar</button>
            <div class="row">
            </div>
            <br>
        </div>
    </form>
</div>
<style>

</style>