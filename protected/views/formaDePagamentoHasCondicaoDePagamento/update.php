<?php
/* @var $this FormaDePagamentoHasCondicaoDePagamentoController */
/* @var $model FormaDePagamentoHasCondicaoDePagamento */

$this->breadcrumbs=array(
	'Forma De Pagamento Has Condicao De Pagamentos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List FormaDePagamentoHasCondicaoDePagamento', 'url'=>array('index')),
	array('label'=>'Create FormaDePagamentoHasCondicaoDePagamento', 'url'=>array('create')),
	array('label'=>'View FormaDePagamentoHasCondicaoDePagamento', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage FormaDePagamentoHasCondicaoDePagamento', 'url'=>array('admin')),
);
?>

<h1>Update FormaDePagamentoHasCondicaoDePagamento <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>