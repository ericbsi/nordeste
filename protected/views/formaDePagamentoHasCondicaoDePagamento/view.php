<?php
/* @var $this FormaDePagamentoHasCondicaoDePagamentoController */
/* @var $model FormaDePagamentoHasCondicaoDePagamento */

$this->breadcrumbs=array(
	'Forma De Pagamento Has Condicao De Pagamentos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List FormaDePagamentoHasCondicaoDePagamento', 'url'=>array('index')),
	array('label'=>'Create FormaDePagamentoHasCondicaoDePagamento', 'url'=>array('create')),
	array('label'=>'Update FormaDePagamentoHasCondicaoDePagamento', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete FormaDePagamentoHasCondicaoDePagamento', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage FormaDePagamentoHasCondicaoDePagamento', 'url'=>array('admin')),
);
?>

<h1>View FormaDePagamentoHasCondicaoDePagamento #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'Forma_de_Pagamento_id',
		'Condicao_de_Pagamento_id',
		'qtdParcelasDe',
		'qtdParcelasAte',
		'carencia',
		'intervalo',
	),
)); ?>
