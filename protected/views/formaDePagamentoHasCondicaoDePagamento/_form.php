<?php
/* @var $this FormaDePagamentoHasCondicaoDePagamentoController */
/* @var $model FormaDePagamentoHasCondicaoDePagamento */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'forma-de-pagamento-has-condicao-de-pagamento-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'Forma_de_Pagamento_id'); ?>
		<?php echo $form->textField($model,'Forma_de_Pagamento_id'); ?>
		<?php echo $form->error($model,'Forma_de_Pagamento_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Condicao_de_Pagamento_id'); ?>
		<?php echo $form->textField($model,'Condicao_de_Pagamento_id'); ?>
		<?php echo $form->error($model,'Condicao_de_Pagamento_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'qtdParcelasDe'); ?>
		<?php echo $form->textField($model,'qtdParcelasDe'); ?>
		<?php echo $form->error($model,'qtdParcelasDe'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'qtdParcelasAte'); ?>
		<?php echo $form->textField($model,'qtdParcelasAte'); ?>
		<?php echo $form->error($model,'qtdParcelasAte'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'carencia'); ?>
		<?php echo $form->textField($model,'carencia'); ?>
		<?php echo $form->error($model,'carencia'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'intervalo'); ?>
		<?php echo $form->textField($model,'intervalo'); ?>
		<?php echo $form->error($model,'intervalo'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->