<?php
/* @var $this FormaDePagamentoHasCondicaoDePagamentoController */
/* @var $data FormaDePagamentoHasCondicaoDePagamento */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Forma_de_Pagamento_id')); ?>:</b>
	<?php echo CHtml::encode($data->Forma_de_Pagamento_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Condicao_de_Pagamento_id')); ?>:</b>
	<?php echo CHtml::encode($data->Condicao_de_Pagamento_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qtdParcelasDe')); ?>:</b>
	<?php echo CHtml::encode($data->qtdParcelasDe); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qtdParcelasAte')); ?>:</b>
	<?php echo CHtml::encode($data->qtdParcelasAte); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('carencia')); ?>:</b>
	<?php echo CHtml::encode($data->carencia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('intervalo')); ?>:</b>
	<?php echo CHtml::encode($data->intervalo); ?>
	<br />


</div>