<?php
/* @var $this FormaDePagamentoHasCondicaoDePagamentoController */
/* @var $model FormaDePagamentoHasCondicaoDePagamento */

$this->breadcrumbs=array(
	'Forma De Pagamento Has Condicao De Pagamentos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FormaDePagamentoHasCondicaoDePagamento', 'url'=>array('index')),
	array('label'=>'Manage FormaDePagamentoHasCondicaoDePagamento', 'url'=>array('admin')),
);
?>

<h1>Create FormaDePagamentoHasCondicaoDePagamento</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>