<?php
/* @var $this FormaDePagamentoHasCondicaoDePagamentoController */
/* @var $model FormaDePagamentoHasCondicaoDePagamento */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Forma_de_Pagamento_id'); ?>
		<?php echo $form->textField($model,'Forma_de_Pagamento_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Condicao_de_Pagamento_id'); ?>
		<?php echo $form->textField($model,'Condicao_de_Pagamento_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'qtdParcelasDe'); ?>
		<?php echo $form->textField($model,'qtdParcelasDe'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'qtdParcelasAte'); ?>
		<?php echo $form->textField($model,'qtdParcelasAte'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'carencia'); ?>
		<?php echo $form->textField($model,'carencia'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'intervalo'); ?>
		<?php echo $form->textField($model,'intervalo'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->