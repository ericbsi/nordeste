<?php
/* @var $this FormaDePagamentoHasCondicaoDePagamentoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Forma De Pagamento Has Condicao De Pagamentos',
);

$this->menu=array(
	array('label'=>'Create FormaDePagamentoHasCondicaoDePagamento', 'url'=>array('create')),
	array('label'=>'Manage FormaDePagamentoHasCondicaoDePagamento', 'url'=>array('admin')),
);
?>

<h1>Forma De Pagamento Has Condicao De Pagamentos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
