<?php
/* @var $this FormaDePagamentoHasCondicaoDePagamentoController */
/* @var $model FormaDePagamentoHasCondicaoDePagamento */

$this->breadcrumbs=array(
	'Forma De Pagamento Has Condicao De Pagamentos'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List FormaDePagamentoHasCondicaoDePagamento', 'url'=>array('index')),
	array('label'=>'Create FormaDePagamentoHasCondicaoDePagamento', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#forma-de-pagamento-has-condicao-de-pagamento-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Forma De Pagamento Has Condicao De Pagamentos</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'forma-de-pagamento-has-condicao-de-pagamento-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'Forma_de_Pagamento_id',
		'Condicao_de_Pagamento_id',
		'qtdParcelasDe',
		'qtdParcelasAte',
		'carencia',
		/*
		'intervalo',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
