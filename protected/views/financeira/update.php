<?php
/* @var $this FinanceiraController */
/* @var $model Financeira */

$this->breadcrumbs=array(
	'Financeiras'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Financeira', 'url'=>array('index')),
	array('label'=>'Create Financeira', 'url'=>array('create')),
	array('label'=>'View Financeira', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Financeira', 'url'=>array('admin')),
);
?>

<h1>Update Financeira <?php echo $model->id; ?></h1>

<?php
	$this->renderPartial('_form', 
		array(
			'model'=>$model,
			'endereco'=>$endereco,
			'telefone'=>$telefone,
			'email'=>$email,
		)
	); 
?>