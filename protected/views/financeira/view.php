<?php
/* @var $this FinanceiraController */
/* @var $model Financeira */

$this->breadcrumbs=array(
	'Financeiras'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Financeira', 'url'=>array('index')),
	array('label'=>'Create Financeira', 'url'=>array('create')),
	array('label'=>'Update Financeira', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Financeira', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Financeira', 'url'=>array('admin')),
);
?>

<h1>View Financeira #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'cnpj',
		'nome',
		'habilitado',
		'data_cadastro',
		'Contato_id',
	),
)); ?>
