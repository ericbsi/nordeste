<?php
/* @var $this FinanceiraController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Financeiras',
);

$this->menu=array(
	array('label'=>'Create Financeira', 'url'=>array('create')),
	array('label'=>'Manage Financeira', 'url'=>array('admin')),
);
?>

<h1>Financeiras</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
