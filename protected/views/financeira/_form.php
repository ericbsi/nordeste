<div class="row">
   <div class="col-md-12">
      <!-- start: FORM VALIDATION 1 PANEL -->
         <div class="row">
                  <div class="col-sm-12">                   
                     <!-- start: PAGE TITLE & BREADCRUMB -->
                     <ol class="breadcrumb">
                        <li>
                           <i class="clip-pencil"></i>
                           <a href="#">
                              Financeira
                           </a>
                        </li>
                        <li class="active">
                              Criar
                        </li>
                        
                     </ol>

                     <div class="page-header">
                        <h1>Nova Financeira</h1>
                     </div>
                     <!-- end: PAGE TITLE & BREADCRUMB -->
                  </div>
         </div>
      <div class="panel panel-default">
         <div class="panel-heading">
            <i class="fa fa-external-link-square"></i>            
            <div class="panel-tools">
               <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
               </a>
            </div>
         </div>
         <div class="panel-body">            
            <hr>
              <?php $form=$this->beginWidget('CActiveForm', array(
               'id'=>'financeiraForm',       
               'enableAjaxValidation'=>false,
               'htmlOptions'=>array(
                  'role'=>'form'
               )
              )); ?>

               <div class="row">
               
                  <div class="col-md-6">
                     <div class="form-group">
                      <?php echo $form->labelEx($model,'nome', array('class'=>'control-label')); ?> <span class="symbol required"></span>
                      <?php echo $form->textField($model,'nome', array('class'=>'form-control', 'required'=>true,'placeholder'=>'Nome')); ?>
                     </div>
                     <div class="form-group">
                        <?php echo $form->labelEx($model,'cnpj', array('class'=>'control-label')); ?> <span class="symbol required"></span>
                        <?php echo $form->textField($model,'cnpj', array('class'=>'form-control', 'placeholder'=>'CNPJ')); ?>
                     </div>
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <?php echo $form->labelEx($telefone,'numero', array('class'=>'control-label')); ?> <span class="symbol required"></span>
                              <?php echo $form->textField($telefone,'numero',array('class'=>'form-control', 'placeholder'=>'Telefone')); ?>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <?php echo $form->labelEx($telefone,'Tipo_Telefone_id', array('class'=>'control-label')); ?> <span class="symbol required"></span>
                              <?php echo $form->dropDownList($telefone,'Tipo_Telefone_id', 
                                 CHtml::listData(TipoTelefone::model()->findAll(),
                                 'id','tipo' ), array('class'=>'form-control search-select')); 
                              ?>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <?php echo $form->labelEx($email,'email', array('class'=>'control-label')); ?> <span class="symbol required"></span>
                              <?php echo $form->textField($email,'email',array('class'=>'form-control', 'placeholder'=>'Email')); ?>
                           </div>
                        </div>
                     </div>
                  </div>
               
                  <div class="col-md-6">
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <?php echo $form->labelEx($endereco,'logradouro', array('class'=>'control-label')); ?> <span class="symbol required"></span>
                              <?php echo $form->textField($endereco,'logradouro',array('class'=>'form-control', 'placeholder'=>'Logradouro')); ?>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <?php echo $form->labelEx($endereco,'numero', array('class'=>'control-label')); ?> <span class="symbol required"></span>
                              <?php echo $form->textField($endereco,'numero',array('class'=>'form-control', 'placeholder'=>'Número')); ?>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <?php echo $form->labelEx($endereco,'complemento', array('class'=>'control-label')); ?> <span class="symbol required"></span>
                              <?php echo $form->textField($endereco,'complemento',array('class'=>'form-control', 'placeholder'=>'Complemento')); ?>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <?php echo $form->labelEx($endereco,'cidade', array('class'=>'control-label')); ?> <span class="symbol required"></span>
                              <?php echo $form->textField($endereco,'cidade',array('class'=>'form-control', 'placeholder'=>'Cidade')); ?>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <?php echo $form->labelEx($endereco,'bairro', array('class'=>'control-label')); ?> <span class="symbol required"></span>
                              <?php echo $form->textField($endereco,'bairro',array('class'=>'form-control', 'placeholder'=>'Bairro')); ?>
                           </div>                           
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <?php echo $form->labelEx($endereco,'cep', array('class'=>'control-label')); ?> <span class="symbol required"></span>
                              <?php echo $form->textField($endereco,'cep',array('class'=>'form-control', 'placeholder'=>'CEP')); ?>
                           </div>                           
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <?php echo $form->labelEx($endereco,'uf', array('class'=>'control-label')); ?> <span class="symbol required"></span>
                              <?php echo $form->dropDownList($endereco,'uf', 
                                 CHtml::listData(Estados::model()->findAll(),
                                 'sigla','nome' ), array('class'=>'form-control search-select')); 
                               ?>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <?php echo $form->labelEx($endereco,'Tipo_Endereco_id', array('class'=>'control-label')); ?> <span class="symbol required"></span>
                              <?php echo $form->dropDownList($endereco,'Tipo_Endereco_id', 
                                 CHtml::listData(TipoEndereco::model()->findAll(),
                                 'id','tipo' ), array('class'=>'form-control search-select')); 
                               ?>
                           </div>
                        </div>
                     </div>
                  </div>

               </div>
               
               <div class="row">
                  <div class="col-md-12">
                     <div>
                        <span class="symbol required"></span> CAMPOS OBRIGATÓRIOS
                        <hr>
                     </div>
                  </div>
               </div>

               <div class="row">
                  <div class="col-md-8">
                  </div>
                  <div class="col-md-4">
                     <input type="submit" value="Cadastrar" class="btn btn-teal btn-block">
                  </div>
               </div>

         <?php $this->endWidget(); ?>
         </div>
      </div>
      <!-- end: FORM VALIDATION 1 PANEL -->
   </div>
</div>

