<!-- start: PAGE HEADER -->
<div class="row">
   <div class="col-sm-12">
      <!-- start: PAGE TITLE & BREADCRUMB -->
      <ol class="breadcrumb">
         <li><a href="#">Financeiras</a></li>
         <li class="active">Admin</li>
      </ol>
      <div class="page-header">
         <h1>Financeiras</h1>
      </div>
      <!-- end: PAGE TITLE & BREADCRUMB -->
   </div>
</div>
<!-- end: PAGE HEADER -->
<div class="row">
   <div class="col-md-12">
      <div class="panel panel-default">
         <div class="panel-heading">
            <i class="fa fa-external-link-square"></i>
            <div class="panel-tools">
               <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
               </a>
               <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
               <i class="fa fa-wrench"></i>
               </a>
               <a class="btn btn-xs btn-link panel-refresh" href="#">
               <i class="fa fa-refresh"></i>
               </a>
               <a class="btn btn-xs btn-link panel-expand" href="#">
               <i class="fa fa-resize-full"></i>
               </a>
               <a class="btn btn-xs btn-link panel-close" href="#">
               <i class="fa fa-times"></i>
               </a>
            </div>
         </div>
		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'financeira-grid',
			'dataProvider'=>$model->search(),
			'filter'=>$model,
			'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width dataTable',
         'summaryText'=>'Exibindo {start} de {end} resultados',
			'columns'=>array(		
				'nome',		
				'cnpj',
				'data_cadastro_br',
				array(
					'class'=>'CButtonColumn',
					'template'=>'{update}'
				),
			),
		)); ?>
      </div>
   </div>
</div>