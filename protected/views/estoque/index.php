
<!--Modal para troca de primeira senha-->
<?php if( Yii::app()->session['usuario']->primeira_senha){ ?>

<div id="static" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
   <div class="modal-body">
      <p>
       O sistema detectou que você ainda não alterou sua senha após o cadastro (ou após solicitar uma nova senha). 
       Por medidas de segurança, solicitamos que você altere a sua senha.
      </p>
   </div>
   <div class="modal-footer">
      <form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/usuario/changePassword">
         <button type="submit"  class="btn btn-primary">
            Mudar senha
         </button>
      </form>
   </div>      
</div>

<?php } ?>



<div class="row">
   <div class="col-sm-12">
      
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
               Estoque
            </a>
         </li>
         <li class="active">
            Home
         </li>
      </ol>
      <div class="page-header">
         <h1>
          Estoques
         </h1>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-sm-12">
      <p>
         <a id="btn_modal_form_new_estoque" data-toggle="modal" href="#modal_form_new_estoque" class="btn btn-success">Cadastrar Estoque <i class="fa fa-plus"></i></a>
      </p>
   </div>
</div>
<br>
<br>
<br>

<!--Filtros-->
<div class="row">
   <div class="col-sm-12">
      <div class="col-md-12">
         <div class="row">
            <div class="col-md-3">
               <div class="form-group">
                  <label class="control-label">Filial</label>
                  <br>
                  <select required="required" multiple="multiple" id="filiais_select" class="form-control multipleselect select_redraw_table" name="Filiais[]">
                     <?php foreach ( Yii::app()->session['usuario']->getEmpresa()->listFiliais() as $f ) { ?>
                        <option value="<?php echo $f->id ?>"><?php echo $f->getConcat() ?></option>
                     <?php } ?>
                  </select>                  
               </div>
            </div>

            <div class="col-md-3">
               <div class="form-group">
                  <label class="control-label">Local</label>
                  <br>
                  <select required="required" multiple="multiple" id="select_locais" class="form-control multipleselect select_redraw_table" name="Locais[]">
                     <?php foreach ( Local::model()->findAll() as $local ) { ?>
                        <option value="<?php echo $local->id ?>"><?php echo $local->nome ?></option>
                     <?php } ?>
                  </select>
               </div>
            </div>
            <div class="col-md-3">
               <div class="form-group">
                  <label class="control-label">Vende?</label>
                  <br>
                  <select required="required" multiple="multiple" id="select_vende" class="form-control multipleselect select_redraw_table" name="Vende[]">
                     <?php foreach ( TTable::model()->findAll() as $vende ) { ?>
                        <option value="<?php echo $vende->id ?>"><?php echo $vende->flag ?></option>
                     <?php } ?>
                  </select>                  
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<!--Grid-->
<div style="margin-bottom:30px" class="row">
   <div class="col-sm-12">
      <form>
         <input type="hidden" value="<?php echo Yii::app()->session['usuario']->getEmpresa()->id ?>" id="empresaId">
      </form>

      <table id="grid_estoques" class="table table-striped table-bordered table-hover table-full-width dataTable">
         <thead>
            <tr>
               <th class="no-orderable">Filial</th>
               <th class="no-orderable">Local</th>
               <th class="no-orderable">Vende</th>
            </tr>
         </thead>
      </table>

   </div>
</div>

<!--Modal-->
<div id="modal_form_new_estoque" class="modal fade" tabindex="-1" data-width="600" style="display: none;">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
      &times;
      </button>
      <h4 class="modal-title">Cadastrar Estoque</h4>
   </div>
   <form id="form-add-estoque">
      <div class="modal-body">
         <div class="row">
            <div class="row-centralize">
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-11">
                        <div class="form-group">
                           <label class="control-label">
                              Filial <span class="symbol required"></span>
                           </label>
                           <select name="Estoque[Filial_id]" id="Estoque_Filial_id" required="required" class="form-control search-select select2">
                              <option value="">Selecione:</option>
                              <?php foreach( Yii::app()->session['usuario']->getEmpresa()->listFiliais() as $filial ){ ?>
                                 <option value="<?php echo $filial->id ?>"><?php echo $filial->getConcat() ?></option>
                              <?php } ?>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-5">
                        <div class="form-group">
                           <label class="control-label">
                           Local <span class="symbol required"></span>
                           </label>
                           <?php echo CHtml::dropDownList('Estoque[Local_id]','Estoque[Local_id]',
                              CHtml::listData(Local::model()->findAll(),
                              'id','nome' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:','required'=>true)); 
                           ?>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Venda <span class="symbol required"></span></label>
                           <?php echo CHtml::dropDownList('Estoque[vende]','Estoque[vende]',
                              CHtml::listData(TTable::model()->findAll(),
                              'id','flag' ), array('class'=>'form-control search-select select2','prompt'=>'Selecione:','required'=>true)); 
                           ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal-footer">
         <label class="checkbox-inline" style="float:left">
            <input id="checkbox_continuar" type="checkbox" value="">
            Continuar Cadastrando
         </label>
         <div class="panel"></div>
         <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
         <button type="submit" class="btn btn-blue">Salvar</button>
         <div class="row">
         </div>
         <br>
         <div id="cadastro_msg_return" class="alert" style="text-align:left">
         </div>
      </div>
   </form>
</div>

<style type="text/css">
   #grid_estoques_length, #grid_estoques_filter{display: none}
   .panel{
      background: transparent!important;
      border:none!important;
   }
   .col-md-3{
      padding-left: 0!important;
   }
</style>