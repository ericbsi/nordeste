<?php  ?>
<div class="row">
   <div class="col-sm-12">
      
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
               Financeiro
            </a>
         </li>
         <li class="active">
            Retorno - Cnab
         </li>
      </ol>
      <div class="page-header">
         <h1>
         	Importar retorno
         </h1>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-sm-12" style="">
      <div class="row">
         <form id="upload-retorno-form" action="/financeiro/importarRetorno/" method="post" enctype="multipart/form-data">
            <div class="col-md-3">
               <div class="form-group">
                  <label class="control-label">
                     Banco <span class="symbol required"></span>
                  </label>
                  <select required="required" id="" class="form-control multipleselect" name="Banco">
                     <?php foreach ( Banco::model()->findAll('id = 5 OR id = 7 or id = 3') as $b ) { ?>
                        <option value="<?php echo $b->id ?>"><?php echo strtoupper($b->nome) ?></option>
                     <?php } ?>
                  </select>  
               </div>
            </div>
            <div class="col-md-3">
               <div class="form-group">
                  <label class="control-label">Arquivo:</label>
                  <input data-icon="false" class="control-label filestyle" required="required" name="FileInput" id="FileInput" type="file" />
                  <input name="forcar_importacao" type="hidden" value="1" id="forcar_importacao">
                  <input name="forcar_importacao_obs" type="hidden" value="1" id="forcar_importacao_obs">
               </div>
            </div>
            <div class="col-md-3">
               <div class="form-group">
                  <label class="control-label">&nbsp;</label>
                  <button class="btn btn-blue next-step btn-block">Importar</button>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>

<div class="row">
	<div class="col-sm-9">
		<div id="progressbox">
            <div id="progressbar"></div >
            <div id="statustxt">0%</div>
      </div>
      <div id="cadastro_anexo_msg_return" class="alert" style="text-align:left; display:none"></div>
	</div>
</div>

<div class="row" id="import-resume-error" style="display:none">
   <div class="col-sm-12" id="resume-error-inner">
   </div>
</div>

<div class="row" id="import-resume" style="display:none">
   <div class="col-sm-12">
      
      <!--Importados com sucesso-->
      <div class="alert alert-success">
         <i class="fa fa-check-circle"></i>
         <strong> <span id="regSuc2"></span> Títulos Importados com sucesso</strong> 
      </div>

      <!--Liquidados-->
      <div class="alert alert-info">
         <i class="fa fa-info-circle"></i>
         <strong> <span id="liq2"></span> Foram liquidados</strong>
      </div>

      <!--Com erros-->
      <div class="alert alert-danger">
         <i class="fa fa-times-circle"></i>
         <strong> <span id="err2"></span> Titulos com erros.</strong>
      </div>
   </div>
</div>

<div data-width="700" id="static" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
         <div class="modal-header">
            <h5>
               Atenção <span id="fileName"></span>
            </h5>
         </div>
         <div class="modal-body">
            
            <div id="modal-mens"></div>
            <div class="form-group">
               
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">Fechar</button>
         </div>
</div>

<style type="text/css">
	/* progress bar style */
   #progressbox {
      border: 1px solid #4cae4c;
      padding: 1px; 
      position:relative;
      width:315px;
      border-radius: 3px;
      margin: 10px;
      margin-left: 0;
      display:none;
      text-align:left;
   }
   #progressbar {
      height:20px;
      border-radius: 3px;
      background-color: #4cae4c;
      width:1%;
   }
   #statustxt {
      top:3px;
      left:50%;
      position:absolute;
      display:inline-block;
      color: #FFF;
      font-size: 11px;
      font-family: arial
   }
</style>