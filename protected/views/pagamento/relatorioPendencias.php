<div class="row">
   <div class="col-sm-12">
      
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
               Administrativo
            </a>
         </li>
         <li class="active">
            Relatório de  Pendências
         </li>
      </ol>
      <div class="page-header">
         <h1 style="font-size:26px">
         	Relatório de Pendências 
         </h1>         
      </div>
   </div>
</div>
<div class="row">
   <div class="col-sm-12">
      <form id="form-propostas-pgto-aprovado" action="/pagamento/efetivarPendencias/" method="POST">
         
          <input type='hidden' id='current_page' />  
          <input type='hidden' id='show_per_page' />  
         <input type="hidden" id="totalAtualGridFinan" name="totalAtualGridFinan" value="0">
         <input type="hidden" id="totalAtualGrid" name="totalAtualGrid" value="0">

         <input type="hidden" value="0" id="item_0_id" name="itens_pgto_aprovados[]" class="itens_pgto_aprovados_arr">
         <input type="hidden" value="0" id="item_0_obs" name="itens_pgto_aprovados_obs[]" class="itens_pgto_aprovados_obs_arr">

         <input type="hidden" id="idPropostaHdn">
         <input type="hidden" id="codigoPropostaHdn">
         <input type="hidden" id="cpfClienteHdn">
         <input type="hidden" id="nomeClienteHdn">
         <input type="hidden" id="valorFinanciadoHdn">
         <input type="hidden" id="valorRepasseHdn">
         <input type="hidden" id="valorFinanciadoDoubleHdn">
         <input type="hidden" id="valorRepasseDoubleHdn">

         <input type="hidden" value="" name="action" id="btn_action">

         <input type="hidden" value="<?php if(isset($processoId)){echo $processoId;}else{echo"0";} ?>" name="processoId">

         <!--<input name="destinatario" type="hidden" value="<?php //echo $parceiro->id?>">-->

      </form>
   </div>
</div>

<div class="row">
   <div class="col-sm-12">
      <div class="row">
         <div class="col-md-3">
            <div class="form-group">
               <form id="form-codigo-proposta">                           
                  <!--<input required="required" onkeydown="bloquear_ctrl_j()" id="codigoProposta" type="text" placeholder="Código da Proposta" class="form-control input-lg required">-->
                  <!--<input id="parceiroId" type="hidden" value="<?php //echo $parceiro->id?>">-->
               </form>
            </div>
         </div>
      </div>
   </div>
</div>



<div class="row">
    <label style="font-size: 26px; margin-left: 28px; font-family: 'Raleway', sans-serif; color: #666666;" >Filtros</label>
   <div class="col-sm-12">
       <div class="row">
           <select name="Parceiros" style="width: 250px!important; margin-left: 30px; margin-bottom: 10px; font-family: 'Raleway', sans-serif;" id="filiais" class="select2 search-select multipleselect"> <br/> <br/>
               <option value="">Parceiros</option> 
                <?php foreach (NucleoFiliais::model()->findAll('habilitado') AS $F ) {?>
                    <option value="<?php echo $F->id ?>"><?php echo $F->nome ?></option>
                <?php }?>
            </select>
       </div>
       <input type="date" style="margin-left: -30px !important; font-family: 'Raleway', sans-serif;" placeholder="De" name="data_de" id="data_de">
       <input  type="date" style="margin-right:  20px; font-family: 'Raleway', sans-serif;" placeholder="Até" name="data_ate" id="data_ate">
        <?php echo CHtml::dropDownList('StatusPre', 'StatusPre', CHtml::listData(Financeira::model()->findAll('habilitado'), 'id', 'nome'), array('class' => 'multiselect dropdown-toggle btn btn-default', 'prompt' => 'Financeira', 'id' => 'selectSFilter')); ?>
       <div class="col-md-3">
                <div class="form-group">
                    <input style="width:250px; border-radius: 0px!important; font-family: 'Raleway', sans-serif;" type="text" class="form-control" id="codigo" placeholder="Código "> <br/> <br/>               
                    
                    
                </div>
            </div>                                        
       <button style="margin-left: 20px; width: 100px;" type="button" id="filtrar" class="btn btn-blue next-step ">Filtrar</button> <br/> <br/>
         <table id="grid_relatorio_pendencias" class="table table-striped table-bordered table-hover table-full-width dataTable">
            <thead>
               <tr>
                  <th width="160">Cód</th>
                  <th width="100">CPF</th>
                  <th width="250">Nome</th>
                  <th width="90">Valor Financiado</th>
                  <th width="90">Valor Repasse</th>
                  <th width="280">Observação</th>
                  <th width="100">Espera</th>
               </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
               <th></th>
               <th></th>
               <th></th>
               <th</th>
               <th</th>
               <th></th>
            </tfoot>
      </table>
   </div>
</div>

<div class="row" style="margin-bottom:30px;">
   <div class="col-sm-12">
       
   </div>
</div>

<div data-width="560" id="modal_form_obs" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
   <div class="modal-header">
      <h4 class="modal-title">Registrar Observação</h4>
   </div>
   <form id="form-add-obs">
      <div class="modal-body">
         <div class="row">
            <div class="row-centralize">
               <div class="col-md-8">
                  <div class="row">
                     <div class="col-md-8">
                        <div class="form-group">
                           <label class="control-label">Observação:</label>
                           <textarea id="obs_content" name="Mensagem[conteudo]" required="required" class="form-control" style="width:518px!important;height:116px;"></textarea>
                           <input type="hidden" value="<?php echo Yii::app()->session['usuario']->id ?>" name="Usuario_id" id="ipt_hdn_user_id">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal-footer">
         <div style="background:transparent;border:none;" class="panel"></div>
         <!--<button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>-->
         <button type="submit" class="btn btn-blue btn-send">Enviar</button>
      </div>
   </form>
</div>

<div data-width="560" id="modal_confirm" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
   <div class="modal-header">
      <h4 class="modal-title">Deseja finalizar?</h4>
   </div>
   <div class="modal-body">
         <div class="row">
            <div class="row-centralize">
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label id="qtd_itens" class="control-label"></label>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
   </div>
   <div class="modal-footer">
      <!--<input name="btn" value="Sim" type="submit" id="btn-concluir-aut-pgto" class="btn btn-blue btn-send" />-->
     
     
   </div>
</div>

<style type="text/css">

   #grid_pgtos_aprovados_length{
      display: none;
   }

   #grid_pgtos_aprovados_filter{
      margin-right: 0!important;
      margin-bottom: 10px;
      display: none;
   }
   
   #grid_pgtos_aprovados_filter input{
      background-color: #FFFFFF;
      border: 1px solid #D5D5D5;
      border-radius: 0 0 0 0 !important;
      color: #858585;
      font-family: inherit;
      font-size: 14px;
      line-height: 1.2;
      padding: 5px 4px;
      transition-duration: 0.1s;
      box-shadow: none;
      height: 45px!important;
   }

   #grid_pgtos_aprovados_filter input:focus{
      border-color: rgba(82, 168, 236, 0.8);
      box-shadow: 0 0 8px rgba(82, 168, 236, 0.6);
      outline: 0 none;
   }
   
   
   
   #grid_relatorio_pendencias_filter {
       display: none;
   }

   tfoot th {
      color: #3D9400;
   }
   
</style>