<div class="row">
   <div class="col-sm-12">
      
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
               Administrativo
            </a>
         </li>
         <li class="active">
            Autorizar Pagamento
         </li>
      </ol>
      <div class="page-header">
         <h1 style="font-size:26px">
         	Autorização de Pagamento | Credshow Operadora de Crédito S/A : 
         </h1>         
      </div>
   </div>
</div>
<div class="row">
   <div class="col-sm-12">
      <div class="row">
         <form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/pagamento/parceiro/">
            <div class="col-md-5">
               <div class="form-group">
                  <label class="control-label">Parceiros: <span class="symbol required"></span></label>
                  <select required="required" id="parceiro_select" class="form-control multipleselect" name="Parceiro">
                     <?php foreach ( Yii::app()->session['usuario']->getEmpresa()->listFiliais() as $f ) { ?>
                        <option value="<?php echo $f->id ?>"><?php echo strtoupper( $f->getConcat() ) ?></option>
                     <?php } ?>
                  </select>  
               </div>
            </div>
            <div class="col-md-3">
               <div class="form-group">
                  <label class="control-label">&nbsp;</label>
                  <button class="btn btn-blue btn-block">Iniciar </button>                  
               </div>
            </div>
         </form>
      </div>
   </div>

</div>
<style type="text/css">
   .dropdown-menu {
      max-height: 250px;
      overflow-y: auto;
      overflow-x: hidden;
   }
</style>