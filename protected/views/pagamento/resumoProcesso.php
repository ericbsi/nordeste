<?php 
   $util                         = new Util;
   $processo->bordero->Status    = 1;
   $processo->bordero->update();
?>

<div class="row">
   <div class="col-sm-12">
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
               Administrativo
            </a>
         </li>
         <li class="active">
            Recebimento de Documentação
         </li>
      </ol>
      <div class="page-header">
         <h1 style="font-size:26px">
         	Recebimento de Documentação / Parceiro : <?php echo strtoupper( $processo->Destinatario->getConcat() ) ?>
         </h1>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-sm-12">
   		<div class="page-header">
         <h1 style="font-size:26px">
         	Detalhes do Processo:
         </h1>
         <table id="grid_detalhes_processo" class="table table-striped table-bordered table-hover table-full-width dataTable">
            <thead>
               <tr>
                  <th width="120">Cód</th>
                  <th width="140">Data Criação</th>
                  <th>Operador</th>
                  <th width="140">Propostas Aprovadas</th>
                  <th width="140">Propostas Pendentes</th>
                  <th>Total Aprovado</th>
                  <th>Total Pendente</th>
                  <th>Repasse Aprovado</th>
                  <th>Repasse Pendente</th>
                  <th width="40"></th>
               </tr>
            </thead>
            <tbody>
            	<tr>
            		<td><?php echo $processo->codigo ?></td>
            		<td><?php echo $util->bd_date_to_view( substr($processo->dataCriacao, 0, 10) ) . ' às ' .substr($processo->dataCriacao, 11)  ?></td>
            		<td><?php echo strtoupper($processo->CriadoPor->nome_utilizador) ?></td>
                  <td><?php echo $processo->totalItensBordero(); ?></td>
            		<td><?php echo $processo->totalItensPendentes(); ?></td>
            		<td><?php echo "R$ " . number_format($processo->totalFinanciadoAprovado(),2,',','.') ?></td>
            		<td><?php echo "R$ " . number_format($processo->totalFinanciadoPendente(),2,',','.') ?></td>
            		<td><?php echo "R$ " . number_format($processo->totalRepasseAprovado(),2,',','.') ?></td>
            		<td><?php echo "R$ " . number_format($processo->totalRepassePendente(),2,',','.') ?></td>
            		<td>
                     <form method="POST" target="_blank" action="/printer/recebimentoDeContrato/">
                        <input type="hidden" value="<?php echo $processo->codigo ?>" name="codigo">
                        <button class="submit"><i class="fa fa-print"></i></button>
                     </form>
            		</td>
            	</tr>
            </tbody>
         </table>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-sm-12">
      <a class="btn btn-primary" href="/pagamento/">
         Novo Recebimento de Contratos <i class="clip-stack"></i>
      </a>
      <a class="btn btn-primary" href="/recebimentoDeDocumentacao/historico/">
         Histórico <i class="clip-stack"></i>
      </a>
   </div>
</div>