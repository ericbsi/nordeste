<div class="row">
   <div class="col-sm-12">
      
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
               Administrativo
            </a>
         </li>
         <li class="active">
            Autorizar Pagamento
         </li>
      </ol>
      <div class="page-header">
         <h1 style="font-size:26px">
         	Autorização de Pagamento / Parceiro : <?php echo strtoupper($parceiro->getConcat()); ?>
         </h1>
         <h1 style="font-size:26px">
            Operador : <?php echo strtoupper( Yii::app()->session['usuario']->nome_utilizador ); ?>
         </h1>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-sm-12">
      <div class="row">
            <div class="col-md-6">
               <form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/pagamento/iniciarBordero/">
                  <div class="form-group">
                     <label class="control-label">&nbsp;</label>
                     <button type="submit" class="btn btn-green btn-lg btn-block">Autorizar Pagamento <i class="fa fa-arrow-circle-right"></i></button>
                     <input type="hidden" value="<?php echo $parceiro->id ?>" name="parceiroId" >
                  </div>
               </form>
            </div>
            <div class="col-md-6">
               <form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/pagamento/registrarPendencias/">
                  <div class="form-group">
                     <label class="control-label">&nbsp;</label>
                     <button type="submit" class="btn btn-warning btn-lg btn-block">Registrar Pendências <i class="fa fa-exclamation-triangle"></i></button>
                     <input type="hidden" value="<?php echo $parceiro->id ?>" name="parceiroId" >
                  </div>
               </form>
            </div>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-sm-12">
      <a href="/pagamento/" class="btn btn-bricky btn-lg">Cancelar</a>
   </div>
</div>
<style type="text/css">
   .dropdown-menu {
      max-height: 250px;
      overflow-y: auto;
      overflow-x: hidden;
   }
</style>