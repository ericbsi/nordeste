<?php
   $util = new Util;
?>
<div class="row">
   <div class="col-sm-12">
      
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
               Administrativo
            </a>
         </li>
         <li class="active">
            Registrar Pendências
         </li>
      </ol>
      <div class="page-header">
         <h1 style="font-size:26px">
         	Registro de Pendências | Parceiro : <?php echo strtoupper($parceiro->getConcat()); ?>
         </h1>         
      </div>
   </div>
</div>
<div class="row">
   <div class="col-sm-12">
      <form id="form-propostas-pgto-aprovado" action="/pagamento/efetivarPendencias/" method="POST">
         
         <input type="hidden" id="totalAtualGridFinan" name="totalAtualGridFinan" value="<?php echo $processo->registroDePendencias->totalFinanciado(); ?>">
         <input type="hidden" id="totalAtualGrid" name="totalAtualGrid" value="<?php echo $processo->registroDePendencias->totalRepasse(); ?>">

         <input type="hidden" value="0" id="item_0_id" name="itens_pgto_aprovados[]" class="itens_pgto_aprovados_arr">
         <input type="hidden" value="0" id="item_0_obs" name="itens_pgto_aprovados_obs[]" class="itens_pgto_aprovados_obs_arr">
         
         <?php foreach ( $processo->registroDePendencias->itemPendentes as $item ) { ?>
            <input type="hidden" value="<?php echo $item->itemPendente->id ?>" id="item_<?php echo $item->itemPendente->id ?>_id" name="itens_pgto_aprovados[]" class="itens_pgto_aprovados_arr">
         <?php } ?>

         <?php foreach ( $processo->registroDePendencias->itemPendentes as $item ) { ?>
            <input type="hidden" value="<?php echo $item->observacao ?>" id="item_<?php echo $item->itemPendente->id ?>_obs" name="itens_pgto_aprovados_obs[]" class="itens_pgto_aprovados_obs_arr">
         <?php } ?>

         <input type="hidden" id="idPropostaHdn">
         <input type="hidden" id="codigoPropostaHdn">
         <input type="hidden" id="cpfClienteHdn">
         <input type="hidden" id="nomeClienteHdn">
         <input type="hidden" id="valorFinanciadoHdn">
         <input type="hidden" id="valorRepasseHdn">
         <input type="hidden" id="valorFinanciadoDoubleHdn">
         <input type="hidden" id="valorRepasseDoubleHdn">

         <input type="hidden" value="" name="action" id="btn_action">

         <input id="idDoProcesso" type="hidden" value="<?php if(isset($processo)){echo $processo->id;}else{echo"0";} ?>" name="processoId">

         <input name="destinatario" type="hidden" value="<?php echo $parceiro->id?>">

      </form>
   </div>
</div>

<div class="row">
   <div class="col-sm-12">
      <div class="row">
         <div class="col-md-3">
            <div class="form-group">
               <form id="form-codigo-proposta">                           
                  <input required="required" onkeydown="bloquear_ctrl_j()" id="codigoProposta" type="text" placeholder="Código da Proposta" class="form-control input-lg required">
                  <input id="parceiroId" type="hidden" value="<?php echo $parceiro->id?>">
               </form>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-sm-12">
         <table id="grid_pgtos_aprovados" class="table table-striped table-bordered table-hover table-full-width dataTable">
            <thead>
               <tr>
                  <th width="160">Cód</th>
                  <th width="100">CPF</th>
                  <th width="250">Nome</th>
                  <th width="90">Valor Financiado</th>
                  <th width="90">Valor Repasse</th>
                  <th width="280">Observação</th>
                  <th width="20"></th>
               </tr>
            </thead>
            <tbody>
               <?php foreach ( $processo->registroDePendencias->itemPendentes as $item ) { ?>
                  <tr>
                     <td><?php echo $item->itemPendente->codigo ?></td>
                     <td><?php echo $item->itemPendente->analiseDeCredito->cliente->pessoa->getCPF()->numero ?></td>
                     <td><?php echo strtoupper($item->itemPendente->analiseDeCredito->cliente->pessoa->nome) ?></td>
                     <td><?php echo "R$ ". number_format( ( $item->itemPendente->valor - $item->itemPendente->valor_entrada ), 2, ',', '.' ) ?></td>
                     <td><?php echo "R$ ". number_format( ( $util->arredondar( $item->itemPendente->valorRepasse() ) ), 2, ',', '.' ) ?></td>
                     <td><?php echo $item->observacao; ?></td>
                     <td>
                        <a data-item-id="<?php echo $item->itemPendente->id ?>" data-id-prop="item_<?php echo $item->itemPendente->id ?>_id" data-valor-fin="<?php echo ($item->itemPendente->valor-$item->itemPendente->valor_entrada) ?>" data-valor-repasse="<?php echo $util->arredondar( $item->itemPendente->valorRepasse() ) ?>" href="#" class="btn btn-xs btn-bricky btn-remover-item"><i class="fa fa-times fa fa-white"></i></a>
                     </td>
                  </tr>
               <?php } ?>
            </tbody>
            <tfoot>
               <th></th>
               <th></th>
               <th></th>
               <th id="count_finan"><?php echo "R$ " . number_format($processo->registroDePendencias->totalFinanciado(), 2, ',', '.'); ?></th>
               <th id="count_repasse"><?php echo "R$ " . number_format($processo->registroDePendencias->totalRepasse(), 2, ',', '.'); ?></th>
               <th></th>
               <th></th>
            </tfoot>
      </table>
   </div>
</div>

<div class="row" style="margin-bottom:30px;">
   <div class="col-sm-12">
      <button id="btn-concluir-grid" type="button" class="btn btn-success btn-lg">Concluir</button>      
      <!--<a href="/pagamento/" class="btn btn-bricky btn-lg">Cancelar</a>-->
   </div>
</div>

<div data-width="560" id="modal_form_obs" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
   <div class="modal-header">
      <h4 class="modal-title">Registrar Observação</h4>
   </div>
   <form id="form-add-obs">
      <div class="modal-body">
         <div class="row">
            <div class="row-centralize">
               <div class="col-md-8">
                  <div class="row">
                     <div class="col-md-8">
                        <div class="form-group">
                           <label class="control-label">Observação:</label>
                           <textarea id="obs_content" name="Mensagem[conteudo]" required="required" class="form-control" style="width:518px!important;height:116px;"></textarea>
                           <input type="hidden" value="<?php echo Yii::app()->session['usuario']->id ?>" name="Usuario_id" id="ipt_hdn_user_id">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal-footer">
         <div style="background:transparent;border:none;" class="panel"></div>
         <!--<button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>-->
         <button type="submit" class="btn btn-blue btn-send">Enviar</button>
      </div>
   </form>
</div>

<div data-width="560" id="modal_confirm" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
   <div class="modal-header">
      <h4 class="modal-title">Deseja inserir itens para pagamento?</h4>
   </div>
   <div class="modal-body">
         <div class="row">
            <div class="row-centralize">
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label id="qtd_itens" class="control-label"></label>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
   </div>
   <div class="modal-footer">
      <input name="btn" value="Sim" type="submit" id="btn-concluir-aut-pgto" class="btn btn-blue btn-send" />
      <input name="btn" value="Não" type="submit" id="btn-concluir" class="btn btn-blue btn-send" />
      <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
   </div>
</div>

<style type="text/css">
   #grid_pgtos_aprovados_length{
      display: none;
   }

   #grid_pgtos_aprovados_filter{
      margin-right: 0!important;
      margin-bottom: 10px;
   }
   
   #grid_pgtos_aprovados_filter input{
      background-color: #FFFFFF;
      border: 1px solid #D5D5D5;
      border-radius: 0 0 0 0 !important;
      color: #858585;
      font-family: inherit;
      font-size: 14px;
      line-height: 1.2;
      padding: 5px 4px;
      transition-duration: 0.1s;
      box-shadow: none;
      height: 45px!important;
   }

   #grid_pgtos_aprovados_filter input:focus{
      border-color: rgba(82, 168, 236, 0.8);
      box-shadow: 0 0 8px rgba(82, 168, 236, 0.6);
      outline: 0 none;
   }
   
   tfoot th {
      color: #3D9400;
   }
   
</style>