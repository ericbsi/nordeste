<div class="row">
    <div class="col-sm-12">

        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="#">
                    Administrativo
                </a>
            </li>
            <li class="active">
                Autorizar Pagamento
            </li>
        </ol>
        <div class="page-header">
            <h1 style="font-size:26px">
                Autorização de Pagamento | Borderô |
            </h1>         
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <form id="form-propostas-pgto-aprovado" action="/pagamento/efetivarBordero/" method="POST">
            <input type="hidden" id="totalAtualGrid" name="totalAtualGrid" value="0">
            <input type="hidden" id="totalAtualGridFinan" name="totalAtualGridFinan" value="0">
            <input type="hidden" value="0" id="item_0_id" name="itens_pgto_aprovados[]" class="itens_pgto_aprovados_arr">
            <input type="hidden" value="0" id="imagem_0_id" name="imagens_pgto_aprovados[]" class="imagens_pgto_aprovados_arr">
            <!--<input name="destinatario" type="hidden" value="<?php //echo $parceiro->id ?>">-->
            <input type="hidden" value="" name="action" id="btn_action">
            <input type="hidden" value="<?php if (isset($processoId)) {
    echo $processoId;
} else {
    echo "0";
} ?>" name="processoId">
        </form>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <form id="form-codigo-proposta">
                        <div class="row">
                            <div class="col-md-3">
                                <input required="required" onkeydown="bloquear_ctrl_j()" id="codigoProposta" type="text" placeholder="Código da Proposta" class="form-control input-lg required">
                            </div>
                            <div style="display: none;" class="col-md-3">
                                <select id="porImagem" style="text-align: center;" class="form-control input-lg">
                                    <option value="0">Pagamento por imagem?</option>
                                    <option value="1">SIM</option>
                                    <option value="0">NÃO</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <table id="grid_pgtos_aprovados" class="table table-striped table-bordered table-hover table-full-width dataTable">
            <thead>
                <tr>
                    <th width="100">Cód</th>
                    <th width="100">CPF</th>
                    <th width="190">PARCEIRO</th>
                    <th width="300">Nome</th>
                    <th width="100">Valor Financiado</th>
                    <th width="100">Valor Repasse</th>
                    <th width="20"></th>
                    <th width="20">Por Imagem</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th id="count_finan">0,00</th>
            <th id="count_repasse">0,00</th>
            </tfoot>
        </table>
    </div>
</div>
<div class="row" style="margin-bottom:30px;">
    <div class="col-sm-12">
        <button style="display:none" id="btn-concluir-grid" type="button" class="btn btn-success btn-lg">Concluir</button>
<?php if (!isset($processoId)) { ?>
            <a href="/pagamento/" class="btn btn-bricky btn-lg">Cancelar</a>
<?php } ?>
    </div>
</div>

<div data-width="560" id="modal_confirm" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
    <div class="modal-header">
        <h4 class="modal-title">Deseja finalizar?</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="row-centralize">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label id="qtd_itens" class="control-label"></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
       <!--<input name="btn" value="Sim" type="submit" id="btn-concluir-reg-pendencia" class="btn btn-blue btn-send" />-->
        <input name="btn" value="Sim" type="submit" id="btn-concluir" class="btn btn-blue btn-send" />
        <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
    </div>
</div>

<!--Retirar pendencia-->
<div data-width="560" id="modal_retirar_pendencia" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
    <div class="modal-header">
        <h4 class="modal-title">Deseja retirar da pendência?</h4>
    </div>
    <div class="modal-footer">
       <!--<input name="btn" value="Sim" type="submit" id="btn-concluir-reg-pendencia" class="btn btn-blue btn-send" />-->
        <input name="btn" value="Sim" type="button" id="btn-retirar-pendencia" class="btn btn-blue btn-send" />
        <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
    </div>
</div>
<style type="text/css">
    #grid_pgtos_aprovados_length{
        display: none;
    }

    #grid_pgtos_aprovados_filter{
        margin-right: 0!important;
        margin-bottom: 10px;
    }

    #grid_pgtos_aprovados_filter input{
        background-color: #FFFFFF;
        border: 1px solid #D5D5D5;
        border-radius: 0 0 0 0 !important;
        color: #858585;
        font-family: inherit;
        font-size: 14px;
        line-height: 1.2;
        padding: 5px 4px;
        transition-duration: 0.1s;
        box-shadow: none;
        height: 45px!important;
    }

    #grid_pgtos_aprovados_filter input:focus{
        border-color: rgba(82, 168, 236, 0.8);
        box-shadow: 0 0 8px rgba(82, 168, 236, 0.6);
        outline: 0 none;
    }

    tfoot th {
        color: #3D9400;
    }
</style>