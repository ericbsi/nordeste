<?php  ?>
<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="#">
                    Recebimento de documentos
                </a>
            </li>
            <li class="active">
                Receber Documentos
            </li>
        </ol>
    </div>
</div>

<div id="conteudo">
    
    <br/>
    <div class="row">
        <div class="col-sm-12" >
            <input type="checkbox" id="myCheckbox" name="myCheckbox" data-on-text="Receber" data-off-text="Malotes" data-on-color="warning" data-off-color="primary" data-size="mini" />
        </div>
    </div>

<br>
   
    <div id="divReceber" class="row" style="border:1px solid #f0ad4e; overflow-x: hidden;">

        <div class="page-header" style="text-align: center;">
                <h3 style="color: #444444">
                Recebimento de Documentos
            </h3>
        </div>
           
        <div class="row">

            <div class="col-sm-3">

                <button style="display: none" class='btn btn-green filtroLecca' value="0"  id="btnFiltroProLecca">
                    <i class='clip-spinner-4'> </i> Processadas
                </button>

            </div>

            <div class="col-sm-2 alert alert-success">
                <b id="qtdMarcadas">0</b> selecionadas
            </div>

            <div class="col-sm-2 alert alert-info">
                R$ <b id="valorFinanciado">0</b> financiado
            </div>

            <div class="col-sm-2 alert alert-warning">
                R$ <b id="valorRepasse">0</b> de repasse
            </div>

           

        </div>

        <br>

        <div class="row">
            <div class="col-sm-12">
                <table id="grid_recebimento" class="table table-striped table-bordered table-hover table-full-width dataTable">
                    <thead>
                        <tr>
                            <th>
                                Filtro
                            </th>
                            
                            
                            <th class="searchable">
                                <input class="input_filterLecca form-control" onkeydown="bloquear_ctrl_j()" id="codigo_filterLecca" style="width:100%" type="text" placeholder="Pesquisar" />
                            </th>
                            <th class="searchable">
                                <input class="input_filterLecca form-control" id="filial_filterLecca" style="width:100%" type="text" placeholder="Pesquisar" />
                            </th>
                            <th class="searchable">
                                <input class="input_filterLecca form-control" id="nome_filterLecca"   style="width:100%" type="text" placeholder="Pesquisar" />
                            </th>
                            <th class="searchable">
                                <input class="input_filterLecca form-control" id="cpf_filterLecca"    style="width:100%" type="text" placeholder="Pesquisar" />
                            </th>
                            <th class="searchable">
                                <input class="input_filterLecca form-control" id="data_filterLecca"   style="width:100%" type="date" placeholder="Pesquisar" />
                            </th>
                             
                            <th  colspan="2">

                            </th>
                            <th id="thBotao">
                                <button class="btn btn-blue" disabled="disabled" id="botaoGerarLecca">
                                    <i style="color: greenyellow" class="fa fa-save"></i> 
                                </button>
                            </th>
                            
                        </tr>
                        <tr>
                            <th width="3px">       
                                
                            </th>
                            
                            <th style="width:100px;">
                                Cód Proposta
                            </th>
                            <th>
                                Filial
                            </th>
                            <th>
                                Cliente
                            </th>
                            <th>
                                CPF
                            </th>
                            <th style="width:100px;">
                                Data Proposta
                            </th>
                            <th style="width:80px;">
                                R$ Financiado
                            </th>
                            <th>
                                R$ repasse
                            </th>
                            <th>
                                Pg?
                            </th>
                           
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
    
    <br>
    
    <div id="divMalotes" class="row" style="border:1px solid #f0ad4e; overflow-x: hidden;">

        <div class="page-header" style="text-align: center;">
            <h3 style="color: #444444">
                Documentação Recebida
            </h3>
        </div>

<!--        <div class="row">

            <div class="col-sm-2 alert alert-success">
                <b id="qtdMarcadas">0</b> selecionadas
            </div>

            <div class="col-sm-2 alert alert-info">
                R$ <b id="valorFinanciado">0</b> financiado
            </div>

            <div class="col-sm-2 alert alert-warning">
                R$ <b id="valorRepasse">0</b> de repasse
            </div>

           

        </div>-->

        <br>

        <div class="row">
            <div class="col-sm-12">
                <table id="grid_recebido" class="table table-striped table-bordered table-hover table-full-width dataTable">
                    <thead>
                        <tr>
                            
                            <th>
                                Filtro
                            </th>
                            
                            <th class="searchable">
                                <input class="input_filtroMalote form-control" id="codigo_filtroMalote"     style="width:100%" type="text" placeholder="Pesquisar" />
                            </th>
                            
                            <th class="searchable">
                                <input class="input_filtroMalote form-control" id="filial_filtroMalote"     style="width:100%" type="text" placeholder="Pesquisar" />
                            </th>
                            
                            <th class="searchable">
                                <input class="input_filtroMalote form-control" id="usuario_filtroMalote"    style="width:100%" type="text" placeholder="Pesquisar" />
                            </th>
                            
                            <th class="searchable">
                                <input class="input_filtroMalote form-control" id="data_filtroMalote"       style="width:100%" type="date" placeholder="Pesquisar" />
                            </th>
                            
                            <th style="width:30px">
                                 
                                <button class="btn btn-blue" disabled="disabled" id="botaoGerarMalote">
                                    Gerar Malotes   
                                </button>

                            </th>
                            
                        </tr>
                        <tr>
                            <th width="3px">       
                                
                            </th>
                            
                            <th>
                                Código
                            </th>
                            
                            <th>
                                Filial
                            </th>
                            
                            <th>
                                Usuário
                            </th>
                            
                            <th>
                                Data 
                            </th>
                            
                            <th>
                            </th>
                            
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>

        <br>
        
    </div>
    
</div>
    
<br>
<br>

<style type="text/css">
    #grid_nfe_length, #grid_nfe_filter, #grid_nfe_lecca_length, #grid_nfe_lecca_filter
    {
        display: none;
    }
    td.details-control 
    {
        background: url('../../images/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.details td.details-control 
    {
        background: url('../../images/details_close.png') no-repeat center center;
    }
    .thChek
    {
        text-align: center!important;
    }
    
    /* progress bar style */
    #progressbox, #progressbox_contrato {
        border: 1px solid #4cae4c;
        padding: 1px; 
        position:relative;
        width:315px;
        border-radius: 3px;
        margin: 10px;
        margin-left: 0;
        display:none;
        text-align:left;
    }
    #progressbar, #progressbar_contrato {
        height:20px;
        border-radius: 3px;
        background-color: #4cae4c;
        width:1%;
    }
    #statustxt, #statustxt_contrato {
        top:3px;
        left:50%;
        position:absolute;
        display:inline-block;
        color: #FFF;
        font-size: 11px;
        font-family: arial
    }
    
    #grid_recebimento_filter {
        display: none;
    }
    
    #grid_recebimento_length, #grid_recebido_filter {
        display: none;
    }

</style>

