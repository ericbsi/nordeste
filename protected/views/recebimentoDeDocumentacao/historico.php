<div class="row">
   <div class="col-sm-12">
      
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
               Administrativo
            </a>
         </li>
         <li class="active">
            Recebimentos de Contratos
         </li>
      </ol>
      <div class="page-header">
         <h1>
         	Recebimento de Contratos - Credshow Operadora de Crédito S/A
         </h1>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-sm-12">
      <a class="btn btn-primary" href="/pagamento/iniciarBordero/">
         Registrar Recebimento de Contratos <i class="clip-stack"></i>
      </a>
   </div>
</div>
<br>
<br>
<div class="row">
   <div class="col-sm-9">
      <div class="row">
         <form id="form-filter">
            <div class="col-md-3" style="width:15%">
               <div class="form-group">
                  <label class="control-label">
                     Data de <span class="symbol required" aria-required="true"></span>
                  </label>                  
                  <input style="width:120px" type="text" name="data_de" class="form-control date-picker" data-date-viewmode="years" data-date-format="dd/mm/yyyy" id="data_de">
               </div>
            </div>
            <div class="col-md-3" style="width:15%">
               <div class="form-group">
                  <label class="control-label">Data até:</label>
                  <input style="width:120px" type="text" name="data_ate" class="form-control date-picker" data-date-viewmode="years" data-date-format="dd/mm/yyyy" id="data_ate">
               </div>
            </div>
            <!--<div class="col-md-5">
               <div class="form-group">
                  <label class="control-label">
                     Parceiros <span class="symbol required"></span>
                  </label>
                  <select required="required" multiple="multiple" id="filiais_select" class="form-control multipleselect" name="Filiais[]">
                     <?php foreach (Filial::model()->listarParaCobranca() as $ap) { ?>
                        <option value="<?php echo $ap->id ?>"><?php echo strtoupper($ap->getConcat()) ?></option>
                     <?php } ?>
                  </select>  
               </div>
            </div>-->
            <div class="col-md-3" style="width:15%">
               <div class="form-group">
                  <label class="control-label">&nbsp;</label>
                  <button id="btn-filter" class="btn btn-blue next-step btn-block">Filtrar</button>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-md-4">
               <div class="form-group">
                     <label class="control-label">
                        Grupo de Filiais <span class="symbol required"></span>
                     </label>

                     <select required="required" multiple="multiple" id="grupos_select" class="form-control search-select multipleselect" >
                        <?php foreach (GrupoFiliais::model()->listarParaProducao() as $gf) { ?>
                           <option value="<?php echo $gf->id ?>"><?php echo strtoupper($gf->nome_fantasia) ?></option>
                        <?php } ?>
                     </select>  
                  </div>
            </div>
         <div class="col-md-4">
            <div class="form-group">
               <label class="control-label">
                  Nucleo Filiais <span class="symbol required"></span>
               </label>

               <select required="required" multiple="multiple" id="nucleos_select" class="form-control search-select multipleselect" >
                  <?php foreach (NucleoFiliais::model()->listarParaProducao() as $nf) { ?>
                     <option value="<?php echo $nf->id ?>"><?php echo strtoupper($nf->nome) ?></option>
                  <?php } ?>
               </select>  

            </div>
         </div>
   <div class="col-md-4">
      <div class="form-group">
         <label class="control-label">
            Parceiros <span class="symbol required"></span>
         </label>
         <select required="required" multiple="multiple" id="filiais_select" class="form-control multipleselect" name="Filiais[]">
            <?php foreach (Filial::model()->listarParaCobranca() as $ap) { ?>
               <option value="<?php echo $ap->id ?>"><?php echo strtoupper($ap->getConcat()) ?></option>
            <?php } ?>
         </select>  
      </div>
   </div>
</div>
<div class="row" style="margin-bottom:40px">
   <div class="col-sm-12">
      <table id="grid_historico_recebimentos_contratos" class="table table-striped table-bordered table-hover table-full-width dataTable">
         <thead>
               <tr>
                  <th class="no-orderable" width="120">Cód</th>
                  <th class="no-orderable" width="140">Data Criação</th>
                  <th class="no-orderable">Operador</th>
                  <th class="no-orderable">Parceiro</th>
                  <th class="no-orderable" width="70">Aprovadas</th>
                  <th class="no-orderable" width="70">Pendentes</th>
                  <th class="no-orderable" width="100">R$ Aprovado</th>
                  <th class="no-orderable" width="100">R$ Pendente</th>
                  <th class="no-orderable">Repasse Aprovado</th>
                  <th class="no-orderable">Repasse Pendente</th>
                  <th width="40"></th>
               </tr>
         </thead>
         <tbody>
         </tbody>
         <tfoot>
            <tr>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th id="th-total"></th>
               <th></th>
               <th></th>
            </tr>
         </tfoot>
      </table>

   </div>
</div>

<style type="text/css">
   .dropdown-menu {
      max-height: 250px;
      overflow-y: auto;
      overflow-x: hidden;
   }
   #grid_historico_recebimentos_contratos_length,
   #grid_historico_recebimentos_contratos_filter{
      display: none;
   }
</style>