<?php  ?>
<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="#">
                    Listagem de Malotes
                </a>
            </li>
            <li class="active">
                Listar Malotes
            </li>
        </ol>
    </div>
</div>


<br>

<div id="conteudo">

   
    <div s id="divMalotes" class="row" style="border:1px solid #f0ad4e; overflow-x: hidden;">

        <div class="page-header" style="text-align: center;">
                <h3 style="color: #444444">
                Listagem de Malotes
            </h3>
        </div>
        
        <br>

        <div class="row">
            <div class="col-sm-12">
                <table id="grid_malotes" class="table table-striped table-bordered table-hover table-full-width dataTable">
                    <thead>
                        
                        <tr>
                            
                            
                            <th style="width:100px;">
                                Código
                            </th>
                            <th>
                                Data
                            </th>
                            <th>
                                Usuário
                            </th>
                            <th style="width: 130px;"></th>
                           
                        </tr>
                        
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
    
</div>

<br>
<br>

<style type="text/css">
    #grid_nfe_length, #grid_nfe_filter, #grid_nfe_lecca_length, #grid_nfe_lecca_filter
    {
        display: none;
    }
    td.details-control 
    {
        background: url('../../images/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.details td.details-control 
    {
        background: url('../../images/details_close.png') no-repeat center center;
    }
    .thChek
    {
        text-align: center!important;
    }
    
    /* progress bar style */
    #progressbox, #progressbox_contrato {
        border: 1px solid #4cae4c;
        padding: 1px; 
        position:relative;
        width:315px;
        border-radius: 3px;
        margin: 10px;
        margin-left: 0;
        display:none;
        text-align:left;
    }
    #progressbar, #progressbar_contrato {
        height:20px;
        border-radius: 3px;
        background-color: #4cae4c;
        width:1%;
    }
    #statustxt, #statustxt_contrato {
        top:3px;
        left:50%;
        position:absolute;
        display:inline-block;
        color: #FFF;
        font-size: 11px;
        font-family: arial
    }
    
    #grid_recebimento_filter, #grid_malotes_length, #grid_malotes_info, #grid_malotes_paginate {
        display: none;
    }
    
    #grid_recebimento_length, #grid_malotes_filter {
        display: none;
    }

</style>

