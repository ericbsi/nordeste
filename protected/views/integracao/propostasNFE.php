<?php  ?>
<div class="row">
   <div class="col-sm-12">
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
            Propostas NFe
            </a>
         </li>
         <li class="active">
            Por Filial
         </li>
      </ol>
      <div class="page-header">
         <h1 style="font-size:24px;">
            Propostas NFe
         </h1>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-sm-12">
      <table id="grid_propostas" class="table table-striped table-bordered table-hover table-full-width dataTable">
         <thead>
            <tr>
               <th></th>
               <th class="searchable">
                  <input class="input_filter form-control" id="codigo_filter" style="width:100%" type="text" placeholder="Pesquisar" />
               </th>
               <th class="searchable">
                  <input class="input_filter form-control" id="nome_filter" style="width:100%" type="text" placeholder="Pesquisar" />
               </th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
               <th></th>
            </tr>
            <tr>
               <th style="width:80px;"></th>
               <th style="width:100px;">Cód</th>
               <th>Cliente</th>
               <th style="width:150px;">Crediarista</th>
               <th style="width:100px;">R$ Inicial</th>
               <th style="width:80px;">Entrada</th>
               <th style="width:80px;">Seguro</th>
               <th style="width:80px;">R$ Financiado</th>
               <th style="width:80px;">Parcelamento</th>
               <th style="width:80px;">R$ final</th>
               <th style="width:75px;">Status</th>
            </tr>
         </thead>
         <tbody>
         </tbody>
      </table>
   </div>
</div>

<div id="modalAnexo" class="modal fade" tabindex="-1" data-width="560" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            &times;
        </button>
        <h4 class="modal-title">Anexar XML -Nf</h4>
    </div>
    <form action="/integracao/anexarXmlNf/" method="POST" enctype="multipart/form-data" id="form-add-xml-nf">
        <div class="modal-body">
            <div class="row">
                <div class="row-centralize">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">XML:</label>
                                    <input data-icon="false" class="control-label filestyle" required="required" name="anexoXML" id="anexoXML" type="file" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
            <button type="submit" class="btn btn-blue btn-send" id="btn-salvar-soli">Salvar</button>
            <input name="proposta-id" id="proposta-id" type="hidden" />
        </div>
    </form>
</div>

<div class="row">
   <div class="col-sm-12">
      <div id="chart_div"></div>
   </div>
</div>

<style type="text/css">
   #grid_propostas_length, #grid_propostas_filter{
   display: none;
   }
   td.details-control {
   background: url('../../images/details_open.png') no-repeat center center;
   cursor: pointer;
   }
   tr.details td.details-control {
   background: url('../../images/details_close.png') no-repeat center center;
   }

   /* progress bar style */
   #progressbox, #progressbox_contrato {
      border: 1px solid #4cae4c;
      padding: 1px; 
      position:relative;
      width:315px;
      border-radius: 3px;
      margin: 10px;
      margin-left: 0;
      display:none;
      text-align:left;
   }
   #progressbar, #progressbar_contrato {
      height:20px;
      border-radius: 3px;
      background-color: #4cae4c;
      width:1%;
   }
   #statustxt, #statustxt_contrato {
      top:3px;
      left:50%;
      position:absolute;
      display:inline-block;
      color: #FFF;
      font-size: 11px;
      font-family: arial
   }

</style>