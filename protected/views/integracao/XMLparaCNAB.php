<?php  ?>
<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="#">
                    Exportar XML para CNAB
                </a>
            </li>
            <li class="active">
                XML para CNAB
            </li>
        </ol>
    </div>
</div>

<br>

<div class="row" style="text-align: center">
    <div class="col-sm-12" >
        <input type="checkbox" id="myCheckbox2" name="myCheckbox2" data-on-text="Gerar" data-off-text="Processar" data-on-color="error" data-off-color="success" data-size="mini" />
    </div>
</div>

<br>

<div id="conteudo">

    <div class="row">
        <div class="col-sm-12" >
            <input type="checkbox" id="myCheckbox" name="myCheckbox" data-on-text="Lecca" data-off-text="FIDC" data-on-color="warning" data-off-color="primary" data-size="mini" />
        </div>
    </div>

    <br>

    <div id="divFIDC" class="row" style="border:1px solid #337ab7">

        <div class="page-header" style="text-align: center;">
            <h3 style="color: #337ab7">
                FIDC
            </h3>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <button id="botaoForceXML" class="btn btn-orange" style="width : 100%">
                    <i class="clip-data"></i> <b>Buscar NFe's</b>
                </button>
            </div>
        </div>

        <br>

        <div class="row">

            <div class="col-sm-8">

                <button class='btn btn-green filtroNFe' value="0"  id="btnFiltroProNFe">
                    <i class='clip-spinner-4'></i> Processadas
                </button>

                <button class="btn filtroNFe active" value="1" id="btnFiltroComNFe">
                    <i class="clip-checkbox-unchecked-2" ></i> Processar NFe's
                </button>

                <button class="btn filtroNFe active" value="1" id="btnFiltroSemNFe">
                    <i class="clip-file-xml" ></i> Sem NFe
                </button>

                <button class="btn filtroNFe active" value="1" id="btnFiltroSemChv">
                    <i class="clip-file-xml" ></i> Sem Chave
                </button>

            </div>

            <div class="col-sm-4" style="text-align: right; right: 1%; padding: 0px!important; margin: 0px!important;">

                <button class="btn filtroNFe active" value="1" id="btnFiltroComJuros">
                    <i class="clip-plus-circle-2" ></i>  <i class="fa fa-dollar" ></i> Com Juros
                </button>

                <button class="btn filtroNFe active" value="1" id="btnFiltroSemJuros">
                    <i class="clip-minus-circle-2" ></i>  <i class="fa fa-dollar" ></i> Sem Juros
                </button>

            </div>

        </div>

        <br>

        <div class="row">
            <div class="col-sm-12">
                <table id="grid_nfe" class="table table-striped table-bordered table-hover table-full-width dataTable">
                    <thead>
                        <tr>
                            <th>
                                Todas
                            </th>
                            <th>
                            </th>
                            <th class="searchable">
                                <input class="input_filter form-control" id="codigo_filter" style="width:100%" type="text" placeholder="Pesquisar" />
                            </th>
                            <th class="searchable">
                                <input class="input_filter form-control" id="filial_filter" style="width:100%" type="text" placeholder="Pesquisar" />
                            </th>
                            <th class="searchable">
                                <input class="input_filter form-control" id="nome_filter"   style="width:100%" type="text" placeholder="Pesquisar" />
                            </th>
                            <th class="searchable">
                                <input class="input_filter form-control" id="cpf_filter"   style="width:100%" type="text" placeholder="Pesquisar" />
                            </th>
                            <th class="searchable">
                                <input class="input_filter form-control" id="data_filter"   style="width:100%" type="date" placeholder="Pesquisar" 
                                       value="<?php echo date("Y-m-d"); ?>"
                                />
                            </th>
                            <th class="searchable">
                            </th>
                            <th class="searchable">
                                <input class="input_filter form-control" id="nfserie_filter"   style="width:100%" type="text" placeholder="Pesquisar" />
                            </th>
                            <th colspan="4">

                            </th>
                            <th id="thBotao">

                                <button class="btn btn-blue" disabled="disabled" id="botaoGerar">
                                    Gerar CNAB
                                </button>

                            </th>
                        </tr>
                        <tr>
                            <th width="3px">       
                                <button class="btn btn-blue" id="btnSelecionaTudo" value="0" style="display : none">
                                    <i class="clip-checkbox-unchecked-2 checkNF" id="iSelecionarTudo"></i>
                                </button>
                            </th>
                            <th style="width:100px;">
                                Modalidade
                            </th>
                            <th style="width:100px;">
                                Cód Proposta
                            </th>
                            <th style="width:100px;">
                                Filial
                            </th>
                            <th>
                                Cliente
                            </th>
                            <th>
                                CPF
                            </th>
                            <th style="width:100px;">
                                Data Proposta
                            </th>
                            <th style="width:100px;">
                                Data NFe
                            </th>
                            <th style="width:150px;">
                                Série - Nota
                            </th>
                            <th style="width:100px;">
                                Chave Eletrônica
                            </th>
                            <th style="width:80px;">
                                R$ Financiado
                            </th>
                            <th style="width:80px;">
                                Parcelamento
                            </th>
                            <th style="width:80px;">
                                R$ final
                            </th>
                            <th style="width:80px;">
                                R$ repasse
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        
                        <tr>
                            <th colspan="10">
                            </th>
                            <th>
                                R$ <b id="valorTotalFinanciado">0,00</b>
                            </th>
                            <th>
                                
                            </th>
                            <th>
                                R$ <b id="valorTotalFinal">0,00</b>
                            </th>
                            <th>
                                R$ <b id="valorTotalRepasse">0,00</b>
                            </th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>

    </div>


    <div id="divLecca" class="row" style="border:1px solid #f0ad4e; overflow-x: hidden">

        <div class="page-header" style="text-align: center;">
            <h3 style="color: #f0ad4e">
                Lecca
            </h3>
        </div>

        <div class="row">

            <div class="col-sm-3">

                <button style="display: none" class='btn btn-green filtroLecca' value="0"  id="btnFiltroProLecca">
                    <i class='clip-spinner-4'> </i> Processadas
                </button>

            </div>

            <div class="col-sm-2 alert alert-success">
                <b id="qtdMarcadas">0</b> selecionadas
            </div>

            <div class="col-sm-2 alert alert-info">
                R$ <b id="valorFinanciado">0</b> financiado
            </div>

            <div class="col-sm-2 alert alert-warning">
                R$ <b id="valorRepasse">0</b> de repasse
            </div>

            <div class="col-sm-3" style="text-align: right; right: 1%; padding: 0px!important; margin: 0px!important;">

                <button class="btn filtroLecca active" value="1" id="btnFiltroComJurosLecca">
                    <i class="clip-plus-circle-2" ></i>  <i class="fa fa-dollar" ></i> Com Juros
                </button>

                <button class="btn filtroLecca active" value="1" id="btnFiltroSemJurosLecca">
                    <i class="clip-minus-circle-2" ></i>  <i class="fa fa-dollar" ></i> Sem Juros
                </button>

            </div>

        </div>

        <br>

        <div class="row">
            <div class="col-sm-12">
                <table id="grid_nfe_lecca" class="table table-striped table-bordered table-hover table-full-width dataTable">
                    <thead>
                        <tr>
                            <th>
                                Filtro
                            </th>
                            <th>
                            </th>
                            <th class="searchable">
                                <input class="input_filterLecca form-control" id="codigo_filterLecca" style="width:100%" type="text" placeholder="Pesquisar" />
                            </th>
                            <th class="searchable">
                                <input class="input_filterLecca form-control" id="filial_filterLecca" style="width:100%" type="text" placeholder="Pesquisar" />
                            </th>
                            <th class="searchable">
                                <input class="input_filterLecca form-control" id="nome_filterLecca"   style="width:100%" type="text" placeholder="Pesquisar" />
                            </th>
                            <th class="searchable">
                                <input class="input_filterLecca form-control" id="cpf_filterLecca"    style="width:100%" type="text" placeholder="Pesquisar" />
                            </th>
                            <th class="searchable">
                                <input class="input_filterLecca form-control" id="data_filterLecca"   style="width:100%" type="date" placeholder="Pesquisar" />
                            </th>
                            <th colspan="2">

                            </th>
                            <th id="thBotao">

                                <button class="btn btn-blue" disabled="disabled" id="botaoGerarLecca">
                                    Gerar CNAB
                                </button>

                            </th>
                        </tr>
                        <tr>
                            <th width="3px">       
                                <button class="btn btn-blue" id="btnFiltroMarcadasLecca" value="2">
                                    <i class="clip-checkbox-partial" id="iFiltroMarcadas"></i>
                                </button>
                            </th>
                            <th style="width:100px;">
                                Modalidade
                            </th>
                            <th style="width:100px;">
                                Cód Proposta
                            </th>
                            <th>
                                Filial
                            </th>
                            <th>
                                Cliente
                            </th>
                            <th>
                                CPF
                            </th>
                            <th style="width:100px;">
                                Data Proposta
                            </th>
                            <th style="width:110px;">
                                R$ Financiado
                            </th>
                            <th style="width:80px;">
                                Parcelamento
                            </th>
                            <th style="width:110px;">
                                R$ Repasse
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="7">
                            </th>
                            <th>
                                R$ <b id="vlrTotFinPropostas">0,00</b>
                            </th>
                            <th>
                            </th>
                            <th>
                                R$ <b id="vlrTotRepPropostas">0,00</b>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="7">
                            </th>
                            <th>
                                <font color="gray">R$ <b id="vlrFilFinPropostas">0,00</b></font>
                            </th>
                            <th>
                            </th>
                            <th>
                                <font color="gray">R$ <b id="vlrFilRepPropostas">0,00</b></font>
                            </th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>

    </div>
    
</div>

<br>
<br>


<div id="divArquivos" class="row" style="border:1px solid #008000; padding: inherit;" >
    
    <div class="page-header" style="text-align: center;">
        <h3 style="color: #008000">
            Arquivos
        </h3>
    </div>

<!--    <div class="row">

        <div class="col-sm-8">

            <button class='btn btn-green filtroArquivo' value="0"  id="btnFiltroArquivosTrasmitidos">
                <i class='clip-spinner-4'></i> Transmitidas/Borderôs
            </button>

        </div>

        <div class="col-sm-4" style="text-align: right; right: 1%; padding: 0px!important; margin: 0px!important;">

        </div>

    </div>

    <br>-->

    <div class="row">
        <div class="col-sm-12">
            <table id="gridArquivos" class="table table-striped table-bordered table-hover table-full-width dataTable">
                <thead>
                    <tr>
                        <th width="3px" class="no-orderable">
                        </th>
                        <th width="50px">
<!--                            <input class="input_filterArquivo form-control" id="data_filterArquivo"   style="width:100%" type="date" placeholder="Pesquisar" />-->
                        </th>
                        <th width="120px">
                            <!--<select class="select2" id="selectStatusFinanceira">
                                <option value="0">
                                    Ambas
                                </option>
                                <option value="1">
                                    FIDC
                                </option>
                                <option value="2">
                                    FIDC Lecca
                                </option>
                            </select>-->
                        </th>
                        <th width="3px">
                        </th>
                        <th width="50px">
                            <!--<select class="select2" id="selectStatusArquivo">
                                <option value="0">
                                    Ambos
                                </option>
                                <option value="1">
                                    Aguardando
                                </option>
                                <option value="2">
                                    Enviado
                                </option>
                            </select>-->
                        </th>
                        <th width="50px">
                        </th>
                        <th>
                        </th>
                        <th>
                        </th>
                        <th>
                        </th>
                        <th width="3px">
                        </th>
                    </tr>
                    <tr>
                        <th>                            
                        </th>
                        <th>
                            Data Geração
                        </th>
                        <th>
                            
                        </th>
                        <th>
                            
                        </th>
                        <th>
                            Status
                        </th>
                        <th>
                            Qtd
                        </th>
                        <th>
                            Usuário
                        </th>
                        <th>
                            R$ Financiado
                        </th>
                        <th>
                            R$ Repasse
                        </th>
                        <th>
                        </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

    <br>
    
</div>


<style type="text/css">
    #grid_nfe_length, #grid_nfe_filter, #grid_nfe_lecca_filter
    {
        display: none;
    }
    td.details-control 
    {
        background: url('../../images/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.details td.details-control 
    {
        background: url('../../images/details_close.png') no-repeat center center;
    }
    .thChek
    {
        text-align: center!important;
    }
    
    /* progress bar style */
    #progressbox, #progressbox_contrato {
        border: 1px solid #4cae4c;
        padding: 1px; 
        position:relative;
        width:315px;
        border-radius: 3px;
        margin: 10px;
        margin-left: 0;
        display:none;
        text-align:left;
    }
    #progressbar, #progressbar_contrato {
        height:20px;
        border-radius: 3px;
        background-color: #4cae4c;
        width:1%;
    }
    #statustxt, #statustxt_contrato {
        top:3px;
        left:50%;
        position:absolute;
        display:inline-block;
        color: #FFF;
        font-size: 11px;
        font-family: arial
    }

</style>