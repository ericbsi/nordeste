<?php 
	$util = new Util;
?>

<table role="grid" style="margin:0!important;border-bottom:none!important">
	<thead>
		<tr>
			<th  width="917" style="text-align:center">RECEBIMENTO DE CONTRATOS: <?php echo strtoupper( $processo->Destinatario->getConcat() ) .' / '. $processo->Destinatario->cnpj; ?></th>
		</tr>
	</thead>
</table>
<table role="grid" style="margin:0!important;border-bottom:none!important">
	<thead>
		<tr>
			<th  width="482" style="text-align:left">DATA DE PROCESSAMENTO: <?php echo $util->bd_date_to_view( substr($processo->dataCriacao, 0, 10) ) . ' às ' .substr($processo->dataCriacao, 11) ?> </th>
			<th  width="433" style="text-align:left">OPERADOR: <?php echo strtoupper($processo->CriadoPor->nome_utilizador); ?> </th>
		</tr>
	</thead>
</table>
<table role="grid" style="margin:0!important;border-bottom:none!important">
	<thead>
		<tr>
			<th  width="324" style="text-align:left">CÓDIGO DO PROCESSO: <?php echo $processo->codigo ?> </th>
			<th  width="290" style="text-align:left">PROPOSTAS APROVADAS: <?php echo $processo->totalItensBordero() ?> </th>
			<th  width="300" style="text-align:left">PROPOSTAS PENDENTES: <?php echo $processo->totalItensPendentes() ?> </th>
		</tr>
	</thead>
</table>
<table role="grid" style="margin:0!important;">
	<thead>
		<tr>
			<th  width="227" style="text-align:left">VALOR APROVADO: <?php echo "R$ " . number_format($processo->totalFinanciadoAprovado(), 2,',','.') ?> </th>
			<th  width="227" style="text-align:left">VALOR PENDENTE: <?php echo "R$ " . number_format($processo->totalFinanciadoPendente(), 2,',','.') ?> </th>
			<th  width="228" style="text-align:left">REPASSE APROVADO: <?php echo "R$ " . number_format($processo->totalRepasseAprovado(),2,',','.') ?> </th>
			<th  width="230" style="text-align:left">REPASSE PENDENTE: <?php echo "R$ " . number_format($processo->totalRepassePendente(),2,',','.') ?> </th>
		</tr>
	</thead>
</table>

<!--<div class="page-break"></div>-->

<?php if( $processo->bordero != NULL ) { ?>
	

	<table role="grid" style="margin:0 0 0 0!important;border-bottom:none!important">
		<thead>
			<tr>
				<th  width="917" style="text-align:center">ITENS APROVADOS</th>
			</tr>
		</thead>
	</table>

	<table role="grid" style="margin:0!important;">
		<thead>
			<tr>
				<th  width="80" style="text-align:left">DATA:</th>
				<th  width="151" style="text-align:left">CÓDIGO:</th>
				<th  width="70" style="text-align:left">CPF:</th>
				<th  width="200" style="text-align:left">NOME:</th>
				<th  width="139" style="text-align:left">VAL FIN:</th>
				<th  width="60" style="text-align:left">CARÊNCIA:</th>
				<th  width="60" style="text-align:left">PARCELAS:</th>
				<th  width="137" style="text-align:left">VAL REPASSE:</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($processo->bordero->itemDoBorderos as $itemBordero) { ?>
				<tr>
					<td><?php echo $util->bd_date_to_view(substr($itemBordero->proposta->data_cadastro, 0,10)) ?></td>
					<td><?php echo $itemBordero->proposta->codigo ?></td>
					<td><?php echo $itemBordero->proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero ?></td>
					<td><?php echo strtoupper($itemBordero->proposta->analiseDeCredito->cliente->pessoa->nome) ?></td>
					<td><?php echo "R$ " . number_format($itemBordero->proposta->valor - $itemBordero->proposta->valor_entrada, 2, ',', '.') ?></td>
					<td><?php echo $itemBordero->proposta->carencia ?></td>
					<td><?php echo $itemBordero->proposta->qtd_parcelas ?></td>
					<td><?php echo "R$ " . number_format($itemBordero->proposta->valorRepasse(), 2, ',', '.') ?></td>
				</tr>
			<?php } ?>
		</tbody>
		<tfoot>
			<tr>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th>TOTAL: <?php echo "R$ " . number_format($processo->bordero->totalFinanciado(), 2, ',','.'); ?></th>
				<th></th>
				<th></th>
				<th>TOTAL: <?php echo "R$ " . number_format($processo->bordero->totalRepasse(), 2, ',','.'); ?></th>
			</tr>
		</tfoot>
	</table>

<?php } ?>


<?php if( $processo->registroDePendencias != NULL ) { ?>
	
	<table role="grid" style="margin:0 0 0 0!important;border-bottom:none!important">
		<thead>
			<tr>
				<th  width="917" style="text-align:center">ITENS PENDENTES</th>
			</tr>
		</thead>
	</table>

	<table role="grid" style="margin:0!important;">
		<thead>
			<tr>	
				<th  width="60" style="text-align:left">DATA:</th>
				<th  width="100" style="text-align:left">CÓDIGO:</th>
				<th  width="65" style="text-align:left">CPF:</th>
				<th  width="160" style="text-align:left">NOME:</th>
				<th  width="102" style="text-align:left">VAL FIN:</th>
				<th  width="172" style="text-align:left">OBSERVAÇÃO:</th>
				<th  width="40" style="text-align:left">CARÊNCIA:</th>
				<th  width="40" style="text-align:left">PARCELAS:</th>
				<th  width="100" style="text-align:left">VAL REPASSE:</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($processo->registroDePendencias->itemPendentes as $itemPendente) { ?>
				<tr>
					<td><?php echo $util->bd_date_to_view(substr($itemPendente->itemPendente->data_cadastro, 0,10)) ?></td>
					<td><?php echo $itemPendente->itemPendente->codigo ?></td>
					<td><?php echo $itemPendente->itemPendente->analiseDeCredito->cliente->pessoa->getCPF()->numero ?></td>
					<td><?php echo strtoupper($itemPendente->itemPendente->analiseDeCredito->cliente->pessoa->nome) ?></td>
					<td><?php echo "R$ " . number_format($itemPendente->itemPendente->valor - $itemPendente->itemPendente->valor_entrada, 2, ',', '.') ?></td>
					<td><?php echo $itemPendente->observacao; ?></td>
					<td><?php echo $itemPendente->itemPendente->carencia; ?></td>
					<td><?php echo $itemPendente->itemPendente->qtd_parcelas; ?></td>
					<td><?php echo "R$ " . number_format($itemPendente->itemPendente->valorRepasse(), 2, ',', '.') ?></td>
				</tr>
			<?php } ?>
		</tbody>
		<tfoot>
			<tr>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th>TOTAL: <?php echo "R$ " . number_format($processo->registroDePendencias->totalFinanciado(), 2, ',','.'); ?></th>
				<th></th>
				<th></th>
				<th></th>
				<th>TOTAL: <?php echo "R$ " . number_format($processo->registroDePendencias->totalRepasse(), 2, ',','.'); ?></th>
			</tr>
		</tfoot>
	</table>
<?php } ?>

<br>
<table role="grid" style="margin:0!important">
	<thead>
		<tr>
			<th  width="481" style="text-align:left">GERADOR POR <?php echo strtoupper($processo->CriadoPor->nome_utilizador); ?> EM <?php echo $util->bd_date_to_view( substr($processo->dataCriacao, 0, 10) ) . ' às ' .substr($processo->dataCriacao, 11) ?></th>
			<th  width="434" style="text-align:left">RECEBIDO POR ___________________________________ EM <?php echo date('d/m/Y') ?> às <?php echo date('H:i:s') ?> </th>
		</tr>
		<tr>
			<th  width="475" style="text-align:left">ASS: ___________________________________</th>
			<th  width="434" style="text-align:left">DOC: ___________________________________, ASS: ___________________________________</th>
		</tr>
	</thead>
</table>

<style type="text/css">
	table tr th, table tr td{
		font-size: 0.480rem;
	}

	@media all {
		.page-break	{ display: none; }
	}

	@media print {
		.page-break	{ display: block; page-break-before: always; }
	}

</style>