<?php
$util = new Util;
$totalFinanciado = 0;
$totalRepasse = 0;
?>
<table role="grid" style="margin:0!important;border-bottom:none!important">
    <thead>
        <tr>
            <th  width="917" style="text-align:center">RECEBIMENTO DE CONTRATOS: <?php echo strtoupper($processo->Destinatario->getConcat()) . ' / ' . $processo->Destinatario->cnpj; ?></th>
        </tr>
    </thead>
</table>
<table role="grid" style="margin:0!important;border-bottom:none!important">
    <thead>
        <tr>
            <th  width="920" style="text-align:left">DATA DE PROCESSAMENTO: <?php echo $util->bd_date_to_view(substr($processo->dataCriacao, 0, 10)) . ' às ' . substr($processo->dataCriacao, 11) ?> </th>
        </tr>
    </thead>
</table>
<table role="grid" style="margin:0!important;border-bottom:none!important">
    <thead>
        <tr>
            <th  width="920" style="text-align:left">CÓDIGO DO PROCESSO: <?php echo $processo->codigo ?> </th>
        </tr>
    </thead>
</table>
<table role="grid" style="margin:0 0 0 0!important;border-bottom:none!important">
    <thead>
        <tr>
            <th  width="920" style="text-align:center">Propostas</th>
        </tr>
    </thead>
</table>
<table role="grid" style="margin:0!important;">
    <thead>
        <tr>
            <th  width="80" style="text-align:left">DATA:</th>
            <th  width="151" style="text-align:left">CÓDIGO:</th>
            <th  width="70" style="text-align:left">CPF:</th>
            <th  width="200" style="text-align:left">NOME:</th>
            <th  width="139" style="text-align:left">VAL FIN:</th>
            <th  width="60" style="text-align:left">CARÊNCIA:</th>
            <th  width="60" style="text-align:left">PARCELAS:</th>
            <th  width="137" style="text-align:left">VAL REPASSE:</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($processo->recebimentoHasPropostas as $itemBordero) { ?>
            <?php $totalRepasse += +$itemBordero->proposta->valorRepasse(); ?>
        <?php } ?>

        <?php foreach ($processo->recebimentoHasPropostas as $itemBordero) { ?>
            <?php $totalFinanciado += ($itemBordero->proposta->valor - $itemBordero->proposta->valor_entrada); ?>
        <?php } ?>
        <?php foreach ($processo->recebimentoHasPropostas as $itemBordero) { ?>
            <tr>
                <td><?php echo $util->bd_date_to_view(substr($itemBordero->proposta->data_cadastro, 0, 10)) ?></td>
                <td><?php echo $itemBordero->proposta->codigo ?></td>
                <td><?php echo $itemBordero->proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero ?></td>
                <td><?php echo strtoupper($itemBordero->proposta->analiseDeCredito->cliente->pessoa->nome) ?></td>
                <td><?php echo "R$ " . number_format($itemBordero->proposta->valor - $itemBordero->proposta->valor_entrada, 2, ',', '.') ?></td>
                <td><?php echo $itemBordero->proposta->carencia ?></td>
                <td><?php echo $itemBordero->proposta->qtd_parcelas ?></td>
                <td><?php echo "R$ " . number_format($itemBordero->proposta->valorRepasse(), 2, ',', '.') ?></td>
            </tr>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th>TOTAL: <?php echo "R$ " . number_format($totalFinanciado, 2, ',', '.'); ?></th>
            <th></th>
            <th></th>
            <th>TOTAL: <?php echo "R$ " . number_format($totalRepasse, 2, ',', '.'); ?></th>
        </tr>
    </tfoot>
</table>
<table role="grid" style="margin:0!important">
    <thead>
        <tr>
            <th  width="481" style="text-align:left">GERADOR POR <?php echo strtoupper($processo->CriadoPor->nome_utilizador); ?> EM <?php echo $util->bd_date_to_view(substr($processo->dataCriacao, 0, 10)) . ' às ' . substr($processo->dataCriacao, 11) ?></th>
            <th  width="434" style="text-align:left">RECEBIDO POR ___________________________________ EM <?php echo date('d/m/Y') ?> às <?php echo date('H:i:s') ?> </th>
        </tr>
        <tr>
            <th  width="475" style="text-align:left">ASS: ___________________________________</th>
            <th  width="434" style="text-align:left">DOC: ___________________________________, ASS: ___________________________________</th>
        </tr>
    </thead>
</table>
<style type="text/css">
    table tr th, table tr td{
        font-size: 0.480rem;
    }

    @media all {
        .page-break	{ display: none; }
    }

    @media print {
        .page-break	{ display: block; page-break-before: always; }
    }
    #grid_recebido_filter {
        display: none;
    }

</style>