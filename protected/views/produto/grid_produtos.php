<div class="row">
   <div class="col-sm-12">
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/produto/listarProdutos">
            Produtos
            </a>
         </li>
         <li class="active">
            Listar
         </li>
      </ol>
      <div class="page-header">
         <h1>
         Produtos
      </div>
   </div>
</div>
<p>
   <a class="btn btn-success" href="#modal_form_new_produto" data-toggle="modal" id="btn_modal_form_new_produto">
   Cadastrar Produto <i class="fa fa-plus"></i>
   </a>
</p>
<p>
</p>
<div class="row">
   <div class="col-sm-12">
      <table id="grid_produtos" class="table table-striped table-bordered table-hover table-full-width dataTable">
         <thead>
            <tr>
               <th>Descrição</th>
               <th class="no-orderable">Tipo de Produto</th>
               <!--<th class="no-orderable">Categoria</th>
               <th class="no-orderable">Modelo</th>-->
               <th class="no-orderable"></th>
            </tr>
         </thead>
         <tfoot>
            <tr>
               <th class="searchable">Descrição</th>
               <th></th>
               
               <th></th>
            </tr>
         </tfoot>
      </table>
   </div>
</div>

<!--Modal novo produto-->
<div id="modal_form_new_produto" class="modal fade" tabindex="-1" data-width="1024" style="display: none;">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
      &times;
      </button>
      <h4 class="modal-title">Cadastrar Produto</h4>
   </div>
   <form id="form-add-produto">
      <div class="modal-body">
         <div class="row">
            <div class="row-centralize">
               <div class="col-md-6">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label class="control-label">Descrição <span class="symbol required"></span></label>
                           <input readonly="true" required name="Produto[descricao]" type="text" class="form-control" id="produto_descricao">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label class="control-label">Categoria <span class="symbol required"></span></label> <a style="padding:2px 4px!important; font-size:8px!important; color:#FFF!important" class="btn-add btn btn-success" href="#modal_form_new_categoria" data-toggle="modal"><i class="fa fa-plus"></i></a>
                           <input data-show-no-father="0" required="true" value="" required="required" name="Produto[Categoria_id]" placeholder="Selecione uma categoria" type="hidden" id="Produto_Categoria_id" class="form-control search-select selectCategorias">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row-centralize">
               <div class="col-md-6">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label class="control-label">Tipo <span class="symbol required"></span></label>
                           <?php echo CHtml::dropDownList('Produto[TipoProduto_id]', 'Produto[TipoProduto_id]', CHtml::listData(TipoProduto::model()->findAll(), 'id', 'descricao'), array('class' => 'form-control search-select select2', 'prompt' => 'Selecione:', 'required' => true)); ?>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label class="control-label">
                           Modelo <span class="symbol required"></span> <a style="padding:2px 4px!important; font-size:8px!important; color:#FFF!important" class="btn-add btn btn-success" href="#modal_form_new_modelo" data-toggle="modal"><i class="fa fa-plus"></i></a>
                           </label>
                           <input required="required" name="Produto[Modelo_id]" placeholder="Selecione um modelo" type="hidden" id="Produto_Modelo_id" class="form-control search-select">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row-centralize">
               <div class="col-md-6">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label class="control-label">NCM <span class="symbol required"></span> <a style="padding:2px 4px!important; font-size:8px!important; color:#FFF!important" class="btn-add btn btn-success" href="#modal_form_new_ncm" data-toggle="modal"><i class="fa fa-plus"></i></a></label>
                           <input value="" required="required" name="Produto[Ncm_id]" placeholder="Selecione um ncm" type="hidden" id="Produto_NCM_id" class="form-control search-select">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-sm-12">
               
            </div>
         </div>
         <div class="row">
            <div class="col-sm-11">
               <h5>Características:</h5>
            </div>
         </div>
         <div class="row">
            <div class="row-centralize">
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">Cores <span class="symbol required"></span></label>
                     <br>
                     <select required="required" multiple="multiple" id="select_cores" class="form-control multipleselect select_redraw_table" name="Cores[]">
                        <?php foreach ( $cores as $cor ) { ?>
                           <option value="<?php echo $cor->id ?>"><?php echo $cor->valor ?></option>
                        <?php } ?>
                     </select>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="control-label">Tamanhos <span class="symbol required"></span></label>
                     <br>
                     <select required="required" multiple="multiple" id="select_tamanhos" class="form-control multipleselect select_redraw_table" name="Tamanhos[]">
                        <?php foreach ( $tamanhos as $tamanho ) { ?>
                           <option value="<?php echo $tamanho->id ?>"><?php echo $tamanho->valor ?></option>
                        <?php } ?>
                     </select>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal-footer">
         <div class="panel"></div>
         <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
         
         <button type="submit" class="btn btn-blue">Salvar</button>
         <div class="row">
         </div>
         <br>
         <div id="cadastro_msg_return" class="alert" style="text-align:left">
         </div>
      </div>
   </form>
</div>

<!--Modal nova categoria-->
<div id="modal_form_new_categoria" class="modal fade" tabindex="-1" data-width="860" style="display: none;">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
      &times;
      </button>
      <h4 class="modal-title">Adicionar Categoria</h4>
   </div>
   <form id="form-add-categoria-produto">
      <div class="modal-body">
         <div class="row">
            <div class="row-centralize">
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Nome <span class="symbol required"></span></label>
                           <input required class="form-control" type="text" name="CategoriaProduto[nome]">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Categoria pai <span class="symbol required"></span></label>
                           <input data-show-no-father="1" value="" name="CategoriaProduto[idPai]" placeholder="Selecione uma categoria" type="hidden" id="" class="form-control search-select selectCategorias">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="row">
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal-footer">
         <label class="checkbox-inline" style="float:left">
         <input id="categorias_checkbox_continuar" type="checkbox" value="">
         Continuar Cadastrando
         </label>
         <div style="background:#ECF0F1; border:none" class="panel"></div>
         <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
         <button type="submit" class="btn btn-blue btn-send">Salvar</button>
         <div class="row">
         </div>
         <br>
         <div id="cadastro_categorias_msg_return" class="alert" style="text-align:left; display:none"></div>
      </div>
   </form>
</div>

<!--Modal novo ncm-->
<div id="modal_form_new_ncm" class="modal fade" tabindex="-1" data-width="860" style="display: none;">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
      &times;
      </button>
      <h4 class="modal-title">Adicionar NCM</h4>
   </div>
   <form id="form-add-ncm">
      <div class="modal-body">
         <div class="row">
            <div class="row-centralize">
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Código <span class="symbol required"></span></label>
                           <input required class="form-control" type="text" name="NCM[codigo]">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Descrição <span class="symbol required"></span></label>
                           <input required class="form-control" type="text" name="NCM[descricao]">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-5">
                  <div class="row">
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal-footer">
         <label class="checkbox-inline" style="float:left">
         <input id="ncms_checkbox_continuar" type="checkbox" value="">
         Continuar Cadastrando
         </label>
         <div style="background:#ECF0F1; border:none" class="panel"></div>
         <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
         <button type="submit" class="btn btn-blue btn-send">Salvar</button>
         <div class="row">
         </div>
         <br>
         <div id="cadastro_ncm_msg_return" class="alert" style="text-align:left; display:none"></div>
      </div>
   </form>
</div>

<!--Modal novo modelo-->
<div id="modal_form_new_modelo" class="modal fade" tabindex="-1" data-width="860" style="display: none;">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
      &times;
      </button>
      <h4 class="modal-title">Adicionar Modelo</h4>
   </div>
   <form id="form-add-modelo">
      <div class="modal-body">
         <div class="row">
            <div class="row-centralize">
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Marca <span class="symbol required"></span> <a style="padding:2px 4px!important; font-size:8px!important; color:#FFF!important" class="btn-add btn btn-success" href="#modal_form_new_marca" data-toggle="modal"><i class="fa fa-plus"></i></a></label>
                           <input required="required" name="Modelo[Marca_id]" placeholder="Selecione uma marca" type="hidden" id="Modelo_marca_id" class="form-control search-select">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Descrição <span class="symbol required"></span></label>
                           <input required="required" class="form-control" type="text" name="Modelo[descricao]">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-5">
                  <div class="row">
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal-footer">
         <label class="checkbox-inline" style="float:left">
         <input id="modelo_checkbox_continuar" type="checkbox" value="">
         Continuar Cadastrando
         </label>
         <div style="background:#ECF0F1; border:none" class="panel"></div>
         <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
         <button type="submit" class="btn btn-blue btn-send">Salvar</button>
         <div class="row">
         </div>
         <br>
         <div id="cadastro_modelo_msg_return" class="alert" style="text-align:left; display:none"></div>
      </div>
   </form>
</div>

<!--Modal nova marca-->
<div id="modal_form_new_marca" class="modal fade" tabindex="-1" data-width="800" style="display: none;">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
      &times;
      </button>
      <h4 class="modal-title">Adicionar Marca</h4>
   </div>
   <form id="form-add-marca">
      <div class="modal-body">
         <div class="row">
            <div class="row-centralize">
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Nome <span class="symbol required"></span></label>
                           <input required="required" class="form-control" type="text" name="Marca[descricao]">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Fabricante <span class="symbol required"></span></label>
                           <input required="required" name="Marca[Fabricante_id]" placeholder="Selecione um fabricante" type="hidden" id="Marca_Fabricante_id" class="form-control search-select">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal-footer">
         <label class="checkbox-inline" style="float:left">
         <input id="marca_checkbox_continuar" type="checkbox" value="">
         Continuar Cadastrando
         </label>
         <div style="background:#ECF0F1; border:none" class="panel"></div>
         <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
         <button type="submit" class="btn btn-blue btn-send">Salvar</button>
         <div class="row">
         </div>
         <br>
         <div id="cadastro_marca_msg_return" class="alert" style="text-align:left; display:none"></div>
      </div>
   </form>
</div>

<style type="text/css">
   #grid_produtos_filter, #grid_produtos_length{
   display: none!important;
   }
   .panel{
   background: transparent!important;
   border:none!important;
   }
   .dropdown-menu {
      max-height: 200px;
      overflow-y: auto;
      overflow-x: hidden;
   }
</style>