<?php

	$util = new Util;

	$parcelasLeft 	= array();
	$parcelasRight 	= array();

	$arrMeses 		= array(
		'01' 		=> 'Jan',
		'02' 		=> 'Feb',
		'03' 		=> 'Mar',
		'04' 		=> 'Apr',
        '05' 		=> 'May',
        '06' 		=> 'Jun',
        '07' 		=> 'Jul',
        '08' 		=> 'Aug',
        '09' 		=> 'Sep',
        '10' 		=> 'Oct',
        '11' 		=> 'Nov',
        '12' 		=> 'Dec'
	);
	
?>
<form>
	<input type="hidden" id="proposta_codigo" value="<?php echo $proposta->codigo ?>">
</form>
<div id="main-center" style="padding-top:0!important;margin-top:0!important;">
	<header style="padding-top:0!important;margin-top:0!important;">
		<h1 style="width:150px;float:right;margin-top:0!important;">
			<img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/chubb_contrato.jpg">
		</h1>
		<h2 style="">PROPOSTA DE ADESÃO E CERTIFICADO DE SEGURO PRESTAMISTA</h2>
	</header>

	
	<section style="text-align:center;margin-top:0px;float:left;width:100%;text-align:left">
		<p><strong>Início de vigência do seguro</strong>: Às 24 horas do dia da contratação (data de assinatura desta proposta)</p>
		<p><strong>Término de vigência do seguro</strong>: Às 24 horas do último dia do período do financiamento</p>
	</section>

	<section id="contrato-section-one">
		<table>
			<thead>
				<tr>
					<th style="text-align:right; border:none!important">
						<strong>APÓLICE Nº 8.070.008</strong>
					</th>
				</tr>
			</thead>
		</table>
		<table>
			<thead>
				<tr>
					<th>DADOS DO SEGURADO</th>
				</tr>
			</thead>
		</table>
		<table>
			<tbody>
				<tr>
					<td><strong>Nome: </strong><?php echo strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome) ?></td>
					<td><strong>CPF: </strong><?php echo $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero ?></td>
				</tr>
			</tbody>
		</table>
		<table>
			<tbody>
				<tr>
					<td><strong>RG: </strong><?php echo $proposta->analiseDeCredito->cliente->pessoa->getRG()->numero ?></td>
					<td><strong>Data de Nascimento: </strong><?php echo $util->bd_date_to_view($proposta->analiseDeCredito->cliente->pessoa->nascimento) ?></td>
					<td><strong>TEL: </strong><?php echo $proposta->analiseDeCredito->cliente->pessoa->contato->getLasTelefone()->getDDD().$proposta->analiseDeCredito->cliente->pessoa->contato->getLasTelefone()->getNumero() ?></td>
				</tr>
			</tbody>
		</table>

		<div style="float:left;width:47%">
	</section>

	<section id="contrato-section-three" style="margin-top:0px">
		<table>
			<thead>
				<tr>
					<th>GARANTIAS</th>
					<th>LIMITE MÁXIMO DE IDENIZAÇÃO</th>
					<th>TAXA ÚNICA INDIVIDUAL (INCLUSO IOF)</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Morte – quita o saldo devedor limitado</td>
					<td>Até R$ 5.000,00</td>
					<td>2,096% sobre o valor do financiamento </td>
				</tr>
				<tr>
					<td>Invalidez Permanente Total por Acidente - quita o saldo devedor limitado</td>
					<td>Até R$ 5.000,00</td>
					<td>0,140% sobre o valor do financiamento</td>
				</tr>
				<tr>
					<td>Proteção Financeira – quita até 03 parcelas limitado</td>
					<td>Até R$ 300,00 (cada)</td>
					<td>7,764% sobre o valor do financiamento</td>
				</tr>
			</tbody>
		</table>
		<table>
			<tbody>
				<tr>
					<td><strong>TOTAL</strong></td>
					<td style="text-align:right;"><strong>10,00% sobre o valor do financiamento</strong></td>
				</tr>
			</tbody>
		</table>
	</section>
	<section id="contrato-section-four">
		<p>
			<strong>
				• Carência: Para a cobertura de Proteção Financeira de 60 dias a contar da data de adesão 
			</strong>
		</p>
		<p>
			<strong>
				• Franquia: Para a cobertura de Proteção Financeira de 30 dias a contar da data do evento coberto.
			</strong>
		</p>
		<p style="font-size:15px">
			Beneficiário: Para as garantias de Morte ou Invalidez Permanente Total por Acidente o único beneficiário
			será o Estipulante da apólice pelo saldo devedor existente na data do evento coberto, até o limite de R$ 5.000,00 por CPF,
			independentemente do número de seguros contratados. Para a garantia de Proteção Financeira o beneficiário será o próprio 
			segurado para recebimento de até 03 parcelas no valor de até R$300,00 (cada). Só serão aceitos neste seguro proponentes
			com idades compreendidas entre 18 (dezoito) e 65 (sessenta e cinco) anos. Se a idade do proponente estiver fora deste intervalo
			ficará prejudicado o direito à indenização. Será pago ao Estipulante da apólice o valor correspondente a 62% (sessenta e dois por cento)
			incidente sobre o prêmio líquido de IOF.
		</p>
		<p style="font-size:15px">
			O Segurado terá o direto de arrependimento previsto no artigo 49 do Código de Defesa do Consumidor, 
			nos termos do qual o Segurado poderá desistir do seguro ora contratado no prazo de 7 (sete) dias corridos a 
			contar da assinatura da Proposta de Adesão, mediante comunicação formal ao Estipulante ou à Seguradora, 
			hipótese em que os valores eventualmente pagos, a qualquer título pelo Segurado, durante o prazo de reflexão, 
			serão devolvidos de imediato, monetariamente atualizados. <span style="text-decoration: underline;">O não repasse do valor do prêmio pelo Estipulante à 
			Seguradora pode gerar o cancelamento do seguro.</span>
		</p>
		<p style="font-size:15px">
			Sim, eu aceito a adesão ao Seguro Prestamista, acima referido, e autorizo a cobrança única no valor correspondente a 
			10,00% sobre o valor do financiamento. Declaro para todos os fins e efeitos: (i) ter prestado informações completas e verídicas; 
			(ii) estar ciente e de acordo com o conteúdo das Condições Gerais e Especiais que neste ato me foram apresentadas, as quais li, 
			compreendi e fui suficientemente esclarecido a respeito de seus termos; (iii) estar ciente de que perderei o direito a uma eventual 
			indenização caso seja constatada a falsidade de qualquer informação conforme determina o Código Civil; e (iv) estar ciente de que o 
			não pagamento do prêmio de seguro poderá ocasionar o cancelamento do seguro.
		</p>
		<p style="font-size:15px">
			O registro deste plano na SUSEP não implica, por parte da Autarquia, incentivo ou recomendação à sua comercialização. 
			O Segurado poderá consultar a situação cadastral do seu corretor de seguros no 
			site www.susep.gov.br por meio do número do seu registro na SUSEP, nome completo, CNPJ ou CPF.
		</p>
		<p>
			<strong>
				Este seguro é por prazo determinado, tendo a Seguradora a faculdade de não renovar
				 a apólice na data de vencimento, sem devolução dos prêmios pagos nos termos da apólice.
			</strong>
		</p>
		<p style="font-size:15px">
			<strong>Estipulante:</strong> CREDSHOW OPERADORA DE CREDITO LTDA EPP, CNPJ: 13.861.348/0001-15
		</p>
		<p style="font-size:15px">
			<strong>Seguradora:</strong> Chubb do Brasil Cia. de Seguros, Cód. SUSEP 501-1, CNPJ 33.170.085/0001-05, Processo SUSEP Seguro Prestamista 15414.000059/2012-55.
		</p>
		<p style="font-size:15px">
			<strong>Corretor:</strong> AMERICAN UNITY CORRETORA DE SEGUROS, CNPJ 09.016.513/0001-65
		</p>
	</section>
	<section id="assinatura">
		<table>
			<tbody>
				<tr>
					<td width="900">________________________________________</td>
					<td style="text-align:right">____/____/____</td>
				</tr>
			</tbody>
		</table>
		<table>
			<tbody>
				<tr>
					<td>Assinatura do segurado</td>
				</tr>
			</tbody>
		</table>
	</section>
	<!--
	<section id="contrato-section-four">
		<div style="float:left;width:100%">
			<p>
				<label style="font-weight: bold;text-decoration: underline;">CLÁUSULA PRIMEIRA:</label> Através do presente, reconhece expressamente a <strong>DEVEDORA</strong> que possui uma divida a ser paga a <strong>CREDORA</strong>, consubstanciada no montante total de R$ <label style="text-decoration: underline;"><?php echo number_format($proposta->getValorFinal(), 2, ',','.') ?> (<?php echo $util->valor_extenso($proposta->getValorFinal(), false); ?> )</label>
			</p>
			<p>
				<label style="font-weight: bold;text-decoration: underline;">PARÁGRAFO ÚNICO:</label> Qualquer recebimento das prestações fora dos prazos acordados constituirá mera tolerância, que não afetará de forma alguma as datas de vencimento daquelas prestações ou demais cláusulas e condições desta composição, nem importará novação ou modificação do ajustado, inclusive quanto aos encargos resultantes da mora.
			</p>
			<p>
				<label style="font-weight: bold;text-decoration: underline;">CLÁUSULA SEGUNDA:</label> No caso de não cumprimento de quaisquer das obrigações previstas no presente instrumento nos respectivos prazos e condições, ou ainda se contra a <strong>DEVEDORA</strong> for proposta medida judicial ou extrajudicial que possa afetar sua capacidade de pagamento da dívida ora confessada, poderá a <strong>CREDORA</strong> independentemente de qualquer aviso ou notificação judicial ou extrajudicial, cobrar, imediatamente, toda a dívida acima descrita e confessada, acrescida dos encargos financeiros pactuados.
			</p>
			<p>
				<label style="font-weight: bold;text-decoration: underline;">CLÁUSULA TERCEIRA:</label> O não pagamento da dívida em conformidade com Cláusula Primeira acarretará, quando de sua efetiva liquidação e obrigação da <strong>DEVEDORA</strong> de pagar a <strong>CREDORA</strong> os juros moratórios de 0,37% (zero vírgula trinta e sete por cento) ao dia, sobre o valor devido.
			</p>
			<p>
				<label style="font-weight: bold;text-decoration: underline;">CLÁUSULA QUARTA:</label> O presente é realizado em caráter irrevogável, irretratável e intransferível o qual obrigam as partes a cumpri-lo a qualquer título, bem como seus herdeiros e sucessores.
			</p>
			<p>
				<label style="font-weight: bold;text-decoration: underline;">CLÁUSULA QUINTA:</label> Fica eleito o foro da Comarca de Natal/RN, para dirimir quaisquer dúvidas oriundas do presente instrumento. E, por estarem justas e avençadas, assinam o presente instrumento, feito em duas (02) vias, de um só teor e forma, na presença das testemunhas abaixo.
			</p>
		</div>
	</section>
	<section id="contrato-section-five">
		<div style="width:100%;">
			<p>
				<?php echo $proposta->analiseDeCredito->filial->getEndereco()->cidade .' / '. $proposta->analiseDeCredito->filial->getEndereco()->uf; ?>,
				<?php echo $util->dataPorExtenso( substr($proposta->data_cadastro, 8,2), $arrMeses[substr($proposta->data_cadastro, 5,2)], substr($proposta->data_cadastro, 0,4) );?>
			</p>
			<p style="margin-top:80px">__________________________________________________________________</p>
			<p><?php echo strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome); ?></p>
		</div>
	</section>
	<section id="contrato-section-six">
		<div style="float:left">
			<p style="text-align:left">Testemunhas (Nome e CPF)</p>
			<p>___________________________________ ______________________</p>
		</div>
		<div style="width:46%;float:right">
			<p>&nbsp;</p>
			<p>___________________________________ ______________________</p>
		</div>
	</section>
	-->
	
</div>

<script>
        $(function(){
                   	
        	$("#codigodebarras").barcode($('#proposta_codigo').val(), "code128", {
				'barWidth' : 2, 'barHeight':50, 'fontSize': 14
			});
        })
</script>

<style type="text/css">
	
	#assinatura{
		margin-bottom: 50px;
	}

	#contrato-section-four p strong{
		font-size: 19px;
	}

	#contrato-section-three, #contrato-section-one, #assinatura{
		width: 100%;
		padding: 0px!important;
		float: left;
		border:none;
	}
	
	#contrato-section-three table, #contrato-section-one table{
		width: 100%;
	}
	#contrato-section-three table tr, #contrato-section-one table tr{
		border: 1px solid ;
	}
	#contrato-section-three table tr th, #contrato-section-one table tr th{
		border: 1px solid ;
		padding:10px!important;
	}
	#contrato-section-three table tr td, #contrato-section-one table tr td{
		border: 1px solid ;
		padding:10px!important;
	}
	#main-center section p {
	    /* text-align: justify; */
	    line-height: 18px;
	    letter-spacing: -0.1px;
	}
</style>