<?php

	$util = new Util;

	$parcelasLeft 	= array();
	$parcelasRight 	= array();

	$arrMeses 		= array(
		'01' 		=> 'Jan',
		'02' 		=> 'Feb',
		'03' 		=> 'Mar',
		'04' 		=> 'Apr',
        '05' 		=> 'May',
        '06' 		=> 'Jun',
        '07' 		=> 'Jul',
        '08' 		=> 'Aug',
        '09' 		=> 'Sep',
        '10' 		=> 'Oct',
        '11' 		=> 'Nov',
        '12' 		=> 'Dec'
	);
	
?>
<form>
	<input type="hidden" id="proposta_codigo" value="<?php echo $proposta->codigo ?>">
</form>
<div id="main-center" 
     style="padding-top:0!important;margin-top:0!important;">
 
    <image id="back_image" src="/images/ccp_contrato/ccp_contrato.png" />  
    <p id="contrato" align="center">
        CARTA DE CRÉDITO CCP REFERENTE AO CONTRATO Nº  <?php echo $proposta->updateCodigoContratoOmni(); ?>
    </p>
    <p id="nome" align="center">
        NOME:  <?php echo $proposta->analiseDeCredito->cliente->pessoa->nome; ?>
    </p>
    <p id="cpf_rg" align="center">
        CPF:  <?php echo $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero; ?> | RG: <?php echo $proposta->analiseDeCredito->cliente->pessoa->getRG()->numero; ?>
    </p>
    <p id="data" align="center">
        DATA:  <?php echo $proposta->data_cadastro_br; ?>
    </p>
<!--    <p align="center">
        <image src="/images/nova_logo.png" />
    </p>
    
    <p align="center">
        CARTA DE CRÉDITO CDC REFERENTE AO CONTRATO Nº  <?php echo $proposta->updateCodigoContratoOmni(); ?>
    </p>
    
    <p align="center" style="font-size: 24px">
        <b>
            Carta de Crédito
            <br>
            <br>
            Parabéns
        </b>
    </p>
    
    <p align="center">
        Sr(a).  <?php echo $proposta->analiseDeCredito->cliente->pessoa->nome; ?>
    </p>
    
    <p align="center" style="font-size: 16px">
            Pela sua carta de crédito.
            <br>
            <br>
            <b>
                Você pode adquirir um produto em toda 
                <br>
                rede credenciada CREDSHOW
            </b>
        </b>
    </p>
    
    <br>
    <br>
    <br>
    
    <p align="center">
        ________________________________________
        <br>
        <?php echo $proposta->analiseDeCredito->cliente->pessoa->nome; ?>
    </p>
    
    <br>
    <br>
    
    <div style="text-align: center">
        <image src="/images/produtos1.png" />
        <image src="/images/produtos2.png" />
    </div>-->
    
</div>
<style type="text/css">
    #back_image{
        width: 27cm;
        height: 38.7cm;
        position: absolute;
    }

    #contrato{
        margin-top: 31cm;
        margin-left: 4.6cm;
        position: relative;
        float: left;
    }
    #nome{
        margin-top: 31.5cm;
        margin-left: -9.9cm;
        position: relative;
        float: left;
    }
    #cpf_rg{
        margin-top: 32cm;
        margin-left: -9.9cm;
        position: relative;
        float: left;
    }
    #data{
        margin-top: 32.5cm;
        margin-left: -9.9cm;
        position: relative;
        float: left;
    }
</style>