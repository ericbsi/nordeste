<?php

	$util = new Util;

	$parcelasLeft 	= array();
	$parcelasRight 	= array();

	$arrMeses 		= array(
		'01' 		=> 'Jan',
		'02' 		=> 'Feb',
		'03' 		=> 'Mar',
		'04' 		=> 'Apr',
        '05' 		=> 'May',
        '06' 		=> 'Jun',
        '07' 		=> 'Jul',
        '08' 		=> 'Aug',
        '09' 		=> 'Sep',
        '10' 		=> 'Oct',
        '11' 		=> 'Nov',
        '12' 		=> 'Dec'
	);
	
?>
<form>
	<input type="hidden" id="proposta_codigo" value="<?php echo $proposta->codigo ?>">
</form>
<div id="main-center" style="padding-top:0!important;margin-top:0!important;">
	<header style="padding-top:0!important;margin-top:0!important;">
		<h1 style="width:300px;float:left;margin-top:0!important;">
			<img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/logocredshow.png">
		</h1>
		<h2 style="float:right;">CÉDULA DE CRÉDITO BANCÁRIO Nº <?php echo $proposta->codigo ?></h2>
	</header>
	<section id="contrato-section-one">
		<div style="float:left;width:47%">
			<p><span>Devedora: </span><?php echo strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome) ?></p>
			<p><span>Endereço: </span><?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->logradouro ?>, N° <?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->numero ?></p>
		</div>
		<div style="float:left;width:38%">
			<p><span>CPF: </span><?php echo $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero ?></p>
			<p><span>Cidade/UF: </span><?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cidade ?>/<?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->uf ?></p>
		</div>
		<div style="float:left;width:15%;">
			<p><span>RG: </span><?php echo $proposta->analiseDeCredito->cliente->pessoa->getRG()->numero ?></p>
			<p><span>CEP: </span><?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cep ?></p>
		</div>
		
	</section>
	<section id="contrato-section-two">
		<div style="float:left;width:80%">
			<p><span>Credora: </span> CREDSHOW OPERADORA DE CRÉDITO S/A</p>
			<p><span>Endereço: </span> Rua Doutor Luiz Felipe Câmara, N° 55, Ed. Themis Tower, sala 506, Lagoa Nova</p>
		</div>
		<div style="float:left;width:20%">
			<p><span>Cidade/UF: </span> Natal/RN</p>
		</div>
	</section>
	<section style="text-align:center;margin-top:10px;float:left;width:100%">
		<h3 style="font-size:15px;">DADOS DA OPERAÇÃO</h3>
	</section>
	<section id="contrato-section-three">
		<div style="float:left;width:50%">
			<p><span>Valor solicitado: </span>R$ <?php echo number_format($proposta->valor-$proposta->valor_entrada, 2, ',','.') ?></p>
			<p><span>Tarifa de cadastro: </span>R$ 0,00</p>
			<!--<p><span>Seguros: </span>R$ 0,00</p>-->
			<p><span>Seguros: </span>R$ <?php echo number_format($proposta->calcularValorDoSeguro(),2,',','.'); ?></p>
			<p><span>Valor total financiado: </span>R$ <?php echo number_format($proposta->getValorFinanciado(), 2, ',','.') ?></p>
			<p><span>Valor da parcela: </span>R$ <?php echo number_format($proposta->getValorParcela(), 2,',','.'); ?></p>
		</div>
		<div style="float:left;width:50%">
			<p><span>Tarifa efetiva a.m. com IOF: </span><?php echo number_format($proposta->tabelaCotacao->taxa,2,',','.') . '%'; ?></p>
			<p><span>Tarifa efetiva a.a. com IOF: </span><?php echo number_format($proposta->tabelaCotacao->taxa_anual,2,',','.') . '%'; ?></p>
			<p><span>Quantidade de parcelas: </span><?php echo $proposta->qtd_parcelas ?></p>
			<p><span>Vencimento 1° parcela: </span><?php echo $proposta->getDataPrimeiraParcela() ?></p>
			<p><span>Valor total a pagar: </span>R$ <?php echo number_format($proposta->getValorFinal(),2,',','.'); ?></p>
		</div>
	</section>
	<section style="text-align:center;margin-top:10px;float:left;width:100%">
		<h3 style="font-size:15px;">CLÁUSULAS E CONDIÇÕES</h3>
	</section>
	<section id="contrato-section-four">
		<div style="float:left;width:100%">
			<p>
				<label style="font-weight: bold;text-decoration: underline;">CLÁUSULA PRIMEIRA:</label> Através do presente, reconhece expressamente a <strong>DEVEDORA</strong> que possui uma divida a ser paga a <strong>CREDORA</strong>, consubstanciada no montante total de R$ <label style="text-decoration: underline;"><?php echo number_format($proposta->valor_final, 2, ',','.') ?> (<?php echo $util->valor_extenso("$proposta->valor_final", false); ?> )</label>
			</p>
			<p>
				<label style="font-weight: bold;text-decoration: underline;">PARÁGRAFO ÚNICO:</label> Qualquer recebimento das prestações fora dos prazos acordados constituirá mera tolerância, que não afetará de forma alguma as datas de vencimento daquelas prestações ou demais cláusulas e condições desta composição, nem importará novação ou modificação do ajustado, inclusive quanto aos encargos resultantes da mora.
			</p>
			<p>
				<label style="font-weight: bold;text-decoration: underline;">CLÁUSULA SEGUNDA:</label> No caso de não cumprimento de quaisquer das obrigações previstas no presente instrumento nos respectivos prazos e condições, ou ainda se contra a <strong>DEVEDORA</strong> for proposta medida judicial ou extrajudicial que possa afetar sua capacidade de pagamento da dívida ora confessada, poderá a <strong>CREDORA</strong> independentemente de qualquer aviso ou notificação judicial ou extrajudicial, cobrar, imediatamente, toda a dívida acima descrita e confessada, acrescida dos encargos financeiros pactuados.
			</p>
			<p>
				<label style="font-weight: bold;text-decoration: underline;">CLÁUSULA TERCEIRA:</label> O não pagamento da dívida em conformidade com Cláusula Primeira acarretará, quando de sua efetiva liquidação e obrigação da <strong>DEVEDORA</strong> de pagar a <strong>CREDORA</strong> os juros moratórios de 0,29% (zero vírgula vinte e nove por cento) ao dia, sobre o valor devido.
			</p>
			<p>
				<label style="font-weight: bold;text-decoration: underline;">CLÁUSULA QUARTA:</label> O presente é realizado em caráter irrevogável, irretratável e intransferível o qual obrigam as partes a cumpri-lo a qualquer título, bem como seus herdeiros e sucessores.
			</p>
			<p>
				<label style="font-weight: bold;text-decoration: underline;">CLÁUSULA QUINTA:</label> Fica eleito o foro da Comarca de Natal/RN, para dirimir quaisquer dúvidas oriundas do presente instrumento. E, por estarem justas e avençadas, assinam o presente instrumento, feito em duas (02) vias, de um só teor e forma, na presença das testemunhas abaixo.
			</p>
		</div>
	</section>
	<section id="contrato-section-five">
		<div style="width:100%;">
			<p>
				<?php echo $proposta->analiseDeCredito->filial->getEndereco()->cidade .' / '. $proposta->analiseDeCredito->filial->getEndereco()->uf; ?>,
				<?php echo $util->dataPorExtenso( substr($proposta->data_cadastro, 8,2), $arrMeses[substr($proposta->data_cadastro, 5,2)], substr($proposta->data_cadastro, 0,4) );?>
			</p>
			<p style="margin-top:80px">__________________________________________________________________</p>
			<p><?php echo strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome); ?></p>
		</div>
	</section>
	<section id="contrato-section-six">
		<div style="float:left">
			<p style="text-align:left">Testemunhas (Nome e CPF)</p>
			<p>___________________________________ ______________________</p>
		</div>
		<div style="width:46%;float:right">
			<p>&nbsp;</p>
			<p>___________________________________ ______________________</p>
		</div>
	</section>
	
	<?php if( $proposta->segurada ){ ?>
		<section id="section-mensagem-seguro">
			<div style="width:10%;float:left;">
				<img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/logo_chubb.gif">
			</div>
			<div style="width:85%;float:left">
				<p>Você adquiriu um seguro da <strong>Chubb do Brasil Cia de Seguros - CNPJ 33.170.085.0001/05</strong>. Em até 40 dias, enviaremos para a sua residência uma cópia do contrato, contendo o número da sua apólice.</p>
			</div>
		</section>
	<?php } ?>
	<section>
		<div id="codigodebarras"></div>
	</section>
</div>

<script>
        $(function(){
                   	
        	$("#codigodebarras").barcode($('#proposta_codigo').val(), "code128", {
				'barWidth' : 2, 'barHeight':50, 'fontSize': 14
			});
        })
</script>