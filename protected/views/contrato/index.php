<?php

	$util = new Util;

	$parcelasLeft 	= array();
	$parcelasRight 	= array();

	$arrMeses 		= array(
		'01' 		=> 'Jan',
		'02' 		=> 'Feb',
		'03' 		=> 'Mar',
		'04' 		=> 'Apr',
        '05' 		=> 'May',
        '06' 		=> 'Jun',
        '07' 		=> 'Jul',
        '08' 		=> 'Aug',
        '09' 		=> 'Nov',
        '10' 		=> 'Sep',
        '11' 		=> 'Oct',
        '12' 		=> 'Dec'
	);


?>
<div id="main-center">
	
	<form>
		<input type="hidden" id="proposta_codigo" value="<?php echo $proposta->codigo ?>">
	</form>

	<header>
		<h1>
			<img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/logocredshow.png">
		</h1>
		<h2>INSTRUMENTO PARTICULAR DE CONFISSÃO DE DÍVIDA</h2>
	</header>
	<section id="intro">
		<p>
			Pelo presente instrumento particular de <strong>CONFISSÃO DE DÍVIDA</strong>, 
			por esta e melhor forma de direito, em que são partes de um lado, 
			<?php echo strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome) ?>, <?php echo $proposta->analiseDeCredito->cliente->pessoa->nacionalidade ?>, <?php echo $proposta->analiseDeCredito->cliente->pessoa->estadoCivil->estado ?>, <?php echo $proposta->analiseDeCredito->cliente->pessoa->getDadosProfissionais()->profissao ?>, titular do CPF n° <strong><?php echo $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero ?></strong>, RG n° <strong><?php echo $proposta->analiseDeCredito->cliente->pessoa->getRG()->numero ?> </strong>
			residente na Rua <?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->logradouro ?>, n° <?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->numero ?>, bairro <?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->bairro ?>, 
			<?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cidade ?>/<?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->uf ?>, doravante denominada <strong>DEVEDORA</strong>, representada neste ato 
			e de outro lado, <strong>CREDSHOW OPERADORA DE CRÉDITO S/A<?php //echo $proposta->analiseDeCredito->filial->empresa->razao_social ?></strong>, titular do CNPJ nº <strong>13.861.348/0001-15<?php //echo $proposta->analiseDeCredito->filial->empresa->cnpj ?></strong>, 
			localizada na Rua Doutor Luiz Felipe Câmara, n° 55, Edifício Themis Tower, sala 506, Lagoa Nova, CEP 59.064-200, Natal-RN,  doravante denominada <strong>CREDORA</strong>, ajustam entre si, o presente <strong>“Contrato de Confissão de Dívida”</strong>, 
			que se regerá pelas cláusulas seguintes e pelas condições descritas no presente. 
		</p>
	</section>

	<section id="clausulas">
		<p>
			<strong class="underline">CLÁUSULA PRIMEIRA: </strong>Através do presente, reconhece expressamente a <strong>DEVEDORA</strong> que possui uma dívida a ser paga à <strong>CREDORA</strong>, consubstanciada no montante total de R$ <span class="underline"><?php echo number_format($proposta->valor_final, 2, ',','.') ?> (<?php echo$util->valor_extenso("$proposta->valor_final", false); ?> )</span>.
		</p>
		<p>Parágrafo Único: A dívida é originada dos itens e condições descritos na proposta <?php echo $proposta->codigo; ?>.</p>

		<p>
			<strong class="underline">CLÁUSULA SEGUNDA: </strong>A dívida ora reconhecida, confessada, será paga pela <strong>DEVEDORA</strong> da seguinte forma:
		</p>

		<?php for ( $i = 1; $i <= $proposta->qtd_parcelas; $i++ ) { ?>

				<!--<p><?php// echo $i .'º'?> Parcela N° <?php //echo $i . '/' . $proposta->qtd_parcelas ?> valor R$ <?php //echo number_format($proposta->getValorParcela(),2,',','.') ?> vencimento <?php //echo $util->bd_date_to_view(Parcela::model()->getVencimento(($i-1), $proposta)); ?></p>-->

				<?php 

					$vencimento = $proposta->getDataPrimeiraParcela();

					if( $i != 0 )
					{
						//$vencimento = $util->bd_date_to_view( Parcela::model()->getVencimento( ($i-1), $proposta ) );

						$vencimento = $util->bd_date_to_view( $util->adicionarMesMantendoDia( $util->view_date_to_bd( $proposta->getDataPrimeiraParcela() ), $i-1 )->format('Y-m-d') );
					}

					$parcela = $i.'° Parcela N° ' . $i . '/' . $proposta->qtd_parcelas . ' valor R$ ' . number_format($proposta->getValorParcela(),2,',','.') . ' vencimento ' . $vencimento;

					if( ($i+1)%2 == 0 )
					{
						$parcelasLeft[] = $parcela;
					}
					else
					{
						$parcelasRight[] = $parcela;	
					}
				?>

			<?php } ?>


		<div id="parcelas">
			
			<div id="parcelas-left">
				<?php
					for ( $l=0; $l < count( $parcelasLeft ); $l++ ) { 
						echo '<p>'.$parcelasLeft[$l].'</p>';
					}
				?>
			</div>
			<div id="parcelas-right">
				<?php
					for ( $r = 0; $r < count( $parcelasRight ); $r++ ) { 
						echo '<p>'.$parcelasRight[$r].'</p>';
					}
				?>
			</div>

			

		</div>

		<br><br><br><br>
	
		<p>
			<span class="underline">PARÁGRAFO ÚNICO: </span> 
			Qualquer recebimento das prestações fora dos prazos 
			avençados constituirá mera tolerância, que não afetará 
			de forma alguma as datas de vencimento daquelas prestações 
			ou demais cláusulas e condições desta composição, 
			nem importará novação ou modificação do ajustado, 
			inclusive quanto aos encargos resultantes da mora.
		</p>

		<p>
			<strong class="underline">CLÁUSULA TERCEIRA: </strong>
			No caso de não cumprimento de quaisquer das obrigações 
			previstas no presente instrumento, nos respectivos prazos e 
			condições, ou ainda se contra a DEVEDORA for proposta medida 
			judicial ou extrajudicial que possa afetar sua capacidade de 
			pagamento da dívida ora confessada, poderá a CREDORA independentemente 
			de qualquer aviso ou notificação judicial ou extrajudicial, cobrar, 
			imediatamente, toda a dívida acima descrita e confessada, acrescida dos encargos 
			financeiros pactuados, ficando sem efeito o disposto na Cláusula Primeira
			 e Cláusula Segunda do presente Instrumento.
		</p>
		<p>
			<strong class="underline">
				CLÁUSULA QUARTA:
			</strong>
			 O não pagamento da dívida em conformidade com Cláusula Segunda acarretará, 
			 quando de sua efetiva liquidação, a obrigação da DEVEDORA de pagar a CREDORA 
			 os juros moratórios de 0,29% (zero virgula vinte e oito por cento) ao dia,
			 sobre o valor devido. 
		</p>
		<p>
			<strong class="underline">CLÁUSULA QUINTA: </strong>
			O presente é realizado em caráter irrevogável, irretratável e 
			intransferível, o qual obrigam as partes a cumpri-lo, a qualquer 
			título, bem como seus herdeiros e sucessores.
		</p>

		<p>
			<strong class="underline">
				CLÁUSULA SEXTA:
			</strong>
			Fica eleito o foro desta Comarca de Natal/RN, para dirimir 
			quaisquer dúvidas oriundas do presente instrumento. E, por 
			estarem justas e avençadas, assinam o presente instrumento, 
			feito em duas (02) vias, de um só teor e forma, na presença 
			das testemunhas abaixo.
		</p>

	</section>

	<section id="data-contrato">

	<p>
		<?php echo $proposta->analiseDeCredito->filial->getEndereco()->cidade .' / '. $proposta->analiseDeCredito->filial->getEndereco()->uf; ?>,
		<?php /*echo $util->dataPorExtenso(date('d'), date('M'), date('Y'));*/ ?>
		<?php echo $util->dataPorExtenso( substr($proposta->data_cadastro, 8,2), $arrMeses[substr($proposta->data_cadastro, 5,2)], substr($proposta->data_cadastro, 0,4) );?>
		<?php  //echo  substr($proposta->data_cadastro, 8,2) ?>
	</p> 	
	</section>

	<footer>
		<div id="ass-cliente">
			<p>___________________________________</p>
			<p><?php echo strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome); ?></p>
		</div>
		<div id="ass-testemunhas">
			<p style="margin-top:100px;">Testemunhas (Nome e CPF):</p>
			<div id="ass-tes-left">
				<p style="">___________________________________ ______________________</p>
			</div>
			<div id="ass-tes-right">
				<p style="">___________________________________ ______________________</p>
			</div>
		</div>
	
		<div id="codigodebarras"></div>

	</footer>

</div>

<script>
        $(function(){
                   	
        	$("#codigodebarras").barcode($('#proposta_codigo').val(), "code128", {
				'barWidth' : 2, 'barHeight':50, 'fontSize': 14
			});
        })
</script>