<?php
$nomeSeguradora = "CHUBB SEGUROS";
$numeroApolice = "8.070.008";

if ($proposta->venda !== NULL) {
    if ($proposta->venda->getProdutoSeguro() !== NULL) {
        $numeroApolice = $proposta->venda->getProdutoSeguro()->itemDoEstoque->getNumeroApolice();

        if ($proposta->venda->getProdutoSeguro()->itemDoEstoque->getFinanceira() !== NULL) {
            $nomeSeguradora = $proposta->venda->getProdutoSeguro()->itemDoEstoque->getFinanceira()->financeira->nome;
        }
    }
}

if ($proposta->Financeira_id == 10)
{

    $nome_fantasia  = "CREDSHOW FUNDO INVESTIMENTO DIREITOS CREDITORIOS LP";
    $cnpj           = "23.273.905/0001-30";
    $endereco       = "Av. Brigadeiro Faria de Lima, 1355, Andar 3, Jardim Paulistano - São Paulo/SP";
    $logom          = "cred_fidc_logo.png";
    $iof            = 0;

}
else
{

    $nome_fantasia  = "CREDSHOW OPERADORA DE CREDITO S/A";
    $cnpj           = "13.861.348/0001-15";
    $endereco       = "Av. Amintas Barros, 3700. CTC SL 1904. Lagoa Nova - Natal/RN";
    $logom          = "logocredshow.png";
    $iof            = $proposta->tabelaCotacao->taxa;

}

$util = new Util;

$parcelasLeft = array();
$parcelasRight = array();

$arrMeses = array(
        '01' => 'Janeiro',
        '02' => 'Fevereiro',
        '03' => 'Março',
        '04' => 'Abril',
        '05' => 'Maio',
        '06' => 'Junho',
        '07' => 'Julho',
        '08' => 'Agosto',
        '09' => 'Setembro',
        '10' => 'Outubro',
        '11' => 'Novembro',
        '12' => 'Dezembro'
);
?>
<form>

    <input type="hidden" id="proposta_codigo" value="<?php echo $proposta->codigo; ?>">

</form>
<div id="main-center" style="padding-top:0!important;margin-top:0!important;">
    <header style="padding-top:0!important;margin-top:0!important;">
        <h1 style="width:300px;float:left;margin-top:0!important;">
            <img width="218" height="50" src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/<?php echo $logom ?>">
        </h1>
        <div style="float:right; margin-right:20px;">
            <h3 style="margin-top:0!important;">Contrato Nº.: <?php echo $proposta->codigo ?></h3>
            <h3 style="float:right; margin-top:-20px;">CESSÃO DE CRÉDITO</h3>
        </div>
    </header>

    <table class="tab_contrato">
        <tr>
            <td style="background-color: #BDBDBD;" colspan="4">
                <strong>I – EMPRESA/LOJA:</strong>
            </td>
        </tr>
        <tr>
            <td colspan="4">
<?php echo $proposta->analiseDeCredito->filial->getConcat() ?>, com sede à
<?php echo $proposta->analiseDeCredito->filial->getEndereco()->logradouro ?>,
<?php echo $proposta->analiseDeCredito->filial->getEndereco()->numero ?>,
                <?php echo $proposta->analiseDeCredito->filial->getEndereco()->bairro ?>,
                <?php echo $proposta->analiseDeCredito->filial->getEndereco()->cidade . '/' . $proposta->analiseDeCredito->filial->getEndereco()->uf ?>,
                inscrita no CNPJ/MF sob o nº.:
                <?php echo $proposta->analiseDeCredito->filial->cnpj ?>
            </td>
        </tr>
        <tr>
            <td style="background-color: #BDBDBD;" colspan="4">
                <strong>II – QUALIFICAÇÃO DO COMPRADOR/CLIENTE</strong>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <strong>Nome:</strong>
<?php echo strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome) ?>
            </td>
            <td style="width:400px;">
                <strong>CPF:</strong>
                <?php echo $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero ?>
            </td>
        </tr>
        <tr>
            <td style="width:250px;">
                <strong>RG Nº:</strong>
<?php echo $proposta->analiseDeCredito->cliente->pessoa->getRG()->numero ?>
            </td>
            <td style="width:250px;">
                <strong >Estado Civil:</strong>
                <?php
                if ($proposta->analiseDeCredito->cliente->pessoa->Estado_Civil_id == 1) {
                    echo 'Solteiro(a)';
                }
                if ($proposta->analiseDeCredito->cliente->pessoa->Estado_Civil_id == 2) {
                    echo 'Casado(a)';
                }
                if ($proposta->analiseDeCredito->cliente->pessoa->Estado_Civil_id == 3) {
                    echo 'Divorciado(a)';
                }
                if ($proposta->analiseDeCredito->cliente->pessoa->Estado_Civil_id == 4) {
                    echo 'Viúvo(a)';
                }
                ?>
            </td>
            <td style="width:80px;">
                <strong>Sexo:</strong>
                <?php echo $proposta->analiseDeCredito->cliente->pessoa->sexo ?>
            </td>
            <td style="width:400px;">
                <strong>Email:</strong>
                <?php echo $proposta->analiseDeCredito->cliente->pessoa->getEmail() ?>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <strong>Endereço Res.:</strong>
<?php
echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->logradouro .
 ", nº " . $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->numero
?>
            </td>
            <td>
                <strong>Bairro:</strong>
                <?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->bairro ?>
            </td>
        </tr>
        <tr>
            <td style="width:250px;">
                <strong>Cidade:</strong>
                <?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cidade ?>
            </td>
            <td style="width:250px;">
                <strong >CEP:</strong>
<?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cep ?>
            </td>
            <td style="width:80px;">
                <strong>UF:</strong>
<?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->uf ?>
            </td>
            <td style="width:400px;">
                <strong>Telefone:</strong>
<?php echo $proposta->analiseDeCredito->cliente->pessoa->getTelefone() ?>
            </td>
        </tr>
        <tr>
            <td style="background-color: #BDBDBD;" colspan="4">
                <strong>III- ESPECIFICAÇÕES DA OPERAÇÃO:
                    <i class="fa fa-square-o"></i> Novo  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    <i class="fa fa-square-o"></i> Refinanciamento Contrato Nº___________________
            </td>
        </tr>
    </table>
    <table class="tab_contrato">
        <tr>
            <td colspan="2">
                <strong>1. Valor Solicitado: R$ </strong>
<?php echo number_format($proposta->valor - $proposta->valor_entrada, 2, ',', '.') ?>
            </td>
            <td colspan="2">
                <strong>2. Tarifa de Cadastro: R$ </strong> 0,00
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>3. (1+2+12) Valor Financiado: R$  </strong>
                R$ <?php echo number_format($proposta->getValorFinanciado(), 2, ',', '.') ?>
            </td>
            <td colspan="2">
                <strong>4. Saldo Devedor Refinanciamento: R$ </strong> -
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>5. (3-4-12) Valor Líquido do Crédito: R$ </strong>
                R$ <?php
echo number_format($proposta->getValorFinanciado() - $proposta->calcularValorDoSeguro(), 2, ',', '.');
?>
            </td>
            <td>
                <strong>6. Valor da Parcela: R$ </strong>
<?php echo number_format($proposta->getValorParcela(), 2, ',', '.'); ?>
            </td>
            <td>
                <strong>7. Quantidade de Parcelas: </strong>
<?php echo $proposta->qtd_parcelas ?>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>8. Vencimento da 1ª Parcela: </strong>
                <?php echo $proposta->getDataPrimeiraParcela() ?>
            </td>
            <td colspan="2">
                <strong>9. Taxa de Juros com IOF:</strong> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
<?php echo number_format($iof, 2, ',', '.'); ?> <strong>% ao mês</strong>
                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                <?php echo number_format($proposta->tabelaCotacao->taxa_anual, 2, ',', '.'); ?><strong>% ao ano </strong>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>10. Vencimento da Última Parcela: </strong>
                <?php echo $proposta->getDataUltimaParcela(); ?>
            </td>
            <td colspan="2">
                <strong>11. Custo Efetivo Total - CET: &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp % ao mês &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp % ao ano </strong>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>12. Seguro Nº: </strong> <?php if ($proposta->segurada == 1) {
                    echo $numeroApolice;
                } ?>
            </td>
            <td>
                <strong>Seguradora: </strong> <?php if ($proposta->segurada == 1) {
                    echo $nomeSeguradora;
                } ?>
            </td>
            <td>
                <strong>Valor do Prêmio: </strong>R$
<?php echo number_format($proposta->calcularValorDoSeguro(), 2, ',', '.'); ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <strong>13. Forma de Pagamento: <i class="fa fa-square-o"></i> Boleto Bancário</strong>
            </td>
        </tr>
        <tr>
            <td style="background-color: #BDBDBD;" colspan="4">
                <strong>IV - DADOS DO ADQUIRENTE/CESSIONÁRIA DO CRÉDITO </strong>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <strong>Razão Social: </strong> <?php echo $nome_fantasia; ?>
            </td>
            <td>
                <strong>CNPJ: </strong> <?php echo $cnpj; ?>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <strong>Endereço: </strong> <?php echo $endereco; ?>
            </td>
            <td>
                <strong>Contato: </strong>084 2040 0800
            </td>
        </tr>
        <tr>
            <td style="background-color: #BDBDBD;" colspan="4">
                <strong>V – ENCARGOS MORATÓRIOS</strong>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <strong>14. Juros Moratórios: </strong><?php echo ConfigLN::model()->valorDoParametro("taxa_mora"); ?>% <strong>ao dia.</strong>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <p>
                    O <strong>Comprador/Cliente</strong> autoriza a <strong>Empresa/Loja</strong>, a ceder, transferir, empenhar, alienar, dispor dos direitos e garantias decorrentes deste contrato,
                    independentemente de prévia comunicação.
                </p>
                <p>
                    Autorizo, também, a <strong>CESSIONÁRIA</strong> (IV) a registrar as informações decorrentes deste contrato e de minha responsabilidade junto ao
                    Sistema de Informações de Crédito (SCR) do Banco Central do Brasil (BACEN), para fins de supervisão do risco de crédito e intercâmbio
                    de informações com outras instituições financeiras. Estou ciente de que a consulta ao SCR pela Cessionário depende dessa prévia autorização
                    e que poderei ter acesso  aos dados do SCR pelos meios colocados a minha disposição pelo BACEN, sendo que eventuais pedidos de correções,
                    exclusões, registros de medidas judiciais e de manifestações de discordância sobre as informações inseridas no SCR deverão ser efetuados
                    por escrito, acompanhados, se necessário, de documentos. Ainda, autorizo (i) a fornecer e compartilhar as informações cadastrais,
                    financeiras e de operações ativas e passivas e serviços prestados junto a outras instituições pertencentes ao Conglomerado da <strong>CESSIONÁRIA</strong>,
                    ficando todas autorizadas a examinar e utilizar, no Brasil e no exterior, tais informações, inclusive para ofertas de produtos e serviços;
                    (ii) a informar aos órgãos de proteção ao crédito, tais como SERASA e SPC, os dados relativos a falta de pagamento de obrigações assumidas e
                    (iii) a compartilhar informações cadastrais com outras instituições financeiras e a contatar-me por meio de Cartas, e-mails,
                    Short Message Service (SMS) e telefone, inclusive para ofertar produtos e serviços.
                </p>
                <p>
                    A quitação das parcelas pagas via boleto bancário somente ocorrerá após a efetiva compensação e disponibilização do recurso ao credor.
                    Em caso de atraso no pagamento por parte do <strong>Comprador/Cliente</strong>, incidirão sobre o valor da obrigação vencida, juros moratórios de <?php echo ConfigLN::model()->valorDoParametro("taxa_mora"); ?>% ao dia.
                </p>
                <p>
                <h4>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<?php echo $proposta->analiseDeCredito->filial->getEndereco()->cidade . '/' . $proposta->analiseDeCredito->filial->getEndereco()->uf . ", " . Date('d') . " de  " . $arrMeses[Date('m')] . " de " . Date('Y'); ?></h4>
                </p>
                <p style="margin-top: 60px;">
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp________________________________________________
                </p>
                <p style="margin-top:-10px; margin-left: 33px;">
                    <strong>COMPRADOR/CLIENTE</strong>
                </p>
                <p style="margin-top: 30px;">
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp________________________________________________
                </p>
                <p style="margin-top:-10px; margin-left: 33px;">
                    <strong>EMPRESA/LOJA</strong>
                </p>
                <p style="margin-top: 30px; margin-left: 33px;">
                    <strong>TESTEMUNHAS:</strong> <br /><br />
                    1. ______________________________________________ &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    2. ______________________________________________ <br />

                    &nbsp&nbsp&nbsp Nome:
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    Nome:
                </p>
                <p style="margin-top:-10px; margin-left: 33px;">
                    &nbsp&nbsp&nbsp CPF:
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    CPF:
                </p>
            </td>
        </tr>
        <tr>
            <td style="background-color: #BDBDBD;" colspan="4">
                <strong>DECLARAÇÃO SE ANALFABETO OU IMPEDIDO DE ASSINAR</strong>
            </td>
        </tr>
    </table>
    <table class="tab_contrato">
        <tr>
            <td style="width:800px">
                <p>
                    Declaro que ouvi atentamente a leitura desta Cédula, na presença das testemunhas abaixo qualificadas,
                    estando ciente e de acordo com as condições e obrigações que assumi na presente operação.
                </p>
                <br />
                <p>
                    Assinatura a rogo do Eminente: ____________________________________________
                </p>
                <p style="margin-left:195px; margin-top:-10px"> Nome: </p>
                <p style="margin-left:195px; margin-top:-10px"> CPF/MF: </p>
                <p>
                    Testemunhas: <br /><br />
                    1. _________________________________________ &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    2. _________________________________________ <br />

                    &nbsp&nbsp&nbsp Nome:
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    Nome:
                </p>
                <p style="margin-top:-10px;">
                    &nbsp&nbsp&nbsp CPF:
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    CPF:
                </p>
            </td>
            <td style="text-align: center;">
                <p style="margin-left: 30px;">Impressao Digital</p>
                <div style="width:130px; height:160px; border: 2px solid; margin-left:17px;">
                </div>
            </td>
        </tr>
    </table>
    <p style="margin: 0 auto;">Central de Relacionamento Credshow: 84 2040 0800 | ouvidoria@credshow.com.br | www.credshow.com.br</p>
    <div id="codigodebarras"></div>
</div>
<script>
    $(function () {

        $("#codigodebarras").barcode($('#proposta_codigo').val(), "code128", {
            'barWidth': 2, 'barHeight': 60, 'fontSize': 12
        });
    });
</script>

<style type="text/css">

    #codigodebarras{
        margin: 0 auto;
        margin-top: -40px;
    }

    .tab_contrato{
        width: 1000px;
        border-collapse: collapse;
    }

    .tab_contrato tr td {
        border:1px solid;
    }

    td {
        padding-left:8px!important;
        padding-right:8px!important;
    }

    p {
        text-align: justify;
    }

    @media all {
        .page-break { display: none; }
    }

    @media print {
        .page-break { display: block; page-break-before: always; }
    }

    @media print {
        * {-webkit-print-color-adjust:exact;}
    }
</style>
