<?php

	$util = new Util;

	$parcelasLeft 	= array();
	$parcelasRight 	= array();

	$arrMeses 		= array(
		'01' 		=> 'Jan',
		'02' 		=> 'Feb',
		'03' 		=> 'Mar',
		'04' 		=> 'Apr',
        '05' 		=> 'May',
        '06' 		=> 'Jun',
        '07' 		=> 'Jul',
        '08' 		=> 'Aug',
        '09' 		=> 'Nov',
        '10' 		=> 'Sep',
        '11' 		=> 'Oct',
        '12' 		=> 'Dec'
	);
	
?>
<form>
	<input type="hidden" id="proposta_codigo" value="<?php echo $proposta->codigo ?>">
</form>
<div id="main-center" style="padding-top:0!important;margin-top:0!important;">
	<header style="padding-top:0!important;margin-top:0!important;">
		<h1 style="width:300px;float:left;margin-top:0!important;">
			<img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/credtresanos.png">
		</h1>
		<h2 style="float:right;">DETALHAMENTO DE AQUISIÇÃO DE SEGURO Nº <?php echo $proposta->codigo ?></h2>
	</header>
	<section id="contrato-section-one">
		<div style="float:left;width:47%">
			<p><span>Emitente: </span><?php echo strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome) ?></p>
			<p><span>Endereço: </span><?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->logradouro ?>, N° <?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->numero ?></p>
		</div>
		<div style="float:left;width:38%">
			<p><span>CPF: </span><?php echo $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero ?></p>
			<p><span>Cidade/UF: </span><?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cidade ?>/<?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->uf ?></p>
		</div>
		<div style="float:left;width:15%;">
			<p><span>RG: </span><?php echo $proposta->analiseDeCredito->cliente->pessoa->getRG()->numero ?></p>
			<p><span>CEP: </span><?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cep ?></p>
		</div>
		
	</section>
	
	<section style="text-align:center;margin-top:10px;float:left;width:100%">
		<h3 style="font-size:15px;">DADOS DA OPERAÇÃO COM SEGURO</h3>
	</section>
	<section id="contrato-section-three">
		<div style="float:left;width:50%">
			<p><span>Valor solicitado: </span>R$ <?php echo number_format($proposta->valor-$proposta->valor_entrada, 2, ',','.') ?></p>
			<p><span>Seguros: </span>R$ <?php echo number_format($proposta->calcularValorDoSeguro(),2,',','.'); ?></p>
			<p><span>Valor total financiado: </span>R$ <?php echo number_format($proposta->getValorFinanciado(), 2, ',','.') ?></p>
		</div>
	</section>
	
	<?php if( $proposta->segurada ){ ?>
		<section id="section-mensagem-seguro" style="width:970px">
			<div style="width:10%;float:left;">
				<img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/logo_chubb.gif">
			</div>
			<div style="width:90%;float:left">
				<p>Você adquiriu um seguro da <strong>Chubb do Brasil Cia de Seguros - CNPJ 33.170.085.0001/05</strong>. Em breve, enviaremos para a sua residência uma cópia do contrato, contendo o número da sua apólice.</p>
			</div>
		</section>
	<?php } ?>
	
	<section id="contrato-section-five">
		<div style="width:100%;">
			<p>
				<?php echo $proposta->analiseDeCredito->filial->getEndereco()->cidade .' / '. $proposta->analiseDeCredito->filial->getEndereco()->uf; ?>,
				<?php echo $util->dataPorExtenso( substr($proposta->data_cadastro, 8,2), $arrMeses[substr($proposta->data_cadastro, 5,2)], substr($proposta->data_cadastro, 0,4) );?>
			</p>
			<p style="margin-top:80px">__________________________________________________________________</p>
			<p><?php echo strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome); ?></p>
		</div>
	</section>
	<section id="contrato-section-six">
		<div style="float:left">
			<p style="text-align:left">Testemunhas (Nome e CPF)</p>
			<p>___________________________________ ______________________</p>
		</div>
		<div style="width:46%;float:right">
			<p>&nbsp;</p>
			<p>___________________________________ ______________________</p>
		</div>
	</section>
	
</div>

<script>
        $(function(){
                   	
        	$("#codigodebarras").barcode($('#proposta_codigo').val(), "code128", {
				'barWidth' : 2, 'barHeight':50, 'fontSize': 14
			});
        })
</script>