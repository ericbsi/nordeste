<?php
$taxa_mora = ConfigLN::model()->find("habilitado AND parametro = 'taxa_mora'");

$util = new Util;

$parcelasLeft = array();
$parcelasRight = array();

$arrMeses = array(
        '01' => 'Janeiro',
        '02' => 'Fevereiro',
        '03' => 'Março',
        '04' => 'Abril',
        '05' => 'Maio',
        '06' => 'Junho',
        '07' => 'Julho',
        '08' => 'Agosto',
        '09' => 'Setembro',
        '10' => 'Outubro',
        '11' => 'Novembro',
        '12' => 'Dezembro'
);

$fator = $proposta->getFator();

if($fator !== null)
{
    if($proposta->tabelaCotacao->ModalidadeId == 2){
        $valor          = $proposta->qtd_parcelas * $proposta->valor_parcela;

        $valorRepasse   = $valor - ($valor* ($fator->porcentagem_retencao/100));
        $valorIOF = ($valorRepasse * $fator->fatorIOF);
    }else{
        $valorIOF = (($proposta->valor - $proposta->valor_entrada) * $fator->fatorIOF);
    }
}
else
{
    $valorIOF = 0;
}

$imagem = Yii::app()->getBaseUrl(true) . "/images/viaNaoNegociavel.png";

$dataProposta   = new DateTime($proposta->data_cadastro);
$dataFormatada  = $dataProposta->format("d/m/Y");

?>
<form>
    <input type="hidden" id="proposta_codigo"   value="<?php echo $proposta->codigo                         ?>">
    <input type="hidden" id="modalidade"        value="<?php echo $proposta->tabelaCotacao->ModalidadeId    ?>">
</form>

<input type="hidden" id="vnum_mes"      value="<?php echo $proposta->qtd_parcelas;                                                                          ?>" />
<input type="hidden" id="vvlr_prin"     value="<?php echo number_format($proposta->getValorFinanciado() - $proposta->calcularValorDoSeguro(), 2, ',', '.'); ?>" />
<input type="hidden" id="vvlr_pres"     value="<?php echo number_format($proposta->getValorParcela(), 2, ',', '.');                                         ?>" />
<input type="hidden" id="data_contrato" value="<?php echo $dataFormatada;                                                                                   ?>" />
<input type="hidden" id="parcela_1"     value="<?php echo $proposta->getDataPrimeiraParcela();                                                              ?>" />

<input type="hidden" id="vjur"          />
<input type="hidden" id="vjuranox"      />
<input type="hidden" id="vjur_fin"      />
<input type="hidden" id="vjuranox_fin"  />

<div id="main-center" style="padding-top:0!important;margin-top:0!important;">
    <header style="padding-top:0!important;margin-top:0!important;">
        <div style="float:left; margin-left:20px;">
            <img src="<?php echo Yii::app()->baseUrl ?> /images/lecca_logo.png" width="100px"/>
        </div>
        <div style="float:right; margin-right:20px;">
            <h3 style="margin-top:0!important;">CÉDULA DE CRÉDITO BANCÁRIO</h3>
            <h3 style="float:right; margin-top:-20px;">Via Negociável</h3>
        </div>
    </header>

    <table class="tab_contrato">
        <tr>
            <td colspan="4">
                <span><i class="fa fa-square-o"></i> <strong>Crédito Pessoal</strong></span>
                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

                <span><i class="fa fa-square"></i> <strong>Crédito Direto ao Consumidor</strong></span>
                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

                <span><strong>CCB Nº: <?php echo $proposta->codigo ?></strong></span>
            </td>
        </tr>
        <tr>
            <td style="background-color: #BDBDBD;" colspan="4">
                <strong>CREDOR:</strong>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <strong>LECCA CRÉDITO FINANCIAMENTO E INVESTIMENTO S/A</strong>, doravante denominada simplesmente <strong>FINANCEIRA</strong>,
                com sede à Rua do Carmo, 08, 11º andar, Centro, Rio de Janeiro/RJ, inscrita no CNPJ/MF sob o nº.: 07.652.226/0001-16.
            </td>
        </tr>
        <tr>
            <td style="background-color: #BDBDBD;" colspan="4">
                <strong>QUADRO I – QUALIFICAÇÃO DO EMITENTE:</strong>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <strong>Nome: </strong><?php echo strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome) ?>
            </td>
            <td style="width:400px;">
                <strong>CPF: </strong><?php echo $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero ?>
            </td>
        </tr>
        <tr>
            <td style="width:250px;">
                <strong>RG Nº: </strong><?php echo $proposta->analiseDeCredito->cliente->pessoa->getRG()->numero ?>
            </td>
            <td style="width:250px;">
                <strong >Estado Civil: </strong>
                <?php
                if ($proposta->analiseDeCredito->cliente->pessoa->Estado_Civil_id == 1) {
                    echo 'Solteiro(a)';
                }
                if ($proposta->analiseDeCredito->cliente->pessoa->Estado_Civil_id == 2) {
                    echo 'Casado(a)';
                }
                if ($proposta->analiseDeCredito->cliente->pessoa->Estado_Civil_id == 3) {
                    echo 'Divorciado(a)';
                }
                if ($proposta->analiseDeCredito->cliente->pessoa->Estado_Civil_id == 4) {
                    echo 'Viúvo(a)';
                }
                ?>
            </td>
            <td style="width:80px;">
                <strong>Sexo: </strong><?php echo $proposta->analiseDeCredito->cliente->pessoa->sexo ?>
            </td>
            <td style="width:400px;">
                <strong>Email: </strong><?php echo $proposta->analiseDeCredito->cliente->pessoa->getEmail() ?>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <strong>Endereço Res.: </strong>
                <?php
                echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->logradouro .
                ", nº " . $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->numero
                ?>
            </td>
            <td>
                <strong>Bairro: </strong>
                    <?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->bairro ?>
            </td>
        </tr>
        <tr>
            <td style="width:250px;">
                <strong>Cidade: </strong>
                <?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cidade ?>
            </td>
            <td style="width:250px;">
                <strong >UF: </strong>
                <?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->uf ?>
            </td>
            <td style="width:80px;">
                <strong>CEP: </strong>
                <?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cep ?>
            </td>
            <td style="width:400px;">
                <strong>Telefone: </strong>
                <?php echo $proposta->analiseDeCredito->cliente->pessoa->getTelefone() ?>
            </td>
        </tr>
        <tr>
            <td style="background-color: #BDBDBD;" colspan="4">

                <strong>QUADRO II - ESPECIFICAÇÕES DO CRÉDITO:

                    <i class="fa fa-square"     ></i> Novo  &nbsp&nbsp&nbsp&nbsp&nbsp
                    <i class="fa fa-square-o"   ></i> Refinanciamento Contrato Nº_____________  &nbsp&nbsp&nbsp&nbsp&nbsp
                    <i class="fa fa-square-o"   ></i> Portabilidade

                </strong>

            </td>
        </tr>
    </table>
    <table class="tab_contrato">
        <tr>
            <td style="width:280px">
                <strong>1. Valor Empréstimo: R$ </strong>
                <?php echo number_format($proposta->valor - $proposta->valor_entrada, 2, ',', '.') ?>
            </td>
            <td style="width:200px;">
                <strong>Valor IOF: R$ </strong>
                <?php echo number_format($valorIOF, 2, ',', '.') ?>
            </td>
            <td colspan="2" style="width:250px;">
                <strong>2. Tarifa de Cadastro: R$ </strong>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>3. (1+2+13) Valor Financiado: R$  </strong>
                <?php echo number_format( ($proposta->getValorFinanciado() ), 2, ',', '.') ?>
            </td>
            <td colspan="2">
                <strong>4. Saldo Devedor Refinanciamento: R$ </strong>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>5. (1-2-4-13) Valor Líquido do Crédito: R$ </strong>
                <?php echo number_format($proposta->getValorFinanciado() - $proposta->calcularValorDoSeguro(), 2, ',', '.');?>
            </td>
            <td>
                <strong>6. Valor da Parcela: R$ </strong>
                <?php echo number_format($proposta->getValorParcela(), 2, ',', '.'); ?>
            </td>
            <td>
                <strong>7. Total de Parcelas: </strong>
                <?php echo $proposta->qtd_parcelas ?>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>8. Vencimento da 1ª Parcela: </strong>
                <?php echo $proposta->getDataPrimeiraParcela() ?>
            </td>
            <td colspan="2">
                <strong>9. Taxa de Juros: </strong>
                <?php if($proposta->tabelaCotacao->ModalidadeId == 1){ echo number_format($proposta->tabelaCotacao->taxa, 2, ',', '.'); }else{ echo "0"; }  ?>% ao mês <?php if($proposta->tabelaCotacao->ModalidadeId == 1){ echo number_format($proposta->tabelaCotacao->taxa_anual, 2, ',', '.'); }else{ echo "0"; } ?>% ao ano
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>10. Vencimento da Última Parcela: </strong>
                <?php echo $proposta->getDataUltimaParcela(); ?>
            </td>
            <td colspan="2">
                <strong>11. Custo Efetivo Total - CET:</strong>
                <font class="vjur_fin_font">0
                    <?php //if($proposta->tabelaCotacao->ModalidadeId == 1){ echo number_format(($proposta->getCustoEfetivoTotal(1)*100), 2, ',', '.'); }else{ echo number_format("0", 2, ',', '.'); } ?>
                </font>% ao mês
                <font class="vjuranox_fin_font">0
                    <?php //if($proposta->tabelaCotacao->ModalidadeId == 1){ echo number_format(($proposta->getCustoEfetivoTotal(2)*100), 2, ',', '.'); }else{ echo number_format("0", 2, ',', '.'); } ?>
                </font>% ao ano
            </td>
        </tr>
        <tr>
            <td colspan="1">
                <strong>12. Seguro Nº: </strong> <?php if($proposta->segurada){ echo "1007700000571"; } ?> <!-- lembrar de ajeitar isso-->
            </td>
            <td colspan="1">
                <strong>Seguradora:</strong>
                <font style="font-size: 12px">
                    <?php if($proposta->segurada){ echo "Usebens Seguros S/A"; } ?> <!-- lembrar de ajeitar isso-->
                </font>
            </td>
            <td colspan="2">
                <strong>13. Valor do Prêmio: R$ </strong>
                <?php if($proposta->segurada){ echo number_format($proposta->calcularValorDoSeguro(), 2, ',', '.'); } ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <strong>14. Forma de Pagamento: &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    <i class="fa fa-square-o"   ></i> Débito em Conta  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    <i class="fa fa-square"     ></i> Boleto Bancário &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    <i class="fa fa-square-o"   ></i> Cheque
                </strong>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <strong>15.Cheques Recebidos: Números de &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;Banco: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Agência: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Conta: </strong>
            </td>

        </tr>
        <tr>
            <td colspan="3">
                <strong>16.Vendedor do Bem | Razão Social: </strong>

                    <?php echo $proposta->analiseDeCredito->filial->nucleoFilial->nome; ?>

            </td>
            <td>
                <strong>CNPJ: </strong>
                <?php echo $proposta->analiseDeCredito->filial->cnpj; ?>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>17.Descriçao do bem: </strong>
                <?php foreach ($proposta->analiseDeCredito->listSubgruposDaAnalise() as $item ): ?>
                    <?php echo strtoupper($item->descricao) . " - "; ?>
                <?php endforeach ?>
            </td>
            <td colspan="2">
                <strong>Nº Pedido/Nota Fiscal:</strong>
            </td>
        </tr>
        <tr>
            <td style="background-color: #BDBDBD;" colspan="4">
                <strong>QUADRO III – DADOS DO CORRESPONDENTE</strong>
            </td>
        </tr>
        <tr>
            <td style="padding-top: 10px!important; padding-bottom: 10px!important; border-left: 1px solid!important; border-right: 0px!important;">
                <img src="<?php echo Yii::app()->baseUrl ?> /images/credlogoinner.png" width="180px" />
            </td>
            <td style="padding-top: 10px!important; padding-bottom: 10px!important; border-right: 1px solid!important; border-left: 0px!important;" colspan="3">
                <strong>Razão Social: </strong> CREDSHOW OPERADORA DE CRÉDITO S/A |
                <strong>CNPJ: </strong>13.861.348/0001-15 <br />
                <strong>Endereço: </strong>Av. Amintas Barros, 3700. CTC SL 1904. Lagoa Nova - Natal/RN <br />
                <strong>Contato: </strong>084 2040 0800
            </td>
        </tr>
        <tr>
            <td style="background-color: #BDBDBD;" colspan="2">
                <strong>QUADRO IV – DADOS DA CONTA BANCÁRIA: <i class="fa fa-square-o"></i> Poupança  <i class="fa fa-square-o"></i> C/C</strong>
            </td>
            <td style="background-color: #BDBDBD;" colspan="2">
                <strong>QUADRO V – ENCARGOS MORATÓRIOS</strong>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>18. Banco: &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    Agência: &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    Conta:
                </strong>
            </td>
            <td colspan="2">
                <strong>19.Multa: - &nbsp&nbsp&nbsp&nbsp 20. Juros Remuneratórios:</strong> <?php echo ConfigLN::model()->valorDoParametro("taxa_mora") ?>% <strong>ao dia.</strong>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <p>
                    <strong>1.</strong> .Emito a presente Cédula de Crédito Bancário (“Cédula”), como título representativo do crédito concedido pelo <strong>CREDOR</strong>,
                    indicado no item 1, do Quadro II acima, e prometo pagar ao <strong>CREDOR</strong>, ou a sua ordem, o valor ora contratado, na forma e
                    prazos previstos nesta Cédula, autorizando o <strong>CREDOR</strong> a efetuar o pagamento do “Valor Líquido do Crédito”, descrito no item
                    5. do Quadro II: (i) CDC – diretamente ao Vendedor do(s) Bem(ns) financiado(s) conforme item 16, ou (ii) Crédito Pessoal –
                    na conta corrente discriminada no item 18 do Quadro IV, de minha titularidade.
                </p>
                <p>
                    <strong>2.</strong> Tenho ciência que as parcelas deverão ser pagas na forma definida no item 14 e na data constante no item 8, até a data de vencimento da última parcela conforme item 10, do Quadro II.
                </p>
                <p>
                    <strong>3.</strong>  Na hipótese de empréstimo/financiamento na modalidade CDC, declaro ter pleno conhecimento que é de responsabilidade
                    exclusiva do Vendedor do Bem, o atendimento a qualquer assunto relacionado ao Bem Financiado, ficando o CREDOR
                    totalmente isento de qualquer responsabilidade, subsistindo, portanto, o meu dever em liquidar o financiamento conforme
                    prometido nesta Cédula, em qualquer circunstância.
                </p>
                <p>
                    <strong>4.</strong>  Caso eu tenha, por livre e espontânea vontade, contratado o seguro de proteção financeira descrito e caracterizado no
                    item 12 do Quadro II, estou ciente e desde já concordo, que o valor do prêmio será descontado do valor financiado, conforme
                    item 3 do Quadro II, e na hipótese de sinistro, a indenização será utilizada para amortizar ou liquidar, conforme o caso, a
                    presente Cédula, prevalecendo o meu dever em quitar eventual saldo ainda existente.
                </p>
                <p>
                    <strong>5.</strong>  Declaro que, previamente à emissão da presente Cédula, tomei ciência dos fluxos que compõem o Custo Efetivo Total
                    ("CET"), e que a taxa percentual anual representa a condição vigente na data do cálculo.
                </p>
                <p>
                    <strong>6.</strong>  Ao emitir a presente Cédula, <strong>declaro estar de acordo com as disposições contidas nas Condições Gerais da Cédula
                        de Crédito Bancário</strong>, registradas no 06º Cartório de Registro de Títulos e Documentos do Rio de Janeiro, em 08/01/2016,
                    sob o n.º 1333847, <strong>cuja cópia me foi entregue antes de firmar o presente Contrato</strong>.
                </p>
                <br />
                <p style="text-align: center">
                    <?php echo $proposta->analiseDeCredito->filial->getEndereco()->cidade . '/' . $proposta->analiseDeCredito->filial->getEndereco()->uf ?>, <?php echo date("d");?> de <?php echo $arrMeses[Date('m')]; ?> de <?php echo date("Y"); ?>
                    __________________________________________
                </p>
                <p style="text-align: center">
                    <strong>EMITENTE</strong>
                </p>
            </td>
        </tr>
        <tr>
            <td style="background-color: #BDBDBD;" colspan="4">
                <strong>DECLARAÇÃO SE ANALFABETO OU IMPEDIDO DE ASSINAR</strong>
            </td>
        </tr>
    </table>
    <table class="tab_contrato">
        <tr>
            <td style="width:800px">
                <p>
                    Declaro que ouvi atentamente a leitura desta Cédula, na presença das testemunhas abaixo qualificadas,
                    estando ciente e de acordo com as condições e obrigações que assumi na presente operação.
                </p>
                <p>
                    Assinatura a rogo do Eminente: ____________________________________________
                </p>
                <p style="margin-left:195px; margin-top:-10px"> Nome: </p>
                <p style="margin-left:195px; margin-top:-10px"> CPF/MF: </p>

                <p>
                    Testemunhas: <br /><br />
                    1. ________________________________________ &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    2. ________________________________________ <br />

                    &nbsp&nbsp&nbsp Nome:
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

                    Nome:
                </p>
                <p style="margin-top:-10px;">
                    &nbsp&nbsp&nbsp CPF:
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

                    CPF:
                </p>
            </td>
            <td style="text-align: center;">
                <p style="margin-left: 30px;">Impressao Digital</p>
                <div style="width:130px; height:160px; border: 2px solid; margin-left:17px;">
                </div>
            </td>
        </tr>
    </table>
    <p style="margin: 0 auto;">Reclamações OUVIDORIA – 0800 709 9944 | e-mail: ouvidoria@lecca.com.br | www.lecca.com.br</p>
    <div class="codigodebarras"></div>
</div>

<br>
<br>
<br>
<br>

<div id="main-center" class="viaNaoNegociavel"
     style="padding-top:0!important;margin-top:0!important; background: url('<?php echo $imagem ?>')!important; background-repeat: no-repeat!important; background-position: center!important; background-size: 100%!important; ">

    <header style="padding-top:0!important;margin-top:0!important;">
        <div style="float:left; margin-left:20px;">
            <img src="<?php echo Yii::app()->baseUrl ?> /images/lecca_logo.png" width="100px"/>
        </div>
        <div style="float:right; margin-right:20px;">
            <h3 style="margin-top:0!important;">CÉDULA DE CRÉDITO BANCÁRIO</h3>
            <h3 style="float:right; margin-top:-20px;">Via não Negociável</h3>
        </div>
    </header>

    <table class="tab_contrato">
        <tr>
            <td colspan="4">
                <span><i class="fa fa-square-o"></i> <strong>Crédito Pessoal</strong></span>
                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

                <span><i class="fa fa-square"></i> <strong>Crédito Direto ao Consumidor</strong></span>
                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

                <span><strong>CCB Nº: <?php echo $proposta->codigo ?></strong></span>
            </td>
        </tr>
        <tr>
            <td style="background-color: #BDBDBD;" colspan="4">
                <strong>CREDOR:</strong>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <strong>LECCA CRÉDITO FINANCIAMENTO E INVESTIMENTO S/A</strong>, doravante denominada simplesmente <strong>FINANCEIRA</strong>,
                com sede à Rua do Carmo, 08, 11º andar, Centro, Rio de Janeiro/RJ, inscrita no CNPJ/MF sob o nº.: 07.652.226/0001-16.
            </td>
        </tr>
        <tr>
            <td style="background-color: #BDBDBD;" colspan="4">
                <strong>QUADRO I – QUALIFICAÇÃO DO EMITENTE:</strong>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <strong>Nome: </strong><?php echo strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome) ?>
            </td>
            <td style="width:400px;">
                <strong>CPF: </strong><?php echo $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero ?>
            </td>
        </tr>
        <tr>
            <td style="width:250px;">
                <strong>RG Nº: </strong><?php echo $proposta->analiseDeCredito->cliente->pessoa->getRG()->numero ?>
            </td>
            <td style="width:250px;">
                <strong >Estado Civil: </strong>
                <?php
                if ($proposta->analiseDeCredito->cliente->pessoa->Estado_Civil_id == 1) {
                    echo 'Solteiro(a)';
                }
                if ($proposta->analiseDeCredito->cliente->pessoa->Estado_Civil_id == 2) {
                    echo 'Casado(a)';
                }
                if ($proposta->analiseDeCredito->cliente->pessoa->Estado_Civil_id == 3) {
                    echo 'Divorciado(a)';
                }
                if ($proposta->analiseDeCredito->cliente->pessoa->Estado_Civil_id == 4) {
                    echo 'Viúvo(a)';
                }
                ?>
            </td>
            <td style="width:80px;">
                <strong>Sexo: </strong><?php echo $proposta->analiseDeCredito->cliente->pessoa->sexo ?>
            </td>
            <td style="width:400px;">
                <strong>Email: </strong><?php echo $proposta->analiseDeCredito->cliente->pessoa->getEmail() ?>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <strong>Endereço Res.: </strong>
                <?php
                echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->logradouro .
                ", nº " . $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->numero
                ?>
            </td>
            <td>
                <strong>Bairro: </strong>
                    <?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->bairro ?>
            </td>
        </tr>
        <tr>
            <td style="width:250px;">
                <strong>Cidade: </strong>
                <?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cidade ?>
            </td>
            <td style="width:250px;">
                <strong >UF: </strong>
                <?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->uf ?>
            </td>
            <td style="width:80px;">
                <strong>CEP: </strong>
                <?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cep ?>
            </td>
            <td style="width:400px;">
                <strong>Telefone: </strong>
                <?php echo $proposta->analiseDeCredito->cliente->pessoa->getTelefone() ?>
            </td>
        </tr>
        <tr>
            <td style="background-color: #BDBDBD;" colspan="4">

                <strong>QUADRO II - ESPECIFICAÇÕES DO CRÉDITO:

                    <i class="fa fa-square"     ></i> Novo  &nbsp&nbsp&nbsp&nbsp&nbsp
                    <i class="fa fa-square-o"   ></i> Refinanciamento Contrato Nº_____________  &nbsp&nbsp&nbsp&nbsp&nbsp
                    <i class="fa fa-square-o"   ></i> Portabilidade

                </strong>

            </td>
        </tr>
    </table>
    <table class="tab_contrato">
        <tr>
            <td style="width:280px">
                <strong>1. Valor Empréstimo: R$ </strong>
                <?php echo number_format($proposta->valor - $proposta->valor_entrada, 2, ',', '.') ?>
            </td>
            <td style="width:200px;">
                <strong>Valor IOF: R$ </strong>
                <?php echo number_format($valorIOF, 2, ',', '.') ?>
            </td>
            <td colspan="2" style="width:250px;">
                <strong>2. Tarifa de Cadastro: R$ </strong>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>3. (1+2+13) Valor Financiado: R$  </strong>
                <?php echo number_format( ($proposta->getValorFinanciado()), 2, ',', '.') ?>
            </td>
            <td colspan="2">
                <strong>4. Saldo Devedor Refinanciamento: R$ </strong>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>5. (1-2-4-13) Valor Líquido do Crédito: R$ </strong>
                <?php echo number_format($proposta->getValorFinanciado() - $proposta->calcularValorDoSeguro(), 2, ',', '.');?>
            </td>
            <td>
                <strong>6. Valor da Parcela: R$ </strong>
                <?php echo number_format($proposta->getValorParcela(), 2, ',', '.'); ?>
            </td>
            <td>
                <strong>7. Total de Parcelas: </strong>
                <?php echo $proposta->qtd_parcelas ?>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>8. Vencimento da 1ª Parcela: </strong>
                <?php echo $proposta->getDataPrimeiraParcela() ?>
            </td>
            <td colspan="2">
                <strong>9. Taxa de Juros: </strong>
                <?php if($proposta->tabelaCotacao->ModalidadeId == 1){ echo number_format($proposta->tabelaCotacao->taxa, 2, ',', '.'); }else{ echo "0"; }  ?>% ao mês <?php if($proposta->tabelaCotacao->ModalidadeId == 1){ echo number_format($proposta->tabelaCotacao->taxa_anual, 2, ',', '.'); }else{ echo "0"; } ?>% ao ano
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>10. Vencimento da Última Parcela: </strong>
                <?php echo $proposta->getDataUltimaParcela(); ?>
            </td>
            <td colspan="2">
                <strong>11. Custo Efetivo Total - CET:</strong>
                <font class="vjur_fin_font">0
                    <?php //if($proposta->tabelaCotacao->ModalidadeId == 1){ echo number_format(($proposta->getCustoEfetivoTotal(1)*100), 2, ',', '.'); }else{ echo number_format("0", 2, ',', '.'); } ?>
                </font>% ao mês
                <font class="vjuranox_fin_font">0
                    <?php //if($proposta->tabelaCotacao->ModalidadeId == 1){ echo number_format(($proposta->getCustoEfetivoTotal(2)*100), 2, ',', '.'); }else{ echo number_format("0", 2, ',', '.'); } ?>
                </font>% ao ano
            </td>
        </tr>
        <tr>
            <td colspan="1">
                <strong>12. Seguro Nº: </strong> <?php if($proposta->segurada){ echo "1007700000571"; } ?> <!-- lembrar de ajeitar isso-->
            </td>
            <td colspan="1">
                <strong>Seguradora:</strong>
                <font style="font-size: 12px">
                    <?php if($proposta->segurada){ echo "Usebens Seguros S/A"; } ?> <!-- lembrar de ajeitar isso-->
                </font>
            </td>
            <td colspan="2">
                <strong>13. Valor do Prêmio: R$ </strong>
                <?php if($proposta->segurada){ echo number_format($proposta->calcularValorDoSeguro(), 2, ',', '.'); } ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <strong>14. Forma de Pagamento: &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    <i class="fa fa-square-o"   ></i> Débito em Conta  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    <i class="fa fa-square"     ></i> Boleto Bancário &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    <i class="fa fa-square-o"   ></i> Cheque
                </strong>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <strong>15.Cheques Recebidos: Números de &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;Banco: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Agência: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Conta: </strong>
            </td>

        </tr>
        <tr>
            <td colspan="3">
                <strong>16.Vendedor do Bem | Razão Social: </strong>

                    <?php echo $proposta->analiseDeCredito->filial->nucleoFilial->nome; ?>

            </td>
            <td>
                <strong>CNPJ: </strong>
                <?php echo $proposta->analiseDeCredito->filial->cnpj; ?>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>17.Descriçao do bem: </strong>
                <?php foreach ($proposta->analiseDeCredito->listSubgruposDaAnalise() as $item ): ?>
                    <?php echo strtoupper($item->descricao) . " - "; ?>
                <?php endforeach ?>
            </td>
            <td colspan="2">
                <strong>Nº Pedido/Nota Fiscal:</strong>
            </td>
        </tr>
        <tr>
            <td style="background-color: #BDBDBD;" colspan="4">
                <strong>QUADRO III – DADOS DO CORRESPONDENTE</strong>
            </td>
        </tr>
        <tr>
            <td style="padding-top: 10px!important; padding-bottom: 10px!important; border-left: 1px solid!important; border-right: 0px!important;">
                <img src="<?php echo Yii::app()->baseUrl ?> /images/credlogoinner.png" width="180px" />
            </td>
            <td style="padding-top: 10px!important; padding-bottom: 10px!important; border-right: 1px solid!important; border-left: 0px!important;" colspan="3">
                <strong>Razão Social: </strong> CREDSHOW OPERADORA DE CRÉDITO S/A |
                <strong>CNPJ: </strong>13.861.348/0001-15 <br />
                <strong>Endereço: </strong>Av. Amintas Barros, 3700. CTC SL 1904. Lagoa Nova - Natal/RN <br />
                <strong>Contato: </strong>084 2040 0800
            </td>
        </tr>
        <tr>
            <td style="background-color: #BDBDBD;" colspan="2">
                <strong>QUADRO IV – DADOS DA CONTA BANCÁRIA: <i class="fa fa-square-o"></i> Poupança  <i class="fa fa-square-o"></i> C/C</strong>
            </td>
            <td style="background-color: #BDBDBD;" colspan="2">
                <strong>QUADRO V – ENCARGOS MORATÓRIOS</strong>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>18. Banco: &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    Agência: &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    Conta:
                </strong>
            </td>
            <td colspan="2">
                <strong>19.Multa: - &nbsp&nbsp&nbsp&nbsp 20. Juros Remuneratórios:</strong> <?php echo ConfigLN::model()->valorDoParametro("taxa_mora") ?>% <strong>ao dia.</strong>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <p>
                    <strong>1.</strong> .Emito a presente Cédula de Crédito Bancário (“Cédula”), como título representativo do crédito concedido pelo <strong>CREDOR</strong>,
                    indicado no item 1, do Quadro II acima, e prometo pagar ao <strong>CREDOR</strong>, ou a sua ordem, o valor ora contratado, na forma e
                    prazos previstos nesta Cédula, autorizando o <strong>CREDOR</strong> a efetuar o pagamento do “Valor Líquido do Crédito”, descrito no item
                    5. do Quadro II: (i) CDC – diretamente ao Vendedor do(s) Bem(ns) financiado(s) conforme item 16, ou (ii) Crédito Pessoal –
                    na conta corrente discriminada no item 18 do Quadro IV, de minha titularidade.
                </p>
                <p>
                    <strong>2.</strong> Tenho ciência que as parcelas deverão ser pagas na forma definida no item 14 e na data constante no item 8, até a data de vencimento da última parcela conforme item 10, do Quadro II.
                </p>
                <p>
                    <strong>3.</strong>  Na hipótese de empréstimo/financiamento na modalidade CDC, declaro ter pleno conhecimento que é de responsabilidade
                    exclusiva do Vendedor do Bem, o atendimento a qualquer assunto relacionado ao Bem Financiado, ficando o CREDOR
                    totalmente isento de qualquer responsabilidade, subsistindo, portanto, o meu dever em liquidar o financiamento conforme
                    prometido nesta Cédula, em qualquer circunstância.
                </p>
                <p>
                    <strong>4.</strong>  Caso eu tenha, por livre e espontânea vontade, contratado o seguro de proteção financeira descrito e caracterizado no
                    item 12 do Quadro II, estou ciente e desde já concordo, que o valor do prêmio será descontado do valor financiado, conforme
                    item 3 do Quadro II, e na hipótese de sinistro, a indenização será utilizada para amortizar ou liquidar, conforme o caso, a
                    presente Cédula, prevalecendo o meu dever em quitar eventual saldo ainda existente.
                </p>
                <p>
                    <strong>5.</strong>  Declaro que, previamente à emissão da presente Cédula, tomei ciência dos fluxos que compõem o Custo Efetivo Total
                    ("CET"), e que a taxa percentual anual representa a condição vigente na data do cálculo.
                </p>
                <p>
                    <strong>6.</strong>  Ao emitir a presente Cédula, <strong>declaro estar de acordo com as disposições contidas nas Condições Gerais da Cédula
                        de Crédito Bancário</strong>, registradas no 06º Cartório de Registro de Títulos e Documentos do Rio de Janeiro, em 08/01/2016,
                    sob o n.º 1333847, <strong>cuja cópia me foi entregue antes de firmar o presente Contrato</strong>.
                </p>
                <br />
                <p style="text-align: center">
                    <?php echo $proposta->analiseDeCredito->filial->getEndereco()->cidade . '/' . $proposta->analiseDeCredito->filial->getEndereco()->uf ?>, <?php echo date("d");?> de <?php echo $arrMeses[Date('m')]; ?> de <?php echo date("Y"); ?>
                    __________________________________________
                </p>
                <p style="text-align: center">
                    <strong>EMITENTE</strong>
                </p>
            </td>
        </tr>
        <tr>
            <td style="background-color: #BDBDBD;" colspan="4">
                <strong>DECLARAÇÃO SE ANALFABETO OU IMPEDIDO DE ASSINAR</strong>
            </td>
        </tr>
    </table>
    <table class="tab_contrato">
        <tr>
            <td style="width:800px">
                <p>
                    Declaro que ouvi atentamente a leitura desta Cédula, na presença das testemunhas abaixo qualificadas,
                    estando ciente e de acordo com as condições e obrigações que assumi na presente operação.
                </p>
                <p>
                    Assinatura a rogo do Eminente: ____________________________________________
                </p>
                <p style="margin-left:195px; margin-top:-10px"> Nome: </p>
                <p style="margin-left:195px; margin-top:-10px"> CPF/MF: </p>

                <p>
                    Testemunhas: <br /><br />
                    1. ________________________________________ &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    2. ________________________________________ <br />

                    &nbsp&nbsp&nbsp Nome:
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

                    Nome:
                </p>
                <p style="margin-top:-10px;">
                    &nbsp&nbsp&nbsp CPF:
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

                    CPF:
                </p>
            </td>
            <td style="text-align: center;">
                <p style="margin-left: 30px;">Impressao Digital</p>
                <div style="width:130px; height:160px; border: 2px solid; margin-left:17px;">
                </div>
            </td>
        </tr>
    </table>
    <p style="margin: 0 auto;">Reclamações OUVIDORIA – 0800 709 9944 | e-mail: ouvidoria@lecca.com.br | www.lecca.com.br</p>
    <!--<div class="codigodebarras"></div>-->
</div>

<br>
<br>
<br>
<br>
<br>
<br>
<br>

<div id="main-center" class="viaNaoNegociavel"
     style="padding-top:0!important;margin-top:0!important; background: url('<?php echo $imagem ?>')!important; background-repeat: no-repeat!important; background-position: center!important; background-size: 100%!important; ">
    <header style="padding-top:0!important;margin-top:0!important;">
        <div style="float:left; margin-left:20px;">
            <img src="<?php echo Yii::app()->baseUrl ?> /images/lecca_logo.png" width="100px"/>
        </div>
        <div style="float:right; margin-right:20px;">
            <h3 style="margin-top:0!important;">CÉDULA DE CRÉDITO BANCÁRIO</h3>
            <h3 style="float:right; margin-top:-20px;">Via não Negociável</h3>
        </div>
    </header>

    <table class="tab_contrato">
        <tr>
            <td colspan="4">
                <span><i class="fa fa-square-o"></i> <strong>Crédito Pessoal</strong></span>
                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

                <span><i class="fa fa-square"></i> <strong>Crédito Direto ao Consumidor</strong></span>
                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

                <span><strong>CCB Nº: <?php echo $proposta->codigo ?></strong></span>
            </td>
        </tr>
        <tr>
            <td style="background-color: #BDBDBD;" colspan="4">
                <strong>CREDOR:</strong>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <strong>LECCA CRÉDITO FINANCIAMENTO E INVESTIMENTO S/A</strong>, doravante denominada simplesmente <strong>FINANCEIRA</strong>,
                com sede à Rua do Carmo, 08, 11º andar, Centro, Rio de Janeiro/RJ, inscrita no CNPJ/MF sob o nº.: 07.652.226/0001-16.
            </td>
        </tr>
        <tr>
            <td style="background-color: #BDBDBD;" colspan="4">
                <strong>QUADRO I – QUALIFICAÇÃO DO EMITENTE:</strong>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <strong>Nome: </strong><?php echo strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome) ?>
            </td>
            <td style="width:400px;">
                <strong>CPF: </strong><?php echo $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero ?>
            </td>
        </tr>
        <tr>
            <td style="width:250px;">
                <strong>RG Nº: </strong><?php echo $proposta->analiseDeCredito->cliente->pessoa->getRG()->numero ?>
            </td>
            <td style="width:250px;">
                <strong >Estado Civil: </strong>
                <?php
                if ($proposta->analiseDeCredito->cliente->pessoa->Estado_Civil_id == 1) {
                    echo 'Solteiro(a)';
                }
                if ($proposta->analiseDeCredito->cliente->pessoa->Estado_Civil_id == 2) {
                    echo 'Casado(a)';
                }
                if ($proposta->analiseDeCredito->cliente->pessoa->Estado_Civil_id == 3) {
                    echo 'Divorciado(a)';
                }
                if ($proposta->analiseDeCredito->cliente->pessoa->Estado_Civil_id == 4) {
                    echo 'Viúvo(a)';
                }
                ?>
            </td>
            <td style="width:80px;">
                <strong>Sexo: </strong><?php echo $proposta->analiseDeCredito->cliente->pessoa->sexo ?>
            </td>
            <td style="width:400px;">
                <strong>Email: </strong><?php echo $proposta->analiseDeCredito->cliente->pessoa->getEmail() ?>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <strong>Endereço Res.: </strong>
                <?php
                echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->logradouro .
                ", nº " . $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->numero
                ?>
            </td>
            <td>
                <strong>Bairro: </strong>
                    <?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->bairro ?>
            </td>
        </tr>
        <tr>
            <td style="width:250px;">
                <strong>Cidade: </strong>
                <?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cidade ?>
            </td>
            <td style="width:250px;">
                <strong >UF: </strong>
                <?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->uf ?>
            </td>
            <td style="width:80px;">
                <strong>CEP: </strong>
                <?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cep ?>
            </td>
            <td style="width:400px;">
                <strong>Telefone: </strong>
                <?php echo $proposta->analiseDeCredito->cliente->pessoa->getTelefone() ?>
            </td>
        </tr>
        <tr>
            <td style="background-color: #BDBDBD;" colspan="4">

                <strong>QUADRO II - ESPECIFICAÇÕES DO CRÉDITO:

                    <i class="fa fa-square"     ></i> Novo  &nbsp&nbsp&nbsp&nbsp&nbsp
                    <i class="fa fa-square-o"   ></i> Refinanciamento Contrato Nº_____________  &nbsp&nbsp&nbsp&nbsp&nbsp
                    <i class="fa fa-square-o"   ></i> Portabilidade

                </strong>

            </td>
        </tr>
    </table>
    <table class="tab_contrato">
        <tr>
            <td style="width:280px">
                <strong>1. Valor Empréstimo: R$ </strong>
                <?php echo number_format($proposta->valor - $proposta->valor_entrada, 2, ',', '.') ?>
            </td>
            <td style="width:200px;">
                <strong>Valor IOF: R$ </strong>
                <?php echo number_format($valorIOF, 2, ',', '.') ?>
            </td>
            <td colspan="2" style="width:250px;">
                <strong>2. Tarifa de Cadastro: R$ </strong>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>3. (1+2+13) Valor Financiado: R$  </strong>
                <?php echo number_format( ($proposta->getValorFinanciado()), 2, ',', '.') ?>
            </td>
            <td colspan="2">
                <strong>4. Saldo Devedor Refinanciamento: R$ </strong>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>5. (1-2-4-13) Valor Líquido do Crédito: R$ </strong>
                <?php echo number_format($proposta->getValorFinanciado() - $proposta->calcularValorDoSeguro(), 2, ',', '.');?>
            </td>
            <td>
                <strong>6. Valor da Parcela: R$ </strong>
                <?php echo number_format($proposta->getValorParcela(), 2, ',', '.'); ?>
            </td>
            <td>
                <strong>7. Total de Parcelas: </strong>
                <?php echo $proposta->qtd_parcelas ?>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>8. Vencimento da 1ª Parcela: </strong>
                <?php echo $proposta->getDataPrimeiraParcela() ?>
            </td>
            <td colspan="2">
                <strong>9. Taxa de Juros: </strong>
                <?php if($proposta->tabelaCotacao->ModalidadeId == 1){ echo number_format($proposta->tabelaCotacao->taxa, 2, ',', '.'); }else{ echo "0"; }  ?>% ao mês <?php if($proposta->tabelaCotacao->ModalidadeId == 1){ echo number_format($proposta->tabelaCotacao->taxa_anual, 2, ',', '.'); }else{ echo "0"; } ?>% ao ano
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>10. Vencimento da Última Parcela: </strong>
                <?php echo $proposta->getDataUltimaParcela(); ?>
            </td>
            <td colspan="2">
                <strong>11. Custo Efetivo Total - CET:</strong>
                <font class="vjur_fin_font">0
                    <?php //if($proposta->tabelaCotacao->ModalidadeId == 1){ echo number_format(($proposta->getCustoEfetivoTotal(1)*100), 2, ',', '.'); }else{ echo number_format("0", 2, ',', '.'); } ?>
                </font>% ao mês
                <font class="vjuranox_fin_font">0
                    <?php //if($proposta->tabelaCotacao->ModalidadeId == 1){ echo number_format(($proposta->getCustoEfetivoTotal(2)*100), 2, ',', '.'); }else{ echo number_format("0", 2, ',', '.'); } ?>
                </font>% ao ano
            </td>
        </tr>
        <tr>
            <td colspan="1">
                <strong>12. Seguro Nº: </strong> <?php if($proposta->segurada){ echo "1007700000571"; } ?> <!-- lembrar de ajeitar isso-->
            </td>
            <td colspan="1">
                <strong>Seguradora:</strong>
                <font style="font-size: 12px">
                    <?php if($proposta->segurada){ echo "Usebens Seguros S/A"; } ?> <!-- lembrar de ajeitar isso-->
                </font>
            </td>
            <td colspan="2">
                <strong>13. Valor do Prêmio: R$ </strong>
                <?php if($proposta->segurada){ echo number_format($proposta->calcularValorDoSeguro(), 2, ',', '.'); } ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <strong>14. Forma de Pagamento: &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    <i class="fa fa-square-o"   ></i> Débito em Conta  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    <i class="fa fa-square"     ></i> Boleto Bancário &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    <i class="fa fa-square-o"   ></i> Cheque
                </strong>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <strong>15.Cheques Recebidos: Números de &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;Banco: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Agência: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Conta: </strong>
            </td>

        </tr>
        <tr>
            <td colspan="3">
                <strong>16.Vendedor do Bem | Razão Social: </strong>

                    <?php echo $proposta->analiseDeCredito->filial->nucleoFilial->nome; ?>

            </td>
            <td>
                <strong>CNPJ: </strong>
                <?php echo $proposta->analiseDeCredito->filial->cnpj; ?>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>17.Descriçao do bem: </strong>
                <?php foreach ($proposta->analiseDeCredito->listSubgruposDaAnalise() as $item ): ?>
                    <?php echo strtoupper($item->descricao) . " - "; ?>
                <?php endforeach ?>
            </td>
            <td colspan="2">
                <strong>Nº Pedido/Nota Fiscal:</strong>
            </td>
        </tr>
        <tr>
            <td style="background-color: #BDBDBD;" colspan="4">
                <strong>QUADRO III – DADOS DO CORRESPONDENTE</strong>
            </td>
        </tr>
        <tr>
            <td style="padding-top: 10px!important; padding-bottom: 10px!important; border-left: 1px solid!important; border-right: 0px!important;">
                <img src="<?php echo Yii::app()->baseUrl ?> /images/credlogoinner.png" width="180px" />
            </td>
            <td style="padding-top: 10px!important; padding-bottom: 10px!important; border-right: 1px solid!important; border-left: 0px!important;" colspan="3">
                <strong>Razão Social: </strong> CREDSHOW OPERADORA DE CRÉDITO S/A |
                <strong>CNPJ: </strong>13.861.348/0001-15 <br />
                <strong>Endereço: </strong>Av. Amintas Barros, 3700. CTC SL 1904. Lagoa Nova - Natal/RN <br />
                <strong>Contato: </strong>084 2040 0800
            </td>
        </tr>
        <tr>
            <td style="background-color: #BDBDBD;" colspan="2">
                <strong>QUADRO IV – DADOS DA CONTA BANCÁRIA: <i class="fa fa-square-o"></i> Poupança  <i class="fa fa-square-o"></i> C/C</strong>
            </td>
            <td style="background-color: #BDBDBD;" colspan="2">
                <strong>QUADRO V – ENCARGOS MORATÓRIOS</strong>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>18. Banco: &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    Agência: &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    Conta:
                </strong>
            </td>
            <td colspan="2">
                <strong>19.Multa: - &nbsp&nbsp&nbsp&nbsp 20. Juros Remuneratórios:</strong> <?php echo ConfigLN::model()->valorDoParametro("taxa_mora") ?>% <strong>ao dia.</strong>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <p>
                    <strong>1.</strong> .Emito a presente Cédula de Crédito Bancário (“Cédula”), como título representativo do crédito concedido pelo <strong>CREDOR</strong>,
                    indicado no item 1, do Quadro II acima, e prometo pagar ao <strong>CREDOR</strong>, ou a sua ordem, o valor ora contratado, na forma e
                    prazos previstos nesta Cédula, autorizando o <strong>CREDOR</strong> a efetuar o pagamento do “Valor Líquido do Crédito”, descrito no item
                    5. do Quadro II: (i) CDC – diretamente ao Vendedor do(s) Bem(ns) financiado(s) conforme item 16, ou (ii) Crédito Pessoal –
                    na conta corrente discriminada no item 18 do Quadro IV, de minha titularidade.
                </p>
                <p>
                    <strong>2.</strong> Tenho ciência que as parcelas deverão ser pagas na forma definida no item 14 e na data constante no item 8, até a data de vencimento da última parcela conforme item 10, do Quadro II.
                </p>
                <p>
                    <strong>3.</strong>  Na hipótese de empréstimo/financiamento na modalidade CDC, declaro ter pleno conhecimento que é de responsabilidade
                    exclusiva do Vendedor do Bem, o atendimento a qualquer assunto relacionado ao Bem Financiado, ficando o CREDOR
                    totalmente isento de qualquer responsabilidade, subsistindo, portanto, o meu dever em liquidar o financiamento conforme
                    prometido nesta Cédula, em qualquer circunstância.
                </p>
                <p>
                    <strong>4.</strong>  Caso eu tenha, por livre e espontânea vontade, contratado o seguro de proteção financeira descrito e caracterizado no
                    item 12 do Quadro II, estou ciente e desde já concordo, que o valor do prêmio será descontado do valor financiado, conforme
                    item 3 do Quadro II, e na hipótese de sinistro, a indenização será utilizada para amortizar ou liquidar, conforme o caso, a
                    presente Cédula, prevalecendo o meu dever em quitar eventual saldo ainda existente.
                </p>
                <p>
                    <strong>5.</strong>  Declaro que, previamente à emissão da presente Cédula, tomei ciência dos fluxos que compõem o Custo Efetivo Total
                    ("CET"), e que a taxa percentual anual representa a condição vigente na data do cálculo.
                </p>
                <p>
                    <strong>6.</strong>  Ao emitir a presente Cédula, <strong>declaro estar de acordo com as disposições contidas nas Condições Gerais da Cédula
                        de Crédito Bancário</strong>, registradas no 06º Cartório de Registro de Títulos e Documentos do Rio de Janeiro, em 08/01/2016,
                    sob o n.º 1333847, <strong>cuja cópia me foi entregue antes de firmar o presente Contrato</strong>.
                </p>
                <br />
                <p style="text-align: center">
                    <?php echo $proposta->analiseDeCredito->filial->getEndereco()->cidade . '/' . $proposta->analiseDeCredito->filial->getEndereco()->uf ?>, <?php echo date("d");?> de <?php echo $arrMeses[Date('m')]; ?> de <?php echo date("Y"); ?>
                    __________________________________________
                </p>
                <p style="text-align: center">
                    <strong>EMITENTE</strong>
                </p>
            </td>
        </tr>
        <tr>
            <td style="background-color: #BDBDBD;" colspan="4">
                <strong>DECLARAÇÃO SE ANALFABETO OU IMPEDIDO DE ASSINAR</strong>
            </td>
        </tr>
    </table>
    <table class="tab_contrato">
        <tr>
            <td style="width:800px">
                <p>
                    Declaro que ouvi atentamente a leitura desta Cédula, na presença das testemunhas abaixo qualificadas,
                    estando ciente e de acordo com as condições e obrigações que assumi na presente operação.
                </p>
                <p>
                    Assinatura a rogo do Eminente: ____________________________________________
                </p>
                <p style="margin-left:195px; margin-top:-10px"> Nome: </p>
                <p style="margin-left:195px; margin-top:-10px"> CPF/MF: </p>

                <p>
                    Testemunhas: <br /><br />
                    1. ________________________________________ &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    2. ________________________________________ <br />

                    &nbsp&nbsp&nbsp Nome:
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

                    Nome:
                </p>
                <p style="margin-top:-10px;">
                    &nbsp&nbsp&nbsp CPF:
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp

                    CPF:
                </p>
            </td>
            <td style="text-align: center;">
                <p style="margin-left: 30px;">Impressao Digital</p>
                <div style="width:130px; height:160px; border: 2px solid; margin-left:17px;">
                </div>
            </td>
        </tr>
    </table>
    <p style="margin: 0 auto;">Reclamações OUVIDORIA – 0800 709 9944 | e-mail: ouvidoria@lecca.com.br | www.lecca.com.br</p>
    <!--<div class="codigodebarras"></div>-->
    <!--<div class="codigodebarras"></div>-->
</div>

<script>
    $(function () {

        $(".codigodebarras").barcode($('#proposta_codigo').val(), "code128", {
            'barWidth': 2, 'barHeight': 50, 'fontSize': 14
        });
    })
</script>

<style type="text/css">

    .codigodebarras{
        margin: 0 auto;
    }

    .tab_contrato{
        width: 1000px;
        border-collapse: collapse;
    }

    .tab_contrato tr td {
        border:1px solid;
    }

    td {
        padding-left:8px!important;
        padding-right:8px!important;
    }

    p {
        text-align: justify;
    }

    @media all {
        .page-break { display: none; }
    }

    @media print {
        .page-break { display: block; page-break-before: always; }
    }

    @media print {
        * {-webkit-print-color-adjust:exact;}
    }
</style>
