<?php
$nomeSeguradora = "CHUBB SEGUROS";
$numeroApolice = "8.070.008";

if ($proposta->venda !== NULL) {
    if ($proposta->venda->getProdutoSeguro() !== NULL) {
        $numeroApolice = $proposta->venda->getProdutoSeguro()->itemDoEstoque->getNumeroApolice();

        if ($proposta->venda->getProdutoSeguro()->itemDoEstoque->getFinanceira() !== NULL) {
            $nomeSeguradora = $proposta->venda->getProdutoSeguro()->itemDoEstoque->getFinanceira()->financeira->nome;
        }
    }
}

if ($proposta->Financeira_id == 10) {

    $nome_fantasia = "CREDSHOW FUNDO INVESTIMENTO DIREITOS CREDITORIOS LP";
    $cnpj = "23.273.905/0001-30";
    $endereco = "Av. Brigadeiro Faria de Lima, 1355, Andar 3, Jardim Paulistano - São Paulo/SP";
    $logom = "cred_fidc_logo.png";
    $iof = 0;
} else {

    $nome_fantasia = "CREDSHOW OPERADORA DE CREDITO S/A";
    $cnpj = "13.861.348/0001-15";
    $endereco = "Av. Amintas Barros, 3700. CTC SL 1904. Lagoa Nova - Natal/RN";
    $logom = "logocredshow.png";
    $iof = $proposta->tabelaCotacao->taxa;
}

$util = new Util;

$parcelasLeft = array();
$parcelasRight = array();

$arrMeses = array(
        '01' => 'Janeiro',
        '02' => 'Fevereiro',
        '03' => 'Março',
        '04' => 'Abril',
        '05' => 'Maio',
        '06' => 'Junho',
        '07' => 'Julho',
        '08' => 'Agosto',
        '09' => 'Setembro',
        '10' => 'Outubro',
        '11' => 'Novembro',
        '12' => 'Dezembro'
);
?>
<form>

    <input type="hidden" id="proposta_codigo" value="<?php echo $proposta->codigo; ?>">

</form>
<div id="main-center" style="padding-top:0!important;margin-top:0!important;">
    <header style="padding-top:0!important;margin-top:0!important;">
        <h1 style="width:300px;float:left;margin-top:0!important;">
            <img width="218" height="50" src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/<?php echo $logom ?>">
        </h1>
        <div style="float:right; margin-right:20px;">
            <h3 style="margin-top:0!important;">Contrato Nº.: <?php echo $proposta->codigo ?></h3>
        </div>
        <h3 style="margin-top:60px;font-size:30px;text-align:left">COMPRA PROGRAMADA</h3>
    </header>

    <table class="tab_contrato">
        <tr>
            <td style="background-color: #BDBDBD;" colspan="4">
                <strong>I – EMPRESA/LOJA/CEDENTE:</strong>
            </td>
        </tr>
        <tr>
            <td colspan="4">
<?php echo $proposta->analiseDeCredito->filial->getConcat() ?>, com sede à
<?php echo $proposta->analiseDeCredito->filial->getEndereco()->logradouro ?>,
<?php echo $proposta->analiseDeCredito->filial->getEndereco()->numero ?>,
<?php echo $proposta->analiseDeCredito->filial->getEndereco()->bairro ?>,
                <?php echo $proposta->analiseDeCredito->filial->getEndereco()->cidade . '/' . $proposta->analiseDeCredito->filial->getEndereco()->uf ?>,
                inscrita no CNPJ/MF sob o nº.:
                <?php echo $proposta->analiseDeCredito->filial->cnpj ?>
            </td>
        </tr>
    </table>
    <table class="tab_contrato">
        <tr>
            <td style="background-color: #BDBDBD;" colspan="4">
                <strong>II – QUALIFICAÇÃO DO COMPRADOR/CLIENTE/DEVEDOR</strong>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <strong>Nome:</strong>
<?php echo strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome) ?>
            </td>
            <td style="width:400px;">
                <strong>CPF:</strong>
                <?php echo $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero ?>
            </td>
        </tr>
        <tr>
            <td style="width:250px;">
                <strong>RG Nº:</strong>
<?php echo $proposta->analiseDeCredito->cliente->pessoa->getRG()->numero ?>
            </td>
            <td style="width:250px;">
                <strong >Estado Civil:</strong>
                <?php
                if ($proposta->analiseDeCredito->cliente->pessoa->Estado_Civil_id == 1) {
                    echo 'Solteiro(a)';
                }
                if ($proposta->analiseDeCredito->cliente->pessoa->Estado_Civil_id == 2) {
                    echo 'Casado(a)';
                }
                if ($proposta->analiseDeCredito->cliente->pessoa->Estado_Civil_id == 3) {
                    echo 'Divorciado(a)';
                }
                if ($proposta->analiseDeCredito->cliente->pessoa->Estado_Civil_id == 4) {
                    echo 'Viúvo(a)';
                }
                ?>
            </td>
            <td style="width:80px;">
                <strong>Sexo:</strong>
                <?php echo $proposta->analiseDeCredito->cliente->pessoa->sexo ?>
            </td>
            <td style="width:400px;">
                <strong>Email:</strong>
                <?php echo $proposta->analiseDeCredito->cliente->pessoa->getEmail() ?>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <strong>Endereço Res.:</strong>
<?php
echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->logradouro .
 ", nº " . $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->numero
?>
            </td>
            <td>
                <strong>Bairro:</strong>
                <?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->bairro ?>
            </td>
        </tr>
        <tr>
            <td style="width:250px;">
                <strong>Cidade:</strong>
<?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cidade ?>
            </td>
            <td style="width:250px;">
                <strong >CEP:</strong>
                <?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cep ?>
            </td>
            <td style="width:80px;">
                <strong>UF:</strong>
                <?php echo $proposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->uf ?>
            </td>
            <td style="width:400px;">
                <strong>Telefone:</strong>
                <?php echo $proposta->analiseDeCredito->cliente->pessoa->getTelefone() ?>
            </td>
        </tr>
    </table>
    <table class="tab_contrato">
        <tr>
            <td style="background-color: #BDBDBD;" colspan="4">
                <strong>III- ESPECIFICAÇÕES DO CRÉDITO:
            </td>
        </tr>

        <tr>
            <td colspan="2">
                <strong>1. Valor da Venda: R$ </strong>
<?php echo number_format($proposta->valor - $proposta->valor_entrada, 2, ',', '.') ?>
            </td>
            <td colspan="2">
                <strong>2. Tarifa de Cadastro: R$ </strong> 0,00
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>3. Valor da Mercadoria: R$  </strong>
                R$ <?php echo number_format($proposta->valor - $proposta->valor_entrada, 2, ',', '.') ?>
            </td>
            <td colspan="2">
                <strong>4. Valor a prazo: R$ </strong>
<?php echo number_format($proposta->getValorFinal(), 2, ',', '.') ?>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>5. Valor da Parcela: </strong>
<?php echo number_format($proposta->getValorParcela(), 2, ',', '.'); ?>
            </td>
            <td colspan="2">
                <strong>6. Quantidade de Parcelas: </strong>
                <?php echo $proposta->qtd_parcelas ?>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>7. Vencimento da 1ª Parcela: </strong>
<?php echo $proposta->getDataPrimeiraParcela() ?>
            </td>
            <td colspan="2">
                <strong>8. Vencimento da Última Parcela: </strong>
                <?php echo $proposta->getDataUltimaParcela(); ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <strong>9. Forma de Pagamento: </strong>
                Boleto Bancário
            </td>
        </tr>
    </table>
    <table class="tab_contrato">
        <tr>
            <td style="background-color: #BDBDBD;" colspan="4">
                <strong>IV - DADOS DO ADQUIRENTE/CESSIONÁRIA DO CRÉDITO </strong>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <strong>Razão Social: </strong> <?php echo $nome_fantasia; ?>
            </td>
            <td>
                <strong>CNPJ: </strong> <?php echo $cnpj; ?>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <strong>Endereço: </strong> <?php echo $endereco; ?>
            </td>
            <td>
                <strong>Contato: </strong>084 2040 0800
            </td>
        </tr>
    </table>
    <table class="tab_contrato">
        <tr>
            <td style="background-color: #BDBDBD;" colspan="4">
                <strong>V – ENCARGOS MORATÓRIOS</strong>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <strong>10. Juros Moratórios: </strong><?php echo ConfigLN::model()->valorDoParametro("taxa_mora"); ?>% <strong>ao dia.</strong>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <p>
                    O <strong>Comprador/Cliente/Devedor</strong> autoriza a <strong>Empresa/Loja/Cedente</strong>, a ceder, transferir, empenhar, alienar
                    o crédito independentemente de prévia comunicação.
                </p>
                <p>
                    Autorizo, também, a <strong>CESSIONÁRIA</strong> (IV) a registrar as informações decorrentes deste contrato e de minha responsabilidade junto ao
                    Sistema de Informações de Crédito (SCR) do Banco Central do Brasil (BACEN), para fins de supervisão do risco de crédito e intercâmbio
                    de informações com outras instituições financeiras. Estou ciente de que a consulta ao SCR pela Cessionária depende dessa prévia autorização
                    e que poderei ter acesso  aos dados do SCR pelos meios colocados a minha disposição pelo BACEN, sendo que eventuais pedidos de correções,
                    exclusões, registros de medidas judiciais e de manifestações de discordância sobre as informações inseridas no SCR deverão ser efetuados
                    por escrito, acompanhados, se necessário, de documentos. Ainda, autorizo (i) a fornecer e compartilhar as informações cadastrais,
                    financeiras e de operações ativas e passivas e serviços prestados junto a outras instituições pertencentes ao Conglomerado da <strong>CESSIONÁRIA</strong>,
                    ficando todas autorizadas a examinar e utilizar, no Brasil e no exterior, tais informações, inclusive para ofertas de produtos e serviços;
                    (ii) a informar aos órgãos de proteção ao crédito, tais como SERASA e SPC, os dados relativos a falta de pagamento de obrigações assumidas e
                    (iii) a compartilhar informações cadastrais com outras instituições financeiras e a contatar-me por meio de Cartas, e-mails,
                    Short Message Service (SMS) e telefone, inclusive para ofertar produtos e serviços.
                </p>
                <p>
                    A quitação das parcelas pagas via boleto bancário somente ocorrerá após a efetiva compensação e disponibilização do recurso ao credor.
                    Em caso de atraso no pagamento por parte do <strong>Comprador/Cliente/Devedor</strong>, incidirão sobre o valor da obrigação vencida, juros moratórios de <?php echo ConfigLN::model()->valorDoParametro("taxa_mora"); ?>% ao dia.
                </p>
                <br>
                <p>
                    <strong>Declaro ter lido e estar de acordo com os termos e condições do Contrato de Intermediação de Compra e Venda de Produtos celebrado nesta data com a CREDSHOW OPERADORA DE CRÉDITO S/A.</strong>
                </p>
                <p>
                    <strong>
                    Declaro ter ciência de que o produto somente será retirado após o adimplemento de 50% (cinquenta por cento) do valor do produto/parcelas, todas adimplidas até seus respectivos vencimentos e sem restrições no SPC/SERASA na data da retirada.
                    </strong>
                </p>
                <p>
                <h4>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<?php echo $proposta->analiseDeCredito->filial->getEndereco()->cidade . '/' . $proposta->analiseDeCredito->filial->getEndereco()->uf . ", " . Date('d') . " de  " . $arrMeses[Date('m')] . " de " . Date('Y'); ?></h4>
                </p>
                <p style="margin-top: 60px;">
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp________________________________________________
                </p>
                <p style="margin-top:-10px; margin-left: 33px;">
                    <strong>COMPRADOR/CLIENTE</strong>
                </p>
                <p style="margin-top: 30px;">
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp________________________________________________
                </p>
                <p style="margin-top:-10px; margin-left: 33px;">
                    <strong>EMPRESA/LOJA</strong>
                </p>
                <p style="margin-top: 30px; margin-left: 33px;">
                    <strong>TESTEMUNHAS:</strong> <br /><br />
                    1. ______________________________________________ &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    2. ______________________________________________ <br />

                    &nbsp&nbsp&nbsp Nome:
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    Nome:
                </p>
                <p style="margin-top:-10px; margin-left: 33px;">
                    &nbsp&nbsp&nbsp CPF:
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    CPF:
                </p>
            </td>
        </tr>
    </table>
    <table class="tab_contrato">
        <tr>
            <td style="background-color: #BDBDBD;" colspan="4">
                <strong>CONTRATO DE INTERMEDIAÇÃO DE COMPRA E VENDA DE PRODUTOS</strong>
            </td>
        </tr>

        <tr>
            <td style="width:800px">
                <p>
                    Pelo presente instrumento, de um lado, CREDSHOW OPERADORA DE CRÉDITO S/A, inscrita no CNPJ sob o nº. 13.861.348/0001-15, com sede na, doravante denominada INTERMEDIADORA e, de outro, COMPRADOR, devidamente qualificado na primeira página deste documento denominado anexo I.
                    Em conjunto denominado Partes e, individualmente, simplesmente Parte, resolvem celebrar o presente CONTRATO DE INTERMEDIAÇÃO DE COMPRA E VENDA DE PRODUTOS (contrato) que se regerá pelas cláusulas e condições a seguir ajustadas.
                </p>
                <br />
                <p>
                    <strong>1. DO OBJETO</strong>
                </p>
                <p>
                    1.1. O Objeto deste contrato é intermediação e compra de produtos a varejo pela Intermediadora ao Comprador, mediante pagamento do preço total do Plano <strong>CompraProgramada</strong>.    
                </p>
                <p>
                    1.2. O Comprador declara ter capacidade legal para contratar do presente.
                </p>
                <p>
                    1.3. O Comprador declara ter ciência de que o Produto somente poderá ser retirado/entregue na loja escolhida, após efetivado o adimplemento em dia e forma das xx (seis, sete, oito) prestações e sem restrições no SPC/SERASA na data da retirada.
                </p>
                <p>
                    <strong>2. DO PREÇO E DA FORMA DE PAGAMENTO</strong>
                </p>
                <p>
                    2.1. O valor do produto será o preço ajustado entre as Partes no ato da assinatura do presente Contrato, conforme condições estabelecidas no Anexo I deste Contrato.
                </p>
                <p>
                    2.2. Todos os preços e condições deste Contrato são válidos apenas no ato da compra realizada através da contratação do Plano <strong>CompraProgramada</strong> e assinatura do presente Contrato.
                </p>
                <p>
                    2.3. O Comprador realizará sua compra mediante o pagamento antecipado das parcelas do preço total do produto (Plano <strong>CompraProgramada</strong>), cujas prestações serão pagas por meio de boletos, conforme o número e valores das prestações escolhidas pelo Comprador no ato da compra.
                </p>
                <p>
                    2.4. O preço do produto será dividido pela quantidade de prestações indicadas pelo Comprador no Anexo I, limitada ao máximo de 15 (quinze) prestações.
                </p>
                <p>
                    2.5. Além do valor do produto o Comprador arcará com os custos referentes aos serviços para contratação do Plano <strong>CompraProgramada</strong> e dos respectivos encargos legais e tributos.
                </p>
                <p>
                    2.6. O vencimento da primeira prestação será agendado para 30 (trinta) dias a contar da data da compra e, deverá ser pago através de boleto de pagamento, exclusivamente nas lojas Maré Mansa.
                </p>
                <p>
                    2.7. As demais prestações deverão ser pagas, em dia, na forma indicada no item 2.6 acima.
                </p>
                <p>
                    2.8. O atraso no pagamento da prestação implicará multa não compensatória de 2% (dois por cento) sobre a prestação em atraso, acrescida da correção monetária de 2% (dois por cento) ao mês, calculados pro rata die, até a data do efetivo pagamento, que se dará com emissão de novo boleto devidamente calculado.
                </p>
                <p>
                    2.9. Se o pagamento de qualquer prestação atrasar por mais de 60 (sessenta) dias a Intermediadora poderá considerar o Comprador desistente e rescindida a compra realizada, cuja devolução do valor das prestações pagas ocorrerá somente ao término do prazo contido no Anexo I, escolhido pelo Comprador, descontada a taxa de cancelamento indicada no item 6.3 do presente contrato.
                </p>
                <p>
                    2.10. Vencendo a prestação aos sábados, domingos e/ou feriados, a mesma poderá ser paga no primeiro dia útil subsequente sem incidência de encargos legais.
                </p>
                <p>
                    2.11. Excepcionalmente no caso de desistência da compra, antes da retirada do produto, a Intermediadora promoverá a devolução dos valores das prestações pagas, descontada a taxa de cancelamento prevista no item 6.3, mediante depósito bancário em conta a ser informada pelo Comprador, ao término do prazo de prestações contido no Anexo I, escolhido pelo Comprador.
                </p>
                <p> 
                    <strong>3. DA ENTREGA DO PRODUTO</strong>
                </p>
                <p>
                    3.1. Após pagamento de 50% (cinquenta por cento) das prestações, adimplidas a tempo e modo e desde que não exista(m) restrição(ões) em seu nome no momento da retirada, o Comprador se dirigirá até a loja escolhida e poderá retirar o produto indicado na data da compra e presente no Anexo I
                </p>
                <p>
                    3.2.  A entrega do produto será realizada pela loja escolhida.
                </p>
                <p>
                    3.2.1. Havendo necessidade de pagamento de frete para transporte do produto, este ficará sob a responsabilidade do Comprador.
                </p>
                <p>
                    3.3. Não será possível a alteração da pessoa responsável pela retirada do produto após processamento do pedido.
                </p>
                <p>
                    3.4. O prazo para entrega do produto variará de acordo com a loja escolhida para retirada do produto.
                </p>
                <p>
                    3.4.1. A Intermediadora não será responsabilizada pela demora na entrega do produto em caso de falta de estoque.
                </p>
                <p>
                    3.4.2. A Intermediadora, a critério do Comprador, poderá realizar a substituição do produto escolhido no ato da contratação, desde que os valores sejam equivalentes.
                </p>
                <p>
                    3.4.2.1. Havendo a opção por produto de maior valor, o Comprador arcará com a diferença no ato da retirada. Esta diferença poderá ser paga pelos meios oferecidos pela loja onde será realizada a retirada do produto.
                </p>
                <p>
                    3.4.2.2. Não poderá o Comprador retirar produto com valor inferior ao escolhido.
                </p>
                <p>
                    3.5. O Comprador deverá verificar o produto e seus acessórios no momento da entrega, quando deverá assinar termo de recebimento.
                </p>
                <p>
                    <strong>4. DO PRODUTO</strong>
                </p>
                <p>
                    4.1. Nos termos da legislação em vigor o Comprador que tenha comprado o produto terá o direito de realizar a sua troca ou devolução pelos seguintes motivos, de acordo com a política da loja escolhida:<br>
                    a) Defeito técnico ou de fabricação do produto;<br>
                    b) Avaria ou estrago do produto que o incapacite de uso;
                </p>
                <p>
                    4.2. A substituição ou devolução do produto somente será realizado na sua caixa original e com todos os acessórios.
                </p>
                <p>
                    4.3. Caso o produto escolhido não esteja disponível, o Comprador poderá optar pelo:
                </p>
                <p>
                    4.3.1. Reembolso do valor pago das prestações até a data da solicitação da entrega/retirada, descontada a taxa de cancelamento prevista no item 6.3, mediante depósito bancário em conta a ser informada pelo Comprador, no prazo de até 10 (dez) dias úteis.
                </p>
                <p>
                    4.3.2. Troca por outro produto disponível, ficando a diferença de preço, se maior, sob responsabilidade do Comprador, que poderá ser paga pelos meios oferecidos pela loja onde será realizada a retirada do produto.
                </p>
                <p>
                    <strong>5. DA SUBSTITUIÇÃO DO PRODUTO</strong>
                </p>
                <p>
                    5.1. O Comprador reconhece que, em função do intervalo (ou período) de tempo entre a contratação o Plano <strong>CompraProgramada</strong> e a data prevista para entrega do produto, é possível acontecer que o produto seja retirado de fabricação/distribuição/comercialização, ou mesmo não estar mais disponível em estoque na loja escolhida, não tendo a Intermediadora condições, qualquer controle ou ingerência de qualquer destas situações. Porém, resta assegurado ao Comprador o recebimento de outro produto com preço similar.
                </p>
                <p>
                    5.1.1. Ocorrendo uma das situações referidas no item 5.1 e não sendo aceita a substituição do produto pelo Comprador, será realizado o reembolso do valor pago das prestações até a data da solicitação da entrega/retirada, descontada a taxa de cancelamento prevista no item 6.3, mediante depósito bancário em conta a ser informada pelo Comprador, no prazo de até 10 (dez) dias úteis.
                </p>
                <p>
                    <strong>6. DA DESISTÊNCIA</strong>
                </p>
                <p>
                    6.1. Deixando o Comprador de pagar suas prestações em dia, com atraso superior a 60 (sessenta dias) a Intermediadora considerará o Comprador desistente e rescindida a compra realizada, cuja devolução do valor das prestações pagas ocorrerá somente ao término do prazo contido no Anexo I, escolhido pelo Comprador, descontada a taxa de cancelamento indicada no item 6.3 do presente contrato.
                </p>
                <p>
                    6.2. Havendo desistência por motivos contidos nos itens 4.3.1 e 5.1.1, o valor das prestações pagas ocorrerá em até 10 (dez) dias úteis, descontada a taxa de cancelamento indicada no item 6.3, mediante depósito a ser realizado em conta bancária informada pelo Comprador.
                </p>
                <p>
                    6.3. O Comprador declara e concorda que, havendo desistência por um dos motivos previstos nos itens 6.1 ou 6.2, receberá os valores pagos, descontando-se a taxa de cancelamento fixada em 25% (vinte e cinco por cento) do valor total pago. Não sendo adimplida nenhuma das parcelas, o Comprador pagará o valor de R$ 100,00 (cem reais) referente a taxa de cancelamento.
                </p>
                <p>
                    <strong>7. DA VIGÊNCIA</strong>
                </p>
                <p>
                    7.1. O presente Contrato vigerá da data de assinatura até a data de pagamento da última prestação.
                </p>
                <p>
                    <strong>8. DAS INFORMAÇÕES</strong>
                </p>
                <p>
                    8.1. É de inteira responsabilidade do Comprador a veracidade e consistência das informações individuais prestadas, sendo assim o Comprador é o único responsável por qualquer ocorrência gerada pela não veracidade ou inexatidão das informações individuais.
                </p>
                <p>
                    8.2. O Comprador indenizará a Intermediadora, por qualquer demanda decorrente do uso de informações inverídicas.
                </p>
                <p>
                    8.3. É de inteira responsabilidade do Comprador a inserção dos dados verdadeiros no formulário de cadastramento constante no Anexo I deste Contrato.
                </p>
                <p>
                    8.4. Caso a Intermediadora constate qualquer erro ou inexatidão nas informações prestadas pelo Comprador ou ilegalidade em sua conduta, poderá, a seu exclusivo critério, suspender e/ou cancelar o correspondente cadastro. Tal atitude, contudo não cessa nem diminui a responsabilidade do Comprador quanto à exatidão e veracidade dos dados que fornecer.
                </p>
                <p>
                    <strong>9. DOS DIREITOS AUTORAIS E PROPRIEDADE INTELECTUAL</strong>
                </p>
                <p>
                    9.1. O uso comercial da expressão <strong>CompraProgramada</strong> como marca, nome empresarial no todo ou em parte ou, qualquer tipo de vinculação pessoal ou comercial, são de propriedade exclusiva da Intermediadora e estão protegidos pelas leis e tratados internacionais de direito autoral, marcas, patentes, modelos e desenhos industriais.
                </p>
                <p>
                    <strong>10. DAS DISPOSIÇÕES GERAIS</strong>
                </p>
                <p>
                    10.1. É terminantemente proibida a cessão ou transferência deste contrato a terceiros, pelo Comprador.
                </p>
                <p>
                    10.2. A Intermediadora poderá, a seu exclusivo critério, não liberar a entrega do produto, caso seja constatada qualquer inconsistência nas informações prestadas pelo Comprador; atraso nos pagamentos; restrição em cadastros de inadimplentes por outras empresas.
                </p>
                <p>
                    10.3. Após a entrega do produto, estando todas as obrigações por parte do Comprador satisfeitas no ato, o Comprador fica ciente que as parcelas vincendas e a pagar do Plano <strong>CompraProgramada</strong>, se tornarão títulos protestáveis e caso o Comprador fique inadimplente, a intermediadora poderá incluir o seu nome nos cadastros restritivos ao crédito (SPC, SERASA etc).
                </p>
                <p>
                    10.4. Qualquer modificação no presente Contrato, se dará por mera liberalidade da Intermediadora, por escrito e, em comum acordo.
                </p>
                <p>
                    10.5. Este Contrato obriga as partes e seus sucessores, a qualquer título.
                </p>
                <p>
                    10.6. Eventuais tolerâncias quanto a possíveis violações aos preceitos deste Contrato, será considerada mera liberalidade da Intermediadora, não implicando em novação, precedente invocável, renúncia de direitos, alteração tácita das regras, direito adquirido ou alteração contratual.
                </p>
                <p>
                    10.7. A nulidade de uma das cláusulas, não implicará em nulidade de outras ou do Contrato. Sempre que possível, as disposições consideradas nulas ou inválidas deverão ser reescritas, de modo a refletir a intenção inicial das Partes em conformidade com a legislação aplicável.
                </p>
                <p>
                    10.8. O presente Contrato com seus anexos constitui um único instrumento contratual que rege a relação entre as Partes. Portanto, expressamente revogadas qualquer condições anteriormente firmadas.
                </p>
                <p>
                    10.9. É expressamente vedado ao Comprador a utilização da expressão <strong>CompraProgramada</strong>, a não ser que seja expressamente autorizado pela Intermediadora e, por escrito.
                </p>
                <p>
                    10.10. Havendo divergência entre documentos apresentados e anexados ao presente Contrato por parte do Comprador e as clausulas e condições do presente Contrato, estas prevalecerão.
                </p>
                <p>
                    <strong>11. DA LEGISLAÇÃO E FORO</strong>
                </p>
                <p>
                    11.1. Este regulamento será regido pelas leis da República Federativa do Brasil.
                </p>
                <p>
                    11.2. Fica eleito o foro da Comarca de Natal/RN, como competente para dirimir quaisquer questões decorrentes do presente instrumento, renunciando expressamente, qualquer outro por mais privilegiado que seja.
                </p>
                <p></p>
                <p></p>
                <p></p>
                <p></p>
                <p></p>
                <p></p>
            </td>
        </tr>
    </table>
    <table class="tab_contrato">
        <tr>
            <td style="background-color: #BDBDBD;" colspan="4">
                <strong>DECLARAÇÃO SE ANALFABETO OU IMPEDIDO DE ASSINAR</strong>
            </td>
        </tr>

        <tr>
            <td style="width:800px">
                <p>
                    Declaro que ouvi atentamente a leitura desta Cédula, na presença das testemunhas abaixo qualificadas,
                    estando ciente e de acordo com as condições e obrigações que assumi na presente operação.
                </p>
                <br />
                <p>
                    Assinatura a rogo do Eminente: ____________________________________________
                </p>
                <p style="margin-left:195px; margin-top:-10px"> Nome: </p>
                <p style="margin-left:195px; margin-top:-10px"> CPF/MF: </p>
                <p>
                    Testemunhas: <br /><br />
                    1. _________________________________________ &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    2. _________________________________________ <br />

                    &nbsp&nbsp&nbsp Nome:
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    Nome:
                </p>
                <p style="margin-top:-10px;">
                    &nbsp&nbsp&nbsp CPF:
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    CPF:
                </p>
            </td>
            <td style="text-align: center;">
                <p style="margin-left: 30px;">Impressao Digital</p>
                <div style="width:130px; height:160px; border: 2px solid; margin-left:17px;">
                </div>
            </td>
        </tr>
    </table>
    
    <p style="margin: 0 auto;">Central de Relacionamento Credshow: 84 2040 0800 | ouvidoria@credshow.com.br | www.credshow.com.br</p>
    <div id="codigodebarras"></div>
</div>
<script>
    $(function () {

        $("#codigodebarras").barcode($('#proposta_codigo').val(), "code128", {
            'barWidth': 2, 'barHeight': 60, 'fontSize': 12
        });
    });
</script>

<style type="text/css">

    #codigodebarras{
        margin: 0 auto;
        margin-top: -40px;
    }

    .tab_contrato{
        width: 1000px;
        border-collapse: collapse;
    }

    .tab_contrato tr td {
        border:1px solid;
    }

    td {
        padding-left:8px!important;
        padding-right:8px!important;
    }

    p {
        text-align: justify;
    }

    @media all {
        .page-break { display: none; }
    }

    @media print {
        .page-break { display: block; page-break-before: always; }
    }

    @media print {
        * {-webkit-print-color-adjust:exact;}
    }
</style>
