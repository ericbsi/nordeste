<?php

	$util = new Util;

	$parcelasLeft 	= array();
	$parcelasRight 	= array();
?>
<form>
	<input type="hidden" id="proposta_codigo" value="<?php echo $proposta->codigo ?>">
</form>
<div id="main-center" style="padding-top:0!important;margin-top:0!important;">
	<header style="padding-top:0!important;margin-top:0!important;">
		<h1 style="width:150px;float:right;margin-top:0!important;">
			<img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/logomarca-tokio.png">
		</h1>
		<h2 style="">PROPOSTA DE ADESÃO SEGURO SHOW DE PROTEÇÃO</h2>
	</header>

	
	<section style="text-align:center;margin-top:0px;float:left;width:100%;text-align:left">
		<p><strong>O início da vigência será a partir das 24h da data do pagamento do prêmio, permanecendo vigente até o final do financiamento.</strong></p>
	</section>

	<section id="contrato-section-one">
		<table>
			<thead>
				<tr>
					<th style="text-align:right; border:none!important">
						<strong>APÓLICE Nº X.XXX.XXX</strong>
					</th>
				</tr>
			</thead>
		</table>
		<table>
			<thead>
				<tr>
					<th>DADOS DO SEGURADO</th>
				</tr>
			</thead>
		</table>
		<table>
			<tbody>
				<tr>
					<td><strong>Nome: </strong><?php echo strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome) ?></td>
					<td><strong>CPF: </strong><?php echo $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero ?></td>
				</tr>
			</tbody>
		</table>
		<table>
			<tbody>
				<tr>
					<td><strong>RG: </strong><?php echo $proposta->analiseDeCredito->cliente->pessoa->getRG()->numero ?></td>
					<td><strong>Data de Nascimento: </strong><?php echo $util->bd_date_to_view($proposta->analiseDeCredito->cliente->pessoa->nascimento) ?></td>
					<td><strong>TEL: </strong><?php echo $proposta->analiseDeCredito->cliente->pessoa->contato->getLasTelefone()->getDDD().$proposta->analiseDeCredito->cliente->pessoa->contato->getLasTelefone()->getNumero() ?></td>
				</tr>
			</tbody>
		</table>
		<table>
			<tbody>
				<tr>
					<td><strong>Endereço: </strong><?php echo $proposta->analiseDeCredito->cliente->pessoa->getEndereco()->logradouro; ?>, <?php echo $proposta->analiseDeCredito->cliente->pessoa->getEndereco()->numero; ?></td>
					<td><strong>Complemento: </strong><?php echo $proposta->analiseDeCredito->cliente->pessoa->getEndereco()->complemento ?></td>
					<td><strong>Cidade: </strong><?php echo $proposta->analiseDeCredito->cliente->pessoa->getEndereco()->cidade ?></td>
					<td><strong>UF: </strong><?php echo $proposta->analiseDeCredito->cliente->pessoa->getEndereco()->uf ?></td>
					<td><strong>CEP: </strong><?php echo $proposta->analiseDeCredito->cliente->pessoa->getEndereco()->cep ?></td>
				</tr>
			</tbody>
		</table>

		<div style="float:left;width:47%">
	</section>

	<section id="contrato-section-three" style="margin-top:0px">
		<table>
			<thead>
				<tr>
					<th>COBERTURAS</th>
					<th>CAPITAL SEGURADO/LMG</th>
					<th>FRANQUIA</th>
					<th>CARÊNCIA</th>
					<th>TAXA (SOBRE O VALOR DA COMPRA)</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Básica - Morte</td>
					<td>
						Quitação do saldo devedor da compra financiada até o limite de R$ 1.500,00
					</td>
					<td>  </td>
					<td>  </td>
					<td>0,77%</td>
				</tr>
				<tr>
					<td>IPTA - Invalidez Permanente Total por Acidente</td>
					<td>
						Quitação do saldo devedor da compra financiada até o limite de R$ 1.500,00
					</td>
					<td>  </td>
					<td>  </td>
					<td>0,03%</td>
				</tr>
				<tr>
					<td>PRD - Perda da renda por desemprego involuntário</td>
					<td>
						Quitação de até cinco parcelas da compra financiada de até R$ 120,00 cada
					</td>
					<td>30 (Trinta) dias</td>
					<td>30 (Trinta) dias</td>
					<td>4,20%</td>
				</tr>
				<tr>
					<td>IFT - Incapacidade Física Temporária por Acidente ou Doença</td>
					<td>
						Quitação de até cinco parcelas da compra financiada de até R$ 120,00 cada
					</td>
					<td>15 (Quinze) dias</td>
					<td>30 (Trinta) dias</td>
					<td>4,20%</td>
				</tr>
				<tr>
					<td colspan="1" >Prêmio Líquido de IOF</td>
					<td colspan="4" ></td>
				</tr>
				<tr>
					<td colspan="1" >IOF 0,38%</td>
					<td colspan="4" ></td>
				</tr>
				<tr>
					<td colspan="1" >Prêmio Total</td>
					<td colspan="4" ></td>
				</tr>
			</tbody>
		</table>

		<br>

		<hr>

		<br>

		<table>
			<tbody>
				<tr>
					<td><strong>Sorteio Único</strong></td>
					<td style="text-align:right;"><strong>R$ 5.000,00 (Bruto de IR)*</strong></td>
				</tr>
			</tbody>
		</table>

		<!--
		<table>
			<tbody>
				<tr>
					<td><strong>TOTAL</strong></td>
					<td style="text-align:right;"><strong>Taxa total, a ser aplicada sobre o valor total do financiamento parcelado: 05%</strong></td>
				</tr>
			</tbody>
		</table>
		-->
	</section>
	<section id="contrato-section-four">
		<p>
			<strong>
				• Carência: Para a cobertura de Perde de renda por desemprego involuntário e para a cobertura de Incapacidade Física temporária por acidente ou doença aplica-se 30 (trinta) dias.
			</strong>
		</p>
		<p>
			<strong>
				• Franquia: Para a cobertura de Perde de renda por desemprego involuntário aplica-se 30 (trinta) dias e para a cobertura de Incapacidade Física temporária por acidente ou doença aplica-se 15 (quinze) dias.
			</strong>
		</p>
		<p style="font-size:15px">
		<strong>Beneficiário: O beneficiário será sempre o Estipulante.</strong>
		</p>
		<strong>
		<p style="font-size:15px">
			A contratação do seguro é opcional, sendo possível a desistência do contrato em até 7 (sete) dias corridos com a 
			devolução integral do valor pago, pelo mesmo meio exercido para a contratação. É proibido condicionar desconto no
			preço do bem à aquisição do seguro, vincular a aquisição do bem à contratação compulsória de qualquer tipo de seguro
			ou ofertar bens em condições mais vantajosas para quem contrata plano de seguro.
		</p>
		</strong>
		<p style="font-size:15px">
			Autorizo a cobrança única do Seguro Show de Proteção pelo mesmo meio de cobrança do produto por ora adquirido.
			Declaração: Declado que neste ato de contratação tenho de 18 a 65 anos de idade, me encontro em boas condições de saúde, 
			que receberei acesso ao meu Certificado e Características de Seguro através do Portal Autoatendimento da Tokio Marine e que
			entendi e aceitei as Condições contratuais do Seguro. Que as informações prestadas são verdadeiras e completas, estando ciente
			que de acordo com o Artigo 766 do Código Civil Brasileiro, se tiver omitido algo ou prestado informações inexatas que possam
			influir na aceitação deste termo ou no valor do prêmio do seguro, perderei o direito à indenização em caso de sinistro.
			Estou ciente que, quando necessário, meus dados poderão ser compartilhados pela Seguradora a empresas parceiras, para o fim
			específico de atender a prestação de serviços decorrente do contrato de seguro, respeitando-se a confidencialidade das informações.
		</p>
		<p style="font-size:15px">
			As disposições aqui referidas são uma breve descrição do produto. Restrições se aplicam. Para mais informações consulte as condições
			gerais do seguro a disposição no site www.tokiomarine.com.br. O Plano de seguro também pode ser consultado no site da SUSEP: 
			http://www.susep.gov.br/menu/informacoes-ao-publico/planos-e-produtos/consulta-publica-de-produtos-1. <br>
			Este seguro é por prazo determinado tendo a seguradora a faculdade de não renovar a apólice na data de vencimento, 
			sem devolução dos prêmios pagos nos termos da apólice. O registro do produto e a aprovação do título de capitalização 
			na SUSEP não implica por parte da autarquia, incentivo ou recomendação à sua comercialização, representando, 
			exclusivamente, sua adequação ás normas em vigor. A aceitação do seguro pela seguradora estará sujeita a 
			análise do risco. Tenho ciência que poderei consultar a situação cadastral do corretor de seguros no site 
			www.susep.gov.br, por meio de seu nº de registro na SUSEP, nome completo, CNPJ ou CPF.
		</p>
		<p>
			<strong>
				SUSEP - Superintendência de Seguros Privados - Autarquia Federal responsável pela fiscalização, normatização e controle
				dos mercados de seguro, previdência complementar aberta, capitalização, resseguro e corretagem de seguros. <br>
				* Sorteio único no último sábado do mês subsequente ao pagamento. Título de Capitalização, da modalidade incetivo, emitido
				pela Sul Americana Capitalização S.A. - Sulacap, CNPJ 03.558-096/0001-04, Ouvidoria Sulamérica 0800 725 3374 
				processo SUSEP 15414.900878/2013-85, prêmio no valor bruto com incidência de 25% de imposto de renda, conforme legislação vigente.
			</strong>
		</p>
		<p style="font-size:15px">
			<strong>Estipulante:</strong> CREDSHOW OPERADORA DE CREDITO LTDA EPP, CNPJ: 13.861.348/0001-15<br>
			<strong>Pró-labore:</strong> 75% R$ XX,XX<br>
			<strong>Endereço:</strong>Av. Amintas Barros, n° 3700, Sala 1904 Bloco A, Corporate Tower, Center Business	, Bairro Lagoa Nova - Natal/RN<br>
			<strong>Seguradora:</strong>Tokio Marine Seguradora S.A, CNPJ 33.164.021/0001-00 Código Susep: 06190-0<br>
			<strong>Ramo:</strong>(0977) Prestamista Processo Susep 15414.002638/2006-94<br>
			<strong>Corretor:</strong>Fundação Escola Nacional de Seguros, CNPJ: 42.161.687/0001-97<br>

		</p>
		<p style="font-size:15px">
			<strong>Centrais de Atendimento:</strong>
			<p>Central de atendimento 0800 27 17377</p>
			<p>SAC Tokio Marine: 0800 703 9000 - 0800 770 1523 (Deficientes Auditivos e de Fala). </p>
			<p>Ouvidoria 0800-449-0000 – E-mail: ouvidoria@tokiomarine.com.br.</p>
			<p>Disque Fraude: 0800 707 6060</p>
			<p>** Acesse sua Apólice através do Portal Auto atendimento em www.tokiomarine.com.br </p>
			<p><strong>Central de Atendimento SUSEP – 0800 021 8484 - Atendimento Exclusivo ao Consumidor (9h30 às 17h00).</strong></p>
		</p>
		
	</section>
	<section id="assinatura">
		<table>
			<tbody>
				<tr>
					<td width="900">________________________________________</td>
					<td style="text-align:right">____/____/____</td>
				</tr>
			</tbody>
		</table>
		<table>
			<tbody>
				<tr>
					<td>Assinatura do segurado</td>
				</tr>
			</tbody>
		</table>
	</section>
	
	
</div>

<script>
        $(function(){
                   	
        	$("#codigodebarras").barcode($('#proposta_codigo').val(), "code128", {
				'barWidth' : 2, 'barHeight':50, 'fontSize': 14
			});
        })
</script>

<style type="text/css">
	
	#assinatura{
		margin-bottom: 50px;
	}

	#contrato-section-four p strong{
		font-size: 19px;
	}

	#contrato-section-three, #contrato-section-one, #assinatura{
		width: 100%;
		padding: 0px!important;
		float: left;
		border:none;
	}
	
	#contrato-section-three table, #contrato-section-one table{
		width: 100%;
	}
	#contrato-section-three table tr, #contrato-section-one table tr{
		border: 1px solid ;
	}
	#contrato-section-three table tr th, #contrato-section-one table tr th{
		border: 1px solid ;
		padding:10px!important;
	}
	#contrato-section-three table tr td, #contrato-section-one table tr td{
		border: 1px solid ;
		padding:10px!important;
	}
	#main-center section p {
	    /* text-align: justify; */
	    line-height: 18px;
	    letter-spacing: -0.1px;
	}
</style>