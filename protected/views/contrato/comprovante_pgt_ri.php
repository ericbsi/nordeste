<?php

$util = new Util;

if ($parcela == Null){
  $parcela = Parcela::model()->findByPk(256579);
}

$textoMora  = '';

$valorMora = $parcela->calcMora();

if( $parcela->vencimento < date('Y-m-d') ){

  $textoMora = ' do qual ' . number_format($parcela->calcMora(), 2, ',', '.') .'('.$util->valor_extenso($parcela->calcMora(), True).')';
  $textoMora .= ' é relativo à mora por '.$parcela->diasVencimento().' dias de atraso, ';

}

$parcelasLeft = array();
$parcelasRight = array();

$arrMeses = array(
        '01' => 'Janeiro',
        '02' => 'Fevereiro',
        '03' => 'Março',
        '04' => 'Abril',
        '05' => 'Maio',
        '06' => 'Junho',
        '07' => 'Julho',
        '08' => 'Agosto',
        '09' => 'Setembro',
        '10' => 'Outubro',
        '11' => 'Novembro',
        '12' => 'Dezembro'
);
$logom = "logocredshow.png";
$codigoAutenticacao = $parcela->id.$parcela->seq.$parcela->titulo->id;
?>

<form>
    <input type="hidden" id="proposta_codigo" value="<?php echo $codigoAutenticacao; ?>">
</form>

<div id="main-center" style="padding-top:0!important;margin-top:0!important;">
    <header style="padding-top:0!important;margin-top:0!important;">
        <h1 style="width:300px;float:left;margin-top:0!important;">
            <img width="218" height="50" src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/<?php echo $logom ?>">
        </h1>
        <div style="float:right; margin-right:20px;">

            <div id="codigodebarras"></div>
        </div>

    </header>

    <table class="tab_contrato">
        <tr>
            <td style="background-color: #BDBDBD;" colspan="4">
                <strong>DECLARAÇÃO DE QUITAÇÃO DE PARCELA</strong>
            </td>
        </tr>

        <tr>
            <td style="width:800px">
                <p>
                    Recebi de (<?php echo strtoupper($parcela->titulo->proposta->analiseDeCredito->cliente->pessoa->nome); ?>), no dia <?php echo date('d/m/Y'); ?>,
                    o valor de <?php echo number_format(($parcela->calcMora()+$parcela->valor), 2, ',', '.'); ?>(<?php echo $util->valor_extenso($parcela->valor+$parcela->calcMora(), true) ?> ),
                    <?php echo $textoMora; ?>
                    referente ao pagamento da <?php echo $parcela->seq; ?>º parcela, com vencimento em <?php echo $util->bd_date_to_view($parcela->vencimento) ?>,
                    do contrato <?php echo $parcela->titulo->proposta->codigo; ?>, pela qual dou plena e geral quitação.
                </p>
                <br />
            </td>
            <!-- <td style="text-align: center;">
                <p style="margin-left: 30px;">Impressao Digital</p>
                <div style="width:130px; height:160px; border: 2px solid; margin-left:17px;">
                </div>
            </td> -->
        </tr>
    </table>
    <p style="margin: 0 auto;">Central de Relacionamento Credshow: 84 2040 0800 | ouvidoria@credshow.com.br | www.credshow.com.br</p>



</div>
<script>
    $(function () {

        $("#codigodebarras").barcode($('#proposta_codigo').val(), "code128", {
            'barWidth': 2, 'barHeight': 60, 'fontSize': 12
        });
    });
</script>

<style type="text/css">

    #codigodebarras{
        margin: 0 auto;
        margin-top: -40px;
    }

    .tab_contrato{
        width: 1000px;
        border-collapse: collapse;
    }

    .tab_contrato tr td {
        border:1px solid;
    }

    td {
        padding-left:8px!important;
        padding-right:8px!important;
    }

    p {
        text-align: justify;
    }

    @media all {
        .page-break { display: none; }
    }

    @media print {
        .page-break { display: block; page-break-before: always; }
    }

    @media print {
        * {-webkit-print-color-adjust:exact;}
    }
</style>
