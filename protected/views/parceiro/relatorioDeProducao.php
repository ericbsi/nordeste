<?php
   
   $util             = new Util;

   $totalInicial            = 0;
   $totalEntrada            = 0;
   $totalSeguros            = 0;
   $totalFinanci            = 0;
   $totalFinal              = 0;

   $totalInicialEmprestimo  = 0;
   $totalEntradaEmprestimo  = 0;
   $totalSegurosEmprestimo  = 0;
   $totalFinanciEmprestimo  = 0;
   $totalFinalEmprestimo    = 0;

?>

<div class="row">
    <div class="col-sm-12">

        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="#">
                    Relatórios
                </a>
            </li>
            <li class="active">
                Produção
            </li>
        </ol>
    </div>
</div>

<div class="row">
   <div class="col-sm-9">
      <div class="row">
         <form id="form-filter" action="/parceiro/relatorioDeProducao/" method="POST">
            <div class="col-md-3" style="width:25%">
               <div class="form-group">
                  <label class="control-label">
                     Data de <span class="symbol required" aria-required="true"></span>
                  </label>                  
                  <input value="<?php if(!is_null($dataDe)){ echo $dataDe; } ?>" required="required" style="width:200px" type="text" name="data_de" class="form-control date-picker" data-date-viewmode="years" data-date-format="dd/mm/yyyy" id="data_de">
               </div>
            </div>
            <div class="col-md-3" style="width:25%">
               <div class="form-group">
                  <label class="control-label">Data até:</label>
                  <input value="<?php if(!is_null($dataAte)){ echo $dataAte; } ?>" required="required" style="width:200px" type="text" name="data_ate" class="form-control date-picker" data-date-viewmode="years" data-date-format="dd/mm/yyyy" id="data_ate">
               </div>
            </div>
            
            <div class="col-md-3" style="width:15%">
               <div class="form-group">
                  <label class="control-label">&nbsp;</label>
                  <button id="btn-filter" class="btn btn-blue next-step btn-block">Filtrar</button>                  
               </div>
            </div>
            <div class="col-md-3" style="width:20%">
               <div class="form-group">
                  <label class="control-label">&nbsp;</label>
                  <button id="btn-print-rel" class="btn btn-blue next-step btn-block">Imprimir relatório</button>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-sm-12">
      <div class="col-sm-5" style="padding:0!important">
         <div id="chart_div"></div>
      </div>
      <div class="col-sm-3" style="padding:0!important">
         <div id="chart_div2"></div>
      </div>
      <div class="col-sm-3" style="padding:0!important">
         <div id="chart_div3"></div>
      </div>
   </div>
</div>

<?php if( sizeof( $producao ) > 0 ) { ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i>
                    Produção CDC
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                        </a>
                    </div>
                </div>

                <div class="panel-body expand">
                    <table id="grid_producao" class="table table-striped table-bordered table-hover table-full-width dataTable">
                        <thead>
                            <tr>
                                <th style="width:15px!important" class="no-orderable">
                                    <label class="checkbox-inline" style="float:left">
                                        <input checked="checked" id="check_select_all" type="checkbox">
                                    </label>
                                </th>
                                <th style="width:70px!important" class="no-orderable">
                                    Cód
                                </th>
                                <th style="width:20px!important" class="no-orderable">
                                    Emissão
                                </th>
                                <th class="no-orderable">
                                    Filial
                                </th>
                                <th class="no-orderable">
                                    Cliente
                                </th>
                                <th class="no-orderable">
                                    CPF
                                </th>
                                <th class="no-orderable">
                                    Crediarista
                                </th>
                                <th style="width:100px!important" class="no-orderable">
                                    R$ Vlr Venda
                                </th>
                                <th style="width:80px!important" class="no-orderable">
                                    R$ Entrada
                                </th>
                                <th style="width:80px!important" class="no-orderable">
                                    R$ Seguro
                                </th>
                                <th style="width:60px!important" class="no-orderable">
                                    Carência
                                </th>
                                <th class="no-orderable">
                                    R$ Financiado
                                </th>
                                <th class="no-orderable">
                                    Parcelamento
                                </th>
                                <!--<th class="no-orderable">R$ Final</th>-->
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($producao as $proposta) {


                                $totalInicial  += $proposta->valor;
                                $totalEntrada  += $proposta->valor_entrada;
                                $totalSeguros  += $proposta->calcularValorDoSeguro();
                                $totalFinanci  += $proposta->getValorFinanciado();
                                $totalFinal    += $proposta->getValorFinanciado();

                            ?>
                            <tr>
                                <td>
                                   <label class="checkbox-inline" style="float:left">
                                      <input checked="checked" class="check_print" type="checkbox" value="<?php echo $proposta->id ?>">
                                   </label>
                                </td>
                                <td>
                                    <?php echo $proposta->codigo ?>
                                </td>
                                <td>
                                    <?php echo $proposta->dataParaView(); ?>
                                </td>
                                <td>
                                    <?php echo $proposta->analiseDeCredito->filial->getConcat(); ?>
                                </td>
                                <td>
                                    <?php echo strtoupper(substr($proposta->analiseDeCredito->cliente->pessoa->nome, 0,22)); ?>
                                </td>
                                <td>
                                    <?php echo $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero; ?>
                                </td>
                                <td>
                                    <?php echo $proposta->analiseDeCredito->usuario->nome_utilizador; ?>
                                </td>
                                <td>
                                    <?php echo 'R$ '.number_format($proposta->valor, 2, ',', '.'); ?>
                                </td>
                                <td>
                                    <?php echo 'R$ '.number_format($proposta->valor_entrada, 2, ',', '.'); ?>
                                </td>
                                <td>
                                    <?php echo number_format($proposta->calcularValorDoSeguro(),2,',','.'); ?>
                                </td>
                                <td>
                                    <?php echo $proposta->carencia; ?>
                                </td>
                                <td>
                                    <?php echo 'R$ '.number_format($proposta->getValorFinanciado(), 2, ',', '.'); ?>
                                </td>
                                <td>
                                    <?php echo $proposta->qtd_parcelas . ' x R$ ' . number_format($proposta->getValorParcela(),2,',','.'); ?>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th id="tfoot-total-ini">
                                <?php echo 'R$ ' . number_format($totalInicial,2,',','.'); ?>
                            </th>
                            <th id="tfoot-total-entradas">
                                <?php echo 'R$ ' . number_format($totalEntrada,2,',','.'); ?>
                            </th>
                            <th>
                                <?php echo 'R$ ' . number_format($totalSeguros,2,',','.'); ?>
                            </th>
                            <th></th>
                            <th id="tfoot-total">
                                <?php echo 'R$ ' . number_format($totalFinanci,2,',','.'); ?>
                            </th>
                            <th></th>
                       </tfoot>
                    </table>
                </div>
            </div>
        </div>

        <form id="form-print" target="_blank" method="POST" action="/printer/printProducaoParceiro/">
            <input type="hidden" id="parceiroId" value="<?php echo $proposta->analiseDeCredito->Filial_id ?>" name="parceiroId">

            <input type="hidden" id="dataDeHdn" name="dataDeHdn">
            <input type="hidden" id="dataAteHdn" name="dataAteHdn">
            <?php foreach ($producao as $proposta) { ?>
                <input type="hidden" value="<?php echo $proposta->id ?>" id="proposta_<?php echo $proposta->id ?>" name="idsPropostas[]">
            <?php } ?>
        </form>

    </div>
<?php } ?>

<?php if( sizeof( $producaoEmprestimo ) > 0 ) { ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i>
                    Produção Empréstimo
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                        </a>
                    </div>
                </div>

                <div class="panel-body expand">
                    <table id="grid_producaoEmprestimo" class="table table-striped table-bordered table-hover table-full-width dataTable">
                        <thead>
                            <tr>
                                <th style="width:15px!important" class="no-orderable">
                                    <label class="checkbox-inline" style="float:left">
                                        <input checked="checked" id="check_select_all" type="checkbox">
                                    </label>
                                </th>
                                <th style="width:70px!important" class="no-orderable">
                                    Cód
                                </th>
                                <th style="width:20px!important" class="no-orderable">
                                    Emissão
                                </th>
                                <th class="no-orderable">
                                    Filial
                                </th>
                                <th class="no-orderable">
                                    Cliente
                                </th>
                                <th class="no-orderable">
                                    CPF
                                </th>
                                <th class="no-orderable">
                                    Crediarista
                                </th>
                                <th class="no-orderable">
                                    Vendedor
                                </th>
                                <th style="width:100px!important" class="no-orderable">
                                    R$ Vlr Venda
                                </th>
                                <th style="width:80px!important" class="no-orderable">
                                    R$ Entrada
                                </th>
                                <th style="width:80px!important" class="no-orderable">
                                    R$ Seguro
                                </th>
                                <th style="width:60px!important" class="no-orderable">
                                    Carência
                                </th>
                                <th class="no-orderable">
                                    R$ Financiado
                                </th>
                                <th class="no-orderable">
                                    Parcelamento
                                </th>
                                <!--<th class="no-orderable">R$ Final</th>-->
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($producaoEmprestimo as $proposta) {


                                $totalInicialEmprestimo += $proposta->valor                     ;
                                $totalEntradaEmprestimo += $proposta->valor_entrada             ;
                                $totalSegurosEmprestimo += $proposta->calcularValorDoSeguro()   ;
                                $totalFinanciEmprestimo += $proposta->getValorFinanciado()      ;
                                $totalFinalEmprestimo   += $proposta->getValorFinanciado()      ;

                            ?>
                                <tr>
                                    <td>
                                       <label class="checkbox-inline" style="float:left">
                                          <input checked="checked" class="check_print" type="checkbox" value="<?php echo $proposta->id ?>">
                                       </label>
                                    </td>
                                    <td>
                                        <?php echo $proposta->codigo ?>
                                    </td>
                                    <td>
                                        <?php echo $proposta->dataParaView(); ?>
                                    </td>
                                    <td>
                                        <?php echo $proposta->emprestimo->filial->getConcat(); ?>
                                    </td>
                                    <td>
                                        <?php echo strtoupper(substr($proposta->analiseDeCredito->cliente->pessoa->nome, 0,22)); ?>
                                    </td>
                                    <td>
                                        <?php echo $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero; ?>
                                    </td>
                                    <td>
                                        <?php echo strtoupper($proposta->analiseDeCredito->usuario->nome_utilizador); ?>
                                    </td>
                                    <td>
                                        <?php echo strtoupper($proposta->analiseDeCredito->vendedor); ?>
                                    </td>
                                    <td>
                                        <?php echo 'R$ '.number_format($proposta->valor, 2, ',', '.'); ?>
                                    </td>
                                    <td>
                                        <?php echo 'R$ '.number_format($proposta->valor_entrada, 2, ',', '.'); ?>
                                    </td>
                                    <td>
                                        <?php echo number_format($proposta->calcularValorDoSeguro(),2,',','.'); ?>
                                    </td>
                                    <td>
                                        <?php echo $proposta->carencia; ?>
                                    </td>
                                    <td>
                                        <?php echo 'R$ '.number_format($proposta->getValorFinanciado(), 2, ',', '.'); ?>
                                    </td>
                                    <td>
                                        <?php echo $proposta->qtd_parcelas . ' x R$ ' . number_format($proposta->getValorParcela(),2,',','.'); ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th id="tfoot-total-ini">
                                <?php echo 'R$ ' . number_format($totalInicialEmprestimo,2,',','.'); ?>
                            </th>
                            <th id="tfoot-total-entradas">
                                <?php echo 'R$ ' . number_format($totalEntradaEmprestimo,2,',','.'); ?>
                            </th>
                            <th>
                                <?php echo 'R$ ' . number_format($totalSegurosEmprestimo,2,',','.'); ?>
                            </th>
                            <th></th>
                            <th id="tfoot-total">
                                <?php echo 'R$ ' . number_format($totalFinanciEmprestimo,2,',','.'); ?>
                            </th>
                            <th></th>
                       </tfoot>
                    </table>
                </div>
            </div>
        </div>

    </div>
<?php } ?>

<div class="row">
   <br>
   <br>
   <br>
</div>

<style>
    #grid_producao_length, #grid_producaoEmprestimo_length
    {
        display: none;
    }

    td.details-control 
    {
        background  : url('../../images/details_open.png') no-repeat center center;
        cursor      : pointer;
        padding     : 0 25px!important;
    }
    tr.details td.details-control 
    {
        background  : url('../../images/details_close.png') no-repeat center center;
    }
    tfoot th 
    {
        color       : #3D9400;
        font-size   : 10px;
    }

    #grid_producao tbody td
    {
        font-size   : 11px!important;
    }

    #grid_producao_filter
    {
        position    : relative;
        bottom      : 50px;
    }

    #grid_producao_filter input
    {

        background-color    : #FFFFFF!important;
        border              : 1px solid #D5D5D5!important;
        border-radius       : 0 0 0 0 !important;
        color               : #858585!important;
        font-family         : inherit!important;
        font-size           : 14px!important;
        line-height         : 1.2!important;
        padding             : 5px 4px!important;
        transition-duration : 0.1s!important;
    }

    #grid_producaoEmprestimo tbody td
    {
        font-size   : 11px!important;
    }

    #grid_producaoEmprestimo_filter
    {
        position    : relative;
        bottom      : 50px;
    }

    #grid_producaoEmprestimo_filter input
    {

        background-color    : #FFFFFF!important;
        border              : 1px solid #D5D5D5!important;
        border-radius       : 0 0 0 0 !important;
        color               : #858585!important;
        font-family         : inherit!important;
        font-size           : 14px!important;
        line-height         : 1.2!important;
        padding             : 5px 4px!important;
        transition-duration : 0.1s!important;
    }
</style>