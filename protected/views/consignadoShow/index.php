<div class="row"  id="initial_place">
   <div class="col-sm-12">
      <ol class="breadcrumb">
         <li><a href="#">Consignado Show</a></li>
         <li class="active"></li>
      </ol>
      <div class="page-header">
         <h1>Consignado Show - Credshow</h1>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-sm-12">
      <div class="tabbable tabs-left">
         <ul id="tabForm" class="nav nav-tabs tab-green">
            <li class="active" id="li-aba-detalhes-proposta">
               <a href="#tab4_detalhes_proposta" data-toggle="tab" data-form-bind="valor_da_compra">
               <i class="fa fa-keyboard-o"></i> Dados da Operação
               </a>
            </li>
         </ul>
         <div class="tab-content">
            <div class="tab-pane active" id="tab4_detalhes_proposta">
               <div class="col-md-12">
                  <form action="#" id="form-proposta">
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">Nome do cliente <span class="symbol required"></span></label>
                                 <input required="true" placeholder="Nome do cliente" id="nome" class="form-control" type="text" name="ConsignadoShow[nomeCliente]">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">CPF : <span class="symbol"></span></label>
                                 <input required name="ConsignadoShow[cpf]" type="text" placeholder="CPF" id="cpf" class="form-control cpf">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">Nascimento : <span class="symbol"></span></label>
                                 <input required name="ConsignadoShow[data_nascimento]" type="text" placeholder="Data de nascimento" id="nascimento" class="form-control dateBR">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">N° do benefício : <span class="symbol"></span></label>
                                 <input required name="ConsignadoShow[numero_do_beneficio]" type="text" placeholder="N° do benefício" id="beneficio" class="form-control">
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-3">                                 
                                 <input value="1" required="true" type="hidden" name="ConsignadoShow[StatusConsignado]">
                                 <input value="<?php echo Yii::app()->session['usuario']->id; ?>" required="true" type="hidden" name="ConsignadoShow[solilcitante]">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                              <div class="col-sm-3">
                                 <label class="control-label">Telefone <span class="symbol required"></span></label>
                                 <input required="true" placeholder="Telefone" id="telefone" class="form-control" type="text" name="ConsignadoShow[telefone]">
                              </div>
                           </div>
                           
                        </div>
                     </div>               
                  </form>
               </div>
               <div class="col-md-12">
                  
               </div>
            </div>
            
         </div>
      </div>
   </div>
</div>

<div data-width="690" id="sucess-return" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
   <div class="modal-body" style="background:#dff0d8!important">
      <h4 style="color:#5cb85c;text-align:center"><i class="fa fa-check-circle" ></i> Intenção de saque registrada com sucesso!</h4>
   </div>
   <div class="modal-footer" style="margin-top:0!important;text-align:center">
      
      <!--
      <form method="POST" action="/saqueFacil/comprovante/" style="display:inline">
         <button type="submit" class="btn btn-success">
            Imprimir comprovante de intenção <span id="span-codigo-proposta"></span>
            <input type="hidden" name="id" id="ipt-hdn-id-saque">
         </button>   
      </form>
      -->

      <form action="/proposta/minhasPropostas/" method="POST" style="display:inline">

         <button type="submit" class="btn btn-success">
            Página Inicial
         </button>   
      </form>
      <form action="/consignadoShow/historicoSolicitacoes/" method="POST" style="display:inline">
         <button type="submit" class="btn btn-success">
            Histórico de Solicitacoes
         </button>   
      </form>
   </div>
</div>

<div class="row" style="margin-bottom:40px;">
   <div class="col-md-3"></div>
   <div class="col-md-6">
      <div class="form-group">
         <button id="btn-enviar-proposta" class="btn btn-yellow btn-block">
            Enviar <i class="fa fa-arrow-circle-right"></i>
         </button>
      </div> 
   </div>
   <div class="col-md-3">        
   </div>
</div>