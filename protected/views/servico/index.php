<div class="row">
   <div class="col-sm-12">
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
                Serviços
            </a>
         </li>
         <li class="active">
            Gerenciar
         </li>
      </ol>
   </div>
</div>

<div class="row">
   <div class="col-sm-12">
      <table id="gridServicos" class="table table-striped table-bordered table-hover table-full-width dataTable">
         <thead>
             <tr>
               <th class="no-orderable">
                   <input class="form form-control" id="inputDescricao" type="text"/>
               </th>
               <th>
               </th>
               <th width="3px">
                   <button id="btnSalvar" class="btn btn-green">
                       <i class="fa fa-save"></i>
                   </button>
               </th>
            </tr>
            <tr>
               <th class="no-orderable">
                   Descrição
               </th>
               <th class="no-orderable">
                   Qtd Filiais
               </th>
               <th>
               </th>
            </tr>
         </thead>
      </table>
   </div>
</div>

<style>
   #gridServicos_length, #gridServicos_filter{
      display: none;
   }
   td.details-control {
      background: url('../../images/details_open.png') no-repeat center center;
      cursor: pointer;
   }
   tr.shown td.details-control {
      background: url('../../images/details_close.png') no-repeat center center;
   }
</style>