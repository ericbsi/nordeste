<?php
    $grupos = GrupoFiliais::model()->findAll('habilitado');
    $nucleos = NucleoFiliais::model()->findAll('habilitado');
    $filiais = Filial::model()->findAll('habilitado');
?>
<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="#">
                    Serviços
                </a>
            </li>
            <li class="active">
                Gerenciar
            </li>
        </ol>
    </div>
</div>
<br /><br />
<div class="row">
    <div class="col-md-4">
        <select required="required" multiple="multiple" id="selectGrupoFiliais" class="form-control search-select multipleselect" >
            <?php foreach ($grupos as $gf) { ?>
                <option value="<?php echo $gf->id ?>"><?php echo strtoupper($gf->nome_fantasia) ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="col-md-4">
        <select required="required" multiple="multiple" id="selectNucleoFiliais" class="form-control search-select multipleselect" >
            <?php foreach ($nucleos as $nf) { ?>
                <option value="<?php echo $nf->id ?>"><?php echo strtoupper($nf->nome) ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="col-md-4">
        <select required="required" multiple="multiple" id="selectFiliais" class="form-control search-select multipleselect" >
            <?php foreach ($filiais as $f) { ?>
                <option value="<?php echo $f->id ?>"><?php echo strtoupper($f->getConcat()) ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<br />
<div class="row">
    <div class="col-sm-12">
        <table id="gridFiliais" class="table table-striped table-bordered table-hover table-full-width dataTable">
            <thead>
                <tr>
                    <th class="no-orderable">
                        Filial
                    </th>
                    <th width="3%">
                    </th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<style>
    .btn-group, .multiselect{
        width: 100%!important;
    }
    .multiselect{
        text-align: center!important;
    }
    .dropdown-menu 
    {
        max-height  : 300px     ;
        padding     : 20px      ;
        overflow-y  : auto      ;
        overflow-x  : hidden    ;
    }
    .checkbox{
        margin-top: 0px!important;
        margin-bottom: 0px!important;
        margin-left: 0px!important;
    }
    #gridFiliais_length, #gridFiliais_filter, #gridFiliais_paginate, #gridFiliais_info{
        display: none;
    }
    td.details-control {
        background: url('../../images/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('../../images/details_close.png') no-repeat center center;
    }
</style>