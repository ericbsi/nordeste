<div class="row">
   <div class="col-sm-12">
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
               Cancelamentos
            </a>
         </li>
         <li class="active">
            Solicitações
         </li>
      </ol>
      <div class="page-header">
         <h1>
            Histórico de cancelamentos
         </h1>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-sm-12">
      <div class="row">
         <div class="col-md-2">
            <label class="control-label">
               Data de <span class="symbol required" aria-required="true"></span>
            </label>
            
         </div>
         <div class="col-md-2">
            <label class="control-label">Data até:</label>
         </div>
         <div class="col-md-2">
            <label class="control-label">
               Títulos Gerados?
            </label>
         </div>
         <div class="col-md-4">
            <label class="control-label">
               Filiais <span class="symbol required"></span>
            </label>
         </div>
         <div class="col-md-2">
            <label class="control-label">&nbsp;</label>
         </div>
      </div>
   </div>
   <div class="col-sm-12">
      <div class="row">
         <form id="form-filter">
            <div class="col-md-2">
               <div class="form-group">     

                  <div class="input-group">
                     <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                     </span>             
                     <input type="date" class="form-control" id="data_de">
                  </div>
               </div>
            </div>
            <div class="col-md-2">
               <div class="form-group">

                  <div class="input-group">
                     <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                     </span>
                     <input type="date" class="form-control" id="data_ate">
                  </div>
               </div>
            </div>
            <div class="col-md-2">
               <div class="form-group">
                  <select id="titulos_select" class="form-control">
                     <option value="A">Ambos</option>
                     <option value="S">Sim</option>
                     <option value="N">Não</option>
                  </select>
               </div>
            </div>
            <div class="col-md-4">
               <select multiple="multiple" id="filiais_select" class="form-control multipleselect" >
                  <?php foreach (Yii::app()->session['usuario']->getEmpresa()->listFiliais() as $f) { ?>
                     <option value="<?php echo $f->id ?>">
                        <?php echo $f->getConcat() ?>
                     </option>
                  <?php } ?>
               </select>
            </div>
            <div class="col-md-2">
               <div class="form-group">
                  <button id="btn-filter" class="btn btn-blue next-step btn-block">Filtrar</button>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>
  
<div class="row">
   <div class="col-sm-12">
      <table id="grid_solicitacoes" class="table table-striped table-bordered table-hover table-full-width dataTable search-box">
         <thead>
            <tr>
               <th class="no-orderable"></th>
               <th style="width:140px;" class="no-orderable">Proposta</th>
               <th style="width:140px;" class="no-orderable">CPF</th>
               <th style="width:070px;" class="no-orderable">Dt Venda</th>
               <th style="width:070px;" class="no-orderable">Emissao</th>
               <th style="width:070px;" class="no-orderable">Aceite</th>
               <th style="width:140px;" class="no-orderable">Solicitante</th>
               <th style="width:135px;" class="no-orderable">Filial</th>
               <th style="width:165px;" class="no-orderable">Concluído Por</th>
               <th style="width:115px;" class="no-orderable">Parcelamento</th>
               <th style="width:100px;" class="no-orderable">R$ Total</th>
               <th class="no-orderable">Status</th>
            </tr>
         </thead>
         <tbody></tbody>
      </table>
   </div>
</div>
<div class="row" style="margin:10px 0;">
</div>
<style type="text/css">
   #grid_solicitacoes_length
   {
      display: none;
   }
   td.details-control
   {
      padding:7px 13px!important;
      background: url('../../images/details_open.png') no-repeat center center;
      cursor: pointer;
   }
   tr.details td.details-control
   {
      background: url('../../images/details_close.png') no-repeat center center;
   }
   #grid_solicitacoes_length{
   }
</style>
