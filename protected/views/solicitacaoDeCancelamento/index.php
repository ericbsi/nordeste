<div class="row">
   <div class="col-sm-12">
      
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
               Cancelamentos
            </a>
         </li>
         <li class="active">
            Solicitações
         </li>
      </ol>
      <div class="page-header">
         <h1>
         	Solicitações pendentes
         </h1>
      </div>
   </div>
</div>
<form>
   <input type="hidden" name="task" value="1">
</form>
<div class="row">
   <div class="col-sm-12">
       <table id="grid_solicitacoes" class="table table-striped table-bordered table-hover table-full-width dataTable">
         <thead>
            <tr>
               <th class="no-orderable"></th>
               <th style="width:1%;" class="no-orderable">Financeira</th>
               <th style="width:060px;" class="no-orderable">Proposta</th>
               <th style="width:060px;" class="no-orderable">Data</th>
               <th style="width:155px;" class="no-orderable">Cliente</th>
               <th style="width:090px;" class="no-orderable">CPF</th>
               <th style="width:155px;" class="no-orderable">Solicitante</th>
               <th style="width:165px;" class="no-orderable">Filial</th>
               <th style="width:1%" class="no-orderable">Parcelamento</th>
               <th style="width:7%;" class="no-orderable">Valor total</th>
               <th class="no-orderable">Banco</th>
               <th style="width:125px;" class="no-orderable">Status</th>
               <th style="width:55px;" class="no-orderable"></th>
            </tr>
         </thead>
         <tbody></tbody>
      </table>
   </div>
</div>

<div id="confirm-modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" style="display: none;">
   <form id="form-efetivar" action="/solicitacaoDeCancelamento/efetivar/">
      <div class="modal-body">
         <p id="main-msg"></p>
         <input type="hidden" id="ipt_hdn_action" name="action">
         <input type="hidden" id="ipt_hdn_soli_id" name="solicitacaoId">
         <input type="hidden" id="" name="usuarioId" value="<?php echo Yii::app()->session['usuario']->id ?>">
         <textarea style="width: 100%" id="resposta_analista" name="resposta_analista" rows="4"></textarea>
      </div>
      <div class="modal-footer">
         <button type="button" data-dismiss="modal" class="btn btn-default">Cancelar</button>
         <button data-dismiss="modal" id="btn-confirm-option" class="btn btn-primary"></button>
      </div>
   </form>
</div>

<style type="text/css">
   #grid_solicitacoes_length, #grid_solicitacoes_filter
   {
      display: none;
   }
   td.details-control
   {
      padding:7px 13px!important;
      background: url('../../images/details_open.png') no-repeat center center;
      cursor: pointer;
   }
   tr.details td.details-control
   {
       background: url('../../images/details_close.png') no-repeat center center;
   }
   #grid_solicitacoes_length{

   }
   
   table tbody tr td{
       font-size: 9.5px!important;
   }
</style>