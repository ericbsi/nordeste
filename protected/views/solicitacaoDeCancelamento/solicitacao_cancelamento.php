<?php

	$util = new Util;

	$parcelasLeft 	= array();
	$parcelasRight 	= array();

	$arrMeses 		= array(
		'01' 		=> 'Jan',
		'02' 		=> 'Feb',
		'03' 		=> 'Mar',
		'04' 		=> 'Apr',
        '05' 		=> 'May',
        '06' 		=> 'Jun',
        '07' 		=> 'Jul',
        '08' 		=> 'Aug',
        '09' 		=> 'Sep',
        '10' 		=> 'Oct',
        '11' 		=> 'Nov',
        '12' 		=> 'Dec'
	);

	$numero_contrato;

	if(empty($proposta->numero_do_contrato)){
		$numero_contrato = $proposta->codigo;
	}else{
		$numero_contrato = $proposta->numero_do_contrato;
	}
?>
<div id="main-center" style="padding-top:0!important;margin-top:0!important;">
	<header style="padding-top:0!important;margin-top:0!important;">
		<h1 style="width:300px;float:left;margin-top:0!important;">
			<img src="<?php echo Yii::app()->getBaseUrl(true) ?>/images/credtresanos.png">
		</h1>
		<h2 style="float:right;">COMPROVANTE DE SOLICITAÇÃO DE CANCELAMENTO</h2>
	</header>
	<section id="contrato-section-one">
		<div style="float:left;width:50%">
			<p><span>Nome Completo: </span> <?php echo strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome) ?></p>
			<p><span>Número de Contrato: </span> <?php echo $numero_contrato ?></p>
			<p><span>Lojista: </span><?php echo $usuario->nomeFilial() ?></p>
			<p><span>Cidade/UF: </span><?php echo $usuario->returnFilial()->getEndereco()->cidade ?> / <?php echo $usuario->returnFilial()->getEndereco()->uf ?></p>
		</div>
		<div style="float:left;width:50%">
			<p><span>Solicitado por: </span><?php echo $usuario->nome_utilizador ?></p>
			<p><span>Data: </span><?php echo date('d/m/Y') ?></p>
			<p><span>Horário: </span><?php echo date('H:i:s') ?></p>
			<p><span>Número de Protocolo: </span><?php echo $proposta->codigo ?></p>
		</div>
		
	</section>
	<section id="contrato-section-two">
		<div style="float:left;width:80%">
			<p><span>Credora: </span> CREDSHOW OPERADORA DE CRÉDITO S/A</p>
			<p><span>Endereço: </span>Av Amintas Barros, 3700, B Corporate Tower Center - Business; 2103, Lagoa Nova,  Natal, RN, CEP 59075-810, Brasil</p>
		</div>
		<div style="float:left;width:20%">
			<p><span>Cidade/UF: </span> Natal/RN</p>
		</div>
	</section>
	
</div>