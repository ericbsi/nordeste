<?php ?>
<div class="row">
   <div class="col-sm-12">
      
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
               Estoque
            </a>
         </li>
         <li class="active">
            Fabricantes
         </li>
      </ol>
      <div class="page-header">
         <h1>
          Fabricantes
         </h1>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-sm-12">
      <p>
         <a data-toggle="modal" href="#modal_form_new_fabricante" class="btn btn-success">Cadastrar Fabricante <i class="fa fa-plus"></i></a>
      </p>
   </div>
</div>

<!--Grid-->
<div style="margin-bottom:30px" class="row">
   
   <div class="col-sm-12">
            
      <table id="grid_fabricantes" class="table table-striped table-bordered table-hover table-full-width dataTable">
         <thead>
            <tr>
               <th class="no-orderable">Fabricante</th>
               <th class="no-orderable">CNPJ</th>
               <th class="no-orderable"></th>
            </tr>
         </thead>
      </table>
   </div>
</div>

<!--Modal-->
<div id="modal_form_new_fabricante" class="modal fade" tabindex="-1" data-width="960" style="display: none;">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
      &times;
      </button>
      <h4 class="modal-title">Cadastrar Fabricante</h4>
   </div>
   <form id="form-add-fabricante">
      <div class="modal-body">
         <div class="row">
            <div class="row-centralize">
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">
                              Razão Social <span class="symbol required"></span>
                           </label>
                           <input required name="Fabricante[razao_social]" type="text" class="form-control">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">
                              Nome fantasia <span class="symbol required"></span>
                           </label>
                           <input required name="Fabricante[nome_fantasia]" type="text" class="form-control">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">CNPJ <span class="symbol required"></span></label>
                           <input required name="Fabricante[cnpj]" type="text" class="form-control" id="cnpj">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Inscrição Estadual</label>
                           <input name="Fabricante[inscricao_estadual]" type="text" class="form-control">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal-footer">
         <label class="checkbox-inline" style="float:left">
            <input id="checkbox_continuar" type="checkbox" value="">
            Continuar Cadastrando
         </label>
         <div class="panel"></div>
         <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
         <button type="submit" class="btn btn-blue">Salvar</button>
         <div class="row">
         </div>
         <br>
         <div id="cadastro_msg_return" class="alert" style="text-align:left">
         </div>
      </div>
   </form>
</div>

<style type="text/css">
   #grid_fabricantes_length, #grid_fabricantes_filter{display: none}
   
   .panel{
      background: transparent!important;
      border:none!important;
   }
   
   .col-md-3{
      padding-left: 0!important;
   }

</style>