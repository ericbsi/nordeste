<div class="row">

    <div class="btn-group btn-group-justified">

        <button id="aberto_btn"
                style="width: 50%"
                type="button"
                class="btn btnDiv btn-success waves-effect waves-light">
            <b>PARCELAS EM ABERTO</b>
        </button>

        <button id="recebido_btn"
                style="width: 50%"
                type="button"
                class="btn btnDiv btn-primary waves-effect waves-light">
            <b>RECEBIDOS</b>

    </div>

</div>
<div class="emAberto">
    <div class="row">
        <div class="col-sm-12">
            <ol class="breadcrumb">
                <li>
                        Recebimento Interno
                </li>
                <li class="active">
                    Parcelas
                </li>
            </ol>
            <div class="page-header" style="text-align: center!important;">
                <h3>
                    PARCELAS EM ABERTO
                </h3>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3"><input id="contrato" type="text" class="form-control" placeholder="Contrato"></div>
        <div class="col-md-3"><input id="cpf_cliente" type="text" class="form-control" placeholder="CPF do Cliente"></div>
        <div class="col-md-6"><button id="filtrar_parcelas" class="btn btn-info" style="width: 100%!important;">Pesquisar</button></div>
    </div>
    <br />
    <div class="row">
        <div class="col-sm-12">
            <table id="grid_parcelas"
                   class="table table-striped table-bordered table-hover table-full-width dataTable"
                   style="width: 100%; margin-bottom: 25px!important;">
                <thead>
                    <tr>
                        <th width="18%">Contrato</th>
                        <th>Cliente</th>
                        <th width="1%">Seq.</th>
                        <th width="5%">Vencimento</th>
                        <th width="10%">Valor</th>
                        <th width="10%">Dias em atraso</th>
                        <th width="10%">Juros por atraso</th>
                        <th width="10%">Valor a receber</th>
                        <th width="1%"></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="recebidos" style="display:none">
    <div class="row">
        <div class="col-sm-12">
            <ol class="breadcrumb">
                <li>
                        Recebimento Interno
                </li>
                <li class="active">
                    Recebidos
                </li>
            </ol>
            <div class="page-header" style="text-align: center!important;">
                <h3>
                    PARCELAS PAGAS
                </h3>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <table id="grid_recebidos"
                   class="table table-striped table-bordered table-hover table-full-width dataTable"
                   style="width: 100%; margin-bottom: 25px!important;">
                <thead>
                    <tr>
                        <th width="1%"></th>
                        <th>Financeira</th>
                        <th>Data</th>
                        <th>Valor Total</th>
                        <th width="1%"></th>
                        <th width="1%"></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
