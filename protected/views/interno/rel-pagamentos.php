<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li>
                    Acompanhamento
            </li>
            <li class="active">
                Recebidos
            </li>
        </ol>
        <div class="page-header" style="text-align: center!important;">
            <h3>
                RECEBIMENTOS REGISTRADOS
            </h3>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
      <input style="width: 100%!important;" type="hidden" class="select2 search-select" id="selectNucleoFiliais">
    </div>
    <div class="col-md-4">
      <input style="width: 100%!important;" type="hidden" class="select2 search-select" id="selectStatus">
    </div>
    <div class="col-md-4">
      <input style="width: 100%!important;" type="hidden" class="select2 search-select" id="selectFinanceiras">
    </div>
</div>

<br />

<div class="row">
  <div class="col-md-4">
    <input style="width: 100%!important;" type="hidden" class="select2 search-select" id="selectFiliais">
  </div>
  <div class="col-md-4">
    <input placeholder="De" type="text" name="data_de" class="form-control date-picker" data-date-viewmode="years" data-date-format="dd/mm/yyyy" id="data_de">
  </div>
  <div class="col-md-4">
    <input placeholder="Até" type="text" name="data_ate" class="form-control date-picker" data-date-viewmode="years" data-date-format="dd/mm/yyyy" id="data_ate">
  </div>
</div>

<br />

<div class="row">
  <div class="col-md-2">
      <button id="filtrar_recebimentos" class="btn btn-info" style="width: 100%!important;">Filtrar</button>
  </div>
</div>

<br />

<div class="row">
    <div class="col-sm-12">
        <!-- <table id="grid_recebidos" class="table table-striped table-bordered table-hover table-full-width dataTable" style="width: 100%; margin-bottom: 25px!important;"> -->
        <table id="grid_recebidos" class="table table-striped table-bordered table-hover table-full-width dataTable" style="width: 100%; margin-bottom: 25px!important;">
            <thead>
                <tr>
                    <th width="1%"></th>
                    <th>Filial</th>
                    <th>Financeira</th>
                    <th>Data</th>
                    <th>Valor Total</th>
                    <th width="1%"></th>
                </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th id="th-valor-total"></th>
              <th></th>
            </tfoot>
        </table>
    </div>
</div>
<style media="screen">
  #grid_recebidos_length, #grid_recebidos_filter{
    display: none;
  }
  #grid_recebidos_paginate, grid_recebidos_info{
      margin-bottom: 30px!important;
  }
</style>
