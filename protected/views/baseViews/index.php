<?php  ?>
<div class="row">
   <div class="col-sm-12">
      
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
               Breadcrumb
            </a>
         </li>
         <li class="active">
            Breadcrumb
         </li>
      </ol>
      <div class="page-header">
         <h1>Titulo da página</h1>
      </div>
   </div>
</div>
<div class="row">
	
	<div class="col-sm-12">

		<table id="datatables-exemplo" class="table table-striped table-bordered table-hover table-full-width dataTable">
			
			<thead>
			
			  <tr>
   	        <th>Col1</th>
   			  <th>Col2</th>
   	        <th>Col3</th>
			  </tr>

		    </thead>

          <tbody>
            
            <?php foreach ($registros as $registro): ?>
               
               <tr>
                  <td><?php echo $registro->id ?></td>
                  <td><?php echo $registro->tipo ?></td>
                  <td><?php echo $registro->data_cadastro ?></td>
               </tr>

            <?php endforeach ?>

          </tbody>

		</table>

	</div>

</div>