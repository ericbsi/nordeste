<?php
/* $tituloPagina   = "Contas a Pagar"  ;
  $colunaPagar    = "Pagar"           ;
  $vPagarReceber  = "Valores a Pagar" ; */
$ehFinanceiro = true;

$usuario = Usuario::model()->findByPk(Yii::app()->session['usuario']->id);

if ($usuario->tipo_id === "11") {
    /*    $tituloPagina   = "A Receber"           ;
      $colunaPagar    = ""                    ; */
    $ehFinanceiro = false;
//    $vPagarReceber  = "Valores a Receber"   ;
}
?>

<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/financeiro/">
                    Financeiro
                </a>
            </li>
            <li class="active">
                Pagamentos Efetuados
            </li>
        </ol>
    </div>
</div>

<?php if ($ehFinanceiro) { ?>
    <div class="row">
        <div class="col-sm-12">

            <div class="panel panel-default">

                <div class="panel-body">

                    <div class="row">

                        <div class="col-sm-3">
                            <form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/financeiro/contasPagar">
                                <button class="btn btn-icon btn-block">
                                    <i class="fa fa-money"></i>
                                    Contas a Pagar
                                </button>
                            </form>
                        </div>

                        <div class="col-sm-3">
                            <form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/financeiro/contasPagas">
                                <button class="btn btn-icon btn-block" disabled="disabled">
                                    <i class="fa fa-barcode"></i>
                                    Pagamentos Efetuados
                                </button>
                            </form>
                        </div>

                        <div class="col-sm-3">
                            <form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/emprestimo/pagarEmprestimo">
                                <button class="btn btn-icon btn-block">
                                    <i class="clip-transfer"></i>
                                    Pagar Empréstimos
                                </button>
                            </form>                            
                        </div>

                        <div class="col-sm-3">
                            <form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/emprestimo/liberarPagamento">
                                <button class="btn btn-icon btn-block">
                                    <i class="clip-transfer"></i>
                                    Liberar Pagamento
                                </button>
                            </form>                            
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
<?php } else { ?>
    <br>
<?php } ?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">

            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i>
                Totais Pagos
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse expand" href="#">
                    </a>
                </div>
            </div>

            <div class="panel-body collapse">
                <div id="demo"></div>
                <table id="grid_totais_pagos" 
                       class="table table-striped table-bordered table-hover table-full-width dataTable">

                    <thead>
                        <tr>
                            <th style="width : 03%!important"></th>
                            <th style="width : 62%!important">
                                Grupo
                            </th>
                            <th style="width : 35%!important">
                                Valor Total
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">

            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i>
                Lotes Pagos
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse expand" href="#">
                    </a>
                </div>
            </div>

            <div class="panel-body collapse">
                <input type="checkbox" id="porImagem">&nbsp;&nbsp;&nbsp;<span>Pagos por Imagem</span>
                <br />
                <input type="checkbox" id="fisicoIndefinido">&nbsp;&nbsp;&nbsp;<span>Contrato Físco ou Indefinidos</span>
                <table id="grid_lotes_pagos" 
                       class="table table-striped table-bordered table-hover table-full-width dataTable">

                    <thead>
                        <tr>
                            <th style="width : 03%!important" class="no-orderable">
                            </th>
                            <th style="width : 22%!important">
                                Núcleo
                            </th>
                            <th style="width : 10%!important">
                                Código
                            </th>
                            <th style="width : 20%!important">
                                <div class="row">
                                    <div class="col-sm-3">
                                        Data
                                    </div>
                                    <div class="col-sm-9">
                                        <input class="form form-control" type="date" id="dataBaixa" />
                                    </div>
                                </div>
                            </th>
                            <th style="width : 22%!important">
                                Responsável
                            </th>
                            <th style="width : 10%!important">
                                Qtd de Borderôs
                            </th>
                            <th style="width : 10%!important">
                                Valor
                            </th>
                            <th style="width : 10%!important">
                                Forma Pagamento
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">

            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i>
                Borderôs Pagos
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse expand" href="#">
                    </a>
                </div>
            </div>

            <div class="panel-body collapse">

                <table id="grid_borderos" 
                       class="table table-striped table-bordered table-hover table-full-width dataTable">

                    <thead>
                        <tr>
                            <td></td>
                            <td>
                                <input type="text" id="buscaParceiro" class="form form-control filtroBordero" placeholder="Filtrar Parceiro..."  />
                            </td>
                            <td>
                                <input type="text" id="buscaCodigo"   class="form form-control filtroBordero" placeholder="Filtrar Borderô..."   />
                            </td>
                            <td>
                                <input type="text" id="buscaDocumentacao"   class="form form-control filtroBordero" placeholder="Filtrar Documentação..."   />
                            </td>
                            <td>
                                <input type="date" id="buscaData"     class="form form-control filtroBordero"                                    />
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <th style="width : 03%!important">
                            </th>
                            <th style="width : 30%!important">
                                Parceiro
                            </th>
                            <th style="width : 15%!important">
                                Borderô
                            </th>
                            <th style="width : 15%!important">
                               Documentação
                            </th>
                            <th style="width : 20%!important">
                                Data
                            </th>
                            <th style="width : 22%!important">
                                Responsável
                            </th>
                            <th style="width : 10%!important">
                                Valor
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    #grid_totais_pagos_length, #grid_lotes_pagos_length, #grid_totais_pagos_filter, 
    #grid_borderos_length, #grid_borderos_filter ,
    #gridPropostas_length, #gridPropostas_filter 
    {
        display: none!important;
    }
    td.details-control {
        background: url('../../images/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('../../images/details_close.png') no-repeat center center;
    }
    input[type=checkbox] {
        transform: scale(1.5);
    }
</style>