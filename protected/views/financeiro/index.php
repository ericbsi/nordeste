<div class="row">
    <div class="col-sm-12">
        <!-- start: PAGE TITLE & BREADCRUMB -->
        <ol class="breadcrumb">
            <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/financeiro/">Financeiro</a></li>
            <li class="active">Home</li>
        </ol>
<!--        <div class="page-header">
            <h1>Painel Financeiro</h1>
        </div>
         end: PAGE TITLE & BREADCRUMB -->
    </div>
</div>
<br>
<div class="row">
    <div class="col-sm-12">

        <div class="panel panel-default">

            <!--<div class="panel-heading">
                <i class="fa fa-money"></i>
                Financeiro
            </div>-->

            <div class="panel-body">
                <div class="row">

                    <div class="col-sm-3">
                        <button class="btn btn-icon btn-block">
                            <i class="clip-stack-empty"></i>
                            Propostas hoje 
                        </button>
                    </div>

                    <div class="col-sm-3">
                        <form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/financeiro/contasAReceber">
                            <button class="btn btn-icon btn-block">
                                <i class="fa fa-money"></i>
                                Contas a receber
                            </button>
                        </form>
                    </div>

                    <div class="col-sm-3">
                        <form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/financeiro/contasPagar">
                            <button class="btn btn-icon btn-block">
                                <i class="fa fa-barcode"></i>
                                Contas a Pagar 
                            </button>
                        </form>
                    </div>

                    <div class="col-sm-3">
                        <form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/financeiro/despesas">
                            <button class="btn btn-icon btn-block">
                                <i class="fa fa-credit-card"></i>
                                Despesas 
                            </button>
                        </form>
                    </div>

                </div>
                
            </div>
        </div>

    </div>
</div>