<?php 

$tituloPagina   = "Contas a Pagar"  ;
$colunaPagar    = "Pagar"           ;
$vPagarReceber  = "Valores a Pagar" ;
$ehFinanceiro   = true              ;
    
/*ob_start();
var_dump(Yii::app()->session['usuario']);
$result = ob_get_clean();

echo $result;*/

$usuario = Usuario::model()->findByPk(Yii::app()->session['usuario']->id);
    
/*ob_start();
var_dump($usuario);
$result = ob_get_clean();

echo $result;*/

if($usuario->tipo_id === "11")
{
    $tituloPagina   = "A Receber"           ;
    $colunaPagar    = ""                    ;
    $ehFinanceiro   = false                 ;
    $vPagarReceber  = "Valores a Receber"   ;
//    echo "tipo_id = " . Yii::app()->session['usuario']->tipo_id;
    
/*    ob_start();
    var_dump(Yii::app()->session['usuario']);
    $result = ob_get_clean();
    
    echo $result;*/
}

?>


<div class="row">
   <div class="col-sm-12">
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/financeiro/">
               Financeiro
            </a>
         </li>
         <li class="active">
            <?php echo $tituloPagina; ?>
         </li>
      </ol>
   </div>
</div>

<?php if($ehFinanceiro) {?>
    <div class="row">
        <div class="col-sm-12">

            <div class="panel panel-default">

                <div class="panel-body">

                    <div class="row">

                        <div class="col-sm-3">
                            <form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/financeiro/contasPagar">
                                <button class="btn btn-icon btn-block" disabled="disabled">
                                    <i class="fa fa-money"></i>
                                    Contas a Pagar
                                </button>
                            </form>
                        </div>

                        <div class="col-sm-3">
                            <form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/financeiro/contasPagas">
                               <button class="btn btn-icon btn-block">
                                    <i class="fa fa-barcode"></i>
                                    Pagamentos Efetuados
                                </button>
                            </form>
                        </div>

                        <div class="col-sm-3">
                            <form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/emprestimo/pagarEmprestimo">
                                <button class="btn btn-icon btn-block">
                                    <i class="clip-transfer"></i>
                                    Pagar Empréstimos
                                </button>
                            </form>                            
                        </div>

                        <div class="col-sm-3">
                            <form method="POST" action="<?php echo Yii::app()->request->baseUrl; ?>/emprestimo/liberarPagamento">
                                <button class="btn btn-icon btn-block">
                                    <i class="clip-transfer"></i>
                                    Liberar Pagamento
                                </button>
                            </form>                            
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
<?php }else{ ?>
    <br>
<?php } ?>

<div class="row">
   <div class="col-sm-12">
      <div class="panel panel-default">

         <div class="panel-heading">
            <i class="fa fa-external-link-square"></i>
            <?php echo $vPagarReceber; ?>
            <div class="panel-tools">
               <a class="btn btn-xs btn-link panel-collapse expand" href="#">
               </a>
            </div>
         </div>

         <div class="panel-body collapse">
            <table id="grid_pagar" 
                   class="table table-striped table-bordered table-hover table-full-width dataTable">

               <thead>
                  <tr>
                     <th style="width : 35%!important">
                        Grupo
                     </th>
                     <th style="width : 15%!important">
                        Valor Aberto
                     </th>
                     <th style="width : 15%!important">
                        Valor Borderô
                     </th>
                     <th style="width : 15%!important">
                        Valor Lote
                     </th>
                     <th style="width : 20%!important">
                        Valor Total
                     </th>
                  </tr>
               </thead>
               <tbody>
               </tbody>
            </table>
         </div>
      </div>
   </div>
</div>

<?php if($ehFinanceiro) {?>
    <div class="row">
       <div class="col-sm-12">
          <div class="panel panel-default">

             <div class="panel-heading">
                <i class="fa fa-external-link-square"></i>
                Gerar Lotes
                <div class="panel-tools">
                   <a class="btn btn-xs btn-link panel-collapse expand" href="#">
                   </a>
                </div>
             </div>

             <div class="panel-body collapse">

                <table id="grid_borderos" 
                       class="table table-striped table-bordered table-hover table-full-width dataTable">

                   <thead>
                      <tr>
                         <td colspan="6" id="tdGerarLotes">
                            <button 
                               class="btn btn-google-plus" 
                               id="btn-bordero-selects" 
                               style="margin:0 0 8px 8px"
                               disabled="disabled">

                               <i class="clip-note"></i> |
                               Processar Borderôs Selecionados
                            </button>
                         </td>
                      </tr>
                      <tr>
                         <td></td>
                         <td>
                            <input 
                                type="text" 
                                id="buscaParceiro" 
                                class="form form-control filtroBordero" 
                                placeholder="Filtrar Parceiro..."  />
                         </td>
                         <td>
                            <input 
                                type="text" 
                                id="buscaCodigo"   
                                class="form form-control filtroBordero" 
                                placeholder="Filtrar Borderô..."   />
                         </td>
                         <td>
                            <input 
                                type="text" 
                                id="buscaDocumentacao"   
                                class="form form-control filtroBordero" 
                                placeholder="Filtrar Borderô..."   />
                         </td>
                         <td>
                            <input 
                                type="date" 
                                id="buscaData"     
                                class="form form-control filtroBordero"                                    />
                         </td>
                         <td></td>
                         <td></td>
                      </tr>
                      <tr>
                         <th style="width : 03%!important">
                         </th>
                         <th style="width : 30%!important">
                            Parceiro
                         </th>
                         <th style="width : 15%!important">
                            Borderô
                         </th>
                         <th style="width : 15%!important">
                            Documentação
                         </th>
                         <th style="width : 20%!important">
                            Data
                         </th>
                         <th style="width : 22%!important">
                            Responsável
                         </th>
                         <th style="width : 10%!important">
                            Valor
                         </th>
                      </tr>
                   </thead>
                   <tbody>
                   </tbody>
                </table>
             </div>
          </div>
       </div>
    </div>
<?php } ?>
    
<div class="row">
   <div class="col-sm-12">
      <div class="panel panel-default">

         <div class="panel-heading">
            <i class="fa fa-external-link-square"></i>
            Lotes
            <div class="panel-tools">
               <a class="btn btn-xs btn-link panel-collapse expand" href="#">
               </a>
            </div>
         </div>

         <div class="panel-body collapse">

            <table id="grid_lotes" 
                   class="table table-striped table-bordered table-hover table-full-width dataTable">

               <thead>
                  <tr>
                     <th width="03%" class="no-orderable">
                        <?php echo $colunaPagar; ?>
                     </th>
                     <th style="width : 22%!important">
                        Núcleo
                     </th>
                     <th style="width : 10%!important">
                        Código
                     </th>
                     <th style="width : 20%!important">
                        Data
                     </th>
                     <th style="width : 22%!important">
                        Dados Bancários
                     </th>
                     <th style="width : 10%!important">
                        CGC D Bancários
                     </th>
                     <th style="width : 10%!important">
                        Valor
                     </th>
                     <th width="03%">
                     </th>
                  </tr>
               </thead>
               <tbody>
               </tbody>
               <tfooter>
                 <tr>
                   <th></th>
                   <th></th>
                   <th></th>
                   <th></th>
                   <th></th>
                   <th></th>
                   <th id="th-total"></th>
                   <th></th>
                 </tr>
               </tfooter>
            </table>
         </div>
      </div>
   </div>
</div>

<div id="modal_form_new_att" 
     class="modal fade"
     data-backdrop="static"
     tabindex="-1" 
     data-width="560" 
     style="display: none;">
     
     <div class="modal-header">
        <h4 class="modal-title">Anexar comprovante</h4>
     </div>

     <form action="/lotePagamento/anexarComprovante/" method="POST" enctype="multipart/form-data" id="form-add-att">
        <div class="modal-body">
           <div class="row">
              <div class="row-centralize">
                 <div class="col-md-12">
                    <div class="row">
                       <div class="col-md-12">
                          <div class="form-group">
                             <label class="control-label">Arquivo:</label>
                             <input required="required" data-icon="false" class="control-label filestyle" name="ComprovanteFile" id="ComprovanteFile" type="file" />
                             <input type="hidden" name="DadosPagamentoId" id="DadosPagamentoId" value="">
                          </div>
                       </div>
                    </div>
                 </div>
              </div>
           </div>
        </div>
        <div class="modal-footer">
           <button type="submit" class="btn btn-blue btn-send">Salvar</button>
        </div>
     </form>
</div>

<style type="text/css">
   #grid_pagar_length, #grid_borderos_filter, #grid_borderos_length, #grid_lotes_filter, #grid_lotes_length{
      display: none!important;
   }
</style>

<?php if(Yii::app()->session['usuario']->tipo_id !== 11){ ?>
    <style type="text/css">
       td.details-control {
          background: url('../../images/details_open.png') no-repeat center center;
          cursor: pointer;
       }
       tr.shown td.details-control {
          background: url('../../images/details_close.png') no-repeat center center;
       }
    </style>
<?php } ?>