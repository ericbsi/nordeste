<?php  ?>
<div class="row">
   <div class="col-sm-12">
      <!-- start: PAGE TITLE & BREADCRUMB -->
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="<?php echo Yii::app()->request->baseUrl;?>/financeiro/">
            Financeiro
            </a>
         </li>
         <li class="active">
            Arquivos Retorno
         </li>
      </ol>
      <div class="page-header">
         <h1>Arquivos Retorno / <small>Importar</small> </h1>
      </div>      
      <!-- end: PAGE TITLE & BREADCRUMB -->
   </div>
</div>
<div class="alert alert-info">
   <i class="fa fa-info-circle"></i>
   <!--<strong>Importante!</strong> Ao importar um arquivo, você pode optar por automaticamente relizar a baixa dos títulos contidos no arquivo ou, então, acumular os títulos e baixá-los em outro momento.-->
</div>
<div class="row">
   <div class="col-md-12">
      <!-- start: ALERTS PANEL -->
      <div class="panel panel-default">
         <div class="panel-heading">
            <i class="fa fa-bullhorn"></i>
            Importar arquivo
         </div>

         <div class="panel-body">
            <form action="<?php echo Yii::app()->request->baseUrl; ?>/financeiro/cnabRetorno" enctype="multipart/form-data" role="form" class="form-horizontal" method="POST">
               <div class="form-group">
                  <div class="col-sm-4">
                     <label></label>
                     <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="input-group">
                           
                           <div class="form-control uneditable-input">
                              <i class="fa fa-file fileupload-exists"></i>
                              <span class="fileupload-preview"></span>
                           </div>

                           <div class="input-group-btn">
                              <div class="btn btn-light-grey btn-file">
                                 <span class="fileupload-new"><i class="fa fa-folder-open-o"></i> Selecionar</span>
                                 <span class="fileupload-exists"><i class="fa fa-folder-open-o"></i> Mudar</span>
                                 <input required type="file" class="file-input" name="arquivoRem">
                              </div>
                              <a href="#" class="btn btn-light-grey fileupload-exists" data-dismiss="fileupload">
                                 <i class="fa fa-times"></i> Remover
                              </a>
                           </div>

                        </div>
                     </div>
                  </div>                  
               </div> 
               <div class="form-group">
                  <div class="col-md-2">
                       <button class="btn btn-yellow btn-block" type="submit">
                       Enviar <i class="fa fa-arrow-circle-right"></i>
                       </button>
                  </div>
               <div>
            </form>
         </div>
      </div>
      <!-- end: ALERTS PANEL -->
   </div>
</div>
<div class="row">
   <div class="col-sm-12">
   
      <?php
         echo '<pre>';
         print_r($retorno);
         echo '</pre>';
      ?>

   </div>
</div>