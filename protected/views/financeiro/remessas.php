<div class="row">
   <div class="col-md-12">
      <div class="row">
         <div class="col-sm-12">
            
            <ol class="breadcrumb">
               <li>
                  <i class="clip-pencil"></i>
                  <a href="<?php echo Yii::app()->request->baseUrl;?>/financeiro/">
                  Financeiro
                  </a>
               </li>
               <li class="active">
                  Arquivos remessa
               </li>
            </ol>
            <div class="page-header">
               <h1>Remessas Bancárias</h1>
            </div>
            
         </div>
      </div>

   </div>
</div>
<div class="row">
   <div class="col-sm-12">
      <a data-toggle="modal" href="#modal_form_new_remessa" class="btn btn-primary">
         Gerar remessa <i class="clip-stack"></i>
      </a>
   </div>
</div>
<br>
<br>
<div class="row">
   <div class="col-sm-9">
      <div class="row">
         <form id="form-filter" action="">
            <div class="col-md-3" style="width:15%">
               <div class="form-group">
                  <label class="control-label">
                     Data de <span class="symbol required" aria-required="true"></span>
                  </label>                  
                  <input style="width:120px" type="text" name="data_de" class="form-control date-picker" data-date-viewmode="years" data-date-format="dd/mm/yyyy" id="data_de">
               </div>
            </div>
            <div class="col-md-3" style="width:15%">
               <div class="form-group">
                  <label class="control-label">Data até:</label>
                  <input style="width:120px" type="text" name="data_ate" class="form-control date-picker" data-date-viewmode="years" data-date-format="dd/mm/yyyy" id="data_ate">
               </div>
            </div>
            <div class="col-md-3">
               <div class="form-group">
                  <label class="control-label">
                     Banco <span class="symbol required"></span>
                  </label>
                  <select required="required" id="bancos_select" class="form-control multipleselect" name="Banco">
                     <?php foreach ( Banco::model()->findAll('id = 5 OR id = 7') as $b ) { ?>
                        <option value="<?php echo $b->id ?>"><?php echo strtoupper($b->nome) ?></option>
                     <?php } ?>
                  </select>  
               </div>
            </div>
            <div class="col-md-3" style="width:15%">
               <div class="form-group">
                  <label class="control-label">&nbsp;</label>
                  <button id="btn-filter" class="btn btn-blue next-step btn-block">Filtrar</button>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>
<div class="row" style="margin-bottom:40px">
   <div class="col-sm-12">
      <table id="grid_arquivos_remessa" class="table table-striped table-bordered table-hover table-full-width dataTable">
         <thead>
               <tr>
                  <th class="no-orderable" width="40">Data Criação</th>
                  <th class="no-orderable" width="200">Criado por</th>
                  <th class="no-orderable" width="200">Banco</th>
               </tr>
         </thead>
         <tbody>
         </tbody>
      </table>

   </div>
</div>

<div id="modal_form_new_remessa" class="modal fade" tabindex="-1" data-width="780" style="display: none;">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
      &times;
      </button>
      <h4 class="modal-title">Gerar remessa</h4>
   </div>
   <form id="form-add-remessa">
      <div class="modal-body">
         <div class="row">
            <div class="row-centralize">
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-4">
                        <div class="form-group">
                           <label class="control-label">
                              Banco <span class="symbol required"></span>
                           </label>
                           <select required="required" id="bancos_select2" class="form-control multipleselect" name="Banco">
                              <?php foreach ( Banco::model()->findAll('id = 5 OR id = 7 OR id = 10') as $b ) { ?>
                                 <option value="<?php echo $b->id ?>"><?php echo strtoupper($b->nome) ?></option>
                              <?php } ?>
                           </select>  
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label class="control-label">
                              Data de <span class="symbol required" aria-required="true"></span>
                           </label>
                           <input required="required" style="width:220px" type="text" name="gerar_data_de" class="form-control date-picker" data-date-viewmode="years" data-date-format="dd/mm/yyyy" id="gerar_data_de">
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label class="control-label">
                              Data ate <span class="symbol required" aria-required="true"></span>
                           </label>
                           <input required="required" style="width:220px" type="text" name="gerar_data_ate" class="form-control date-picker" data-date-viewmode="years" data-date-format="dd/mm/yyyy" id="gerar_data_ate">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label class="control-label">
                              Nome do arquivo <span class="symbol required"></span>
                           </label>
                           <input required="required" type="text" name="gerar_nome_do_arquivo" class="form-control" id="gerar_nome_do_arquivo">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal-footer">
         <button type="button" data-dismiss="modal" class="btn btn-light-grey">Cancelar</button>
         <button type="submit" class="btn btn-blue">Salvar</button>
         <div class="row">
         </div>
         <br>
         <div id="cadastro_msg_return" class="alert" style="text-align:left">
         </div>
      </div>
   </form>
</div>

<style type="text/css">
   .dropdown-menu {
      max-height: 250px;
      overflow-y: auto;
      overflow-x: hidden;
   }
   #grid_arquivos_remessa_length,
   #grid_arquivos_remessa_filter{
      display: none;
   }
</style>