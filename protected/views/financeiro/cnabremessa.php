<div class="row">
   <div class="col-md-12">
      <div class="row">
         <div class="col-sm-12">
            <!-- start: PAGE TITLE & BREADCRUMB -->
            <ol class="breadcrumb">
               <li>
                  <i class="clip-pencil"></i>
                  <a href="<?php echo Yii::app()->request->baseUrl;?>/financeiro/">
                  Financeiro
                  </a>
               </li>
               <li class="active">
                  Arquivos remessa
               </li>
            </ol>
            <div class="page-header">
               <h1>Arquivos remessa</h1>
            </div>
            <!-- end: PAGE TITLE & BREADCRUMB -->
         </div>
      </div>
      <!-- start: DYNAMIC TABLE PANEL -->
      <!-- end: DYNAMIC TABLE PANEL -->
   </div>
</div>
<div class="row">
   <div class="col-md-12">
      <!-- start: DYNAMIC TABLE PANEL -->
      <div class="panel panel-default">
         <div class="panel-heading">
            <i class="fa fa-external-link-square"></i>
            Arquivos remessa
            <div class="panel-tools">
               <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
               </a>
               <a class="btn btn-xs btn-link panel-close" href="#">
               <i class="fa fa-times"></i>
               </a>
            </div>
         </div>
         <div class="panel-body">
            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
               <thead>
                  <tr>
                     <th>Data da geração</th>
                     <th class="hidden-xs">Gerado por</th>
                  </tr>
               </thead>
               <tbody>
               </tbody>
            </table>
            <button <?php if ($this->actionCheckRemessaJaGerada() == 1) {echo "disabled='disabled'";} ?> id="btn-create-remessa" type="button" class="btn btn-primary">Gerar arquivo de remessa</button>
            <span id="span-loading"></span>
            <br><br>
            <?php if ($this->actionCheckRemessaJaGerada() == 0) { ?>
            <div id="msg-remessa-gerada" class="alert alert-warning">
               <i id="ico_err"  class="fa fa-exclamation-triangle"></i>
               <span id="msg-remessa-gerada">
               <strong>Atenção!</strong> O arquivo de remessa ainda não foi gerado hoje.
               </span>
            </div>
            <?php } else{?>
            <div class="alert alert-success">
               <i id="ico_ok" class="fa fa-check-circle"></i> 
               <strong>Feito!<strong> Arquivo de remessa já foi gerado hoje.
            </div>
            <?php } ?>
         </div>
      </div>
      <!-- end: DYNAMIC TABLE PANEL -->
   </div>
</div>
<style>
   #msg-remessa-gerada{
   -webkit-transition: all 1.5s ease;
   -moz-transition: all 1.5s ease;
   -o-transition: all 1.5s ease;
   transition: all 1.5s ease;
   }
</style>