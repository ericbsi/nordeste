<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/financeiro/">
                    Financeiro
                </a>
            </li>
            <li class="active">
                Despesas
            </li>
        </ol>
    </div>
</div>

<p></p>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">

            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i>
                Nova Despesa
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse expand" href="#">
                    </a>
                </div>
            </div>

            <div class="panel-body collapse">
                <div class="tabbable">
                    <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="painelFornecedor">
                        <li id="liDados" class="active">
                            <a data-toggle="tab" href="#painel_despesa">
                                Lançar
                            </a>
                        </li>
                        <buton id="btnSalvar" class="btn btn-green">
                            <i class="fa fa-save"></i> Salvar
                        </buton>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="painel_despesa" class="tab-pane in active">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-sm-6">
                                            <label class="control-label">
                                                Descrição : 
                                                <span class="symbol required"></span>
                                            </label>
                                            <input 
                                                required
                                                type="text" 
                                                placeholder="Despesa..." 
                                                id="descricao" 
                                                class="form form-control" />
                                            <span for="descricao" class="help-block valid"></span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="control-label">
                                                Fornecedor : 
                                                <span class="symbol required"></span>
                                            </label>
                                            <select 
                                                id="fornecedor"
                                                class="form form-control search-select select2 fornecedorSelect">
                                                <option value="0">
                                                    Selecione...
                                                </option>
                                                <?php foreach (
                                                        Fornecedor::
                                                        model()->findAll(
                                                                'habilitado AND TipoFornecedor_id = 1'
                                                                ) as $fornecedor) { ?>
                                                    <option value="<?php echo $fornecedor->id; ?>">
                                                        <?php echo $fornecedor->pessoa->nome; ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                            <span for="fornecedor" class="help-block valid"></span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="control-label">
                                                Natureza : 
                                                <span class="symbol required"></span>
                                            </label>
                                            <select 
                                                id="natureza"
                                                class="form form-control search-select select2 naturezaSelect">
                                                <option value="0">
                                                    Selecione...
                                                </option>
                                                <?php foreach (
                                                        NaturezaTitulo::
                                                        model()->findAll(
                                                                "habilitado AND Left(codigo,1) = '2' AND NaturezaTitulo_id IS NOT NULL" 
                                                                ) as $natureza) { ?>
                                                    <option value="<?php echo $natureza->id; ?>">
                                                        <?php echo $natureza->codigo . ' ' . $natureza->descricao; ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                            <span for="natureza" class="help-block valid"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="control-label">
                                                Data : 
                                                <span class="symbol required" aria-required="true"></span>
                                            </label>
                                            <input 
                                                required
                                                type="date" 
                                                placeholder="Data..." 
                                                id="dataDespesa" 
                                                class="form form-control" />
                                            <span for="dataDespesa" class="help-block valid"></span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="col-sm-1">
                                            <label class="control-label">
                                                Série : 
                                            </label>
                                            <input 
                                                required
                                                type="text" 
                                                id="serieNF" 
                                                class="form form-control"/>
                                            <span for="serieNF" class="help-block valid"></span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="col-sm-2">
                                            <label class="control-label">
                                                Documento : 
                                            </label>
                                            <input 
                                                required
                                                type="text" 
                                                id="docNF"
                                                class="form form-control"/>
                                            <span for="docNF" class="help-block valid"></span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="control-label">
                                                Valor : 
                                                <span class="symbol required" aria-required="true"></span>
                                            </label>
                                            <input 
                                                required
                                                type="number" 
                                                id="valor" 
                                                class="form form-control"
                                                value="0"/>
                                            <span for="valor" class="help-block valid"></span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="col-sm-1">
                                            <label class="control-label">
                                                Rateio : 
                                                <span class="symbol required"></span>
                                            </label>
                                            <input 
                                                required
                                                type="number" 
                                                id="rateio" 
                                                class="form form-control"
                                                value="1"/>
                                            <span for="rateio" class="help-block valid"></span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="col-sm-1">
                                            <label class="control-label">
                                                Carência : 
                                            </label>
                                            <input 
                                                required
                                                type="number" 
                                                id="carencia" 
                                                class="form form-control"
                                                value="0"/>
                                            <span for="carencia" class="help-block valid"></span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="col-sm-1">
                                            <label class="control-label">
                                                Intervalo : 
                                            </label>
                                            <input 
                                                required
                                                type="number" 
                                                id="intervalo" 
                                                class="form form-control"
                                                value="0"/>
                                            <span for="intervalo" class="help-block valid"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-12">
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">

            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i>
                Despesas
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse expand" href="#">
                    </a>
                </div>
            </div>

            <div class="panel-body collapse">
                <table id="grid_despesas" 
                       class="table table-striped table-bordered table-hover table-full-width dataTable">
                    <thead>
                        <tr>
                            <th>

                            </th>
                            <th>
                                <input 
                                    class="filtro"
                                    type="text" 
                                    placeholder="Filtre Despesa..." 
                                    id="filtroDespesa" 
                                    class="form form-control" />
                            </th>
                            <th>
                                <input 
                                    class="filtro"
                                    type="text" 
                                    placeholder="Filtre Fornecedor..." 
                                    id="filtroFornecedor" 
                                    class="form form-control" />
                            </th>
                            <th>
                                <input 
                                    class="filtro"
                                    type="text" 
                                    placeholder="Filtre Natureza..." 
                                    id="filtroNatureza" 
                                    class="form form-control" />
                            </th>
                            <th>
                                <input 
                                    class="filtro"
                                    type="date" 
                                    placeholder="Filtre Data..." 
                                    id="filtroData" 
                                    class="form form-control" />
                            </th>
                            <th>
                                <input 
                                    class="filtro"
                                    type="text" 
                                    placeholder="Filtre Série..." 
                                    id="filtroSerie" 
                                    class="form form-control" />
                            </th>
                            <th>
                                <input 
                                    class="filtro"
                                    type="text" 
                                    placeholder="Filtre Documento..." 
                                    id="filtroDocumento" 
                                    class="form form-control" />
                            </th>
                            <th width="3%">
                            </th>
                        </tr>
                        <tr>
                            <th>    
                            </th>
                            <th class="no-orderable">
                                Despesa
                            </th>
                            <th class="no-orderable">
                                Fornecedor
                            </th>
                            <th class="no-orderable">
                                Natureza
                            </th>
                            <th class="no-orderable">
                                Data
                            </th>
                            <th class="no-orderable">
                                Série
                            </th>
                            <th class="no-orderable">
                                Documento
                            </th>
                            <th class="no-orderable">
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">

    <div class="panel-heading">
        <i class="fa fa-external-link-square"></i>
        A pagar
        <div class="panel-tools">
            <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
            </a>
        </div>
    </div>

    <div class="panel-body">
                
        <div class="row">
            <div class="col-sm-12"  style="text-align: left">

                <font color="gray">
                    <b>Legendas: </b>
                </font>

                <i class="clip-circle-2" style="color: blue"></i>
                <font color="blue">
                    <b>Parcelas vencendo</b>
                </font>

                <i class="clip-circle-2" style="color: green"></i>
                <font color="green">
                    <b>Parcelas pagas</b>
                </font>

                <i class="clip-circle-2" style="color: red"></i>
                <font color="red">
                    <b>Parcelas vencidas</b>
                </font>

            </div>
        </div>
        
        <div class="row">
            
            <div class="col-sm-12" style="text-align: right">

                <font color="gray">
                    <b>Filtros: </b>
                </font>

                <i class="clip-checkbox-unchecked-2 checkFiltro"
                   style="color: crimson"
                   id="checkFiltroCGC"
                   value="1"></i>
                <font color="crimson">
                    <b>Vencidas</b>
                </font>

                <i class="clip-checkbox-unchecked-2 checkFiltro"
                   style="color: blueviolet"
                   id="checkFiltroNome"
                   value="2"></i>
                <font color="blueviolet">
                    <b>Vencendo</b>
                </font>

                <i class="clip-checkbox-unchecked-2 checkFiltro"
                   style="color: chocolate"
                   id="checkFiltroValor"
                   value="3"></i>
                <font color="chocolate">
                    <b>Pagas</b>
                </font>

                <i class="clip-checkbox-unchecked-2 checkFiltro"
                   style="color: tomato"
                   id="checkFiltroValor"
                   value="4"></i>
                <font color="tomato">
                    <b>A vencer</b>
                </font>
                
            </div>
        </div>  
        <div class="row">
            <div class="col-sm-12">
                <table id="grid_parcelas" 
                       class="table table-striped table-bordered table-hover table-full-width dataTable">
                    <thead>
                        <tr>
                            <th>
                                <input 
                                    class="filtro"
                                    type="text" 
                                    placeholder="Filtre Despesa..." 
                                    id="filtroDespesa2" 
                                    class="form form-control" />
                            </th>
                            <th>
                            </th>
                            <th>
                                De:
                                <input 
                                    class="filtro"
                                    type="date" 
                                    placeholder="Filtre Vencimento..." 
                                    id="filtroVencimento" 
                                    class="form form-control" />
                                Até:
                                <input 
                                    class="filtro"
                                    type="date" 
                                    placeholder="Filtre Vencimento..." 
                                    id="filtroVencimento2" 
                                    class="form form-control" />
                            </th>
                            <th>
                            </th>
                            <th>
                                De:
                                <input 
                                    class="filtro"
                                    type="date" 
                                    placeholder="Filtre Baixa..." 
                                    id="filtroDataBaixa" 
                                    class="form form-control" />
                                Até:
                                <input 
                                    class="filtro"
                                    type="date" 
                                    placeholder="Filtre Baixa..." 
                                    id="filtroDataBaixa2" 
                                    class="form form-control" />
                            </th>
                            <th>
                            </th>
                            <th>
                            </th>
                            <th>
                            </th>
                        </tr>
                        <tr>
                            <th class="no-orderable">
                                Despesa
                            </th>
                            <th class="no-orderable" width="03%">
                                Parcela
                            </th>
                            <th class="no-orderable" width="10%">
                                Vencimento
                            </th>
                            <th class="no-orderable" width="07%">
                                Valor
                            </th>
                            <th class="no-orderable" width="05%">
                                Data Baixa
                            </th>
                            <th class="no-orderable" width="10%">
                                Valor Juros
                            </th>
                            <th class="no-orderable" width="10%">
                                Valor Desconto
                            </th>
                            <th class="no-orderable" width="10%">
                                Valor Baixa
                            </th>
                            <th class="no-orderable" width="03%">
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<br>
<br>

<style type="text/css">
    #grid_despesas_filter   , #grid_despesas_length, #grid_parcelas_filter, #grid_parcelas_length, 
    #parcelas_filter        , #parcelas_length
    {
        display: none;
    }
    td.details-control {
        background: url('../../images/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('../../images/details_close.png') no-repeat center center;
    }
</style>