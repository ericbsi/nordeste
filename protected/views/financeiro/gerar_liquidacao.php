<?php ?>

<hr />

<div class="row" style="border:1px solid #5bc0de; margin: 1px; overflow-x: hidden">

    <br>
    
    <div class="row">
        
        <div class="col-md-12">
    
            <form id="form_dp" action="/baixa/importarDP" method="post" enctype="multipart/form-data">
    
                <div class="row" style="margin: 1px">

                    <div class="col-sm-3">
                        <input class="form-control" type="file" id="anexo_importacao" name="anexo_importacao" />
                    </div>

                    <div class="col-sm-9">
                        <button class="btn btn-info" type="submit" style="width: 100%">
                            <i class="fa fa-arrow-circle-down" aria-hidden="true"></i> Importar (DP)
                        </button>
                    </div>
                    
                </div>

            </form>
            
        </div>
        
    </div>
    
    <br>
    
    <div class="row" style="margin: 1px">
    
        <div class="col-md-12">
        
            <table id="grid_dePara" class="table table-striped table-hover dataTable">

                <thead>

                    <th width="33%">Data de Importação</th>
                    <th width="33%">Qtd. Titulos</th>
                    <th width="33%">Usuário</th>
                    <th width="1%"></th>

                </thead>

                <tbody>
                </tbody>

            </table>
            
        </div>
        
    </div>
    
</div>

<br />
<br />

<!--<div class="row" style="border:1px solid #5cb85c; margin: 1px; overflow-x: hidden">
    
    <br>
    
    <div class="row" style="margin: 1px">
        
        <div class="col-md-12">
        
            <button style="width: 100%" id="botaoGerar" class="btn btn-success">
                Gerar Arquivo de Baixas
            </button>

        </div>
        
    </div>
    
    <br>
    
    <div class="row" style="margin: 1px">
    
        <div class="col-md-12">
        
            <table id="grid_gerados" class="table table-striped table-hover dataTable">

                <thead>

                    <th width="33%">Data de Geração</th>
                    <th width="33%">Qtd. Titulos</th>
                    <th width="33%">Usuário</th>
                    <th width="1%"></th>

                </thead>

                <tbody>
                </tbody>

            </table>
            
        </div>
        
    </div>
    
</div>!-->
    
<style type="text/css">
#grid_gerados_length, #grid_gerados_filter, #grid_gerados_paginate, #grid_gerados_info
{
    display: none;
}

#grid_dePara_length, #grid_dePara_filter, #grid_dePara_paginate, #grid_dePara_info
{
    display: none;
}
</style>

