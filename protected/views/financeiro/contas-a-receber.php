<?php
   $util = new Util;
   $empresa = Yii::app()->session['usuario']->getEmpresa();
   $totaisRecebimentos = $empresa->totaisRecebimentos();
   
?>
<div class="row">
   <div class="col-sm-12">
      <!-- start: PAGE TITLE & BREADCRUMB -->
      <ol class="breadcrumb">
         <li><a href="<?php echo Yii::app()->request->baseUrl;?>/financeiro/">Financeiro</a></li>
         <li class="active">Contas a receber</li>
      </ol>
      <div class="page-header">
         <h1>Contas a receber</h1>
      </div>
      <!-- end: PAGE TITLE & BREADCRUMB -->
   </div>
</div>

<div class="row">
   <div class="col-sm-12">
         <div class="panel-body">
            <div class="row">
                    
            </div>         
         </div>
   </div>
</div>

<div class="row">
   <div class="col-md-12">
      <!-- start: DYNAMIC TABLE PANEL -->
      <div class="panel panel-default">
         <div class="panel-heading">
            <i class="fa fa-external-link-square"></i>
            Títulos a receber
            <div class="panel-tools">
               <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
               </a>
               <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
               <i class="fa fa-wrench"></i>
               </a>
               <a class="btn btn-xs btn-link panel-refresh" href="#">
               <i class="fa fa-refresh"></i>
               </a>
               <a class="btn btn-xs btn-link panel-expand" href="#">
               <i class="fa fa-resize-full"></i>
               </a>
               <a class="btn btn-xs btn-link panel-close" href="#">
               <i class="fa fa-times"></i>
               </a>
            </div>
         </div>
         <div class="panel-body">
            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
               <thead>
                  <tr>
                     <th class="hidden-xs">Origem</th>
                     <th class="hidden-xs">Cód. Proposta / Venda</th>
                     <th>Cliente</th>
                     <th class="hidden-xs">CPF</th>
                     <th>Vencimento</th>
                     <th class="hidden-xs">Valor</th>
                     <!--<th></th>-->
                  </tr>
               </thead>
               <tbody>
                  <?php for ( $i = 0; $i < count($output['aaData']); $i++ ) { ?>
                     <tr>
                        <td class="hidden-xs"><?php echo $output['aaData'][$i][0] ?></td>
                        <td class="hidden-xs"><?php echo $output['aaData'][$i][1] ?></td>
                        <td><?php echo strtoupper($output['aaData'][$i][2]) ?></td>
                        <td class="hidden-xs"><?php echo $output['aaData'][$i][3] ?></td>
                        <td><?php echo $util->bd_date_to_view($output['aaData'][$i][6]) ?></td>
                        <td class="hidden-xs"><?php echo "R$ " .number_format($output['aaData'][$i][7], 2, ',', '.') ?></td>
                        <!--
                        <td class="center">                              
                           <div class="visible-md visible-lg hidden-sm hidden-xs">                                
                              <a data-toggle="modal" href="#responsive" class="btn btn-xs btn-green tooltips btn-proposta-more-details" data-placement="top" data-original-title="Mais detalhes" modal-iframe-uri="">
                                 <i class="clip-plus-circle"></i>
                              </a>
                           </div>
                        </td>
                        -->
                     </tr>
                  <?php } ?>
               </tbody>
               <!--<tfoot>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th style="text-align">Total:</th>
                    <th id="th_total"></th> 
                </tr>
            </tfoot>-->
            </table>
         </div>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-sm-12">
      <div class="alert alert-info" style="text-align:center">
         <strong>Total a receber: R$ <?php echo number_format($totaisRecebimentos['totalAReceber'], 2, ',', '.') ?> </strong>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-sm-4">
      <div class="alert alert-success">
         Total a receber hoje: <strong>R$ <?php echo number_format($totaisRecebimentos['totalAReceberHoje'], 2, ',', '.') ?></strong>
      </div>
   </div>
   <div class="col-sm-4">
      <div class="alert alert-warning">
         Total a receber amanhã:<strong>R$ <?php echo number_format($totaisRecebimentos['totalAReceberAmanha'], 2, ',', '.') ?></strong>
      </div>
   </div>
   <div class="col-sm-4">
      <div class="alert alert-danger">
         Total em atraso:<strong>R$ <?php echo number_format($totaisRecebimentos['totalAtrasado'], 2, ',', '.') ?></strong>
      </div>
   </div>
</div>

<?php ?>