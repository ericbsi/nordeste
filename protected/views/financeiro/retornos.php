<?php  ?>
<div class="row">
   <div class="col-sm-12">
      
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="#">
               Financeiro
            </a>
         </li>
         <li class="active">
            Retornos
         </li>
      </ol>
      <div class="page-header">
         <h1>
         	Retornos
         </h1>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-sm-9">
      <div class="row">
         <form id="form-filter" action="">
            <div class="col-md-3" style="width:15%">
               <div class="form-group">
                  <label class="control-label">
                     Data de <span class="symbol required" aria-required="true"></span>
                  </label>                  
                  <input style="width:120px" type="text" name="data_de" class="form-control date-picker" data-date-viewmode="years" data-date-format="dd/mm/yyyy" id="data_de">
               </div>
            </div>
            <div class="col-md-3" style="width:15%">
               <div class="form-group">
                  <label class="control-label">Data até:</label>
                  <input style="width:120px" type="text" name="data_ate" class="form-control date-picker" data-date-viewmode="years" data-date-format="dd/mm/yyyy" id="data_ate">
                  <input type="hidden" name="Filial_id" id="Filial_id" value="22">
               </div>
            </div>
            <div class="col-md-3" style="width:15%">
               <div class="form-group">
                  <label class="control-label">&nbsp;</label>
                  <button id="btn-filter" class="btn btn-blue next-step btn-block">Filtrar</button>
               </div>
            </div>
            <div class="col-md-4" >
               <div class="form-group">
                  <label class="control-label">&nbsp;</label>
                  <button id="btn-clear-confirmados" class="btn btn-blue next-step btn-block">Apagar importações confirmadas</button>
               </div>
            </div>
            <div class="col-md-2">
               <div class="panel">
               
               </div>
            </div>
         </form>
      </div>
   </div>
</div>

<div class="row">
   <div class="col-sm-12">
      <table id="grid_retornos" class="table table-striped table-bordered table-hover table-full-width dataTable">
         <thead>
            <tr>
               <th class="no-orderable">Título</th>
               <th class="no-orderable">Ocorrência</th>
               <th style="width:100px" class="no-orderable">Data da importação</th>
               <th style="width:100px" class="no-orderable">Data da ocorrência</th>
               <th class="no-orderable">Valor Pago</th>
               <th style="width:90px" class="no-orderable">Juros de mora</th>
               <!--<th class="no-orderable">Origem do pagamento</th>-->
               <th style="width:300px" class="no-orderable">Motivo da rejeição</th>
            </tr>
         </thead>
      </table>
   </div>
</div>

<style type="text/css">
	#grid_retornos_length, #grid_retornos_filter{
		display: none;
	}
</style>