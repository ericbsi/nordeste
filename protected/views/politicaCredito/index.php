<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            <li>
                <i class="clip-file"></i>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/politicaCredito/index">
                    Polit&iacute;cas de Cr&eacute;dito
                </a>
            </li>
            <li class="active">
                Index
            </li>
        </ol>
        <div class="page-header">
            <h1>
                Polit&iacute;cas de Cr&eacute;dito
        </div>
    </div>
</div>
<p>

</p>
<div class="row">
      <div class="col-sm-12">
         
          <table id="grid_politicas" 
                 class="table table-striped table-bordered table-hover table-full-width dataTable">
              
            <thead>
               <tr>
                  <th class="no-orderable" style="width : 05%!important">
                     Legenda
                  </th>
                  <th class="no-orderable" style="width : 90%!important">
                     Descrição
                  </th>
                  <th class="no-orderable" style="width : 05%!important">
                     <p>
                        <a class="btn btn-success" 
                           href="#modal_form_nova_politica" 
                           data-toggle="modal" 
                           id="btn_modal_form_nova_politica">
                           <i class="fa fa-plus"></i>
                        </a>
                     </p>
                  </th>
               </tr>
            </thead>
         </table>
      </div>
</div>


<div id="modal_form_nova_politica" 
     class="modal fade" 
     tabindex="-1" 
     data-width="780" 
     style="display: none;">
    
   <div class="modal-header">
       
      <button type="button" 
              class="close" 
              data-dismiss="modal" 
              aria-hidden="true">
          
         &times;
      </button>
       
      <h4 class="modal-title">
          Cadastrar Pol&iacute;tica de Cr&eacute;dito
      </h4>
       
   </div>
   <form id="form-politica">
      <div class="modal-body">
         <div class="row">
            <div class="row-centralize">
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-md-10">
                        <div class="form-group">
                           
                            <label class="control-label">
                              Descrição 
                              <span class="symbol required"></span>
                           </label>
                            
                           <input required name="PoliticaCredito[descricao]" 
                                  type="text" 
                                  class="form-control">
                           
                        </div>
                     </div>
                            
                     <div class="col-md-2">
                        <div class="form-group">
                           
                            <label class="control-label">
                              Legenda 
                              <span class="symbol required"></span>
                           </label>
                           <input 
                              id="cor"
                              required 
                              type="color" 
                              value="#000000"
                              class="form-control">
                            
                           <input   id="corHdn"
                                    value="#000000" 
                                    type="hidden" 
                                    name="PoliticaCredito[cor]" 
                                    class="form-control" />
                           
                        </div>
                     </div>
                            
                  </div>
               </div>
            </div>
         </div>
      </div>
       
      <div class="modal-footer">
         
          <label class="checkbox-inline" 
                 style="float:left">
              
            <input id="checkbox_continuar" 
                   type="checkbox" 
                   value="">
            
               Continuar Cadastrando
               
         </label>
          
         <div class="panel"></div>
         
         <button type="button" 
                 data-dismiss="modal" 
                 class="btn btn-light-grey">
         
            Cancelar
             
         </button>
         
         <button type="submit" 
                 class="btn btn-blue">
             Salvar
         </button>
         <div class="row">
         </div>
         
         <br>
            
         <div id="cadastro_msg_return" 
              class="alert" 
              style="text-align:left">
         </div>
            
      </div>
   </form>
</div>

<style type="text/css">
    #grid_politicas_filter, #grid_politicas_length{
        display: none!important;
    }
    .panel{
        background: transparent!important;
        border:none!important;
    }
</style>