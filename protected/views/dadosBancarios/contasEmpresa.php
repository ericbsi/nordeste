<div class="row">
   <div class="col-sm-12">
      <ol class="breadcrumb">
         <li>
            <i class="clip-file"></i>
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/financeiro/">
               Dados Bancários
            </a>
         </li>
         <li class="active">
            Contas da Empresa
         </li>
      </ol>
   </div>
</div>
<p>

</p>
<div class="row">
   <div class="col-sm-12">

      <table id="grid_contas" 
             class="table table-striped table-bordered table-hover table-full-width dataTable">

         <thead>
            <tr>
               <th class="no-orderable">
                  <select class="bancoConta" id="bancoConta" name="Conta[Banco_id]">
                     <option value="0" selected="">
                        Selecione um Banco
                     </option>
                     <?php 
                        foreach (Banco::model()->findAll() as $banco)
                        { 
                     ?>
                        <option value="<?php echo $banco->id ?>" >
                           <?php echo $banco->codigo . ' - ' . $banco->nome  ?>
                        </option>
                     <?php 
                        
                        } 
                     ?>
                  </select>
               </th>
               <th class="no-orderable">
                  <input 
                     class="form-control" 
                     id="inputAgencia" 
                     type="text" 
                     name="Conta[agencia]" 
                     value="" 
                     placeholder="Digite o nº da Agência..."/>
               </th>
               <th class="no-orderable">
                  <input 
                     class="form-control" 
                     id="inputConta" 
                     type="text" 
                     name="Conta[conta]" 
                     value="" 
                     placeholder="Digite o nº da Conta..."/>
               </th>
               <th class="no-orderable">
                  <input 
                     class="form-control" 
                     id="inputOperacao" 
                     type="text" 
                     name="Conta[operacao]" 
                     value="" 
                     placeholder="Digite o cod Operação..."/>
               </th>
               <th class="no-orderable">
                  <input 
                     class="form-control" 
                     id="inputDataAbertura" 
                     type="date" 
                     name="Conta[dataAbertura]" 
                     value="" />
               </th>
               <th class="no-orderable" style="width : 03%!important">
                  <button id="btnSalvar" 
                          type="button" 
                          class="btn btn-success">
                     <i class="fa fa-save"></i>
                  </button>
               </th>
            </tr>
            <tr>
               <th style="width : 10%!important">
                  Banco
               </th>
               <th style="width : 10%!important">
                  Agência
               </th>
               <th style="width : 10%!important">
                  Conta
               </th>
               <th style="width : 05%!important">
                  Operação
               </th>
               <th style="width : 05%!important">
                  Data de Abertura
               </th>
               <th style="width : 03%!important"></th>
            </tr>
         </thead>
         <tbody>
         </tbody>
      </table>
   </div>
</div>

<style type="text/css">
   #grid_contas_length{
      display: none!important;
   }
   .panel{
      background: transparent!important;
      border:none!important;
   }
</style>