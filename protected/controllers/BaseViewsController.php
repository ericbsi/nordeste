<?php

class BaseViewsController extends Controller {

	public $layout = '//layouts/main_sigac_template';

	public function actionIndex()
	{

		$baseUrl = Yii::app()->baseUrl;
    	$cs = Yii::app()->getClientScript();
    	$cs->registerScriptFile($baseUrl . '/js/jquery.min.js');

		$this->render('index');
	}

	public function actionView()
	{
		$baseUrl = Yii::app()->baseUrl;
    	$cs = Yii::app()->getClientScript();
    	
    	/*Javascripts*/
    	$cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
		$cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js',CClientScript::POS_END);
		$cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
		$cs->registerScriptFile($baseUrl . '/js/basedatatables.js',CClientScript::POS_END);
		$cs->registerScriptFile($baseUrl . '/js/select2.js',CClientScript::POS_END);

		/*css*/
		$cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
		$cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');

		$this->render('index', array(
			'registros' => TipoTelefone::model()->findAll(),
		));
	}
}