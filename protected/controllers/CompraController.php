<?php

class CompraController extends Controller {

    public $layout = '//layouts/main_sigac_template';

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('precoItem', 'add', 'cadastrarCompra', 'create', 'update', 'index', 'view', 'delete', 'admin', 'edit', 'teste', 'listFP', 'listProdutos'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionCadastrarCompra() {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');

        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ui-modals.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-grid-compra.js', CClientScript::POS_END);
        //$cs->registerScriptFile($baseUrl . '/js/jquery.maskMoney.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScript('select2', '$("select.search-select").select2()');
        $cs->registerScript('UIModals', 'UIModals.init();');
        //$cs->registerScript('moneypicker', '$(".currency").maskMoney({decimal:",", thousands:"."});');

        $model = new Compra;

        if (isset($_POST['Compra'])) {

            $model->attributes = $_POST['Compra'];
            $model->data = date('Y-m-d H:i:s');

            $produtoId = $_POST['Item_da_Compra_id_hdn'];
            $estoqueId = $_POST['estoque_hdn'];
            $quantidade = $_POST['quantidade_hdn'];
            $desconto = $_POST['desconto_hdn'];
            $precoCompra = $_POST['precoTotal_hdn'];

            $FPId = $_POST['FP_id_hdn'];
            $parcelas = $_POST['parcelas_hdn'];
            $valorFP = $_POST['valorFP_hdn'];

            if ($model->save()) {

                $codigo = trim((string) $model->id);

                $model->codigo = str_pad($codigo, 15, '0', STR_PAD_LEFT);

                if ($model->save()) {

                    for ($i = 0; $i < sizeof($produtoId); $i++) {

                        $itemCompra = new ItemDaCompra;
//                        $itemEstoque

                        $estoque = Estoque::model()->findByPk($estoqueId);
                        $produto = Produto::model()->findByPk($produtoId);

                        $itemEstoque = ItemDoEstoque::model()->buscaItemEstoque($estoque, $produto);

                        $itemCompra->Compra_id = $model->id;
                        $itemCompra->Item_do_Estoque_id = $itemEstoque->id;
                        $itemCompra->seq = $i;
                        $itemCompra->quantidade = $quantidade[$i];
                        $itemCompra->desconto = $desconto[$i];
                        $itemCompra->valor = $precoCompra[$i];

                        $itemCompra->save();
                    }

                    for ($i = 0; $i < sizeof($FPId); $i++) {

                        $fpHasCp = FormaDePagamentoHasCondicaoDePagamento::model()->findByPk($FPId[$i]);

                        $cFPCP = new CompraHasFormaDePagamentoHasCondicaoDePagamento;

                        $cFPCP->Compra_id = $model->id;
                        $cFPCP->Forma_de_Pagamento_has_Condicao_de_Pagamento_id = $fpHasCp->id;
                        $cFPCP->qtdParcelas = $parcelas[$i];
                        $cFPCP->valor = $valorFP[$i];

                        if ($cFPCP->save()) {

                            $titulo = new Titulo;

                            $titulo->emissao = date('Y-m-d');
                            $titulo->Compra_has_FP_has_CP_FP_has_CP_id = $cFPCP->id;
                            $titulo->prefixo = "TST";
                            $titulo->novo($cFPCP);
                        } else {
                            var_dump($cFPCP->getErrors());
                        }
                    }
                }
            } else {
                var_dump($model);
            }

            //$this->redirect(array('admin'));
        }

        $this->render('cadastrarCompra', array(
            'model' => $model,
        ));
    }

    public function actionListFP() {

        $params = [];
        $postId = [];
        $postPar = [];


        if (!empty($_POST) && $_POST['formPGs'] !== null && !empty($_POST['formPGs'])) {

            $postId = $_POST['formPGs'];
            $postPar = $_POST['parcelas'];
        }

        for ($i = 0; $i < sizeof($postId); $i++) {
            $params[] = array(
                'formPGs' => $postId[$i],
                'parcelas' => $postPar[$i]
            );
        }



        $retorno = FormaDePagamentoHasCondicaoDePagamento::model()->searchUnique($params);



        echo json_encode($retorno);
    }

    public function actionListProdutos() {

        $retorno = null;

        foreach (Produto::model()->findAll() as $item) {
            $retorno[] = array('id' => $item->id, 'text' => $item->descricao);
        }

        echo json_encode($retorno);
    }

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionCreate() {

        $model = new Compra;

        if (isset($_POST['Compra'])) {
            $model->attributes = $_POST['Compra'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id) {

        $model = $this->loadModel($id);

        if (isset($_POST['Compra'])) {
            $model->attributes = $_POST['Compra'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {

        $this->loadModel($id)->delete();

        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    public function actionIndex() {

        $dataProvider = new CActiveDataProvider('Compra');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionTeste() {
        echo date('Y-m-d', strtotime("+10 days"));
    }

    public function actionAdmin() {
        $model = new Compra('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Compra']))
            $model->attributes = $_GET['Compra'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function loadModel($id) {
        $model = Compra::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'compra-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
