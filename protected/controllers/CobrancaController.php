<?php

class CobrancaController extends Controller {

    public $layout = '//layouts/main_sigac_template';

    public function filters() {
        return array(
            'accessControl',
            'postOnly + delete'
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('listarAtendimentos',
                    'novoAtendimento',
                    'contatosClientes',
                    'getCliente',
                    'Cobrancas',
                    'listarParcelas',
                    'listarAtrasados',
                    'listarFichamentos',
                    'removerFichamento',
                    'atrasos',
                    'listarInadimplencias',
                    'CobrancasParcela',
                    'listarCobrancas',
                    'persist',
                    'negativarCliente',
                    'fichamentos',
                    'addContato',
                    'novaSeq',
                    'listarChamados',
                    'listarSeqs',
                    'listarParcAtend',
                    'novoChamado',
                    'removerFich'
                ),
                'users' => array('@'),
                'expression' => 'Yii::app()->session["usuario"]->role == "analista_de_credito" || Yii::app()->session["usuario"]->role == "empresa_admin"'
            ),
            array(
                'deny',
                'users' => array('*')
            ),
        );
    }

    public function actionCobrancas()
    {

        $this->layout = '//layouts/main_sigac_template';

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js'                                              );
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js'    , CClientScript::POS_END    );
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js'       , CClientScript::POS_END    );
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js'     , CClientScript::POS_END    );
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js'                 , CClientScript::POS_END    );
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js'             , CClientScript::POS_END    );
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js'      , CClientScript::POS_END    );
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js'          , CClientScript::POS_END    );
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js'          , CClientScript::POS_END    );
        $cs->registerScriptFile($baseUrl . '/js/cobranca/fn-cobranca-index.js?v=2', CClientScript::POS_END  );
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-filestyle.min.js'     , CClientScript::POS_END    );
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js'                 , CClientScript::POS_END    );
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js'       , CClientScript::POS_END    );


        $this->render('cobrancas');
    }

    public function actionFichamentos() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cpfPesquisa = 0;

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/cobranca/fn-fichamentos.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js', CClientScript::POS_END);

        $cs->registerScriptFile($baseUrl . '/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js', CClientScript::POS_END);

        if (isset($_POST['cpfPesquisa'])) {
            $cpfPesquisa = $_POST['cpfPesquisa'];
        }

        $this->render('fichamentos', array('cpfPesquisa' => $cpfPesquisa));
    }

    public function actionLoad() {
        echo json_encode(Cobranca::model()->loadFormEdit($_POST['id']));
    }

    public function actionPersist() {
        echo json_encode(Cobranca::model()->persist($_POST['Cobranca'], $_POST['action_type'], $_POST['Cobranca_id']));
    }

    public function actionIndex() {

    }

    public function actionCobrancasParcela() {
        $this->layout = '//layouts/main_sigac_template';

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css');


        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-grid-cobrancas-parcela.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js', CClientScript::POS_END);

        $cs->registerScript('datepicker', "$('.date-picker').datepicker({autoclose: true});");

        $parcela = Parcela::model()->findByPk($_POST['idParcela']);
        $cpf_pesquisa = $_POST['cpfPesquisa'];

        $this->render('cobrancasParcela', array(
            'parcela' => $parcela,
            'cpfPesquisa' => $cpf_pesquisa
        ));
    }

    public function actionListarAtrasos() {
        //echo json_encode( Empresa::model()->listarAtrasos( $_POST['draw'], $_POST['start'], $_POST['filiais'] /*,$_POST['analistas']*/ ) );
    }

    public function actionListarFichamentos() {
        echo json_encode(Fichamento::model()->listarFichamentos($_POST['draw'], $_POST['start']));
    }

    public function actionRemoverFich()
    {

        if(isset($_POST["fichamentoId"]) && !empty($_POST["fichamentoId"]))
        {

            if(!isset($_POST['senha']) || Empty($_POST['senha']))
            {


                $retorno  = [
                                'hasErrors' => 1                                            ,
                                'titulo'    => 'Não foi possível concluir o fichamento!'    ,
                                'texto'     => 'Senha de permissão inválida.'               ,
                                'tipo'      => 'error'
                            ];
            }
            else
            {

                $configuracoes  = SigacConfig::model()->find("habilitado AND auth_reanalise = 'reanalise'");

                if ($configuracoes !== null && $configuracoes->auth_fichamento != $_POST['senha'])
                {

                    $retorno  =     [
                                        'hasErrors' => 1                                            ,
                                        'titulo'    => 'Não foi possível concluir o fichamento!'    ,
                                        'texto'     => 'Senha de permissão inválida.'               ,
                                        'tipo'      => 'error'
                                    ];
                }
                else
                {

                    $fichamentoHasStatus = FichamentoHasStatusFichamento::model()->find("habilitado AND id = " . $_POST["fichamentoId"]);

                    if($fichamentoHasStatus == null)
                    {

                        $retorno =  [
                                        'hasErrors' => 1                                    ,
                                        'titulo'    => 'Fichamento não encontrado'          ,
                                        'texto'     => 'Entre em contato com o suporte.'    ,
                                        'tipo'      => 'error'
                                    ];
                    }
                    else
                    {

                        $observacao = "";

                        if(isset($_POST["observacao"]) && !empty($_POST["observacao"]))
                        {
                            $observacao = $_POST["observacao"];
                        }

                        $transaction = Yii::app()->db->beginTransaction();

                        $fichamentoHasStatus->habilitado = 0;

                        if ($fichamentoHasStatus->update())
                        {

                            $fichamentoHasStatusFichamento                      = new FichamentoHasStatusFichamento     ;
                            $fichamentoHasStatusFichamento->StatusFichamento_id = 3                                     ; //
                            $fichamentoHasStatusFichamento->Fichamento_id       = $fichamentoHasStatus->fichamento->id  ;
                            $fichamentoHasStatusFichamento->habilitado          = 1                                     ;
                            $fichamentoHasStatusFichamento->Usuario_id          = Yii::app()->session["usuario"]->id    ;
                            $fichamentoHasStatusFichamento->observacao          = $observacao                           ;
                            $fichamentoHasStatusFichamento->data_cadastro       = date("Y-m-d H:i:s")                   ;

                            if($fichamentoHasStatusFichamento->save())
                            {

                                $fichamentoHasStatusHasMotivo                                       = new FichamentoHasStatusFichamentoHasMotivoExclusao    ;
                                $fichamentoHasStatusHasMotivo->habilitado                           = 1                                                     ;
                                $fichamentoHasStatusHasMotivo->data_cadastro                        = date("Y-m-d H:i:s")                                   ;
                                $fichamentoHasStatusHasMotivo->Fichamento_has_StatusFichamento_id   = $fichamentoHasStatusFichamento->id                    ;
                                $fichamentoHasStatusHasMotivo->MotivoExclusao_id                    = $_POST["idMotivo"]                                    ;

                                if($fichamentoHasStatusHasMotivo->save())
                                {

                                    $retornoFichamento = $fichamentoHasStatusFichamento->fichamento->retirar();

                                    if($retornoFichamento["resultado"])
                                    {

                                        $transaction->commit()                                                              ;

                                        $retorno  =     [
                                                            'hasErrors' => 0                                                ,
                                                            'titulo'    => 'Operação realizada com sucesso.'                ,
                                                            'texto'     => 'Fichamento retirado com sucesso!'
                                                                        .  ' Resultado ' . $retornoFichamento["mensagem"]   ,
                                                            'tipo'      => 'success'
                                                        ];

                                    }
                                    else
                                    {

                                        $transaction->rollBack()                                            ;

                                        $retorno  =   [
                                                            'hasErrors' => 1                                ,
                                                            'titulo'    => 'Erro ao retirar fichamento'     ,
                                                            'texto'     => $retornoFichamento["mensagem"]   ,
                                                            'tipo'      => 'error'
                                                        ];

                                    }

                                }
                                else
                                {

                                    $transaction->rollBack()                                ;

                                    ob_start()                                              ;
                                    var_dump($fichamentoHasStatusHasMotivo->getErrors())    ;
                                    $resultado = ob_get_clean()                             ;

                                    $retorno =  [
                                                    'hasErrors' => true                                                 ,
                                                    'titulo'    => 'Não foi possível remover o fichamento!'             ,
                                                    'texto'     => "Entre em contato com o suporte. Erro: $resultado"   ,
                                                    'tipo'      => 'error'                                              ,
                                                ];
                                }

                            }
                            else
                            {

                                $transaction->rollBack()                                ;

                                ob_start()                                              ;
                                var_dump($fichamentoHasStatusFichamento->getErrors())   ;
                                $resultado = ob_get_clean()                             ;

                                $retorno =  [
                                                'hasErrors' => true                                                 ,
                                                'titulo'    => 'Não foi possível remover o fichamento!'             ,
                                                'texto'     => "Entre em contato com o suporte. Erro: $resultado"   ,
                                                'tipo'      => 'error'                                              ,
                                            ];

                            }

                        }
                        else
                        {

                            $transaction->rollBack()                    ;

                            ob_start()                                  ;
                            var_dump($fichamentoHasStatus->getErrors()) ;
                            $resultado = ob_get_clean()                 ;

                            $retorno =  [
                                            'hasErrors' => true                                                 ,
                                            'titulo'    => 'Não foi possível remover o fichamento!'             ,
                                            'texto'     => "Entre em contato com o suporte. Erro: $resultado"   ,
                                            'tipo'      => 'error'                                              ,
                                        ];

                        }

                    }

                }

            }

        }
        else
        {

            $retorno =  [
                            "hasErrors" => true                                     ,
                            'titulo'    => 'Não foi possível remover o fichamento!' ,
                            "tipo"      => "error"                                  ,
                            "texto"     => "Faltou o ID do Fichamento"              ,

                        ];

        }

        echo json_encode($retorno);

    }

    public function actionRemoverFichamento() {
        echo json_encode(Fichamento::model()->removerFichamento(
                        $_POST['FichamentoAtual'], $_POST['FichamentoHasStatusFichamento'], $_POST['Operacao'], $_FILES['ComprovanteFile2']
                )
        );
    }

    public function actionNegativarCliente() {
        //var_dump($_POST);
        //var_dump($_FILES['ComprovanteFile2']);

/*        $retorno = Cliente::model()->ficharCliente(
                $_POST ['Fichamento'], $_POST ['FichamentoHasStatusFichamento'], $_POST ['Operacao'], $_FILES ['ComprovanteFile2']//Novo campo para anexar arquivo incluído
        );*/

        if(isset($_POST['parcelaId']) && !Empty($_POST['parcelaId']))
        {

            if(isset($_POST['orgaoNegacao']) && !Empty($_POST['orgaoNegacao']) && $_POST['orgaoNegacao'] !== "0")
            {

                if(isset($_POST['senha']) && !Empty($_POST['senha']))
                {

                    $configuracoes  = SigacConfig::model()->find("habilitado AND auth_reanalise = 'reanalise'");

                    if ($configuracoes !== null && $configuracoes->auth_fichamento != $_POST['senha'])
                    {

                        $retorno  =   [
                                            'hasErrors' => 1                                            ,
                                            'titulo'    => 'Não foi possível concluir o fichamento!'    ,
                                            'texto'     => 'Senha de permissão inválida.'               ,
                                            'tipo'      => 'error'
                                        ];
                    }
                    else
                    {

                        $idParcela  = $_POST['parcelaId'    ];
                        $orgaoId    = $_POST['orgaoNegacao' ];

                        $fichamento = Fichamento::model()->find("Parcela = $idParcela AND OrgaoNegacao_id = $orgaoId AND habilitado");

                        if($fichamento !== null)
                        {

                            $retorno =    [
                                                'hasErrors' => 1                                        ,
                                                'titulo'    => 'Já existe fichamento para essa parcela' ,
                                                'texto'     => 'Entre em contato com o suporte.'        ,
                                                'tipo'      => 'error'
                                            ];

                        }
                        else
                        {

                            $transaction = Yii::app()->db->beginTransaction();

                            $fichamento                     = new Fichamento        ;
                            $fichamento->data_cadastro      = date("Y-m-d H:i:s")   ;
                            $fichamento->habilitado         = 1                     ;
                            $fichamento->Parcela            = $idParcela            ;
                            $fichamento->OrgaoNegacao_id    = $orgaoId              ;

                            if($fichamento->save())
                            {

                                if(isset($_POST["observacao"]) && !empty($_POST["observacao"]))
                                {
                                    $observacao = $_POST["observacao"];
                                }
                                else
                                {
                                    $observacao = "";
                                }

                                $fichamentoHasStatus                        = new FichamentoHasStatusFichamento ;
                                $fichamentoHasStatus->habilitado            = 1                                 ;
                                $fichamentoHasStatus->Fichamento_id         = $fichamento->id                   ;
                                $fichamentoHasStatus->StatusFichamento_id   = 2                                 ;
                                $fichamentoHasStatus->data_cadastro         = date("Y-m-d H:i:s")               ;
                                $fichamentoHasStatus->Usuario_id            = Yii::app()->session['usuario']->id;
                                $fichamentoHasStatus->observacao            = $observacao                       ;

                                if($fichamentoHasStatus->save())
                                {

                                    $retornoFichamento = $fichamento->fichar();

                                    if($retornoFichamento["resultado"])
                                    {

                                        $transaction->commit();

                                        $retorno  =     [
                                                            'hasErrors' => 0                                                ,
                                                            'titulo'    => 'Operação realizada com sucesso.'                ,
                                                            'texto'     => 'Fichamento realizado com sucesso!'
                                                                        .  ' Resultado ' . $retornoFichamento["mensagem"]   ,
                                                            'tipo'      => 'success'
                                                        ];

                                    }
                                    else
                                    {

                                        $transaction->rollBack();

                                        $retorno  =   [
                                                            'hasErrors' => 1                                ,
                                                            'titulo'    => 'Erro ao realizar fichamento'    ,
                                                            'texto'     => $retornoFichamento["mensagem"]   ,
                                                            'tipo'      => 'error'
                                                        ];

                                    }

                                }

                                else
                                {

                                    $transaction->rollBack();

                                    ob_start()                                  ;
                                    var_dump($fichamentoHasStatus->getErrors()) ;
                                    $resultado = ob_get_clean()                 ;

                                    $retorno  =   [
                                                        'hasErrors' => 1                                                                        ,
                                                        'titulo'    => "Erro ao salvar o fichamento. FichamentoHasStatusFichamento: $resultado" ,
                                                        'texto'     => 'Entre em contato com o suporte.'                                        ,
                                                        'tipo'      => 'error'                                                                  ,
                                                    ];

                                }

                            }
                            else
                            {

                                $transaction->rollBack();

                                ob_start()                          ;
                                var_dump($fichamento->getErrors())  ;
                                $resultado = ob_get_clean()         ;

                                $retorno =    [
                                                    'hasErrors' => 1                                                        ,
                                                    'titulo'    => "Erro ao salvar o fichamento. Fichamento: $resultado"    ,
                                                    'texto'     => 'Entre em contato com o suporte.'                        ,
                                                    'tipo'      => 'error'
                                                ];

                            }

                        }

                    }
                }
                else
                {

                    $retorno =  [
                                    "type"  =>  "error"                         ,
                                    "msg"   =>  "Escolha o Orgão de Negação"
                                ];

                }

            }
            else
            {

                $retorno =  [
                                "type"  =>  "error"                         ,
                                "msg"   =>  "Escolha o Orgão de Negação"
                            ];

            }

        }
        else
        {

            $retorno =  [
                            "type"  =>  "error"                     ,
                            "msg"   =>  "Faltou o ID da Parcela"
                        ];

        }

        echo json_encode($retorno);
    }

    public function actionAtrasos() {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cpfPesquisa = 0;

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-atrasosv2.js', CClientScript::POS_END);

        $cs->registerScriptFile($baseUrl . '/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js', CClientScript::POS_END);

        if (isset($_POST['cpfPesquisa'])) {
            $cpfPesquisa = $_POST['cpfPesquisa'];
        }

        $this->render('atrasos', array('cpfPesquisa' => $cpfPesquisa));
    }

    public function actionGetCliente() {
        $cliente = Cliente::model()->getClienteByCpf($_POST['cpf_cliente']);

        $retorno = array(
            "id" => $cliente->id
        );
        if ($cliente != null) {
            echo json_encode($retorno);
        } else {
            return null;
        }
    }

    public function actionAddContato() {
        $cliente = Cliente::model()->findByPk($_GET['id_cliente']);
        $numero = $_GET['num_ncontato'];
        $retorno = array(
            'hasErrors' => true,
            'msg' => 'Não foi possível adicionar o novo contato',
            'type' => 'error'
        );

        $transaction = Yii::app()->db->beginTransaction();

        if ($cliente != null) {
            $contato_id = $cliente->pessoa->Contato_id;
            if ($contato_id != null && !empty($numero)) {
                $tel = new Telefone;
                $tel->numero = substr($numero, 2);
                $tel->data_cadastro = Date('Y-m-d');
                $tel->discagem_direta_distancia = substr($numero, 0, 2);
                $tel->Tipo_Telefone_id = 2;
                if ($tel->save()) {
                    $cht = new ContatoHasTelefone;
                    $cht->Contato_id = $contato_id;
                    $cht->Telefone_id = $tel->id;
                    $cht->data_cadastro = Date('Y-m-d');
                    if ($cht->save()) {
                        $retorno = array(
                            'hasErrors' => false,
                            'msg' => 'Contato adicionado com sucesso',
                            'type' => 'success'
                        );
                        $transaction->commit();
                    } else {
                        $retorno = array(
                            'hasErrors' => true,
                            'msg' => 'Não foi possível adicionar o ContatoHasTelefone',
                            'type' => 'error'
                        );
                        $transaction->rollBack();
                    }
                } else {
                    $retorno = array(
                        'hasErrors' => true,
                        'msg' => 'Não foi possível adicionar o Telefone',
                        'type' => 'error'
                    );
                    $transaction->rollBack();
                }
            } else {
                $retorno = array(
                    'hasErrors' => true,
                    'msg' => 'Não foi possível encontrar o contato_id do cliente',
                    'type' => 'error'
                );
                $transaction->rollBack();
            }
        } else {
            $retorno = array(
                'hasErrors' => true,
                'msg' => 'Não foi possível encontrar o cliente',
                'type' => 'error'
            );
            $transaction->rollBack();
        }

        echo json_encode($retorno);
    }

    public function actionContatosClientes() {
        $cliente = Cliente::model()->findByPk($_GET['id_cliente']);

        if ($cliente != null) {
            echo json_encode($cliente->listarContatos());
        }
    }

    public function actionListarAtendimentos() {
        $cpf = $_POST['cpf_cliente'];

        $sql = "SELECT MIN(Agen.data_interacao) as 'prox', Seq.observacao as 'obs', AHP.Atendimento_id as 'id', AHP.data_cadastro as 'inicio', MAX(Seq.fim) as 'ult_intera',Count(DISTINCT AHP.Parcela_id) as 'qtd'
                    FROM Parcela AS P
                    INNER JOIN Titulo AS T ON T.id = P.Titulo_id AND T.NaturezaTitulo_id = 1 AND T.VendaW_id IS NULL
                    INNER JOIN Proposta AS Pr ON T.Proposta_id = Pr.id AND Pr.Status_Proposta_id = 2
                    INNER JOIN Analise_de_Credito AS AC ON AC.id = Pr.Analise_de_Credito_id
                    INNER JOIN Cliente AS CLI ON AC.Cliente_id = CLI.id
                    INNER JOIN Pessoa AS PES ON CLI.Pessoa_id = PES.id
                    INNER JOIN Pessoa_has_Documento PHD ON PHD.Pessoa_id = PES.id
                    INNER JOIN Documento DOC ON DOC.id = PHD.Documento_id AND DOC.Tipo_documento_id = 1 AND DOC.numero = '" . $cpf . "'
                    INNER JOIN Analista_has_Proposta_has_Status_Proposta AS Aps ON Aps.Proposta_id = Pr.id AND Aps.Status_Proposta_id = 2
                    INNER JOIN Usuario AS U ON Aps.Analista_id = U.id
                    INNER JOIN Ranges_gerados  AS Rg ON Rg.Parcela_id = P.id
                    INNER JOIN Atendimento_has_Parcela AS AHP ON AHP.Parcela_id = P.id AND AHP.habilitado = 1
                    INNER JOIN Atendimento AS Atend ON Atend.id = AHP.Atendimento_id
                    INNER JOIN Chamado AS Cha ON Cha.Atendimento_id = AHP.Atendimento_id
                    INNER JOIN Sequencia AS Seq ON Seq.Chamado_id = Cha.id
                    LEFT  JOIN Agendamento AS Agen ON Agen.Atendimento_id = Atend.id AND DATEDIFF('" . Date('Y-m-d') . "', Agen.data_interacao) <= 0
                WHERE
                    DATEDIFF('" . Date('Y-m-d') . "', P.vencimento) >= 5
                    AND P.habilitado
		GROUP BY id
                ORDER BY P.vencimento ASC";

        $atendimentos = Yii::app()->db->createCommand($sql)->queryAll();
        $row = [];
        $rows = [];
        $util = new Util;
        $prox_inter = "";

        foreach ($atendimentos as $atend) {
            if ($atend['qtd'] != 0) {
                $cha = ' <input type="hidden" value= "' . $atend['id'] . '">'
                        . ' <a id="btn_chamado" class="btn"><i class="fa fa-phone"></i></a>';
                if ($atend['prox'] != null) {
                    $prox_inter = $util->bd_date_to_view(substr($atend['prox'], 0, 10));
                } else {
                    $prox_inter = "Sem agendamento";
                }

                $novo_cha = '<input type="hidden" value= "' . $atend['id'] . '">'
                        . '<a id="novo_cha" class="btn"><i class="fa fa-plus-circle"></i></a>';

                $qtd_parcelas = '<input type="hidden" value= "' . $atend['id'] . '">'
                        . '<a id="parc_atend" class="btn">' . $atend['qtd'] . ' <i class="fa fa-list-ul"></i></a>';

                $row = array(
                    "id" => $atend['id'],
                    "inicio" => $util->bd_date_to_view(substr($atend['inicio'], 0, 10)),
                    "ult_inter" => $util->bd_date_to_view(substr($atend['ult_intera'], 0, 10)),
                    "prox_inter" => $prox_inter,
                    "qtd" => $qtd_parcelas,
                    "cha" => $cha,
                    "novo_cha" => $novo_cha
                );

                $rows[] = $row;
            }
        }
        echo json_encode(array("data" => $rows));
    }

    public function actionNovoChamado() {
        $id_atend = $_GET['id_atend'];
        $dt_agend = $_GET['data'];
        $transaction = Yii::app()->db->beginTransaction();

        $retorno = array(
            'hasErrors' => true,
            'msg' => 'Não foi possível cadastra o novo chamado para este atendimento. Verifique se o campo de agendamento foi preenchido',
            'type' => 'error'
        );

        $atendimento = Atendimento::model()->find('habilitado AND id = ' . $id_atend);
        if ($atendimento != null && !empty($dt_agend)) {
            $chamado = new Chamado;

            $chamado->data_cadastro = $dt_agend;
            $chamado->Atendimento_id = $id_atend;

            $agendamento = new Agendamento;

            $agendamento->Atendimento_id = $id_atend;
            $agendamento->Usuario_cliente = Yii::app()->session['usuario']->id;
            $agendamento->Usuario_cobranca = Yii::app()->session['usuario']->id;
            $agendamento->data_cadastro = Date('Y-m-d');
            $agendamento->data_interacao = $dt_agend;
            $agendamento->Tipo_Agendamento_id = 1;

            if ($chamado->save() && $agendamento->save()) {
                $retorno = array(
                    'hasErrors' => false,
                    'msg' => 'Novo chamado cadastrado com sucesso, fique atento a data que foi agendada para este chamado',
                    'type' => 'success'
                );
                $transaction->commit();
            } else {
                $transaction->rollBack();
            }
        }

        echo json_encode($retorno);
    }

    public function actionlistarChamados() {
        $id_atend = $_GET['id_atend'];
        $row = [];
        $rows = [];
        $chamados = Chamado::model()->findAll('habilitado AND Atendimento_id = ' . $id_atend);
        $util = new Util;

        if (count($chamados) > 0) {
            $i = 1;
            foreach ($chamados as $chm) {
                $row = array(
                    "ordem" => $i,
                    "data" => $util->bd_date_to_view(substr($chm->data_cadastro, 0, 10)),
                    "button" => '<input type="hidden" value="' . $chm->id . '">'
                    . '<a class="btn btn_seqs"><i class="fa fa-comment-o"></i></a>'
                );
                $rows[] = $row;
                $i++;
            }
        }

        echo json_encode(array("data" => $rows));
    }

    public function actionlistarSeqs() {
        $id_chamado = $_GET['id_chamado'];
        $row = [];
        $rows = [];
        $seqs = Sequencia::model()->findAll('habilitado AND Chamado_id = ' . $id_chamado);
        $util = new Util;

        if (count($seqs) > 0) {
            foreach ($seqs as $seq) {
                $row = array(
                    "inicio" => $util->bd_date_to_view(substr($seq->inicio, 0, 10)) . ' ' . substr($seq->inicio, 10, 18),
                    "fim" => $util->bd_date_to_view(substr($seq->fim, 0, 10)) . ' ' . substr($seq->fim, 10, 18),
                    "obs" => $seq->observacao,
                    "user" => $seq->usuario->nome_utilizador
                );
                $rows[] = $row;
            }
        }

        echo json_encode(array("data" => $rows));
    }

    public function actionNovaSeq() {

        $transaction = Yii::app()->db->beginTransaction();

        $retorno = array(
            "hasError" => true,
            "msg" => "Tenha certeza que preencheu todos os campos do formulário! Se o erro persistir, entre em contato com o suporte",
            "type" => "error"
        );

        $error = false;
        $id_chamado = $_GET['id_chamado'];
        $inicio = $_GET['inicio'];
        $fim = $_GET['fim'];
        $obs = $_GET['obs'];

        $chamado = Chamado::model()->find('habilitado AND id = ' . $id_chamado);

        if ($chamado != null && !empty($obs)) {

            $seq = new Sequencia;
            $seq->inicio = Date('Y-m-d') . " " . $inicio;
            $seq->fim = Date('Y-m-d') . " " . $fim;
            $seq->data_cadastro = Date('Y-m-d');
            $seq->Usuario_id = Yii::app()->session['usuario']->id;
            $seq->Chamado_id = $chamado->id;
            $seq->observacao = $obs;
            $seq->habilitado = 1;

            if ($seq->save()) {

                $contato = $_GET['id_contato'];
                $shc = new SequenciaHasContato;

                $shc->Contato_id = $contato;
                $shc->Sequencia_id = $seq->id;
                $shc->habilitado = 1;
                $shc->data_cadastro = Date('Y-m-d');

                if ($shc->save()) {

                    $retorno = array(
                        "hasError" => false,
                        "msg" => "Sequencia salva com sucesso!",
                        "type" => "success"
                    );


                    $dt_ag = $_GET['dt_agendamento'];

                    if ($dt_ag != "" && $dt_ag != NULL) {

                        $agendamento = new Agendamento;

                        $agendamento->Atendimento_id = $chamado->Atendimento_id;
                        $agendamento->Usuario_cliente = Yii::app()->session['usuario']->id;
                        $agendamento->Usuario_cobranca = Yii::app()->session['usuario']->id;
                        $agendamento->data_cadastro = Date('Y-m-d');
                        $agendamento->data_interacao = $dt_ag;
                        $agendamento->Tipo_Agendamento_id = 2;

                        if ($agendamento->save()) {

                            $retorno = array(
                                "hasError" => false,
                                "msg" => $retorno["msg"] . " Sequencia salvo com sucesso!",
                                "type" => "success"
                            );
                        } else {

                            $retorno = array(
                                "hasError" => true,
                                "msg" => "Erro ao salvar o agendamento!",
                                "type" => "error"
                            );
                            $error = true;
                        }
                    }
                } else {

                    $retorno = array(
                        "hasError" => true,
                        "msg" => "Não foi possível salvar o contato relacionado à sequencia",
                        "type" => "error"
                    );

                    $error = true;
                }
            } else {
                $error = true;
            }
        } else {
            $error = true;
        }

        if (!$error) {
            $transaction->commit();
        } else {
            $transaction->rollBack();
        }

        echo json_encode($retorno);
    }

    public function actionNovoAtendimento() {
        $transaction = Yii::app()->db->beginTransaction();
        $error = false;
        $retorno = array(
            "hasError" => true,
            "msg" => "Tenha certeza que preencheu todos os campos do formulário! Se o erro persistir, entre em contato com o suporte",
            "type" => "error"
        );


        $atendimento = new Atendimento;
        $atendimento->data_cadastro = Date('Y-m-d');
        $atendimento->habilitado = 1;

        if ($atendimento->save()) {

            $chamado = new Chamado;
            $chamado->habilitado = 1;
            $chamado->data_cadastro = Date('Y-m-d');
            $chamado->Atendimento_id = $atendimento->id;

            if ($chamado->save() && isset($_POST['inicio']) &&
                    isset($_POST['fim']) && isset($_POST['obs']) &&
                    isset($_POST['id_contato'])) {

                $sequencia = new Sequencia;

                $inicio = $_POST['inicio'];
                $fim = $_POST['fim'];
                $obs = $_POST['obs'];
                $contato = $_POST['id_contato'];

                $sequencia->inicio = Date('Y-m-d') . " " . $inicio;
                $sequencia->fim = Date('Y-m-d') . " " . $fim;
                $sequencia->data_cadastro = Date('Y-m-d');
                $sequencia->Usuario_id = Yii::app()->session['usuario']->id;
                $sequencia->Chamado_id = $chamado->id;
                $sequencia->observacao = $obs;
                $sequencia->habilitado = 1;

                if ($sequencia->save() && isset($_POST['parc_ids'])) {

                    $shc = new SequenciaHasContato;

                    $shc->Contato_id = $contato;
                    $shc->Sequencia_id = $sequencia->id;
                    $shc->habilitado = 1;
                    $shc->data_cadastro = Date('Y-m-d');

                    if ($shc->save() && !$error) {

                        $retorno = array(
                            "hasError" => false,
                            "msg" => "Atendimento salvo com sucesso!",
                            "type" => "success"
                        );

                        $parcelas = $_POST['parc_ids'];
                        foreach ($parcelas as $id) {
                            $ahp = new AtendimentoHasParcela;
                            $ahp->Atendimento_id = $atendimento->id;
                            $ahp->Parcela_id = $id;
                            $ahp->data_cadastro = Date('Y-m-d');
                            $ahp->habilitado = 1;

                            if (!$ahp->save()) {
                                $error = true;
                                $retorno = array(
                                    "hasError" => true,
                                    "msg" => "Não foi possível salvar o atendimento relacionado a alguma parcela",
                                    "type" => "error"
                                );
                                break;
                            }
                        }

                        $dt_ag = $_POST['dt_agendamento'];

                        if ($dt_ag != "" && $dt_ag != null && isset($dt_ag)) {
                            $agendamento = new Agendamento;

                            $agendamento->Atendimento_id = $atendimento->id;
                            $agendamento->Usuario_cliente = Yii::app()->session['usuario']->id;
                            $agendamento->Usuario_cobranca = Yii::app()->session['usuario']->id;
                            $agendamento->data_cadastro = Date('Y-m-d');
                            $agendamento->data_interacao = $dt_ag;
                            $agendamento->Tipo_Agendamento_id = 2;

                            if ($agendamento->save()) {
                                $retorno = array(
                                    "hasError" => false,
                                    "msg" => "Atendimento salvo com sucesso!",
                                    "type" => "success"
                                );
                            } else {
                                $retorno = array(
                                    "hasError" => true,
                                    "msg" => "Erro ao salvar o agendamento!",
                                    "type" => "error"
                                );
                                $error = true;
                            }
                        }
                    } else {
                        $error = true;
                        $retorno = array(
                            "hasError" => true,
                            "msg" => "Não foi possível salvar o contato relacionado à sequencia",
                            "type" => "error"
                        );
                    }
                }
            } else {
                $error = true;
                $retorno = array(
                    "hasError" => true,
                    "msg" => "Não foi possível salvar o chamado",
                    "type" => "error"
                );
            }
        } else {
            $error = true;
            $retorno = array(
                "hasError" => true,
                "msg" => "Não foi possível salvar o atendimento",
                "type" => "error"
            );
        }

        if (!$error) {
            $transaction->commit();
        } else {
            $transaction->rollBack();
        }

        echo json_encode($retorno);
    }

    public function actionListarParcAtend() {
        $id_atend = $_GET['id_atend'];
        $ahp = AtendimentoHasParcela::model()->findAll('habilitado AND Atendimento_id = ' . $id_atend);
        $row = [];
        $rows = [];
        $util = new Util;

        if (count($ahp) > 0) {
            foreach ($ahp as $parc) {
                $disableNeg = '';
                $disableRet = '';
                $fich = Fichamento::model()->find('habilitado AND Parcela = ' . $parc->Parcela_id);
                $id_fich_hs = '';

                if ($fich != null) {
                    $fichHasStatus = FichamentoHasStatusFichamento::model()->find('habilitado AND Fichamento_id = ' . $fich->id);
                    $disableNeg = ' disabled="disabled" ';

                    if ($fichHasStatus->statusFichamento->id != 5) {
                        $disableRet = '';
                        $id_fich_hs = $fichHasStatus->id;
                    } else {
                        $disableRet = '';
                        $disableNeg = ' disabled="disabled" ';
                        $id_fich_hs = $fichHasStatus->id;
                    }
                } else {
                    $disableRet = '';
                    $disableNeg = '';
                }

                $parcela = Parcela::model()->find('habilitado AND id = ' . $parc->Parcela_id);
/*                $button = '<input type="hidden" value="' . $parc->Parcela_id . '">'
                        . '<button ' . $disableNeg . ' class="btn btn-danger btn-sm btn-add-fich">Negativar <i class="fa fa-exclamation-circle"></i></button>';*/

//                $titulo = Titulo::model()->find("habilitado AND NaturezaTitulo_id IN () AND Proposta_id = " . $parcela->titulo->Proposta_id);

                $button = "<button $disableNeg value='$parc->Parcela_id' class='btn btn-danger btn-sm btn-add-fich'>Negativar <i class='fa fa-exclamation-circle'></i></button>";

/*                $retirar = '<input type="hidden" value="' . $id_fich_hs . '">'
                        . '<button ' . $disableRet . ' class="btn btn-success btn-sm btn-rmv-fich">Retirar <i class="fa fa-exclamation-circle"></i></button>';*/

                $retirar = "<button $disableRet value='$id_fich_hs' class='btn btn-success btn-sm btn-rmv-fich'>Retirar <i class='fa fa-exclamation-circle'></i></button>";

                if($parcela->valor > $parcela->valor_atual){
                    $row = array(
                        "seq" => '<span style="color:red;">' . $parcela->seq . '</span>',
                        "valor" => '<span style="color:red;">' . $parcela->valor . '</span>',
                        "venc" => '<span style="color:red;">' . $util->bd_date_to_view(substr($parcela->vencimento, 0, 10)) . '</span>',
                        "button" => $button,
                        "retirar" => $retirar
                    );
                }else{
                    $row = array(
                        "seq" => '<span style="color:green;">' . $parcela->seq . '</span>',
                        "valor" => '<span style="color:green;">' . $parcela->valor . '</span>',
                        "venc" => '<span style="color:green;">' . $util->bd_date_to_view(substr($parcela->vencimento, 0, 10)) . '</span>',
                        "button" => $button,
                        "retirar" => $retirar
                    );
                }
                $rows[] = $row;
            }
        }

        echo json_encode(array("data" => $rows));
    }

    //ação que lista todas as parcelas que estão em atraso, levando em consideração o cpf de um cliente..
    public function actionListarParcelas() {
        $retorno = null;
        if (isset($_POST['cpf_cliente'])) {
            $retorno = Cobranca::model()->listarParcelas($_POST['cpf_cliente']);
        }
        echo json_encode($retorno);
    }

    //ação que lista todos os atrasos existentes no banco, considerando um parâmetro de vencimento..
    public function actionListarAtrasados() {

        $retorno = Cobranca::model()->listarCobrancas($_POST['draw'], $_POST['start'], $_POST['filiais'],
                $_POST['data_de'], $_POST['data_ate'], $_POST['mtodos'], $_POST['aghoje'], $_POST['agend'],
                $_POST['fich'], $_POST['cpf'], $_POST['csc'], $_POST['fdic'], $_POST['qtd_atrasos_de'], $_POST['qtd_atrasos_ate'] );
        echo json_encode($retorno);
    }

    public function actionListarInadimplencias() {
        echo json_encode(Empresa::model()->listarInadimplencias($_POST['draw'], $_POST['start'], $_POST['filiais'], $_POST['analistas'], $_POST['cpf']));
    }

    public function actionListarCobrancas() {

        echo json_encode(Cobranca::model()->cobrancasParcela($_POST['draw'], $_POST['Parcela_id'], $_POST['start']));
    }

}
