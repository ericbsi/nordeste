<?php

/* Sigac Nordeste */

class PropostaController extends Controller {

    public $layout = '//layouts/main_sigac_template';

    public function filters() {

        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete' // we only allow deletion via POST request
        );
    }

    public function accessRules()
    {
        return array(
            array(
                'allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array(
                    'atualizarSeguroNovo',
                    'getPropostaCodigo',
                    'addAnexoContrato',
                    'simular',
                    'getAnexoContrato',
                    'alterarCondicoes',
                    'statusChangeListener',
                    'listarBaixasParcela',
                    'maisDetalhes',
                    'pollListner',
                    'getMensagens',
                    'getDetalhesFinanceiros',
                    'propostasCrediaristaCharts',
                    'getContratoOmni',
                    'consultarCliente',
                    'reaproveitarList',
                    'reaproveitarPropostas',
                    'enviarParaFila',
                    'tiraPendencia',
                    'liberarPropostas',
                    'liberarPropostasAnalise',
                    'pendenciaSemear',
                    'maisConversa'

                ),
                'users' => array(
                    '@'
                )
            ),
            array('allow',
                'actions' => array('atualizarSeguro', 'minhasPropostas', 'propostasCrediarista', 'persist', 'more', 'concluir', 'alterarEntrada', 'alterarValorInicial'),
                'users' => array('@'),
                'expression' => 'Yii::app()->session["usuario"]->role == "crediarista" || '
                . 'Yii::app()->session["usuario"]->role == "emprestimo" '
            ),
            array('allow',
                'actions' => array('adminReanalises', 'ultimasPropostasNegadasReanalise', 'admin', 'ultimasPropostas', 'analise', 'aprovar', 'recusar', 'cancelarAnalise', 'desistencia'),
                'users' => array('@'),
                'expression' => 'Yii::app()->session["usuario"]->role == "analista_de_credito"'
            ),
            array('deny',
                'actions' => array('index'),
                'users' => array('*'),
            ),
            array('allow',
                'actions' => array('getNFe'),
                'users' => array('*'),
            ),
            array(
                'deny', // deny all users
                'users' => array(
                    '*'
                )
            )
        );
    }

    public function actionGetAnexoContrato()
    {

        $rows               = [];
        $anexos             = [];

        $recordsTotal       = 0;
        $recordsFiltered    = 0;
        $util               = new Util;

        $btn                = "";

        if(isset($_POST["Entidade"]))
        {
            $anexos = Anexo::model()->findAll("habilitado AND entidade_relacionamento = '" . $_POST["Entidade"] . "' AND id_entidade_relacionamento = " . $_POST["Entidade_id"]);

        }


        foreach ($anexos as $anexo)
        {
            $url                = '';

            if( $anexo->url != NULL ){
                $url = $anexo->url;
            }
            else{
                $url = $anexo->relative_path;
            }

            $btn    = $btn = '<a target="_blank" href="' . $anexo->relative_path . '"' . 'class="btn btn-primary" style="padding:2px 5px!important; font-size:11px!important;" ><i class=" clip-search"></i></a>';

            $row = array
            (
                'descricao'     => $anexo->descricao                                                                                            ,
                'ext'           => $anexo->extensao                                                                                             ,
                'data_envio'    => $util->bd_date_to_view(substr($anexo->data_cadastro, 0, 10)) . ' às ' . substr($anexo->data_cadastro, 10)    ,
                'btn'           => $btn
            );

            $rows[] = $row;

        }

        echo json_encode
        (
            array
            (
                "draw"              => $_POST['draw']   ,
                "recordsTotal"      => 0                ,
                "recordsFiltered"   => 0                ,
                "data"              => $rows            ,
            )
        );
    }

    public function actionAddAnexoContrato()
    {

        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && isset($_FILES["FileInputContrato"]) && $_FILES["FileInputContrato"]["error"] == UPLOAD_ERR_OK && !empty($_POST['Anexo']['descricao']))
        {
            /*echo json_encode(Anexo::model()->novo($_FILES["FileInputContrato"], $_POST["Entidade"], $_POST["Entidade_id"], $_POST["Anexo"]['descricao']));*/

            $S3Client = new S3Client;
            echo json_encode($S3Client->put($_FILES["FileInputContrato"], $_POST["Entidade"], $_POST["Entidade_id"], $_POST["Anexo"]['descricao']));
        }
    }

    public function actionListarBaixasParcela() {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $this->layout = '//layouts/template_vazio';

        $id = $_GET['id'];
        $banco = $_GET['banco'];

        $this->render('/baixa/lista', array(
            'id_parc' => $id,
            'banco' => $banco
        ));
    }

    public function actionAtualizarSeguro() {
        echo json_encode(Proposta::model()->atualizarSeguro($_POST['opcao'], $_POST['proposta']));
    }

    public function actionAtualizarSeguroNovo() {

        /*
            $_POST['opcao'      ] -- marcou como habilitar ou desabilitar
            $_POST['proposta'   ] -- id da proposta
            $_POST['seguro'     ] -- o id do produto
        */

        $proposta           = Proposta::model()->findByPk( $_POST['proposta'   ] );

        /*-------------------------------------------------------
            * A opção é remover o seguro, não importa qual seja ele
            * A proposta não estará mais marcada como segurada
            * Se houver produto atual na cesta de serviços, marca
        -------------------------------------------------------*/

        /*Apenas permite a operação, se for status igual a modificar*/
        if( $proposta->Status_Proposta_id   == 9    )
        {
            if( $_POST['opcao']             === '0' )
            {
                /**/
                $proposta->segurada         = 0;
                $proposta->valor_parcela    = ($proposta->valor - $proposta->valor_entrada) / $proposta->qtd_parcelas;

                if( $proposta->venda        !== NULL )
                {
                    if( ($proposta->venda->getProdutoSeguro() !== NULL) )
                    {
                        if( $proposta->venda->getProdutoSeguro()->Item_do_Estoque_id === $_POST['seguro'] )
                        {
                            $produtoSeguro              = $proposta->venda->getProdutoSeguro();
                            $produtoSeguro->habilitado  = 0;
                            $produtoSeguro->update();
                        }
                    }
                }
            }

            /*-------------------------------------------------------
                * A opção é adicionar o seguro
                * A proposta já pode ter um produto marcado, então
                * remove-se o marcado e cria um novo, caso ainda
                * o produto selecionado pode estar desativado (outrora
                * desativado pelo usuario) então apenas habilitamos o produto,
                * e desabilitamos os outros que possam estar habilitados
            -------------------------------------------------------*/
            else
            {
                $coberturaSeguro    = ConfigLN::model()->find("habilitado AND parametro = 'cobertura_seguro_".$_POST['seguro']."'");
                /*
                    * Vamos usar esta variável para decidir se criamos,
                    * ou não, um novo item desta venda
                */
                $produtoJaNaCesta   = false;

                /*
                    * Primeiro vamos verificar se este produto já existe na venda
                    * Se houver, vamos apenas atualizá-lo, setando habilitado 1,
                    * e atualizando o valor do produto, pois as condições (valor, entrada, etc) da proposta
                    * podem ter sido alterados
                */

                /*Tem venda?*/
                if( $proposta->venda !== NULL )
                {
                    if( $proposta->venda->itemDaVendas !== NULL )
                    {
                        /*
                            * Varre todos os itens da venda,
                            * para comparar ao produto selecionado
                        */
                        foreach( $proposta->venda->itemDaVendas as $item )
                        {
                            /*
                                * caso existe, vai ter algum item com o mesmo
                                * Item_do_Estoque_id do selecionado na view
                            */
                            if( $item->Item_do_Estoque_id   === $_POST['seguro'] )
                            {
                                $produtoJaNaCesta           = true;
                                $item->habilitado           = 1;
                                $item->update();
                                $proposta->atualizarValorProdutoSeguro();

                                /*
                                    * Todos os diferentes, desabilita
                                */
                                foreach ( $proposta->venda->itemDaVendas as $itemE )
                                {
                                    if( $itemE->id !== $item->id )
                                    {
                                        $itemE->habilitado = 0;
                                        $itemE->update();
                                    }
                                }
                            }
                        }

                        /*
                          * Após varrer, vamos verificar se há a necessidade
                          * de criar um novo item da venda
                        */
                        if( !$produtoJaNaCesta )
                        {
                            $novoItem           = ItemDaVenda::model()->novo($_POST['seguro'], $proposta->venda->id, 1, 1, 0, (($proposta->valor-$proposta->valor_entrada)/100)*intval($coberturaSeguro->valor), 1);

                            if( $novoItem !== NULL )
                            {
                                /*
                                    * Proposta com outro seguro habilitado
                                    * precisamos desabilitar
                                */
                                foreach ( $proposta->venda->itemDaVendas as $item )
                                {
                                    if( $novoItem->id !== $item->id )
                                    {
                                        $item->habilitado = 0;
                                        $item->update();
                                    }
                                }
                            }
                        }
                    }
                }

                /*
                    *Não tem venda, precisamos criar
                */
                else
                {
                    $novaVenda              = Venda::model()->novo(substr($proposta->codigo, -10), date('Y-m-d H:i:s'), $proposta->analiseDeCredito->Cliente_id, 1, NULL, $proposta->id);
                    $novoItem               = ItemDaVenda::model()->novo($_POST['seguro'], $novaVenda->id, 1, 1, 0, (($proposta->valor-$proposta->valor_entrada)/100)*intval($coberturaSeguro->valor), 1);
                }

                $proposta->segurada         = 1;
            }

            $proposta->Status_Proposta_id = 4;

            foreach (AnalistaHasPropostaHasStatusProposta::model()->findAll('Proposta_id = ' . $proposta->id) as $saa)
            {
                $saa->delete();
            }

            $proposta->update();
            if( $proposta->hasOmniConfig() != Null ){
              $proposta->alterarCondicoesOmni();
            }
        }
    }

    public function actionStatusChangeListener() {

        if (!empty($_POST['statusPropostaId']))
        {

            $proposta = Proposta::model()->find("habilitado AND id = " . $_POST['PropostaId']);

            if($proposta !== null && (Yii::app()->session["usuario"]->id == $proposta->analiseDeCredito->Usuario_id))
            {
                echo json_encode(Proposta::model()->getStatuInfo($_POST['statusPropostaId'], $_POST['PropostaId']));
            }
            else
            {
                echo json_encode(array());
            }

        }
        else
        {
            echo json_encode(array());
        }
    }

    public function actionPersist() {

        //echo json_encode([ $_POST['Proposta'],$_POST['clienteId'], $_POST['AnaliseDeCredito'], $_POST['Bens'] ]);

        $bens = [];

        if (isset($_POST['Bens']) && $_POST['Bens'] !== null) {
            $bens = $_POST['Bens'];
        }

        echo json_encode( Proposta::model()->persist($_POST['Proposta'], $_POST['clienteId'], $_POST['AnaliseDeCredito'], $bens, $_POST['emprestimo'], $_POST['semear'], $_POST['compraProgramada'] ) );
    }

    public function actionSimular() {
        echo json_encode(Proposta::model()->simularCondicoes($_POST['Proposta'], $_POST['semear']));
    }

    public function actionMaisConversa()
   {

      $baseUrl = Yii::app()->baseUrl;
      $cs = Yii::app()->getClientScript();
      $this->layout = '//layouts/template_vazio';


      /* -------------------------------------- */
      $id = $_GET['id'];
      $banco = $_GET['banco'];

      $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
      

      $proposta = Proposta::model()->findByPk($id);



      $this->render('mais_conversa_proposta', array(
          'proposta' => $proposta,
          'banco' => $banco
      ));
   }

    public function actionMaisDetalhes() {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $this->layout = '//layouts/template_vazio';


        /* -------------------------------------- */
        $id = $_GET['id'];
        $banco = $_GET['banco'];

        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        //$cs->registerScriptFile($baseUrl . '/js/fn-parcelas-proposta-table.js', CClientScript::POS_END);

        if($banco == 1){
            $proposta = Yii::app()->db->createCommand("SELECT P.* FROM beta.Proposta as P where P.id = ".$id." AND P.habilitado")->queryRow();
            $titulo = Yii::app()->db->createCommand("SELECT T.* FROM beta.Titulo as T where T.Proposta_id = ".$id." AND T.habilitado AND T.NaturezaTitulo_id = 1")->queryRow();
        }else{
            $proposta = Yii::app()->db->createCommand("SELECT P.* FROM nordeste2.Proposta as P where P.id = ".$id." AND P.habilitado")->queryRow();
            $titulo = Yii::app()->db->createCommand("SELECT T.* FROM nordeste2.Titulo as T where T.Proposta_id = ".$id." AND T.habilitado AND T.NaturezaTitulo_id = 1")->queryRow();
        }

        $this->render('mais_detalhes_proposta', array(
            'proposta' => $proposta,
            'titulo' => $titulo,
            'banco' => $banco
        ));
    }

    public function actionConcluir() {

        $code = $_POST['c'];

        $proposta = Proposta::model()->findByCode($code, 0);

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');

        $cs->registerScriptFile($baseUrl . '/js/fn-btn-acc-contrato.js', CClientScript::POS_END);

        $this->render('propostaSummary', array(
            'proposta' => $proposta,
            'data_primeira_parcela' => $proposta->getDataPrimeiraParcela(),
            'data_ultima_parcela' => $proposta->getDataUltimaParcela()
        ));
    }

    public function actionCrediaristaPropostas() {

    }

    public function actionMinhasPropostas() {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js');
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js');

        if (Yii::app()->session['usuario']->primeira_senha == 1) {

            $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/ui-modals.js', CClientScript::POS_END);
            $cs->registerScript('UIModals', 'UIModals.init();');
            $cs->registerScript('ShowModal', '$("#static").modal("show")');
        }
        if(Yii::app()->session['usuario']->primeira_senha == 0 && yii::app()->user->getState('userSessionTimeout') != 5000){
            $cs->registerScript('ShowModal', '$("#aviso_mp").modal("show")');
            yii::app()->user->setState('userSessionTimeout', 5000) ;
        }

        $this->render('/usuario/crediaristaDashboard');
    }

    public function actionAprovar() {

        $id = $_POST['id'];

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');

        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ui-modals.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ui-elements.js', CClientScript::POS_END);

        $cs->registerScriptFile($baseUrl . '/js/form-elements.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.inputlimiter.1.3.1.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.autosize.min.js', CClientScript::POS_END);

        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskMoney.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-timepicker.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/daterangepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.tagsinput.js', CClientScript::POS_END);

        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/summernote.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ckeditor/ckeditor.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ckeditor/adapter/jquery.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/formEmpresaValidation.js', CClientScript::POS_END);

        $cs->registerScriptFile($baseUrl . '/js/bootstrap-fileupload.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.pulsate.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/pages-user-profile.js', CClientScript::POS_END);

        $proposta = Proposta::model()->findByPk($id);
        $proposta->aprovar();

        $this->redirect($this->createUrl('admin'));
    }

    public function actionReaproveitarList() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-grid-reaproveitar.js', CClientScript::POS_END);

        $this->render('reaproveitarGrid');
    }

    public function actionLiberarPropostas() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-grid-liberar.js', CClientScript::POS_END);

        $this->render('liberarGrid');
    }

    public function actionAdmin() {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js',            CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/btn-status-fnv4.js?version=4.3', CClientScript::POS_END);

        if (Yii::app()->session['usuario']->primeira_senha == 1) {
            $cs->registerScript('ShowModal', '$("#static").modal("show")');
        }

        Proposta::model()->cancelarPropostas();

        $this->render('/usuario/analistaDashboard');
    }

    public function actionAdminReanalises() {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-tela-negadas-reanalisev2.js', CClientScript::POS_END);

        if (Yii::app()->session['usuario']->primeira_senha == 1) {
            $cs->registerScript('ShowModal', '$("#static").modal("show")');
        }

        $this->render('/usuario/analistaDashboard');
    }

    public function actionGetMensagens() {

        $propostaId = $_POST['propostaId'];

        if (isset($_POST['propostaId']) && !empty($_POST['propostaId'])) {
            //echo json_encode(Proposta::model()->listarMensagens($_POST['propostaId'], $_POST['draw']));
            echo json_encode(Proposta::model()->getComentariosOmni($_POST['propostaId'], $_POST['draw']));
        }
    }

    public function actionGetDetalhesFinanceiros() {

        $propostaId = $_POST['propostaId'];

        if (isset($_POST['propostaId']) && !empty($_POST['propostaId'])) {
            echo json_encode(Proposta::model()->getDetalhesFinanceiros($_POST['propostaId'], $_POST['draw']));
        }
    }

    public function actionMore() {
        $id = $_POST['id'];

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        /* Css */
        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/bootstrap-editable.css');

        /* JS */
        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.inputlimiter.1.3.1.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.autosize.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskMoney.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.mousewheel.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/perfect-scrollbar.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-editable.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-mensagens-analisev7-crediarista.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js', CClientScript::POS_END);

        $cs->registerScript('moneypicker', '$(".currency").maskMoney({decimal:",", thousands:".", allowZero: true});');
        $cs->registerScript('mousewheel', "$('.panel-scroll').perfectScrollbar({wheelSpeed: 50,minScrollbarLength: 20});");

        $proposta = Proposta::model()->findByPk($id);

        $sigacLog = new SigacLog;
        $sigacLog->saveLog('Análise de proposta', 'Proposta', $proposta->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Usuario " . Yii::app()->session['usuario']->username . " acessou a página da proposta " . $proposta->codigo, "127.0.0.1", NULL, Yii::app()->session->getSessionId());

        $data_primeira_parcela = date('d/m/Y', strtotime($proposta->data_cadastro . ' +' . $proposta->carencia . ' days'));
        $data_ultima_parcela = date('Y-m-d', strtotime($proposta->data_cadastro . ' +' . $proposta->carencia . ' days'));
        $data_ultima_parcela = date('d/m/Y', strtotime("+" . $proposta->qtd_parcelas - 1 . "months", strtotime($data_ultima_parcela)));

        //$cadastro = Cadastro::model()->find('Cliente_id = ' . $proposta->analiseDeCredito->cliente->id);

        $this->render('propostaResumeCrediarista', array(
            'proposta' => $proposta,
            'data_primeira_parcela' => $data_primeira_parcela,
            'data_ultima_parcela' => $data_ultima_parcela,
            'dialogo' => $proposta->getDialogo(),
        ));
    }

    public function actionAnalise() {
        $id = $_POST['id'];
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.inputlimiter.1.3.1.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);


        $cs->registerScriptFile($baseUrl . '/js/fn-mensagens-analisev11.js', CClientScript::POS_END);

        $cs->registerScriptFile($baseUrl . '/js/jquery.pulsate.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.mousewheel.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/perfect-scrollbar.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-editable.min.js', CClientScript::POS_END);

        $cs->registerScriptFile($baseUrl . '/js/fn-cliente-perfil-modals.js', CClientScript::POS_END);

        $cs->registerScript('mousewheel', "$('.panel-scroll').perfectScrollbar({wheelSpeed: 50,minScrollbarLength: 20});");

        $proposta = Proposta::model()->findByPk($id);
        $isEmprestimo = Emprestimo::model()->find('habilitado AND Proposta_id = ' . $proposta->id);

        $data_primeira_parcela = date('d/m/Y', strtotime($proposta->data_cadastro . ' +' . $proposta->carencia . ' days'));
        $data_ultima_parcela = date('Y-m-d', strtotime($proposta->data_cadastro . ' +' . $proposta->carencia . ' days'));
        $data_ultima_parcela = date('d/m/Y', strtotime("+" . $proposta->qtd_parcelas - 1 . "months", strtotime($data_ultima_parcela)));

        /* TRUE OR FALSE --- ANALISTA ATUAL ESTÁ ANLISANDO A PROPOSTA? */
        $analistaHasP = Yii::app()->session['usuario']->analisando($proposta);

        /*
          $this->render('propostaResume', array(
          'proposta'              => $proposta,
          'data_primeira_parcela' => $data_primeira_parcela,
          'data_ultima_parcela'   => $data_ultima_parcela,
          'dialogo'               => $proposta->getDialogo()
          ));
         */


        /* Se a proposta não possuir uma configuração Omni, então, significa que pode ser analisada internamente */
        if ($proposta->hasOmniConfig() !== NULL || $proposta->Status_Proposta_id == 2 || $proposta->Status_Proposta_id == 7 || $proposta->Status_Proposta_id == 3) {

            $sigacLog = new SigacLog;
            $sigacLog->saveLog('Análise Iniciada', 'Proposta', $proposta->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Analista " . Yii::app()->session['usuario']->nome_utilizador . " iniciou a análise da proposta com código: " . $proposta->codigo, "127.0.0.1", null, Yii::app()->session->getSessionId());

            $this->render('propostaResume', array(
                'proposta' => $proposta,
                'data_primeira_parcela' => $data_primeira_parcela,
                'data_ultima_parcela' => $data_ultima_parcela,
                'dialogo' => $proposta->getDialogo(),
                'cliente'=> $proposta->analiseDeCredito->cliente,
                'isEmprestimo' => $isEmprestimo
            ));
        } else {

            if ($proposta->Status_Proposta_id == 4)
            {

                $analistaHasPropHasStatusP = new AnalistaHasPropostaHasStatusProposta;
                $analistaHasPropHasStatusP->Analista_id = Yii::app()->session['usuario']->id;
                $analistaHasPropHasStatusP->Proposta_id = $proposta->id;
                $analistaHasPropHasStatusP->Status_Proposta_id = 1;
                $analistaHasPropHasStatusP->habilitado = 1;
                $analistaHasPropHasStatusP->data_cadastro = date('Y-m-d H:i:s');
                $analistaHasPropHasStatusP->updated_at = date('Y-m-d H:i:s');

                //try {

                    if ($analistaHasPropHasStatusP->save())
                    {

                        $proposta->Status_Proposta_id = 1;

                        if ($proposta->update())
                        {

                            $sigacLog = new SigacLog;
                            $sigacLog->saveLog('Análise Iniciada', 'Proposta', $proposta->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Analista " . Yii::app()->session['usuario']->nome_utilizador . " iniciou a análise da proposta com código: " . $proposta->codigo, "127.0.0.1", null, Yii::app()->session->getSessionId());

                            $this->render('propostaResume', array(
                                'proposta' => $proposta,
                                'data_primeira_parcela' => $data_primeira_parcela,
                                'data_ultima_parcela' => $data_ultima_parcela,
                                'dialogo' => $proposta->getDialogo(),
                                'cliente'=> $proposta->analiseDeCredito->cliente
                            ));

                        }
                        else
                        {

                            ob_start();
                            var_dump($proposta->getErrors());
                            $resultErro = ob_get_clean();

                            $htmlMail = "Erro: $resultErro";

                            $message            = new YiiMailMessage                            ;
                            $message->view      = "mandaMailFichamento"                         ;
                            $message->subject   = 'propostaResume .::. Erro proposta Update'    ;

                            $parametros         =   [
                                                        'email' =>  [
                                                                        'link'      => ""                                           ,
                                                                        'titulo'    => "propostaResume .::. Erro proposta Update"   ,
                                                                        'mensagem'  => $htmlMail                                    ,
                                                                        'tipo'      => "1"
                                                                    ]
                                                    ];

                            $message->setBody($parametros, 'text/html');

                            $message->addTo('contato@totorods.com');
                            $message->from = 'no-reply@credshow.com.br';

                            Yii::app()->mail->send($message);

                        }

                    }
                    else
                    {

                        ob_start();
                        var_dump($analistaHasPropHasStatusP->getErrors());
                        $resultErro = ob_get_clean();

                        $htmlMail = "Erro: $resultErro";

                        $message            = new YiiMailMessage                                        ;
                        $message->view      = "mandaMailFichamento"                                     ;
                        $message->subject   = 'propostaResume .::. Erro analistaHasPropostaHasStatus'   ;

                        $parametros         =   [
                                                    'email' =>  [
                                                                    'link'      => ""                                                       ,
                                                                    'titulo'    => "propostaResume .::. Erro analistaHasPropostaHasStatus"  ,
                                                                    'mensagem'  => $htmlMail                                                ,
                                                                    'tipo'      => "1"
                                                                ]
                                                ];

                        $message->setBody($parametros, 'text/html');

                        $message->addTo('contato@totorods.com');
                        $message->from = 'no-reply@credshow.com.br';

                        Yii::app()->mail->send($message);

                    }

                /*} catch (CDbException $e) {
                    $this->redirect('/proposta/admin/');
                }*/
            }
            else if ($analistaHasP != NULL)
            {

                if (!$analistaHasP->habilitado)
                {
                    $analistaHasP->habilitado = 1;
                }

                $analistaHasP->updated_at = date('Y-m-d H:i:s');

                if($analistaHasP->update())
                {

                    $sigacLog = new SigacLog;
                    $sigacLog->saveLog('Análise Iniciada', 'Proposta', $proposta->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Analista " . Yii::app()->session['usuario']->nome_utilizador . " iniciou a análise da proposta com código: " . $proposta->codigo, "127.0.0.1", null, Yii::app()->session->getSessionId());

                    $this->render('propostaResume', array(
                        'proposta' => $proposta,
                        'data_primeira_parcela' => $data_primeira_parcela,
                        'data_ultima_parcela' => $data_ultima_parcela,
                        'dialogo' => $proposta->getDialogo(),
                        'cliente'=> $proposta->analiseDeCredito->cliente
                    ));

                }
                else
                {

                    ob_start();
                    var_dump($analistaHasP->getErrors());
                    $resultErro = ob_get_clean();

                    $htmlMail = "Erro: $resultErro";

                    $message            = new YiiMailMessage                        ;
                    $message->view      = "mandaMailFichamento"                     ;
                    $message->subject   = 'propostaResume .::. Erro $analistaHasP'  ;

                    $parametros         =   [
                                                'email' =>  [
                                                                'link'      => ""                                       ,
                                                                'titulo'    => "propostaResume .::. Erro analistaHasP"  ,
                                                                'mensagem'  => $htmlMail                                ,
                                                                'tipo'      => "1"
                                                            ]
                                            ];

                    $message->setBody($parametros, 'text/html');

                    $message->addTo('contato@totorods.com');
                    $message->from = 'no-reply@credshow.com.br';

                    Yii::app()->mail->send($message);

                }

            }
            else
            {
                    /*
                    ob_start();
                    var_dump($analistaHasP->getErrors());
                $resultErro = ob_get_clean();
                */

                $htmlMail   = "Erro: <br>"
                            . "   Status Proposta: $proposta->Status_Proposta_id <br>";

                $message            = new YiiMailMessage                        ;
                $message->view      = "mandaMailFichamento"                     ;
                $message->subject   = 'propostaResume .::. Erro Voti'  ;

                $parametros         =   [
                                            'email' =>  [
                                                            'link'      => ""                                       ,
                                                            'titulo'    => "propostaResume .::. Erro voti"  ,
                                                            'mensagem'  => $htmlMail                                ,
                                                            'tipo'      => "1"
                                                        ]
                                        ];

                $message->setBody($parametros, 'text/html');

                $message->addTo('contato@totorods.com');
                $message->from = 'no-reply@credshow.com.br';

                Yii::app()->mail->send($message);

                $this->redirect('/proposta/admin/');

            }
        }
    }

    public function actionIndex() {

        $dataProvider = new CActiveDataProvider('Proposta');

        $this->render('index', array(
            'dataProvider' => $dataProvider
        ));
    }

    public function actionEnviarParaFila() {
        echo json_encode(Proposta::model()->enviarParaFila($_POST['Operacao']));
    }

    public function actionUltimasPropostas() {
        echo json_encode(Yii::app()->session['usuario']->listAnalistaPropostas($_POST['draw'], $_POST['start'], $_POST['codigo_filter'], $_POST['nome_filter'], FALSE, $_POST['semear'], $_POST['prestige']));
    }

    public function actionUltimasPropostasNegadasReanalise() {
        echo json_encode(Yii::app()->session['usuario']->listAnalistaPropostas($_POST['draw'], $_POST['start'], $_POST['codigo_filter'], $_POST['nome_filter'], TRUE));
    }

    public function actionReaproveitarPropostas() {
        echo json_encode(Yii::app()->session['usuario']->listarReanalise($_POST['draw'], $_POST['start'], $_POST['codigo_filter']));
    }

    public function actionLiberarPropostasAnalise() {
        echo json_encode(Yii::app()->session['usuario']->listarLiberar($_POST['draw'], $_POST['start'], $_POST['codigo_filter']));
    }

    public function actionPropostasCrediarista() {
        echo json_encode(Yii::app()->session['usuario']->listCrediaristaPropostas(array(4, 1, 2, 9, 3), $_POST['draw'], $_POST['start']));
    }

    public function actionDesistencia() {
        $proposta_id = $_POST['proposta_id'];
        $proposta = Proposta::model()->findByPk($proposta_id);

        if ($proposta->desistirAnalise()) {
            $analistaHasPropHasStatusProp = AnalistaHasPropostaHasStatusProposta::model()->find('Proposta_id = ' . $proposta->id . ' AND Analista_id = ' . Yii::app()->session['usuario']->id . ' AND habilitado AND Status_Proposta_id = 1');

            if ($analistaHasPropHasStatusProp != NULL) {
                $analistaHasPropHasStatusProp->habilitado = 0;
                $analistaHasPropHasStatusProp->update();
            }
        }

        $this->redirect($this->createUrl('admin'));
    }

    public function loadModel($id) {

        $model = Proposta::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        return $model;
    }

    public function actionRecusar() {

        $proposta_id = $_POST['proposta_id'];
        $motivoDeNegacao_id = $_POST['MotivoDeNegacao'];
        $obs = $_POST['obs'];

        $proposta = Proposta::model()->findByPk($proposta_id);
        $proposta->recusar($motivoDeNegacao_id, $obs);

        $this->redirect($this->createUrl('admin'));
    }

    public function actionCancelarAnalise() {

        $proposta_id = $_POST['proposta_id'];
        $proposta = Proposta::model()->findByPk($proposta_id);
        $proposta->cancelarAnalise();

        $analistaHasPropHasStatusProp = AnalistaHasPropostaHasStatusProposta::model()->find('Proposta_id = ' . $proposta->id . ' AND Analista_id = ' . Yii::app()->session['usuario']->id . ' AND habilitado AND Status_Proposta_id = 1');

        if ($analistaHasPropHasStatusProp != NULL) {
            $analistaHasPropHasStatusProp->delete();
        }

        $this->redirect($this->createUrl('admin'));
    }

    public function actionPropostasCrediaristaCharts() {

        if (!empty($_POST['userId'])) {
            echo json_encode(Proposta::model()->propostasCrediaristaCharts($_POST['userId']));
        }
    }

    public function actionAlterarCondicoes() {

        $propostaId = $_POST['propostaId'];
        $cotacao_id = $_POST['cotacao_id'];
        $carencia = $_POST['carencia'];
        $numero_parcelas = $_POST['numero_parcelas'];
        $val_parcelas = $_POST['val_parcelas'];

        echo json_encode(Proposta::model()->alterarCondicoes($propostaId, $cotacao_id, $carencia, $numero_parcelas, $val_parcelas));
    }

    public function actionAlterarEntrada() {

        echo json_encode(Proposta::model()->atualizarEntrada($_POST['propostaId'], $_POST['entrada']));
    }

    public function actionAlterarValorInicial() {

        echo json_encode(Proposta::model()->atualizarValorInicial($_POST['pk'], $_POST['value']));
    }

    public function actionGetPropostaCodigo() {
        //echo json_encode(Proposta::model()->getByCodigo($_POST['codigo'], $_POST['parceiroId'], $_POST['itensPgtoAprovados'], $_POST['totalAtualGrid'], $_POST['totalAtualGridFinan']));
        echo json_encode(Proposta::model()->getByCodigoContrato($_POST['codigo'], $_POST['itensPgtoAprovados'], $_POST['totalAtualGrid'], $_POST['totalAtualGridFinan'], $_POST['porImagem']));
    }

    //--------------inicio alteracao-------------------
    public function actionTiraPendencia() {
        echo json_encode(Proposta::model()->desfazerPendencia($_POST['codigo']));
    }

    //--------------fim alteracao----------------------

    protected function performAjaxValidation($model) {

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'proposta-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionPendenciaSemear(){
        $id = $_POST['id_prop'];
        $mensagem = $_POST['Mensagem'];
        $did = $_POST['Dialogo_id'];
        if(isset($id) && isset($mensagem) && isset($did)){
            $proposta = Proposta::model()->find("habilitado AND id = " . $id);
            Mensagem::model()->novo($mensagem['conteudo'], Yii::app()->session['usuario']->id, $did);
            echo json_encode($proposta->pendenciaSemear($mensagem));
        }else{
            echo json_encode("");
        }
    }

    public function actionGetContratoOmni() {
        $this->layout = '//layouts/main_sigac_template';
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');

        $retorno = null;

        if (isset($_POST['Importacao'])) {
            $retorno = Proposta::model()->resgatarCodigoContrato($_POST['Importacao']);
        }

        $this->render('getContratoOmni', ['r' => $retorno]);
    }

    public function actionConsultarCliente() {

        $cgc = trim($_POST['cgc']);

        $consultaCliente = new ConsultaCliente;

        $consultaCliente->consultar($cgc, 1);

        $sigacLog = new SigacLog;
        $sigacLog->saveLog('Consulta ao SPC Realizada', 'Cliente', $consultaCliente->nomeConsumidor, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Analista " . Yii::app()->session['usuario']->nome_utilizador . " consultou dados de um cliente no SPC", "127.0.0.1", null, Yii::app()->session->getSessionId());

        if ($consultaCliente->retorno != null) {
            $arrayResposta = array(
                'restricao' => $consultaCliente->restricao,
                'protocolo' => $consultaCliente->protocolo,
                'operador' => $consultaCliente->nomeOperador,
                'nomeConsumidor' => $consultaCliente->nomeConsumidor,
                'cpfConsumidor' => $consultaCliente->cgcConsumidor,
                'nascimentoConsumidor' => $consultaCliente->nascimentoConsumidor,
                'maeConsumidor' => $consultaCliente->maeConsumidor,
                'logradouroConsumidor' => $consultaCliente->logradouroConsumidor,
                'numEndConsumidor' => $consultaCliente->numEndConsumidor,
                'bairroConsumidor' => $consultaCliente->bairroConsumidor,
                'comEndConsumidor' => $consultaCliente->comEndConsumidor,
                'cidadeConsumidor' => $consultaCliente->cidadeConsumidor,
                'ufConsumidor' => $consultaCliente->ufConsumidor,
                'cepConsumidor' => $consultaCliente->cepConsumidor,
                /* 'qtdDadosTelefonicos'   => $consultaCliente->qtdDadosTelefonicos  ,
                  'ultDadosTelefonicos'   => $consultaCliente->ultDadosTelefonicos  ,
                  'vlrDadosTelefonicos'   => $consultaCliente->vlrDadosTelefonicos  , */
                'qtdTelefoneVinculado' => $consultaCliente->qtdTelefoneVinculado,
                'ultTelefoneVinculado' => $consultaCliente->ultTelefoneVinculado,
                'vlrTelefoneVinculado' => $consultaCliente->vlrTelefoneVinculado,
                'qtdRegistroSPC' => $consultaCliente->qtdRegistroSPC,
                'ultRegistroSPC' => $consultaCliente->ultRegistroSPC,
                'vlrRegistroSPC' => $consultaCliente->vlrRegistroSPC,
                'qtdRegistroSerasa' => $consultaCliente->qtdRegistroSerasa,
                'ultRegistroSerasa' => $consultaCliente->ultRegistroSerasa,
                'vlrRegistroSerasa' => $consultaCliente->vlrRegistroSerasa,
                'qtdPoderJudiciario' => $consultaCliente->qtdPoderJudiciario,
                'ultPoderJudiciario' => $consultaCliente->ultPoderJudiciario,
                'vlrPoderJudiciario' => $consultaCliente->vlrPoderJudiciario,
                'qtdConsultaRealizada' => $consultaCliente->qtdConsultaRealizada,
                'ultConsultaRealizada' => $consultaCliente->ultConsultaRealizada,
                'vlrConsultaRealizada' => $consultaCliente->vlrConsultaRealizada,
                'qtdAlertaDocumentos' => $consultaCliente->qtdAlertaDocumentos,
                'ultAlertaDocumentos' => $consultaCliente->ultAlertaDocumentos,
                'vlrAlertaDocumentos' => $consultaCliente->vlrAlertaDocumentos, /*
                  'qtdCreditoConcedido'   => $consultaCliente->qtdCreditoConcedido  ,
                  'ultCreditoConcedido'   => $consultaCliente->ultCreditoConcedido  ,
                  'vlrCreditoConcedido'   => $consultaCliente->vlrCreditoConcedido  ,
                  'qtdContraOrdem'        => $consultaCliente->qtdContraOrdem       ,
                  'ultContraOrdem'        => $consultaCliente->ultContraOrdem       ,
                  'vlrContraOrdem'        => $consultaCliente->vlrContraOrdem       ,
                  'qtdCODocDif'           => $consultaCliente->qtdCODocDif          ,
                  'ultCODocDif'           => $consultaCliente->ultCODocDif          ,
                  'vlrCODocDif'           => $consultaCliente->vlrCODocDif */
                'HTMLSPC' => $consultaCliente->HTMLSPC,
                'HTMLConsulta' => $consultaCliente->HTMLConsulta,
                'HTMLSerasa' => $consultaCliente->HTMLSerasa
            );
        } else {
            $arrayResposta = ["erro" => 'deu merda!'];
        }
        echo json_encode($arrayResposta);
    }

    public function actionGetNFe()
    {

        $data = [];

        if(isset($_POST["idProposta"]) && !empty($_POST["idProposta"]))
        {

            $proposta = Proposta::model()->find("habilitado AND id = " . $_POST["idProposta"]);

            if($proposta !== null)
            {

                $notas = $proposta->getNFs();

                foreach ($notas as $nf)
                {

                    $btn    = "<button class='btn btn-blue btnAnexarNF' value='$nf->id'>"
                            . "     <i class='clip-attachment'></i>"
                            . "</button>";

                    $anexo = Anexo::model()->find("habilitado AND entidade_relacionamento = 'NotaFiscal' AND id_entidade_relacionamento = $nf->id");

                    $date       = new DateTime  ($nf->data  )   ;
                    $dataNF     = $date->format ("d/m/Y"    )   ;

                    if($anexo !== null)
                    {
                        $serie      = "<a href='$anexo->relative_path' target='blank'>$nf->serie       </a>";
                        $documento  = "<a href='$anexo->relative_path' target='blank'>$nf->documento   </a>";
                        $chave      = "<a href='$anexo->relative_path' target='blank'>$nf->chaveNFe    </a>";
                        $dataNF     = "<a href='$anexo->relative_path' target='blank'>$dataNF          </a>";
                        $observacao = "<a href='$anexo->relative_path' target='blank'>$nf->observacao  </a>";
                    }
                    else
                    {
                        $serie      = $nf->serie                    ;
                        $documento  = $nf->documento                ;
                        $chave      = $nf->chaveNFe                 ;
                        $observacao = $nf->observacao               ;
                    }

                    $data[] =   [
                                    "idNF"          => $nf->id      ,
                                    "serie"         => $serie       ,
                                    "documento"     => $documento   ,
                                    "data"          => $dataNF      ,
                                    "chave"         => $chave       ,
                                    "observacao"    => $observacao  ,
                                    "btn"           => $btn
                                ];
                }

            }

        }

        echo json_encode
        (
            [
                "draw"              => $_POST["draw"]   ,
                "data"              => $data            ,
                "recordsTotal"      => count($data)     ,
                "recordsFiltered"   => count($data)
            ]
        );

    }

}
