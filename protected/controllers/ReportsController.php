<?php

class ReportsController extends Controller {

    public $layout = '//layouts/main_sigac_template';

    public function actionIndex() {

        echo json_encode(Yii::app()->session['usuario']->getEmpresa()->getFinanciamentos($_POST['draw'], $_POST['start'], $_POST['dataDe'], $_POST['dataAte'], $_POST['filiais'], $_POST['qtd_par_de'], $_POST['qtd_par_ate']));
    }

    public function actionComparativoAnalistas() {
        echo json_encode(Usuario::model()->performanceComparativo($_POST['tipo'], $_POST['analistas'], $_POST['ano']));
    }
    
    public function actionRelatorioTitulos(){
        $this->layout = '//layouts/main_sigac_template';

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/blockInterface.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/financeiro/fn-gerar-relatorio.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);

        $this->render('rel_titulos');
    }
    
    public function actionRelatorioTitulosCorban(){
        $this->layout = '//layouts/main_sigac_template';

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/blockInterface.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/financeiro/fn-gerar-relatorio-corban.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);

        $this->render('rel_titulos_corban');
    }
    
    public function actionProducaoAnalistas() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jsapi', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-report-producao-analistas.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);


        $this->render('producaoAnalistas', array(
            'appConfig' => SigacConfig::model()->find('habilitado')
        ));
    }

    public function actionHistoricoAnalista() {
        echo json_encode(Yii::app()->session['usuario']->meuHistoricoAnalista($_POST['aprneg'], $_POST['draw'], $_POST['start'], $_POST['dataDe'], $_POST['dataAte'], array(), 0, 10));
    }

    public function actionMinhaProducao() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jsapi', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-report-producao-analista.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);

        $cs->registerScript('datepicker', "$('.date-picker').datepicker({autoclose: true});");

        $this->render('producaoAnalistaReport');
    }

    public function actionFinanciamentos() {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jsapi', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-report-financiamentos.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);

        $cs->registerScript('datepicker', "$('.date-picker').datepicker({autoclose: true});");

        $this->render('financiamentosReport');
    }

    public function actionAnalisarBads() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/reports/fn-analise-de-bads.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);

        $cs->registerScript('datepicker', "$('.date-picker').datepicker({autoclose: true});");

        $this->render('analiseDeBads');
    }

    public function actionReceberVencimento() {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/reports/receberVencimento/fn-relatorio.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);

        $cs->registerScript('datepicker', "$('.date-picker').datepicker({autoclose: true});");

        $this->render('receberVencimento');
    }

    public function actionJurosAtraso() {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/reports/jurosAtraso/fn-relatorio.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);

        $cs->registerScript('datepicker', "$('.date-picker').datepicker({autoclose: true});");

        $this->render('jurosAtraso');
    }

    public function actionRelatorioNordeste() {
        yii::app()->user->setState('userSessionTimeout', 21600);
        set_time_limit(-1);

        $fileName = $_POST['nome_arquivo'] . ".csv";
        ini_set('memory_limit', '2048M');
        header("Content-type: text/csv; charset=utf-8");
        header("Content-Disposition: attachment; filename=" . $fileName);
        header("Pragma: no-cache");
        header("Expires: 0");

        $query = "SELECT DISTINCT UPPER(PESS.nome) AS Nome, 

        P.id AS idProposta,

        AC.id AS idAnalise,
        
        PAR.id AS idParcela,

        PESS.id AS idPessoa,

        BORD.id AS idBordero,
        
        AC.Cliente_id AS idCliente, 

        PESS.Contato_id AS idContato,
        
        D.numero AS CPF,

        CASE
            WHEN PAR.seq = 1 OR (DP.id IS NOT NULL AND PAR.id IS NULL) then DP.profissao
            ELSE ' '
        END AS Profissao,

        CASE
            WHEN PAR.seq = 1 OR (PESS.id IS NOT NULL AND PAR.id IS NULL) then DATE_FORMAT(PESS.nascimento,'%d/%m/%Y')
            ELSE ' '
        END AS Nascimento,

        CASE
            WHEN PAR.seq = 1 OR (P.id IS NOT NULL AND PAR.id IS NULL) then P.valor
            ELSE ' '
        END AS ValorNominal,

        CASE
            WHEN PAR.seq = 1 OR (B2.id IS NOT NULL AND PAR.id IS NULL) then B2.valor_pago 
            ELSE ' '
        END AS ValorRepasse,

        P.codigo AS NumeroProposta,

        PAR.seq AS Sequencial,

        PAR.ref_lecca AS Referencia,

        PAR.valor AS ValorParcela,

        DATE_FORMAT(PAR.vencimento,'%d/%m/%Y') AS Vencimento,

         R.label AS NossoNumero,

        CASE
            WHEN B.baixado = 1 then 'Sim'
            ELSE 'Não'
        END AS Pago,

        DATE_FORMAT(B.data_da_ocorrencia,'%d/%m/%Y') AS DataPagamento,

        BAN.nome AS Banco,

        B.valor_pago AS ValorPago,

        CASE
            WHEN PAR.valor_atual > 0 then (PAR.valor_atual - PAR.valor)
            ELSE '0'
        END AS Juros,

        CASE
            WHEN FI.Parcela IS NOT NULL AND FS.StatusFichamento_id IN (1,2) then 'Sim'
            ELSE 'Nao'
        END AS ParcelaFichada,

        CASE
            WHEN (F.id IS NOT NULL) then concat(upper(F.nome_fantasia),' - ',upper(concat(ENDE.cidade,'/',ENDE.uf)))
            ELSE ' '
        END AS EmissorContrato,

        CASE
            WHEN PAR.seq = 1 OR P.id IS NOT NULL then DATE_FORMAT(P.data_cadastro,'%d/%m/%Y')
            ELSE ' '
        END AS DataCompra,

        CASE
            WHEN (FIN.id IS NOT NULL AND PAR.id IS NULL AND POC.id IS NOT NULL) then 'OMNI'
            WHEN PAR.seq IS NOT NULL OR (FIN.id IS NOT NULL AND PAR.id IS NULL) then upper(FIN.nome)
            ELSE ' '
        END AS Financeira,

        CASE
            WHEN (PAR.seq = 1 OR (P.id IS NOT NULL AND PAR.id IS NULL)) AND P.Status_Proposta_id = 8 then 'Sim'
            ELSE 'Não'
        END AS Cancelado,

        CASE
            WHEN (PAR.seq = 1 OR (P.id IS NOT NULL AND PAR.id IS NULL)) AND P.segurada = 1 then 'Sim'
            WHEN (PAR.seq = 1 OR (P.id IS NOT NULL AND PAR.id IS NULL)) AND P.segurada = 0 then 'Nao'
            ELSE ' '
        END AS Segurada,

        CASE
            WHEN (PAR.seq = 1 OR (VEND.id IS NOT NULL AND PAR.id IS NULL))  AND (P.segurada = 1 AND VEND.id IS NOT NULL) then 'Usebens'
                WHEN (PAR.seq = 1 OR ((VEND.id IS NOT NULL OR VEND.id IS NULL) AND PAR.id IS NULL))  AND (P.segurada = 1 AND VEND.id IS NULL) then 'Credshow'
            ELSE ' '
        END AS Seguradora,

        CASE
            WHEN PAR.seq = 1 OR (USU.id IS NOT NULL AND PAR.id IS NULL) then upper(USU.nome_utilizador)
            ELSE ' '
        END AS Crediarista,

        CASE
            WHEN (PAR.seq = 1 OR (USU2.id IS NOT NULL AND PAR.id IS NULL)) then USU2.nome_utilizador
            ELSE ' '
        END AS Analista,

       

        CASE

            WHEN (PAR.seq = 1 OR (P.id IS NOT NULL AND PAR.id IS NULL))  then FIC.classe
            ELSE ' '
        END AS Score,

        CASE
            WHEN (PAR.seq = 1 OR (B2.id IS NOT NULL AND PAR.id IS NULL)) AND B2.id IS NOT NULL THEN 'Sim'
            ELSE ' '
        END AS PagoParceiro,


         CASE
            WHEN (PAR.seq = 1 OR (BORD.id IS NOT NULL AND PAR.id IS NULL)) AND B2.id IS NOT NULL AND BORD.id IS NOT NULL THEN BORD.id
            ELSE ' '
        END AS Bordero,

        CASE
            WHEN (PAR.seq = 1 OR (B2.id IS NOT NULL AND PAR.id IS NULL)) AND B2.id IS NOT NULL then DATE_FORMAT(B2.data_da_baixa,'%d/%m/%Y')
            ELSE ' '
        END AS DataRepasse,

        CASE
            WHEN (PAR.seq = 1 OR (IB.id IS NOT NULL OR IR.id IS NOT NULL AND PAR.id IS NULL)) AND (IB.id IS NOT NULL OR IR.id IS NOT NULL) then 'Sim'
            WHEN (PAR.seq = 1 OR (IB.id IS NOT NULL OR IR.id IS NOT NULL AND PAR.id IS NULL)) AND IB.id IS NULL AND IR.id IS NULL then 'Nao'
            ELSE ''
        END AS ContratoFormalizado,

        CASE
            WHEN (PAR.seq = 1 OR (IR.id IS NOT NULL  AND PAR.id IS NULL)) AND IR.id IS NOT NULL then DATE_FORMAT(IR.data_cadastro,'%d/%m/%Y')
            ELSE ' '
        END AS DataFormalizacao

        FROM Proposta AS P
        LEFT JOIN Analise_de_Credito AS AC ON P.Analise_de_Credito_id = AC.id AND AC.habilitado
        LEFT JOIN Filial AS F ON F.id = AC.Filial_id AND F.habilitado
        LEFT JOIN Filial_has_Endereco AS FhE ON FhE.Filial_id = F.id AND FhE.habilitado
        LEFT JOIN Banco AS BAN ON BAN.id = P.Banco_id
        LEFT JOIN Cliente AS C ON C.id = AC.Cliente_id AND C.habilitado
        LEFT JOIN ItemDoBordero AS IB ON IB.Proposta_id = P.id AND IB.habilitado
        LEFT JOIN Bordero AS BORD ON BORD.id = IB.Bordero AND BORD.habilitado
        LEFT JOIN Pessoa AS PESS ON PESS.id = C.Pessoa_id AND PESS.habilitado
        LEFT JOIN Dados_Profissionais AS DP ON DP.Pessoa_id = PESS.id AND DP.habilitado
        LEFT JOIN Pessoa_has_Documento AS PhD ON PhD.Pessoa_id = PESS.id AND PhD.habilitado
        LEFT JOIN Contato_has_Telefone AS ChT ON ChT.Contato_id = PESS.Contato_id AND ChT.habilitado   
        LEFT JOIN Documento AS D ON D.id = PhD.Documento_id
        LEFT JOIN Titulo AS T ON T.Proposta_id = P.id AND T.habilitado AND T.NaturezaTitulo_id = 1
        LEFT JOIN Parcela AS PAR ON PAR.Titulo_id = T.id AND PAR.habilitado
        LEFT JOIN Fichamento AS FI ON FI.Parcela = PAR.id AND FI.habilitado
        LEFT JOIN Fichamento_has_StatusFichamento AS FS ON FS.Fichamento_id = FI.id AND FS.habilitado
        LEFT JOIN Ranges_gerados AS R ON PAR.id = R.Parcela_id
        LEFT JOIN Baixa AS B ON B.Parcela_id = PAR.id AND B.baixado
        LEFT JOIN Financeira AS FIN ON FIN.id = P.Financeira_id
        LEFT JOIN Endereco AS ENDE ON ENDE.id = FhE.Endereco_id AND ENDE.habilitado
        LEFT JOIN Ficha AS FIC ON FIC.Pessoa_id = PESS.id AND FIC.habilitado AND LEFT(P.data_cadastro,10) <= LEFT(FIC.data_expiracao,10)
        LEFT JOIN Usuario AS USU ON USU.id = AC.Usuario_id
        LEFT JOIN Analista_has_Proposta_has_Status_Proposta AS AhP ON AhP.Proposta_id = P.id 
        LEFT JOIN Usuario AS USU2 ON USU2.id = AhP.Analista_id 
        LEFT JOIN Titulo AS T2 ON T2.Proposta_id = P.id AND T2.habilitado AND T2.NaturezaTitulo_id = 2
        LEFT JOIN Parcela AS PAR2 ON PAR2.Titulo_id = T2.id AND PAR2.habilitado
        LEFT JOIN ItemRecebimento AS IR ON IR.Proposta_id = P.id AND IR.habilitado
        LEFT JOIN Baixa AS B2 ON B2.Parcela_id = PAR2.id AND B2.baixado
        LEFT JOIN Venda AS VEND ON VEND.Proposta_id = P.id AND VEND.habilitado
        LEFT JOIN Item_da_Venda AS IV ON IV.Venda_id = VEND.id AND IV.habilitado
        LEFT JOIN Produto AS PROD ON PROD.id = IV.Item_do_Estoque_id AND PROD.habilitado
        LEFT JOIN PropostaOmniConfig AS POC ON POC.habilitado AND POC.Proposta = P.id 
        WHERE P.habilitado AND P.Status_Proposta_id IN(2,7,8) AND CASE WHEN D.id IS NOT NULL THEN D.Tipo_documento_id = 1 ELSE 0 END    
        LIMIT 50000 OFFSET " . $_POST['offset'];

        try {

            $resultado = Yii::app()->db->createCommand($query)->queryAll();

            $header = array('Nome', 'CPF', 'Profissao', 'Nascimento', 'Valor Nominal', 'Valor Repasse', 'NumeroProposta', 'Sequencial', 'Referencia',
                'ValorParcela', 'Vencimento', 'NossoNumero', 'Pago', 'DataPagamento', 'Banco', 'ValorPago', 'Juros', 'Endereco',
                'Estado', 'Cidade', 'Telefone(s)','Referencias Clientes', 'ParcelaFichada', 'EmissorContrato', 'DataCompra', 'Produto(s)', 'Financeira',
                'Pago ao Parceiro', 'Bordero', 'Lote', 'Data Repasse', 'Cancelado', 'Contrato Formalizado', 'DataFormalizacao', 'Segurada','ValorSeguro','Seguradora', 'Crediarista', 'Analista', 'Score', 'ObsCobranca');

            $list = array();
            array_push($list, $header);


            foreach ($resultado as $data) {
                
                $resultReferencias = array();
                $resultCobranca = array();
                $resultProduto = array();
                $resultTelefone = array();
                $resultLote = array();
                $resultEndereco = array();
                $arrItensProdutos = array();
                $arrItensTelefones = array();
                $arrItensLote = array();
                $arrItensEndereco = array();
                
                $resultsequencia = array();

                $cidade = '';
                $estado = '';
                $atendimento = '';
                $cob = '';
                $chamado = '';
                $ref = '';
                $referencia = '';
                
                // Lista nomes e telefones das referencias 
                
                if ($data['idContato'] !== NULL && !empty($data['idContato'])) {
                    
                    $ref = Referencia::model()->findAll('Cliente_id = ' . $data['idCliente']);
                    
                    foreach ($ref as $r) {
                                              
                        $contato = ContatoHasTelefone::model()->find('Contato_id = ' . $r->Contato_id);
                        
                        $telefone = Telefone::model()->find('id = ' . $contato->Telefone_id);
                        
                        if ($data["Sequencial"] == 1 OR $contato !== null && $data["Sequencial"] == null){
                            $resultReferencias[] = $r->nome . ' - ' . $telefone->discagem_direta_distancia . ' ' . $telefone->numero . '//';
                        }                 
                    }
                    
                }
                    
                   
                    
                
                
                //Pegar valor do seguro
                
                
                if ($data["idProposta"] !== NULL && !empty($data["idProposta"])) {
                    
                    $prop = Proposta::model()->findByPk($data["idProposta"]);
                    
                    if ($prop !== NULL && !empty($prop) && $data["Sequencial"] == 1 OR  $prop !== NULL && !empty($prop) && $data["Sequencial"] == NULL) {
                        
                        $valorSeguro = number_format($prop->calcularValorDoSeguro(), 2);
                        
                    } else {
                        $valorSeguro = '';
                    }
                }
                 
                // Lista as cobranças das parceletas
                if ($data["idParcela"] !== NULL && !empty($data["idParcela"])) {
                    $atendimentos = AtendimentoHasParcela::model()->findAll('Parcela_id = ' . $data["idParcela"]);
                    
                               
                    foreach ($atendimentos as $atend) {
                        if($atend !== NULL && !empty($atend)) {
                            
                            $chamado = Chamado::model()->find('Atendimento_id = ' . $atend->Atendimento_id);
                            
                            $sequencias = Sequencia::model()->findAll('Chamado_id =' . $chamado->id);
                            
                            foreach ($sequencias as $s) {
                                $resultsequencia[] = Sequencia::model()->find('Chamado_id = ' . $chamado->id);
                            }
                            
                            //$sequencia = Sequencia::model()->find('Chamado_id = ' . $chamado->id);
                            
                            //$usuario = Usuario::model()->find('id = ' . $sequencia->Usuario_id);
                            
                            foreach ($sequencias as $r) {
                                $usuario = Usuario::model()->find('id = ' . $r->Usuario_id);
                                $resultCobranca[] = '  Obs: ' . str_replace("\n", "", $r->observacao)  . ' - Data da Cobranca: ' . date("d-m-Y", strtotime($r->data_cadastro)) . '- Usuario Cobranca: ' . $usuario->nome_utilizador;
                            }
                            
                            //$resultCobranca[] = 'Obs: ' . $sequencia->observacao . ' // Data da Cobranca: ' . date("d-m-Y", strtotime($sequencia->data_cadastro));
                        }
                    }
                }
                    
                if (isset($data["idPessoa"]) && !empty($data["idPessoa"])) {
                    $pessoaHasEndereco = PessoaHasEndereco::model()->findAll('habilitado AND Pessoa_id = ' . $data["idPessoa"]);
                }

                if (isset($data["idBordero"]) && !empty($data["idBordero"])) {
                    $loteHasBordero = LoteHasBordero::model()->findAll('habilitado AND Bordero_id = ' . $data["idBordero"]);
                }

                if (isset($data["idAnalise"]) && !empty($data["idAnalise"])) {
                    $analiseHasItens = AnaliseHasSubgrupoDeProduto::model()->findAll('Analise_de_Credito_id = ' . $data["idAnalise"]);
                }

                if (isset($data["idContato"]) && !empty($data["idContato"])) {
                    $contatoHasTelefone = ContatoHasTelefone::model()->findAll('habilitado AND Contato_id = ' . $data["idContato"]);
                }


                // Lista os Enderecos
                foreach ($pessoaHasEndereco as $phe) {
                    
                    if ($phe !== NULL && !empty($phe)) {
                        $enderecos = Endereco::model()->find("habilitado AND id = $phe->Endereco_id");
                    }

               
                    if ($enderecos !== null && !empty($enderecos) && $data["Sequencial"] == 1 OR ( $enderecos !== null && !empty($enderecos) && $data["Sequencial"] == null)) {

                        array_push($arrItensEndereco, $enderecos);

                        $resultEndereco[] = $enderecos->logradouro . ', ' . $enderecos->numero . ', ' . $enderecos->bairro . ' - ' . $enderecos->cep;

                        $cidade = $enderecos->cidade;

                        $estado = $enderecos->uf;
                    }
                }



                // Lista os lotes
                foreach ($loteHasBordero as $lhb) {

                    if ($lhb !== NULL && !empty($lhb)) {
                        $lotes = LotePagamento::model()->find("habilitado AND id = $lhb->Lote_id");
                    }
                    

                    if ($lotes !== null && !empty($lotes)) {
                        array_push($arrItensLote, $lotes);
                        $resultLote[] = $lotes->id;
                    }
                }


                // Lista os telefones do cliente
                foreach ($contatoHasTelefone as $cht) {
                    
                    if ($cht !== NULL && !empty($cht)) {
                        $contato = Telefone::model()->find("habilitado AND id = $cht->Telefone_id");
                    }
                    
                    

                    if ($contato !== null && $data["Sequencial"] == 1 OR $contato !== null && $data["Sequencial"] == null) {
                        array_push($arrItensTelefones, $contato);
                        $resultTelefone[] = $contato->discagem_direta_distancia . ' ' . $contato->numero;
                    }
                }

                // Lista os produtos comprados pelo cliente
                foreach ($analiseHasItens as $ahi) {
                    
                    if ($ahi !== NULL && !empty($ahi)) {
                        $item = SubgrupoDeProduto::model()->find("id = $ahi->SubgrupoDeProtudo_id");
                    } 
                    

                    if ($item !== null &&  !empty($item) && $data["Sequencial"] == 1 OR $item !== null && !empty($item) && $data["Sequencial"] == null) {
                        array_push($arrItensProdutos, $item);
                        $resultProduto[] = $item->descricao;
                    }
                }

                $row = array(
                    $data["Nome"],
                    $data["CPF"],
                    $data["Profissao"],
                    $data["Nascimento"],
                    $data["ValorNominal"],
                    $data["ValorRepasse"],
                    $data["NumeroProposta"],
                    $data["Sequencial"],
                    $data["Referencia"],
                    $data["ValorParcela"],
                    $data["Vencimento"],
                    $data["NossoNumero"],
                    $data["Pago"],
                    $data["DataPagamento"],
                    $data["Banco"],
                    $data["ValorPago"],
                    $data["Juros"],
                    join(" // ", $resultEndereco),
                    $estado,
                    $cidade,
                    join(" // ", $resultTelefone),
                    join(" // ", $resultReferencias),
                    $data["ParcelaFichada"],
                    $data["EmissorContrato"],
                    $data["DataCompra"],
                    join(" // ", $resultProduto),
                    $data["Financeira"],
                    $data["PagoParceiro"],
                    $data["Bordero"],
                    join(" // ", $resultLote),
                    $data["DataRepasse"],
                    $data["Cancelado"],
                    $data["ContratoFormalizado"],
                    $data["DataFormalizacao"],
                    $data["Segurada"],
                    $valorSeguro,
                    $data["Seguradora"],            
                    $data["Crediarista"],
                    $data["Analista"],
                    $data["Score"],
                    join(" // ", $resultCobranca),
                );

                array_push($list, $row);
            }

            $output = fopen("php://output", "w");

            foreach ($list as $row) {
                fputcsv($output, $row); // here you can change delimiter/enclosure
            }

            fclose($output);

            die();
        } catch (Exception $ex) {

            echo "Erros aconteceram. " . $ex->getMessage();
        }
    }
    
    public function actionRelatorioCorban() {
        yii::app()->user->setState('userSessionTimeout', 21600);
        set_time_limit(-1);

        $fileName = $_POST['nome_arquivo'] . ".csv";
        ini_set('memory_limit', '2048M');
        header("Content-type: text/csv; charset=utf-8");
        header("Content-Disposition: attachment; filename=" . $fileName);
        header("Pragma: no-cache");
        header("Expires: 0");

        $query = "SELECT UPPER(PESS.nome) AS Nome, D.numero AS CPF, P.codigo AS NumeroProposta, PAR2.seq AS Sequencial, PAR2.ref_lecca AS Referencia,
        PAR2.valor AS ValorParcela, DATE_FORMAT(PAR2.vencimento,'%d/%m/%Y') AS Vencimento, R2.label AS NossoNumero, DATE_FORMAT(RI.data_cadastro,'%d/%m/%Y') AS DataPagamento,
        PAR2.valor_atual AS ValorPago,

	CASE
            WHEN PAR2.valor_atual > 0 then PAR2.valor_atual - PAR2.valor 
            ELSE '0'
        END AS Juros,
        
        
        FIN.nome AS Financeira, 

        concat(upper(F.nome_fantasia),' - ',upper(concat(ENDE.cidade,'/',ENDE.uf))) AS LojaRecebimento,

	CASE
            WHEN S.id = 3 then 'SIM' 
            ELSE 'NAO'
        END AS ComprovanteAnexado,
        
        R.label AS NumeroBoletoRepasse,
        
        B.valor_pago AS ValorRepasse,
        
        T.id AS numeroTitulo
 
        FROM RecebimentoInterno as RI 
        LEFT JOIN Titulo AS T ON T.id = RI.Titulo_id AND T.NaturezaTitulo_id = 1
        LEFT JOIN RecebimentoInterno_has_Parcela AS RhP ON RhP.RecebimentoInterno_id = RI.id AND RhP.habilitado
        LEFT JOIN Parcela AS PAR ON PAR.Titulo_id = T.id AND PAR.habilitado
        LEFT JOIN Ranges_gerados AS R ON R.Parcela_id = PAR.id AND R.habilitado
        LEFT JOIN Filial AS F ON F.id = RI.Filial_id AND F.habilitado 
        LEFT JOIN Filial_has_Endereco AS FhE ON FhE.Filial_id = F.id 
        LEFT JOIN Endereco AS ENDE ON ENDE.id = FhE.Endereco_id 
        LEFT JOIN Parcela AS PAR2 ON PAR2.id = RhP.Parcela_id 
        LEFT JOIN Titulo AS T2 ON T2.id = PAR2.Titulo_id AND T2.NaturezaTitulo_id = 1
        LEFT JOIN Ranges_gerados AS R2 ON R2.Parcela_id = PAR2.id 
        LEFT JOIN Proposta AS P ON P.id = T2.Proposta_id AND P.habilitado
        LEFT JOIN StatusRI AS S ON S.id = RI.StatusRI_id 
        LEFT JOIN Analise_de_Credito AS AC ON AC.id = P.Analise_de_Credito_id 
        LEFT JOIN Cliente AS C ON C.id = AC.Cliente_id
        LEFT JOIN Pessoa AS PESS ON PESS.id = C.Pessoa_id
        LEFT JOIN Pessoa_has_Documento AS PhD ON PhD.Pessoa_id = PESS.id
        LEFT JOIN Documento AS D ON D.id = PhD.Documento_id AND D.Tipo_documento_id = 1 
        LEFT JOIN Financeira AS FIN ON FIN.id = RI.Financeira_id
        LEFT JOIN Baixa AS B ON B.Parcela_id = PAR.id
        WHERE RI.habilitado AND D.Tipo_documento_id = 1
        LIMIT 100000 OFFSET " . $_POST['offset'];

        try {

            $resultado = Yii::app()->db->createCommand($query)->queryAll();

            $header = array('Nome', 'CPF', 'NumeroProposta', 'Sequencial', 'Referencia', 'Valor Parcela', 'Vencimento', 'Nosso Numero', 'Data Pagamento',
                'Valor Pago', 'Juros', 'Financeira', 'Loja Recebimento', 'Comprovante anexado', 'Numero Boleto Repasse', 'ValorRepasse');

            $list = array();
            array_push($list, $header);


            foreach ($resultado as $data) { 
             
                $row = array(
                    $data["Nome"],
                    $data["CPF"],
                    $data["NumeroProposta"],
                    $data["Sequencial"],
                    $data["Referencia"],
                    $data["ValorParcela"],
                    $data["Vencimento"],
                    $data["NossoNumero"],
                    $data["DataPagamento"],
                    $data["ValorPago"],
                    $data["Juros"],
                    $data["Financeira"],
                    $data["LojaRecebimento"],
                    $data["ComprovanteAnexado"],
                    $data["NumeroBoletoRepasse"],
                    $data["ValorRepasse"],
                );

                array_push($list, $row);
            }

            $output = fopen("php://output", "w");

            foreach ($list as $row) {
                fputcsv($output, $row); // here you can change delimiter/enclosure
            }

            fclose($output);

            die();
        } catch (Exception $ex) {

            echo "Erros aconteceram. " . $ex->getMessage();
        }
    }

    public function actionGetBads() {
        echo json_encode(Empresa::model()->analisarBadFinanceira($_POST['mes'], $_POST['ano'], $_POST['draw'], $_POST['parceiro'], $_POST['analista']));
    }

    public function actionGetReceber() {

        $relatorio = new Relatorios;

        echo json_encode($relatorio->getReceberVencimento($_POST['dataDe'], $_POST['dataAte'], $_POST['draw']));
    }

    public function actionGetJurosAtraso() {

        $relatorio = new Relatorios;

        echo json_encode($relatorio->getJurosAtraso($_POST['dataDe'], $_POST['dataAte'], $_POST['draw']));
    }

    public function actionGetTotaisClientesCidades() {

        $relatorio = new Relatorios;

        echo json_encode($relatorio->getTotaisClientesCidades($_POST['draw'], $_POST['cidades']));
    }

    public function actionProducao() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskMoney.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-report-producao201608111308.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);

        $cs->registerScript('datepicker', "$('.date-picker').datepicker({autoclose: true});");
        $cs->registerScript('dinheiro', 'jQuery("input.dinheiro").maskMoney({decimal:",", thousands:"."});');

        $this->render('producaoReport');
    }

    public function actionProducaoOmni() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/reports/producaoOmni/fn-report-producao.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);

        $cs->registerScript('datepicker', "$('.date-picker').datepicker({autoclose: true});");

        $this->render('producaoOmni');
    }

    public function actionTotaisClientesRegiao() {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/reports/totaisClientes/fn-relatorio.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);

        $this->render('totaisClientesCidades');
    }

    public function actionAnalistaProgress() {
        echo json_encode(Usuario::model()->performance($_POST['analistaId'], $_POST['ano']));
    }

    public function actionAnalistaPerformance() {
        echo json_encode(Usuario::model()->score($_POST['analistaId']));
    }

    public function actionReportProducao() {

        $tipoFin = [];
        $servicos = [];

        if (isset($_POST['tipoFin']) && !empty($_POST['tipoFin'])) {
            $tipoFin = $_POST['tipoFin'];
        }

        if (isset($_POST['servicos']) && !empty($_POST['servicos'])) {
            $servicos = $_POST['servicos'];
        }

        echo json_encode(Empresa::model()->reportProducao($_POST['draw'], $_POST['start'], $_POST['dataDe'], $_POST['dataAte'], $_POST['filiais'], $_POST['valorDe'], $_POST['valorAte']/* , $_POST['ambiente'] */, $tipoFin, $servicos));
    }

    public function actionReportProducaoOmni() {
        $relatorio = new Relatorios;

        echo json_encode($relatorio->reportProducaoOmni($_POST['draw'], $_POST['start'], $_POST['dataDe'], $_POST['dataAte'], $_POST['filiais']));
    }

}
