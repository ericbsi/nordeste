<?php

class FinanceiraController extends Controller{
	
	public $layout = '//layouts/main_sigac_template';


	public function filters(){

		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	
	public function accessRules(){

		return array(
	
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin','delete','index','view'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public function actionView($id){

		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	
	public function actionCreate(){

		$model = new Financeira;
		$endereco = new Endereco;
		$contato = new Contato;
		$email = new Email;
		$telefone = new Telefone;
		$dadosBancarios = new DadosBancarios;
		$financeiraHasDadosBancarios = new FinanceiraHasDadosBancarios;
		
		$tiposTelefone = TipoTelefone::model()->findAll("habilitado ='".true."'");
		$tiposEndereco = TipoEndereco::model()->findAll("habilitado ='".true."'");

		if( isset( $_POST['Telefone'] ) && isset( $_POST['Email'] ) ){

			$model->attributes = $_POST['Financeira'];
			$dadosBancarios->attributes = $_POST['DadosBancarios'];

			$contato->data_cadastro = date('Y-m-d H:i:s');
			$contato->data_cadastro_br = date('d/m/Y H:i:s');

			$dadosBancarios->data_cadastro = date('Y-m-d H:i:s');
			$dadosBancarios->data_cadastro_br = date('d/m/Y H:i:s');

			if ( $contato->save() ){

				$telefone->attributes = $_POST['Telefone'];
				$email->attributes = $_POST['Email'];

				$telefone->data_cadastro = date('Y-m-d H:i:s');
				$email->data_cadastro = date('Y-m-d H:i:s');

				$telefone->data_cadastro_br = date('d/m/Y H:i:s');
				$email->data_cadastro_br = date('d/m/Y H:i:s');

				if ( $telefone->save() && $email->save() ){

					$contatoHasEmail = new ContatoHasEmail;
					$contatoHasTelefone = new ContatoHasTelefone;

					$contatoHasEmail->Contato_id = $contato->id;
					$contatoHasEmail->Email_id = $email->id;

					$contatoHasTelefone->Contato_id = $contato->id;
					$contatoHasTelefone->Telefone_id = $telefone->id;

					$contatoHasEmail->data_cadastro = date('Y-m-d H:i:s');
					$contatoHasTelefone->data_cadastro = date('Y-m-d H:i:s');

					$contatoHasEmail->data_cadastro_br = date('d/m/Y H:i:s');
					$contatoHasTelefone->data_cadastro_br = date('d/m/Y H:i:s');

					$contatoHasEmail->save();
					$contatoHasTelefone->save();

				}

				$model->Contato_id = $contato->id;
				$model->data_cadastro = date('Y-m-d H:i:s');
				$model->data_cadastro_br = date('d/m/Y H:i:s');

				$endereco->attributes = $_POST['Endereco'];
				$endereco->data_cadastro = date('Y-m-d H:i:s');
				$endereco->data_cadastro_br = date('d/m/Y H:i:s');

				if ( $model->save() && $endereco->save() ) {
					
					$financeiraHasEndereco = new FinanceiraHasEndereco;
					$financeiraHasEndereco->data_cadastro = date('Y-m-d H:i:s');
					$financeiraHasEndereco->data_cadastro_br = date('d/m/Y H:i:s');
					$financeiraHasEndereco->Financeira_id = $model->id;
					$financeiraHasEndereco->Endereco_id = $endereco->id;

					if ( $dadosBancarios->save() ) {

						$financeiraHasDadosBancarios->Financeira_id = $model->id;
						$financeiraHasDadosBancarios->Dados_Bancarios_id = $dadosBancarios->id;						
						$financeiraHasDadosBancarios->data_cadastro = date('Y-m-d H:i:s');
						$financeiraHasDadosBancarios->save();
					}


					if ( $financeiraHasEndereco->save() ) {
						
						$this->redirect('admin');
					}
				}

			}
		}


		$baseUrl = Yii::app()->baseUrl; 

		$cs = Yii::app()->getClientScript();

	  	$cs->registerScriptFile($baseUrl.'/js/jquery.min.js');
		$cs->registerScriptFile($baseUrl.'/js/form-wizard-financeira.js',  CClientScript::POS_END);
	  	$cs->registerScriptFile($baseUrl.'/js/jquery.validate.min.js',  CClientScript::POS_END);
	  	$cs->registerScriptFile($baseUrl.'/js/jquery.smartWizard.js',  CClientScript::POS_END);
	  	$cs->registerScriptFile($baseUrl.'/js/jquery.maskedinput.js',CClientScript::POS_END);
	  	$cs->registerScriptFile($baseUrl.'/js/jquery.maskMoney.js',CClientScript::POS_END);
	  	$cs->registerScriptFile($baseUrl.'/js/form-elements.js',CClientScript::POS_END);
	  	$cs->registerScriptFile($baseUrl.'/js/select2.min.js',CClientScript::POS_END);	  	
	  	$cs->registerScriptFile($baseUrl.'/js/jquery.inputlimiter.1.3.1.min.js',CClientScript::POS_END);
	  	$cs->registerScriptFile($baseUrl.'/js/jquery.autosize.min.js',CClientScript::POS_END);
	  	$cs->registerScriptFile($baseUrl.'/js/update-select.js',CClientScript::POS_END);  	    
  	    $cs->registerScriptFile($baseUrl.'/js/daterangepicker.js',CClientScript::POS_END);
  	    $cs->registerScriptFile($baseUrl.'/js/bootstrap-datepicker.js',CClientScript::POS_END);
  	    $cs->registerScriptFile($baseUrl.'/js/bootstrap-timepicker.min.js',CClientScript::POS_END);
  	    $cs->registerScriptFile($baseUrl.'/js/bootstrap-colorpicker.js',CClientScript::POS_END);
  	    $cs->registerScriptFile($baseUrl.'/js/jquery.tagsinput.js',CClientScript::POS_END);
  	    $cs->registerScriptFile($baseUrl.'/js/summernote.min.js',  CClientScript::POS_END);
  	    $cs->registerScriptFile($baseUrl.'/js/ckeditor/ckeditor.js',CClientScript::POS_END);
  	    $cs->registerScriptFile($baseUrl.'/js/ckeditor/adapter/jquery.js',CClientScript::POS_END);

	  	$cs->registerScript('FormWizard','FormWizard.init();');
	  	$cs->registerScript('FormElements','FormElements.init();');
	  	$cs->registerScript('CNPJMask','jQuery(function($){$("#Financeira_cnpj").mask("99.999.999/9999-99");});');

		$this->render('create',array(
			'model' => $model,
			'endereco' => $endereco,
			'telefone' => $telefone,
			'email' => $email,
			'dadosBancarios' => $dadosBancarios,
		));

	}
	
	public function actionUpdate ( $id ){
		
		$model = $this->loadModel($id);

		$financeiraHasEndereco = FinanceiraHasEndereco::model()->find("Financeira_id = ". $model->id);
		$endereco = Endereco::model()->findByPk( $financeiraHasEndereco->Endereco_id );

		$contatoHasTelefone = ContatoHasTelefone::model()->find("Contato_id = ". $model->contato->id);		
		$telefone = Telefone::model()->findByPk( $contatoHasTelefone->Telefone_id );

		$contatoHasEmail = ContatoHasEmail::model()->find("Contato_id = ". $model->contato->id);
		$email = Email::model()->findByPk( $contatoHasEmail->Email_id );
		
		if( isset( $_POST['Telefone'] ) && $model->attributes = $_POST['Endereco'] &&
			isset( $_POST['Email'] ) && $model->attributes = $_POST['Financeira']){

			$model->attributes = $_POST['Financeira'];
			$telefone->attributes = $_POST['Telefone'];
			$email->attributes = $_POST['Email'];
			$endereco->attributes = $_POST['Endereco'];
						
			$telefone->save();
			$email->save();
			$model->save();
			$endereco->save();

			$this->redirect($this->createUrl('admin'));
			
		}
	
		$this->render('update',array(
			'model'=>$model,
			'endereco'=>$endereco,
			'telefone'=>$telefone,
			'email'=>$email,
		));

	}
	
	public function actionDelete( $id ){

		$this->loadModel($id)->delete();
		
		if( !isset($_GET['ajax']) ){
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	public function actionIndex(){

		$dataProvider = new CActiveDataProvider('Financeira');

		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
	
	public function actionAdmin(){

		$model = new Financeira('search');

		$model->unsetAttributes(); 

		if(isset($_GET['Financeira'])){
			$model->attributes=$_GET['Financeira'];
		}

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	public function loadModel($id){

		$model = Financeira::model()->findByPk($id);

		if( $model === null ){
			throw new CHttpException(404,'The requested page does not exist.');
		}

		return $model;
	}
	
	protected function performAjaxValidation( $model ){

		if( isset( $_POST['ajax'] ) && $_POST['ajax'] === 'financeira-form' ){

			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
