<?php

class RecebimentoDeDocumentacaoController extends Controller {

    public $layout = '//layouts/main_sigac_template';

    public function actionIndex() {
        $this->render('index');
    }

    public function actionGetRecebimentos() {
        echo json_encode(Empresa::model()->listarRecebimentosDeContratos($_POST['draw'], $_POST['filial'], $_POST['start'], $_POST['data_de'], $_POST['data_ate']));
    }

    public function actionHistorico() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-recebimento-doc-historico.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);

        $cs->registerScript('datepicker', "$('.date-picker').datepicker({autoclose: true});");

        $this->render('historico');
    }

    public function actionRecebimentoDocumentacao() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');
        $cs->registerCssFile    ($baseUrl . '/css/bootstrap-switch.css'); 

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile ($baseUrl . '/js/bootstrap-switch.js'                           , CClientScript::POS_END    );
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-recebimento-doc-historico.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/recebimentoDocumentos/fn-recebimentoDeDocumentos.js', CClientScript::POS_END);

        $cs->registerScript('datepicker', "$('.date-picker').datepicker({autoclose: true});");

        $this->render('recebimentoDocumentacao');
    }

    public function actionMalotes() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-recebimento-doc-historico.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/recebimentoDocumentos/fn-recebimentoDeDocumentos.js', CClientScript::POS_END);

        $this->render('listarMalotes');
    }

    public function actionListarMalotes() {

        $erro = "";
        $dados = [];

        $sql = " SELECT M.id AS codigo, M.data_cadastro AS dataMalote, M.Usuario_id AS usuario "
                . " FROM Malote AS M "
                . " WHERE M.habilitado";

        try {
            $resultado = Yii::app()->db->createCommand($sql)->queryAll();

            foreach ($resultado as $r) {

                $dateTime = new DateTime($r["dataMalote"]);
                $dataMalote = $dateTime->format("d/m/Y H:i:s");
                $cod = str_pad($r["codigo"], 8, '0', STR_PAD_LEFT);

                $dados [] = [
                    "codigo" => $cod,
                    "dataProposta" => $dataMalote,
                    "usuario" => $r["usuario"],
                    "btn_printer" => '<form method="post" target="_blank" action="/printer/printerPropostasMalote/"><input name="idMalote" type="hidden" value=' . $r["codigo"] . '><button style="width: 190px; height: 30px; margin-left: 20px; font-size: 13px;" type="submit" class="fa fa-print btn-blue" id="botaoImprimir">    Imprimir capa de malote</button></form>'
                ];
            }
        } catch (Exception $ex) {
            $erro = $ex->getMessage();
        }

        echo json_encode([
            'draw' => $_POST["draw"],
            'recordsTotal' => 0,
            'recordsFiltered' => 0,
            'data' => $dados,
        ]);
    }

    public function actionListarItemBordero() {


        $bipProposta = "";
        $hasErrors = false;
        $dados = [];
        $recordsFiltered = 0;
        $recordsTotal = 0;
        $erro = "";

        $marcados = [];



        $sql = " SELECT  UPPER(Pe.nome)      AS nomeCliente, PAR.id as 'parcelaid', "
                . " P.codigo            AS codigoProposta   ,  P.data_cadastro  AS dataProposta ,"
                . " P.id                AS idProposta       ,  concat(F.nome_fantasia, ' - ', E.cidade, '/', E.uf) AS nome_filial, "
                . " (P.valor - P.valor_entrada) AS valor_financiado, DOC.numero AS cpf, "
                . " CASE WHEN TC.ModalidadeId = 1 THEN (P.valor - P.valor_entrada) ELSE ((P.valor - P.valor_entrada) - ( (P.valor - P.valor_entrada) * (Fa.porcentagem_retencao/100) )) END AS valorRepasse, "
                . " CASE WHEN BA.id IS NULL THEN '0' ELSE '1' END AS baixa"
                . " FROM         Proposta     AS P "
                . " INNER JOIN   Analise_de_Credito                       AS AC   ON   AC.habilitado  AND  AC.id =    P.Analise_de_Credito_id  "
                . " INNER JOIN   Filial                                   AS F    ON    F.habilitado  AND   F.id =   AC.Filial_id "
                . " INNER JOIN   Filial_has_Endereco                      AS FhE  ON  FhE.habilitado  AND   F.id =  FhE.Filial_id  "
                . " INNER JOIN   Endereco                                 AS E    ON    E.habilitado  AND   E.id =  FhE.Endereco_id  "
                . " INNER JOIN   NucleoFiliais                            AS NuF  ON  NuF.habilitado  AND NuF.id =    F.NucleoFiliais_id"
                . " INNER JOIN   Cliente                                  AS C    ON    C.habilitado  AND   C.id =   AC.Cliente_id"
                . " INNER JOIN   Pessoa                                   AS Pe   ON   Pe.habilitado  AND  Pe.id =    C.Pessoa_id"
                . " INNER JOIN   Pessoa_has_Documento                     AS PHD  ON   PHD.habilitado AND  Pe.id =    PHD.Pessoa_id"
                . " INNER JOIN   Documento                                AS DOC  ON   DOC.habilitado AND  DOC.id =    PHD.Documento_id AND DOC.Tipo_documento_id = 1"
                . " INNER JOIN   Tabela_Cotacao                           AS TC   ON   TC.habilitado  AND  TC.id =    P.Tabela_id"
                . " INNER JOIN   Fator                                    AS Fa   ON Fa.habilitado AND TC.id = Fa.Tabela_Cotacao_id AND Fa.carencia = P.carencia AND Fa.parcela = P.qtd_parcelas"
                . " INNER JOIN   ModalidadeTabela                         AS M    ON                       M.id =   TC.ModalidadeId"
                . " LEFT  JOIN   RecebimentoDeDocumentacao_has_Proposta   AS RDhP ON RDhP.habilitado AND RDhP.Proposta_id = P.id "
                . " LEFT  JOIN   RecebimentoDeDocumentacao                AS RD   ON RD.habilitado AND RDhP.RecebimentoDeDocumentacao_id = RD.id "
                . " INNER JOIN Titulo                                     AS T    ON    T.habilitado  AND P.id = T.Proposta_id AND T.NaturezaTitulo_id = 2 "
                . " INNER JOIN Parcela 					  AS PAR  ON  PAR.habilitado  AND T.id = PAR.Titulo_id "
                . " LEFT  JOIN Baixa 				          AS BA   ON   BA.baixado     AND BA.Parcela_id = PAR.id"
                . " WHERE P.habilitado AND P.Status_Proposta_id IN (2,7) AND P.Financeira_id = 11 AND (RDhP.id IS NULL OR RD.id IS NULL) ";


        try {

            $resultado = Yii::app()->db->createCommand($sql)->queryAll();

            $recordsTotal = count($resultado);

            if (isset($_POST["codigo_filter"]) && !empty($_POST["codigo_filter"])) {
                $sql .= " AND P.codigo = '" . $_POST["codigo_filter"] . "'";
            }

            if (isset($_POST["filial_filter"]) && !empty($_POST["filial_filter"])) {
                $sql .= " AND CONCAT(F.nome_fantasia, ' / ', E.cidade, '-', E.uf) LIKE '%" . $_POST["filial_filter"] . "%' ";
            }

            if (isset($_POST["nome_filter"]) && !empty($_POST["nome_filter"])) {
                $sql .= " AND Pe.nome LIKE '%" . $_POST["nome_filter"] . "%' ";
            }

            if (isset($_POST["cpf_filter"]) && !empty($_POST["cpf_filter"])) {
                $sql .= " AND DOC.numero LIKE '%" . $_POST["cpf_filter"] . "%' ";
            }


            if (isset($_POST["data_filter"]) && !empty($_POST["data_filter"])) {
                $sql .= " AND LEFT(P.data_cadastro,10) LIKE '" . $_POST["data_filter"] . "' ";
            }

            //$sql .= " AND NOT EXISTS (SELECT * FROM LinhaCNAB AS LC WHERE LC.Parcela_id = PAR.id AND LC.habilitado ) ";

            $resultado = Yii::app()->db->createCommand($sql)->queryAll();


            foreach ($resultado as $r) {

                if (
                        (isset($_POST["propostasMarcadas"]) && in_array($r["idProposta"], $_POST["propostasMarcadas"]))
                ) {
                    $marcados[] = $r["idProposta"];
                }
            }

            $recordsFiltered = count($resultado);

            $sql .= "ORDER BY P.id DESC                ";

            if (isset($_POST["start"])) {
                $sql .= "LIMIT " . $_POST["start"] . ", 10  ";
            }

            $resultado = Yii::app()->db->createCommand($sql)->queryAll();

            foreach ($resultado as $r) {

                $boolpago = false;

                if( LinhaCNAB::model()->find('t.habilitado AND t.Parcela_id = ' . $r['parcelaid'] ) != NULL )
                {
                    $boolpago = true;
                }

                $dateTime = new DateTime($r["dataProposta"]);
                $dataProposta = $dateTime->format("d/m/Y H:i:s");

                $proposta = Proposta::model()->findByPk($r["idProposta"]);

                $bipProposta = "";

                if (in_array($r["idProposta"], $marcados) || (isset($_POST["codigo_filter"]) && $_POST["codigo_filter"] == $proposta->codigo)) {
                    $bipProposta = $r["idProposta"];
                    $iClasseSelecionaTudo = "clip-square";
                } else {
                    $iClasseSelecionaTudo = "clip-checkbox-unchecked-2";
                }

                $check = "<button class='btn btn-blue'>"
                        . "     <i class='$iClasseSelecionaTudo checkProposta' id='iSelecionar' value='" . $r["idProposta"] . "'></i>"
                        . "</button>";

                $valorFinanciado = "R$ " . number_format($r["valor_financiado"], 2, ",", ".");
                $valorRepasse = "R$ " . number_format($r["valorRepasse"], 2, ",", ".");
                
                $pago = "";
                
                if ($boolpago) {
                    $pago = '<i style="color:green" class="fa fa-check"></i>';
                } else {
                    $pago = '<i style="color:red" class="clip-close-2"></i>';
                }

                $dados [] = [
                    "check" => $check,
                    "idProposta" => $r["idProposta"],
                    "codigoProposta" => $r["codigoProposta"],
                    "nomeFilial" => $r["nome_filial"],
                    "nomeCliente" => $r["nomeCliente"],
                    "cpf" => $r["cpf"],
                    "dataProposta" => $dataProposta,
                    "valorFinanciado" => $valorFinanciado,
                    "valorRepasse" => $valorRepasse,
                    "pago"         => $pago, 
                ];
            }
        } catch (Exception $ex) {
            $erro = $ex->getMessage();
        }

        echo json_encode(
                [
                    "data" => $dados,
                    "recordsFiltered" => $recordsFiltered,
                    "recordsTotal" => $recordsTotal,
                    "erro" => $erro,
                    "sql" => $sql,
                    "hasErrors" => $hasErrors,
                    "marcados" => $marcados,
                    "bipProposta" => $bipProposta
                ]
        );
    }

    public function actionProcessarRecebimento() {

        $retorno = [
            "title" => "Sucesso",
            "type" => "success",
            "msg" => "Recebimentos processados com sucesso",
            "hasErrors" => 0,
            "erro" => "",
        ];

        $erro = "";

        if (isset($_POST["propostasMarcadas"]) && !empty($_POST["propostasMarcadas"])) {

            $propostasMarcadas = join(",", $_POST["propostasMarcadas"]);

            $sql = " SELECT AC.Filial_id AS id_filial, P.id AS id_proposta, F.id AS id_filial, F.nome_fantasia, NF.nome AS nome_nucleo, GF.id AS nucleo"
                    . " FROM Proposta as P "
                    . " INNER JOIN Analise_de_Credito AS AC ON AC.habilitado AND AC.id = P.Analise_de_Credito_id "
                    . " INNER JOIN Filial AS F ON F.habilitado AND F.id = AC.Filial_id "
                    . " INNER JOIN NucleoFiliais AS NF ON NF.habilitado AND F.NucleoFiliais_id = NF.id "
                    . " INNER JOIN GrupoFiliais AS GF ON GF.habilitado AND GF.id = NF.GrupoFiliais_id "
                    . " LEFT  JOIN RecebimentoDeDocumentacao_has_Proposta AS RDhP ON RDhP.habilitado AND RDhP.Proposta_id = P.id "
                    . " LEFT  JOIN RecebimentoDeDocumentacao AS RD ON RD.habilitado AND RDhP.RecebimentoDeDocumentacao_id = RD.id "
                    . " WHERE P.habilitado AND P.id IN ($propostasMarcadas) AND RDhP.id IS NULL AND RD.id IS NULL "
                    . " ORDER BY AC.Filial_id , P.id";
            
            try {
                                
                $resultado = Yii::app()->db->createCommand($sql)->queryAll();

                $filialId = 0;

                $transaction = Yii::app()->db->beginTransaction();

                foreach ($resultado as $r) {

                    if ($filialId !== $r["id_filial"]) {

                        $recebimento = new RecebimentoDeDocumentacao;
                        $recebimento->codigo = date("YmdHis");
                        $recebimento->dataCriacao = date("Y-m-d H:i:s");
                        $recebimento->criadoPor = Yii::app()->session["usuario"]->id;
                        $recebimento->destinatario = $r["id_filial"];
                        $recebimento->habilitado = 1;

                        if (!$recebimento->save()) {

                            ob_start();
                            var_dump($recebimento->getErrors());
                            $erro = ob_get_clean();

                            $transaction->rollBack();

                            $retorno = [
                                "title" => "Erro",
                                "type" => "error",
                                "msg" => $erro,
                                "hasErrors" => 1,
                                "erro" => $erro,
                            ];

                            break;
                        }

                        $filialId = $r["id_filial"];
                    }

                    $rHasProposta = new RecebimentoDeDocumentacaoHasProposta;
                    $rHasProposta->Proposta_id = $r["id_proposta"];
                    $rHasProposta->RecebimentoDeDocumentacao_id = $recebimento->id;
                    $rHasProposta->data_cadastro = date("Y-m-d H:i:s");
                    $rHasProposta->habilitado = 1;

                    if (!$rHasProposta->save()) {

                        ob_start();
                        var_dump($rHasProposta->getErrors());
                        $erro = ob_get_clean();

                        $transaction->rollBack();

                        $retorno = [
                            "title" => "Erro",
                            "type" => "error",
                            "msg" => $erro,
                            "hasErrors" => 1,
                            "erro" => $erro,
                        ];
                        break;
                    }
                }

                if (!$retorno["hasErrors"]) {
                    $transaction->commit();
                }
            } catch (Exception $ex) {

                $erro = $ex->getMessage();

                $retorno = [
                    "title" => "Erro",
                    "type" => "error",
                    "msg" => $erro,
                    "hasErrors" => 1,
                    "erro" => $erro,
                ];
            }
        } else {

            $retorno = [
                "title" => "Erro",
                "type" => "error",
                "msg" => "Nenhuma proposta selecionada",
                "hasErrors" => 1,
                "erro" => "",
            ];
        }

        echo json_encode($retorno);
    }

    public function actionGetValoresSelecionadosLecca() {

        $retorno = [
            "msg" => "",
            "valorFinanciado" => 0,
            "valorRepasse" => 0
        ];

        $valorFinanciado = 0;
        $valorRepasse = 0;

        if (isset($_POST["propostasMarcadas"])) {

            foreach ($_POST["propostasMarcadas"] as $idProposta) {

                $proposta = Proposta::model()->find("habilitado AND id = $idProposta");

                if ($proposta !== null) {
                    $valorFinanciado += $proposta->valor - $proposta->valor_entrada;
                    $valorRepasse += $proposta->valorRepasse();
                }
            }

            $retorno["valorFinanciado"] = number_format($valorFinanciado, 2, ",", ".");
            $retorno["valorRepasse"] = number_format($valorRepasse, 2, ",", ".");
        } else {
            $retorno["msg"] = "POST vazio";
        }

        echo json_encode($retorno);
    }

    public function actionListarRecebimentos() {

        $dados = [];
        $retorno = [];
        $marcados = [];

        $erro = "";

        $recordsTotal = 0;
        $recordsFiltered = 0;

        $sqlCabecTotais = "SELECT COUNT(*) AS QTD ";

        $sqlCabec = "SELECT   RD.id               AS idRecebimento        , CONCAT(F.nome_fantasia, ' - ', E.cidade,'/', E.uf)    AS nomeFilial       ,   "
                . "         RD.codigo           AS codigoRecebimento    ,                                                                               "
                . "         U.nome_utilizador   AS nomeUsuario          , RD.dataCriacao                                        AS dataRecebimento, "
                . "F.id AS id_filial, F.nome_fantasia, NF.nome AS nome_nucleo, NF.id AS id_nucleo      ";


        $sql = "FROM         RecebimentoDeDocumentacao               AS RD                                                                       "
                . "INNER JOIN   Filial                                  AS F    ON    F.habilitado AND  F.id =   RD.destinatario                    "
                . "INNER JOIN NucleoFiliais AS NF ON NF.habilitado AND F.NucleoFiliais_id = NF.id                                                  " 
                . "INNER JOIN   Filial_has_Endereco                     AS FhE  ON  FhE.habilitado AND  F.id = FhE.Filial_id                        "
                . "INNER JOIN   Endereco                                AS E    ON    E.habilitado AND  E.id = FhE.Endereco_id                      "
                . "INNER JOIN   Usuario                                 AS U    ON    U.habilitado AND  U.id =   RD.criadoPor                       "
                . "LEFT  JOIN   Malote_has_RecebimentoDeDocumentacao    AS MhRD ON MhRD.habilitado AND RD.id = MhRD.RecebimentoDeDocumentacao_id    "
                . "LEFT  JOIN   Malote                                  AS M    ON    M.habilitado AND  M.id = MhRD.Malote_id                       "
                . "WHERE RD.habilitado AND MhRD.id IS NULL AND M.id IS NULL AND "
                . "EXISTS   (   SELECT * "
                . "             FROM RecebimentoDeDocumentacao_has_Proposta AS RDhP "
                . "             WHERE RDhP.habilitado AND RDhP.RecebimentoDeDocumentacao_id = RD.id "
                . "         ) "
                . "ORDER BY CONCAT(F.nome_fantasia, ' - ', E.cidade,'/', E.uf)";

        try {

            $resultado = Yii::app()->db->createCommand($sqlCabecTotais . $sql)->queryRow();

            $recordsTotal = $resultado["QTD"];

            $resultado = Yii::app()->db->createCommand($sqlCabecTotais . $sql)->queryRow();

            $recordsFiltered = $resultado["QTD"];

            $sql .= " LIMIT " . $_POST["start"] . ", " . $_POST["length"] . " ";

            $resultado = Yii::app()->db->createCommand($sqlCabec . $sql)->queryAll();

            foreach ($resultado as $r) {

                if
                (
                        (isset($_POST["recebimentosMarcados"]) && in_array($r["idRecebimento"], $_POST["recebimentosMarcados"]))
                ) {
                    $marcados[] = $r["idRecebimento"];
                }
            }

            foreach ($resultado as $r) {

                $dateTime = new DateTime($r["dataRecebimento"]);
                $dataRecebimento = $dateTime->format("d/m/Y H:i:s");

                if (in_array($r["idRecebimento"], $marcados) /* || (isset($_POST["codigo_filter"]) && $_POST["codigo_filter"] == $proposta->codigo) */) {
                    //$bipProposta = $r["idProposta"];
                    $iClasseSelecionaTudo = "clip-square";
                } else {
                    $iClasseSelecionaTudo = "clip-checkbox-unchecked-2";
                }

                $check = "<button class='btn btn-blue'>"
                        . "     <i class='$iClasseSelecionaTudo checkRecebimento' id='iSelecionar' value='" . $r["idRecebimento"] . "'></i>"
                        . "</button>";

                $dados[] = [
                    "check" => $check,
                    "idRecebimento" => $r["idRecebimento"],
                    "codigo" => $r["codigoRecebimento"],
                    "nomeFilial" => $r["nomeFilial"],
                    "nomeUsuario" => $r["nomeUsuario"],
                    "dataRecebimento" => $dataRecebimento,
                    "btnImprimir" => '<form method="post" target="_blank" action="/printer/recebimentoDeContrato/"><input name="codigo" type="hidden" value=' . $r["idRecebimento"] . '><button style="width: 190px; height: 30px; margin-left: 20px; font-size: 13px;" type="submit" class="fa fa-print btn-blue" id="botaoImprimir">    Imprimir via do parceiro</button></form>',
                ];
            }
        } catch (Exception $ex) {
            $erro = $ex->getMessage();
        }

        echo json_encode(
                [
                    "data" => $dados,
                    "erro" => $erro,
                    "recordsTotal" => $recordsTotal,
                    "recordsFiltered" => $recordsFiltered,
                    "sql" => $sql,
                ]
        );
    }

    public function actionGerarMalote() {

        $retorno = [
            "title" => "Sucesso",
            "msg" => "Malote gerado com sucesso",
            "hasErrors" => 0,
            "type" => "success",
            "erro" => "",
        ];

        if (isset($_POST["recebimentosMarcados"]) && !empty($_POST["recebimentosMarcados"])) {

            $malote = new Malote;
            $malote->habilitado = 1;
            $malote->Usuario_id = Yii::app()->session["usuario"]->id;
            $malote->data_cadastro = date("Y-m-d H:i:s");

            $transaction = Yii::app()->db->beginTransaction();

            try {
                if ($malote->save()) {

                    foreach ($_POST["recebimentosMarcados"] as $idRD) {

                        $recebimento = RecebimentoDeDocumentacao::model()->find("habilitado AND id = $idRD");

                        if ($recebimento !== null) {

                            $maloteHasRD = MaloteHasRecebimentoDeDocumentacao::model()->find("habilitado AND RecebimentoDeDocumentacao_id = $recebimento->id");

                            if ($maloteHasRD !== null) {

                                $retorno = [
                                    "title" => "Erro",
                                    "msg" => "Recebimento de código $recebimento->codigo já está no Malote $maloteHasRD->id ",
                                    "hasErrors" => 1,
                                    "type" => "error",
                                    "erro" => "Recebimento de código $recebimento->codigo já está no Malote $maloteHasRD->id ",
                                ];

                                $transaction->rollBack();

                                break;
                            } else {

                                $maloteHasRD = new MaloteHasRecebimentoDeDocumentacao;
                                $maloteHasRD->habilitado = 1;
                                $maloteHasRD->data_cadastro = date("Y-m-d H:i:s");
                                $maloteHasRD->Malote_id = $malote->id;
                                $maloteHasRD->RecebimentoDeDocumentacao_id = $recebimento->id;

                                if (!$maloteHasRD->save()) {

                                    ob_start();
                                    var_dump($maloteHasRD->getErrors());
                                    $erro = ob_get_clean();

                                    $retorno = [
                                        "title" => "Erro",
                                        "msg" => $erro,
                                        "hasErrors" => 1,
                                        "type" => "error",
                                        "erro" => $erro,
                                    ];

                                    $transaction->rollBack();

                                    break;
                                }
                            }
                        } else {

                            $retorno = [
                                "title" => "Erro",
                                "msg" => "Recebimento de id $idRD não encontrado. ",
                                "hasErrors" => 1,
                                "type" => "error",
                                "erro" => "Recebimento de id $idRD não encontrado. ",
                            ];

                            $transaction->rollBack();

                            break;
                        }
                    }

                    $transaction->commit();
                } else {

                    ob_start();
                    var_dump($malote->getErrors());
                    $erro = ob_get_clean();

                    $retorno = [
                        "title" => "Erro",
                        "msg" => $erro,
                        "hasErrors" => 1,
                        "type" => "error",
                        "erro" => $erro,
                    ];

                    $transaction->rollBack();
                }
            } catch (Exception $ex) {

                $transaction->rollBack();

                $erro = $ex->getMessage();

                $retorno = [
                    "title" => "Erro",
                    "msg" => $erro,
                    "hasErrors" => 1,
                    "type" => "error",
                    "erro" => $erro,
                ];
            }
        }

        echo json_encode($retorno);
    }

}
