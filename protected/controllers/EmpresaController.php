<?php

class EmpresaController extends Controller {

    public $layout = '//layouts/main_sigac_template';
    private $sigacLog;

    public function actionGetRecebimentos() {

        echo json_encode(Filial::model()->getRecebimentos($_POST['draw'], $_POST['filiais'], $_POST['data_de'], $_POST['data_ate'], $_POST['start']));
    }

    public function actionCancelamentosPagos() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-propostas-canceladas-pagas.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);

        $this->render('propostasCanceladasPagas');
    }

    public function actionBaixaCP() {
        $transaction = Yii::app()->db->beginTransaction();

        $retorno = array(
            "hasError" => false,
            "msg" => "Baixa realizada com sucesso"
        );

        $prop = Proposta::model()->findByPk($_POST['prop_id']);
        $titulo = null;
        if ($prop != null) {
            $titulo = Titulo::model()->find('habilitado AND NaturezaTitulo_id = 41 AND Proposta_id = ' . $prop->id);
        }

        if ($titulo != null) {
            $parcB = Parcela::model()->find('habilitado AND Titulo_id = ' . $titulo->id);
            if ($parcB != null) {
                $parcB->valor_atual = $parcB->valor;

                if ($parcB->update()) {
                    $baixa = new Baixa;
                    $baixa->valor_pago = $parcB->valor;
                    $baixa->baixado = 1;
                    $baixa->data_da_baixa = Date('Y-m-d');
                    $baixa->valor_abatimento = $parcB->valor;
                    $baixa->Parcela_id = $parcB->id;
                    $baixa->Tipo_Baixa_id = 3;
                    $baixa->Filial_id = $prop->analiseDeCredito->filial->id;

                    if (!$baixa->save()) {
                        $retorno = array(
                            "hasError" => true,
                            "msg" => "Não foi possível salvar a baixa"
                        );
                        $transaction->rollBack();
                    } else {
                        $transaction->commit();
                    }
                } else {
                    $retorno = array(
                        "hasError" => true,
                        "msg" => "Não foi possível atualizar o valor atual da parcela"
                    );
                    $transaction->rollBack();
                }
            } else {
                $retorno = array(
                    "hasError" => true,
                    "msg" => "Não foi possível encontrar a parcela correspondente a este título"
                );
                $transaction->rollBack();
            }
        } else {
            if ($prop != null) {
                $tReceber = new Titulo;

                $tReceber->prefixo = '002';
                $tReceber->emissao = Date('Y-m-d');
                $tReceber->Proposta_id = $prop->id;
                $tReceber->NaturezaTitulo_id = 41;

                if ($tReceber->save()) {
                    $nParcela = new Parcela;
                    $nParcela->seq = 1;
                    $nParcela->valor = $prop->valorRepasse();
                    $nParcela->Titulo_id = $tReceber->id;
                    $nParcela->valor_atual = $prop->valorRepasse();

                    if ($nParcela->save()) {
                        $parcB = Parcela::model()->find('habilitado AND Titulo_id = ' . $tReceber->id);

                        $baixa = new Baixa;
                        $baixa->valor_pago = $parcB->valor;
                        $baixa->data_da_baixa = Date('Y-m-d');
                        $baixa->valor_abatimento = $parcB->valor;
                        $baixa->Parcela_id = $parcB->id;
                        $baixa->Tipo_Baixa_id = 3;
                        $baixa->Filial_id = $prop->analiseDeCredito->filial->id;
                        $baixa->baixado = 1;

                        if ($baixa->save()) {
                            $transaction->commit();
                        } else {
                            $retorno = array(
                                "hasError" => true,
                                "msg" => "Não foi possível salvar a baixa correspondente para este título"
                            );
                            $transaction->rollBack();
                        }
                    } else {
                        $retorno = array(
                            "hasError" => true,
                            "msg" => "Não foi possível criar a parcela correspondente para este título"
                        );
                        $transaction->rollBack();
                    }
                } else {
                    $retorno = array(
                        "hasError" => true,
                        "msg" => "Não foi possível criar o titulo de abatimento para essa proposta"
                    );
                    $transaction->rollBack();
                }
            } else {
                $retorno = array(
                    "hasError" => true,
                    "msg" => "Não foi possível encontrar essa proposta nos nossos registros. Entre em contato com o suporte"
                );
                $transaction->rollBack();
            }
        }

        echo json_encode($retorno);
    }

    public function actionGCP() {
        $filtroGrupoFilial = $_POST['filtroGrupoFilial'];
        $draw = $_POST['draw'];

        $sql = "SELECT P.id AS 'PROPOSTAID', SC.id AS 'SOLICITACAOID' 
                    FROM Solicitacao_de_Cancelamento        AS SC
                    INNER JOIN Proposta                     AS P    ON  P.id            = SC.Proposta_id    AND P.habilitado
                    INNER JOIN Titulo                       AS T    ON  T.Proposta_id   = P.id              AND T.habilitado 
                                                                                                            AND T.NaturezaTitulo_id = 2 
                    INNER JOIN Parcela                      AS Pa   ON  Pa.Titulo_id    = T.id              AND Pa.habilitado
                    INNER JOIN Baixa                        AS B    ON  B.Parcela_id    = Pa.id             AND B.baixado
                ";

        if (((int) $filtroGrupoFilial) > 0) {
            $sql .= " INNER JOIN Analise_de_Credito AS AC ON AC.id =  P.Analise_de_Credito_id                                                       ";
            $sql .= " INNER JOIN Filial             AS F  ON  F.id = AC.Filial_id                                                                   ";
            $sql .= " INNER JOIN NucleoFiliais      AS NF ON NF.id =  F.NucleoFiliais_id      AND NF.GrupoFiliais_id  = " . $filtroGrupoFilial . "  ";
        }

        $sql .= " WHERE SC.aceite AND P.titulos_gerados AND SC.habilitado
                  ORDER BY SC.data_solicitacao DESC
                  ";

        $resultado = Yii::app()->db->createCommand($sql)->queryAll();

        $recordsTotal = count($resultado);
        $recordsFiltered = count($resultado);

        $rows = [];
        $row;
        $util = new Util;
        $totalFinanciado = 0;
        $totalRepasse = 0;

        //$sql .= "LIMIT " . $_POST['start'] . ", 10";

        $resultado = Yii::app()->db->createCommand($sql)->queryAll();

        if (count($resultado) > 0) {
            foreach ($resultado as $r) {
                $tit_canc = Titulo::model()->find('habilitado AND NaturezaTitulo_id = 41 AND Proposta_id = ' . $r['PROPOSTAID']);

                if ($tit_canc == null) {
                    $proposta = Proposta::model()->findByPk($r['PROPOSTAID']);

                    if ($proposta->segurada) {
                        $seguroColor = ' style="color:#3D9400" ';
                    } else {
                        $seguroColor = '';
                    }

                    $button = '<button id="confirm_baixa" class="btn btn-dark-beige btn-sm">Baixa</button>
                		   <input type="hidden" value="' . $proposta->id . '">';

                    $row = array(
                        'codigo' => '<span' . $seguroColor . '>' . $proposta->codigo . '</span>',
                        'data' => $util->bd_date_to_view(substr($proposta->data_cadastro, 0, 10)),
                        'cliente' => strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome) . ' - ' . $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero,
                        'repasse' => '<span style="color:#3D9400!important;">' . number_format($proposta->getValorFinanciado(), 2, ',', '.') . '</span><span style="font-weight: bold;font-size:13px;"> <i class="fa fa-arrow-circle-right"> </i> </span><span style="color:#DA5251">' . number_format($proposta->valorRepasse(), 2, ',', '.') . '</span>',
                        'parceiro' => strtoupper($proposta->analiseDeCredito->filial->getConcat()),
                        'button' => $button,
                        'contrato' => $proposta->numero_do_contrato
                    );

                    $rows[] = $row;

                    $seguroColor = '';

                    $totalFinanciado += $proposta->getValorFinanciado();
                    $totalRepasse += $proposta->valorRepasse();
                } else {
                    $parc_canc = Parcela::model()->find('habilitado AND Titulo_id = ' . $tit_canc->id);
                    $baixa_canc = Baixa::model()->find('baixado AND Parcela_id = ' . $parc_canc->id);

                    if ($baixa_canc == null) {
                        $proposta = Proposta::model()->findByPk($r['PROPOSTAID']);

                        if ($proposta->segurada) {
                            $seguroColor = ' style="color:#3D9400" ';
                        } else {
                            $seguroColor = '';
                        }

                        $button = '<button id="confirm_baixa" class="btn btn-dark-beige btn-sm">Baixa</button>
                		   <input type="hidden" value="' . $proposta->id . '">';

                        $row = array(
                            'codigo' => '<span' . $seguroColor . '>' . $proposta->codigo . '</span>',
                            'data' => $util->bd_date_to_view(substr($proposta->data_cadastro, 0, 10)),
                            'cliente' => strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome) . ' - ' . $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero,
                            'repasse' => '<span style="color:#3D9400!important;">' . number_format($proposta->getValorFinanciado(), 2, ',', '.') . '</span><span style="font-weight: bold;font-size:13px;"> <i class="fa fa-arrow-circle-right"> </i> </span><span style="color:#DA5251">' . number_format($proposta->valorRepasse(), 2, ',', '.') . '</span>',
                            'parceiro' => strtoupper($proposta->analiseDeCredito->filial->getConcat()),
                            'button' => $button,
                            'contrato' => $proposta->numero_do_contrato
                        );

                        $rows[] = $row;

                        $seguroColor = '';

                        $totalFinanciado += $proposta->getValorFinanciado();
                        $totalRepasse += $proposta->valorRepasse();
                    }
                }
            }

            echo json_encode(
                    [
                        "draw" => $draw,
                        "recordsFiltered" => $recordsFiltered,
                        "recordsTotal" => $recordsTotal,
                        "data" => $rows,
                        "customReturn" => [
                            "total" => '<span style="color:#3D9400!important;">' . number_format($totalFinanciado, 2, ',', '.') . '</span><span style="font-weight: bold;font-size:15px;"> <i class="fa fa-arrow-circle-right"> </i> </span><span style="color:#d9534f!important;">' . number_format($totalRepasse, 2, ',', '.') . '</span>',
                        ]
                    ]
            );
        }
    }

    public function actionGetCancelamentosPagos() {
        echo json_encode(Empresa::model()->cancelamentosPagos($_POST['start'], $_POST['draw'], $_POST['filtroGrupoFilial']));
    }

    public function actionRecebimentos() {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-recebimentos-empresa.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);

        $cs->registerScript('datepicker', "$('.date-picker').datepicker({autoclose: true});");

        $this->render('recebimentos');
    }

    public function actionGetRetornos() {

        echo json_encode(Empresa::model()->getRetornos($_POST['draw'], $_POST['data_de'], $_POST['data_ate'], $_POST['start']));
    }

    public function filters() {

        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules() {

        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('baixaCP', 'GCP', 'recebimentos', 'getRecebimentos', 'create', 'update', 'getRetornos', 'getCancelamentosPagos', 'cancelamentosPagos'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin', '@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionView($id) {

        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionCreate() {

        $empresa = new Empresa;
        $endereco = new Endereco;
        $contato = new Contato;
        $dadosBancarios = new DadosBancarios;
        $filial = new Filial;

        $telefone = new Telefone;
        $email = new Email;

        $empresaHasDadosBancarios = new EmpresaHasDadosBancarios;
        $contatoHasEmail = new ContatoHasEmail;
        $contatoHasTelefone = new ContatoHasTelefone;

        $empresaHasContato = new EmpresaHasContato;
        $empresaHasEndereco = new EmpresaHasEndereco;
        $filialHasEndereco = new FilialHasEndereco;

        $empresaHasAtividadeEconomicaSecundaria = new EmpresaHasAtividadeEconomicaSecundaria;
        $filialHasAtividadeEconomicaSecundaria = new FilialHasAtividadeEconomicaSecundaria;

        $util = new Util;

        if (isset($_POST['Empresa'])) {

            $empresa->attributes = $_POST['Empresa'];
            $empresa->data_cadastro = date('Y-m-d H:i:s');
            $empresa->data_cadastro_br = date('d/m/Y H:i:s');

            $empresa->data_de_abertura = $util->view_date_to_bd($empresa->data_de_abertura);

            if ($empresa->save()) {

                $filial->nome_fantasia = $empresa->nome_fantasia;
                $filial->cnpj = $empresa->cnpj;
                $filial->inscricao_estadual = $empresa->inscricao_estadual;
                $filial->Unidade_de_negocio_id = $empresa->Unidade_de_negocio_id;
                $filial->data_cadastro = date('Y-m-d H:i:s');
                $filial->data_cadastro_br = date('d/m/Y H:i:s');
                $filial->Empresa_id = $empresa->id;
                $filial->data_de_abertura = $empresa->data_de_abertura;
                $filial->isMatriz = 1;

                $undsDeNegocio = $_POST['UnidadesDeNegocio'];

                for ($i = 0; $i < sizeof($undsDeNegocio); $i++) {

                    $empresaHasAtividadeEconomicaSecundaria->Empresa_id = $empresa->id;
                    $empresaHasAtividadeEconomicaSecundaria->Unidade_de_negocio_id = $undsDeNegocio[$i];
                    $empresaHasAtividadeEconomicaSecundaria->data_cadastro = date('Y-m-d H:i:s');
                    $empresaHasAtividadeEconomicaSecundaria->save();
                }

                if ($contato->save()) {

                    $filial->Contato_id = $contato->id;

                    $email->attributes = $_POST['Email'];
                    $email->data_cadastro = date('Y-m-d H:i:s');
                    $email->data_cadastro_br = date('d/m/Y H:i:s');

                    $telefone->attributes = $_POST['Telefone'];
                    $telefone->data_cadastro = date('Y-m-d H:i:s');
                    $telefone->data_cadastro_br = date('d/m/Y H:i:s');

                    if ($email->save()) {

                        $contatoHasEmail->Email_id = $email->id;
                        $contatoHasEmail->Contato_id = $contato->id;
                        $contatoHasEmail->data_cadastro = date('Y-m-d H:i:s');
                        $contatoHasEmail->data_cadastro_br = date('d/m/Y H:i:s');
                        $contatoHasEmail->save();
                    }

                    if ($telefone->save()) {

                        $contatoHasTelefone->Telefone_id = $telefone->id;
                        $contatoHasTelefone->Contato_id = $contato->id;
                        $contatoHasTelefone->data_cadastro = date('Y-m-d H:i:s');
                        $contatoHasTelefone->data_cadastro_br = date('d/m/Y H:i:s');
                        $contatoHasTelefone->save();
                    }

                    $empresaHasContato->Empresa_id = $empresa->id;
                    $empresaHasContato->Contato_id = $contato->id;
                    $empresaHasContato->data_cadastro = date('Y-m-d H:i:s');
                    $empresaHasContato->save();
                }

                $endereco->attributes = $_POST['Endereco'];
                $endereco->data_cadastro = date('Y-m-d H:i:s');
                $endereco->data_cadastro_br = date('d/m/Y H:i:s');

                if ($endereco->save()) {

                    if ($filial->save()) {

                        $filialHasEndereco->Filial_id = $filial->id;
                        $filialHasEndereco->Endereco_id = $endereco->id;
                        $filialHasEndereco->data_cadastro = date('Y-m-d H:i:s');
                        $filialHasEndereco->data_cadastro_br = date('d/m/Y H:i:s');
                        $filialHasEndereco->save();

                        /* Atividades economicas filial */
                        for ($i = 0; $i < sizeof($undsDeNegocio); $i++) {

                            $filialHasAtividadeEconomicaSecundaria->Filial_id = $filial->id;
                            $filialHasAtividadeEconomicaSecundaria->Unidade_de_negocio_id = $undsDeNegocio[$i];
                            $filialHasAtividadeEconomicaSecundaria->data_cadastro = date('Y-m-d H:i:s');
                            $filialHasAtividadeEconomicaSecundaria->save();
                        }
                    }

                    $empresaHasEndereco->Endereco_id = $endereco->id;
                    $empresaHasEndereco->Empresa_id = $empresa->id;
                    $empresaHasEndereco->data_cadastro = date('Y-m-d H:i:s');
                    $empresaHasEndereco->save();
                }

                $dadosBancarios->attributes = $_POST['DadosBancarios'];
                $dadosBancarios->data_cadastro = date('Y-m-d H:i:s');
                $dadosBancarios->data_cadastro_br = date('d/m/Y H:i:s');

                if ($dadosBancarios->save()) {

                    $empresaHasDadosBancarios->Empresa_id = $empresa->id;
                    $empresaHasDadosBancarios->Dados_Bancarios_id = $dadosBancarios->id;
                    $empresaHasDadosBancarios->data_cadastro = date('Y-m-d H:i:s');
                    $empresaHasDadosBancarios->save();
                }

                //Sempre associar o usuário root às empresas cadastradas
                $empresaHasUsuario = new EmpresaHasUsuario;
                $empresaHasUsuario->Empresa_id = $empresa->id;
                $empresaHasUsuario->Usuario_id = 1;
                $empresaHasUsuario->data_cadastro = date('Y-m-d H:i:s');

                if ($empresaHasUsuario->save()) {

                    $dadosBancarios->attributes = $_POST['DadosBancarios'];
                    $dadosBancarios->data_cadastro = date('Y-m-d H:i:s');
                    $dadosBancarios->data_cadastro_br = date('d/m/Y H:i:s');

                    if ($dadosBancarios->save()) {

                        $empresaHasDadosBancarios->Empresa_id = $empresa->id;
                        $empresaHasDadosBancarios->Dados_Bancarios_id = $dadosBancarios->id;
                        $empresaHasDadosBancarios->data_cadastro = date('Y-m-d H:i:s');
                        $empresaHasDadosBancarios->save();
                    }
                }

                $this->sigacLog = new SigacLog;
                //$this->sigacLog->saveLog('cadastro_de_empresa','empresa', $empresa->id, date('Y-m-d H:i:s'),1, Yii::app()->session['usuario']->id, "", "127.0.0.1",null, Yii::app()->session->getSessionId() );
                $this->redirect('admin');
            }
        }

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/form-wizard-empresa.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.smartWizard.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskMoney.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/form-elements.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.inputlimiter.1.3.1.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.autosize.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/update-select.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/daterangepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-timepicker.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.tagsinput.js', CClientScript::POS_END);

        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/summernote.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ckeditor/ckeditor.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ckeditor/adapter/jquery.js', CClientScript::POS_END);

        $cs->registerScript('FormWizard', 'FormWizard.init();');
        $cs->registerScript('FormElements', 'FormElements.init();');
        $cs->registerScript('CNPJMask', 'jQuery(function($){$("#Empresa_cnpj").mask("99.999.999/9999-99");});');
        $cs->registerScript('TelefoneMask', 'jQuery(function($){$("#Telefone_numero").mask("(99)9999-9999");});');
        $cs->registerScript('DataMask', 'jQuery(function($){$("#Empresa_data_de_abertura").mask("99/99/9999");});');
        $cs->registerScript('DataMask2', 'jQuery(function($){$("#DadosBancarios_data_abertura").mask("99/99/9999");});');

        $this->render('create', array(
            'model' => $empresa,
            'dadosBancarios' => $dadosBancarios,
            'endereco' => $endereco,
            'telefone' => $telefone,
            'email' => $email,
        ));
    }

    public function actionUpdate($id) {

        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Empresa'])) {
            $model->attributes = $_POST['Empresa'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {

        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    public function actionIndex() {

        $dataProvider = new CActiveDataProvider('Empresa');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionAdmin() {

        $model = new Empresa('search');

        $model->unsetAttributes();

        if (isset($_GET['Empresa']))
            $model->attributes = $_GET['Empresa'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function loadModel($id) {

        $model = Empresa::model()->findByPk($id);

        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        return $model;
    }

    protected function performAjaxValidation($model) {

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'empresa-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
