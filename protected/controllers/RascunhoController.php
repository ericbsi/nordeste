<?php

class RascunhoController extends Controller {
    
    public $layout = '//layouts/main_sigac_template';
         
    public function actionIndex() {
        
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        
        $this->render('index');
    }
}
