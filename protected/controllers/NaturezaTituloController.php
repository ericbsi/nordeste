<?php

class NaturezaTituloController extends Controller {

    public $layout = '//layouts/main_sigac_template';

    public function filters() {
        return array(
            'accessControl',
            'postOnly + delete',
        );
    }

    public function accessRules() {

        return array(
            array('allow',
                'actions' => array(
                    'index',
                    'listar',
                    'salvar'
                ),
                'users' => array('@'),
                'expression' => 'Yii::app()->session["usuario"]->role == "financeiro" || Yii::app()->session["usuario"]->role == "empresa_admin"'
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css');


        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/natureza/fn-naturezas-grid.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);

        $this->render('index');
    }
    
    public function actionListar()
    {
        
        $retorno        = [];
        
        $criteria               = new CDbCriteria                                   ;
        $criteria->condition    = 'habilitado AND NaturezaTitulo_id IS NOT NULL'    ;
        $criteria->order        = 't.codigo'                                        ;
        
        $naturezas          = NaturezaTitulo::model()->findAll($criteria)   ;
        $recordsTotal       = count($naturezas)                              ;
        
        if(isset($_POST['filtroPai'])  && $_POST['filtroPai'] !== "0")
        {
            
            $criteria->condition .= " AND NaturezaTitulo_id = " . $_POST['filtroPai'];
            
        }
        
        if(isset($_POST['filtroCodigo']) && !empty($_POST['filtroCodigo']))
        {
            
            $criteria->condition .= " AND codigo LIKE '%" . $_POST['filtroCodigo'] . "%' ";
            
        }
        
        if(isset($_POST['filtroDescricao']) && !empty($_POST['filtroDescricao']))
        {
            
            $criteria->condition .= " AND descricao LIKE '%" . $_POST['filtroDescricao'] . "%' ";
            
        }
        
        $naturezas   = NaturezaTitulo::model()->findAll($criteria);
        
        $recordsFiltered    = count($naturezas)     ;
        
        $criteria->limit    = 10                ;
        $criteria->offset   = $_POST['start']   ;
        
        $naturezas   = NaturezaTitulo::model()->findAll($criteria);
        
        foreach ($naturezas as $natureza)
        {
            
            $naturezaPai = NaturezaTitulo::model()->find("habilitado AND id = $natureza->NaturezaTitulo_id");
            
            $btnRemover = "<button id='btnRemover' class='btn btn-red' value='$natureza->id'>"
                        . " <i class='fa fa-trash-o'></i>"
                        . "</button>";
            
            $retorno[]  =   [
                                'idNatureza'        => $natureza->id                                            ,
                                'paiNatureza'       => $naturezaPai->codigo . ' - ' .$naturezaPai->descricao    ,
                                'codigoNatureza'    => $natureza->codigo                                        ,
                                'descricaoNatureza' => $natureza->descricao                                     ,
                                'btnRemover'        => $btnRemover
                            ];
        }
        
        echo json_encode(
                            [
                                'data'              => $retorno         ,
                                'recordsTotal'      => $recordsTotal    ,
                                'recordsFiltered'   => $recordsFiltered
                            ]
                        );
        
    }
    
    public function actionSalvar()
    {
        
        $retorno =  [
                        'hasErrors'     => 0                                ,
                        'msg'           => 'Natureza salva com Sucesso.'    ,
                        'pntfyClass'    => 'success'
                    ];
        
        if(isset($_POST['paiNatureza']) && isset($_POST['codigoNatureza']) && isset($_POST['descricaoNatureza']))
        {
                        
            $natureza                       = new NaturezaTitulo            ;
            $natureza->habilitado           = 1                             ;
            $natureza->data_cadastro        = date('Y-m-d H:i:s')           ;
            $natureza->NaturezaTitulo_id    = $_POST['paiNatureza'      ]   ;
            $natureza->codigo               = $_POST['codigoNatureza'   ]   ;
            $natureza->descricao            = $_POST['descricaoNatureza']   ;
            
            if(!$natureza->save())
            {
                ob_start();
                var_dump($natureza->getErrors());
                $result = ob_get_clean();
                
                $retorno['hasErrors'    ] = 1                                       ;
                $retorno['msg'          ] = "Erro ao salvar Natureza. " . $result   ;
                $retorno['pntfyClass'   ] = "error"                                 ;
                
            }
            
        }
        else
        {
            $retorno['hasErrors'    ] = 1                                                   ;
            $retorno['msg'          ] = 'Verifique se todos os campos estão preenchidos'    ;
            $retorno['pntfyClass'   ] = 'error'                                             ;
        }
        
        echo json_encode($retorno);
        
    }

}
