<?php

class GrupoFiliaisController extends Controller {

    public $layout = '//layouts/main_sigac_template';

    public function filters() {

        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules() {

        return array(
            array(
                'allow',
                'actions' => array(
                    'index',
                    'listar',
                    'listarSelect',
                    'listarNucleos',
                    'salvar',
                    'salvarNucleo',
                    'mudarNome',
                    'mudarGrupoNucleo',
                    'mudarNomeNucleo',
                    'mudarAgenciaNucleo',
                    'mudarContaNucleo',
                    'mudarOperacaoNucleo',
                    'mudarBancoNucleo',
                    'nucleosFiliais',
                    'listarSelectNucleo',
                    'listarSelectBanco',
                    'getNucleoFilial',
                    'getGrupoFilial',
                    'getFilial',
                    'mudarCGCNucleoDB'
                ),
                'users' => array('@'),
            ),
            array(
                'deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {

        $this->layout = '//layouts/main_sigac_template';

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css');
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpalette.css');
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/grupoFiliais/fn-grid.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpalette.js', CClientScript::POS_END);

        $this->render('index');
    }

    public function actionNucleosFiliais() {

        $this->layout = '//layouts/main_sigac_template';

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css');
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpalette.css');
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/nucleoFiliais/fn-grid.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpalette.js', CClientScript::POS_END);

        $this->render('nucleosFiliais');
    }

    public function actionListar() {

        echo json_encode(
                GrupoFiliais::model()->listar()
        );
    }

    public function actionListarSelect() {

        echo json_encode(
                GrupoFiliais::model()->listarSelect($_POST['idNucleo'])
        );
    }

    public function actionListarSelectBanco() {

        echo json_encode(
                Banco::model()->listarSelect($_POST['idNucleo'])
        );
    }

    public function actionListarSelectNucleo() {

        echo json_encode(
                GrupoFiliais::model()->listarSelectNucleo($_POST['idNucleo'])
        );
    }

    public function actionListarNucleos() {

        echo json_encode(
                GrupoFiliais::model()->listarNucleos()
        );
    }

    public function actionSalvar() {

        if (!empty($_POST['nomeGrupo'])) {

            $retorno = GrupoFiliais::model()->salvar($_POST['nomeGrupo']);

            echo json_encode($retorno);
        }
    }

    public function actionSalvarNucleo() {

        if (!empty($_POST['grupoFilial']) && !empty($_POST['nomeNucleo'])) {

            $retorno = GrupoFiliais::model()->salvarNucleo($_POST['grupoFilial'], $_POST['nomeNucleo'], $_POST['bancoNucleo'], $_POST['agenciaNucleo'], $_POST['contaNucleo'], $_POST['operacaoNucleo']);

            echo json_encode($retorno);
        }
    }

    ///////////////////////////////////////////////
    // funcao para alterar o nome. ////////////////
    ///////////////////////////////////////////////
    // Autor : André Willams // Data : 06-07-2015//
    ///////////////////////////////////////////////
    public function actionMudarNome() {

        $arrayRetorno = array(
            'hasErrors' => 0,
            'msg' => 'Nome alterado com Sucesso.',
            'pntfyClass' => 'success'
        );

        $id = $_POST['idGrupo']; //resgate o id enviado pelo post
        $nome = $_POST['nomeGrupo']; //resgate o nome enviado pelo post

        $grupo = GrupoFiliais::model()->find("habilitado and id = '" . $id . "'  "); //busque o registro no banco pelo id

        $grupo->nome_fantasia = $nome;   //altere o nome

        if (empty($grupo->nome_fantasia)) {
            $arrayRetorno['hasErrors'] = 1;
            $arrayRetorno['msg'] = 'Digite um nome válido para o Grupo de Filiais.';
            $arrayRetorno['pntfyClass'] = 'error';
        } elseif (!$grupo->update()) {

            ob_start();
            var_dump($grupo->getErrors());
            $result = ob_get_clean();

            $arrayRetorno['hasErrors'] = 1;
            $arrayRetorno['msg'] = 'Ocorreu um erro. Tente novamente.' . $result;
            $arrayRetorno['pntfyClass'] = 'error';
        } else {
            //salve os logs
            $sigacLog = new SigacLog;
            $sigacLog->saveLog('Alteração de Grupo de Filiais', 'Grupo de Filiais', $grupo->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Usuário " . Yii::app()->session['usuario']->username . " alterou o grupo.", "127.0.0.1", null, Yii::app()->session->getSessionId());
        }

        echo json_encode($arrayRetorno);
    }

    ///////////////////////////////////////////////
    // funcao para alterar o grupo do nucleo. /////
    ///////////////////////////////////////////////
    // Autor : André Willams // Data : 07-07-2015//
    ///////////////////////////////////////////////
    public function actionMudarGrupoNucleo() {

        $arrayRetorno = array(
            'hasErrors' => 0,
            'msg' => 'Grupo alterado com Sucesso.',
            'pntfyClass' => 'success'
        );

        $idGrupo = $_POST['idGrupo']; //resgate o id do grupo enviado pelo post
        $idNucleo = $_POST['idNucleo']; //resgate o id do nucleo pelo post

        $nucleo = NucleoFiliais::model()->find('habilitado AND id = ' . $idNucleo);

        $nucleo->GrupoFiliais_id = $idGrupo;

        if (!$nucleo->update()) {

            ob_start();
            var_dump($nucleo->getErrors());
            $result = ob_get_clean();

            $arrayRetorno['hasErrors'] = 1;
            $arrayRetorno['msg'] = 'Ocorreu um erro. Tente novamente.' . $result;
            $arrayRetorno['pntfyClass'] = 'error';
        } else {
            //salve os logs
            $sigacLog = new SigacLog;
            $sigacLog->saveLog('Alteração de Grupo no Núcleo de Filiais', 'Núcleo de Filiais', $nucleo->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Usuário " . Yii::app()->session['usuario']->username . " alterou o grupo.", "127.0.0.1", null, Yii::app()->session->getSessionId());
        }

        echo json_encode($arrayRetorno);
    }

    ///////////////////////////////////////////////
    // funcao para alterar o grupo do nucleo. /////
    ///////////////////////////////////////////////
    // Autor : André Willams // Data : 07-07-2015//
    ///////////////////////////////////////////////
    public function actionMudarBancoNucleo() {

        $arrayRetorno = array(
            'hasErrors' => 0,
            'msg' => 'Banco alterado com Sucesso.',
            'pntfyClass' => 'success'
        );

        $idBanco = $_POST['idBanco']; //resgate o id do banco enviado pelo post
        $idNucleo = $_POST['idNucleo']; //resgate o id do nucleo pelo post

        $nucleoHasDadosBancarios = NucleoFiliaisHasDadosBancarios::model()->find("NucleoFiliais_id = '" . $idNucleo . "'  "); //busque o registro no banco pelo id

        if ($nucleoHasDadosBancarios == null) {

            $dadosBancarios = new DadosBancarios;
            $dadosBancarios->data_cadastro = date('Y-m-d H:i:s');
            $dadosBancarios->habilitado = 1;
            $dadosBancarios->save();

            $nucleoHasDadosBancarios = new NucleoFiliaisHasDadosBancarios;
            $nucleoHasDadosBancarios->NucleoFiliais_id = $idBanco;
            $nucleoHasDadosBancarios->Dados_Bancarios_id = $dadosBancarios->id;
            $nucleoHasDadosBancarios->save();
        } else {
            $dadosBancarios = DadosBancarios::model()->find('habilitado AND id = ' . $nucleoHasDadosBancarios->Dados_Bancarios_id);
        }

        $banco = Banco::model()->findByPk($idBanco);

        $dadosBancarios->Banco_id = $idBanco;

        if ($banco == null) {
            $arrayRetorno['hasErrors'] = 1;
            $arrayRetorno['msg'] = 'Selecione um banco válido para o Núcleo de Filiais.';
            $arrayRetorno['pntfyClass'] = 'error';
        } elseif (!$dadosBancarios->update()) {

            ob_start();
            var_dump($dadosBancarios->getErrors());
            $result = ob_get_clean();

            $arrayRetorno['hasErrors'] = 1;
            $arrayRetorno['msg'] = 'Ocorreu um erro. Tente novamente.' . $result;
            $arrayRetorno['pntfyClass'] = 'error';
        } else {
            //salve os logs
            $sigacLog = new SigacLog;
            $sigacLog->saveLog('Alteração de Núcleo de Filiais', 'Dados Bancários', $dadosBancarios->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Usuário " . Yii::app()->session['usuario']->username . " alterou o núcleo.", "127.0.0.1", null, Yii::app()->session->getSessionId());
        }

        echo json_encode($arrayRetorno);
    }

    ///////////////////////////////////////////////
    // funcao para alterar o nome. ////////////////
    ///////////////////////////////////////////////
    // Autor : André Willams // Data : 06-07-2015//
    ///////////////////////////////////////////////
    public function actionMudarNomeNucleo() {

        $arrayRetorno = array(
            'hasErrors' => 0,
            'msg' => 'Nome alterado com Sucesso.',
            'pntfyClass' => 'success'
        );

        $id = $_POST['idNucleo']; //resgate o id enviado pelo post
        $nome = $_POST['nomeNucleo']; //resgate o nome enviado pelo post

        $nucleo = NucleoFiliais::model()->find("habilitado and id = '" . $id . "'  "); //busque o registro no banco pelo id

        $nucleo->nome = $nome;   //altere o nome

        if (empty($nucleo->nome)) {
            $arrayRetorno['hasErrors'] = 1;
            $arrayRetorno['msg'] = 'Digite um nome válido para o Núcleo de Filiais.';
            $arrayRetorno['pntfyClass'] = 'error';
        } elseif (!$nucleo->update()) {

            ob_start();
            var_dump($nucleo->getErrors());
            $result = ob_get_clean();

            $arrayRetorno['hasErrors'] = 1;
            $arrayRetorno['msg'] = 'Ocorreu um erro. Tente novamente.' . $result;
            $arrayRetorno['pntfyClass'] = 'error';
        } else {
            //salve os logs
            $sigacLog = new SigacLog;
            $sigacLog->saveLog('Alteração de Núcleo de Filiais', 'Núcleo de Filiais', $nucleo->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Usuário " . Yii::app()->session['usuario']->username . " alterou o núcleo.", "127.0.0.1", null, Yii::app()->session->getSessionId());
        }

        echo json_encode($arrayRetorno);
    }

    ///////////////////////////////////////////////
    // funcao para alterar o nome. ////////////////
    ///////////////////////////////////////////////
    // Autor : André Willams // Data : 06-07-2015//
    ///////////////////////////////////////////////
    public function actionMudarAgenciaNucleo() {

        $arrayRetorno = array(
            'hasErrors' => 0,
            'msg' => 'Agência alterado com Sucesso.',
            'pntfyClass' => 'success'
        );

        $id = $_POST['idNucleo']; //resgate o id enviado pelo post
        $agencia = $_POST['agenciaNucleo']; //resgate o aagencia enviado pelo post

        $nucleoHasDadosBancarios = NucleoFiliaisHasDadosBancarios::model()->find("NucleoFiliais_id = '" . $id . "'  "); //busque o registro no banco pelo id

        if ($nucleoHasDadosBancarios == null) {

            $dadosBancarios = new DadosBancarios;
            $dadosBancarios->data_cadastro = date('Y-m-d H:i:s');
            $dadosBancarios->habilitado = 1;
            $dadosBancarios->save();

            $nucleoHasDadosBancarios = new NucleoFiliaisHasDadosBancarios;
            $nucleoHasDadosBancarios->NucleoFiliais_id = $id;
            $nucleoHasDadosBancarios->Dados_Bancarios_id = $dadosBancarios->id;
            $nucleoHasDadosBancarios->save();
        } else {
            $dadosBancarios = DadosBancarios::model()->find('habilitado AND id = ' . $nucleoHasDadosBancarios->Dados_Bancarios_id);
        }

        $dadosBancarios->agencia = $agencia;

        if (empty($dadosBancarios->agencia)) {
            $arrayRetorno['hasErrors'] = 1;
            $arrayRetorno['msg'] = 'Digite um nome válido para o Núcleo de Filiais.';
            $arrayRetorno['pntfyClass'] = 'error';
        } elseif (!$dadosBancarios->update()) {

            ob_start();
            var_dump($dadosBancarios->getErrors());
            $result = ob_get_clean();

            $arrayRetorno['hasErrors'] = 1;
            $arrayRetorno['msg'] = 'Ocorreu um erro. Tente novamente.' . $result;
            $arrayRetorno['pntfyClass'] = 'error';
        } else {
            //salve os logs
            $sigacLog = new SigacLog;
            $sigacLog->saveLog('Alteração de Núcleo de Filiais', 'Dados Bancários', $dadosBancarios->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Usuário " . Yii::app()->session['usuario']->username . " alterou o núcleo.", "127.0.0.1", null, Yii::app()->session->getSessionId());
        }

        echo json_encode($arrayRetorno);
    }

    ///////////////////////////////////////////////
    // funcao para alterar o nome. ////////////////
    ///////////////////////////////////////////////
    // Autor : André Willams // Data : 06-07-2015//
    ///////////////////////////////////////////////
    public function actionMudarContaNucleo() {

        $arrayRetorno = array(
            'hasErrors' => 0,
            'msg' => 'Nº da conta alterado com Sucesso.',
            'pntfyClass' => 'success'
        );

        $id = $_POST['idNucleo']; //resgate o id enviado pelo post
        $conta = $_POST['contaNucleo']; //resgate o aagencia enviado pelo post

        $nucleoHasDadosBancarios = NucleoFiliaisHasDadosBancarios::model()->find("NucleoFiliais_id = '" . $id . "'  "); //busque o registro no banco pelo id

        if ($nucleoHasDadosBancarios == null) {

            $dadosBancarios = new DadosBancarios;
            $dadosBancarios->data_cadastro = date('Y-m-d H:i:s');
            $dadosBancarios->habilitado = 1;
            $dadosBancarios->save();

            $nucleoHasDadosBancarios = new NucleoFiliaisHasDadosBancarios;
            $nucleoHasDadosBancarios->NucleoFiliais_id = $id;
            $nucleoHasDadosBancarios->Dados_Bancarios_id = $dadosBancarios->id;
            $nucleoHasDadosBancarios->save();
        } else {
            $dadosBancarios = DadosBancarios::model()->find('habilitado AND id = ' . $nucleoHasDadosBancarios->Dados_Bancarios_id);
        }

        $dadosBancarios->numero = $conta;

        if (empty($dadosBancarios->numero)) {
            $arrayRetorno['hasErrors'] = 1;
            $arrayRetorno['msg'] = 'Digite um nº da conta válido para o Núcleo de Filiais.';
            $arrayRetorno['pntfyClass'] = 'error';
        } elseif (!$dadosBancarios->update()) {

            ob_start();
            var_dump($dadosBancarios->getErrors());
            $result = ob_get_clean();

            $arrayRetorno['hasErrors'] = 1;
            $arrayRetorno['msg'] = 'Ocorreu um erro. Tente novamente.' . $result;
            $arrayRetorno['pntfyClass'] = 'error';
        } else {
            //salve os logs
            $sigacLog = new SigacLog;
            $sigacLog->saveLog('Alteração de Núcleo de Filiais', 'Dados Bancários', $dadosBancarios->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Usuário " . Yii::app()->session['usuario']->username . " alterou o núcleo.", "127.0.0.1", null, Yii::app()->session->getSessionId());
        }

        echo json_encode($arrayRetorno);
    }

    ///////////////////////////////////////////////
    // funcao para alterar o nome. ////////////////
    ///////////////////////////////////////////////
    // Autor : André Willams // Data : 06-07-2015//
    ///////////////////////////////////////////////
    public function actionMudarOperacaoNucleo() {

        $arrayRetorno = array(
            'hasErrors' => 0,
            'msg' => 'Operação alterado com Sucesso.',
            'pntfyClass' => 'success'
        );

        $id = $_POST['idNucleo']; //resgate o id enviado pelo post
        $operacao = $_POST['operacaoNucleo']; //resgate o aagencia enviado pelo post

        $nucleoHasDadosBancarios = NucleoFiliaisHasDadosBancarios::model()->find("NucleoFiliais_id = '" . $id . "'  "); //busque o registro no banco pelo id

        if ($nucleoHasDadosBancarios == null) {

            $dadosBancarios = new DadosBancarios;
            $dadosBancarios->data_cadastro = date('Y-m-d H:i:s');
            $dadosBancarios->habilitado = 1;
            $dadosBancarios->save();

            $nucleoHasDadosBancarios = new NucleoFiliaisHasDadosBancarios;
            $nucleoHasDadosBancarios->NucleoFiliais_id = $id;
            $nucleoHasDadosBancarios->Dados_Bancarios_id = $dadosBancarios->id;
            $nucleoHasDadosBancarios->save();
        } else {
            $dadosBancarios = DadosBancarios::model()->find('habilitado AND id = ' . $nucleoHasDadosBancarios->Dados_Bancarios_id);
        }

        $dadosBancarios->operacao = $operacao;

        if (!$dadosBancarios->update()) {

            ob_start();
            var_dump($dadosBancarios->getErrors());
            $result = ob_get_clean();

            $arrayRetorno['hasErrors'] = 1;
            $arrayRetorno['msg'] = 'Ocorreu um erro. Tente novamente.' . $result;
            $arrayRetorno['pntfyClass'] = 'error';
        } else {
            //salve os logs
            $sigacLog = new SigacLog;
            $sigacLog->saveLog('Alteração de Núcleo de Filiais', 'Dados Bancários', $dadosBancarios->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Usuário " . Yii::app()->session['usuario']->username . " alterou o núcleo.", "127.0.0.1", null, Yii::app()->session->getSessionId());
        }

        echo json_encode($arrayRetorno);
    }

    ///////////////////////////////////////////////
    // funcao para alterar o cgc. /////////////////
    ///////////////////////////////////////////////
    // Autor : André Willams // Data : 25-11-2015//
    ///////////////////////////////////////////////
    public function actionMudarCGCNucleoDB() {

        $arrayRetorno = array(
            'hasErrors' => 0,
            'msg' => 'CGC dos dados bancários alterado com Sucesso.',
            'pntfyClass' => 'success'
        );

        $id = $_POST['idNucleo']; //resgate o id enviado pelo post
        $cgc = $_POST['cgcNucleoDB']; //resgate o cgc enviado pelo post

        $nucleoHasDadosBancarios = NucleoFiliaisHasDadosBancarios::model()->find("NucleoFiliais_id = '" . $id . "'  "); //busque o registro no banco pelo id

        if ($nucleoHasDadosBancarios == null) {

            $dadosBancarios = new DadosBancarios;
            $dadosBancarios->data_cadastro = date('Y-m-d H:i:s');
            $dadosBancarios->habilitado = 1;
            $dadosBancarios->save();

            $nucleoHasDadosBancarios = new NucleoFiliaisHasDadosBancarios;
            $nucleoHasDadosBancarios->NucleoFiliais_id = $id;
            $nucleoHasDadosBancarios->Dados_Bancarios_id = $dadosBancarios->id;
            $nucleoHasDadosBancarios->save();
        } else {
            $dadosBancarios = DadosBancarios::model()->find('habilitado AND id = ' . $nucleoHasDadosBancarios->Dados_Bancarios_id);
        }

        $dadosBancarios->cgc = $cgc;

        if (!$dadosBancarios->update()) {

            ob_start();
            var_dump($dadosBancarios->getErrors());
            $result = ob_get_clean();

            $arrayRetorno['hasErrors'] = 1;
            $arrayRetorno['msg'] = 'Ocorreu um erro. Tente novamente.' . $result;
            $arrayRetorno['pntfyClass'] = 'error';
        } else {
            //salve os logs
            $sigacLog = new SigacLog;
            $sigacLog->saveLog('Alteração de Núcleo de Filiais', 'Dados Bancários', $dadosBancarios->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Usuário " . Yii::app()->session['usuario']->username . " alterou o núcleo.", "127.0.0.1", null, Yii::app()->session->getSessionId());
        }

        echo json_encode($arrayRetorno);
    }
    
    //ação para atualizar os campos de seleção (Grupos e Filiais), utilizada nos filtro da tela principal do modulo de cobranca
    public function actionGetNucleoFilial() {

        $dataNucleos = [];
        $dataFiliais = [];

        $retorno =  [
                        "hasErrors"     => false    ,
                        "dataNucleos"   => []       ,
                        "dataFiliais"   => []       ,
                        "debug"         => ""
                    ];
        
        //verificando se algum grupo foi selecionado, e validando o POST
        if (isset($_POST["idGrupos"]) && gettype($_POST["idGrupos"]) == "array" && count($_POST["idGrupos"]) > 0) 
        {
                       
            $idGrupos = $_POST["idGrupos"];
            
            //para cada id de grupo...
            foreach ($idGrupos AS $id)
            {
                //selecionando o grupo correspondente..
                $grupo = GrupoFiliais::model()->find("habilitado AND id = $id");

                if ($grupo !== null) 
                {
                    //selecionando o núcleo corresponde ao grupo corrente..
                    $dadosNucleos = NucleoFiliais::model()->listarParaCobranca($grupo->id);
                    
                    //para cada núcleo...
                    foreach ($dadosNucleos as $dn) 
                    {   
                        //selecionando as filiais do núcleo corrente..
                        $filiais        = Filial::model()->findAll("habilitado AND NucleoFiliais_id = " . $dn["id"]);

                        foreach ($filiais as $f) 
                        {
                            //colocando os id's das filiais na variável de retorno
                            $dataFiliais[] = $f->id; 
                        }
                        
                        $dataNucleos[] = $dn["id"];
                    }
                }
            }

            $retorno["dataNucleos"] = $dataNucleos;
            $retorno["dataFiliais"] = $dataFiliais;
            $retorno['debug'] = $idGrupos;
        }
        else
        {
            
            ob_start();
            var_dump($_POST);
            $retornoErro = ob_get_clean();
            
            $retorno["debug"] = "POST: $retornoErro";
        }

        echo json_encode($retorno);
    }
    
    //ação para atualizar os campos de seleção (Núcleo e Filial), utilizada nos filtros da tela do módulo de cobranca
    public function actionGetGrupoFilial() 
    {

        $nucleosID      = "";

        $dataGrupos     = [];
        $dataFiliais    = [];

        $retorno =  [
                        "hasErrors"     => false    ,
                        "dataGrupos"    => []       ,
                        "dataFiliais"   => []
                    ];
        //verificando se algum nucleo foi selecionado e validando o POST
        if (isset($_POST["idNucleos"]) && gettype($_POST["idNucleos"]) == "array" && count($_POST["idNucleos"]) > 0) {

            $idNucleos = $_POST["idNucleos"];
            
            //colocando o POST em um formato que se adeque a query utilizada na função de listagem
            $nucleosID = join(",", $idNucleos);
            
            //listando os grupos correspondentes aos núcleos selecionados
            $gruposID = GrupoFiliais::model()->listarParaCobranca($nucleosID);
            
            //para cada grupo...
            foreach ($gruposID AS $gID) {
                //colocando os id's dos grupos na variável de retorno
                $dataGrupos[] = $gID["id"];
            }
            
            //para cada nucleo...
            foreach ($idNucleos AS $id) {
                //listando as filiais correspondentes ao núcleo corrente
                $filiais = Filial::model()->findAll("habilitado AND NucleoFiliais_id = $id");
                
                //para cada filial...
                foreach ($filiais as $f) {
                    //colocando os id's das filiais na variável de retorno
                    $dataFiliais[] = $f->id;
                }
            }

            $retorno["dataGrupos"] = $dataGrupos;
            $retorno["dataFiliais"] = $dataFiliais;
        }

        echo json_encode($retorno);
    }
    
    //ação utilizada atualizar os campos de seleção (Nucleos e Grupos), utilizada nos filtros da tela principal do módulo de cobrança..
    public function actionGetFilial() 
    {

        $nucleosID      = "";

        $dataGrupos     = [];
        $dataNucleos    = [];

        $retorno =  [
                        "hasErrors"     => false    ,
                        "dataGrupos"    => []       ,
                        "dataNucleos"   => []       ,
                        "debug"         => ""
                    ];
        
        //verificando se alguma filial foi selecionada e validando o POST
        if (isset($_POST["idFiliais"]) && gettype($_POST["idFiliais"]) == "array" && count($_POST["idFiliais"]) > 0) 
        {
            
            $idFiliais  = $_POST["idFiliais"]                                       ;
            
            //colocando os id's em uma formatação que possa ser utilizada na query de listagem
            $filiaisID  = join(",", $idFiliais)                                     ;
            
            //listando os grupos correspondentes as filiais selecionadas
            $gruposID   = GrupoFiliais::model()->listarParaCobranca(null, $filiaisID) ;
            
            //para cada grupo...
            foreach ($gruposID AS $gID) 
            {
                //colocando os id's dos grupos na variável de retorno
                $dataGrupos[] = $gID["id"];
            }
            
            //listando os núcleos correspondetes a cada filial selecionada
            $nucleosID = NucleoFiliais::model()->listarParaCobranca(null, $filiaisID);
            
            //para cada núcleo...
            foreach ($nucleosID AS $nID) 
            {
                //colocando os id's dos núcleos na variável de retorno..
                $dataNucleos[] = $nID["id"];
            }

            $retorno["dataGrupos"   ] = $dataGrupos     ;
            $retorno["dataNucleos"  ] = $dataNucleos    ;
        }
        else
        {
            
            ob_start();
            var_dump($_POST);
            $retornoErro = ob_get_clean();
            
            $retorno["debug"] = "POST: $retornoErro";
        }

        echo json_encode($retorno);
    }

}
