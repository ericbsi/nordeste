<?php

class ConjugeController extends Controller{

	public function actionIndex(){
		$this->render('index');
	}

	public function actionPersist(){

		echo json_encode(Familiar::model()->persist( $_POST['CPFConjuge'], $_POST['Conjuge_id'], $_POST['Conjuge'], $_POST['DadosProfissionaisConjuge'], $_POST['EnderecoProfissConjuge'], $_POST['RGConjuge'], $_POST['TelefoneProfissConjuge'], $_POST['clienteId'], $_POST['CadastroConjuge'], $_POST['ControllerAction'] ));
	}

	public function actionCheckStatus()
	{
		echo json_encode(Familiar::model()->checkConjugeStatus($_POST['cpfConj'], $_POST['clienteId']));
	}
}