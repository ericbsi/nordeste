<?php

class AnaliseDeCreditoController extends Controller{

    public $layout = '//layouts/main_sigac_template';

    /*CPF Receita*/
    private $cookieFile;
    private $token;
    private $imgCaptcha;

    public function filters(){

        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }


    public function accessRules()
    {
        return array(
            array('allow',
                'actions'       => array(
                                            'verPropostas',
                                            'maisDetalhes',
                                            'historicoCrediarista',
                                            'analiseCliente',
                                            'updateCaptcha',
                                            'getCpfReceita',
                                            'getCaptchaReceita',
                                            'getReceitaToken',
                                            'buscarCEP',
                                            'getCaptchaReceita',
                                            'getImgReceitaId',
                                            'lasts',
                                            'uploadTeste',
                                            'doAnalise',
                                            'analiseResume',
                                            'analiseResumeCliente',
                                            'analiseCheckout',
                                            'validaCpfReceita',
                                            'renderReceitaForm',
                                            'step1',
                                            'create',
                                            'update',
                                            'index',
                                            'view',
                                            'admin',
                                            'delete',
                                            'programada',
                                            'doAnaliseCliente'
                                        ),
                'users'         => array('@'),
            ),
            array('allow',
                'actions'       => array('iniciarAnalise'),
                'users'         => array('@'),
                'expression'    =>  'Yii::app()->session["usuario"]->role == "crediarista" &&   '
                                .   'Yii::app()->session["usuario"]->temCDC()                   '
            ),
            array('allow',
                'actions'       => array('iniciarEmprestimoSemear','iniciarEmprestimo'),
                'users'         => array('@'),
                'expression'    =>  '(Yii::app()->session["usuario"]->role == "crediarista" ||      '
                                .   ' Yii::app()->session["usuario"]->role == "emprestimo"     ) && '
                                .   ' Yii::app()->session["usuario"]->temEmprestimo()               '
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }

    public function actionIniciarAnalise()
    {
        $baseUrl        = Yii::app()->baseUrl;
        $cs             = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');
        $cs->registerCssFile($baseUrl.'/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl.'/css/sigax_template/dataTables.tableTools.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js',                CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js',               CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js',        CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ui-elements.js',                   CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.autosize.min.js',           CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.js',                       CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskMoney.js',              CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js',            CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.inputlimiter.1.3.1.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js',                   CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js',            CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js',            CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js',             CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js',      CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js',       CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/fn-search-cep.js',                   CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/blockInterface.js',                  CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/fn-sigac-camera.js',                 CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js',                CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js',          CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/fn-iniciar-analisev4.js?v=1.8',      CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js',            CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-filestyle.min.js',       CClientScript::POS_END);

        $cs->registerScript('moneypicker','$(".grana").maskMoney({decimal:",", thousands:"."});');
        $cs->registerScript('select2','');
        $cs->registerScript('ipt_date','jQuery(function($){$("input.dateBR").mask("99/99/9999");});');

        $cs->registerScript('ipt_tempo_residencia','jQuery(function($){$("#tempo_de_residencia").mask("99/9999");});');
        $cs->registerScript('ipt_data','jQuery(function($){$("input.dateBR").mask("99/99/9999");});');
        $cs->registerScript('ipt_mes_ano','jQuery(function($){$("input.mes_ano").mask("99/9999");});');
        //$cs->registerScript('ipt_fone','jQuery(function($){$("input.telefone").mask("(99)9999-9999");});');
        $cs->registerScript('ipt_date','jQuery(function($){$("input.cpfmask").mask("99999999999");});');
        $cs->registerScript('ipt_date','jQuery(function($){$("input.cepmask").mask("99999999");});');
        $cs->registerScript('datepicker',"$('.date-picker').datepicker({autoclose: true});");
        $cs->registerScript('moneypicker2','$(".currency").maskMoney({decimal:",", thousands:".", allowZero: true});');
        $cs->registerScript('moneypicker','$(".currency2").maskMoney({decimal:",", thousands:".", allowZero: false});');
        $cs->registerScript('MesAnoRenda','jQuery(function($){$("input.mes_ano_renda").mask("99/9999");});');

        $documento        = new Documento;

        $this->render(          'iniciar_analise_com_seguro_novo',array(
                                'documento' => $documento,
                                'imgName' => $this->actionGetCaptchaReceita( $this->token[0] ),
                                'token'=> $this->token,
                                'emprestimo'=> 0,
                                'semear' => 0,
                                'cdc'           => 1,
                                'pgd'           => 0
        ));
    }

    public function actionProgramada()
    {
        $baseUrl        = Yii::app()->baseUrl;
        $cs             = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');
        $cs->registerCssFile($baseUrl.'/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl.'/css/sigax_template/dataTables.tableTools.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js',                CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js',               CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js',        CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ui-elements.js',                   CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.autosize.min.js',           CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.js',                       CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskMoney.js',              CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js',            CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.inputlimiter.1.3.1.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js',                   CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js',            CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js',            CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js',             CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js',      CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js',       CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/fn-search-cep.js',                   CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/blockInterface.js',                  CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/fn-sigac-camera.js',                 CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js',                CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js',          CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/fn-iniciar-analisev4.js?v=1.8',      CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js',            CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-filestyle.min.js',       CClientScript::POS_END);

        $cs->registerScript('moneypicker','$(".grana").maskMoney({decimal:",", thousands:"."});');
        $cs->registerScript('select2','');
        $cs->registerScript('ipt_date','jQuery(function($){$("input.dateBR").mask("99/99/9999");});');

        $cs->registerScript('ipt_tempo_residencia','jQuery(function($){$("#tempo_de_residencia").mask("99/9999");});');
        $cs->registerScript('ipt_data','jQuery(function($){$("input.dateBR").mask("99/99/9999");});');
        $cs->registerScript('ipt_mes_ano','jQuery(function($){$("input.mes_ano").mask("99/9999");});');
        //$cs->registerScript('ipt_fone','jQuery(function($){$("input.telefone").mask("(99)9999-9999");});');
        $cs->registerScript('ipt_date','jQuery(function($){$("input.cpfmask").mask("99999999999");});');
        $cs->registerScript('ipt_date','jQuery(function($){$("input.cepmask").mask("99999999");});');
        $cs->registerScript('datepicker',"$('.date-picker').datepicker({autoclose: true});");
        $cs->registerScript('moneypicker2','$(".currency").maskMoney({decimal:",", thousands:".", allowZero: true});');
        $cs->registerScript('moneypicker','$(".currency2").maskMoney({decimal:",", thousands:".", allowZero: false});');
        $cs->registerScript('MesAnoRenda','jQuery(function($){$("input.mes_ano_renda").mask("99/9999");});');

        $documento        = new Documento;

        $this->render(
            'iniciar_analise_com_seguro_novo',array(
            'documento'     => $documento,
            'imgName'       => $this->actionGetCaptchaReceita( $this->token[0] ),
            'token'         => $this->token,
            'emprestimo'    => 0,
            'semear'        => 0,
            'cdc'           => 0,
            'pgd'           => 1,
        ));
    }

    public function actionIniciarEmprestimoSemear(){
        $baseUrl        = Yii::app()->baseUrl;
        $cs             = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');
        $cs->registerCssFile($baseUrl.'/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl.'/css/sigax_template/dataTables.tableTools.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js',                CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js',               CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js',        CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ui-elements.js',                   CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.autosize.min.js',           CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.js',                       CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskMoney.js',              CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js',            CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.inputlimiter.1.3.1.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js',                   CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js',            CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js',            CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js',             CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js',      CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js',       CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/fn-search-cep.js',                                        CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/blockInterface.js',                                       CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/fn-sigac-camera.js',                                      CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js',                CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js',          CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/fn-iniciar-analisev4.js',                                 CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js',            CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-filestyle.min.js',       CClientScript::POS_END);

        $cs->registerScript('moneypicker','$(".grana").maskMoney({decimal:",", thousands:"."});');
        $cs->registerScript('select2','');
        $cs->registerScript('ipt_date','jQuery(function($){$("input.dateBR").mask("99/99/9999");});');

        $cs->registerScript('ipt_tempo_residencia','jQuery(function($){$("#tempo_de_residencia").mask("99/9999");});');
        $cs->registerScript('ipt_data','jQuery(function($){$("input.dateBR").mask("99/99/9999");});');
        $cs->registerScript('ipt_mes_ano','jQuery(function($){$("input.mes_ano").mask("99/9999");});');
        //$cs->registerScript('ipt_fone','jQuery(function($){$("input.telefone").mask("(99)9999-9999");});');
        $cs->registerScript('ipt_date','jQuery(function($){$("input.cpfmask").mask("99999999999");});');
        $cs->registerScript('ipt_date','jQuery(function($){$("input.cepmask").mask("99999999");});');
        $cs->registerScript('datepicker',"$('.date-picker').datepicker({autoclose: true});");
        $cs->registerScript('moneypicker2','$(".currency").maskMoney({decimal:",", thousands:".", allowZero: true});');
        $cs->registerScript('moneypicker','$(".currency2").maskMoney({decimal:",", thousands:".", allowZero: false});');
        $cs->registerScript('MesAnoRenda','jQuery(function($){$("input.mes_ano_renda").mask("99/9999");});');

        $documento        = new Documento;

        $this->render   (
                                'iniciar_analise_com_seguro_novo',array(
                                'documento' => $documento,
                                'imgName' => $this->actionGetCaptchaReceita( $this->token[0] ),
                                'token'=> $this->token,
                                'emprestimo'=> true,
                                'semear' => 1,
                                'pgd' => 0,
                            )
                        );
    }

    public function actionIniciarEmprestimo()
    {
        $baseUrl        = Yii::app()->baseUrl;
        $cs             = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');
        $cs->registerCssFile($baseUrl.'/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl.'/css/sigax_template/dataTables.tableTools.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js',                CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js',               CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js',        CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ui-elements.js',                   CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.autosize.min.js',           CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.js',                       CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskMoney.js',              CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js',            CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.inputlimiter.1.3.1.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js',                   CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js',            CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js',            CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js',             CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js',      CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js',       CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/fn-search-cep.js',                                        CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/blockInterface.js',                                       CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/fn-sigac-camera.js',                                      CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js',                CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js',          CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/fn-iniciar-analisev4.js',                                 CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js',            CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-filestyle.min.js',       CClientScript::POS_END);

        $cs->registerScript('moneypicker','$(".grana").maskMoney({decimal:",", thousands:"."});');
        $cs->registerScript('select2','');
        $cs->registerScript('ipt_date','jQuery(function($){$("input.dateBR").mask("99/99/9999");});');

        $cs->registerScript('ipt_tempo_residencia','jQuery(function($){$("#tempo_de_residencia").mask("99/9999");});');
        $cs->registerScript('ipt_data','jQuery(function($){$("input.dateBR").mask("99/99/9999");});');
        $cs->registerScript('ipt_mes_ano','jQuery(function($){$("input.mes_ano").mask("99/9999");});');
        //$cs->registerScript('ipt_fone','jQuery(function($){$("input.telefone").mask("(99)9999-9999");});');
        $cs->registerScript('ipt_date','jQuery(function($){$("input.cpfmask").mask("99999999999");});');
        $cs->registerScript('ipt_date','jQuery(function($){$("input.cepmask").mask("99999999");});');
        $cs->registerScript('datepicker',"$('.date-picker').datepicker({autoclose: true});");
        $cs->registerScript('moneypicker2','$(".currency").maskMoney({decimal:",", thousands:".", allowZero: true});');
        $cs->registerScript('moneypicker','$(".currency2").maskMoney({decimal:",", thousands:".", allowZero: false});');
        $cs->registerScript('MesAnoRenda','jQuery(function($){$("input.mes_ano_renda").mask("99/9999");});');

        $documento        = new Documento;

        $this->render   (
                                'iniciar_analise_com_seguro_novo',array(
                                'documento' => $documento,
                                'imgName' => $this->actionGetCaptchaReceita( $this->token[0] ),
                                'token'=> $this->token,
                                'emprestimo'=> true,
                                'semear' => 0,
                                'pgd' => 0
                            )
                        );
    }

    public function actionLasts(){

        $baseUrl = Yii::app()->baseUrl;

        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl.'/js/jquery.min.js');

        $this->render('ultimas_propostas',array('model'=>Proposta::model()));

    }

    public function actionRetomarCadastro(){

        $rascunho_id    = $_POST['rascunho_id'];
        $rascunho       = AnaliseRascunho::model()->findByPk($rascunho_id);
        $cotacao        = Cotacao::model()->findByPk($rascunho->cotacao_id);

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerScriptFile($baseUrl.'/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl.'/js/jquery.maskedinput.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.maskMoney.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/form-elements.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/daterangepicker.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/bootstrap-datepicker.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/bootstrap-timepicker.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/bootstrap-colorpicker.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.tagsinput.js',CClientScript::POS_END);

        $cs->registerScriptFile($baseUrl.'/js/summernote.min.js',  CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/ckeditor/ckeditor.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/ckeditor/adapter/jquery.js',CClientScript::POS_END);

        $cs->registerScriptFile($baseUrl.'/js/form-wizard.js',  CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.validate.min.js',  CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.smartWizard.js',  CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/select2.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.inputlimiter.1.3.1.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.autosize.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/update-select.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/bootstrap-fileupload.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/analise-upload-file-manager.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/fn-search-cep.js',CClientScript::POS_END);
        //$cs->registerScriptFile($baseUrl.'/js/fn-save-draft.js',CClientScript::POS_END);

        $cs->registerCssFile($baseUrl.'/css/sigax_template/bootstrap-fileupload.min.css');

        $cs->registerScript('MesAnoRenda','jQuery(function($){$("input.mes_ano_renda").mask("99/9999");});');
        $cs->registerScript('cep','jQuery(function($){$("input.cep").mask("99999999");});');
        $cs->registerScript('telefone','jQuery(function($){$("input.telefone").mask("(99)9999-9999");});');
        $cs->registerScript('dinheiro','jQuery("input.dinheiro").maskMoney({decimal:"."});');
        $cs->registerScript('ipt_date','jQuery(function($){$("input.ipt_date").mask("99/99/9999");});');
        $cs->registerScript('tempo_resi','jQuery(function($){$("#tempo_de_residencia").mask("99/9999");});');

        $cs->registerScript('FormWizard','FormWizard.init();');
        $cs->registerScript('FormElements','FormElements.init();');

        $this->render('analise_retomar_cadastro', array(
            'rascunho' => $rascunho,
            'cotacao' => $cotacao
        ));
    }

    public function actionHistoricoCrediarista(){

        /*Histórico*/

        $this->layout = '//layouts/main_sigac_template';

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerScriptFile($baseUrl.'/js/jquery.min.js',  CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/bootstrap.min.js',  CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/fn-crediarista-historico.js');
        $cs->registerScriptFile($baseUrl.'/js/ui-modals.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/bootstrap-modal.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/bootstrap-modalmanager.js',CClientScript::POS_END);

        $cs->registerScript('UIModals','UIModals.init();');

        $userId = $_POST['userId'];

        $this->render('crediaristaHist',array(
            'analises' => AnaliseDeCredito::model()->analisesCrediarista( $userId )
        ));
    }

    public function actionStep1(){

        $nome                           = $_POST['nome'];
        $cpf                            = $_POST['cpf'];
        $situacao_cadastral             = $_POST['situacao'];
        $renda_liquida                  = $_POST['renda_liquida'];
        $valor_soli                     = $_POST['valor_soli'];
        $valor_entrada                  = $_POST['valor_entrada'];

        $parcela_sugerida = ( $renda_liquida * 30 ) / 100;

        $pessoa                         = new Pessoa;
        $oCpf                           = new Documento;
        //$dcoHasSituacaoCadastral = new DocumentoHasSituacaoCadastral;

        $isClient                       = "";
        $criteria                       = new CDbCriteria;
        $criteria->addCondition("numero = " . $cpf . " AND Tipo_documento_id = 1");

        if( sizeof( Documento::model()->find( $criteria ) ) > 0 ){

            $isClient                   = "Sim";
            $oCpf                       = Documento::model()->find( $criteria );

            $cpfPessoa                  = PessoaHasDocumento::model()->find('Documento_id = ' . $oCpf->id);
            $pessoa                     = Pessoa::model()->findByPk( $cpfPessoa->Pessoa_id );

        }

        else{

            $isClient                   = "Não";
            $pessoa->nome               = $nome;
            $oCpf->numero               = $cpf;
            $oCpf->Tipo_documento_id    = 1;

        }

        $baseUrl                        = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl.'/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl.'/js/form-elements.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/select2.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/table-parcelas-fn.js');
        $cs->registerScriptFile($baseUrl.'/js/jquery.inputlimiter.1.3.1.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.autosize.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/spin.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/ladda.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/bootstrap-switch.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/ui-buttons.js',CClientScript::POS_END);

        $cs->registerScript('UIButtons','UIButtons.init();');
        $cs->registerScript('FormElements','FormElements.init();');

        $model                      = new Documento;
        $util                       = new Util;

        $this->render('_analise_form',array(
            'model'                 =>  $model,
            'pessoa'                =>  $pessoa,
            'cpf'                   =>  $oCpf,
            'situacao_cadastral'    =>  $situacao_cadastral,
            'isClient'              =>  $isClient,
            'renda_liquida'         =>  $renda_liquida,
            'valor_soli'            =>  $valor_soli,
            'parcela_sugerida'      =>  $parcela_sugerida,
            'valor_entrada'         =>  $valor_entrada
        ));
    }

    public function actionVerPropostas(){

        $this->layout = '//layouts/template_vazio';

        $id = $_GET['id'];
        $analise = AnaliseDeCredito::model()->findByPk( $id );

        $this->render('ver_propostas_analise', array(
            'analise'      => $analise,
            'propsotasTG'  => $analise->getPropostasAnalise(array(1,2,3,4,5), 1),
            'propsotasTNG' => $analise->getPropostasAnalise(array(1,2,3,4,5), 0)
        ));
    }

    public function actionMaisDetalhes(){

        $this->layout = '//layouts/template_vazio';

        $id = $_GET['id'];
        $analise = AnaliseDeCredito::model()->findByPk( $id );

        $this->render('mais_detalhes_da_analise', array(
            'analise' => $analise
        ));
    }

    public function actionAnaliseCliente(){

        $baseUrl    = Yii::app()->baseUrl;
        $cs         = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        //$cs->registerScriptFile($baseUrl.'/js/form-elements.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/table-parcelas-fn.js');
        //$cs->registerScriptFile($baseUrl . '/js/jquery.inputlimiter.1.3.1.min.js',CClientScript::POS_END);
        //$cs->registerScriptFile($baseUrl . '/js/jquery.autosize.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/spin.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ladda.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-switch.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ui-buttons.js',CClientScript::POS_END);

        $cs->registerScript('UIButtons','UIButtons.init();');
        $cs->registerScript('FormElements','$(".search-select").select2({placeholder: "Select a State",allowClear: true});');

        $clienteId = $_POST['input_hdn_pessoa_id'];
        $cliente = Cliente::model()->findByPk( $clienteId );

        $situacao_cadastral = $_POST['situacao'];
        $valor_soli = $_POST['valor_soli'];
        $valor_entrada = $_POST['valor_entrada'];
        $renda_liquida =

        $parcela_sugerida = ( $cliente->pessoa->getDadosProfissionais()->renda_liquida * 30 ) / 100;

        $sigacLog                               = new SigacLog;
        $util                                   = new Util;
        $sigacLog->saveLog('Início de cadastro de análise','AnaliseDeCredito', NULL, date('Y-m-d H:i:s'),1, Yii::app()->session['usuario']->id, "Usuario ". Yii::app()->session['usuario']->username . " iniciou o cadastro de uma análise.", "127.0.0.1",NULL, Yii::app()->session->getSessionId() );

        $this->render('_analise_form_cliente',array(

            'pessoa'                =>  $cliente->pessoa,
            'cliente'               =>  $cliente,
            'cpf'                   =>  $cliente->pessoa->getCPF(),
            'situacao_cadastral'    =>  $situacao_cadastral,
            'isClient'              =>  "Sim",
            'renda_liquida'         =>  number_format( $cliente->pessoa->getDadosProfissionais()->renda_liquida, 2 , '.', ''),
            'valor_soli'            =>  $util->moedaViewToBD($valor_soli),
            'parcela_sugerida'      =>  $parcela_sugerida,
            'valor_entrada'         =>  $util->moedaViewToBD($valor_entrada)
        ));
    }

    public function actionStep2(){

        $documento = new Documento;
        $numero = $_POST['Documento']['numero'];

        $arrayDados = Yii::app()->session['dados'];

        $arrayDados['cpf'] = $numero;

        Yii::app()->session['dados'] = $arrayDados;

        $this->render('_form_step1',array(
            'documento' => $documento,
        ));
    }

    public function actionView($id){

        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    public function actionCreate(){

        $model = new AnaliseDeCredito;

        if( isset( $_POST['AnaliseDeCredito'] ) ){

            $model->attributes=$_POST['AnaliseDeCredito'];

            if( $model->save() ){

                $this->redirect(array('view','id'=>$model->id));
            }
        }

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl.'/js/jquery.min.js');

        $this->render('create',array(
            'model'=>$model,
        ));
    }

    public function actionUpdate($id){

        $model = $this->loadModel($id);


        if(isset($_POST['AnaliseDeCredito'])){

            $model->attributes = $_POST['AnaliseDeCredito'];

            if($model->save()){
                $this->redirect(array('view','id'=>$model->id));
            }
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    public function actionDelete($id){

        $this->loadModel($id)->delete();

        if( !isset( $_GET['ajax'] ) ){
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }

    public function actionGetReceitaToken(){

        Yii::import('application.vendor.*');

        $this->cookieFile = 'cookie/' . session_id();

        if ( !file_exists( $this->cookieFile ) ) {

            $file = fopen($this->cookieFile, 'w');
            fclose($file);

        }


        $ch = curl_init('http://www.receita.fazenda.gov.br/aplicacoes/atcta/cpf/consultapublica.asp');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookieFile);
        $html = curl_exec($ch);

        if (!$html) {
            return false;
        }

        $html = new Simple_html_dom($html);
        $url_imagem = $tokenValue = '';
        $imgcaptcha = $html->find('img[id=imgcaptcha]');

        if ( count($imgcaptcha) ) {

            foreach($imgcaptcha as $imgAttr)
                $url_imagem = $imgAttr->src;

            if ( preg_match('#guid=(.*)$#', $url_imagem, $arr) ) {

                $idCaptcha = $arr[1];
                $viewstate = $html->find('input[id=viewstate]');

                if ( count($viewstate) ) {
                    foreach ($viewstate as $inputViewstate)
                        $tokenValue = $inputViewstate->value;
                }

                if (!empty($idCaptcha) && !empty($tokenValue)) {

                    $this->token = array(
                        $idCaptcha,
                        $tokenValue
                    );
                }
                else{
                    $this->token = false;
                }
            }
        }
    }

    public function actionGetCaptchaReceita( $idCaptcha, $oldCaptcha = NULL ){

        $util = new Util;

        $imgName = $util->getToken(50).'.jpg';

        if ( preg_match('#^[a-z0-9-]{36}$#', $idCaptcha) ) {

            $url = 'http://www.receita.fazenda.gov.br/scripts/captcha/Telerik.Web.UI.WebResource.axd?type=rca&guid=' . $idCaptcha;
            $ch  = curl_init( $url );

            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
            $imgsource = curl_exec($ch);
            curl_close($ch);

            if ( !empty($imgsource) ) {

                if ( $oldCaptcha != NULL ) {

                    if ( file_exists( 'images/captchas/captchas_'.Yii::app()->session['usuario']->id.'/'.$oldCaptcha ) ) {
                        unlink('images/captchas/captchas_'.Yii::app()->session['usuario']->id.'/'.$oldCaptcha);
                    }

                }

                $fp = fopen('images/captchas/captchas_'.Yii::app()->session['usuario']->id.'/'.$imgName, 'x');
                fwrite($fp, $imgsource);
                fclose($fp);
            }
        }

        return $imgName;
    }

    public function actionGetCpfReceita(){

        $this->cookieFile = 'cookie/' . session_id();

        $cpf        = $_POST['cpf'];

        /*$captcha    = $_POST['captcha'];
        $token_0    = $_POST['token_0'];
        $token_1    = $_POST['token_1'];

        $token = array(
            $token_0,
            $token_1
        );

        if ( !file_exists( $this->cookieFile ) ) {

            return false;

        }

        $post = array(
            'txtCPF'        => $cpf,
            'captcha'       => $captcha,
            'captchaAudio'  => '',
            'Enviar'        => 'Consultar',
            'viewstate'     => $token[1]
        );

        $data   = http_build_query($post, NULL, '&');

        $cookie = array(
            'flag' => 1
        );

        $ch = curl_init('http://www.receita.fazenda.gov.br/aplicacoes/atcta/cpf/ConsultaPublicaExibir.asp');

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookieFile);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookieFile);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0) Gecko/20100101 Firefox/8.0');
        curl_setopt($ch, CURLOPT_COOKIE, http_build_query($cookie, NULL, '&'));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
        curl_setopt($ch, CURLOPT_REFERER, 'http://www.receita.fazenda.gov.br/aplicacoes/atcta/cpf/consultapublica.asp');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $html = curl_exec($ch);
        curl_close($ch);

        Yii::import('application.vendor.*');

        $dom = new DomDocument();

        libxml_use_internal_errors(true);

        $dom->loadHTML($html);
        $nodes  = $dom->getElementsByTagName('span');

        $campos = array();

        for ( $i = 5; $i < 10; $i++ ) {

            if (!isset($nodes->item(($i + 1))->nodeValue)) {
                break;
            }

            $current = trim($nodes->item($i)->nodeValue);
            $prox    = trim($nodes->item(($i + 1))->nodeValue);

            if (strpos($current, 'o Cadastral') !== false) {

                $campos['situacao'] = explode(':', $current);
                if (count($campos['situacao']) == 2)
                    $campos['situacao'] = trim($campos['situacao'][1]);
            }

            if (strpos($current, 'Nome da Pessoa F') !== false) {

                $campos['nome'] = explode(':', $current);
                if (count($campos['nome']) == 2)
                    $campos['nome'] = trim($campos['nome'][1]);
            }
        }


        $dadosResponse = array(
            "statusReturn"  => "und",
            "nome"          => "und",
            "situacao"      => "und",
            "cpf"           => $cpf,
            "isClient"      => "0",
            "inputCount"    => "2",
        );


        if( count( $campos ) < 1)
        {

            $dadosResponse['inputCount'] = '0';
            echo json_encode($dadosResponse);
        }

        else
        {

            $dadosResponse["nome"]      = trim($campos['nome']);
            $dadosResponse["situacao"]  = trim($campos['situacao']);

            $pessoa = Pessoa::model()->isClient( $cpf );

            if ( $pessoa != NULL )
            {

                $cliente = Cliente::model()->find( 'Pessoa_id = ' . $pessoa->id );

                if( $cliente != NULL )
                {

                    $cadastroClienteStatus              = $cliente->clienteCadastroStatus();
                    $dadosResponse["nome"]              = $pessoa->nome;
                    $dadosResponse["situacao"]          = trim($campos['situacao']);

                    if( $cadastroClienteStatus['status'] == 2 )
                    {
                        $dadosResponse["statusReturn"]  = "2";
                        $dadosResponse["isClient"]      = "1";
                        $dadosResponse["observacoes"]   = $cadastroClienteStatus['observacoes'];
                        $dadosResponse["clienteId"]     = $cliente->id;
                    }
                    else
                    {
                        $dadosResponse["statusReturn"]  = "1";
                        $dadosResponse["isClient"]      = "1";
                        $dadosResponse["clienteId"]     = $cliente->id;
                        $dadosResponse["rendaLiquida"]  = number_format($pessoa->getDadosProfissionais()->renda_liquida, 2, '.','');
                    }
                }
            }

            else
            {
                $dadosResponse["isClient"] = "0";
            }

            echo json_encode( $dadosResponse );
        }*/

        $dadosResponse = array(
            "statusReturn"  => "und",
            "nome"          => "und",
            "situacao"      => "und",
            "cpf"           => $cpf,
            "isClient"      => "0",
            "inputCount"    => "2",
        );
        $pessoa = Pessoa::model()->isClient( $cpf );

        if ( $pessoa != NULL )
        {

            $cliente = Cliente::model()->find( 'Pessoa_id = ' . $pessoa->id );

            if( $cliente != NULL )
            {
                $cadastroClienteStatus              = $cliente->clienteCadastroStatus();
                $dadosResponse["nome"]              = $pessoa->nome;
                $dadosResponse["situacao"]          = "REGULAR";

                if( $cadastroClienteStatus['status'] == 2 )
                {
                        $dadosResponse["statusReturn"]  = "2";
                        $dadosResponse["isClient"]      = "1";
                        $dadosResponse["observacoes"]   = $cadastroClienteStatus['observacoes'];
                        $dadosResponse["clienteId"]     = $cliente->id;
                }
                else
                {
                        $dadosResponse["statusReturn"]  = "1";
                        $dadosResponse["isClient"]      = "1";
                        $dadosResponse["clienteId"]     = $cliente->id;
                        $dadosResponse["rendaLiquida"]  = number_format($pessoa->getDadosProfissionais()->renda_liquida, 2, '.','');
                }
            }
        }

        else
        {
            $dadosResponse["isClient"] = "0";
        }

        echo json_encode( $dadosResponse );
    }

    public function actionIndex(){

        /*Limpando pasta de imagens do crediarista*/
        Yii::app()->session['usuario']->cleanCaptchas();

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js');
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ui-elements.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.autosize.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskMoney.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.inputlimiter.1.3.1.min.js',CClientScript::POS_END);
        $cs->registerScript('moneypicker','$(".grana").maskMoney({decimal:",", thousands:"."});');


        $documento = new Documento;

        $this->actionGetReceitaToken();

        $this->render('_form_step1',array(
            'documento' => $documento,
            'imgName' => $this->actionGetCaptchaReceita( $this->token[0] ),
            'token'=> $this->token,
        ));
    }

    public function actionUpdateCaptcha(){

        $oldCaptcha = $_POST['oldCaptcha'];

        $this->actionGetReceitaToken();

        $imgName = $this->actionGetCaptchaReceita( $this->token[0], $oldCaptcha );

        echo json_encode(

            array(
                'imgName'   => $imgName,
                'token'     => array(
                    'token_0' => $this->token[0],
                    'token_1' => $this->token[1]
                ),
                'newImgUrl' => Yii::app()->request->baseUrl.'/images/captchas/captchas_'.Yii::app()->session['usuario']->id.'/'.$imgName
            )

        );
    }

    public function actionAdmin(){

        $model = new AnaliseDeCredito('search');
        $model->unsetAttributes();

        if( isset( $_GET['AnaliseDeCredito'] ) ){
            $model->attributes=$_GET['AnaliseDeCredito'];
        }

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    public function loadModel($id){

        $model = AnaliseDeCredito::model()->findByPk($id);

        if($model===null){
            throw new CHttpException(404,'The requested page does not exist.');
        }

        return $model;
    }

    public function actionRenderReceitaForm(){

            if( !file_exists( "RF_cookie_cpf.txt" ) ){
               $fp = fopen("RF_cookie_cpf.txt","w");
               fwrite($fp, "");
               fclose($fp);
            }

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_COOKIEFILE, realpath("RF_cookie_cpf.txt"));
            curl_setopt($ch, CURLOPT_COOKIEJAR, realpath("RF_cookie_cpf.txt"));
            curl_setopt($ch, CURLOPT_URL, "http://www.receita.fazenda.gov.br/aplicacoes/atcta/cpf/consultapublica.asp");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_COOKIE, "flag=0");

            $retorno = curl_exec( $ch );
            curl_close( $ch );

            $dom = new DomDocument();

            @$dom->loadHTML($retorno);

            $xpath = new DOMXPath($dom);

            $q = $xpath->query("//img[@id='imgcaptcha']");

            $imagem = "http://www.receita.fazenda.gov.br/" . utf8_decode(trim($q->item(0)->getAttribute('src')));

            $q = $xpath->query("//input[@id='viewstate']");

            Yii::app()->session["viewstate"] = utf8_decode( trim( $q->item(0)->getAttribute( 'value' ) ) );

            echo $imagem; /*to _form_step1*/
    }

    public function actionValidaCpfReceita(){

        $cpf = $_POST['CPF'];
        $captha = $_POST['Captha'];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_COOKIEFILE, realpath("RF_cookie_cpf.txt"));
        curl_setopt($ch, CURLOPT_COOKIEJAR, realpath("RF_cookie_cpf.txt"));
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0) Gecko/20100101 Firefox/8.0');
        curl_setopt($ch, CURLOPT_URL, "http://www.receita.fazenda.gov.br/aplicacoes/atcta/cpf/ConsultaPublicaExibir.asp");
        curl_setopt($ch, CURLOPT_COOKIE, "flag=1");
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'txtCPF=' . $cpf . '&captcha=' . $captha . '&captchaAudio=&viewstate=' . urlencode(Yii::app()->session["viewstate"] ).'&id_submit=Consultar');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FILETIME, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $resultado = curl_exec($ch);

        curl_close($ch);

        $dom = new DomDocument();
        @$dom->loadHTML($resultado);


        $xpath = new DOMXPath($dom);

        $q = $xpath->query('//div[@class="clConteudoCentro"]');
        $q2 = $xpath->query('//span[@class="clConteudoDados"]');

        $nome = trim(utf8_decode(@$q2->item(1)->nodeValue));
        $nome = explode(":", $nome);

        $situacao = trim(utf8_decode(@$q2->item(2)->nodeValue));
        $situacao = explode(":", $situacao);

        $dadosResponse = array(
            "statusReturn" => "und",
            "nome" => "und",
            "situacao" => "und",
            "cpf" => $cpf,
            "isClient" => "0"
        );

        if(@$situacao[1] == ""){
            $dadosResponse["statusReturn"] = "0";
            $dadosResponse["nome"] = "";
            $dadosResponse["situacao"] = "";
        }

        else{
            $dadosResponse["statusReturn"] = "1";
            $dadosResponse["nome"] = trim($nome[1]) ;
            $dadosResponse["situacao"] = trim($situacao[1]);
        }

        $pessoa = Pessoa::model()->isClient($cpf);

        if ($pessoa != NULL)
            $dadosResponse["isClient"] = "1";
        else
            $dadosResponse["isClient"] = "0";

        /*$dadosResponse = array(
            "statusReturn" => "1",
            "nome" => "Eric Vinicius Vieira Ferreira",
            "situacao" => "Regular",
            "cpf" => $cpf
        );*/

        echo json_encode( $dadosResponse );

    }

    public function actionAnaliseCheckout(){

        $cotacao_id                     = $_POST['cotacao_id'];
        $carencia                       = $_POST['carencia'];
        $val_financiado                 = $_POST['val_fin'];
        $numero_parcelas                = $_POST['numero_parcelas'];
        $valor_parcela                  = $_POST['valor_parcela'];

        $cotacao                        = TabelaCotacao::model()->findByPk( $cotacao_id );

        $hoje                           = date('Y-m-d');

        $data_primeira_parcela          = date('d/m/Y', strtotime($hoje. ' +'.$carencia .' days'));
        $data_ultima_parcela            = date('Y-m-d', strtotime($hoje. ' +'.$carencia .' days'));
        $data_ultima_parcela            = date( 'd/m/Y', strtotime( "+".$numero_parcelas-1 . " months", strtotime( $data_ultima_parcela ) ) );
        $valor_parcelaMASK              = number_format($valor_parcela, 2, ",", ".");
        $val_financiadoMASK             = number_format($val_financiado, 2, ",", ".");
        $taxa_MASK                      = number_format($cotacao->taxa, 2, ",", ".");
        $valor_totalMASK                = number_format($valor_parcela * $numero_parcelas, 2, ",", ".");

        $dadosResponse                  = array(
            "valor_parcelaMASK"         => $valor_parcelaMASK,
            "valor_financiadoMASK"      => $val_financiadoMASK,
            "taxa_MASK"                 => $taxa_MASK,
            "valor_totalMASK"           => $valor_totalMASK,
            "numero_parcelas"           => $numero_parcelas,
            "valor_parcela"             => $valor_parcela,
            "valor_total"               => $valor_parcela * $numero_parcelas,
            "data_primeira_parcela"     => $data_primeira_parcela,
            "data_ultima_parcela"       => $data_ultima_parcela,
            "cotacao_id"                => $cotacao_id,
            "carencia"                  => $carencia,
            "cotacao_descri"            => $cotacao->descricao,
            "cotacao_taxa"              => $cotacao->taxa,
            "valor_financiado"          => $val_financiado,
        );

        echo json_encode( $dadosResponse );
    }

    public function actionSaveDraft(){

        $idRascunho                                         = $_POST['Rascunho_id'];

        $analiseRascunho                                    = AnaliseRascunho::model()->findByPk($idRascunho);

        $nomeCliente                                        = $_POST['Pessoa']['nome'];
        $cpf                                                = $_POST['CPFCliente']['numero'];
        $renda_liquida_cliente                              = $_POST['cliente_renda_liquida'];
        $valor_solicitado                                   = $_POST['valor_solicitado_analise'];
        $valor_entrada                                      = $_POST['Proposta']['valor_entrada'];
        $valor_financiado                                   = $_POST['valor_financiado'];
        $num_parcelas                                       = $_POST['Proposta']['qtd_parcelas'];
        $val_parcelas                                       = $_POST['Proposta']['valor_parcela'];
        $cotacao_id                                         = $_POST['Proposta']['Cotacao_id'];
        $carencia                                           = $_POST['Proposta']['carencia'];
        $valor_total                                        = $_POST['Proposta']['valor_final'];
        $data_primeira_parcela                              = $_POST['data_pri_par'];
        $data_ultima_parcela                                = $_POST['data_pri_par'];
        $titular_do_cpf                                     = $_POST['Cadastro']['titular_do_cpf'];
        $nascimento                                         = $_POST['Pessoa']['nascimento'];
        $sexo                                               = $_POST['Pessoa']['sexo'];
        $nacionalidade                                      = $_POST['Pessoa']['nacionalidade'];
        $naturalidade_cliente                               = $_POST['naturalidade_br'];
        $naturalidade_ext_cliente                           = $_POST['naturalidade_ext'];
        $nome_do_pai                                        = $_POST['Cadastro']['nome_do_pai'];
        $nome_da_mae                                        = $_POST['Cadastro']['nome_da_mae'];
        $numero_de_dependentes                              = $_POST['Cadastro']['numero_de_dependentes'];
        $estado_civil                                       = $_POST['Pessoa']['estado_civil'];
        $conjugue_compoe_renda                              = $_POST['Cadastro']['conjugue_compoe_renda'];
        $tipo_de_documento                                  = $_POST['DocumentoCliente']['Tipo_documento_id'];
        $numero_do_documento                                = $_POST['DocumentoCliente']['numero'];
        $orgao_emissor_documento                            = $_POST['DocumentoCliente']['orgao_emissor'];
        $uf_emissor                                         = $_POST['DocumentoCliente']['uf_emissor'];
        $data_emissao_documento                             = $_POST['DocumentoCliente']['data_emissao'];
        $cep_pessoal                                        = $_POST['EnderecoCliente']['cep'];
        $logradouro                                         = $_POST['EnderecoCliente']['logradouro'];
        $cidade                                             = $_POST['EnderecoCliente']['cidade'];
        $bairro                                             = $_POST['EnderecoCliente']['bairro'];
        $tempo_de_residencia                                = $_POST['EnderecoCliente']['tempo_de_residencia'];
        $tipo_moradia_id                                    = $_POST['EnderecoCliente']['Tipo_Moradia_id'];
        $numero                                             = $_POST['EnderecoCliente']['numero'];
        $complemento                                        = $_POST['EnderecoCliente']['complemento'];
        $uf_endereco_pessoal                                = $_POST['EnderecoCliente']['uf'];
        $tipo_endereco_pessoal                              = $_POST['EnderecoCliente']['Tipo_Endereco_id'];
        $telefone1_numero                                   = $_POST['TelefoneCliente']['numero'];
        $telefone1_tipo                                     = $_POST['TelefoneCliente']['Tipo_Telefone_id'];
        $telefone1_ramal                                    = $_POST['TelefoneCliente']['ramal'];
        $telefone2_numero                                   = $_POST['TelefoneCliente2']['numero'];
        $telefone2_tipo                                     = $_POST['TelefoneCliente2']['Tipo_Telefone_id'];
        $telefone2_ramal                                    = $_POST['TelefoneCliente2']['ramal'];
        $email_cliente                                      = $_POST['EmailCliente']['email'];
        $cliente_facebook                                   = $_POST['ContatoCliente']['facebook'];
        $cliente_skype                                      = $_POST['ContatoCliente']['skype'];
        $nome_completo_conjuge                              = $_POST['Conjuge']['nome'];
        $cpf_do_conjuge                                     = $_POST['CPF_Conjuge']['numero'];
        $tipo_de_documento_conjuge                          = $_POST['Doc_Conjuge']['Tipo_documento_id'];
        $numero_do_documento_conjuge                        = $_POST['Doc_Conjuge']['numero'];
        $orgao_emissor_documento_conjuge                    = $_POST['Doc_Conjuge']['orgao_emissor'];
        $uf_emissor_documento_conjuge                       = $_POST['Doc_Conjuge']['uf_emissor'];
        $data_emissao_documento_conjuge                     = $_POST['Doc_Conjuge']['data_emissao'];
        $conjuge_nascimento                                 = $_POST['Conjuge']['nascimento'];
        $sexo_conjuge                                       = $_POST['Conjuge']['sexo'];
        $nacionalidade_conjuge                              = $_POST['Conjuge']['nacionalidade'];
        $naturalidade_conjuge_ext                           = $_POST['naturalidade_con_ext'];
        $naturalidade_conjuge                               = $_POST['naturalidade_con_br'];
        $renda_liquida_conjuge                              = $_POST['DadosProfissionaisConjuge']['renda_liquida'];
        $mes_ano_renda_conjuge                              = $_POST['DadosProfissionaisConjuge']['mes_ano_renda'];
        $ocupacao_atual_conjuge                             = $_POST['DadosProfissionaisConjuge']['Tipo_Ocupacao_id'];
        $profissao_conjuge                                  = $_POST['DadosProfissionaisConjuge']['profissao'];
        $tipo_de_comprovante_conjuge                        = $_POST['DadosProfissionaisConjuge']['TipoDeComprovante'];
        $mesmo_endereco_conjuge                             = $_POST['MesmoEndereco'];
        $cep_conjuge                                        = $_POST['EnderecoConjuge']['cep'];
        $logradouro_conjuge                                 = $_POST['EnderecoConjuge']['logradouro'];
        $cidade_conjuge                                     = $_POST['EnderecoConjuge']['cidade'];
        $bairro_conjuge                                     = $_POST['EnderecoConjuge']['bairro'];
        $numero_endereco_conjuge                            = $_POST['EnderecoConjuge']['numero'];
        $complemento_endereco_conjuge                       = $_POST['EnderecoConjuge']['complemento'];
        $uf_endereco_conjuge                                = $_POST['EnderecoConjuge']['uf'];
        $tipo_de_endereco_conjuge                           = $_POST['EnderecoConjuge']['Tipo_Endereco_id'];
        $telefone_conjuge                                   = $_POST['TelefoneConjuge']['numero'];
        $tipo_de_telefone_conjuge                           = $_POST['TelefoneConjuge']['Tipo_Telefone_id'];
        $email_conjuge                                      = $_POST['EmailConjuge']['email'];
        $numero_conta                                       = $_POST['DadosBancarios']['numero'];
        $tipo_de_conta                                      = $_POST['DadosBancarios']['Tipo_Conta_Bancaria_id'];
        $agencia_conta                                      = $_POST['DadosBancarios']['agencia'];
        $banco_conta                                        = $_POST['DadosBancarios']['Banco_id'];
        $data_de_abertura_conta                             = $_POST['DadosBancarios']['data_abertura'];
        $classe_profissional                                = $_POST['DadosProfissionais']['Classe_Profissional_id'];
        $cpf_cnpj_profissional                              = $_POST['DadosProfissionais']['cnpj_cpf'];
        $empresa_profissional                               = $_POST['DadosProfissionais']['empresa'];
        $ocupacao_atual                                     = $_POST['DadosProfissionais']['Tipo_Ocupacao_id'];
        $data_admissao                                      = $_POST['DadosProfissionais']['data_admissao'];
        $profissao                                          = $_POST['DadosProfissionais']['profissao'];
        $mes_ano_renda                                      = $_POST['DadosProfissionais']['mes_ano_renda'];
        $orgao_pagador                                      = $_POST['DadosProfissionais']['orgao_pagador'];
        $numero_do_beneficio                                = $_POST['DadosProfissionais']['numero_do_beneficio'];
        $tipo_de_comprovante                                = $_POST['DadosProfissionais']['TipoDeComprovante'];

        $cep_endereco_profissional                          = $_POST['EnderecoProfiss']['cep'];
        $logradouro_endereco_profissional                   = $_POST['EnderecoProfiss']['logradouro'];
        $numero_endereco_profissional                       = $_POST['EnderecoProfiss']['numero'];
        $cidade_endereco_profissional                       = $_POST['EnderecoProfiss']['cidade'];
        $bairro_endereco_profissional                       = $_POST['EnderecoProfiss']['bairro'];
        $uf_endereco_profissional                           = $_POST['EnderecoProfiss']['uf'];
        $complemento_endereco_profissional                  = $_POST['EnderecoProfiss']['complemento'];
        $telefone_profissional                              = $_POST['TelefoneProfiss']['numero'];
        $ramal_telefone_profissional                        = $_POST['TelefoneProfiss']['ramal'];
        $tipo_telefone_profissional                         = $_POST['TelefoneProfiss']['Tipo_Telefone_id'];
        $email_telefone_profissional                        = $_POST['EmailProfiss']['email'];

        $referencia1_nome                                   = $_POST['Referencia1']['nome'];
        $referencia1_tipo                                   = $_POST['Referencia1']['Tipo_Referencia_id'];
        $referencia1_telefone_numero                        = $_POST['TelefoneRef1']['numero'];
        $referencia1_telefone_ramal                         = $_POST['TelefoneRef1']['ramal'];

        $referencia2_nome                                   = $_POST['Referencia2']['nome'];
        $referencia2_telefone_numero                        = $_POST['TelefoneRef2']['numero'];
        $referencia2_tipo                                   = $_POST['Referencia2']['Tipo_Referencia_id'];
        $referencia2_telefone_ramal                         = $_POST['TelefoneRef2']['ramal'];

        $numero_do_pedido                                   = $_POST['AnaliseDeCredito']['numero_do_pedido'];
        $vendedor                                           = $_POST['AnaliseDeCredito']['vendedor'];
        $promotor                                           = $_POST['AnaliseDeCredito']['promotor'];
        $procedencia_da_compra                              = $_POST['AnaliseDeCredito']['Procedencia_compra_id'];
        $valor                                              = $_POST['AnaliseDeCredito']['valor'];
        $entrada                                            = $_POST['AnaliseDeCredito']['entrada'];
        $alerta                                             = $_POST['AnaliseDeCredito']['alerta'];
        $mercadoria_retirada_na_hora                        = $_POST['AnaliseDeCredito']['mercadoria_retirada_na_hora'];
        $celular_pre_pago                                   = $_POST['AnaliseDeCredito']['celular_pre_pago'];
        $mercadoria                                         = $_POST['AnaliseDeCredito']['mercadoria'];
        $observacao                                         = $_POST['AnaliseDeCredito']['observacao'];

        /*---------------------------------------------------------------------------------------------------*/
        $analiseRascunho->nome_completo                     = $nomeCliente;
        $analiseRascunho->cpf                               = $cpf;
        $analiseRascunho->renda_liquida                     = $renda_liquida_cliente;
        $analiseRascunho->valor_solicitado                  = $valor_solicitado;

        if ( $valor_entrada == NULL )
            $analiseRascunho->valor_entrada                 = 0;
        else
            $analiseRascunho->valor_entrada                 = $valor_entrada;

        $analiseRascunho->valor_financiado                  = $valor_financiado;
        $analiseRascunho->num_parcelas                      = $num_parcelas;
        $analiseRascunho->val_parcelas                      = $val_parcelas;

        $cotacao                                            = Cotacao::model()->findByPk($cotacao_id);
        $analiseRascunho->taxa                              = $cotacao->taxa;
        $analiseRascunho->carencia                          = $carencia;
        $analiseRascunho->valor_total                       = $valor_total;
        $analiseRascunho->data_primeira_parcela             = $data_primeira_parcela;
        $analiseRascunho->data_ultima_parcela               = $data_ultima_parcela;
        $analiseRascunho->cotacao_id                        = $cotacao->id;
        $analiseRascunho->titular_do_cpf                    = $titular_do_cpf;
        $analiseRascunho->nascimento                        = $nascimento;
        $analiseRascunho->sexo                              = $sexo;
        $analiseRascunho->nacionalidade                     = $nacionalidade;
        $analiseRascunho->naturalidade_cliente              = $naturalidade_cliente;
        $analiseRascunho->naturalidade_ext                  = $naturalidade_ext_cliente;
        $analiseRascunho->nome_do_pai                       = $nome_do_pai;
        $analiseRascunho->nome_da_mae                       = $nome_da_mae;
        $analiseRascunho->numero_de_dependentes             = $numero_de_dependentes;
        $analiseRascunho->estado_civil                      = $estado_civil;
        $analiseRascunho->conjugue_compoe_renda             = $conjugue_compoe_renda;
        $analiseRascunho->tipo_de_documento                 = $tipo_de_documento;
        $analiseRascunho->numero_do_documento               = $numero_do_documento;
        $analiseRascunho->orgao_emissor_documento           = $orgao_emissor_documento;
        $analiseRascunho->uf_emissor                        = $uf_emissor;
        $analiseRascunho->data_emissao_documento            = $data_emissao_documento;
        $analiseRascunho->cep_pessoal                       = $cep_pessoal;
        $analiseRascunho->logradouro                        = $logradouro;
        $analiseRascunho->cidade                            = $cidade;
        $analiseRascunho->bairro                            = $bairro;
        $analiseRascunho->tempo_de_residencia               = $tempo_de_residencia;
        $analiseRascunho->tipo_moradia_id                   = $tipo_moradia_id;
        $analiseRascunho->numero                            = $numero;
        $analiseRascunho->complemento                       = $complemento;
        $analiseRascunho->uf_endereco_pessoal               = $uf_endereco_pessoal;
        $analiseRascunho->tipo_endereco_pessoal             = $tipo_endereco_pessoal;
        $analiseRascunho->telefone1_numero                  = $telefone1_numero;
        $analiseRascunho->telefone1_tipo                    = $telefone1_tipo;
        $analiseRascunho->telefone1_ramal                   = $telefone1_ramal;
        $analiseRascunho->telefone2_numero                  = $telefone2_numero;
        $analiseRascunho->telefone2_tipo                    = $telefone2_tipo;
        $analiseRascunho->telefone2_ramal                   = $telefone2_ramal;
        $analiseRascunho->email_cliente                     = $email_cliente;
        $analiseRascunho->cliente_facebook                  = $cliente_facebook;
        $analiseRascunho->cliente_skype                     = $cliente_skype;
        $analiseRascunho->nome_completo_conjuge             = $nome_completo_conjuge;
        $analiseRascunho->cpf_do_conjuge                    = $cpf_do_conjuge;
        $analiseRascunho->tipo_de_documento_conjuge         = $tipo_de_documento_conjuge;
        $analiseRascunho->numero_do_documento_conjuge       = $numero_do_documento_conjuge;
        $analiseRascunho->orgao_emissor_documento_conjuge   = $orgao_emissor_documento_conjuge;
        $analiseRascunho->uf_emissor_documento_conjuge      = $uf_emissor_documento_conjuge;
        $analiseRascunho->data_emissao_documento_conjuge    = $data_emissao_documento_conjuge;
        $analiseRascunho->conjuge_nascimento                = $conjuge_nascimento;
        $analiseRascunho->sexo_conjuge                      = $sexo_conjuge;
        $analiseRascunho->nacionalidade_conjuge             = $nacionalidade_conjuge;
        $analiseRascunho->naturalidade_conjuge              = $naturalidade_conjuge;
        $analiseRascunho->naturalidade_conjuge_ext          = $naturalidade_conjuge_ext;
        $analiseRascunho->renda_liquida_conjuge             = $renda_liquida_conjuge;
        $analiseRascunho->mes_ano_renda_conjuge             = $mes_ano_renda_conjuge;
        $analiseRascunho->ocupacao_atual_conjuge            = $ocupacao_atual_conjuge;
        $analiseRascunho->profissao_conjuge                 = $profissao_conjuge;
        $analiseRascunho->tipo_de_comprovante_conjuge       = $tipo_de_comprovante_conjuge;
        $analiseRascunho->cep_conjuge                       = $cep_conjuge;
        $analiseRascunho->logradouro_conjuge                = $logradouro_conjuge;
        $analiseRascunho->cidade_conjuge                    = $cidade_conjuge;
        $analiseRascunho->bairro_conjuge                    = $bairro_conjuge;
        $analiseRascunho->numero_endereco_conjuge           = $numero_endereco_conjuge;
        $analiseRascunho->complemento_endereco_conjuge      = $complemento_endereco_conjuge;
        $analiseRascunho->uf_endereco_conjuge               = $uf_endereco_conjuge;
        $analiseRascunho->tipo_de_endereco_conjuge          = $tipo_de_endereco_conjuge;
        $analiseRascunho->telefone_conjuge                  = $telefone_conjuge;
        $analiseRascunho->tipo_de_telefone_conjuge          = $tipo_de_telefone_conjuge;
        $analiseRascunho->email_conjuge                     = $email_conjuge;
        $analiseRascunho->numero_conta                      = $numero_conta;
        $analiseRascunho->tipo_de_conta                     = $tipo_de_conta;
        $analiseRascunho->agencia_conta                     = $agencia_conta;
        $analiseRascunho->banco_conta                       = $banco_conta;
        $analiseRascunho->data_de_abertura_conta            = $data_de_abertura_conta;
        $analiseRascunho->classe_profissional               = $classe_profissional;
        $analiseRascunho->cpf_cnpj_profissional             = $cpf_cnpj_profissional;
        $analiseRascunho->empresa_profissional              = $empresa_profissional;
        $analiseRascunho->ocupacao_atual                    = $ocupacao_atual;
        $analiseRascunho->data_admissao                     = $data_admissao;
        $analiseRascunho->profissao                         = $profissao;
        $analiseRascunho->mes_ano_renda                     = $mes_ano_renda;
        $analiseRascunho->orgao_pagador                     = $orgao_pagador;
        $analiseRascunho->numero_do_beneficio               = $numero_do_beneficio;
        $analiseRascunho->tipo_de_comprovante               = $tipo_de_comprovante;
        $analiseRascunho->cep_endereco_profissional         = $cep_endereco_profissional;
        $analiseRascunho->logradouro_endereco_profissional  = $logradouro_endereco_profissional;
        $analiseRascunho->numero_endereco_profissional      = $numero_endereco_profissional;
        $analiseRascunho->cidade_endereco_profissional      = $cidade_endereco_profissional;
        $analiseRascunho->bairro_endereco_profissional      = $bairro_endereco_profissional;
        $analiseRascunho->uf_endereco_profissional          = $uf_endereco_profissional;
        $analiseRascunho->complemento_endereco_profissional = $complemento_endereco_profissional;
        $analiseRascunho->telefone_profissional             = $telefone_profissional;
        $analiseRascunho->ramal_telefone_profissional       = $ramal_telefone_profissional;
        $analiseRascunho->tipo_telefone_profissional        = $tipo_telefone_profissional;
        $analiseRascunho->email_telefone_profissional       = $email_telefone_profissional;
        $analiseRascunho->referencia1_nome                  = $referencia1_nome;
        $analiseRascunho->referencia1_tipo                  = $referencia1_tipo;
        $analiseRascunho->referencia1_telefone_numero       = $referencia1_telefone_numero;
        $analiseRascunho->referencia1_telefone_ramal        = $referencia1_telefone_ramal;
        $analiseRascunho->referencia2_nome                  = $referencia2_nome;
        $analiseRascunho->referencia2_tipo                  = $referencia2_tipo;
        $analiseRascunho->referencia2_telefone_numero       = $referencia2_telefone_numero;
        $analiseRascunho->referencia2_telefone_ramal        = $referencia2_telefone_ramal;
        $analiseRascunho->numero_do_pedido                  = $numero_do_pedido;
        $analiseRascunho->vendedor                          = $vendedor;
        $analiseRascunho->promotor                          = $promotor;
        $analiseRascunho->procedencia_da_compra             = $procedencia_da_compra;
        $analiseRascunho->valor                             = $valor;
        $analiseRascunho->entrada                           = $entrada;
        $analiseRascunho->alerta                            = $alerta;
        $analiseRascunho->mercadoria_retirada_na_hora       = $mercadoria_retirada_na_hora;
        $analiseRascunho->celular_pre_pago                  = $celular_pre_pago;
        $analiseRascunho->mercadoria                        = $mercadoria;
        $analiseRascunho->observacao                        = $observacao;
        $analiseRascunho->ultima_atualizacao                = date('Y-m-d H:i:s');

        $analiseRascunho->update();
    }

    public function actionBuscarCEP(){

        $cep = $_POST['cep'];

        $util = new Util;
        $util->buscarCEP($cep);
    }

    public function actionAnaliseResume(){


        $analise_de_credito = new AnaliseDeCredito;

        $nome = $_POST['pessoa_nome'];
        $cpf = $_POST['cpf'];
        $renda_liquida = $_POST['renda_liquida'];
        $isClient = $_POST['isClient'];
        $situacao_cadastral = $_POST['situacao_cadastral'];

        $valor_compra = $_POST['valor_compra'];
        $valor_entrada = $_POST['valor_entrada'];

        $num_parcelas = $_POST['num_parcelas_hidden'];
        $val_parcelas = $_POST['val_parcelas_hidden'];
        $val_total_hidden = $_POST['val_total_hidden'];
        $data_pri_par_hidden = $_POST['data_pri_par_hidden'];
        $data_ult_par_hidden = $_POST['data_ult_par_hidden'];
        $cotacao_id_hidden = $_POST['cotacao_id_hidden'];
        $carencia_hidden = $_POST['carencia_hidden'];
        $valor_financiado_hidden = $_POST['valor_financiado_hidden'];

        $cotacao = Cotacao::model()->findByPk( $cotacao_id_hidden );

        $analise_de_credito->data_cadastro = date('Y-m-d H:i:s');
        $analise_de_credito->numero_do_pedido = AnaliseDeCredito::model()->codeAnaliseGenerate();
        $analise_de_credito->valor = '';
        $analise_de_credito->entrada = '';

        $analise_de_credito->Cliente_id = ''; /*pegar no final*/
        $analise_de_credito->vendedor = ''; /*pegar no final*/
        $analise_de_credito->promotor = ''; /*pegar no final*/
        $analise_de_credito->Procedencia_compra_id = ''; /*pegar no final*/
        $analise_de_credito->mercadoria_retirada_na_hora = ''; /*pegar no final*/
        $analise_de_credito->observacao = ''; /*pegar no final*/
        $analise_de_credito->alerta = ''; /*pegar no final*/

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerScriptFile($baseUrl.'/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl.'/js/jquery.maskedinput.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.maskMoney.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/form-elements.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/daterangepicker.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/bootstrap-datepicker.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/bootstrap-timepicker.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/bootstrap-colorpicker.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.tagsinput.js',CClientScript::POS_END);


        $cs->registerScriptFile($baseUrl.'/js/summernote.min.js',  CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/ckeditor/ckeditor.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/ckeditor/adapter/jquery.js',CClientScript::POS_END);

        $cs->registerScriptFile($baseUrl.'/js/form-wizard.js',  CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.validate.min.js',  CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.smartWizard.js',  CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/select2.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.inputlimiter.1.3.1.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.autosize.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/update-select.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/bootstrap-fileupload.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/analise-upload-file-manager.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/fn-search-cep.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/fn-save-draft.js',CClientScript::POS_END);

        $cs->registerCssFile($baseUrl.'/css/sigax_template/bootstrap-fileupload.min.css');

        $cs->registerScript('MesAnoRenda','jQuery(function($){$("input.mes_ano_renda").mask("99/9999");});');
        $cs->registerScript('cep','jQuery(function($){$("input.cep").mask("99999999");});');
        $cs->registerScript('telefone','jQuery(function($){$("input.telefone").mask("(99)9999-9999");});');
        $cs->registerScript('dinheiro','jQuery("input.dinheiro").maskMoney({decimal:"."});');
        $cs->registerScript('ipt_date','jQuery(function($){$("input.ipt_date").mask("99/99/9999");});');
        $cs->registerScript('tempo_resi','jQuery(function($){$("#tempo_de_residencia").mask("99/9999");});');

        $cs->registerScript('FormWizard','FormWizard.init();');
        $cs->registerScript('FormElements','FormElements.init();');

        $analiseRascunho                            = new AnaliseRascunho;
        $analiseRascunho->Usuario_id                = Yii::app()->session['usuario']->id;
        $analiseRascunho->nome_completo             = $nome;
        $analiseRascunho->cpf                       = $cpf;
        $analiseRascunho->situacao_cadastral_cpf    = $situacao_cadastral;
        $analiseRascunho->renda_liquida             = $renda_liquida;
        $analiseRascunho->valor_solicitado          = $valor_compra;
        $analiseRascunho->valor_entrada             = $valor_entrada;
        $analiseRascunho->cotacao_id                = $cotacao->id;
        $analiseRascunho->ultima_atualizacao = date('Y-m-d H:i:s');
        $analiseRascunho->save();

        $this->render( 'analise_cadastro', array(
            'nome' => $nome,
            'cpf' => $cpf,
            'renda_liquida' => $renda_liquida,
            'isClient' => $isClient,
            'valor_compra' => $valor_compra,
            'valor_entrada'=>$valor_entrada,
            'num_parcelas'=>$num_parcelas,
            'val_parcelas'=>$val_parcelas,
            'val_total_hidden'=>$val_total_hidden,
            'data_pri_par_hidden' => $data_pri_par_hidden,
            'data_ult_par_hidden' => $data_ult_par_hidden,
            'cotacao' => $cotacao,
            'carencia'=> $carencia_hidden,
            'valor_financiado_hidden'=> $valor_financiado_hidden,
            'rascunho'=> $analiseRascunho,
        ));
    }

    public function actionAnaliseResumeCliente(){


        $clienteId                  = $_POST['clienteId'];

        $cliente                    = Cliente::model()->findByPk( $clienteId );

        $baseUrl                    = Yii::app()->baseUrl;
        $cs                         = Yii::app()->getClientScript();

        $cs->registerScriptFile($baseUrl . '//js/jquery.min.js');
        $cs->registerScriptFile($baseUrl.'/js/form-wizard-analise-cliente.js',      CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '//js/jquery.validate.min.js',              CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '//js/jquery.smartWizard.js',               CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '//js/jquery.maskedinput.js',               CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '//js/jquery.maskMoney.js',                 CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '//js/form-elements.js',                    CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '//js/select2.min.js',                      CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '//js/jquery.inputlimiter.1.3.1.min.js',    CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '//js/jquery.autosize.min.js',              CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '//js/update-select.js',                    CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '//js/bootstrap-datepicker.js',             CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js',                    CClientScript::POS_END);

        $cs->registerScript('FormWizard','FormWizard.init();');
        $cs->registerScript('FormElements','FormElements.init();');

        $cs->registerScript('MesAnoRenda','jQuery(function($){$("input.mes_ano_renda").mask("99/9999");});');
        $cs->registerScript('cep','jQuery(function($){$("input.cep").mask("99999999");});');
        $cs->registerScript('telefone','jQuery(function($){$("input.telefone").mask("(99)9999-9999");});');
        $cs->registerScript('dinheiro','$("input.dinheiro").maskMoney({decimal:",", thousands:"."});');
        $cs->registerScript('ipt_date','jQuery(function($){$("input.ipt_date").mask("99/99/9999");});');
        $cs->registerScript('tempo_resi','jQuery(function($){$("#tempo_de_residencia").mask("99/9999");});');

        //$cs->registerScript('dinheiro','jQuery("input.dinheiro").maskMoney({decimal:"."});');


        $valor_compra               = $_POST['valor_compra'];
        $valor_entrada              = $_POST['valor_entrada'];
        $num_parcelas               = $_POST['num_parcelas_hidden'];
        $val_parcelas               = $_POST['val_parcelas_hidden'];
        $val_total_hidden           = $_POST['val_total_hidden'];
        $data_pri_par_hidden        = $_POST['data_pri_par_hidden'];
        $data_ult_par_hidden        = $_POST['data_ult_par_hidden'];
        $cotacao_id_hidden          = $_POST['cotacao_id_hidden'];
        $carencia_hidden            = $_POST['carencia_hidden'];
        $valor_financiado_hidden    = $_POST['valor_financiado_hidden'];

        $cotacao = TabelaCotacao::model()->findByPk( $cotacao_id_hidden );

        $this->render( 'analise_cadastro_cliente', array(
            'nome'                      => $cliente->pessoa->nome,
            'clienteId'                 => $cliente->id,
            'cpf'                       => $cliente->pessoa->getCpf()->numero,
            'renda_liquida'             => $cliente->pessoa->getDadosProfissionais()->renda_liquida,
            'isClient'                  => "Sim",
            'valor_compra'              => $valor_compra,
            'valor_entrada'             => $valor_entrada,
            'num_parcelas'              => $num_parcelas,
            'val_parcelas'              => $val_parcelas,
            'val_total_hidden'          => $val_total_hidden,
            'data_pri_par_hidden'       => $data_pri_par_hidden,
            'data_ult_par_hidden'       => $data_ult_par_hidden,
            'cotacao'                   => $cotacao,
            'carencia'                  => $carencia_hidden,
            'valor_financiado_hidden'   => $valor_financiado_hidden,

        ));
    }

    public function actionDoAnalise(){

        $this->layout = '//layouts/blank_template';

        $util = new Util;
        $filial = Yii::app()->session['usuario']->returnFilial();

        $pessoa             = new Pessoa;
        $cliente            = new Cliente;
        $cadastro           = new Cadastro;//cadastro_has_filial
        $documentoCliente   = new Documento;
        $CPFCliente         = new Documento;
        $dadosBancarios     = new DadosBancarios; //Pessoa_id, Tipo_Conta_Bancaria_id, Banco_id
        $enderecoCliente    = new Endereco;//Pessoa_has_Endereco
        $enderecoConjuge    = new Endereco;//Pessoa_has_Endereco
        $dadosProfissionais = new DadosProfissionais;//Pessoa_id, Endereco_id, Contato_id
        $enderecoProfiss    = new Endereco;
        $contatoProfiss     = new Contato;//Contato_has_Email, Contato_has_telefone
        $emailProfiss       = new Email;
        $telefoneProfiss    = new Telefone;
        $contatoCliente     = new Contato;//Contato_has_Email, Contato_has_telefone
        $telefoneCliente    = new Telefone;
        $telefoneCliente2   = new Telefone;
        $emailCliente       = new Email;
        $referencia1        = new Referencia;
        $referencia2        = new Referencia;
        $contatoRef1        = new Contato;
        $contatoRef2        = new Contato;
        $telefoneRef1       = new Telefone;
        $telefoneRef2       = new Telefone;
        $analiseDeCredito   = new AnaliseDeCredito;//Cliente_id,

        $cotacaoEscolhida   = Cotacao::model()->findByPk( $_POST['Proposta_id'] );

        if ( !empty( $_POST['ContatoCliente'] ) ) {

            $contatoCliente->attributes = $_POST['ContatoCliente'];
            $contatoCliente->data_cadastro = date('Y-m-d H:i:s');
            $contatoCliente->data_cadastro_br = date('d/m/Y H:i:s');

            //Salvando o contato do Cliente... EMAIL, TELEFONES...
            if ( $contatoCliente->save() ) {

                if ( !empty( $_POST['EmailCliente'] ) ){

                    $emailCliente->data_cadastro = date('Y-m-d H:i:s');
                    $emailCliente->data_cadastro_br = date('d/m/Y H:i:s');

                    if ( $emailCliente->save() ) {

                        $contatoHasEmail = new ContatoHasEmail;
                        $contatoHasEmail->Email_id = $emailCliente->id;
                        $contatoHasEmail->Contato_id = $contatoCliente->id;
                        $contatoHasEmail->data_cadastro = date('Y-m-d H:i:s');
                        $contatoHasEmail->data_cadastro_br = date('d/m/Y H:i:s');
                        $contatoHasEmail->save();

                    }
                }

                if ( !empty( $_POST['TelefoneCliente'] ) ) {

                    $telefoneCliente->attributes = $_POST['TelefoneCliente'];
                    $telefoneCliente->data_cadastro = date('Y-m-d H:i:s');
                    $telefoneCliente->data_cadastro_br = date('d/m/Y H:i:s');

                    if ( $telefoneCliente->save() ) {

                        $contatoHasTelefone = new ContatoHasTelefone;
                        $contatoHasTelefone->Telefone_id = $telefoneCliente->id;
                        $contatoHasTelefone->Contato_id = $contatoCliente->id;
                        $contatoHasTelefone->data_cadastro = date('Y-m-d H:i:s');
                        $contatoHasTelefone->data_cadastro_br = date('d/m/Y H:i:s');
                        $contatoHasTelefone->save();
                    }

                }

                if ( !empty( $_POST['TelefoneCliente2'] ) ) {

                    $telefoneCliente2->attributes = $_POST['TelefoneCliente2'];
                    $telefoneCliente2->data_cadastro = date('Y-m-d H:i:s');
                    $telefoneCliente2->data_cadastro_br = date('d/m/Y H:i:s');

                    if ( $telefoneCliente2->save() ) {

                        $contatoHasTelefone = new ContatoHasTelefone;
                        $contatoHasTelefone->Telefone_id = $telefoneCliente2->id;
                        $contatoHasTelefone->Contato_id = $contatoCliente->id;
                        $contatoHasTelefone->data_cadastro = date('Y-m-d H:i:s');
                        $contatoHasTelefone->data_cadastro_br = date('d/m/Y H:i:s');
                        $contatoHasTelefone->save();
                    }
                }
            }

            $pessoa->attributes = $_POST['Pessoa'];
            $pessoa->Contato_id = $contatoCliente->id;
            $pessoa->data_cadastro = date('Y-m-d H:i:s');
            $pessoa->data_cadastro_br = date('d/m/Y H:i:s');
            $pessoa->nacionalidade = $_POST['Pessoa']['nacionalidade'];
            $pessoa->nascimento = $util->view_date_to_bd( $_POST['Pessoa']['nascimento'] );

            if ( $pessoa->nacionalidade == 'Brasileira' )
                $pessoa->naturalidade =  $_POST['naturalidade_br'];
            else
                $pessoa->naturalidade =  $_POST['naturalidade_ext'];

            /*Salvando a pessoa*/
            if ( $pessoa->save() ) {

                /*Dados bancários*/
                if ( !empty( $_POST['DadosBancarios'] ) ) {

                    $dadosBancarios->attributes = $_POST['DadosBancarios'];
                    $dadosBancarios->data_cadastro = date('Y-m-d H:i:s');
                    $dadosBancarios->data_cadastro_br = date('d/m/Y H:i:s');

                    if ( $dadosBancarios->save() ) {

                        $pessoaHasDadosBancarios = new PessoaHasDadosBancarios;
                        $pessoaHasDadosBancarios->Pessoa_id = $pessoa->id;
                        $pessoaHasDadosBancarios->habilitado = 1;
                        $pessoaHasDadosBancarios->Dados_Bancarios_id = $dadosBancarios->id;
                        $pessoaHasDadosBancarios->data_cadastro = date('Y-m-d H:i:s');
                        $pessoaHasDadosBancarios->save();
                    }
                }/*FIM dados bancários*/


                /*Dados profissionais*/
                if ( !empty( $_POST['DadosProfissionais'] ) ) {

                    $dadosProfissionais->attributes = $_POST['DadosProfissionais'];
                    $dadosProfissionais->data_admissao = $util->view_date_to_bd( $_POST['DadosProfissionais']['data_admissao'] );
                    $dadosProfissionais->Pessoa_id  = $pessoa->id;
                    $dadosProfissionais->data_cadastro = date('Y-m-d H:i:s');
                    $dadosProfissionais->data_cadastro_br = date('d/m/Y H:i:s');

                    if ( $dadosProfissionais->Classe_Profissional_id == 1 ) {
                        $dadosProfissionais->aposentado = 1;
                        $dadosProfissionais->pensionista = 1;
                    }

                    else{
                        $dadosProfissionais->aposentado = 0;
                        $dadosProfissionais->pensionista = 0;
                    }

                    /*Endereço profissional*/
                    if ( !empty($_POST['EnderecoProfiss']) ) {

                        $enderecoProfiss->attributes = $_POST['EnderecoProfiss'];
                        $enderecoProfiss->data_cadastro = date('Y-m-d H:i:s');
                        $enderecoProfiss->data_cadastro_br = date('d/m/Y H:i:s');
                        $enderecoProfiss->Tipo_Endereco_id = 1;

                        if ( $enderecoProfiss->save() ) {
                            $dadosProfissionais->Endereco_id = $enderecoProfiss->id;
                        }
                    }


                    /*Contato profissional*/
                    $contatoProfiss->data_cadastro = date('Y-m-d H:i:s');
                    $contatoProfiss->data_cadastro_br = date('d/m/Y H:i:s');

                    if ( !empty( $_POST['EmailProfiss'] ) ) {

                        $emailProfiss->attributes = $_POST['EmailProfiss'];
                        $emailProfiss->data_cadastro = date('Y-m-d H:i:s');
                        $emailProfiss->data_cadastro_br = date('d/m/Y H:i:s');
                    }

                    if ( !empty( $_POST['TelefoneProfiss'] ) ) {

                        $telefoneProfiss->attributes = $_POST['TelefoneProfiss'];
                        $telefoneProfiss->data_cadastro = date('Y-m-d H:i:s');
                        $telefoneProfiss->data_cadastro_br = date('d/m/Y H:i:s');
                    }

                    if ( $contatoProfiss->save() ) {

                        $dadosProfissionais->Contato_id = $contatoProfiss->id;

                        if ( $emailProfiss->save() ) {

                            $contatoProfissHasEmail = new ContatoHasEmail;
                            $contatoProfissHasEmail->data_cadastro = date('Y-m-d H:i:s');
                            $contatoProfissHasEmail->data_cadastro_br = date('d/m/Y H:i:s');
                            $contatoProfissHasEmail->Email_id = $emailProfiss->id;
                            $contatoProfissHasEmail->Contato_id = $contatoProfiss->id;
                            $contatoProfissHasEmail->save();
                        }

                        if ( $telefoneProfiss->save() ) {

                            $contatoProfissHasTelefone = new ContatoHasTelefone;
                            $contatoProfissHasTelefone->data_cadastro = date('Y-m-d H:i:s');
                            $contatoProfissHasTelefone->data_cadastro_br = date('d/m/Y H:i:s');
                            $contatoProfissHasTelefone->Telefone_id = $telefoneProfiss->id;
                            $contatoProfissHasTelefone->Contato_id = $contatoProfiss->id;
                            $contatoProfissHasTelefone->save();
                        }
                    }

                    $dadosProfissionais->save();

                }/*FIM Dados profissionais*/


                /*Documentos....*/
                if ( !empty( $_POST['DocumentoCliente'] ) ) {

                    $documentoCliente->attributes = $_POST['DocumentoCliente'];
                    $documentoCliente->data_emissao = $util->view_date_to_bd( $_POST['DocumentoCliente']['data_emissao'] );
                    $documentoCliente->data_cadastro = date('Y-m-d H:i:s');
                    $documentoCliente->data_cadastro_br = date('d/m/Y H:i:s');

                    if ( $documentoCliente->save() ) {

                        $clinteHasDocumento = new PessoaHasDocumento;
                        $clinteHasDocumento->Documento_id = $documentoCliente->id;
                        $clinteHasDocumento->Pessoa_id = $pessoa->id;
                        $clinteHasDocumento->data_cadastro = date('Y-m-d H:i:s');
                        $clinteHasDocumento->data_cadastro_br = date('d/m/Y H:i:s');
                        $clinteHasDocumento->save();
                        /*Salvando o documento adicional do cliente*/
                    }
                }

                if ( !empty( $_POST['CPFCliente'] ) ) {

                    /*Salvando o CPF do cliente*/
                    $CPFCliente->attributes = $_POST['CPFCliente'];
                    $CPFCliente->Tipo_documento_id = 1;
                    $CPFCliente->data_cadastro = date('Y-m-d H:i:s');
                    $CPFCliente->Tipo_documento_id = 1;

                    if ( $CPFCliente->save() ) {
                        $clinteHasCPF = new PessoaHasDocumento;
                        $clinteHasCPF->Documento_id = $CPFCliente->id;
                        $clinteHasCPF->Pessoa_id = $pessoa->id;
                        $clinteHasCPF->data_cadastro = date('Y-m-d H:i:s');
                        $clinteHasCPF->data_cadastro_br = date('d/m/Y H:i:s');
                        $clinteHasCPF->save();

                    }
                }/*FIM Documentos...*/


                /*Endereço Cliente..*/
                if ( !empty( $_POST['EnderecoCliente'] ) ) {

                    $enderecoCliente->attributes = $_POST['EnderecoCliente'];
                    $enderecoCliente->tempo_de_residencia = $_POST['EnderecoCliente']['tempo_de_residencia'];
                    $enderecoCliente->data_cadastro = date('Y-m-d H:i:s');
                    $enderecoCliente->data_cadastro_br = date('d/m/Y H:i:s');

                    if ( $enderecoCliente->save() ) {

                        $clienteHasEndereco = new PessoaHasEndereco;
                        $clienteHasEndereco->Pessoa_id = $pessoa->id;
                        $clienteHasEndereco->Endereco_id = $enderecoCliente->id;
                        $clienteHasEndereco->data_cadastro = date('Y-m-d H:i:s');
                        $clienteHasEndereco->data_cadastro_br = date('d/m/Y H:i:s');
                        $clienteHasEndereco->save();

                    }
                }/*FIM Endereço Cliente*/


                /*Cliente...*/
                $cliente->Pessoa_id = $pessoa->id;

                if ( $cliente->save() ) {

                    /*Cadastro*/
                    if ( !empty( $_POST['Cadastro'] ) ) {

                        $cadastro->attributes = $_POST['Cadastro'];
                        $cadastro->Cliente_id = $cliente->id;
                        $cadastro->data_cadastro = date('Y-m-d H:i:s');
                        $cadastro->data_cadastro_br = date('d/m/Y H:i:s');

                        if ( $cadastro->save() ) {

                            if ( !empty( $_POST['descricoes'] ) ) {
                                $descricoes = $_POST['descricoes'];
                            }

                            $_UPCONFIG['dir'] = '/var/www/sigac/uploads/';

                            if ( !empty( $_FILES['arquivos'] ) ) {

                                $anexos = $_FILES['arquivos'];
                                $tamanho = sizeof($anexos['name']);

                                for ($i = 0; $i < $tamanho; $i++) {

                                    if ( move_uploaded_file( $anexos['tmp_name'][$i], $_UPCONFIG['dir'].$anexos['name'][$i] ) ) {

                                        $anexo = new Anexo;
                                        $anexo->descricao = $descricoes[$i];
                                        $anexo->relative_path = '/uploads/'.$anexos['name'][$i];
                                        $anexo->absolute_path = $_UPCONFIG['dir'].$anexos['name'][$i];
                                        $anexo->entidade_relacionamento = 7;
                                        $anexo->id_entidade_relacionamento = $cadastro->id;
                                        $anexo->habilitado = 1;
                                        $anexo->data_cadastro = date('Y-m-d H:i:s');
                                        $anexo->save();
                                    }
                                }
                            }
                        }

                        $cadastroHasFilial = new CadastroHasFilial;
                        $cadastroHasFilial->Filial_id = $filial->id;
                        $cadastroHasFilial->Cadastro_id = $cadastro->id;
                        $cadastroHasFilial->data_cadastro = date('Y-m-d H:i:s');
                        $cadastroHasFilial->data_cadastro_br = date('d/m/Y H:i:s');

                        $cadastroHasFilial->save();
                    }/*FIM Cadastro*/

                    /*Referencias...*/
                    if ( !empty( $_POST['Referencia1'] ) ) {

                        $referencia1->attributes = $_POST['Referencia1'];
                        $referencia1->data_cadastro = date('Y-m-d H:i:s');
                        $referencia1->data_cadastro_br = date('d/m/Y H:i:s');
                        $referencia1->Cliente_id = $cliente->id;

                        if ( !empty( $_POST['TelefoneRef1'] ) ) {

                            $telefoneRef1->attributes = $_POST['TelefoneRef1'];
                            $telefoneRef1->data_cadastro = date('Y-m-d H:i:s');
                            $telefoneRef1->data_cadastro_br = date('d/m/Y H:i:s');
                            $telefoneRef1->Tipo_Telefone_id = 3;

                            $contatoRef1->data_cadastro = date('Y-m-d H:i:s');
                            $contatoRef1->data_cadastro_br = date('d/m/Y H:i:s');

                            if ( $contatoRef1->save() ) {

                                $referencia1->Contato_id = $contatoRef1->id;

                                if ( $telefoneRef1->save() ) {

                                    $contatoRef1HasTelefone = new ContatoHasTelefone;
                                    $contatoRef1HasTelefone->data_cadastro = date('Y-m-d H:i:s');
                                    $contatoRef1HasTelefone->data_cadastro_br = date('d/m/Y H:i:s');
                                    $contatoRef1HasTelefone->Contato_id = $contatoRef1->id;
                                    $contatoRef1HasTelefone->Telefone_id = $telefoneRef1->id;
                                    $contatoRef1HasTelefone->save();

                                    $referencia1->save();
                                }
                            }
                        }
                    }

                    if ( !empty( $_POST['Referencia2'] ) ) {

                        $referencia2->attributes = $_POST['Referencia2'];
                        $referencia2->data_cadastro = date('Y-m-d H:i:s');
                        $referencia2->data_cadastro_br = date('d/m/Y H:i:s');
                        $referencia2->Cliente_id = $cliente->id;

                        if ( !empty( $_POST['TelefoneRef2'] ) ) {

                            $telefoneRef2->attributes = $_POST['TelefoneRef2'];
                            $telefoneRef2->data_cadastro = date('Y-m-d H:i:s');
                            $telefoneRef2->data_cadastro_br = date('d/m/Y H:i:s');
                            $telefoneRef2->Tipo_Telefone_id = 3;

                            $contatoRef2->data_cadastro = date('Y-m-d H:i:s');
                            $contatoRef2->data_cadastro_br = date('d/m/Y H:i:s');

                            if ( $contatoRef2->save() ) {

                                $referencia2->Contato_id = $contatoRef2->id;

                                if ( $telefoneRef2->save() ) {

                                    $contatoRef2HasTelefone = new ContatoHasTelefone;
                                    $contatoRef2HasTelefone->data_cadastro = date('Y-m-d H:i:s');
                                    $contatoRef2HasTelefone->data_cadastro_br = date('d/m/Y H:i:s');
                                    $contatoRef2HasTelefone->Contato_id = $contatoRef2->id;
                                    $contatoRef2HasTelefone->Telefone_id = $telefoneRef2->id;
                                    $contatoRef2HasTelefone->save();

                                    $referencia2->save();
                                }
                            }
                        }
                    }/*FIM Referencias...*/


                    /*Dados Cônjuge*/
                    if ( $cadastro->conjugue_compoe_renda == '1') {

                        $familiar = new Familiar;//OK
                        $conjuge = new Pessoa;//OK
                        $contatoConjuge = new Contato;//OK
                        $telefoneConjuge = new Telefone;//OK
                        $emailConjuge = new Email;//OK
                        $enderecoConjuge = new Endereco;//OK
                        $docConjuge = new Documento;//Pessoa_has_Documento
                        $cpfConjuge = new Documento;//Pessoa_has_Documento
                        $dadosProfissionaisConjuge = new DadosProfissionais;//Pessoa_id

                        $familiar->Tipo_Familiar_id = 2;
                        $familiar->Cliente_id = $cliente->id;

                        $contatoConjuge->data_cadastro = date('Y-m-d H:i:s');
                        $contatoConjuge->data_cadastro_br = date('d/m/Y H:i:s');

                            /*Contato conjuge*/
                        if ( $contatoConjuge->save() ) {

                            if ( !empty( $_POST['EmailConjuge'] ) ){

                                $emailConjuge->attributes = $_POST['EmailConjuge'];
                                $emailConjuge->data_cadastro = date('Y-m-d H:i:s');
                                $emailConjuge->data_cadastro_br = date('d/m/Y H:i:s');

                                if ( $emailConjuge->save() ) {

                                    $contatoConjHasEmail = new ContatoHasEmail;
                                    $contatoConjHasEmail->Email_id = $emailConjuge->id;
                                    $contatoConjHasEmail->Contato_id = $contatoConjuge->id;
                                    $contatoConjHasEmail->data_cadastro = date('Y-m-d H:i:s');
                                    $contatoConjHasEmail->data_cadastro_br = date('d/m/Y H:i:s');
                                    $contatoConjHasEmail->save();
                                }

                                $telefoneConjuge->attributes = $_POST['TelefoneConjuge'];
                                $telefoneConjuge->data_cadastro = date('Y-m-d H:i:s');
                                $telefoneConjuge->data_cadastro_br = date('d/m/Y H:i:s');

                                if ( $telefoneConjuge->save() ) {

                                    $contatoConjHasTelefone = new ContatoHasTelefone;
                                    $contatoConjHasTelefone->Telefone_id = $telefoneConjuge->id;
                                    $contatoConjHasTelefone->Contato_id = $contatoConjuge->id;
                                    $contatoConjHasTelefone->data_cadastro = date('Y-m-d H:i:s');
                                    $contatoConjHasTelefone->data_cadastro_br = date('d/m/Y H:i:s');
                                    $contatoConjHasTelefone->save();

                                }
                            }
                        }
                        /*FIm Contato Conjuge*/

                        if ( !empty( $_POST['Conjuge'] ) ) {

                            $conjuge->attributes = $_POST['Conjuge'];
                            $conjuge->estado_civil = 'Casado';
                            $conjuge->data_cadastro = date('Y-m-d H:i:s');
                            $conjuge->data_cadastro_br = date('d/m/Y H:i:s');
                            $conjuge->Contato_id = $contatoConjuge->id;
                            $conjuge->nacionalidade = $_POST['Conjuge']['nacionalidade'];
                            $conjuge->nascimento = $util->view_date_to_bd( $_POST['Conjuge']['nascimento'] );

                            if ( $conjuge->nacionalidade == 'Brasileira' )
                                $conjuge->naturalidade =  $_POST['naturalidade_con_br'];
                            else
                                $conjuge->naturalidade =  $_POST['naturalidade_con_ext'];

                            /*Salvando Pessoa Conjuge*/
                            if ( $conjuge->save() )
                            {

                                $conjCliente = new Cliente;
                                $conjCliente->Pessoa_id = $conjuge->id;
                                $conjCliente->habilitado = 1;

                                if ( $conjCliente->save() )
                                {
                                    $cadastroConj = new Cadastro;
                                    $cadastroConj->Cliente_id = $conjCliente->id;
                                    $cadastroConj->data_cadastro = date('Y-m-d H:i:s');
                                    $cadastroConj->data_cadastro_br = date('d/m/Y');


                                    if ( $cadastroConj->save() ) {

                                        $cadastroHasFilialConj = new CadastroHasFilial;
                                        $cadastroHasFilialConj->Filial_id = $filial->id;
                                        $cadastroHasFilialConj->Cadastro_id = $cadastroConj->id;
                                        $cadastroHasFilialConj->data_cadastro = date('Y-m-d H:i:s');
                                        $cadastroHasFilialConj->data_cadastro_br = date('d/m/Y H:i:s');
                                        $cadastroHasFilialConj->save();
                                    }

                                }

                                $familiar->Pessoa_id = $conjuge->id;
                                $familiar->save();

                                $conjugeHasEndereco = new PessoaHasEndereco;
                                $conjugeHasEndereco->data_cadastro = date('Y-m-d H:i:s');
                                $conjugeHasEndereco->Pessoa_id = $conjuge->id;

                                /*Endereço Conjuge*/
                                if ( !empty($_POST['EnderecoConjuge']) && $_POST['MesmoEndereco'] == 0 ){

                                    $enderecoConjuge->attributes = $_POST['EnderecoConjuge'];
                                    $enderecoConjuge->data_cadastro = date('Y-m-d H:i:s');

                                    if ( $enderecoConjuge->save() ){

                                        $conjugeHasEndereco->Endereco_id = $enderecoConjuge->id;
                                    }
                                }

                                else{
                                    $conjugeHasEndereco->Endereco_id = $enderecoCliente->id;
                                }

                                $conjugeHasEndereco->save();

                                /*FIM Endereço conjuge*/

                                /*Documentos Conjuge*/
                                if ( !empty( $_POST['CPF_Conjuge'] ) ) {

                                    $cpfConjuge->attributes = $_POST['CPF_Conjuge'];
                                    $cpfConjuge->data_cadastro = date('Y-m-d H:i:s');
                                    $cpfConjuge->data_cadastro_br = date('m/d/Y');
                                    $cpfConjuge->Tipo_documento_id = 1;

                                    if ( $cpfConjuge->save() ) {

                                        $conjugeHasCPF = new PessoaHasDocumento;
                                        $conjugeHasCPF->data_cadastro = date('Y-m-d H:i:s');
                                        $conjugeHasCPF->Documento_id = $cpfConjuge->id;
                                        $conjugeHasCPF->Pessoa_id = $conjuge->id;
                                        $conjugeHasCPF->save();
                                    }

                                }

                                if (!empty($_POST['Doc_Conjuge'])) {

                                    $docConjuge->attributes = $_POST['Doc_Conjuge'];
                                    $docConjuge->data_cadastro = date('Y-m-d H:i:s');
                                    $docConjuge->data_cadastro_br = date('m/d/Y');

                                    if ( $docConjuge->save() ) {
                                        $conjugeHasDocumento = new PessoaHasDocumento;
                                        $conjugeHasDocumento->data_cadastro = date('Y-m-d H:i:s');
                                        $conjugeHasDocumento->Documento_id = $docConjuge->id;
                                        $conjugeHasDocumento->Pessoa_id = $conjuge->id;
                                        $conjugeHasDocumento->save();
                                    }
                                }
                                /*FIM Documentos Conjuge*/

                                /*Dados profissionais Conjuge*/
                                if ( !empty($_POST['DadosProfissionaisConjuge']) ) {

                                    $dadosProfissionaisConjuge->attributes = $_POST['DadosProfissionaisConjuge'];
                                    $dadosProfissionaisConjuge->Pessoa_id = $conjuge->id;
                                    $dadosProfissionaisConjuge->data_cadastro = date('Y-m-d H:i:s');
                                    $dadosProfissionaisConjuge->data_cadastro_br = date('d-m-Y');
                                    $dadosProfissionaisConjuge->save();

                                }
                                /*FIM Dados profissionais Conjuge*/
                            }
                            /*FIM Salvando Pessoa Conjuge*/



                        }/*fim isset(conjuge)*/

                    }
                    /*FIM Cônjuge*/

                    /*Analise de crédito*/
                    if ( !empty( $_POST['AnaliseDeCredito'] ) ) {

                        $codigo = str_replace("-", "", substr($filial->cnpj, 11));
                        $codigo .= Yii::app()->session['usuario']->id;
                        $codigo .= date('dmyhis');

                        $analiseDeCredito->attributes = $_POST['AnaliseDeCredito'];
                        $analiseDeCredito->codigo = $codigo;
                        $analiseDeCredito->data_cadastro = date('Y-m-d H:i:s');
                        $analiseDeCredito->data_cadastro_br = date('d/m/Y H:i:s');
                        $analiseDeCredito->Cliente_id = $cliente->id;
                        $analiseDeCredito->Filial_id = $filial->id;
                        $analiseDeCredito->Usuario_id = Yii::app()->session['usuario']->id;

                        if ( $analiseDeCredito->save() ) {

                            $rascunho_id        = $_POST['Rascunho_id'];

                            $analiseRascunho    = AnaliseRascunho::model()->findByPk($rascunho_id);
                            $analiseRascunho->delete();

                            foreach ( $cotacaoEscolhida->listFinanceiras() as $fin ) {

                                $proposta = new Proposta;
                                $proposta->attributes = $_POST['Proposta'];

                                $proposta->valor_texto = $proposta->valor;
                                $proposta->valor_parcela_texto = $proposta->valor_parcela;

                                $proposta->Analise_de_Credito_id = $analiseDeCredito->id;
                                $proposta->codigo = $codigo .'1';
                                $proposta->Financeira_id = $fin->Financeira_id;
                                $proposta->data_cadastro = date('Y-m-d H:i:s');
                                $proposta->data_cadastro_br = date('d/m/Y H:i:s');
                                $proposta->Status_Proposta_id = 4;
                                $proposta->valor_parcela = floatval($proposta->valor_parcela);
                                $proposta->valor_entrada = floatval($proposta->valor_entrada);
                                $proposta->valor_final = floatval($proposta->valor_final);

                                $proposta->save();
                            }

                            for ($i = 0; $i < sizeof( $_POST['Bens'] ) ; $i++) {

                                $itemDaAnalise = new ItemDaAnalise;
                                $itemDaAnalise->Analise_de_Credito_id = $analiseDeCredito->id;
                                $itemDaAnalise->Grupo_de_Produto_id = $_POST['Bens'][$i];
                                $itemDaAnalise->data_cadastro = date('Y-m-d H:i:s');
                                $itemDaAnalise->data_cadastro_br = date('d/m/Y H:i:s');

                                $itemDaAnalise->save();
                            }

                            $this->redirect('/proposta/minhasPropostas/');
                        }
                    }

                    /*FIM Analise de crédito*/
                }
            }
        }

        $idCliente = $_POST['idCliente'];
        $cliente = Cliente::model()->findByPk( $idCliente );
    }

    public function actionDoAnaliseCliente(){

        $this->layout = '//layouts/blank_template';

        $idCliente = $_POST['clienteId'];

        $cliente = Cliente::model()->findByPk( $idCliente );

        $util = new Util;
        $filial = Yii::app()->session['usuario']->returnFilial();

        $analiseDeCredito = new AnaliseDeCredito;

        if ( !empty( $_POST['AnaliseDeCredito'] ) )
        {

            $codigo                                 = str_replace( "-", "", substr( $filial->cnpj, 11 ) );
            $codigo                                 .= Yii::app()->session['usuario']->id;
            $codigo                                 .= date('dmyhis');

            $_POST['AnaliseDeCredito']['valor']     = $util->moedaViewToBD( $_POST['AnaliseDeCredito']['valor'] );
            $_POST['AnaliseDeCredito']['entrada']     = $util->moedaViewToBD( $_POST['AnaliseDeCredito']['entrada'] );

            $analiseDeCredito->attributes           = $_POST['AnaliseDeCredito'];
            $analiseDeCredito->codigo               = $codigo;
            $analiseDeCredito->data_cadastro        = date('Y-m-d H:i:s');
            $analiseDeCredito->data_cadastro_br     = date('d/m/Y H:i:s');
            $analiseDeCredito->Cliente_id           = $cliente->id;
            $analiseDeCredito->Filial_id            = $filial->id;
            $analiseDeCredito->Usuario_id           = Yii::app()->session['usuario']->id;
            $analiseDeCredito->observacao           = "";
            $analiseDeCredito->mercadoria           = $_POST['AnaliseDeCredito']['mercadoria'];

            $cotacaoEscolhida = TabelaCotacao::model()->findByPk( $_POST['Proposta_id'] );

            if ( $analiseDeCredito->save() ) {

                $sigacLog                               = new SigacLog;
                $sigacLog->saveLog('Cadastro de análise','AnaliseDeCredito', $analiseDeCredito->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Usuario ". Yii::app()->session['usuario']->username . " cadastrou a análise " . $analiseDeCredito->codigo, "127.0.0.1",NULL, Yii::app()->session->getSessionId() );

                foreach ( $cotacaoEscolhida->listFinanceiras() as $fin ) {

                    $proposta                           = new Proposta;
                    $proposta->attributes               = $_POST['Proposta'];
                    $proposta->valor_texto              = $proposta->valor;
                    $proposta->valor_parcela_texto      = $proposta->valor_parcela;
                    $proposta->Analise_de_Credito_id    = $analiseDeCredito->id;
                    $proposta->codigo                   = $codigo .'1';
                    $proposta->Financeira_id            = $fin->Financeira_id;
                    $proposta->data_cadastro            = date('Y-m-d H:i:s');
                    $proposta->data_cadastro_br         = date('d/m/Y H:i:s');
                    $proposta->Status_Proposta_id       = 4;
                    $proposta->valor_parcela            = floatval($proposta->valor_parcela);
                    $proposta->valor_entrada            = floatval($proposta->valor_entrada);
                    $proposta->valor_final              = floatval($proposta->valor_final);

                    $proposta->save();


                    $dialogo                                = new Dialogo;
                    $dialogo->assunto                       = "Proposta ".$proposta->codigo;
                    $dialogo->data_criacao                  = date('Y-m-d H:i:s');
                    $dialogo->habilitado                    = 1;
                    $dialogo->Entidade_Relacionamento       = 'Proposta';
                    $dialogo->Entidade_Relacionamento_id    = $proposta->id;
                    $dialogo->criado_por                    = Yii::app()->session['usuario']->id;

                    if( $dialogo->save() )
                    {
                        if( isset( $_POST['AnaliseDeCredito']['observacao'] ) && !empty( $_POST['AnaliseDeCredito']['observacao'] ) )
                        {
                            $mensagem                           = new Mensagem;
                            $mensagem->Dialogo_id               = $dialogo->id;
                            $mensagem->conteudo                 = $_POST['AnaliseDeCredito']['observacao'];
                            $mensagem->lida                     = 0;
                            $mensagem->habilitado               = 1;
                            $mensagem->editada                  = 0;
                            $mensagem->data_criacao             = date('Y-m-d H:i:s');
                            $mensagem->data_ultima_edicao       = date('Y-m-d H:i:s');
                            $mensagem->Usuario_id               = Yii::app()->session['usuario']->id;
                            $mensagem->save();
                        }
                    }
                }

                for ( $i = 0; $i < sizeof( $_POST['Bens'] ) ; $i++ ) {

                    /*$itemDaAnalise                            = new ItemDaAnalise;
                    $itemDaAnalise->Analise_de_Credito_id   = $analiseDeCredito->id;
                    $itemDaAnalise->Grupo_de_Produto_id     = $_POST['Bens'][$i];
                    $itemDaAnalise->data_cadastro           = date('Y-m-d H:i:s');
                    $itemDaAnalise->data_cadastro_br        = date('d/m/Y H:i:s');

                    $itemDaAnalise->save();*/

                    $analiseHasSubgrupoDeProduto                        = new AnaliseHasSubgrupoDeProduto;
                    $analiseHasSubgrupoDeProduto->Analise_de_Credito_id = $analiseDeCredito->id;
                    $analiseHasSubgrupoDeProduto->SubgrupoDeProtudo_id  = $_POST['Bens'][$i];
                    $analiseHasSubgrupoDeProduto->save();
                }

                $this->redirect('/proposta/minhasPropostas/');
            }
        }
    }

    public function actionUpLoadTeste(){

        if (isset($_POST['descricoes'])) {
            $descricoes = $_POST['descricoes'];
        }

        $_UPCONFIG['dir'] = '/var/www/sigac.v1/uploads/';

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        if( isset($_FILES['arquivos']) ){

            $anexos = $_FILES['arquivos'];
            $tamanho = sizeof($anexos['name']);

            for ($i = 0; $i < $tamanho; $i++) {

                //echo $anexos['name'][$i] . '===' . $descricoes[$i] .'<br>';

                if ( move_uploaded_file( $anexos['tmp_name'][$i], $_UPCONFIG['dir'].$anexos['name'][$i] ) ) {
                    echo "Arquivo enviado com suscesso <br>";
                }else{
                    echo "Comportamento inesperado do sistem";
                }
            }
        }

        $cs->registerScriptFile($baseUrl.'/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl.'/js/jquery.maskedinput.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.maskMoney.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/form-elements.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/daterangepicker.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/bootstrap-datepicker.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/bootstrap-timepicker.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/bootstrap-colorpicker.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.tagsinput.js',CClientScript::POS_END);


        $cs->registerScriptFile($baseUrl.'/js/summernote.min.js',  CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/ckeditor/ckeditor.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/ckeditor/adapter/jquery.js',CClientScript::POS_END);

        $cs->registerScriptFile($baseUrl.'/js/form-wizard.js',  CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.validate.min.js',  CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.smartWizard.js',  CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/select2.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.inputlimiter.1.3.1.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.autosize.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/bootstrap-fileupload.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/analise-upload-file-manager.js',CClientScript::POS_END);

        $cs->registerCssFile($baseUrl.'/css/sigax_template/bootstrap-fileupload.min.css');

        $this->render('upTeste');
    }

    public function actionGetCaptchaId(){

        Yii::import('application.vendor.*');

        $t = new Tcpf;

        echo Yii::app()->session->getSessionId();
    }

    protected function performAjaxValidation($model){

        if( isset( $_POST['ajax'] ) && $_POST['ajax'] === 'analise-de-credito-form' ){

            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
