<?php

class ListaNegraController extends Controller {

    public $layout = '//layouts/main_sigac_template';

    public function filters() {

        return array(
                'accessControl', // perform access control for CRUD operations
                'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules() {

        return array(
                array(
                        'allow',
                        'actions' => array(
                                'index',
                                'excluirParametro',
                                'mudarValor',
                                'listarParametros',
                                'incluirConfig',
                                'config',
                                'configLecca',
                                'nucleos',
                                'getParametrosLecca',
                                'salvarConfigLecca'
                        ),
                        'users' => array('@'),
                        'expression' => 'Yii::app()->session["usuario"]->role == "empresa_admin"        '
                ),
                array(
                        'deny',
                        'users' => array('*'),
                ),
        );
    }

    public function actionIndex() {
        $this->render('index');
    }

    public function actionExcluirParametro() {
        $arrayRetorno = array(
                'hasErrors' => false,
                'msg' => 'Parâmetro excluido com sucesso.',
                'pntfyClass' => 'success'
        );

        if (isset($_POST['id_par'])) {
            $id_par = $_POST['id_par'];

            $config = ConfigLN::model()->find("habilitado AND id = " . $id_par);
            $config->habilitado = 0;

            if ($config != null) {
                if (!$config->update()) {

                    $arrayRetorno = array(
                            'hasErrors' => true,
                            'msg' => 'Não foi possível excluir o parâmetro.',
                            'pntfyClass' => 'error'
                    );
                }
            } else {
                $arrayRetorno = array(
                        'hasErrors' => true,
                        'msg' => 'Não foi possível encontrar o parâmetro especificado.',
                        'pntfyClass' => 'error'
                );
            }
        }

        echo json_encode($arrayRetorno);
    }

    public function actionMudarValor() {
        $arrayRetorno = array(
                'hasErrors' => 0,
                'msg' => 'Valor alterado com sucesso.',
                'pntfyClass' => 'success'
        );

        if (isset($_POST['nomeParametro']) && isset($_POST['valor'])) {
            $nomeParametro = $_POST['nomeParametro'];
            $valor = $_POST['valor'];

            $config = ConfigLN::model()->find("habilitado AND parametro = '" . $nomeParametro . "'");

            if ($config != null) {
                $config->valor = $valor;
                if (!$config->update()) {
                    ob_start();
                    var_dump($config->getErrors());
                    $result = ob_get_clean();

                    $arrayRetorno = array(
                            'hasErrors' => 1,
                            'msg' => 'Não foi possível alterar o valor do parâmetro.' . $result,
                            'pntfyClass' => 'error'
                    );
                } else {
                    $sigacLog = new SigacLog;
                    $sigacLog->saveLog('Alteração de Parâmetros', 'Usuario', $config->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Usuário " . Yii::app()->session['usuario']->username . " alterou o parâmetro: " . $config->parametro, "127.0.0.1", null, Yii::app()->session->getSessionId());
                }
            } else {
                $arrayRetorno = array(
                        'hasErrors' => 1,
                        'msg' => 'Não foi possível encontrar o parâmetro especificado.',
                        'pntfyClass' => 'error'
                );
            }
        }

        echo json_encode($arrayRetorno);
    }

    public function actionListarParametros() {
        echo json_encode(
            ConfigLN::model()->listarParametros()
        );
    }

    public function actionIncluirConfig() {

        $retorno ['hasError'] = true;
        $retorno ['msg'] = "Estão faltando alguns parâmetros. Certifique-se de preencher todos os campos.";

        if ($_POST['config_par'] != "" && $_POST['config_val'] != "" && $_POST['config_tipo'] != "" && $_POST['config_data'] != "") {
            $config = new ConfigLN;
            $config->parametro = $_POST['config_par'];
            $config->valor = $_POST['config_val'];
            $config->tipo = $_POST['config_tipo'];
            $config->data_cadastro = $_POST['config_data'];
            $config->descricao = $_POST['config_descricao'];
            $config->habilitado = 1;

            if ($config->save()) {
                $retorno ['hasError'] = false;
                $retorno ['msg'] = "Parâmetro incluído com sucesso. TECLE 'F5' PARA ATUALIZAR A TABELA.";
            } else {
                $retorno ['hasError'] = true;
                $retorno ['msg'] = "Erro ao salvar parâmetro de configuração.";
            }
        }

        echo json_encode($retorno);
    }

    public function actionConfigLecca() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ui-elements.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.autosize.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskMoney.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.inputlimiter.1.3.1.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-search-cep.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/blockInterface.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/config_ln/config_lecca-v2.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
        $cs->registerScript('select2', '');
        $cs->registerScript('codTab', 'jQuery(function($){$("input.codTab").mask("999");});');
        $cs->registerScript('codLoj', 'jQuery(function($){$("input.codLoj").mask("99999");});');
        $this->render('config_lecca');
    }

    public function actionNucleos() {
        $nucleos = NucleoFiliais::model()->findAll('habilitado');
        $nucs = [];
        foreach ($nucleos as $nc) {
            $n = array(
                    'id' => $nc->id,
                    'text' => (string) strtoupper($nc->nome),
            );

            $nucs[] = $n;
        }
        echo json_encode($nucs);
    }

    public function actionSalvarConfigLecca() {
        $id = $_POST['nucleo'];
        $parametros = ConfiguracoesLecca::model()->find('NucleoFiliais_id = ' . $id);

        $retorno ['hasError'] = false;
        $retorno ['msg'] = "Configurações salvas com sucesso";
        $retorno ['tipo'] = "success";

        if ($parametros != null) {
            $parametros->cod_lojista = $_POST['clojista'];
            $parametros->cod_tab15 = $_POST['t15'];
            $parametros->cod_tab30 = $_POST['t30'];
            $parametros->cod_tab45 = $_POST['t45'];
            $parametros->cod_tabJuros = $_POST['tjuros'];
            $parametros->cod_tabJuros90 = $_POST['tjuros90'];
            $parametros->cod_tabPromo = $_POST['tpromo'];

            if (!$parametros->update()) {
                
                ob_start();
                var_dump($parametros->getErrors());
                $resultado = ob_get_clean();
                
                $retorno ['hasError'] = true;
                $retorno ['msg'] = "Erro ao atualizar configurações! " . $resultado;
                $retorno ['tipo'] = "error";
            }
        } else {
            $parametros = new ConfiguracoesLecca;
            $parametros->cod_lojista = $_POST['clojista'];
            $parametros->cod_tab15 = $_POST['t15'];
            $parametros->cod_tab30 = $_POST['t30'];
            $parametros->cod_tab45 = $_POST['t45'];
            $parametros->cod_tabJuros = $_POST['tjuros'];
            $parametros->cod_tabJuros90 = $_POST['tjuros90'];
            $parametros->cod_tabPromo = $_POST['tpromo'];
            $parametros->habilitado = 1;
            $parametros->NucleoFiliais_id = $id;

            if (!$parametros->save()) {

                ob_start();
                var_dump($parametros->getErrors());
                $resultado = ob_get_clean();

                $retorno ['hasError'] = true;
                $retorno ['msg'] = "Erro ao salvar configurações! " . $resultado;
                $retorno ['tipo'] = "error";
            }
        }

        echo json_encode($retorno);
    }

    public function actionGetParametrosLecca() {
        $id = $_POST['nucleo'];

        $codLojista = '';
        $codTabJuros = '';
        $codTabJuros90 = '';
        $codTabPromo = '';
        $codTab15 = '';
        $codTab30 = '';
        $codTab45 = '';

        if (isset($id) && $id != '') {
            $parametros = ConfiguracoesLecca::model()->find('NucleoFiliais_id = ' . $id);
        } else {
            $parametros = null;
        }

        if ($parametros != null) {
            $codLojista = $parametros->cod_lojista;
            $codTab15 = $parametros->cod_tab15;
            $codTab30 = $parametros->cod_tab30;
            $codTab45 = $parametros->cod_tab45;
            $codTabJuros = $parametros->cod_tabJuros;
            $codTabJuros90 = $parametros->cod_tabJuros90;
            $codTabPromo = $parametros->cod_tabPromo;
        }

        echo json_encode(
            array(
                    'lojista' => $codLojista,
                    'tjuros' => $codTabJuros,
                    't15' => $codTab15,
                    't30' => $codTab30,
                    't45' => $codTab45,
                    't90' => $codTabJuros90,
                    'tpr' => $codTabPromo,
            )
        );
    }

    public function actionConfig() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ui-elements.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.autosize.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskMoney.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.inputlimiter.1.3.1.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-search-cep.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/blockInterface.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/config_ln/parametrosv2.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-filestyle.min.js', CClientScript::POS_END);

        $cs->registerScript('moneypicker', '$(".grana").maskMoney({decimal:",", thousands:"."});');
        $cs->registerScript('select2', '');
        $cs->registerScript('ipt_date', 'jQuery(function($){$("input.dateBR").mask("99/99/9999");});');

        $cs->registerScript('ipt_tempo_residencia', 'jQuery(function($){$("#tempo_de_residencia").mask("99/9999");});');
        $cs->registerScript('ipt_data', 'jQuery(function($){$("input.dateBR").mask("99/99/9999");});');
        $cs->registerScript('ipt_mes_ano', 'jQuery(function($){$("input.mes_ano").mask("99/9999");});');
        $cs->registerScript('ipt_date', 'jQuery(function($){$("input.cpfmask").mask("99999999999");});');
        $cs->registerScript('ipt_date', 'jQuery(function($){$("input.cepmask").mask("99999999");});');
        $cs->registerScript('datepicker', "$('.date-picker').datepicker({autoclose: true});");
        $cs->registerScript('moneypicker2', '$(".currency").maskMoney({decimal:",", thousands:".", allowZero: true});');
        $cs->registerScript('moneypicker', '$(".currency2").maskMoney({decimal:",", thousands:".", allowZero: false});');
        $cs->registerScript('MesAnoRenda', 'jQuery(function($){$("input.mes_ano_renda").mask("99/9999");});');

        $this->render('config_ln');
    }

    // Uncomment the following methods and override them if needed
    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }

      public function actions()
      {
      // return external action classes, e.g.:
      return array(
      'action1'=>'path.to.ActionClass',
      'action2'=>array(
      'class'=>'path.to.AnotherActionClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }
     */
}
