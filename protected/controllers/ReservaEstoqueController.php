<?php

class ReservaEstoqueController extends Controller {

    public $layout = '//layouts/main_sigac_template';

    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index','reservar'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        
        $cabec = NULL;
        $retorno = NULL;

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-reservaestoque-table.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js', CClientScript::POS_END);

        $cs->registerScript('select_filial', '$(function($){$("#select_filial"  ).select2();});');
        $cs->registerScript('select_positivo', '$(function($){$("#select_positivo").select2();});');

        $sql = "SELECT FIRST 5 P.CODIGO, NOME_COMERCIAL, E.ESTOQUE " .
        "FROM PRODUTOS P " .
        "INNER JOIN ESTOQUE_LOJAS E ON P.CODIGO = E.CODIGO " .
        "WHERE BLOQUEADO = 0 AND LOJA = 25 AND E.ESTOQUE > 0 ";

        $db = new PDO("firebird:dbname=187.12.141.11/3050:C:\automacao\dados\MARE MANSA\LOJA 25\camaleao.gdb", "sysdba", "masterkey");

//       $db = Yii::app()->db1;

        //$db->execute("SET CHARACTER SET utf8");
        //$db = new PDO("odbc:Server=10.0.254.8,1433;Database=GMM", "sa", 'Grup0M@r3SA');

        /*if (empty($_POST['filial'])) {
            $filial = '01010004';
        } else {
            $filial = $_POST['filial'];
        }

        if (empty($_POST['positivo'])) {
            $positivo = '2';
        } else {
            $positivo = $_POST['positivo'];
        }

        $produto = '';
        $descricao = $_POST['descricao'];
        $referencia = $_POST['referencia'];
        $local = 'VE';*/

//            $sth = $db->prepare('EXEC SALDOMAISFILTRO :filial, :produto, :descricao, :referencia, :local, :positivo, :preco');

        $sth = $db->prepare($sql);

/*            $sth->bindParam(':filial', $filial, PDO::PARAM_STR, 08);
        $sth->bindParam(':produto', $produto, PDO::PARAM_STR, 17);
        $sth->bindParam(':descricao', $descricao, PDO::PARAM_STR, 80);
        $sth->bindParam(':referencia', $referencia, PDO::PARAM_STR, 20);
        $sth->bindParam(':local', $local, PDO::PARAM_STR, 02);
        $sth->bindParam(':positivo', $positivo, PDO::PARAM_STR, 01);
        $sth->bindParam(':preco', $preco, PDO::PARAM_STR, 01);*/

        $sth->execute();

        $cabec = ['CODIGO','NOME','QUANTIDADE','RESERVADO', 'DISPONIVEL'];

        $retorno = $sth->fetchAll();
        
        $dados = Reserva::model()->listReservaEstoque($retorno);
        
        $this->render('index', array(
            'cabec' => $cabec,
            'query' => $dados,
            //'parametros' => $_POST,
        ));
    }
    
    
    public function actionReservar()
    {
        $prod = $_POST['prod'];
        $qtd  = $_POST['qtd'];
        
        $reserva = new Reserva;
        
        $reserva->StatusReserva_id = 1;
        $reserva->codigoSisOrig = $prod;
        $reserva->qtdReservada = $qtd;
        $reserva->qtdEfetivada = 0;
        $reserva->qtdRecusada = 0;
        $reserva->data = date('Y-m-d H:m:s');
        $reserva->data_cadastro = date('Y-m-d H:m:s');
        $reserva->Filial_id = 20;
        
        return json_encode($reserva->grava());
        
    }
    

}
