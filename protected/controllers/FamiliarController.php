<?php

class FamiliarController extends Controller
{

	public function actionIndex()
	{
		$this->render('index');
	}


	public function actionUpdate(){

		$action 	= $_POST['controller_action'];
		$msgReturn 	= "";

		if( $action == 'add' )
		{
			Familiar::model()->novo($_POST['Pessoa'], $_POST['Parentesco_id'], $_POST['Cliente_id']);
			$msgReturn = "Parente cadastrado com sucesso!";
		}
		else
		{
			//Familiar::model()->atualizar($_POST['Parentesco_id'], $_POST['Parentesco_id'], $_POST['referencia_id'], $_POST['telefone_referencia_id']);
			$msgReturn = "Os dados do parente foram atualizados!";
		}

		echo json_encode(array(
			'msgReturn' => $msgReturn
		));
	}

	public function actionLoad(){

		$id 		= $_GET['entity_id'];
		echo json_encode( Familiar::model()->loadFormEdit($id) );
	}
}