<?php

class ModeloController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionSearchModelo(){
		echo json_encode( Modelo::model()->searchModeloSelec2( $_POST['q'] ) );
	}

	public function actionAdd(){
		echo json_encode( Modelo::model()->add( $_POST['Modelo'] ) );
	}
}