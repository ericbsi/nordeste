<?php

class SiteController extends Controller{
	
	public $layout = '//layouts/login';

	//sobrescreve a função do Controller, classe pai
	public function init(){

	}

	public function actions(){

		return array(
				
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
			
			'page'=>array(
				'class'=>'CViewAction',
			),
		);

	}

	public function actionIndex(){

		$this->render('login');		

	}

	public function actionError(){

		if( $error=Yii::app()->errorHandler->error ){

			if(Yii::app()->request->isAjaxRequest){
				echo $error['message'];
			}

			else{
				$this->render('error', $error);
			}

		}

	}

	public function actionLogin(){
		
		header('Access-Control-Allow-Origin: *');
		
		$model = new LoginForm;

		if( isset( $_POST['LoginForm'] ) ){

			$model->attributes = $_POST['LoginForm'];

			if( $model->validate() && $model->login() ){

				$this->redirect( Yii::app()->session['usuario']->getRole()->login_redirect );
			}
		}

		//se o usuario estiver logado na sessão, redireciona para a inicial do sistema
		elseif( !Yii::app()->user->isGuest ){
			$this->redirect(Yii::app()->session['usuario']->getRole()->login_redirect);
		}
		
		
		if( isset( $_GET['u'] ) ){
			$model->username = $_GET['u'];
		}
		
		/*else{
			$sConfig 				= SigacConfig::model()->findByPk(1);
			$this->redirect($sConfig->local_login);
		}*/
		

		$this->render('login',array('model'=>$model));
	}

	public function actionLogout(){
		$sConfig 				= SigacConfig::model()->findByPk(1);
		$sigacLog = new SigacLog;
		$sigacLog->saveLog('logout_de_usuario','usuario', Yii::app()->session['usuario']->id, date('Y-m-d H:i:s'),1, null, "Logout de usuario | Usuário: ".Yii::app()->session['usuario']->username, "127.0.0.1",null, Yii::app()->session->getSessionId() );

		if ( Yii::app()->session['usuario']->tipo == 'crediarista' )
			Yii::app()->session['usuario']->cleanCaptchas();
		
		Yii::app()->user->logout();
		Yii::app()->session->clear();
		Yii::app()->session->destroy();
		$this->redirect($sConfig->local_login);
	}
}