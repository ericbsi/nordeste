<?php

class MarcaController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionAdd(){
		echo json_encode( Marca::model()->add( $_POST['Marca'] ) );
	}

	public function actionSearchMarcas(){
		echo json_encode( Marca::model()->searchMarcaSelec2( $_POST['q'] ) );
	}
}