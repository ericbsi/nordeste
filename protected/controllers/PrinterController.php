<?php

class PrinterController extends Controller
{

	public $layout 	= '//printer/template';

    public function actionPrintBads()
    {
        //$impressao     = Empresa::model()->analisarBadFinanceira( $_POST['mes'], $_POST['ano'], 1, $_POST['Parceiros'],  );
        $impressao     = Empresa::model()->analisarBadFinanceira( $_POST['mes'], $_POST['ano'], 1, $_POST['Parceiros'], $_POST['analista'] );
        
        $this->render('printBads',array(
            'printConfig' => $impressao
        ));
    }
    
    public function actionTestePDF()
    {
        $this->layout = '//printer/pdf_template';

        require_once Yii::app()->basePath . '/vendor/fpdf/fpdf.php';

        $pdf = new fpdf;

        $pdf->AddPage();
        $pdf->SetFont('Arial','B',16);
        $pdf->Cell(40,10,'Hello World!');
        $pdf->Output();

    }

    public function actionBoleto()
    {
        $omniConfigId               = $_POST['omniConfig'];
        $omniConfig                 = PropostaOmniConfig::model()->findByPk( $omniConfigId );
        $proposta                   = Proposta::model()->findByPk($omniConfig->Proposta);
        
        if($proposta->titulos_gerados == 0)
        {
            $emprestimo = Emprestimo::model()->find("habilitado AND Proposta_id = $proposta->id");

            if($emprestimo !== null)
            {
                $retornoEmprestimo = $emprestimo->gerarTituloPagar();
                $ehEmprestimo      = true                           ;
            }
        }

        if( $omniConfig != NULL )
        {
            $omniConfig->atualizarLinks();
        }

        /*Só devemos considerar o titulo gerado se o link estiver gravado na configuração*/
        if( $omniConfig->urlBoleto != NULL && trim($omniConfig->urlBoleto) != '' )
        {
            $proposta->titulos_gerados  = 1;
            $proposta->update();

            $sigacLog = new SigacLog;
            $sigacLog->saveLog('Impressão de Boletos','Proposta', $proposta->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Crediarista ".Yii::app()->session['usuario']->nome_utilizador . " imprimiu os boletos referentes a proposta com código: " . $proposta->codigo, "127.0.0.1", null, Yii::app()->session->getSessionId() );

            header('Location: ' . $omniConfig->urlBoleto);
        }
        else
        {
            echo "Boletos não gerados";
        }

    }

    public function actionRecebimentoDeContrato()
    {
        $this->render('/integracao/recebimentoDeContratoReport', array('processo' => RecebimentoDeDocumentacao::model()->find("habilitado AND id = '" . $_POST['codigo'] . "'")));
    }
    
    public function actionPrinterPropostasMalote(){
		$this->render('recebimentoDeMalotesReport', array('malote' => Malote::model()->find('habilitado AND id = '. $_POST['idMalote'])));
    }
    
    public function actionRecebimentoDeContratoOmni()
    {
        $this->render('/integracao/recebimentoDeContratoReportOmni', array('processo' => RecebimentoDeDocumentacao::model()->find("habilitado AND id = '" . $_POST['codigo'] . "'")));
    }

    public function actionPrintProducaoFiliais()
    {
        $this->render( 'printProducao', array( 'params' => $_POST ) );
        /*echo '<pre>';
        print_r($_POST);
        echo '</pre>';*/
    }

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionPrintProducaoCapa()
    {
    	$printer 		= new Printer;
    	$retorno		= $printer->printCapaProducao($_POST['hdnDataDe'], $_POST['hdnDataAte']);

        $this->render('capaProducao',array('parametros'=>$retorno));
    }

    public function actionPrintProducaoParceiro(){

        $printer        = new Printer;
        $retorno        = $printer->printAllProducaoParceiro( $_POST['idsPropostas'] );

        $this->render   
        (
            'allProducaoParceiro'   , array (
                                                'parametros'    => array(
                                                                            'propostas' => $retorno                 ,
                                                                            'dataDe'    => $_POST['dataDeHdn'   ]   ,
                                                                            'dataAte'   => $_POST['dataAteHdn'  ]   ,
                                                                            'idFilial'  => $_POST['parceiroId'  ]
                                                                        )
                                            )
        );
    }

    public function actionPrintProducaoAll()
    {
    	$printer 		= new Printer;
    	$retorno 		= $printer->printAllProducao($_POST['hdnDataDe'],$_POST['hdnDataAte'],$_POST['hdnFiliais']);

    	$this->render('allProducao',array(
    		'parametros'    => array(
    			'propostas' => $retorno,
    			'dataDe'	=> $_POST['hdnDataDe'],
    			'dataAte'	=> $_POST['hdnDataAte']
    		)
    	));
    }
}