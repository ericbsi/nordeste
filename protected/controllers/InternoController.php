<?php

class InternoController extends Controller {

    public function actionIndex() {
        $this->render('index');
    }

    public function actionRelatorioPagamentos() {
        $this->layout = '//layouts/main_sigac_template';

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css');
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpalette.css');
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpicker.css');
        $cs->registerCssFile($baseUrl . '/js/sweet-alert2/sweetalert2.min.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/blockInterface.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpalette.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/interno/fn-rel-pagamentov2.js?v=2.3', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/sweet-alert2/sweetalert2.min.js', CClientScript::POS_END);

        $cs->registerScript('datepicker', "$('.date-picker').datepicker({autoclose: true});");

        $this->render('rel-pagamentos');
    }

    public function actionPagamentos() {
        $this->layout = '//layouts/main_sigac_template';

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css');
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpalette.css');
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpicker.css');
        $cs->registerCssFile($baseUrl . '/js/sweet-alert2/sweetalert2.min.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/blockInterface.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpalette.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/interno/fn-pagamento-interno.js?v=1.3', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/sweet-alert2/sweetalert2.min.js', CClientScript::POS_END);
        $cs->registerScript('select2', '');

        $this->render('parc_pendentes');
    }

    public function actionNucleos() {
        $nucleos = NucleoFiliais::model()->findAll('habilitado');
        $nucs = [];
        foreach ($nucleos as $nc) {
            $n = array(
                'id' => $nc->id,
                'text' => (string) strtoupper($nc->nome),
            );

            $nucs[] = $n;
        }
        echo json_encode($nucs);
    }

    public function actionFiliais() {

        $criteria = new CDbCriteria();

        $nucleo = 0;

        if (isset($_POST['nucleo'])) {
            $nucleo = $_POST['nucleo'];
        }

        $criteria->addInCondition("NucleoFiliais_id", [$nucleo]);
        $filiais = Filial::model()->findAll($criteria);
        $fils = [];

        foreach ($filiais as $f) {
            $fils[] = [
                'id' => $f->id,
                'text' => strtoupper($f->getConcat()),
            ];
        }

        echo json_encode($fils);
    }

    public function actionFinanceiras() {

        $criteria = new CDbCriteria();
        $criteria->addInCondition("id", [5, 10, 11]);

        $financeiras = Financeira::model()->findAll($criteria);
        $fins = [];

        foreach ($financeiras as $fin) {
            $fins[] = [
                'id' => $fin->id,
                'text' => strtoupper($fin->nome),
            ];
        }

        echo json_encode($fins);
    }

    public function actionStatus() {
        $status = StatusRI::model()->findAll('habilitado');
        $sts = [];
        foreach ($status as $st) {
            $n = array(
                'id' => $st->id,
                'text' => (string) strtoupper($st->status),
            );

            $sts[] = $n;
        }
        echo json_encode($sts);
    }

    public function actionGerarBoleto() {

        $this->layout = '//layouts/boleto_layout';

        $recebimentoId = $_POST['recebimentoId'];
        $recebimento = RecebimentoInterno::model()->findByPk($recebimentoId);

        // titulo nulo, então geramos o novo boleto
        if ($recebimento->titulo == null) {

            $retorno = RecebimentoInterno::model()->gerarBoleto($recebimento);

            if ($retorno != Null) {
                $recebimento->Titulo_id = $retorno['titulo_id'];
                $recebimento->update();

                $this->render('/boleto/indexRI', [
                    'recebimento' => $recebimento,
                    'boletoConfig' => $retorno
                ]);
            } else {
                throw new Exception('Não foi possível gerar o boleto.', 0);
            }
        }

        // boleto ja gerado, segunda via
        else {
            $this->render('/boleto/indexRI', [
                'recebimento' => $recebimento,
                'boletoConfig' => $recebimento->segundaViaBoleto()
            ]);
        }
    }

    public function actionListarParcelas() {
        $sql = "SELECT
                    P.codigo,
                    PE.nome,
                    PA.seq,
                    PA.valor,
                    PA.id,
                    PA.vencimento

                FROM
                                Proposta 		AS P
                INNER JOIN 	Titulo 			AS T 	ON T.Proposta_id = P.id             AND T.NaturezaTitulo_id = 1 AND T.habilitado
                INNER JOIN 	Parcela 		AS PA 	ON PA.Titulo_id = T.id              AND PA.habilitado
                INNER JOIN 	Analise_de_Credito 	AS AC 	ON AC.id = P.Analise_de_Credito_id  AND AC.habilitado
                INNER JOIN 	Cliente  		AS C 	ON C.id = AC.Cliente_id             AND C.habilitado
                INNER JOIN 	Pessoa 			AS PE 	ON PE.id = C.Pessoa_id              AND PE.habilitado
                INNER JOIN 	Pessoa_has_Documento 	AS PD 	ON PD.Pessoa_id = PE.id             AND PD.habilitado
                INNER JOIN 	Documento 		AS D 	ON D.id = PD.Documento_id           AND D.Tipo_documento_id = 1 AND D.habilitado
                LEFT  JOIN 	PropostaOmniConfig 	AS PO 	ON PO.Proposta = P.id               AND PO.habilitado

                WHERE
                    P.Status_Proposta_id IN (2) AND
                    PA.valor_atual = 0 		AND
                    PO.id IS NULL";
        /* AND AC.Filial_id = " . Yii::app()->session['usuario']->getFilial()->id */

        $contrato = $_POST['contrato'];
        $cpf = $_POST['cpf'];

        if (isset($contrato) && $contrato != '') {
            $sql .= " AND P.codigo = " . $contrato;
        }

        if (isset($cpf) && $cpf != '') {
            $sql .= " AND D.numero = " . $cpf;
        }

        $parcelas = [];
        if ($cpf != '' || $contrato != '') {
            $parcelas = Yii::app()->db->createCommand($sql)->queryAll();
        }

        $rows = [];
        $util = new Util;

        foreach ($parcelas as $pa) {
            $parc = Parcela::model()->findByPk($pa['id']);
            $valor = $parc->valor; //+ $parc->calcMora();

            $comprovante = '<form class="comprovante_pgto" action="/interno/comprovantePgto/" method="post" target="_blank"><input type="hidden" value="' . $pa['id'] . '" name="parcelaid"><button value=' . $pa['id'] . ' class="btn btn-xs btn-info">Comprovante</button></form>';

            /*            $comprovante    = '<form class="formComprovante" action="/interno/comprovantePgto/" method="post" target="_blank">'
              . '     <input type="hidden" value="' . $pa['id'] . '" name="parcelaid" />'
              . '</form>'
              . '<button value=' . $pa['id'] . ' class="btn btn-xs btn-info comprovante_pgto">'
              . '     Comprovante'
              . '</button>'; */

            //$anexar = '<button value=' . $pa['id'] . ' class="btn btn-xs btn-warning">Anexar</button>';

            $row = array(
                "contrato" => $pa['codigo'],
                "cliente" => strtoupper($pa['nome']),
                "seq" => $pa['seq'],
                "venc" => $util->bd_date_to_view($pa['vencimento']),
                "valor" => "R$ " . number_format($valor, 2, ',', '.'),
                "dias_atraso" => $parc->diasVencimento(),
                "valor_atraso" => "R$ " . number_format($parc->calcMora(), 2, ',', '.'),
                "valor_receber" => "R$ " . number_format($valor + $parc->calcMora(), 2, ',', '.'),
                "comprovante" => $comprovante
            );

            $rows[] = $row;
        }

        if ($cpf == '' && $contrato == '') {
            $rows = [];
        }

        echo json_encode(
                array(
                    "data" => $rows
                )
        );
    }

    public function actionDetalharParcelas() {
        $id = $_POST['id_rec'];
        $recebimento = RecebimentoInterno::model()->findByPk($id);

        $parcelas = $recebimento->parcelas;
        $rows = [];
        $util = new Util;

        foreach ($parcelas as $pa) {

            if (RecebimentoInternoHasParcela::model()->find('Parcela_id = ' . $pa->id . ' AND RecebimentoInterno_id = ' . $recebimento->id . ' AND habilitado') != Null) {

                $row = array(
                    "contrato" => $pa->titulo->proposta->codigo,
                    "cliente" => strtoupper($pa->titulo->proposta->analiseDeCredito->cliente->pessoa->nome),
                    "seq" => $pa->seq,
                    "venc" => $util->bd_date_to_view($pa->vencimento),
                    "valor" => "R$ " . number_format($pa->valor, 2, ',', '.'),
                    "valor_pago" => "R$ " . number_format($pa->valor_atual, 2, ',', '.')
                );

                $rows[] = $row;
            }
        }
        echo json_encode(
                array(
                    "data" => $rows
                )
        );
    }

    public function actionListarRecebimentos() {
        $filtro = "habilitado and StatusRI_id IN (1, 3) and Filial_id = " . Yii::app()->session['usuario']->getFilial()->id;

        $recebimentos = RecebimentoInterno::model()->findAll(array('order' => 'data_cadastro desc', 'condition' => $filtro));

        $rows = [];
        $util = new Util;

        foreach ($recebimentos as $rec) {
            $boleto = '<form action="/interno/gerarBoleto/" method="post" target="_blank"><input name="recebimentoId" value=' . $rec['id'] . ' type="hidden"><button class="btn btn-xs btn-info">Gerar Boleto</button></form>';
            if ($rec->statusRI->id == 1) {
                $anexar = '<button value=' . $rec['id'] . ' class="anx btn btn-xs btn-warning">Anexar Comprovante</button>';
            } else {
                $parcela = Parcela::model()->find('habilitado and Titulo_id = ' . $rec->titulo->id);
                $anexo = Anexo::model()->find('habilitado and entidade_relacionamento = "ComprovanteRI" and id_entidade_relacionamento = ' . $parcela->id);
                $anexar = '<a href="' . $anexo->absolute_path . '" target="_blank" class="btn btn-xs btn-danger">Anexado ( Visualizar )</a>';
            }

            $detalhar = '<button value=' . $rec['id'] . ' class="detail btn btn-xs btn-success">Detalhar</button>';
            $valor = 0;
            foreach ($rec->parcelas as $par) {
                $valor += $par->valor_atual;
            }

            $row = array(
                "detalhar" => $detalhar,
                "financeira" => strtoupper($rec->financeira->nome),
                "data" => $util->bd_date_to_view(substr($rec->data_cadastro, 0, 10)),
                "valor" => "R$ " . number_format($valor, 2, ',', '.'),
                "boleto" => $boleto,
                "anexar" => $anexar
            );

            $rows[] = $row;
        }

        echo json_encode(
                array(
                    "data" => $rows
                )
        );
    }

    public function actionListarRelatorio() {
        $filtro = "habilitado";
        $valor = 0;
        $valor_total = 0;

        $util = new Util;

        if (isset($_POST['nucleo']) && $_POST['nucleo'] != '') {
            $filiais = [];
            foreach (NucleoFiliais::model()->findByPk($_POST['nucleo'])->filials as $f) {
                $filiais[] = $f->id;
            }
            $filtro .= " and Filial_id IN (" . join(',', $filiais) . ")";
        }

        if (isset($_POST['status']) && $_POST['status'] != '') {
            $filtro .= " and StatusRI_id = " . $_POST['status'];
        }

        if (isset($_POST['financeira']) && $_POST['financeira'] != '') {
            $filtro .= " and Financeira_id = " . $_POST['financeira'];
        }

        if (isset($_POST['filial']) && $_POST['filial'] != '') {
            $filtro .= " and Filial_id = " . $_POST['filial'];
        }

        if ((isset($_POST['de']) && trim($_POST['de']) != '' && $_POST['de'] != Null) && (isset($_POST['ate']) && trim($_POST['ate']) != '' && $_POST['ate'] != Null)) {
            $filtro .= " and data_cadastro BETWEEN '" . $util->view_date_to_bd($_POST['de']) . " 00:00:00' AND '" . $util->view_date_to_bd($_POST['ate']) . " 23:59:59' ";
        }

        $filtered = RecebimentoInterno::model()->findAll(array('order' => 'data_cadastro desc', 'condition' => $filtro));
        
        //$filtro .= " LIMIT " . $_POST["start"] . ", " . $_POST["length"];

        $recebimentos = RecebimentoInterno::model()->findAll(array('order' => 'data_cadastro desc', 'condition' => $filtro, 'limit' => 10, 'offset' =>$_POST['start']));

        $rows = [];
        $util = new Util;
        
        foreach ($filtered as $fil) {
            $valor = 0;
            foreach ($fil->parcelas as $fi) {
                $valor += $fi->valor_atual;
            }

            $valor_total += $valor;
        }
        
        foreach ($recebimentos as $rec) {
            if ($rec->statusRI->id == 1) {
                $anexo = '<button style="width:135px!important;" value=' . $rec['id'] . ' class="anx btn btn-xs btn-warning">Sem Comprovante</button>';
            } else {
                if ($rec->statusRI->id == 2) {
                    $parcela = Parcela::model()->find('habilitado and Titulo_id = ' . $rec->titulo->id);
                    $anx = Anexo::model()->find('habilitado and entidade_relacionamento = "ComprovanteRI" and id_entidade_relacionamento = ' . $parcela->id);
                    if($anx != null){
                        $anexo = '<a style="width:135px!important;" href="' . $anx->absolute_path . '" target="_blank" class="btn btn-xs btn-success">Pago ( Visualizar )</a>';
                    }else{
                        $anexo = '<a style="width:135px!important;" href="#" target="_blank" class="btn btn-xs btn-success">Pago ( Sem Anexo )</a>';
                    }
                    
                    //$anexo = '<button style="width:135px!important;" value=' . $rec['id'] . ' class="anx btn btn-xs btn-success">Recebimento Pago</button>';
                } else {
                    $parcela = Parcela::model()->find('habilitado and Titulo_id = ' . $rec->titulo->id);
                    $anx = Anexo::model()->find('habilitado and entidade_relacionamento = "ComprovanteRI" and id_entidade_relacionamento = ' . $parcela->id);
                    $anexo = '<a style="width:135px!important;" href="' . $anx->absolute_path . '" target="_blank" class="btn btn-xs btn-danger">Anexado ( Visualizar )</a>';
                }
            }

            $detalhar = '<button value=' . $rec['id'] . ' class="detail btn btn-xs btn-success">Detalhar</button>';
            $valor = 0;
            foreach ($rec->parcelas as $par) {
                $valor += $par->valor_atual;
            }

            //$valor_total += $valor;

            $row = array(
                "detalhar" => $detalhar,
                "financeira" => strtoupper($rec->financeira->nome),
                "filial" => strtoupper($rec->filial->getConcat()),
                "data" => $util->bd_date_to_view(substr($rec->data_cadastro, 0, 10)),
                "valor" => "R$ " . number_format($valor, 2, ',', '.'),
                "anexo" => $anexo
            );

            $rows[] = $row;
        }

        echo json_encode([
            'draw' => $_POST['draw'],
            'recordsTotal' => count($filtered),
            'recordsFiltered' => count($filtered),
            'data' => $rows,
            "customReturn" => [
                'total' => number_format($valor_total, 2, ',', '.')
            ],
        ]);
    }

    public function actionAnexarComprovante() {
        $arrReturn = array(
            "hasError" => true,
            "msg" => "Boleto nao gerado ou recebimento nao encontrado! Se o erro persistir entre em contato com o suporte!",
            "tipo" => "error"
        );

        $id_ri = $_POST['id_ri'];
        $ri = RecebimentoInterno::model()->findByPk($id_ri);

        if ($ri->Titulo_id != NULL) {
            $parcela = Parcela::model()->find('habilitado and Titulo_id = ' . $ri->titulo->id);
        } else {
            $parcela = NULL;
        }

        $transaction = Yii::app()->db->beginTransaction();

        if (isset($_FILES['anexo_comprovante']) && $parcela != NULL) {
            $ext = substr($_FILES['anexo_comprovante']['name'], strrpos($_FILES['anexo_comprovante']['name'], '.'));
            $aleatorio = rand(0, 9999999999);

            $s3Client = new S3Client;
            $retornoAnexo = $s3Client->put($_FILES["anexo_comprovante"], "ComprovanteRI", $parcela->id, 'Comprovante de recebimento interno');
            $ri->StatusRI_id = 3;

            if (!$retornoAnexo['hasErrors'] && $ri->update()) {
                $arrReturn = array(
                    "hasError" => false,
                    "msg" => "Comprovante anexado com sucesso",
                    "tipo" => "success"
                );
            } else {
                $arrReturn = array(
                    "hasError" => true,
                    "msg" => "Comprovante nao anexado! Nao foi possivel salvar o anexo",
                    "tipo" => "error"
                );
            }
        }

        if (!$arrReturn["hasError"]) {
            $transaction->commit();
        } else {
            $transaction->rollBack();
        }

        echo json_encode($arrReturn);
    }

    public function actionComprovantePgto() {

        $this->layout = '//layouts/template_contrato';
        $this->render('/contrato/comprovante_pgt_ri', ['parcela' => Parcela::model()->findByPk($_POST['parcelaid'])]);
    }

    public function actionBaixaParcela() {

        $arrReturn = array(
            "hasError" => true,
            "msg" => "Não foi possível realizar a baixa, parcela não encontrada. Se o erro persistir entre em contato com o suporte",
            "tipo" => "error"
        );

        if (isset($_POST['id_parcela']) && !empty($_POST['id_parcela'])) {

            $id_parcela = $_POST['id_parcela'];

            $parcela = Parcela::model()->find("habilitado AND id = " . $id_parcela);

            if ($parcela != null) {

                $transaction = Yii::app()->db->beginTransaction();

                $parcela->valor_atual = $parcela->valor + $parcela->calcMora();

                if ($parcela->update()) {

                    $baixa = new Baixa;
                    $baixa->valor_pago = $parcela->valor + $parcela->calcMora();
                    $baixa->data_da_ocorrencia = date('Y-m-d');
                    $baixa->data_da_baixa = date('Y-m-d');
                    $baixa->baixado = 1;
                    $baixa->Parcela_id = $parcela->id;
                    $baixa->Filial_id = $parcela->titulo->proposta->analiseDeCredito->filial->id;
                    $baixa->Cliente_id = $parcela->titulo->proposta->analiseDeCredito->Cliente_id;

                    if ($baixa->save()) {

                        $recebimento = RecebimentoInterno::model()->find(
                                "habilitado and "
                                . "data_cadastro > '" . date('Y-m-d') . " 00:00:00' and "
                                . "data_cadastro < '" . date('Y-m-d') . " 23:59:59' and "
                                . "Financeira_id = " . $parcela->titulo->proposta->Financeira_id . " and "
                                . "StatusRI_id = 1 and "
                                . "Filial_id = " . Yii::app()->session['usuario']->getFilial()->id . " and "
                                . "Titulo_id is NULL"
                        );

                        if ($recebimento != null) {

                            $rhp = new RecebimentoInternoHasParcela;
                            $rhp->Parcela_id = $parcela->id;
                            $rhp->RecebimentoInterno_id = $recebimento->id;
                            $rhp->habilitado = 1;

                            if ($rhp->save()) {

                                $arrReturn = array(
                                    "hasError" => false,
                                    "msg" => "Parcela liquidada com sucesso!",
                                    "tipo" => "success"
                                );
                            } else {

                                $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                                $erros = $rhp->getErrors();

                                foreach ($erros as $e) {

                                    $msgErro .= $e[0] . chr(13) . chr(10);
                                }

                                $msgErro .= "</font></b>";

                                $arrReturn = array(
                                    "hasError" => true,
                                    "msg" => "Erro ao dar baixa na parcela! Gravação do RhP. Erros: $msgErro",
                                    "tipo" => "error"
                                );
                            }
                        } else {
                            $recebimento = new RecebimentoInterno;

                            $recebimento->Filial_id = Yii::app()->session['usuario']->getFilial()->id;
                            $recebimento->Financeira_id = $parcela->titulo->proposta->Financeira_id;
                            $recebimento->StatusRI_id = 1;
                            $recebimento->data_cadastro = date('Y-m-d H:i:s');
                            $recebimento->habilitado = 1;

                            if ($recebimento->save()) {
                                $rhp = new RecebimentoInternoHasParcela;
                                $rhp->Parcela_id = $parcela->id;
                                $rhp->RecebimentoInterno_id = $recebimento->id;
                                $rhp->habilitado = 1;

                                if ($rhp->save()) {
                                    $arrReturn = array(
                                        "hasError" => false,
                                        "msg" => "Parcela liquidada com sucesso!",
                                        "tipo" => "success"
                                    );
                                } else {

                                    $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                                    $erros = $rhp->getErrors();

                                    foreach ($erros as $e) {

                                        $msgErro .= $e[0] . chr(13) . chr(10);
                                    }

                                    $msgErro .= "</font></b>";

                                    $arrReturn = array(
                                        "hasError" => true,
                                        "msg" => "Erro ao dar baixa na parcela! Gravação do RhP. Erros: $msgErro",
                                        "tipo" => "error"
                                    );
                                }
                            } else {

                                $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                                $erros = $recebimento->getErrors();

                                foreach ($erros as $e) {

                                    $msgErro .= $e[0] . chr(13) . chr(10);
                                }

                                $msgErro .= "</font></b>";

                                $arrReturn = array(
                                    "hasError" => true,
                                    "msg" => "Nao foi possivel salvar o recebimento. Erros: $msgErro",
                                    "tipo" => "error"
                                );
                            }
                        }
                    } else {

                        $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                        $erros = $baixa->getErrors();

                        foreach ($erros as $e) {

                            $msgErro .= $e[0] . chr(13) . chr(10);
                        }

                        $msgErro .= "</font></b>";

                        $arrReturn = array(
                            "hasError" => true,
                            "msg" => "Nao foi possivel criar a baixa. Erros: $msgErro",
                            "tipo" => "error"
                        );
                    }
                } else {

                    $msgErro = chr(13) . chr(10) . "<b><font color='red'>";

                    $erros = $parcela->getErrors();

                    foreach ($erros as $e) {

                        $msgErro .= $e[0] . chr(13) . chr(10);
                    }

                    $msgErro .= "</font></b>";

                    $arrReturn = array(
                        "hasError" => true,
                        "msg" => "Erro ao atualizar a parcela! Erros: " . $msgErro,
                        "tipo" => "error"
                    );
                }
            } else {

                $arrReturn = array(
                    "hasError" => true,
                    "msg" => "Parcela não encontrada. ID $id_parcela",
                    "tipo" => "error"
                );
            }

            if (!$arrReturn["hasError"]) {
                $transaction->commit();
            } else {
                $transaction->rollBack();
            }
        }

        echo json_encode($arrReturn);
    }

}
