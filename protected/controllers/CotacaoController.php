<?php

class CotacaoController extends Controller{

	//public $layout='//layouts/column1';
	public $layout = '//layouts/main_sigac_template';
	public $pageIsModal;

	public function filters(){

		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function actionTabelas()
	{
		//Yii::app()->session['usuario']->updateVistoPorUltimo();
		
		$baseUrl	= Yii::app()->baseUrl;
        $cs 		= Yii::app()->getClientScript();

		$cs->registerCssFile($baseUrl.'/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl.'/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl.'/css/sigax_template/iCheck/skins/all.css');
		
		$cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl.'/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/select2.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.maskedinput.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/bootstrap-modal.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/bootstrap-modalmanager.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.validate.12.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/additional-methods.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/fn-tabelas-cotacoes-grid.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.icheck.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.blockUI.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/bootstrap-multiselect.js', CClientScript::POS_END);
               
        $this->render('grid_tabelas_cotacoes');
	}

	public function actionSimularCondicoes()
	{
		$params 	= array();
		$baseUrl 	= Yii::app()->baseUrl;
	    $cs 		= Yii::app()->getClientScript();

		$cs->registerScriptFile($baseUrl.'/js/select2.min.js',  CClientScript::POS_END);
		$cs->registerScriptFile($baseUrl.'/js/jquery.validate.12.js',CClientScript::POS_END);
		$cs->registerScriptFile($baseUrl.'/js/jquery.maskedinput.js',CClientScript::POS_END);
		$cs->registerScriptFile($baseUrl.'/js/jquery.maskMoney.js',  CClientScript::POS_END);
		$cs->registerScriptFile($baseUrl.'/js/fn-form-simular.js',  CClientScript::POS_END);
		$cs->registerScriptFile($baseUrl.'/js/bootstrap-modal.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/bootstrap-modalmanager.js',CClientScript::POS_END);

		$cs->registerScript('moneypicker','$(".currency").maskMoney({decimal:",", thousands:".", allowZero: true});');
		$cs->registerScript('select2','$(".select2").select2();');
		
		if( isset( $_POST['valor'] ) && !empty( $_POST['valor'] ) && isset( $_POST['tabela'] ) && !empty( $_POST['tabela'] ) )
		{
			$util 						= new Util;
			$valor 						= $util->moedaViewToBD($_POST['valor']);
			$params['valor_solicitado']	= $valor;

			$tabela 					= TabelaCotacao::model()->findByPk( $_POST['tabela'] );

			if($tabela != NULL)
			{
				$params['tabela'] 		= $tabela;
			}
		}

		$this->render('simularCondicoes', $params);
	}

	public function actionRenderTableEditar(){

		$this->layout 	= '//layouts/blank_template';

		$baseUrl 		= Yii::app()->baseUrl; 
	    $cs 			= Yii::app()->getClientScript();
  	    $cs->registerScriptFile($baseUrl.'/js/jquery.min.js');
  	    //$cs->registerScriptFile($baseUrl.'/js/table-parcelas-fn.js');
  	    
		$proposta_id 	= $_GET['proposta_id'];

		$proposta 		= Proposta::model()->findByPk( $proposta_id );

		$this->render('renderTabelaCotacaoEdit',
			array(
				//'valor_solicitado'	=> ($proposta->valor - $proposta->valor_entrada),
				'valor_solicitado'	=> ($proposta->getValorFinanciado()),
				'tabela'			=> $proposta->tabelaCotacao,
				'proposta'			=> $proposta,
			)
		);

	}

	public function actionRenderTable(){
		
		$this->layout 	= '//layouts/blank_template';

		$baseUrl 		= Yii::app()->baseUrl; 
	    $cs 			= Yii::app()->getClientScript();
  	    $cs->registerScriptFile($baseUrl.'/js/jquery.min.js');
  	    $cs->registerScriptFile($baseUrl.'/js/table-parcelas-fn.js');
  	    
		/*$cotacao_id 	= $_POST['cotacao_id'];
		$valor_soli 	= $_POST['valor_soli'];
		$valor_entrada 	= $_POST['valor_entrada'];*/

		$cotacao_id 	= $_POST['cotacao_id'];
		$valor_soli 	= $_POST['valor_soli'];
		$valor_entrada 	= $_POST['valor_entrada'];
		//$tabela_id 		= $_POST['tabela_id'];

		$valor_soli 	= $valor_soli - $valor_entrada;

		$cotacao 		= Cotacao::model()->findByPk( $cotacao_id );
		$tabela 		= TabelaCotacao::model()->findByPk( $cotacao_id );

		$this->render('renderTabelaCotacao',
			array(
				//'cotacao'			=> $cotacao,
				'valor_solicitado'	=> $valor_soli,
				'tabela'			=> $tabela,
			)
		);
	}

	public function accessRules(){

		return array(
			array('allow',
				'actions'=>array('simularCondicoes','renderTableEditar','tabelas','rendertable','create','update','admin','delete','loadfinanceiras','index','view','rmfinanceira', 'addfinanceira'),
				'users'=>array('@'),
			),			
			array('deny',
				'users'=>array('*'),
			),
		);
	}


	public function actionRmFinanceira(){

		$cotacao_id = $_POST['cotacao_id'];
		$financeira_id = $_POST['financeira_id'];



		$this->layout = '//layouts/iframe';
		$criteria = new CDbCriteria;
		$criteria->condition = 'Cotacao_id = 1 AND Financeira_id = 2';
		$criteria->condition = "Cotacao_id = " . $cotacao_id;

		$cotacaoHasFinanceira = CotacaoHasFinanceira::model()->find( $criteria );

		echo "Cotacao_id" . $cotacaoHasFinanceira->Cotacao_id . '<br>';
		echo "Financeira_id" . $cotacaoHasFinanceira->Financeira_id;

		/*$cotacaoHasFinanceira->habilitado = 0;

		if ( $cotacaoHasFinanceira->save() ) {
			
			echo "tudo ok";

		}*/
	}

	public function actionAddFinanceira(){

		$this->layout = '//layouts/iframe';

	}

	public function actionLoadFinanceiras(){

		$this->layout = '//layouts/iframe';

		$cotacao = Cotacao::model()->findByPk( $_POST['id_cotacao'] );
		
		$this->render('listFinanceiras',
			array(
				'cotacoesAssociadas'=>$cotacao->financeirasAssociadas('in'),
				'cotacoesNaoAssociadas'=>$cotacao->financeirasAssociadas('out'),
				'cotacao'=>$cotacao
			)
		);
	}

	public function actionView($id){

		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	
	public function actionCreate(){
		
		$this->pageIsModal = true;
		$this->layout = '//layouts/blank_template';
		
		$baseUrl = Yii::app()->baseUrl; 
	    $cs = Yii::app()->getClientScript();
  	      	  
		$cotacao = new Cotacao;
		
		if( isset( $_POST['Cotacao'] ) ){
			
			$cotacao->attributes = $_POST['Cotacao'];
			$cotacao->data_cadastro = date('Y-m-d H:i:s');
			$cotacao->data_cadastro_br = date('d/m/Y H:i:s');
			$cotacao->Empresa_id = Yii::app()->session['usuario']->getEmpresa()->id;

			if ( $cotacao->save() ) {

				$sigacLog = new SigacLog;
				$sigacLog->saveLog('Cadastro de cotação','cotacao', $cotacao->id, date('Y-m-d H:i:s'),1, Yii::app()->session['usuario']->id, "Usuário ".Yii::app()->session['usuario']->username . " cadastrou a cotação " . $cotacao->descricao, "127.0.0.1",null, Yii::app()->session->getSessionId() );

				$arrFiliais = $_POST['Filiais'];

				for ( $i = 0; $i < sizeof( $arrFiliais ); $i++ ) { 

					$filialHasCotacao = new FilialHasCotacao;
					$filialHasCotacao->Cotacao_id = $cotacao->id;
					$filialHasCotacao->Filial_id = $arrFiliais[$i];
					$filialHasCotacao->data_cadastro = date('Y-m-d H:i:s');
					$filialHasCotacao->data_cadastro_br = date('d/m/Y H:i:s');
					$filialHasCotacao->save();
				}

				for ($x = 0; $x < sizeof( $_POST['Financeiras'] ); $x++) { 
					
					$cotacao_has_financeira = new CotacaoHasFinanceira;
					$cotacao_has_financeira->data_cadastro = date('Y-m-d H:i:s');
					$cotacao_has_financeira->data_cadastro_br = date('d/m/Y H:i:s');
					$cotacao_has_financeira->Financeira_id = $_POST['Financeiras'][$x];
					$cotacao_has_financeira->Cotacao_id = $cotacao->id;
					$cotacao_has_financeira->save();
				}

				echo CHtml::script("
					window.parent.$('#responsive').modal('hide');
					window.parent.$('#cru-frame').attr('src','');
					window.parent.$.fn.yiiGridView.update('fornecedores-grid');
				");

               	Yii::app()->end();
			}
		}
		
		$this->render('create',array(
			'model'=>$cotacao,
		));
	}

	
	public function actionUpdate($id){

		$model = $this->loadModel($id);
	
		if(isset($_POST['Cotacao'])){

			$model->attributes =$_POST['Cotacao'];

			if($model->save()){
				$this->redirect(array('view','id'=>$model->id));
			}

		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionDelete($id){

		$this->loadModel($id)->delete();

		if(!isset($_GET['ajax'])){
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}
	
	public function actionIndex(){

		$dataProvider = new CActiveDataProvider('Cotacao');

		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	
	public function actionAdmin(){

		$baseUrl = Yii::app()->baseUrl; 

	    $cs = Yii::app()->getClientScript();
  	    //$cs->registerScriptFile($baseUrl.'/js/jquery.min.js');
  	    $cs->registerScriptFile($baseUrl.'/js/form-elements.js',CClientScript::POS_END);
  	    $cs->registerScriptFile($baseUrl.'/js/select2.min.js',CClientScript::POS_END);
  	    $cs->registerScriptFile($baseUrl.'/js/jquery.inputlimiter.1.3.1.min.js',CClientScript::POS_END);
  	    $cs->registerScriptFile($baseUrl.'/js/jquery.autosize.min.js',CClientScript::POS_END);

  	    $cs->registerScriptFile($baseUrl.'/js/spin.min.js',CClientScript::POS_END);
  	    //$cs->registerScriptFile($baseUrl.'/js/ladda.js',CClientScript::POS_END);
  	    $cs->registerScriptFile($baseUrl.'/js/ladda.min.js',CClientScript::POS_END);
  	    $cs->registerScriptFile($baseUrl.'/js/bootstrap-switch.min.js',CClientScript::POS_END);
  	    $cs->registerScriptFile($baseUrl.'/js/ui-buttons.js',CClientScript::POS_END);

  	    $cs->registerScriptFile($baseUrl.'/js/bootstrap-modal.js',CClientScript::POS_END);
  	    $cs->registerScriptFile($baseUrl.'/js/bootstrap-modalmanager.js',CClientScript::POS_END);

  	    $cs->registerScriptFile($baseUrl.'/js/modal-crud-fn.js',CClientScript::POS_END);

  	    //$cs->registerScriptFile($baseUrl.'/js/jquery.maskedinput.js',CClientScript::POS_END);
  	    //$cs->registerScriptFile($baseUrl.'/js/jquery.maskMoney.js',CClientScript::POS_END);
  	   	//$cs->registerScriptFile($baseUrl.'/js/bootstrap-datepicker.js',CClientScript::POS_END);
  	    //$cs->registerScriptFile($baseUrl.'/js/bootstrap-timepicker.min.js',CClientScript::POS_END);
  	    //$cs->registerScriptFile($baseUrl.'/js/daterangepicker.js',CClientScript::POS_END);
  	    //$cs->registerScriptFile($baseUrl.'/js/bootstrap-datepicker.js',CClientScript::POS_END);
  	    //$cs->registerScriptFile($baseUrl.'/js/bootstrap-timepicker.min.js',CClientScript::POS_END);
  	    //$cs->registerScriptFile($baseUrl.'/js/bootstrap-colorpicker.js',CClientScript::POS_END);
  	    //$cs->registerScriptFile($baseUrl.'/js/jquery.tagsinput.js',CClientScript::POS_END);

  	    //$cs->registerScriptFile($baseUrl.'/js/jquery.validate.min.js',  CClientScript::POS_END);
  	    //$cs->registerScriptFile($baseUrl.'/js/summernote.min.js',  CClientScript::POS_END);
  	    //$cs->registerScriptFile($baseUrl.'/js/ckeditor/ckeditor.js',CClientScript::POS_END);
  	    //$cs->registerScriptFile($baseUrl.'/js/ckeditor/adapter/jquery.js',CClientScript::POS_END);
  	    //$cs->registerScriptFile($baseUrl.'/js/formEmpresaValidation.js',  CClientScript::POS_END);

  	    //$cs->registerScript('FormElements','FormValidator.init();');
  	    $cs->registerScript('FormElements','FormElements.init();');
  	    $cs->registerScript('FormElements','UIButtons.init();');
  	    //$cs->registerScript('FormElements','UIModals.init();');

		
		$model = new Cotacao('search');
		$model->unsetAttributes(); 

		if(isset($_GET['Cotacao'])){
			$model->attributes = $_GET['Cotacao'];
		}

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	
	public function loadModel($id){

		$model = Cotacao::model()->findByPk($id);

		if($model === null){
			throw new CHttpException(404,'The requested page does not exist.');
		}

		return $model;
	}
	
	protected function performAjaxValidation($model){

		if(isset($_POST['ajax']) && $_POST['ajax']==='cotacao-form'){

			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
