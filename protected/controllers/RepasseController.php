<?php

class RepasseController extends Controller
{	

	public $layout = '//layouts/main_sigac_template';

        public function filters(){
                return array(
                        'accessControl',
                        'postOnly + delete',
                );
        }

        public function accessRules(){

                return array(
                        array('allow',
                                'actions'               => array('index','getRepasses'),
                                'users'                 => array('@'),
                                'expression'    => 'Yii::app()->session["usuario"]->role == "financeiro"'
                        ),

                        array('deny',  // deny all users
                                'users'=>array('*'),
                        ),
                );
        }

	public function actionIndex()
	{
        	$baseUrl = Yii::app()->baseUrl;
                $cs = Yii::app()->getClientScript();

                $cs->registerCssFile($baseUrl.'/css/sigax_template/jquery.dataTables-1.10.0.css');
                $cs->registerCssFile($baseUrl.'/css/sigax_template/dataTables.tableTools.css');
                $cs->registerCssFile($baseUrl.'/css/sigax_template/datepicker.css');

                $cs->registerScriptFile($baseUrl .'/js/jquery.min.js');
                $cs->registerScriptFile($baseUrl .'/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
                $cs->registerScriptFile($baseUrl .'/js/dataTables.tableTools.js', CClientScript::POS_END);
                $cs->registerScriptFile($baseUrl .'/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
                $cs->registerScriptFile($baseUrl.'/js/bootstrap-datepicker.js',  CClientScript::POS_END);
                $cs->registerScriptFile($baseUrl.'/js/fn-repasses-grid.js',  CClientScript::POS_END);
                $cs->registerScriptFile($baseUrl.'/js/bootstrap-multiselect.js', CClientScript::POS_END);

                $cs->registerScript('datepicker',"$('.date-picker').datepicker({autoclose: true});");

        		$this->render('index');
	}

	public function actionGetRepasses()
	{
	       echo json_encode( Empresa::model()->repasse( $_POST['data_de'], $_POST['data_ate'], $_POST['filiais'], $_POST['draw'] ) );
	}
}