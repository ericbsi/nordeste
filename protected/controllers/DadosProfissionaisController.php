<?php

class DadosProfissionaisController extends Controller
{

	public function actionPersist()
	{
		echo json_encode( DadosProfissionais::model()->persist( $_POST['DadosProfissionais'], $_POST['EnderecoProfiss'], $_POST['TelefoneProfiss'], $_POST['clienteId'], $_POST['dadosProfissionaisId'], $_POST['ControllerAction'] ) );
	}

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionLoad(){

		$id 		= $_GET['entity_id'];
		echo json_encode( DadosProfissionais::model()->loadFormEdit($id) );
	}

	public function actionUpdate(){

		$action 	= $_POST['controller_action'];
		$msgReturn 	= "";

		if( $action == 'add' )
		{
			DadosProfissionais::model()->novo($_POST['DadosProfissionais'],$_POST['EnderecoProfiss'],$_POST['TelefoneProfiss'],$_POST['EmailProfiss'],$_POST['Cliente_id']);
			$msgReturn = "Dados cadastrados com sucesso!";
		}
		else
		{
			DadosProfissionais::model()->atualizar($_POST['DadosProfissionais'], $_POST['EnderecoProfiss'], $_POST['TelefoneProfiss'], $_POST['EmailProfiss'], $_POST['email_id'], $_POST['dados_profissionais_id']);
			$msgReturn 	= 'Dados atualizados com sucesso!';
		}

		echo json_encode(array(
			'msgReturn' => $msgReturn
		));
	}
}