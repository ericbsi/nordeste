<?php 

class ContabilController extends Controller {

	public $layout = '//layouts/main_sigac_template';

	public function filters(){
	
		return [
			'accessControl',
			'postOnly + delete'
		];
	}
	
	public function accessRules(){

		return [
			[
				'allow',
				'actions'	=> ['index','contratos','listarContratos'],
				'users'		=> ['@']
			],
			[
				'deny',
				'users'		=> ['*']
			]
		];
	}

	public function actionIndex()
	{
		$baseUrl 	= Yii::app()->baseUrl;
        $cs 		= Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');

		$this->render('index');
	}

	public function actionContratos()
	{
		$baseUrl 	= Yii::app()->baseUrl;
        $cs 		= Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js',      	CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', 			CClientScript::POS_END);
		$cs->registerScriptFile($baseUrl . '/js/contabil/fn-contabil-contratos.js', CClientScript::POS_END);

		$this->render('contratos');
	}

	public function actionListarContratos()
	{

		if( isset( $_POST['Nucleos'] ) )
		{
			$contabil 	= new Contabil;
			echo json_encode( $contabil->todasAsPropostas( $_POST['Nucleos'] ) );
		}

		else
		{
			echo json_encode([]);
		}
	}
}