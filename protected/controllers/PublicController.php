<?php

class PublicController extends Controller {

    public $layout = '';

    public function actionIndex() {
        
    }

    public function actionSegundaVia() {
        $this->layout = '//layouts/boleto_layout';

        $objRange = new Range;

        $parcelaAux = Parcela::model()->findByPk($_POST['ParcelasIds'][0]);
        $proposta = Proposta::model()->findByPk($parcelaAux->titulo->Proposta_id);

        if ($proposta->Banco_id == 5) {
            $boletoConfig = $objRange->getSegundaVia($_POST['ParcelasIds'], $proposta);
        } else if ($proposta->Banco_id == 7) {
            $boletoConfig = $objRange->getSegundaViaSantander($_POST['ParcelasIds'], $proposta);
        }

        $this->render('/boleto/index_segunda_via', array(
            'proposta' => $proposta,
            'boletoConfig' => $boletoConfig
        ));
    }

    public function actionBoletos() {
        $this->layout = '//layouts/segunda_via_boleto';
        $proposta = NULL;
        $postback = 0;

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/public/fn-search-boletos.js', CClientScript::POS_END);
        $cs->registerScript('cpf', 'jQuery(function($){$("input.cpfmask").mask("99999999999");});');

        if ((isset($_POST['cpf']) && $_POST['cpf'] != null) && (isset($_POST['codigo']) && $_POST['codigo'] != null)) {
            $proposta = Proposta::model()->propostaECliente($_POST['cpf'], $_POST['codigo']);
            $postback = 1;
        }

        $this->render('index', ['proposta' => $proposta, 'postback' => $postback]);
    }

    public function init() {
        
    }

    public function actionSegundaViaBoletos() {

        $this->layout = '//layouts/boleto_layout';

        $propostaId = $_POST['id'];
        $boletoConfig = null;

        $objRange = new Range;

        $proposta = Proposta::model()->findByPk($propostaId);

        $omniConfig = $proposta->hasOmniConfig();

        if ($omniConfig != NULL) {

            $this->redirect($omniConfig->urlBoleto);
        } else {
            
            if(in_array($proposta->Financeira_id, [10,11]))
            {
                
                if(!isset($proposta->Banco_id))
                {
                    $proposta->Banco_id = 3;
                    
                    if(!$proposta->update())
                    {
                        
                    }
                    
                }
                
            }
            
//            if ($proposta->Banco_id == 3) {
//                $boletoConfig = $objRange->getSegundaViaBradesco([], $proposta, 1);
//                $boletoConfig = $objRange->getSegundaViaBradesco([], $proposta, 0);
//            }
            
            if ($proposta->Banco_id == 5) {
                $boletoConfig = $objRange->getSegundaVia([], $proposta, 1);
            } else if ($proposta->Banco_id == 7) {
                $boletoConfig = $objRange->getSegundaViaSantander([], $proposta, 1);
            } else if ($proposta->Banco_id == 3) {
                $boletoConfig = $objRange->getSegundaViaBradesco([], $proposta, 1);
            } else if ($proposta->Banco_id == 10) {
                $boletoConfig = $objRange->getSegundaViaCredshow([], $proposta, 1);
            }

            $this->render('/boleto/index_segunda_via', array(
                'proposta' => $proposta,
                'boletoConfig' => $boletoConfig
            ));
        }
    }

}
