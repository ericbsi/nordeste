<?php

class RenegociacaoController extends Controller
{

    public $layout = '//layouts/main_sigac_template';

    public function filters()
    {

        return array(
            'accessControl',
            'postOnly + delete',
        );
    }

    public function accessRules()
    {

        return array    (
                            array   (
                                        'allow'                                                     ,
                                        'actions'       => array(
                                                                    'reimpressaoAjuste'         ,
                                                                    'listarPropostasAjuste'     ,
                                                                    'listarParcelasAjuste'      ,
                                                                    'gerarRangesParcelas'       ,
                                                                    'mudarVencimento'           ,
                                                                    'negociacao'                ,
                                                                    'contratosCPF'              ,
                                                                    'getValorParcela'           ,
                                                                    'anexarComprovanteBaixa'    ,
                                                                    'gerarEstornoParcela'
                                                                ),
                                        'users'         => array(
                                                                    '@'
                                                                ),
                                        'expression'    => 'Yii::app()->session["usuario"]->role == "empresa_admin" || in_array(Yii::app()->session["usuario"]->id,[713,714])'
                                    ),
                            array   (
                                        'deny'                  , // deny all users
                                        'users' => array(
                                                            '*'
                                                        ),
                                    ),
                        );
    }

    public function actionNegociacao(){
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css');


        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/renegociacao/fn-negociacao.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);

        $this->render('negociacaoPropostas');
    }

    public function actionContratosCPF(){
        $cpf = $_POST['cpf_cliente'];
        $data = [];
        if(isset($cpf) && $cpf != ''){
            $query = "  SELECT Pr.*, Analista.nome_utilizador           FROM Proposta       AS Pr
                    INNER JOIN Analise_de_Credito   AS AC       ON AC.id            = Pr.Analise_de_Credito_id AND AC.habilitado
                    INNER JOIN Cliente              AS C        ON C.id             = AC.Cliente_id AND C.habilitado
                    INNER JOIN Pessoa_has_Documento AS PHD      ON PHD.Pessoa_id    = C.Pessoa_id AND PHD.habilitado
                    INNER JOIN Documento            AS CPF      ON PHD.Documento_id = CPF.id AND CPF.numero = '" . $cpf . "' AND CPF.habilitado
                    INNER JOIN Usuario              AS Analista ON AC.Usuario_id    = Analista.id AND Analista.habilitado
                    WHERE Pr.habilitado";

            $contratos = Yii::app()->db->createCommand($query)->queryAll();

            $util = new Util;
            foreach ($contratos as $c) {
                $proposta = Proposta::model()->findByPk($c['id']);

                $row = array(
                    'codigo' => $proposta->codigo,
                    'cliente' => strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome),
                    'parcelas' => $proposta->qtd_parcelas . 'x R$ ' . number_format($proposta->valor_parcela, 2, ',', ''),
                    'negociar' => 'NEGOCIAR'
                );

                $data[] = $row;
            }
        }

        echo json_encode(array('data' => $data));
    }

    public function actionGerarRangesParcelas()
    {

        $retorno    =   [
                            "hasErrors" => 0                                ,
                            "type"      => "success"                        ,
                            "title"     => "Sucesso"                        ,
                            "msg"       => "Boletos gerados com sucesso"
                        ];

        if($_POST["idsParcelas"] && !empty($_POST["idsParcelas"]))
        {

            $idsParcelas    = $_POST["idsParcelas"];

            $transaction    = Yii::app()->db->beginTransaction();

            $zeros          = "";

            $mora           = ConfigLN::model()->valorDoParametro("taxa_mora") / 100;
            $util           = new Util;
            $configBradesco = Banco::model()->configuracoesBanco();

            foreach($idsParcelas as $idParcela)
            {

                $parcela = Parcela::model()->findByPk($idParcela);

                if ($parcela !== null)
                {

                    $range = Range::model()->findByPk(4);

                    if($range !== null)
                    {

                        $range->ultimo_gerado = $range->ultimo_gerado + 1;

                        for ($y = 1; $y <= 5 - strlen((string) $range->ultimo_gerado); $y ++)
                        {
                            $zeros .= "0";
                        }

                        $rangeSeq                       = strval($range->ultimo_gerado);
                        $rangeBoleto                    = $zeros . $rangeSeq;
                        $rangesGerados                  = new RangesGerados;
                        $rangesGerados->numero          = $rangeBoleto;
                        $rangesGerados->data_geracao    = date('Y-m-d H:i:s');
                        $rangesGerados->Range_id        = $range->id;
                        $rangesGerados->Parcela_id      = $parcela->id;
                        $rangesGerados->label           = strval($range->prefixo) . $zeros . $rangeSeq;
                        $rangesGerados->data_cadastro   = date('Y-m-d H:i:s');
                        $rangesGerados->habilitado      = 1;

                        $objProposta = Proposta::model()->find("habilitado AND id = " . $parcela->titulo->Proposta_id);

                        if($objProposta !== null)
                        {

                            if (in_array($objProposta->Financeira_id, [10, 11]))
                            {
                                $config = 'credshow_bradesco_fidic';
                                $inst7 = '';
                            }
                            else
                            {
                                $config = 'credshow_bradesco';
                                $inst7 = '';
                            }

                            if ($rangesGerados->save())
                            {

                                $bradesco                                                               = new Bradesco('2014-05-05', "0.00", strval($range->prefixo) . $zeros . $rangeSeq, $objProposta);
                                $cnabConfig                                                             = new CampoCnabConfig;
                                $cnabDetalhe                                                            = new CnabDetalhe;

                                $cnabDetalhe->cod_registro                                              = '1';
                                $cnabDetalhe->agencia_de_debito_bradesco                                = '00000';
                                $cnabDetalhe->digito_da_agencia_de_debito_bradesco                      = '0';
                                $cnabDetalhe->razao_da_conta_corrente_bradesco                          = '00000';
                                $cnabDetalhe->conta_corrente_bradesco                                   = '0000000';
                                $cnabDetalhe->digito_da_conta_corrente_bradesco                         = '0';
                                $cnabDetalhe->ident_da_empresa_beneficiaria_no_banco                    = $configBradesco[$config]['idBradesco'];
                                $cnabDetalhe->controle_participante                                     = $cnabConfig->cnabValorMonetario(strval($range->prefixo) . $zeros . $rangeSeq, 25);
                                //$cnabDetalhe->codigo_do_banco_debitado_bradesco                       = $configBradesco['credshow_bradesco']['codBradesco'];
                                $cnabDetalhe->codigo_do_banco_debitado_bradesco                         = '000';
                                $cnabDetalhe->campo_de_multa                                            = '0';
                                $cnabDetalhe->percentual_de_multa_bradesco                              = '0000';
                                $cnabDetalhe->ident_do_titulo_no_banco_bradesco                         = '0' . strval($range->prefixo) . $zeros . $rangeSeq;
                                $cnabDetalhe->digito_de_auto_conf_numero_bancario_bradesco              = $bradesco->getDigitoVerificador_nossonumero();
                                $cnabDetalhe->desconto_bonificacao_por_dia_bradesco                     = '0000000000';
                                $cnabDetalhe->emissao_da_papelete_bradesco                              = '2';
                                $cnabDetalhe->emite_para_debito_automatico_bradesco                     = 'N';
                                $cnabDetalhe->identificacao_da_operacao_do_banco_bradesco               = '';
                                $cnabDetalhe->indicador_rateio_credito_bradesco                         = 'R';
                                $cnabDetalhe->enderecamento_para_aviso_do_debito_aut_em_conta_corrente  = '0';
                                //$cnabDetalhe->branco_bradesco_2                                       = '';
                                $cnabDetalhe->cod_ocorrencia                                            = '01';
                                $cnabDetalhe->seu_numero                                                = strval($range->prefixo) . $zeros . $rangeSeq;
                                $cnabDetalhe->vencimento_texto                                          = $cnabConfig->cnabDate($parcela->vencimento); //OK;
                                $cnabDetalhe->valor_do_titulo_texto                                     = $cnabConfig->cnabValorMonetario(number_format((float) $objProposta->getValorParcela(), 2, '.', ''), 13);
                                //$cnabDetalhe->valor_do_titulo_texto                                   = $cnabConfig->cnabValorMonetario($parcela->valor_texto,13);
                                $cnabDetalhe->banco_cobrador                                            = '000';
                                $cnabDetalhe->agencia_depositaria                                       = '00000';
                                $cnabDetalhe->especie                                                   = '01';
                                $cnabDetalhe->aceite                                                    = 'N';
                                $cnabDetalhe->data_emissao_texto                                        = date('dmy');
                                $cnabDetalhe->data_emissao                                              = date('Y-m-d');
                                $cnabDetalhe->instrucao01                                               = '00';
                                $cnabDetalhe->instrucao02                                               = '00';
                                $cnabDetalhe->juros_de_mora_texto                                       = $cnabConfig->cnabValorMonetario($util->round_up(( $parcela->valor * $mora), 2), 13);
                                $cnabDetalhe->desconto_data_texto                                       = '000000';
                                $cnabDetalhe->valor_desconto_texto                                      = '0000000000000';
                                $cnabDetalhe->valor_iof_texto                                           = '0000000000000';
                                $cnabDetalhe->valor_abatimento_texto                                    = '0000000000000';
                                $cnabDetalhe->codigo_de_inscricao                                       = '01';
                                $cnabDetalhe->numero_de_inscricao                                       = '000' . $objProposta->analiseDeCredito->cliente->pessoa->getCPF()->numero;
                                $cnabDetalhe->nome_do_sacado                                            = substr($util->cleanStr($objProposta->analiseDeCredito->cliente->pessoa->nome, 'upp'), 0, 35);
                                $cnabDetalhe->endereco_do_pagador_sant                                  = substr($util->cleanStr($objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->logradouro, 'upp'), 0, 30) . ' ' . $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->numero;
                                $cnabDetalhe->cep_do_sacado                                             = substr($objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cep, 0, 5);
                                $cnabDetalhe->sufixo_do_cep                                             = substr($objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cep, 5);
                                $cnabDetalhe->numero_sequencial                                         = '000000';
                                $cnabDetalhe->mensagem_bradesco_um                                      = '';
                                $cnabDetalhe->sacador_avalista_ou_2_mensagem_bradesco                   = '';
                                $cnabDetalhe->Banco_id                                                  = 3;

                                if($cnabDetalhe->save())
                                {

                                    if(!$range->update())
                                    {

                                        ob_start();
                                        var_dump($range->getErrros());
                                        $erro = ob_get_clean();

                                        $retorno    =   [
                                                            "hasErrors" => 1                                                    ,
                                                            "type"      => "error"                                              ,
                                                            "title"     => "Erro"                                               ,
                                                            "msg"       => "Erro ao atualizar registro em Range. Erro: $erro"
                                                        ];

                                        $transaction->rollBack();

                                        break;

                                    }

                                }
                                else
                                {

                                    ob_start();
                                    var_dump($cnabDetalhe->getErrros());
                                    $erro = ob_get_clean();

                                    $retorno    =   [
                                                        "hasErrors" => 1                                                        ,
                                                        "type"      => "error"                                                  ,
                                                        "title"     => "Erro"                                                   ,
                                                        "msg"       => "Erro ao salvar registro em CnabDetalhe. Erro: $erro"
                                                    ];

                                    $transaction->rollBack();

                                    break;

                                }

                                $zeros = "";
                            }
                            else
                            {

                                ob_start();
                                var_dump($rangesGerados->getErrros());
                                $erro = ob_get_clean();

                                $retorno    =   [
                                                    "hasErrors" => 1                                                        ,
                                                    "type"      => "error"                                                  ,
                                                    "title"     => "Erro"                                                   ,
                                                    "msg"       => "Erro ao salvar registro em RangesGerados. Erro: $erro"
                                                ];

                                $transaction->rollBack();

                                break;

                            }

                        }
                        else
                        {

                            $retorno    =   [
                                                "hasErrors" => 1                                                    ,
                                                "type"      => "error"                                              ,
                                                "title"     => "Erro"                                               ,
                                                "msg"       => "Proposta não encontrada. ID Parcela: $idParcela "
                                                            .  "ID Proposta: $parcela->Titulo_id->Proposta_id"
                                            ];

                            $transaction->rollBack();

                            break;

                        }

                    }
                    else
                    {

                        $retorno    =   [
                                            "hasErrors" => 1                                ,
                                            "type"      => "error"                          ,
                                            "title"     => "Erro"                           ,
                                            "msg"       => "Range de ID 4 não encontrado"
                                        ];

                        $transaction->rollBack();

                        break;

                    }

                }
                else
                {

                    $retorno    =   [
                                        "hasErrors" => 1                                            ,
                                        "type"      => "error"                                      ,
                                        "title"     => "Erro"                                       ,
                                        "msg"       => "Parcela de ID $idParcela não encontrada"
                                    ];

                    $transaction->rollBack();

                    break;

                }

            }

            if(!$retorno["hasErrors"])
            {
                $transaction->commit();
            }

        }
        else
        {

            $retorno    =   [
                                "hasErrors" => 0                            ,
                                "type"      => "alert"                      ,
                                "title"     => "Alerta"                     ,
                                "msg"       => "Nenhuma parcela enviada."
                            ];

        }

        echo json_encode($retorno);

    }

    public function actionReimpressaoAjuste()
    {

        $baseUrl    = Yii::app()->baseUrl                                                                                   ;
        $cs         = Yii::app()->getClientScript()                                                                         ;

        $cs->registerCssFile($baseUrl       . '/css/sigax_template/jquery.dataTables-1.10.0.css'                        )   ;
        $cs->registerCssFile($baseUrl       . '/css/sigax_template/dataTables.tableTools.css'                           )   ;
        $cs->registerCssFile($baseUrl       . '/css/sigax_template/iCheck/skins/all.css'                                )   ;


        $cs->registerScriptFile($baseUrl    . '/js/jquery.min.js'                                                       )   ;
        $cs->registerScriptFile($baseUrl    . '/js/jquery.dataTables-1.10.0.js'             , CClientScript::POS_END    )   ;
        $cs->registerScriptFile($baseUrl    . '/js/dataTables.fnReloadAjax.js'              , CClientScript::POS_END    )   ;
        $cs->registerScriptFile($baseUrl    . '/js/select2.min.js'                          , CClientScript::POS_END    )   ;
        $cs->registerScriptFile($baseUrl    . '/js/jquery.maskedinput.js'                   , CClientScript::POS_END    )   ;
        $cs->registerScriptFile($baseUrl    . '/js/bootstrap-modal.js'                      , CClientScript::POS_END    )   ;
        $cs->registerScriptFile($baseUrl    . '/js/bootstrap-modalmanager.js'               , CClientScript::POS_END    )   ;
        $cs->registerScriptFile($baseUrl    . '/js/jquery.validate.12.js'                   , CClientScript::POS_END    )   ;
        $cs->registerScriptFile($baseUrl    . '/js/additional-methods.js'                   , CClientScript::POS_END    )   ;
        $cs->registerScriptFile($baseUrl    . '/js/renegociacao/fn-reimpressaoAjuste.js'    , CClientScript::POS_END    )   ;
        $cs->registerScriptFile($baseUrl    . '/js/jquery.icheck.min.js'                    , CClientScript::POS_END    )   ;
        $cs->registerScriptFile($baseUrl    . '/js/jquery.blockUI.js'                       , CClientScript::POS_END    )   ;
        $cs->registerScriptFile($baseUrl    . '/js/jquery.form.js'                          , CClientScript::POS_END    )   ;
        $cs->registerScriptFile($baseUrl    . '/js/blockInterface.js'                       , CClientScript::POS_END    )   ;

        $this->render('reimpressaoAjuste');
    }

    public function actionListarPropostasAjuste()
    {

        $dados              = []    ;

        $erro               = ""    ;

        $recordsTotal       = 0     ;
        $recordsFiltered    = 0     ;

        $sql = "SELECT P.id, COUNT(DISTINCT RG.id) AS QTDRG, P.qtd_parcelas, UPPER(Pe.nome) AS nome, D.numero AS CPF
                FROM 		Proposta                AS P
                INNER   JOIN    Analise_de_Credito      AS AC   ON  AC.habilitado AND   P.Analise_de_Credito_id =  AC.id
                INNER   JOIN    Cliente                 AS C    ON   C.habilitado AND  AC.Cliente_id            =   C.id
                INNER   JOIN    Pessoa                  AS Pe   ON  Pe.habilitado AND   C.Pessoa_id             =  Pe.id
                INNER   JOIN    Pessoa_has_Documento    AS PhD  ON PhD.habilitado AND PhD.Pessoa_id             =  Pe.id
                INNER   JOIN    Documento               AS D    ON   D.habilitado AND PhD.Documento_id          =   D.id    AND D.Tipo_documento_id = 1
                INNER   JOIN    Titulo                  AS T    ON   T.habilitado AND   T.Proposta_id           =   P.id    AND T.NaturezaTitulo_id = 1
                INNER   JOIN    Parcela                 AS Pa   ON  Pa.habilitado AND  Pa.Titulo_id             =   T.id
                LEFT    JOIN    Ranges_gerados          AS RG   ON  RG.habilitado AND  RG.Parcela_id            =  Pa.id
                WHERE P.habilitado AND /*P.titulos_gerados AND P.Financeira_id = 11 AND*/   P.Status_Proposta_id IN (2,7) ";

        $sqlGroup = "GROUP BY P.id, P.qtd_parcelas, Pe.nome, D.numero
                     /*HAVING P.qtd_parcelas > QTDRG*/ ";

        $retorno = [];

        try
        {

            $resultado = Yii::app()->db->createCommand($sql . $sqlGroup)->queryAll();

            $recordsTotal   = count($resultado);

            if(isset($_POST["search"]))
            {

                $search = $_POST["search"];

                if(isset($search["value"]) && !empty($search["value"]))
                {

                    $sql .= " AND D.numero LIKE '%" . $search["value"] . "%' ";

                }

            }

            $resultado          = Yii::app()->db->createCommand($sql . $sqlGroup)->queryAll();

            $recordsFiltered    = count($resultado);

            $sql .= $sqlGroup;

            $sql .= " LIMIT " . $_POST["start"] . ", 10 ";

            $resultado          = Yii::app()->db->createCommand($sql)->queryAll();

            foreach ($resultado as $r)
            {

                $proposta = Proposta::model()->findByPk($r["id"]);

                if($proposta !== null)
                {

                    $dateTime       = new DateTime      ($proposta->data_cadastro                       );
                    $dataCadastro   = $dateTime->format ("d/m/Y"                                        );

                    $valor          = number_format     ($proposta->valor               , 2, ",", "."   );
                    $entrada        = number_format     ($proposta->valor_entrada       , 2, ",", "."   );
                    $vlrParcela     = number_format     ($proposta->getValorParcela()   , 2, ",", "."   );

                    $dados[]    =   [
                                        "idProposta"    => $proposta->id                                    ,
                                        "codigo"        => $proposta->codigo                                ,
                                        "dataProposta"  => $dataCadastro                                    ,
                                        "nomeFilial"    => $proposta->analiseDeCredito->filial->getConcat() ,
                                        "cliente"       => $r["nome"]                                       ,
                                        "cpf"           => $r["CPF"]                                        ,
                                        "valor"         => $valor                                           ,
                                        "entrada"       => $entrada                                         ,
                                        "qtdParcelas"   => $r["qtd_parcelas"]                               ,
                                        "vlrParcela"    => $vlrParcela                                      ,
                                    ];


                }

            }

        }
        catch (Exception $ex)
        {
            $erro = $ex->getMessage();
        }

        $retorno =  [
                        "data"              => $dados           ,
                        "erro"              => $erro            ,
                        "recordsTotal"      => $recordsTotal    ,
                        "recordsFiltered"   => $recordsFiltered ,
                        "sql"               => $sql
                    ];

        echo json_encode($retorno);

    }

    public function actionListarParcelasAjuste()
    {

        $dados      = [];

        $retorno    = [];
        
        $sql        = "";
        
        if(isset($_POST["idProposta"]) && $_POST["idProposta"] !== null)
        {

            $proposta = Proposta::model()->findByPk($_POST["idProposta"]);

            if($proposta !== null)
            {

                $titulo = Titulo::model()->find("habilitado AND Proposta_id = $proposta->id AND NaturezaTitulo_id = 1");

                if($titulo !== null)
                {

                    $parcelas = Parcela::model()->findAll("habilitado AND Titulo_id = $titulo->id");

                    foreach ($parcelas as $parcela)
                    {
                        
                        $idEstorno          = $parcela->id          ;
                        
                        $alteraVencimento   = 0                     ;
                        
                        $checkParcela       = ""                    ;
                            
                        $disabledImprimir   = ""                    ;
                        $disabledGerar      = ""                    ;
                        $disabledBaixar     = ""                    ;
                        $disabledEstornar   = ""                    ;
                        $iClassBaixa        = "clip-history"        ;
                        $btnCorBaixa        = "dark-grey"           ;
                        $tituloEstorno      = "Estornar Baixa"      ;
                        $classEstorno       = "estornarBaixaBtn"    ;
                        
                        $range              = RangesGerados::model()->find("habilitado AND Parcela_id = $parcela->id");

                        if($range == null)
                        {
                            
                            $baixa = Baixa::model()->find("Parcela_id = $parcela->id");
                            
                            if($baixa !== null)
                            {
                                
                                if($baixa->baixado)
                                {
                                    $disabledBaixar     = "disabled"    ;
                                    $disabledGerar      = "disabled"    ;
                                }
                                
                                $sql    = "SELECT       IFNULL(MAX(StatusEstorno_id),0) AS idEstorno,                                                       "
                                        . "             EB.id                           AS id                                                               "
                                        . "FROM         EstornoBaixa_has_StatusEstorno  AS EBhSE                                                            "
                                        . "INNER JOIN   EstornoBaixa                    AS EB           ON EB.habilitado AND EB.id = EBhSE.EstornoBaixa_id  "
                                        . "WHERE EBhSE.habilitado AND EB.Baixa_id = $baixa->id                                                              "
                                        . "GROUP BY EB.id";

                                $resultado = Yii::app()->db->createCommand($sql)->queryRow();
                                
                                if(isset($resultado["id"]))
                                {
                                    $idEstorno = $resultado["id"];
                                }

                                if($resultado["idEstorno"] == "1")
                                {
                                    //$disabledEstornar   = "disabled"                    ;
                                    $disabledBaixar     = "disabled"                    ;
                                    $disabledGerar      = "disabled"                    ;
                                    $disabledImprimir   = "disabled"                    ;
                                    $iClassBaixa        = "fa fa-exclamation-triangle"  ;
                                    $btnCorBaixa        = "orange"                      ;
                                    $tituloEstorno      = "Estorno Solicitado"          ;
                                    $classEstorno       = "btnMensagens"                ;
                                }
                                else if($resultado["idEstorno"] == "2")
                                {
                                    $iClassBaixa        = "clip-checkmark-circle"       ;
                                    $btnCorBaixa        = "blue"                        ;
                                    $tituloEstorno      = "Estorno Aprovado"            ;
                                    $classEstorno       = "btnMensagens"                ;
                                    
                                }
                                else if($resultado["idEstorno"] == "3")
                                {
                                    $iClassBaixa        = "clip-cancel-circle"          ;
                                    $btnCorBaixa        = "red"                         ;
                                    $tituloEstorno      = "Estorno Negado"              ;
                                    $classEstorno       = "btnMensagens"                ;
                                    
                                }

                            }
                            else
                            {
                                $disabledEstornar   = "disabled"    ;
                            }

                            $classCheck         = "gerarRange"  ;
                            $disabledImprimir   = "disabled"    ;

                            $alteraVencimento   = 1             ;

                        }
                        else
                        {
                            
                            $baixa = Baixa::model()->find("Parcela_id = $parcela->id");
                            
                            if($baixa !== null)
                            {
                                
                                if($baixa->baixado)
                                {
                                    $disabledBaixar     = "disabled"    ;
                                }
                                
                                $sql    = "SELECT       IFNULL(MAX(StatusEstorno_id),0) AS idEstorno,                                                       "
                                        . "             EB.id                           AS id                                                               "
                                        . "FROM         EstornoBaixa_has_StatusEstorno  AS EBhSE                                                            "
                                        . "INNER JOIN   EstornoBaixa                    AS EB           ON EB.habilitado AND EB.id = EBhSE.EstornoBaixa_id  "
                                        . "WHERE EBhSE.habilitado AND EB.Baixa_id = $baixa->id "
                                        . "GROUP BY EB.id";

                                $resultado = Yii::app()->db->createCommand($sql)->queryRow();
                                
                                if(isset($resultado["id"]))
                                {
                                    $idEstorno = $resultado["id"];
                                }

                                if($resultado["idEstorno"] == "1")
                                {
//                                    $disabledEstornar   = "disabled"                    ;
                                    $disabledBaixar     = "disabled"                    ;
                                    $disabledGerar      = "disabled"                    ;
                                    $disabledImprimir   = "disabled"                    ;
                                    $iClassBaixa        = "fa fa-exclamation-triangle"  ;
                                    $btnCorBaixa        = "orange"                      ;
                                    $tituloEstorno      = "Estorno Solicitado"          ;
                                    $classEstorno       = "btnMensagens "               ;
                                }
                                else if($resultado["idEstorno"] == "2")
                                {
                                    $iClassBaixa        = "clip-checkmark-circle"       ;
                                    $btnCorBaixa        = "blue"                        ;
                                    $tituloEstorno      = "Estorno Aprovado"            ;
                                    $classEstorno       = "btnMensagens"                ;
                                    
                                }
                                else if($resultado["idEstorno"] == "3")
                                {
                                    $iClassBaixa        = "clip-cancel-circle"          ;
                                    $btnCorBaixa        = "red"                         ;
                                    $tituloEstorno      = "Estorno Negado"              ;
                                    $classEstorno       = "btnMensagens"                ;
                                    
                                }

                            }
                            else
                            {
                                $disabledEstornar   = "disabled"    ;
                            }

                            $classCheck         = "imprimirBoleto"  ;

                            $disabledGerar      = "disabled"        ;
/*                            $disabledBaixar     = "disabled"        ;*/
                        }

                        /*$checkParcela       = "<button class='btn btn-blue $classCheck'value='$parcela->id'>"
                                            . "     <i class='clip-checkbox-unchecked checkParcela'></i>"
                                            . "</button>";*/
                        
                        $btnGerarRange      = "<button class='btn btn-yellow        gerarRangeBtn'      $disabledGerar      value='$parcela->id'  title='Gerar Boleto'>"
                                            . "     <i class='clip-spinner'         ></i>"
                                            . "</button>";
                        
                        $btnImprimirBoleto  = "<button class='btn btn-green         imprimirBoletoBtn'  $disabledImprimir   value='$parcela->id'  title='Imprimir Boleto'>"
                                            . "     <i class='clip-file-pdf'        ></i>"
                                            . "</button>";
                        
                        $btnBaixarParcela   = "<button class='btn btn-purple        baixarParcelaBtn'   $disabledBaixar     value='$parcela->id'  title='Baixar Parcela'>"
                                            . "     <i class='fa fa-credit-card'    ></i>"
                                            . "</button>";
                        
                        $btnEstornarBaixa   = "<button class='btn btn-$btnCorBaixa  $classEstorno'      $disabledEstornar   value='$idEstorno'    title='$tituloEstorno'>"
                                            . "     <i class='$iClassBaixa'         ></i>"
                                            . "</button>";

                        $valorParcela   = "R$ " . number_format($parcela->valor,2,",",".")  ;

                        $dateTime       = new DateTime($parcela->vencimento)                ;

                        $vencimento     = $dateTime->format("d/m/Y")                        ;

                        $dados[] =  [
                                        "idParcela"         => $parcela->id         ,
                                        "alteraVencimento"  => $alteraVencimento    ,
                                        "checkParcela"      => $checkParcela        ,
                                        "seq"               => $parcela->seq        ,
                                        "vencimento"        => $vencimento          ,
                                        "valor"             => $valorParcela        ,
                                        "btnGerarRange"     => $btnGerarRange       ,
                                        "btnImprimirBoleto" => $btnImprimirBoleto   ,
                                        "btnBaixarParcela"  => $btnBaixarParcela    ,
                                        "btnEstornarBaixa"  => $btnEstornarBaixa    ,
                                        "sql"   => $sql
                                    ];

                    }

                }

            }

        }

        $retorno    =   [
                            "data"  => $dados   
                        ];

        echo json_encode($retorno);
    }

    public function actionMudarVencimento()
    {

        $retorno =  [
                        "hasErrors" => 0                                    ,
                        "type"      => "success"                            ,
                        "title"     => "Sucesso"                            ,
                        "msg"       => "Vencimento alterado com sucesso"
                    ];

        if(isset($_POST["idParcela"]) && isset($_POST["vencimento"]))
        {

            $parcela = Parcela::model()->find("habilitado AND id = " . $_POST["idParcela"]);

            if($parcela !== null)
            {

                $parcela->vencimento = $_POST["vencimento"];

                if(!$parcela->update())
                {

                    ob_start();
                    var_dump($parcela->getErrors());

                    $erro = ob_get_clean();

                    $retorno =  [
                                    "hasErrors" =>  1                                   ,
                                    "type"      =>  "error"                             ,
                                    "title"     =>  "Erro ao atualizar o vencimento"    ,
                                    "msg"       =>  $erro
                                ];

                }

            }
            else
            {

                $retorno =  [
                                "hasErrors" =>  1                                           ,
                                "type"      =>  "error"                                     ,
                                "title"     =>  "Parcela não encontrada"                    ,
                                "msg"       =>  "Parcela de id " . $_POST["idParcela"]  .
                                                " não encontrada."
                            ];

            }

        }
        else
        {

            $retorno =  [
                            "hasErrors" => 1            ,
                            "type"      => "alert"      ,
                            "title"     => "Alerta"     ,
                            "msg"       => "POST vazio"
                        ];

        }

        echo json_encode($retorno);

    }

    public function actionGetValorParcela()
    {

        $retorno =  [
                        "hasErrors"     => 0            ,
                        "type"          => "success"    ,
                        "msg"           => ""           ,
                        "valorParcela"  => 0            ,
                    ];

        if(isset($_POST["idParcela"]))
        {

            $parcela = Parcela::model()->find("habilitado AND id = " . $_POST["idParcela"]);

            if($parcela !== null)
            {
                $retorno["valorParcela"] = $parcela->valor;
            }
            else
            {

                $retorno =  [
                                "hasErrors"     => 1                                            ,
                                "type"          => "error"                                      ,
                                "msg"           => "Parcela de id " . $_POST["idParcela"] . " "
                                                .  "não encontrada."
                            ];

            }

        }
        else
        {

            $retorno =  [
                            "hasErrors"     => 1                                            ,
                            "type"          => "error"                                      ,
                            "msg"           => "Faltou id da Parcela no POST. "
                                            .  "Não foi possível buscar o valor da Parcela"
                        ];

        }

        echo json_encode($retorno);

    }

    public function actionAnexarComprovanteBaixa()
    {

        $arrReturn =    array   (
                                    "hasError"  => true                 ,
                                    "msg"       => "Escolha um arquivo" ,
                                    "tipo"      => "error"
                                );

        $transaction = Yii::app()->db->beginTransaction();

        try
        {

            if (isset($_FILES['anexo_importacao']))
            {

                if(isset($_POST['idParcelaHdn']))
                {

                    $parcela = Parcela::model()->find("habilitado AND id = " . $_POST["idParcelaHdn"]);

                    if($parcela !== null)
                    {

                        if(isset($_POST["valorPago"]))
                        {

                            //$valorPago = (double) strtr(trim($_POST["valorPago"]), [",","."]);
                            $valorPago = $_POST["valorPago"];

                            if($valorPago > 0)
                            {

                                if(isset($_POST["dataOcorrencia"]) && $_POST["dataOcorrencia"] <= date("Y-m-d"))
                                {

                                    $parcela->valor_atual = $_POST["valorPago"];

                                    if($parcela->update())
                                    {

                                        $baixa                      = new Baixa                                                 ;
                                        $baixa->baixado             = 1                                                         ;
                                        $baixa->valor_pago          = $_POST["valorPago"        ]                               ;
                                        $baixa->valor_abatimento    = $_POST["valorPago"        ]                               ;
                                        $baixa->data_da_ocorrencia  = $_POST["dataOcorrencia"   ]                               ;
                                        $baixa->data_da_baixa       = date("Y-m-d")                                             ;
                                        $baixa->Parcela_id          = $parcela->id                                              ;
                                        $baixa->Filial_id           = $parcela->titulo->proposta->analiseDeCredito->Filial_id   ;
                                        $baixa->Cliente_id          = $parcela->titulo->proposta->analiseDeCredito->Cliente_id  ;

                                        if ($baixa->save())
                                        {
                                            $s3Client = new S3Client;

                                            $retornoS3 = $s3Client->put( $_FILES["anexo_importacao"], "Baixa", $baixa->id, $_FILES["anexo_importacao"]["name"] );

                                            if (!$retornoS3["hasErrors"])
                                            {

                                                $arrReturn =    array   (
                                                                            "hasError"  => false                        ,
                                                                            "msg"       => "Baixa efetuada com sucesso" ,
                                                                            "tipo"      => "success"
                                                                        );

                                                $transaction->commit();

                                            }
                                            else
                                            {

                                                $arrReturn =    array   (
                                                                            "hasError"  => true                     ,
                                                                            "msg"       => $retornoS3["msgReturn"]  ,
                                                                            "tipo"      => "error"
                                                                        );

                                            }

                                        }
                                        else
                                        {

                                            ob_start();
                                            var_dump($baixa->getErrors());
                                            $erroBaixa = ob_get_clean();

                                            $arrReturn =    array   (
                                                                        "hasError"  => true         ,
                                                                        "msg"       => $erroBaixa   ,
                                                                        "tipo"      => "error"
                                                                    );

                                        }

                                    }
                                    else
                                    {

                                        ob_start();
                                        var_dump($parcela->getErrors());
                                        $erroParcela = ob_get_clean();

                                        $arrReturn =    array   (
                                                                    "hasError"  => true         ,
                                                                    "msg"       => $erroParcela ,
                                                                    "tipo"      => "error"
                                                                );

                                    }

                                }
                                else
                                {

                                    $arrReturn =    array   (
                                                                "hasError"  => true                                                             ,
                                                                "msg"       => "Data deve ser preeenchida e não pode exceder a data de hoje"    ,
                                                                "tipo"      => "error"
                                                            );

                                }
                            }
                            else
                            {

                                $arrReturn =    array   (
                                                            "hasError"  => true                                 ,
                                                            "msg"       => "Valor pago deve ser maior que 0"    ,
                                                            "tipo"      => "error"
                                                        );

                            }

                        }
                        else
                        {

                            $arrReturn =    array   (
                                                        "hasError"  => true                                 ,
                                                        "msg"       => "Valor pago deve ser maior que 0"    ,
                                                        "tipo"      => "error"
                                                    );

                        }

                    }
                    else
                    {

                        $arrReturn =    array   (
                                                    "hasError"  => true                                                             ,
                                                    "msg"       => "Parcela de ID " . $_POST["idParcelaHdn"] . " não encontrada."   ,
                                                    "tipo"      => "error"
                                                );

                    }

                }
                else
                {

                    ob_start();
                    var_dump($_POST);
                    //var_dump($_FILES);
                    $erroPOST = ob_get_clean();

                    $arrReturn =    array   (
                                                "hasError"  =>  true                                    ,
                                                "msg"       =>  "Id da Parcela não enviado no POST "
                                                            .   " " .$erroPOST                          ,
                                                "tipo"      =>  "error"
                                            );

                }

            }

        }
        catch (Exception $ex)
        {

            $arrReturn =    array   (
                                        "hasError"  => true                 ,
                                        "msg"       => $ex->getMessage()    ,
                                        "tipo"      => "error"
                                    );

        }

        echo json_encode($arrReturn);

    }

    public function actionGerarEstornoParcela()
    {

        $retorno =  [
                        "hasErrors" => FALSE                        ,
                        "title"     => "Sucesso"                    ,
                        "type"      => "success"                    ,
                        "msg"       => "Estorno gerado com sucesso"
                    ];

        if(isset($_POST["idParcela"]) && !empty($_POST["idParcela"]))
        {

            $idParcela = $_POST["idParcela"];

            if(isset($_POST["observacao"]) && !empty($_POST["observacao"]))
            {

                $observacao = $_POST["observacao"];

                $baixa = Baixa::model()->find("baixado AND Parcela_id = $idParcela");

                if($baixa !== null)
                {

                    $estornoBaixa                   = new EstornoBaixa                      ;
                    $estornoBaixa->habilitado       = 1                                     ;
                    $estornoBaixa->data_cadastro    = date("Y-m-d H:i:s")                   ;
                    $estornoBaixa->Baixa_id         = $baixa->id                            ;
                    #$estornoBaixa->Solicitante      = Yii::app()->session["usuario"]->id    ;

                    if($estornoBaixa->save())
                    {

                        $EBHasSE                    = new EstornoBaixaHasStatusEstorno      ;
                        $EBHasSE->EstornoBaixa_id   = $estornoBaixa->id                     ;
                        $EBHasSE->StatusEstorno_id  = 1                                     ;
                        $EBHasSE->Usuario_id        = Yii::app()->session["usuario"]->id    ;
                        $EBHasSE->data_cadastro     = date("Y-m-d H:i:s")                   ;
                        $EBHasSE->habilitado        = 1                                     ;
                        $EBHasSE->observacao        = $observacao                           ;

                        if(!$EBHasSE->save())
                        {

                            $msgErro = chr(13) . chr(10) . "";

                            $erros = $EBHasSE->getErrors();

                            foreach ($erros as $e)
                            {
                                $msgErro .= $e[0] . chr(13) . chr(10);

                            }

                            $retorno =  [
                                            "hasErrors" => true                             ,
                                            "type"      => "error"                          ,
                                            "title"     => "Erro"                           ,
                                            "msg"       => "Erros: $msgErro"
                                        ];

                        }

                    }
                    else
                    {

                        $msgErro = chr(13) . chr(10) . "";

                        $erros = $estornoBaixa->getErrors();

                        foreach ($erros as $e)
                        {
                            $msgErro .= $e[0] . chr(13) . chr(10);

                        }

                        $retorno =  [
                                        "hasErrors" => true                             ,
                                        "type"      => "error"                          ,
                                        "title"     => "Erro"                           ,
                                        "msg"       => "Erros: $msgErro"
                                    ];

                    }

                }
                else
                {

                    $retorno =  [
                                    "hasErrors" => true                             ,
                                    "type"      => "error"                          ,
                                    "title"     => "Erro"                           ,
                                    "msg"       => "Parcela não continha baixa!"
                                ];

                }

            }
            else
            {

                $retorno =  [
                                "hasErrors" => true                                     ,
                                "type"      => "warning"                                ,
                                "title"     => "Alerta"                                 ,
                                "msg"       => "Por favor, adicione alguma observacao!"
                            ];

            }

        }
        else
        {

            $retorno =  [
                            "hasErrors" => true                             ,
                            "type"      => "error"                          ,
                            "title"     => "Erro"                           ,
                            "msg"       => "ID da parcela não encontrado"
                        ];

        }

        echo json_encode($retorno);

    }

}
