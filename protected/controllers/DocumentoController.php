<?php

class DocumentoController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionAdd(){

		//Documento::model()->novo($_POST['Documento'],$_POST['Cliente_id']);
		/*
		echo json_encode(array(
			'msgReturn' => 'Documento cadastrado com sucesso!'
		));*/

		echo print_r($_POST);
	}

	public function actionLoad(){

		$id 		= $_GET['entity_id'];

		echo json_encode( Documento::model()->loadFormEdit($id) );
	}

	public function actionUpdate(){

		$action 	= $_POST['controller_action'];
		$msgReturn 	= "";

		if( $action == 'add' )
		{
			Documento::model()->novo($_POST['Documento'],$_POST['Cliente_id']);
			$msgReturn = "Documento cadastrado com sucesso!";
		}
		else
		{
			Documento::model()->atualizar($_POST['Documento'], $_POST['documento_id']);
			$msgReturn = "Documento atualizado com sucesso!";
		}

		echo json_encode(array(
			'msgReturn' => $msgReturn
		));
	}

	public function actionEditAttr()
	{
		echo json_encode( Documento::model()->editarAtributo( $_POST['pk'], $_POST['name'], $_POST['value'] ) );
	}
}