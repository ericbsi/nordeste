<?php

class TituloController extends Controller {

    public $layout = '//layouts/main_sigac_template';

    public function actionIndex() {
        
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js', CClientScript::POS_END);
        
        $cs->registerScriptFile($baseUrl . '/js/fn-titulos-grid.js', CClientScript::POS_END);
        
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.pnotify.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/bootstrap-multiselect.js', CClientScript::POS_END);

        $this->render('grid_titulos');
    }

    public function accessRules() {

        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('add', 'editar', 'listarTitulos', 'create', 'update', 'admin', 'delete', 'index', 'view', 'teste'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    public function actionTeste()
    {
        $compra = Compra::model()->findByPK(7);
        
        var_dump($compra);
        
        var_dump($compra->getValorTotal());
    }

    public function actionGetTitulos() {

        echo json_encode(Titulo::model()->listar($_POST['draw'], $_POST['start'], $_POST['order'], $_POST['columns']));
        
        //Titulo::model()->listar();
        
    }

}