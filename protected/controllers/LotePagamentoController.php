<?php

class LotePagamentoController extends Controller {

    public $layout = '//layouts/main_sigac_template';

    public function filters() {

        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules() 
    {

        return array(
            array('allow',
                  'actions' => array('pagar','getDadosBancarios','getDadosBancariosPagos','anexarComprovante'),
                  'users' => array('@'),
                  'expression' => 'Yii::app()->session["usuario"]->role == "financeiro" || Yii::app()->session["usuario"]->role == "parceiros_admin"'
            ),
            array('allow',
                'actions' => array(
                    'getDadosBancariosPagos'    ,
                    'getPropostas'              ,
                ),
                'users' => array('@'),
                'expression'    =>  'Yii::app()->session["usuario"]->role == "financeiro"       ||  '
                                .   'Yii::app()->session["usuario"]->role == "empresa_admin"    ||  '
                                .   'Yii::app()->session["usuario"]->role == "parceiros_admin"      '
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

   public function actionAnexarComprovante()
   {
      echo json_encode  ( 
                           DadosPagamento::model()->anexarComprovante( 
                                                                        $_POST['DadosPagamentoId'] , 
                                                                        $_FILES['ComprovanteFile'] , 
                                                                        'Comprovante de pagamento' 
                                                                     ) 
                        );
   }

    public function actionPagar()
    { 
        if (!empty($_POST['idLote']))
        {

            $retorno = LotePagamento::model()->pagar    (
                                                            $_POST['idLote'         ], 
                                                            $_POST['contaDebito'    ], 
                                                            $_POST['valorPago'      ], 
                                                            $_POST['comprovante'    ], 
                                                            $_POST['dataPagamento'  ], 
                                                            $_POST['contaCredito'   ], 
                                                            $_POST['observacao'     ]
                                                        );
        }
        else
        {

            $retorno = array(
                                'hasErrors'   => 1                                             ,
                                'msg'         => 'Ocorreu um erro. Id do Lote não encontrado.' ,
                                'pntfyClass'  => 'error'
                            );
        }

        echo json_encode($retorno);
    }

   public function actionGetDadosBancarios()
   {

      $resposta = LotePagamento::model()->getDadosBancarios (
                                                               $_POST['idLote']
                                                            );

      echo json_encode(
         $resposta
      );
   }

   public function actionGetDadosBancariosPagos()
   {

      $resposta = LotePagamento::model()->getDadosBancariosPagos  (
                                                                     $_POST['idLote']
                                                                  );

      echo json_encode(
         $resposta
      );
   }

    public function actionGetPropostas() 
    {
        
        $idBordero = 0;
        
        $dados = [];
        
        if(isset($_POST['idBordero']) && !empty($_POST['idBordero']))
        {
            $idBordero = $_POST['idBordero'];
        }

        $Qry = " SELECT P.id ";
        $Qry .= " FROM          Proposta            as P                                                            ";
        $Qry .= " INNER JOIN    Analise_de_Credito  as AC ON AC.habilitado  AND AC.id =  P.Analise_de_Credito_id    ";
        $Qry .= " INNER JOIN    ItemDoBordero       as IB ON  P.id = IB.Proposta_id AND IB.habilitado             ";
        //$Qry .= "INNER JOIN   Lote_has_Bordero        AS LB ON IB.Bordero          = LB.Bordero_id                AND LB.habilitado";//tira isso daqui
        $Qry .= " INNER JOIN    Bordero             as B  ON  B.habilitado  AND  B.id = IB.Bordero                  ";
        $Qry .= " WHERE P.habilitado AND B.id = $idBordero "; //volta isso
//        $Qry .= " WHERE P.habilitado AND LB.Lote_id = 1830 "; //tira isso

        $dadosTotal = Yii::app()->db->createCommand($Qry)->queryAll();
        
        $Qry .= "LIMIT " . $_POST["start"] . ", 10";

        $resultQuery = Yii::app()->db->createCommand($Qry)->queryAll();
        
        $soma = 0; //tira
        $somaR = 0; //tira
        
        foreach ($resultQuery as $r)
        {
            
            $proposta = Proposta::model()->findByPk($r['id']);
            
            if($proposta !== null)
            {
                
                $data           = new DateTime($proposta->data_cadastro)    ;
                $dataProposta   = $data->format('d/m/Y H:i:s')              ;
                
                $dados[] =  [
                                'dataProposta'  => $dataProposta                                                                ,
                                'codigo'        => $proposta->codigo                                                            ,
                                'filial'        => $proposta->analiseDeCredito->filial->getConcat()                             ,
                                'cliente'       => $proposta->analiseDeCredito->cliente->pessoa->nome                           ,
                                'inicial'       => 'R$ ' . number_format($proposta->valor                           ,2,',','.') ,
                                'entrada'       => 'R$ ' . number_format($proposta->valor_entrada                   ,2,',','.') ,
                                'carencia'      => $proposta->carencia .' dias'                                                 ,
                                'financiado'    => 'R$ ' . number_format($proposta->valor-$proposta->valor_entrada  ,2,',','.') ,
                                'repasse'       => 'R$ ' . number_format($proposta->valorRepasse()                  ,2,',','.') ,
                                'parcelas'      => $proposta->qtd_parcelas . 'x'                                                ,
                            ];
                
                $soma += ($proposta->valor-$proposta->valor_entrada);
                $somaR += $proposta->valorRepasse();
                
            }
            
        }

        echo json_encode    (
                                [
                                    'data'              =>          $dados  ,
                                    'recordsTotal'      => count(   $dadosTotal) ,
                                    'recordsFiltered'   => count(   $dadosTotal) ,
                                    'Qry'               => $Qry ,
                                    'soma'              => $soma,
                                    'somaR'              => $somaR,
                                ]
                            );
    }


}
