<?php

class CartaoController extends Controller {

    public $layout = '//layouts/main_sigac_template';

    public function filters() {

        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules() 
    {

        return array(
            array(
                'allow' ,
                'actions'       => array(
                                            'solicitarCartao'       , 
                                            'getEnderecoCliente'    , 
                                            'getContatoCliente'     , 
                                            'getCliente'            , 
                                            'enviarPropostaCartao'  , 
                                            'teste'                 , 
                                            'teste2'                , 
                                            'gravarVenda'           ,
                                            'autenticarcartao'      ,
                                            'getVendas'             ,
                                            'listarVendas'          ,
                                            'titulos'
                                        ),
                'users'         => array('@'),
                'expression'    => 'false',
                //'expression'    =>  'Yii::app()->session["usuario"]->role == "crediarista"      ||  '
                //                .   'Yii::app()->session["usuario"]->role == "empresa_admin"        '
            ),
            array(
                'allow' ,
                'actions'       => array(
                                            'aprovarProposta', 
                                        ),
                'users'         => array('@'),
                'expression'    =>  'Yii::app()->session["usuario"]->role == "analista_de_credito" ||   '
                                .   'Yii::app()->session["usuario"]->role == "empresa_admin"            '
            ),
            array(
                'deny'                  ,
                'users' => array('*')   ,
            ),
        );
    }

    public function actionTitulos(){
        echo json_encode(
              Cartao::model()->getParcelas($_POST['cod_venda'], $_POST['parc'])
        );
    }

    public function actionGetVendas(){
        $this->layout = '//layouts/main_sigac_template';

      $baseUrl = Yii::app()->baseUrl;
      $cs = Yii::app()->getClientScript();

      $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
      $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
      $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css');
      $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpalette.css');
      $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpicker.css');

      $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
      $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/js/additional-methods.js', CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/js/cartao/fn-grid-vendas.js', CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/js/jquery.form.js', CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js', CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpicker.js', CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpalette.js', CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);

      $this->render('grid_vendas');
    }

    public function actionListarVendas(){
        echo json_encode(
              Cartao::model()->listar()
        );
    }

    public function actionAutenticarCartao(){
        $baseUrl        = Yii::app()->baseUrl; 
        $cs             = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');
        $cs->registerCssFile($baseUrl.'/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl.'/css/sigax_template/dataTables.tableTools.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js',                CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js',               CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js',        CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ui-elements.js',                   CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.autosize.min.js',           CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.js',                       CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskMoney.js',              CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js',            CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.inputlimiter.1.3.1.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js',                   CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js',            CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js',            CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js',             CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js',      CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js',       CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/fn-search-cep.js',                                        CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/blockInterface.js',                                       CClientScript::POS_END);
        //$cs->registerScriptFile($baseUrl.'/js/fn-sigac-camera.js',                                      CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js',                CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js',          CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/cartao/fn-autenticar-cartao.js',                          CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js',            CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-filestyle.min.js',       CClientScript::POS_END);

        $cs->registerScript('moneypicker','$(".grana").maskMoney({decimal:",", thousands:"."});');
        $cs->registerScript('select2','');
        $cs->registerScript('ipt_date','jQuery(function($){$("input.dateBR").mask("99/99/9999");});');

        $cs->registerScript('ipt_tempo_residencia','jQuery(function($){$("#tempo_de_residencia").mask("99/9999");});');
        $cs->registerScript('ipt_data','jQuery(function($){$("input.dateBR").mask("99/99/9999");});');
        $cs->registerScript('ipt_mes_ano','jQuery(function($){$("input.mes_ano").mask("99/9999");});');
        $cs->registerScript('ipt_date','jQuery(function($){$("input.cpfmask").mask("99999999999");});');
        $cs->registerScript('ipt_date','jQuery(function($){$("input.cepmask").mask("99999999");});');
        $cs->registerScript('datepicker',"$('.date-picker').datepicker({autoclose: true});");
        $cs->registerScript('moneypicker2','$(".currency").maskMoney({decimal:",", thousands:".", allowZero: true});');
        $cs->registerScript('moneypicker','$(".currency2").maskMoney({decimal:",", thousands:".", allowZero: false});');
        $cs->registerScript('MesAnoRenda','jQuery(function($){$("input.mes_ano_renda").mask("99/9999");});');

        $this->render('autenticar_cartao');
    }

    public function actionSolicitarCartao() {

        /*
        $this->layout = '//layouts/main_sigac_template';

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css');
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpalette.css');
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/cartao/fn-solicitarCartao.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpalette.js', CClientScript::POS_END);
        */

        $baseUrl        = Yii::app()->baseUrl; 
        $cs             = Yii::app()->getClientScript();
        
        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');
        $cs->registerCssFile($baseUrl.'/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl.'/css/sigax_template/dataTables.tableTools.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js',                CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js',               CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js',        CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ui-elements.js',                   CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.autosize.min.js',           CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.js',                       CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskMoney.js',              CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js',            CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.inputlimiter.1.3.1.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js',                   CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js',            CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js',            CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js',             CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js',      CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js',       CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/fn-search-cep.js',                                        CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/blockInterface.js',                                       CClientScript::POS_END);
        //$cs->registerScriptFile($baseUrl.'/js/fn-sigac-camera.js',                                      CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js',                CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js',          CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/cartao/fn-iniciar-solicitacao-cartao.js',                 CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js',            CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-filestyle.min.js',       CClientScript::POS_END);

        $cs->registerScript('moneypicker','$(".grana").maskMoney({decimal:",", thousands:"."});');
        $cs->registerScript('select2','');
        $cs->registerScript('ipt_date','jQuery(function($){$("input.dateBR").mask("99/99/9999");});');

        $cs->registerScript('ipt_tempo_residencia','jQuery(function($){$("#tempo_de_residencia").mask("99/9999");});');
        $cs->registerScript('ipt_data','jQuery(function($){$("input.dateBR").mask("99/99/9999");});');
        $cs->registerScript('ipt_mes_ano','jQuery(function($){$("input.mes_ano").mask("99/9999");});');
        $cs->registerScript('ipt_date','jQuery(function($){$("input.cpfmask").mask("99999999999");});');
        $cs->registerScript('ipt_date','jQuery(function($){$("input.cepmask").mask("99999999");});');
        $cs->registerScript('datepicker',"$('.date-picker').datepicker({autoclose: true});");
        $cs->registerScript('moneypicker2','$(".currency").maskMoney({decimal:",", thousands:".", allowZero: true});');
        $cs->registerScript('moneypicker','$(".currency2").maskMoney({decimal:",", thousands:".", allowZero: false});');
        $cs->registerScript('MesAnoRenda','jQuery(function($){$("input.mes_ano_renda").mask("99/9999");});');

        $this->render(
            'iniciar_solicitacao',array(
            'emprestimo'        => false,
        ));

    }

    public function actionGetEnderecoCliente() {

        $retorno = [];

        $cliente = null;
        $teste = "";
                
        if(isset($_POST['idCliente']) && $_POST['idCliente'] !== null)
        {
            $cliente = Cliente::model()->find('habilitado AND id = ' . $_POST['idCliente']);
        }

        if ($cliente !== null) {

            $pessoaHasEndereco = PessoaHasEndereco::model()->findAll('habilitado AND Pessoa_id = ' . $cliente->Pessoa_id);
            
            if($pessoaHasEndereco == null)
            {   
                $teste = "pessoaHasEndereco nulo";
            }

            foreach ($pessoaHasEndereco as $pHe) {

                if ($pHe->endereco->habilitado) {

                    $entrega = ($pHe->endereco->entrega == null || !($pHe->endereco->entrega)) ? 0 : 1;

                    if ($entrega) {
                        $corEntrega = 'dark-beige';
                        $classeEntrega = 'up';
                        $entregaMesmo = 'Sim';
                    } else {
                        $corEntrega = 'beige';
                        $classeEntrega = 'down';
                        $entregaMesmo = 'Não';
                    }

                    $btnRemoverEndereco = '     <button 
                                                    class="btn btn-red btnRemoveEndereco"
                                                    value="' . $pHe->id . '">
                                                    <i class="fa fa-minus-circle"></i>
                                                </button>';

                    $btnEntrega = '<button 
                                            class="btn btn-' . $corEntrega . ' btnEntrega"
                                            value="' . $pHe->id . '">
                                            <i class="fa fa-thumbs-' . $classeEntrega . ' fa-white"> ' .
                            $entregaMesmo
                            . '         </i>
                                   </button>';

                    if ($pHe->endereco->Tipo_Moradia_id !== null) {
                        $tipoMoradia = $pHe->endereco->tipoMoradia->tipo;
                    } else {
                        $tipoMoradia = "";
                    }

                    $retorno[] = array(
                        'btnEntrega' => $btnEntrega,
                        'cepEndereco' => $pHe->endereco->cep,
                        'logradouroEndereco' => $pHe->endereco->logradouro,
                        'bairroEndereco' => $pHe->endereco->bairro,
                        'numeroEndereco' => $pHe->endereco->numero,
                        'complementoEndereco' => $pHe->endereco->complemento,
                        'ufEndereco' => $pHe->endereco->uf,
                        'cidadeEndereco' => $pHe->endereco->cidade,
                        'tipoMoradiaEndereco' => $tipoMoradia,
                        'tipoEndereco' => $pHe->endereco->tipoEndereco->tipo,
                        'removerEndereco' => $btnRemoverEndereco
                    );
                }
                
                else
                {
                    $teste .= "Endereco $pHe->Endereco_id não ahbilitado";
                }
            }
        }
        else 
        {
            $teste = "cliente nulo";
        }

        echo json_encode(array('data' => $retorno, 'teste' => $teste));
        
    }

    public function actionGetContatoCliente() {

        $retorno = [];

        $cliente = null;
                
        if(isset($_POST['idCliente']) && $_POST['idCliente'] !== null)
        {
            $cliente = Cliente::model()->find('habilitado AND id = ' . $_POST['idCliente']);
        }

        if ($cliente !== null) {

            $contatoHasTelefone = ContatoHasTelefone::model()->findAll('habilitado AND Contato_id = ' . $cliente->pessoa->Contato_id);

            foreach ($contatoHasTelefone as $cHt) {

                if ($cHt->telefone->habilitado) {

                    $btnRemoverContato = '     <button 
                                                    type="button" 
                                                    class="btn btn-red btnRemoveContato"
                                                    value="' . $cHt->id . '">
                                                    <i class="fa fa-minus-circle"></i>
                                                </button>';

                    $retorno[] = array(
                        'numeroContato' => $cHt->telefone->numero,
                        'tipoTelefoneContato' => $cHt->telefone->tipoTelefone->tipo,
                        'ramalContato' => $cHt->telefone->ramal,
                        'removerContato' => $btnRemoverContato
                    );
                }
            }
        }

        echo json_encode(array('data' => $retorno));
    }

    public function actionGetCliente() {
        
        $cliente = null;

        $retorno = [
            'dadosCliente' => array(),
            'msgConfig' => array(),
        ];

        if(isset($_POST['idCliente']) && $_POST['idCliente'] !== null)
        {
            $cliente = Cliente::model()->find('habilitado AND id = ' . $_POST['idCliente']);
        }
        else
        {
            ob_start();
            var_dump($_POST);
            $resultado = ob_get_clean();
            
            echo "Resultado: $resultado";
        }

        if ($cliente == null) {

            /*$retorno['msgConfig'] = array(
                'titulo' => 'Digite seu CPF!',
                'texto' => 'Se você já tiver cadastro, localizaremos.',
                'tipo' => 'info',
                'hasErrors' => true
            );*/
            
            $retorno['msgConfig'] = array(
                'titulo' => 'Cliente não encontrado!',
                'texto' => '',
                'tipo' => 'info',
                'hasErrors' => true
            );
        } else {

            $rg = $cliente->pessoa->getRG();
            $cpf = $cliente->pessoa->getCPF();
            $cidade = $cliente->pessoa->getCidadeNaturalidade();

            if ($cidade == null) {
                $cidadeCliente = '';
            } else {
                $cidadeCliente = $cidade->id;
            }

            if ($rg == null) {
                $rgCliente['numero'] = '';
                $rgCliente['orgaoEmissor'] = '';
                $rgCliente['ufEmissor'] = '';
                $rgCliente['dataEmissao'] = '';
            } else {
                $rgCliente['numero'] = $rg->numero;
                $rgCliente['orgaoEmissor'] = $rg->orgao_emissor;
                $rgCliente['ufEmissor'] = $rg->uf_emissor;
                $rgCliente['dataEmissao'] = $rg->data_emissao;
            }

            if ($cpf == null) {
                $cpfCliente['numero'] = '';
            } else {
                $cpfCliente['numero'] = $cpf->numero;
            }

            $retorno['dadosCliente']['nomeCliente'] = $cliente->pessoa->nome;
            $retorno['dadosCliente']['sexoCliente'] = $cliente->pessoa->sexo;

            $retorno['dadosCliente']['nascimentoCliente'] = $cliente->pessoa->nascimento;
            $retorno['dadosCliente']['cpfCliente'] = $cpfCliente['numero'];
            $retorno['dadosCliente']['rgCliente'] = $rgCliente ['numero'];
            $retorno['dadosCliente']['orgaoEmissorCliente'] = $rgCliente ['orgaoEmissor'];
            $retorno['dadosCliente']['ufEmissorCliente'] = $rgCliente ['ufEmissor'];
            $retorno['dadosCliente']['dataEmissaoCliente'] = $rgCliente ['dataEmissao'];
            $retorno['dadosCliente']['nacionalidadeCliente'] = $cliente->pessoa->nacionalidade;
            $retorno['dadosCliente']['ufCliente'] = $cliente->pessoa->naturalidade;
            $retorno['dadosCliente']['cidadeCliente'] = $cidadeCliente;
            $retorno['dadosCliente']['estadoCivilCliente'] = $cliente->pessoa->Estado_Civil_id;

            $retorno['msgConfig'] = array(
                'titulo' => 'Atualize o seu cadastro!',
                'texto' => 'Verifique os dados do seu cadastro e atualize o necessário.',
                'tipo' => 'info',
                'hasErrors' => false
            );
        }

        echo json_encode($retorno);
    }
    
    public function actionEnviarPropostaCartao()
    {
        
        $idCartao = null;
        
        $diaVencimento = 5;
        
        if(isset($_POST['diaVencimento']))
        {
            $diaVencimento = $_POST['diaVencimento'];
        }
        
        $aRetorno = [
                        'type'      => 'success'                         ,
                        'title'     => 'Sucesso'                        ,
                        'msg'       => 'Proposta enviada com sucesso'   ,
                        'hasErrors' => false
                    ];
        
        if(isset($_POST['idCliente']))
        {
            
            $transaction = Yii::app()->db->beginTransaction();
            
            $cliente = Cliente::model()->find("habilitado AND id = " . $_POST['idCliente']);
            
            if($cliente !== null)
            {
                
                if(isset($_POST['idCartao']))
                {
                    $idCartao = $_POST['idCartao'];
                }
                
                $analiseCredito                         = new AnaliseDeCredito                                  ;
                
                $analiseCredito->codigo                 = 'C' . date('YmdHis')                                  ;
                $analiseCredito->Filial_id              = Yii::app()->session['usuario']->returnFilial()->id    ;
                $analiseCredito->Usuario_id             = Yii::app()->session['usuario']->id                    ;
                $analiseCredito->Cliente_id             = $cliente->id                                          ;
                $analiseCredito->Procedencia_compra_id  = 9                                                     ;
                $analiseCredito->habilitado             = 1                                                     ;
                $analiseCredito->data_cadastro          = date('Y-m-d H:i:s')                                   ;
                
                if($analiseCredito->save())
                {
                    
                    if(isset($_POST['valorLimite']))
                    {
                        $valorLimite = $_POST['valorLimite'];
                    }
                    else
                    {
                        $valorLimite = 0;
                    }
                
                    $proposta                           = new Proposta              ;

                    $proposta->codigo                   = $analiseCredito->codigo   ;
                    $proposta->Analise_de_Credito_id    = $analiseCredito->id       ;
                    $proposta->Financeira_id            = 5                         ;
                    $proposta->Status_Proposta_id       = 4                         ;
                    $proposta->habilitado               = 1                         ;
                    $proposta->data_cadastro            = date('Y-m-d H:i:s')       ;
                    $proposta->valor_entrada            = 0                         ;
                    $proposta->valor_final              = $valorLimite              ;
                    $proposta->titulos_gerados          = 0                         ;
                    $proposta->segurada                 = 0                         ;
                    
                    if($proposta->save())
                    {
                        if($idCartao == null)
                        {
                            $cartao                 = new Cartao            ;
                            $cartao->habilitado     = 1                     ;
                            $cartao->data_cadastro  = date('Y-m-d H:i:s')   ;
                            $cartao->numero         = '98' . date('YmdHis') ;
                            $cartao->csv            = $this->generateCVC($cartao->numero);                 ;
                        }
                        else
                        {
                            $cartao = Cartao::model()->find("habilitado AND id = $idCartao");
                        }
                        
                        if($idCartao !== null && $cartao == null)
                        {

                            $aRetorno = [
                                            'type'      => 'error'                                  ,
                                            'title'     => 'Erro ao gravar proposta.'               ,
                                            'msg'       => "Cartão não encontrado. Id: $idCartao"   ,
                                            'hasErrors' => true
                                        ];

                            $transaction->rollBack();
                        }
                        else
                        {
                            
                            $cartao->diaVencimento = $diaVencimento;
                            
                            if($idCartao == null)
                            {
                                $cartaoSalvo = $cartao->save();
                            }
                            else
                            {
                                $cartaoSalvo = $cartao->update();
                            }

                            if($cartaoSalvo)
                            {

                                $cartaoHasProposta                  = new CartaoHasProposta ;

                                $cartaoHasProposta->habilitado      = 1                     ;
                                $cartaoHasProposta->data_cadastro   = date('Y-m-d H:i:s')   ;
                                $cartaoHasProposta->Cartao_id       = $cartao->id           ;
                                $cartaoHasProposta->Proposta_id     = $proposta->id         ;

                                if($cartaoHasProposta->save())
                                {

                                    $dialogo                                = new Dialogo                           ;
                                    $dialogo->assunto                       = "Proposta " . $proposta->codigo       ;
                                    $dialogo->data_criacao                  = date('Y-m-d H:i:s')                   ;
                                    $dialogo->habilitado                    = 1                                     ;
                                    $dialogo->Entidade_Relacionamento       = 'Proposta'                            ;
                                    $dialogo->Entidade_Relacionamento_id    = $proposta->id                         ;
                                    $dialogo->criado_por                    = Yii::app()->session['usuario']->id    ;

                                    if( $dialogo->save() )
                                    {

                                        $mensagem                           = new Mensagem                          ;
                                        $mensagem->Dialogo_id               = $dialogo->id                          ;
                                        $mensagem->conteudo                 = "Proposta de cartão de crédito"       ;
                                        $mensagem->lida                     = 0                                     ;
                                        $mensagem->habilitado               = 1                                     ;
                                        $mensagem->editada                  = 0                                     ;
                                        $mensagem->data_criacao             = date('Y-m-d H:i:s')                   ;
                                        $mensagem->data_ultima_edicao       = date('Y-m-d H:i:s')                   ;
                                        $mensagem->Usuario_id               = Yii::app()->session['usuario']->id    ;

                                        if ($mensagem->save())
                                        {

                                            $transaction->commit();

                                        }
                                        else
                                        {

                                            ob_start()                          ;
                                            var_dump($mensagem->getErrors())    ;
                                            $resultado = ob_get_clean()         ;

                                            $aRetorno = [
                                                            'type'      => 'error'                                      ,
                                                            'title'     => 'Erro'                                       ,
                                                            'msg'       => "Erro a gravar cartão. Mensagem: $resultado" ,
                                                            'hasErrors' => true
                                                        ];

                                            $transaction->rollBack();

                                        }

                                    }
                                    else
                                    {

                                        ob_start()                      ;
                                        var_dump($dialogo->getErrors()) ;
                                        $resultado = ob_get_clean()     ;

                                        $aRetorno = [
                                                        'type'      => 'error'                                      ,
                                                        'title'     => 'Erro'                                       ,
                                                        'msg'       => "Erro a gravar cartão. Dialogo: $resultado"  ,
                                                        'hasErrors' => true
                                                    ];

                                        $transaction->rollBack();

                                    }

                                }
                                else 
                                {

                                    ob_start();
                                    var_dump($cartaoHasProposta->getErrors());
                                    $resultado = ob_get_clean();

                                    $aRetorno = [
                                                    'type'      => 'error'                              ,
                                                    'title'     => 'Erro'                               ,
                                                    'msg'       => "Erro a gravar cartão: $resultado"   ,
                                                    'hasErrors' => true
                                                ];

                                    $transaction->rollBack();

                                }

                            }
                            else
                            {

                                ob_start();
                                var_dump($cartao->getErrors());
                                $resultado = ob_get_clean();

                                $aRetorno = [
                                                'type'      => 'error'                              ,
                                                'title'     => 'Erro'                               ,
                                                'msg'       => "Erro ao gravar cartão: $resultado"  ,
                                                'hasErrors' => true
                                            ];

                                $transaction->rollBack();
                            }
                        
                        }
                        
                    }
                    else
                    {
                    
                        ob_start();
                        var_dump($proposta->getErrors());
                        $resultado = ob_get_clean();

                        $aRetorno = [
                                        'type'      => 'error'                              ,
                                        'title'     => 'Erro'                               ,
                                        'msg'       => "Erro a gravar cartão: $resultado"   ,
                                        'hasErrors' => true
                                    ];
                                
                        $transaction->rollBack();
                    }
                    
                }
                else
                {
                    
                    ob_start();
                    var_dump($analiseCredito->getErrors());
                    $resultado = ob_get_clean();
                    
                    $aRetorno = [
                                    'type'      => 'error'                              ,
                                    'title'     => 'Erro'                               ,
                                    'msg'       => "Erro a gravar análise: $resultado"  ,
                                    'hasErrors' => true
                                ];
                                
                    $transaction->rollBack();
                    
                }
                
            }
            else
            {
                $aRetorno = [
                                'type'      => 'error'                  ,
                                'title'     => 'Erro'                   ,
                                'msg'       => 'Cliente não encontrado' ,
                                'hasErrors' => true
                            ];
                                
                $transaction->rollBack();
            }
            
        }
        else
        {
            $aRetorno = [
                            'type'      => 'error'                      ,
                            'title'     => 'Erro'                       ,
                            'msg'       => 'Id do cliente não enviado'  ,
                            'hasErrors' => true
                        ];
                                
            $transaction->rollBack();
        }
        
        echo json_encode($aRetorno);
        
    }
    
    public function actionTeste()
    {
        
        $cliente = Cliente::model()->getClienteByCpf('08267742450');
        
        if($cliente !== null)
        {
            
            $_POST['idCliente'  ] = $cliente->id    ;
            $_POST['idCartao'   ] = 3               ;
            $_POST['valorLimite'] = 1200            ;
            
            $this->actionEnviarPropostaCartao();
            
        }
        
    }
    
    public function actionTeste2()
    {
        
        $cliente = Cliente::model()->getClienteByCpf('08267742450');
        
        if($cliente !== null)
        {
            
            $_POST['idCliente'  ] = $cliente->id    ;
            $_POST['idCartao'   ] = 3               ;
            $_POST['valorLimite'] = 1200            ;
            $_POST['carencia'   ] = 30              ;
            $_POST['valorVenda' ] = 500             ;
            $_POST['qtdParcelas'] = 5               ;
            
            $this->actionGravarVenda();
            
        }
        
    }
    
    public function actionAprovarProposta()
    {
        
        $aRetorno["hasErrors"] = false  ;
        $aRetorno["teste"    ] = 0      ;
        
        $valorLimite = 0;
        
        if(isset($_POST['valorLimite']))
        {
            $valorLimite = $_POST['valorLimite'];
        }
        
        $aMensagem =    [
                            'type'      => 'success'                        ,
                            'title'     => 'Sucesso'                        ,
                            'msg'       => 'Proposta aprovada com sucesso'  
                        ];
        
        $transaction = Yii::app()->db->beginTransaction();
        
        if(isset($_POST['idProposta']))
        {
            
            $aRetorno["teste"] = 1;
            
            $proposta = Proposta::model()->find("habilitado AND id = " . $_POST['idProposta']);
            
            if($proposta !== null)
            {
                
                $aRetorno["teste"] = 2;
                
                $cartaoHasProposta = CartaoHasProposta::model()->find("habilitado AND Proposta_id = $proposta->id");
                
                if($cartaoHasProposta !== null)
                {
                    
                    $aRetorno["teste"] = 3;
                    
                    $cartao = Cartao::model()->find("habilitado AND id = $cartaoHasProposta->Cartao_id");
                    
                    if($cartao !== null)
                    {
                        
                        $aRetorno["teste"] = 4;
                        
                        //mudar aqui. inativar todos
                        $cartaoHasLimite = CartaoHasLimite::model()->find("habilitado AND ativo AND Cartao_has_Proposta_id = $cartaoHasProposta->id");
                        
                        if($cartaoHasLimite !== null)
                        {
                            
                            $aRetorno["teste"] = 5;
                            
                            $cartaoHasLimite->ativo = 0;
                            
                            if(!$cartaoHasLimite->update())
                            {
                                
                                $transaction->rollBack();
                                    
                                ob_start();
                                var_dump($cartaoHasLimite->getErrors());
                                $resultado = ob_get_clean();
                                
                                $aMensagem =    [
                                                    'type'      =>  'error'                                                                 ,
                                                    'title'     =>  'Erro ao aprovar cartão'                                                ,
                                                    'msg'       =>  "Inativando limite atual. CartaoHasLimite ID: $cartaoHasLimite->id. "   .   
                                                                    "Erro: $resultado"
                                                ];

                                $aRetorno["hasErrors"] = true;
                                
                            }
                            
                        }
                            
                        $aRetorno["teste"] = 6;
                            
                        $limite                 = new Limite            ;
                        $limite->habilitado     = 1                     ;
                        $limite->data_cadastro  = date("Y-m-d H:i:s")   ;
                        $limite->valor          = $valorLimite          ;
                        $limite->TipoLimite_id  = 1                     ;

                        if($limite->save())
                        {
                            
                            $aRetorno["teste"] = 7;

                            $cartaoHasLimite                            = new CartaoHasLimite       ;
                            $cartaoHasLimite->habilitado                = 1                         ;
                            $cartaoHasLimite->ativo                     = 1                         ;
                            $cartaoHasLimite->data_cadastro             = date("Y-m-d H:i:s")       ;
                            $cartaoHasLimite->Cartao_has_Proposta_id    = $cartaoHasProposta->id    ;
                            $cartaoHasLimite->Limite_id                 = $limite->id               ;

                            if($cartaoHasLimite->save())
                            {
                            
                                $aRetorno["teste"] = 8;

                                $proposta->Status_Proposta_id = 2;

                                if($proposta->update())
                                {
                            
                                    $aRetorno["teste"] = 9;

                                    $analistaHasStatusProposta                      = new AnalistaHasPropostaHasStatusProposta  ;

                                    $analistaHasStatusProposta->habilitado          = 1                                         ;
                                    $analistaHasStatusProposta->data_cadastro       = date('Y-m-d H:i:s')                       ;
                                    $analistaHasStatusProposta->updated_at          = date('Y-m-d H:i:s')                       ;
                                    $analistaHasStatusProposta->Analista_id         = Yii::app()->session['usuario']->id        ;
                                    $analistaHasStatusProposta->Proposta_id         = $proposta->id                             ;
                                    $analistaHasStatusProposta->Status_Proposta_id  = 2                                         ;

                                    if($analistaHasStatusProposta->save())
                                    {
                                        
                                        $aRetorno["teste"] = 10;
                            
                                        $transaction->commit();
                                    }
                                    else
                                    {

                                        $transaction->rollBack();

                                        ob_start();
                                        var_dump($analistaHasStatusProposta->getErrors());
                                        $resultado = ob_get_clean();

                                        $aMensagem =    [
                                                            'type'      => 'error'                                                                          ,
                                                            'title'     => 'Erro ao aprovar cartão'                                                         ,
                                                            'msg'       => "Salvando AnalistaHasStatus. AnalistaHasPropostaHasStatusProposta: $resultado"
                                                        ];

                                        $aRetorno["hasErrors"] = true;

                                    }

                                }
                                else
                                {

                                    $transaction->rollBack();

                                    ob_start();
                                    var_dump($proposta->getErrors());
                                    $resultado = ob_get_clean();

                                    $aMensagem =    [
                                                        'type'      => 'error'                                      ,
                                                        'title'     => 'Erro ao aprovar cartão'                     ,
                                                        'msg'       => "Salvando Proposta. Proposta: $resultado"
                                                    ];

                                    $aRetorno["hasErrors"] = true;

                                }

                            }
                            else
                            {

                                $transaction->rollBack();

                                ob_start();
                                var_dump($cartaoHasLimite->getErrors());
                                $resultado = ob_get_clean();

                                $aMensagem =    [
                                                    'type'      => 'error'                                                              ,
                                                    'title'     => 'Erro ao aprovar cartão'                                             ,
                                                    'msg'       => "Salvando Limite. CartaoHasLimite: $resultado"
                                                ];

                                $aRetorno["hasErrors"] = true;

                            }

                        }
                        else
                        {

                            $transaction->rollBack();

                            ob_start();
                            var_dump($limite->getErrors());
                            $resultado = ob_get_clean();

                            $aMensagem =    [
                                                'type'      => 'error'                                  ,
                                                'title'     => 'Erro ao aprovar cartão'                 ,
                                                'msg'       => "Salvando Limite. Limite: $resultado"
                                            ];

                            $aRetorno["hasErrors"] = true;
                        }
                        
                    }
                    else
                    {
                                
                        $transaction->rollBack();
                                
                        $aMensagem =    [
                                            'type'      => 'error'                                                      ,
                                            'title'     => 'Erro'                                                       ,
                                            'msg'       => "Cartão vinculado não encontrado. ID: $cartaoHasProposta->id"
                                        ];

                        $aRetorno["hasErrors"] = true;
                    }
                    
                }
                else
                {
                                
                    $transaction->rollBack();
                                
                    $aMensagem =    [
                                        'type'      => 'error'                      ,
                                        'title'     => 'Erro'                       ,
                                        'msg'       => 'Nenhum cartão vinculado'
                                    ];

                    $aRetorno["hasErrors"] = true;
                }
                
            }
            else
            {
                                
                $transaction->rollBack();

                $aMensagem =    [
                                    'type'      => 'error'                                                  ,
                                    'title'     => 'Erro'                                                   ,
                                    'msg'       => 'Proposta não encontrada. Id: ' . $_POST['idProposta']   
                                ];
                
                $aRetorno["hasErrors"] = true;
                
            }
            
        }
        else
        {
                                
            $transaction->rollBack();

            $aMensagem =    [
                                'type'      => 'error'                                                      ,
                                'title'     => 'Erro'                                                       ,
                                'msg'       => 'Id da Proposta não enviado. Id: ' . $_POST['idProposta']    
                            ];
                
            $aRetorno["hasErrors"] = true;
                
        }
        
        $aRetorno["mensagem"] = $aMensagem;
        
        echo json_encode($aRetorno);
        
    }
    
    public function actionGravarVenda()
    {
        $util = new Util;
        $aMensagem =    [
                            'type'      => 'success'                    ,
                            'title'     => 'Sucesso'                    ,
                            'msg'       => "Venda gravada com sucesso."
                        ];

        $aRetorno["hasErrors"] = false;
        
        $transaction = Yii::app()->db->beginTransaction();
        
        if(!isset($_POST['idCliente']) || !isset($_POST['idCartao']) || !isset($_POST['carencia']) || !isset($_POST['valorVenda']) || !isset($_POST['qtdParcelas']))
        {
            
            $transaction->rollBack();

            $aMensagem =    [
                                'type'      => 'error'                      ,
                                'title'     => 'Erro ao gravar a venda'     ,
                                'msg'       => "Estão faltando parâmetros."
                            ];

            $aRetorno["hasErrors"] = true;
                        
        }
        else
        {
            
            $idCliente      = $_POST['idCliente'    ]   ;
            $idCartao       = $_POST['idCartao'     ]   ;
            $carencia       = $_POST['carencia'     ]   ;
            $valorVenda     = number_format($util->moedaViewToBD($_POST['valorVenda']), 2, ',', '.');
            $valorVenda     = str_replace(array('.',','), array('','.'), $valorVenda);
            $qtdParcelas    = $_POST['qtdParcelas'  ]   ;
            $intervalo      = 30                        ;
            $idTipoLimite   = 1                         ;
            
            $cliente = Cliente::model()->find("habilitado AND id = $idCliente");
            
            if($cliente !== null)
            {
                
                $cartao = Cartao::model()->find("habilitado AND id = $idCartao");
                
                if($cartao !== null)
                {
                    
                    $query = "
                                SELECT L.valor
                                FROM 	Limite              AS L
                                INNER JOIN 	TipoLimite          AS TL 	ON  TL.habilitado AND   L.TipoLimite_id             =  TL.id
                                INNER JOIN 	Cartao_has_Limite   AS ChL 	ON ChL.habilitado AND ChL.Limite_id                 =   L.id AND ChL.ativo 
                                INNER JOIN 	Cartao_has_Proposta AS ChP 	ON ChP.habilitado AND ChL.Cartao_has_Proposta_id    = ChP.id
                                INNER JOIN 	Cartao              AS C 	ON   C.habilitado AND ChP.Cartao_id                 =   C.id
                                INNER JOIN 	Proposta            AS P 	ON   P.habilitado AND ChP.Proposta_id               =   P.id
                                INNER JOIN 	Analise_de_Credito  AS AC 	ON  AC.habilitado AND   P.Analise_de_Credito_id     =  AC.id
                                WHERE L.habilitado AND TL.id = $idTipoLimite AND AC.Cliente_id = $cliente->id AND C.id = $cartao->id";
                    
                    $consulta = Yii::app()->db->createCommand($query)->queryRow();

                    if($consulta !== null)
                    {
                        $formaPagamento = FormaDePagamento::model()->find("habilitado AND sigla = 'CC'");

                        if($formaPagamento !== null)
                        {
                        
                            $saldoDevedor = $cliente->getSaldoDevedor($cartao->id);

                            if($consulta['valor'] > ($valorVenda + $saldoDevedor))
                            {

                                $filtro = "habilitado AND carencia = $carencia AND qtdParcelasDe <= $qtdParcelas AND qtdParcelasAte >= $qtdParcelas AND intervalo = $intervalo";

                                $formaPagamentoHasCP = FormaDePagamentoHasCondicaoDePagamento::model()->find($filtro);

                                if($formaPagamentoHasCP !== null)
                                {

                                    $venda              = new Venda             ;
                                    $venda->Cliente_id  = $cliente->id          ;
                                    $venda->codigo      = date("YmdHis"     )   ;
                                    $venda->data        = date("Y-m-d H:i:s")   ;
                                    $venda->habilitado  = 1                     ;

                                    if($venda->save())
                                    {

                                        $itemVenda                      = new ItemDaVenda   ;
                                        $itemVenda->Item_do_Estoque_id  = 1                 ;
                                        $itemVenda->Venda_id            = $venda->id        ;
                                        $itemVenda->habilitado          = 1                 ;
                                        $itemVenda->seq                 = 1                 ;
                                        $itemVenda->quantidade          = 1                 ;
                                        $itemVenda->desconto            = 0                 ;
                                        $itemVenda->preco               = $valorVenda       ;

                                        if($itemVenda->save())
                                        {

                                            $vendaHasFPhasCP = new VendaHasFormaDePagamentoHasCondicaoDePagamento;
                                            $vendaHasFPhasCP->Forma_de_Pagamento_has_Condicao_de_Pagamento_id = $formaPagamentoHasCP->id;
                                            $vendaHasFPhasCP->Venda_id = $venda->id;
                                            $vendaHasFPhasCP->qtdParcelas = $qtdParcelas;
                                            $vendaHasFPhasCP->valor = $itemVenda->preco;
                                            $vendaHasFPhasCP->Cartao_id = $cartao->id;

                                            if($vendaHasFPhasCP->save())
                                            {
                                                $natureza_tit = NaturezaTitulo::model()->find("habilitado AND codigo = '10101'");
                                                $titulo                                     = new Titulo            ;
                                                $titulo->habilitado                         = 1                     ;
                                                $titulo->emissao                            = date("Y-m-d H:i:s")   ;
                                                $titulo->prefixo                            = "CC"                  ;
                                                $titulo->Venda_has_FP_has_CP_FP_has_CP_id   = $vendaHasFPhasCP->id  ;
                                                $titulo->NaturezaTitulo_id                  = $natureza_tit->id;

                                                if($titulo->save())
                                                {

                                                    $erroParcela = false;

                                                    $valorParcela = (round(($vendaHasFPhasCP->valor/$vendaHasFPhasCP->qtdParcelas), 2));

                                                    for ($i = 1; $i <= $vendaHasFPhasCP->qtdParcelas; $i++)
                                                    {

                                                        $diasSoma               = ($i * $intervalo);

                                                        $parcela                = new Parcela                                   ;

                                                        $parcela->seq           = $i                                            ;
                                                        $parcela->Titulo_id     = $titulo->id                                   ;
                                                        $parcela->habilitado    = 1                                             ;
                                                        $parcela->valor         = $valorParcela                                 ;
                                                        $parcela->valor_atual   = $valorParcela                                 ;
                                                        $parcela->vencimento    = date('Y-m-d', strtotime("+$diasSoma days"))   ;

                                                        if(!$parcela->save())
                                                        {
                                                            $erroParcela = true;

                                                            $transaction->rollBack();

                                                            ob_start();
                                                            var_dump($parcela->getErrors());
                                                            $resultado = ob_get_clean();

                                                            $aMensagem =    [
                                                                                'type'      => 'error'                                          ,
                                                                                'title'     => 'Erro ao gravar a venda'                         ,
                                                                                'msg'       => "Salvando Parcela. Parcela: $resultado. i: $i"
                                                                            ];

                                                            $aRetorno["hasErrors"] = true;

                                                            break;

                                                        }

                                                    }

                                                    if(!$erroParcela)
                                                    {
                                                        $natureza_tit = NaturezaTitulo::model()->find("habilitado AND codigo = '299'");
                                                        $tituloPagar                                    = new Titulo            ;
                                                        $tituloPagar->habilitado                        = 1                     ;
                                                        $tituloPagar->emissao                           = date("Y-m-d H:i:s")   ;
                                                        $tituloPagar->prefixo                           = "CC"                  ;
                                                        $tituloPagar->Venda_has_FP_has_CP_FP_has_CP_id  = $vendaHasFPhasCP->id  ;
                                                        $tituloPagar->NaturezaTitulo_id                 = $natureza_tit->id;

                                                        if($tituloPagar->save())
                                                        {

                                                            $erroParcela = false;

                                                            $valorPagar = (((100-$formaPagamentoHasCP->taxaAdministrativa) * $vendaHasFPhasCP->valor)/100);
                                                            $valorParcela = (round(($valorPagar/$vendaHasFPhasCP->qtdParcelas), 2));

                                                            for ($i = 1; $i <= $vendaHasFPhasCP->qtdParcelas; $i++)
                                                            {

                                                                $diasSoma = ($i * $intervalo);

                                                                $parcelaPagar               = new Parcela                                   ;

                                                                $parcelaPagar->seq          = $i                                            ;
                                                                $parcelaPagar->Titulo_id    = $titulo->id                                   ;
                                                                $parcelaPagar->habilitado   = 1                                             ;
                                                                $parcelaPagar->valor        = $valorParcela                                 ;
                                                                $parcelaPagar->valor_atual  = $valorParcela                                 ;
                                                                $parcelaPagar->vencimento   = date('Y-m-d', strtotime("+$diasSoma days"))   ;

                                                                if(!$parcelaPagar->save())
                                                                {

                                                                    $erroParcela = true;

                                                                    $transaction->rollBack();

                                                                    ob_start();
                                                                    var_dump($parcelaPagar->getErrors());
                                                                    $resultado = ob_get_clean();

                                                                    $aMensagem =    [
                                                                                        'type'      => 'error'                                                  ,
                                                                                        'title'     => 'Erro ao gravar a venda'                                 ,
                                                                                        'msg'       => "Salvando Parcela a Pagar. Parcela: $resultado. i: $i"
                                                                                    ];

                                                                    $aRetorno["hasErrors"] = true;

                                                                    break;

                                                                }
                                                            }

                                                            if(!$erroParcela)
                                                            {
                                                                $transaction->commit();
                                                            }

                                                        }
                                                        else
                                                        {

                                                            $transaction->rollBack();

                                                            ob_start();
                                                            var_dump($tituloPagar->getErrors());
                                                            $resultado = ob_get_clean();

                                                            $aMensagem =    [
                                                                                'type'      => 'error'                                          ,
                                                                                'title'     => 'Erro ao gravar a venda'                         ,
                                                                                'msg'       => "Salvando Titulo a Pagar. Titulo: $resultado"
                                                                            ];

                                                            $aRetorno["hasErrors"] = true;

                                                        }

                                                    }

                                                }
                                                else
                                                {

                                                    $transaction->rollBack();

                                                    ob_start();
                                                    var_dump($titulo->getErrors());
                                                    $resultado = ob_get_clean();

                                                    $aMensagem =    [
                                                                        'type'      => 'error'                                          ,
                                                                        'title'     => 'Erro ao gravar a venda'                         ,
                                                                        'msg'       => "Salvando Titulo a Receber. Titulo: $resultado"
                                                                    ];

                                                    $aRetorno["hasErrors"] = true;

                                                }

                                            }
                                            else
                                            {

                                                $transaction->rollBack();

                                                ob_start();
                                                var_dump($vendaHasFPhasCP->getErrors());
                                                $resultado = ob_get_clean();

                                                $aMensagem =    [
                                                                    'type'      => 'error'                                                  ,
                                                                    'title'     => 'Erro ao gravar a venda'                                 ,
                                                                    'msg'       => "Salvando FP's da Venda. VendaHasFPHasCP: $resultado"
                                                                ];

                                                $aRetorno["hasErrors"] = true;

                                            }

                                        }
                                        else
                                        {

                                            $transaction->rollBack();

                                            ob_start();
                                            var_dump($itemVenda->getErrors());
                                            $resultado = ob_get_clean();

                                            $aMensagem =    [
                                                                'type'      => 'error'                                      ,
                                                                'title'     => 'Erro ao gravar a venda'                     ,
                                                                'msg'       => "Salvando Item da Venda. Titulo: $resultado"
                                                            ];

                                            $aRetorno["hasErrors"] = true;

                                        }

                                    }
                                    else
                                    {

                                        $transaction->rollBack();

                                        ob_start();
                                        var_dump($venda->getErrors());
                                        $resultado = ob_get_clean();

                                        $aMensagem =    [
                                                            'type'      => 'error'                              ,
                                                            'title'     => 'Erro ao gravar a venda'             ,
                                                            'msg'       => "Salvando Venda. Venda: $resultado"
                                                        ];

                                        $aRetorno["hasErrors"] = true;

                                    }

                                }
                                else
                                {

                                    $transaction->rollBack();

                                    $aMensagem =    [
                                                        'type'      =>  'error'                                                         ,
                                                        'title'     =>  'Erro ao gravar a venda'                                        ,
                                                        'msg'       =>  "Não existe forma de pagamento compatível com a venda. <br>"    . 
                                                                        "Carência: $carencia <br>"                                      .
                                                                        "Qtd Parcelas: $qtdParcelas <br>"                               .
                                                                        "Intervalo: $intervalo"
                                                    ];

                                    $aRetorno["hasErrors"] = true;

                                }

                            }
                            else
                            {

                                $transaction->rollBack();

                                $aMensagem =    [
                                                    'type'      => 'error'                                              ,
                                                    'title'     => 'Erro ao gravar a venda'                             ,
                                                    'msg'       => "Cartão sem limite para efetuar compra desse valor."
                                                ];

                                $aRetorno["hasErrors"] = true;
                                
                            }
                        
                        }
                        else
                        {

                            $transaction->rollBack();

                            $aMensagem =    [
                                                'type'      => 'error'                                                      ,
                                                'title'     => 'Erro ao gravar a venda'                                     ,
                                                'msg'       => "Não existe forma de pagamento do tipo Cartão de Crédito."
                                            ];

                            $aRetorno["hasErrors"] = true;
                            
                        }
                    
                    }
                    else
                    {

                        $transaction->rollBack();

                        $aMensagem =    [
                                            'type'      => 'error'                                          ,
                                            'title'     => 'Erro ao gravar a venda'                         ,
                                            'msg'       => "Cliente não possui cartão com limite ativo."
                                        ];

                        $aRetorno["hasErrors"] = true;
                    }
                    
                }
                else
                {

                    $transaction->rollBack();

                    $aMensagem =    [
                                        'type'      => 'error'                                              ,
                                        'title'     => 'Erro ao gravar a venda'                             ,
                                        'msg'       => "Cartão de Crédito não encontrado. ID: $idCartao"
                                    ];

                    $aRetorno["hasErrors"] = true;
                    
                }
                
            }
            else
            {

                $transaction->rollBack();

                $aMensagem =    [
                                    'type'      => 'error'                                  ,
                                    'title'     => 'Erro ao gravar a venda'                 ,
                                    'msg'       => "Cliente não encontrado. ID: $idCliente"
                                ];

                $aRetorno["hasErrors"] = true;
                        
            }
            
        }
        
        $aRetorno['mensagem'] = $aMensagem;
        
        echo json_encode($aRetorno);
        
    }

    public function generateCVC($num_cartao){
        $digito1 = $num_cartao[0] + $num_cartao[1] + $num_cartao[15] + $num_cartao[14];
        $array = str_split((string)$digito1);        
        while (sizeof($array) >= 2 ) {
            $digito1 = intval($array[0]) + intval($array[1]);
            $array = str_split((string)$digito1);
        }

        $digito2 = $num_cartao[2] + $num_cartao[3] + $num_cartao[4] + $num_cartao[13] + $num_cartao[12] + $num_cartao[11];
        $array = str_split((string)$digito2);        
        while (sizeof($array) >= 2 ) {
            $digito2 = intval($array[0]) + intval($array[1]);
            $array = str_split((string)$digito2);
        }

        $digito3 = $num_cartao[5] + $num_cartao[6] + $num_cartao[7] + $num_cartao[10] + $num_cartao[9] + $num_cartao[8];
        $array = str_split((string)$digito3);        
        while (sizeof($array) >= 2 ) {
            $digito3 = intval($array[0]) + intval($array[1]);
            $array = str_split((string)$digito3);
        }

        return (string)$digito1 . (string)$digito2 . (string)$digito3;
    }

}
