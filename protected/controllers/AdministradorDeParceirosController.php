<?php

class AdministradorDeParceirosController extends Controller {

    public $layout = '//layouts/main_sigac_template';

    public function filters() {
        return array(
            'accessControl',
            'postOnly + delete'
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('getRanking', 'producao', 'getProducaoParceiros', 'producaoPorStatus', 'index', 'getCrediaristas'),
                'users' => array('@'),
                'expression' => 'Yii::app()->session["usuario"]->role == "parceiros_admin" '
            ),
            array(
                'deny',
                'users' => array('*')
            ),
        );
    }

    public function actionIndex() {
        $baseUrl = Yii::app()->baseUrl;

        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-backoffice-parceiro-initv2.js', CClientScript::POS_END);

        if (Yii::app()->session['usuario']->primeira_senha == 1) {

            $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/ui-modals.js', CClientScript::POS_END);
            $cs->registerScript('UIModals', 'UIModals.init();');
            $cs->registerScript('ShowModal', '$("#static").modal("show")');
        }

        $this->render('index');
    }

    public function actionGetRanking() {

        echo json_encode(Empresa::model()->getRankingBackofficeIndex($_POST['draw']));
    }

    public function actionProducao() {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-producao-admin-parceiros.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);

        $cs->registerScript('datepicker', "$('.date-picker').datepicker({autoclose: true});");

        $this->render('producao');
    }

    public function actionProducaoPorStatus() {

        $baseUrl = Yii::app()->baseUrl;

        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.tagsinput.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.tagsinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-grid-producao-por-statusv5.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);

        $cs->registerScript('ShowModalCrud', "$('#tags_1').tagsInput({width: 'auto', 'interactive':false});");

        $this->render('producaoPorStatus', array('params' => $_POST));
    }

    public function actionGetProducaoParceiros() {

        echo json_encode(Empresa::model()->producaoParceiros($_POST['draw'], $_POST['filiais'], $_POST['data_de'], $_POST['data_ate']));
    }

    public function actionGetCrediaristas()
    {
        echo json_encode( Empresa::model()->listarCrediaristasGrupo( $_POST['filiais'] ) );
    }
}
