<?php

class SolicitacaoDeCancelamentoController extends Controller {

    public $layout = '//layouts/main_sigac_template';

    public function actionChange() {
        echo json_encode(SolicitacaoDeCancelamento::model()->change($_POST));
    }

    public function actionAdd() {
        echo json_encode(SolicitacaoDeCancelamento::model()->persist($_POST['Solicitacao'], $_FILES['ComprovanteFile']));
    }

    public function actionConcluidos() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-grid-historico-cancelamentos.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);
         $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);

        $cs->registerScript('datepicker', "$('.date-picker').datepicker({autoclose: true});");

        $this->render('concluidos');
    }

    public function actionComprovanteCancelamento(){
        $this->layout = '//layouts/template_cancelamento';
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        if (isset($_POST['Solicitacao']) && !empty($_POST['Solicitacao'])) {
            $solicitacao = $_POST['Solicitacao'];

            $proposta = Proposta::model()->findByPk($solicitacao['Proposta_id']);
            $usuario = Usuario::model()->findByPk($solicitacao['Solicitante']);

            $sigacLog = new SigacLog;
            $sigacLog->saveLog('Impressão do Comprovante de Solicitação de Cancelamento','Proposta', $proposta->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Crediarista ".Yii::app()->session['usuario']->nome_utilizador . " solicitou o cancelamento da proposta com código: " . $proposta->codigo, "127.0.0.1", null, Yii::app()->session->getSessionId() );

            $this->render('solicitacao_cancelamento', array(
                'proposta' => $proposta,
                'usuario' => $usuario
            ));
        }
    }

    public function actionIndex() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-grid-solicitacoes-cancelamentos-v3.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);

        $this->render('index');
    }

    public function actionListar() {

        echo json_encode(SolicitacaoDeCancelamento::model()->listar($_POST['draw']));
    }

    public function actionHistorico() {
        
        echo json_encode(
            SolicitacaoDeCancelamento::model()->historico
            (
                    $_POST['draw'       ]   , 
                    $_POST['search'     ]   , 
                    $_POST['start'      ]   , 
                    $_POST['data_de'    ]   , 
                    $_POST['data_ate'   ]   , 
                    $_POST['titulos'    ]   , 
                    $_POST['filiais'    ]   
            )
        );
    }

}
