<?php

class ValoresCaracteristicaController extends Controller
{	

	public $layout = '//layouts/main_sigac_template';

	public function actionIndex()
	{
	}

	public function actionAdd(){

		if( !empty( $_POST['ValoresCaracteristica']['valor'] ) )
		{
			echo json_encode( ValoresCaracteristica::model()->novo( $_POST['ValoresCaracteristica'] ) );
			//echo json_encode( $_POST['ValoresCaracteristica'] );
		}
	}
	
	public function actionGetItensCaracteristica(){
		echo json_encode( ValoresCaracteristica::model()->listItensCaracteristica( $_POST['draw'], $_POST['caracteristicaId'], $_POST['start'] ) );
	}
}