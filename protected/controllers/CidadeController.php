<?php

class CidadeController extends Controller
{

	public $layout = '//layouts/empty_template';

	public function actionIndex()
	{	
		$this->render('index');
	}

	public function actionListCidades(){

		$uf = "AC";

		if( !empty( $_GET['uf'] ) ){
			$uf = $_GET['uf'];
		}

		echo json_encode(Cidade::model()->listCidades($uf));
	}
}