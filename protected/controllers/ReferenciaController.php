<?php

class ReferenciaController extends Controller {

    public function actionPersist() {
        echo json_encode(Referencia::model()->persist($_POST['Referencia'], $_POST['TelefoneReferencia'], $_POST['Referencia2'], $_POST['TelefoneReferencia2'], $_POST['Referencia3'], $_POST['TelefoneReferencia3'], $_POST['clienteId'], $_POST['ref1Id'], $_POST['ref2Id'], $_POST['ref3Id'], $_POST['t1_id'], $_POST['t2_id'], $_POST['t3_id'], $_POST['ControllerAction']));
    }

    public function actionIndex() {
        $this->render('index');
    }

    public function actionAlterar() {

        $id = $_POST['id']; //resgate o id enviado pelo post
        $conteudo = $_POST['obs']; //resgate o conteudo enviado pelo post

        $ref = Referencia::model()->findByPk($id); //busque o registro no banco pelo id

        $ref->observacao = $conteudo; //atribua o valor da observacao

        $ref->update(); //salve
    }

    public function actionUpdate() {

        $action = $_POST['controller_action'];
        $msgReturn = "";

        if ($action == 'add') {
            Referencia::model()->novo($_POST['Referencia'], $_POST['TelefoneReferencia'], $_POST['Cliente_id']);
            $msgReturn = "Referência cadastrada com sucesso!";
        } else {
            Referencia::model()->atualizar($_POST['Referencia'], $_POST['TelefoneReferencia'], $_POST['referencia_id'], $_POST['telefone_referencia_id']);
            $msgReturn = "Referência atualizada com sucesso!";
        }

        echo json_encode(array(
                'msgReturn' => $msgReturn
        ));
    }

    public function actionLoad() {
        $id = $_GET['entity_id'];
        echo json_encode(Referencia::model()->loadFormEdit($id));
    }

}
