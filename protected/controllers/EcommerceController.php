<?php

class EcommerceController extends Controller {

    public $layout = '//layouts/boleto_layout';

    public function actionIndex(){
        
        $codigo     = $_GET['c']; //codigo da venda
        $at         = $_GET['t']; //token de acesso da venda

        $venda      = VendaW::model()->find('codigo = ' . $codigo . ' AND external_access_token = ' . "'$at' AND titulos_gerados = 0");

        if ($venda === null)
        {
            throw new CHttpException(404, 'Registro não encontrado');
        }

        else
        {
            $boletoConfig = Range::boletoECommerce($venda);
            
            $this->render('/boleto/boletoEcommerceBase', array(
                'venda'         => $venda,
                'boletoConfig'  => $boletoConfig
            ));
        }

    }

    public function actionBoleto()
    {
        $objRange               = new Range;                
        $at                     = $_POST['accessToken'];
        $codVenda               = $_POST['codVenda'];

        $venda                  = VendaW::model()->find('codigo = "' . $codVenda . '" AND external_access_token = ' . "'$at' AND titulos_gerados = 1");

        if( $venda != NULL )
        {
            $boletoConfig       = $objRange->getBoletoEcommerce( $_POST['ParcelasSeq'], $_POST['codVenda'], $_POST['accessToken'] );

            $this->render('/boleto/boletoEcommerceBase',array(
                'venda'         => $venda,
                'boletoConfig'  => $boletoConfig
            ));
        }
        else
        {
            throw new CHttpException(404, 'Registro não encontrado');
        }
    }

    public function init() {
    }

}
