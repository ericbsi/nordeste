<?php

class FatorController extends Controller
{

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionGetFatoresTabela()
    {

        if (isset($_POST['TabelaId']) && !empty($_POST['TabelaId']))
        {
            echo json_encode(TabelaCotacao::model()->listFatores($_POST['draw'], $_POST['TabelaId'], $_POST['start']));
        }
    }

    public function actionAdd()
    {

        if (isset($_POST['Fator']['fator']) && !empty($_POST['Fator']['carencia']) && !empty($_POST['Fator']['parcela']) && !empty($_POST['TabelaId']))
        {
            echo json_encode(Fator::model()->novo($_POST['Fator'], $_POST['TabelaId']));
        }
    }
    
    public function actionMudarFatorIOF()
    {
        
        $retorno =  [
                        "hasErrors"     => 0                                    ,
                        "pntfyClass"    => "success"                            ,
                        "msg"           => "Fator do IOF alterado com sucesso"
                    ];
        
        if(isset($_POST["idFator"]) && !empty($_POST["idFator"]) && isset($_POST["fatorIOF"]) && !empty($_POST["fatorIOF"]))
        {
         
            $fator = Fator::model()->find("habilitado AND id = " . $_POST["idFator"]);
            
            if($fator !== null)
            {
                
                $fator->fatorIOF = $_POST["fatorIOF"];
                
                if(!$fator->update())
                {
                    
                    ob_start()                      ;
                    var_dump($fator->getErrors())   ;
                    $resErro = ob_get_clean()       ;
        
                    $retorno =  [
                                    "hasErrors"     => 1                                                ,
                                    "pntfyClass"    => "error"                                          ,
                                    "msg"           => "Erro ao alterar o Fator IOF. Erro: $resErro"
                                ];
                    
                }
                
            }
            else
            {
                
                $retorno =  [
                                "hasErrors"     => 1                                                        ,
                                "pntfyClass"    => "error"                                                  ,
                                "msg"           => "Objeto Fator não encontrado. ID " . $_POST["idFator"]
                            ];
                
            }
            
        }
        else
        {
            
            $retorno =  [
                            "hasErrors"     => 1                                            ,
                            "pntfyClass"    => "error"                                      ,
                            "msg"           => "ID ou valor do fator IOF não submetidos."
                        ];
                
        }
        
        echo json_encode($retorno);
        
    }

}
