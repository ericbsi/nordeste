<?php

class EmailController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionUpdate(){

		$action 	= $_POST['controller_action'];
		$msgReturn 	= "";

		if( $action == 'add' )
		{
			Email::model()->novo($_POST['EmailCliente'],$_POST['Cliente_id']);
			$msgReturn = "Email cadastrado com sucesso!";
		}
		else
		{
			Email::model()->atualizar($_POST['EmailCliente'], $_POST['email_id']);
			$msgReturn = "Email atualizado com sucesso!";
		}

		echo json_encode(array(
			'msgReturn' => $msgReturn
		));
	}

	public function actionLoad(){

		$id 		= $_GET['entity_id'];
		echo json_encode( Email::model()->loadFormEdit($id) );
	}
}