<?php

class EstoqueController extends Controller {

    public $layout = '//layouts/main_sigac_template';

    public function accessRules() {

        return array(
            array('allow',
                'actions' => array('index', 'getEstoquesEmpresa', 'add', 'buscaPorFilial'),
                'users' => array('@'),
            )
        );
    }

    public function actionIndex() {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ui-modals.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js');
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js');
        $cs->registerScriptFile($baseUrl . '/js/btn-status-fn.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-estoque-index.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);

        $cs->registerScript('UIModals', 'UIModals.init();');

        if (Yii::app()->session['usuario']->primeira_senha == 1) {

            $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/ui-modals.js', CClientScript::POS_END);
            $cs->registerScript('UIModals', 'UIModals.init();');
            $cs->registerScript('ShowModal', '$("#static").modal("show")');
        }

        $this->render('index');
    }

    public function actionGetEstoquesEmpresa() {

        if (!empty($_POST['empresaId'])) {
            $empresa = Empresa::model()->findByPk($_POST['empresaId']);

            echo json_encode($empresa->listEstoques($_POST['draw'], $_POST['filiais'], $_POST['start'], $_POST['locais'], $_POST['vende']));
        }
    }

    public function actionGetItem() {
        echo json_encode(ItemDoEstoque::model()->loadJson($_POST['itemId']));
    }

    /* public function actionGetProdDesc(){
      $it = ItemDoEstoque::model()->findByPk( $_POST['itemId'] );
      echo json_encode( Produto::model()->findByPk($it->Produto_id) );
      } */

    public function actionAdd() {

        if (!empty($_POST['Estoque']['Filial_id']) && !empty($_POST['Estoque']['Local_id'])) {
            echo json_encode(Estoque::model()->novo($_POST['Estoque']));
        }
    }

    public function actionBuscaPorFilial() {
        $filialId = $_POST['estoque'];

        echo json_encode(Estoque::model()->buscaPorFilial($filialId));
    }

    public function actionGetDescricao() {
        $estoqueId = $_POST;

        $estoque = Estoque::model()->findByPk($estoqueId);

        echo json_encode(
                Local::model()->findByPk(
                        $estoque->local->id
                )->nome
        );
    }

    public function actionGetItens() {

        $estoqueId = $_POST['select_estoques_name'];

        $estoque = Estoque::model()->findByPk($estoqueId);

        echo json_encode($estoque->listItensEstoqueDataTable($_POST['draw']));
    }

}
