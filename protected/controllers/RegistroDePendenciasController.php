<?php

class RegistroDePendenciasController extends Controller{

	public function actionIndex()
	{

	}

	public function actionRemoverItem()
	{	
		$recebDoc 	= RecebimentoDeDocumentacao::model()->findByPk( $_POST['processoId'] );

		echo json_encode( $recebDoc->registroDePendencias->removerItem( $_POST['itemId'] ) );
	}
}