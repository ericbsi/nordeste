<?php

class AnexosController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionRemove(){

		echo json_encode(Anexo::model()->remove($_POST['ipt_id_anexo']));
	}

	public function actionAdd(){

		//Se temos uma origem, então faz parte da migração de uploads para o bucket s3
		if( isset( $_POST['origem'] ) && !empty( $_POST['origem'] ) && trim( $_POST['origem'] ) != '' ){

			$s3Client = new S3Client;

			$cliente  = Cliente::model()->findByPk($_POST["Cliente_id"]);

			echo json_encode( $s3Client->put( $_FILES["FileInput"], "cadastro", $cliente->getCadastro()->id, $_POST["Anexo"]["descricao"] ) );
		}
		else{
			if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && isset( $_FILES["FileInput"] ) && $_FILES["FileInput"]["error"] == UPLOAD_ERR_OK && !empty( $_POST['Anexo']['descricao'] ) )
			{	
				echo json_encode( Anexo::model()->add($_FILES["FileInput"], $_POST["Cliente_id"], $_POST["Anexo"]) );
			}
		}
	
		//{"name":"teste.png","type":"image\/png","tmp_name":"\/tmp\/phpbgfi72","error":0,"size":48089}
	}
}