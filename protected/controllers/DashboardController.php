<?php

class DashboardController extends Controller{

	public $layout='//layouts/main_sigac_template';

	public function actionIndex(){
		
		$baseUrl = Yii::app()->baseUrl; 

	  $cs = Yii::app()->getClientScript();

  	$cs->registerScriptFile($baseUrl.'/js/jquery.min.js');
  	$cs->registerScriptFile($baseUrl.'/js/jquery.dataTables.min.js', CClientScript::POS_END);
  	$cs->registerScriptFile($baseUrl.'/js/bootstrap-multiselect.js', CClientScript::POS_END);
  	  
  	//$cs->registerScript('DataMask2',"$.pnotify({title: 'Regular Notice',type: 'success',text: 'Check me out! I\'m a notice.'});");
		
		if (Yii::app()->session['usuario']->primeira_senha == 1) {

            $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/ui-modals.js', CClientScript::POS_END);
            $cs->registerScript('UIModals', 'UIModals.init();');
            $cs->registerScript('ShowModal', '$("#static").modal("show")');
    }

		$this->render('index');
	}


	public function actionChangePassword(){

		$retorno = array('empty' => true);
		
		$baseUrl = Yii::app()->baseUrl; 
	    $cs = Yii::app()->getClientScript();
  	    $cs->registerScriptFile($baseUrl.'/js/jquery.min.js');
  	    $cs->registerScriptFile($baseUrl.'/js/jquery.validate.min.js');
  	    $cs->registerScriptFile($baseUrl.'/js/validate-form-change-pass.js');
  	    $cs->registerScriptFile($baseUrl.'/js/summernote.min.js');
  	    $cs->registerScript('FormWizard','FormValidator.init();');

  	    if ( isset( $_POST['old_password'] ) && isset( $_POST['new_password'] ) && isset( $_POST['password_again'] ) ) {
  	    	
  	    	$senha_antiga =	$_POST['old_password'];
  	    	$nova_senha   =	$_POST['new_password'];

  	    	$retorno = Yii::app()->session['usuario']->changePassword( $senha_antiga, $nova_senha );
  	    }

  	    $this->render('changePass',array( 
  	    	'retorno' => $retorno
  	    ));

		/*if ( !Yii::app()->session['usuario']->primeira_senha ){
			
		}

		else{
			$this->render('changePass');
		}*/
	}
}