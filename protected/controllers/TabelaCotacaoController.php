<?php

class TabelaCotacaoController extends Controller
{

    public function actionGetCondicoes()
    {
        echo json_encode(TabelaCotacao::model()->returnCarenciasXParcelas($_POST['Proposta']['Tabela_id'], $_POST['idFilialCrediarista']));
    }

    public function actionChangeParceiroRelation()
    {
        echo json_encode(TabelaCotacao::model()->parceiroRelationChange($_POST['parceiroId'], $_POST['check'], $_POST['tabelaId']));
    }

    public function actionIndex()
    {

        $this->render('index');
    }

    public function actionTabelaSemear()
    {
        if (isset($_GET['indicador']))
        {
            $cliente = $_GET['indicador'];
            if ($cliente == 0)
            {
                $id = ConfigLN::model()->valorDoParametro('idTabelaCN');
            }
            if ($cliente == 1)
            {
                $id = ConfigLN::model()->valorDoParametro('idTabelaCC');
            }

            $tabela = TabelaCotacao::model()->find('habilitado AND id = ' . $id);

            $retorno[] = array(
                'id' => $tabela->id,
                'text' => $tabela->descricao,
            );
            echo json_encode($retorno);
        } else
        {
            $retorno[] = array(
                'id' => 0,
                'text' => 'Tabela nao encontrada...'
            );
            echo json_encode($retorno);
        }
    }

    public function actionEditar()
    {
        $this->layout = '//layouts/main_sigac_template';

        if (isset($_POST['tabelaId']) && !empty($_POST['tabelaId']))
        {

            //Yii::app()->session['usuario']->updateVistoPorUltimo();

            $baseUrl = Yii::app()->baseUrl;
            $cs = Yii::app()->getClientScript();

            $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
            $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
            $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css');

            $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
            $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/additional-methods.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/fn-editar-tabela-cotacao_v2.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);

            $tabela = TabelaCotacao::model()->findByPk($_POST['tabelaId']);

            $this->render('/cotacao/editar_tabela_cotacao', array(
                'tabela' => $tabela
            ));
        }
    }

    public function actionAtualizarAtributo()
    {
        $tabela = TabelaCotacao::model()->findByPk($_GET['objId']);
        $tabela->setAttribute($_GET['coluna'], $_GET['novoValor']);
        $tabela->update();
    }

    public function actionGetTabelas()
    {

        echo json_encode(TabelaCotacao::model()->listar($_POST['draw'], $_POST['start'], $_POST['order'], $_POST['columns']));
    }

    public function actionAdd()
    {

        if (!empty($_POST['TabelaCotacao']) && !empty($_POST['Filiais']) && !empty($_POST['Financeiras']))
        {
            echo json_encode(TabelaCotacao::model()->novo($_POST['TabelaCotacao'], $_POST['Filiais'], $_POST['Financeiras']));
        }
    }

    public function actionMudarDescricao()
    {

        $arrayReturn = array(
            'hasErrors' => 0,
            'msg' => 'Descrição alterada com Sucesso.',
            'pntfyClass' => 'success'
        );

        if (isset($_POST['idTabela']) && isset($_POST['descricao']))
        {

            $tabela = TabelaCotacao::model()->find('habilitado AND id = ' . $_POST['idTabela']);

            if ($tabela !== null)
            {
                $tabela->descricao = $_POST['descricao'];

                if (!$tabela->update())
                {

                    ob_start();
                    var_dump($tabela->getErrors());
                    $result = ob_get_clean();

                    $arrayReturn = array(
                        'hasErrors' => 1,
                        'msg' => 'Erro encontrado. Tabela: ' . $result,
                        'pntfyClass' => 'error'
                    );
                }
            } else
            {

                $arrayReturn = array(
                    'hasErrors' => 1,
                    'msg' => 'Tabela não encontrada.',
                    'pntfyClass' => 'error'
                );
            }
        } else
        {

            $arrayReturn = array(
                'hasErrors' => 1,
                'msg' => 'POST sem dados.',
                'pntfyClass' => 'error'
            );
        }

        echo json_encode($arrayReturn);
    }

}
