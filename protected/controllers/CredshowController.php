<?php

class CredshowController extends Controller
{
	public $layout = '//layouts/login';

	public function actionIndex(){
		
		header('Access-Control-Allow-Origin: *');
		
		$model = new LoginForm;

		if( isset( $_POST['LoginForm'] ) ){

			$model->attributes = $_POST['LoginForm'];

			if( $model->validate() && $model->login() ){

				$this->redirect( Yii::app()->session['usuario']->getRole()->login_redirect );
			}
		}

		//se o usuario estiver logado na sessão, redireciona para a inicial do sistema
		elseif( !Yii::app()->user->isGuest ){
			$this->redirect(Yii::app()->session['usuario']->getRole()->login_redirect);
		}
		
		$this->render('login',array('model'=>$model));
	}

	public function filters()
    {

      return array(
          'accessControl', // perform access control for CRUD operations
          'postOnly + delete' // we only allow deletion via POST request
      );
    }

    public function accessRules()
    {
    	return array(
    		array('allow',
              'actions' => array('index'),
              'users' => array('*'),
          )
    	);
    }
}