<?php

class ContratoController extends Controller {

    public function actionCcb() {

        $this->layout = '//layouts/template_contrato';

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/login/font-awesome.min.css');
        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery-barcode.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/matematica/fn-custoEfetivo.js', CClientScript::POS_END);

        $this->render('ccb_fidc');
    }

    public function actionCessao() {

        $this->layout = '//layouts/template_contrato';

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/login/font-awesome.min.css');
        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery-barcode.min.js', CClientScript::POS_END);

        $this->render('cessao_credito');
    }

    public function actionCCNova() {
        $this->layout = '//layouts/template_contrato';

        $proposta = Proposta::model()->findByPk(87440);

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/login/font-awesome.min.css');
        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery-barcode.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/matematica/fn-custoEfetivo.js', CClientScript::POS_END);

        $this->render('ccb_fidc', array(
                'proposta' => $proposta
        ));
    }

    public function actionIndex() {

        $this->layout = '//layouts/template_contrato';

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css');
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpalette.css');
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpicker.css');
        $cs->registerCssFile($baseUrl . '/css/login/font-awesome.min.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/financeiro/contasPagar/fn-grid.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/matematica/fn-custoEfetivo-v2.js', CClientScript::POS_END);

        if (isset($_POST['propostaId']) && !empty($_POST['propostaId'])) {
            $proposta = Proposta::model()->findByPk($_POST['propostaId']);

            $sigacLog = new SigacLog;
            $sigacLog->saveLog('Impressão de Contrato', 'Proposta', $proposta->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Crediarista " . Yii::app()->session['usuario']->nome_utilizador . " imprimou o contrato referente a proposta com código: " . $proposta->codigo, "127.0.0.1", null, Yii::app()->session->getSessionId());

            $configContrato = ConfigLN::model()->find("habilitado AND parametro = 'idModeloContratoAtual'");

            if( CompraProgramada::model()->find('habilitado AND Proposta_id = ' . $proposta->id ) != Null ){
                $this->render('compra_programada', array(
                    'proposta' => $proposta
                ));
            }

            else if ($proposta->Financeira_id == 11) {
                if ($proposta->tabelaCotacao->ModalidadeId == 2) {
                    $this->render('ccb_fdic_sj', array(
                            'proposta' => $proposta
                    ));
                } 
                else {
                    $this->render('ccb_fidc', array(
                            'proposta' => $proposta
                    ));
                }
            } 
            
            else if ($proposta->Financeira_id == 10 && $proposta->tabelaCotacao->ModalidadeId == 1) {
                $this->render('cessao_thiago', array(
                        'proposta' => $proposta
                ));
            } 

            else {
                if ($configContrato !== null) {
                    $idModeloContrato = $configContrato->valor;

                    $modeloContrato = ModeloContrato::model()->find("habilitado AND id = $idModeloContrato");

                    if ($modeloContrato !== null) {
                        $viewContrato = $modeloContrato->view;

                        if ($proposta->ModeloContrato_id == null) {

                            $proposta->ModeloContrato_id = $modeloContrato->id;

                            if (!$proposta->update()) {

                                ob_start();
                                var_dump($proposta->getErrors());
                                $resErro = ob_get_clean();

                                $texto = "Proposta<br>"
                                    . "id: $proposta->id <br>"
                                    . "codigo: $proposta->codigo <br>"
                                    . "erro: $resErro";

                                $parametros = [
                                        'email' => [
                                                'titulo' => "Erro ao definir Modelo de Contrato na prposta.",
                                                'texto' => $texto
                                        ]
                                ];             
                            }
                        }
                    } else {
                        $viewContrato = "novo_contrato";
                    }
                } else {
                    $viewContrato = "novo_contrato";
                }

                $this->render($viewContrato, array(
                        'proposta' => $proposta
                ));
            }
        }
    }

    public function actionCartaCredito() {

        $this->layout = '//layouts/template_contrato';

        if (isset($_POST['propostaId']) && !empty($_POST['propostaId'])) {

            $proposta = Proposta::model()->findByPk($_POST['propostaId']);

            $this->render(
                'cartaCredito', array(
                    'proposta' => $proposta
                )
            );
        }
    }

    public function actionSeguro() {
        $this->layout = '//layouts/template_contrato';

        $viewContrato = 'contrato_seguro';

        if (isset($_POST['propostaId']) && !empty($_POST['propostaId'])) {
            $proposta = Proposta::model()->findByPk($_POST['propostaId']);

            $sigacLog = new SigacLog;
            $sigacLog->saveLog('Impressão de Contrato do Seguro', 'Proposta', $proposta->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Crediarista " . Yii::app()->session['usuario']->nome_utilizador . " imprimou o contrato do seguro referente a proposta com código: " . $proposta->codigo, "127.0.0.1", null, Yii::app()->session->getSessionId());

            if($proposta->Financeira_id == 11){
                $viewContrato = 'contrato_seguro_usebens';
            }

            if ($proposta->venda !== NULL) {
                if ($proposta->venda->getProdutoSeguro() !== NULL) {
                    $viewContrato = $proposta->venda->getProdutoSeguro()->itemDoEstoque->getViewContrato();
                }
            }
            $this->render($viewContrato, array(
                    'proposta' => $proposta
            ));
        }
    }

}
