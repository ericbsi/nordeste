<?php

class ProdutoController extends Controller {

    public $layout = '//layouts/main_sigac_template';

    public function actionIndex() {
        
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-produtos-grid.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.pnotify.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/bootstrap-multiselect.js', CClientScript::POS_END);

        $this->render('grid_produtos', array(
            'cores'     => ValoresCaracteristica::model()->findAll('Caracteristica_id = ' . 1),
            'tamanhos'  => ValoresCaracteristica::model()->findAll('Caracteristica_id = ' . 2),
        ));
    }

    public function accessRules() {

        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('add', 'editar', 'listarProdutos', 'create', 'update', 'admin', 'delete', 'index', 'view'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionGetAttributes(){

        $produto = Produto::model()->findByPk( $_POST['Produtos'] );
        echo json_encode( $produto->attributes );
    }

    public function actionGetProdutos() {

        //echo json_encode(Produto::model()->listar($_POST['draw'], $_POST['start'], $_POST['order'], $_POST['columns']));
        echo json_encode(Produto::model()->listar($_GET['draw'], $_GET['start'], $_GET['order'], $_GET['columns']));
    }

    public function actionAdd() {
        echo json_encode( Produto::model()->novo( $_POST['Produto'],$_POST['Cores'],$_POST['Tamanhos'] ) );
        //echo json_encode ($_POST);
    }

    public function actionSearchProdutos(){

        $idsPro = array();

        if( isset( $_POST['idsPro'] ) && !empty( $_POST['idsPro'] ) )
        {
            $idsPro = $_POST['idsPro'];
        }

        echo json_encode( Produto::model()->searchProdutosSelec2( $_POST['q'], $idsPro ) );
    }
}
