<?php

class MobileController extends Controller {

//    public $layout = '//layouts/boleto_layout';

    public function filters() {

        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete' // we only allow deletion via POST request
        );
    }

    public function accessRules() {

        return array(
            array('allow',
                'actions' => array('testes', 'login', 'producao', 'getTabelas', 'getFatores'),
                'users' => array('*'),
//                'expression'    => 'Yii::app()->session["usuario"]->role == "super"'
            ),
            /*          array('allow',
              'actions' => array('producao'),
              'users' => array('@'),
              //                'expression'    => 'Yii::app()->session["usuario"]->role == "super"'
              ), */
            array(
                'deny',
                'users' => array('*')
            )
        );
    }

    public function actionLogin() {

        header('Access-Control-Allow-Origin: *');

        $this->layout = 'empty_template';

        if ($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST)) {
            
            $_POST = json_decode(file_get_contents('php://input'), true);
            
        }

        if (isset($_POST['username'])) {

            $mobile = new Mobile;

            $retorno = $mobile->login($_POST['username'], $_POST['password']);

            if ($retorno) {
                echo json_encode(array('resposta' => 'Login efetuado com sucesso'));
            } else {
                echo json_encode(array('resposta' => 'Usuário e ou senha incorreto(s)'));
            }
        } else {
            echo json_encode(array('resposta' => 'Faltou parâmetros.'));
        }
    }

    public function actionGetTabelas() {

        header('Access-Control-Allow-Origin: *');

        $this->layout = 'empty_template';

        if ($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST)) {
            
            $_POST = json_decode(file_get_contents('php://input'), true);
            
        }

        if (isset($_POST['username'])) {

            $mobile = new Mobile;

            $retorno = $mobile->login($_POST['username'], $_POST['password']);

            if ($retorno) {

                $tabelas = $mobile->getTabelas();

                if ($tabelas !== null && sizeof($tabelas) > 0) {
                    echo json_encode(array('resposta' => true, 'dados' => $tabelas, 'mensagem' => 'Tabelas retornada'));
                } else {
                    echo json_encode(array('resposta' => false, 'dados' => null, 'mensagem' => 'Nenhuma tabela retornada'));
                }
            } else {
                echo json_encode(array('resposta' => false, 'dados' => null, 'mensagem' => 'Erro ao fazer login. Verifique sua sessão'));
            }
        } else {
            echo json_encode(array('resposta' => false, 'dados' => null, 'mensagem' => 'Erro ao fazer login. Envie dados do login'));
        }
    }

    public function actionGetFatores() {

        header('Access-Control-Allow-Origin: *');

        $this->layout = 'empty_template';

        if ($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST)) {
            
            $_POST = json_decode(file_get_contents('php://input'), true);
            
        }

        if (isset($_POST['username'])) {

            $mobile = new Mobile;

            $retorno = $mobile->login($_POST['username'], $_POST['password']);

            if ($retorno) {

                if (isset($_POST['idTabela'])) {

                    $tabela = TabelaCotacao::model()->find('habilitado AND id = ' . $_POST['idTabela']);

                    if ($tabela !== null && sizeof($tabela) > 0) {
                        $retorno = $mobile->getFatores($tabela->id);

                        if ($retorno !== null && sizeof($retorno) > 0) {
                            echo json_encode(array('resposta' => false, 'dados' => $retorno, 'mensagem' => 'Carências e Parcelas Encontradas'));
                        } else {
                            echo json_encode(array('resposta' => false, 'dados' => null, 'mensagem' => 'Nenhuma carência encontrada'));
                        }
                    } else {
                        echo json_encode(array('resposta' => false, 'dados' => null, 'mensagem' => 'Nenhuma tabela encontrada'));
                    }
                } else {
                    echo json_encode(array('resposta' => false, 'dados' => null, 'mensagem' => 'Envie o id da Tabela'));
                }
            } else {
                echo json_encode(array('resposta' => false, 'dados' => null, 'mensagem' => 'Erro ao fazer login. Verifique sua sessão'));
            }
        } else {
            echo json_encode(array('resposta' => false, 'dados' => null, 'mensagem' => 'Erro ao fazer login. Envie dados do login'));
        }
    }

    public function actionTestes() {

        header('Access-Control-Allow-Origin: *');

        $this->layout = 'empty_template';

        $retorno = [];
        $filiais = Filial::model()->findAll();

//        var_dump($filiais);

        foreach ($filiais as $f) {
            $retorno[] = ['id' => $f->id, 'nome' => $f->nome_fantasia];
        }

        echo json_encode($retorno);
    }

    public function actionProducao() {

        header('Access-Control-Allow-Origin: *');

        $this->layout = 'empty_template';

        $filIds = [];

        $filiais = Filial::model()->findAll();

        foreach ($filiais as $f) {
            $filIds[] = $f->id;
        }

        $retorno = Empresa::model()->producaoGeral($filIds, "01/01/2013", "31/12/2015");
        /*
          var_dump($retorno);

          echo '<br><br>';

          $this->actionTestes();

          echo '<br><br>'; */

        echo json_encode($retorno);
        //echo json_encode(array('teste'=>'agora vai'));
    }

    public function init() {
        
    }

}
