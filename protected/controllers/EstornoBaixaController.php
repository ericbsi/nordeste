<?php

class EstornoBaixaController extends Controller {

    public $layout = '//layouts/main_sigac_template';

    public function filters() 
    {

        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules() 
    {

        return array(
            array('allow',
                  'actions'     => array(
                                            'index'                     ,
                                            'parcelas'                  ,
                                            'gridEstornos'              ,
                                            'gridEstornosParcelas'      ,
                                            'gridMensagens'             ,
                                            'confirmarStatusEstorno'    ,
                                            'voltarEstorno'             ,
                                        ),
                  'users'       => array(
                                            '@'
                                        ),
                  'expression'  => 'Yii::app()->session["usuario"]->role == "financeiro"    ||  '
                                .  'Yii::app()->session["usuario"]->role == "empresa_admin"  ||   '
                                .  'Yii::app()->session["usuario"]->role == "analista_de_credito"   '
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() 
    {
        $this->layout   = '//layouts/main_sigac_template'   ;
        $baseUrl        = Yii::app()->baseUrl               ;
        $cs             = Yii::app()->getClientScript()     ;

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css'  );
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css'     );
        $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css'          );
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpalette.css'                   );
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpicker.css'                    );

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js'                                                                  );
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js'                        , CClientScript::POS_END    );
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js'                         , CClientScript::POS_END    );
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js'                                     , CClientScript::POS_END    );
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js'                              , CClientScript::POS_END    );
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js'                                 , CClientScript::POS_END    );
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js'                          , CClientScript::POS_END    );
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js'                              , CClientScript::POS_END    );
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js'                              , CClientScript::POS_END    );
        $cs->registerScriptFile($baseUrl . '/js/estornoBaixa/fn-solicitacoes.js'                    , CClientScript::POS_END    );
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js'                               , CClientScript::POS_END    );
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js'                                  , CClientScript::POS_END    );
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js'                           , CClientScript::POS_END    );
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpicker.js'                           , CClientScript::POS_END    );
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpalette.js'                          , CClientScript::POS_END    );
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-filestyle.min.js'    , CClientScript::POS_END    );
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js'                , CClientScript::POS_END    );

        $this->render('solicitacoes');
        
    }

    public function actionParcelas() 
    {
        $this->layout   = '//layouts/main_sigac_template'   ;
        $baseUrl        = Yii::app()->baseUrl               ;
        $cs             = Yii::app()->getClientScript()     ;

        
        $cs->registerCssFile    ($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css'                                  );
        $cs->registerCssFile    ($baseUrl . '/css/sigax_template/dataTables.tableTools.css'                                     );
        $cs->registerCssFile    ($baseUrl . '/css/sigax_template/iCheck/skins/all.css'                                          );
        $cs->registerCssFile    ($baseUrl . '/css/bootstrap-colorpalette.css'                                                   );
        $cs->registerCssFile    ($baseUrl . '/css/bootstrap-colorpicker.css'                                                    );

        $cs->registerScriptFile ($baseUrl . '/js/jquery.min.js'                                                                 );
        $cs->registerScriptFile ($baseUrl . '/js/jquery.dataTables-1.10.0.js'                       , CClientScript::POS_END    );
        $cs->registerScriptFile ($baseUrl . '/js/dataTables.fnReloadAjax.js'                        , CClientScript::POS_END    );
        $cs->registerScriptFile ($baseUrl . '/js/select2.min.js'                                    , CClientScript::POS_END    );
        $cs->registerScriptFile ($baseUrl . '/js/jquery.maskedinput.js'                             , CClientScript::POS_END    );
        $cs->registerScriptFile ($baseUrl . '/js/bootstrap-modal.js'                                , CClientScript::POS_END    );
        $cs->registerScriptFile ($baseUrl . '/js/bootstrap-modalmanager.js'                         , CClientScript::POS_END    );
        $cs->registerScriptFile ($baseUrl . '/js/jquery.validate.12.js'                             , CClientScript::POS_END    );
        $cs->registerScriptFile ($baseUrl . '/js/additional-methods.js'                             , CClientScript::POS_END    );
        $cs->registerScriptFile ($baseUrl . '/js/estornoBaixa/fn-solicitacoesParcelas.js'           , CClientScript::POS_END    );
        $cs->registerScriptFile ($baseUrl . '/js/jquery.icheck.min.js'                              , CClientScript::POS_END    );
        $cs->registerScriptFile ($baseUrl . '/js/jquery.blockUI.js'                                 , CClientScript::POS_END    );
        $cs->registerScriptFile ($baseUrl . '/js/bootstrap-multiselect.js'                          , CClientScript::POS_END    );
        $cs->registerScriptFile ($baseUrl . '/js/bootstrap-colorpicker.js'                          , CClientScript::POS_END    );
        $cs->registerScriptFile ($baseUrl . '/js/bootstrap-colorpalette.js'                         , CClientScript::POS_END    );
        $cs->registerScriptFile ($baseUrl . '/js/bootstrap-filestyle.min.js'                        , CClientScript::POS_END    );
        $cs->registerScriptFile ($baseUrl . '/js/jquery.form.js'                                    , CClientScript::POS_END    );

        
        $this->render('solicitacoesParcelas');
        
    }
    
    public function actionGridEstornos()
    {
        
        $btnAprovar = "";
        
        $naturezaTituloPagarEmprestimo = ConfigLN::model()->find("habilitado AND parametro = 'naturezaTituloPagarEmprestimo'");
        
        if($naturezaTituloPagarEmprestimo !== null)
        {
            $idNaturezaEmprestimo = $naturezaTituloPagarEmprestimo->valor;
        }
        else
        {
            $idNaturezaEmprestimo = 32; //valor padrao
        }
        
        $retorno = [];
        
        $query = "
                    SELECT  EB.id AS idEstorno,  EB.data_cadastro AS dataSolicitacao, Pr.codigo AS codigoProposta, Pr.data_cadastro AS dataProposta, B.data_da_baixa AS dataBaixa,
                            EB.dataRetorno AS dataRetorno, Pe.nome AS nomeCliente, D.numero AS cpf, Ba.codigo AS codigoBanco, Ba.nome AS nomeBanco, DB.agencia AS agenciaDB,
                            DB.numero AS numeroDB, DB.operacao AS operacaoDB, A.relative_path AS caminhoAnexo, P.valor AS valor, DP.comprovante AS comprovantePG, EB.comprovante AS comprovanteEB,
                            MAX(EBS.StatusEstorno_id) AS statusMaximo 
                            #*
                    FROM 	EstornoBaixa                    AS EB 
                    INNER JOIN  EstornoBaixa_has_StatusEstorno  AS EBS  ON EBS.habilitado   AND EBS.EstornoBaixa_id         = EB.id 
                    INNER JOIN 	Baixa                           AS B 	ON 			 EB.Baixa_id                =  B.id
                    INNER JOIN 	Parcela                         AS P 	ON   P.habilitado   AND   B.Parcela_id              =  P.id
                    INNER JOIN 	Titulo                          AS T 	ON   T.habilitado   AND   P.Titulo_id               =  T.id		AND T.NaturezaTitulo_id             = $idNaturezaEmprestimo
                    INNER JOIN	Proposta                        AS Pr	ON   P.habilitado   AND	  T.Proposta_id             = Pr.id
                    INNER JOIN	Analise_de_Credito              AS AC	ON  AC.habilitado   AND	 Pr.Analise_de_Credito_id   = AC.id
                    INNER JOIN	Cliente                         AS C	ON   C.habilitado   AND	 AC.Cliente_id              =  C.id
                    INNER JOIN	Pessoa                          AS Pe	ON  Pe.habilitado   AND	  C.Pessoa_id               = Pe.id
                    INNER JOIN	Pessoa_has_Documento            AS PhD	ON PhD.habilitado   AND	PhD.Pessoa_id               = Pe.id
                    INNER JOIN	Documento                       AS D	ON   D.habilitado   AND	PhD.Documento_id            =  D.id             AND D.Tipo_documento_id             = 1
                    INNER JOIN	Bordero_has_Parcela             AS BhP	ON BhP.habilitado   AND	BhP.Parcela_id              =  P.id
                    INNER JOIN	Bordero                         AS Bo	ON  Bo.habilitado   AND	BhP.Bordero_id              = Bo.id
                    INNER JOIN	DadosPagamento                  AS DP	ON                        B.DadosPagamento_id       = DP.id
                    INNER JOIN	Dados_Bancarios                 AS DB	ON  DB.habilitado   AND	 DP.Dados_Bancarios_Credito = DB.id
                    INNER JOIN  Banco                           AS Ba   ON                       DB.Banco_id                = Ba.id
                    INNER JOIN  Anexo                           AS A    ON   A.habilitado   AND   A.entidade_relacionamento = 'DadosPagamento'  AND A.id_entidade_relacionamento    = DP.id
                    WHERE EB.habilitado
                    GROUP BY    EB.id           , EB.data_cadastro  , Pr.codigo     , Pr.data_cadastro  , B.data_da_baixa   ,
                                EB.dataRetorno  , Pe.nome           , D.numero      , Ba.codigo         , Ba.nome           , 
                                DB.agencia      , DB.numero         , DB.operacao   , A.relative_path   , P.valor           , 
                                DP.comprovante  , EB.comprovante 
                
                ";
        
        $resultado = Yii::app()->db->createCommand($query)->queryAll();
        
        foreach ($resultado as $r)
        {
            
            $datetime           = new DateTime($r["dataProposta"        ]   );
            $dataProposta       = $datetime->format("d/m/Y"                 );
            
            $datetime           = new DateTime($r["dataBaixa"           ]   );
            $dataPagamento      = $datetime->format("d/m/Y"                 );
            
            $datetime           = new DateTime($r["dataSolicitacao"     ]   );
            $dataSolicitacao    = $datetime->format("d/m/Y"                 );
            
            $datetime           = new DateTime($r["dataRetorno"         ]   );
            $dataRetorno        = $datetime->format("d/m/Y"                 );
            
            $comprovantePG = "<a href='" . $r["caminhoAnexo"] . "' target='blank'>" . $r["comprovantePG"] . "</a>";
            
            $valorParcela = "R$ " . number_format($r["valor"],2,".",",");
            
            $dadosBancarios = "Banco: " . $r["codigoBanco"] . " - " . $r["nomeBanco"] . " | Agência: " . $r["agenciaDB"] . " | Conta: " . $r["numeroDB"] . " | Operação: " . $r["operacaoDB"];;
            
            if($r["statusMaximo"] == 1)
            {
                $btnAprovar = "<button class='btn btn-green btnAprovar' value='" . $r["idEstorno"] . "'><i class='fa fa-thumbs-up'></i></button>";
            }
            else
            {
                $btnAprovar = "<button class='btn btn-gray btnAprovar' disabled value='" . $r["idEstorno"] . "'><i class='clip-checkmark-circle-2'></i></button>";
            }
            
            $retorno[] =    [
                                "idEstorno"         => $r["idEstorno"       ]   ,
                                "proposta"          => $r["codigoProposta"  ]   ,
                                "dataProposta"      => $dataProposta            ,
                                "dataPagamento"     => $dataPagamento           ,
                                "dataSolicitacao"   => $dataSolicitacao         ,
                                "dataRetorno"       => $dataRetorno             ,
                                "cliente"           => $r["nomeCliente"     ]   ,
                                "cpf"               => $r["cpf"             ]   ,
                                "dadosBancarios"    => $dadosBancarios          ,
                                "anexo"             => $r["caminhoAnexo"    ]   ,
                                "valor"             => $valorParcela            ,
                                "comprovantePG"     => $comprovantePG           ,
                                "comprovanteEB"     => $r["comprovanteEB"   ]   ,
                                "btnAprovar"        => $btnAprovar

                            ];
            
        }
        
        echo json_encode(
                            [
                                'data'              => $retorno         ,
                                'recordsFiltered'   => count($retorno)  ,
                                'recordsTotal'      => count($retorno)
                            ]
                        );
        
        
    }
    
    public function actionGridEstornosParcelas()
    {
        
        $btnAprovar     = "";
        $btnMensagens   = "";
        
        $retorno = [];
        
        $query = "
                    SELECT  EB.id               AS idEstorno    , EB.data_cadastro      AS dataSolicitacao  , Pr.codigo                     AS codigoProposta   , 
                            Pr.data_cadastro    AS dataProposta , B.data_da_baixa       AS dataBaixa        , Pe.nome                       AS nomeCliente      , 
                            D.numero            AS cpf          , P.valor               AS valorParcela     , MAX(EBS.StatusEstorno_id  )   AS statusMaximo     ,
                            P.vencimento        AS vencimento   , B.valor_pago          AS valorBaixa       , MAX(EBS.id                )   AS idStatusMaximo   ,    
                                                                                                              MIN(EBS.id                )   AS idStatusMinimo   
                            #*
                    FROM 	EstornoBaixa                    AS EB 
                    INNER JOIN  EstornoBaixa_has_StatusEstorno  AS EBS  ON EBS.habilitado   AND EBS.EstornoBaixa_id         = EB.id 
                    INNER JOIN 	Baixa                           AS B 	ON 			 EB.Baixa_id                =  B.id
                    INNER JOIN 	Parcela                         AS P 	ON   P.habilitado   AND   B.Parcela_id              =  P.id
                    INNER JOIN 	Titulo                          AS T 	ON   T.habilitado   AND   P.Titulo_id               =  T.id		AND T.NaturezaTitulo_id             = 1
                    INNER JOIN	Proposta                        AS Pr	ON   P.habilitado   AND	  T.Proposta_id             = Pr.id
                    INNER JOIN	Analise_de_Credito              AS AC	ON  AC.habilitado   AND	 Pr.Analise_de_Credito_id   = AC.id
                    INNER JOIN	Cliente                         AS C	ON   C.habilitado   AND	 AC.Cliente_id              =  C.id
                    INNER JOIN	Pessoa                          AS Pe	ON  Pe.habilitado   AND	  C.Pessoa_id               = Pe.id
                    INNER JOIN	Pessoa_has_Documento            AS PhD	ON PhD.habilitado   AND	PhD.Pessoa_id               = Pe.id
                    INNER JOIN	Documento                       AS D	ON   D.habilitado   AND	PhD.Documento_id            =  D.id             AND D.Tipo_documento_id             = 1
                    WHERE EB.habilitado
                    GROUP BY    EB.id           , EB.data_cadastro  , Pr.codigo , Pr.data_cadastro      , B.data_da_baixa       ,
                                Pe.nome         , D.numero          , P.valor   , 
                                P.vencimento
                ";
        
        $resultado = Yii::app()->db->createCommand($query)->queryAll();
        
        foreach ($resultado as $r)
        {
            
            $solicitante = "";
            $responsavel = "";
            
            $datetime           = new DateTime($r["dataProposta"            ]   );
            $dataProposta       = $datetime->format("d/m/Y"                     );
            
            $datetime           = new DateTime($r["dataBaixa"               ]   );
            $dataBaixa          = $datetime->format("d/m/Y"                     );
            
            $datetime           = new DateTime($r["dataSolicitacao"         ]   );
            $dataSolicitacao    = $datetime->format("d/m/Y"                     );
            
            $datetime           = new DateTime($r["vencimento"              ]   );
            $vencimento         = $datetime->format("d/m/Y"                     );
            
            $valorParcela       = "R$ " . number_format($r["valorParcela"   ],2,".",",");
            $valorBaixa         = "R$ " . number_format($r["valorBaixa"     ],2,".",",");
            
            $btnMensagens   =   "<button class='btn btn-blue btnMensagens' value='" . $r["idEstorno"] . "' style='padding : 0px; width: 100%; height: 100%'>"
                            .   "   <i class='fa fa-envelope'></i>"
                            .   "</button>";
                
            $estornoHasStatus = EstornoBaixaHasStatusEstorno::model()->find("habilitado AND id = " . $r["idStatusMinimo"]);

            if($estornoHasStatus !== null)
            {

                $usuario = Usuario::model()->find("habilitado AND id = $estornoHasStatus->Usuario_id");

                if($usuario !== null )
                {
                    $solicitante = $usuario->nome_utilizador;
                }

            }
            
            if($r["statusMaximo"] == 1)
            {
                
                $btnAprovar = "<div class='row'>"
                            . "     <div class='col-sm-6' style='padding-right : 1px!important;'>"
                            . "         <button class='btn btn-green btnMudarStatus' "
                            . "                 title='Aprovar' "
                            . "                 value='" . $r["idEstorno"] . "' "
                            . "                 style='padding : 0px; width: 100%; height: 100%'"
                            . "                 data-status='Aprovar'"
                            . "                 data-id-status='2'>"
                            . "             <i class='fa fa-thumbs-up'></i>"
                            . "         </button>"
                            . "     </div>"
                            . "     <div class='col-sm-6' style='padding-left : 1px!important;'>"
                            . "         <button class='btn btn-red   btnMudarStatus' "
                            . "                 title='Negar'   "
                            . "                 value='" . $r["idEstorno"] . "' "
                            . "                 style='padding : 0px; width: 100%; height: 100%'"
                            . "                 data-status='Negar'"
                            . "                 data-id-status='3'>"
                            . "             <i class='fa fa-thumbs-down'></i>"
                            . "         </button>"
                            . "     </div>"
                            . "</div>";
            }
            else
            {
                
                $estornoHasStatus = EstornoBaixaHasStatusEstorno::model()->find("habilitado AND id = " . $r["idStatusMaximo"]);
                
                if($estornoHasStatus !== null)
                {
                    
                    $usuario = Usuario::model()->find("habilitado AND id = $estornoHasStatus->Usuario_id");
                    
                    if($usuario !== null )
                    {
                        $responsavel = $usuario->nome_utilizador;
                    }
                    
                }
                
                $btnAprovar = "<div class='row'>"
                            . "     <div class='col-sm-6' style='padding-right : 1px!important;'>";
                
                if($r["statusMaximo"] == 2)
                {
                    $btnAprovar .=  "   <button class='btn btn-green' title='Aprovado' disabled style='padding : 0px; width: 100%; height: 100%'>"
                                .   "       <i class='fa fa-smile-o'></i>"
                                .   "   </button>";
                    
                }
                else
                {
                    $btnAprovar .=  "   <button class='btn btn-red'   title='Negado'   disabled style='padding : 0px; width: 100%; height: 100%'>"
                                .   "       <i class='fa fa-frown-o'></i>"
                                .   "   </button>";
                }
                
                $btnAprovar .=  "    </div>"
                            .   "    <div class='col-sm-6' style='padding-left : 1px!important;'>"
                            .   "       <button class='btn btn-orange btnVoltarEstorno' title='Voltar Estorno' value='" . $r["idEstorno"] . "' style='padding : 0px; width: 100%; height: 100%'>"
                            .   "           <i class='fa fa-undo'></i>"
                            .   "       </button>"
                            .   "    </div>"
                            .   "</div>";
            }
            
            $retorno[] =    [
                                "idEstorno"         => $r["idEstorno"       ]   ,
                                "proposta"          => $r["codigoProposta"  ]   ,
                                "dataProposta"      => $dataProposta            ,
                                "vencimento"        => $vencimento              ,
                                "dataBaixa"         => $dataBaixa               ,
                                "dataSolicitacao"   => $dataSolicitacao         ,
                                "solicitante"       => $solicitante             ,
                                "responsavel"       => $responsavel             ,
                                "cliente"           => $r["nomeCliente"     ]   ,
                                "cpf"               => $r["cpf"             ]   ,
                                "valorParcela"      => $valorParcela            ,
                                "valorBaixa"        => $valorBaixa              ,
                                "btnMensagens"      => $btnMensagens            ,
                                "btnAprovar"        => $btnAprovar

                            ];
            
        }
        
        echo json_encode(
                            [
                                'data'              => $retorno         ,
                                'recordsFiltered'   => count($retorno)  ,
                                'recordsTotal'      => count($retorno)
                            ]
                        );
        
        
    }
    
    public function actionAprovar()
    {
            
        $idStatusEstornoAprovado = ConfigLN::model()->find("habilitado AND parametro = 'idStatusEstornoAprovado' ")->valor;
        
        $retorno =  [
                        "type"      => "success"                        ,
                        "msg"       => "Estorno aprovado com sucesso"   ,
                        "hasErrors" => false
                    ];
        
        $transaction = Yii::app()->db->beginTransaction();
        
        if(isset($_POST["idEstorno"]) && !empty($_POST["idEstorno"]))
        {
            
            $idEstorno = $_POST["idEstorno"];
            
            $estorno = EstornoBaixa::model()->find("habilitado AND id = $idEstorno");
            
            if($estorno !== null)
            {
                
                $baixa = Baixa::model()->find("baixado AND id = $estorno->Baixa_id");
                
                if($baixa !== null)
                {
                    
                    $baixa->baixado = 0;
                    
                    if($baixa->update())
                    {
                        
                        $dadosPagamento = DadosPagamento::model()->find("habilitado AND id = $baixa->DadosPagamento_id");
                        
                        if($dadosPagamento !== null)
                        {
                            
                            $dadosPagamento->habilitado = 0;
                            
                            if($dadosPagamento->update())
                            {
                                
                                $estornoHasStatus                    = new EstornoBaixaHasStatusEstorno                      ;
                                
                                $estornoHasStatus->habilitado        = 1                                                     ;
                                $estornoHasStatus->data_cadastro     = date("Y-m-d H:i:s")                                   ;
                                $estornoHasStatus->EstornoBaixa_id   = $estorno->id                                          ;
                                $estornoHasStatus->Usuario_id        = Yii::app()->session["usuario"]->id                    ;
                                $estornoHasStatus->StatusEstorno_id  = $idStatusEstornoAprovado                              ;
                                $estornoHasStatus->observacao        = "Usuário " . Yii::app()->session["usuario"]->username .
                                                                      " aprovou o estorno."                                 ;
                                
                                if($estornoHasStatus->save())
                                {
                                    
                                    $borderoHasParcela = BorderoHasParcela::model()->find("habilitado AND Parcela_id = $baixa->Parcela_id");
                                    
                                    if($borderoHasParcela !== null && isset($borderoHasParcela->bordero))
                                    {
                                        
                                        $borderoHasParcela->bordero->Status = 1;
                                        
                                        if($borderoHasParcela->bordero->update())
                                        {
                                            $transaction->commit();
                                        }
                                        else
                                        {
                        
                                            $transaction->rollBack()                            ;

                                            ob_start()                                          ;
                                            var_dump($borderoHasParcela->bordero->getErrors())  ;
                                            $resultado = ob_get_clean()                         ;

                                            $retorno["type"         ] = "success"                                       ;
                                            $retorno["msg"          ] = "Erro ao estornar baixa. Bordero: $resultado"   ;
                                            $retorno["hasErrors"    ] = true                                            ;
                                            
                                        }
                                        
                                    }
                                    else
                                    {

                                        $transaction->rollBack()                                ;

                                        $retorno["type"         ] = "success"                   ;
                                        $retorno["msg"          ] = "Borderô não encontrado"    ;
                                        $retorno["hasErrors"    ] = true                        ;
                                        
                                    }
                                    
                                }
                                else
                                {
                        
                                    $transaction->rollBack()                    ;

                                    ob_start()                                  ;
                                    var_dump($estornoHasStatus->getErrors())    ;
                                    $resultado = ob_get_clean()                 ;

                                    $retorno["type"         ] = "success"                                                           ;
                                    $retorno["msg"          ] = "Erro ao estornar baixa. EstornoBaixaHasStatusEstorno: $resultado"  ;
                                    $retorno["hasErrors"    ] = true                                                                ;
                                    
                                }
                                
                            }
                            else
                            {
                        
                                $transaction->rollBack()                ;

                                ob_start()                              ;
                                var_dump($dadosPagamento->getErrors())  ;
                                $resultado = ob_get_clean()             ;

                                $retorno["type"         ] = "success"                                               ;
                                $retorno["msg"          ] = "Erro ao estornar baixa. DadosPagamento: $resultado"    ;
                                $retorno["hasErrors"    ] = true                                                    ;
                                
                            }
                            
                        }
                        else
                        {
                        
                            $transaction->rollBack()                                        ;
                
                            $retorno["type"         ] = "success"                           ;
                            $retorno["msg"          ] = "Dados do pagamento não encontrado" ;
                            $retorno["hasErrors"    ] = true                                ;
                            
                        }
                        
                    }
                    else
                    {
                        
                        $transaction->rollBack();
                        
                        ob_start()                      ;
                        var_dump($baixa->getErrors())   ;
                        $resultado = ob_get_clean()     ;
                
                        $retorno["type"         ] = "success"                                   ;
                        $retorno["msg"          ] = "Erro ao estornar baixa. Baixa: $resultado" ;
                        $retorno["hasErrors"    ] = true                                        ;
                        
                    }
                    
                }
                else
                {
                
                    $retorno["type"         ] = "success"               ;
                    $retorno["msg"          ] = "Baixa não encontrada"  ;
                    $retorno["hasErrors"    ] = true                    ;
                
                }
                
            }
            else
            {
                
                $retorno["type"         ] = "success"                   ;
                $retorno["msg"          ] = "Estorno não encontrado"    ;
                $retorno["hasErrors"    ] = true                        ;
            
            }
            
        }
        else
        {
            
            $retorno["type"         ] = "success"                       ;
            $retorno["msg"          ] = "ID do Estorno não encontrado"  ;
            $retorno["hasErrors"    ] = true                            ;
            
        }
        
        echo json_encode($retorno);
        
    }
    
    public function actionConfirmarStatusEstorno()
    {
        
        if(isset($_POST["idEstorno"]) && !empty($_POST["idEstorno"]))
        {
            
            $idEstorno  = $_POST["idEstorno"];
            
            if(isset($_POST["idStatus"]) && !empty($_POST["idStatus"]))
            {
                
                $observacao = "";
                
                if(isset($_POST["observacao"]) && !empty($_POST["observacao"]))
                {
                    $observacao = $_POST["observacao"];
                }
                
                if($_POST["idStatus"] == "2")
                {
                    $retorno    = $this->aprovarParcela ($idEstorno, $observacao);
                }
                else
                {
                    $retorno    = $this->negarParcela   ($idEstorno, $observacao);
                    
                }
                
            }
            else
            {
            
                $retorno["type"         ] = "error"                                     ;
                $retorno["msg"          ] = "ID do Status do Estorno não encontrado"    ;
                $retorno["hasErrors"    ] = true                                        ;
                
            }
            
        }
        else
        {
            
            $retorno["type"         ] = "error"                         ;
            $retorno["msg"          ] = "ID do Estorno não encontrado"  ;
            $retorno["hasErrors"    ] = true                            ;
            
        }
        
        echo json_encode($retorno);
        
    }
    
    public function negarParcela($idEstorno, $observacao)
    {
        
        $retorno =  [
                        "type"      => "success"                    ,
                        "msg"       => "Estorno negado com sucesso" ,
                        "hasErrors" => false
                    ];
            
        $estorno = EstornoBaixa::model()->find("habilitado AND id = $idEstorno");

        if($estorno !== null)
        {

            $estornoHasStatus                    = new EstornoBaixaHasStatusEstorno                                 ;

            $estornoHasStatus->habilitado        =  1                                                               ;
            $estornoHasStatus->data_cadastro     =  date("Y-m-d H:i:s")                                             ;
            $estornoHasStatus->EstornoBaixa_id   =  $estorno->id                                                    ;
            $estornoHasStatus->Usuario_id        =  Yii::app()->session["usuario"]->id                              ;
            $estornoHasStatus->StatusEstorno_id  =  3                                                               ;
            $estornoHasStatus->observacao        =  "Usuário " . Yii::app()->session["usuario"]->username       .
                                                    " negou o estorno." . chr(13) . chr(10) . "Observação: "    .
                                                    $observacao                                                     ;

            if(!$estornoHasStatus->save())
            {

                ob_start()                                  ;
                var_dump($estornoHasStatus->getErrors())    ;
                $resultado = ob_get_clean()                 ;

                $retorno["type"         ] = "error"                                                                 ;
                $retorno["msg"          ] = "Erro ao negar estorno baixa. EstornoBaixaHasStatusEstorno: $resultado" ;
                $retorno["hasErrors"    ] = true                                                                    ;

            }

        }
        else
        {

            $retorno["type"         ] = "error"                     ;
            $retorno["msg"          ] = "Estorno não encontrado"    ;
            $retorno["hasErrors"    ] = true                        ;

        }
        
        return $retorno;
                    
    }
    
    public function aprovarParcela($idEstorno, $observacao)
    {
            
        $idStatusEstornoAprovado = ConfigLN::model()->find("habilitado AND parametro = 'idStatusEstornoAprovado' ")->valor;
        
        $retorno =  [
                        "type"      => "success"                        ,
                        "msg"       => "Estorno aprovado com sucesso"   ,
                        "hasErrors" => false
                    ];
        
        $transaction = Yii::app()->db->beginTransaction();
            
        $estorno = EstornoBaixa::model()->find("habilitado AND id = $idEstorno");

        if($estorno !== null)
        {

            $baixa = Baixa::model()->find("baixado AND id = $estorno->Baixa_id");

            if($baixa !== null)
            {

                $baixa->baixado = 0;

                if($baixa->update())
                {

                    $estornoHasStatus                    = new EstornoBaixaHasStatusEstorno                                 ;

                    $estornoHasStatus->habilitado        =  1                                                               ;
                    $estornoHasStatus->data_cadastro     =  date("Y-m-d H:i:s")                                             ;
                    $estornoHasStatus->EstornoBaixa_id   =  $estorno->id                                                    ;
                    $estornoHasStatus->Usuario_id        =  Yii::app()->session["usuario"]->id                              ;
                    $estornoHasStatus->StatusEstorno_id  =  $idStatusEstornoAprovado                                        ;
                    $estornoHasStatus->observacao        =  "Usuário " . Yii::app()->session["usuario"]->username       .
                                                            " aprovou o estorno." . chr(13) . chr(10) . "Observação: "  .
                                                            $observacao                                                     ;

                    if($estornoHasStatus->save())
                    {


                        $parcela = Parcela::model()->find("habilitado AND id = $baixa->Parcela_id");

                        if($parcela !== null)
                        {

                            $parcela->valor_atual = 0;

                            if($parcela->update())
                            {
                                $transaction->commit();
                            }
                            else
                            {

                                $transaction->rollBack()        ;

                                ob_start()                      ;
                                var_dump($parcela->getErrors()) ;
                                $resultado = ob_get_clean()     ;

                                $retorno["type"         ] = "error"                                         ;
                                $retorno["msg"          ] = "Erro ao estornar baixa. Parcela: $resultado"   ;
                                $retorno["hasErrors"    ] = true                                            ;

                            }

                        }
                        else
                        {

                            $retorno["type"         ] = "error"                 ;
                            $retorno["msg"          ] = "Baixa não encontrada"  ;
                            $retorno["hasErrors"    ] = true                    ;

                        }

                    }
                    else
                    {

                        $transaction->rollBack()                    ;

                        ob_start()                                  ;
                        var_dump($estornoHasStatus->getErrors())    ;
                        $resultado = ob_get_clean()                 ;

                        $retorno["type"         ] = "error"                                                             ;
                        $retorno["msg"          ] = "Erro ao estornar baixa. EstornoBaixaHasStatusEstorno: $resultado"  ;
                        $retorno["hasErrors"    ] = true                                                                ;

                    }

                }
                else
                {

                    $transaction->rollBack();

                    ob_start()                      ;
                    var_dump($baixa->getErrors())   ;
                    $resultado = ob_get_clean()     ;

                    $retorno["type"         ] = "error"                                     ;
                    $retorno["msg"          ] = "Erro ao estornar baixa. Baixa: $resultado" ;
                    $retorno["hasErrors"    ] = true                                        ;

                }

            }
            else
            {

                $retorno["type"         ] = "error"                 ;
                $retorno["msg"          ] = "Baixa não encontrada"  ;
                $retorno["hasErrors"    ] = true                    ;

            }

        }
        else
        {

            $retorno["type"         ] = "error"                     ;
            $retorno["msg"          ] = "Estorno não encontrado"    ;
            $retorno["hasErrors"    ] = true                        ;

        }
        
        return $retorno;
        
    }
    
    public function actionGridMensagens()
    {
        
        $dados = [];
        
        if(isset($_POST["idEstorno"]) && !empty($_POST["idEstorno"]) && $_POST["idEstorno"] !== "0")
        {
            
            $idEstorno = $_POST["idEstorno"];
            
            $estornoHasStatus = EstornoBaixaHasStatusEstorno::model()->findAll("habilitado AND EstornoBaixa_id = $idEstorno");
            
            foreach ($estornoHasStatus as $eHs)
            {
                
                $dateTime   = new DateTime($eHs->data_cadastro) ;
                $dataStatus = $dateTime->format("d/m/Y H:i:s")  ;
                
                $dados[] =  [
                                "dataStatus"        => $dataStatus                      ,
                                "descricaoStatus"   => $eHs->statusEstorno->descricao   ,
                                "usuario"           => $eHs->usuario->nome_utilizador   ,
                                "mensagem"          => $eHs->observacao
                            ];
                
            }
            
        }
        
        echo json_encode(["data" => $dados]);
        
    }
    
    public function actionVoltarEstorno()
    {
        
        $retorno =  [
                        "type"      => "success"                        ,
                        "msg"       => "Estorno reaberto com sucesso"   ,
                        "hasErrors" => false
                    ];
        
        if(isset($_POST["idEstorno"]))
        {
            
            $idEstorno = $_POST["idEstorno"];

            $transaction = Yii::app()->db->beginTransaction();

            $estorno = EstornoBaixa::model()->find("habilitado AND id = $idEstorno");
            
            if($estorno !== null)
            {
                             
                $sql    = "SELECT       IFNULL(MAX(EBhSE.id),0)         AS idEstorno        "
                        . "FROM         EstornoBaixa_has_StatusEstorno  AS EBhSE            "
                        . "WHERE EBhSE.habilitado AND EBhSE.EstornoBaixa_id = $estorno->id  ";

                $resultado = Yii::app()->db->createCommand($sql)->queryRow();
                
                try
                {
                    
                    $estornoHasStatus = EstornoBaixaHasStatusEstorno::model()->find("habilitado AND id = " . $resultado["idEstorno"]);
                    
                    if($estornoHasStatus !== null)
                    {
                        
                        $estornoHasStatus->habilitado = 0;
                        
                        if($estornoHasStatus->update())
                        {
                            
                            $baixa = Baixa::model()->findByPk($estorno->Baixa_id);
                            
                            if($baixa !== null)
                            {
                                $baixa->baixado = 1;
                                
                                if($baixa->update())
                                {
                                    
                                    $parcela = Parcela::model()->find("habilitado AND id = $baixa->Parcela_id");
                                    
                                    if($parcela !== null)
                                    {
                                        
                                        $parcela->valor_atual = $baixa->valor_pago;
                                        
                                        if($parcela->update())
                                        {
                                            $transaction->commit();
                                        }
                                        else
                                        {

                                            $transaction->rollBack();

                                            ob_start()                      ;
                                            var_dump($parcela->getErrors()) ;
                                            $resultado = ob_get_clean()     ;

                                            $retorno["type"         ] = "error"                                     ;
                                            $retorno["msg"          ] = "Erro reabrir estorno. Baixa: $resultado"   ;
                                            $retorno["hasErrors"    ] = true                                        ;
                                            
                                        }
                                        
                                    }
                                    else
                                    {

                                        $transaction->rollBack();

                                        $retorno["type"         ] = "error"                                             ;
                                        $retorno["msg"          ] = "Parcela não encontrada. ID: $baixa->Parcela_id"    ;
                                        $retorno["hasErrors"    ] = true                                                ;
                                        
                                    }
                                    
                                }
                                else
                                {

                                    $transaction->rollBack();

                                    ob_start()                      ;
                                    var_dump($baixa->getErrors())   ;
                                    $resultado = ob_get_clean()     ;

                                    $retorno["type"         ] = "error"                                     ;
                                    $retorno["msg"          ] = "Erro reabrir estorno. Baixa: $resultado"   ;
                                    $retorno["hasErrors"    ] = true                                        ;
                                    
                                }
                                
                                
                            }
                            else
                            {
                                
                                $retorno["msg"] .= "commitou";
                                
                                $transaction->commit();
                            }
                            
                        }
                        else
                        {

                            $transaction->rollBack();

                            ob_start()                                  ;
                            var_dump($estornoHasStatus->getErrors())    ;
                            $resultado = ob_get_clean()                 ;

                            $retorno["type"         ] = "error"                                                             ;
                            $retorno["msg"          ] = "Erro reabrir estorno. EstornoBaixaHasStatusEstorno: $resultado"    ;
                            $retorno["hasErrors"    ] = true                                                                ;
                            
                        }
                        
                    }
                    else
                    {

                        $retorno["type"         ] = "error"                                                         ;
                        $retorno["msg"          ] = "Status do Estorno não encontrado. ID: " .  $_POST["idEstorno"] ;
                        $retorno["hasErrors"    ] = true                                                            ;
                        
                    }
                    
                } 
                catch (Exception $ex) 
                {

                    $retorno["type"         ] = "error"                         ;
                    $retorno["msg"          ] = "Erro: " . $ex->getMessage()    ;
                    $retorno["hasErrors"    ] = true                            ;

                }
                
            }
            else
            {

                $retorno["type"         ] = "error"                     ;
                $retorno["msg"          ] = "Estorno não encontrado"    ;
                $retorno["hasErrors"    ] = true                        ;
                
            }
            
        }
        else
        {

            $retorno["type"         ] = "error"                         ;
            $retorno["msg"          ] = "ID do Estorno não encontrado"  ;
            $retorno["hasErrors"    ] = true                            ;
            
        }
        
        echo json_encode($retorno);
        
    }
}
