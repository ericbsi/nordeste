<?php

class ParceiroController extends Controller
{	
    public $layout = '//layouts/main_sigac_template';

    public function actionIndex()
    {
            $this->render('index');
    }

    public function actionRelatorioDeProducao()
    {
        $baseUrl        = Yii::app()->baseUrl;
        $cs             = Yii::app()->getClientScript();

        $producao           = array();
        $producaoEmprestimo = array();
        $dataDe             = NULL;
        $dataAte            = NULL;

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css'                              );
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css'                                 );
        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css'                                            );

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js'                                                          );
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js'                ,   CClientScript::POS_END  );
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js'                   ,   CClientScript::POS_END  );
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js'                 ,   CClientScript::POS_END  );
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js'                    ,   CClientScript::POS_END  );
        $cs->registerScriptFile($baseUrl . '/js/fn-parceiro-producao.js'                    ,   CClientScript::POS_END  );
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js'                      ,   CClientScript::POS_END  );
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js'                   ,   CClientScript::POS_END  );
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js'  ,   CClientScript::POS_END  );

        $cs->registerScript('datepicker',"$('.date-picker').datepicker({autoclose: true});");

        if( isset( $_POST['data_de'] ) && isset( $_POST['data_ate'] ) )
        {

            $relatorios = new Relatorios;

            $producao           = Empresa::model()->reportProducaoParceiro  (Yii::app()->session['usuario']->returnFilial()->id, $_POST['data_de'], $_POST['data_ate'] )    ;
            $producaoEmprestimo = $relatorios->getProducaoEmprestimo        (Yii::app()->session['usuario']->returnFilial()->id, $_POST['data_de'], $_POST['data_ate'] )    ;
            $dataDe             = $_POST['data_de'  ]                                                                                                                       ;
            $dataAte            = $_POST['data_ate' ]                                                                                                                       ;
        }

        $this->render   ('relatorioDeProducao', array  (
                                                            'producao'              =>  $producao           ,
                                                            'producaoEmprestimo'    =>  $producaoEmprestimo ,
                                                            'dataDe'                =>  $dataDe             ,
                                                            'dataAte'               =>  $dataAte
                                                        )
                        );
    }
}