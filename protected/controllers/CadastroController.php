<?php

class CadastroController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionUpdate(){
		$action 	= $_POST['controller_action'];
		$msgReturn 	= "";

		if( $action == 'add' )
		{
			//Cadastro::model()->novo($_POST['TelefoneCliente'],$_POST['Cliente_id']);
			//$msgReturn = "Telefone cadastrado com sucesso!";
			break;
		}
		else
		{
			Cadastro::model()->atualizar($_POST['Cadastro'], $_POST['cadastro_id'], $_POST['Estado_Civil']);
			$msgReturn = "Dados atualizados com sucesso!";		
		}

		echo json_encode(array(
			'msgReturn' => $msgReturn
		));
	}
	
	public function actionLoad(){

		$id 		= $_GET['entity_id'];

		echo json_encode( Cadastro::model()->loadFormEdit( $id ) );

	}
}