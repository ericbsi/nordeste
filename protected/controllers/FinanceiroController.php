<?php

class FinanceiroController extends Controller
{

    public $layout = '//layouts/main_sigac_template';

    public function filters()
    {
        return array(
            'accessControl',
            'postOnly + delete',
        );
    }

    public function accessRules()
    {

        return array(
            array('allow',
                'actions' => array(
                    'gerarNovaRemessa',
                    'getArquivosRemessa',
                    'add',
                    'index',
                    'cnabremessa',
                    'remessas',
                    'retorno',
                    'arquivosRemessas',
                    'importarRetorno',
                    'importarTudoBradesco',
                    'ImportarTudoSantander',
                    'contasAReceber',
                    'contasPagar',
                    'contasPagas',
                    'gridContasPagar',
                    'gridContasPagas',
                    'gridBorderos',
                    'gridBorderosPagos',
                    'gridLotes',
                    'gerarLotes',
                    'excluirLote',
                    'getTotalNucleo',
                    'gridLotesPagos',
                    'despesas',
                    'salvarDespesa',
                    'listarDespesas',
                    'getParcelasDespesa',
                    'baixarParcelaDespesa',
                    'excluirDespesa',
                    'mudarVencimentoParcela',
                    'gerarArquivoLecca',
                    'comunicaLecca',
                    'gerarLiquidacao'
                ),
                'users' => array('@'),
                'expression' => 'Yii::app()->session["usuario"]->role == "financeiro"       ||  '
                . 'Yii::app()->session["usuario"]->role == "analista_de_credito"       ||  '
                . 'Yii::app()->session["usuario"]->role == "empresa_admin"        '
            ),
            array('allow',
                'actions' => array(
                    'contasPagar',
                    'contasPagas',
                    'gridContasPagar',
                    'gridContasPagas',
                    'gridBorderos',
                    'gridBorderosPagos',
                    'gridLotes',
                    'getTotalNucleo',
                    'gridLotesPagos'
                ),
                'users' => array('@'),
                'expression' => 'Yii::app()->session["usuario"]->role == "financeiro"       ||  '
                . 'Yii::app()->session["usuario"]->role == "empresa_admin"    ||  '
                . 'Yii::app()->session["usuario"]->role == "parceiros_admin"      '
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionGerarLiquidacao(){
        $this->layout = '//layouts/main_sigac_template';

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile    ($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile    ($baseUrl . '/css/sigax_template/dataTables.tableTools.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/blockInterface.js',CClientScript::POS_END);
        $cs->registerScriptFile ($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile ($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/financeiro/fn-gerar-liquidacao.js', CClientScript::POS_END);
        $cs->registerScriptFile ($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);

        $this->render('gerar_liquidacao');
    }

    public function actionImportarTudoBradesco() {

        $totalBaixas = 0;
        $rangeNaoEncontrado = 0;
        $naoBaixados = 0;
        $qdtArquivosImportados = 0;
        $jaBaixados = 0;
        foreach (glob("/var/www/nordeste/importarBradesco/*.RET") as $file) {
            $arquivo = fopen($file, "r");
            while (!feof($arquivo)) {
                $arquivoRetorno = fgets($arquivo,4096);


            $arrBaixasConfig = array();
            $arrTitulos = array();
            $util = new Util;
            $campoCnabConfig = new CampoCnabConfig;

            //echo intval(substr($arquivoRetorno, 70, 11));

            $arrOcorrencias = array();

                $labelRangeGerado = intval(substr($arquivoRetorno, 70, 11));
                $rangerGerado = RangesGerados::model()->find('label = "' . $labelRangeGerado . '"');
                $ocorrencia = OcorrenciaRetornoCnab::model()->findByPk(intval(substr($arquivoRetorno, 108, 2)));

                if ($rangerGerado != NULL)
                {
                    if (Baixa::model()->find('Parcela_id = "' . $rangerGerado->Parcela_id . '"')) {
                        $jaBaixados++;
                    }

                    $tiposLiquidacoes = array(6, 15, 16, 17);
                    $motivoRejeicao = MotivoRejeicaoTitulo::model()->findByPk(intval(substr($arquivoRetorno, 301, 2)));
                    //echo intval(substr($arquivoRetorno, 301, 2));
                    $retornoParcelaCnab = new RetornoParcelaCnab;
                    $retornoParcelaCnab->Ocorrencia_retorno_cnab_id = $ocorrencia->id;
                    $retornoParcelaCnab->nosso_numero = intval(substr($arquivoRetorno, 70, 11));

                    if ($motivoRejeicao != NULL)
                    {
                        $retornoParcelaCnab->Motivo_rejeicao_titulo_id = $motivoRejeicao->id;
                    }

                    $retornoParcelaCnab->Range_gerado_id = $rangerGerado->id;
                    $retornoParcelaCnab->data_ocorrencia = '20' . substr(substr($arquivoRetorno, 110, 6), 4, 2) . '-' . substr(substr($arquivoRetorno, 110, 6), 2, 2) . '-' . substr(substr($arquivoRetorno, 110, 6), 0, 2);


                    //Houve liquidação do título
                    if (in_array(intval(substr($arquivoRetorno, 108, 2)), $tiposLiquidacoes) && $campoCnabConfig->cnabValorMonetarioToDouble(substr($arquivoRetorno, 253, 13)))
                    {
                        $totalBaixas++;
                        $retornoParcelaCnab->valor_pago = $campoCnabConfig->cnabValorMonetarioToDouble(substr($arquivoRetorno, 253, 13));
                        $retornoParcelaCnab->juros_de_mora = $campoCnabConfig->cnabValorMonetarioToDouble(substr($arquivoRetorno, 266, 13));

                        if ($rangerGerado->parcela->valor_atual < $rangerGerado->parcela->valor)
                        {

                            $rangerGerado->parcela->valor_atual = $campoCnabConfig->cnabValorMonetarioToDouble(substr($arquivoRetorno, 253, 13));

                            if ($rangerGerado->parcela->valor_atual > 0)
                            {
                                $rangerGerado->parcela->update();
                                //$arrReturn['liquidados'] += 1;

                                $baixa = new Baixa;
                                $baixa->data_da_ocorrencia = $retornoParcelaCnab->data_ocorrencia;
                                $baixa->codigo_banco_cobrador = substr($arquivoRetorno, 165, 3);
                                $baixa->agencia_cobradora = substr($arquivoRetorno, 168, 5);
                                $baixa->valor_pago = $retornoParcelaCnab->valor_pago;
                                $baixa->tarifa_custas = substr($arquivoRetorno, 175, 13);
                                $baixa->valor_abatimento = substr($arquivoRetorno, 227, 13);
                                $baixa->valor_desconto = substr($arquivoRetorno, 240, 13);
                                $baixa->valor_mora = substr($arquivoRetorno, 266, 13);
                                $baixa->baixado = 1;
                                $baixa->Parcela_id = $rangerGerado->Parcela_id;
                                $baixa->data_da_baixa = date('Y-m-d');
                                $baixa->Filial_id = $rangerGerado->parcela->titulo->proposta->analiseDeCredito->Filial_id;
                                $baixa->Cliente_id = $rangerGerado->parcela->titulo->proposta->analiseDeCredito->Cliente_id;
                                try {
                                     $baixa->save();
                                } catch (Exception $ex) {
                                    echo 'Baixa não salva';
                                }

                            }
                        }
                    } else
                    {
                        $naoBaixados++;
                        //$arrReturn['registrados'] += 1;
                    }

                    /*
                      else
                      {
                      $arrReturn['comErro']					+= 1;
                      }
                     */

                    $retornoParcelaCnab->data_importacao = date('Y-m-d');
                    $retornoParcelaCnab->save();
                } else
                {

                    //$arrReturn['naoEncontrados'] += 1;
                }


        }
            $qdtArquivosImportados++;
            fclose($arquivo);
        }
        echo '<br/> TOTAL DE BAIXAS: '. $totalBaixas . '<br/>';
        echo 'RANGES NÃO ENCONTRADOS: ' . ($rangeNaoEncontrado - 2) . '<br/>';
        echo 'ENCONTRADOS MAS NÃO BAIXADOS: ' . $naoBaixados . '<br/>';
        echo 'NUMERO DE ARQUIVOS IMPORTADOS: ' . $qdtArquivosImportados . '<br/>';
        echo 'JÁ BAIXADAS: ' . $jaBaixados;
    }

    public function  actionImportarTudoSantander() {
        $forcar_importacao = '1';
        foreach (glob("/var/www/nordeste/importarSantander/*.TXT") as $file) {
            $arquivo = fopen($file, "r");
            while (!feof($arquivo)) {
                $arquivoRetorno = fgets($arquivo,4096);

                if (substr($arquivoRetorno, 76, 3) === 033) {
                    $forcar_importacao = true;
                    if ($forcar_importacao == '1') {
                        $fi = true;
                    } else {
                        $fi = ArquivoCnab::validarImportacao(substr($arquivoRetorno, 94, 6), 7);
                    }

                    if ($fi) {
                        $geracaoTxt = substr($arquivoRetorno, 94, 6);

                        $arquivoCnab = new ArquivoCnab;
                        $arquivoCnab->relative_path = $arquivo['name'];
                        $arquivoCnab->absolute_path = NULL;
                        $arquivoCnab->tipo = 2;
                        $arquivoCnab->data_importacao = date('Y-m-d');
                        $arquivoCnab->data_geracao = '20' . substr($geracaoTxt, 4, 2) . '-' . substr($geracaoTxt, 2, 2) . '-' . substr($geracaoTxt, 0, 2);
                        $arquivoCnab->Usuario_id = Yii::app()->session['usuario']->id;
                        $arquivoCnab->Filial_id = 19;
                        $arquivoCnab->Banco_id = 7;

                        if ($arquivoCnab->save()) {
                            if ($forcar_importacao_obs !== '0') {
                                Dialogo::model()->novo('Importação de Cnab - Observação', 'ArquivoCnab', $arquivoCnab->id, Yii::app()->session['usuario']->id, $forcar_importacao_obs);
                            }
                        }

                        for ($i = 1; $i < count($arquivoRetorno) - 1; $i++) {
                            $labelRangeGerado = substr($arquivoRetorno, 62, 7);
                            $rangerGerado = RangesGerados::model()->find('label = "' . $labelRangeGerado . '"');
                            $dataOcorrencia = '20' . substr(substr($arquivoRetorno, 110, 6), 4, 2) . '-' . substr(substr($arquivoRetorno, 110, 6), 2, 2) . '-' . substr(substr($arquivoRetorno, 110, 6), 0, 2);
                            $ocorrencia = intval(substr($arquivoRetorno, 108, 2));

                            /* Verifica se o título existe no sistema */
                            if ($rangerGerado != NULL) {
                                /* Tipo possiveis de liquidação - Segundo as documentações */
                                $tiposLiquidacoes = array(6, 7, 8, 17);

                                /* OK, o titulo existe.. Então, vamos varres as possibilidades de ocorrência do retorno */

                                /* 2 - Tiulo teve seu registro confirmado */
                                if ($ocorrencia == 2) {
                                    RetornoParcelaCnab::model()->novo(NULL, $labelRangeGerado, NULL, NULL, 2, NULL, $dataOcorrencia, $rangerGerado->id, date('Y-m-d'));
                                    $arrReturn['registrados'] += 1;
                                }

                                /* Verifica se o título foi liquidado... */ else if (in_array($ocorrencia, $tiposLiquidacoes)) {
                                    $valor_pago = $campoCnabConfig->cnabValorMonetarioToDouble(substr($arquivoRetorno, 253, 13));
                                    $juros_de_mora = $campoCnabConfig->cnabValorMonetarioToDouble(substr($arquivoRetorno, 266, 13));

                                    $retornoParcelaCnab = RetornoParcelaCnab::model()->novo(3, $labelRangeGerado, $valor_pago, $juros_de_mora, $ocorrencia, NULL, $dataOcorrencia, $rangerGerado->id, date('Y-m-d'));

                                    /* Título pago, mas sem registro no sistema do banco */
                                    if ($ocorrencia == 38 || $ocorrencia == 39) {
                                        //$arrReturn['pagNaoReg'] += 1;
                                    } else {
                                        //$arrReturn['liquidados'] += 1;
                                    }

                                    /* Verifica se o título não está totalmente baixado (Não existe baixa parcial, apenas do valor integral) */
                                    if ($rangerGerado->parcela->valor_atual < $rangerGerado->parcela->valor) {
                                        $rangerGerado->parcela->valor_atual = $campoCnabConfig->cnabValorMonetarioToDouble(substr($arquivoRetorno, 253, 13));

                                        if ($rangerGerado->parcela->update()) {
                                            $baixaFilial_id = 0;
                                            $baixaCliente_id = 0;

                                            if ($rangerGerado->parcela->titulo->proposta != NULL) {
                                                $baixaFilial_id = $rangerGerado->parcela->titulo->proposta->analiseDeCredito->Filial_id;
                                                $baixaCliente_id = $rangerGerado->parcela->titulo->proposta->analiseDeCredito->Cliente_id;
                                            } else if ($rangerGerado->parcela->titulo->vendaW != NULL) {
                                                $baixaFilial_id = $rangerGerado->parcela->titulo->vendaW->Filial_id;
                                                $baixaCliente_id = $rangerGerado->parcela->titulo->vendaW->Cliente_id;
                                            } else {
                                                exit();
                                            }

                                            $this->novo(
                                                    $retornoParcelaCnab->data_ocorrencia, substr($arquivoRetorno, 165, 3), substr($arquivoRetorno, 168, 5), $retornoParcelaCnab->valor_pago, 0, 0, 0, substr($arquivoRetorno, 266, 13), 1, $rangerGerado->Parcela_id, date('Y-m-d'), $baixaFilial_id, $baixaCliente_id
                                            );
                                        }
                                    }
                                }

                                /* Outro tipo de ocorrência (Instrução de baixa - geralmente em cancelamentos, erros, etc) */ else {
                                    RetornoParcelaCnab::model()->novo(NULL, $labelRangeGerado, NULL, NULL, $ocorrencia, NULL, $dataOcorrencia, $rangerGerado->id, date('Y-m-d'));
                                }
                            }
                        }
                    } else {


                        //$arrReturn['msgs'][] = "O arquivo anterior não foi importado. Para prosseguir, force a importação.";
                    }
                } else {

                   // $arrReturn['msgs'][] = 'O arquivo enviado não corresponde ao banco selecionado.';
                }
            }
            echo "O arquivo terminou aqui";
            fclose($arquivo);

        }
    }

    public function novo($data_da_ocorrencia, $codigo_banco_cobrador, $agencia_cobradora, $valor_pago, $tarifa_custas, $valor_abatimento, $valor_desconto, $valor_mora, $baixado, $Parcela_id, $data_da_baixa, $baixaFilial_id, $baixaCliente_id) {
        $baixa = new Baixa;
        $baixa->data_da_ocorrencia = $data_da_ocorrencia;
        $baixa->codigo_banco_cobrador = $codigo_banco_cobrador;
        $baixa->agencia_cobradora = $agencia_cobradora;
        $baixa->valor_pago = $valor_pago;
        $baixa->tarifa_custas = $tarifa_custas;
        $baixa->valor_abatimento = $valor_abatimento;
        $baixa->valor_desconto = $valor_desconto;
        $baixa->valor_mora = $valor_mora;
        $baixa->baixado = $baixado;
        $baixa->Parcela_id = $Parcela_id;
        $baixa->data_da_baixa = $data_da_baixa;
        $baixa->Filial_id = $baixaFilial_id;
        $baixa->Cliente_id = $baixaCliente_id;
        $baixa->save();
    }


    public function actionComunicaLecca()
    {

        $servidor = "ftp.lecca.com.br";
        $porta = 821;

        $local_arquivo = '/var/www/nordeste/lecca/arquivo.txt';
        $ftp_pasta = '/credshow/';
        $ftp_arquivo = 'arquivo.txt';

        //        $ftp = Yii::app()->ftp;
        //        $ftp->put('teste.txt', '/var/www/nordeste/lecca/arquivo.txt');

        // Abre a conexão com o servidor FTP
        if($ftp = ftp_connect($servidor,$porta))
        {
            //            if(ftp_pasv($ftp, true))
            //            {

            //                if(ftp_set_option($ftp, USEPASVADDRESS, true))
            //                {
            //                    if(ftp_chdir($ftp, '/credshow'))
            //                    {

                        if(ftp_login($ftp, "credshow" , "credshow@123"))
                        {
                            if(ftp_put($ftp, "teste.txt", $local_arquivo, FTP_BINARY))
                            //if( move_uploaded_file($local_arquivo, "ftp://ftp.lecca.com.br:821/credshow/"))
                            {
                                echo "O arquivo foi enviado";
                                ftp_close($ftp);
                            }
                            else
                            {
                                echo "O arquivo deu erro";
                            }

                            /*
                            if(ftp_mkdir($ftp, "credshow"))
                            {
                                echo "deu certo";
                            }
                            else
                            {
                                echo "não deu certo";
                            }*/

                        }
                        else
                        {
                            echo "não logou.";
                        }

                        //                    }
                        //                    else
                        //                    {
                        //                        echo "não acessou a pasta";
                        //                    }
                        //                }
                        //                else
                        //                {
                        //                    echo "deu erro no set_option";
                        //                }
                        //            }
                        //            else
                        //            {
                        //                echo "deu tilt no pasv";
                        //            }

                        /*            if($envio = ftp_put($ftp, $ftp_pasta.$ftp_arquivo, $local_arquivo, FTP_ASCII))
                                    {
                                        echo "O arquivo foi enviado";
                                    }
                                    else
                                    {
                                        echo "O arquivo deu erro";
                                    }*/

        }
        else
        {
            echo "nem deu certo";
        }

        /*        // Faz o login no servidor FTP
          $login = ftp_login($ftp, $usuario, $senha); // Retorno: true ou false

          // Define variáveis para o envio de arquivo
          $local_arquivo = './arquivos/documento.doc'; // Localização (local)
          $ftp_pasta = '/public_html/arquivos/'; // Pasta (externa)
          $ftp_arquivo = 'documento.doc'; // Nome do arquivo (externo)

          // Envia o arquivo pelo FTP em modo ASCII
          $envio = ftp_put($ftp, $ftp_pasta . $ftp_arquivo, $local_arquivo, FTP_ASCII); */
    }

    public function actionGerarArquivoLecca()
    {
        //$proposta_id                  = $_POST['proposta_id'];
        //$proposta = Proposta::model()->findByPk(91314);

        $content = "";

        $propostas = [
            98571,
            98552,
            98547,
            98543,
            98513,
            98511,
            98510,
            98506,
            98505,
            98487,
            98455,
            97679,
            97666,
            97516,
            97501,
            97462,
            97385,
            97367,
            97307,
            97229,
            97219,
            97176,
            97145,
            97095,
            97023,
            96930,
            96927,
            96876,
            96835,
            96782,
            96742,
            96721,
            96686,
            96673,
            96636,
            96621,
            96567,
            96563,
            96552,
            96547,
            96528,
            96512,
            96506,
            96466,
            96460,
            96445,
            96420,
            95960
            ];
        function tirarAcentos($string)
        {
            return preg_replace(array("/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/", "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/", "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/", "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/", "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/", "/(ñ)/", "/(Ñ)/", "/(Ç)/", "/(ç)/"), explode(" ", "a A e E i I o O u U n N C c"), $string);
        }

        $valorTotal = 0;
        $valorRepasseTotal = 0;

        foreach ($propostas as $p)
        {

            $proposta = Proposta::model()->find("habilitado AND id = $p");

            if($proposta !==null)
            {

                if($proposta->analiseDeCredito->filial->NucleoFiliais_id == 40)
                {
                    $codigoRede         = "96020"   ;
                    $digitoCodigoRede   = "0"       ;
                }
                else if($proposta->analiseDeCredito->filial->NucleoFiliais_id == 99)
                {
                    $codigoRede         = "96022"   ;
                    $digitoCodigoRede   = "0"       ;
                }
                else if($proposta->analiseDeCredito->filial->NucleoFiliais_id == 59)
                {
                    $codigoRede         = "96024"   ;
                    $digitoCodigoRede   = "0"       ;
                }
                else if($proposta->analiseDeCredito->filial->NucleoFiliais_id == 75)
                {
                    $codigoRede         = "96023"   ;
                    $digitoCodigoRede   = "0"       ;
                }
                else if($proposta->analiseDeCredito->filial->NucleoFiliais_id == 63)
                {
                    $codigoRede         = "96025"   ;
                    $digitoCodigoRede   = "0"       ;
                }
                else if($proposta->analiseDeCredito->filial->NucleoFiliais_id == 41)
                {
                    $codigoRede         = "96026"   ;
                    $digitoCodigoRede   = "0"       ;
                }

                //                $valorCompra    = number_format(    (   $this->valor                    - $this->valor_entrada) , 2, '', '');
                $valorEntrada   = number_format(        $proposta->valor_entrada        , 2, '', '')    ;
                $valorParcela   = number_format(        $proposta->valor_parcela        , 2, '', '')    ;
                $nomeCliente    = $proposta->analiseDeCredito->cliente->pessoa->nome                    ;

                $valorIOF = 0;

                $valor = 0;

                if($proposta->tabelaCotacao->ModalidadeId == 2)
                {

                    $valor          = $proposta->qtd_parcelas * $proposta->valor_parcela;

                    $valorTotal    += $valor;

                    $valorCompra    = number_format(($valor ) , 2, '', '');

                    $taxa = str_pad("0", 10, "0", STR_PAD_LEFT);

                    $fator = $proposta->getFator();

                    $valorRepasse   = $valor - ($valor* ($fator->porcentagem_retencao/100));

                    if($fator !== null)
                    {
                        $valorIOF = ($valorRepasse * $fator->fatorIOF);
                    }

                    if($proposta->analiseDeCredito->filial->NucleoFiliais_id == 40)
                    {
                        $codTabela = "00000602";
                    }
                    else if(in_array($proposta->analiseDeCredito->filial->NucleoFiliais_id, [59,99]))
                    {

                        if($proposta->carencia == 15)
                        {
                            $codTabela = "00000607";
                        }
                        else if($proposta->carencia == 30)
                        {
                            $codTabela = "00000608";
                        }
                        else if($proposta->carencia == 45)
                        {
                            $codTabela = "00000609";
                        }

                    }

                }
                else
                {

                    $valor          = $proposta->valor - $proposta->valor_entrada;
                    $valorRepasse = $valor;

                    $valorTotal    += $valor;

                    $valorCompra    = number_format(($valor) , 2, '', '');

                    $taxa = str_pad(number_format($proposta->tabelaCotacao->taxa,4,'',''), 10, "0", STR_PAD_LEFT);

                    $fator = $proposta->getFator();

                    if($fator !== null)
                    {
                        $valorIOF = (($proposta->valor - $proposta->valor_entrada) * $fator->fatorIOF);
                    }

                    if($proposta->analiseDeCredito->filial->NucleoFiliais_id == 40)
                    {
                        $codTabela = "00000601";
                    }
                    else if($proposta->analiseDeCredito->filial->NucleoFiliais_id == 75)
                    {
                        $codTabela = "00000610";
                    }
                    else if($proposta->analiseDeCredito->filial->NucleoFiliais_id == 41)
                    {
                        $codTabela = "00000611";
                    }
                    else if($proposta->analiseDeCredito->filial->NucleoFiliais_id == 63)
                    {
                        $codTabela = "00000612";
                    }

                }

                $valorRepasseTotal += $valorRepasse;

                $valorIOFstr = str_pad(number_format($valorIOF,2,'',''), 15, "0", STR_PAD_LEFT);

                $cadastro = Cadastro::model()->find('habilitado AND Cliente_id = ' . $proposta->analiseDeCredito->cliente->id);
                if ($cadastro != null)
                {
                    if (isset($cadastro->nome_do_pai))
                    {
                        $nomePai = str_pad($cadastro->nome_do_pai, 35, " ", STR_PAD_RIGHT);
                    } else
                    {
                        $nomePai = str_repeat(" ", 35);
                    }
                    if (isset($cadastro->nome_da_mae))
                    {
                        $nomeMae = str_pad($cadastro->nome_da_mae, 35, " ", STR_PAD_RIGHT);
                    } else
                    {
                        $nomeMae = str_repeat(" ", 35);
                    }
                } else
                {
                    $nomeMae = str_repeat(" ", 35);
                    $nomePai = str_repeat(" ", 35);
                }

                $dadosProfissionais = DadosProfissionais::model()->find('habilitado AND Pessoa_id = ' . $proposta->analiseDeCredito->cliente->pessoa->id . ' AND principal');
                if ($dadosProfissionais != null)
                {
                    if (isset($dadosProfissionais->profissao))
                    {
                        $profissao = str_pad($profissao = $dadosProfissionais->profissao, 35, " ", STR_PAD_RIGHT);
                    } else
                    {
                        $profissao = str_pad("NÃO INFORMADO", 35, " ", STR_PAD_RIGHT);
                    }
                    if (isset($dadosProfissionais->renda_liquida))
                    {
                        $salario = number_format($dadosProfissionais->renda_liquida, 2, "", "");
                    } else
                    {
                        $salario = number_format(0, 2, "", "");
                    }
                    if (isset($dadosProfissionais->empresa))
                    {
                        $razaoSocial = $dadosProfissionais->empresa;
                    } else
                    {
                        $razaoSocial = "NÃO INFORMADO";
                    }
                } else
                {
                    $profissao = str_pad("NÃO INFORMADO", 35, " ", STR_PAD_RIGHT);
                    $salario = number_format(0, 2, " ", " ");
                    $razaoSocial = "NÃO INFORMADO";
                }

                $endereco = $proposta->analiseDeCredito->cliente->pessoa->getEndereco();
                $enderecoCEP = trim($endereco->cep);

                $nomeCliente = strtoupper(strtr(utf8_decode($nomeCliente), utf8_decode("áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ"), "aaaaeeiooouucAAAAEEIOOOUUC"));

                $cht = ContatoHasTelefone::model()->find('habilitado AND Contato_id = ' . $proposta->analiseDeCredito->cliente->pessoa->Contato_id);
                if ($cht != null)
                {
                    $telefone = Telefone::model()->find('habilitado AND id = ' . $cht->Telefone_id)->numero;
                    $tddd = Telefone::model()->find('habilitado AND id = ' . $cht->Telefone_id)->discagem_direta_distancia;
                } else
                {
                    $telefone = "0";
                    $tddd = "0";
                }

                $nucleo = $proposta->analiseDeCredito->filial->nucleoFilial;
                $nHDB = NucleoFiliaisHasDadosBancarios::model()->find('NucleoFiliais_id = ' . $nucleo->id);
                if ($nHDB != null)
                {
                    $dadosBancarios = DadosBancarios::model()->find('habilitado AND id = ' . $nHDB->Dados_Bancarios_id);

                    $nome_agencia = $dadosBancarios->banco->nome;

                    $posicaoA = strpos($dadosBancarios->agencia, '-');

                    if ($posicaoA != 0 || $posicaoA === true)
                    {
                        $agencia = substr($dadosBancarios->agencia, 0, $posicaoA);
                        $digitoAgencia = substr($dadosBancarios->agencia, -($posicaoA - 3));
                    } else
                    {
                        $agencia = $dadosBancarios->agencia;
                        $digitoAgencia = "-";
                    }

                    $posicaoB = strpos($dadosBancarios->numero, '-');
                    if ($posicaoB != 0 || $posicaoB === true)
                    {
                        $conta = substr($dadosBancarios->numero, 0, $posicaoB);
                        $digitoConta = substr($dadosBancarios->numero, -($posicaoB - 2));
                    } else
                    {
                        $conta = $dadosBancarios->numero;
                        $digitoConta = "-";
                    }

                    if ($dadosBancarios->Tipo_Conta_Bancaria_id == 1)
                    {
                        $flagConta = '001';
                    } else
                    {
                        $flagConta = '002';
                    }
                } else
                {
                    $dadosBancarios = null;
                    $nome_agencia = "-";
                    $digitoAgencia = '-';
                    $digitoConta = '-';
                    $conta = "-";
                    $agencia = '-';
                    $flagConta = '-';
                }

                $cnpj = $proposta->analiseDeCredito->filial->cnpj;

                $cnpj = str_replace('.', '', $cnpj);
                $cnpj = str_replace('-', '', $cnpj);
                $cnpj = str_replace('/', '', $cnpj);

                $msg = Mensagem::model()->find('Dialogo_id = ' . $proposta->getDialogo()->id);

                $content .= "CDCS"                                                                                                              ; //codigo do produto (4)                   <--    1 :    4-->
                $content .= date('Ymd', strtotime($proposta->data_cadastro))                                                                    ; //data da operacao (8)                    <--    5 :   12-->
                $content .= "0005"                                                                                                              ; //codigo da regional (4)                  <--   13 :   16-->
                //              $content .= "96020"                                                                                                             ; //codigo da rede (5)                      <--   17 :   21-->
                //              $content .= "0"                                                                                                                 ; //digito da rede (1)                      <--   22 :   22-->
                $content .= $codigoRede                                                                                                         ; //codigo da rede (5)                      <--   17 :   21-->
                $content .= $digitoCodigoRede                                                                                                   ; //digito da rede (1)                      <--   22 :   22-->
                $content .= "00001"                                                                                                             ; //codigo da loja (5)                      <--   23 :   27-->
                $content .= "0"                                                                                                                 ; //digito da loja (1)                      <--   28 :   28-->
                $content .= str_pad("CREDSHOW S/A", 35, " ", STR_PAD_RIGHT)                                                                     ; //nome da loja (35)                       <--   29 :   63-->
                $content .= str_pad($proposta->id, 12, "0", STR_PAD_LEFT)                                                                       ; //numero do contrato/operacao (12)        <--   64 :   75-->
                $content .= $codTabela                                                                                                          ; //codigo da tabela de juros (8)           <--   76 :   83-->
                $content .= $taxa                                                                                                               ; //"0000074200"; //taxa de juros (10)      <--   84 :   93-->
                $content .= date('Ymd', strtotime("+" . $proposta->carencia . "days", strtotime($proposta->data_cadastro)))                     ; //data primeira parcela (8)               <--   94 :  101-->
                $content .= str_pad($valorCompra, 15, "0", STR_PAD_LEFT)                                                                        ; //valor da compra (15)                    <--  102 :  116-->
                $content .= str_pad($valorEntrada, 15, "0", STR_PAD_LEFT)                                                                       ; //valor da entrada (15)                   <--  117 :  131-->
                $content .= str_pad($valorCompra, 15, "0", STR_PAD_LEFT)                                                                        ; //valor do principal (15)                 <--  132 :  146-->
                $content .= "000000000000000"                                                                                                   ; //valor CAC (15)                          <--  147 :  161-->
                $content .= str_pad($proposta->qtd_parcelas, 3, "0", STR_PAD_LEFT)                                                              ; //quantidade de parcelas (3)              <--  162 :  164-->
                $content .= str_pad($valorParcela, 15, "0", STR_PAD_LEFT)                                                                       ; //valor da parcela (15)                   <--  165 :  179-->
                $content .= $valorIOFstr                                                                                                        ; //valor IOC (15)                          <--  180 :  194-->
                $content .= "D"                                                                                                                 ; //forma liberacao (1)                     <--  195 :  195-->
                $content .= "I"                                                                                                                 ; //forma liquidacao (1)                    <--  196 :  196-->
                $content .= str_pad(substr($digitoAgencia, 0, 1), 1, " ", STR_PAD_RIGHT)                                                        ; //digito da agencia (1)                   <--  197 :  197-->
                $content .= str_pad(substr($digitoConta, 0, 2), 2, "0", STR_PAD_LEFT)                                                           ; //digito da conta (2)                     <--  198 :  199-->
                $content .= str_pad(substr(tirarAcentos($nomeCliente), 0, 35), 35, " ", STR_PAD_RIGHT)                                          ; //nome do favorecido (35)                 <--  200 :  234-->
                $content .= "001"                                                                                                               ; //praca do DOC (3)                        <--  235 :  237-->
                $content .= $dadosBancarios->banco->codigo                                                                                      ; //banco para pagamento do DOC (3)         <--  238 :  240-->
                $content .= str_pad($agencia, 4, "0", STR_PAD_LEFT)                                                                             ; //agencia para pagamento do DOC (4)       <--  241 :  244-->
                $content .= str_pad($conta, 8, "0", STR_PAD_LEFT)                                                                               ; //conta para pagamento do DOC (8)         <--  245 :  252-->
                $content .= $cnpj                                                                                                               ; //CGC/CPF do DOC (Cliente) (14)           <--  253 :  266-->
                $content .= "N"                                                                                                                 ; //TAC Financiada (1)                      <--  267 :  267-->
                $content .= str_pad($valorCompra, 15, "0", STR_PAD_LEFT)                                                                        ; //valor do DOC (15)                       <--  268 :  282-->
                $content .= str_pad(substr(tirarAcentos($nomeCliente), 0, 60), 60, " ", STR_PAD_RIGHT)                                          ; //nome do cliente (60)                    <--  283 :  342-->
                $content .= "00001"                                                                                                             ; //pais/nacionalidade (5)                  <--  343 :  347-->
                $content .= "4"                                                                                                                 ; //estado civil (1)                        <--  348 :  348-->
                $content .= $proposta->analiseDeCredito->cliente->pessoa->sexo                                                                  ; //sexo (1)                                <--  349 :  349-->
                $content .= str_pad($proposta->analiseDeCredito->cliente->pessoa->getRG()->numero, 16, " ", STR_PAD_RIGHT)                      ; //numero do rg (16)                       <--  350 :  365-->
                $content .= $proposta->analiseDeCredito->cliente->pessoa->getRG()->uf_emissor                                                   ; //UF do RG (2)                            <--  366 :  367-->
                $content .= $proposta->analiseDeCredito->cliente->pessoa->getRG()->uf_emissor                                                   ; //UF da Naturalidade (2)                  <--  368 :  369-->
                $content .= str_pad("BRASILEIRA", 35, " ", STR_PAD_RIGHT)                                                                       ; //Naturalidade (35)                       <--  370 :  404-->
                $content .= str_pad(substr(tirarAcentos($nomePai), 0, 35), 35, STR_PAD_LEFT)                                                    ; //nome do pai (35)                        <--  405 :  439-->
                $content .= str_pad(substr(tirarAcentos($nomeMae), 0, 35), 35, STR_PAD_LEFT)                                                    ; //nome da mae (35)                        <--  440 :  474-->
                $content .= str_pad(substr($proposta->analiseDeCredito->cliente->pessoa->getRG()->orgao_emissor, 0, 5), 5, " ", STR_PAD_RIGHT)  ; //orgao emissor do RG (5)                 <--  475 :  479-->
                $content .= $proposta->analiseDeCredito->cliente->pessoa->getRG()->uf_emissor                                                   ; //sigla UF outros documentos (2)          <--  480 :  481-->
                $content .= substr(tirarAcentos($profissao), 0, 35)                                                                             ; //descricao da profissao (35)             <--  482 :  516-->
                $content .= substr(tirarAcentos($profissao), 0, 35)                                                                             ; //descricao do cargo (35)                 <--  517 :  551-->
                $content .= $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero                                                      ; //cpf do cliente (11)                     <--  552 :  562-->
                $content .= date('Ymd', strtotime($proposta->analiseDeCredito->cliente->pessoa->nascimento))                                    ; //data de nascimento do cliente (8)       <--  563 :  570-->
                $content .= "00000001"                                                                                                          ; //codigo da profissao (8)                 <--  571 :  578-->
                $content .= str_pad($salario, 12, "0", STR_PAD_LEFT)                                                                            ; //valor do salario liquido (12)           <--  579 :  590-->
                $content .= date('Ymd', strtotime($proposta->analiseDeCredito->cliente->pessoa->getRG()->data_emissao))                         ; //data de expedicao do RG (8)             <--  591 :  598-->
                $content .= str_pad(substr(tirarAcentos(utf8_decode(preg_replace('/\s/',' ', $msg->conteudo))),0,3000), 3000, " ", STR_PAD_RIGHT)  ; //observacao (3000!)                      <--  599 : 3598-->
                $content .= "1"                                                                                                                 ; //tipo de residencia (1)                  <-- 3599 : 3600-->
                $content .= str_pad(substr(tirarAcentos(($endereco->logradouro)), 0, 60), 60, " ", STR_PAD_RIGHT)                               ; //descricao do logradouro (60)            <-- 3601 : 3660-->
                $content .= str_pad($endereco->numero, 5, "0", STR_PAD_LEFT)                                                                    ; //numero do logradouro (5)                <-- 3661 : 3665-->
                $content .= str_pad(substr(tirarAcentos(($endereco->complemento)), 0, 30), 30, " ", STR_PAD_RIGHT)                              ; //complemento do logradouro (30)          <-- 3666 : 3695-->
                $content .= $endereco->uf                                                                                                       ; //UF do endereco (2)                      <-- 3696 : 3697-->
                $content .= str_pad(substr(tirarAcentos(($endereco->bairro)), 0, 30), 30, " ", STR_PAD_RIGHT)                                   ; //bairro (30)282                          <-- 3698 : 3727-->
                $content .= str_pad(substr(tirarAcentos(($endereco->cidade)), 0, 40), 40, " ", STR_PAD_RIGHT)                                   ; //cidade (40)                             <-- 3728 : 3767-->
                $content .= "00001"                                                                                                             ; //pais (5)                                <-- 3768 : 3772-->
                $content .= $enderecoCEP                                                                                                        ; //CEP (8)                                 <-- 3773 : 3780-->
                $content .= "0000"                                                                                                              ; //area do telefone (4)                    <-- 3781 : 3784-->
                $content .= "000000000000"                                                                                                      ; //numero do telefone (12)                 <-- 3785 : 3796-->
                $content .= str_pad(substr($telefone,0,12), 12, "0", STR_PAD_LEFT)                                                              ; //"000000000000" numero do celular (12)   <-- 3797 : 4008-->
                $content .= str_pad("SEMEMAIL", 50, " ", STR_PAD_LEFT)                                                                          ; //email (50)                              <-- 4009 : 4058-->
                $content .= str_pad($tddd, 4, "0", STR_PAD_LEFT)                                                                                ; //area do celular (4)                     <-- 4059 : 4062-->
                $content .= str_pad(substr(tirarAcentos($razaoSocial), 0, 30), 30, " ", STR_PAD_RIGHT); //"BN 1566947577                 ";           //razao social (30)
                $content .= "20130201";                                 //data de admissao (8)
                $content .= "00000000000000";                           //CGC da Empresa (14)
                $content .= str_pad(substr(tirarAcentos(($dadosProfissionais->endereco->logradouro)), 0, 60), 60, " ", STR_PAD_RIGHT); //"MARIA LUIZA MIRANDA RONKOSKI                                "; //logradouro (60)
                $content .= str_pad($dadosProfissionais->endereco->numero, 5, " ", STR_PAD_RIGHT); //"00000";                                    //numero do logradouro (5)
                $content .= str_pad(substr(tirarAcentos(($dadosProfissionais->endereco->complemento)), 0, 30), 30, " ", STR_PAD_RIGHT); //"                   complemento";           //complemento (30)
                $content .= str_pad(substr(tirarAcentos(($dadosProfissionais->endereco->bairro)), 0, 30), 30, " ", STR_PAD_RIGHT); //"RIO PEQUENO                   ";           //bairro (30)
                $content .= str_pad(substr(tirarAcentos(($dadosProfissionais->endereco->cidade)), 0, 40), 40, " ", STR_PAD_RIGHT); //"SAO JOSE DOS PI                         "; //cidade (40)
                $content .= str_pad($dadosProfissionais->endereco->uf, 2, " ", STR_PAD_RIGHT); //"RN";                                       //RN (2)
                $content .= str_pad($salario, 12, "0", STR_PAD_LEFT); //"000000088000";                             //valor da renda (12)
                $content .= str_pad(substr(str_replace("-", "", $dadosProfissionais->endereco->cep), 0, 8), 8, "0", STR_PAD_LEFT); //"08308526";                                 //CEP (8)
                $content .= substr($digitoAgencia, 0, 1);                                        //digito da agencia (1)
                $content .= str_pad(substr($digitoConta, 0, 2), 2, "0", STR_PAD_LEFT);//digito da conta (2)
                $content .= str_pad(substr(tirarAcentos($nome_agencia), 0, 20), 20, " ", STR_PAD_RIGHT);                     //nome da agencia (20)
                $content .= "1";                                        //FLAG titularidade (1)
                $content .= str_pad(substr(tirarAcentos($nucleo->nome), 0, 35), 35, " ", STR_PAD_RIGHT);//nome do favorecido (35)
                $content .= "000000000000";                             //numero do telefone (12)
                $content .= "001";                                      //praca (3)
                $content .= $dadosBancarios->banco->codigo;                                      //banco (3)
                $content .= str_pad($agencia, 4, "0", STR_PAD_LEFT);                                     //agencia (4)
                $content .= str_pad($conta, 8, "0", STR_PAD_LEFT);                                 //conta (8)
                $content .= str_pad($flagConta, 3, "0", STR_PAD_RIGHT);                                      //tipo da conta (3)
                $content .= "001";                                      //tipo de documento cpf (3)
                $content .= date('Ymd', strtotime("+" . $proposta->carencia . "days", strtotime($proposta->data_cadastro)));                                 //data de abertura (8)
                $content .= "0";                                        //codigo de natureza da residencia (1)

                $content .= chr(13) . chr(10);

            }

        }

        echo 'Valor total: R$ ' . number_format(($valorTotal) , 2, ',', '.') . chr(13) . chr(10) . 'Valor total repasse: R$ ' . number_format(($valorRepasseTotal) , 2, ',', '.');

        $arquivo = fopen("lecca/arquivo" . date("YmdHis") . ".txt", 'w+');
        fwrite($arquivo, $content);
        fclose($arquivo);

    }

    public function actionGerarNovaRemessa()
    {
        echo json_encode(CampoCnabConfig::model()->gerarRemessa($_POST['Banco'], $_POST['gerar_data_de'], $_POST['gerar_data_ate'], $_POST['gerar_nome_do_arquivo']));
    }

    public function actionGetArquivosRemessa()
    {
        echo json_encode(ArquivoCnab::model()->getArquivosRemessa($_POST['data_de'], $_POST['data_ate'], $_POST['banco'], 1, $_POST['draw'], $_POST['start']));
    }

    public function actionContasAReceber()
    {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');

        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/DT_bootstrap.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/table-data-contas-a-receber.js', CClientScript::POS_END);

        /* $cs->registerScriptFile($baseUrl.'/js/jquery.flot.js',  CClientScript::POS_END);
          $cs->registerScriptFile($baseUrl.'/js/jquery.flot.resize.js',  CClientScript::POS_END);
          $cs->registerScriptFile($baseUrl.'/js/jquery.flot.categories.js',  CClientScript::POS_END);
          $cs->registerScriptFile($baseUrl.'/js/jquery.flot.pie.js',  CClientScript::POS_END);
          $cs->registerScriptFile($baseUrl.'/js/charts.js',  CClientScript::POS_END); */

        $cs->registerScript('TableData', 'TableData.init();');
        //$cs->registerScript('Charts','Charts.init();');

        $this->render('contas-a-receber', array('output' => Empresa::model()->contasAReceber()));
    }

    public function actionContasPagar()
    {
        $this->layout = '//layouts/main_sigac_template';

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css');
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpalette.css');
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/financeiro/contasPagar/fn-gridContasPagar.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpalette.js', CClientScript::POS_END);

        Bordero::model()->ajustarTitulosPagar();

        $this->render('contasPagar');
    }

    public function actionDespesas()
    {
        $this->layout = '//layouts/main_sigac_template';

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css');
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpalette.css');
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/financeiro/despesa/fn-gridDespesa.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpalette.js', CClientScript::POS_END);

        $this->render('despesa');
    }

    public function actionContasPagas()
    {
        $this->layout = '//layouts/main_sigac_template';

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css');
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpalette.css');
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/financeiro/contasPagas/fn-gridContasPagas.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpalette.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);

        //        Bordero::model()->ajustarTitulosPagar();

        $this->render('contasPagas');
    }

    public function actionCheckRemessaJaGerada()
    {

        $data = date('dmy');
        return ArquivoCnab::model()->checkRemessaJaGerada($data);
    }

    public function actionRemessas()
    {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-arquivos-remessa.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js', CClientScript::POS_END);

        $cs->registerScript('datepicker', "$('.date-picker').datepicker({autoclose: true});");

        $this->render('remessas');
    }

    public function actionArquivosRemessas()
    {
        sleep(1);

        $aColumns = array('id', 'relative_path');
        $sIndexColumn = "id";

        $gaSql['user'] = "root";
        $gaSql['password'] = "admin";
        $gaSql['db'] = "sigac";
        $gaSql['server'] = "localhost";

        /*
         * MySQL connection
         */

        $gaSql['link'] = mysqli_connect($gaSql['server'], $gaSql['user'], $gaSql['password']) or
                die('Could not open connection to server');

        mysqli_select_db($gaSql['link'], $gaSql['db']) or
                die('Could not select database ' . $gaSql['db']);


        /*
         * Paging
         */
        $sLimit = "";

        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1')
        {
            $sLimit = "LIMIT " . mysqli_real_escape_string($gaSql['link'], $_GET['iDisplayStart']) . ", " .
                    mysqli_real_escape_string($gaSql['link'], $_GET['iDisplayLength']);
        }

        /*
         * Ordering
         */
        if (isset($_GET['iSortCol_0']))
        {
            $sOrder = "ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++)
            {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true")
                {
                    $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . "
                        " . mysqli_real_escape_string($gaSql['link'], $_GET['sSortDir_' . $i]) . ", ";
                }
            }

            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == "ORDER BY")
            {
                $sOrder = "";
            }
        }


        $sWhere = "";

        if ($_GET['sSearch'] != "")
        {

            $sWhere = "WHERE (";
            for ($i = 0; $i < count($aColumns); $i++)
            {
                $sWhere .= $aColumns[$i] . " LIKE '%" . mysqli_real_escape_string($gaSql['link'], $_GET['sSearch']) . "%' OR ";
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ')';
        }


        /* Individual column filtering */
        for ($i = 0; $i < count($aColumns); $i++)
        {

            if ($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '')
            {
                if ($sWhere == "")
                {
                    $sWhere = "WHERE ";
                } else
                {
                    $sWhere .= " AND ";
                }
                $sWhere .= $aColumns[$i] . " LIKE '%" . mysqli_real_escape_string($gaSql['link'], $_GET['sSearch_' . $i]) . "%' ";
            }
        }

        $sTable = "Arquivo_cnab";

        /*
         * SQL queries
         * Get data to display
         */

        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS " . str_replace(" , ", " ", implode(", ", $aColumns)) . "
            FROM  $sTable
            $sWhere
            $sOrder
            $sLimit
        ";

        $rResult = mysqli_query($gaSql['link'], $sQuery) or die(mysqli_error());

        /* Data set length after filtering */
        $sQuery = "
            SELECT FOUND_ROWS()
        ";

        $rResultFilterTotal = mysqli_query($gaSql['link'], $sQuery) or die(mysqli_error());
        $aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
        $iFilteredTotal = $aResultFilterTotal[0];

        /* Total data set length */
        $sQuery = "
            SELECT COUNT(" . $sIndexColumn . ")
            FROM   $sTable
        ";

        $rResultTotal = mysqli_query($gaSql['link'], $sQuery) or die(mysqli_error());
        $aResultTotal = mysqli_fetch_array($rResultTotal);
        $iTotal = $aResultTotal[0];

        /*
         * Output
         */
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );



        while ($aRow = mysqli_fetch_array($rResult))
        {
            $row = array();
            for ($i = 0; $i < count($aColumns); $i++)
            {
                if ($aColumns[$i] == "version")
                {
                    /* Special output formatting for 'version' column */
                    $row[] = ($aRow[$aColumns[$i]] == "0") ? '-' : $aRow[$aColumns[$i]];
                } else if ($aColumns[$i] != ' ')
                {
                    /* General output */
                    $row[] = $aRow[$aColumns[$i]];
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode($output);
    }

    public function actionCnabRemessa()
    {

        $baseUrl = Yii::app()->baseUrl;

        $cs = Yii::app()->getClientScript();

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/DT_bootstrap.js', CClientScript::POS_END);
        //$cs->registerScriptFile($baseUrl.'/js/table-data.js',  CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-view-remessas.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);

        //$cs->registerScript('TableData','TableData.init();');

        $this->render('cnabremessa');
    }

    public function actionCnabRetorno()
    {

        $retorno = NULL;

        $baseUrl = Yii::app()->baseUrl;

        $cs = Yii::app()->getClientScript();

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-fileupload.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/analise-upload-file-manager.js', CClientScript::POS_END);
        $cs->registerCssFile($baseUrl . '/css/sigax_template/bootstrap-fileupload.min.css');

        if (isset($_FILES['arquivoRem']))
        {

            $_UPCONFIG['dir'] = '/var/www/sigac/retornos_import/';

            $retornoFile = $_FILES['arquivoRem'];

            $_UPCONFIG['finalFile'] = $_UPCONFIG['dir'] . $retornoFile['name'];

            if (move_uploaded_file($retornoFile['tmp_name'], $_UPCONFIG['finalFile']))
            {

                $oldUmask = umask(0);
                chmod($_UPCONFIG['finalFile'], 0755);
                umask($oldUmask);
                $retorno = Baixa::model()->registrarBaixa($_UPCONFIG['finalFile'], 1, $retornoFile['name']);
            }
        }

        $this->render('cnabretorno', array('retorno' => $retorno));
    }

    public function actionImportarRetorno()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && isset($_FILES["FileInput"]) && $_FILES["FileInput"]["error"] == UPLOAD_ERR_OK && !empty($_FILES['FileInput']['name']) && !empty($_FILES['FileInput']['tmp_name']))
        {
            echo json_encode(Baixa::model()->importarRetorno($_FILES['FileInput'], $_POST['Banco'], $_POST['forcar_importacao'], $_POST['forcar_importacao_obs']));
        }
    }

    public function actionRetornos()
    {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-retornos.js', CClientScript::POS_END);

        $cs->registerScript('datepicker', "$('.date-picker').datepicker({autoclose: true});");

        $this->render('retornos');
    }

    public function actionClearRetornosRegistrados()
    {

        Empresa::model()->clearRetornosRegistrados();
    }

    public function actionRetorno()
    {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-retorno-import-v3.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);

        $this->render('/retornoCnab/index');
    }

    public function actionIndex()
    {

        $baseUrl = Yii::app()->baseUrl;

        $cs = Yii::app()->getClientScript();

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');

        $this->render('index', array(
            'countAnalisesDia' => count(AnaliseDeCredito::model()->analisesDia()),
            'countPropostasDia' => count(Proposta::model()->propostasDia()),
        ));
    }

    public function actionGridContasPagar()
    {

        $dados = Titulo::model()->listarPagar($_POST['draw'], $_POST['search'], $_POST['start']);

        echo json_encode(
                $dados
        );
    }

    public function actionGridContasPagas()
    {

        $dados = Titulo::model()->listarPagamentos($_POST['draw'], $_POST['search'], $_POST['start']);

        echo json_encode(
                $dados
        );
    }

    public function actionGetTotalNucleo()
    {

        $dados = Titulo::model()->listarPagamentosNucleo($_POST['idGrupo']);

        echo json_encode(
                $dados
        );
    }

    public function actionGridBorderos()
    {

        $dados = Titulo::model()->listarBorderos($_POST['draw'], $_POST['search'], $_POST['start'], $_POST['borderosSelecionados'], $_POST['buscaParceiro'], $_POST['buscaData'], $_POST['buscaCodigo']);

        echo json_encode(
                $dados
        );
    }

    public function actionGridPropostas()
    {

        $data = [];

        $recordsTotal = 0;
        $recordsFiltered = 0;

        $query = "SELECT   Pr.id                                                       AS idProposta   , Pr.data_cadastro  AS dataProposta , "
                . "         Pe.nome                                                     AS nome         ,  D.numero         AS cpf          , "
                . "         UPPER(CONCAT(F.nome_fantasia, ' ', E.cidade, ' ', E.uf))    AS filial       "
                . "FROM             Proposta                AS Pr   "
                . "INNER    JOIN    Analise_de_Credito      AS AC  ON  Pr.Analise_de_Credito_id = AC.id AND  AC.habilitado "
                . "INNER    JOIN    Filial                  AS F   ON  AC.Filial_id             =  F.id AND   F.habilitado "
                . "INNER    JOIN    Filial_has_Endereco     AS FhE ON FhE.Filial_id             =  F.id AND FhE.habilitado "
                . "INNER    JOIN    Endereco                AS E   ON FhE.Endereco_id           =  E.id AND   E.habilitado "
                . "INNER    JOIN    Cliente                 AS C   ON  AC.Cliente_id            =  C.id AND   C.habilitado "
                . "INNER    JOIN    Pessoa                  AS Pe  ON   C.Pessoa_id             = Pe.id AND  Pe.habilitado "
                . "INNER    JOIN    Pessoa_has_Documento    AS PhD ON PhD.Pessoa_id             = Pe.id AND PhD.habilitado "
                . "INNER    JOIN    Documento               AS D   ON PhD.Documento_id          =  D.id AND   D.habilitado AND Tipo_documento_id = 1 "
                . "LEFT     JOIN    ItemDoBordero           AS IB  ON IB.Proposta_id            = Pr.id AND  IB.habilitado "
                . "LEFT     JOIN    Bordero                 AS B   ON IB.Bordero                =  B.id AND   B.habilitado "
                . "WHERE Pr.habilitado AND Pr.Status_Proposta_id = 2 AND IB.id IS NULL ";

        $recordsTotal = count(Yii::app()->db->createCommand($query)->queryAll());

        if (isset($_POST["buscaFilial"]) && !empty($_POST["buscaFilial"]))
        {
            $query .= "AND UPPER(CONCAT(F.nome_fantasia, ' ', E.cidade, ' ', E.uf)) LIKE '%" . strtoupper(trim($_POST["buscaFilial"])) . "%' ";
        }

        if ($_POST['buscaData'] !== null && !empty($_POST['buscaData']))
        {

            $query .= "AND LEFT(P.data_cadastro,10) = '" . trim($_POST['buscaData']) . "' ";
        }

        if ($_POST['buscaCodigo'] !== null && !empty($_POST['buscaCodigo']))
        {

            $query .= "AND P.codigo LIKE '%" . strtoupper(trim($_POST['buscaCodigo'])) . "%' ";
        }

        $query .= "ORDER BY P.id DESC ";

        $recordsFiltered = count(Yii::app()->db->createCommand($query)->queryAll());

        $query .= "LIMIT $start, 10";

        $resultado = Yii::app()->db->createCommand($query)->queryAll();

        foreach ($resultado as $r)
        {

            $dateTime = new DateTime($r["dataProposta"]);
            $dataProposta = $dateTime->format("d/m/Y H:i:s");

            $proposta = Proposta::model()->findByPk($r["idProposta"]);

            $valorFinanciado = number_format($proposta->valor, 2, ",", ".");
            $valorRepasse = number_format($proposta->valorRepasse(), 2, ",", ".");

            $data[] = [
                "idProposta" => $r["idProposta"],
                "codigoProposta" => $r["codigoProposta"],
                "dataProposta" => $dataProposta,
                "filial" => $r["filial"],
                "cpf" => $r["cpf"],
                "nome" => $r["nome"],
                "valorFinanciado" => $valorFinanciado,
                "valorRepasse" => $valorRepasse,
            ];
        }

        echo json_encode
                (
                [
                    "data" => $data,
                    "recordsTotal" => $recordsTotal,
                    "recordsFiltered" => $recordsFiltered,
                    "query" => $query
                ]
        );
    }

    public function actionGridBorderosPagos()
    {

        $nucleosMarcados = [];

        if (isset($_POST['nucleosMarcados']))
        {
            $nucleosMarcados = $_POST['nucleosMarcados'];
        }

        $dados = Titulo::model()->listarBorderosPagos(
                $_POST['draw'], $_POST['search'], $_POST['start'], $_POST['buscaParceiro'], $_POST['buscaData'], $_POST['buscaCodigo'], $_POST['idGrupo'], $_POST['idLote'], $nucleosMarcados
        );

        echo json_encode(
                $dados
        );
    }

    public function actionGridLotes()
    {

        $dados = Titulo::model()->listarLotesPagamento($_POST['draw'], $_POST['search'], $_POST['start']);

        echo json_encode(
                $dados
        );
    }

    public function actionGridLotesPagos()
    {

        $dataBaixa = "";
        $nucleosMarcados = [];
        $idGrupo = 0;
        $imagem = 0;
        $fisico = 0;
        
        $dataFiltro = "";

        if (isset($_POST['dataBaixa']))
        {
            $dataBaixa = $_POST['dataBaixa'];
            $dataFiltro = $_POST['dataBaixa'];//gambiarrissima
        }

        if (isset($_POST['nucleosMarcados']))
        {
            $nucleosMarcados = $_POST['nucleosMarcados'];
        }

        if (isset($_POST['idGrupo']))
        {
            $idGrupo = $_POST['idGrupo'];
        }

        if(isset($_POST['porImagem'])){
            $imagem = $_POST['porImagem'];
        }

        if(isset($_POST['fisicoIndefinido'])){
            $fisico = $_POST['fisicoIndefinido'];
        }

        $dados = Titulo::model()->listarLotesPagamentoPagos($_POST['draw'], $_POST['search'], $_POST['start'], $dataBaixa, $nucleosMarcados, $idGrupo, $imagem, $fisico, $dataFiltro);

        echo json_encode(
                $dados
        );
    }

    public function actionGerarLotes()
    {

        $resposta = LotePagamento::model()->gerarLotes($_POST['borderosSelecionados']);

        echo json_encode(
                $resposta
        );
    }

    public function actionExcluirLote()
    {

        $resposta = LotePagamento::model()->excluirLote($_POST['idLote']);

        echo json_encode(
                $resposta
        );
    }

    public function actionSalvarDespesa()
    {

        $retorno = [
            'hasErrors' => 0,
            'msg' => '',
            'pntfyClass' => 'success'
        ];

        if (!isset($_POST['descricao']) || empty($_POST['descricao']))
        {
            $retorno['hasErrors'] = 1;
            $retorno['msg'] .= 'Descrição não pode ser vazia. ';
            $retorno['pntfyClass'] = 'error';
        }

        if (!isset($_POST['valor']) || ((int) $_POST['valor'] <= 0))
        {
            $retorno['hasErrors'] = 1;
            $retorno['msg'] .= 'Valor deve ser superior a 0. ';
            $retorno['pntfyClass'] = 'error';
        }

        if (!isset($_POST['dataDespesa']) || empty($_POST['dataDespesa']))
        {
            $retorno['hasErrors'] = 1;
            $retorno['msg'] .= 'Data não pode ser vazia. ';
            $retorno['pntfyClass'] = 'error';
        }

        if (!isset($_POST['fornecedor']) || ((int) $_POST['fornecedor'] <= 0))
        {
            $retorno['hasErrors'] = 1;
            $retorno['msg'] .= 'Escolha um fornecedor. ';
            $retorno['pntfyClass'] = 'error';
        }

        if (!isset($_POST['natureza']) || ((int) $_POST['natureza'] <= 0))
        {
            $retorno['hasErrors'] = 1;
            $retorno['msg'] .= 'Escolha uma natureza. ';
            $retorno['pntfyClass'] = 'error';
        }

        if (!isset($_POST['rateio']) || ((int) $_POST['rateio'] <= 0))
        {
            $retorno['hasErrors'] = 1;
            $retorno['msg'] .= 'Rateio deve ser positivo.';
            $retorno['pntfyClass'] = 'error';
        }

        /* if(!isset($_POST['carencia']) || ((int) $_POST['carencia'] < 0))
          { */
        if (!isset($_POST['carencia']))
        {
            $retorno['hasErrors'] = 1;
            $retorno['msg'] .= 'Por favor, informe a carência. ';
            $retorno['pntfyClass'] = 'error';
        }

        if (!isset($_POST['intervalo']) || ((int) $_POST['intervalo'] < 0))
        {
            $retorno['hasErrors'] = 1;
            $retorno['msg'] .= 'Intervalo não pode ser negativo. ';
            $retorno['pntfyClass'] = 'error';
        }

        if (!$retorno['hasErrors'])
        {

            $transaction = Yii::app()->db->beginTransaction();

            $despesa = new Despesa;

            $despesa->habilitado = 1;
            $despesa->data_cadastro = date('Y-m-d H:i:s');
            $despesa->Usuario_criacao = Yii::app()->session['usuario']->id;
            $despesa->Fornecedor_id = $_POST['fornecedor'];
            $despesa->descricao = $_POST['descricao'];
            $despesa->dataDespesa = $_POST['dataDespesa'];

            if (!$despesa->save())
            {

                ob_start();
                var_dump($despesa->getErrors());
                $result = ob_get_clean();

                $retorno['hasErrors'] = 1;
                $retorno['msg'] = 'Erro ao salvar despesa. ' . $result;
                $retorno['pntfyClass'] = 'error';
            } else
            {

                $titulo = new Titulo;

                $titulo->habilitado = 1;
                $titulo->emissao = $_POST['dataDespesa'];
                $titulo->NaturezaTitulo_id = $_POST['natureza'];
                $titulo->prefixo = '001';

                if ($titulo->save())
                {

                    $rateio = (int) $_POST['rateio'];
                    $intervalo = (int) $_POST['intervalo'];
                    $carencia = (int) $_POST['carencia'];
                    $valor = (float) $_POST['valor'];
                    $valorParcela = ($valor / $rateio);

                    for ($i = 0; $i < $rateio; $i++)
                    {

                        $dias = ( ($i * $intervalo ) + $carencia );
                        $vencimento = date('Y-m-d', strtotime("+" . $dias . " days"));

                        $parcela = new Parcela;
                        $parcela->Titulo_id = $titulo->id;
                        $parcela->seq = $i + 1;
                        $parcela->vencimento = $vencimento;
                        $parcela->valor = $valorParcela;
                        $parcela->valor_atual = $valorParcela;

                        if (!$parcela->save())
                        {

                            ob_start();
                            var_dump($parcela->getErrors());
                            $result = ob_get_clean();

                            $retorno['hasErrors'] = 1;
                            $retorno['msg'] = 'Erro ao salvar despesa. ' . $result;
                            $retorno['pntfyClass'] = 'error';
                        }
                    }
                }
            }

            if ($retorno['hasErrors'])
            {
                $transaction->rollBack();
            } else
            {

                $despesa->Titulo_id = $titulo->id;

                if (isset($_POST['serieNF']) && isset($_POST['docNF']) && !empty($_POST['serieNF']) && !empty($_POST['docNF']))
                {

                    $notaFiscal = new NotaFiscal;
                    $notaFiscal->data_cadastro = date('Y-m-d H:i:s');
                    $notaFiscal->habilitado = 1;
                    $notaFiscal->Status_Nota_Fiscal_id = 1;
                    $notaFiscal->data = $_POST['dataDespesa'];
                    $notaFiscal->serie = $_POST['serieNF'];
                    $notaFiscal->documento = $_POST['docNF'];

                    if ($notaFiscal->save())
                    {
                        $despesa->Nota_Fiscal_id = $notaFiscal->id;

                        if ($despesa->update())
                        {
                            $retorno['msg'] = 'Despesa cadastrada com sucesso.';
                            $transaction->commit();
                        } else
                        {

                            ob_start();
                            var_dump($despesa->getErrors());
                            $result = ob_get_clean();

                            $retorno['hasErrors'] = 1;
                            $retorno['msg'] = 'Erro ao salvar despesa. ' . $result;
                            $retorno['pntfyClass'] = 'error';
                        }
                    } else
                    {

                        ob_start();
                        var_dump($notaFiscal->getErrors());
                        $result = ob_get_clean();

                        $retorno['hasErrors'] = 1;
                        $retorno['msg'] = 'Erro ao salvar despesa. ' . $result;
                        $retorno['pntfyClass'] = 'error';
                    }
                } else
                {

                    if ($despesa->update())
                    {
                        $retorno['msg'] = 'Despesa cadastrada com sucesso.';
                        $transaction->commit();
                    } else
                    {

                        ob_start();
                        var_dump($despesa->getErrors());
                        $result = ob_get_clean();

                        $retorno['hasErrors'] = 1;
                        $retorno['msg'] = 'Erro ao salvar despesa. ' . $result;
                        $retorno['pntfyClass'] = 'error';
                    }
                }
            }
        }

        echo json_encode($retorno);
    }

    public function actionListarDespesas()
    {

        $retorno = [];
        $start = $_POST['start'];

        $filtroDescricao = "";
        $filtroData = "";
        $filtroFornecedor = "";
        $filtroNatureza = "";
        $filtroSerieNF = "";
        $filtroDocNF = "";

        if (isset($_POST['filtroDescricao']))
        {
            $filtroDescricao = $_POST['filtroDescricao'];
        }

        if (isset($_POST['filtroData']))
        {
            $filtroData = $_POST['filtroData'];
        }

        if (isset($_POST['filtroFornecedor']))
        {
            $filtroFornecedor = $_POST['filtroFornecedor'];
        }

        if (isset($_POST['filtroNatureza']))
        {
            $filtroNatureza = $_POST['filtroNatureza'];
        }

        if (isset($_POST['filtroSerieNF']))
        {
            $filtroSerieNF = $_POST['filtroSerieNF'];
        }

        if (isset($_POST['filtroDocNF']))
        {
            $filtroDocNF = $_POST['filtroDocNF'];
        }

        $sql = "SELECT * FROM Despesa AS D WHERE D.habilitado";

        $despesas = Despesa::model()->findAllBySql($sql);
        $recordsTotal = count($despesas);

        $sql = "SELECT D.* "
                . "FROM                 Despesa         AS D    "
                . "INNER        JOIN    Fornecedor      AS F    ON  F.habilitado AND D.Fornecedor_id        =  F.id "
                . "INNER        JOIN    Pessoa          AS P    ON  P.habilitado AND F.Pessoa_id            =  P.id "
                . "INNER        JOIN    Titulo          AS T    ON  T.habilitado AND D.Titulo_id            =  T.id "
                . "INNER        JOIN    NaturezaTitulo  AS NT   ON NT.habilitado AND T.NaturezaTitulo_id    = NT.id "
                . "LEFT         JOIN    Nota_Fiscal     AS NF   ON NF.habilitado AND D.Nota_Fiscal_id       = NF.id "
                . "WHERE D.habilitado ";

        if (!empty($filtroDescricao))
        {
            $sql .= " AND UPPER(D.descricao) LIKE '%" . strtoupper($filtroDescricao) . "%'";
        }

        if (!empty($filtroFornecedor))
        {
            $sql .= " AND UPPER(P.nome) LIKE '%" . strtoupper($filtroFornecedor) . "%'";
        }

        if (!empty($filtroData))
        {
            $sql .= " AND left(D.dataDespesa,10) = '$filtroData'";
        }

        if (!empty($filtroNatureza))
        {
            $sql .= " AND UPPER(CONCAT(NT.codigo,' ',NT.descricao)) LIKE '%" . strtoupper($filtroNatureza) . "%'";
        }

        if (!empty($filtroSerieNF))
        {
            $sql .= " AND UPPER(NF.serie) LIKE '%" . strtoupper($filtroSerieNF) . "%'";
        }

        if (!empty($filtroDocNF))
        {
            $sql .= " AND UPPER(NF.documento) LIKE '%" . strtoupper($filtroDocNF) . "%'";
        }

        $despesas = Despesa::model()->findAllBySql($sql);
        $recordsFiltered = count($despesas);

        $sql .= "LIMIT $start, 10 ";

        $despesas = Despesa::model()->findAllBySql($sql);

        foreach ($despesas as $despesa)
        {

            $btnRemover = "<button                              "
                    . "     class='btn btn-red btnRemover'  "
                    . "     value='$despesa->id'            ";

            if ($despesa->teveBaixa())
            {
                $btnRemover .= "disabled";
            }

            $btnRemover .= ">"
                    . "     <i class='fa fa-trash-o'></i>"
                    . "</button>";

            $notaFiscal = null;

            if ($despesa->Nota_Fiscal_id !== null)
            {
                $notaFiscal = NotaFiscal::model()->find("habilitado AND id = $despesa->Nota_Fiscal_id ");
            }

            if ($notaFiscal == null)
            {
                $serieNF = "";
                $docNF = "";
            } else
            {
                $serieNF = $notaFiscal->serie;
                $docNF = $notaFiscal->documento;
            }

            $descNatureza = $despesa->titulo->natureza->codigo . ' ' . $despesa->titulo->natureza->descricao;

            $date = new DateTime($despesa->dataDespesa);
            $dataDespesa = $date->format('d/m/Y');

            $retorno[] = [
                'idDespesa' => $despesa->id,
                'descricao' => $despesa->descricao,
                'fornecedor' => $despesa->fornecedor->pessoa->nome,
                'natureza' => $descNatureza,
                'dataDespesa' => $dataDespesa,
                'serieNF' => $serieNF,
                'docNF' => $docNF,
                'btnRemover' => $btnRemover
            ];
        }

        echo json_encode(
                [
                    'data' => $retorno,
                    'recordsTotal' => $recordsTotal,
                    'recordsFiltered' => $recordsFiltered
                ]
        );
    }

    public function actionExcluirDespesa()
    {
        $retorno = [];

        if (isset($_POST['idDespesa']))
        {

            $despesa = Despesa::model()->find('habilitado AND id = ' . $_POST['idDespesa']);

            $transaction = Yii::app()->db->beginTransaction();

            if ($despesa !== null)
            {
                if (!$despesa->teveBaixa())
                {
                    $titulo = Titulo::model()->find("habilitado AND id = $despesa->Titulo_id");

                    if ($titulo !== null)
                    {

                        $parcelas = Parcela::model()->findAll("habilitado AND Titulo_id = $titulo->id");

                        foreach ($parcelas as $parcela)
                        {
                            $parcela->habilitado = 0;

                            if (!$parcela->save())
                            {

                                ob_start();
                                var_dump($parcela->getErrors());
                                $result = ob_get_clean();

                                $retorno = [
                                    'title' => 'Erro.',
                                    'msg' => "Não foi possível excluir a Despesa. Erro ao "
                                    . "excluir parcela de ID $parcela->id. " . $result,
                                    'type' => 'error',
                                    'hasErrors' => 1
                                ];

                                $transaction->rollBack();

                                break;
                            }
                        }

                        if (!isset($retorno['hasErrors']) || !$retorno['hasErrors'])
                        {

                            $titulo->habilitado = 0;

                            if (!$titulo->save())
                            {

                                ob_start();
                                var_dump($parcela->getErrors());
                                $result = ob_get_clean();

                                $retorno = [
                                    'title' => 'Erro.',
                                    'msg' => "Não foi possível excluir a Despesa. Erro ao "
                                    . "excluir o título de ID $titulo->id. " . $result,
                                    'type' => 'error',
                                    'hasErrors' => 1
                                ];

                                $transaction->rollBack();
                            }
                        }
                    }

                    if (!isset($retorno['hasErrors']) || !$retorno['hasErrors'])
                    {
                        $despesa->habilitado = 0;

                        if ($despesa->save())
                        {
                            $retorno = [
                                'title' => 'Sucesso.',
                                'msg' => 'Despesa excluída com sucesso ',
                                'type' => 'success',
                                'hasErrors' => 0
                            ];

                            $transaction->commit();
                        } else
                        {

                            ob_start();
                            var_dump($despesa->getErrors());
                            $result = ob_get_clean();

                            $retorno = [
                                'title' => 'Erro.',
                                'msg' => "Erro ao excluir Despesa. $result ",
                                'type' => 'error',
                                'hasErrors' => 1
                            ];

                            $transaction->rollBack();
                        }
                    }
                } else
                {
                    $retorno = [
                        'title' => 'Erro.',
                        'msg' => 'Despesa não pode ser excluída. A mesma já contém baixas',
                        'type' => 'error',
                        'hasErrors' => 1
                    ];
                }
            } else
            {
                $retorno = [
                    'title' => 'Erro.',
                    'msg' => 'Despesa não encontrada. ID: ' . $_POST['idDespesa'],
                    'type' => 'error',
                    'hasErrors' => 1
                ];
            }
        } else
        {
            $retorno = [
                'title' => 'Erro.',
                'msg' => 'ID da Despesa não informado.',
                'type' => 'error',
                'hasErrors' => 1
            ];
        }

        echo json_encode($retorno);
    }

    public function actionGetParcelasDespesa()
    {

        $retorno = [];
        $parcelas = [];
        $filtroDivergencia = [];

        $sqlDivergencia = "";

        if (isset($_POST['filtroDivergencia']))
        {
            $filtroDivergencia = $_POST['filtroDivergencia'];
        }

        $sql = "";

        if (isset($_POST['idDespesa']) && !empty($_POST['idDespesa']))
        {

            $idDespesa = $_POST['idDespesa'];

            $despesa = Despesa::model()->find("habilitado AND id = $idDespesa");

            if ($despesa !== null)
            {

                $parcelas = Parcela::model()->findAll("habilitado AND Titulo_id = $despesa->Titulo_id");
            }

            $recordsTotal = count($parcelas);
            $recordsFiltered = count($parcelas);
        } else
        {

            $sql = "   SELECT P.id
                        FROM        Titulo      AS T
                        INNER JOIN  Despesa     AS D ON D.habilitado AND T.id = D.Titulo_id
                        INNER JOIN  Parcela     AS P ON P.habilitado AND T.id = P.Titulo_id
                        LEFT  JOIN  Baixa       AS B ON B.baixado    AND P.id = B.Parcela_id
                        WHERE T.habilitado
                    ";

            $recordsTotal = count(Yii::app()->db->createCommand($sql)->queryAll());

            if (!empty($filtroDivergencia))
            {

                $dataDivergencia = date('Y-m-d');

                if ($filtroDivergencia[0])
                {
                    $sqlDivergencia .= " (LEFT(P.vencimento,10) < '$dataDivergencia' AND B.id IS NULL) ";
                }

                if ($filtroDivergencia[1])
                {
                    if (!empty($sqlDivergencia))
                    {
                        $sqlDivergencia .= " OR ";
                    }
                    $sqlDivergencia .= " (LEFT(P.vencimento,10) = '$dataDivergencia' AND B.id IS NULL) ";
                }

                if ($filtroDivergencia[2])
                {
                    if (!empty($sqlDivergencia))
                    {
                        $sqlDivergencia .= " OR ";
                    }
                    $sqlDivergencia .= " (B.id IS NOT NULL) ";
                }

                if ($filtroDivergencia[3])
                {
                    if (!empty($sqlDivergencia))
                    {
                        $sqlDivergencia .= " OR ";
                    }
                    $sqlDivergencia .= " (LEFT(P.vencimento,10) > '$dataDivergencia' AND B.id IS NULL) ";
                }
            }

            if (!empty($sqlDivergencia))
            {
                $sql .= " AND ($sqlDivergencia) ";
            }

            if (isset($_POST['filtroDespesa2']))
            {
                $filtroDespesa2 = $_POST['filtroDespesa2'];
                if (!empty($filtroDespesa2))
                {
                    $sql .= " AND UPPER(D.descricao) LIKE '%" . strtoupper($filtroDespesa2) . "%'";
                }
            }

            if (isset($_POST['filtroVencimento']))
            {
                $filtroVencimento = $_POST['filtroVencimento'];
                if (!empty($filtroVencimento))
                {
                    $sql .= " AND left(P.vencimento,10) >= '" . $filtroVencimento . "' ";
                }
            }

            if (isset($_POST['filtroVencimento2']))
            {
                $filtroVencimento = $_POST['filtroVencimento2'];
                if (!empty($filtroVencimento))
                {
                    $sql .= " AND left(P.vencimento,10) <= '" . $filtroVencimento . "' ";
                }
            }

            if (isset($_POST['filtroDataBaixa']))
            {
                $filtroDataBaixa = $_POST['filtroDataBaixa'];
                if (!empty($filtroDataBaixa))
                {
                    $sql .= " AND left(B.data_da_baixa,10) >= '$filtroDataBaixa'";
                }
            }

            if (isset($_POST['filtroDataBaixa2']))
            {
                $filtroDataBaixa = $_POST['filtroDataBaixa2'];
                if (!empty($filtroDataBaixa))
                {
                    $sql .= " AND left(B.data_da_baixa,10) <= '$filtroDataBaixa'";
                }
            }

            $recordsFiltered = count(Yii::app()->db->createCommand($sql)->queryAll());

            $sql .= "LIMIT " . $_POST["start"] . ", 10 ";

            $resultado = Yii::app()->db->createCommand($sql)->queryAll();

            foreach ($resultado as $r)
            {

                $parcelas[] = Parcela::model()->findByPk($r['id']);
            }
        }

        foreach ($parcelas as $parcela)
        {

            $cor = "";

            $date = new DateTime($parcela->vencimento);
            $vencimento = $date->format('d/m/Y');
            $dataVenc = $date->format('Y-m-d');

            $baixa = Baixa::model()->find("baixado AND Parcela_id = $parcela->id");

            $despesa = Despesa::model()->find("habilitado AND Titulo_id = $parcela->Titulo_id");

            if ($dataVenc < date('Y-m-d'))
            {
                $cor = "red";
            } else if ($date == date('Y-m-d'))
            {
                $cor = "blue";
            } else
            {
                $cor = "black";
            }

            if ($baixa !== null)
            {

                $cor = "green";

                $btn = "<button class='btn btn-red btnBaixa' data-tipo='cancelar' value='$baixa->id'>"
                        . "     <i class='clip-cancel-circle'></i>"
                        . "</button>";

                $date = new DateTime($baixa->data_da_baixa);

                $dataBaixa = "<font color='$cor'>R$ "
                        . $date->format('d/m/Y')
                        . "</font>";

                $valorDesconto = "<font color='$cor'>R$ "
                        . number_format($baixa->valor_desconto, 2, ",", ".")
                        . "</font>";
                $valorJuros = "<font color='$cor'>R$ "
                        . number_format($baixa->valor_mora, 2, ",", ".")
                        . "</font>";
                $valorPago = "<font color='$cor'>R$ "
                        . number_format($baixa->valor_pago, 2, ",", ".")
                        . "</font>";
            } else
            {

                $btn = "<button class='btn btn-green btnBaixa' data-tipo='baixar' value='$parcela->id'>"
                        . "     <i class='fa fa-money'></i>"
                        . "</button>";

                $dataBaixa = "<input   type='date' "
                        . "         value='" . date('Y-m-d') . "' "
                        . "         class='form form-control inputBaixa dataBaixa' "
                        . "         data-idparcela='$parcela->id' />";

                $valorDesconto = "<input   type='number' "
                        . "         value='0'"
                        . "         class='form form-control inputBaixa valorDesconto'   "
                        . "         data-idparcela='$parcela->id' />";
                $valorJuros = "<input   type='number' "
                        . "         value='0'"
                        . "         class='form form-control inputBaixa valorJuros'      "
                        . "         data-idparcela='$parcela->id' />";
                $valorPago = "<input   type='number' "
                        . "         value='$parcela->valor'"
                        . "         class='form form-control inputBaixa valorPago'       "
                        . "         data-idparcela='$parcela->id' />";
            }

            $descricaoDespesa = "<font color='$cor'>" . $despesa->descricao . "</font>";
            $parcelaSeq = "<font color='$cor'>" . $parcela->seq . "</font>";
            $vencimento = "<font color='$cor'>" . $vencimento . "</font>";
            $valorParcela = "<font color='$cor'>R$ " . number_format($parcela->valor, 2, ",", ".") . "</font>";

            $retorno[] = array(
                'idParcela' => $parcela->id,
                'descricaoDespesa' => $descricaoDespesa,
                'parcela' => $parcelaSeq,
                'vencimento' => $vencimento,
                'valor' => $valorParcela,
                'valorParcela' => $parcela->valor,
                'dataBaixa' => $dataBaixa,
                'valorJuros' => $valorJuros,
                'valorDesconto' => $valorDesconto,
                'valorBaixa' => $valorPago,
                'btnBaixar' => $btn
            );
        }


        echo json_encode(
                array(
                    'recordsTotal' => $recordsTotal,
                    'recordsFiltered' => $recordsFiltered,
                    'data' => $retorno,
                    'sql' => $sql
                )
        );
    }

    public function actionBaixarParcelaDespesa()
    {

        $retorno = [
            'hasErrors' => 0,
            'msg' => '',
            'pntfyClass' => 'success'
        ];

        if (isset($_POST['idEntidade']) && isset($_POST['tipo']))
        {

            $id = $_POST['idEntidade'];
            $tipo = $_POST['tipo'];

            if ($tipo == "baixar")
            {

                $parcela = Parcela::model()->find("habilitado AND id = $id");

                if ($parcela !== null)
                {

                    if (isset($_POST['valorPago']))
                    {
                        $valorPago = $_POST['valorPago'];
                        $valorDesconto = $_POST['valorDesconto'];
                        $valorJuros = $_POST['valorJuros'];
                        $dataBaixa = $_POST['dataBaixa'];
                    } else
                    {
                        $valorPago = $parcela->valor;
                        $valorDesconto = 0;
                        $valorJuros = 0;
                        $dataBaixa = date('Y-m-d');
                    }

                    $baixa = new Baixa;
                    $baixa->Parcela_id = $parcela->id;
                    $baixa->valor_pago = $valorPago;
                    $baixa->valor_abatimento = $valorPago;
                    $baixa->valor_mora = $valorJuros;
                    $baixa->valor_desconto = $valorDesconto;
                    $baixa->data_da_baixa = $dataBaixa;
                    $baixa->baixado = 1;
                    $baixa->Filial_id = 19; //lembrar de mudar

                    if ($baixa->save())
                    {
                        $retorno['msg'] = "Baixa efetuada com sucesso.";
                    } else
                    {

                        ob_start();
                        var_dump($baixa->getErrors());
                        $result = ob_get_clean();

                        $retorno['hasErrors'] = 1;
                        $retorno['msg'] = 'Erro ao baixar parcela. ' . $result;
                        $retorno['pntfyClass'] = 'error';
                    }
                } else
                {
                    $retorno['hasErrors'] = 1;
                    $retorno['msg'] = "Parcela não encontrada";
                    $retorno['pntfyClass'] = "error";
                }
            } else
            {

                $baixa = Baixa::model()->find("baixado AND id = $id");

                if ($baixa !== null)
                {

                    $baixa->baixado = 0;

                    if ($baixa->update())
                    {
                        $retorno['msg'] = "Baixa cancelada com sucesso.";
                    } else
                    {

                        ob_start();
                        var_dump($baixa->getErrors());
                        $result = ob_get_clean();

                        $retorno['hasErrors'] = 1;
                        $retorno['msg'] = 'Erro ao cancelar baixa. ' . $result;
                        $retorno['pntfyClass'] = 'error';
                    }
                } else
                {
                    $retorno['hasErrors'] = 1;
                    $retorno['msg'] = "Baixa não encontrada";
                    $retorno['pntfyClass'] = "error";
                }
            }
        }

        echo json_encode($retorno);
    }

    public function actionMudarVencimentoParcela()
    {

        $retorno = [
            'msg' => 'Vencimento alterado com sucesso.',
            'type' => 'success',
            'hasErrors' => false
        ];

        if (isset($_POST['idParcela']))
        {

            if (isset($_POST['vencimento']))
            {

                $parcela = Parcela::model()->find('habilitado AND id = ' . $_POST['idParcela']);

                if ($parcela !== null)
                {
                    $parcela->vencimento = $_POST['vencimento'];

                    if ($parcela->save())
                    {
                        $dataVencimento = new DateTime($parcela->vencimento);
                        $novoVencimento = $dataVencimento->format('d/m/Y');

                        $retorno['novoVencimento'] = $novoVencimento;
                    } else
                    {
                        ob_start();
                        var_dump($parcela->getErrors());
                        $result = ob_get_clean();

                        $retorno = [
                            'msg' => 'Erro ao atualizar vencimento. ' . $result,
                            'type' => 'error',
                            'hasErrors' => true
                        ];
                    }
                } else
                {
                    $retorno = [
                        'msg' => 'Parcela não encontrada.',
                        'type' => 'error',
                        'hasErrors' => true
                    ];
                }
            } else
            {
                $retorno = [
                    'msg' => 'Vencimento não encontrado.',
                    'type' => 'error',
                    'hasErrors' => true
                ];
            }
        } else
        {
            $retorno = [
                'msg' => 'ID da parcela não encongtrado.',
                'type' => 'error',
                'hasErrors' => true
            ];
        }

        echo json_encode($retorno);
    }

}
