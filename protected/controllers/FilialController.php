<?php

class FilialController extends Controller
{

    public $layout = '//layouts/main_sigac_template';

    public function actionPropostas()
    {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-propostas-por-filial.js', CClientScript::POS_END);

        $this->render('propostas');
    }

    public function actionListarPropostas()
    {
        echo json_encode(Filial::model()->propostasFilial($_POST['draw'], $_POST['start'], array($_POST['codigo_filter'], $_POST['nome_filter'])));
    }

    public function filters()
    {

        return array(
            'accessControl',
            'postOnly + delete',
        );
    }

    public function actionGetRecebimentos()
    {

        echo json_encode(Filial::model()->getRecebimentos($_POST['draw'], array($_POST['Filial_id']), $_POST['data_de'], $_POST['data_ate'], $_POST['start']));
    }

    public function actionRecebimentos()
    {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-recebimentos-filial.js', CClientScript::POS_END);

        $cs->registerScript('datepicker', "$('.date-picker').datepicker({autoclose: true});");

        $this->render('recebimentos');
    }

    public function accessRules()
    {

        return array(
            array('allow',
                'actions' => array('propostas', 'listarPropostas', 'getRecebimentos', 'recebimentos', 'changeAtividadePrincipal', 'changeAtividadeSecundaria', 'admin', 'delete', 'create', 'update', 'index', 'view', 'mudarNucleo', 'mudarUrlLogin'),
                'users' => array('@'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionView($id)
    {

        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionCreate()
    {
        $data = 'Entrou aqui';
        $model = new Filial;
        $endereco = new Endereco;
        $contato = new Contato;
        $email = new Email;
        $telefone = new Telefone;

        $util = new Util;

        if (isset($_POST['Telefone']) && isset($_POST['Email']))
        {
            
            $model->attributes = $_POST['Filial'];
            $unidade_de_negocio = $_POST['Unidade_de_negocio_id'];
            $model->Unidade_de_negocio_id = $unidade_de_negocio;
            $model->data_de_abertura = $util->view_date_to_bd($model->data_de_abertura);

            $contato->data_cadastro = date('Y-m-d H:i:s');
            $contato->data_cadastro_br = date('d/m/Y H:i:s');

            if ($contato->save())
            {
                
                $telefone->attributes = $_POST['Telefone'];
                $telefone->data_cadastro = date('Y-m-d H:i:s');
                $telefone->data_cadastro_br = date('d/m/Y H:i:s');

                $email->attributes = $_POST['Email'];
                $email->data_cadastro = date('Y-m-d H:i:s');
                $email->data_cadastro_br = date('d/m/Y H:i:s');

                if ($telefone->save())
                {
                    
                    $contatoHasTelefone = new ContatoHasTelefone;
                    $contatoHasTelefone->Contato_id = $contato->id;
                    $contatoHasTelefone->Telefone_id = $telefone->id;
                    $contatoHasTelefone->data_cadastro = date('Y-m-d H:i:s');
                    $contatoHasTelefone->data_cadastro_br = date('d/m/Y H:i:s');
                    $contatoHasTelefone->save();
                }

                if ($email->save())
                {
                    
                    $contatoHasEmail = new ContatoHasEmail;
                    $contatoHasEmail->Contato_id = $contato->id;
                    $contatoHasEmail->Email_id = $email->id;
                    $contatoHasEmail->data_cadastro = date('Y-m-d H:i:s');
                    $contatoHasEmail->save();
                    $contatoHasEmail->data_cadastro_br = date('d/m/Y H:i:s');
                }

                $model->Contato_id = $contato->id;
                $model->data_cadastro = date('Y-m-d H:i:s');
                $model->data_cadastro_br = date('d/m/Y H:i:s');
                $model->PoliticaCredito_id = $_POST["PoliticaCredito_id"];

                $endereco->attributes = $_POST['Endereco'];
                $endereco->data_cadastro = date('Y-m-d H:i:s');
                $endereco->data_cadastro_br = date('d/m/Y H:i:s');
                
                if ($model->save() && $endereco->save())
                {
                    
                    if(isset($_POST["PoliticaCredito_id"]))
                    {
                        
                        $politicaCredito = PoliticaCredito::model()->findByPk($_POST["PoliticaCredito_id"]);
                        
                        if($politicaCredito !== null)
                        {
                            
                            $filialHasPoliticaCredito                       = new FilialHasPoliticaCredito  ;
                            
                            $filialHasPoliticaCredito->habilitado           = 1                             ;
                            $filialHasPoliticaCredito->data_cadastro        = date("Y-m-d H:i:s")           ;
                            $filialHasPoliticaCredito->dataInicio           = date("Y-m-d H:i:s")           ;
                            $filialHasPoliticaCredito->Filial_id            = $model->id                    ;
                            $filialHasPoliticaCredito->PoliticaCredito_id   = $politicaCredito->id          ;
                            
                            if($filialHasPoliticaCredito->save())
                            {
                                
                            }
                            
                        }
                        
                    }

                    if (isset($_POST['UnidadesDeNegocio']))
                    {

                        $unidadesDeNegocio = $_POST['UnidadesDeNegocio'];

                        for ($i = 0; $i < sizeof($unidadesDeNegocio); $i++)
                        {
                            $filialHasAtividadeEconomicaSecundaria = new FilialHasAtividadeEconomicaSecundaria;
                            $filialHasAtividadeEconomicaSecundaria->Filial_id = $model->id;
                            $filialHasAtividadeEconomicaSecundaria->Unidade_de_negocio_id = $unidadesDeNegocio[$i];
                            $filialHasAtividadeEconomicaSecundaria->data_cadastro = date('Y-m-d H:i:s');
                            $filialHasAtividadeEconomicaSecundaria->habilitado = 1;
                            $filialHasAtividadeEconomicaSecundaria->save();
                        }
                    }

                    $filialHasEndereco = new FilialHasEndereco;
                    $filialHasEndereco->data_cadastro = date('Y-m-d H:i:s');
                    $filialHasEndereco->data_cadastro_br = date('d/m/Y H:i:s');
                    $filialHasEndereco->Filial_id = $model->id;
                    $filialHasEndereco->Endereco_id = $endereco->id;

                    if ($filialHasEndereco->save())
                    {
                        
                        $this->redirect('admin');
                    }
                }else {
                    ob_start();
                    var_dump($model->getErrors());
                    $resErro = ob_get_clean();
                    echo("<script>console.log('PHP: ".json_encode($resErro)."');</script>");
                }
            }
        }

        $baseUrl = Yii::app()->baseUrl;

        $cs = Yii::app()->getClientScript();

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/form-wizard-filial.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.smartWizard.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskMoney.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/form-elements.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.inputlimiter.1.3.1.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.autosize.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/update-select.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/daterangepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-timepicker.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.tagsinput.js', CClientScript::POS_END);

        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/summernote.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ckeditor/ckeditor.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ckeditor/adapter/jquery.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/filial-form-fn.js', CClientScript::POS_END);

        $cs->registerScript('FormWizard', 'FormWizard.init();');
        $cs->registerScript('FormElements', 'FormElements.init();');
        $cs->registerScript('CNPJMask', 'jQuery(function($){$("#Filial_cnpj").mask("99.999.999/9999-99");});');
        $cs->registerScript('DataMask', 'jQuery(function($){$("#Filial_data_de_abertura").mask("99/99/9999");});');
        $cs->registerScript('TelefoneMask', 'jQuery(function($){$("#Telefone_numero").mask("(99) 9999-9999");});');



        $this->render('create', array(
            'model' => $model,
            'endereco' => $endereco,
            'telefone' => $telefone,
            'email' => $email,
            'unidadesDeNegocio' => UnidadeDeNegocio::model()->findAll()
        ));
    }

    public function actionUpdate()
    {

        if (!empty(filter_input(INPUT_POST, 'id')))
        {

            $id = filter_input(INPUT_POST, 'id');

            $model = $this->loadModel($id);
            $util = new Util;

            $filialHasEndereco = FilialHasEndereco::model()->find("Filial_id = " . $model->id);
            $endereco = Endereco::model()->findByPk($filialHasEndereco->Endereco_id);

            $contatoHasTelefone = ContatoHasTelefone::model()->find("Contato_id = " . $model->contato->id);
            
            if($contatoHasTelefone != null){
                $telefone = Telefone::model()->findByPk($contatoHasTelefone->Telefone_id);
            }
            else{
                $telefone = new Telefone;
            }

            $contatoHasEmail = ContatoHasEmail::model()->find("Contato_id = " . $model->contato->id);
            
            if($contatoHasEmail != null){
                $email = Email::model()->findByPk($contatoHasEmail->Email_id);
            }
            else{
                $email = new Email;
            }

            if (!empty($_POST['Telefone']) && !empty($_POST['Email']))
            {

                $model->attributes = $_POST['Filial'];
                $telefone->attributes = $_POST['Telefone'];
                $email->attributes = $_POST['Email'];
                $endereco->attributes = $_POST['Endereco'];

                $model->data_de_abertura = $util->view_date_to_bd($model->data_de_abertura);

                $telefone->save();
                $email->save();
                $model->update();
                $endereco->update();
                        
                if(isset($_POST["PoliticaCredito_id"]))
                {

                    $politicaCredito = PoliticaCredito::model()->findByPk($_POST["PoliticaCredito_id"]);

                    if(!isset($model->filialHasPoliticaCredito->politicaCredito->id) || $model->filialHasPoliticaCredito->politicaCredito->id !== $_POST["PoliticaCredito_id"])
                    {
                        
                        if(isset($model->filialHasPoliticaCredito->politicaCredito->id))
                        {
                            $model->filialHasPoliticaCredito->dataFim = date("Y-m-d H:i:s");
                            
                            $model->filialHasPoliticaCredito->update();
                        }
                    
                        if($politicaCredito !== null)
                        {

                            $filialHasPoliticaCredito                       = new FilialHasPoliticaCredito  ;

                            $filialHasPoliticaCredito->habilitado           = 1                             ;
                            $filialHasPoliticaCredito->data_cadastro        = date("Y-m-d H:i:s")           ;
                            $filialHasPoliticaCredito->dataInicio           = date("Y-m-d H:i:s")           ;
                            $filialHasPoliticaCredito->Filial_id            = $model->id                    ;
                            $filialHasPoliticaCredito->PoliticaCredito_id   = $politicaCredito->id          ;

                            if($filialHasPoliticaCredito->save())
                            {

                            }

                        }
                        
                    }

                }

                $this->redirect($this->createUrl('admin'));
            }


            $baseUrl = Yii::app()->baseUrl;

            $cs = Yii::app()->getClientScript();

            $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
            $cs->registerScriptFile($baseUrl . '/js/form-wizard-filial.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/jquery.validate.min.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/jquery.smartWizard.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/jquery.maskMoney.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/form-elements.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/jquery.inputlimiter.1.3.1.min.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/jquery.autosize.min.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/update-select.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/daterangepicker.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/bootstrap-timepicker.min.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpicker.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/jquery.tagsinput.js', CClientScript::POS_END);

            $cs->registerScriptFile($baseUrl . '/js/jquery.validate.min.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/summernote.min.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/ckeditor/ckeditor.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/ckeditor/adapter/jquery.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/filial-form-edit-fn.js', CClientScript::POS_END);

            $cs->registerScript('FormWizard', 'FormWizard.init();');
            $cs->registerScript('FormElements', 'FormElements.init();');
            $cs->registerScript('CNPJMask', 'jQuery(function($){$("#Filial_cnpj").mask("99.999.999/9999-99");});');
            $cs->registerScript('DataMask', 'jQuery(function($){$("#Filial_data_de_abertura").mask("99/99/9999");});');
            $cs->registerScript('TelefoneMask', 'jQuery(function($){$("#Telefone_numero").mask("(99) 9999-9999");});');
            $this->render('update', array(
                'model' => $model,
                'endereco' => $endereco,
                'telefone' => $telefone,
                'email' => $email,
                'unidadesDeNegocio' => UnidadeDeNegocio::model()->findAll()
            ));
        }
    }

    public function actionChangeAtividadePrincipal()
    {

        $id = $_GET['id'];
        $idFilial = $_GET['filialId'];

        $filial = Filial::model()->findByPk($idFilial);

        $retorno = $filial->mudarAtividadePrimaria($id);

        echo json_encode($retorno);
    }

    public function actionChangeAtividadeSecundaria()
    {

        $id = $_GET['id'];
        $idFilial = $_GET['filialId'];
        $checked = $_GET['check'];

        $filial = Filial::model()->findByPk($idFilial);

        $result = $filial->mudarRelacaoAtividadeSecundaria($id, $checked);

        //echo $result;
    }

    public function actionDelete($id)
    {

        $filial = $this->loadModel($id);

        $filialHasEndereco = FilialHasEndereco::model()->find("Filial_id = " . $filial->id);
        $endereco = Endereco::model()->findByPk($filialHasEndereco->Endereco_id);

        $filial->contato->habilitado = 0;
        $filialHasEndereco->habilitado = 0;
        $filial->habilitado = 0;

        $filial->contato->save();
        $filialHasEndereco->save();
        $filial->save();

        $this->redirect($this->createUrl('admin'));
    }

    public function actionIndex()
    {

        $dataProvider = new CActiveDataProvider('Filial');

        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionAdmin()
    {

        $baseUrl = Yii::app()->baseUrl;

        $cs = Yii::app()->getClientScript();

        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/DT_bootstrap.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/table-data.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/filial/fn-grid.js', CClientScript::POS_END);

        $cs->registerScript('TableData', 'TableData.init();');

        $model = new Filial('search');

        $model->unsetAttributes();

        if (isset($_GET['Filial']))
            $model->attributes = $_GET['Filial'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionMudarNucleo()
    {

        echo json_encode(
                Filial::model()->salvarNucleo($_POST['idFilial'], $_POST['idNucleo'])
        );
    }

    public function actionMudarUrlLogin()
    {

        echo json_encode(
                Filial::model()->salvarUrlLogin($_POST['idFilial'], $_POST['urlLogin'])
        );
    }

    public function loadModel($id)
    {

        $model = Filial::model()->findByPk($id);

        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model)
    {

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'filial-form')
        {

            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
