<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PropostaNovoController
 *
 * @author andre
 */
class FormalizacaoController extends Controller 
{

    public $layout = '//layouts/main_sigac_template';

    public function filters() {

        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete' // we only allow deletion via POST request
        );
    }
    
    public function accessRules() 
    {
        return array
        (
            array
            (
                'allow'                     , // allow authenticated user to perform 'create' and 'update' actions
                'actions'   => array
                (
                    'propostasFormalizacao' ,
                    'listarPropostas'       ,
                    'anexarDanfe'
                ),
                'users'     => array(
                    '@'
                )
            ),
            array
            (
                'deny'                      ,
                'actions'   => array
                (
                    'index'
                )                           ,
                'users'     => array
                (
                    '*'
                )                           ,
            ),
            array
            (
                'deny'                      , // deny all users
                'users'     => array
                (
                    '*'
                )
            )
        );
    }
    
    public function actionPropostasFormalizacao()
    {

        $this->layout   = '//layouts/main_sigac_template'   ;
        
        $baseUrl        = Yii::app()->baseUrl               ;
        $cs             = Yii::app()->getClientScript()     ;

        $cs->registerCssFile    ($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css'                              );
        $cs->registerCssFile    ($baseUrl . '/css/sigax_template/dataTables.tableTools.css'                                 );

        $cs->registerScriptFile ($baseUrl . '/js/jquery.min.js'                                                             );
        $cs->registerScriptFile ($baseUrl . '/js/jquery.dataTables-1.10.0.js'                   , CClientScript::POS_END    );
        $cs->registerScriptFile ($baseUrl . '/js/bootstrap-modal.js'                            , CClientScript::POS_END    );
        $cs->registerScriptFile ($baseUrl . '/js/bootstrap-modalmanager.js'                     , CClientScript::POS_END    );
        $cs->registerScriptFile ($baseUrl . '/js/dataTables.tableTools.js'                      , CClientScript::POS_END    );
        $cs->registerScriptFile ($baseUrl . '/js/jquery.validate.min.js'                        , CClientScript::POS_END    );
        $cs->registerScriptFile ($baseUrl . '/js/jquery.form.js'                                , CClientScript::POS_END    );
        $cs->registerScriptFile ($baseUrl . '/js/dataTables.fnReloadAjax.js'                    , CClientScript::POS_END    );
        $cs->registerScriptFile ($baseUrl . '/js/bootstrap-filestyle.min.js'                    , CClientScript::POS_END    );
        $cs->registerScriptFile ($baseUrl . '/js/bootstrap-multiselect.js'                      , CClientScript::POS_END    );
        $cs->registerScriptFile ($baseUrl . '/js/formalizacao/fn-propostasFormalizacaov3'
                . '.js'    , CClientScript::POS_END    );

        $this->render('propostaFormalizacao');
        
    }

    public function actionListarPropostas()
    {
        
        $hasErrors          = false ;
        $dados              = []    ;
        $recordsFiltered    = 0     ;
        $recordsTotal       = 0     ;
        $erro               = ""    ;
        
        $marcados           = []    ;
        
        $sql    = "SELECT   UPPER(Pe.nome)    AS nomeCliente    ,  P.codigo         AS codigoProposta   , 
                             P.id             AS idProposta     ,  
                             P.qtd_parcelas   AS qtdParcelas    ,  P.valor_parcela  AS valorParcela     ,  
                             P.valor_final    AS valorFinal     ,  P.data_cadastro  AS dataProposta     ,
                             M.id             AS idModalidade   ,  M.descricao      AS modalidade       ,
                             D.numero         AS cpf
                    FROM         Proposta                AS P                                                                
                    LEFT  JOIN   Venda                   AS V    ON    V.habilitado AND   P.id =    V.Proposta_id            
                    INNER JOIN   Analise_de_Credito      AS AC   ON   AC.habilitado AND  AC.id =    P.Analise_de_Credito_id  
                    INNER JOIN   Filial                  AS F    ON    F.habilitado AND   F.id =   AC.Filial_id              
                    INNER JOIN   Filial_has_Endereco     AS FhE  ON  FhE.habilitado AND   F.id =  FhE.Filial_id              
                    INNER JOIN   Endereco                AS E    ON    E.habilitado AND   E.id =  FhE.Endereco_id              
                    INNER JOIN   NucleoFiliais           AS NuF  ON  NuF.habilitado AND NuF.id =    F.NucleoFiliais_id       
                    INNER JOIN   Cliente                 AS C    ON    C.habilitado AND   C.id =   AC.Cliente_id             
                    INNER JOIN   Pessoa                  AS Pe   ON   Pe.habilitado AND  Pe.id =    C.Pessoa_id              
                    INNER JOIN   Pessoa_has_Documento    AS PhD  ON  PhD.habilitado AND  Pe.id =  PhD.Pessoa_id              
                    INNER JOIN   Documento               AS D    ON    D.habilitado AND   D.id =  PhD.Documento_id              AND Tipo_documento_id = 1 
                    INNER JOIN   Tabela_Cotacao          AS TC   ON   TC.habilitado AND  TC.id =    P.Tabela_id
                    INNER JOIN   ModalidadeTabela        AS M    ON                       M.id =   TC.ModalidadeId
                    WHERE P.habilitado AND P.Status_Proposta_id IN (2,7) AND P.Financeira_id = 10 ";
        
        $sql .= " AND   (   
                            (

                                V.id IS NOT NULL 
                                
                                AND

                                EXISTS  (   
                                            SELECT *
                                            FROM        Venda_has_Nota_Fiscal   AS VNF                                     
                                            INNER JOIN  Nota_Fiscal             AS NF   ON   NF.habilitado AND VNF.Nota_Fiscal_id   = NF.id
                                            WHERE                                           VNF.habilitado AND VNF.Venda_id         =  V.id AND NF.chaveNFe IS NOT NULL AND NF.chaveNFe <> ''
                                        )
                            )
                            
                            OR
                            
                            (
                                V.id IS NULL
                            )
                            
                        )";
        
        try
        {
            
            $resultado      = Yii::app()->db->createCommand($sql)->queryAll();
            
            $recordsTotal   = count($resultado);
            
            if(isset($_POST["codigo_filter"]) && !empty($_POST["codigo_filter"]))
            {
                $sql .= " AND P.codigo LIKE '%" . $_POST["codigo_filter"] . "%' " ;
            }
            
            if(isset($_POST["filial_filter"]) && !empty($_POST["filial_filter"]))
            {
                $sql .= " AND CONCAT(F.nome_fantasia, ' / ', E.cidade, '-', E.uf) LIKE '%" . $_POST["filial_filter"] . "%' " ;
            }
            
            if(isset($_POST["cpf_filter"]) && !empty($_POST["cpf_filter"]))
            {
                $sql .= " AND D.numero LIKE '%" . $_POST["cpf_filter"] . "%' " ;
            }
            
            if(isset($_POST["nome_filter"]) && !empty($_POST["nome_filter"]))
            {
                $sql .= " AND Pe.nome LIKE '%" . $_POST["nome_filter"] . "%' " ;
            }
            
            if(isset($_POST["data_filter"])  && !empty($_POST["data_filter"]))
            {
                $sql .= " AND LEFT(P.data_cadastro,10) LIKE '" . $_POST["data_filter"] . "' " ;
            }
            
            if( (isset($_POST["comJuros"]) && $_POST["comJuros"] == "1") && (!isset($_POST["semJuros"]) || $_POST["semJuros"] == "0") )
            {
                $sql .= " AND TC.ModalidadeId = 1 ";
            }
            else if(  (isset($_POST["semJuros"]) && $_POST["semJuros"] == "1") && (!isset($_POST["comJuros"]) || $_POST["comJuros"] == "0") )
            {
                    
                $sql .= " AND TC.ModalidadeId = 2 ";
                
            }
            
            $resultado = Yii::app()->db->createCommand($sql)->queryAll();
            
            foreach ($resultado as $r)
            {
                
                if( 
                        (       isset($_POST["selecionaTudo"]) && $_POST["selecionaTudo"] == "1"                        
                        )                                                                                           || 
                        (   
                            (   isset($_POST["selecionaTudo"]) && $_POST["selecionaTudo"] == "2"                )   && 
                            (   isset($_POST["notasMarcadas"]) && in_array($r["idNF"], $_POST["notasMarcadas"]) ) 
                        )
                    )
                {
                    $marcados[] = $r["idNF"];
                }
                
            }
            
            $recordsFiltered    = count($resultado);
            
            $sql .= "ORDER BY P.id DESC                "   ;
            
            if(isset($_POST["start"]))
            {
                $sql .= "LIMIT " . $_POST["start"] . ", 10  "   ;
            }
            
            $resultado = Yii::app()->db->createCommand($sql)->queryAll();
            
            foreach ($resultado as $r)
            {
                
                $dateTime       = new DateTime      ($r["dataProposta"] )       ;
                $dataProposta   = $dateTime->format ("d/m/Y H:i:s"      )       ;
                
                $proposta       = Proposta::model()->findByPk($r["idProposta"]) ;
                
                $valorFinanciado    = "R$ " . number_format($proposta->getValorFinanciado() , 2, ",", ".")  ;
                $valorParcela       = "R$ " . number_format($r["valorParcela"   ]           , 2, ",", ".")  ;
                $valorFinal         = "R$ " . number_format($r["valorFinal"     ]           , 2, ",", ".")  ;
                
                if($r["idModalidade"] == "1")
                {
                    $modalidade = "<b><font color='#cc9'>" . $r["modalidade"] . "</font></b>";
                }
                else
                {
                    $modalidade = "<b><font color='#afaa6d'>" . $r["modalidade"] . "</font></b>";   
                }
                
                if(in_array($r["idProposta"], $marcados))
                {
                    $iClasseSelecionaTudo = "clip-square";
                }
                else
                {
                    $iClasseSelecionaTudo = "clip-checkbox-unchecked-2";
                }

                $check  = "<button class='btn btn-blue'>"
                        . "     <i class='$iClasseSelecionaTudo checkNF' id='iSelecionar' value='" . $r["idProposta"] . "'></i>"
                        . "</button>"    ;
                
                $btnExpande = "";
                
                $anexoContrato  = Anexo::model()->findAll("habilitado AND entidade_relacionamento = 'Proposta' AND ID_entidade_relacionamento = $proposta->id" );
                
                $corLinha = "";

                $notasAnexos = $proposta->nfAnexos();
                
                if(count($anexoContrato) > 0 && ($notasAnexos["qtdNFeAnexo"]> 0 && $notasAnexos["qtdNFe"] == $notasAnexos["qtdNFeAnexo"]))
                {
                    $corLinha = "purple";
                }
                else if(count($anexoContrato) > 0 && ($notasAnexos["qtdNFeAnexo"]> 0 && $notasAnexos["qtdNFe"] !== $notasAnexos["qtdNFeAnexo"]))
                {
                    $corLinha = "dark-green";
                }
                else if (count($anexoContrato) > 0)
                {
                    $corLinha = "red";
                }
                else if($notasAnexos["qtdNFeAnexo"]> 0 && $notasAnexos["qtdNFe"] == $notasAnexos["qtdNFeAnexo"])
                {
                    $corLinha = "blue";
                }
                else if($notasAnexos["qtdNFeAnexo"]> 0 && $notasAnexos["qtdNFe"] !== $notasAnexos["qtdNFeAnexo"])
                {
                    $corLinha = "light-blue";
                }

                $parcelamento = $r["qtdParcelas"     ] . "x "  . $valorParcela;
                
                if(!Empty($corLinha))
                {
                    $modalidade         = "<b><font color='$corLinha'>      $modalidade                                             </font></b>";
                    $codigoProposta     = "<b><font color='$corLinha'>" .   $r["codigoProposta"  ]                              .  "</font></b>";
                    $cpf                = "<b><font color='$corLinha'>" .   $r["cpf"             ]                              .  "</font></b>";
                    $nomeFilial         = "<b><font color='$corLinha'>" .   $proposta->analiseDeCredito->filial->getConcat()    .  "</font></b>";
                    $nomeCliente        = "<b><font color='$corLinha'>" .   $r["nomeCliente"     ]                              .  "</font></b>";
                    $dataProposta       = "<b><font color='$corLinha'>      $dataProposta                                           </font></b>";
                    $valorFinanciado    = "<b><font color='$corLinha'>      $valorFinanciado                                        </font></b>";
                    $parcelamento       = "<b><font color='$corLinha'>      $parcelamento                                           </font></b>";
                    $valorFinal         = "<b><font color='$corLinha'>      $valorFinal                                             </font></b>";
                }
                else
                {
                    $codigoProposta     = $r["codigoProposta"  ]                            ;
                    $cpf                = $r["cpf"             ]                            ;
                    $nomeFilial         = $proposta->analiseDeCredito->filial->getConcat()  ;
                    $nomeCliente        = $r["nomeCliente"     ]                            ;
                }
                
                $dados[] =  [
                                "check"             => $check               ,
                                "modalidade"        => $modalidade          ,
                                "idProposta"        => $r["idProposta"  ]   ,
                                "codigoProposta"    => $codigoProposta      ,
                                "cpf"               => $cpf                 ,
                                "nomeFilial"        => $nomeFilial          ,
                                "nomeCliente"       => $nomeCliente         ,
                                "dataProposta"      => $dataProposta        ,
                                "valorFinanciado"   => $valorFinanciado     ,
                                "parcelamento"      => $parcelamento        ,
                                "valorFinal"        => $valorFinal          ,
                                "btnExpande"        => $btnExpande
                            ];
                
            }
            
        } 
        catch (Exception $ex) 
        {
            $hasErrors  = true                                              ;
            $erro       = "Erro na consulta. Erro: " . $ex->getMessage()    ;
        }
        
        $retorno =  [
                        "data"              => $dados           ,
                        "recordsFiltered"   => $recordsFiltered ,
                        "recordsTotal"      => $recordsTotal    ,
                        "erro"              => $erro            ,
                        "sql"               => $sql             ,
                        "hasErrors"         => $hasErrors       ,
                        "marcados"          => $marcados
                    ];
        
        echo json_encode($retorno);
        
    }
    
    public function actionAnexarDanfe()
    {
        
        $retorno = [];
        
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && isset($_FILES["anexoDanfe"]) && $_FILES["anexoDanfe"]["error"] == UPLOAD_ERR_OK && !empty($_POST['idNF'])) 
        {
            
            $notaFiscal = NotaFiscal::model()->find("habilitado AND id = " . $_POST["idNF"]);
            
            if($notaFiscal !== null)
            {
                $retorno = Anexo::model()->anexar($_FILES["anexoDanfe"], "NotaFiscal", $notaFiscal->id, $notaFiscal->serie . " " . $notaFiscal->documento);
            }
            else
            {
                
                $retorno =  [
                                "hasErrors"     => 1                                ,
                                "msg"           => "Nota Fiscal não encontrada."    ,
                                "pntfyClass"    => "error"                          ,
                            ];
            }
            
        }
        else
        {
                
            $retorno =  [
                            "hasErrors"     => 1                    ,
                            "msg"           => "Faltou parâmetros." ,
                            "pntfyClass"    => "error"              ,
                        ];
            
        }
        
        echo json_encode($retorno);
        
    }
    
}
