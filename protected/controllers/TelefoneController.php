<?php

class TelefoneController extends Controller{

	public function actionIndex(){
		$this->render('index');
	}

	public function actionUpdate(){

		$action 	= $_POST['controller_action'];
		$msgReturn 	= "";

		if( $action == 'add' )
		{
			Telefone::model()->novo($_POST['TelefoneCliente'],$_POST['Cliente_id']);
			$msgReturn = "Telefone cadastrado com sucesso!";
		}
		else
		{
			Telefone::model()->atualizar($_POST['TelefoneCliente'], $_POST['telefone_id']);
			$msgReturn = "Telefone atualizado com sucesso!";
		}

		echo json_encode(array(
			'msgReturn' => $msgReturn
		));
	}
	
	public function actionLoad(){

		$id 		= $_GET['entity_id'];

		echo json_encode( Telefone::model()->loadFormEdit($id) );
	}

	public function actionPersist()
	{
		echo json_encode(Telefone::model()->persist($_POST['TelefoneCliente'],$_POST['TelefoneCliente2'],$_POST['TelefoneCliente3'],$_POST['clienteId'], $_POST['telefone1_id'], $_POST['telefone2_id'], $_POST['telefone3_id'], $_POST['ControllerAction']));
		//echo json_encode($_POST);
	}
}