<?php

class ConciliacaoController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'listar','importarArquivo'),
                'users' => array('@'),
                'expression' => 'Yii::app()->session["usuario"]->role == "empresa_admin"'
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {

        $this->layout = '//layouts/main_sigac_template';

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css');
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpalette.css');
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/conciliacao/fn-gridConciliacao.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpalette.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);

        $this->render('index');
    }

    public function actionListar() {

        $dataRetorno    = []                ;

        $start          = $_POST['start']   ;

        $queryFiltro    = ""                ;

        $vlrTotalOmni   = 0                 ;
        $vlrTotalSigac  = 0                 ;

        $vlrFiltroOmni  = 0                 ;
        $vlrFiltroSigac = 0                 ;
        
        $filtroDataDe   = date('Y-m-d',strtotime('-1 days'));
        $filtroDataAte  = date('Y-m-d',strtotime('-1 days'));
        
        $query1 = "";
        $query2 = "";
        $query3 = "";

        $query = "   SELECT  COUNT(*) AS QTD, 
                                SUM(IC.valorProposta) AS VLROMNI,
                                
                                SUM(CASE WHEN P.segurada    THEN (  (Pa.valor) *1.1)
                                                            ELSE    (Pa.valor)
                                                                END) AS VLRTOTAL
                                                                
                        FROM        Proposta                AS   P 
                        LEFT  JOIN  ItemConciliacao         AS  IC ON  IC.habilitado AND   P.id           =  IC.Proposta_id
                        LEFT  JOIN  Conciliacao             AS   C ON   C.habilitado AND   C.id           =  IC.Conciliacao_id
                        INNER JOIN  Analise_de_Credito      AS  AC ON  AC.habilitado AND  AC.id           =   P.Analise_de_Credito_id
                        INNER JOIN  Filial                  AS   F ON   F.habilitado AND   F.id           =  AC.Filial_id
                        INNER JOIN  Filial_has_Endereco     AS FhE ON FhE.habilitado AND   F.id           = FhE.Filial_id
                        INNER JOIN  Endereco                AS   E ON   E.habilitado AND   E.id           = FhE.Endereco_id
                        INNER JOIN  Cliente                 AS  Cl ON  Cl.habilitado AND  Cl.id           =  AC.Cliente_id
                        INNER JOIN  Pessoa                  AS  Pe ON  Pe.habilitado AND  Pe.id           =  Cl.Pessoa_id
                        INNER JOIN  Pessoa_has_Documento    AS PhD ON PhD.habilitado AND PhD.Pessoa_id    =  Cl.Pessoa_id
                        INNER JOIN  Documento               AS   D ON   D.habilitado AND   D.id           = PhD.Documento_id          AND
                                                                                           D.Tipo_documento_id  = 1
                        INNER JOIN  Titulo                  AS   T ON   T.habilitado AND   P.id		=   T.Proposta_id           AND
                                                                              T.NaturezaTitulo_id = 2
                        INNER JOIN  Parcela		  AS  Pa ON  Pa.habilitado AND   T.id           =  Pa.Titulo_id
                        INNER JOIN  PropostaOmniConfig      AS POC ON POC.habilitado AND   P.id           = POC.Proposta			  
                        WHERE P.habilitado AND P.Status_Proposta_id IN (2,7) 
                    ";

        $query1     = $query;
        $resultado  = Yii::app()->db->createCommand($query)->queryRow();

        $recordsTotal   = $resultado['QTD'      ];
        $vlrTotalOmni   = $resultado['VLROMNI'  ];
        $vlrTotalSigac  = $resultado['VLRTOTAL' ];

        if (isset($_POST['filtroContrato']) && !empty($_POST['filtroContrato'])) {
            $queryFiltro .= " AND IC.codigoContrato LIKE '%'" . $_POST['filtroContrato'] . "'%'";
        }

        if (isset($_POST['filtroCodigo']) && !empty($_POST['filtroCodigo'])) {
            $queryFiltro .= " AND P.codigo LIKE '%" . $_POST['filtroCodigo'] . "%' ";
        }

        if (isset($_POST['filtroLoja']) && !empty($_POST['filtroLoja'])) {
            $queryFiltro .= " AND CONCAT(F.nome_fantasia,' / ',E.cidade,'-',E.uf) LIKE '%" . $_POST['filtroLoja'] . "%' ";
        }

        if (isset($_POST['filtroDataSigacDe']) && !empty($_POST['filtroDataSigacDe'])) {
            $queryFiltro .= " AND LEFT(P.data_cadastro,10) >= '" . $_POST['filtroDataSigacDe'] . "' ";
        }

        if (isset($_POST['filtroDataSigacAte']) && !empty($_POST['filtroDataSigacAte'])) {
            $queryFiltro .= " AND LEFT(P.data_cadastro,10) <= '" . $_POST['filtroDataSigacAte'] . "' ";
        }

        if (isset($_POST['filtroCGCOmni']) && !empty($_POST['filtroCGCOmni'])) {
            $queryFiltro .= " AND IC.cgcCliente LIKE '%" . $_POST['filtroCGCOmni'] . "%'";
        }

        if (isset($_POST['filtroCGCSigac']) && !empty($_POST['filtroCGCSigac'])) {
            $queryFiltro .= " AND D.numero LIKE '%" . $_POST['filtroCGCSigac'] . "%'";
        }

        if (isset($_POST['filtroNomeOmni']) && !empty($_POST['filtroNomeOmni'])) {
            $queryFiltro .= " AND IC.nomeCliente LIKE '%" . $_POST['filtroNomeOmni'] . "%'";
        }

        if (isset($_POST['filtroNomeSigac']) && !empty($_POST['filtroNomeSigac'])) {
            $queryFiltro .= " AND Pe.nome LIKE '%" . $_POST['filtroNomeSigac'] . "%' ";
        }

        $query .= $queryFiltro;

        $queryDivergencia = "";

        //divergência de CGCs
        if (isset($_POST['filtroDivergencia']) && $_POST['filtroDivergencia'][1] == "1") {
            if (!empty($queryDivergencia)) {
                $queryDivergencia .= "OR ";
            }

            $queryDivergencia .= "TRIM(IC.cgcCliente) <> TRIM(D.numero) ";
        }

        //divergência de nomes
        if (isset($_POST['filtroDivergencia']) && $_POST['filtroDivergencia'][2] == "1") {
            if (!empty($queryDivergencia)) {
                $queryDivergencia .= "OR ";
            }

            $queryDivergencia .= "TRIM(UPPER(IC.nomeCliente)) <> TRIM(UPPER(Pe.nome)) ";
        }

        //divergência de valores
        if (isset($_POST['filtroDivergencia']) && $_POST['filtroDivergencia'][3] == "1") {
            if (!empty($queryDivergencia)) {
                $queryDivergencia .= "OR ";
            }

            $queryDivergencia .= "ROUND(IC.valorProposta,0) <>  CASE    WHEN P.segurada
                                                                        THEN ROUND  ( ((Pa.valor) *1.1), 0 )
                                                                        ELSE ROUND  ( ( Pa.valor)      , 0 )
                                                                END ";
        }

        if (isset($_POST['filtroDivergencia']) && $_POST['filtroDivergencia'][4] == "1") {
            if (!empty($queryDivergencia)) {
                $queryDivergencia .= "OR ";
            }

            $queryDivergencia .= " IC.id IS NULL ";
        }

        if (!empty($queryDivergencia)) {
            $query .= " AND ($queryDivergencia) ";
        }

        $query2 = $query;
        
        $resultado = Yii::app()->db->createCommand($query)->queryRow();

        $recordsFiltered    = $resultado['QTD'      ];
        $vlrFiltroOmni      = $resultado['VLROMNI'  ];
        $vlrFiltroSigac     = $resultado['VLRTOTAL' ];

        $query = "   SELECT  IC.codigoContrato                           , P.codigo          , 
                                AC.Filial_id                                , IC.dataProposta   , 
                                P.data_cadastro             AS dataSigac    , IC.cgcCliente     , 
                                D.numero                    AS cgcSigac     , IC.nomeCliente    ,
                                Pe.nome                     AS nomeSigac    , IC.valorProposta  ,
                                
                                CASE WHEN P.segurada    THEN (  (Pa.valor) *1.1)
                                                        ELSE    (Pa.valor)
                                END AS valorSigac   ,  
                                
                                P.segurada
                        FROM        Proposta                AS   P 
                        LEFT  JOIN  ItemConciliacao         AS  IC ON  IC.habilitado AND   P.id           =  IC.Proposta_id
                        LEFT  JOIN  Conciliacao             AS   C ON   C.habilitado AND   C.id           =  IC.Conciliacao_id
                        INNER JOIN  Analise_de_Credito      AS  AC ON  AC.habilitado AND  AC.id           =   P.Analise_de_Credito_id
                        INNER JOIN  Filial                  AS   F ON   F.habilitado AND   F.id           =  AC.Filial_id
                        INNER JOIN  Filial_has_Endereco     AS FhE ON FhE.habilitado AND   F.id           = FhE.Filial_id
                        INNER JOIN  Endereco                AS   E ON   E.habilitado AND   E.id           = FhE.Endereco_id
                        INNER JOIN  Cliente                 AS  Cl ON  Cl.habilitado AND  Cl.id           =  AC.Cliente_id
                        INNER JOIN  Pessoa                  AS  Pe ON  Pe.habilitado AND  Pe.id           =  Cl.Pessoa_id
                        INNER JOIN  Pessoa_has_Documento    AS PhD ON PhD.habilitado AND PhD.Pessoa_id    =  Cl.Pessoa_id
                        INNER JOIN  Documento               AS   D ON   D.habilitado AND   D.id           = PhD.Documento_id          AND
                                                                                           D.Tipo_documento_id  = 1 
                        INNER JOIN  Titulo                  AS   T ON   T.habilitado AND   P.id		=   T.Proposta_id           AND
                                                                              T.NaturezaTitulo_id = 2
                        INNER JOIN  Parcela		  AS  Pa ON  Pa.habilitado AND   T.id           =  Pa.Titulo_id			  
                        INNER JOIN  PropostaOmniConfig      AS POC ON POC.habilitado AND   P.id           = POC.Proposta			  
                        WHERE P.habilitado AND P.Status_Proposta_id IN (2,7) 
                    ";

        $query .= $queryFiltro;

        if (!empty($queryDivergencia)) {
            $query .= " AND ($queryDivergencia) ";
        }

        $query .= " LIMIT $start, 10 ";

        $query3 = $query;
        
        $itens = Yii::app()->db->createCommand($query)->queryAll();

        foreach ($itens as $itemConciliacao) {

            $filial = Filial::model()->find('habilitado AND id = ' . $itemConciliacao['Filial_id']);

            if ($itemConciliacao['dataProposta'] !== null) {
                $dateTime = new DateTime($itemConciliacao['dataProposta']);
                $dataOmni = $dateTime->format('d/m/Y');
            } else {
                $dataOmni = "";
            }

            $dateTime = new DateTime($itemConciliacao['dataSigac']);
            $dataSigac = $dateTime->format('d/m/Y');

            $nomeOmni = strtoupper($itemConciliacao['nomeCliente']);
            $nomeSigac = strtoupper($itemConciliacao['nomeSigac']);

            $valorOmni = 'R$ ' . number_format($itemConciliacao['valorProposta'], 2, ',', '.');

            $valorSigac = $itemConciliacao['valorSigac'];

            $valorSigac = 'R$ ' . number_format($valorSigac, 2, ',', '.');

            $cor = 'gray';

            $aDatas = '';
            $dDatas = '';
            $aCGCs = '';
            $dCGCs = '';
            $aNomes = '';
            $dNomes = '';
            $aValores = '';
            $dValores = '';

            if (trim($itemConciliacao['cgcCliente']) !== trim($itemConciliacao['cgcSigac'])) {
                $cor = 'red';
                $aCGCs = '<b><i>';
                $dCGCs = '</b></i>';
            }

            if (trim($nomeOmni) !== trim($nomeSigac)) {
                $cor = 'red';
                $aNomes = '<b><i>';
                $dNomes = '</b></i>';
            }

            if (($valorOmni > ($valorSigac + 0.99)) || ($valorOmni < ($valorSigac - 0.99))) {
                $cor = 'red';
                $aValores = '<b><i>';
                $dValores = '</b></i>';
            }

            $contrato = '<font color="' . $cor . '">' . $itemConciliacao['codigoContrato'] . '</font>';
            $codigo = '<font color="' . $cor . '">' . $itemConciliacao['codigo'] . '</font>';
            $loja = '<font color="' . $cor . '">' . $filial->getConcat() . '</font>';

            $dtSigac = $aDatas .
                    '<font color="' . $cor . '">' . $dataSigac .
                    '</font>' .
                    $dCGCs;

            $cgcOmni = $aCGCs .
                    '<font color="' . $cor . '">' . $itemConciliacao['cgcCliente'] .
                    '</font>' .
                    $dCGCs;
            $cgcSigac = $aCGCs .
                    '<font color="' . $cor . '">' . $itemConciliacao['cgcSigac'] .
                    '</font>' .
                    $dCGCs;

            $nOmni = $aNomes .
                    '<font color="' . $cor . '">' . $nomeOmni .
                    '</font>' .
                    $dNomes;
            $nSigac = $aNomes .
                    '<font color="' . $cor . '">' . $nomeSigac .
                    '</font>' .
                    $dNomes;

            $vOmni = $aValores .
                    '<font color="' . $cor . '">' . $valorOmni .
                    '</font>' .
                    $dValores;
            $vSigac = $aValores .
                    '<font color="' . $cor . '">' . $valorSigac .
                    '</font>' .
                    $dValores;

            $dataRetorno[] = [
                'contrato' => $contrato,
                'codigo' => $codigo,
                'loja' => $loja,
                'dataSigac' => $dtSigac,
                'cgcOmni' => $cgcOmni,
                'cgcSigac' => $cgcSigac,
                'nomeOmni' => $nOmni,
                'nomeSigac' => $nSigac,
                'valorOmni' => $vOmni,
                'valorSigac' => $vSigac
            ];
        }

        echo json_encode(
                [
                    'data'              => $dataRetorno                                 ,
                    'recordsFiltered'   => $recordsFiltered                             ,
                    'recordsTotal'      => $recordsTotal                                ,
                    'valorTotalOmni'    => number_format($vlrTotalOmni  , 2, ',', '.')  ,
                    'valorTotalSigac'   => number_format($vlrTotalSigac , 2, ',', '.')  ,
                    'valorFiltroOmni'   => number_format($vlrFiltroOmni , 2, ',', '.')  ,
                    'valorFiltroSigac'  => number_format($vlrFiltroSigac, 2, ',', '.')  ,
                    'filtroDataDe'      => $filtroDataDe                                ,
                    'filtroDataAte'     => $filtroDataAte                               ,
                    'query1'            => $query1                                      ,
                    'query2'            => $query2                                      ,
                    'query3'            => $query3
                ]
        );
    }

    public function actionImportarArquivo() {

        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && isset($_FILES["FileInput"]) && $_FILES["FileInput"]["error"] == UPLOAD_ERR_OK && !empty($_FILES['FileInput']['name']) && !empty($_FILES['FileInput']['tmp_name'])) {
            $retorno = Conciliacao::model()->importarArquivo($_FILES['FileInput']);
            echo json_encode($retorno);
        }
    }

}
