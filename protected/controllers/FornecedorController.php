<?php

class FornecedorController extends Controller {

    public $layout = '//layouts/main_sigac_template';

    public function filters() {
        return array(
            'accessControl',
            'postOnly + delete',
        );
    }

    public function accessRules() {

        return array(
            array('allow',
                'actions' => array(
                    'index',
                    'listar',
                    'salvar'
                ),
                'users' => array('@'),
                'expression' => 'Yii::app()->session["usuario"]->role == "financeiro" || Yii::app()->session["usuario"]->role == "empresa_admin"'
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css');


        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fornecedor/fn-fornecedores-grid.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);

        $this->render('index');
    }
    
    public function actionListar()
    {
        
        $retorno        = [];
        
        $criteria               = new CDbCriteria   ;
        $criteria->condition    = 't.habilitado'    ;
        
        $fornecedores   = Fornecedor::model()->findAll('habilitado');
        
        $recordsTotal       = count($fornecedores);
        
        $criteria->with = ['pessoa'=>['alias'=>'P']];
        
        if(isset($_POST['filtroRazaoSocial']) && !empty($_POST['filtroRazaoSocial']))
        {
            $criteria->condition .= " AND P.razao_social LIKE '%" . $_POST['filtroRazaoSocial'] . "%' ";
        }
        
        if(isset($_POST['filtroNomeFantasia']) && !empty($_POST['filtroNomeFantasia']))
        {
            $criteria->condition .= " AND P.nome LIKE '%" . $_POST['filtroNomeFantasia'] . "%' ";
        }
        
        if(isset($_POST['filtroTipo']) && !empty($_POST['filtroTipo']))
        {
            $criteria->condition .= " AND t.TipoFornecedor_id = " . $_POST['filtroTipo'] . " ";
        }
        
        $fornecedores       = Fornecedor::model()->findAll($criteria)   ;
        $recordsFiltered    = count($fornecedores)                      ;
        
        $criteria->limit        = 10                ;
        $criteria->offset       = $_POST['start']   ;
        
        $fornecedores   = Fornecedor::model()->findAll($criteria);
        
        foreach ($fornecedores as $fornecedor)
        {
            
            $btnRemover = "<button id='btnRemover' class='btn btn-red' value='$fornecedor->id'>"
                        . " <i class='fa fa-trash-o'></i>"
                        . "</button>";
            
            $documento = PessoaHasDocumento::model()->find("habilitado AND Pessoa_id = $fornecedor->Pessoa_id ");
            
            if($documento == null)
            {
                $cgcNumero = "";
            }
            else
            {
                $cgcNumero = $documento->documento->numero;
            }
            
            $retorno[]  =   [
                                'idFornecedor'  => $fornecedor->id                          ,
                                'razaoSocial'   => $fornecedor->pessoa->razao_social        ,
                                'nomeFantasia'  => $fornecedor->pessoa->nome                ,
                                'cgc'           => $cgcNumero                               ,
                                'tipo'          => $fornecedor->tipoFornecedor->descricao   ,
                                'uf'            => $fornecedor->pessoa->naturalidade        ,
                                'cidade'        => $fornecedor->pessoa->naturalidade_cidade ,
                                'btnRemover'    => $btnRemover
                            ];
        }
        
        echo json_encode(
                            [
                                'data'              => $retorno         ,
                                'recordsTotal'      => $recordsTotal    ,
                                'recordsFiltered'   => $recordsFiltered
                            ]
                        );
        
    }
    
    public function actionSalvar()
    {
        
        $retorno =  [
                        'hasErrors'     => 0                                ,
                        'msg'           => 'Fornecedor salvo com Sucesso.'  ,
                        'pntfyClass'    => 'success'
                    ];
        
        if(isset($_POST['razaoSocial']))
        {
            
            $transaction = Yii::app()->db->beginTransaction();
            
            $contato                = new Contato           ;
            $contato->habilitado    = 1                     ;
            $contato->data_cadastro = date('Y-m-d H:i:s')   ;
            
            if(!$contato->save())
            {
                ob_start();
                var_dump($contato->getErrors());
                $result = ob_get_clean();
                
                $retorno['hasErrors'    ] = 1                                       ;
                $retorno['msg'          ] = "Erro ao salvar Fornecedor. " . $result ;
                $retorno['pntfyClass'   ] = "error"                                 ;
                
            }
            else
            {
                
                if(isset($_POST['ramalContato']))
                {
                    $ramal = $_POST['ramalContato'];
                }
                else
                {
                    $ramal = '';
                }
                
                $telefone                   = new Telefone                      ;
                $telefone->data_cadastro    = date('Y-m-d H:i:s')               ;
                $telefone->ramal            = $ramal                            ;
                $telefone->habilitado       = 1                                 ;
                $telefone->numero           = $_POST['numeroContato'        ]   ;
                $telefone->Tipo_Telefone_id = $_POST['tipoTelefoneContato'  ]   ;

                if(!$telefone->save())
                {
                    ob_start();
                    var_dump($telefone->getErrors());
                    $result = ob_get_clean();

                    $retorno['hasErrors'    ] = 1                                       ;
                    $retorno['msg'          ] = "Erro ao salvar Fornecedor. " . $result ;
                    $retorno['pntfyClass'   ] = "error"                                 ;

                }
                else
                {

                    $contatoHasTelefone                 = new ContatoHasTelefone    ;
                    $contatoHasTelefone->habilitado     = 1                         ;
                    $contatoHasTelefone->data_cadastro  = date('Y-m-d H:i:s')       ;
                    $contatoHasTelefone->Telefone_id    = $telefone->id             ;
                    $contatoHasTelefone->Contato_id     = $contato->id              ;
                    
                    if(!$contatoHasTelefone->save())
                    {
                        ob_start();
                        var_dump($contatoHasTelefone->getErrors());
                        $result = ob_get_clean();

                        $retorno['hasErrors'    ] = 1                                       ;
                        $retorno['msg'          ] = "Erro ao salvar Fornecedor. " . $result ;
                        $retorno['pntfyClass'   ] = "error"                                 ;
                    }
                    else
                    {
                                    
                        $cidade = Cidade::model()->findByPk($_POST['cidadeCliente']);
                        
                        $pessoa                         = new Pessoa                    ;
                        $pessoa->nome                   = $_POST['nomeFantasia'     ]   ;
                        $pessoa->razao_social           = $_POST['razaoSocial'      ]   ;
                        $pessoa->nacionalidade          = 'Brasileira'                  ;
                        $pessoa->naturalidade           = $_POST['ufCliente'        ]   ;
                        $pessoa->naturalidade_cidade    = $cidade->id                   ;
                        $pessoa->Contato_id             = $contato->id                  ;
                        $pessoa->Estado_Civil_id        = 1                             ;
                        $pessoa->habilitado             = 1                             ;
                        $pessoa->data_cadastro          = date('Y-m-d H:i:s')           ;
                        
                        if(!$pessoa->save())
                        {
                            ob_start();
                            var_dump($pessoa->getErrors());
                            $result = ob_get_clean();

                            $retorno['hasErrors'    ] = 1                                       ;
                            $retorno['msg'          ] = "Erro ao salvar Fornecedor. " . $result ;
                            $retorno['pntfyClass'   ] = "error"                                 ;
                        }
                        else
                        {
                            $documento                      = new Documento         ;
                            $documento->numero              = $_POST['cgc'      ]   ;
                            $documento->habilitado          = 1                     ;
                            $documento->data_cadastro       = date('Y-m-d H:i:s')   ;
                            $documento->Tipo_documento_id   = $_POST['tipoCGC'  ]   ;

                            if(!$documento->save())
                            {
                                ob_start();
                                var_dump($documento->getErrors());
                                $result = ob_get_clean();

                                $retorno['hasErrors'    ] = 1                                       ;
                                $retorno['msg'          ] = "Erro ao salvar Fornecedor. " . $result ;
                                $retorno['pntfyClass'   ] = "error"                                 ;
                            }
                            else
                            {

                                $pessoaHasDocumento                 = new PessoaHasDocumento    ;
                                $pessoaHasDocumento->Documento_id   = $documento->id            ;
                                $pessoaHasDocumento->Pessoa_id      = $pessoa->id               ;
                                $pessoaHasDocumento->habilitado     = 1                         ;
                                $pessoaHasDocumento->data_cadastro  = date('Y-m-d H:i:s')       ;
                                
                                if(!$pessoaHasDocumento->save())
                                {
                                    ob_start();
                                    var_dump($pessoaHasDocumento->getErrors());
                                    $result = ob_get_clean();

                                    $retorno['hasErrors'    ] = 1                                       ;
                                    $retorno['msg'          ] = "Erro ao salvar Fornecedor. " . $result ;
                                    $retorno['pntfyClass'   ] = "error"                                 ;
                                }
                                else
                                {
                                    
                                    $cidade = Cidade::model()->findByPk($_POST['cidadeEndereco']);
                                    
                                    if(isset($_POST['complementoEndereco']))
                                    {
                                        $complemento = $_POST['complementoEndereco'];
                                    }
                                    else
                                    {
                                        $complemento = '';
                                    }
                                    
                                    $endereco                   = new Endereco;
                                    $endereco->logradouro       = $_POST['logradouroEndereco'   ]   ;
                                    $endereco->bairro           = $_POST['bairroEndereco'       ]   ;
                                    $endereco->cep              = $_POST['cepEndereco'          ]   ;
                                    $endereco->cidade           = $cidade->nome                     ;
                                    $endereco->uf               = $_POST['ufEndereco'           ]   ;
                                    $endereco->complemento      = $complemento                      ;
                                    $endereco->Tipo_Endereco_id = $_POST['tipoEndereco'         ]   ;
                                    $endereco->numero           = $_POST['bairroEndereco'       ]   ;
                                    $endereco->habilitado       = 1                                 ;
                                    $endereco->data_cadastro    = date('Y-m-d H:i:s')               ;
                                    
                                    if(!$endereco->save())
                                    {
                                        ob_start();
                                        var_dump($endereco->getErrors());
                                        $result = ob_get_clean();

                                        $retorno['hasErrors'    ] = 1                                       ;
                                        $retorno['msg'          ] = "Erro ao salvar Fornecedor. " . $result ;
                                        $retorno['pntfyClass'   ] = "error"                                 ;
                                    }
                                    else
                                    {
                                        
                                        $pessoaHasEndereco                  = new PessoaHasEndereco ;
                                        $pessoaHasEndereco->Pessoa_id       = $pessoa->id           ;
                                        $pessoaHasEndereco->Endereco_id     = $endereco->id         ;
                                        $pessoaHasEndereco->habilitado      = 1                     ;
                                        $pessoaHasEndereco->data_cadastro   = date('Y-m-d H:i:s')   ;
                                        
                                        if(!$pessoaHasEndereco->save())
                                        {
                                            ob_start();
                                            var_dump($pessoaHasEndereco->getErrors());
                                            $result = ob_get_clean();

                                            $retorno['hasErrors'    ] = 1                                       ;
                                            $retorno['msg'          ] = "Erro ao salvar Fornecedor. " . $result ;
                                            $retorno['pntfyClass'   ] = "error"                                 ;
                                        }
                                        else
                                        {
                                            
                                            $fornecedor                     = new Fornecedor    ;
                                            $fornecedor->Pessoa_id          = $pessoa->id       ;
                                            $fornecedor->TipoFornecedor_id  = 1                 ;
                                            $fornecedor->habilitado         = 1                 ;
                                            
                                            if(!$fornecedor->save())
                                            {
                                                ob_start();
                                                var_dump($fornecedor->getErrors());
                                                $result = ob_get_clean();

                                                $retorno['hasErrors'    ] = 1                                       ;
                                                $retorno['msg'          ] = "Erro ao salvar Fornecedor. " . $result ;
                                                $retorno['pntfyClass'   ] = "error"                                 ;
                                            }
                                            
                                        }
                                        
                                    }
                                    
                                }

                            }
                            
                        }
                        
                    }
                
                }
                
            }
            
            if($retorno['hasErrors'])
            {
                $transaction->rollBack();
            }
            else
            {
                $transaction->commit();
            }
            
        }
        else
        {
            $retorno['hasErrors'    ] = 1                                                   ;
            $retorno['msg'          ] = 'Verifique se todos os campos estão preenchidos'    ;
            $retorno['pntfyClass'   ] = 'error'                                             ;
        }
        
        echo json_encode($retorno);
        
    }

}
