<?php

class VendaController extends Controller {

    
    public $layout = '//layouts/main_sigac_template';

    
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    
    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('precoItem', 'add', 'cadastrarVenda', 'create', 'update', 'index', 'view', 'delete', 'admin', 'edit', 'teste', 'listFP', 'listProdutos'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionCadastrarVenda() {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');

        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ui-modals.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-grid-venda.js', CClientScript::POS_END);
        //$cs->registerScriptFile($baseUrl . '/js/jquery.maskMoney.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScript('select2', '$("select.search-select").select2()');
        $cs->registerScript('UIModals', 'UIModals.init();');
        //$cs->registerScript('moneypicker', '$(".currency").maskMoney({decimal:",", thousands:"."});');

        $model = new Venda;

        if (isset($_POST['Venda'])) {

            $model->attributes  = $_POST['Venda'];
            $model->data        = date('Y-m-d H:i:s');

            $itemEstoqueId      = $_POST['Item_do_Estoque_id_hdn'];
            $quantidade         = $_POST['quantidade_hdn'];
            $desconto           = $_POST['desconto_hdn'];
            $precoVenda         = $_POST['precoVenda_hdn'];

            $FPId               = $_POST['FP_id_hdn'];
            $parcelas           = $_POST['parcelas_hdn'];
            $valorFP            = $_POST['valorFP_hdn'];

            if ( $model->save() ) {
            
                $codigo         = trim((string) $model->id);

                $model->codigo  = str_pad($codigo, 15, '0', STR_PAD_LEFT);

                if ($model->save()) {

                    if ( isset($_POST['finaliza']) && $_POST['finaliza'] ) {

                        $nf     = new NotaFiscal;

                        //$titulo = new Titulo;

                        $nf->Status_Nota_Fiscal_id = 1;
                        $nf->data = date('Y-m-d H:i:s');
                        $nf->serie = '1';
                        $nf->documento = 'teste1';

                        $nf->save();
                    }

                    for ($i = 0; $i < sizeof($itemEstoqueId); $i++) {

                        $itemVenda = new ItemDaVenda;

                        $itemVenda->Venda_id = $model->id;
                        $itemVenda->Item_do_Estoque_id = $itemEstoqueId[$i];
                        $itemVenda->seq = $i;
                        $itemVenda->quantidade = $quantidade[$i];
                        $itemVenda->desconto = $desconto[$i];
                        $itemVenda->preco = $precoVenda[$i];

                        $itemVenda->save();

                        if (isset($_POST['finaliza']) && $_POST['finaliza']) {
                            $it = new ItemNotaFiscal;

                            $it->Nota_Fiscal_id = $nf->id;

                            $seq = trim((string) ($i + 1));

                            $it->item = str_pad($seq, 4, '0', STR_PAD_LEFT);

                            if ($it->save()) {
                                $itemVenda->Item_Nota_Fiscal_id = $it->id;
                                $itemVenda->efetiva();
                            }
                        }
                    }

                    for ($i = 0; $i < sizeof($FPId); $i++) {

                        $fpHasCp = FormaDePagamentoHasCondicaoDePagamento::model()->findByPk($FPId[$i]);

                        $vFPCP = new VendaHasFormaDePagamentoHasCondicaoDePagamento;

                        $vFPCP->Venda_id = $model->id;
                        $vFPCP->Forma_de_Pagamento_has_Condicao_de_Pagamento_id = $fpHasCp->id;
                        $vFPCP->qtdParcelas = $parcelas[$i];
                        $vFPCP->valor = $valorFP[$i];

                        
                          /*var_dump($vFPCP);
                          echo '<br><br>';
                          var_dump($fpHasCp);
                          
                          echo '<br>';*/
                        

                        //try {
                            if($vFPCP->save()){

                                if (isset($_POST['finaliza']) && $_POST['finaliza']) {
                                    $titulo = new Titulo;
                                    
                                    $titulo->emissao = date('d-m-y');
                                    $titulo->Venda_has_FP_has_CP_FP_has_CP_id = $vFPCP->id;
                                    $titulo->prefixo = "TST";
                                    $titulo->novo($vFPCP);
                                    
                                }
                                
//                                $this->redirect(array('admin'));
                            }else{
                                var_dump($vFPCP->getErrors());
                            }
                        /*} catch (Exception $exc) {
                            echo $exc->getTraceAsString();
                        }*/
                    }
                }

                        //$transaction->commit();
                        //$this->redirect(array('view','id'=>$model->id));
                        //$this->redirect(array('admin'));
            } else {
                var_dump($model);
            }
            
            /*
                } catch (Exception $e) {
                  $transaction->rollBack();
                }
            */
                                
            $this->redirect(array('admin'));
            
        }

        $this->render('cadastrarVenda', array(
            'model' => $model,
        ));
    }

    public function actionListFP() {

        $params = [];
        $postId = [];
        $postPar = [];


        if (!empty($_POST) && $_POST['formPGs'] !== null && !empty($_POST['formPGs'])) {

            $postId = $_POST['formPGs'];
            $postPar = $_POST['parcelas'];
        }

        for ($i = 0; $i < sizeof($postId); $i++) {
            $params[] = array(
                'formPGs' => $postId[$i],
                'parcelas' => $postPar[$i]
            );
        }



        $retorno = FormaDePagamentoHasCondicaoDePagamento::model()->searchUnique($params);



        echo json_encode($retorno);
    }

    public function actionListProdutos() {

        $retorno = null;
        
        foreach(ItemDoEstoque::model()->findAll(array("condition" => "saldo > 0")) as $item)
        {
            $retorno[] = array('id' => $item->id,'text' => $item->produto->descricao);
        }
        
        echo json_encode($retorno);
    }
    
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionCreate() {

        $model = new Venda;

        if (isset($_POST['Venda'])) {
            $model->attributes = $_POST['Venda'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id) {

        $model = $this->loadModel($id);

        if (isset($_POST['Venda'])) {
            $model->attributes = $_POST['Venda'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }
    
    public function actionDelete($id) {

        $this->loadModel($id)->delete();

        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }
    
    public function actionIndex() {

        $dataProvider = new CActiveDataProvider('Venda');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }
    
    public function actionTeste()
    {
        echo date('Y-m-d', strtotime("+10 days"));
    }

    public function actionAdmin() {
        $model = new Venda('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Venda']))
            $model->attributes = $_GET['Venda'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    
    public function loadModel($id) {
        $model = Venda::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'venda-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
