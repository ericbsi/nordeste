<?php

class FabricanteController extends Controller
{	

	public $layout = '//layouts/main_sigac_template';
	
	public function actionIndex()
	{	

		$baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl.'/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl.'/css/sigax_template/dataTables.tableTools.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js');
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js');
        $cs->registerScriptFile($baseUrl . '/js/btn-status-fn.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/select2.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-fabricantes-index.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.validate.12.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);

        $cs->registerScript('CNPJMask', 'jQuery(function($){$("#cnpj").mask("99.999.999/9999-99");});');

		$this->render('index');
	}

	public function actionEditar()
	{

		$fabID 	= $_POST['fabricante_id'];

	}

	public function actionSearchFabricantes(){
		echo json_encode( Fabricante::model()->searchFabricantesSelec2( $_POST['q'] ) );
	}

	public function actionGetFabricantes(){

		echo json_encode( Fabricante::model()->listFabricantes( $_POST['draw'], $_POST['start'] ) );

	}

	public function actionAdd(){
		echo json_encode( Fabricante::model()->novo( $_POST['Fabricante'] ) );
		
	}
}