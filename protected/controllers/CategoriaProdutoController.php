<?php

class CategoriaProdutoController extends Controller
{	
	
	public $layout = '//layouts/main_sigac_template';

	public function actionIndex()
	{

		$baseUrl = Yii::app()->baseUrl;
	    $cs = Yii::app()->getClientScript();


		$cs->registerCssFile($baseUrl.'/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl.'/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl.'/css/sigax_template/datepicker.css');

	    $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl.'/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        
        $cs->registerScriptFile($baseUrl.'/js/jquery.blockUI.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.validate.12.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.maskedinput.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.pnotify.min.js',CClientScript::POS_END);
		$cs->registerScriptFile($baseUrl.'/js/select2.min.js',  CClientScript::POS_END);
		$cs->registerScriptFile($baseUrl.'/js/bootstrap-modal.js',CClientScript::POS_END);
  	    $cs->registerScriptFile($baseUrl.'/js/bootstrap-modalmanager.js',CClientScript::POS_END);
	    $cs->registerScriptFile($baseUrl.'/js/jquery.form.js',  CClientScript::POS_END);

	    $cs->registerScriptFile($baseUrl.'/js/fn-categorias-produtos-table.js',  CClientScript::POS_END);

		$this->render('index');
	}

	public function actionGetCategorias()
	{
		echo json_encode( CategoriaProduto::model()->listarCategorias($_POST['draw'], $_POST['start'], $_POST['order'], $_POST['columns']) );
	}

	public function actionListCategorias(){

		echo json_encode(CategoriaProduto::model()->listCategorias());
	}

	public function actionAdd(){

		echo json_encode( CategoriaProduto::model()->add($_POST['CategoriaProduto']) );	
	}

	public function actionSearchCategorias(){

		echo json_encode( CategoriaProduto::model()->searchCategoriasSelec2( $_POST['q'], $_POST['showPai'] ) );
	}
}