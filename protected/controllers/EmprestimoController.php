<?php

class EmprestimoController extends Controller {

    public $layout = '//layouts/main_sigac_template';

    public function filters() {

        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules() {

        return array(
            array('allow',
                'actions' => array(
                    'pagarEmprestimo',
                    'liberarPagamento',
                    'aprovarLiberacao',
                    'gridEmprestimos',
                    'gridLiberarPagamento',
                    'getDadosBancarios',
                    'anexarComprovante',
                    'excluirContratoAnexo',
                    'pagar',
                    'estornar',
                    'retornarLiberar'
                ),
                'users' => array(
                    '@'
                ),
                'expression' => 'Yii::app()->session["usuario"]->role == "financeiro"    ||  '
                . 'Yii::app()->session["usuario"]->role == "empresa_admin"     '
            ),
            array('allow',
                'actions' => array(
                    'getDadosBancariosPagos',
                    'getPropostas',
                ),
                'users' => array('@'),
                'expression' => 'Yii::app()->session["usuario"]->role == "financeiro"       ||  '
                . 'Yii::app()->session["usuario"]->role == "empresa_admin"    ||  '
                . 'Yii::app()->session["usuario"]->role == "parceiros_admin"      '
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionPagarEmprestimo() {
        $this->layout = '//layouts/main_sigac_template';
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css');
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpalette.css');
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/financeiro/emprestimo/fn-gridEmprestimo.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpalette.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js', CClientScript::POS_END);

//        Bordero::model()->ajustarTitulosPagar();

        $this->render('pagarEmprestimo');
    }

    public function actionLiberarPagamento() {
        $this->layout = '//layouts/main_sigac_template';
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css');
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpalette.css');
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/financeiro/emprestimo/fn-gridLiberarPagamento.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpalette.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js', CClientScript::POS_END);

//        Bordero::model()->ajustarTitulosPagar();

        $this->render('liberarPagamento');
    }

    public function actionGetPropostas() {

        $idBordero = 0;

        $dados = [];

        if (isset($_POST['idBordero']) && !empty($_POST['idBordero'])) {
            $idBordero = $_POST['idBordero'];
        }

        $Qry = " SELECT P.id ";
        $Qry .= " FROM          Proposta            as P                                                            ";
        $Qry .= " INNER JOIN    Analise_de_Credito  as AC ON AC.habilitado  AND AC.id =  P.Analise_de_Credito_id    ";
        $Qry .= " INNER JOIN    ItemDoBordero       as IB ON                     P.id = IB.Proposta_id              ";
        $Qry .= " INNER JOIN    Bordero             as B  ON  B.habilitado  AND  B.id = IB.Bordero                  ";
        $Qry .= " WHERE P.habilitado AND B.id = $idBordero ";
//        $Qry .= " WHERE P.habilitado AND B.id = $idBordero ";

        $resultQuery = Yii::app()->db->createCommand($Qry)->queryAll();

        foreach ($resultQuery as $r) {

            $proposta = Proposta::model()->findByPk($r['id']);

            if ($proposta !== null) {

                $data = new DateTime($proposta->data_cadastro);
                $dataProposta = $data->format('d/m/Y H:i:s');

                $dados[] = [
                    'dataProposta' => $dataProposta,
                    'codigo' => $proposta->codigo,
                    'filial' => $proposta->analiseDeCredito->filial->getConcat(),
                    'cliente' => strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome),
                    'inicial' => 'R$ ' . number_format($proposta->valor, 2, ',', '.'),
                    'entrada' => 'R$ ' . number_format($proposta->valor_entrada, 2, ',', '.'),
                    'carencia' => $proposta->carencia . ' dias',
                    'financiado' => 'R$ ' . number_format($proposta->valor - $proposta->valor_entrada, 2, ',', '.'),
                    'repasse' => 'R$ ' . number_format($proposta->valorRepasse(), 2, ',', '.'),
                    'parcelas' => $proposta->qtd_parcelas . 'x',
                ];
            }
        }

        echo json_encode(
                [
                    'data' => $dados,
                    'recordsTotal' => count($dados),
                    'recordsFiltered' => count($dados),
                ]
        );
    }

    public function actionGridLiberarPagamento() {

        $codigoNatureza = "2999999";
        $sql = "";
        $erroQuery = "";

        $natureza = NaturezaTitulo::model()->find("habilitado AND codigo = '$codigoNatureza'");

        $dados = [];
        $retAnexos = ["contrato" => [], "proposta" => []];

        $recordsTotal = 0;
        $recordsFiltered = 0;

        $idAnexo = NULL;
        $anexos = NULL;
        
        if(isset($_POST['semear'])){
            if($_POST['semear'] == "true"){
                $semear = " AND Pr.Financeira_id = 7 ";
            }else{
                $semear = " AND Pr.Financeira_id != 7 ";
            }
        }else{
            $semear = " AND Pr.Financeira_id != 7 ";
        }
        
        if ($natureza !== null) {

            $sql = "
                        SELECT 
                                 P.id               AS idParcela        , 
                                 C.id               AS idCliente        , 
                                Pr.id               AS idProposta       , 
                                DB.id               AS idDB             , 
                                Pr.codigo           AS codigoProposta   , 
                                Pr.data_cadastro    AS dataProposta     , 
                                Pe.nome             AS nomeCliente      , 
                                DB.Banco_id         AS idBanco          , 
                                DB.agencia          AS agencia          , 
                                DB.numero           AS numero           , 
                                DB.operacao         AS operacao         , 
                                 P.valor            AS valorParcela 
                        FROM            Parcela                     AS P
                        INNER   JOIN    Titulo                      AS T    ON  T.id =   P.Titulo_id                AND   T.habilitado
                        INNER   JOIN    Proposta                    AS Pr   ON Pr.id =   T.Proposta_id              AND  Pr.habilitado
                        INNER   JOIN    Analise_de_Credito          AS AC   ON AC.id =  Pr.Analise_de_Credito_id    AND  AC.habilitado
                        INNER   JOIN    Cliente                     AS C    ON  C.id =  AC.Cliente_id               AND   C.habilitado
                        INNER   JOIN    Pessoa                      AS Pe   ON Pe.id =   C.Pessoa_id                AND  Pe.habilitado
                        LEFT    JOIN    Pessoa_has_Dados_Bancarios  AS PhD  ON Pe.id = PhD.Pessoa_id
                        LEFT    JOIN    Dados_Bancarios             AS DB   ON DB.id = PhD.Dados_Bancarios_id       AND  DB.habilitado
                        LEFT    JOIN    Bordero_has_Parcela         AS BhP  ON  P.id = BhP.Parcela_id               AND BhP.habilitado
                        LEFT    JOIN    Bordero                     AS B    ON  B.id = BhP.Bordero_id               AND   B.habilitado AND B.Status <> 3 
                        WHERE P.habilitado AND T.NaturezaTitulo_id = $natureza->id AND B.id IS NULL " .$semear. " 

                      ";

            try {
                $resultado = Yii::app()->db->createCommand($sql)->queryAll();
            } catch (Exception $e) {
                $resultado = [];
                $erroQuery = "Erro: " . $e->getMessage();
            }

            $recordsTotal = count($resultado);
            $recordsFiltered = count($resultado);

            foreach ($resultado as $linha) {

                $retAnexos = ["contrato" => [], "proposta" => []];

                $linkContrato = "";

                $emprestimo = Emprestimo::model()->find("habilitado AND Proposta_id = " . $linha["idProposta"]);

                if ($emprestimo !== null) {
                    $anexos = Anexo::model()->findAll("habilitado AND entidade_relacionamento = 'Emprestimo' AND id_entidade_relacionamento = $emprestimo->id");

                    foreach ($anexos as $anexo) {

                        $url = '';

                        if( $anexo->url == null  ){
                            $url = $anexo->relative_path;
                        }
                        else{
                            $url = $anexo->url;
                        }

                        $retAnexos["contrato"][] = [
                            "id" => $anexo->id,
                            "relative_path" => $url
                        ];
                    }
                }

                $cadastro = Cadastro::model()->find('habilitado AND Cliente_id = ' . $linha["idCliente"]);

                if ($cadastro != NULL) {

                    $anexos = Anexo::model()->findAll('entidade_relacionamento = "cadastro" AND id_entidade_relacionamento = ' . $cadastro->id);

                    foreach ($anexos as $anexo) {

                        $url = '';

                        if( $anexo->url == null  ){
                            $url = $anexo->relative_path;
                        }
                        else{
                            $url = $anexo->url;
                        }

                        $retAnexos["proposta"][] = [
                            "id" => $anexo->id,
                            "relative_path" => $url
                        ];
                    }
                }

                $dateTime = new DateTime($linha["dataProposta"]);
                $dataProposta = $dateTime->format("d/m/Y H:i:s");

                $valorParcela = "R$ " . number_format($linha["valorParcela"], 2, ".", ",");

                $banco = Banco::model()->findByPk($linha["idBanco"]);

                $disabled = "";
                $tituloBtn = "Liberar";

                if ($banco == null) {
                    $disabled = "disabled";
                    $tituloBtn = "Dados Bancários do Cliente não encontrado. Favor, verificar o cadastro do mesmo.";
                    $dadosBancarios = "<font color='red'>$tituloBtn</font>";
                } else {

                    $nomeBanco = $banco->codigo . " - " . $banco->nome;
                    $dadosBancarios = "Banco: $nomeBanco | Agência: " . $linha["agencia"] . " | Conta: " . $linha["numero"] . " | Operação: " . $linha["operacao"];
                }

                $cliente = Cliente::model()->find("habilitado AND id = " . $linha['idCliente']);
                $cpf = $cliente->pessoa->getCPF()->numero;

                $proposta = Proposta::model()->find("habilitado AND id = " . $linha["idProposta"]);
                $nomeCrediarista = $proposta->analiseDeCredito->usuario->nome_utilizador;
                $nomeLoja = $proposta->emprestimo->filial->getConcat();

                $solicitacao = SolicitacaoDeCancelamento::model()->find("habilitado AND Proposta_id = $proposta->id");

                if ($solicitacao !== null) {

                    if ($solicitacao->aceite) {
                        $recordsTotal --;
                        $recordsFiltered --;

                        continue;
                    } else {
                        $disabled = "disabled";
                        $tituloBtn = "Solicitação de Cancelamento pendente...";
                    }
                }

                $btnAprovar = "<button class='btn btn-green btnAprovar' $disabled value='" . $linha["idParcela"] . "' " . (count($anexos) > 0 ? "" : "disabled") . ">"
                        . "     <span title='$tituloBtn'>"
                        . "         <i class='fa fa-thumbs-o-up'></i>"
                        . "     </span>"
                        . "</button>";

                $dados[] = [
                    "idParcela" => $linha["idParcela"],
//                                "idAnexo"           => $idAnexo                     ,
                    "anexos" => $retAnexos,
                    "proposta" => $linha["codigoProposta"],
                    "dataProposta" => $dataProposta,
                    "nomeLoja" => $nomeLoja,
                    "nomeCrediarista" => $nomeCrediarista,
                    "cliente" => $linha["nomeCliente"],
                    "cpf" => $cpf,
                    "dadosBancarios" => $dadosBancarios,
                    "valor" => $valorParcela,
                    "valorNumero" => $linha["valorParcela"],
                    "btnAprovar" => $btnAprovar,
//                                "linkContrato"      => $linkContrato                ,
                ];
            }
        }

        echo json_encode(
                [
                    "data" => $dados,
                    "recordsFiltered" => $recordsFiltered,
                    "recordsTotal" => $recordsTotal,
                    "teste" => $sql,
                    "hasErrors" => (!empty($erroQuery) ? true : false),
                    "erro" => $erroQuery
                ]
        );
    }

    public function actionGridEmprestimos() {
        $url = '';
        $codigoNatureza = "2999999";
        $sql = "";
        $erroQuery = "";

        $natureza = NaturezaTitulo::model()->find("habilitado AND codigo = '$codigoNatureza'");

        $dados = [];

        $recordsTotal = 0;
        $recordsFiltered = 0;

        $spanAlerta = "";

        if ($natureza !== null) {

            $sql = "
                        SELECT 
                                        P.id                        AS idParcela        , 
                                       Pr.id                        AS idProposta       , 
                                       DB.id                        AS idDB             , 
                                       Pr.codigo                    AS codigoProposta   , 
                                       Pr.data_cadastro             AS dataProposta     , 
                                       UPPER(Pe.nome)               AS nomeCliente      , 
                                       DB.Banco_id                  AS idBanco          , 
                                       DB.agencia                   AS agencia          , 
                                       DB.numero                    AS numero           , 
                                       DB.operacao                  AS operacao         , 
                                        P.valor                     AS valorParcela     , 
                                       Bo.dataCriacao               AS dataLiberacao    ,
                                IFNULL( B.data_da_baixa     , '' )  AS dataBaixa        ,
                                IFNULL( B.id                , '' )  AS idBaixa          ,
                                IFNULL(DP.comprovante       , '' )  AS comprovante      ,
                                IFNULL(DP.id                , '' )  AS idDP             ,
                            MAX(IFNULL(EBS.StatusEstorno_id , 0  )) AS maximoStatus
                        FROM            Parcela                         AS P
                        INNER   JOIN    Titulo                          AS T    ON  T.id =   P.Titulo_id                AND   T.habilitado
                        INNER   JOIN    Proposta                        AS Pr   ON Pr.id =   T.Proposta_id              AND  Pr.habilitado
                        INNER   JOIN    Analise_de_Credito              AS AC   ON AC.id =  Pr.Analise_de_Credito_id    AND  AC.habilitado
                        INNER   JOIN    Cliente                         AS C    ON  C.id =  AC.Cliente_id               AND   C.habilitado
                        INNER   JOIN    Pessoa                          AS Pe   ON Pe.id =   C.Pessoa_id                AND  Pe.habilitado
                        INNER   JOIN    Pessoa_has_Dados_Bancarios      AS PhD  ON Pe.id = PhD.Pessoa_id                AND PhD.habilitado
                        INNER   JOIN    Dados_Bancarios                 AS DB   ON DB.id = PhD.Dados_Bancarios_id       AND  DB.habilitado
                        INNER   JOIN    Bordero_has_Parcela             AS BhP  ON  P.id = BhP.Parcela_id               AND BhP.habilitado
                        INNER   JOIN    Bordero                         AS Bo   ON Bo.id = BhP.Bordero_id               AND  Bo.habilitado
                        LEFT    JOIN    Baixa                           AS B    ON  P.id =   B.Parcela_id               AND   B.baixado
                        LEFT    JOIN    DadosPagamento                  AS DP   ON DP.id =   B.DadosPagamento_id        AND  DP.habilitado
                        LEFT    JOIN    EstornoBaixa                    AS EB   ON  B.id =  EB.Baixa_id                 AND  EB.habilitado
                        LEFT    JOIN    EstornoBaixa_has_StatusEstorno  AS EBS  ON EB.id = EBS.EstornoBaixa_id          AND EBS.habilitado 
                        WHERE Pr.Financeira_id != 7 AND P.habilitado AND T.NaturezaTitulo_id = $natureza->id 

                      ";

            $btnEstornar = "";

            if ($_POST["pago"] == "1") {
                $sql .= " AND Bo.Status = 4 AND B.id IS NOT NULL";
            } else {
                $sql .= " AND Bo.Status = 1 AND B.id IS NULL ";
            }

            $sql .= "   GROUP BY    P.id    , Pr.id             , DB.id             , Pr.codigo , Pr.data_cadastro  , 
                                    Pe.nome , DB.Banco_id       , DB.agencia        , DB.numero , DB.operacao       , 
                                    P.valor , Bo.dataCriacao    , B.data_da_baixa   , B.id      , DP.comprovante    , 
                                    DP.id
                                
                        HAVING MAX(IFNULL(EBS.StatusEstorno_id , 0  )) <> 1
                                ";

            try {
                $resultado = Yii::app()->db->createCommand($sql)->queryAll();
                $recordsFiltered = count($resultado);
            } catch (Exception $e) {
                $resultado = [];
                $erroQuery = "Erro: " . $e->getMessage();
            }

            $sql .= " LIMIT " . $_POST['start'] . ", 10";

            try {
                $resultado = Yii::app()->db->createCommand($sql)->queryAll();
                $recordsTotal = count($resultado);
            } catch (Exception $e) {
                $resultado = [];
                $erroQuery = "Erro: " . $e->getMessage();
            }

            foreach ($resultado as $linha) {

                $anexo = "";

                $dateTime = new DateTime($linha["dataProposta"]);
                $dataProposta = $dateTime->format("d/m/Y H:i:s");

                $dateTime = new DateTime($linha["dataLiberacao"]);
                $dataLiberacao = $dateTime->format("d/m/Y H:i:s");

                $dateTime = new DateTime($linha["dataBaixa"]);
                $dataBaixa = $dateTime->format("d/m/Y H:i:s");

                $valorParcela = "R$ " . number_format($linha["valorParcela"], 2, ",", ".");

                $banco = Banco::model()->findByPk($linha["idBanco"]);

                if ($_POST["pago"] == "1") {

                    $url = '';

                    $anexo = Anexo::model()->find("habilitado AND entidade_relacionamento = 'DadosPagamento' AND id_entidade_relacionamento = " . $linha["idDP"]);

                    if( $anexo->url != NULL ){
                        $url = $anexo->url;
                    }
                    else{
                        $url = $anexo->relative_path;
                    }

                    $btnEstornar = "<button class='btn btn-red btnEstorno' value='" . $linha["idBaixa"] . "'><i class='clip-cancel-circle-2'></i></button>";
                } else {
                    $btnEstornar = "<button class='btn btn-red btnDesfaz'  value='" . $linha["idParcela"] . "'><i class='clip-cancel-circle-2'></i></button>";
                }


                if ($banco == null) {
                    $nomeBanco = "Banco não encontrado";
                } else {
                    $nomeBanco = $banco->codigo . " - " . $banco->nome;
                }
                
                if(strpos($linha["agencia"], '-')){
                    $agencia = str_pad(trim(str_replace(".", "", $linha["agencia"])), 8, "0", STR_PAD_LEFT);
                }else{
                    $agencia = str_pad(trim(str_replace(".", "", $linha["agencia"])), 6, "0", STR_PAD_LEFT);
                }
                
                if(strpos($linha["numero"], '-')){
                    $numero = str_pad(trim(str_replace(".", "", $linha["numero"])), 12, "0", STR_PAD_LEFT);
                }else{
                    $numero = str_pad(trim(str_replace(".", "", $linha["numero"])), 10, "0", STR_PAD_LEFT);
                }
                
                if($linha['operacao'] != ''){
                    $operacao = str_pad(trim(str_replace(".", "", $linha["operacao"])), 3, "0", STR_PAD_LEFT);
                }else{
                    $operacao = ' ';
                }
                
                $dadosBancarios = "Banco: $nomeBanco | Agência: " . $agencia . " | Conta: " . $numero . " | Operação: " . $operacao;

                $proposta = Proposta::model()->find("habilitado AND id = " . $linha["idProposta"]);

                $spanAlerta = "";
                
                if ($proposta !== null) {
                    $cpfCliente = $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero;

                    $solicitacao = SolicitacaoDeCancelamento::model()->find("habilitado AND Proposta_id = $proposta->id");

                    if ($solicitacao !== null) {

                        if ($solicitacao->aceite) {
                            $spanAlerta = "<span class='label buttonpulsate'>Cancelada</span>";
                        } else if (!isset($solicitacao->data_finalizacao) && $solicitacao->data_finalizacao !== null) {
                            $spanAlerta = "<span class='label buttonpulsate'>Solicitação de Cancelamento</span>";
                        }
                    }
                } else {
                    $cpfCliente = "";
                }

                if ($linha['maximoStatus'] != 0) {
                    $spanAlerta = "<span class='label buttonpulsate'>Extorno</span>";
                }

                $dados[] = [
                    "idParcela" => $linha["idParcela"],
                    "idDB" => $linha["idDB"],
                    "idBaixa" => $linha["idBaixa"],
                    "dataBaixa" => $linha["dataBaixa"],
                    "proposta" => $linha["codigoProposta"],
                    "comprovantePG" => $linha["comprovante"],
                    "dataProposta" => $dataProposta,
                    "dataLiberacao" => $dataLiberacao,
                    "cliente" => $linha["nomeCliente"],
                    'cpf' => $cpfCliente,
                    "dadosBancarios" => $dadosBancarios,
                    "valor" => $valorParcela,
                    "valorNumero" => $linha["valorParcela"],
                    "alerta" => $spanAlerta,
                    "btnEstornar" => $btnEstornar,
                    "anexo" => $url
                ];
            }
        }

        echo json_encode(
                [
                    "data" => $dados,
                    "recordsFiltered" => $recordsFiltered,
                    "recordsTotal" => $recordsTotal,
                    "teste" => $sql . "\n Erro: $erroQuery"
                ]
        );
    }

    public function actionGetDadosBancarios() {

        $parcela = Parcela::model()->find('habilitado AND id = ' . $_POST["idParcela"]);

        $options = array(
            'empresa' => '<option value="0">Selecione uma Conta</option>',
            'cliente' => '<option value="0">Selecione uma Conta</option>',
            'dataBaixa' => date('Y-m-d')
        );

        if ($parcela !== null) {

            $empresaDB = Empresa::model()->getDadosBancarios();

            foreach ($empresaDB as $edb) {
                $descricao = 'Banco: ' . $edb['codBanco'] . '-' . $edb['nomeBanco'] . ' ';
                $descricao .= 'Ag.: ' . $edb['agencia'] . ' ';
                $descricao .= 'CC: ' . $edb['numero'];
                /* DB.agencia					, DB.numero				, DB.operacao */

                $options['empresa'] .= '<option value="' . $edb['id'] . '">' . $descricao . '</option>';
            }

            $dadosBancarios = DadosBancarios::model()->find('habilitado AND id = ' . $_POST["idDB"]);

            if (
                    (!$dadosBancarios !== null && $dadosBancarios->Banco_id !== null ) &&
                    (!$dadosBancarios->agencia !== null && !empty($dadosBancarios->agencia) ) &&
                    (!$dadosBancarios->numero !== null && !empty($dadosBancarios->numero) )
            ) {

                $descricao = 'Banco: ' . $dadosBancarios->banco->codigo . '-' . $dadosBancarios->banco->nome . ' ';
                $descricao .= 'Ag.: ' . $dadosBancarios->agencia . ' ';
                $descricao .= 'CC: ' . $dadosBancarios->numero;

                $options['cliente'] .= '<option value="' . $dadosBancarios->id . '">' . $descricao . '</option>';
            }
        }

        echo json_encode($options);
    }

    public function actionGetDadosBancariosPagos() {

        $parcela = Parcela::model()->find('habilitado AND id = ' . $_POST["idParcela"]);

        $options = array(
            'empresa' => '',
            'cliente' => '',
            'dataBaixa' => date('Y-m-d'),
            'valor' => '0'
        );

        if ($parcela !== null) {

            $baixa = Baixa::model()->find("baixado AND Parcela_id = $parcela->id");

            if ($baixa == null) {
                
            } else {

                $dateTime = new DateTime($baixa->data_da_baixa);

                $options['dataBaixa'] = $dateTime->format('d/m/Y');
                $options['valorBaixa'] = 'R$' . number_format($baixa->valor_pago, 2, ',', '.');


                $dadosPagamento = DadosPagamento::model()->find("habilitado AND id = $baixa->DadosPagamento_id");

                if ($dadosPagamento == null) {
                    
                } else {

                    $options['comprovante'] = $dadosPagamento->comprovante;
                    $options['observacao'] = $dadosPagamento->observacao;

                    $edb = DadosBancarios::model()->find("habilitado AND id = $dadosPagamento->Dados_Bancarios_Debito");

                    if ($edb == null) {
                        
                    } else {

                        $descricao = 'Banco: ' . $edb->banco->codigo . '-' . $edb->banco->nome . ' ';
                        $descricao .= 'Ag.: ' . $edb->agencia . ' ';
                        $descricao .= 'CC: ' . $edb->numero;

                        $options['empresa'] = $descricao;
                    }

                    $dadosBancarios = DadosBancarios::model()->find("habilitado AND id = $dadosPagamento->Dados_Bancarios_Credito");

                    if ($dadosBancarios == null) {
                        
                    } else {

                        $descricao = 'Banco: ' . $dadosBancarios->banco->codigo . '-' . $dadosBancarios->banco->nome . ' ';
                        $descricao .= 'Ag.: ' . $dadosBancarios->agencia . ' ';
                        $descricao .= 'CC: ' . $dadosBancarios->numero;

                        $options['cliente'] = $descricao;
                    }
                }
            }
        }

        echo json_encode($options);
    }

    public function actionRetornarLiberar() {

        if (isset($_POST["idParcela"]) && !empty($_POST["idParcela"])) {

            $idParcela = $_POST["idParcela"];

            $parcela = Parcela::model()->find("habilitado AND id = $idParcela");

            if ($parcela !== null) {

                $borderoHasParcela = BorderoHasParcela::model()->find("habilitado AND Parcela_id = $parcela->id");

                if ($borderoHasParcela == null || !($borderoHasParcela->bordero->habilitado)) {

                    $retorno = [
                        "hasErrors" => true,
                        "title" => "Erro",
                        "msg" => "Parcela não encontrada. ID da parcela: $idParcela",
                        "style" => "error"
                    ];
                } else {

                    $transaction = Yii::app()->db->beginTransaction();

                    $borderoHasParcela->bordero->habilitado = 0;

                    if ($borderoHasParcela->bordero->save()) {

                        $borderoHasParcela->habilitado = 0;

                        if ($borderoHasParcela->save()) {

                            $dialogo = new Dialogo;
                            $dialogo->habilitado = 1;
                            $dialogo->data_criacao = date("Y-m-d");
                            $dialogo->Entidade_Relacionamento = "ParcelaRetorno";
                            $dialogo->Entidade_Relacionamento_id = $parcela->id;
                            $dialogo->assunto = "Retorno da parcela id $parcela->id para uma nova liberação.";
                            $dialogo->criado_por = Yii::app()->session["usuario"]->id;

                            if ($dialogo->save()) {

                                $mensagem = new Mensagem;
                                $mensagem->habilitado = 1;
                                $mensagem->data_criacao = date("Y-m-d H:i:s");
                                $mensagem->data_ultima_edicao = date("Y-m-d H:i:s");
                                $mensagem->Dialogo_id = $dialogo->id;
                                $mensagem->lida = 0;
                                $mensagem->editada = 0;
                                $mensagem->Usuario_id = Yii::app()->session["usuario"]->id;
                                $mensagem->conteudo = $_POST["observacao"];

                                if ($mensagem->save()) {

                                    $retorno = [
                                        "hasErrors" => false,
                                        "title" => "Sucesso",
                                        "msg" => "Liberação desfeita com sucesso. ",
                                        "style" => "success"
                                    ];

                                    $transaction->commit();
                                } else {

                                    ob_start();
                                    var_dump($mensagem->getErrors());
                                    $resultado = ob_get_clean();

                                    $retorno = [
                                        "hasErrors" => true,
                                        "title" => "Erro",
                                        "msg" => "Erro ao desfazer liberação. Mensagem: $resultado",
                                        "style" => "error"
                                    ];

                                    $transaction->rollBack();
                                }
                            } else {

                                ob_start();
                                var_dump($dialogo->getErrors());
                                $resultado = ob_get_clean();

                                $retorno = [
                                    "hasErrors" => true,
                                    "title" => "Erro",
                                    "msg" => "Erro ao desfazer liberação. Dialogo: $resultado",
                                    "style" => "error"
                                ];

                                $transaction->rollBack();
                            }
                        } else {

                            ob_start();
                            var_dump($borderoHasParcela->getErrors());
                            $resultado = ob_get_clean();

                            $retorno = [
                                "hasErrors" => true,
                                "title" => "Erro",
                                "msg" => "Erro ao desfazer liberação. BorderoHasParcela: $resultado",
                                "style" => "error"
                            ];

                            $transaction->rollBack();
                        }
                    } else {

                        ob_start();
                        var_dump($borderoHasParcela->bordero->getErrors());
                        $resultado = ob_get_clean();

                        $retorno = [
                            "hasErrors" => true,
                            "title" => "Erro",
                            "msg" => "Erro ao desfazer liberação. Bordero: $resultado",
                            "style" => "error"
                        ];

                        $transaction->rollBack();
                    }
                }
            } else {

                $retorno = [
                    "hasErrors" => true,
                    "title" => "Erro",
                    "msg" => "Parcela não encontrada. ID: $idParcela",
                    "style" => "error"
                ];
            }
        } else {

            $retorno = [
                "hasErrors" => true,
                "title" => "Erro",
                "msg" => "ID da Parcela não encontrado.",
                "style" => "error"
            ];
        }

        echo json_encode($retorno);
    }

    public function actionAprovarLiberacao() {

        if (isset($_POST["idParcela"])) {

            $idParcela = $_POST["idParcela"];

            $parcela = Parcela::model()->find("habilitado AND id = $idParcela");

            if ($parcela !== null) {

                $borderoHasParcela = BorderoHasParcela::model()->find("habilitado AND Parcela_id = $parcela->id");

                if ($borderoHasParcela !== null && $borderoHasParcela->bordero->habilitado && $borderoHasParcela->bordero->Status !== 3) {

                    $retorno = [
                        "hasErrors" => true,
                        "title" => "Erro",
                        "msg" => "Empréstimo já autorizado a pagar. ID BorderoHasParcela: $borderoHasParcela->id",
                        "style" => "error"
                    ];
                } else {

                    $transaction = Yii::app()->db->beginTransaction();

                    $bordero = new Bordero;
                    $bordero->habilitado = 1;
                    $bordero->dataCriacao = date("Y-m-d H:i:s");
                    $bordero->destinatario = 184; //rever isso aqui depois
                    $bordero->criadoPor = Yii::app()->session["usuario"]->id;
                    $bordero->Status = 1;

                    if ($bordero->save()) {

                        $borderoHasParcela = new BorderoHasParcela;
                        $borderoHasParcela->data_cadastro = date("Y-m-d H:i:s");
                        $borderoHasParcela->habilitado = 1;
                        $borderoHasParcela->Bordero_id = $bordero->id;
                        $borderoHasParcela->Parcela_id = $parcela->id;

                        if ($borderoHasParcela->save()) {

                            $retorno = [
                                "hasErrors" => false,
                                "title" => "Sucesso",
                                "msg" => "Liberação efetuado com sucesso. ",
                                "style" => "success"
                            ];

                            $transaction->commit();
                        } else {

                            ob_start();
                            var_dump($borderoHasParcela->getErrors());
                            $resultado = ob_get_clean();

                            $retorno = [
                                "hasErrors" => true,
                                "title" => "Erro",
                                "msg" => "Erro ao efetuar liberação. BorderoHasParcela: $resultado",
                                "style" => "error"
                            ];

                            $transaction->rollBack();
                        }
                    } else {

                        ob_start();
                        var_dump($bordero->getErrors());
                        $resultado = ob_get_clean();

                        $retorno = [
                            "hasErrors" => true,
                            "title" => "Erro",
                            "msg" => "Erro ao efetuar liberação. Bordero: $resultado",
                            "style" => "error"
                        ];

                        $transaction->rollBack();
                    }
                }
            } else {

                $retorno = [
                    "hasErrors" => true,
                    "title" => "Erro",
                    "msg" => "Parcela não encontrada. ID: $idParcela",
                    "style" => "error"
                ];
            }
        } else {

            $retorno = [
                "hasErrors" => true,
                "title" => "Erro",
                "msg" => "ID da Parcela não encontrado.",
                "style" => "error"
            ];
        }

        echo json_encode($retorno);
    }

    public function actionPagar() {

        $retorno = [];

        $transaction = Yii::app()->db->beginTransaction();

        if (!empty($_POST['idParcelaHdn'])) {

            $idParcela = $_POST['idParcelaHdn'];
            $contaDebito = $_POST['contaDebitoHdn'];
            $valorPago = $_POST['valorPagoHdn'];
            $comprovante = $_POST['comprovanteHdn'];
            $dataPagamento = $_POST['dataPagamentoHdn'];
            $contaCredito = $_POST['contaCreditoHdn'];
            $observacao = $_POST['observacaoHdn'];

            $parcela = Parcela::model()->find("habilitado AND id = $idParcela");

            if ($parcela !== null) {

                $dadosBancariosDebito = DadosBancarios::model()->find('habilitado AND id = ' . $contaDebito);
                $dadosBancariosCredito = DadosBancarios::model()->find('habilitado AND id = ' . $contaCredito);

                if ($dadosBancariosCredito !== null && $dadosBancariosDebito !== null) {

                    $dadosPagamento = new DadosPagamento;
                    $dadosPagamento->Dados_Bancarios_Credito = $dadosBancariosCredito->id;
                    $dadosPagamento->Dados_Bancarios_Debito = $dadosBancariosDebito->id;
                    $dadosPagamento->comprovante = $comprovante;
                    $dadosPagamento->dataCompensacao = $dataPagamento;
                    $dadosPagamento->valorPago = $valorPago;
                    $dadosPagamento->data_cadastro = date('Y-m-d H:i:s');
                    $dadosPagamento->observacao = $observacao;
                    $dadosPagamento->habilitado = 1;

                    if ($dadosPagamento->save()) {

                        $baixa = new Baixa;

                        $baixa->Filial_id = $parcela->titulo->proposta->analiseDeCredito->Filial_id;
                        $baixa->Parcela_id = $parcela->id;
                        $baixa->data_da_baixa = date('Y-m-d H:i:s');
                        $baixa->valor_pago = $parcela->valor;
                        $baixa->valor_abatimento = $parcela->valor;
                        $baixa->baixado = 1;
                        $baixa->DadosPagamento_id = $dadosPagamento->id;

                        if ($baixa->save()) {

                            $retorno = DadosPagamento::model()->anexarComprovante(
                                    $dadosPagamento->id, $_FILES ['ComprovanteFile'], 'Comprovante de pagamento', 2
                            );

                            if (!$retorno["hasErrors"]) {

                                $borderoHasParcela = BorderoHasParcela::model()->find("habilitado AND Parcela_id = $parcela->id");

                                if ($borderoHasParcela !== null) {

                                    if ($borderoHasParcela->bordero->habilitado && $borderoHasParcela->bordero->Status == 1) {

                                        $bordero = $borderoHasParcela->bordero;
                                        $bordero->Status = 4;

                                        if ($bordero->update()) {
                                            $retorno = array(
                                                'hasErrors' => 0,
                                                'msg' => 'Pagamento registrado com sucesso',
                                                'pntfyClass' => 'success'
                                            );

                                            $transaction->commit();
                                        } else {

                                            ob_start();
                                            var_dump($bordero->getErrors());
                                            $result = ob_get_clean();

                                            $transaction->rollBack();

                                            $retorno = array(
                                                'hasErrors' => 1,
                                                'msg' => 'Ocorreu um erro. Bordero: ' . $result,
                                                'pntfyClass' => 'error'
                                            );
                                        }
                                    } else {

                                        ob_start();
                                        var_dump($borderoHasParcela->bordero);
                                        $result = ob_get_clean();

                                        $transaction->rollBack();

                                        $retorno = array(
                                            'hasErrors' => 1,
                                            'msg' => 'Bordero não encontrado ou Status diferente de Efetivado (1). Bordero: ' . $result,
                                            'pntfyClass' => 'error'
                                        );
                                    }
                                } else {
                                    
                                }
                            } else {

                                $transaction->rollBack();
                            }
                        } else {

                            ob_start();
                            var_dump($baixa->getErrors());
                            $result = ob_get_clean();

                            $transaction->rollBack();

                            $retorno = array(
                                'hasErrors' => 1,
                                'msg' => 'Ocorreu um erro. Tente novamente.' . $result,
                                'pntfyClass' => 'error'
                            );

                            /**/
                        }
                    } else {

                        ob_start();
                        var_dump($dadosPagamento->getErrors());
                        $result = ob_get_clean();

                        $transaction->rollBack();

                        $retorno = array(
                            'hasErrors' => 1,
                            'msg' => "Erro ao efetuar o pagamento. DadosPagamento: $result",
                            'pntfyClass' => 'error'
                        );
                    }
                } else {

                    if ($dadosBancariosCredito == null) {
                        $deQuem = "do cliente";
                    } else {
                        $deQuem = "da empresa";
                    }

                    $retorno = array(
                        'hasErrors' => 1,
                        'msg' => "Ocorreu um erro. Dados bancários $deQuem não encontrados.",
                        'pntfyClass' => 'error'
                    );
                }
            } else {

                $retorno = array(
                    'hasErrors' => 1,
                    'msg' => "Parcela não encontrado. ID: $idParcela",
                    'pntfyClass' => 'error'
                );
            }
        } else {

            $retorno = array(
                'hasErrors' => 1,
                'msg' => 'Ocorreu um erro. Id da Parcela não encontrado.',
                'pntfyClass' => 'error'
            );
        }

        echo json_encode($retorno);
    }

    public function actionEstornar() {

        $idStatusEstornoAberto = ConfigLN::model()->find("habilitado AND parametro = 'idStatusEstornoAberto' ")->valor;

        $statusEstornoAberto = StatusEstorno::model()->find("habilitado AND id = $idStatusEstornoAberto");

        if ($statusEstornoAberto == null) {
            $idStatusEstornoAberto = 1; //valor padrão
        }

        $retorno = [
            "hasErrors" => false,
            "type" => "success",
            "title" => "Sucesso",
            "msg" => "Solicitação de Estorno efetuada com sucesso"
        ];

        $transaction = Yii::app()->db->beginTransaction();

        if (isset($_POST['idBaixa']) && !empty($_POST['idBaixa'])) {

            if (isset($_POST['comprovanteEstorno']) && !empty($_POST['comprovanteEstorno'])) {

                $estorno = new EstornoBaixa;

                $estorno->data_cadastro = date("Y-m_d");
                $estorno->habilitado = 1;
                $estorno->comprovante = $_POST["comprovanteEstorno"];
                $estorno->Baixa_id = $_POST["idBaixa"];
                $estorno->dataEstorno = $_POST["dataEstorno"];
                $estorno->dataRetorno = $_POST["dataRetorno"];

                if ($estorno->save()) {

                    if (isset($_POST["observacao"]) && !empty($_POST["observacao"])) {
                        $observacao = $_POST["observacao"];
                    } else {
                        $observacao = "";
                    }

                    $estornoHasStatus = new EstornoBaixaHasStatusEstorno;

                    $estornoHasStatus->habilitado = 1;
                    $estornoHasStatus->data_cadastro = date("Y-m-d");
                    $estornoHasStatus->EstornoBaixa_id = $estorno->id;
                    $estornoHasStatus->StatusEstorno_id = $idStatusEstornoAberto;
                    $estornoHasStatus->Usuario_id = Yii::app()->session["usuario"]->id;
                    $estornoHasStatus->observacao = $observacao;

                    if ($estornoHasStatus->save()) {
                        $transaction->commit();
                    } else {

                        $transaction->rollBack();

                        ob_start();
                        var_dump($estornoHasStatus->getErrors());
                        $resultado = ob_get_clean();

                        $retorno = [
                            "hasErrors" => true,
                            "type" => "error",
                            "title" => "Erro",
                            "msg" => "Erro ao salvar a solicitação de estorno. "
                            . "<br>EstornoHasStatus: $resultado"
                        ];
                    }
                } else {

                    $transaction->rollBack();

                    ob_start();
                    var_dump($estorno->getErrors());
                    $resultado = ob_get_clean();

                    $retorno = [
                        "hasErrors" => true,
                        "type" => "error",
                        "title" => "Erro",
                        "msg" => "Erro ao salvar a solicitação de estorno. "
                        . "<br>Estorno: $resultado"
                    ];
                }
            } else {

                $retorno = [
                    "hasErrors" => true,
                    "type" => "error",
                    "title" => "Erro",
                    "msg" => "Por favor, digite o número do comprovante do estorno."
                ];
            }
        } else {

            $retorno = [
                "hasErrors" => true,
                "type" => "error",
                "title" => "Erro",
                "msg" => "ID da baixa não encontrado."
            ];
        }

        echo json_encode($retorno);
    }

    public function actionExcluirContratoAnexo() {

        $retorno = [
            "hasErrors" => 0,
            "title" => "Sucesso",
            "text" => "Anexo de contrato excluído com sucesso",
            "type" => "success",
        ];

        if (isset($_POST["idAnexo"])) {

            $anexo = Anexo::model()->find("habilitado AND id = " . $_POST["idAnexo"]);

            if ($anexo !== null) {

                $anexo->habilitado = 0;

                if (!$anexo->update()) {

                    ob_start();
                    var_dump($anexo->getErrors());
                    $resultado = ob_get_clean();

                    $retorno = [
                        "hasErrors" => 1,
                        "title" => "Erro",
                        "text" => "Erro ao excluir anexo. Anexo: $resultado",
                        "type" => "error",
                    ];
                }
            } else {

                $retorno = [
                    "hasErrors" => 1,
                    "title" => "Erro",
                    "text" => "Anexo não encontrado. ID: " . $_POST["idAnexo"],
                    "type" => "error",
                ];
            }
        } else {
            $retorno = [
                "hasErrors" => 1,
                "title" => "Erro",
                "text" => "ID do anexo não encontrado",
                "type" => "error",
            ];
        }

        echo json_encode($retorno);
    }

}
