<?php

class EnderecoController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionLoad(){

		$id 		= $_GET['entity_id'];

		echo json_encode( Endereco::model()->loadFormEdit($id) );
	}

	public function actionUpdate(){

		$action 	= $_POST['controller_action'];
		$msgReturn 	= "";

		if( $action == 'add' )
		{
			Endereco::model()->novo($_POST['EnderecoCliente'],$_POST['Cliente_id']);
			$msgReturn = "Endereço cadastrado com sucesso!";
		}
		else
		{
			Endereco::model()->atualizar($_POST['EnderecoCliente'], $_POST['endereco_id'], $_POST['Cliente_id']);
			$msgReturn = "Endereço atualizado com sucesso!";
		}

		echo json_encode(array(
			'msgReturn'=>$msgReturn
		));
	}

	public function actionPersist()
	{
		echo json_encode( Endereco::model()->persist( $_POST['EnderecoCliente'], $_POST['clienteId'], $_POST['EnderecoId'], $_POST['ControllerAction'] ) );
	}
}