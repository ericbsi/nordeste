<?php

class LoginController extends Controller
{
    
    public $layout = '//layouts/login';

    public function filters()
    {

        return array(
                        'accessControl'     ,   // perform access control for CRUD operations
                        'postOnly + delete'     // we only allow deletion via POST request
                    );
      
    }

    public function accessRules()
    {
    	return array(
                        array   (
                                    'allow'                                         ,
                                    'actions'   => array(
                                                            'index'                 ,
                                                            'validarUsuario'        ,
                                                            'listarFiliaisUsuario'
                                                        )                           ,
                                    'users'     => array('*')               
                                )
                    );
    }

    public function actionIndex()
    {

        header('Access-Control-Allow-Origin: *');

        $baseUrl    = Yii::app()->baseUrl           ;
        $cs         = Yii::app()->getClientScript() ;

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css'  );
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css'     );
        $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css'          );
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpalette.css'                   );
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpicker.css'                    );

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js'                                          );
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js'    , CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js'     , CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js'                 , CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js'          , CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js'             , CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js'      , CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js'          , CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js'          , CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/login/fn-validarLogin.js'       , CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js'           , CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js'              , CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js'       , CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpicker.js'       , CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpalette.js'      , CClientScript::POS_END);
        
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js',CClientScript::POS_END);

        $this->render('login');
        
    }

    public function actionValidarUsuario()
    {
        
        $retorno =  [
                        "hasErrors" => false                            ,
                        "msg"       => "Insira sua senha de usuário"    ,
                        "type"      => "success"
                    ];

        if(isset($_POST["username"]))
        {
            
            $usuario = Usuario::model()->find("habilitado AND username = '" . $_POST["username"] . "' ");
            
            if($usuario == null)
            {
                $retorno =  [
                                "hasErrors" => true                     ,
                                "msg"       => "Usuário não encontrado" ,
                                "type"      => "error"
                            ];
            }
            
        }
        else
        {
            
            $retorno =  [
                            "hasErrors" => true                         ,
                            "msg"       => "Digite um nome de usuário"  ,
                            "type"      => "error"
                        ];
            
        }
        
        echo json_encode($retorno);
        
    }

    public function actionValidarSenha()
    {
        
        $retorno =  [
                        "hasErrors" => false                    ,
                        "msg"       => "Usuário autenticado."   ,
                        "type"      => "success"
                    ];

        if(isset($_POST["username"]))
        {
            
            $usuario = Usuario::model()->find("habilitado AND username = '" . $_POST["username"] . "' ");
            
            if(!$usuario == null)
            {
                
                if (!Yii::app()->hasher->checkPassword( $_POST["senha"], $usuario->password ))
                {
                    $retorno =  [
                                    "hasErrors" => true                             ,
                                    "msg"       => "Senha do usuário não confere."  ,
                                    "type"      => "error"
                                ];
                }    
                
            }
            else
            {
                $retorno =  [
                                "hasErrors" => true                     ,
                                "msg"       => "Usuário não encontrado" ,
                                "type"      => "error"
                            ];
            }
            
        }
        else
        {
            
            $retorno =  [
                            "hasErrors" => true                         ,
                            "msg"       => "Digite um nome de usuário"  ,
                            "type"      => "error"
                        ];
            
        }
        
        echo json_encode($retorno);
        
    }

    public function actionGetRoleUsuario()
    {
        
        $retorno["data"] =  [
                                    /*"id"    => 0                        ,
                                    "text"  => "Selecione um módulo"*/
                            ];
        
        $retorno["hasErrors"] = false;

        if(isset($_POST["username"]))
        {
            
            $usuario = Usuario::model()->find("habilitado AND username = '" . $_POST["username"] . "' ");
            
            if(!$usuario == null)
            {
                $usuarioRole = UsuarioRole::model()->findAll("habilitado AND Usuario_id = $usuario->id");
                
                foreach ($usuarioRole as $ur)
                {
                    
                    $role = Role::model()->find("habilitado AND id = $ur->Role_id");
                    
                    if($role !== null)
                    {
                        $retorno["data"][] =    [
                                                    "id"    => $role->id    ,
                                                    "text"  => $role->label
                                                ];
                    }
                    
                }
                
            }
            else
            {
                $retorno =  [
                                "hasErrors" => true                         ,
                                "msg"       => "Usuário não encontrado."    ,
                                "type"      => "error"
                            ];
            }
            
        }
        else
        {
            $retorno =  [
                            "hasErrors" => true                     ,
                            "msg"       => "Username não enviado."  ,
                            "type"      => "error"
                        ];
        }
        
        echo json_encode($retorno);
        
    }
    
    public function actionGetFiliaisUsuario()
    {
        
        $retorno["data"] =    [
                                    /*"id"    => 0                        ,
                                    "text"  => "Selecione uma filial..."*/
                                ];
        
        $retorno["hasErrors"] = false;

        if(isset($_POST["username"]) && isset($_POST["idModulo"]))
        {
            
            $usuario = Usuario::model()->find("habilitado AND username = '" . $_POST["username"] . "' ");
            
            if(!$usuario == null)
            {
                $filialUsuario = FilialHasUsuario::model()->findAll("habilitado AND Usuario_id = $usuario->id");
                
                foreach ($filialUsuario as $fHu)
                {
                    
                    $filial = Filial::model()->find("habilitado AND id = $fHu->Filial_id");
                    
                    if($filial !== null)
                    {
                        
                        $retorno["data"][] =    [
                                                    "id"    => $filial->id          ,
                                                    "text"  => $filial->getConcat()
                                                ];
                    }
                    
                }
                
            }
            else
            {
                $retorno =  [
                                "hasErrors" => true                         ,
                                "msg"       => "Usuário não encontrado."    ,
                                "type"      => "error"
                            ];
            }
            
        }
        else
        {
            $retorno =  [
                            "hasErrors" => true                     ,
                            "msg"       => "Username não enviado."  ,
                            "type"      => "error"
                        ];
        }
        
        echo json_encode($retorno);
        
    }
    
}
