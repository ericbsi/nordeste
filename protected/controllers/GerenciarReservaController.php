<?php

class GerenciarReservaController extends Controller {

    public $layout = '//layouts/main_sigac_template';

    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index','gravar'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        
//        var_dump($_POST);

        $cabec = NULL;
        $retorno = NULL;

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-gerenciarreserva-table.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js', CClientScript::POS_END);

        $cs->registerScript('select_filial', '$(function($){$("#select_filial"  ).select2();});');
        $cs->registerScript('select_positivo', '$(function($){$("#select_positivo").select2();});');

        $cabec = ['RESERVA','FILIAL','CODIGO','DATA','NOME'];
        
        $dados = Reserva::model()->listReservas();
        
        $this->render('index', array(
            'cabec' => $cabec,
            'query' => $dados,
            //'parametros' => $_POST,
        ));
    }
    
    
    public function actionGravar()
    {
        $rese = $_POST['rese'];
        $qtd  = $_POST['qtd'];
        
        $reserva = Reserva::model()->findByPk($rese);
        
        if ($qtd['efe'] == $reserva->qtdReservada)
        {
            $reserva->StatusReserva_id = 3;
        }elseif ($qtd['rec'] == $reserva->qtdReservada) {
            $reserva->StatusReserva_id = 4;
        }else{
            $reserva->StatusReserva_id = 2;
        }
        
        $reserva->qtdEfetivada = $qtd['efe'];
        $reserva->qtdRecusada  = $qtd['rec'];
        
        return json_encode($reserva->grava());
        
    }
    

}
