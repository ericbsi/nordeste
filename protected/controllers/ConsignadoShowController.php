<?php

class ConsignadoShowController extends Controller
{	

	public $layout = '//layouts/main_sigac_template';

	public function actionIndex()
	{	

		$baseUrl        = Yii::app()->baseUrl; 
        $cs             = Yii::app()->getClientScript();
        
        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');
        $cs->registerCssFile($baseUrl.'/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl.'/css/sigax_template/dataTables.tableTools.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js',                CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js',               CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js',        CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ui-elements.js',                   CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.autosize.min.js',           CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.js',                       CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskMoney.js',              CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js',            CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.inputlimiter.1.3.1.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js',                   CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js',            CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js',            CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js',             CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js',      CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js',       CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/blockInterface.js',                                       CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js',                CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js',          CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/fn-consignado-show.js',                              		CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js',            CClientScript::POS_END);

        $cs->registerScript('moneypicker','$(".grana").maskMoney({decimal:",", thousands:"."});');
        $cs->registerScript('select2','');
        $cs->registerScript('ipt_date','jQuery(function($){$("input.dateBR").mask("99/99/9999");});');

        $cs->registerScript('ipt_tempo_residencia','jQuery(function($){$("#tempo_de_residencia").mask("99/9999");});');
        $cs->registerScript('ipt_data','jQuery(function($){$("input.dateBR").mask("99/99/9999");});');
        $cs->registerScript('ipt_mes_ano','jQuery(function($){$("input.mes_ano").mask("99/9999");});');
        $cs->registerScript('ipt_date','jQuery(function($){$("input.cpfmask").mask("99999999999");});');
        $cs->registerScript('ipt_date','jQuery(function($){$("input.cepmask").mask("99999999");});');
        $cs->registerScript('moneypicker2','$(".currency").maskMoney({decimal:",", thousands:".", allowZero: true});');
        $cs->registerScript('moneypicker','$(".currency2").maskMoney({decimal:",", thousands:".", allowZero: false});');
        $cs->registerScript('telefone','jQuery(function($){$("#telefone").mask("(99) 99999-9999");});');

		$this->render('index');
	}

	public function actionEnviar(){
		echo json_encode( ConsignadoShow::model()->novo( $_POST['ConsignadoShow'] ) );
	}
}