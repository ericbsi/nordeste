<?php

class CaracteristicaProdutoController extends Controller
{	

	public $layout = '//layouts/main_sigac_template';

	public function actionIndex()
	{	

		$baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl.'/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl.'/css/sigax_template/dataTables.tableTools.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js');
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js');
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-carac-produtos-index.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.validate.12.js',CClientScript::POS_END);
        
		$this->render('index');
	}

	public function actionAdd(){

		if( !empty( $_POST['CaracteristicaProduto']['descricao'] ) )
		{
			echo json_encode( CaracteristicaProduto::model()->novo( $_POST['CaracteristicaProduto'] ) );
		}
	}

	public function actionEditar(){

		$baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl.'/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl.'/css/sigax_template/dataTables.tableTools.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js');
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js');
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-item-carac-produtos.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.validate.12.js',CClientScript::POS_END);

		$caracteristicaId 		= $_POST['caracteristicaId'];
		$caracteristica 		= CaracteristicaProduto::model()->findByPk( $caracteristicaId );

		$this->render('editar', array(
			'caracteristica'	=>	$caracteristica
		));
	}

	public function actionGetCaracteristicas(){
		echo json_encode( CaracteristicaProduto::model()->listCaracteristicas( $_POST['draw'] ) );
	}
}