<?php

class TabelaDePrecosController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/main_sigac_template';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('add', 'cadastrarTP', 'create', 'update', 'index', 'view', 'delete', 'admin', 'edit', 'getMenorPreco'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionCadastrarTP() {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');

        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ui-modals.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-grid-tabelaDePrecos.js', CClientScript::POS_END);
        $cs->registerScript('select2', '$("select.search-select").select2()');
        $cs->registerScript('UIModals', 'UIModals.init();');

        $model = new TabelaDePrecos;

        if (isset($_POST['TabelaDePrecos'])) {

            $model->attributes = $_POST['TabelaDePrecos'];

                

            $ieId   = $_POST['ItemTabelaDePrecos_ItemDoEstoque_id_hdn'];
            $valor  = $_POST['valor_hdn'];

            $model->habilitado  = 1;

            if ($model->save()) {

                for ($i = 0; $i < sizeof($ieId); $i++) {

                    $modelITP = new ItemTabelaDePrecos;

                    $modelITP->Tabela_de_Precos_id = $model->id;
                    $modelITP->Item_do_Estoque_id = $ieId[$i];
                    $modelITP->valor = $valor[$i];

                    $modelITP->save();
                }

                //$this->redirect(array('view','id'=>$model->id));
                $this->redirect(array('admin'));
            }
        }

        $this->render('cadastrarTP', array(
            'model' => $model,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new TabelaDePrecos;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['TabelaDePrecos'])) {
            $model->attributes = $_POST['TabelaDePrecos'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['TabelaDePrecos'])) {
            $model->attributes = $_POST['TabelaDePrecos'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('TabelaDePrecos');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new TabelaDePrecos('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['TabelaDePrecos']))
            $model->attributes = $_GET['TabelaDePrecos'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionGetMenorPreco() {

        $itemEstoque    = ItemDoEstoque::model()->findBypk( $_POST['idItem'] );
        
        $valor          = ItemTabelaDePrecos::model()->searchMenorPreco($itemEstoque);
        
        echo json_encode( $valor);
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return TabelaDePrecos the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = TabelaDePrecos::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param TabelaDePrecos $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'tabela-de-precos-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
