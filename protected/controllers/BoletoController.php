<?php

class BoletoController extends Controller {

    public $layout = '//layouts/boleto_layout';

    public function filters(){

        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete' // we only allow deletion via POST request
        );
    }

    public function accessRules(){

        return array(
            array('allow',
                'actions'       => array('index','segundaVia'),
                'users'         => array('@'),
                'expression'    => 'Yii::app()->session["usuario"]->role == "crediarista"'
            ),
            array('allow',
                'actions'       => array('gerarRemessa'),
                'users'         => array('@'),
                'expression'    => 'Yii::app()->session["usuario"]->role == "financeiro"'
            ),
            /*array('allow',
                'actions'       => array('testes'),
                'users'         => array('@')
                //'expression'    => 'Yii::app()->session["usuario"]->role == "super"'
            ),*/
            array('allow',
                'actions'       => array('externalClient','testes'),
                'users'         => array('*')
                //'expression'    => 'Yii::app()->session["usuario"]->role == "super"'
            ),
            array(
                'deny',
                'users' => array('*')
            )
        );
    }

    public function actionIndex(){

        $codigo         = $_POST['c'];
        $tg             = $_POST['tg'];
        $view_boleto    = 'index';

        if( $tg == 1 )
        {
            throw new Exception('Os títulos desta proposta já foram gerados.', 0);
        }

        else
        {
            $objRange           = new Range;
            $proposta           = Proposta::model()->findByCode($codigo, 0);

            if( $proposta->analiseDeCredito->filial->boletoNaoBancario() ){
              $boletoConfig       = $objRange->gerarTitulosCredshow( $proposta );
              $view_boleto        = 'index_bnb';
            }
            else{
              $boletoConfig       = $objRange->gerarTitulosBradesco( $proposta );
            }

            $this->render($view_boleto, array(
              'proposta'      => $proposta,
              'boletoConfig'  => $boletoConfig
            ));
        }
    }

    public function actionSegundaVia()
    {
        $objRange                   = new Range;
        $view_boleto                = 'index_segunda_via';

        $parcelaAux                 = Parcela::model()->findByPk($_POST['ParcelasIds'][0]);
        $proposta                   = Proposta::model()->findByPk($parcelaAux->titulo->Proposta_id);

        $baseUrl                    = Yii::app()->baseUrl;
    	  $cs                         = Yii::app()->getClientScript();
        // $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        // $cs->registerScriptFile($baseUrl . '/js/jquery-barcode.min.js');

        if( $proposta->Banco_id == 5 )
        {
            $boletoConfig           = $objRange->getSegundaVia( $_POST['ParcelasIds'], $proposta );
        }

        else if( $proposta->Banco_id == 7 )
        {
            $boletoConfig           = $objRange->getSegundaViaSantander( $_POST['ParcelasIds'], $proposta );
        }

        else if( $proposta->Banco_id == 3 )
        {
            $boletoConfig           = $objRange->getSegundaViaBradesco( $_POST['ParcelasIds'], $proposta );
        }

        else if( $proposta->Banco_id == 10 )
        {
          $boletoConfig           = $objRange->getSegundaViaCredshow( $_POST['ParcelasIds'], $proposta );
        }

        $this->render($view_boleto,  array(
            'proposta'          => $proposta,
            'boletoConfig'      => $boletoConfig
        ));
    }

    public function actionGerarRemessa(){

        $financeira     = Financeira::model()->findByPk(5);
        $range          = new Range;
        $range->gerarRemessa($financeira);
    }

    public function actionEcommerce() {

        $codigo     = $_GET['c']; //codigo da venda
        $at         = $_GET['t']; //token de acesso da venda
        $venda      = VendaW::model()->find('codigo = ' . $codigo . ' AND external_access_token = ' . "'$at' AND titulos_gerados = 0");

        if ($venda === null) {

            throw new CHttpException(404, 'Registro não encontrado');

        } else {

            $boletoConfig = Range::boletoECommerce($venda);

            $this->render('boletoEcommerceBase', array(
                'venda'         => $venda,
                'boletoConfig'  => $boletoConfig
            ));
        }
    }

    public function actionSSDT(){

        echo json_encode(Yii::app()->session['usuario']->listAnalistaPropostas( $_POST['draw']) );
    }

    public function actionXEditable()
    {
    }

    public function actionTestes(){



        $this->layout = '//layouts/main_sigac_template';
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerCssFile($baseUrl.'/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl.'/css/sigax_template/dataTables.tableTools.css');

        $cs->registerCssFile($baseUrl.'/css/sigax_template/bootstrap-editable.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-editable.min.js', CClientScript::POS_END);

        $cs->registerScript('inlineMode',"$.fn.editable.defaults.mode = 'inline';");
        $cs->registerScript('inlineModeApply'," $(document).ready(function(){ $('#username').editable(); }); ");


        $this->render('blank2');

    }

    public function actionExternalClient(){

        header('Access-Control-Allow-Origin: *');

        $this->layout                       = 'empty_template';
        $ven                                = $_POST['ven'];
        $range                              = Range::model()->find('habilitado');
        $util                               = new Util;
        $p                                  = Pessoa::model()->isClient($ven['cliente']['cpf']);
        $cliente                            = null;

        if( $util->validarCPF( $ven['cliente']['cpf'] ) )
        {
            if($p == NULL)
            {
                $pessoa                         = new Pessoa;
                $contato                        = new Contato;
                $email                          = new Email;
                $telefone                       = new Telefone;

                $email->email                   = $ven['cliente']['contato']['email'];
                $email->data_cadastro           = date('Y-m-d H:i:s');
                $email->data_cadastro_br        = date('d-m-Y');

                $telefone->numero               = $ven['cliente']['contato']['telefone'];
                $telefone->Tipo_Telefone_id     = 1;
                $telefone->data_cadastro        = date('Y-m-d H:i:s');
                $telefone->data_cadastro_br     = date('d-m-Y');

                $contato->data_cadastro         = date('Y-m-d H:i:s');
                $contato->data_cadastro_br      = date('d-m-Y');


                if ( $contato->save() ) {

                    $pessoa->nome               = $ven['cliente']['nome'];
                    $pessoa->Contato_id         = $contato->id;
                    $pessoa->data_cadastro      = date('Y-m-d H:i:s');
                    $pessoa->data_cadastro_br   = date('d-m-Y');
                    $pessoa->Estado_Civil_id    = 1;

                    if ($email->save()) {

                        $contato_has_email      = new ContatoHasEmail;
                        $contato_has_email->Contato_id          = $contato->id;
                        $contato_has_email->Email_id            = $email->id;
                        $contato_has_email->data_cadastro       = date('Y-m-d H:i:s');
                        $contato_has_email->data_cadastro_br    = date('d-m-Y');
                        $contato_has_email->save();
                    }

                    if ($telefone->save()) {

                        $contato_has_telefone = new ContatoHasTelefone;
                        $contato_has_telefone->Contato_id = $contato->id;
                        $contato_has_telefone->Telefone_id = $telefone->id;
                        $contato_has_telefone->data_cadastro = date('Y-m-d H:i:s');
                        $contato_has_telefone->data_cadastro_br = date('d-m-Y');
                        $contato_has_telefone->save();
                    }

                    if ( $pessoa->save() ) {

                        $cliente                    = new Cliente;
                        $cliente->Pessoa_id         = $pessoa->id;

                        $endereco                   = new Endereco;
                        $endereco->logradouro       = $ven['cliente']['endereco']['logradouro'];
                        $endereco->numero           = $ven['cliente']['endereco']['numero'];
                        $endereco->bairro           = $ven['cliente']['endereco']['bairro'];
                        $endereco->cidade           = $ven['cliente']['endereco']['cidade'];
                        $endereco->uf               = $ven['cliente']['endereco']['estado'];
                        $endereco->cep              = $ven['cliente']['endereco']['cep'];
                        $endereco->data_cadastro    = date('Y-m-d H:i:s');
                        $endereco->data_cadastro_br = date('d-m-Y');
                        $endereco->Tipo_Endereco_id = 2;

                        $documento                    = new Documento;
                        $documento->numero            = $ven['cliente']['cpf'];
                        $documento->data_cadastro     = date('Y-m-d H:i:s');
                        $documento->data_cadastro_br  = date('d-m-Y');
                        $documento->tipo_documento    = 'CPF';
                        $documento->Tipo_documento_id = 1;

                        if ( $documento->save() ) {

                            $pessoaHasDocumento = new PessoaHasDocumento;
                            $pessoaHasDocumento->Documento_id = $documento->id;
                            $pessoaHasDocumento->Pessoa_id = $pessoa->id;
                            $pessoaHasDocumento->data_cadastro = date('Y-m-d H:i:s');
                            $pessoaHasDocumento->data_cadastro_br = date('d/m/Y');
                            $pessoaHasDocumento->save();
                        }

                        if ( $endereco->save() ) {

                            $pessoaHasEndereco = new PessoaHasEndereco;
                            $pessoaHasEndereco->Pessoa_id = $pessoa->id;
                            $pessoaHasEndereco->Endereco_id = $endereco->id;
                            $pessoaHasEndereco->data_cadastro = date('Y-m-d H:i:s');
                            $pessoaHasEndereco->data_cadastro_br = date('d/m/Y');
                            $pessoaHasEndereco->save();
                        }

                        if ( $cliente->save() ){
                            $cadastro                   = new Cadastro;
                            $cadastro->Cliente_id       = $cliente->id;
                            $cadastro->Empresa_id       = 5;
                            $cadastro->data_cadastro    = date('Y-m-d H:i:s');
                            $cadastro->data_cadastro_br = date('d/m/Y');
                            $cadastro->save();
                        }
                    }
                }
            }
            else
            {
                $cliente                        = Cliente::model()->find('Pessoa_id = ' . $p->id);
            }

            if( $cliente != NULL )
            {
                $venda                              = new VendaW;
                //$venda->codigo                      = $util->getCodeVenda("$range->prefixo" . date('dmyhis'), 10);
                $venda->codigo                      = $ven['codigoExterno'];
                $venda->codigo_externo              = $ven['codigoExterno'];
                $venda->numero_parcelas             = sizeof($ven['parcelas'][0]);
                $venda->data_compra                 = substr($ven['dataDaVenda'], 4, 4) . '-' . substr($ven['dataDaVenda'], 2, 2) . '-' . substr($ven['dataDaVenda'], 0, 2);
                $venda->valor_total                 = $ven['valorCobrado'];
                $venda->valor_total_texto           = $ven['valorCobrado'];
                $venda->Origem_venda_id             = 1;
                $venda->titulos_gerados             = 0;
                $venda->Cliente_id                  = $cliente->id;
                $venda->Filial_id                   = 19;
                $venda->external_access_token       = $util->getToken(50);

                if ( $venda->save() ) {

                    $titulo                         = new Titulo;
                    $titulo->prefixo                = '002';
                    $titulo->NaturezaTitulo_id      = 1;
                    $titulo->VendaW_id              = $venda->id;

                    if ( $titulo->save() )
                    {

                        for ($i = 0; $i < $venda->numero_parcelas; $i++)
                        {

                            $pa                     = new Parcela;
                            $pa->seq                = $i + 1;
                            $pa->vencimento         = substr($ven['parcelas'][0][$i]['vencimento'], 4, 4) . '-' . substr($ven['parcelas'][0][$i]['vencimento'], 2, 2) . '-' . substr($ven['parcelas'][0][$i]['vencimento'], 0, 2);
                            $pa->valor              = $ven['parcelas'][0][$i]['valor'];
                            $pa->valor_texto        = $ven['parcelas'][0][$i]['valor'];
                            $pa->valor_atual        = 0;
                            $pa->Titulo_id          = $titulo->id;
                            $pa->save();
                        }
                    }
                }

                echo json_encode(array(
                    'c'     => $venda->codigo,
                    't'     => $venda->external_access_token,
                    'url'   => 'https://beta.sigacbr.com.br/ecommerce'
                ));
            }
        }

        else
        {
            echo json_encode( array(
                'erro' => 'CPF Inválido'
            ) );
        }

    }

    public function init() {
    }
}
