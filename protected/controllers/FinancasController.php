<?php

class FinancasController extends Controller
{	

	public $layout = '//layouts/main_sigac_template';

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionDashboard()
	{
		//Yii::app()->session['usuario']->updateVistoPorUltimo();

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js',           CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js',	CClientScript::POS_END);
        
        if ( Yii::app()->session['usuario']->primeira_senha == 1 ){
            $cs->registerScript('ShowModal', '$("#static").modal("show")');
        }

		$this->render('/financas/dashboard');
	}
}