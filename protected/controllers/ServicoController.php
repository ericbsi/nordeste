<?php

class ServicoController extends Controller {

    public $layout = '//layouts/main_sigac_template';

    public function filters() {

        return array(
                'accessControl', // perform access control for CRUD operations
                'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules() {

        return array(
                array('allow',
                        'actions' => array(
                                'index',
                                'getServicos',
                                'getFiliaisAdmin',
                                'marcarTodasFiliaisAdmin',
                                'marcarFilialAdmin',
                                'salvarServico',
                                'atulizarFinanceira',
                                'filialServicos',
                                'getFiliais'
                        ),
                        'users' => array(
                                '@'
                        ),
                        'expression' => 'Yii::app()->session["usuario"]->role == "empresa_admin" '
                ),
                array('deny',
                        'users' => array('*'),
                ),
        );
    }

    public function actionFilialServicos() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/servico/fn-filial-servicos.js', CClientScript::POS_END);

        $this->render('filial_servicos');
    }

    public function actionGetFiliais() {
        $filiais = $_POST['filiais'];
        $rows = [];
        $ids = [];
        if (isset($_POST['filiais']) && $_POST['filiais'] != '') {
            foreach ($_POST['filiais'] as $id) {
                $ids[] = $id;
            }
            $filiais = Filial::model()->findAll('habilitado AND id IN (' . join(',', $ids) . ')');

            foreach ($filiais as $f) {

                $row = array(
                        'id' => $f->id,
                        'filial' => $f->getConcat()
                );

                $rows[] = $row;
            }
        }

        echo json_encode(array('data' => $rows));
    }

    public function actionIndex() {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/servico/fn-servicos-grid.js?v=1.4', CClientScript::POS_END);

        $this->render('index');
    }

    public function actionGetServicos() {

        $rows = array();
        $criteria = new CDbCriteria;
        $criteria->offset = $_POST['start'];
        $criteria->limit = 10;
        $criteria->condition = 'habilitado';
        $criteria->order = 't.descricao';

        foreach (Servico::model()->findAll($criteria) as $s) {

            $fHasS = FilialHasServico::model()->findAll("habilitado AND Servico_id = $s->id");

            $row[] = array(
                    'idServico' => $s->id,
                    'descricao' => $s->descricao,
                    'qtdFiliais' => count($fHasS),
            );

            $rows = $row;
        }


        $retorno = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => count($rows),
                "recordsFiltered" => count($rows),
                "data" => $rows
        );

        echo json_encode($retorno);
    }

    public function actionAtulizarFinanceira() {
        $idFil = $_POST['idFil'];
        $idFin = $_POST['idFin'];
        $return = array(
                'erro' => true,
                'type' => 'error',
                'msg' => 'Não foi possivel alterar a financeira!'
        );
        $filialHS = FilialHasServico::model()->find('habilitado AND Servico_id = 1 AND Filial_id = ' . $idFil);
        if ($filialHS !== null) {
            $mudar = FinanceiraHasFilialHasServico::model()->find('habilitado AND Filial_has_Servico_id = ' . $filialHS->id);

            if ($mudar === null) {
                $mudar = new FinanceiraHasFilialHasServico;
                $mudar->Filial_has_Servico_id = $filialHS->id;
                $mudar->data_cadastro = Date('Y-m-d');
                $mudar->habilitado = 1;
                $mudar->ordem = 0;
                $mudar->Financeira_id = $idFin;
                if ($mudar->save()) {
                    $return = array(
                            'erro' => false,
                            'type' => 'success',
                            'msg' => 'Financeira alterada com sucesso!'
                    );
                } else {
                    $return = array(
                            'erro' => true,
                            'type' => 'error',
                            'msg' => 'Não foi possivel alterar a financeira!'
                    );
                }
            } else {
                $mudar->Financeira_id = $idFin;
                if ($mudar->update()) {
                    $return = array(
                            'erro' => false,
                            'type' => 'success',
                            'msg' => 'Financeira alterada com sucesso!'
                    );
                } else {
                    $return = array(
                            'erro' => true,
                            'type' => 'error',
                            'msg' => 'Não foi possivel alterar a financeira!'
                    );
                }
            }
        }

        echo json_encode($return);
    }

    public function actionGetFiliaisAdmin() {

        $retorno = Servico::model()->listFiliaisAdmin(
            $_POST['idServico'], $_POST['draw'], $_POST['start'], $_POST['search']
        );

        echo json_encode($retorno);
    }

    public function actionMarcarTodasFiliaisAdmin() {
        $retorno = Servico::model()->marcarTodasFiliaisAdmin($_POST['idServico'], $_POST['classeCheck']);

        echo json_encode($retorno);
    }

    public function actionMarcarFilialAdmin() {
        $retorno = Servico::model()->marcarFilialAdmin($_POST['idServico'], $_POST['idFilial'], $_POST['classeCheck']);

        echo json_encode($retorno);
    }

    public function actionSalvarServico() {

        $retorno = [
                "title" => "Sucesso",
                "msg" => "Serviço cadastrado com sucesso",
                "pntfyClass" => "success",
                "hasErrors" => false
        ];

        if (isset($_POST["nomeServico"]) && !Empty($_POST["nomeServico"])) {

            $servico = new Servico;
            $servico->habilitado = 1;
            $servico->data_cadastro = date("Y-m-d H:i:s");
            $servico->descricao = $_POST["nomeServico"];

            if (!$servico->save()) {

                ob_start();
                var_dump($servico->getErrors());
                $retErro = ob_get_clean();

                $retorno = [
                        "title" => "Erro",
                        "msg" => "Erro ao salvar o Serviço. $retErro",
                        "pntfyClass" => "error",
                        "hasErrors" => true
                ];
            }
        } else {
            $retorno = [
                    "title" => "Erro",
                    "msg" => "Defina o nome do Serviço",
                    "pntfyClass" => "error",
                    "hasErrors" => true
            ];
        }

        echo json_encode($retorno);
    }

}
