<?php

////////////////////////////////////////////
//classe controller da politica de credito//
////////////////////////////////////////////
//Autor : Andre//Data : 08-04-2015//////////
////////////////////////////////////////////
class PoliticaCreditoController extends Controller
{

   public function actionIndex()
   {

      $this->layout = '//layouts/main_sigac_template';

      $baseUrl = Yii::app()->baseUrl;
      $cs = Yii::app()->getClientScript();

      $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css'  );
      $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css'     );
      $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css'          );
      $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpalette.css'                   );
      $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpicker.css'                   );

      $cs->registerScriptFile($baseUrl . '/js/jquery.min.js'                                       );
      $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js' , CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js'  , CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/js/select2.min.js'              , CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js'       , CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js'          , CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js'   , CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js'       , CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/js/additional-methods.js'       , CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/js/politicaCredito/fn-grid.js'  , CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js'        , CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js'           , CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js'    , CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpicker.js'    , CClientScript::POS_END);
      $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpalette.js'   , CClientScript::POS_END);
//      $cs->registerScriptFile($baseUrl . '/js/commits.js'                  , CClientScript::POS_END);

      $this->render('index'); //redirecione para a pagina principal deste controller
   }

   public function actionEditar()
   {
      
   }

   public function actionGetPoliticas()
   {

      echo json_encode(
              PoliticaCredito::model()->listar()
      );
   }

   public function actionSalvar()
   {

      if (!empty($_POST['PoliticaCredito']))
      {

         $retorno = PoliticaCredito::model()->salvar($_POST['PoliticaCredito']);

         echo json_encode($retorno);
      }
   }

   public function accessRules()
   {

      return array(
          array('allow',
              'actions' => array('index', 'salvar', 'getPoliticas'),
              'users'   => array('@'                              ),
          ),
          array('deny',
              'users' => array('*'),
          ),
      );
   }

}
