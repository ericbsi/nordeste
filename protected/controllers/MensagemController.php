<?php

class MensagemController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionRead(){

		echo json_encode( Mensagem::model()->read($_POST['mensagemId'], $_POST['usuarioId']) );
	}

	public function actionAdd(){

		if( !empty ( $_POST['Mensagem'] ) && !empty ( $_POST['Usuario_id'] ) && !empty ( $_POST['Dialogo_id'] ) && !is_int ( $_POST['interna'] ))
			echo json_encode( Mensagem::model()->novo( $_POST['Mensagem'],$_POST['Usuario_id'],$_POST['Dialogo_id'], $_POST['interna']) );
		else
			echo json_encode(array(
			'msgReturn'		=> empty($_POST['Mensagem']['conteudo']) . '-' . empty($_POST['Usuario_id']) . '-' .  empty($_POST['Dialogo_id']) . '-' .  is_int($_POST['interna']),
			'classNotify'	=>'error'
		));
	}
}