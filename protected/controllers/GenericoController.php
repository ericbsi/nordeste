<?php

class GenericoController extends Controller {

    public function actionIndex() {
        $this->layout = '//layouts/main_sigac_template';

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css');
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpalette.css');
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/generico/fn-grid-parceiros.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/motivoDeNegacao/fn-grid-motivos-negacao.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpalette.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);

        $this->render('index');
    }
    
    public function actionMotivoNegacao() {
        $this->layout = '//layouts/main_sigac_template';

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css');
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpalette.css');
        $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/motivoDeNegacao/fn-grid-motivos-negacao.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpalette.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);

        $this->render('motivo-negacao');
    }

    public function actionSalvarStatus() {
        $idStatus = $_POST['idStatus'];
        $idLoja = $_POST['idLoja'];

        $alterar = PreLoja::model()->find('id = ' . $idLoja);

        if ($alterar !== null) {
            $alterar->StatusPreLoja_id = $idStatus;
            if ($alterar->update()) {
                $retorno = array(
                    'error' => false,
                    'msg' => "Status alterado com sucesso",
                    'type' => "success"
                );
            } else {
                $retorno = array(
                    'error' => true,
                    'msg' => "Não foi possível alterar o status",
                    'type' => "error"
                );
            }
        } else {
            $retorno = array(
                'error' => true,
                'msg' => "Não foi possível alterar o status",
                'type' => "error"
            );
        }
        
        echo json_encode($retorno);
    }

    public function actionListarParceiros() {
        $rows = array();
        $statusID = $_POST['statusFiltro'];
        if($statusID !== null && $statusID !== ''){
            $parceiros = PreLoja::model()->findAll('StatusPreLoja_id = '. $statusID);
        }else{
            $parceiros = PreLoja::model()->findAll();
        }

        foreach ($parceiros as $par) {
            $status_atual = "Status Atual: " . $par->statusPreLoja->nome_status;
            $status = "<input type='hidden' value='" . $par->id . "'>"
                    . CHtml::dropDownList('StatusPre', 'StatusPre', CHtml::listData(StatusPreLoja::model()->findAll(), 'id', 'nome_status'), array('class' => 'form-control search-select select2 selectSt', 'prompt' => $status_atual, 'id' => 'selectStatus'));
            
            if ($par->obs !== null && $par->obs !== '' && isset($par->obs)) {
                $input = "<label>".$par->obs."</label>" . "<br><br><input type='hidden' value='".$par->id."'><button id='edit_obs' class='btn btn-xs btn-success'>Editar</button>";
            } else {
                $input = "<input type='hidden' value='".$par->id."'><button id='edit_obs' class='btn btn-xs btn-success'>Editar</button>";
            }
            
            $row = array(
                "nome" => $par->nome,
                "status" => $status,
                "cidade" => $par->cidade,
                "email" => $par->email,
                "tel" => $par->telefone,
                "obs" => $input
            );

            $rows[] = $row;
        }

        echo json_encode(array(
            "data" => $rows
        ));
    }
    
    public function actionSalvarMotivos() {
        $nomeMotivo = $_POST['descricao'];

        $motivoSave = new MotivoDeNegacao;
        $motivoSave->descricao = strtoupper($nomeMotivo);
        $motivoSave->habilitado = 1;
        $motivoSave->data_cadastro = date('Y-m-d H:i:s');

        if ($motivoSave->save()) {
            $retorno = array(
                'msg' => "Motivo de negação cadastrado com sucesso",
                'type' => "success"
            );
        } else {
            $retorno = array(
                'msg' => "Não foi possível cadastrar esse motivo de negação",
                'type' => "error"
            );
        }

        echo json_encode($retorno);
    }
    
    public function actionGetMotivosDeNegacao() {
        $motivos = MotivoDeNegacao::model()->findAll();
        $rows = [];
        foreach ($motivos as $m) {
            $row = array(
                'descricao' => $m->descricao
            );
            $rows[] = $row;
        }
        
        echo json_encode(array('data' => $rows));
    }


    public function actionEditObs(){
        $obs = $_POST['alt_obs'];
        $id = $_POST['plID'];
        
        $parc = PreLoja::model()->findByPk($id);
        $parc->obs = $obs;
        
        if ($parc->update()) {
            $retorno = array(
                'error' => false,
                'msg' => "Observação salva!",
                'type' => "success"
            );
        } else {
            $retorno = array(
                'error' => true,
                'msg' => "Não foi salvar a observação",
                'type' => "error"
            );
        }

        echo json_encode($retorno);
    }
    
    public function actionNovaLoja() {
        $nomeLoja = $_POST['nomeLoja'];
        $cidadeLoja = $_POST['cidLoja'];
        $emailLoja = $_POST['emailLoja'];
        $telLoja = $_POST['telLoja'];

        $lojaSave = new PreLoja;
        $lojaSave->nome = strtoupper($nomeLoja);
        $lojaSave->cidade = strtoupper($cidadeLoja);
        $lojaSave->email = $emailLoja;
        $lojaSave->telefone = $telLoja;
        $lojaSave->StatusPreLoja_id = 1;

        if ($lojaSave->save()) {
            $retorno = array(
                'error' => false,
                'msg' => "Parceiro cadastrado com sucesso",
                'type' => "success"
            );
        } else {
            $retorno = array(
                'error' => true,
                'msg' => "Não foi possível cadastrar o parceiro",
                'type' => "error"
            );
        }

        echo json_encode($retorno);
    }

    public function actionNovoStatus() {
        $nomeStatus = $_POST['nomeStatus'];

        $statusSave = new StatusPreLoja;
        $statusSave->nome_status = strtoupper($nomeStatus);
        $statusSave->obs = "Novo status";

        if ($statusSave->save()) {
            $retorno = array(
                'msg' => "Status cadastrado com sucesso",
                'type' => "success"
            );
        } else {
            $retorno = array(
                'msg' => "Não foi possível cadastrar esse status",
                'type' => "error"
            );
        }

        echo json_encode($retorno);
    }

    // Uncomment the following methods and override them if needed
    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }

      public function actions()
      {
      // return external action classes, e.g.:
      return array(
      'action1'=>'path.to.ActionClass',
      'action2'=>array(
      'class'=>'path.to.AnotherActionClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }
     */
}
