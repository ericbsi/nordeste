<?php

class DadosBancariosController extends Controller {

    public function actionIndex() {

        $this->render('index');
    }

    public function actionLoad() {

        $id = $_GET['entity_id'];
        echo json_encode(DadosBancarios::model()->loadFormEdit($id));
    }

    public function actionUpdate() {

        $action = $_POST['controller_action'];
        $msgReturn = "";

        if ($action == 'add') {
            DadosBancarios::model()->novo($_POST['DadosBancarios'], $_POST['Cliente_id']);
            $msgReturn = "Dados cadastrados com sucesso!";
        } else {
            DadosBancarios::model()->atualizar($_POST['DadosBancarios'], $_POST['dados_bancarios_id']);
            $msgReturn = "Dados atualizados com sucesso!";
        }

        echo json_encode(array(
            'msgReturn' => $msgReturn
        ));
    }

   public function actionContasEmpresa()
   {

      $this->layout  = '//layouts/main_sigac_template'   ;
      $baseUrl       = Yii::app()->baseUrl               ;
      $cs            = Yii::app()->getClientScript()     ;

      $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css'  );
      $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css'     );
      $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css'          );
      $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpalette.css'                   );
      $cs->registerCssFile($baseUrl . '/css/bootstrap-colorpicker.css'                    );

      $cs->registerScriptFile($baseUrl . '/js/jquery.min.js'                                                      );
      $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js'             , CClientScript::POS_END   );
      $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js'              , CClientScript::POS_END   );
      $cs->registerScriptFile($baseUrl . '/js/select2.min.js'                          , CClientScript::POS_END   );
      $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js'                   , CClientScript::POS_END   );
      $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js'                      , CClientScript::POS_END   );
      $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js'               , CClientScript::POS_END   );
      $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js'                   , CClientScript::POS_END   );
      $cs->registerScriptFile($baseUrl . '/js/additional-methods.js'                   , CClientScript::POS_END   );
      $cs->registerScriptFile($baseUrl . '/js/dadosBancarios/fn-gridContasEmpresa.js'  , CClientScript::POS_END   );
      $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js'                    , CClientScript::POS_END   );
      $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js'                       , CClientScript::POS_END   );
      $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js'                , CClientScript::POS_END   );
      $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpicker.js'                , CClientScript::POS_END   );
      $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpalette.js'               , CClientScript::POS_END   );
      
      $this->render('contasEmpresa');
      
   }
   
   public function actionListarContas()
   {
       
       $dados = [];
       
       $eHasDB = EmpresaHasDadosBancarios::model()->findAll();
       
       foreach ($eHasDB as $eHDB)
       {
           
           /*$data            = new DateTime($eHDB->dadosBancarios->data_abertura);
           $dataAbertura    = $data->format('d/m/Y');*/
           
           $dados[] = array (
                                'banco'         => $eHDB->dadosBancarios->banco->nome   ,
                                'agencia'       => $eHDB->dadosBancarios->agencia       ,
                                'conta'         => $eHDB->dadosBancarios->numero        ,
                                'operacao'      => $eHDB->dadosBancarios->operacao      ,
                                'dataAbertura'  => $eHDB->dadosBancarios->data_abertura , //$dataAbertura                        ,
                                'btn'           => ''
                            );
           
       }
       
       echo json_encode (
                            array   (
                                        'recordsTotal'      => count($dados)  ,
                                        'recordsFiltered'   => count($dados)  ,
                                        'data'              => $dados
                                    )
                        );
       
   }
   
   public function actionSalvarContaEmpresa()
   {
       
        $arrayReturn = array(
                                'hasErrors'     => 0                                            ,
                                'msg'           => 'Dados bancários cadastrados com sucesso.'   ,
                                'pntfyClass'    => 'success'
                            );
       
        if  (
                isset($_POST['bancoConta'   ]   ) && 
                isset($_POST['agenciaConta' ]   ) && 
                isset($_POST['numeroConta'  ]   )
            )
        {
            
            $transaction = Yii::app()->db->beginTransaction();
            
            $data           = new DateTime($_POST['dataAberturaConta'])     ;
            $dataAbertura   = $data->format('d/m/Y')                        ;
           
            $dadosBancarios                         = new DadosBancarios            ;
            $dadosBancarios->habilitado             = 1                             ;
            $dadosBancarios->data_cadastro          = date('Y-m-d H:i:s')           ;
            $dadosBancarios->numero                 = $_POST['numeroConta'      ]   ;
            $dadosBancarios->Banco_id               = $_POST['bancoConta'       ]   ;
            $dadosBancarios->agencia                = $_POST['agenciaConta'     ]   ;
            $dadosBancarios->operacao               = $_POST['operacaoConta'    ]   ;
            $dadosBancarios->data_abertura          = $dataAbertura                 ;
            $dadosBancarios->Tipo_Conta_Bancaria_id = 1                             ;
            
            if($dadosBancarios->save())
            {
                
                $empresaHasDados                        = new EmpresaHasDadosBancarios  ;
                $empresaHasDados->Empresa_id            = 5                             ;
                $empresaHasDados->Dados_Bancarios_id    = $dadosBancarios->id           ;
                $empresaHasDados->habilitado            = 1                             ;
                $empresaHasDados->data_cadastro         = date('Y-m-d H:i:s')           ;
                
                if($empresaHasDados->save())
                {
                    $transaction->commit();
                }
                else
                {
                    
                    ob_start()                              ;
                    var_dump($empresaHasDados->getErrors()) ;
                    $result = ob_get_clean()                ;
                    
                    $arrayReturn = array(
                                            'hasErrors'     =>  1                                                       ,
                                            'msg'           =>  'Erro ao salvar a conta. EmpresaHasDadosBancario: ' . 
                                                                $result                                                 ,
                                            'pntfyClass'    =>  'error'
                                        );
                    
                    $transaction->rollBack();
                    
                }
                
            }
            else
            {
                    
                ob_start()                              ;
                var_dump($dadosBancarios->getErrors())  ;
                $result = ob_get_clean()                ;

                $arrayReturn = array(
                                        'hasErrors'     =>  1                                           ,
                                        'msg'           =>  'Erro ao salvar a conta. DadosBancario: ' . 
                                                            $result                                     ,
                                        'pntfyClass'    =>  'error'
                                    );
                    
                $transaction->rollBack();
                
            }
            
        }
        else
        {

            $arrayReturn = array(
                                    'hasErrors'     =>  1                                           ,
                                    'msg'           =>  'Erro ao salvar a conta. Dados não vieram ' ,
                                    'pntfyClass'    =>  'error'
                                );
        }
        
        echo json_encode($arrayReturn);
       
   }
}
