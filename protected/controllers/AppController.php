<?php

class AppController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionAppConnectivityStatus()
	{
		echo json_encode(array('requestStatus'=>'alive'));
	}
}