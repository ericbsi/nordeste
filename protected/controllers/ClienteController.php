<?php

class ClienteController extends Controller {

    public $layout = '//layouts/main_sigac_template';

    public function filters() {
        return array(
                'accessControl',
                'postOnly + delete',
        );
    }

    public function accessRules() {

        return array(
                array('allow',
                        'actions' => array('buscarHistorico', 'historicoIntegrado', 'importarClientes', 'pessoaEditAttr', 'clienteIdadeApto', 'checkStatusDoCadastro', 'clientePersist', 'analisarCadastro', 'getAnexos', 'getFamiliares', 'getReferencias', 'getDadosSociais', 'getDocumentos', 'getEmails', 'getTelefones', 'getDadosProfissionais', 'getDadosBancarios', 'getEnderecos', 'documentoCadastrado', 'renderFormEditEndereco', 'renderFormEditDadosProfissionais', 'addContatoElement', 'renderFormAddContactElement', 'changeClienteContatoAttribute', 'renderFormChangeContato', 'changeClienteAttribute', 'renderFormChangeClienteAttribute', 'getClienteCPF', 'create', 'update', 'admin', 'delete', 'index', 'view'),
                        'users' => array('@'),
                ),
                /* Crediaristas */
                array('allow',
                        'actions' => array('situacaoCadastralCPF', 'getCartao', 'getCvc'),
                        'users' => array('@'),
                        'expression' => 'Yii::app()->session["usuario"]->role == "crediarista" || '
                        . 'Yii::app()->session["usuario"]->tipo_id == 14 '
                ),
                /* Analistas */
                array('allow',
                        'actions' => array('consultarScore', 'historicoPropostas', 'listarAnalisesCliente', 'listarClientes', 'getClientes', 'editar'),
                        'users' => array('@'),
                        'expression' => 'Yii::app()->session["usuario"]->role == "analista_de_credito" || Yii::app()->session["usuario"]->role == "consultar_cliente"'
                ),
                /* Analistas, Crediaristas */
                array('allow',
                        'actions' => array('add'),
                        'users' => array('@'),
                        'expression' => 'Yii::app()->session["usuario"]->role == "analista_de_credito" || Yii::app()->session["usuario"]->role == "crediarista"'
                ),
                array('deny', // deny all users
                        'users' => array('*'),
                ),
        );
    }

    public function actionLista() {
        /*
          ini_set('memory_limit', '-1');
          set_time_limit(2700000000000000000000000000000000);

          $baseUrl = Yii::app()->baseUrl;
          $cs = Yii::app()->getClientScript();

          $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
          $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
          $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css');

          $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');

          $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
          $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
          $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
          $cs->registerScriptFile($baseUrl . '/js/fn-mailing-grid.js', CClientScript::POS_END);

          $this->render('mailing');
         */
    }

    public function actionSituacaoCadastralCPF() {
        echo json_encode(Cliente::model()->pessoaSituacaoCadastral($_POST['cpf']));
    }

    public function actionGetCartao() {
        echo json_encode(Cliente::model()->pessoaGetCartao($_POST['num_cartao'], $_POST['cpf']));
    }

    public function actionGetCvc() {
        echo json_encode(Cliente::model()->pessoaGetCvc($_POST['num_cartao'], $_POST['cvc_dig']));
    }

    public function actionConsultarScore()
    {
        $id_cliente             = $_POST['id_cl'];
        $cpf_cliente            = $_POST['cpf_cl'];

        $scores                 = new ConsultaCliente;
        $scores->consultarScore($cpf_cliente, 1, $id_cliente);

        $classe                 = $scores->classe;
        $risco                  = 'Desconhecido';
        $cssClass               = "alert-danger";

        if ($scores->probabilidade > 0.10 && $scores->probabilidade <= 10.20)
        {
            $risco              = 'Baixo';
            $cssClass           = "alert-success";
        }

        if ($scores->probabilidade > 10.30 && $scores->probabilidade <= 20.7)
        {
            $risco              = 'Médio';
            $cssClass           = "alert-warning";
        }

        if ($scores->probabilidade > 20.7 && $scores->probabilidade <= 98.8)
        {
            $risco              = 'Alto';
            $cssClass           = "alert-danger";
        }

        $fscore                 = $scores->score;

        $rest                   = 'Desconhecido';

        if ($scores->restricao  == 'true')
        {
            $rest               = 'SIM';
        }
        else
        {
            if ($scores->restricao == 'OFF')
            {
                $rest           = 'OFF';
            }
            else
            {
                $rest           = 'NÃO';
            }
        }

        $retorno            = array(
                'classe'    => $classe,
                'risco'     => $risco,
                'score'     => $fscore,
                'rest'      => $rest,
                'css'       => $cssClass,
                'proc'      => $scores->procedencia
        );

        echo json_encode(
            array
            (
                'data'      => $retorno
            )
        );
    }

    public function actionChangeClienteContatoAttribute() {

        $tipo = $_POST['tipo'];
        $id = $_POST['id'];

        $retorno = array();

        if ($tipo == 'email') {

            $emailVal = $_POST['email'];

            $email = Email::model()->findByPk($id);
            $email->email = $emailVal;
            $email->update();
        } else {
            $numero = $_POST['numero'];
            $ramal = $_POST['ramal'];
            $tipo = $_POST['tipo'];

            $telefone = Telefone::model()->findByPk($id);
            $telefone->numero = $numero;
            $telefone->ramal = $ramal;
            $telefone->Tipo_Telefone_id = $tipo;
            $telefone->update();
        }
    }

    public function actionChangeClienteAttribute() {

        $util = new Util;
        $attribute = $_POST['attribute'];
        $attribute_value = $_POST['attribute_value'];
        $pessoa_id = $_POST['pessoa_id'];

        if ($attribute == 'nascimento') {

            $attribute_value = $util->view_date_to_bd($attribute_value);
        }

        $pessoa = Pessoa::model()->findByPk($pessoa_id);

        $pessoa->setAttribute($attribute, $attribute_value);
        $pessoa->update();

        if ($attribute == 'nascimento')
            echo $util->bd_date_to_view($attribute_value);
        else
            echo $attribute_value;
    }

    public function actionRenderFormChangeClienteAttribute() {

        $this->layout = '//layouts/empty_template';
        $view = $_GET['view'];
        $cliente_id = $_GET['cliente_id'];
        $cliente = Cliente::model()->findByPk($cliente_id);

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');

        $this->render($view, array(
                'cliente' => $cliente
            )
        );
    }

    public function actionAddContatoElement() {

        $arrReturn = array();

        $tipo = $_POST['tipo'];
        $contato_id = $_POST['contato_id'];

        if ($tipo == 'telefone') {
            $contatoHasTelefone = new ContatoHasTelefone;
            $contatoHasTelefone->Contato_id = $contato_id;
            $contatoHasTelefone->data_cadastro = date('Y-m-d H:i:S');
            $contatoHasTelefone->data_cadastro_br = date('d/m/Y H:i:S');

            $telefone_numero = $_POST['numero'];
            $telefone_tipo = $_POST['tipo_telefone'];
            $telefone_ramal = $_POST['ramal'];

            $newTelefone = new Telefone;
            $newTelefone->numero = $telefone_numero;
            $newTelefone->data_cadastro = date('Y-m-d H:i:S');
            $newTelefone->data_cadastro_br = date('d/m/Y H:i:S');
            $newTelefone->ramal = $telefone_ramal;
            $newTelefone->Tipo_Telefone_id = $telefone_tipo;

            if ($newTelefone->save()) {
                $contatoHasTelefone->Telefone_id = $newTelefone->id;
                $contatoHasTelefone->save();

                $arrReturn['telefoneId'] = $newTelefone->id;
                $arrReturn['telefoneNumero'] = $newTelefone->numero;
                $arrReturn['telefoneTipo'] = $newTelefone->tipoTelefone->tipo;
                $arrReturn['telefoneRamal'] = $newTelefone->ramal;
            }
        } else {

            $contatoHasEmail = new ContatoHasEmail;
            $contatoHasEmail->Contato_id = $contato_id;
            $contatoHasEmail->data_cadastro = date('Y-m-d H:i:S');
            $contatoHasEmail->data_cadastro_br = date('d/m/Y H:i:S');

            $emailV = $_POST['email'];
            $email = new Email;
            $email->email = $emailV;
            $email->data_cadastro = date('Y-m-d H:i:S');
            $email->data_cadastro_br = date('d/m/Y H:i:S');

            if ($email->save()) {
                $contatoHasEmail->Email_id = $email->id;
                $contatoHasEmail->save();

                $arrReturn['emailId'] = $email->id;
                $arrReturn['email'] = $email->email;
            }
        }

        echo json_encode($arrReturn);
    }

    public function actionImportarClientes() {

        $this->layout = '//layouts/main_sigac_template';
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');

        $total = 0;

        if (isset($_POST['Importacao'])) {
            $total = Cliente::model()->importarCPF($_POST['Importacao']);
        }

        $this->render('importarClientes', ['total' => $total]);
    }

    public function actionRenderFormAddContactElement() {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');

        $this->layout = '//layouts/empty_template';
        $view = $_GET['view'];
        $tipo = $_GET['tipo'];
        $contato_id = $_GET['contato_id'];

        if ($tipo == 'telefone') {
            $entity = new Telefone;
        } else {
            $entity = new Email;
        }

        $this->render($view, array(
                'entity' => $entity,
                'contato_id' => $contato_id,
            )
        );
    }

    public function actionRenderFormChangeContato() {

        $this->layout = '//layouts/empty_template';

        $view = $_GET['view'];
        $tipo = $_GET['tipo'];
        $id = $_GET['id'];

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');

        if ($tipo == 'telefone') {
            $entity = Telefone::model()->findByPk($id);
        } else {
            $entity = Email::model()->findByPk($id);
        }

        $this->render($view, array(
                'entity' => $entity
            )
        );
    }

    public function actionClientePersist() {
        echo json_encode(Cliente::model()->persist($_POST['CPF'], $_POST['Cadastro'], $_POST['Pessoa'], $_POST['RG'], $_POST['DadosBancarios'], $_POST['ControllerAction']));
    }

    public function actionAdd() {

        if (!empty($_POST['Pessoa']['nome']) &&
            !empty($_POST['CPFCliente']['numero']) &&
            !empty($_POST['DocumentoCliente']['numero']) &&
            !empty($_POST['Cadastro']['nome_da_mae'])) {

            Cliente::model()->novo($_POST['Pessoa'], $_POST['CPFCliente'], $_POST['DocumentoCliente'], $_POST['Cadastro']);
        }
    }

    public function actionBuscarHistorico() {
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/fn-buscar-historico.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js', CClientScript::POS_END);

        $this->render('/cliente/buscarHistorico');
    }

    public function actionHistoricoIntegrado() {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $nome = NULL;
        $cpf = NULL;

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/fn-buscar-historico.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js', CClientScript::POS_END);

        $this->render('/cliente/historicoIntegrado', [ 'propostas' => Cliente::model()->historicoIntegrado($_POST['cpf'])]);
    }

    public function actionHistoricoPropostas() {
        if (isset($_POST['cpf'])) {
            $cpf = $_POST['cpf'];
            $row = [];
            $rows = [];

            $sql = "SELECT 

                    Pn.id, 
                    Pn.codigo, 
                    Pn.data_cadastro_br,
                    Fn.nome_fantasia,
                    En.cidade,
                    En.uf,
                    Un.nome_utilizador,
                    Pn.valor,
                    Pn.valor_entrada,
                    Pn.segurada,
                    Pn.valor_final, 
                    Pn.qtd_parcelas,
                    Pn.valor_parcela, 
                    Pn.Status_Proposta_id, 
                    SPn.status 

                    FROM       nordeste2.Analise_de_Credito 	AS ACn
                    INNER JOIN nordeste2.Cliente             	AS Cn		ON Cn.id = ACn.Cliente_id 		AND Cn.habilitado
                    INNER JOIN nordeste2.Pessoa			AS Psn		ON Psn.id = Cn.Pessoa_id 		AND Psn.habilitado
                    INNER JOIN nordeste2.Pessoa_has_Documento	AS PHDn		ON PHDn.Pessoa_id = Psn.id 		AND PHDn.habilitado
                    INNER JOIN nordeste2.Documento		As Dn		ON Dn.Tipo_documento_id = 1 		AND Dn.id = PHDn.Documento_id 	AND Dn.numero = '" . $cpf . "' AND Dn.habilitado
                    INNER JOIN nordeste2.Proposta		AS Pn		ON Pn.Analise_de_credito_id = ACn.id 	AND ACn.habilitado		AND Pn.Status_Proposta_id IN (2,3,7) 
                    INNER JOIN nordeste2.Status_Proposta		AS SPn		ON SPn.id = Pn.Status_Proposta_id
                    INNER JOIN nordeste2.Filial			AS Fn		ON Fn.id = ACn.Filial_id		AND Fn.habilitado
                    INNER JOIN nordeste2.Usuario			AS Un 		ON Un.id = ACn.Usuario_id		
                    INNER JOIN nordeste2.Filial_has_Endereco	AS FEn		ON FEn.Filial_id = Fn.id		AND FEn.habilitado
                    INNER JOIN nordeste2.Endereco		AS En		ON En.id = FEn.Endereco_id		AND En.habilitado";

            $propostas = Yii::app()->db->createCommand($sql)->queryAll();

            foreach ($propostas as $prop) {
                if ($prop['Status_Proposta_id'] == 2) {
                    $btn_det = "<div class='visible-md visible-lg hidden-sm hidden-xs'>                                
                                    <a data-toggle='modal' 
                                        href='#responsive' 
                                        class='btn btn-xs btn-green tooltips btn-proposta-more-details' 
                                        data-placement='top' 
                                        data-original-title='Parcelas' 
                                        modal-iframe-uri=" . Yii::app()->baseUrl . "/proposta/maisDetalhes?id=" . $prop['id'] . "&banco=2>
                                        <i class='clip-plus-circle'> Parcelas</i>
                                    </a>
                                </div>";
                } else {
                    $btn_det = "";
                }
                
                if ($prop['Status_Proposta_id'] == 2) {
                    $status = '<span style="width: 100%!important;" class="btn btn-xs btn-success">' . $prop['status'] . '</span>';
                }
                else if ($prop['Status_Proposta_id'] == 7) {
                    $status = '<span style="width: 100%!important;" class="btn btn-xs btn-warning">' . $prop['status'] . '</span>';
                } else {
                    $status = '<span style="width: 100%!important;" class="btn btn-xs btn-danger">' . $prop['status'] . '</span>';
                }
                
                $btn_conv = "<div class='visible-md visible-lg hidden-sm hidden-xs'>    
                                <a data-toggle='modal' 
                                    href='#conversa' 
                                    class='btn btn-xs btn-blue tooltips btn-proposta-conversa' 
                                    data-placement='top' 
                                    data-original-title='Conversa' 
                                    modal-iframe-uri=" . Yii::app()->baseUrl . "/proposta/maisConversa?id=" . $prop['id'] . "&banco=2>
                                    <i class='clip-plus-circle'> Conversa</i>
                                </a>
                             </div>";

                if ($prop['segurada'] == 1) {
                    $seguro = (($prop['valor'] - $prop['valor_entrada']) / 100) * 10;
                } else {
                    $seguro = "0.00";
                }

                $row = array(
                        "conversa" => $btn_conv,
                        "codigo" => $prop['codigo'],
                        "data_cadastro" => $prop['data_cadastro_br'],
                        "parceiro" => $prop['nome_fantasia'] . " - " . $prop['cidade'] . "/" . $prop['uf'],
                        "crediarista" => $prop['nome_utilizador'],
                        "valor" => number_format($prop['valor'], 2, ',', '.'),
                        "entrada" => number_format($prop['valor'], 2, ',', '.'),
                        "seguro" => number_format($seguro, 2, ',', '.'),
                        "valor_final" => number_format($prop['valor_final'], 2, ',', '.'),
                        "qtd_parc" => $prop['qtd_parcelas'] . "x de " . $prop['valor_parcela'],
                        "status" => $status,
                        "detalhes" => $btn_det
                );

                $rows[] = $row;
            }

            $sql2 = "SELECT 

                    Pn.id, 
                    Pn.codigo, 
                    Pn.data_cadastro_br,
                    Fn.nome_fantasia,
                    En.cidade,
                    En.uf,
                    Un.nome_utilizador,
                    Pn.valor,
                    Pn.valor_entrada,
                    Pn.segurada,
                    Pn.valor_final, 
                    Pn.qtd_parcelas,
                    Pn.valor_parcela, 
                    Pn.Status_Proposta_id, 
                    SPn.status 

                    FROM       beta.Analise_de_Credito 	AS ACn
                    INNER JOIN beta.Cliente             	AS Cn		ON Cn.id = ACn.Cliente_id 		AND Cn.habilitado
                    INNER JOIN beta.Pessoa			AS Psn		ON Psn.id = Cn.Pessoa_id 		AND Psn.habilitado
                    INNER JOIN beta.Pessoa_has_Documento	AS PHDn		ON PHDn.Pessoa_id = Psn.id 		AND PHDn.habilitado
                    INNER JOIN beta.Documento                  As Dn		ON Dn.Tipo_documento_id = 1 		AND Dn.id = PHDn.Documento_id 	AND Dn.numero = '" . $cpf . "' AND Dn.habilitado
                    INNER JOIN beta.Proposta                   AS Pn		ON Pn.Analise_de_credito_id = ACn.id 	AND ACn.habilitado		AND ( Pn.Status_Proposta_id = 2 OR Pn.Status_Proposta_id = 3 )
                    INNER JOIN beta.Status_Proposta		AS SPn		ON SPn.id = Pn.Status_Proposta_id
                    INNER JOIN beta.Filial			AS Fn		ON Fn.id = ACn.Filial_id		AND Fn.habilitado
                    INNER JOIN beta.Usuario			AS Un 		ON Un.id = ACn.Usuario_id		
                    INNER JOIN beta.Filial_has_Endereco	AS FEn		ON FEn.Filial_id = Fn.id		AND FEn.habilitado
                    INNER JOIN beta.Endereco                   AS En		ON En.id = FEn.Endereco_id		AND En.habilitado";

            $propostas2 = Yii::app()->db->createCommand($sql2)->queryAll();

            foreach ($propostas2 as $prop) {
                if ($prop['Status_Proposta_id'] == 2) {
                    $btn_det = "<div class='visible-md visible-lg hidden-sm hidden-xs'>                                
                                    <a data-toggle='modal' 
                                        href='#responsive' 
                                        class='btn btn-xs btn-green tooltips btn-proposta-more-details' 
                                        data-placement='top' 
                                        data-original-title='Parcelas' 
                                        modal-iframe-uri=" . Yii::app()->baseUrl . "/proposta/maisDetalhes?id=" . $prop['id'] . "&banco=1>
                                        <i class='clip-plus-circle'> Parcelas</i>
                                    </a>
                                </div>";
                } else {
                    $btn_det = "";
                }

                if ($prop['Status_Proposta_id'] == 2) {
                    $status = '<span style="width: 100%!important;" class="btn btn-xs btn-success">' . $prop['status'] . '</span>';
                } else {
                    $status = '<span style="width: 100%!important;" class="btn btn-xs btn-danger">' . $prop['status'] . '</span>';
                }

                $btn_conv = "<div class='visible-md visible-lg hidden-sm hidden-xs'>    
                                <a data-toggle='modal' 
                                    href='#conversa' 
                                    class='btn btn-xs btn-blue tooltips btn-proposta-conversa' 
                                    data-placement='top' 
                                    data-original-title='Conversa' 
                                    modal-iframe-uri=" . Yii::app()->baseUrl . "/proposta/maisConversa?id=" . $prop['id'] . "&banco=1>
                                    <i class='clip-plus-circle'> Conversa</i>
                                </a>
                             </div>";

                if ($prop['segurada'] == 1) {
                    $seguro = (($prop['valor'] - $prop['valor_entrada']) / 100) * 10;
                } else {
                    $seguro = "0.00";
                }

                $row = array(
                        "conversa" => $btn_conv,
                        "codigo" => $prop['codigo'],
                        "data_cadastro" => $prop['data_cadastro_br'],
                        "parceiro" => $prop['nome_fantasia'] . " - " . $prop['cidade'] . "/" . $prop['uf'],
                        "crediarista" => $prop['nome_utilizador'],
                        "valor" => number_format($prop['valor'], 2, ',', '.'),
                        "entrada" => number_format($prop['valor'], 2, ',', '.'),
                        "seguro" => number_format($seguro, 2, ',', '.'),
                        "valor_final" => number_format($prop['valor_final'], 2, ',', '.'),
                        "qtd_parc" => $prop['qtd_parcelas'] . "x de " . $prop['valor_parcela'],
                        "status" => $status,
                        "detalhes" => $btn_det
                );

                $rows[] = $row;
            }

            echo json_encode(array("data" => $rows));
        }
    }

    public function actionListarAnalisesCliente() {

        if (isset($_POST['cpf'])) {
            $cpf = $_POST['cpf'];
            $row = [];
            $rows = [];

            $sql = "SELECT Ps.id, Ps.codigo, Ps.data_cadastro_br, Ps.valor, Ps.valor_final, Ps.qtd_parcelas, SCs.id AS 'sc_id', Ps.Status_Proposta_id, SPs.status FROM beta.Analise_de_Credito AS ACs

                    INNER JOIN beta.Cliente				AS Cs		ON Cs.id = ACs.Cliente_id 		AND Cs.habilitado
                    INNER JOIN beta.Pessoa				AS Pss		ON Pss.id = Cs.Pessoa_id 		AND Pss.habilitado
                    INNER JOIN beta.Pessoa_has_Documento		AS PHDs		ON PHDs.Pessoa_id = Pss.id 		AND PHDs.habilitado
                    INNER JOIN beta.Documento				As Ds		ON Ds.Tipo_documento_id = 1             AND Ds.id = PHDs.Documento_id 	AND Ds.numero = '" . $cpf . "' AND Ds.habilitado
                    INNER JOIN beta.Proposta				AS Ps		ON Ps.Analise_de_credito_id = ACs.id 	AND ACs.habilitado		AND Ps.Status_Proposta_id IN (2,7) 
                    INNER JOIN beta.Status_Proposta			AS SPs		ON SPs.id = Ps.Status_Proposta_id
                    LEFT  JOIN beta.Solicitacao_de_Cancelamento 	AS SCs		ON SCs.Proposta_id = Ps.id 		AND SCs.aceite 			AND SCs.habilitado";

            $propostas = Yii::app()->db->createCommand($sql)->queryAll();

            foreach ($propostas as $prop) {
                if ($prop['Status_Proposta_id'] == 2) {
                    $button = "<div class='visible-md visible-lg hidden-sm hidden-xs'>                                
                                <a data-toggle='modal' href='#responsive' class='btn btn-xs btn-green tooltips btn-proposta-more-details' data-placement='top' data-original-title='Parcelas' modal-iframe-uri='" . Yii::app()->baseUrl . "/proposta/maisDetalhes?id=" . $prop['id'] . "&banco=1'>
                                    <i class='clip-plus-circle'></i>
                                </a>
                               </div>";
                } 
                elseif ($prop['Status_Proposta_id'] == 7) 
                {
                    $button = "<div class='visible-md visible-lg hidden-sm hidden-xs'>                                
                                <a data-toggle='modal' href='#responsive' class='btn btn-xs btn-orange tooltips btn-proposta-more-details' data-placement='top' data-original-title='Parcelas' modal-iframe-uri='" . Yii::app()->baseUrl . "/proposta/maisDetalhes?id=" . $prop['id'] . "&banco=1'>
                                    <i class='clip-plus-circle'></i>
                                </a>
                               </div>";
                } 
                else 
                {
                    $button = "";
                }
                $row = array(
                        "prop_id" => $prop['id'],
                        "codigo" => $prop['codigo'],
                        "data_cadastro" => $prop['data_cadastro_br'],
                        "valor" => number_format($prop['valor'], 2, ',', '.'),
                        "valor_final" => number_format($prop['valor_final'], 2, ',', '.'),
                        "qtd_parc" => $prop['qtd_parcelas'],
                        "sc_id" => $prop['sc_id'],
                        "status" => $prop['status'],
                        "status_id" => $prop['Status_Proposta_id'],
                        "button" => $button
                );

                $rows[] = $row;
            }

            $sql2 = "SELECT Pn.id, Pn.codigo, Pn.data_cadastro_br, Pn.valor, Pn.valor_final, Pn.qtd_parcelas, SCn.id AS 'sc_id', Pn.Status_Proposta_id, SPn.status FROM nordeste2.Analise_de_Credito AS ACn

                    INNER JOIN nordeste2.Cliente                         AS Cn		ON Cn.id = ACn.Cliente_id 		AND Cn.habilitado
                    INNER JOIN nordeste2.Pessoa				AS Psn		ON Psn.id = Cn.Pessoa_id 		AND Psn.habilitado
                    INNER JOIN nordeste2.Pessoa_has_Documento		AS PHDn		ON PHDn.Pessoa_id = Psn.id 		AND PHDn.habilitado
                    INNER JOIN nordeste2.Documento			As Dn		ON Dn.Tipo_documento_id = 1 		AND Dn.id = PHDn.Documento_id 	AND Dn.numero = '" . $cpf . "' AND Dn.habilitado
                    INNER JOIN nordeste2.Proposta			AS Pn		ON Pn.Analise_de_credito_id = ACn.id 	AND ACn.habilitado		AND Pn.Status_Proposta_id IN (2,7) 
                    INNER JOIN nordeste2.Status_Proposta			AS SPn		ON SPn.id = Pn.Status_Proposta_id
                    LEFT  JOIN nordeste2.Solicitacao_de_Cancelamento 	AS SCn		ON SCn.Proposta_id = Pn.id 		AND SCn.aceite 			AND SCn.habilitado";

            $propostas2 = Yii::app()->db->createCommand($sql2)->queryAll();

            foreach ($propostas2 as $prop) {
                if ($prop['Status_Proposta_id'] == 2) {
                    $button = "<div class='visible-md visible-lg hidden-sm hidden-xs'>                                
                                <a data-toggle='modal' href='#responsive' class='btn btn-xs btn-green tooltips btn-proposta-more-details' data-placement='top' data-original-title='Parcelas' modal-iframe-uri='" . Yii::app()->baseUrl . "/proposta/maisDetalhes?id=" . $prop['id'] . "&banco=2'>
                                    <i class='clip-plus-circle'></i>
                                </a>
                               </div>";
                }
                elseif ($prop['Status_Proposta_id'] == 7) 
                {
                    $button = "<div class='visible-md visible-lg hidden-sm hidden-xs'>                                
                                <a data-toggle='modal' href='#responsive' class='btn btn-xs btn-orange tooltips btn-proposta-more-details' data-placement='top' data-original-title='Parcelas' modal-iframe-uri='" . Yii::app()->baseUrl . "/proposta/maisDetalhes?id=" . $prop['id'] . "&banco=2'>
                                    <i class='clip-plus-circle'></i>
                                </a>
                               </div>";
                } else {
                    $button = "";
                }

                $row = array(
                        "prop_id" => $prop['id'],
                        "codigo" => $prop['codigo'],
                        "data_cadastro" => $prop['data_cadastro_br'],
                        "valor" => number_format($prop['valor'], 2, ',', '.'),
                        "valor_final" => number_format($prop['valor_final'], 2, ',', '.'),
                        "qtd_parc" => $prop['qtd_parcelas'],
                        "sc_id" => $prop['sc_id'],
                        "status" => $prop['status'],
                        "status_id" => $prop['Status_Proposta_id'],
                        "button" => $button
                );

                $rows[] = $row;
            }

            echo json_encode(array("data" => $rows));
        }
    }

    public function actionListarClientes() {

        //Yii::app()->session['usuario']->updateVistoPorUltimo();

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/additional-methods.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-search-cliente.js', CClientScript::POS_END);
        $cs->registerScript('cpf', 'jQuery(function($){$("input.cpfmask").mask("99999999999");});');

        $retorno = NULL;

        if (isset($_POST['cpf']) && $_POST['cpf'] != NULL) {

            $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
            $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
            $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');
            $cs->registerCssFile($baseUrl . '/css/sigax_template/bootstrap-editable.css');

            $cs->registerCssFile($baseUrl . '/css/lightbox/lightbox.css');

            $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
            $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/fn-search-cep.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);

            $cs->registerScriptFile($baseUrl . '/js/fn-cliente-perfil-modals.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/jquery.maskMoney.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/jquery.form.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/fn-client-history-tableV2.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/bootstrap-editable.min.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/moment.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/lightbox/lightbox.min.js', CClientScript::POS_END);

            $cs->registerScript('ipt_tempo_residencia', 'jQuery(function($){$("#tempo_de_residencia").mask("99/9999");});');
            $cs->registerScript('ipt_data', 'jQuery(function($){$("input.dateBR").mask("99/99/9999");});');
            $cs->registerScript('ipt_mes_ano', 'jQuery(function($){$("input.mes_ano").mask("99/9999");});');
            $cs->registerScript('ipt_date', 'jQuery(function($){$("input.telefone").mask("(99)9999-9999");});');
            $cs->registerScript('datepicker', "$('.date-picker').datepicker({autoclose: true});");
            $cs->registerScript('datepicker2', "$('.date-picker-mes-ano').datepicker({autoclose: true, format: 'mm/yyyy'});");
            $cs->registerScript('moneypicker', '$(".currency").maskMoney({decimal:",", thousands:"."});');

            $retorno = Cliente::model()->getClienteByCpf($_POST['cpf']);

            $cliente = Cliente::model()->findByPk($retorno['id']);

            $cpf = $_POST['cpf']; 

            if ($retorno != null && $cliente != null) {
                $this->render('perfil', array(
                        'cliente' => $cliente,
                        'analises' => $cliente->getAnalisesCliente(),
                        'cpf_cliente' => $cpf
                ));
            } else {
                $this->render('searchCliente'); 
            }
        } else {
            $this->render('searchCliente');
        }
    }

    public function actionGetClientes() {

        echo json_encode(Cliente::model()->listar($_POST['draw'], $_POST['start'], $_POST['order'], $_POST['columns']));
    }

    public function actionGetEnderecos() {

        $clienteId = $_POST['clienteId'];
        $draw = $_POST['draw'];

        $cliente = Cliente::model()->findByPk($clienteId);

        echo json_encode($cliente->listarEnderecos($draw));
    }

    public function actionGetReferencias() {

        $clienteId = $_POST['clienteId'];
        $draw = $_POST['draw'];

        $cliente = Cliente::model()->findByPk($clienteId);

        echo json_encode($cliente->listarReferencias($draw));
    }

    public function actionGetFamiliares() {

        $clienteId = $_POST['clienteId'];
        $draw = $_POST['draw'];

        $cliente = Cliente::model()->findByPk($clienteId);

        echo json_encode($cliente->listarFamiliares($draw));
    }

    public function actionGetDadosSociais() {

        $clienteId = $_POST['clienteId'];
        $draw = $_POST['draw'];

        $cliente = Cliente::model()->findByPk($clienteId);

        echo json_encode($cliente->getDadosSociais($draw));
    }

    public function actionGetDadosBancarios() {

        $clienteId = $_POST['clienteId'];
        $draw = $_POST['draw'];

        $cliente = Cliente::model()->findByPk($clienteId);

        echo json_encode($cliente->listarDadosBancarios($draw));
    }

    public function actionGetTelefones() {

        $clienteId = $_POST['clienteId'];
        $draw = $_POST['draw'];

        $cliente = Cliente::model()->findByPk($clienteId);

        echo json_encode($cliente->listarTelefones($draw));
    }

    public function actionGetEmails() {

        $clienteId = $_POST['clienteId'];
        $draw = $_POST['draw'];

        $cliente = Cliente::model()->findByPk($clienteId);

        echo json_encode($cliente->listarEmails($draw));
    }

    public function actionGetAnexos() {

        $clienteId = $_POST['clienteId'];
        $canDelete = $_POST['canDelete'];
        $draw = $_POST['draw'];

        $cliente = Cliente::model()->findByPk($clienteId);

        if ($cliente != NULL) {
            echo json_encode($cliente->listarAnexos($draw, $canDelete));
        } else {
            echo json_encode(array(
                    "draw" => $draw,
                    "recordsTotal" => 0,
                    "recordsFiltered" => 0,
                    "data" => array()
            ));
        }
    }

    public function actionGetDocumentos() {

        $clienteId = $_POST['clienteId'];
        $draw = $_POST['draw'];

        $cliente = Cliente::model()->findByPk($clienteId);

        echo json_encode($cliente->listarDocumentos($draw));
    }

    public function actionGetDadosProfissionais() {

        $clienteId = $_POST['clienteId'];
        $draw = $_POST['draw'];

        $cliente = Cliente::model()->findByPk($clienteId);

        echo json_encode($cliente->listarDadosProfissionais($draw));
    }

    public function actionPessoaEditAttr() {
        echo json_encode(Pessoa::model()->editarAtributo($_POST['pk'], $_POST['name'], $_POST['value']));
    }

    public function actionEditar() {

        if (Yii::app()->session['usuario']->tipo_id == 5) {
            $this->redirect('/analiseDeCredito/iniciarAnalise/');
        }

        if ($_POST['idCliente']) {
            $idCliente = $_POST['idCliente'];
        }

        $cliente = $this->loadModel($idCliente);

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();


        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/bootstrap-editable.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-search-cep.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        //$cs->registerScriptFile($baseUrl.'/js/ui-elements.js',CClientScript::POS_END);


        $cs->registerScriptFile($baseUrl . '/js/fn-cliente-perfil-modals.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskMoney.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-client-history-tableV2.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-editable.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/moment.js', CClientScript::POS_END);

        $cs->registerScript('ipt_tempo_residencia', 'jQuery(function($){$("#tempo_de_residencia").mask("99/9999");});');
        $cs->registerScript('ipt_data', 'jQuery(function($){$("input.dateBR").mask("99/99/9999");});');
        $cs->registerScript('ipt_mes_ano', 'jQuery(function($){$("input.mes_ano").mask("99/9999");});');
        $cs->registerScript('ipt_date', 'jQuery(function($){$("input.telefone").mask("(99)9999-9999");});');
        $cs->registerScript('datepicker', "$('.date-picker').datepicker({autoclose: true});");
        $cs->registerScript('datepicker2', "$('.date-picker-mes-ano').datepicker({autoclose: true, format: 'mm/yyyy'});");
        $cs->registerScript('moneypicker', '$(".currency").maskMoney({decimal:",", thousands:"."});');



        //Yii::app()->session['usuario']->updateVistoPorUltimo();


        $this->render('perfil', array(
                'cliente' => $cliente,
                'analises' => $cliente->getAnalisesCliente()
        ));
    }

    public function actionAnalisarCadastro() {

        if ($_POST['idCliente']) {
            $idCliente = $_POST['idCliente'];
        }

        $cliente = $this->loadModel($idCliente);

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();


        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/fn-cliente-analisar-cadastrov2.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ui-elements.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-cliente-perfil-modals.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskMoney.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js', CClientScript::POS_END);

        $cs->registerScript('ipt_tempo_residencia', 'jQuery(function($){$("#tempo_de_residencia").mask("99/9999");});');
        $cs->registerScript('ipt_data', 'jQuery(function($){$("input.dateBR").mask("99/99/9999");});');
        $cs->registerScript('ipt_mes_ano', 'jQuery(function($){$("input.mes_ano").mask("99/9999");});');
        $cs->registerScript('ipt_date', 'jQuery(function($){$("input.telefone").mask("(99)9999-9999");});');
        $cs->registerScript('datepicker', "$('.date-picker').datepicker({autoclose: true});");
        $cs->registerScript('datepicker2', "$('.date-picker-mes-ano').datepicker({autoclose: true, format: 'mm/yyyy'});");
        $cs->registerScript('moneypicker', '$(".currency").maskMoney({decimal:",", thousands:"."});');

        //Yii::app()->session['usuario']->updateVistoPorUltimo();

        $sigacLog = new SigacLog;
        $sigacLog->saveLog('Visualização de Dados de Cliente', 'Cliente', $cliente->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Analista " . Yii::app()->session['usuario']->nome_utilizador . " consultou dados cadastrais de um cliente", "127.0.0.1", null, Yii::app()->session->getSessionId());

        $this->render('analisarCadastro', array(
                'cliente' => $cliente,
                'analises' => $cliente->getAnalisesCliente()
        ));
    }

    public function actionView($id) {

        $this->render('view', array(
                'model' => $this->loadModel($id),
        ));
    }

    public function actionCreate() {

        $model = new Cliente;
        $pessoa = new Pessoa;
        $cpf = new Documento;
        $contato = new Contato;
        $endereco = new Endereco;
        $email = new Email;
        $telefone = new Telefone;
        $cadastro = new Cadastro;

        $pessoaHasEndereco = new PessoaHasEndereco;
        $pessoaHasDocumento = new PessoaHasDocumento;
        $contatoHasTelefone = new ContatoHasTelefone;
        $contatoHasEmail = new ContatoHasEmail;
        $cadastroHasFilial = new CadastroHasFilial;


        if (isset($_POST['Pessoa'])) {

            $endereco->attributes = $_POST['Endereco'];
            $pessoa->attributes = $_POST['Pessoa'];
            //$pessoa->nascimento = $_POST['Pessoa[nascimento]']; --consertar

            if (isset($_POST['Telefone']) && isset($_POST['Email'])) {

                $telefone->attributes = $_POST['Telefone'];
                $email->attributes = $_POST['Email'];

                $telefone->data_cadastro = date('Y-m-d H:i:S');
                $email->data_cadastro = date('Y-m-d H:i:S');

                if ($telefone->save() && $email->save()) { //OK
                    $contato->data_cadastro = date('Y-m-d H:i:s');

                    if ($contato->save()) {//OK						

                        /* Informações de contato */
                        $contatoHasEmail->Contato_id = $contato->id;
                        $contatoHasEmail->Email_id = $email->id;

                        $contatoHasTelefone->Contato_id = $contato->id;
                        $contatoHasTelefone->Telefone_id = $telefone->id;

                        $contatoHasEmail->data_cadastro = date('Y-m-d H:i:s');
                        $contatoHasTelefone->data_cadastro = date('Y-m-d H:i:s');

                        $contatoHasEmail->save(); //OK
                        $contatoHasTelefone->save(); //OK

                        $pessoa->Contato_id = $contato->id;
                    }

                    $pessoa->data_cadastro = date('Y-m-d H:i:s');
                    $endereco->data_cadastro = date('Y-m-d H:i:s');

                    if ($pessoa->save() && $endereco->save()) {//OK
                        $pessoaHasEndereco->Pessoa_id = $pessoa->id;
                        $pessoaHasEndereco->Endereco_id = $endereco->id;
                        $pessoaHasEndereco->data_cadastro = date('Y-m-d H:i:s');

                        $pessoaHasEndereco->save(); // OK

                        if (isset($_POST['Documento'])) {

                            $cpf->attributes = $_POST['Documento'];
                            $cpf->Tipo_documento_id = 1;
                            $cpf->data_cadastro = date('Y-m-d H:i:s');

                            if ($cpf->save()) { //OK
                                $pessoaHasDocumento->Documento_id = $cpf->id;
                                $pessoaHasDocumento->Pessoa_id = $pessoa->id;
                                $pessoaHasDocumento->data_cadastro = date('Y-m-d H:i:s');
                                $pessoaHasDocumento->save(); //OK
                            }
                        }

                        $model->Pessoa_id = $pessoa->id;

                        if ($model->save()) { //OK
                            $cadastro->Cliente_id = $model->id;

                            if ($cadastro->save()) { //OK	
                                $cadastroHasFilial->attributes = $_POST['CadastroHasFilial'];
                                $cadastroHasFilial->Cadastro_id = $cadastro->id;
                                $cadastroHasFilial->data_cadastro = date('Y-m-d H:i:s');
                                $cadastroHasFilial->save(); //OK

                                $this->redirect($this->createUrl('admin'));
                            }
                        }
                    }
                }
            }
        }


        $baseUrl = Yii::app()->baseUrl;

        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');

        $this->render('create', array(
                'model' => $model,
                'pessoa' => $pessoa,
                'cpf' => $cpf,
                'endereco' => $endereco,
                'telefone' => $telefone,
                'email' => $email,
                'endereco' => $endereco,
                'cadastroHasFilial' => $cadastroHasFilial,
        ));
    }

    public function actionUpdate($id) {

        $model = $this->loadModel($id);

        if (isset($_POST['Cliente'])) {

            $model->attributes = $_POST['Cliente'];

            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
                'model' => $model,
        ));
    }

    public function actionDelete($id) {

        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    public function actionIndex() {

        $dataProvider = new CActiveDataProvider('Cliente');

        $this->render('index', array(
                'dataProvider' => $dataProvider,
        ));
    }

    public function actionAdmin() {

        $model = new Cliente('search');

        $model->unsetAttributes();

        if (isset($_GET['Cliente'])) {
            $model->attributes = $_GET['Cliente'];
        }

        $this->render('admin', array(
                'model' => $model,
        ));
    }

    public function loadModel($id) {

        $model = Cliente::model()->findByPk($id);

        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionRenderFormEditDadosProfissionais() {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery-ui-1.10.2.custom.min.js');
        $cs->registerScriptFile($baseUrl . '/js/bootstrap.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.mousewheel.js');
        $cs->registerScriptFile($baseUrl . '/js/perfect-scrollbar.js');
        $cs->registerScriptFile($baseUrl . '/js/less-1.5.0.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.cookie.js');
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpalette.js');
        $cs->registerScriptFile($baseUrl . '/js/main.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskMoney.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/form-elements.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/daterangepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-timepicker.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.tagsinput.js', CClientScript::POS_END);


        $cs->registerScriptFile($baseUrl . '/js/summernote.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ckeditor/ckeditor.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ckeditor/adapter/jquery.js', CClientScript::POS_END);

        $cs->registerScriptFile($baseUrl . '/js/form-wizard.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.smartWizard.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.inputlimiter.1.3.1.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.autosize.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/update-select.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-fileupload.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/analise-upload-file-manager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-search-cep.js', CClientScript::POS_END);

        $cs->registerCssFile($baseUrl . '/css/sigax_template/bootstrap-fileupload.min.css');

        $cs->registerScript('MesAnoRenda', 'jQuery(function($){$("input.mes_ano_renda").mask("99/9999");});');
        $cs->registerScript('cep', 'jQuery(function($){$("input.cep").mask("99999999");});');
        $cs->registerScript('telefone', 'jQuery(function($){$("input.telefone").mask("(99)9999-9999");});');
        $cs->registerScript('dinheiro', 'jQuery("input.dinheiro").maskMoney({decimal:"."});');
        $cs->registerScript('ipt_date', 'jQuery(function($){$("input.ipt_date").mask("99/99/9999");});');
        $cs->registerScript('tempo_resi', 'jQuery(function($){$("#tempo_de_residencia").mask("99/9999");});');

        $cs->registerScript('FormWizard', 'FormWizard.init();');
        $cs->registerScript('FormElements', 'FormElements.init();');


        $this->layout = '//layouts/empty_template';

        $id = $_GET['id'];
        $dadosProfissionais = DadosProfissionais::model()->findByPk($id);
        $enderecoProfissao = Endereco::model()->findByPk($dadosProfissionais->Endereco_id);
        $contatoProfissao = Contato::model()->findByPk($dadosProfissionais->Contato_id);

        $this->render('formEditDadosProfissionais', array(
                'DadosProfissionais' => $dadosProfissionais,
                'enderecoProfissao' => $enderecoProfissao,
        ));
    }

    public function actionRenderFormEditEndereco() {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery-ui-1.10.2.custom.min.js');
        $cs->registerScriptFile($baseUrl . '/js/bootstrap.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.blockUI.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.icheck.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.mousewheel.js');
        $cs->registerScriptFile($baseUrl . '/js/perfect-scrollbar.js');
        $cs->registerScriptFile($baseUrl . '/js/less-1.5.0.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.cookie.js');
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpalette.js');
        $cs->registerScriptFile($baseUrl . '/js/main.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.maskMoney.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/form-elements.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/daterangepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-timepicker.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-colorpicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.tagsinput.js', CClientScript::POS_END);


        $cs->registerScriptFile($baseUrl . '/js/summernote.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ckeditor/ckeditor.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ckeditor/adapter/jquery.js', CClientScript::POS_END);

        $cs->registerScriptFile($baseUrl . '/js/form-wizard.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.smartWizard.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.inputlimiter.1.3.1.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.autosize.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/update-select.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-fileupload.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/analise-upload-file-manager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-search-cep.js', CClientScript::POS_END);

        $cs->registerCssFile($baseUrl . '/css/sigax_template/bootstrap-fileupload.min.css');

        $cs->registerScript('MesAnoRenda', 'jQuery(function($){$("input.mes_ano_renda").mask("99/9999");});');
        $cs->registerScript('cep', 'jQuery(function($){$("input.cep").mask("99999999");});');
        $cs->registerScript('telefone', 'jQuery(function($){$("input.telefone").mask("(99)9999-9999");});');
        $cs->registerScript('dinheiro', 'jQuery("input.dinheiro").maskMoney({decimal:"."});');
        $cs->registerScript('ipt_date', 'jQuery(function($){$("input.ipt_date").mask("99/99/9999");});');
        $cs->registerScript('tempo_resi', 'jQuery(function($){$("#tempo_de_residencia").mask("99/9999");});');

        $cs->registerScript('FormWizard', 'FormWizard.init();');
        $cs->registerScript('FormElements', 'FormElements.init();');


        $this->layout = '//layouts/empty_template';

        $id = $_GET['id'];
        $endereco = Endereco::model()->findByPk($id);

        $this->render('formEditEndereco', array(
                'Endereco' => $endereco
        ));
    }

    public function actionDocumentoCadastrado() {

        $doc = $_POST['doc'];

        $return = Documento::model()->docCadastrado($doc);
    }

    public function actionClienteIdadeApto() {
        $util = new Util;
        $nascimento = $_POST['nascimento'];
        $vip = $_POST['vip'];

        $return = $util->clienteIdade($nascimento, $vip);
    }

    public function actionCheckStatusDoCadastro() {
        echo json_encode(Cliente::model()->returnSatusCadastro($_POST['clienteId']));
    }

    protected function performAjaxValidation($model) {

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'cliente-form') {

            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionGetClienteCPF() {

        $retorno = [];

        $cpf = $_POST['cpf'];

        if ($cpf !== null && !empty($cpf)) {

            $cliente = Cliente::model()->getClienteByCpf($cpf);

            if ($cliente !== null) {
                $retorno['idCliente'] = $cliente->id;
                $retorno['nome'] = $cliente->pessoa->nome;
                $retorno['sucesso'] = true;
                $retorno['mensagem'] = '';
            } else {
                $retorno['idCliente'] = '';
                $retorno['nome'] = '';
                $retorno['sucesso'] = false;
                $retorno['mensagem'] = 'Digite um CPF válido';
            }
        } else {
            $retorno['idCliente'] = '';
            $retorno['nome'] = '';
            $retorno['sucesso'] = false;
            $retorno['mensagem'] = 'Digite um CPF válido';
        }

        echo json_encode($retorno);
    }

}
