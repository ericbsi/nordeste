<?php

Yii::import('application.vendor.*');
require_once 'Math/Finance.php';
require_once 'Math/Finance_FunctionParameters.php';
require_once 'Math/Numerical/RootFinding/NewtonRaphson.php';
    
class TesteController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/main_sigac_template';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'abrirCSV', 'testeCobranca', 'listarInadimplencias', 'abrirXMLNF', 'testarLeccaIRR', 'politicaCredito'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    public function actionTesteCobranca()
    {

        $baseUrl    = Yii::app()->baseUrl           ;
        $cs         = Yii::app()->getClientScript() ;

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css'  );
        $cs->registerCssFile($baseUrl . '/css/sigax_template/iCheck/skins/all.css'          );
        
        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js',   CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js',      CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js',    CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js',      CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js',      CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js',            CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js',     CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js',                CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js',         CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js',         CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap.min.js'        ,      CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/teste/fn-testes-grid.js',       CClientScript::POS_END);

        $this->render('testeCobranca');
        
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {

/*        $resposta = [];
        
        $xml =   '<s:envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
                        <s:body>
                            <s:fault xmlns:ns4="http://www.w3.org/2003/05/soap-envelope">
                                <faultcode>S:Client</faultcode>
                                <faultstring>org.xml.sax.SAXParseException; cvc-elt.1: Cannot find the declaration of element "web:incluirSpc".</faultstring>
                            </s:fault>
                        </s:body>
                    </s:envelope>';

        $objXML = simplexml_load_string($xml);

        $namespaces = $objXML->getNamespaces(true);

        $objXML->registerXPathNamespace('s'     , $namespaces["s"   ]);

        
        if(count($namespaces) > 1)
        {
            $objXML->registerXPathNamespace('ns2'   , $namespaces["ns2" ]);

            $resposta  = $objXML->xpath('//ns2:incluirSpcResponse');
        }

        ob_start();
        var_dump($namespaces);
        $resNameSpaces = '$nameSpaces: ' . ob_get_clean();
                
        if(count($resposta) ==  0)
        {

            if(count($namespaces) > 1)
            {
                $erro  = $objXML->xpath('//ns2:Fault');
            }
            else
            {
                $erro  = $objXML->xpath('//s:fault');
            }

            if(count($erro) == 0)
            {

                echo    "Erro no WS do SPC. Sem resposta. <br>"
                        .   $resNameSpaces;

            }
            else
            {

                ob_start();
                var_dump($erro);
                $resErro = ob_get_clean();
                
                echo "<br>resErro: $resErro";

            }
        }*/
        
        /*$cs         = Yii::app()->getClientScript() ;
        
        $cs->registerScriptFile('/js/jquery.min.js');
        $cs->registerScriptFile('/js/teste/fn-testes2-grid.js',       CClientScript::POS_END);
        
        /*echo 'Yii::app()->baseurl: ' . Yii::app()->getBaseUrl(true) . "<br>";
        echo "teste";*/
        
        //var_dump($_SERVER);
        
//        echo $_SERVER["SERVER_ADDR"];
        
//        echo base64_encode("1628476:32084269");
        
/*        $teste = "";
        
        eval('$teste = "Eita";');
        
        echo $teste;*/
        
//        $this->render('teste');
        
/*        $xml =  "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:web='http://webservice.spc.insumo.spcjava.spcbrasil.org/'>
                    <soapenv:Header/>
                    <soapenv:Body>
                       <web:listarMotivoExclusao/>
                    </soapenv:Body>
                 </soapenv:Envelope>";
        
        
        $linkSPC    = "https://treina.spc.org.br/spc/remoting/ws/insumo/spc/spcWebService?wsdl" ;
        $oSPC       = "395165"                                                                  ;
        $sSPC       = "27072015"                                                                ;
        
        $autorizacao    = 'Basic ' . base64_encode("$oSPC:$sSPC")   ;

        $headers = array(
                            'Content-Type:text/xml;charset=UTF-8',
                            'Connection: close',
                            'Authorization: ' . $autorizacao, // <---,
                            'Content-length: ' . strlen($xml)
                        );

        $ch = curl_init($linkSPC);

        curl_setopt($ch, CURLOPT_HTTPHEADER       , $headers    );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER   , 1           );
        curl_setopt($ch, CURLOPT_POSTFIELDS       , $xml        );

        $retorno    = curl_exec($ch)                    ;
        $objXML     = simplexml_load_string($retorno)   ;
        $namespaces = $objXML->getNamespaces(true)      ;

        $objXML->registerXPathNamespace('s'     , $namespaces["S"   ]);
        $objXML->registerXPathNamespace('ns2'   , $namespaces["ns2" ]);
                                    
        $resposta  = $objXML->xpath('//ns2:listarMotivoExclusaoResponse');
        
        //var_dump($resposta);
        var_dump($resposta[0]);
        
        echo "<br>";
        
        foreach ($resposta[0] as $motivo)
        {
        
            echo "<br>";
            
            echo '$motivo["id"]: ' . $motivo->id . ' $motivo["descricao-motivo"]: ' . $motivo->xpath("descricao-motivo")[0];
            
        }*/
        
/*        echo strtr  (
                        "(84) 9 8132-6490" ,
                        [
                            "(" => ""   ,
                            ")" => ""   ,
                            " " => ""   ,
                            "-" => ""
                        ]   
                    );*/
        
/*        $sql = "SELECT ROUND(SUM(Pa.valor),2) AS Valor
            FROM Parcela AS Pa
            INNER JOIN Titulo AS T ON T.habilitado AND Pa.Titulo_id = T.id AND T.NaturezaTitulo_id = 2
            INNER JOIN Proposta AS P ON P.habilitado AND T.Proposta_id = P.id 
            WHERE Pa.habilitado AND P.Financeira_id = 10 AND EXISTS(SELECT * FROM LinhaCNAB AS LC INNER JOIN Parcela AS Pa2 ON Pa2.id = LC.Parcela_id AND Pa2.habilitado INNER JOIN Titulo AS T2 ON T2.habilitado AND Pa2.Titulo_id = T2.id AND T2.NaturezaTitulo_id = 1 WHERE LC.habilitado AND T2.Proposta_id = P.id AND LC.ArquivoExtportado_id NOT IN (32,33))";
        
        $resultado = Yii::app()->db->createCommand($sql)->queryRow();
        
        echo $resultado["Valor"];*/
        
//        $tabela = TabelaCotacao::model()->findByPk(44);
  
//        number_format($tabela->taxa,2,'','');
        
//        echo str_pad(number_format($tabela->taxa,2,'',''), 15, "0", STR_PAD_LEFT);        
        
        $this->layout = '';
    
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        
        $cs->registerScriptFile ($baseUrl . '/js/jquery.min.js'                                           );
        $cs->registerScriptFile ($baseUrl . '/js/matematica/fn-custoEfetivo.js', CClientScript::POS_END   );
        
        $this->render('teste');
        
/*        $dataPrimeira = new DateTime('2016-09-05');
        
        $cet = Cet::mensal(1000, 123.96, 12, date('Y-m-d'), $dataPrimeira);
        
        echo $cet;*/
        
    }

    public function actionAbrirCSV() 
    {


        $file = fopen('importarConciliacaoOMNI/teste1.csv', 'r');

        if ($file) {
            
            $transaction = Yii::app()->db->beginTransaction();
            
            $conciliacao                = new Conciliacao                       ;
            $conciliacao->Usuario_id    = Yii::app()->session['usuario']->id    ;
            $conciliacao->data_cadastro = date('Y-m-d H:i:s')                   ;
            $conciliacao->habilitado    = 1                                     ;
            
            if(!$conciliacao->save())
            {
                var_dump($conciliacao->getErrors());
                $transaction->rollBack();
            }
            else
            {
                
                $qtdImportada = 0;

                while (!feof($file)) {

                    // Ler uma linha do arquivo
                    $linha = fgetcsv($file, 0, ";");

                    if (!$linha) {
                        continue;
                    }

                    if(ItemConciliacao::model()->find("habilitado AND codigoProposta = $linha[1]") !== null)
                    {
                        
                        echo "Proposta já importada: $linha[1] <br>";
                        continue;
                    
                    }

                    $date           = str_replace('/', '', trim($linha[2]));
                    $dateFormato    = '20' . substr($date, 4, 2) . '-' . substr($date, 2, 2) . '-' . substr($date, 0, 2);
                    $dataProposta   = date('Y-m-d', strtotime($dateFormato));

                    $valorP = str_replace('.', '' , $linha[5]);
                    $valorP = str_replace(',', '.', $valorP);

                    $cgcCliente = str_replace('.', ''   , $linha[4]     );
                    $cgcCliente = str_replace('-', ''   , $cgcCliente   );
                    $cgcCliente = str_replace('/', ''   , $cgcCliente   );

                    $itemConciliacao                    = new ItemConciliacao   ;
                    $itemConciliacao->habilitado        = 1                     ;
                    $itemConciliacao->data_cadastro     = date('Y-m-d H:i:s')   ;
                    $itemConciliacao->codigoContrato    = trim($linha[0])       ;
                    $itemConciliacao->codigoProposta    = trim($linha[1])       ;
                    $itemConciliacao->nomeCliente       = trim($linha[3])       ;
                    $itemConciliacao->cgcCliente        = $cgcCliente           ;
                    $itemConciliacao->dataProposta      = $dataProposta         ;
                    $itemConciliacao->valorProposta     = $valorP               ;
                    $itemConciliacao->Conciliacao_id    = $conciliacao->id      ;

                    if(!$itemConciliacao->save())
                    {
                        $transaction->rollBack();
                        echo "Erro ao importar Proposta: $linha[1]";
                        var_dump($itemConciliacao->getErrors());
                        exit;
                    }
                    else
                    {
                        $qtdImportada++;
                    }

                    echo 'CNPJ Filial   :    ' . $linha[0] . ' ';
                    echo 'Código        :    ' . $linha[1] . ' ';
                    echo 'Data          :    ' . $linha[2] . ' ';
                    echo 'Nome Cliente  :    ' . $linha[3] . ' ';
                    echo 'CPF Cliente   :    ' . $linha[4] . ' ';
                    echo 'Valor         : R$ ' . $linha[5] . ' ';

                    echo '<br>';

                }
                
                if($qtdImportada > 0)
                {
                    $transaction->commit();
                }
                
            }

            fclose($file);
        }
    }

    public function actionAbrirXMLNF()
    {
        
        $xml = '<?xml version="1.0" encoding="UTF-8"?>
<nfeProc xmlns="http://www.portalfiscal.inf.br/nfe" versao="3.10">
    <NFe xmlns="http://www.portalfiscal.inf.br/nfe">
        <infNFe Id="NFe24160408106783000102550010000663621000411448" versao="3.10">
            <ide>
                <cUF>24</cUF>
                <cNF>00041144</cNF>
                <natOp>NOTA PARA CUPOM</natOp>
                <indPag>2</indPag>
                <mod>55</mod>
                <serie>1</serie>
                <nNF>66362</nNF>
                <dhEmi>2016-04-20T10:38:00-03:00</dhEmi>
                <dhSaiEnt>2016-04-20T10:38:00-03:00</dhSaiEnt>
                <tpNF>1</tpNF>
                <idDest>1</idDest>
                <cMunFG>2403103</cMunFG>
                <tpImp>1</tpImp>
                <tpEmis>1</tpEmis>
                <cDV>8</cDV>
                <tpAmb>1</tpAmb>
                <finNFe>1</finNFe>
                <indFinal>1</indFinal>
                <indPres>1</indPres>
                <procEmi>0</procEmi>
                <verProc>2.49</verProc>
                <NFref>
                    <refECF>
                        <mod>2C</mod>
                        <nECF>6</nECF>
                        <nCOO>189533</nCOO>
                    </refECF>
                </NFref>
            </ide>
            <emit>
                <CNPJ>08106783000102</CNPJ>
                <xNome>A MARE MANSA COMERCIO DE MOVEIS E ELETRODOMESTICO LTDA</xNome>
                <xFant>CURRAIS NOVOS 1</xFant>
                <enderEmit>
                    <xLgr>R LULA GOMES</xLgr>
                    <nro>157</nro>
                    <xBairro>CENTRO</xBairro>
                    <cMun>2403103</cMun>
                    <xMun>CURRAIS NOVOS</xMun>
                    <UF>RN</UF>
                    <CEP>59380000</CEP>
                    <cPais>1058</cPais>
                    <xPais>BRASIL</xPais>
                    <fone>558434311247</fone>
                </enderEmit>
                <IE>200001604</IE>
                <CRT>3</CRT>
            </emit>
            <dest>
                <CPF>03038489441</CPF>
                <xNome>MARIA LIDENEIDE LOPES</xNome>
                <enderDest>
                    <xLgr>RUA MANOEL BATISTA</xLgr>
                    <nro>60</nro>
                    <xBairro>MANOEL SALUSTINO</xBairro>
                    <cMun>2403103</cMun>
                    <xMun>CURRAIS NOVOS</xMun>
                    <UF>RN</UF>
                    <CEP>59380000</CEP>
                    <cPais>1058</cPais>
                    <xPais>BRASIL</xPais>
                    <fone>84996505301</fone>
                </enderDest>
                <indIEDest>9</indIEDest>
            </dest>
            <det nItem="1">
                <prod>
                    <cProd>07030008040050102</cProd>
                    <cEAN>0703000804035</cEAN>
                    <xProd>LIQUIDIFICADOR ARNO C/FILTRO NEW FACICLIC LN37 BR X</xProd>
                    <NCM>85094010</NCM>
                    <CFOP>5929</CFOP>
                    <uCom>UN</uCom>
                    <qCom>1.0000</qCom>
                    <vUnCom>118.00000000</vUnCom>
                    <vProd>118.00</vProd>
                    <cEANTrib>0703000804035</cEANTrib>
                    <uTrib>UN</uTrib>
                    <qTrib>1.0000</qTrib>
                    <vUnTrib>118.00000000</vUnTrib>
                    <indTot>1</indTot>
                </prod>
                <imposto>
                    <vTotTrib>43.12</vTotTrib>
                    <ICMS>
                        <ICMS00>
                            <orig>0</orig>
                            <CST>00</CST>
                            <modBC>3</modBC>
                            <vBC>118.00</vBC>
                            <pICMS>18.0000</pICMS>
                            <vICMS>21.24</vICMS>
                        </ICMS00>
                    </ICMS>
                    <IPI>
                        <cEnq>999</cEnq>
                        <IPINT>
                            <CST>53</CST>
                        </IPINT>
                    </IPI>
                    <PIS>
                        <PISAliq>
                            <CST>01</CST>
                            <vBC>118.00</vBC>
                            <pPIS>1.6500</pPIS>
                            <vPIS>1.95</vPIS>
                        </PISAliq>
                    </PIS>
                    <COFINS>
                        <COFINSAliq>
                            <CST>01</CST>
                            <vBC>118.00</vBC>
                            <pCOFINS>7.6000</pCOFINS>
                            <vCOFINS>8.97</vCOFINS>
                        </COFINSAliq>
                    </COFINS>
                </imposto>
            </det>
            <total>
                <ICMSTot>
                    <vBC>118.00</vBC>
                    <vICMS>21.24</vICMS>
                    <vICMSDeson>0</vICMSDeson>
                    <vBCST>0</vBCST>
                    <vST>0</vST>
                    <vProd>118.00</vProd>
                    <vFrete>0</vFrete>
                    <vSeg>0</vSeg>
                    <vDesc>0</vDesc>
                    <vII>0</vII>
                    <vIPI>0</vIPI>
                    <vPIS>1.95</vPIS>
                    <vCOFINS>8.97</vCOFINS>
                    <vOutro>0</vOutro>
                    <vNF>118.00</vNF>
                    <vTotTrib>43.12</vTotTrib>
                </ICMSTot>
            </total>
            <transp>
                <modFrete>1</modFrete>
            </transp>
            <infAdic>
                <infAdFisco>MD-5:5EA2FAB8521FDB16EADD13EE59AAC501</infAdFisco>
                <infCpl>CF/SERIE:189533   /078 ECF:0006 Valor Aproximado dos Tributos: R$ 43.12 (36.54%). Fonte: IBPT.</infCpl>
            </infAdic>
        </infNFe>
        <Signature xmlns="http://www.w3.org/2000/09/xmldsig#">
            <SignedInfo xmlns="http://www.w3.org/2000/09/xmldsig#">
                <CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"></CanonicalizationMethod>
                <SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1"></SignatureMethod>
                <Reference URI="#NFe24160408106783000102550010000663621000411448">
                    <Transforms>
                        <Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"></Transform>
                        <Transform Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"></Transform>
                    </Transforms>
                    <DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1"></DigestMethod>
                    <DigestValue>RWl78tGzuS+eLhO0UyHBkyCZIBM=</DigestValue>
                </Reference>
            </SignedInfo>
            <SignatureValue>JodD/Nng/08UvqPilIZL3hWRYbeHuIpM/hAYHScstnKMYQ8oCMadeknqopxapyc6BeKDHp5WelFCg7gNH98Dm1X9Sz0ZuG8IlgTd6WKhDeVWgshi0i3YhhDT6HPygOc3MPE83mRA0eZbpfLZ8+asgysj6z6cRwfEdmyeEGXSJJ0wMHKK0xjF1O0KvNhGhNa1eoOr62RctdqLvnEMBcxRzmRse6bN+ulRF2ZEMKNMQUeWGyiW/P/1+fafvFnsh/WRqARhkcMRPnIRJzUaZHnoGyql9HbK1V/Pa+I0UWF94nNDfMEQcFU35Rkt6V18YnWlD3z5EkULpmEfNWPtVNYfsQ==</SignatureValue>
            <KeyInfo>
                <X509Data>
                    <X509Certificate>MIIIDjCCBfagAwIBAgIIdwuITz7EruwwDQYJKoZIhvcNAQELBQAwdTELMAkGA1UEBhMCQlIxEzARBgNVBAoTCklDUC1CcmFzaWwxNjA0BgNVBAsTLVNlY3JldGFyaWEgZGEgUmVjZWl0YSBGZWRlcmFsIGRvIEJyYXNpbCAtIFJGQjEZMBcGA1UEAxMQQUMgU0VSQVNBIFJGQiB2MjAeFw0xNTA5MTQxMzI4MDBaFw0xNjA5MTMxMzI4MDBaMIH0MQswCQYDVQQGEwJCUjELMAkGA1UECBMCUk4xFjAUBgNVBAcTDUNVUlJBSVMgTk9WT1MxEzARBgNVBAoTCklDUC1CcmFzaWwxNjA0BgNVBAsTLVNlY3JldGFyaWEgZGEgUmVjZWl0YSBGZWRlcmFsIGRvIEJyYXNpbCAtIFJGQjEWMBQGA1UECxMNUkZCIGUtQ05QSiBBMTEQMA4GA1UECxMHQVIgQ05ETDFJMEcGA1UEAxNAQSBNQVJFIE1BTlNBIENPTUVSQ0lPIERFIE1PVkVJUyBFIEVMRVRST0RPTUVTVElDTzowODEwNjc4MzAwMDEwMjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJtPzwib1vNWCOSV4AxmaZj/4B+DFvUSTw43dD/qtvOtFJlkCa5SMmfwNs2oIJDj3OatH0YZyFKT8YSc5ck9wSdoEXHOgLhbivLvx+Cr5FxZUQNsJU3RU+gxcjFFuuj19ZRpESbTE3nW0GFVqDVhej8wkhWe2UYnUFsP/4Mb0x28mFytCMCn6S1nrKUO130YUPzX1zaGmi9xdJLV5uXQtNsebFRY3UFbRojhRhh/jgihOTxktykcUCrndinMUHoKqSjVe9qUKSx0rdZVjuj0x841M3lN9gMp9T6amDmz97P+fjAx3tXSj+IMAXYAcI7iUqZyHhyCDlBFkKmMBn0eCTECAwEAAaOCAyAwggMcMIGZBggrBgEFBQcBAQSBjDCBiTBIBggrBgEFBQcwAoY8aHR0cDovL3d3dy5jZXJ0aWZpY2Fkb2RpZ2l0YWwuY29tLmJyL2NhZGVpYXMvc2VyYXNhcmZidjIucDdiMD0GCCsGAQUFBzABhjFodHRwOi8vb2NzcC5jZXJ0aWZpY2Fkb2RpZ2l0YWwuY29tLmJyL3NlcmFzYXJmYnYyMAkGA1UdEwQCMAAwHwYDVR0jBBgwFoAUsqDEPUaefMiFbAgeEDKUZUZwQXMwcQYDVR0gBGowaDBmBgZgTAECAQ0wXDBaBggrBgEFBQcCARZOaHR0cDovL3B1YmxpY2FjYW8uY2VydGlmaWNhZG9kaWdpdGFsLmNvbS5ici9yZXBvc2l0b3Jpby9kcGMvZGVjbGFyYWNhby1yZmIucGRmMIHzBgNVHR8EgeswgegwSqBIoEaGRGh0dHA6Ly93d3cuY2VydGlmaWNhZG9kaWdpdGFsLmNvbS5ici9yZXBvc2l0b3Jpby9sY3Ivc2VyYXNhcmZidjIuY3JsMESgQqBAhj5odHRwOi8vbGNyLmNlcnRpZmljYWRvcy5jb20uYnIvcmVwb3NpdG9yaW8vbGNyL3NlcmFzYXJmYnYyLmNybDBUoFKgUIZOaHR0cDovL3JlcG9zaXRvcmlvLmljcGJyYXNpbC5nb3YuYnIvbGNyL1NlcmFzYS9yZXBvc2l0b3Jpby9sY3Ivc2VyYXNhcmZidjIuY3JsMA4GA1UdDwEB/wQEAwIF4DAdBgNVHSUEFjAUBggrBgEFBQcDAgYIKwYBBQUHAwQwgbkGA1UdEQSBsTCBroEfQ09OVFJPTEFET1JJQUBBTUFSRU1BTlNBLkNPTS5CUqAdBgVgTAEDAqAUExJEVVJWQUwgSk9TRSBEQU5UQVOgGQYFYEwBAwOgEBMOMDgxMDY3ODMwMDAxMDKgOAYFYEwBAwSgLxMtMjYxMjE5NDIwNTAxNTY5MzQwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwoBcGBWBMAQMHoA4TDDAwMDAwMDAwMDAwMDANBgkqhkiG9w0BAQsFAAOCAgEAWjthkQeyyxo+BnWqFsOazX2DxZ0evUbOYoCkT2qH7FUqDI5C5Jx+qhn4sjFh2LHs+vxUPtxOLx+KPfoPuqISeS5q74Wzz7ilmmE2sDC8PYhpCxBhboUNmgh7HYyC7SPrIGfp1i2gUjaucBHGLfFGclJp2mWujMq6CwYDBFkhcHK5AZNP1bEF3HVKZk51p97Y0nl2heko/su8W8N35mGihYjXWLp2fD8fAskLENITTVp3xvfj5umfZLVU1JDfPXE4pwsZ5o5ZqLh9QqtSlL5nn4o6gurufrsv9SXt7PFP/AGncEH98NEzhuY/btsUELM/k2IVnVuIEHZVtPLuliUsMHgteEZJK0uNAV/Tx0G3CcKuX70UXaouER4vDHef9btgCRibJfRAPvrTrOgt5mzsQ8pv+hgzhaAxKPP4h2ZcnKqutxziPYWbEwnz7muhUQrDXv4TGs1nbQhz+Hm9RtSUF5tTeN/icVUiYxaQtvQjDENipVYCv6n32G0e8K76RlXwdFBE4JeDF2tYrQitp3c0OB7n8guHzrcJdNOZIevbU28DZWy0wBw73BIZhs508+YCrtMhEbwSl6MQkEMkKoDrlwXe1yfl4dzUn5ke6LDYjOPQ8Y1zlLMc5Knt/SDufqY3AVuY2B4wBMXv1nmbEmSvormTEfCm0hIujgQAQHUMXJw=</X509Certificate>
                </X509Data>
            </KeyInfo>
        </Signature>
    </NFe>
    <protNFe xmlns="http://www.portalfiscal.inf.br/nfe" versao="3.10">
        <infProt>
            <tpAmb>1</tpAmb>
            <verAplic>SVRS201604141332</verAplic>
            <chNFe>24160408106783000102550010000663621000411448</chNFe>
            <dhRecbto>2016-04-20T10:38:29-03:00</dhRecbto>
            <nProt>324160006272541</nProt>
            <digVal>RWl78tGzuS+eLhO0UyHBkyCZIBM=</digVal>
            <cStat>100</cStat>
            <xMotivo>Autorizado o uso da NF-e</xMotivo>
        </infProt>
    </protNFe>
</nfeProc>';
        
        $objXML = simplexml_load_string($xml);
        
        var_dump($objXML->NFe->infNFe->dest->CPF);
        
        echo "<br>";
        
        var_dump($objXML->protNFe->infProt->chNFe);
        
    }
    
    public function actionTestarLeccaIRR()
    {
        
        //echo Math_Finance::internalRateOfReturn( array(-584.31, 80.52, 80.52, 80.52, 80.52, 80.52, 80.52, 80.52, 80.52, 80.52, 80.52 ));
        
        echo "<br>";
        
        echo Math_Finance::internalRateOfReturn( array(-1000, 140.4, 140.4, 140.4, 140.4, 140.4, 140.4, 140.4, 140.4, 140.4, 140.4 ), 0.02);
        
        echo "<br>";
        
//        echo "<br>";
        
        echo IRRHelper::IRR(array(-1000, 129.57, 129.57, 129.57, 129.57, 129.57, 129.57, 129.57, 129.57, 129.57, 129.57, 129.57, 129.57),0.02);
//        $cetMes = IRRHelper::IRR(array(-309, 47.96, 47.96, 47.96, 47.96, 47.96, 47.96, 47.96, 47.96, 47.96, 47.96),0.02);
        
/*        echo '$cetMes . ' . ($cetMes*100) . '%';
        
        $cetAno = pow((1+$cetMes),12)-1;
        
        echo '<br> $cetAno: ' . ($cetAno*100) . '%';*/
        
        $proposta = Proposta::model()->findByPk(92403);
        
        echo "<br>Mensal:" . $proposta->getCustoEfetivoTotal(1);
        echo "<br>Anual :" . $proposta->getCustoEfetivoTotal(2);
        
    }
    
    public function actionPoliticaCredito()
    {
        
        $filiais = Filial::model()->findAll("habilitado AND PoliticaCredito_id IS NOT NULL");
        
        $i = 0;
        
        foreach ($filiais as $filial)
        {
            
//            $i++;
//            
//            echo "voti $i <br>";
            
            $filialHasPoliticaCredito = new FilialHasPoliticaCredito;
            
            $filialHasPoliticaCredito->habilitado           = 1                             ;
            $filialHasPoliticaCredito->data_cadastro        = date("Y-m-d H:i:s")           ;
            $filialHasPoliticaCredito->dataInicio           = "2013-01-01 00:00:00"         ;
            $filialHasPoliticaCredito->Filial_id            = $filial->id                   ;
            $filialHasPoliticaCredito->PoliticaCredito_id   = $filial->PoliticaCredito_id   ;
            
            if(!$filialHasPoliticaCredito->save())
            {
                
                ob_start()                                          ;
                var_dump($filialHasPoliticaCredito->getErrors())    ;
                $erro =  ob_get_clean()                             ;
                
                echo "erro: $erro <br>";
                
            }
            
        }
        
    }
    
}
