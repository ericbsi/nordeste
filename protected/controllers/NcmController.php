<?php

class NcmController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionAdd(){
		echo json_encode( Ncm::model()->add($_POST['NCM']) );
	}

	public function actionSearchNcm(){

		echo json_encode( Ncm::model()->searchNcmSelec2( $_POST['q'] ) );
	}
}