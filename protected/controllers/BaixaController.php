<?php

class BaixaController extends Controller {

    public function actionIndex() {
        $this->render('index');
    }

    //FUNÇÃO PARA GERAR TITULOS E PARCELAS DE NATUREZA 1, PARA PROPOSTAS QUE NÃO OS TENHA
    //FUNÇÃO UTILIZADAS PARA PROPOSTAS DO PERIODO DE 09/09 A 13/09
    public function actionGerarTP() {
        $sql = "SELECT P.codigo FROM

                    Proposta AS P

                WHERE
                    P.habilitado
                    AND P.Financeira_id     = 11
                    AND P.Status_Proposta_id IN (7,2)
                    AND P.titulos_gerados   = 1
                    AND P.segurada          = 0
                    AND P.codigo IN ('" . $_GET['c'] . "')";

        $semTP = Yii::app()->db->createCommand($sql)->queryAll();
        $util = new Util;
        $retorno = "DEU CERTO!";
        foreach ($semTP as $proposta) {
            $proposta = Proposta::model()->find("codigo = '" . $proposta['codigo'] . "' AND habilitado");
            //$proposta->data_cadastro = '2016-12-30 12:34:56';
            //if(!$proposta->update()){
            //    $retorno = "DEU ERRADO";
            //    break;
            //}
            $titulo = Titulo::model()->find('habilitado AND NaturezaTitulo_id = 1 AND Proposta_id = ' . $proposta->id);

            if ($titulo == null) {
                $titulo = new Titulo;
                $titulo->prefixo = '001';
                $titulo->NaturezaTitulo_id = 1;
                $titulo->Proposta_id = $proposta->id;
                $titulo->habilitado = 1;

                if ($titulo->save()) {
                    for ($i = 0; $i < $proposta->qtd_parcelas; $i++) {
                        $parcela = new Parcela;
                        $parcela->seq = $i + 1;
                        $parcela->valor = $proposta->getValorParcela();
                        $parcela->valor_texto = $proposta->valor_parcela_texto;
                        $parcela->valor_atual = 0;
                        $parcela->Titulo_id = $titulo->id;

                        if ($i == 0) {
                            $parcela->vencimento = $util->view_date_to_bd($proposta->getDataPrimeiraParcela());
                        } else {
                            $parcela->vencimento = $util->adicionarMesMantendoDia($util->view_date_to_bd($proposta->getDataPrimeiraParcela()), $i)->format('Y-m-d');
                        }

                        if (!$parcela->save()) {

                            ob_start();
                            var_dump($parcela->getErrors());
                            $erro = ob_get_clean();

                            $retorno = "DEU ERRADO! 1! " . $erro;
                            break;
                        }
                    }
                } else {

                    ob_start();
                    var_dump($parcela->getErrors());
                    $erro = ob_get_clean();

                    $retorno = "DEU ERRADO! 2! " . $erro;
                    break;
                }
            } else {
                $parcelas = Parcela::model()->findAll("Titulo_id = " . $titulo->id);

                foreach ($parcelas as $p) {
                    $p->habilitado = 0;
                    $p->update();
                }

                for ($i = 0; $i < $proposta->qtd_parcelas; $i++) {
                    $parcela = new Parcela;
                    $parcela->seq = $i + 1;
                    $parcela->valor = $proposta->getValorParcela();
                    $parcela->valor_texto = $proposta->valor_parcela_texto;
                    $parcela->valor_atual = 0;
                    $parcela->Titulo_id = $titulo->id;

                    if ($i == 0) {
                        $parcela->vencimento = $util->view_date_to_bd($proposta->getDataPrimeiraParcela());
                    } else {
                        $parcela->vencimento = $util->adicionarMesMantendoDia($util->view_date_to_bd($proposta->getDataPrimeiraParcela()), $i)->format('Y-m-d');
                    }

                    if (!$parcela->save()) {

                        ob_start();
                        var_dump($parcela->getErrors());
                        $erro = ob_get_clean();

                        $retorno = "DEU ERRADO! 3! " . $erro;
                        break;
                    }
                }
            }
        }

        echo $retorno;
    }

    //FUNÇÃO PARA GERAR TITULOS E PARCELAS DE NATUREZA 1, PARA PROPOSTAS QUE NÃO OS TENHA
    //FUNÇÃO UTILIZADAS PARA PROPOSTAS DO PERIODO DE 09/09 A 13/09
    public function gerarTP($codigo) {
        $sql = "SELECT P.codigo FROM

                    Proposta AS P

                WHERE
                    P.habilitado
                    AND P.codigo IN ('" . $codigo . "')";

        $semTP = Yii::app()->db->createCommand($sql)->queryAll();
        $util = new Util;
        $retorno = "DEU CERTO!";
        foreach ($semTP as $proposta) {
            $proposta = Proposta::model()->find("codigo = '" . $proposta['codigo'] . "' AND habilitado");
            //$proposta->data_cadastro = '2016-12-30 12:34:56';
            //if(!$proposta->update()){
            //    $retorno = "DEU ERRADO";
            //    break;
            //}
            $titulo = Titulo::model()->find('habilitado AND NaturezaTitulo_id = 1 AND Proposta_id = ' . $proposta->id);

            if ($titulo == null) {
                $titulo = new Titulo;
                $titulo->prefixo = '001';
                $titulo->NaturezaTitulo_id = 1;
                $titulo->Proposta_id = $proposta->id;
                $titulo->habilitado = 1;

                if ($titulo->save()) {
                    for ($i = 0; $i < $proposta->qtd_parcelas; $i++) {
                        $parcela = new Parcela;
                        $parcela->seq = $i + 1;
                        $parcela->valor = $proposta->getValorParcela();
                        $parcela->valor_texto = $proposta->valor_parcela_texto;
                        $parcela->valor_atual = 0;
                        $parcela->Titulo_id = $titulo->id;

                        if ($i == 0) {
                            $parcela->vencimento = $util->view_date_to_bd($proposta->getDataPrimeiraParcela());
                        } else {
                            $parcela->vencimento = $util->adicionarMesMantendoDia($util->view_date_to_bd($proposta->getDataPrimeiraParcela()), $i)->format('Y-m-d');
                        }

                        if (!$parcela->save()) {

                            ob_start();
                            var_dump($parcela->getErrors());
                            $erro = ob_get_clean();

                            $retorno = "DEU ERRADO! 1! " . $erro;
                            break;
                        }
                    }
                } else {

                    ob_start();
                    var_dump($parcela->getErrors());
                    $erro = ob_get_clean();

                    $retorno = "DEU ERRADO! 2! " . $erro;
                    break;
                }
            } else {
                $parcelas = Parcela::model()->findAll("Titulo_id = " . $titulo->id);
                if(count($parcelas == 0)){
                    
                    foreach ($parcelas as $p) {
                        $p->habilitado = 0;
                        $p->update();
                    }

                    for ($i = 0; $i < $proposta->qtd_parcelas; $i++) {
                        $parcela = new Parcela;
                        $parcela->seq = $i + 1;
                        $parcela->valor = $proposta->getValorParcela();
                        $parcela->valor_texto = $proposta->valor_parcela_texto;
                        $parcela->valor_atual = 0;
                        $parcela->Titulo_id = $titulo->id;

                        if ($i == 0) {
                            $parcela->vencimento = $util->view_date_to_bd($proposta->getDataPrimeiraParcela());
                        } else {
                            $parcela->vencimento = $util->adicionarMesMantendoDia($util->view_date_to_bd($proposta->getDataPrimeiraParcela()), $i)->format('Y-m-d');
                        }

                        if (!$parcela->save()) {

                            ob_start();
                            var_dump($parcela->getErrors());
                            $erro = ob_get_clean();

                            $retorno = "DEU ERRADO! 3! " . $erro;
                            break;
                        }
                    }
                }
            }
        }
    }

    //ESTA FUNÇÃO GERAR PARCELAS PARA TITULOS DE NATUREZA 2, PARA PROPOSTAS QUE POR ALGUM MOTIVO NÃO TIVERAM ESSAS PARCELAS GERADAS
    public function actionGerarParceletas() {
        $sql = "SELECT DISTINCT P.codigo, T.id FROM Proposta AS P
                LEFT JOIN Titulo AS T ON T.NaturezaTitulo_id = 2 AND T.habilitado AND T.Proposta_id = P.id
                LEFT JOIN Parcela AS PA ON PA.Titulo_id = T.id AND T.habilitado
                WHERE P.habilitado AND P.data_cadastro > '2016-01-01 00:00:00' AND P.Financeira_id = 11 AND PA.id is null AND P.Status_Proposta_id IN (2,7)";

        $retorno = "GERADAS COM SUCESSO";

        $semParcela = Yii::app()->db->createCommand($sql)->queryAll();
        $i = 0;
        foreach ($semParcela as $sp) {
            $proposta = Proposta::model()->find("codigo = '" . $sp['codigo'] . "' AND habilitado");

            if ($sp['id'] == NULL) {
                $titulo = new Titulo;
                $titulo->NaturezaTitulo_id = 2;
                $titulo->Proposta_id = $proposta->id;
                $titulo->habilitado = 1;
                $titulo->save();

                $tid = $titulo->id;
                $i++;
            } else {
                $tid = $sp['id'];
            }

            $parcela = new Parcela;
            $parcela->seq = 1;
            $parcela->Titulo_id = $tid;
            $parcela->valor = $proposta->valorRepasse();
            $parcela->valor_atual = 0;
            $parcela->habilitado = 1;

            if (!$parcela->save()) {
                $retorno = "NÃO GERADAS";
                break;
            }
        }
        echo $retorno . " " . $i;
    }

    public function actionArquivosImportados() {

        $sql = "SELECT AC.*, COUNT(LC.ArquivoExtportado_id) AS qtd
                FROM        ArquivoCNAB AS  AC
                INNER JOIN  LinhaCNAB   AS  LC  ON LC.ArquivoExtportado_id = AC.id AND LC.habilitado
                WHERE AC.TipoCNAB_id = 4 AND AC.habilitado
                GROUP BY AC.id
                ORDER BY AC.data_cadastro DESC";

        $arquivos = Yii::app()->db->createCommand($sql)->queryAll();

        $rows = [];

        $util = new Util;

        foreach ($arquivos as $arq) {

            $row = array(
                'idArquivo' => $arq['id'],
                'dt_geracao' => $util->bd_date_to_view(substr($arq['data_cadastro'], 0, 10)),
                'qtd_titulos' => $arq['qtd'],
                'usuario' => Usuario::model()->findByPk($arq['Usuario_id'])->nome_utilizador,
                'download' => ''
                . '<a href="' . $arq['urlArquivo'] . '" download>'
                . '     <i class="clip-download">'
                . '     </i>'
                . '</a>'
            );

            $rows[] = $row;
        }

        echo json_encode(array('data' => $rows));
    }

    public function actionArquivosGerados() {

        $sql = "SELECT AC.*, COUNT(LC.ArquivoExtportado_id) AS qtd
                FROM        ArquivoCNAB AS  AC
                INNER JOIN  LinhaCNAB   AS  LC  ON LC.ArquivoExtportado_id = AC.id AND LC.habilitado
                WHERE AC.TipoCNAB_id = 3 AND AC.habilitado
                GROUP BY AC.id
                ORDER BY AC.data_cadastro DESC";

        $arquivos = Yii::app()->db->createCommand($sql)->queryAll();

        $rows = [];

        $util = new Util;

        foreach ($arquivos as $arq) {

            $row = array(
                'idArquivo' => $arq['id'],
                'dt_geracao' => $util->bd_date_to_view(substr($arq['data_cadastro'], 0, 10)),
                'qtd_titulos' => $arq['qtd'],
                'usuario' => Usuario::model()->findByPk($arq['Usuario_id'])->nome_utilizador,
                'download' => ''
                . '<a href="' . $arq['urlArquivo'] . '" download>'
                . '     <i class="clip-download">'
                . '     </i>'
                . '</a>'
            );

            $rows[] = $row;
        }

        echo json_encode(array('data' => $rows));
    }

    public function actionImportarDP() {

        $certo = true;
        $arrReturn = array(
            "hasError" => true,
            "msg" => "Escolha um arquivo",
            "tipo" => "error"
        );

        try {

            if (isset($_FILES['anexo_importacao'])) {

                $aleatorio = date('YmdHis');
                $ext = substr($_FILES['anexo_importacao']['name'], strrpos($_FILES['anexo_importacao']['name'], '.'));

                if (move_uploaded_file($_FILES['anexo_importacao']['tmp_name'], '/var/www/nordeste/uploads/' . $aleatorio . '_DePara' . $ext)) {

                    $transaction = Yii::app()->db->beginTransaction();

                    $arquivoCNAB = new ArquivoCNAB;
                    $arquivoCNAB->data_cadastro = date("Y-m-d H:i:s");
                    $arquivoCNAB->habilitado = 1;
                    $arquivoCNAB->Usuario_id = Yii::app()->session["usuario"]->id;
                    $arquivoCNAB->TipoCNAB_id = 4;
                    $arquivoCNAB->urlArquivo = "/uploads/" . $aleatorio . "_DePara" . $ext;

                    if ($arquivoCNAB->save()) {

                        $arquivo = fopen('/var/www/nordeste/uploads/' . $aleatorio . '_DePara' . $ext, 'r');
                        $i = 0;

                        while (!feof($arquivo)) {

                            $i++;
                            $linha = fgets($arquivo);

                            if (substr($linha, 0, 1) == "1" && $linha != '') {

                                $idProposta = (int) substr($linha, 1, 12);

                                $proposta = Proposta::model()->find("habilitado AND id = $idProposta");

                                if ($proposta !== null) {

                                    $titulo = Titulo::model()->find("habilitado AND NaturezaTitulo_id = 1 AND Proposta_id = $proposta->id");

                                    if ($proposta->Status_Proposta_id == 8 || $titulo !== null || ($proposta->data_cadastro > '2016-09-08 23:59:59' && $proposta->data_cadastro < '2016-09-13 23:59:59')) {

                                        $seqParcela = (int) substr($linha, 41, 3);

                                        if ($proposta->Status_Proposta_id == 8 || ($proposta->data_cadastro > '2016-09-08 23:59:59' && $proposta->data_cadastro < '2016-09-13 23:59:59')) {
                                            $parcela = 0;
                                        } else {
                                            $parcela2 = Parcela::model()->find("habilitado AND Titulo_id = $titulo->id AND seq = $seqParcela");
                                            if ($parcela2 !== null) {
                                                $parcela = Parcela::model()->find("habilitado AND Titulo_id = $titulo->id AND seq = $seqParcela")->id;
                                            } else {
                                                $parcela = null;
                                            }
                                        }
                                        
                                        if($titulo == null){
                                            $this->gerarTP($proposta->codigo);
                                        }
                                        $titulo = Titulo::model()->find("habilitado AND NaturezaTitulo_id = 1 AND Proposta_id = $proposta->id");
                                        $parcela2 = Parcela::model()->find("habilitado AND Titulo_id = $titulo->id AND seq = $seqParcela");
                                        if ($parcela2 !== null) {
                                            $parcela = Parcela::model()->find("habilitado AND Titulo_id = $titulo->id AND seq = $seqParcela")->id;
                                        } else {
                                            $parcela = null;
                                        }
                                        
                                        if ($parcela !== null) {

                                            if ($parcela2 != null) {
                                                $parcela2->ref_lecca = intval(substr($linha, 13, 12)) . substr($linha, 41, 3);

                                                $parcela2->update();
                                            }

                                            if (($proposta->data_cadastro > '2016-09-08 23:59:59' && $proposta->data_cadastro < '2016-09-13 23:59:59')) {
                                                $linhaCNAB = null;
                                            } else {
                                                $linhaCNAB = LinhaCNAB::model()->find("habilitado AND Parcela_id = $parcela");
                                            }

                                            if ($linhaCNAB == null || ($linhaCNAB->arquivoExtportado->TipoCNAB_id !== 4)) {

                                                $linhaCNAB = new LinhaCNAB;
                                                $linhaCNAB->data_cadastro = date("Y-m-d H:i:s");
                                                $linhaCNAB->habilitado = 1;
                                                $linhaCNAB->ArquivoExtportado_id = $arquivoCNAB->id;
                                                $linhaCNAB->Parcela_id = $parcela;
                                                $linhaCNAB->caracteres = $linha;

                                                if (!$linhaCNAB->save()) {

                                                    ob_start();
                                                    var_dump($linhaCNAB->getErrors());
                                                    $r1 = ob_get_clean();

                                                    $arrReturn = array(
                                                        "hasError" => false,
                                                        "msg" => $r1 . '(' . $i . ')' . "linhas: " . $linha,
                                                        "tipo" => "error"
                                                    );

                                                    $certo = false;

                                                    break;
                                                }
                                            } else {
                                                if (!($proposta->data_cadastro > '2016-09-08 23:59:59' && $proposta->data_cadastro < '2016-09-13 23:59:59')) {
                                                    $arrReturn = array(
                                                        "hasError" => false,
                                                        "msg" => "Proposta $proposta->codigo já importada. ID Parcela: $parcela->id"
                                                        . " SeqParcela: $seqParcela " . ' substr($linha, 41,3): ' . substr($linha, 41, 3)
                                                        . " i: $i",
                                                        "tipo" => "error"
                                                    );

                                                    $certo = false;

                                                    break;
                                                }
                                            }
                                        } else {

                                            $certo = false;

                                            $arrReturn = array(
                                                "hasError" => true,
                                                "msg" => "Não foi possível realizar a importação. Parcela do Título a pagar da Proposta de $proposta->codigo não encontrada"
                                                . " SeqParcela: $seqParcela",
                                                "tipo" => "error"
                                            );

                                            break;
                                        }
                                    } else {

                                        $certo = false;

                                        $arrReturn = array(
                                            "hasError" => true,
                                            "msg" => "Não foi possível realizar a importação. Título a pagar da Proposta de $proposta->codigo não encontrado",
                                            "tipo" => "error"
                                        );

                                        break;
                                    }
                                } else {

                                    $certo = false;

                                    $arrReturn = array(
                                        "hasError" => true,
                                        "msg" => "Não foi possível realizar a importação. Proposta de id $idProposta não encontrada",
                                        "tipo" => "error"
                                    );

                                    break;
                                }
                            }
                        }

                        if ($certo) {
                            $arrReturn = array(
                                "hasError" => false,
                                "msg" => "Importação realizada com sucesso",
                                "tipo" => "success"
                            );
                        }
                    } else {

                        $certo = false;

                        $arrReturn = array(
                            "hasError" => true,
                            "msg" => "Não foi possível realizar a importação",
                            "tipo" => "error"
                        );
                    }

                    if ($certo) {

                        $transaction->commit();
                    } else {

                        $transaction->rollBack();
                    }
                } else {

                    $arrReturn = array(
                        "hasError" => true,
                        "msg" => "Não foi possível mover o arquivo",
                        "tipo" => "error"
                    );
                }
            } else {

                $arrReturn = array(
                    "hasError" => true,
                    "msg" => "Nenhum arquivo enviado",
                    "tipo" => "error"
                );
            }
        } catch (Exception $ex) {


            $arrReturn = array(
                "hasError" => true,
                "msg" => $ex->getMessage(),
                "tipo" => "error"
            );
        }

        echo json_encode($arrReturn);
    }

    public function actionExportarLinhasCNAB() {

        $retorno2 = [
            "hasErrors" => false,
            "title" => "Sucesso",
            "msg" => "CNAB gerado com sucesso",
            "type" => "success",
            "nomeArquivo" => "nada",
            "url" => "nada",
            "cnab" => ""
        ];

        $erro = "";
        $linhas = "";
        $retorno = "";
        $cont = 1;

        try {

            $transaction = Yii::app()->db->beginTransaction();

            $arquivoCNAB = new ArquivoCNAB;
            $arquivoCNAB->data_cadastro = date("Y-m-d H:i:s");
            $arquivoCNAB->habilitado = 1;
            $arquivoCNAB->Usuario_id = Yii::app()->session["usuario"]->id;
            $arquivoCNAB->TipoCNAB_id = 3;
            $arquivoCNAB->urlArquivo = "/";

            $sql = "SELECT P.id as idProposta, P.Financeira_id as fin, Pa.seq as seq, Pa.id AS idParcela, B.valor_pago AS valor_pago, Pa.ref_lecca AS NU_DOCUMENTO, B.data_da_baixa AS dataBaixa
                    FROM       Parcela          AS Pa
                    INNER JOIN Baixa            AS B    ON   B.baixado    AND Pa.id =   B.Parcela_id
                    INNER JOIN Titulo           AS T    ON   T.habilitado AND  T.id =  Pa.Titulo_id            AND  T.NaturezaTitulo_id    =   1
                    INNER JOIN Proposta         AS P    ON   P.habilitado AND  P.id =   T.Proposta_id          AND  P.Financeira_id        IN (11)
                    INNER JOIN RecebimentoInterno_has_Parcela 	AS RP 	ON   RP.Parcela_id = Pa.id 	AND RP.habilitado
                    WHERE Pa.habilitado AND (Pa.ref_lecca is not null) AND B.data_da_baixa > '2017-03-23' 
                    GROUP BY B.Parcela_id";

            $parcelasBaixadas = Yii::app()->db->createCommand($sql)->queryAll();

            if (count($parcelasBaixadas) > 0) {

                if ($arquivoCNAB->save()) {

                    foreach ($parcelasBaixadas as $pb) {

                        if ($pb['fin'] == 11) {


                            $consultoria = str_pad($pb ["NU_DOCUMENTO"], 18, "0", STR_PAD_LEFT);
                            $consultoria = str_pad($consultoria, 25, " ", STR_PAD_RIGHT);

                            $documento = str_pad($pb["NU_DOCUMENTO"], 11, "0", STR_PAD_LEFT);
                            $titulo = str_pad(substr($pb["NU_DOCUMENTO"], 0, -3), 10, "0", STR_PAD_LEFT);
                        } else {
                            $consultoria = str_pad($pb["idParcela"], 25, " ", STR_PAD_RIGHT);

                            $proposta = Proposta::model()->findByPk($pb['idProposta']);
                            $venda = $proposta->venda;
                            $notaF = null;
                            foreach ($venda->notasFiscais as $nf) {
                                $notaF = $nf->notaFiscal;
                            }

                            $documento = str_pad(($notaF->documento . $pb['seq']), 11, "0", STR_PAD_RIGHT);
                            $titulo = $titulo = str_pad(substr($pb["idParcela"], 0, -3), 10, "0", STR_PAD_LEFT);
                        }

                        $parcela = Parcela::model()->findByPk($pb['idParcela']);

                        $valorParcelaRepasse = ($parcela->titulo->proposta->valorRepasse() / $parcela->titulo->proposta->qtd_parcelas);
                        $valorParcela = $parcela->valor;

                        $cont++;

                        $linha = "";

                        $valorFormatadoRepasse = number_format($valorParcelaRepasse, 2, '', '');
                        $valorFormatado = number_format($valorParcela, 2, '', '');
                        $valorPago = number_format($pb['valor_pago'], 2, '', '');

                        $cpf = $parcela->titulo->proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero;
                        $nomeCliente = $parcela->titulo->proposta->analiseDeCredito->cliente->pessoa->nome;
                        $endereco = $parcela->titulo->proposta->analiseDeCredito->cliente->pessoa->getEndereco();

                        $cedente = "CREDSHOW FUNDO DE INVESTIMENTOS EM DIREITOS CR23273905000130";


                        if ($endereco == null) {

                            str_repeat(" ", 40);
                        } else {
                            $enderecoCompleto = trim($endereco->logradouro) . " ";
                            $enderecoCompleto .= trim($endereco->numero) . " ";
                            $enderecoCompleto .= trim($endereco->bairro) . " ";
                            $enderecoCompleto .= trim($endereco->cidade) . " ";
                            $enderecoCompleto .= trim($endereco->uf) . " ";
                            $enderecoCompleto .= trim($endereco->complemento) . " ";

                            $enderecoCEP = trim($endereco->cep);

                            $enderecoCompleto = strtoupper(substr(strtr(utf8_decode(trim($enderecoCompleto)), utf8_decode("áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ"), "aaaaeeiooouucAAAAEEIOOOUUC"), 0, 40));
                            $nomeCliente = strtoupper(substr(strtr(utf8_decode(trim($nomeCliente)), utf8_decode("áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ"), "aaaaeeiooouucAAAAEEIOOOUUC"), 0, 40));
                        }

                        $dateTime = new DateTime($pb["dataBaixa"]);
                        $dataBaixa = $dateTime->format("dmy");

                        $linha .= "1"; //de:   1 ate:   1
                        $linha .= str_repeat(" ", 19); //de:   2 ate:  20
                        $linha .= "02"; //de:  21 ate:  22
                        $linha .= "00"; //de:  23 ate:  24
                        $linha .= str_repeat("0", 04); //de:  25 ate:  28
                        $linha .= "00"; //de:  29 ate:  30
                        $linha .= "0199"; //de:  31 ate:  34
                        $linha .= "  "; //de:  35 ate:  36
                        $linha .= "0"; //de:  37 ate:  37
                        $linha .= $consultoria; //de:  38 ate:  62 /** nosso numero?*/ verificar isso depois
                        $linha .= str_repeat("0", 03); //de:  63 ate:  65
                        $linha .= str_repeat("0", 05); //de:  66 ate:  70
                        $linha .= $documento; //de:  71 ate:  81
                        $linha .= str_repeat(" ", 01); //de:  82 ate:  82
                        $linha .= str_pad($valorPago, 10, "0", STR_PAD_LEFT); //de:  83 ate:  92
                        $linha .= str_repeat(" ", 01); //de:  93 ate:  93
                        $linha .= str_repeat(" ", 01); //de:  94 ate:  94
                        //$linha .= str_repeat("0", 06); //de:  95 ate: 100
                        $linha .= $dataBaixa; //de:  95 ate: 100
                        $linha .= str_repeat(" ", 04); //de: 101 ate: 104
                        $linha .= str_repeat(" ", 01); //de: 105 ate: 105
                        $linha .= str_repeat(" ", 01); //de: 106 ate: 106
                        $linha .= str_repeat(" ", 02); //de: 107 ate: 108
                        //                    $linha .= str_pad($parcela->seq         ,  2, "0", STR_PAD_LEFT)          ; //de: 109 ate: 110
                        $linha .= "02"; //de: 109 ate: 110
                        //$linha .= "          "; //de: 111 ate: 120
                        $linha .= $titulo; //de: 111 ate: 120
                        $linha .= date("dmy", strtotime($parcela->vencimento)); //de: 121 ate: 126
                        $linha .= str_pad($valorFormatado, 13, "0", STR_PAD_LEFT); //de: 127 ate: 139
                        $linha .= str_repeat("0", 03); //de: 140 ate: 142
                        $linha .= str_repeat("0", 05); //de: 143 ate: 147
                        $linha .= "01"; //de: 148 ate: 149
                        $linha .= " "; //de: 150 ate: 150
                        $linha .= date("dmy", strtotime($parcela->titulo->proposta->data_cadastro)); //de: 151 ate: 156
                        $linha .= "00"; //de: 157 ate: 158
                        $linha .= "0"; //de: 159 ate: 159
                        $linha .= "02"; //de: 160 ate: 161
                        $linha .= str_repeat("0", 12); //de: 162 ate: 173
                        $linha .= str_repeat("0", 19); //de: 174 ate: 192
                        $linha .= str_pad($valorFormatadoRepasse, 13, "0", STR_PAD_LEFT); //de: 193 ate: 205
                        $linha .= str_repeat("0", 13); //de: 206 ate: 218
                        $linha .= "01"; //de: 219 ate: 220
                        $linha .= str_pad($cpf, 14, " ", STR_PAD_LEFT); //de: 221 ate: 234
                        $linha .= str_pad($nomeCliente, 40, " ", STR_PAD_RIGHT); //de: 235 ate: 274
                        $linha .= str_pad($enderecoCompleto, 40, " ", STR_PAD_RIGHT); //de: 275 ate: 314
                        $linha .= "         "; //de: 315 ate: 323
                        $linha .= "   "; //de: 324 ate: 326
                        $linha .= str_pad($enderecoCEP, 8, "0", STR_PAD_LEFT); //de: 327 ate: 334
                        $linha .= $cedente; //de: 335 ate: 394
                        $linha .= str_pad(" ", 44, " ", STR_PAD_LEFT);
                        //de: 395 ate: 438
                        $linha .= str_pad($cont, 06, "0", STR_PAD_LEFT); //de: 439 ate: 444

                        $linha .= chr(13) . chr(10);

                        $linhas .= $linha;

                        $linhaCNAB = new LinhaCNAB;
                        $linhaCNAB->data_cadastro = date("Y-m-d H:i:s");
                        $linhaCNAB->habilitado = 1;
                        $linhaCNAB->ArquivoExtportado_id = $arquivoCNAB->id;
                        $linhaCNAB->Parcela_id = $parcela->id;
                        $linhaCNAB->caracteres = utf8_encode($linha);

                        if (!$linhaCNAB->save()) {

                            ob_start();
                            var_dump($linhaCNAB->getErrors());
                            $erro = ob_get_clean();

                            $transaction->rollBack();

                            $linhas = "";

                            break;
                        }
                    }

                    if (!empty($linhas)) {

                        $cont++;

                        $qtdLinhas = str_pad($cont, 6, "0", STR_PAD_LEFT);

                        $dataRemessa = date("dmy");

                        $retorno = "01REMESSA01COBRANCA       00000000000000001291CREDSHOW FUNDO DE INVESTIMENTO611PAULISTA S.A.  $dataRemessa        MX0000001                                                                                                                                                                                                                                                                                                                                 000001";

                        $retorno .= chr(13) . chr(10);

                        $retorno .= $linhas;

                        $retorno .= "9                                                                                                                                                                                                                                                                                                                                                                                                                                                     $qtdLinhas";
                    }
                } else {

                    ob_start();
                    var_dump($arquivoCNAB->getErrors());
                    $erro = ob_get_clean();

                    $transaction->rollBack();
                }

                //return ["linhas" => $retorno, "arquivoCNAB" => $arquivoCNAB, "erro" => $erro];

                $cnab = $retorno;

                if (!empty($cnab)) {

                    $pasta = "liquidacoesLECCA";

                    if (!is_dir($pasta)) {
                        mkdir("liquidacoesLECCA", 0700);
                    }

                    $cont = 1;

                    $arquivo = "CB" . date("dm") . str_pad($cont, 2, "0", STR_PAD_LEFT) . ".txt";

                    while (file_exists($pasta . "/" . $arquivo)) {

                        $cont++;

                        $arquivo = "CB" . date("dm") . str_pad($cont, 2, "0", STR_PAD_LEFT) . ".txt";
                    }

                    $file = fopen($pasta . "/" . $arquivo, 'w');
                    fwrite($file, $cnab);
                    fclose($file);

                    $retorno2["url"] = "/" . $pasta . "/" . $arquivo;
                    $retorno2["nomeArquivo"] = $arquivo;
                    $retorno2["sql"] = $sql;

                    $arquivoCNAB->urlArquivo = $retorno2["url"];

                    if (!$arquivoCNAB->update()) {

                        ob_start();
                        var_dump($arquivoCNAB->getErrors());
                        $erro = ob_get_clean();

                        $transaction->rollBack();
                    } else {
                        $transaction->commit();
                    }
                } else {

                    $erroMsg = "";

                    if (isset($erro) && !empty($erro)) {
                        $erroMsg = $erro;
                    }

                    $retorno2 = [
                        "hasErrors" => true,
                        "title" => "Erro",
                        "msg" => $erroMsg,
                        "type" => "error",
                        "cnab" => $cnab,
                        "sql" => $sql,
                    ];
                }
            } else {

                $retorno2 = [
                    "hasErrors" => true,
                    "title" => "Erro",
                    "msg" => "Não foram registradas baixas desde a geração do ultimo arquivo",
                    "type" => "error"
                ];
            }
        } catch (Exception $ex) {

            $retorno2 = [
                "hasErrors" => true,
                "title" => "Erro",
                "msg" => $ex->getMessage(),
                "type" => "error"
            ];
        }

        echo json_encode($retorno2);
    }

}
