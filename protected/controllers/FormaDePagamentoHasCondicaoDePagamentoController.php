<?php

class FormaDePagamentoHasCondicaoDePagamentoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','getDescricao','getIntervalo'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new FormaDePagamentoHasCondicaoDePagamento;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['FormaDePagamentoHasCondicaoDePagamento']))
		{
			$model->attributes=$_POST['FormaDePagamentoHasCondicaoDePagamento'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['FormaDePagamentoHasCondicaoDePagamento']))
		{
			$model->attributes=$_POST['FormaDePagamentoHasCondicaoDePagamento'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
        
        public function actionGetDescricao()
        {
            $fpHasCP_id = $_POST['id'];
            
            $fpHasCP = FormaDePagamentoHasCondicaoDePagamento::model()->findByPK($fpHasCP_id);
            
            $retorno = $fpHasCP->formaDePagamento->sigla . ' ' . $fpHasCP->qtdParcelasDe . ' até ' . $fpHasCP->qtdParcelasAte . 'X';
            
            echo json_encode(array('text'=>$retorno));
            
        }
        
        public function actionGetIntervalo()
        {
            $fpHasCP_id = $_POST['id'];
            
            $fpHasCP = FormaDePagamentoHasCondicaoDePagamento::model()->findByPK($fpHasCP_id);
            
            echo json_encode(array('de'=>$fpHasCP->qtdParcelasDe,'ate'=>$fpHasCP->qtdParcelasAte));
            
        }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('FormaDePagamentoHasCondicaoDePagamento');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new FormaDePagamentoHasCondicaoDePagamento('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['FormaDePagamentoHasCondicaoDePagamento']))
			$model->attributes=$_GET['FormaDePagamentoHasCondicaoDePagamento'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return FormaDePagamentoHasCondicaoDePagamento the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=FormaDePagamentoHasCondicaoDePagamento::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param FormaDePagamentoHasCondicaoDePagamento $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='forma-de-pagamento-has-condicao-de-pagamento-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
