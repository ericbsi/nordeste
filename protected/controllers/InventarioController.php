<?php

class InventarioController extends Controller {

    public $layout = '//layouts/main_sigac_template';
    
    public function actionIndex() {

        $baseUrl = Yii::app()->baseUrl;

        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        
        //$cs->registerScript('ipt_date', '$(document).ready(function() { $("#select-estoques").select2(); });');

        $cs->registerScript('UIModals', 'UIModals.init();');

        $empresa = Yii::app()->session['usuario']->getEmpresa();

        $this->render('index',array(
            'Filiais' => $empresa->listFiliais()
        ));
    }

    public function actionAdd(){

        /*
            Recebe três arrays POST: 
            $_POST['itens_estoque_ids']
            $_POST['itens_estoque_qtds']
            $_POST['ids_produtos_em_inventario']
        */

        if( isset( $_POST['itens_estoque_ids'] ) && !empty( $_POST['itens_estoque_ids'] ) && 
            isset( $_POST['itens_estoque_qtds'] ) && !empty( $_POST['itens_estoque_qtds'] ))
        {
            $this->render( 'efetivar', array(
                'POST' => $_POST
            ));
        }
    }

    public function actionInventariarEstoque(){

        $baseUrl                = Yii::app()->baseUrl;
        $cs                     = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl.'/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl.'/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl.'/css/sigax_template/jquery-ui.min.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl.'/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/inventario-estoque-select.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/select2.min.js',  CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.pnotify.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.validate.12.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery-ui.min.js',CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/jquery.jeditable.js',CClientScript::POS_END);

        $cs->registerScript('select_estoques_id','$(function($){$("#select_estoques_id").select2();});');

        $itensEstoque           = NULL;
        $estoque                = NULL;

        if( isset( $_POST['Filial_id'] ) && !empty($_POST['Filial_id']) )
        {
            $filial_id          = $_POST['Filial_id'];
            $Filial             = Filial::model()->findByPk( $filial_id );

            if( isset( $_POST['Estoque_id'] ) && !empty($_POST['Estoque_id']) )
            {
                $estoque        = Estoque::model()->findByPk( $_POST['Estoque_id'] );
                $itensEstoque   = $estoque->listItensEstoque();
            }

            $this->render('inventario_grid', array(
                'Filial'        => $Filial,
                'itensEstoque'  => $itensEstoque,
                'estoqueSel'    => $estoque
            ));
        }        
    }

    public function actionEfetivarInventario(){

        $itens_do_estoque           = $_POST['item_estoque_id'];
        $quantidades_inventariadas  = $_POST['quantidade_inventariada'];

        $cont = count( $_POST['item_estoque_id'] );

        for ($i = 0; $i < $cont; $i++) { 
            
            echo json_encode("id: ". $_POST['item_estoque_id'][$i] . " Quantidade: ".$_POST['quantidade_inventariada'][$i] ) . ' | ';

        }
    }
}
