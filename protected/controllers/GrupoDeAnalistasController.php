<?php

class GrupoDeAnalistasController extends Controller{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/main_sigac_template';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('changeFilialRelation','create','update','index','view','admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionCreate(){

		$baseUrl = Yii::app()->baseUrl; 
		$cs = Yii::app()->getClientScript();
	  	$cs->registerScriptFile($baseUrl.'/js/jquery.min.js');
		$cs->registerScriptFile($baseUrl.'/js/form-wizard.js',  CClientScript::POS_END);
	  	$cs->registerScriptFile($baseUrl.'/js/jquery.validate.min.js',  CClientScript::POS_END);
	  	$cs->registerScriptFile($baseUrl.'/js/jquery.smartWizard.js',  CClientScript::POS_END);
	  	$cs->registerScriptFile($baseUrl.'/js/jquery.maskedinput.js',CClientScript::POS_END);
	  	$cs->registerScriptFile($baseUrl.'/js/form-elements.js',CClientScript::POS_END);
	  	$cs->registerScriptFile($baseUrl.'/js/select2.min.js',CClientScript::POS_END);
	  	$cs->registerScriptFile($baseUrl.'/js/jquery.inputlimiter.1.3.1.min.js',CClientScript::POS_END);
	  	$cs->registerScriptFile($baseUrl.'/js/jquery.autosize.min.js',CClientScript::POS_END);
	  	$cs->registerScriptFile($baseUrl.'/js/update-select.js',CClientScript::POS_END);
	  	$cs->registerScriptFile($baseUrl.'/js/bootstrap-multiselect.js', CClientScript::POS_END);
  	    $cs->registerScriptFile($baseUrl.'/js/grupo-de-analistas-form-fn.js', CClientScript::POS_END);
	  	    
	  	$cs->registerScript('FormWizard','FormWizard.init();');
	  	$cs->registerScript('FormElements','FormElements.init();');
		
		$model = new GrupoDeAnalistas;

		if( isset( $_POST['GrupoDeAnalistas'] ) ){

			$empresaUsuario = Yii::app()->session['usuario']->getEmpresa();

			$model->attributes = $_POST['GrupoDeAnalistas'];
			$model->data_cadastro = date('Y-m-d H:i:s');
			$model->data_cadastro_br = date('d/m/Y H:i:s');
			$model->Empresa_id = $empresaUsuario->id;

			if( $model->save() ){
				
				$sigacLog = new SigacLog;
				$sigacLog->saveLog('Cadastro de grupo de analistas','Grupo_de_Analistas', $model->id, date('Y-m-d H:i:s'),1, Yii::app()->session['usuario']->id, "Usuário ".Yii::app()->session['usuario']->username . " cadastrou o grupo de analistas " . $model->nome, "127.0.0.1",null, Yii::app()->session->getSessionId() );

				if ( isset( $_POST['Filiais'] ) ) {

					for ($i = 0; $i < sizeof($_POST['Filiais']); $i++) {
						$grupoDeAnalistasHasFilial = new GrupoDeAnalistasHasFilial;
						$grupoDeAnalistasHasFilial->Filial_id = $_POST['Filiais'][$i];
						$grupoDeAnalistasHasFilial->Grupo_de_Analistas_id = $model->id;
						$grupoDeAnalistasHasFilial->data_cadastro = date('Y-m-d H:i:s');
						$grupoDeAnalistasHasFilial->data_cadastro_br = date('d/m/Y H:i:s');
						$grupoDeAnalistasHasFilial->save();
					}
				}
				
				$this->redirect('admin');
			}			

		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionChangeFilialRelation(){

		$grupoId = $_GET['grupoId'];
		$id = $_GET['id'];
		$checked = $_GET['check'];
		
		$grupo = GrupoDeAnalistas::model()->findByPk($grupoId);
		$grupo->filialRelationChange($id, $checked);

	}

	public function actionUpdate( $id ){

		$model = $this->loadModel($id);

		if( isset( $_POST['GrupoDeAnalistas'] ) ) {

			$model->attributes = $_POST['GrupoDeAnalistas'];

			if ($model->save()){				
				$this->redirect($this->createUrl('admin'));
			}
		}

		$baseUrl = Yii::app()->baseUrl; 
		$cs = Yii::app()->getClientScript();
	  	$cs->registerScriptFile($baseUrl.'/js/jquery.min.js');
		$cs->registerScriptFile($baseUrl.'/js/form-wizard.js',  CClientScript::POS_END);
	  	$cs->registerScriptFile($baseUrl.'/js/jquery.validate.min.js',  CClientScript::POS_END);
	  	$cs->registerScriptFile($baseUrl.'/js/jquery.smartWizard.js',  CClientScript::POS_END);
	  	$cs->registerScriptFile($baseUrl.'/js/jquery.maskedinput.js',CClientScript::POS_END);
	  	$cs->registerScriptFile($baseUrl.'/js/form-elements.js',CClientScript::POS_END);
	  	$cs->registerScriptFile($baseUrl.'/js/select2.min.js',CClientScript::POS_END);
	  	$cs->registerScriptFile($baseUrl.'/js/jquery.inputlimiter.1.3.1.min.js',CClientScript::POS_END);
	  	$cs->registerScriptFile($baseUrl.'/js/jquery.autosize.min.js',CClientScript::POS_END);
	  	$cs->registerScriptFile($baseUrl.'/js/update-select.js',CClientScript::POS_END);
	  	$cs->registerScriptFile($baseUrl.'/js/bootstrap-multiselect.js', CClientScript::POS_END);
  	    $cs->registerScriptFile($baseUrl.'/js/grupo-de-analistas-form-edit-fn.js', CClientScript::POS_END);  	    
	  	    
	  	$cs->registerScript('FormWizard','FormWizard.init();');
	  	$cs->registerScript('FormElements','FormElements.init();');

		$this->render('update',array(
			'model'=>$model,
		));
	}
	
	public function actionDelete($id){

		$this->loadModel($id)->delete();
		
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionIndex(){

		$dataProvider = new CActiveDataProvider('GrupoDeAnalistas');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new GrupoDeAnalistas('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['GrupoDeAnalistas']))
			$model->attributes=$_GET['GrupoDeAnalistas'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return GrupoDeAnalistas the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=GrupoDeAnalistas::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param GrupoDeAnalistas $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='grupo-de-analistas-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
