<?php

class CondicaoDePagamentoController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/main_sigac_template';

    //public $layout='//layouts/main_sigac_template';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('add', 'cadastrarCP', 'create', 'update', 'index', 'view', 'delete', 'admin', 'edit', 'teste'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new CondicaoDePagamento;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['CondicaoDePagamento'])) {
            $model->attributes = $_POST['CondicaoDePagamento'];
            if ($model->save())
            //$this->redirect(array('view','id'=>$model->id));
                $this->redirect(array('formaDePagamentoHasCondicaoDePagamento/admin', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionCadastrarCP() {

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');

        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/ui-modals.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-grid-condicoes-de-pagamento.js', CClientScript::POS_END);
        $cs->registerScript('select2', '$("select.search-select").select2()');
        $cs->registerScript('UIModals', 'UIModals.init();');

        $model = new CondicaoDePagamento;

        if (isset($_POST['Condicao_de_Pagamento'])) { 

            $model->attributes = $_POST['Condicao_de_Pagamento'];

            $fpId = $_POST['Forma_de_Pagamento_has_Condicao_de_Pagamento_Forma_de_Pagamento_id_hdn'];
            $fpQtdDe = $_POST['qtd_parcelas_de_hdn'];
            $fpQtdAte = $_POST['qtd_parcelas_ate_hdn'];
            $fpCarencia = $_POST['carencia_hdn'];
            $fpIntervalo = $_POST['intervalo_hdn'];
//            
//            var_dump($fpId);
//            var_dump($fpQtdDe);
//            var_dump($fpQtdAte);
//            var_dump($fpCarencia);
//            var_dump($fpIntervalo);
            
            if ($model->save()) {

                for ($i = 0; $i < sizeof($fpId); $i++) {

                    $modelFPhasCP = new FormaDePagamentoHasCondicaoDePagamento;

                    $modelFPhasCP->Condicao_de_Pagamento_id = $model->id;
                    $modelFPhasCP->Forma_de_Pagamento_id = $fpId[$i];
                    $modelFPhasCP->qtdParcelasDe = $fpQtdDe[$i];
                    $modelFPhasCP->qtdParcelasAte = $fpQtdAte[$i];
                    $modelFPhasCP->carencia = $fpCarencia[$i];
                    $modelFPhasCP->intervalo = $fpIntervalo[$i];

                    $modelFPhasCP->save();
                }

                //$this->redirect(array('view','id'=>$model->id));
                $this->redirect(array('admin'));
            }
        }

        $this->render('cadastrarCP', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['CondicaoDePagamento'])) {
            $model->attributes = $_POST['CondicaoDePagamento'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('CondicaoDePagamento');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionAdmin() {
        $model = new CondicaoDePagamento('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['CondicaoDePagamento']))
            $model->attributes = $_GET['CondicaoDePagamento'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function loadModel($id) {
        $model = CondicaoDePagamento::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'condicao-de-pagamento-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function listFormaDePagamentoHasCondicaoDePagamento() {

        /*
          $formasDePagamentoHasCondicaoDePagamento = FormaDePagamentoHasCondicaoDePagamento::model()->findAll('Condicao_De_Pagamento_id = ' . $this->id);

          if(count($formasDePagamentoHasCondicaoDePagamento) == 0)
          {
         */

        $formasDePagamento = FormaDePagamento::model()->findAll();

        foreach ($formasDePagamento as $formaDePagamento) {

            $formaDePagamentoHasCondicaoDePagamento = new FormaDePagamentoHasCondicaoDePagamento;

            $formaDePagamentoHasCondicaoDePagamento->formaDePagamento = $formaDePagamento;
            $formaDePagamentoHasCondicaoDePagamento->qtdParcelasDe = 0;
            $formaDePagamentoHasCondicaoDePagamento->qtdParcelasAte = 0;
            $formaDePagamentoHasCondicaoDePagamento->carencia = 0;
            $formaDePagamentoHasCondicaoDePagamento->intervalo = 0;

            $formasDePagamentoHasCondicaoDePagamento[] = $formaDePagamentoHasCondicaoDePagamento;
        }
        //}

        return $formasDePagamentoHasCondicaoDePagamento;
    }

}
