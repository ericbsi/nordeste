<?php

class CrediaristaController extends Controller{

	public $layout = '//layouts/main_sigac_template';

	public function actionIndex(){
		$this->render('index');
	}

	public function actionMinhasAnalises(){

		$baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl.'/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl.'/css/sigax_template/dataTables.tableTools.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile('https://www.google.com/jsapi', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-crediarista-analises.js', CClientScript::POS_END);

		$this->render('minhasAnalises');
	}

	public function actionListAnalises()
	{
		echo json_encode( Yii::app()->session['usuario']->doGridAnalises( $_POST['draw'], $_POST['start'] ) );
	}
}