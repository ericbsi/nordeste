<?php

class AuditoriaController extends Controller
{
	public $layout = '//layouts/main_sigac_template';

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionNovo()
	{

		$anexo = NULL;

		if( isset( $_FILES['ComprovanteFile'] ) )
		{
			$anexo = $_FILES['ComprovanteFile'];
		}

		echo json_encode( Auditoria::model()->novo( $_POST['Auditoria'], $anexo ) );
	}

	public function actionAnexar()
	{
		echo json_encode( Auditoria::model()->anexar( $_POST['Auditoria_id'], $_FILES['ComprovanteFile'], $_POST['Anexo']['descricao'] ) );
	}

	public function actionParcelas()
	{
		$baseUrl 	= Yii::app()->baseUrl;
        $cs 		= Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', 	CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', 	CClientScript::POS_END);        
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-multiselect.js', 		CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', 			CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', 	CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', 				CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-auditoria-parcelas.js',		CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js', 		CClientScript::POS_END);

		$this->render('parcelas');
	}

	public function actionAuditoriaParcelas()
	{
		echo json_encode( Auditoria::model()->listar('Parcela', 1, $_POST['draw'], $_POST['start'] ) );
	}

	public function actionMore()
	{
		$baseUrl 	= Yii::app()->baseUrl;
        $cs 		= Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', 						CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', 						CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', 								CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', 						CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/fn-auditoria-more.js',								CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js', 							CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js', 							CClientScript::POS_END);

        $cs->registerScriptFile($baseUrl . '/js/bootstrap-filestyle.min.js', 	CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js', 				CClientScript::POS_END);

		$this->render('more',[
			'auditoria' => Auditoria::model()->findByPk( $_POST['AuditoriaId'] ), 
			'util'		=> new Util
		]);
	}

	public function actionGetObservacoes()
	{
		echo json_encode( Auditoria::model()->getObservacoes( $_POST['AuditoriaId'], $_POST['draw'], $_POST['start'] ) );
	}

	public function actionAddObs()
	{
		echo json_encode( Auditoria::model()->addObs( $_POST['Auditoria_id'], $_POST['Observacao'] ) );
	}

	public function actionFinalizar()
	{
		Auditoria::model()->finalizar($_POST['Auditoria']);

		$this->actionParcelas();

	}

	public function actionGetAnexos()
	{
		echo json_encode( Auditoria::model()->getAnexos( $_POST['AuditoriaId'], $_POST['draw'] ) );
	}
}