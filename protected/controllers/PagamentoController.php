<?php

class PagamentoController extends Controller
{
	public $layout = '//layouts/main_sigac_template';

	public function actionIndex()
	{
		$baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        
        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl.'/js/bootstrap-multiselect.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl.'/js/fn-iniciar-auth-pgto.js', CClientScript::POS_END);
        
		$this->render('index');
	}

	public function actionEfetivarPendencias()
	{
		$baseUrl 	= Yii::app()->baseUrl;
		$cs 		= Yii::app()->getClientScript();

		$arrFiliais = [];

		$cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
		$cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
		$cs->registerScriptFile($baseUrl . '/js/util.js', 											CClientScript::POS_END);
		$cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', 	CClientScript::POS_END);
		$cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', 	CClientScript::POS_END);
		$cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js',        CClientScript::POS_END);
		$cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js',		CClientScript::POS_END);
		$cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js',           CClientScript::POS_END);
		$cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js',    CClientScript::POS_END);

		for ($i = 0; $i < sizeof($_POST['filiaisProcesso']); $i++)
		{
			$arrFiliais[$_POST['filiaisProcesso'][$i]]['filial'] 						= $_POST['filiaisProcesso'][$i];
			$arrFiliais[$_POST['filiaisProcesso'][$i]]['propostas'] 					= [];
			$arrFiliais[$_POST['filiaisProcesso'][$i]]['comentarios'] 					= [];
		}

		for ($y = 0; $y < sizeof($_POST['itens_pgto_aprovados']); $y++){

			$proposta = Proposta::model()->findByPk( $_POST['itens_pgto_aprovados'][$y] );

			if( $proposta != null )
			{
				$arrFiliais[$proposta->analiseDeCredito->filial->id]['propostas'][] 	= $proposta->id;
				$arrFiliais[$proposta->analiseDeCredito->filial->id]['comentarios'][] 	= $_POST['itens_pgto_aprovados_obs'][$y];
			}
		}

		
		foreach($arrFiliais as $fconfig)
		{
			if( sizeof( $fconfig['propostas'] ) > 0 )
			{
				$novoRegPendencias 	= RegistroDePendencias::model()->novo( $fconfig['propostas'], $fconfig['comentarios'], $fconfig['filial'] );

				if( !$novoRegPendencias['hasErrors'] )
				{
					$recebDeDoc 			= RecebimentoDeDocumentacao::model()->novo( NULL, $novoRegPendencias['registroId'], $fconfig['filial'] );
				}
			}
		}
		
		$this->redirect('/recebimentoDeDocumentacao/historico/');
		
		/*
		if( $_POST['processoId'] == 0 )
		{
			$novoRegPendencias 	= RegistroDePendencias::model()->novo( $_POST['itens_pgto_aprovados'], $_POST['itens_pgto_aprovados_obs'], $_POST['destinatario'] );

			if( !$novoRegPendencias['hasErrors'] )
			{
				$recebDeDoc  	= RecebimentoDeDocumentacao::model()->novo( NULL, $novoRegPendencias['registroId'], $_POST['destinatario'] );

				if( $_POST['action'] == 'aut_pgto' )
				{
					$cs->registerScriptFile($baseUrl . '/js/fn-grid-bordero.js', 							CClientScript::POS_END);
					$this->render( 'gridBordero', array( 'parceiro' => Filial::model()->findByPk( $_POST['destinatario'] ), 'processoId'=>$recebDeDoc->id ) );
				}
				else
				{
					$this->render('resumoProcesso', array('processo' => $recebDeDoc));
				}
			}
			else
			{
				throw new Exception("Não foi possível completar sua requisição. Contate o suporte.", 0);
			}
		}

		else
		{
			$recebDeDoc  							= RecebimentoDeDocumentacao::model()->findByPk( $_POST['processoId'] );

			if( $recebDeDoc->registroDePendencias == NULL )
			{
				$novoRegPendencias 					= RegistroDePendencias::model()->novo( $_POST['itens_pgto_aprovados'], $_POST['itens_pgto_aprovados_obs'], $_POST['destinatario'] );
				$recebDeDoc->RegistroDePendencias 	= $novoRegPendencias['registroId'];
				$recebDeDoc->update();
			}

			else
			{
				$recebDeDoc->registroDePendencias->editarItens( $_POST['itens_pgto_aprovados'], $_POST['itens_pgto_aprovados_obs'] );
			}

			if( $_POST['action'] == 'aut_pgto' )
			{
				if( $recebDeDoc->bordero != NULL )
				{
					$cs->registerScriptFile($baseUrl . '/js/fn-grid-editarBordero.js', 							CClientScript::POS_END);
					$this->render( 'gridEditarBordero', array( 'parceiro' => Filial::model()->findByPk( $_POST['destinatario'] ), 'processo'=>$recebDeDoc ) );
				}
				else
				{
					$this->render( 'gridBordero', array( 'parceiro' => Filial::model()->findByPk( $_POST['destinatario'] ) ) );
				}
			}
			else
			{	
				$recebDeDoc  							= RecebimentoDeDocumentacao::model()->findByPk( $_POST['processoId'] );
				$this->render( 'resumoProcesso', array('processo' => $recebDeDoc ) );
			}
		}
		*/
	}


	public function actionEfetivarBordero()
	{
		ini_set('max_execution_time', 1699999);

		$baseUrl = Yii::app()->baseUrl;
		$cs = Yii::app()->getClientScript();

		$cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
		$cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
		$cs->registerScriptFile($baseUrl . '/js/util.js', 											CClientScript::POS_END);
		$cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', 	CClientScript::POS_END);
		$cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', 	CClientScript::POS_END);
		$cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js',        CClientScript::POS_END);
		$cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js',		CClientScript::POS_END);
		$cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js',           CClientScript::POS_END);
		$cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js',    CClientScript::POS_END);

		$arrFiliais = [];

		for ($i = 0; $i < sizeof($_POST['filiaisProcesso']); $i++)
		{
			$arrFiliais[$_POST['filiaisProcesso'][$i]]['filial'] 	= $_POST['filiaisProcesso'][$i];
			$arrFiliais[$_POST['filiaisProcesso'][$i]]['propostas'] = [];
                        $arrFiliais[$_POST['filiaisProcesso'][$i]]['imagens'] = [];
		}
                
                for ($j = 1; $j < sizeof($_POST['imagens_pgto_aprovados']); $j++)
		{       
                        $proposta = Proposta::model()->findByPk( $_POST['itens_pgto_aprovados'][$j] );
			$arrFiliais[$proposta->analiseDeCredito->filial->id]['imagens'][] = $_POST['imagens_pgto_aprovados'][$j];
		}
                
		for ($y = 0; $y < sizeof($_POST['itens_pgto_aprovados']); $y++){

			$proposta = Proposta::model()->findByPk( $_POST['itens_pgto_aprovados'][$y] );

			if( $proposta != null )
			{
				$arrFiliais[$proposta->analiseDeCredito->filial->id]['propostas'][] = $proposta->id;
			}
		}
		
		
		if( $_POST['processoId'] == 0 )
		{
			foreach($arrFiliais as $fconfig)
			{
				if( sizeof( $fconfig['propostas'] ) > 0 )
				{
					$novoBordero = Bordero::model()->novo( $fconfig['propostas'], $fconfig['filial'], $fconfig['imagens']);

					if( !$novoBordero['hasErrors'] )
					{
						$recebDeDoc 			= RecebimentoDeDocumentacao::model()->novo( $novoBordero['borderoId'], NULL, $fconfig['filial'] );
					}
				}
			}

			$this->redirect('/recebimentoDeDocumentacao/historico/');
		}
	}

	public function actionParceiro()
	{
		$parceiro_id 	= $_POST['Parceiro'];

		$baseUrl 		= Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
		$this->render( 'iniciar', array('parceiro'=>Filial::model()->find('id = '.$parceiro_id .' AND habilitado' ) ) );
	}

	public function actionIniciarBordero()
	{
		
		//$parceiroId 	= $_POST['parceiroId'];
		$baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        
        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
	    
        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/fn-grid-bordero3.js', 								CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/util.js', 											CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', 	CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', 	CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js',        CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js',		CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js',           CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js',    CClientScript::POS_END);

		//$this->render( 'gridBordero', array( 'parceiro' => Filial::model()->findByPk( $parceiroId ) ) );
		$this->render('gridBordero');
	}

	public function actionRegistrarPendencias()
	{
            	
		$baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        
        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
	    
        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/fn-grid-pendencias.js',								CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/util.js', 											CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', 	CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', 	CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js',        CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js',		CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js',           CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js',    CClientScript::POS_END);

		$this->render('gridPendencias');
	}
        
    public function actionRelatorioPendencias() {
        
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        
        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/datepicker.css');
	    
        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile ($baseUrl . '/js/select2.min.js' , CClientScript::POS_END    );
        $cs->registerScriptFile($baseUrl . '/js/pendencias/fn-grid-relatorio-pendencias.js',								CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/util.js', 											CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', 	CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', 	CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.12.js',        CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.pnotify.min.js',		CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-datepicker.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js',           CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js',    CClientScript::POS_END);
        
       
        $cs->registerScript('datepicker', "$('.date-picker').datepicker({autoclose: true});");

		$this->render('relatorioPendencias');
    }
        
    public function actionGetRelatorioPendencias() {
        
            $recordsTotal       = 0;
            $recordsFiltered    = 0;
            
            $Qry = "SELECT P.codigo, D.numero, PE.nome, P.valor - P.valor_entrada AS valor_financiado, "
                    . "CASE WHEN TC.ModalidadeId = 1 THEN P.valor - P.valor_entrada else (P.valor - P.valor_entrada) - ((P.valor - P.valor_entrada) * (FA.porcentagem_retencao/100)) end "
                    . "AS valor_repasse, IP.observacao, P.data_cadastro, DATEDIFF(LEFT(now(),10), LEFT(RP.dataCriacao,10)) AS dias_atrasados   ";
            $Qry .= " FROM       ItemPendente         AS     IP                                                            ";
            $Qry .= " INNER JOIN RegistroDePendencias AS RP ON RP.id = IP.RegistroDePendencias_id ";
            $Qry .= " INNER JOIN Proposta             AS P ON P.id = IP.ItemPendente AND P.habilitado               ";
            $Qry .= " INNER JOIN Analise_de_Credito   AS A ON A.id = P.Analise_de_Credito_id AND A.habilitado                 ";
            $Qry .= " INNER JOIN Cliente              AS C ON C.id = A.Cliente_id AND C.habilitado ";
            $Qry .= " INNER JOIN Pessoa               AS PE ON PE.id = C.Pessoa_id  ";
            $Qry .= " INNER JOIN Pessoa_has_Documento AS PD ON PE.id = PD.Pessoa_id AND PD.habilitado  ";
            $Qry .= " INNER JOIN Documento            AS D ON D.id = PD.Documento_id AND D.habilitado ";
            $Qry .= " INNER JOIN Tipo_documento       AS T ON T.id = D.Tipo_documento_id AND D.Tipo_documento_id = '1'  ";
            $Qry .= " INNER JOIN Tabela_Cotacao AS TC ON TC.id = P.Tabela_id AND TC.habilitado";
            $Qry .= " INNER JOIN Fator AS FA ON FA.Tabela_Cotacao_id = TC.id AND FA.carencia = P.carencia AND FA.parcela = P.qtd_parcelas AND FA.habilitado";
            $Qry .= " INNER JOIN Filial AS FIL ON FIL.id = A.Filial_id AND FIL.habilitado ";
            $Qry .= " INNER JOIN NucleoFiliais AS N ON N.id = FIL.NucleoFiliais_id AND N.habilitado";
            $Qry .= " WHERE IP.habilitado  ";
            
            $resultQuery = Yii::app()->db->createCommand($Qry)->queryAll();
            
            $recordsTotal = count($resultQuery);
            
            //colocar filtros aqui          
           
            if (isset($_POST["codigo"]) && $_POST["codigo"] != "") {
                $Qry .= " AND P.codigo LIKE '" . $_POST["codigo"] . "%'";
            }
            
            if (isset($_POST["filiais"]) && $_POST["filiais"] != "") {
                $Qry .= " AND N.id = " . $_POST["filiais"] . "";  
            }
           
            if (isset($_POST["financeira"]) && $_POST["financeira"] != "") {
                $Qry .= " AND P.Financeira_id = " . $_POST["financeira"] . "";
            }
            
            /*if ((isset($_POST["data_de"]) && $_POST["data_de"] != "") && (isset($_POST["data_ate"]) && $_POST["data_ate"] != "")) {
                $Qry .=  " WHERE P.data_cadastro BETWEEN '"  . $_POST["data_de"] . "%'" . " AND '" . $_POST["data_ate"] . "%'"; 
            }*/
            
            if (isset($_POST["de"]) && $_POST["de"] != "") {
                $Qry .=  " AND LEFT(P.data_cadastro,10) >= '"  . $_POST["de"] . "' "; 
            }
            
            if (isset($_POST["ate"]) && $_POST["ate"] != "") {
                $Qry .=  " AND LEFT(P.data_cadastro,10) <= '"  . $_POST["ate"] . "' "; 
            }
            
            $resultQuery = Yii::app()->db->createCommand($Qry)->queryAll();
            $recordsFiltered = count($resultQuery);
            
            $Qry .= " LIMIT " . $_POST["start"] . ", " . $_POST["length"];
            
            $resultQuery = Yii::app()->db->createCommand($Qry)->queryAll();
            
            $rows = [];
            foreach ($resultQuery as $r) {
                
                $row = array(
                    'codigo'             => $r['codigo'],
                    'numero'             => $r['numero'],
                    'nome'               => $r['nome'],
                    'valor_financiado'   => 'R$ ' .number_format($r['valor_financiado'],2,",","."),
                    'valor_repasse'      => 'R$ ' . number_format($r['valor_repasse'],2,",","."),
                    'obs'                => $r['observacao'], 
                    'atraso'             => '<span class="btn label label-danger">' . $r['dias_atrasados'] . ' dias </span>', 
                );
                
                $rows[] = $row;
                
            }
            
            
            
            echo json_encode(array('data' => $rows,'recordsTotal'=>$recordsTotal,'recordsFiltered'=>$recordsFiltered,"querol"=>$Qry));
            
    }
}