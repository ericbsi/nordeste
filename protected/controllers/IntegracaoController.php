<?php

class IntegracaoController extends Controller {

    public function filters() {

        return array(
                'accessControl', // perform access control for CRUD operations
                'postOnly + delete' // we only allow deletion via POST request
        );
    }

    public function accessRules() {

        return array(
                array(
                        'allow',
                        'actions' => array(
                                'getTotaisProducao',
                                'informacoesProposta',
                                'printHistoricoIntegrado',
                                'getValorCrediarista',
                                'informacoesRecebimentos',
                                'ajustaTitulosPagar',
                                'revisarLinksContrato',
                                'testeCNAB',
                                'testeWSMareMansa'
                        ),
                        'users' => array(
                                '*'
                        ),
                ),
                array(
                        'allow',
                        'actions' => array(
                                'validarNFe',
                                'listarPropostasNFe',
                                'anexarXmlNf',
                                'listarNFeCNAB',
                                'listarPropostasLecca',
                                'propostaXMLparaCNAB',
                                'gerarCNAB',
                                'buscarNFeWS',
                                'importarFIDC',
                                'gerarArquivoLecca',
                                'getValoresSelecionadosLecca',
                                'listarArquivosCNAB',
                                'gerarBorderosCNAB',
                                'getPropostasArquivo'
                        ),
                        'users' => array(
                                '@'
                        )
                ),
                array(
                        'deny',
                        'users' => array(
                                '*'
                        )
                )
        );
    }

    public function actionCadastroCliente() {
        $util = new Util;
        $cpf = $_POST['cpf'];

        $cliente = Cliente::model()->getClienteByCpf($cpf);

        if ($cliente != null) {
            $nome = $cliente->pessoa->nome;
            $sexo = $cliente->pessoa->sexo;

            if ($cliente->pessoa->nascimento != NULL) {
                $nascimento = $util->bd_date_to_view($cliente->pessoa->nascimento);
            }

            $rg = $cliente->pessoa->getRG();
            $rg_num = ( $rg->numero != NULL ? $rg->numero : NULL );
            $rg_emissor = ( $rg->orgao_emissor != NULL ? $rg->orgao_emissor : NULL );
            $rg_ufemisor = ( $rg->uf_emissor != NULL ? $rg->uf_emissor : NULL );
            $rg_dtemissao = ( $rg->data_emissao != NULL ? $util->bd_date_to_view($rg->data_emissao) : NULL );
            $nacionalide = $cliente->pessoa->nacionalidade;
            $naturalidade = $cliente->pessoa->naturalidade;
            $est_civil = $cliente->pessoa->Estado_Civil_id;
            $conj_renda = $cliente->getCadastro()->conjugue_compoe_renda;
            $clt_casa = $cliente->getCadastro()->cliente_da_casa;
            $nome_mae = $cliente->getCadastro()->nome_da_mae;
            $nome_pai = $cliente->getCadastro()->nome_do_pai;
            $num_dependentes = $cliente->getCadastro()->numero_de_dependentes;
            $cidade = $cliente->pessoa->naturalidade_cidade;
        }
    }

    public function actionGetValorCrediarista() {

        header('Access-Control-Allow-Origin: *');

        $this->layout = 'empty_template';

        $retorno = 0;

        if (isset($_POST)) {
            $usuario = Usuario::model()->find("habilitado AND lower(username) = '" . strtolower($_POST['username']) . "' ");
            $retorno = $usuario->getSaldoPontos();
        }

        echo $retorno;
    }

    public function actionRevisarLinksContrato() {

        header('Access-Control-Allow-Origin: *');

        $this->layout = 'empty_template';

        $retorno = [
                "sucesso" => 1,
                "mensagem" => "Links atualizados com sucesso."
        ];

        if (isset($_POST["codigoProposta"])) {

            $proposta = Proposta::model()->find("habilitado AND codigo = '" . $_POST["codigoProposta"] . "'");

            if ($proposta !== null) {

                $pOmniConfig = PropostaOmniConfig::model()->find("habilitado AND Proposta = $proposta->id");

                if ($pOmniConfig !== null) {

                    $pOmniConfig->atualizarLinks();
                } else {

                    /*                    $message            = new YiiMailMessage    ;
                      $message->view      = "mandaMailFichamento" ;
                      $message->subject   = 'Links Contrato'      ;

                      $parametros         =   [
                      'email' =>  [
                      'link'      => ""                               ,
                      'titulo'    => "Links Contrato 1"               ,
                      'mensagem'  => "Não achou a Proposta Config"    ,
                      'tipo'      => "1"
                      ]
                      ];

                      $message->setBody($parametros, 'text/html');

                      $message->addTo('andre@credshow.com.br');
                      $message->from = 'no-reply@credshow.com.br';

                      Yii::app()->mail->send($message); */

                    $retorno = [
                            "sucesso" => 0,
                            "mensagem" => "Configurações de PropostaOmniConfig não encontradas."
                    ];
                }
            } else {

                /*                $message            = new YiiMailMessage    ;
                  $message->view      = "mandaMailFichamento" ;
                  $message->subject   = 'Links Contrato'      ;

                  $parametros         =   [
                  'email' =>  [
                  'link'      => ""                       ,
                  'titulo'    => "Links Contrato"         ,
                  'mensagem'  => "Não achou a proposta"   ,
                  'tipo'      => "1"
                  ]
                  ];

                  $message->setBody($parametros, 'text/html');

                  $message->addTo('andre@credshow.com.br');
                  $message->from = 'no-reply@credshow.com.br';

                  Yii::app()->mail->send($message); */

                $retorno = [
                        "sucesso" => 0,
                        "mensagem" => "Proposta não encontrada."
                ];
            }
        } else {

            /*            $message            = new YiiMailMessage    ;
              $message->view      = "mandaMailFichamento" ;
              $message->subject   = 'Links Contrato'      ;

              $parametros         =   [
              'email' =>  [
              'link'      => ""                   ,
              'titulo'    => "Links Contrato"     ,
              'mensagem'  => "Não veio o POST"    ,
              'tipo'      => "1"
              ]
              ];

              $message->setBody($parametros, 'text/html');

              $message->addTo('andre@credshow.com.br');
              $message->from = 'no-reply@credshow.com.br';

              Yii::app()->mail->send($message); */
        }

        echo json_encode($retorno);
    }

    public function actionTestes() {

        header('Access-Control-Allow-Origin: *');
        $this->layout = '//printer/template';
        /* $this->layout = 'empty_template';

          $retorno = [];
          $filiais = Filial::model()->findAll();

          foreach ($filiais as $f) {
          $retorno[] = ['id' => $f->id, 'nome' => $f->nome_fantasia];
          }

          echo json_encode($retorno); */
    }

    public function actionTesteCNAB() {

        $notaFiscal = NotaFiscal::model()->findByPk(3);

        $linhas = $notaFiscal->exportarLinhaCNAB();

        echo $linhas;

        //        echo "eita";
    }

    public function actionAjustaTitulosPagar() {
        Bordero::model()->ajustarTitulosPagar();
    }

    public function actionInformacoesRecebimentos() {

        echo json_encode(RecebimentoDeDocumentacao::model()->returnInformacoes($_POST['id']));
    }

    public function actionInformacoesProposta() {
        echo json_encode(Proposta::model()->returnInformacoes($_POST['id']));
    }

    public function actionGetTotaisProducao() {
        echo json_encode(Empresa::model()->producaoCont($_POST['de'], $_POST['ate'], $_POST['filiais']));
    }

    public function actionPrintHistoricoIntegrado() {
        $this->layout = '//printer/template';

        $this->render('recebimentoDeContratoReport', array('processo' => RecebimentoDeDocumentacao::model()->find('codigo = ' . $_POST['codigo'])));
    }

    public function init() {
        
    }

    public function actionValidarNFe() {
        $this->layout = '//layouts/main_sigac_template';

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/integracao/importarNFe/fn-propostas-NFE.js', CClientScript::POS_END);

        $this->render('propostasNFE');
    }

    public function actionPropostaXMLparaCNAB() {

        $this->layout = '//layouts/main_sigac_template';

        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();

        Bordero::model()->ajustarTitulosPagar();

        $cs->registerCssFile($baseUrl . '/css/sigax_template/jquery.dataTables-1.10.0.css');
        $cs->registerCssFile($baseUrl . '/css/sigax_template/dataTables.tableTools.css');
        $cs->registerCssFile($baseUrl . '/css/bootstrap-switch.css');

        $cs->registerScriptFile($baseUrl . '/js/jquery.min.js');
        $cs->registerScriptFile($baseUrl . '/js/select2.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.dataTables-1.10.0.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-switch.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-modalmanager.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.tableTools.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.validate.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/jquery.form.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/dataTables.fnReloadAjax.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/bootstrap-filestyle.min.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/integracao/importarNFe/fn-XMLCNABv5.js', CClientScript::POS_END);
        $cs->registerScriptFile($baseUrl . '/js/integracao/importarNFe/fn-arquivosGerados.js', CClientScript::POS_END);

        $this->render('XMLparaCNAB');
    }

    public function actionListarPropostasNFe() {

        $listaPropostas = Proposta::model()->propostasNFE($_POST['draw'], $_POST['start'], array($_POST['codigo_filter'], $_POST['nome_filter']));

        echo json_encode($listaPropostas);
    }

    public function actionListarNFeCNAB() {

        $hasErrors = false;
        $dados = [];
        $recordsFiltered = 0;
        $recordsTotal = 0;
        $valorTotal = "0,00";
        $valorTotalFinal = "0,00";
        $valorTotalRepasse = "0,00";
        $erro = "";

        $marcados = [];

        $sqlCabecTotais = "SELECT COUNT(*) AS QTD, SUM(P.valor-P.valor_entrada) AS valorTotal, SUM(P.valor_final) as valorTotalFinal, "
            . "CASE WHEN TC.ModalidadeId = 1 THEN SUM(DISTINCT P.valor - P.valor_entrada) ELSE SUM( (P.valor - P.valor_entrada) - ( ((P.valor - P.valor_entrada)/100) * (Fa.porcentagem_retencao))) END AS valorTotalRepasse ";

        $sqlCabec = "SELECT   NF.id               AS idNF             , UPPER(Pe.nome)    AS nomeCliente  ,                   
                             P.codigo           AS codigoProposta   , NF.serie          AS serieNF      ,                   
                            NF.documento        AS documentoNF      , NF.chaveNFe       AS chaveNFe     ,                   
                             P.id               AS idProposta       ,  P.qtd_parcelas   AS qtdParcelas  ,                   
                             P.valor_parcela    AS valorParcela     ,  P.valor_final    AS valorFinal   ,                   
                            NF.data             AS dataNF           ,  P.data_cadastro  AS dataProposta ,
                             M.id               AS idModalidade     ,  M.descricao      AS modalidade ";

        $sql = "FROM         Proposta                AS P                                                                
                    LEFT  JOIN   Venda                   AS V    ON     V.habilitado AND    P.id =    V.Proposta_id            
                    LEFT  JOIN   Venda_has_Nota_Fiscal   AS VhNF ON  VhNF.habilitado AND    V.id = VhNF.Venda_id               
                    LEFT  JOIN   Nota_Fiscal             AS NF   ON    NF.habilitado AND   NF.id = VhNF.Nota_Fiscal_id         
                    INNER JOIN   Analise_de_Credito      AS AC   ON    AC.habilitado AND   AC.id =    P.Analise_de_Credito_id  
                    INNER JOIN   Filial                  AS F    ON     F.habilitado AND    F.id =   AC.Filial_id              
                    INNER JOIN   Filial_has_Endereco     AS FhE  ON   FhE.habilitado AND    F.id =  FhE.Filial_id              
                    INNER JOIN   Endereco                AS E    ON     E.habilitado AND    E.id =  FhE.Endereco_id              
                    INNER JOIN   NucleoFiliais           AS NuF  ON   NuF.habilitado AND  NuF.id =    F.NucleoFiliais_id       
                    INNER JOIN   Cliente                 AS C    ON     C.habilitado AND    C.id =   AC.Cliente_id             
                    INNER JOIN   Pessoa                  AS Pe   ON    Pe.habilitado AND   Pe.id =    C.Pessoa_id              
                    INNER JOIN   Pessoa_has_Documento    AS PHD  ON   PHD.habilitado AND   Pe.id =    PHD.Pessoa_id
                    INNER JOIN   Documento               AS DOC  ON   DOC.habilitado AND  DOC.id =    PHD.Documento_id AND DOC.Tipo_documento_id = 1
                    INNER JOIN   Tabela_Cotacao          AS TC   ON    TC.habilitado AND   TC.id =    P.Tabela_id
                    INNER JOIN   ModalidadeTabela        AS M    ON                         M.id =   TC.ModalidadeId
                    INNER JOIN   Fator                   AS Fa   ON   Fa.habilitado  AND   TC.id =   Fa.Tabela_Cotacao_id AND Fa.carencia = P.carencia AND Fa.parcela = P.qtd_parcelas 
                    WHERE P.habilitado AND P.Status_Proposta_id IN (2,7) AND P.Financeira_id = 10 ";

        //provisorio
        $sql .= " AND NOT EXISTS(   
                                    SELECT VNF.Venda_id, COUNT(*)
                                    FROM Venda_has_Nota_Fiscal AS VNF                                     
                                    WHERE VNF.habilitado AND VNF.Venda_id = V.id                                    
                                    GROUP BY VNF.Venda_id                                     
                                    HAVING COUNT(*) > 1
                                )";
        //provisorio

        try {

            $resultado = Yii::app()->db->createCommand($sqlCabecTotais . $sql)->queryRow();

            $recordsTotal = $resultado["QTD"]; //count($resultado);

            if (!isset($_POST["proNFe"]) || $_POST["proNFe"] == "0") {

                $sql .= " AND NOT EXISTS    (
                                                SELECT * 
                                                FROM        LinhaCNAB   AS LC 
                                                INNER JOIN  Parcela     AS Pa   ON Pa.habilitado AND Pa.id 	= LC.Parcela_id
                                                INNER JOIN  Titulo      AS T    ON  T.habilitado AND  T.id 	= Pa.Titulo_id
                                                WHERE LC.habilitado AND T.NaturezaTitulo_id = 1 AND P.id = T.Proposta_id
                                            ) ";
            } else {

                if (!isset($_POST["semNFe"]) || $_POST["semNFe"] == "0") {

                    $sql .= " AND EXISTS   (
                                                SELECT * 
                                                FROM        LinhaCNAB   AS LC 
                                                INNER JOIN  Parcela     AS Pa   ON Pa.habilitado AND Pa.id 	= LC.Parcela_id
                                                INNER JOIN  Titulo      AS T    ON  T.habilitado AND  T.id 	= Pa.Titulo_id
                                                WHERE LC.habilitado AND T.NaturezaTitulo_id = 1 AND P.id = T.Proposta_id
                                            ) ";
                }
            }

            if ((isset($_POST["comNFe"]) && $_POST["comNFe"] == "1") && (isset($_POST["semNFe"]) && $_POST["semNFe"] == "1") && (!isset($_POST["semChv"]) || $_POST["semChv"] == "0")) {
                $sql .= " AND (NF.id IS NULL OR (NF.chaveNFe IS NOT NULL AND NF.chaveNFe <> '') ) ";
            } else if ((isset($_POST["comNFe"]) && $_POST["comNFe"] == "1") && (!isset($_POST["semNFe"]) || $_POST["semNFe"] == "0") && (!isset($_POST["semChv"]) || $_POST["semChv"] == "0")) {

                $sql .= " AND (NF.chaveNFe IS NOT NULL AND NF.chaveNFe <> '') ";
            } else if ((isset($_POST["comNFe"]) && $_POST["comNFe"] == "1") && (!isset($_POST["semNFe"]) || $_POST["semNFe"] == "0") && (isset($_POST["semChv"]) && $_POST["semChv"] == "1")) {
                $sql .= " AND NF.id IS NOT NULL ";
            } else if ((!isset($_POST["comNFe"]) || $_POST["comNFe"] == "0") && (isset($_POST["semNFe"]) && $_POST["semNFe"] == "1") && (!isset($_POST["semChv"]) || $_POST["semChv"] == "0")) {
                $sql .= " AND NF.id IS NULL ";
            } else if ((!isset($_POST["comNFe"]) || $_POST["comNFe"] == "0") && (isset($_POST["semNFe"]) && $_POST["semNFe"] == "1") && (isset($_POST["semChv"]) && $_POST["semChv"] == "1")) {
                $sql .= " AND ( NF.id IS NULL OR (NF.id IS NOT NULL AND (NF.chaveNFe IS NULL OR NF.chaveNFe = '') ) ) ";
            } else if ((!isset($_POST["comNFe"]) || $_POST["comNFe"] == "0") && (!isset($_POST["semNFe"]) || $_POST["semNFe"] == "0") && (isset($_POST["semChv"]) && $_POST["semChv"] == "1")) {
                $sql .= " AND NF.id IS NOT NULL AND (NF.chaveNFe IS NULL OR NF.chaveNFe = '') ";
            }

            if (isset($_POST["codigo_filter"]) && !empty($_POST["codigo_filter"])) {
                $sql .= " AND P.codigo LIKE '%" . $_POST["codigo_filter"] . "%' ";
            }

            if (isset($_POST["filial_filter"]) && !empty($_POST["filial_filter"])) {
                $sql .= " AND CONCAT(F.nome_fantasia, ' / ', E.cidade, '-', E.uf) LIKE '%" . $_POST["filial_filter"] . "%' ";
            }

            if (isset($_POST["nome_filter"]) && !empty($_POST["nome_filter"])) {
                $sql .= " AND Pe.nome LIKE '%" . $_POST["nome_filter"] . "%' ";
            }

            if (isset($_POST["cpf_filter"]) && !empty($_POST["cpf_filter"])) {
                $sql .= " AND DOC.numero LIKE '%" . $_POST["cpf_filter"] . "%' ";
            }

            if (isset($_POST["nfserie_filter"]) && !empty($_POST["nfserie_filter"])) {
                $sql .= " AND NF.documento LIKE '%" . $_POST["nfserie_filter"] . "%' ";
            }

            if (isset($_POST["data_filter"]) && !empty($_POST["data_filter"])) {
                $sql .= " AND LEFT(P.data_cadastro,10) LIKE '" . $_POST["data_filter"] . "' ";
            }

            if ((isset($_POST["comJuros"]) && $_POST["comJuros"] == "1") && (!isset($_POST["semJuros"]) || $_POST["semJuros"] == "0")) {
                $sql .= " AND TC.ModalidadeId = 1 ";
            } else if ((isset($_POST["semJuros"]) && $_POST["semJuros"] == "1") && (!isset($_POST["comJuros"]) || $_POST["comJuros"] == "0")) {

                $sql .= " AND TC.ModalidadeId = 2 ";
            }

            $resultado = Yii::app()->db->createCommand($sqlCabecTotais . $sql)->queryRow();

            $recordsFiltered = $resultado["QTD"];

            $valorTotal = number_format($resultado["valorTotal"], 2, ",", ".");
            $valorTotalFinal = number_format($resultado["valorTotalFinal"], 2, ",", ".");
            $valorTotalRepasse = number_format($resultado["valorTotalRepasse"], 2, ",", ".");

            $resultado = Yii::app()->db->createCommand($sqlCabec . $sql)->queryAll();

            foreach ($resultado as $r) {

                if (
                    ( isset($_POST["selecionaTudo"]) && $_POST["selecionaTudo"] == "1"
                    ) ||
                    (
                    ( isset($_POST["selecionaTudo"]) && $_POST["selecionaTudo"] == "2" ) &&
                    ( isset($_POST["notasMarcadas"]) && in_array($r["idNF"], $_POST["notasMarcadas"]) )
                    )
                ) {
                    $marcados[] = $r["idNF"];
                }
            }

            $sql .= "ORDER BY NF.id DESC                ";

            if (isset($_POST["start"])) {
                $sql .= "LIMIT " . $_POST["start"] . ", 10  ";
            }

            $resultado = Yii::app()->db->createCommand($sqlCabec . $sql)->queryAll();

            foreach ($resultado as $r) {

                if (isset($r["dataNF"]) && !Empty($r["dataNF"])) {
                    $dateTime = new DateTime($r["dataNF"]);
                    $dataNFe = $dateTime->format("d/m/Y");
                } else {
                    $dataNFe = "  /  /    ";
                }

                $dateTime = new DateTime($r["dataProposta"]);
                $dataProposta = $dateTime->format("d/m/Y H:i:s");

                $proposta = Proposta::model()->findByPk($r["idProposta"]);

                $temCNAB = LinhaCNAB::model()->temProposta($proposta);

                if ($temCNAB) {
                    $check = "<button class='btn btn-green'>"
                        . "     <i class='clip-spinner-4 checkNF' value='" . $r["idNF"] . "'></i>"
                        . "</button>";
                } else if (isset($r["chaveNFe"]) && !Empty($r["chaveNFe"])) { //chaveNFe
                    if (in_array($r["idNF"], $marcados)) {
                        $iClasseSelecionaTudo = "clip-square";
                    } else {
                        $iClasseSelecionaTudo = "clip-checkbox-unchecked-2";
                    }

                    $check = "<button class='btn btn-blue'>"
                        . "     <i class='$iClasseSelecionaTudo checkNF' id='iSelecionar' value='" . $r["idNF"] . "'></i>"
                        . "</button>";
                } else {

                    if (isset($r["idNF"]) && !Empty($r["idNF"])) {
                        $check = "<button class='btn btn-red btnForcarXML' value='" . $r["idProposta"] . "'><i class='clip-file-xml'></i></button>";
                    } else {
                        $check = "<button class='btn btn-yellow btnForcarXML' value='" . $r["idProposta"] . "'><i class='clip-file-xml'></i></button>";
                    }
                }

                $valorFinanciado = "R$ " . number_format($proposta->valor - $proposta->valor_entrada, 2, ",", ".");
                $valorRepasse = "R$ " . number_format($proposta->valorRepasse(), 2, ",", ".");
                $valorParcela = "R$ " . number_format($r["valorParcela"], 2, ",", ".");
                $valorFinal = "R$ " . number_format($r["valorFinal"], 2, ",", ".");

                if ($r["idModalidade"] == "1") {
                    $modalidade = "<b><font color='#cc9'>" . $r["modalidade"] . "</font></b>";
                } else {
                    $modalidade = "<b><font color='#afaa6d'>" . $r["modalidade"] . "</font></b>";
                }

                $dados[] = [
                        "check" => $check,
                        "modalidade" => $modalidade,
                        "codigoProposta" => $r["codigoProposta"],
                        "nomeFilial" => $proposta->analiseDeCredito->filial->getConcat(),
                        "nomeCliente" => $r["nomeCliente"],
                        "cpfCliente" => $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero,
                        "dataNFe" => $dataNFe,
                        "dataProposta" => $dataProposta,
                        "serieNota" => $r["serieNF"] . " - " . $r["documentoNF"],
                        "chaveNFe" => $r["chaveNFe"],
                        "valorFinanciado" => $valorFinanciado,
                        "parcelamento" => $r["qtdParcelas"] . "x " . $valorParcela,
                        "valorFinal" => $valorFinal,
                        "valorRepasse" => $valorRepasse
                ];
            }
        } catch (Exception $ex) {
            $hasErrors = true;
            $erro = "Erro na consulta. Erro: " . $ex->getMessage();
        }

        $retorno = [
                "data" => $dados,
                "recordsFiltered" => $recordsFiltered,
                "recordsTotal" => $recordsTotal,
                "erro" => $erro,
                "sql" => $sqlCabec . ' ' . $sql,
                "hasErrors" => $hasErrors,
                "marcados" => $marcados,
                "valorTotal" => $valorTotal,
                "valorTotalFinal" => $valorTotalFinal,
                "valorTotalRepasse" => $valorTotalRepasse,
        ];

        echo json_encode($retorno);
    }

    public function actionListarPropostasLecca() {

        $bipProposta = "";
        $hasErrors = false;
        $dados = [];
        $recordsFiltered = 0;
        $recordsTotal = 0;
        $valorTotalFinanciado = 0;
        $valorTotalRepasse = 0;
        $valorFiltrFinanciado = 0;
        $valorFiltrRepasse = 0;
        $erro = "";
        $marcados = [];
        $colunas = [
                "1" => "modalidade",
                "2" => "codigoProposta",
                "3" => "CONCAT(F.nome_fantasia, E.cidade, E.uf)",
                "4" => "Pe.nome",
                "5" => "DOC.numero",
                "6" => "dataProposta",
                "7" => "(P.valor-P.valor_entrada)",
                "8" => "CONCAT(LPAD(P.qtd_parcelas,2,'0'), LPAD(P.valor_parcela,20,'0'))",
                "9" => "valorRepasse "
        ];


        $sqlCabecTotais = "SELECT COUNT(*) AS QTD, SUM(P.valor-P.valor_entrada) AS valorTotal, SUM(P.valor_final) as valorTotalFinal, "
            . "CASE WHEN TC.ModalidadeId = 1 THEN SUM(DISTINCT P.valor - P.valor_entrada) ELSE SUM( (P.valor - P.valor_entrada) - ( ((P.valor - P.valor_entrada)/100) * (Fa.porcentagem_retencao))) END AS valorTotalRepasse ";

        $sqlCabec = "SELECT DISTINCT UPPER(Pe.nome)      AS nomeCliente      ,                   
                            P.codigo            AS codigoProposta   ,  P.data_cadastro  AS dataProposta ,
                            P.id                AS idProposta       ,  P.qtd_parcelas   AS qtdParcelas  ,                   
                            P.valor_parcela     AS valorParcela     ,  P.valor_final    AS valorFinal   ,                   
                            M.id                AS idModalidade     ,  M.descricao      AS modalidade   ,
                            CASE WHEN TC.ModalidadeId = 1 THEN (P.valor - P.valor_entrada) ELSE ((P.valor - P.valor_entrada) - ( (P.valor - P.valor_entrada) * (Fa.porcentagem_retencao/100) )) END AS valorRepasse";

        $sql = "
                    FROM         Proposta                AS P                                                                
                    INNER JOIN   Analise_de_Credito      AS AC   ON   AC.habilitado  AND  AC.id =    P.Analise_de_Credito_id  
                    INNER JOIN   Filial                  AS F    ON    F.habilitado  AND   F.id =   AC.Filial_id              
                    INNER JOIN   Filial_has_Endereco     AS FhE  ON  FhE.habilitado  AND   F.id =  FhE.Filial_id              
                    INNER JOIN   Endereco                AS E    ON    E.habilitado  AND   E.id =  FhE.Endereco_id              
                    INNER JOIN   NucleoFiliais           AS NuF  ON  NuF.habilitado  AND NuF.id =    F.NucleoFiliais_id       
                    INNER JOIN   Cliente                 AS C    ON    C.habilitado  AND   C.id =   AC.Cliente_id             
                    INNER JOIN   Pessoa                  AS Pe   ON   Pe.habilitado  AND  Pe.id =    C.Pessoa_id              
                    INNER JOIN   Pessoa_has_Documento    AS PHD  ON   PHD.habilitado AND  Pe.id =    PHD.Pessoa_id
                    INNER JOIN   Documento               AS DOC  ON   DOC.habilitado AND DOC.id =    PHD.Documento_id AND DOC.Tipo_documento_id = 1
                    INNER JOIN   Tabela_Cotacao          AS TC   ON   TC.habilitado  AND  TC.id =    P.Tabela_id
                    INNER JOIN   ModalidadeTabela        AS M    ON                        M.id =   TC.ModalidadeId
                    INNER JOIN   Fator                   AS Fa   ON   Fa.habilitado  AND  TC.id =   Fa.Tabela_Cotacao_id AND Fa.carencia = P.carencia AND Fa.parcela = P.qtd_parcelas 
                    LEFT  JOIN   ItemDoBordero           AS IB   ON   IB.habilitado  AND IB.Proposta_id = P.id 
                    WHERE IB.id IS NULL AND P.habilitado AND P.Status_Proposta_id IN (2,7) AND P.Financeira_id = 11 ";

        try {

            $sql .= " AND NOT EXISTS    (
                                            SELECT * 
                                            FROM        LinhaCNAB   AS LC 
                                            INNER JOIN  Parcela     AS Pa   ON Pa.habilitado AND Pa.id 	= LC.Parcela_id
                                            INNER JOIN  Titulo      AS T    ON  T.habilitado AND  T.id 	= Pa.Titulo_id
                                            WHERE LC.habilitado AND T.NaturezaTitulo_id = 2 AND P.id = T.Proposta_id
                                        ) ";

            $resultado = Yii::app()->db->createCommand($sqlCabecTotais . $sql)->queryRow();

            $valorTotalFinanciado = number_format($resultado["valorTotal"], 2, ",", ".");
            $valorTotalRepasse = number_format($resultado["valorTotalRepasse"], 2, ",", ".");
            $recordsTotal = $resultado["QTD"];

            if (isset($_POST["codigo_filter"]) && !empty($_POST["codigo_filter"])) {
                $sql .= " AND P.codigo LIKE '%" . $_POST["codigo_filter"] . "%' ";
            }

            if (isset($_POST["filial_filter"]) && !empty($_POST["filial_filter"])) {
                $sql .= " AND CONCAT(F.nome_fantasia, ' / ', E.cidade, '-', E.uf) LIKE '%" . $_POST["filial_filter"] . "%' ";
            }

            if (isset($_POST["nome_filter"]) && !empty($_POST["nome_filter"])) {
                $sql .= " AND Pe.nome LIKE '%" . $_POST["nome_filter"] . "%' ";
            }

            if (isset($_POST["cpf_filter"]) && !empty($_POST["cpf_filter"])) {
                $sql .= " AND DOC.numero LIKE '%" . $_POST["cpf_filter"] . "%' ";
            }

            if (isset($_POST["nfserie_filter"]) && !empty($_POST["nfserie_filter"])) {
                $sql .= " AND NF.documento LIKE '%" . $_POST["nfserie_filter"] . "%' ";
            }

            if (isset($_POST["data_filter"]) && !empty($_POST["data_filter"])) {
                $sql .= " AND LEFT(P.data_cadastro,10) LIKE '" . $_POST["data_filter"] . "' ";
            }

            if ((isset($_POST["comJuros"]) && $_POST["comJuros"] == "1") && (!isset($_POST["semJuros"]) || $_POST["semJuros"] == "0")) {
                $sql .= " AND TC.ModalidadeId = 1 ";
            } else if ((isset($_POST["semJuros"]) && $_POST["semJuros"] == "1") && (!isset($_POST["comJuros"]) || $_POST["comJuros"] == "0")) {

                $sql .= " AND TC.ModalidadeId = 2 ";
            }

            if (isset($_POST["filtroMarcadas"]) && isset($_POST["propostasMarcadas"]) && $_POST["filtroMarcadas"] !== "2") {

                $filtroMarcadas = join(",", $_POST["propostasMarcadas"]);

                $sql .= " AND  P.id ";

                if ($_POST["filtroMarcadas"] == "0") {
                    $sql .= " NOT ";
                }

                $sql .= " IN ($filtroMarcadas) ";
            }

            $resultado = Yii::app()->db->createCommand($sqlCabecTotais . $sql)->queryRow();

            $valorFiltrFinanciado = number_format($resultado["valorTotal"], 2, ",", ".");
            $valorFiltrRepasse = number_format($resultado["valorTotalRepasse"], 2, ",", ".");
            $recordsFiltered = $resultado["QTD"];

            $resultado = Yii::app()->db->createCommand($sqlCabec . $sql)->queryAll();

            foreach ($resultado as $r) {

                if (
                    ( isset($_POST["propostasMarcadas"]) && in_array($r ["idProposta"], $_POST["propostasMarcadas"]))
                ) {
                    $marcados[] = $r["idProposta"];
                }
            }

            $ordem = $_POST["order"][0];

            $ordemCriterio = $colunas[$ordem["column"]];
            $ordenacao = $ordem["dir"];

            $sql .= "ORDER BY $ordemCriterio $ordenacao ";

            if (isset($_POST["start"])) {
                $sql .= "LIMIT " . $_POST["start"] . ", " . $_POST["length"];
            }

            $resultado = Yii::app()->db->createCommand($sqlCabec . $sql)->queryAll();

            foreach ($resultado as $r) {

                $dateTime = new DateTime($r["dataProposta"]);
                $dataProposta = $dateTime->format("d/m/Y H:i:s");

                $proposta = Proposta::model()->findByPk($r["idProposta"]);

                $temCNAB = LinhaCNAB::model()->temProposta($proposta);

                $bipProposta = "";

                if ($temCNAB) {
                    $check = "<button class='btn btn-green'>"
                        . "     <i class='clip-spinner-4 checkProposta' value='" . $r["idProposta"] . "'></i>"
                        . "</button>";
                } else {

                    if (in_array($r["idProposta"], $marcados) || (isset($_POST["codigo_filter"]) && $_POST["codigo_filter"] == $proposta->codigo)) {
                        $bipProposta = $r["idProposta"];
                        $iClasseSelecionaTudo = "clip-square";
                    } else {
                        $iClasseSelecionaTudo = "clip-checkbox-unchecked-2";
                    }

                    $check = "<button class='btn btn-blue'>"
                        . "     <i class='$iClasseSelecionaTudo checkProposta' id='iSelecionar' value='" . $r["idProposta"] . "'></i>"
                        . "</button>";
                }

                $valorFinanciado = "R$ " . number_format($proposta->getValorFinanciado(), 2, ",", ".");
                $valorParcela = "R$ " . number_format($r["valorParcela"], 2, ",", ".");
                $valorFinal = "R$ " . number_format($r["valorFinal"], 2, ",", ".");
                $valorRepasse = "R$ " . number_format($r["valorRepasse"], 2, ",", ".");

                if ($r["idModalidade"] == "1") {
                    $modalidade = "<b><font color='#cc9'>" . $r["modalidade"] . "</font></b>";
                } else {
                    $modalidade = "<b><font color='#afaa6d'>" . $r["modalidade"] . "</font></b>";
                }

                $dados[] = [
                        "check" => $check,
                        "modalidade" => $modalidade,
                        "codigoProposta" => $r["codigoProposta"],
                        "nomeFilial" => $proposta->analiseDeCredito->filial->getConcat(),
                        "nomeCliente" => $r["nomeCliente"],
                        "cpfCliente" => $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero,
                        "dataProposta" => $dataProposta,
                        "valorFinanciado" => $valorFinanciado,
                        "parcelamento" => $r["qtdParcelas"] . "x " . $valorParcela,
                        "valorRepasse" => $valorRepasse
                ];
            }
        } catch (Exception $ex) {
            $hasErrors = true;
            $erro = "Erro na consulta. Erro: " . $ex->getMessage();
        }

        ob_start();
        var_dump($_POST["order"]);
        $order = ob_get_clean();

        ob_start();
        var_dump($order[0]);
        $order0 = ob_get_clean();

        $retorno = [
                "data" => $dados,
                "recordsFiltered" => $recordsFiltered,
                "recordsTotal" => $recordsTotal,
                "vlrFilFin" => $valorFiltrFinanciado,
                "vlrFilRep" => $valorFiltrRepasse,
                "vlrTotFin" => $valorTotalFinanciado,
                "vlrTotRep" => $valorTotalRepasse,
                "erro" => $erro,
                "sql" => $sql,
                "hasErrors" => $hasErrors,
                "marcados" => $marcados,
                "bipProposta" => $bipProposta,
                "order" => $order,
                "order0" => $order0
        ];

        echo json_encode($retorno);
    }

    public function actionAnexarXmlNf() {

        $anexoXML = $_FILES['anexoXML'];
        $objXML = simplexml_load_file($anexoXML["tmp_name"]);

        $retorno = [
                "hasErrors" => false,
                "msg" => "XML importado com sucesso",
                "type" => "success"
        ];

        if (isset($_POST['proposta-id'])) {
            $proposta = Proposta::model()->find("habilitado AND id = " . $_POST["proposta-id"]);

            if ($proposta !== null) {

                if (trim($proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero) !== trim($objXML->NFe->infNFe->dest->CPF)) {
                    $retorno = [
                            "hasErrors" => true,
                            "msg" => "CPF divergente",
                            "type" => "error"
                    ];
                } else {

                    $transaction = Yii::app()->db->beginTransaction();

                    $venda = Venda::model()->find("habilitado AND Proposta_id = $proposta->id");

                    if ($venda == null) {

                        $venda = new Venda;

                        $venda->habilitado = 1;
                        $venda->data_cadastro = date("Y-m-d H:i:s");
                        $venda->Proposta_id = $proposta->id;

                        if (!$venda->save()) {

                            ob_start();
                            var_dump($venda->getErrors());
                            $resErro = ob_get_clean();

                            $retorno = [
                                    "hasErrors" => true,
                                    "msg" => "Erro ao gravar registro da venda: $resErro",
                                    "type" => "error"
                            ];
                        }
                    }

                    if (!($retorno["hasErrors"])) {

                        $notaFiscal = NotaFiscal::model()->find("habilitado AND documento = '" . $objXML->NFe->infNFe->ide->nNF . "' AND serie = '" . $objXML->NFe->infNFe->ide->serie . "'");

                        if ($notaFiscal !== null) { //caso ja exista uma Nota Fiscal com esse numero e serie
                            $vendaHasNF = VendaHasNotaFiscal::model()->find("habilitado AND Nota_Fiscal_id = $notaFiscal->id");

                            //verifica se, por algum motivo, essa notaFiscal foi atrelada a uma venda diferente
                            if ($vendaHasNF->id !== $venda->id) {

                                $vendaHasNF->habilitado = 0; //desabilite esse registro

                                if (!$vendaHasNF->update()) {

                                    ob_start();
                                    var_dump($vendaHasNF->getErrors());
                                    $resErro = ob_get_clean();

                                    $retorno = [
                                            "hasErrors" => true,
                                            "msg" => "Erro ao gravar registro da venda (VendaHasNF): $resErro",
                                            "type" => "error"
                                    ];
                                } else {

                                    //crie um novo registro para atrelar a Nota Fiscal a Venda correta
                                    $vendaHasNF = new VendaHasNotaFiscal;
                                    $vendaHasNF->habilitado = 1;
                                    $vendaHasNF->data_cadastro = date("Y-m-d H:i:s");
                                    $vendaHasNF->Nota_Fiscal_id = $notaFiscal->id;
                                    $vendaHasNF->Venda_id = $venda->id;

                                    if (!$vendaHasNF->save()) {

                                        ob_start();
                                        var_dump($vendaHasNF->getErrors());
                                        $resErro = ob_get_clean();

                                        $retorno = [
                                                "hasErrors" => true,
                                                "msg" => "Erro ao gravar registro da venda (VendaHasNF): $resErro",
                                                "type" => "error"
                                        ];
                                    }
                                }
                            }
                        }

                        if (!($retorno["hasErrors"])) {

                            if ($notaFiscal == null) {

                                $notaFiscal = new NotaFiscal;

                                $notaFiscal->data_cadastro = date("Y-m-d H:i:s");
                                $notaFiscal->habilitado = 1;

                                if (!$notaFiscal->save()) {

                                    ob_start();
                                    var_dump($notaFiscal->getErrors());
                                    $erroNF = ob_get_clean();

                                    $retorno = [
                                            "hasErrors" => true,
                                            "msg" => "Erro ao salvar Nota Fiscal. NotaFiscal: $erroNF",
                                            "type" => "error"
                                    ];
                                } else {

                                    $vendaHasNF = new VendaHasNotaFiscal;
                                    $vendaHasNF->habilitado = 1;
                                    $vendaHasNF->data_cadastro = date("Y-m-d H:i:s");
                                    $vendaHasNF->Nota_Fiscal_id = $notaFiscal->id;
                                    $vendaHasNF->Venda_id = $venda->id;

                                    if (!$vendaHasNF->save()) {

                                        ob_start();
                                        var_dump($vendaHasNF->getErrors());
                                        $resErro = ob_get_clean();

                                        $retorno = [
                                                "hasErrors" => true,
                                                "msg" => "Erro ao gravar registro da venda (VendaHasNF): $resErro",
                                                "type" => "error"
                                        ];
                                    }
                                }
                            }

                            if (!($retorno["hasErrors"])) {

                                $statusNF = StatusNotaFiscal::model()->find("habilitado AND label = '" . $objXML->protNFe->infProt->cStat . "'");

                                if ($statusNF == null) {
                                    $statusNF = StatusNotaFiscal::model()->findByPk(1);
                                }

                                $notaFiscal->Status_Nota_Fiscal_id = $statusNF->id;
                                $notaFiscal->documento = $objXML->NFe->infNFe->ide->nNF;
                                $notaFiscal->serie = $objXML->NFe->infNFe->ide->serie;
                                $notaFiscal->observacao = $objXML->NFe->infNFe->infAdic->infCpl;
                                $notaFiscal->data = substr($objXML->NFe->infNFe->ide->dhEmi, 0, 10);
                                $notaFiscal->chaveNFe = $objXML->protNFe->infProt->chNFe;

                                if (!$notaFiscal->update()) {

                                    ob_start();
                                    var_dump($notaFiscal->getErrors());
                                    $erroNF = ob_get_clean();

                                    $retorno = [
                                            "hasErrors" => true,
                                            "msg" => "Erro ao salvar Nota Fiscal. NotaFiscal: $erroNF",
                                            "type" => "error"
                                    ];

                                    $transaction->rollBack();
                                } else {
                                    $transaction->commit();
                                }
                            } else {
                                $transaction->rollBack();
                            }
                        }
                    } else {
                        $transaction->rollBack();
                    }
                }
            } else {
                $retorno = [
                        "hasErrors" => true,
                        "msg" => "Proposta não encontrada",
                        "type" => "error"
                ];
            }
        } else {

            $retorno = [
                    "hasErrors" => true,
                    "msg" => "ID da proposta não enviado",
                    "type" => "error"
            ];
        }

        /*
          $proposta = Proposta::model()->findByPk($propostaId);
          Anexo::model()->novo($ComprovanteFile, 'proposta_xmlnf', $proposta->id, 'XML DE NOTA FISCAL - ' . $proposta->codigo); */

        echo json_encode($retorno);
    }

    public function actionGerarCNAB() {

        $retorno = [
                "hasErrors" => false,
                "title" => "Sucesso",
                "msg" => "CNAB gerado com sucesso",
                "type" => "success",
                "nomeArquivo" => "nada",
                "url" => "nada",
                "cnab" => ""
        ];

        $sql = "SELECT   NF.id               AS idNF
                    FROM         Proposta                AS P                                                                
                    INNER JOIN   Venda                   AS V    ON    V.habilitado AND   P.id =    V.Proposta_id            
                    INNER JOIN   Venda_has_Nota_Fiscal   AS VhNF ON VhNF.habilitado AND   V.id = VhNF.Venda_id               
                    INNER JOIN   Nota_Fiscal             AS NF   ON   NF.habilitado AND  NF.id = VhNF.Nota_Fiscal_id         
                    INNER JOIN   Analise_de_Credito      AS AC   ON   AC.habilitado AND  AC.id =    P.Analise_de_Credito_id  
                    INNER JOIN   Filial                  AS F    ON    F.habilitado AND   F.id =   AC.Filial_id              
                    INNER JOIN   Filial_has_Endereco     AS FhE  ON  FhE.habilitado AND   F.id =  FhE.Filial_id              
                    INNER JOIN   Endereco                AS E    ON    E.habilitado AND   E.id =  FhE.Endereco_id              
                    INNER JOIN   NucleoFiliais           AS NuF  ON  NuF.habilitado AND NuF.id =    F.NucleoFiliais_id       
                    INNER JOIN   Cliente                 AS C    ON    C.habilitado AND   C.id =   AC.Cliente_id             
                    INNER JOIN   Pessoa                  AS Pe   ON   Pe.habilitado AND  Pe.id =    C.Pessoa_id              
                    INNER JOIN   Tabela_Cotacao          AS TC   ON   TC.habilitado AND  TC.id =    P.Tabela_id
                    INNER JOIN   ModalidadeTabela        AS M    ON                       M.id =   TC.ModalidadeId
                    WHERE P.habilitado AND P.Status_Proposta_id IN (2,7) AND P.Financeira_id = 10 AND (NF.chaveNFe IS NOT NULL AND NF.chaveNFe <> '') ";

        //provisorio
        $sql .= " AND NOT EXISTS(   
                                    SELECT VNF.Venda_id, COUNT(*)
                                    FROM Venda_has_Nota_Fiscal AS VNF                                     
                                    WHERE VNF.habilitado AND VNF.Venda_id = V.id                                    
                                    GROUP BY VNF.Venda_id                                     
                                    HAVING COUNT(*) > 1
                                )";

        $sql .= " AND NOT EXISTS    (
                                        SELECT * 
                                        FROM        LinhaCNAB   AS LC 
                                        INNER JOIN  Parcela     AS Pa   ON Pa.habilitado AND Pa.id 	= LC.Parcela_id
                                        INNER JOIN  Titulo      AS T    ON  T.habilitado AND  T.id 	= Pa.Titulo_id
                                        WHERE LC.habilitado AND T.NaturezaTitulo_id = 1 AND P.id = T.Proposta_id
                                    ) ";
        //provisorio

        try {

            if (isset($_POST["codigo_filter"]) && !empty($_POST["codigo_filter"])) {
                $sql .= " AND P.codigo LIKE '%" . $_POST["codigo_filter"] . "%' ";
            }

            if (isset($_POST["filial_filter"]) && !empty($_POST["filial_filter"])) {
                $sql .= " AND CONCAT(F.nome_fantasia, ' / ', E.cidade, '-', E.uf) LIKE '%" . $_POST["filial_filter"] . "%' ";
            }

            if (isset($_POST["nome_filter"]) && !empty($_POST["nome_filter"])) {
                $sql .= " AND Pe.nome LIKE '%" . $_POST["nome_filter"] . "%' ";
            }

            if (isset($_POST["nfserie_filter"]) && !empty($_POST["nfserie_filter"])) {
                $sql .= " AND NF.documento LIKE '%" . $_POST["nfserie_filter"] . "%' ";
            }

            if (isset($_POST["data_filter"]) && !empty($_POST["data_filter"])) {
                $sql .= " AND LEFT(P.data_cadastro,10) LIKE '" . $_POST["data_filter"] . "' ";
            }

            if (isset($_POST["notasMarcadas"])) {

                $nm = join(",", $_POST["notasMarcadas"]);

                $sql .= " AND NF.id in ($nm) ";
            }

            if ((isset($_POST["comJuros"]) && $_POST["comJuros"] == "1") && (!isset($_POST["semJuros"]) || $_POST["semJuros"] == "0")) {
                $sql .= " AND TC.ModalidadeId = 1 ";
            } else if ((isset($_POST["semJuros"]) && $_POST["semJuros"] == "1") && (!isset($_POST["comJuros"]) || $_POST["comJuros"] == "0")) {

                $sql .= " AND TC.ModalidadeId = 2 ";
            }

            $sql .= "ORDER BY NF.id DESC ";

            $resultado = Yii::app()->db->createCommand($sql)->queryAll();

            foreach ($resultado as $r) {
                $notasMarcadas[] = $r["idNF"];
            }


            if (!empty($notasMarcadas)) {

                $linhas = "";

                $retLinhasCNAB = $this->exportarLinhasCNAB($notasMarcadas);

                $cnab = $retLinhasCNAB["linhas"];
                $arquivoCNAB = $retLinhasCNAB["arquivoCNAB"];

                if (!empty($cnab)) {

                    $pasta = "remessaFDIC";

                    if (!is_dir($pasta)) {
                        mkdir("remessaFDIC", 0700);
                    }

                    $cont = 1;

                    $arquivo = "CB" . date("dm") . str_pad($cont, 2, "0", STR_PAD_LEFT) . ".txt";

                    while (file_exists($pasta . "/" . $arquivo)) {

                        $cont++;

                        $arquivo = "CB" . date("dm") . str_pad($cont, 2, "0", STR_PAD_LEFT) . ".txt";
                    }

                    $file = fopen($pasta . "/" . $arquivo, 'w');
                    fwrite($file, $cnab);
                    fclose($file);

                    $retorno["url"] = "/" . $pasta . "/" . $arquivo;
                    $retorno["nomeArquivo"] = $arquivo;
                    $retorno["sql"] = $sql;

                    $arquivoCNAB->urlArquivo = $retorno["url"];

                    if (!$arquivoCNAB->update()) {
                        
                    }
                } else {

                    $erroMsg = "";

                    if (isset($retLinhasCNAB["erro"]) && !empty($retLinhasCNAB["erro"])) {
                        $erroMsg = $retLinhasCNAB["erro"];
                    }

                    $retorno = [
                            "hasErrors" => true,
                            "title" => "Erro",
                            "msg" => $erroMsg,
                            "type" => "error",
                            "cnab" => $cnab,
                            "sql" => $sql,
                    ];
                }
            } else {

                $retorno = [
                        "hasErrors" => true,
                        "title" => "Erro",
                        "msg" => "Nenhuma nota enviada.",
                        "type" => "error",
                        "cnab" => $cnab,
                ];
            }
        } catch (Exception $ex) {
            
        }

        echo json_encode($retorno);
    }

    public function exportarLinhasCNAB($idsNotas) {

        $erro = "";
        $linhas = "";
        $retorno = "";
        $cont = 1;

        $transaction = Yii::app()->db->beginTransaction();

        $arquivoCNAB = new ArquivoCNAB;
        $arquivoCNAB->data_cadastro = date("Y-m-d H:i:s");
        $arquivoCNAB->habilitado = 1;
        $arquivoCNAB->Usuario_id = Yii::app()->session["usuario"]->id;
        $arquivoCNAB->TipoCNAB_id = 1;
        $arquivoCNAB->urlArquivo = "/";

        if ($arquivoCNAB->save()) {

            foreach ($idsNotas as $idNota) {

                $notaFiscal = NotaFiscal::model()->find("habilitado AND id = $idNota");

                if ($notaFiscal !== null) {

                    $vendaHasNF = VendaHasNotaFiscal::model()->find("habilitado AND Nota_Fiscal_id = $notaFiscal->id");

                    if ($vendaHasNF !== null) {

                        $venda = Venda::model()->find("habilitado AND id = $vendaHasNF->Venda_id");

                        if ($venda !== null) {

                            $idNaturezaTitulo = ConfigLN::model()->valorDoParametro("idNaturezaTituloReceber");

                            if (!isset($idNaturezaTitulo)) {
                                $idNaturezaTitulo = 1;
                            }

                            $titulo = Titulo::model()->find("habilitado AND NaturezaTitulo_id = $idNaturezaTitulo AND Proposta_id = $venda->Proposta_id");

                            if ($titulo !== null) {

                                $parcelas = Parcela::model()->findAll("habilitado AND Titulo_id = $titulo->id");

                                foreach ($parcelas as $parcela) {

                                    $valorParcelaRepasse = ($parcela->titulo->proposta->valorRepasse() / $parcela->titulo->proposta->qtd_parcelas);
                                    $valorParcela = $parcela->valor;

                                    $cont++;

                                    $linha = "";

                                    $valorFormatadoRepasse = number_format($valorParcelaRepasse, 2, '', '');
                                    $valorFormatado = number_format($valorParcela, 2, '', '');

                                    $cpf = $parcela->titulo->proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero;
                                    $nomeCliente = $parcela->titulo->proposta->analiseDeCredito->cliente->pessoa->nome;
                                    $endereco = $parcela->titulo->proposta->analiseDeCredito->cliente->pessoa->getEndereco();

                                    //                  $cedente        = "CREDSHOW FUNDO DE INVESTIMENTOS EM DIREITOS CR23273905000130"                    ;
                                    $cedente = "A MARE MANSA COMERCIO DE MOVEIS E ELETRODOMEST08106783000102";

                                    $documentoNF = str_pad(substr($notaFiscal->documento, 2, 7), 7, "0", STR_PAD_LEFT) . "/" .
                                        str_pad($parcela->seq, 2, "0", STR_PAD_LEFT);

                                    if ($endereco == null) {
                                        str_repeat(" ", 40);
                                    } else {
                                        $enderecoCompleto = trim($endereco->logradouro) . " ";
                                        $enderecoCompleto .= trim($endereco->numero) . " ";
                                        $enderecoCompleto .= trim($endereco->bairro) . " ";
                                        $enderecoCompleto .= trim($endereco->cidade) . " ";
                                        $enderecoCompleto .= trim($endereco->uf) . " ";
                                        $enderecoCompleto .= trim($endereco->complemento) . " ";

                                        $enderecoCEP = trim($endereco->cep);

                                        $enderecoCompleto = strtoupper(substr(strtr(utf8_decode(trim($enderecoCompleto)), utf8_decode("áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ"), "aaaaeeiooouucAAAAEEIOOOUUC"), 0, 40));
                                        $nomeCliente = strtoupper(substr(strtr(utf8_decode(trim($nomeCliente)), utf8_decode("áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ"), "aaaaeeiooouucAAAAEEIOOOUUC"), 0, 40));
                                    }

                                    $linha .= "1"; //de:   1 ate:   1
                                    $linha .= str_repeat(" ", 19); //de:   2 ate:  20
                                    $linha .= "02"; //de:  21 ate:  22 
                                    $linha .= "00"; //de:  23 ate:  24 
                                    $linha .= str_repeat("0", 04); //de:  25 ate:  28 
                                    $linha .= "00"; //de:  29 ate:  30 
                                    $linha .= "0199"; //de:  31 ate:  34 
                                    $linha .= "  "; //de:  35 ate:  36
                                    $linha .= "0"; //de:  37 ate:  37
                                    $linha .= str_pad($parcela->id, 25, "0", STR_PAD_LEFT); //de:  38 ate:  62 /** nosso numero?*/ verificar isso depois
                                    $linha .= str_repeat("0", 03); //de:  63 ate:  65
                                    $linha .= str_repeat("0", 05); //de:  66 ate:  70
                                    $linha .= str_repeat(" ", 11); //de:  71 ate:  81
                                    $linha .= str_repeat(" ", 01); //de:  82 ate:  82
                                    $linha .= str_repeat("0", 10); //de:  83 ate:  92
                                    $linha .= str_repeat(" ", 01); //de:  93 ate:  93
                                    $linha .= str_repeat(" ", 01); //de:  94 ate:  94
                                    $linha .= str_repeat("0", 06); //de:  95 ate: 100
                                    $linha .= str_repeat(" ", 04); //de: 101 ate: 104
                                    $linha .= str_repeat(" ", 01); //de: 105 ate: 105
                                    $linha .= str_repeat(" ", 01); //de: 106 ate: 106
                                    $linha .= str_repeat(" ", 02); //de: 107 ate: 108
                                    //                    $linha .= str_pad($parcela->seq         ,  2, "0", STR_PAD_LEFT)          ; //de: 109 ate: 110
                                    $linha .= "01"; //de: 109 ate: 110
                                    $linha .= $documentoNF; //de: 111 ate: 120
                                    $linha .= date("dmy", strtotime($parcela->vencimento)); //de: 121 ate: 126
                                    $linha .= str_pad($valorFormatado, 13, "0", STR_PAD_LEFT); //de: 127 ate: 139
                                    $linha .= str_repeat("0", 03); //de: 140 ate: 142
                                    $linha .= str_repeat("0", 05); //de: 143 ate: 147
                                    $linha .= "01"; //de: 148 ate: 149
                                    $linha .= " "; //de: 150 ate: 150
                                    $linha .= date("dmy", strtotime($parcela->titulo->proposta->data_cadastro)); //de: 151 ate: 156
                                    $linha .= "00"; //de: 157 ate: 158
                                    $linha .= "0"; //de: 159 ate: 159
                                    $linha .= "02"; //de: 160 ate: 161
                                    $linha .= str_repeat("0", 12); //de: 162 ate: 173
                                    $linha .= str_repeat("0", 19); //de: 174 ate: 192
                                    $linha .= str_pad($valorFormatadoRepasse, 13, "0", STR_PAD_LEFT); //de: 193 ate: 205
                                    $linha .= str_repeat("0", 13); //de: 206 ate: 218
                                    $linha .= "01"; //de: 219 ate: 220
                                    $linha .= str_pad($cpf, 14, " ", STR_PAD_LEFT); //de: 221 ate: 234
                                    $linha .= str_pad($nomeCliente, 40, " ", STR_PAD_RIGHT); //de: 235 ate: 274
                                    $linha .= str_pad($enderecoCompleto, 40, " ", STR_PAD_RIGHT); //de: 275 ate: 314
                                    $linha .= str_pad(trim($notaFiscal->documento), 9, "0", STR_PAD_LEFT); //de: 315 ate: 323
                                    $linha .= str_pad($notaFiscal->serie, 3, "0", STR_PAD_LEFT); //de: 324 ate: 326
                                    $linha .= str_pad($enderecoCEP, 8, "0", STR_PAD_LEFT); //de: 327 ate: 334
                                    $linha .= $cedente; //de: 335 ate: 394
                                    $linha .= $notaFiscal->chaveNFe; //de: 395 ate: 438
                                    $linha .= str_pad($cont, 06, "0", STR_PAD_LEFT); //de: 439 ate: 444

                                    $linha .= chr(13) . chr(10);

                                    //         1         2         3         4         5         6         7         8         9        10        11        12        13        14        15        16        17        18        19        20        21        22        23        24        25        26        27        28        29        30        31        32        33        34        35        36        37        38        39        40        41        42        43        44                 
                                    //123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234
                                    //1                   02000000000199  0000000000000000000008697200000000            0000000000  000000        01000004890110061600000000039600000000001 1005160000200000000000000000000000000000000000000003445000000000000001   04524059474LUCIANA DA SILVA ALENCAR                SITIO BUJARI SN MUNICIPIO CATOLE DO ROCH00002472800158884000A MARE MANSA COMERCIO DE MOVEIS E ELETRODOMEST0810678300010225160508106783003462550010000247281004299275000002


                                    $linhas .= $linha;

                                    $linhaCNAB = new LinhaCNAB;
                                    $linhaCNAB->data_cadastro = date("Y-m-d H:i:s");
                                    $linhaCNAB->habilitado = 1;
                                    $linhaCNAB->ArquivoExtportado_id = $arquivoCNAB->id;
                                    $linhaCNAB->Parcela_id = $parcela->id;
                                    $linhaCNAB->caracteres = $linha;

                                    if ($linhaCNAB->save()) {
                                        
                                    }
                                }
                            } else {
                                $linhas .= "não achou titulo" . chr(13) . chr(10);
                            }
                        } else {
                            $linhas .= "não achou venda" . chr(13) . chr(10);
                        }
                    } else {
                        $linhas .= "não achou vendaHasNota" . chr(13) . chr(10);
                    }
                    //                $linhas     .= chr(13) .chr(10) ;
                } else {
                    $linhas .= "não achou nota" . chr(13) . chr(10);
                }
            }

            if (!empty($linhas)) {

                $cont++;

                $qtdLinhas = str_pad($cont, 6, "0", STR_PAD_LEFT);

                $dataRemessa = date("dmy");

                $retorno = "01REMESSA01COBRANCA       00000000000000001291CREDSHOW FUNDO DE INVESTIMENTO611PAULISTA S.A.  $dataRemessa        MX0000001                                                                                                                                                                                                                                                                                                                                 000001";

                $retorno .= chr(13) . chr(10);

                $retorno .= $linhas;

                $retorno .= "9                                                                                                                                                                                                                                                                                                                                                                                                                                                     $qtdLinhas";

                $transaction->commit();
            }
        } else {

            ob_start();
            var_dump($arquivoCNAB->getErrors());
            $erro = ob_get_clean();

            $transaction->rollBack();
        }

        return ["linhas" => $retorno, "arquivoCNAB" => $arquivoCNAB, "erro" => $erro];
    }

    public function actionGerarArquivoLecca() {

        $retorno = [
                "hasErrors" => false,
                "title" => "Sucesso",
                "msg" => "CNAB gerado com sucesso",
                "type" => "success",
                "nomeArquivo" => "nada",
                "url" => "nada",
                "cnab" => "",
                "sucesso" => ""
        ];

        $hasErrors = false;
        $dados = [];
        $recordsFiltered = 0;
        $recordsTotal = 0;
        $erro = "";

        $marcadas = [];

        $sql = "SELECT  P.id                AS idProposta       
                FROM         Proposta                AS P                                                                
                INNER JOIN   Analise_de_Credito      AS AC   ON   AC.habilitado  AND  AC.id =    P.Analise_de_Credito_id  
                INNER JOIN   Filial                  AS F    ON    F.habilitado  AND   F.id =   AC.Filial_id              
                INNER JOIN   Filial_has_Endereco     AS FhE  ON  FhE.habilitado  AND   F.id =  FhE.Filial_id              
                INNER JOIN   Endereco                AS E    ON    E.habilitado  AND   E.id =  FhE.Endereco_id              
                INNER JOIN   NucleoFiliais           AS NuF  ON  NuF.habilitado  AND NuF.id =    F.NucleoFiliais_id       
                INNER JOIN   Cliente                 AS C    ON    C.habilitado  AND   C.id =   AC.Cliente_id             
                INNER JOIN   Pessoa                  AS Pe   ON   Pe.habilitado  AND  Pe.id =    C.Pessoa_id              
                INNER JOIN   Pessoa_has_Documento    AS PHD  ON   PHD.habilitado AND  Pe.id =    PHD.Pessoa_id
                INNER JOIN   Documento               AS DOC  ON   DOC.habilitado AND  DOC.id =    PHD.Documento_id AND DOC.Tipo_documento_id = 1
                INNER JOIN   Tabela_Cotacao          AS TC   ON   TC.habilitado  AND  TC.id =    P.Tabela_id
                INNER JOIN   ModalidadeTabela        AS M    ON                       M.id =   TC.ModalidadeId
                LEFT  JOIN   ItemDoBordero           AS IB   ON   IB.habilitado  AND IB.Proposta_id = P.id 
                WHERE P.habilitado AND P.Status_Proposta_id IN (2,7) AND P.Financeira_id = 11 AND IB.id IS NULL ";

        $sql .= " AND NOT EXISTS    (
                                        SELECT * 
                                        FROM        LinhaCNAB   AS LC 
                                        INNER JOIN  Parcela     AS Pa   ON Pa.habilitado AND Pa.id 	= LC.Parcela_id
                                        INNER JOIN  Titulo      AS T    ON  T.habilitado AND  T.id 	= Pa.Titulo_id
                                        WHERE LC.habilitado AND T.NaturezaTitulo_id = 2 AND P.id = T.Proposta_id
                                    ) ";

        if (isset($_POST["propostasMarcadas"])) {

            $pm = join(",", $_POST["propostasMarcadas"]);

            $sql .= " AND P.id in ($pm) ";
        }

        $sql .= "ORDER BY P.id DESC                ";

        /* try { */

        $resultado = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($resultado as $r) {
            $marcadas[] = $r["idProposta"];
        }

        if (!empty($marcadas)) {

            $linhas = "";

            $retLinhasLecca = $this->exportarLinhasLecca($marcadas);

            $cnab = $retLinhasLecca["linhas"];
            $arquivoCNAB = $retLinhasLecca["arquivoCNAB"];
            $erro = $retLinhasLecca["erro"];
            $sucesso = $retLinhasLecca["sucesso"];
            $passos = $retLinhasLecca["passos"];
            $idsPropostas = $retLinhasLecca['$vdIdsPropostas'];

            if (empty($erro)) {

                $pasta = "remessaFDICLecca";

                if (!is_dir($pasta)) {
                    mkdir("remessaFDICLecca", 0700);
                }

                $arquivo = "arquivo" . date("YmdHis") . ".txt";

                $file = fopen($pasta . "/" . $arquivo, 'w');

                fwrite($file, $cnab);
                fclose($file);

                $retorno["url"] = "/" . $pasta . "/" . $arquivo;
                $retorno["nomeArquivo"] = $arquivo;
                $retorno["sql"] = $sql;

                $arquivoCNAB->urlArquivo = $retorno["url"];

                if (!$arquivoCNAB->update()) {

                    ob_start();
                    var_dump($arquivo->getErrors());
                    $erroArquivoCNAB = ob_get_clean();

                    $retorno = [
                            "hasErrors" => true,
                            "title" => "Erro",
                            "msg" => $erroArquivoCNAB,
                            "type" => "error",
                            "cnab" => $cnab,
                            "sql" => $sql,
                    ];
                } else {

                    ob_start();
                    var_dump($arquivoCNAB);
                    $varDumpArquivoCNAB = ob_get_clean();

                    $retorno["sucesso"] = $sucesso . $varDumpArquivoCNAB;
                    $retorno["passos"] = $passos;
                    $retorno["idsPropostas"] = $idsPropostas;
                }
            } else {

                $retorno = [
                        "hasErrors" => true,
                        "title" => "Erro",
                        "msg" => $erro,
                        "type" => "error",
                        "sql" => $sql,
                ];
            }
        } else {

            $retorno = [
                    "hasErrors" => true,
                    "title" => "Erro ao gerar arquivo",
                    "msg" => "Nenhuma proposta marcadas",
                    "type" => "error",
                    "nomeArquivo" => "",
                    "url" => "",
                    "sql" => $sql
            ];
        }
        /* } catch (Exception $ex) {
          $retorno = [
          "hasErrors" => true,
          "title" => "Erro ao gerar arquivo",
          "msg" => $ex->getMessage(),
          "type" => "error",
          "nomeArquivo" => "nada",
          "url" => "nada",
          "cnab" => ""
          ];
          } */

        echo json_encode($retorno);
    }

    public function exportarLinhasLecca($idsPropostas) {


        $content = "";
        $erro = "";
        $sucesso = "";

        function tirarAcentos($string) {
            return preg_replace(array("/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/", "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/", "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/", "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/", "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/", "/(ñ)/", "/(Ñ)/", "/(Ç)/", "/(ç)/"), explode(" ", "a A e E i I o O u U n N C c"), $string);
        }

        $transaction = Yii::app()->db->beginTransaction();

        $arquivoCNAB = new ArquivoCNAB;
        $arquivoCNAB->data_cadastro = date("Y-m-d H:i:s");
        $arquivoCNAB->habilitado = 1;
        $arquivoCNAB->Usuario_id = Yii::app()->session["usuario"]->id;
        $arquivoCNAB->TipoCNAB_id = 2;
        $arquivoCNAB->urlArquivo = "/";

        if ($arquivoCNAB->save()) {

            $valorTotal = 0;

            $passos = 0;

            ob_start();
            var_dump($idsPropostas);
            $vdIdsPropostas = ob_get_clean();

            foreach ($idsPropostas as $p) {

                $linha = "";

                $proposta = Proposta::model()->find("habilitado AND id = $p");

                $passos++;

                if ($proposta !== null) {

                    $parametrosLecca = ConfiguracoesLecca::model()->find('habilitado AND NucleoFiliais_id = ' . $proposta->analiseDeCredito->filial->NucleoFiliais_id);

                    if ($parametrosLecca != null) {
                        $codigoRede = $parametrosLecca->cod_lojista;
                        $digitoCodigoRede = "0";
                    }

                    if ($proposta->analiseDeCredito->filial->NucleoFiliais_id == 40) {
                        $codigoRede = "96020";
                        $digitoCodigoRede = "0";
                    } else if ($proposta->analiseDeCredito->filial->NucleoFiliais_id == 99) {
                        $codigoRede = "96022";
                        $digitoCodigoRede = "0";
                    } else if ($proposta->analiseDeCredito->filial->NucleoFiliais_id == 59) {
                        $codigoRede = "96024";
                        $digitoCodigoRede = "0";
                    } else if ($proposta->analiseDeCredito->filial->NucleoFiliais_id == 75) {
                        $codigoRede = "96023";
                        $digitoCodigoRede = "0";
                    } else if (in_array($proposta->analiseDeCredito->filial->NucleoFiliais_id, [63, 122])) {
                        $codigoRede = "96025";
                        $digitoCodigoRede = "0";
                    } else if (in_array($proposta->analiseDeCredito->filial->NucleoFiliais_id, [41, 121])) {
                        $codigoRede = "96026";
                        $digitoCodigoRede = "0";
                    } else if (in_array($proposta->analiseDeCredito->filial->NucleoFiliais_id, [67])) {
                        $codigoRede = "96030";
                        $digitoCodigoRede = "0";
                    } else if ($proposta->analiseDeCredito->filial->NucleoFiliais_id == 87) {
                        $codigoRede = "96033";
                        $digitoCodigoRede = "0";
                    } else if ($proposta->analiseDeCredito->filial->NucleoFiliais_id == 43) {
                        $codigoRede = "96034";
                        $digitoCodigoRede = "0";
                    } else if ($proposta->analiseDeCredito->filial->NucleoFiliais_id == 120) {
                        $codigoRede = "96035";
                        $digitoCodigoRede = "0";
                    }

                    $valorEntrada = number_format($proposta->valor_entrada, 2, '', '');
                    $valorParcela = number_format($proposta->valor_parcela, 2, '', '');
                    $nomeCliente = $proposta->analiseDeCredito->cliente->pessoa->nome;

                    $valorIOF = 0;

                    $valor = 0;

                    if ($proposta->tabelaCotacao->ModalidadeId == 2) {

                        $valor = $proposta->qtd_parcelas * $proposta->valor_parcela;

                        $valorTotal += $valor;

                        $valorCompra = number_format(($valor), 2, '', '');

                        $taxa = str_pad("0", 10, "0", STR_PAD_LEFT);

                        $fator = $proposta->getFator();

                        $valorRepasse = $valor - ($valor * ($fator->porcentagem_retencao / 100));

                        if ($fator !== null) {
                            $valorIOF = ($valorRepasse * $fator->fatorIOF);
                        }

                        $parametrosLecca = ConfiguracoesLecca::model()->find('habilitado AND NucleoFiliais_id = ' . $proposta->analiseDeCredito->filial->NucleoFiliais_id);

                        if ($parametrosLecca != null) {
                            if ($proposta->carencia == 15) {
                                $codTabela = str_pad($parametrosLecca->cod_tab15, 8, "0", STR_PAD_LEFT);
                            } else if ($proposta->carencia == 30) {
                                $codTabela = str_pad($parametrosLecca->cod_tab30, 8, "0", STR_PAD_LEFT);
                            } else if ($proposta->carencia == 45) {
                                $codTabela = str_pad($parametrosLecca->cod_tab45, 8, "0", STR_PAD_LEFT);
                            }
                        }

                        if ($proposta->analiseDeCredito->filial->NucleoFiliais_id == 40) {
                            $codTabela = "00000602";
                        } else if (in_array($proposta->analiseDeCredito->filial->NucleoFiliais_id, [59, 99])) {

                            if ($proposta->carencia == 15) {
                                $codTabela = "00000607";
                            } else if ($proposta->carencia == 30) {
                                $codTabela = "00000608";
                            } else if ($proposta->carencia == 45) {
                                $codTabela = "00000609";
                            }
                        } else if ($proposta->analiseDeCredito->filial->NucleoFiliais_id == 67) {
                            if ($proposta->carencia == 15) {
                                $codTabela = "00000613";
                            } else if ($proposta->carencia == 30) {
                                $codTabela = "00000614";
                            } else if ($proposta->carencia == 45) {
                                $codTabela = "00000615";
                            }
                        } else if ($proposta->analiseDeCredito->filial->NucleoFiliais_id == 87) {

                            if ($proposta->carencia == 15) {
                                $codTabela = "00000451";
                            } else if ($proposta->carencia == 30) {
                                $codTabela = "00000452";
                            } else if ($proposta->carencia == 45) {
                                $codTabela = "00000453";
                            }
                        } else if ($proposta->analiseDeCredito->filial->NucleoFiliais_id == 43) {

                            if ($proposta->carencia == 30) {
                                $codTabela = "00000450";
                            }
                        }
                    } else {

                        $parametrosLecca = ConfiguracoesLecca::model()->find('habilitado AND NucleoFiliais_id = ' . $proposta->analiseDeCredito->filial->NucleoFiliais_id);

                        if ($parametrosLecca != null) {
                            $codTabela = str_pad($parametrosLecca->cod_tabJuros, 8, "0", STR_PAD_LEFT);
                        }

                        $valor = $proposta->valor - $proposta->valor_entrada;

                        $valorTotal += $valor;

                        $valorCompra = number_format(($valor), 2, '', '');

                        $taxa = str_pad(number_format($proposta->tabelaCotacao->taxa, 4, '', ''), 10, "0", STR_PAD_LEFT);

                        $fator = $proposta->getFator();

                        if ($fator !== null) {
                            $valorIOF = (($proposta->valor - $proposta->valor_entrada) * $fator->fatorIOF);
                        }

                        if ($proposta->analiseDeCredito->filial->NucleoFiliais_id == 40) {
                            $codTabela = "00000601";
                        } else if ($proposta->analiseDeCredito->filial->NucleoFiliais_id == 75) {
                            $codTabela = "00000610";
                        } else if (in_array($proposta->analiseDeCredito->filial->NucleoFiliais_id, [41, 121])) {
                            $codTabela = "00000611";
                        } else if (in_array($proposta->analiseDeCredito->filial->NucleoFiliais_id, [63, 122])) {
                            $codTabela = "00000612";
                        } else if (in_array($proposta->analiseDeCredito->filial->NucleoFiliais_id, [67])) {
                            $codTabela = "00000615";
                        } else if ($proposta->analiseDeCredito->filial->NucleoFiliais_id == 120) {
                            $codTabela = "00000449";
                        }
                    }

                    $valorIOFstr = str_pad(number_format($valorIOF, 2, '', ''), 15, "0", STR_PAD_LEFT);

                    $cadastro = Cadastro::model()->find('habilitado AND Cliente_id = ' . $proposta->analiseDeCredito->cliente->id);
                    if ($cadastro != null) {
                        if (isset($cadastro->nome_do_pai)) {
                            $nomePai = str_pad($cadastro->nome_do_pai, 35, " ", STR_PAD_RIGHT);
                        } else {
                            $nomePai = str_pad("EDUARDO MONICA", 35, " ", STR_PAD_RIGHT);
                        }
                        if (isset($cadastro->nome_da_mae)) {
                            $nomeMae = str_pad($cadastro->nome_da_mae, 35, " ", STR_PAD_RIGHT);
                        } else {
                            $nomeMae = str_pad("MONICA EDUARDO", 35, " ", STR_PAD_RIGHT);
                        }
                    } else {
                        $nomePai = str_pad("EDUARDO MONICA", 35, " ", STR_PAD_RIGHT);
                        $nomeMae = str_pad("MONICA EDUARDO", 35, " ", STR_PAD_RIGHT);
                    }

                    $dadosProfissionais = DadosProfissionais::model()->find('habilitado AND Pessoa_id = ' . $proposta->analiseDeCredito->cliente->pessoa->id . ' AND principal');
                    if ($dadosProfissionais != null) {
                        if (isset($dadosProfissionais->profissao)) {
                            $profissao = str_pad($profissao = $dadosProfissionais->profissao, 35, " ", STR_PAD_RIGHT);
                        } else {
                            $profissao = str_pad("OUTROS", 35, " ", STR_PAD_RIGHT);
                        }
                        if (isset($dadosProfissionais->renda_liquida)) {
                            $salario = number_format($dadosProfissionais->renda_liquida, 2, "", "");
                        } else {
                            $salario = number_format(1000, 2, "", "");
                        }
                        if (isset($dadosProfissionais->empresa)) {
                            $razaoSocial = $dadosProfissionais->empresa;
                        } else {
                            $razaoSocial = "OUTROS";
                        }
                        $logradouro = $dadosProfissionais->endereco->logradouro;
                        $numero = $dadosProfissionais->endereco->numero;
                        $complemento = $dadosProfissionais->endereco->complemento;
                        $bairro = $dadosProfissionais->endereco->bairro;
                        $cidade = $dadosProfissionais->endereco->cidade;
                        $uf_dp = $dadosProfissionais->endereco->uf;
                        $cep_dp = $dadosProfissionais->endereco->cep;
                    } else {
                        $profissao = str_pad("OUTROS", 35, " ", STR_PAD_RIGHT);
                        $salario = number_format(1000, 2, "", "");
                        $razaoSocial = "OUTROS";

                        $logradouro = "DADO INEXISTENTE";
                        $numero = "DADO INEXISTENTE";
                        $complemento = "DADO INEXISTENTE";
                        $bairro = "DADO INEXISTENTE";
                        $cidade = "DADO INEXISTENTE";
                        $uf_dp = "RN";
                        $cep_dp = "59380000";
                    }

                    $endereco = $proposta->analiseDeCredito->cliente->pessoa->getEndereco();
                    if ($endereco != null) {
                        $logradouro_cliente = $endereco->logradouro;
                        $numero_cliente = $endereco->numero;
                        $complemento_cliente = $endereco->complemento;
                        $uf_cliente = $endereco->uf;
                        $bairro_cliente = $endereco->bairro;
                        $cidade_cliente = $endereco->cidade;
                        $enderecoCEP = trim($endereco->cep);
                    } else {
                        $logradouro_cliente = "DADO INEXISTENTE";
                        $numero_cliente = "DADO INEXISTENTE";
                        $complemento_cliente = "DADO INEXISTENTE";
                        $uf_cliente = "RN";
                        $bairro_cliente = "DADO INEXISTENTE";
                        $cidade_cliente = "DADO INEXISTENTE";
                        $enderecoCEP = "59380000";
                    }

                    $nomeCliente = strtoupper(strtr(utf8_decode($nomeCliente), utf8_decode("áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ"), "aaaaeeiooouucAAAAEEIOOOUUC"));

                    $cht = ContatoHasTelefone::model()->find('habilitado AND Contato_id = ' . $proposta->analiseDeCredito->cliente->pessoa->Contato_id);
                    if ($cht != null) {
                        $telefone = Telefone::model()->find('habilitado AND id = ' . $cht->Telefone_id)->numero;
                        $tddd = Telefone::model()->find('habilitado AND id = ' . $cht->Telefone_id)->discagem_direta_distancia;
                    } else {
                        $telefone = "0";
                        $tddd = "0";
                    }

                    $nucleo = $proposta->analiseDeCredito->filial->nucleoFilial;
                    $nHDB = NucleoFiliaisHasDadosBancarios::model()->find('NucleoFiliais_id = ' . $nucleo->id);
                    if ($nHDB != null) {
                        $dadosBancarios = DadosBancarios::model()->find('habilitado AND id = ' . $nHDB->Dados_Bancarios_id);

                        $nome_agencia = $dadosBancarios->banco->nome;

                        $posicaoA = strpos($dadosBancarios->agencia, '-');

                        if ($posicaoA != 0 || $posicaoA === true) {
                            $agencia = substr($dadosBancarios->agencia, 0, $posicaoA);
                            $digitoAgencia = substr($dadosBancarios->agencia, -($posicaoA - 3));
                        } else {
                            $agencia = $dadosBancarios->agencia;
                            $digitoAgencia = "-";
                        }

                        $posicaoB = strpos($dadosBancarios->numero, '-');
                        if ($posicaoB != 0 || $posicaoB === true) {
                            $conta = substr($dadosBancarios->numero, 0, $posicaoB);
                            $digitoConta = substr($dadosBancarios->numero, -($posicaoB - 2));
                        } else {
                            $conta = $dadosBancarios->numero;
                            $digitoConta = "-";
                        }

                        if ($dadosBancarios->Tipo_Conta_Bancaria_id == 1) {
                            $flagConta = '001';
                        } else {
                            $flagConta = '002';
                        }
                    } else {
                        $dadosBancarios = null;
                        $nome_agencia = "-";
                        $digitoAgencia = '-';
                        $digitoConta = '-';
                        $conta = "-";
                        $agencia = '-';
                        $flagConta = '-';
                    }

                    $cnpj = $proposta->analiseDeCredito->filial->cnpj;

                    $cnpj = str_replace('.', '', $cnpj);
                    $cnpj = str_replace('-', '', $cnpj);
                    $cnpj = str_replace('/', '', $cnpj);
                    $cnpj = str_replace(' ', '', $cnpj);

                    if ($proposta->getDialogo() != null) {
                        $msg = Mensagem::model()->find('Dialogo_id = ' . $proposta->getDialogo()->id)->conteudo;
                    } else {
                        $msg = "-";
                    }

                    $linha .= "CDCS"; //codigo do produto (4)                   <--    1 :    4-->
                    $linha .= date('Ymd', strtotime($proposta->data_cadastro)); //data da operacao (8)                    <--    5 :   12-->
                    $linha .= "0005"; //codigo da regional (4)                  <--   13 :   16-->
                    //$linha .= "96020"                                                                                                             ; //codigo da rede (5)                      <--   17 :   21-->
                    //$linha .= "0"                                                                                                                 ; //digito da rede (1)                      <--   22 :   22-->
                    $linha .= $codigoRede; //codigo da rede (5)                      <--   17 :   21-->
                    $linha .= $digitoCodigoRede; //digito da rede (1)                      <--   22 :   22-->
                    $linha .= "00001"; //codigo da loja (5)                      <--   23 :   27-->
                    $linha .= "0"; //digito da loja (1)                      <--   28 :   28-->
                    $linha .= str_pad("CREDSHOW S/A", 35, " ", STR_PAD_RIGHT); //nome da loja (35)                       <--   29 :   63-->
                    $linha .= str_pad($proposta->id, 12, "0", STR_PAD_LEFT); //numero do contrato/operacao (12)        <--   64 :   75-->
                    $linha .= $codTabela; //codigo da tabela de juros (8)           <--   76 :   83-->
                    $linha .= $taxa; //"0000074200"; //taxa de juros (10)      <--   84 :   93-->
                    $linha .= date('Ymd', strtotime("+" . $proposta->carencia . "days", strtotime($proposta->data_cadastro))); //data primeira parcela (8)               <--   94 :  101-->
                    $linha .= str_pad($valorCompra, 15, "0", STR_PAD_LEFT); //valor da compra (15)                    <--  102 :  116-->
                    $linha .= str_pad($valorEntrada, 15, "0", STR_PAD_LEFT); //valor da entrada (15)                   <--  117 :  131-->
                    $linha .= str_pad($valorCompra, 15, "0", STR_PAD_LEFT); //valor do principal (15)                 <--  132 :  146-->
                    $linha .= "000000000000000"; //valor CAC (15)                          <--  147 :  161-->
                    $linha .= str_pad($proposta->qtd_parcelas, 3, "0", STR_PAD_LEFT); //quantidade de parcelas (3)              <--  162 :  164-->
                    $linha .= str_pad($valorParcela, 15, "0", STR_PAD_LEFT); //valor da parcela (15)                   <--  165 :  179-->
                    $linha .= $valorIOFstr; //valor IOC (15)                          <--  180 :  194-->
                    $linha .= "D"; //forma liberacao (1)                     <--  195 :  195-->
                    $linha .= "I"; //forma liquidacao (1)                    <--  196 :  196-->
                    $linha .= str_pad(substr($digitoAgencia, 0, 1), 1, " ", STR_PAD_RIGHT); //digito da agencia (1)                   <--  197 :  197-->
                    $linha .= str_pad(substr($digitoConta, 0, 2), 2, "0", STR_PAD_LEFT); //digito da conta (2)                     <--  198 :  199-->
                    $linha .= str_pad(substr(tirarAcentos($nomeCliente), 0, 35), 35, " ", STR_PAD_RIGHT); //nome do favorecido (35)                 <--  200 :  234-->
                    $linha .= "001"; //praca do DOC (3)                        <--  235 :  237-->
                    $linha .= $dadosBancarios->banco->codigo; //banco para pagamento do DOC (3)         <--  238 :  240-->
                    $linha .= str_pad($agencia, 4, "0", STR_PAD_LEFT); //agencia para pagamento do DOC (4)       <--  241 :  244-->
                    $linha .= str_pad($conta, 8, "0", STR_PAD_LEFT); //conta para pagamento do DOC (8)         <--  245 :  252-->
                    $linha .= $cnpj; //CGC/CPF do DOC (Cliente) (14)           <--  253 :  266-->
                    $linha .= "N"; //TAC Financiada (1)                      <--  267 :  267-->
                    $linha .= str_pad($valorCompra, 15, "0", STR_PAD_LEFT); //valor do DOC (15)                       <--  268 :  282-->
                    $linha .= str_pad(substr(tirarAcentos($nomeCliente), 0, 60), 60, " ", STR_PAD_RIGHT); //nome do cliente (60)                    <--  283 :  342-->
                    $linha .= "00001"; //pais/nacionalidade (5)                  <--  343 :  347-->
                    $linha .= "4"; //estado civil (1)                        <--  348 :  348-->
                    $linha .= $proposta->analiseDeCredito->cliente->pessoa->sexo; //sexo (1)                                <--  349 :  349-->
                    $linha .= str_pad($proposta->analiseDeCredito->cliente->pessoa->getRG()->numero, 16, " ", STR_PAD_RIGHT); //numero do rg (16)                       <--  350 :  365-->
                    $linha .= $proposta->analiseDeCredito->cliente->pessoa->getRG()->uf_emissor; //UF do RG (2)                            <--  366 :  367-->
                    $linha .= $proposta->analiseDeCredito->cliente->pessoa->getRG()->uf_emissor; //UF da Naturalidade (2)                  <--  368 :  369-->
                    $linha .= str_pad("BRASILEIRA", 35, " ", STR_PAD_RIGHT); //Naturalidade (35)                       <--  370 :  404-->
                    $linha .= str_pad(substr(tirarAcentos($nomePai), 0, 35), 35, STR_PAD_LEFT); //nome do pai (35)                        <--  405 :  439-->
                    $linha .= str_pad(substr(tirarAcentos($nomeMae), 0, 35), 35, STR_PAD_LEFT); //nome da mae (35)                        <--  440 :  474-->
                    $linha .= str_pad(substr($proposta->analiseDeCredito->cliente->pessoa->getRG()->orgao_emissor, 0, 5), 5, " ", STR_PAD_RIGHT); //orgao emissor do RG (5)                 <--  475 :  479-->
                    $linha .= $proposta->analiseDeCredito->cliente->pessoa->getRG()->uf_emissor; //sigla UF outros documentos (2)          <--  480 :  481-->
                    $linha .= substr(tirarAcentos($profissao), 0, 35); //descricao da profissao (35)             <--  482 :  516-->
                    $linha .= substr(tirarAcentos($profissao), 0, 35); //descricao do cargo (35)                 <--  517 :  551-->
                    $linha .= $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero; //cpf do cliente (11)                     <--  552 :  562-->
                    $linha .= date('Ymd', strtotime($proposta->analiseDeCredito->cliente->pessoa->nascimento)); //data de nascimento do cliente (8)       <--  563 :  570-->
                    $linha .= "00000001"; //codigo da profissao (8)                 <--  571 :  578-->
                    $linha .= str_pad($salario, 12, "0", STR_PAD_LEFT); //valor do salario liquido (12)           <--  579 :  590-->
                    $linha .= date('Ymd', strtotime($proposta->analiseDeCredito->cliente->pessoa->getRG()->data_emissao)); //data de expedicao do RG (8)             <--  591 :  598-->
                    $linha .= str_pad(substr(tirarAcentos(utf8_decode(preg_replace('/\s/', ' ', $msg))), 0, 3000), 3000, "-", STR_PAD_RIGHT); //observacao (3000!)                      <--  599 : 3598-->
                    $linha .= "1"; //tipo de residencia (1)                  <-- 3599 : 3600-->
                    $linha .= str_pad(substr(tirarAcentos(($logradouro_cliente)), 0, 60), 60, " ", STR_PAD_RIGHT); //descricao do logradouro (60)            <-- 3601 : 3660-->
                    $linha .= str_pad(substr($numero_cliente, 0, 5), 5, "0", STR_PAD_LEFT); //numero do logradouro (5)                <-- 3661 : 3665-->
                    $linha .= str_pad(substr(tirarAcentos(($complemento_cliente)), 0, 30), 30, " ", STR_PAD_RIGHT); //complemento do logradouro (30)          <-- 3666 : 3695-->
                    $linha .= $uf_cliente; //UF do endereco (2)                      <-- 3696 : 3697-->
                    $linha .= str_pad(substr(tirarAcentos(($bairro_cliente)), 0, 30), 30, " ", STR_PAD_RIGHT); //bairro (30)282                          <-- 3698 : 3727-->
                    $linha .= str_pad(substr(tirarAcentos(($cidade_cliente)), 0, 40), 40, " ", STR_PAD_RIGHT); //cidade (40)                             <-- 3728 : 3767-->
                    $linha .= "00001"; //pais (5)                                <-- 3768 : 3772-->
                    $linha .= $enderecoCEP; //CEP (8)                                 <-- 3773 : 3780-->
                    $linha .= "0000"; //area do telefone (4)                    <-- 3781 : 3784-->
                    $linha .= "000000000000"; //numero do telefone (12)                 <-- 3785 : 3796-->
                    $linha .= str_pad(substr($telefone, 0, 12), 12, "0", STR_PAD_LEFT); //"000000000000" numero do celular (12)   <-- 3797 : 4008-->
                    $linha .= str_pad("SEMEMAIL", 50, " ", STR_PAD_LEFT); //email (50)                              <-- 4009 : 4058-->
                    $linha .= str_pad($tddd, 4, "0", STR_PAD_LEFT); //area do celular (4)                     <-- 4059 : 4062-->
                    $linha .= str_pad(substr(tirarAcentos($razaoSocial), 0, 30), 30, " ", STR_PAD_RIGHT); //"BN 1566947577                 ";           //razao social (30)
                    $linha .= "20130201";                                 //data de admissao (8)
                    $linha .= "00000000000000";                           //CGC da Empresa (14)
                    $linha .= str_pad(substr(tirarAcentos(($logradouro)), 0, 60), 60, " ", STR_PAD_RIGHT); //"MARIA LUIZA MIRANDA RONKOSKI                                "; //logradouro (60)
                    $linha .= str_pad(substr($numero, 0, 5), 5, " ", STR_PAD_RIGHT); //"00000";                                    //numero do logradouro (5)
                    $linha .= str_pad(substr(tirarAcentos(($complemento)), 0, 30), 30, " ", STR_PAD_RIGHT); //"                   complemento";           //complemento (30)
                    $linha .= str_pad(substr(tirarAcentos(($bairro)), 0, 30), 30, " ", STR_PAD_RIGHT); //"RIO PEQUENO                   ";           //bairro (30)
                    $linha .= str_pad(substr(tirarAcentos(($cidade)), 0, 40), 40, " ", STR_PAD_RIGHT); //"SAO JOSE DOS PI                         "; //cidade (40)
                    $linha .= str_pad($uf_dp, 2, " ", STR_PAD_RIGHT); //"RN";                                       //RN (2)
                    $linha .= str_pad($salario, 12, "0", STR_PAD_LEFT); //"000000088000";                             //valor da renda (12)
                    $linha .= str_pad(substr(str_replace("-", "", $cep_dp), 0, 8), 8, "0", STR_PAD_LEFT); //"08308526";                                 //CEP (8)
                    $linha .= substr($digitoAgencia, 0, 1);                                        //digito da agencia (1)
                    $linha .= str_pad(substr($digitoConta, 0, 2), 2, "0", STR_PAD_LEFT); //digito da conta (2)
                    $linha .= str_pad(substr(tirarAcentos($nome_agencia), 0, 20), 20, " ", STR_PAD_RIGHT);                     //nome da agencia (20)
                    $linha .= "1";                                        //FLAG titularidade (1)
                    $linha .= str_pad(substr(tirarAcentos($nucleo->nome), 0, 35), 35, " ", STR_PAD_RIGHT); //nome do favorecido (35)
                    $linha .= "000000000000";                             //numero do telefone (12)
                    $linha .= "001";                                      //praca (3)
                    $linha .= $dadosBancarios->banco->codigo;                                      //banco (3)
                    $linha .= str_pad($agencia, 4, "0", STR_PAD_LEFT);                                     //agencia (4)
                    $linha .= str_pad($conta, 8, "0", STR_PAD_LEFT);                                 //conta (8)
                    $linha .= str_pad($flagConta, 3, "0", STR_PAD_RIGHT);                                      //tipo da conta (3)
                    $linha .= "001";                                      //tipo de documento cpf (3)
                    $linha .= date('Ymd', strtotime("+" . $proposta->carencia . "days", strtotime($proposta->data_cadastro)));                                 //data de abertura (8)
                    $linha .= "0";                                        //codigo de natureza da residencia (1)

                    $linha .= chr(13) . chr(10);

                    $content .= $linha;

                    $titulo = Titulo::model()->find("habilitado AND Proposta_id = $proposta->id AND NaturezaTitulo_id = 2");

                    if ($titulo !== null) {

                        $parcela = Parcela::model()->find("habilitado AND Titulo_id = $titulo->id");

                        if ($parcela !== null) {

                            $linhaCNAB = new LinhaCNAB;
                            $linhaCNAB->data_cadastro = date("Y-m-d H:i:s");
                            $linhaCNAB->habilitado = 1;
                            $linhaCNAB->ArquivoExtportado_id = $arquivoCNAB->id;
                            $linhaCNAB->Parcela_id = $parcela->id;
                            $linhaCNAB->caracteres = $linha;

                            if (!$linhaCNAB->save()) {

                                ob_start();
                                var_dump($linhaCNAB->getErrors());
                                $erro = ob_get_clean();

                                break;
                            }
                        } else {

                            $erro = "Parcela da proposta $proposta->codigo não encontrada.";

                            break;
                        }
                    } else {

                        $erro = "Título da proposta $proposta->codigo não encontrada.";

                        break;
                    }
                } else {

                    $erro = "Proposta de id $p não encontrada.";

                    break;
                }
            }

            if (empty($content)) {

                $erro = "Nenhuma linha gerada.";

                $transaction->rollBack();
            } else if (empty($erro)) {
                $sucesso = "Deu certo.";
                $transaction->commit();
            }
        } else {

            ob_start();
            var_dump($arquivoCNAB->getErrors());
            $erro = ob_get_clean();

            $transaction->rollBack();
        }


        return ["linhas" => $content, "arquivoCNAB" => $arquivoCNAB, "erro" => $erro, "sucesso" => $sucesso, "passos" => $passos, '$vdIdsPropostas' => $vdIdsPropostas];
    }

    public function comunicaWSNFe($numeroContrato) {

        $retorno = [
                "hasErrors" => false,
                "msg" => "XML importado com sucesso",
                "type" => "success",
                "codigoProposta" => ""
        ];

        $link = "http://app01.maremansa.net.br:8093/ws/MARWS013.apw?WSDL";

        //        $numeroContrato = "101872014243716"                                         ;

        $arrayNFs = [];

        $soapClient = new SoapClient(
            $link, array(
                "soap_version" => 1,
                "trace" => 1,
                "exceptions" => 1,
                "features" => SOAP_SINGLE_ELEMENT_ARRAYS
            )
        );

        try {

            $proposta = Proposta::model()->find("habilitado AND (numero_do_contrato = '$numeroContrato' OR codigo = '$numeroContrato' )");

            if ($proposta !== null) {

                $resultado = $soapClient->__soapCall(
                    "CONSULTAR", array(
                        "CONSULTAR" => array(
                                "DADOS" => array(
                                        "PROPOSTA" => $numeroContrato,
                                        "TOKEN" => "8711620350"
                                )
                        )
                    )
                );


                if (isset($resultado->CONSULTARRESULT)) {

                    $xmlResult = simplexml_load_string($resultado->CONSULTARRESULT);

                    if (isset($xmlResult->NFEntrega) && !empty($xmlResult->NFEntrega)) {

                        foreach (get_object_vars($xmlResult->NFEntrega) as $nf) {

                            $datetime = DateTime::createFromFormat('d/m/y', $xmlResult->DataVenda);
                            $dataEmissao = $datetime->format('Y-m-d');

                            $arrayNFs[] = [
                                    "documento" => $nf->NF,
                                    "serie" => $nf->Serie,
                                    "emissao" => $dataEmissao,
                                    "chave" => $nf->ChaveNFe
                            ];
                        }
                    }

                    if (isset($xmlResult->NFCupom)) {

                        foreach (get_object_vars($xmlResult->NFCupom) as $nf) {

                            $datetime = DateTime::createFromFormat('d/m/y', $xmlResult->DataVenda);
                            $dataEmissao = $datetime->format('Y-m-d');

                            $arrayNFs[] = [
                                    "documento" => $nf->NF,
                                    "serie" => $nf->Serie,
                                    "emissao" => $dataEmissao,
                                    "chave" => $nf->ChaveNFe
                            ];
                        }
                    }


                    if (count($arrayNFs) > 0) {

                        $transaction = Yii::app()->db->beginTransaction();

                        $venda = Venda::model()->find("habilitado AND Proposta_id = $proposta->id");

                        if ($venda == null) {

                            $venda = new Venda;

                            $venda->habilitado = 1;
                            $venda->data_cadastro = date("Y-m-d H:i:s");
                            $venda->Proposta_id = $proposta->id;

                            if (!$venda->save()) {

                                ob_start();
                                var_dump($venda->getErrors());
                                $resErro = ob_get_clean();

                                $retorno = [
                                        "hasErrors" => true,
                                        "msg" => "Erro ao gravar registro da venda: $resErro. "
                                        . "ID da Propsota: $proposta->id",
                                        "type" => "error",
                                        "codigoProposta" => $proposta->codigo
                                ];
                            }
                        }

                        if (!($retorno["hasErrors"])) {

                            foreach ($arrayNFs as $nf) {

                                $notaFiscal = NotaFiscal::model()->find("habilitado AND documento = '" . $nf["documento"] . "' AND serie = '" . $nf["serie"] . "'");

                                if (!isset($notaFiscal)) {

                                    $notaFiscal = new NotaFiscal;

                                    $notaFiscal->data_cadastro = date("Y-m-d H:i:s");
                                    $notaFiscal->habilitado = 1;

                                    if (!$notaFiscal->save()) {

                                        ob_start();
                                        var_dump($notaFiscal->getErrors());
                                        $erroNF = ob_get_clean();

                                        $retorno = [
                                                "hasErrors" => true,
                                                "msg" => "Erro ao salvar Nota Fiscal. NotaFiscal: $erroNF. "
                                                . "",
                                                "type" => "error",
                                                "codigoProposta" => $proposta->codigo
                                        ];

                                        break;
                                    } else {

                                        $vendaHasNF = new VendaHasNotaFiscal;
                                        $vendaHasNF->habilitado = 1;
                                        $vendaHasNF->data_cadastro = date("Y-m-d H:i:s");
                                        $vendaHasNF->Nota_Fiscal_id = $notaFiscal->id;
                                        $vendaHasNF->Venda_id = $venda->id;

                                        if (!$vendaHasNF->save()) {

                                            ob_start();
                                            var_dump($vendaHasNF->getErrors());
                                            $resErro = ob_get_clean();

                                            $retorno = [
                                                    "hasErrors" => true,
                                                    "msg" => "Erro ao gravar registro da venda (VendaHasNF): $resErro",
                                                    "type" => "error",
                                                    "codigoProposta" => $proposta->codigo
                                            ];

                                            break;
                                        }
                                    }
                                }

                                if (!($retorno["hasErrors"])) {

                                    //                            $statusNF = StatusNotaFiscal::model()->find("habilitado AND label = '" . $objXML->protNFe->infProt->cStat . "'");
                                    //                            if($statusNF == null)
                                    //                            {
                                    $statusNF = StatusNotaFiscal::model()->findByPk(2);
                                    //                        }

                                    $notaFiscal->Status_Nota_Fiscal_id = $statusNF->id;
                                    $notaFiscal->documento = $nf["documento"];
                                    $notaFiscal->serie = $nf["serie"];
                                    $notaFiscal->data = $nf["emissao"];
                                    $notaFiscal->chaveNFe = $nf["chave"];

                                    if (!($notaFiscal->update())) {

                                        ob_start();
                                        var_dump($notaFiscal->getErrors());
                                        $erroNF = ob_get_clean();

                                        $retorno = [
                                                "hasErrors" => true,
                                                "msg" => "Erro ao salvar Nota Fiscal. NotaFiscal: $erroNF",
                                                "type" => "error",
                                                "codigoProposta" => $proposta->codigo
                                        ];

                                        $transaction->rollBack();

                                        break;
                                    }
                                } else {
                                    $transaction->rollBack();
                                }
                            }

                            if (!($retorno["hasErrors"])) {
                                $transaction->commit();

                                $retorno["codigoProposta"] = $proposta->codigo;
                            }
                        } else {
                            $transaction->rollBack();
                        }
                    } else {
                        $retorno["msg"] = "Venda sem Nota Fiscal, foi gerado apenas o Cupom.";
                        $retorno["hasErrors"] = true;
                        $retorno["type"] = "error";
                        $retorno["codigoProposta"] = $proposta->codigo;
                    }
                } else {

                    ob_start();
                    var_dump($resultado);
                    $htmlMail = ob_get_clean();

                    $retorno["msg"] = "WS retornou algum erro. Erro: $htmlMail";
                    $retorno["type"] = "error";
                    $retorno["hasErrors"] = true;
                    $retorno["codigoProposta"] = $proposta->codigo;
                }
            } else {
                $retorno["msg"] = "Proposta com número de contrato não encontrada.";
                $retorno["hasErrors"] = true;
                $retorno["type"] = "error";
                $retorno["codigoProposta"] = $numeroContrato;
            }
        } catch (Exception $ex) {

            $retorno["msg"] = $ex->getMessage();
            $retorno["type"] = "error";
            $retorno["hasErrors"] = true;
            $retorno["codigoProposta"] = $numeroContrato;
        }

        return $retorno;
    }

    public function actionBuscarNFeWS() {

        $logs = [];
        $qtdErros = 0;
        $qtdOK = 0;
        $retorno = [
                "hasErrors" => false,
                "type" => "success",
                "msg" => ""
        ];

        $sql = "
                        SELECT      IFNULL(P.numero_do_contrato,P.codigo)  AS NC
                        FROM        Proposta            AS P
                        INNER JOIN  Analise_de_Credito  AS AC   ON   AC.habilitado AND  AC.id =   P.Analise_de_Credito_id
                        INNER JOIN  Filial              AS F    ON    F.habilitado AND   F.id =  AC.Filial_id 
                        INNER JOIN  NucleoFiliais       AS NuF  ON  NuF.habilitado AND NuF.id =   F.NucleoFiliais_id
                        LEFT  JOIN  PropostaOmniConfig  AS POC  ON  POC.habilitado AND   P.id = POC.Proposta
                        WHERE P.habilitado AND NuF.GrupoFiliais_id IN (4,7,21) AND POC.id IS NULL AND P.Status_Proposta_id IN (2,7) AND P.Financeira_id = 10 
                            AND NOT EXISTS  (
                                                SELECT * 
                                                FROM        LinhaCNAB   AS LC 
                                                INNER JOIN  Parcela     AS Pa   ON Pa.habilitado AND Pa.id 	= LC.Parcela_id
                                                INNER JOIN  Titulo      AS T    ON  T.habilitado AND  T.id 	= Pa.Titulo_id
                                                WHERE LC.habilitado AND T.NaturezaTitulo_id = 1 AND P.id = T.Proposta_id
                                            ) 
                    ";

        if (isset($_POST["idProposta"])) {
            $sql .= " AND P.id = " . $_POST["idProposta"];
        }

        $resultado = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($resultado as $r) {

            $retornoWS = $this->comunicaWSNFe($r["NC"]);

            if ($retornoWS["hasErrors"]) {
                $qtdErros++;
            } else {
                $qtdOK++;
            }

            $logs[] = $retornoWS;
        }

        $this->disparaEmailLogWS($logs);

        if ($qtdErros > 0) {
            if ($qtdOK > 0) {
                $retorno["type"] = "info";

                $retorno["msg"] = "Foram resgatadas " . ($qtdErros + $qtdOK) . " NFe's. <br>  "
                    . "$qtdOK importadas com sucesso.                             "
                    . "$qtdErros não foram importadas.                            "
                    . "Confira o log por email.                                   ";
            } else {
                $retorno["type"] = "error";

                $retorno["msg"] = "Foram resgatadas " . ($qtdErros + $qtdOK) . " NFe's. <br>  "
                    . "Todas importadas com erro                                  "
                    . "Confira o log por email.                                   ";
            }
        } else {
            if ($qtdOK > 0) {
                $retorno["type"] = "success";

                $retorno["msg"] = "Foram resgatadas " . ($qtdErros + $qtdOK) . " NFe's. <br>  "
                    . "$qtdOK importadas com sucesso.                             "
                    . "$qtdErros não foram importadas.                            "
                    . "Confira o log por email.                                   ";
            } else {
                $retorno["type"] = "info";

                $retorno["msg"] = "Nenhuma nota foi importada.";
            }
        }

        $retorno["sql"] = $sql;

        echo json_encode($retorno);
    }

    public function disparaEmailLogWS($logs) {

        if (count($logs) > 0) {

            $htmlMail = "<table border='1'>"
                . "   <tr>"
                . "       <th colspan='3'>"
                . "           Log de Notas Fiscais Eletrônicas/Propostas"
                . "       </th>"
                . "   </tr>"
                . "   <tr>"
                . "       <th>"
                . "           Status"
                . "       </th>"
                . "       <th>"
                . "           Mensagem"
                . "       </th>"
                . "       <th>"
                . "           Proposta"
                . "       </th>"
                . "   </tr>";

            foreach ($logs as $log) {


                $htmlMail .= "   <tr>"
                    . "       <th>"
                    . "           <font color='" . ($log["hasErrors"] ? "red' > Erro" : "blue' > Sucesso" ) . "</font>"
                    . "       </th>"
                    . "       <th>"
                    . "           <font color='" . ($log["hasErrors"] ? "red' >" : "blue' >" ) . $log["msg"] . "</font>"
                    . "       </th>"
                    . "       <th>"
                    . "           <font color='" . ($log["hasErrors"] ? "red' >" : "blue' >" ) . $log["codigoProposta"] . "</font>"
                    . "       </th>"
                    . "   </tr>";
            }

            $htmlMail . "</table>";

            $message = new YiiMailMessage;
            $message->view = "mandaMailFichamento";
            $message->subject = 'Log Importação NFe';

            $parametros = [
                    'email' => [
                            'link' => "",
                            'titulo' => "Log NFe Proposta .::. Importação",
                            'mensagem' => $htmlMail,
                            'tipo' => "1"
                    ]
            ];

            $message->setBody($parametros, 'text/html');

            $message->addTo('contato@totorods.com');
            $message->from = 'no-reply@credshow.com.br';

            Yii::app()->mail->send($message);
        }
    }

    public function actionImportarFIDC() {

        $sql = "SELECT CODIGO AS CODIGO, lpad(nf,9,'0') AS NF, CHAVE AS CHAVE  FROM ImportarFIDC WHERE 1";

        try {

            $resultado = Yii::app()->db->createCommand($sql)->queryAll();

            foreach ($resultado as $r) {

                $transaction = Yii::app()->db->beginTransaction();

                $proposta = Proposta::model()->find("habilitado AND codigo = '" . $r["CODIGO"] . "'");

                if ($proposta !== null) {

                    $venda = Venda::model()->find("habilitado AND Proposta_id = $proposta->id");

                    if ($venda == null) {

                        $venda = new Venda;

                        $venda->habilitado = 1;
                        $venda->data_cadastro = date("Y-m-d H:i:s");
                        $venda->Proposta_id = $proposta->id;

                        if (!$venda->save()) {

                            ob_start();
                            var_dump($venda->getErrors());
                            $resErro = ob_get_clean();

                            echo "Erro ao gravar registro da venda: $resErro <br>";

                            $transaction->rollBack();

                            continue;
                        }
                    }

                    $notaFiscal = NotaFiscal::model()->find("habilitado AND serie = '1' AND documento = '" . $r["NF"] . "'");

                    if ($notaFiscal == null) {

                        $notaFiscal = new NotaFiscal;

                        $notaFiscal->data_cadastro = date("Y-m-d H:i:s");
                        $notaFiscal->habilitado = 1;
                        $notaFiscal->documento = $r["NF"];
                        $notaFiscal->serie = '1';

                        if (!$notaFiscal->save()) {

                            ob_start();
                            var_dump($notaFiscal->getErrors());
                            $erroNF = ob_get_clean();

                            echo "Erro ao salvar Nota Fiscal. NotaFiscal: $erroNF <br>";

                            $transaction->rollBack();

                            continue;
                        }
                    }

                    $vendaHasNF = VendaHasNotaFiscal::model()->find("habilitado AND Nota_Fiscal_id = $notaFiscal->id AND Venda_id = $venda->id");

                    if ($vendaHasNF == null) {

                        $vendaHasNF = new VendaHasNotaFiscal;
                        $vendaHasNF->habilitado = 1;
                        $vendaHasNF->data_cadastro = date("Y-m-d H:i:s");
                        $vendaHasNF->Nota_Fiscal_id = $notaFiscal->id;
                        $vendaHasNF->Venda_id = $venda->id;

                        if (!$vendaHasNF->save()) {

                            ob_start();
                            var_dump($vendaHasNF->getErrors());
                            $resErro = ob_get_clean();

                            echo "Erro ao gravar registro da venda (VendaHasNF): $resErro <br>";

                            $transaction->rollBack();

                            continue;
                        }
                    }

                    $notaFiscal->Status_Nota_Fiscal_id = 2;
                    $notaFiscal->data = $proposta->data_cadastro;
                    $notaFiscal->chaveNFe = $r["CHAVE"];

                    if (!$notaFiscal->update()) {

                        ob_start();
                        var_dump($notaFiscal->getErrors());
                        $erroNF = ob_get_clean();

                        echo "Erro ao salvar Nota Fiscal. NotaFiscal: $erroNF <br>";

                        $transaction->rollBack();

                        continue;
                    }

                    $transaction->commit();

                    echo "NFe " . $r["NF"] . " atrelada à proposta " . $proposta->codigo;
                } else {
                    echo "Proposta " . $r["CODIGO"] . " não encontrada <br>";
                }
            }
        } catch (Exception $ex) {

            echo $ex->getTraceAsString();
        }
    }

    public function actionGetValoresSelecionadosLecca() {

        $retorno = [
                "msg" => "",
                "valorFinanciado" => 0,
                "valorRepasse" => 0
        ];

        $valorFinanciado = 0;
        $valorRepasse = 0;

        if (isset($_POST["propostasMarcadas"])) {

            foreach ($_POST["propostasMarcadas"] as $idProposta) {

                $proposta = Proposta::model()->find("habilitado AND id = $idProposta");

                if ($proposta !== null) {
                    $valorFinanciado += $proposta->valor - $proposta->valor_entrada;
                    $valorRepasse += $proposta->valorRepasse();
                }
            }

            $retorno["valorFinanciado"] = number_format($valorFinanciado, 2, ",", ".");
            $retorno["valorRepasse"] = number_format($valorRepasse, 2, ",", ".");
        } else {
            $retorno["msg"] = "POST vazio";
        }

        echo json_encode($retorno);
    }

    public function actionListarArquivosCNAB() {

        $erro = "";
        $dados = [];
        $recordsTotal = 0;
        $recordsFiltered = 0;

        $sql = "SELECT AC.id AS idArquivo, AC.TipoCNAB_id AS tipoCNAB, AC.data_cadastro AS dataArquivo, AC.urlArquivo AS url, U.nome_utilizador AS nomeUsuario, "
            . "COUNT(P.id) AS qtdPropostas, "
            . "SUM(P.valor - P.valor_entrada) AS valorFinanciado, "
            . "SUM(CASE WHEN TC.ModalidadeId = 1 THEN (P.valor - P.valor_entrada) ELSE ( (P.valor - P.valor_entrada) - ( (P.valor - P.valor_entrada) * (Fa.porcentagem_retencao/100) ) ) END) AS valorRepasse "
            //                        .   "CASE WHEN TC.ModalidadeId = 1 THEN SUM(P.valor - P.valor_entrada) ELSE SUM((P.valor - P.valor_entrada) - ( (P.valor - P.valor_entrada) * (Fa.porcentagem_retencao/100) )) END AS valorRepasse, "
            //                        .   "CASE WHEN AC.Bordero_id IS NULL THEN 0 ELSE AC.Bordero_id END AS idBordero "
            . "FROM ArquivoCNAB AS AC "
            . "INNER JOIN LinhaCNAB AS LC ON LC.habilitado AND AC.id = LC.ArquivoExtportado_id "
            . "INNER JOIN Parcela AS Pa ON Pa.habilitado AND Pa.id = LC.Parcela_id "
            . "INNER JOIN Titulo AS T ON T.habilitado AND T.id = Pa.Titulo_id "
            . "INNER JOIN Proposta AS P ON P.habilitado AND P.id = T.Proposta_id "
            . "INNER JOIN Tabela_Cotacao AS TC ON TC.habilitado AND TC.id = P.Tabela_id "
            . "INNER JOIN Fator AS Fa ON Fa.habilitado AND TC.id = Fa.Tabela_Cotacao_id AND Fa.carencia = P.carencia AND Fa.parcela = P.qtd_parcelas "
            . "INNER JOIN Usuario AS U ON U.habilitado AND AC.Usuario_id = U.id "
            . "LEFT  JOIN ItemDoBordero AS IB ON IB.habilitado AND IB.Proposta_id = P.id "
            . "WHERE AC.habilitado AND AC.TipoCNAB_id = 2 AND IB.id IS NULL ";

        $sqlAgrupamento = "GROUP BY AC.id, AC.data_cadastro, AC.urlArquivo, U.nome_utilizador ";

        try {

            $recordsTotal = count(Yii::app()->db->createCommand($sql . $sqlAgrupamento)->queryAll());

            //põe o filtro

            $recordsFiltered = count(Yii::app()->db->createCommand($sql . $sqlAgrupamento)->queryAll());

            $sqlLimite = "LIMIT " . $_POST["start"] . ", " . $_POST["length"];

            $resultado = Yii::app()->db->createCommand($sql . $sqlAgrupamento . $sqlLimite)->queryAll();

            foreach ($resultado as $r) {

                $btnBaixar = "<a href='" . $r["url"] . "' download>"
                    . " <i class='clip-download'></i>"
                    . "</a>";

                $dateTime = new DateTime($r["dataArquivo"]);
                $dataArquivo = $dateTime->format("d/m/Y H:i:s");

                $valorFinanciado = "R$ " . number_format($r["valorFinanciado"], 2, ",", ".");
                $valorRepasse = "R$ " . number_format($r["valorRepasse"], 2, ",", ".");

                if ($r["tipoCNAB"] == "1") {

                    /*                    $status = "<font color='blue'><b>Aguardando Borderôs</b></font>";

                      $btnEnviar  = "<button class='btn btn-blue btnBordero' title='Gerar Borderô' value=" . $r["idArquivo"] . ">"
                      . " <i class=' clip-data'></i>"
                      . "</button>"; */

                    $financeira = "FIDC";
                } else {

                    /*                    $status = "<font color='green'><b>Aguardando Envio</b></font>";

                      $btnEnviar  = "<button class='btn btn-green btnFTP' title='Enviar Arquivo' value=" . $r["idArquivo"] . ">"
                      . " <i class=' clip-arrow-right'></i>"
                      . "</button>"; */

                    $financeira = "FIDC Lecca";
                }

                /*                if ($r["idBordero"] !== "0")
                  {

                  $btnEnviar  = "<button class='btn btn-purple btnBordero' disabled title='Borderôs Gerados' value=" . $r["idArquivo"] . ">"
                  . " <i class='clip-checkmark-circle'></i>"
                  . "</button>";

                  $status = "<font color='purple'><b>Borderôs gerados</b></font>";

                  }
                  else
                  { */

                $status = "<font color='blue'><b>Aguardando Borderôs</b></font>";

                $btnEnviar = "<button class='btn btn-blue btnBordero' title='Gerar Borderô' value=" . $r["idArquivo"] . ">"
                    . " <i class=' clip-data'></i>"
                    . "</button>";

                //                }


                $dados[] = [
                        "idArquivo" => $r["idArquivo"],
                        "financeira" => $financeira,
                        "dataArquivo" => $dataArquivo,
                        "arquivo" => $btnBaixar,
                        "status" => $status,
                        "nomeUsuario" => $r["nomeUsuario"],
                        "qtdPropostas" => $r["qtdPropostas"],
                        "valorFinanciado" => $valorFinanciado,
                        "valorRepasse" => $valorRepasse,
                        "btnAcao" => $btnEnviar,
                ];
            }
        } catch (Exception $ex) {
            $erro = $ex->getMessage();
        }

        echo json_encode(
            [
                    "data" => $dados,
                    "recordsFiltered" => $recordsFiltered,
                    "recordsTotal" => $recordsTotal,
                    "erro" => $erro,
                    "sql" => $sql,
            ]
        );
    }

    public function actionGerarBorderosCNAB() {

        $erro = "";
        $retorno = [];

        if (isset($_POST["idArquivo"]) && $_POST["idArquivo"] !== "") {

            $sql = "SELECT DISTINCT F.id AS filialID, P.id AS propostaID                                           "
                . "FROM       ArquivoCNAB         AS A                                                            "
                . "INNER JOIN LinhaCNAB           AS LC   ON LC.habilitado AND  A.id  = LC.ArquivoExtportado_id   "
                . "INNER JOIN Parcela             AS Pa   ON Pa.habilitado AND Pa.id  = LC.Parcela_id             "
                . "INNER JOIN Titulo              AS T    ON  T.habilitado AND  T.id  = Pa.Titulo_id              "
                . "INNER JOIN Proposta            AS P    ON  P.habilitado AND  P.id  =  T.Proposta_id            "
                . "INNER JOIN Analise_de_Credito  AS AC   ON AC.habilitado AND AC.id  =  P.Analise_de_Credito_id  "
                . "INNER JOIN Filial              AS F    ON  F.habilitado AND  F.id  = AC.Filial_id              "
                . "WHERE A.habilitado AND A.id = " . $_POST["idArquivo"] . " "
                . "ORDER BY F.id, P.id ";

            try {

                $resultado = Yii::app()->db->createCommand($sql)->queryAll();

                $filialId = 0;

                $transaction = Yii::app()->db->beginTransaction();

                foreach ($resultado as $r) {

                    if ($filialId !== $r["filialID"]) {

                        /* $bordero                = new Bordero                           ;

                          $bordero->habilitado    = 1                                     ;
                          $bordero->dataCriacao   = date("Y-m-d H:i:s")                   ;
                          $bordero->destinatario  = $r["filialID"]                        ;
                          $bordero->criadoPor     = Yii::app()->session["usuario"]->id    ;
                          $bordero->Status        = 2                                     ;

                          if (!$bordero->save())
                          {

                          ob_start()                      ;
                          var_dump($bordero->getErrors()) ;
                          $erro = ob_get_clean()          ;

                          $transaction->rollBack()        ;

                          break                           ;

                          }
                          else
                          { */

                        $arquivo = ArquivoCNAB::model()->findByPk($_POST["idArquivo"]);

                        /*                            if($arquivo !== null)
                          {

                          $arquivo->Bordero_id = $bordero->id;

                          if(!$arquivo->save())
                          {

                          ob_start()                      ;
                          var_dump($arquivo->getErrors()) ;
                          $erro = ob_get_clean()          ;

                          $transaction->rollBack()        ;

                          break                           ;

                          }

                          }
                          else
                          { */

                        if ($arquivo == null) {

                            $erro = "Registro de arquivo não encontrado.";
                            $transaction->rollBack();
                            break;
                        } else {

                            $bordero = new Bordero;

                            $bordero->habilitado = 1;
                            $bordero->dataCriacao = date("Y-m-d H:i:s");
                            $bordero->destinatario = $r["filialID"];
                            $bordero->criadoPor = Yii::app()->session["usuario"]->id;
                            $bordero->Status = 1;
                            $bordero->ArquivoCNAB_id = $arquivo->id;

                            if (!$bordero->save()) {

                                ob_start();
                                var_dump($bordero->getErrors());
                                $erro = ob_get_clean();

                                $transaction->rollBack();

                                break;
                            }

                            $filialId = $r["filialID"];
                        }

                        //}
                    }

                    $filial = Filial::model()->findByPk($r["filialID"]);

                    if ($filial == null) {

                        $erro = "Proposta não encontrada";
                        $transaction->rollBack();
                        break;
                    }

                    $itemBordero = new ItemDoBordero;
                    $itemBordero->Bordero = $bordero->id;
                    $itemBordero->Proposta_id = $r["propostaID"];
                    $itemBordero->habilitado = 1;
                    $itemBordero->Status = 2;

                    $recebimentoHasProposta = RecebimentoDeDocumentacaoHasProposta::model()->find("habilitado AND Proposta_id = " . $r["propostaID"]);

                    if ($recebimentoHasProposta == null) {

                        $itemBordero->pagoImagem = 1;
                    } else {

                        $itemBordero->pagoImagem = 0;
                    }

                    if (!$itemBordero->save()) {

                        ob_start();
                        var_dump($itemBordero->getErrors());
                        $erro = ob_get_clean();

                        $transaction->rollBack();

                        break;
                    }

                    $titulo = Titulo::model()->find("habilitado AND NaturezaTitulo_id = 2 AND Proposta_id = " . $r["propostaID"]);

                    if ($titulo !== null) {

                        $parcela = Parcela::model()->find("habilitado AND Titulo_id = $titulo->id");

                        if ($parcela !== null) {

                            $borderoHasParcela = new BorderoHasParcela;
                            $borderoHasParcela->Bordero_id = $bordero->id;
                            $borderoHasParcela->Parcela_id = $parcela->id;
                            $borderoHasParcela->data_cadastro = date("Y-m-d H:i:s");
                            $borderoHasParcela->habilitado = 1;

                            if (!$borderoHasParcela->save()) {


                                ob_start();
                                var_dump($borderoHasParcela->getErrors());
                                $erro = ob_get_clean();

                                $transaction->rollBack();

                                break;
                            }
                        } else {

                            $erro = "Proposta de ID " . $r["propostaID"] . " sem parcela de título a pagar gerada.";

                            $transaction->rollBack();

                            break;
                        }
                    } else {

                        $erro = "Proposta de ID " . $r["propostaID"] . " sem título a pagar gerado.";

                        $transaction->rollBack();

                        break;
                    }
                }

                if (empty($erro)) {

                    $transaction->commit();

                    $retorno = [
                            "title" => "Sucesso",
                            "msg" => "Borderô(s) gerado(s) com sucesso",
                            "hasErrors" => 0,
                            "type" => "success"
                    ];
                } else {

                    $retorno = [
                            "title" => "Erro",
                            "msg" => $erro,
                            "hasErrors" => 1,
                            "type" => "error"
                    ];
                }
            } catch (Exception $ex) {

                $retorno = [
                        "title" => "Erro",
                        "msg" => $ex->getMessage(),
                        "hasErrors" => 1,
                        "type" => "error"
                ];
            }
        } else {

            $retorno = [
                    "title" => "Atenção",
                    "msg" => "POST sem dados",
                    "hasErrors" => 1,
                    "type" => "info"
            ];
        }

        echo json_encode($retorno);
    }

    public function actionGetPropostasArquivo() {

        $dados = [];
        $retorno = [];
        $valorTotalFinanciado = 0;
        $valorTotalRepasse = 0;
        $recordsTotal = 0;
        $recordsFiltered = 0;
        $erro = "";

        if (isset($_POST["idArquivo"]) && $_POST["idArquivo"] !== "") {

            $sql = "SELECT P.id                                        AS idProposta       , "
                . "P.codigo                                           AS codigoProposta   , "
                . "P.data_cadastro                                    AS dataProposta     , "
                . "D.numero                                           AS cpf              , "
                . "Pe.nome                                            AS nomeCliente      , "
                . "CONCAT(F.nome_fantasia,' - ',E.cidade,'/',E.uf)    AS filial           , "
                . "(P.valor - P.valor_entrada)                        AS valorFinanciado  , "
                . "CASE "
                . "   WHEN TC.ModalidadeId = 1 THEN (P.valor - P.valor_entrada) "
                . "   ELSE ((P.valor - P.valor_entrada) - ( (P.valor - P.valor_entrada) * (Fa.porcentagem_retencao/100) ) ) "
                . "END  AS valorRepasse       "
                . "FROM       ArquivoCNAB             AS A                                                                                            "
                . "INNER JOIN LinhaCNAB               AS LC   ON  LC.habilitado AND   A.id    =  LC.ArquivoExtportado_id                              "
                . "INNER JOIN Parcela                 AS Pa   ON  Pa.habilitado AND  Pa.id    =  LC.Parcela_id                                        "
                . "INNER JOIN Titulo                  AS T    ON   T.habilitado AND   T.id    =  Pa.Titulo_id                                         "
                . "INNER JOIN Proposta                AS P    ON   P.habilitado AND   P.id    =   T.Proposta_id                                       "
                . "INNER JOIN Analise_de_Credito      AS AC   ON  AC.habilitado AND  AC.id    =   P.Analise_de_Credito_id                             "
                . "INNER JOIN Cliente                 AS C    ON   C.habilitado AND   C.id    =  AC.Cliente_id                                        "
                . "INNER JOIN Pessoa                  AS Pe   ON  Pe.habilitado AND  Pe.id    =   C.Pessoa_id                                         "
                . "INNER JOIN Pessoa_has_Documento    AS PhD  ON PhD.habilitado AND  Pe.id    = PhD.Pessoa_id                                         "
                . "INNER JOIN Documento               AS D    ON   D.habilitado AND   D.id    = PhD.Documento_id          AND D.Tipo_documento_id = 1 "
                . "INNER JOIN Filial                  AS F    ON   F.habilitado AND   F.id    =  AC.Filial_id                                         "
                . "INNER JOIN Filial_has_Endereco     AS FhE  ON FhE.habilitado AND   F.id    = FhE.Filial_id                                         "
                . "INNER JOIN Endereco                AS E    ON   E.habilitado AND   E.id    = FhE.Endereco_id                                       "
                . "INNER JOIN Tabela_Cotacao          AS TC   ON  TC.habilitado AND  TC.id    =   P.Tabela_id                                         "
                . "INNER JOIN Fator                   AS Fa   ON  Fa.habilitado AND  TC.id    =  Fa.Tabela_Cotacao_id     AND Fa.carencia = P.carencia AND Fa.parcela = P.qtd_parcelas "
                . "WHERE A.habilitado AND A.id = " . $_POST["idArquivo"] . " ";

            $sqlGroup = "GROUP BY P.id, P.data_cadastro, D.numero, Pe.nome, CONCAT(F.nome_fantasia,' - ',E.cidade,'/',E.uf) ";

            try {

                $resultado = Yii::app()->db->createCommand($sql . $sqlGroup)->queryAll();

                $recordsTotal = count($resultado);

                foreach ($resultado as $r) {
                    $valorTotalFinanciado += $r["valorFinanciado"];
                    $valorTotalRepasse += $r["valorRepasse"];
                }

                $resultado = Yii::app()->db->createCommand($sql . $sqlGroup)->queryAll();

                $recordsFiltered = count($resultado);

                "ORDER BY F.nome_fantasia, P.id ";

                $sql .= $sqlGroup . "LIMIT " . $_POST["start"] . ", " . $_POST["length"];

                $resultado = Yii::app()->db->createCommand($sql)->queryAll();

                foreach ($resultado as $r) {

                    $dateTime = new DateTime($r["dataProposta"]);
                    $dataProposta = $dateTime->format("d/m/Y H:i:s");

                    $valorFinanciado = "R$ " . number_format($r["valorFinanciado"], 2, ",", ".");
                    $valorRepasse = "R$ " . number_format($r["valorRepasse"], 2, ",", ".");

                    $dados[] = [
                            "idProposta" => $r["idProposta"],
                            "codigoProposta" => $r["codigoProposta"],
                            "dataProposta" => $dataProposta,
                            "cpf" => $r["cpf"],
                            "nomeCliente" => $r["nomeCliente"],
                            "filial" => $r["filial"],
                            "valorFinanciado" => $valorFinanciado,
                            "valorRepasse" => $valorRepasse,
                    ];
                }
            } catch (Exception $ex) {

                $erro = $ex->getMessage();
            }
        } else {

            $erro = "POST vazio";
        }

        echo json_encode(
            [
                    "data" => $dados,
                    "recordsTotal" => $recordsTotal,
                    "recordsFiltered" => $recordsFiltered,
                    "vlrTotFin" => number_format($valorTotalFinanciado, 2, ",", "."),
                    "vlrTotRep" => number_format($valorTotalRepasse, 2, ",", "."),
                    "erro" => $erro,
                    "sql" => $sql
            ]
        );
    }

}
