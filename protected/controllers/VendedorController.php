<?php

class VendedorController extends Controller
{	

	public $layout = '//layouts/main_sigac_template';

	public function filters() {

        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules() 
    {

        return array(
            array(
                'allow' ,
                'actions'       => array(
                                            'index'       , 
                                            'novoVendedor'    , 
                                            'vendedorCadastrado'    , 
                                            'getVendedores'
                                        ),
                'users'         => array('@'),
                'expression'    =>  'Yii::app()->session["usuario"]->role == "empresa_admin"        || 	'
                				.	'Yii::app()->session["usuario"]->role == "crediarista"				'
            ),
            array(
                'deny'                  ,
                'users' => array('*')   ,
            ),
        );
    }

	public function actionIndex()
	{
		$baseUrl 	= Yii::app()->baseUrl;
        $cs 		= Yii::app()->getClientScript();
        
		$cs->registerScriptFile($baseUrl . '/js/jquery.min.js');

		$this->render('index');
	}

	public function actionNovoVendedor(){
		if( isset( $_POST['cpf'] ) && isset( $_POST['nome'] ) && isset($_POST['filial_id']))
      	{ 
        	echo json_encode( Vendedor::model()->adicionar($_POST['nome'], $_POST['cpf'], $_POST['filial_id']) );
      	}
	}

	public function actionVendedorCadastrado(){
		if( isset( $_POST['cpf'] ) && isset($_POST['filial_id']))
      	{ 
        	echo json_encode( Vendedor::model()->cadastrado($_POST['cpf'], $_POST['filial_id']) );
      	}
	}

	public function actionGetVendedores(){
		$filia_id = 184;

		if( !empty( $_GET['filial_id'] ) ){
			$filial_id = $_GET['filial_id'];
		}

		echo json_encode(Vendedor::model()->listar($filial_id));
	}
}