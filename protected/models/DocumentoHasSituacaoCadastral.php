<?php

/**
 * This is the model class for table "Documento_has_Situacao_cadastral".
 *
 * The followings are the available columns in table 'Documento_has_Situacao_cadastral':
 * @property integer $id
 * @property integer $Documento_id
 * @property integer $Situacao_cadastral_id
 * @property string $data_cadastro
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property Documento $documento
 * @property SituacaoCadastral $situacaoCadastral
 */
class DocumentoHasSituacaoCadastral extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Documento_has_Situacao_cadastral';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Documento_id, Situacao_cadastral_id', 'required'),
			array('Documento_id, Situacao_cadastral_id, habilitado', 'numerical', 'integerOnly'=>true),
			array('data_cadastro', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, Documento_id, Situacao_cadastral_id, data_cadastro, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'documento' => array(self::BELONGS_TO, 'Documento', 'Documento_id'),
			'situacaoCadastral' => array(self::BELONGS_TO, 'SituacaoCadastral', 'Situacao_cadastral_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Documento_id' => 'Documento',
			'Situacao_cadastral_id' => 'Situacao Cadastral',
			'data_cadastro' => 'Data Cadastro',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Documento_id',$this->Documento_id);
		$criteria->compare('Situacao_cadastral_id',$this->Situacao_cadastral_id);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DocumentoHasSituacaoCadastral the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
