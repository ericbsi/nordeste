<?php

class ItemTabelaDePrecos extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Item_Tabela_de_Precos';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Tabela_de_Precos_id, Item_do_Estoque_id', 'required'),
            array('Tabela_de_Precos_id, Item_do_Estoque_id, habilitado', 'numerical', 'integerOnly' => true),
            array('valor', 'numerical'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, valor, Tabela_de_Precos_id, Item_do_Estoque_id, habilitado', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'tabelaDePrecos' => array(self::BELONGS_TO, 'TabelaDePrecos', 'Tabela_de_Precos_id'),
            'itemDoEstoque' => array(self::BELONGS_TO, 'ItemDoEstoque', 'Item_do_Estoque_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'valor' => 'Valor',
            'Tabela_de_Precos_id' => 'Tabela De Precos',
            'Item_do_Estoque_id' => 'Item Do Estoque',
            'habilitado' => 'Habilitado',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('valor', $this->valor);
        $criteria->compare('Tabela_de_Precos_id', $this->Tabela_de_Precos_id);
        $criteria->compare('Item_do_Estoque_id', $this->Item_do_Estoque_id);
        $criteria->compare('habilitado', $this->habilitado);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchMenorPreco($itEst) 
    {
        
        $qry  = 'select min(valor) as menorValor ';
        $qry .= 'from       Item_Tabela_de_Precos i ';
        $qry .= 'inner join Tabela_de_Precos      t on t.habilitado and t.id = i.Tabela_de_Precos_id ';
        $qry .= 'where i.habilitado and "' . date('Y-m-d') . '" between t.dataDe and dataAte and i.Item_do_Estoque_id = ' . $itEst->id;
        
        //var_dump($qry);
        
        $menorValor = Yii::app()->db->createCommand($qry)->queryAll();
        
        return number_format($menorValor[0]['menorValor'],2,',','.');
        
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ItemTabelaDePrecos the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
