<?php

/**
 * This is the model class for table "ListaNegra_has_Cliente".
 *
 * The followings are the available columns in table 'ListaNegra_has_Cliente':
 * @property integer $ListaNegra_id
 * @property integer $Cliente_id
 * @property string $data_cadastro
 * @property integer $habilitado
 * @property integer $ativo
 */
class ListaNegraHasCliente extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ListaNegra_has_Cliente';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ListaNegra_id, Cliente_id, data_cadastro, habilitado, ativo', 'required'),
			array('ListaNegra_id, Cliente_id, habilitado, ativo', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ListaNegra_id, Cliente_id, data_cadastro, habilitado, ativo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ListaNegra_id' => 'Lista Negra',
			'Cliente_id' => 'Cliente',
			'data_cadastro' => 'Data Cadastro',
			'habilitado' => 'Habilitado',
			'ativo' => 'Ativo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ListaNegra_id',$this->ListaNegra_id);
		$criteria->compare('Cliente_id',$this->Cliente_id);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('ativo',$this->ativo);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ListaNegraHasCliente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
