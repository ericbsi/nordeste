<?php

/**
 * This is the model class for table "ItensDePara".
 *
 * The followings are the available columns in table 'ItensDePara':
 * @property integer $id
 * @property string $tabelaDe
 * @property string $campoDe
 * @property string $tabelaPara
 * @property string $campoPara
 * @property string $data_cadastro
 * @property integer $habilitado
 * @property integer $dePara_id
 *
 * The followings are the available model relations:
 * @property DePara $dePara
 * @property ValoresDePara[] $valoresDeParas
 */
class ItensDePara extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ItensDePara';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tabelaDe, campoDe, tabelaPara, campoPara, data_cadastro, dePara_id', 'required'),
			array('habilitado, dePara_id', 'numerical', 'integerOnly'=>true),
			array('tabelaDe', 'length', 'max'=>45),
			array('campoDe', 'length', 'max'=>100),
			array('tabelaPara', 'length', 'max'=>150),
			array('campoPara', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tabelaDe, campoDe, tabelaPara, campoPara, data_cadastro, habilitado, dePara_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'dePara' => array(self::BELONGS_TO, 'DePara', 'dePara_id'),
			'valoresDeParas' => array(self::HAS_MANY, 'ValoresDePara', 'ItensDePara_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tabelaDe' => 'Tabela De',
			'campoDe' => 'Campo De',
			'tabelaPara' => 'Tabela Para',
			'campoPara' => 'Campo Para',
			'data_cadastro' => 'Data Cadastro',
			'habilitado' => 'Habilitado',
			'dePara_id' => 'De Para',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tabelaDe',$this->tabelaDe,true);
		$criteria->compare('campoDe',$this->campoDe,true);
		$criteria->compare('tabelaPara',$this->tabelaPara,true);
		$criteria->compare('campoPara',$this->campoPara,true);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('dePara_id',$this->dePara_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ItensDePara the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
