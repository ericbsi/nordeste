<?php

class Fabricante extends CActiveRecord
{	

	public function novo( $Fabricante )
	{	

		$aReturn = array(
            'hasErrors'     => 0,
            'msg'           => 'Fabricante cadastrado com Sucesso.',
            'pntfyClass'    => 'success'
        );


		if( !empty( $Fabricante['cnpj'] ) )
		{
			$Empresa 				=  new Empresa;
			$Empresa->attributes	=  $Fabricante;


			if( !$Empresa->save() )
			{
				$aReturn['hasErrors']	= 1;
				$aReturn['msg']			= 'Ocorreu um erro.';
			}

			else
			{

				$Fabri 					= new Fabricante;
				$Fabri->Empresa_id		= $Empresa->id;
				$Fabri->habilitado		= 1;
				
				if( !$Fabri->save() )
				{
					$aReturn['hasErrors']	= 1;
					$aReturn['msg']			= 'Ocorreu um erro.';
				}

			}

		}

		if( $aReturn['hasErrors'] )
		{
			$aReturn['pntfyClass'] 	= 'error';
		}

		return $aReturn;

	}

	public function searchFabricantesSelec2($q){

		$retorno = null;

		if( !empty( $q ) ){

			$crt 			= new CDbCriteria;
			
			$crt->with   	= array('empresa' => array(
		    	'alias'  	=> 'e'
			));

			$crt->addSearchCondition('e.razao_social', $q, TRUE, 'OR');
			$crt->addSearchCondition('e.nome_fantasia', $q, TRUE, 'OR');

			$fabricantes 	= Fabricante::model()->findAll( $crt );

			if( count( $fabricantes ) > 0 )
			{
				foreach( $fabricantes as $fabricante )
				{
					$row = array(
						'id'	=> $fabricante->id,
						'text'	=> $fabricante->empresa->nome_fantasia
					);
					$retorno[] 	= $row;
				}
			}
		}
		return $retorno;
	}

	public function listFabricantes( $draw, $offset ){

		$recordsTotal		= 0;
		$recordsFiltered	= 0;
		$idsFiliais			= array();
		$rows 				= array();
		$util 				= new Util;
		
		/*			
		$criteriaNoFilter 				= new CDbCriteria;
		$criteriaFilter 	 			= new CDbCriteria;

		$criteriaNoFilter->addInCondition('Filial_id', $idsFiliais);
		$criteriaNoFilter->addInCondition('habilitado', array(1));

		$recordsTotal 					= count( Estoque::model()->findAll( $criteriaNoFilter ) );

		$criteriaFilter->addInCondition('t.Filial_id', $idsFiliais, 'AND');
		$criteriaFilter->addInCondition('t.habilitado', array(1), 'AND');

		if( $locais !== "" )
		{
			$criteriaFilter->addInCondition('t.Local_id', $locais, 'AND');
		}

		if( $vende !== "" )
		{
			$criteriaFilter->addInCondition('t.vende', $vende, 'AND');
		}


		$criteriaFilter->offset    		= $offset;
		$criteriaFilter->limit     		= 10;
		$criteriaFilter->with       	= array('local');
		$criteriaFilter->order 			= "local.nome ASC";

		$estoquesFiltro 				= Estoque::model()->findAll($criteriaFilter);
		
		if( count($estoquesFiltro) > 0 )
		{

			foreach( $estoquesFiltro as $estoque )
			{
				$row = array(
					'filial' => $estoque->filial->getConcat(),
					'local'  => $estoque->local->nome,
					'vende'  => ($estoque->vende ? 'Sim' : 'Não' ),
				);

				$rows[] =  $row;
			}
		}*/

		foreach( Fabricante::model()->findAll() as $f )
		{	

			$btn                         = '<form method="post" action="'.Yii::app()->getBaseUrl(true).'/fabricante/editar">';
            $btn                        .= '  <button class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Editar"><i class="fa fa-edit"></i></button>';
            $btn                        .= '  <input type="hidden" name="fabricante_id" value="'.$f->id.'">';
            $btn                        .= '</form>';

			$row 					= array(
				'fabricante'		=> $f->empresa->razao_social,
				'cnpj'				=> $f->empresa->cnpj,
				'btn'				=> $btn,
			);

			$rows[] 				= $row;
			$btn 	 				= "";
		}

		return (array(
            "draw"              	=> $draw,
            "recordsTotal"      	=> count($rows),
            "recordsFiltered"   	=> count($rows),
            "data"              	=> $rows,
        ));
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Fabricante';
	}

	public function rules()
	{		
		return array(
			array('habilitado, Empresa_id', 'required'),
			array('habilitado', 'numerical', 'integerOnly'=>true),			
			array('id, Empresa_id, habilitado', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{		
		return array(
			'modelos' 	=> array(self::HAS_MANY, 'Modelo', 'Fabricante_id'),
			'empresa' 	=> array(self::BELONGS_TO, 'Empresa', 'Empresa_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' 			=> 'ID',
			'Empresa_id'	=> 'Empresa',
			'habilitado' 	=> 'Habilitado',
		);
	}

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Empresa_id',$this->Empresa_id,true);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Fabricante the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
