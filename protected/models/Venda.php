<?php

class Venda extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'Venda';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
                        //array('codigo, data, Cliente_id', 'required'),
                        array('data_cadastro, habilitado'                                                       , 'required'                                ),
                        array('Cliente_id, NotaFiscal_id, Proposta_id, habilitado'                              , 'numerical'   , 'integerOnly' => true     ),
                        array('codigo'                                                                          , 'length'      , 'max'         => 15       ),
                        // The following rule is used by search().
                        // @todo Please remove those attributes that should not be searched.
                        array('id, codigo, data_cadastro, Cliente_id, NotaFiscal_id, Proposta_id, habilitado'   , 'safe'        , 'on'          => 'search' ),
                    );
        
    }

    /**
     * @return array relational rules.
     */
    public function relations()
{
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
                        'itemDaVendas'  => array(self::HAS_MANY     , 'ItemDaVenda'             , 'Venda_id'        ),
                        'notasFiscais'  => array(self::HAS_MANY     , 'VendaHasNotaFiscal'   , 'Venda_id'        ),
                        'cliente'       => array(self::BELONGS_TO   , 'Cliente'                 , 'Cliente_id'      ),
//                                'notaFiscal'    => array(self::BELONGS_TO   , 'NotaFiscal'  , 'NotaFiscal_id'   ),
                        'proposta'      => array(self::BELONGS_TO   , 'Proposta'                , 'Propsota_id'     ),
                    );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
                        'id'            => 'ID'             ,
                        'codigo'        => 'Codigo'         ,
                        'data_cadastro' => 'Data'           ,
                        'Cliente_id'    => 'Cliente'        ,
                        'habilitado'    => 'Habilitado'     ,
                        'NotaFiscal_id' => 'Nota Fiscal'    ,
                        'Proposta'      => 'Proposta'       ,
                    );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id'             , $this->id                     );
        $criteria->compare('codigo'         , $this->codigo         ,true   );
        $criteria->compare('data_cadastro'  , $this->data_cadastro  ,true   );
        $criteria->compare('Cliente_id'     , $this->Cliente_id             );
        $criteria->compare('NotaFiscal_id'  , $this->NotaFiscal_id          );
        $criteria->compare('Proposta_id'    , $this->Proposta_id            );
        $criteria->compare('habilitado'     , $this->habilitado             );

        return new CActiveDataProvider  (
                                            $this                                   , 
                                            array   (
                                                        'criteria' => $criteria ,
                                                    )
                                        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Venda the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function novo($codigo, $data_cadastro, $Cliente_id, $habilitado, $NotaFiscal_id, $Proposta_id)
    {
        
        $venda                  = new Venda         ;
        $venda->codigo          = $codigo           ;
        $venda->data_cadastro   = $data_cadastro    ;
        $venda->Cliente_id      = $Cliente_id       ;
        $venda->habilitado      = $habilitado       ;
        $venda->NotaFiscal_id   = $NotaFiscal_id    ;
        $venda->Proposta_id     = $Proposta_id      ;

        if ($venda->save())
        {
            
            if (isset($NotaFiscal_id))
            {
                
                $notaFiscal = NotaFiscal::model()->find("habilitado AND id = $NotaFiscal_id");
                
                if ($notaFiscal !== null)
                {
                    
                    $vendaHasNF = VendaHasNotaFiscal::model()->find("habilitado AND Nota_Fiscal_id = $notaFiscal->id AND Venda_id = $venda->id");
                    
                    if($vendaHasNF !== null)
                    {
                        
                        $vendaHasNF                 = new VendaHasNotaFiscal    ;
                        $vendaHasNF->habilitado     = 1                         ;
                        $vendaHasNF->data_cadastro  = date("Y-m-d H:i:s")       ;
                        $vendaHasNF->Venda_id       = $venda->id                ;
                        $vendaHasNF->Nota_Fiscal_id = $notaFiscal->id           ;
                        
                        $vendaHasNF->save();
                        
                    }
                    
                }
                
            }
            
            return $venda;
        } 
        else
        {
            return NULL;
        }
    }

    public function getProdutoSeguro()
    {
        
        if( $this->itemDaVendas !== NULL )
        {
            foreach( $this->itemDaVendas as $item )
            {
                if( ( $item->habilitado ) && ( $item->itemDoEstoque->produto->id == 1 ) )
                {
                    return $item;
                }
            }
        }
        else
        {
            return NULL;
        }
        
    }
	
}
