<?php

/**
 * This is the model class for table "EstornoBaixa_has_StatusEstorno".
 *
 * The followings are the available columns in table 'EstornoBaixa_has_StatusEstorno':
 * @property integer $EstornoBaixa_id
 * @property integer $StatusEstorno_id
 * @property integer $Usuario_id
 * @property integer $id
 * @property integer $habilitado
 * @property string $data_cadastro
 * @property string $observacao
 *
 * The followings are the available model relations:
 * @property EstornoBaixa $estornoBaixa
 * @property StatusEstorno $statusEstorno
 * @property Usuario $usuario
 */
class EstornoBaixaHasStatusEstorno extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'EstornoBaixa_has_StatusEstorno';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('EstornoBaixa_id, StatusEstorno_id, Usuario_id, data_cadastro', 'required'),
			array('EstornoBaixa_id, StatusEstorno_id, Usuario_id, habilitado', 'numerical', 'integerOnly'=>true),
			array('observacao', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('EstornoBaixa_id, StatusEstorno_id, Usuario_id, id, habilitado, data_cadastro, observacao', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'estornoBaixa' => array(self::BELONGS_TO, 'EstornoBaixa', 'EstornoBaixa_id'),
			'statusEstorno' => array(self::BELONGS_TO, 'StatusEstorno', 'StatusEstorno_id'),
			'usuario' => array(self::BELONGS_TO, 'Usuario', 'Usuario_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'EstornoBaixa_id' => 'Estorno Baixa',
			'StatusEstorno_id' => 'Status Estorno',
			'Usuario_id' => 'Usuario',
			'id' => 'ID',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'observacao' => 'Observacao',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('EstornoBaixa_id',$this->EstornoBaixa_id);
		$criteria->compare('StatusEstorno_id',$this->StatusEstorno_id);
		$criteria->compare('Usuario_id',$this->Usuario_id);
		$criteria->compare('id',$this->id);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('observacao',$this->observacao,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EstornoBaixaHasStatusEstorno the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
