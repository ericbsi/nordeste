<?php

class Baixa extends CActiveRecord {

    public function tableName() {
        return 'Baixa';
    }

    public function rules() {
        return array(
            array('Parcela_id', 'required'),
            array('baixado, Parcela_id, Filial_id, Cliente_id, DadosPagamento_id', 'numerical', 'integerOnly' => true),
            array('valor_pago, data_da_ocorrencia, data_da_baixa, codigo_banco_cobrador, agencia_cobradora, valor_multa, valor_iof, valor_mora, tarifa_custas, valor_abatimento, valor_desconto', 'safe'),
            array('id, valor_pago, Filial_id, Cliente_id, data_da_ocorrencia, data_da_baixa codigo_banco_cobrador, agencia_cobradora, valor_multa, valor_iof, valor_mora, tarifa_custas, valor_abatimento, valor_desconto, baixado, Parcela_id, observacao, DadosPagamento_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'parcela' => array(self::BELONGS_TO, 'Parcela', 'Parcela_id'),
            'filial' => array(self::BELONGS_TO, 'Filial', 'Filial_id'),
            'cliente' => array(self::BELONGS_TO, 'Cliente', 'Cliente_id'),
            'dadosPagamento' => array(self::BELONGS_TO, 'DadosPagamento', 'DadosPagamento_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'valor_pago' => 'Valor Pago',
            'data_da_ocorrencia' => 'Data Da Ocorrencia',
            'data_da_baixa' => 'Data Da Baixa',
            'codigo_banco_cobrador' => 'Codigo Banco Cobrador',
            'agencia_cobradora' => 'Agencia Cobradora',
            'valor_multa' => 'Valor Multa',
            'valor_iof' => 'Valor Iof',
            'valor_mora' => 'Valor Mora',
            'tarifa_custas' => 'Tarifa Custas',
            'valor_abatimento' => 'Valor Abatimento',
            'valor_desconto' => 'Valor Desconto',
            'baixado' => 'Baixado',
            'Parcela_id' => 'Parcela',
            'Filial_id' => 'Filial',
            'Cliente_id' => 'Cliente',
            'observacao' => 'Observação',
            'DadosPagamento_id' => 'Dados Pagamento',
        );
    }

    public function search() {

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('valor_pago', $this->valor_pago, true);
        $criteria->compare('data_da_ocorrencia', $this->data_da_ocorrencia, true);
        $criteria->compare('data_da_baixa', $this->data_da_baixa, true);
        $criteria->compare('codigo_banco_cobrador', $this->codigo_banco_cobrador, true);
        $criteria->compare('agencia_cobradora', $this->agencia_cobradora, true);
        $criteria->compare('valor_multa', $this->valor_multa, true);
        $criteria->compare('valor_iof', $this->valor_iof, true);
        $criteria->compare('valor_mora', $this->valor_mora, true);
        $criteria->compare('tarifa_custas', $this->tarifa_custas, true);
        $criteria->compare('valor_abatimento', $this->valor_abatimento, true);
        $criteria->compare('valor_desconto', $this->valor_desconto, true);
        $criteria->compare('baixado', $this->baixado);
        $criteria->compare('Parcela_id', $this->Parcela_id);
        $criteria->compare('Filial_id', $this->Filial_id);
        $criteria->compare('Cliente_id', $this->Cliente_id);
        $criteria->compare('observacao', $this->observacao, true);
        $criteria->compare('DadosPagamento_id', $this->DadosPagamento_id, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /* Funções Eric Vieira */

    public function baixasAbertas() {

        return Baixa::model()->findAll('baixado = ' . 0);
    }

    public function importarRetorno($arquivo, $BancoId, $forcar_importacao, $forcar_importacao_obs) {
        $Banco = Banco::model()->findByPk($BancoId);
        $campoCnabConfig = new CampoCnabConfig;

        if ($Banco->codigo == '399') {
            /* HSBC */
            return $this->registrarBaixa($arquivo, 1, $Banco->codigo, $forcar_importacao_obs);
        } else if ($Banco->codigo == '237') {
            return $this->registrarBaixaBradesco($arquivo, $forcar_importacao, $Banco->codigo, $forcar_importacao_obs);
        } else {
            $fi = true;

            $arrReturn = array(
                'hasErrors' => false,
                'msgs' => array(
                ),
                'registrados' => 0,
                'pagNaoReg' => 0,
                'liquidados' => 0,
                'naoEncontrados' => 0,
                'comErro' => 0,
                'fileName' => $arquivo['name']
            );

            $_UPCONFIG['dir'] = '/var/www/nordeste/retornos_import/';
            $_UPCONFIG['finalFile'] = $_UPCONFIG['dir'] . $arquivo['name'];

            /* Tentando salvar o arquivo em disco. */
            if (move_uploaded_file($arquivo['tmp_name'], $_UPCONFIG['finalFile'])) {
                $oldUmask = umask(0);
                chmod($_UPCONFIG['finalFile'], 0755);
                umask($oldUmask);
                /* Carregando o arquivo final */
                $arquivoRetorno = file($_UPCONFIG['finalFile']);

                if (substr($arquivoRetorno[0], 76, 3) === $Banco->codigo) {
                    $forcar_importacao = true;
                    if ($forcar_importacao == '1') {
                        $fi = true;
                    } else {
                        $fi = ArquivoCnab::validarImportacao(substr($arquivoRetorno[0], 94, 6), 7);
                    }

                    if ($fi) {
                        $geracaoTxt = substr($arquivoRetorno[0], 94, 6);

                        $arquivoCnab = new ArquivoCnab;
                        $arquivoCnab->relative_path = $arquivo['name'];
                        $arquivoCnab->absolute_path = $_UPCONFIG['finalFile'];
                        $arquivoCnab->tipo = 2;
                        $arquivoCnab->data_importacao = date('Y-m-d');
                        $arquivoCnab->data_geracao = '20' . substr($geracaoTxt, 4, 2) . '-' . substr($geracaoTxt, 2, 2) . '-' . substr($geracaoTxt, 0, 2);
                        $arquivoCnab->Usuario_id = Yii::app()->session['usuario']->id;
                        $arquivoCnab->Filial_id = 19;
                        $arquivoCnab->Banco_id = 7;

                        if ($arquivoCnab->save()) {
                            if ($forcar_importacao_obs !== '0') {
                                Dialogo::model()->novo('Importação de Cnab - Observação', 'ArquivoCnab', $arquivoCnab->id, Yii::app()->session['usuario']->id, $forcar_importacao_obs);
                            }
                        }

                        for ($i = 1; $i < count($arquivoRetorno) - 1; $i++) {
                            $labelRangeGerado = substr($arquivoRetorno[$i], 62, 7);
                            $rangerGerado = RangesGerados::model()->find('label = "' . $labelRangeGerado . '"');
                            $dataOcorrencia = '20' . substr(substr($arquivoRetorno[$i], 110, 6), 4, 2) . '-' . substr(substr($arquivoRetorno[$i], 110, 6), 2, 2) . '-' . substr(substr($arquivoRetorno[$i], 110, 6), 0, 2);
                            $ocorrencia = intval(substr($arquivoRetorno[$i], 108, 2));

                            /* Verifica se o título existe no sistema */
                            if ($rangerGerado != NULL) {
                                /* Tipo possiveis de liquidação - Segundo as documentações */
                                $tiposLiquidacoes = array(6, 7, 8, 17);

                                /* OK, o titulo existe.. Então, vamos varres as possibilidades de ocorrência do retorno */

                                /* 2 - Tiulo teve seu registro confirmado */
                                if ($ocorrencia == 2) {
                                    RetornoParcelaCnab::model()->novo(NULL, $labelRangeGerado, NULL, NULL, 2, NULL, $dataOcorrencia, $rangerGerado->id, date('Y-m-d'));
                                    $arrReturn['registrados'] += 1;
                                }

                                /* Verifica se o título foi liquidado... */ else if (in_array($ocorrencia, $tiposLiquidacoes)) {
                                    $valor_pago = $campoCnabConfig->cnabValorMonetarioToDouble(substr($arquivoRetorno[$i], 253, 13));
                                    $juros_de_mora = $campoCnabConfig->cnabValorMonetarioToDouble(substr($arquivoRetorno[$i], 266, 13));

                                    $retornoParcelaCnab = RetornoParcelaCnab::model()->novo(3, $labelRangeGerado, $valor_pago, $juros_de_mora, $ocorrencia, NULL, $dataOcorrencia, $rangerGerado->id, date('Y-m-d'));

                                    /* Título pago, mas sem registro no sistema do banco */
                                    if ($ocorrencia == 38 || $ocorrencia == 39) {
                                        $arrReturn['pagNaoReg'] += 1;
                                    } else {
                                        $arrReturn['liquidados'] += 1;
                                    }

                                    /* Verifica se o título não está totalmente baixado (Não existe baixa parcial, apenas do valor integral) */
                                    if ($rangerGerado->parcela->valor_atual < $rangerGerado->parcela->valor) {
                                        $rangerGerado->parcela->valor_atual = $campoCnabConfig->cnabValorMonetarioToDouble(substr($arquivoRetorno[$i], 253, 13));

                                        if ($rangerGerado->parcela->update()) {
                                            $baixaFilial_id = 0;
                                            $baixaCliente_id = 0;

                                            if ($rangerGerado->parcela->titulo->proposta != NULL) {
                                                $baixaFilial_id = $rangerGerado->parcela->titulo->proposta->analiseDeCredito->Filial_id;
                                                $baixaCliente_id = $rangerGerado->parcela->titulo->proposta->analiseDeCredito->Cliente_id;
                                            } else if ($rangerGerado->parcela->titulo->vendaW != NULL) {
                                                $baixaFilial_id = $rangerGerado->parcela->titulo->vendaW->Filial_id;
                                                $baixaCliente_id = $rangerGerado->parcela->titulo->vendaW->Cliente_id;
                                            } else {
                                                exit();
                                            }

                                            $this->novo(
                                                    $retornoParcelaCnab->data_ocorrencia, substr($arquivoRetorno[$i], 165, 3), substr($arquivoRetorno[$i], 168, 5), $retornoParcelaCnab->valor_pago, 0, 0, 0, substr($arquivoRetorno[$i], 266, 13), 1, $rangerGerado->Parcela_id, date('Y-m-d'), $baixaFilial_id, $baixaCliente_id
                                            );
                                        }
                                    }
                                }

                                /* Outro tipo de ocorrência (Instrução de baixa - geralmente em cancelamentos, erros, etc) */ else {
                                    RetornoParcelaCnab::model()->novo(NULL, $labelRangeGerado, NULL, NULL, $ocorrencia, NULL, $dataOcorrencia, $rangerGerado->id, date('Y-m-d'));
                                }
                            }
                        }
                    } else {
                        $oldUmask = umask(0);
                        chmod($_UPCONFIG['finalFile'], 0755);
                        umask($oldUmask);
                        unlink($_UPCONFIG['finalFile']);
                        $arrReturn['hasErrors'] = true;
                        $arrReturn['msgs'][] = "O arquivo anterior não foi importado. Para prosseguir, force a importação.";
                    }
                } else {
                    $arrReturn['hasErrors'] = true;
                    $arrReturn['msgs'][] = 'O arquivo enviado não corresponde ao banco selecionado.';
                }
            } else {
                $arrReturn['hasErrors'] = true;
                $arrReturn['msgs'][] = 'Não foi possível ler o arquivo, por favor, entre em contato com o suporte.';
            }

            return $arrReturn;
        }
    }

    public function novo($data_da_ocorrencia, $codigo_banco_cobrador, $agencia_cobradora, $valor_pago, $tarifa_custas, $valor_abatimento, $valor_desconto, $valor_mora, $baixado, $Parcela_id, $data_da_baixa, $baixaFilial_id, $baixaCliente_id) {
        $baixa = new Baixa;
        $baixa->data_da_ocorrencia = $data_da_ocorrencia;
        $baixa->codigo_banco_cobrador = $codigo_banco_cobrador;
        $baixa->agencia_cobradora = $agencia_cobradora;
        $baixa->valor_pago = $valor_pago;
        $baixa->tarifa_custas = $tarifa_custas;
        $baixa->valor_abatimento = $valor_abatimento;
        $baixa->valor_desconto = $valor_desconto;
        $baixa->valor_mora = $valor_mora;
        $baixa->baixado = $baixado;
        $baixa->Parcela_id = $Parcela_id;
        $baixa->data_da_baixa = $data_da_baixa;
        $baixa->Filial_id = $baixaFilial_id;
        $baixa->Cliente_id = $baixaCliente_id;
        $baixa->save();
    }

    /**
     * Registra as baixas, mas não necessariamente realiza a baixa, dos títulos importados
     * no arquivo retorno
     *
     * Uso típico:
     * - Usário do tipo financeiro, no menu Cnab > Importar Retorno, faz upload de um arquivo retorno
     * @param string $arquivo - endereço do arquivo já importado (Cnab400 Retorno)
     * @return array com títulos baixados e/ou registrados
     *
     */
    public function registrarBaixa($arquivo, $forcar_importacao, $codigo_banco, $forcar_importacao_obs) {
        ini_set('max_execution_time', 1600000);

        $arrReturn = array(
            'registrados' => 0,
            'hasErrors' => false,
            'pagNaoReg' => 0,
            'liquidados' => 0,
            'naoEncontrados' => 0,
            'comErro' => 0,
            'fileName' => $arquivo['name']
        );

        $fi = false;

        $_UPCONFIG['dir'] = '/var/www/nordeste/retornos_import/';
        $_UPCONFIG['finalFile'] = $_UPCONFIG['dir'] . $arquivo['name'];

        if (move_uploaded_file($arquivo['tmp_name'], $_UPCONFIG['finalFile'])) {
            $oldUmask = umask(0);
            chmod($_UPCONFIG['finalFile'], 0755);
            umask($oldUmask);
            $arquivoRetorno = file($_UPCONFIG['finalFile']);

            if (substr($arquivoRetorno[0], 76, 3) === $codigo_banco) {
                if ($forcar_importacao == '1') {
                    $fi = true;
                } else {
                    $fi = ArquivoCnab::validarImportacao(substr($arquivoRetorno[0], 94, 6), 5);
                }

                if ($fi) {
                    $geracaoTxt = substr($arquivoRetorno[0], 94, 6);

                    $arquivoCnab = new ArquivoCnab;
                    $arquivoCnab->relative_path = $arquivo['name'];
                    $arquivoCnab->absolute_path = $_UPCONFIG['finalFile'];
                    $arquivoCnab->tipo = 2;
                    $arquivoCnab->data_importacao = date('Y-m-d');
                    $arquivoCnab->data_geracao = '20' . substr($geracaoTxt, 4, 2) . '-' . substr($geracaoTxt, 2, 2) . '-' . substr($geracaoTxt, 0, 2);
                    $arquivoCnab->Usuario_id = Yii::app()->session['usuario']->id;
                    $arquivoCnab->Filial_id = 19;
                    $arquivoCnab->Banco_id = 5;

                    if ($arquivoCnab->save()) {
                        if ($forcar_importacao_obs !== '0') {
                            Dialogo::model()->novo('Importação de Cnab - Observação', 'ArquivoCnab', $arquivoCnab->id, Yii::app()->session['usuario']->id, $forcar_importacao_obs);
                        }
                    }

                    $arrBaixasConfig = array();
                    $arrTitulos = array();
                    $util = new Util;
                    $campoCnabConfig = new CampoCnabConfig;

                    $arrOcorrencias = array();

                    for ($i = 1; $i < count($arquivoRetorno) - 1; $i++) {

                        $labelRangeGerado = substr($arquivoRetorno[$i], 62, 10);
                        $codOrigemPgto = substr($arquivoRetorno[$i], 35, 1);
                        $rangerGerado = RangesGerados::model()->find('label = "' . $labelRangeGerado . '"');
                        $origemPgto = OrigemDoPagamento::model()->find('codigo = "' . $codOrigemPgto . '"');
                        $ocorrencia = OcorrenciaRetornoCnab::model()->findByPk(intval(substr($arquivoRetorno[$i], 108, 2)));

                        if ($rangerGerado != NULL) {
                            $tiposLiquidacoes = array(6, 7, 15, 16, 31, 32, 33, 36, 38, 39);
                            $motivoRejeicao = MotivoRejeicaoTitulo::model()->findByPk(intval(substr($arquivoRetorno[$i], 301, 2)));

                            $retornoParcelaCnab = new RetornoParcelaCnab;
                            $retornoParcelaCnab->Ocorrencia_retorno_cnab_id = $ocorrencia->id;
                            $retornoParcelaCnab->nosso_numero = substr($arquivoRetorno[$i], 62, 11);

                            if ($motivoRejeicao != NULL) {
                                $retornoParcelaCnab->Motivo_rejeicao_titulo_id = $motivoRejeicao->id;
                            }

                            $retornoParcelaCnab->Range_gerado_id = $rangerGerado->id;
                            $retornoParcelaCnab->data_ocorrencia = '20' . substr(substr($arquivoRetorno[$i], 110, 6), 4, 2) . '-' . substr(substr($arquivoRetorno[$i], 110, 6), 2, 2) . '-' . substr(substr($arquivoRetorno[$i], 110, 6), 0, 2);


                            //Houve liquidação do título
                            if (in_array(intval(substr($arquivoRetorno[$i], 108, 2)), $tiposLiquidacoes) && $campoCnabConfig->cnabValorMonetarioToDouble(substr($arquivoRetorno[$i], 253, 13))) {

                                if (intval(substr($arquivoRetorno[$i], 108, 2)) == 38 || intval(substr($arquivoRetorno[$i], 108, 2)) == 39) {
                                    $arrReturn['pagNaoReg'] += 1;
                                } else {
                                    $arrReturn['liquidados'] += 1;
                                }

                                $retornoParcelaCnab->valor_pago = $campoCnabConfig->cnabValorMonetarioToDouble(substr($arquivoRetorno[$i], 253, 13));
                                $retornoParcelaCnab->juros_de_mora = $campoCnabConfig->cnabValorMonetarioToDouble(substr($arquivoRetorno[$i], 266, 13));
                                $retornoParcelaCnab->origem_do_pagamento_id = $origemPgto->id;

                                if ($rangerGerado->parcela->valor_atual < $rangerGerado->parcela->valor) {

                                    $rangerGerado->parcela->valor_atual = $campoCnabConfig->cnabValorMonetarioToDouble(substr($arquivoRetorno[$i], 253, 13));

                                    if ($rangerGerado->parcela->update()) {
                                        $baixa = new Baixa;
                                        $baixa->data_da_ocorrencia = $retornoParcelaCnab->data_ocorrencia;
                                        $baixa->codigo_banco_cobrador = substr($arquivoRetorno[$i], 165, 3);
                                        $baixa->agencia_cobradora = substr($arquivoRetorno[$i], 168, 5);
                                        $baixa->valor_pago = $retornoParcelaCnab->valor_pago;
                                        $baixa->tarifa_custas = substr($arquivoRetorno[$i], 175, 13);
                                        $baixa->valor_abatimento = substr($arquivoRetorno[$i], 227, 13);
                                        $baixa->valor_desconto = substr($arquivoRetorno[$i], 240, 13);
                                        $baixa->valor_mora = substr($arquivoRetorno[$i], 266, 13);
                                        $baixa->baixado = 1;
                                        $baixa->Parcela_id = $rangerGerado->Parcela_id;
                                        $baixa->data_da_baixa = date('Y-m-d');

                                        if ($rangerGerado->parcela->titulo->proposta != NULL) {
                                            $baixa->Filial_id = $rangerGerado->parcela->titulo->proposta->analiseDeCredito->Filial_id;
                                            $baixa->Cliente_id = $rangerGerado->parcela->titulo->proposta->analiseDeCredito->Cliente_id;
                                        } else if ($rangerGerado->parcela->titulo->vendaW != NULL) {
                                            $baixa->Filial_id = $rangerGerado->parcela->titulo->vendaW->Filial_id;
                                            $baixa->Cliente_id = $rangerGerado->parcela->titulo->vendaW->Cliente_id;
                                        } else {
                                            exit();
                                        }

                                        $baixa->save();
                                    }
                                }
                            } else if (intval(substr($arquivoRetorno[$i], 108, 2)) == 2) {
                                $arrReturn['registrados'] += 1;
                            }


                            $retornoParcelaCnab->data_importacao = date('Y-m-d');
                            $retornoParcelaCnab->save();
                        }
                    }
                } else {
                    $oldUmask = umask(0);
                    chmod($_UPCONFIG['finalFile'], 0755);
                    umask($oldUmask);
                    unlink($_UPCONFIG['finalFile']);
                    $arrReturn['hasErrors'] = true;
                    $arrReturn['msgs'][] = 'O arquivo anterior não foi importado. Para prosseguir, force a importação.';
                }
            } else {
                $arrReturn['hasErrors'] = true;
                $arrReturn['msgs'][] = 'O arquivo enviado não corresponde ao banco selecionado.';
            }
        } else {
            $arrReturn['hasErrors'] = true;
            $arrReturn['msgs'][] = 'Não foi possível importar o arquivo. Contate o suporte';
        }

        return $arrReturn;
    }

    public function registrarBaixaBradesco($arquivo, $forcar_importacao, $codigo_banco, $forcar_importacao_obs) {
        ini_set('max_execution_time', 1600000);

        $arrReturn = array(
            'registrados' => 0,
            'hasErrors' => false,
            'pagNaoReg' => 0,
            'liquidados' => 0,
            'naoEncontrados' => 0,
            'comErro' => 0,
            'fileName' => $arquivo['name'],
            'debug' => [],
        );

        $fi = false;

        $_UPCONFIG['dir'] = '/var/www/nordeste/retornos_import/bradesco/';
        $_UPCONFIG['finalFile'] = $_UPCONFIG['dir'] . $arquivo['name'];

        if (move_uploaded_file($arquivo['tmp_name'], $_UPCONFIG['finalFile'])) {
            $oldUmask = umask(0);
            chmod($_UPCONFIG['finalFile'], 0755);
            umask($oldUmask);
            $arquivoRetorno = file($_UPCONFIG['finalFile']);

            if (substr($arquivoRetorno[0], 76, 3) === $codigo_banco) {
                if ($forcar_importacao == '1') {
                    $fi = true;
                } else {
                    $fi = ArquivoCnab::validarImportacao(substr($arquivoRetorno[0], 94, 6), 3);
                }

                if ($fi) {
                    $geracaoTxt = substr($arquivoRetorno[0], 94, 6);

                    $arquivoCnab = new ArquivoCnab;
                    $arquivoCnab->relative_path = $arquivo['name'];
                    $arquivoCnab->absolute_path = $_UPCONFIG['finalFile'];
                    $arquivoCnab->tipo = 2;
                    $arquivoCnab->data_importacao = date('Y-m-d');
                    $arquivoCnab->data_geracao = '20' . substr($geracaoTxt, 4, 2) . '-' . substr($geracaoTxt, 2, 2) . '-' . substr($geracaoTxt, 0, 2);
                    $arquivoCnab->Usuario_id = Yii::app()->session['usuario']->id;
                    $arquivoCnab->Filial_id = 19;
                    $arquivoCnab->Banco_id = 3;

                    if ($arquivoCnab->save()) {
                        if ($forcar_importacao_obs !== '0') {
                            Dialogo::model()->novo('Importação de Cnab - Observação', 'ArquivoCnab', $arquivoCnab->id, Yii::app()->session['usuario']->id, $forcar_importacao_obs);
                        }
                    }

                    $arrBaixasConfig = array();
                    $arrTitulos = array();
                    $util = new Util;
                    $campoCnabConfig = new CampoCnabConfig;

                    $arrOcorrencias = array();

                    for ($i = 1; $i < count($arquivoRetorno) - 1; $i++) {
                        $labelRangeGerado = intval(substr($arquivoRetorno[$i], 70, 11));
                        $rangerGerado = RangesGerados::model()->find('label = "' . $labelRangeGerado . '"');
                        $ocorrencia = OcorrenciaRetornoCnab::model()->findByPk(intval(substr($arquivoRetorno[$i], 108, 2)));

                        if ($rangerGerado != NULL) {
                            $tiposLiquidacoes = array(6, 10, 15, 16, 17);
                            $motivoRejeicao = MotivoRejeicaoTitulo::model()->findByPk(intval(substr($arquivoRetorno[$i], 301, 2)));

                            $retornoParcelaCnab = new RetornoParcelaCnab;
                            $retornoParcelaCnab->Ocorrencia_retorno_cnab_id = $ocorrencia->id;
                            $retornoParcelaCnab->nosso_numero = substr($arquivoRetorno[$i], 62, 11);

                            if ($motivoRejeicao != NULL) {
                                $retornoParcelaCnab->Motivo_rejeicao_titulo_id = $motivoRejeicao->id;
                            }

                            $retornoParcelaCnab->Range_gerado_id = $rangerGerado->id;
                            $retornoParcelaCnab->data_ocorrencia = '20' . substr(substr($arquivoRetorno[$i], 110, 6), 4, 2) . '-' . substr(substr($arquivoRetorno[$i], 110, 6), 2, 2) . '-' . substr(substr($arquivoRetorno[$i], 110, 6), 0, 2);


                            //Houve liquidação do título
                            //if( in_array( intval( substr( $arquivoRetorno[$i], 108, 2 ) ), $tiposLiquidacoes ) && $campoCnabConfig->cnabValorMonetarioToDouble( substr( $arquivoRetorno[$i], 253, 13 ) ) )
                            if (in_array(intval(substr($arquivoRetorno[$i], 108, 2)), $tiposLiquidacoes)) {
                                $retornoParcelaCnab->valor_pago = $campoCnabConfig->cnabValorMonetarioToDouble(substr($arquivoRetorno[$i], 253, 13));
                                $retornoParcelaCnab->juros_de_mora = $campoCnabConfig->cnabValorMonetarioToDouble(substr($arquivoRetorno[$i], 266, 13));

                                if (isset($rangerGerado->parcela)) {
                                  if ($rangerGerado->parcela->valor_atual < $rangerGerado->parcela->valor) {

                                      $rangerGerado->parcela->valor_atual = $campoCnabConfig->cnabValorMonetarioToDouble(substr($arquivoRetorno[$i], 253, 13));

                                      if ($rangerGerado->parcela->valor_atual > 0) {

                                          //Verificar se é uma parcela relacionada a um recebimento interno
                                          if( $rangerGerado->parcela->titulo->recebimentoInterno != Null ){
                                            $rangerGerado->parcela->titulo->recebimentoInterno->StatusRI_id = 2;
                                            $rangerGerado->parcela->titulo->recebimentoInterno->update();
                                          }


                                          $rangerGerado->parcela->update();

                                          $baixa = new Baixa;
                                          $baixa->data_da_ocorrencia = $retornoParcelaCnab->data_ocorrencia;
                                          $baixa->codigo_banco_cobrador = substr($arquivoRetorno[$i], 165, 3);
                                          $baixa->agencia_cobradora = substr($arquivoRetorno[$i], 168, 5);
                                          $baixa->valor_pago = $retornoParcelaCnab->valor_pago;
                                          $baixa->tarifa_custas = substr($arquivoRetorno[$i], 175, 13);
                                          $baixa->valor_abatimento = substr($arquivoRetorno[$i], 227, 13);
                                          $baixa->valor_desconto = substr($arquivoRetorno[$i], 240, 13);
                                          $baixa->valor_mora = substr($arquivoRetorno[$i], 266, 13);
                                          $baixa->baixado = 1;
                                          $baixa->Parcela_id = $rangerGerado->Parcela_id;
                                          $baixa->data_da_baixa = date('Y-m-d');

                                          if( $rangerGerado->parcela->titulo->proposta != Null ){
                                            $baixa->Filial_id = $rangerGerado->parcela->titulo->proposta->analiseDeCredito->Filial_id;
                                            $baixa->Cliente_id = $rangerGerado->parcela->titulo->proposta->analiseDeCredito->Cliente_id;
                                          }
                                          else{
                                            $baixa->Filial_id = 188;
                                          }

                                          if ($baixa->save()) {
                                              $arrReturn['liquidados'] += 1;
                                              $arrReturn['debug'][] = $rangerGerado->label;
                                          }
                                      }
                                  }
                                }
                            } else if (intval(substr($arquivoRetorno[$i], 108, 2)) == 2) {
                                $arrReturn['registrados'] += 1;
                            }



                            $retornoParcelaCnab->data_importacao = date('Y-m-d');
                            $retornoParcelaCnab->save();
                        } else {
                            $retornoParcelaCnab = new RetornoParcelaCnab;
                            $retornoParcelaCnab->Ocorrencia_retorno_cnab_id = 75;
                            $retornoParcelaCnab->nosso_numero = substr($arquivoRetorno[$i], 62, 11);
                            $retornoParcelaCnab->data_importacao = date('Y-m-d');
                            $retornoParcelaCnab->save();
                            $arrReturn['naoEncontrados'] += 1;
                        }
                    }
                } else {
                    $oldUmask = umask(0);
                    chmod($_UPCONFIG['finalFile'], 0755);
                    umask($oldUmask);
                    unlink($_UPCONFIG['finalFile']);
                    $arrReturn['hasErrors'] = true;
                    $arrReturn['msgs'][] = 'O arquivo anterior não foi importado.';
                }
            } else {
                $arrReturn['hasErrors'] = true;
                $arrReturn['msgs'][] = 'O arquivo enviado não corresponde ao banco selecionado.';
            }
        } else {
            $arrReturn['hasErrors'] = true;
            $arrReturn['msgs'][] = 'Não foi possível importar o arquivo. Contate o suporte';
        }

        return $arrReturn;
    }

}
