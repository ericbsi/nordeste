<?php

class Relatorios {

    public function getTotaisClientesCidades($draw, $cidades = null) {

        $util = new Util;
        $total = 0;
        $totalAtivos = 0;
        $totalInativos = 0;
        $query = new Query;
        $rows = [];
        $cidadesUF = "";

//        var_dump($cidades);

        if (is_array($cidades)) {
            $cidadesUF = join(',', $cidades);
        }

        $strQuery = $query->getQueryClientesCidades($cidadesUF);
        $resultado = Yii::app()->db->createCommand($strQuery)->queryAll();

        foreach ($resultado as $r) {
            $rows[] = array
                (
                'cidade' => trim($r['cidade']) . '/' . $r['uf'],
                'ativos' => (int) $r['ativos'],
                'inativos' => (int) $r['inativos'],
                'total' => (int) $r['ativos'] + $r['inativos'],
            );


            $totalAtivos += ((int) $r['ativos'] );
            $totalInativos += ((int) $r['inativos'] );
            $total += ((int) $r['ativos'] + $r['inativos'] );
        }

        return array(
            "draw" => $draw,
            "recordsTotal" => count($rows),
            "recordsFiltered" => count($rows),
            "data" => $rows,
            "retornoDetalhado" => array
                (
                "totalAtivos" => $totalAtivos,
                "totalInativos" => $totalInativos,
                "totalTotal" => $total,
            )
        );
    }

    public function getReceberVencimento($dataDe, $dataAte, $draw, $parceiros = null) {

        $util = new Util;
        $totalProducao = 0;
        $query = new Query;
        $rows = [];
        $filiais = "";

        if (is_array($parceiros)) {

            for ($i = 0; $i < sizeof($parceiros); $i++) {

                if (strlen($parceiros[$i]) > 0) {
                    $filiais .= $parceiros[$i];

                    if ($i < (sizeof($parceiros) - 1)) {
                        $filiais .= ',';
                    }
                }
            }
        }

        $strQuery = $query->getQueryReceberVencimento($dataDe, $dataAte, $filiais);
        $resultado = Yii::app()->db->createCommand($strQuery)->queryAll();

        foreach ($resultado as $r) {
            $rows[] = array
                (
                'parceiro' => $r['NOME'] . ' - ' . $r['CIDADE'] . '/' . $r['UF'],
                'valorTotal' => number_format($r['VALOR'], 2, ',', '.'),
            );

            $totalProducao += $r['VALOR'];
        }

        return array(
            "draw" => $draw,
            "recordsTotal" => count($rows),
            "recordsFiltered" => count($rows),
            "data" => $rows,
            "retornoDetalhado" => array
                (
                "totalProducao" => number_format($totalProducao, 2, ',', '.'),
            )
        );
    }

    public function getJurosAtraso($dataDe, $dataAte, $draw, $parceiros = null) {

        $util = new Util;
        $totalQuitado = 0;
        $totalRecebido = 0;
        $totalJuros = 0;
        $query = new Query;
        $rows = [];
        $filiais = "";

        if (is_array($parceiros)) {

            for ($i = 0; $i < sizeof($parceiros); $i++) {

                if (strlen($parceiros[$i]) > 0) {
                    $filiais .= $parceiros[$i];

                    if ($i < (sizeof($parceiros) - 1)) {
                        $filiais .= ',';
                    }
                }
            }
        }

        $strQuery = $query->getQueryJurosAtraso($dataDe, $dataAte, $filiais);
        $resultado = Yii::app()->db->createCommand($strQuery)->queryAll();

        foreach ($resultado as $r) {
            $rows[] = array
                (
                'parceiro' => $r['NOME'] . ' - ' . $r['CIDADE'] . '/' . $r['UF'],
                'valorQuitado' => number_format($r['VALOR_QUITADO'], 2, ',', '.'),
                'valorRecebido' => number_format($r['VALOR_RECEBIDO'], 2, ',', '.'),
                'valorJuros' => number_format($r['VALOR_JUROS'], 2, ',', '.'),
            );

            $totalQuitado += $r['VALOR_QUITADO'];
            $totalRecebido += $r['VALOR_RECEBIDO'];
            $totalJuros += $r['VALOR_JUROS'];
        }

        return array(
            "draw" => $draw,
            "recordsTotal" => count($rows),
            "recordsFiltered" => count($rows),
            "data" => $rows,
            "retornoDetalhado" => array
                (
                "totalQuitado" => number_format($totalQuitado, 2, ',', '.'),
                "totalRecebido" => number_format($totalRecebido, 2, ',', '.'),
                "totalJuros" => number_format($totalJuros, 2, ',', '.'),
            )
        );
    }

    public function reportProducaoOmni($draw, $start, $dataDe, $dataAte, $filiais) {

        $util = new Util;
        $rows = array();
        $criteria = new CDbCriteria;
        $countValFin = 0;

        if (!empty($dataDe) && !empty($dataAte)) {
            $criteria->addBetweenCondition('t.data_cadastro', $util->view_date_to_bd($dataDe) . ' 00:00:00', $util->view_date_to_bd($dataAte) . ' 23:59:59', 'AND');
        }

        $criteria->with = array('analiseDeCredito' => array(
                'alias' => 'ac'
        ));

        if (!empty($filiais)) {
            $idsFiliais = array();

            for ($i = 0; $i < count($filiais); $i ++) {
                $idsFiliais[] = $filiais[$i];
            }

            $criteria->addInCondition('ac.Filial_id', $idsFiliais, ' AND ');
        }

        $criteria->addInCondition('t.habilitado', array(1), ' AND ');
        $criteria->addInCondition('t.Titulos_gerados', array(1), ' AND ');
        $criteria->addInCondition('t.Status_Proposta_id', array(2, 7), ' AND ');
        $criteria->order = 't.data_cadastro DESC';

        $resultado = Proposta::model()->findAll($criteria);


        if (count($resultado) > 0) {
            foreach ($resultado as $proposta) {
                $countValFin += $proposta->getValorFinanciado();
            }

            /*$criteria->offset = $start;
            $criteria->limit = 10;*/

            foreach (Proposta::model()->findAll($criteria) as $r) {

                $row = array(
                    "emissao" => $util->bd_date_to_view(substr($r->data_cadastro, 0, 10)),
                    "codigo" => $r->codigo,
                    "cliente" => strtoupper($r->analiseDeCredito->cliente->pessoa->nome),
                    "val_financiado" => number_format($r->getValorFinanciado(), 2, ',', '.'),
                    "nomeLoja" => "MARÉ MANSA - LAGOA NOVA",
                );

                $rows[] = $row;
            }
        }

        return array(
            'draw' => $draw,
            'recordsTotal' => count($rows),
            'recordsFiltered' => count($resultado),
            'data' => $rows,
            "customReturn" => array(
                'totalFin' => "R$ " . number_format($countValFin, 2, ',', '.'),
            ),
            'criteria' => $criteria
        );
    }
    
    public function getProducaoEmprestimo($idFilial, $dataDe, $dataAte)
    {
        
        $criteria   = new CDbCriteria   ;
        $util       = new Util          ;

        if (!empty($dataDe) && !empty($dataAte)) 
        {
            $criteria->addBetweenCondition('t.data_cadastro', $util->view_date_to_bd($dataDe) . ' 00:00:00', $util->view_date_to_bd($dataAte) . ' 23:59:59', 'AND');
        }

        $criteria->with = array (
                                    'emprestimo'    => array    (
                                                                    'alias' => 'e'
                                                                )
                                );

        $criteria->addInCondition('e.Filial_id'             , array($idFilial   ), ' AND ');
        $criteria->addInCondition('e.habilitado'            , array(1           ), ' AND ');
        $criteria->addInCondition('t.habilitado'            , array(1           ), ' AND ');
        $criteria->addInCondition('t.Titulos_gerados'       , array(1           ), ' AND ');
        $criteria->addInCondition('t.Status_Proposta_id'    , array(2, 7        ), ' AND ');
        
        $criteria->order = 't.data_cadastro DESC';

        return Proposta::model()->findAll($criteria);
    }

}

class Query {

    public function getQueryClientesCidades($cidades = null) {
        $query = "SELECT CIDADE AS cidade, UF AS uf, SUM(ATIVOS) AS ativos, SUM(INATIVOS) AS inativos "
                . "FROM( "
                . "SELECT  "
                . "  CASE "
                . "     WHEN E1.id IS NULL THEN TRIM(UPPER(E2.cidade)) ELSE TRIM(UPPER(E1.cidade)) "
                . "  END AS CIDADE, "
                . "	CASE "
                . "     WHEN E1.id IS NULL THEN TRIM(UPPER(E2.uf)) ELSE TRIM(UPPER(E1.uf)) "
                . "	END AS UF, "
                . "  COUNT(DISTINCT C.id	) AS ATIVOS, 0 AS INATIVOS "
                . "FROM                Cliente              AS C "
                . "INNER         JOIN  Pessoa               AS P     ON P.habilitado            AND "
                . "                                                     P.id =   C.Pessoa_id "
                . "INNER         JOIN  Pessoa_has_Endereco  AS PhE 	ON PhE.habilitado          AND "
                . "                                                     P.id = PhE.Pessoa_id "
                . "LEFT    OUTER JOIN  Endereco             AS E1 	ON E1.habilitado           AND "
                . "                                                     E1.id = PhE.Endereco_id AND "
                . "                                                     E1.Tipo_Endereco_id = 1 "
                . "LEFT    OUTER JOIN  Endereco             AS E2 	ON E2.habilitado           AND "
                . "                                                     E2.id = PhE.Endereco_id AND "
                . "                                                     E2.Tipo_Endereco_id = 2 AND "
                . "                                                     E1.id IS NULL "
                . "LEFT    OUTER JOIN  Analise_de_Credito   AS AC	ON AC.habilitado           AND "
                . "                                                     C.id	=  AC.Cliente_id	    "
                . "WHERE C.habilitado AND AC.id IS NOT NULL "
                . "GROUP BY "
                . "CASE WHEN E1.id IS NULL THEN TRIM(UPPER(E2.cidade)) ELSE TRIM(UPPER(E1.cidade)) END "
                . "UNION "
                . "SELECT  "
                . "  CASE "
                . "     WHEN E1.id IS NULL THEN TRIM(UPPER(E2.cidade)) ELSE TRIM(UPPER(E1.cidade)) "
                . "  END AS CIDADE, "
                . "  CASE "
                . "     WHEN E1.id IS NULL THEN TRIM(UPPER(E2.uf)) ELSE TRIM(UPPER(E1.uf)) "
                . "  END AS UF, "
                . "  0 AS ATIVOS, COUNT(DISTINCT C.id	) AS INATIVOS "
                . "FROM                Cliente              AS C "
                . "INNER         JOIN  Pessoa               AS P     ON P.habilitado            AND "
                . "                                                     P.id =   C.Pessoa_id "
                . "INNER         JOIN  Pessoa_has_Endereco  AS PhE 	ON PhE.habilitado          AND "
                . "                                                     P.id = PhE.Pessoa_id "
                . "LEFT    OUTER JOIN  Endereco             AS E1 	ON E1.habilitado           AND "
                . "                                                     E1.id = PhE.Endereco_id AND "
                . "                                                     E1.Tipo_Endereco_id = 1 "
                . "LEFT    OUTER JOIN  Endereco             AS E2    ON E2.habilitado           AND "
                . "                                                     E2.id = PhE.Endereco_id AND "
                . "                                                     E2.Tipo_Endereco_id = 2 AND "
                . "                                                     E1.id IS NULL "
                . "LEFT    OUTER JOIN  Analise_de_Credito   AS AC	ON AC.habilitado           AND "
                . "                                                     C.id	=  AC.Cliente_id "
                . "WHERE C.habilitado AND AC.id IS NULL "
                . "GROUP BY "
                . "CASE WHEN E1.id IS NULL THEN TRIM(UPPER(E2.cidade)) ELSE TRIM(UPPER(E1.cidade)) END "
                . ") "
                . "AS TEMP ";

        if ($cidades !== null && !empty($cidades)) {
//            var_dump($cidades);
            $query .= "WHERE UPPER(CONCAT(cidade,uf)) IN (" . $cidades . ") ";
        }

        $query .= "GROUP BY cidade, uf "
                . "ORDER BY 1, 2 ";
//        var_dump($query);

        return $query;
    }

    public function getQueryReceberVencimento($de = null, $ate = null, $filiais = null) {
        $query = "SELECT   F.cnpj        AS CNPJ     ,   F.nome_fantasia   AS NOME , "
                . "         E.cidade      AS CIDADE   ,   E.uf              AS UF   , "
                . "         SUM(Pa.Valor) AS VALOR                                                  "
                . "FROM                 Parcela             AS Pa               "
                . "INNER        JOIN	Titulo              AS T 	ON      T.habilitado AND    T.id    =    Pa.Titulo_id               "
                . "INNER        JOIN	Proposta            AS P 	ON      P.habilitado AND    P.id    =     T.Proposta_id             "
                . "INNER        JOIN	Analise_de_Credito  AS AC	ON     AC.habilitado AND   AC.id    =     P.Analise_de_Credito_id   "
                . "INNER        JOIN	Filial              AS F	ON      F.habilitado AND    F.id    =    AC.Filial_id               "
                . "INNER        JOIN	Filial_has_Endereco AS FhE	ON    FhE.habilitado AND    F.id    =   FhE.Filial_id               "
                . "INNER        JOIN	Endereco            AS E	ON	E.habilitado AND    E.id    =   FhE.Endereco_id             "
                . "LEFT OUTER 	JOIN	Baixa               AS B	ON	B.baixado    AND   Pa.id    =	  B.Parcela_id              "
                . "WHERE Pa.habilitado  AND B.id IS NULL ";

        if ($de !== null && !empty($de)) {
            $query .= "AND LEFT(Pa.vencimento,10) >= '" . $de . "' ";
        }

        if ($ate !== null && !empty($ate)) {
            $query .= "AND LEFT(Pa.vencimento,10) <= '" . $ate . "' ";
        }

        if ($filiais !== null && !empty($filiais)) {
            $query .= "AND F.id IN (" . $filiais . ") ";
        }

        $query .= "GROUP BY F.cnpj		, F.nome_fantasia	, E.cidade	, E.uf  "
                . "ORDER BY F.nome_fantasia	, E.cidade		, E.uf                  ";

//        var_dump($query);

        return $query;
    }

    public function getQueryJurosAtraso($de = null, $ate = null, $filiais = null) {
        $query = "SELECT   F.cnpj            AS CNPJ           , F.nome_fantasia   AS NOME             , "
                . "         E.cidade          AS CIDADE         , E.uf              AS UF               , "
                . "         SUM(B.valor_pago) AS VALOR_RECEBIDO , SUM(Pa.Valor)     AS VALOR_QUITADO    , "
                . "         SUM(B.valor_pago)                   - SUM(Pa.Valor)     AS VALOR_JUROS        "
                . "FROM                 Parcela             AS Pa               "
                . "INNER        JOIN	Titulo              AS T 	ON      T.habilitado AND    T.id    =    Pa.Titulo_id               "
                . "INNER        JOIN	Proposta            AS P 	ON      P.habilitado AND    P.id    =     T.Proposta_id             "
                . "INNER        JOIN	Analise_de_Credito  AS AC	ON     AC.habilitado AND   AC.id    =     P.Analise_de_Credito_id   "
                . "INNER        JOIN	Filial              AS F	ON      F.habilitado AND    F.id    =    AC.Filial_id               "
                . "INNER        JOIN	Filial_has_Endereco AS FhE	ON    FhE.habilitado AND    F.id    =   FhE.Filial_id               "
                . "INNER        JOIN	Endereco            AS E	ON	E.habilitado AND    E.id    =   FhE.Endereco_id             "
                . "INNER 	JOIN	Baixa               AS B	ON	B.baixado    AND   Pa.id    =	  B.Parcela_id              "
                . "WHERE Pa.habilitado   ";

        if ($de !== null && !empty($de)) {
            $query .= "AND LEFT(B.data_da_baixa,7) >= '" . $de . "' ";
        }

        if ($ate !== null && !empty($ate)) {
            $query .= "AND LEFT(B.data_da_baixa,7) <= '" . $ate . "' ";
        }

        if ($filiais !== null && !empty($filiais)) {
            $query .= "AND F.id IN (" . $filiais . ") ";
        }

        $query .= "GROUP BY    F.cnpj		, F.nome_fantasia	, E.cidade	, E.uf  "
                . "ORDER BY 	F.nome_fantasia	, E.cidade		, E.uf                  ";


        return $query;
    }

}
