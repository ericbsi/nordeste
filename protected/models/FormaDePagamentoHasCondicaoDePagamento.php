<?php

class FormaDePagamentoHasCondicaoDePagamento extends CActiveRecord {

    public function tableName() {
        return 'Forma_de_Pagamento_has_Condicao_de_Pagamento';
    }

    
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Forma_de_Pagamento_id, Condicao_de_Pagamento_id, qtdParcelasDe, qtdParcelasAte, carencia, intervalo, repasse, taxaAdministrativa, habilitado', 'required'),
            array('Forma_de_Pagamento_id, Condicao_de_Pagamento_id, qtdParcelasDe, qtdParcelasAte, carencia, intervalo, repasse, habilitado', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, Forma_de_Pagamento_id, Condicao_de_Pagamento_id, qtdParcelasDe, qtdParcelasAte, carencia, intervalo, repasse, taxaAdministrativa, habilitado', 'safe', 'on' => 'search'),
        );
    }

    
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'formaDePagamento'      => array(self::BELONGS_TO, 'FormaDePagamento'       , 'Forma_de_Pagamento_id'       ),
            'condicaoDePagamento'   => array(self::BELONGS_TO, 'CondicaoDePagamento'    , 'Condicao_de_Pagamento_id'    ),
        );
    }

    public function attributeLabels() {
        return array(
            'id'                        => 'ID'                     ,
            'Forma_de_Pagamento_id'     => 'Forma De Pagamento'     ,
            'Condicao_de_Pagamento_id'  => 'Condicao De Pagamento'  ,
            'qtdParcelasDe'             => 'Qtd Parcelas De'        ,
            'qtdParcelasAte'            => 'Qtd Parcelas Ate'       ,
            'carencia'                  => 'Carencia'               ,
            'intervalo'                 => 'Intervalo'              ,
            'repasse'                   => 'Repasse'                ,
            'taxaAdministrativa'        => 'Taxa Administrativa'    ,
            'habilitado'                => 'Habilitado'             ,
        );
    }
   
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id'                         , $this->id                         );
        $criteria->compare('Forma_de_Pagamento_id'      , $this->Forma_de_Pagamento_id      );
        $criteria->compare('Condicao_de_Pagamento_id'   , $this->Condicao_de_Pagamento_id   );
        $criteria->compare('qtdParcelasDe'              , $this->qtdParcelasDe              );
        $criteria->compare('qtdParcelasAte'             , $this->qtdParcelasAte             );
        $criteria->compare('carencia'                   , $this->carencia                   );
        $criteria->compare('intervalo'                  , $this->intervalo                  );
        $criteria->compare('repasse'                    , $this->repasse                    );
        $criteria->compare('taxaAdministrativa'         , $this->taxaAdministrativa         );
        $criteria->compare('habilitado'                 , $this->habilitado                 );

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchUnique($paramQuery){

        $retorno = [];

        $sql = "SELECT fpHasCp.id as id, concat(fp.sigla, ' (De ', fpHasCp.qtdParcelasDe, 'x até ', fpHasCp.qtdParcelasAte, 'x) Primeira em ', fpHasCp.carencia, ' dias ') as text "
                . "FROM       Forma_de_Pagamento_has_Condicao_de_Pagamento as fpHasCp "
                . "INNER JOIN Forma_de_Pagamento                           as fp      on fp.habilitado AND fpHasCp.Forma_de_Pagamento_id = fp.id "
                . "INNER JOIN Condicao_de_Pagamento                        as cp      on cp.habilitado AND fpHasCp.Condicao_de_Pagamento_id = cp.id "
                . "where '" . date('Y-m-d') . "' between cp.dataInicial and cp.dataFinal ";


        if ($paramQuery !== null && !empty($paramQuery)) {

            for ($i = 0; $i < sizeof($paramQuery); $i++) {

                $fpHasCP = FormaDePagamentoHasCondicaoDePagamento::model()->findByPk($paramQuery[$i]['formPGs']);

                $sql .= " AND (fpHasCp.id <> "
                        . $fpHasCP->id . " AND "
                        . "(" . $paramQuery[$i]['parcelas'] . " BETWEEN qtdParcelasDe AND qtdParcelasAte )) ";
            }
        }

        $sql .= " ORDER BY 2 ";

        $list = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($list as $fp){

            array_push($retorno, array(
                'id' => $fp['id'],
                'text' => $fp['text'],
                    //'text' => $fp->formaDePagamento->sigla . ' ' . $fp->qtdParcelasDe . ' até ' . $fp->qtdParcelasAte . 'X',
                )
            );
        }

        return $retorno;
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
