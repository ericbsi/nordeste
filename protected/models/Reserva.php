<?php

/**
 * This is the model class for table "Reserva".
 *
 * The followings are the available columns in table 'Reserva':
 * @property integer $id
 * @property string $data
 * @property string $data_cadastro
 * @property integer $habilitado
 * @property double $qtdReservada
 * @property double $qtdEfetivada
 * @property double $qtdRecusada
 * @property integer $Filial_id
 * @property string $codigoSisOrig
 * @property integer $StatusReserva_id
 *
 * The followings are the available model relations:
 * @property Filial $filial
 * @property StatusReserva $statusReserva
 */
class Reserva extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Reserva';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('data, data_cadastro, qtdReservada, qtdEfetivada, qtdRecusada, Filial_id, codigoSisOrig, StatusReserva_id', 'required'),
			array('habilitado, Filial_id, StatusReserva_id', 'numerical', 'integerOnly'=>true),
			array('qtdReservada, qtdEfetivada, qtdRecusada', 'numerical'),
			array('codigoSisOrig', 'length', 'max'=>200),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, data, data_cadastro, habilitado, qtdReservada, qtdEfetivada, qtdRecusada, Filial_id, codigoSisOrig, StatusReserva_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'filial' => array(self::BELONGS_TO, 'Filial', 'Filial_id'),
			'statusReserva' => array(self::BELONGS_TO, 'StatusReserva', 'StatusReserva_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'data' => 'Data',
			'data_cadastro' => 'Data Cadastro',
			'habilitado' => 'Habilitado',
			'qtdReservada' => 'Qtd Reservada',
			'qtdEfetivada' => 'Qtd Efetivada',
			'qtdRecusada' => 'Qtd Recusada',
			'Filial_id' => 'Filial',
			'codigoSisOrig' => 'Codigo Sis Orig',
			'StatusReserva_id' => 'Status Reserva',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('data',$this->data,true);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('qtdReservada',$this->qtdReservada);
		$criteria->compare('qtdEfetivada',$this->qtdEfetivada);
		$criteria->compare('qtdRecusada',$this->qtdRecusada);
		$criteria->compare('Filial_id',$this->Filial_id);
		$criteria->compare('codigoSisOrig',$this->codigoSisOrig,true);
		$criteria->compare('StatusReserva_id',$this->StatusReserva_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Reserva the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function listReservaEstoque($estoque)
        {
            
            $retorno = [];
            
            //$sql  = ;
            
            $command = Yii::app()->db->createCommand()
                        ->select("IfNull(sum(qtdReservada - (qtdEfetivada + qtdRecusada)),0) as QTD")
                        ->from("Reserva r")
                        ->where('habilitado AND codigoSisOrig = :codigoProd');
            
            foreach ($estoque as $e)
            {
                $command->bindParam('codigoProd',$e['CODIGO']);
                
//                var_dump($command);
                
                $qtdReservada = $command->queryRow();
                
                $qtdDisponivel = floatval($e['ESTOQUE']) - floatval($qtdReservada['QTD']);
                
//                echo '(' . $qtdReservada['QTD'] . ') ' . floatval($e['ESTOQUE']) . ' - '. floatval($qtdReservada['QTD']) . ' = ' .$qtdDisponivel . '<br>';
                
                if ($qtdDisponivel > 0)
                {
                    $retorno[] = [$e['CODIGO'], $e['NOME_COMERCIAL'], $e['ESTOQUE'], $qtdReservada['QTD'], $qtdDisponivel];
                }
            }
            
            return $retorno;
            
        }
        
        public function getNomeSisOrig($codSisOrig)
        {
            
            $retorno;
            
            $sql = "SELECT NOME_COMERCIAL " .
            "FROM PRODUTOS " .
            "WHERE BLOQUEADO = 0 AND CODIGO = '" . $codSisOrig . "' ";
            
            $db = new PDO("firebird:dbname=187.12.141.11/3050:C:\automacao\dados\MARE MANSA\LOJA 25\camaleao.gdb", "sysdba", "masterkey");
                
            $sth = $db->prepare($sql);

            $sth->execute();

            $retorno = $sth->fetch();
            
            return $retorno['NOME_COMERCIAL'];
            
        }
        
        public function listReservas()
        {
            $retorno = [];
            
            foreach (Reserva::model()->findAll() as $r)
            {
                
                $nomeFilial  = $r->Filial_id . ' ' . Filial::model()->findByPk($r->Filial_id)->nome_fantasia;
                
                $nomeProduto = $this->getNomeSisOrig($r->codigoSisOrig);
                
                $retorno[] = [$r->id,$nomeFilial, date('d/m/Y',  strtotime($r->data)),$r->codigoSisOrig,$nomeProduto,$r->qtdReservada,$r->qtdEfetivada,$r->qtdRecusada,$r->StatusReserva_id];
            }
            
            return $retorno;
            
        }


        public function grava()
        {
            
            $retorno;
            
            if ($this->save())
            {
                $retorno = ['message' => 'sucesso'];
            }else{
                $retorno = ['message' => var_dump($this->getErrors())];
            }
            
            return $retorno;
            
        }
}
