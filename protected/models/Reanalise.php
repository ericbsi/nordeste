<?php

class Reanalise extends CActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'Reanalise';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('data_cadastro, Proposta, Analista', 'required'),
            array('habilitado, Proposta, visivel, Analista', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, data_cadastro, habilitado, Proposta, visivel, Analista', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'proposta' => array(self::BELONGS_TO, 'Proposta', 'Proposta'),
            'analista' => array(self::BELONGS_TO, 'Usuario', 'Analista'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'data_cadastro' => 'Data Cadastro',
            'habilitado' => 'Habilitado',
            'Proposta' => 'Proposta',
            'visivel' => 'Visivel',
            'Analista' => 'Analista',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('data_cadastro', $this->data_cadastro, true);
        $criteria->compare('habilitado', $this->habilitado);
        $criteria->compare('Proposta', $this->Proposta);
        $criteria->compare('visivel', $this->visivel);
        $criteria->compare('Analista', $this->Analista);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function novo($PropostaId)
    {

        $proposta = Proposta::model()->findByPk($PropostaId);

        if ($proposta->reanalise == NULL)
        {
            $reanalise = new Reanalise;
            $reanalise->Proposta = $PropostaId;
            $reanalise->Analista = Yii::app()->session['usuario']->id;
            $reanalise->habilitado = 1;
            $reanalise->visivel = 1;
            $reanalise->data_cadastro = date('Y-m-d H:i:s');

            if ($reanalise->save())
            {
                return $reanalise;
            } else
            {
                return NULL;
            }
        } else
        {
            $proposta->reanalise->visivel = 1;

            if ($proposta->reanalise->update())
            {
                return $proposta->reanalise;
            } else
            {
                return NULL;
            }
        }
    }

    public function esconder($PropostaId)
    {
        $proposta = Proposta::model()->findByPk($PropostaId);
        $proposta->reanalise->visivel = 0;

        if ($proposta->reanalise->update())
        {
            return $proposta->reanalise;
        } else
        {
            return NULL;
        }
    }

    public function getQtdMsgNaoLida($proposta, $usuario)
    {
        $query = "";

        $query .= "SELECT count(*) AS QTD ";
        $query .= "FROM Mensagem as M ";
        $query .= "INNER JOIN Dialogo AS D ON D.habilitado AND M.Dialogo_id = D.id ";
        $query .= "Where M.habilitado AND Entidade_relacionamento = 'Proposta' AND Entidade_Relacionamento_id = " . $proposta->id . " ";
        $query .= "		AND Usuario_id <> " . $usuario->id . " AND NOT lida ";

        $queryReturn = Yii::app()->db->createCommand($query)->queryRow();

        return $queryReturn['QTD'];
    }

    public function listAnalistaPropostas($draw, $start)
    {
        $criteria = new CDbCriteria;
        $criteria->with = [ 'proposta' => [ 'alias' => 'p']];
        $criteria->addInCondition('p.Status_Proposta_id', [1], ' AND ');
        $criteria->addInCondition('t.habilitado', [1], ' AND ');
        $criteria->addInCondition('t.visivel', [1], ' AND ');

        $reanalises = Reanalise::model()->findAll($criteria);
        $rows = [];
        $util = new Util;

        foreach ($reanalises as $reanalise)
        {
            $btnDetails = '';

            $btnDetails .= '<form method="post" action="' . Yii::app()->getBaseUrl(true) . '/reanalise/analise/">';
            $btnDetails .= '	<input type="hidden" name="id" value="' . $reanalise->proposta->id . '">';
            $btnDetails .= '	<button style="padding:6px;font-size:8px;" type="submit" class="btn btn-primary"><i class="clip-search"></i></button>';
            $btnDetails .= '</form>';

            $btnPC = "";

            $msgNaoLidas = $this->getQtdMsgNaoLida($reanalise->proposta, Yii::app()->session['usuario']);

            if ($msgNaoLidas > 0)
            {
                $btnAtualizacoes = '<span class="label label-danger buttonpulsate">Atualizações</span>';
            } else
            {
                $btnAtualizacoes = '';
            }

            //if ($reanalise->proposta->analiseDeCredito->filial->filialHasPoliticaCredito->politicaCredito !== null)
            if (isset($reanalise->proposta->analiseDeCredito->filial->filialHasPoliticaCredito->politicaCredito))
            {
                $btnPC .= '<a style="font-size:93%!important; font-weight: bold!important; background:' . $reanalise->proposta->analiseDeCredito->filial->filialHasPoliticaCredito->politicaCredito->cor . '" class="label" href="#">   <span class="btn label">' . strtoupper($reanalise->proposta->analiseDeCredito->filial->filialHasPoliticaCredito->politicaCredito->descricao) . '</span></a>';
            } else
            {
                $btnPC .= '<a style="font-size:93%!important;" class="label" href="#"><span class="btn label label-default">SEM POLÍTICA</span></a>';
            }

            $rows[] = [
                'btn-more' => $btnDetails,
                'codigo' => $reanalise->proposta->codigo,
                'cliente' => strtoupper($reanalise->proposta->analiseDeCredito->cliente->pessoa->nome),
                'filial' => strtoupper($reanalise->proposta->analiseDeCredito->filial->getConcat()),
                'politicaCredito' => $btnPC,
                'valor' => number_format($reanalise->proposta->getValorFinanciado(), 2, ',', '.'),
                'qtd_parcelas' => $reanalise->proposta->qtd_parcelas . 'x ' . "R$ " . number_format($reanalise->proposta->getValorParcela(), 2, ',', '.'),
                'btnAtualizacoes' => $btnAtualizacoes,
                'btn' => '<span class="' . $reanalise->proposta->statusProposta->cssClass . '">' . $reanalise->proposta->statusProposta->status . '</span>',
                'data_cadastro' => $reanalise->proposta->data_cadastro_br,
            ];
        }

        return (array(
            "draw" => $draw,
            "recordsTotal" => count($rows),
            "recordsFiltered" => count($rows),
            "data" => $rows,
        ));
    }

    public function propostasNegadas($draw, $start)
    {
        $util = new Util;
        $rows = [];

        $criteria = new CDbCriteria;
        $criteria->with = [ 'analiseDeCredito' => [ 'alias' => 'ac']];
        $criteria->addInCondition('ac.Filial_id', [98, 89, 81, 45, 35, 99, 108, 91, 39, 103, 96, 44, 83, 87, 92, 97, 36, 48, 95, 100, 40, 102, 47, 37, 82, 43, 86, 101, 85, 41, 90, 93, 42, 88, 46, 84, 38, 106, 107, 94, 105, 104], ' AND ');
        $criteria->addInCondition('t.Status_Proposta_id', [3, 8, 10], ' AND ');

        $countTotal = count(Proposta::model()->findAll($criteria));

        $criteria->offset = $start;
        $criteria->limit = 10;
        $criteria->order = 't.data_cadastro DESC';

        foreach (Proposta::model()->findAll($criteria) as $proposta)
        {
            $htmlDialogo = '';
            $htmlAnexos = '';

            $dialogo = $proposta->getDialogo();
            $cadastroCliente = $proposta->analiseDeCredito->cliente->getCadastro();

            if ($cadastroCliente != NULL)
            {
                $anexos = Anexo::model()->findAll('entidade_relacionamento = "cadastro" AND id_entidade_relacionamento = ' . $cadastroCliente->id);

                if ($anexos != NULL)
                {
                    foreach ($anexos as $anexo)
                    {
                        $htmlAnexos .= '<tr>';
                        $htmlAnexos .= '    <td><a target="_blank" href="' . $anexo->relative_path . '">' . strtoupper($anexo->descricao) . '</a></td>';
                        $htmlAnexos .= '</tr>';
                    }
                }
            }

            if ($dialogo != NULL)
            {
                if ($dialogo->getMensagens() != NULL)
                {
                    foreach ($dialogo->getMensagens() as $mensagem)
                    {
                        $htmlDialogo .= '<tr>';
                        $htmlDialogo .= '    <td> De: <strong>' . strtoupper($mensagem->usuario->nome_utilizador) . '</strong>: ' . strtoupper($mensagem->conteudo) . '</td>';
                        $htmlDialogo .= '</tr>';
                    }
                }
            }

            $motivo = 'SEM MOTIVO';

            if ($proposta->negacaoDeCredito != NULL)
            {
                $motivo = strtoupper($proposta->negacaoDeCredito->observacao . ' - ' . $proposta->negacaoDeCredito->motivoDeNegacao->descricao);
            }

            $rows[] = [
                'codigo' => $proposta->codigo,
                'cliente' => strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome),
                'dprofissa' => '<a data-dados="' . $proposta->analiseDeCredito->cliente->pessoa->getDadosProfissionais()->id . '" style="padding:2px 5px; font-size:9px;" class="btn btn-blue exibir-dados-profissionais" href="#"><i class="fa fa-search"></i></a>',
                'filial' => strtoupper($proposta->analiseDeCredito->filial->getConcat()),
                'valor' => number_format($proposta->getValorFinanciado(), 2, ',', '.'),
                'qtd_parcelas' => $proposta->qtd_parcelas . 'x ' . "R$ " . number_format($proposta->getValorParcela(), 2, ',', '.'),
                'btn' => '<span class="' . $proposta->statusProposta->cssClass . '">' . $proposta->statusProposta->status . '</span>',
                'data_cadastro' => $util->bd_date_to_view(substr($proposta->data_cadastro, 0, 10)),
                'moreDetails' => [
                    '<table class="table table-striped table-bordered table-hover table-full-width">'
                    . '<thead>'
                    . '<tr>'
                    . '<th>Motivo da Negação</th>'
                    . '</tr>'
                    . '</thead>'
                    . '<tbody><tr><td>' . $motivo . '</tr></td></tbody>'
                    . '</table>'
                    . '<table class="table table-striped table-bordered table-hover table-full-width">'
                    . '<thead>'
                    . '<tr>'
                    . '<th>Dialogo</th>'
                    . '</tr>'
                    . '</thead>'
                    . '<tbody>' . $htmlDialogo . '</tbody>'
                    . '</table>'
                    . '<table class="table table-striped table-bordered table-hover table-full-width">'
                    . '<thead>'
                    . '<tr>'
                    . '<th>Documentação do Cliente</th>'
                    . '</tr>'
                    . '</thead>'
                    . '<tbody>' . $htmlAnexos . '</tbody>'
                    . '</table>'
                ]
            ];
        }

        return (array(
            "draw" => $draw,
            "recordsTotal" => count($rows),
            "recordsFiltered" => $countTotal,
            "data" => $rows,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Reanalise the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
