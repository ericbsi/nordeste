<?php

class CompraHasFormaDePagamentoHasCondicaoDePagamento extends CActiveRecord
{

    public function novo($Compra_id, $FPCP, $Proposta, $qtdParcelas, $idNatureza = 2)
    {
        $arrReturn                                                              = array (
                                                                                            'title'     => "Sucesso"                        ,
                                                                                            'text'      => "Títulos gerados com sucesso"    ,
                                                                                            'type'      => "success"                        ,
                                                                                            'hasErrors' => false
                                                                                        );

        $CFDePgtoCDePgto                                                        = new CompraHasFormaDePagamentoHasCondicaoDePagamento;
        $CFDePgtoCDePgto->Compra_id                                             = $Compra_id;
        $CFDePgtoCDePgto->Forma_de_Pagamento_has_Condicao_de_Pagamento_id       = $FPCP;
        $CFDePgtoCDePgto->valor                                                 = $Proposta->valorRepasse();
        $CFDePgtoCDePgto->qtdParcelas                                           = $qtdParcelas;

        if( !$CFDePgtoCDePgto->save() )
        { 
            
            ob_start();
            var_dump($CFDePgtoCDePgto->getErrors());
            $resultado = ob_get_clean();
            
            $arrReturn['title'      ] = "Erro"                                                  ;
            $arrReturn['text'       ] = "Erro ao gerar títulos. CompraHasFPHasCP: $resultado"   ;
            $arrReturn['type'       ] = "error"                                                 ;
            $arrReturn['hasErrors'  ] = true                                                    ;
        }

        else
        {
            
            if($Proposta->getEmprestimo() !== null && $Proposta->Financeira_id == 6)
            {
                $idNatureza = 32;
            }
            
            $titulo = Titulo::model()->find("habilitado AND Proposta_id = $Proposta->id AND NaturezaTitulo_id = $idNatureza ");
            
            if($titulo == null)
            {
                $arrReturn = Titulo::model()->gerarTituloAPagar( $idNatureza,$CFDePgtoCDePgto->id, $qtdParcelas, $Proposta->valorRepasse(), $Proposta );
            }
            else
            {
                $titulo->Compra_has_FP_has_CP_FP_has_CP_id = $CFDePgtoCDePgto->id;
                
                if(!$titulo->update())
                {
            
                    ob_start();
                    var_dump($titulo->getErrors());
                    $resultado = ob_get_clean();

                    $arrReturn['title'      ] = "Erro"                                          ;
                    $arrReturn['text'       ] = "Erro ao atualizar título. Titulo: $resultado"  ;
                    $arrReturn['type'       ] = "error"                                         ;
                    $arrReturn['hasErrors'  ] = true                                            ;
            
                }
                
            }
        }

        return $arrReturn;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'Compra_has_Forma_de_Pagamento_has_Condicao_de_Pagamento';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        
        return array(
            array('Compra_id, Forma_de_Pagamento_has_Condicao_de_Pagamento_id, valor, qtdParcelas', 'required'),
            array('Compra_id, Forma_de_Pagamento_has_Condicao_de_Pagamento_id, qtdParcelas', 'numerical', 'integerOnly'=>true),
            array('valor', 'numerical'),
            array('id, Compra_id, Forma_de_Pagamento_has_Condicao_de_Pagamento_id, valor, qtdParcelas', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'compraHasFPHasCPHasAdministradoraFinanceiras'  => array(self::HAS_MANY,    'CompraHasFPHasCPHasAdministradoraFinanceira',  'Compra_has_FP_has_CP_id'),
            'formaDePagamentoHasCondicaoDePagamento'        => array(self::BELONGS_TO,  'FormaDePagamentoHasCondicaoDePagamento',       'Forma_de_Pagamento_has_Condicao_de_Pagamento_id'),
            'compra'                                        => array(self::BELONGS_TO,  'Compra',                                       'Compra_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'                                                => 'ID',
            'Compra_id'                                         => 'Compra',
            'Forma_de_Pagamento_has_Condicao_de_Pagamento_id'   => 'Forma De Pagamento Has Condicao De Pagamento',
            'valor'                                             => 'Valor',
            'qtdParcelas'                                       => 'Qtd Parcelas',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('Compra_id',$this->Compra_id);
        $criteria->compare('Forma_de_Pagamento_has_Condicao_de_Pagamento_id',$this->Forma_de_Pagamento_has_Condicao_de_Pagamento_id);
        $criteria->compare('valor',$this->valor);
        $criteria->compare('qtdParcelas',$this->qtdParcelas);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CompraHasFormaDePagamentoHasCondicaoDePagamento the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}