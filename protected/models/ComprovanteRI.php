<?php

/**
 * This is the model class for table "ComprovanteRI".
 *
 * The followings are the available columns in table 'ComprovanteRI':
 * @property integer $id
 * @property integer $habilitado
 * @property string $data_cadastro
 * @property integer $Parcela_id
 * @property integer $Anexo_id
 *
 * The followings are the available model relations:
 * @property Anexo $anexo
 * @property Parcela $parcela
 */
class ComprovanteRI extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ComprovanteRI';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('habilitado, data_cadastro, Parcela_id, Anexo_id', 'required'),
			array('habilitado, Parcela_id, Anexo_id', 'numerical', 'integerOnly'=>true),
			array('data_cadastro', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, habilitado, data_cadastro, Parcela_id, Anexo_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'anexo' => array(self::BELONGS_TO, 'Anexo', 'Anexo_id'),
			'parcela' => array(self::BELONGS_TO, 'Parcela', 'Parcela_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'Parcela_id' => 'Parcela',
			'Anexo_id' => 'Anexo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('Parcela_id',$this->Parcela_id);
		$criteria->compare('Anexo_id',$this->Anexo_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ComprovanteRI the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
