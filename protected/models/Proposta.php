<?php

Yii::import('application.vendor.*');
require_once 'Math/Finance.php';
require_once 'Math/Finance_FunctionParameters.php';
require_once 'Math/Numerical/RootFinding/NewtonRaphson.php';

class Proposta extends CActiveRecord {

    private $util;

    public $nucleosFIDCLeccaSegundaOpcao = [0];

    public function propostaECliente($cpf, $codigo) {
        $proposta = Proposta::model()->find('codigo = ' . $codigo . ' AND Status_Proposta_id IN (2,7) AND titulos_gerados');

        if ($proposta != NULL) {
            if ($proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero == $cpf) {
                return $proposta;
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }
    }

    public function parcelaMes($dataConfig) {

        $Qry = " SELECT P.id AS ParcelaId FROM Parcela AS P ";
        $Qry .= " INNER JOIN Titulo AS T ON T.id = P.Titulo_id AND T.NaturezaTitulo_id = 1 AND T.habilitado AND P.habilitado ";
        $Qry .= " INNER JOIN Proposta AS PR ON PR.id = T.Proposta_id AND PR.id = " . $this->id . " AND PR.habilitado AND YEAR(P.vencimento) = " . $dataConfig['ano'];
        $Qry .= " AND MONTH(P.vencimento) = " . $dataConfig['mes'];

        return Yii::app()->db->createCommand($Qry)->queryAll();
    }

    public function getSolicitacaoDeCancelamento() {

    }

    public function calcularValorSeguroOmni($valor, $valor_entrada){
      return (($valor - $valor_entrada) > 5000) ? (5000/100)*8.99 : (($valor-$valor_entrada)/100)*8.99;
      // return (($valor - $valor_entrada) > 5000) ? (5000/100)*5 : (($valor+$valor_entrada)/100)*5;
    }

    public function calcularValorDoSeguro() {

        $valorDoSeguro = 0;

        /* Possui seguro */
        if ($this->segurada) {
            /* Proposta segurada nos novos moldes */

            if( $this->hasOmniConfig() != NULL ){
              return Proposta::model()->calcularValorSeguroOmni($this->valor, $this->valor_entrada);
            }
            else if (( $this->venda !== NULL ) && (count($this->venda->itemDaVendas) > 0)) {
                foreach ($this->venda->itemDaVendas as $item) {
                    /* É o seguro */
                    if (($item->itemDoEstoque->produto->id == 1) && ($item->habilitado)) {
                        return $item->preco;
                    }
                }
            }
            /* Trata-se de uma proposta com o modelo antigo de seleção de seguro */
            else {
                return ( ( $this->valor - $this->valor_entrada ) / 100 ) * 5;
            }
        }

        /* Não possui seguro */ else {
            return 0;
        }
    }

    public function calcularValorDoSeguro2($valor, $valor_entrada) {
        return ( ( $valor - $valor_entrada ) / 100 ) * 10;
    }

    public function getValorFinanciadoOmni() {
        return $this->valor + /*$this->calcularValorSeguroOmni() +*/ $this->calcularTAC();
    }

    public function getValorFinanciado() {
        return ( ( $this->valor - $this->valor_entrada ) + $this->calcularValorDoSeguro() + $this->calcularTAC());
    }

    public function calcularTAC() {
        $financiado = ( ( $this->valor - $this->valor_entrada ) + $this->calcularValorDoSeguro() );
        if ($this->hasOmniConfig()) {
            if ($this->propostaOmniConfig->numPropLoj != null) {
                if ((($financiado / 100) * 10) > 150) {
                    return 150;
                } else {
                    return (($financiado / 100) * 10);
                }
            } else {
                return 0;
            }
        } else {
            if ($this->analiseDeCredito->filial->nucleoFilial->id == 40 && $this->tabelaCotacao->ModalidadeId == 1) {
                return 0;//((($financiado-$this->calcularValorDoSeguro()) / 100) * 5);
            } else {
                return 0;
            }
        }
    }

    public function dataParaView() {

        $this->util = new Util;

        return $this->util->bd_date_to_view(substr($this->data_cadastro, 0, 10));
    }

    public function valorRepasse() {

        $this->hasOmniConfig() ? $valorFinanciado = $this->valor - $this->valor_entrada : $valorFinanciado = $this->valor - $this->valor_entrada + $this->calcularValorDoSeguro();

        $fator = Fator::model()->find('Tabela_Cotacao_id = ' . $this->tabelaCotacao->id . ' AND parcela = ' . $this->qtd_parcelas . ' AND carencia = ' . $this->carencia . ' AND habilitado');

        if( CompraProgramada::model()->find('habilitado AND Proposta_id = ' . $this->id ) != Null ){
            return 0;
        }
        else{
            if ($fator != NULL) {
                return $valorFinanciado - $fator->getValorRetido($valorFinanciado);
            } else {
                return $valorFinanciado; //mudanca inserida 11/02/2016. Retorne o valor financiado
            }
        }
    }

    public function aprovarAutomatico() {

        $atrasos = $this->analiseDeCredito->cliente->hasAtraso();

        if (( ( $this->valor - $this->valor_entrada ) < 100 ) && sizeof($atrasos) < 1) {
            $this->Status_Proposta_id = 2;
            $this->update();
            //$this->gerarRegistroRetornoParceiro('01');

            /* Registrando quem analisou */
            $analistaHasPropHasStatusA = new AnalistaHasPropostaHasStatusProposta;
            $analistaHasPropHasStatusA->Analista_id = 13; //analista padrão do sistema
            $analistaHasPropHasStatusA->Proposta_id = $this->id;
            $analistaHasPropHasStatusA->Status_Proposta_id = 1;
            $analistaHasPropHasStatusA->habilitado = 1;
            $analistaHasPropHasStatusA->data_cadastro = date('Y-m-d H:i:s');
            $analistaHasPropHasStatusA->updated_at = date('Y-m-d H:i:s');
            $analistaHasPropHasStatusA->save();

            /* Registrando quem aprovou */
            $analistaHasPropHasStatusP = new AnalistaHasPropostaHasStatusProposta;
            $analistaHasPropHasStatusP->Analista_id = 13; //analista padrão do sistema
            $analistaHasPropHasStatusP->Proposta_id = $this->id;
            $analistaHasPropHasStatusP->Status_Proposta_id = $this->Status_Proposta_id;
            $analistaHasPropHasStatusP->habilitado = 1;
            $analistaHasPropHasStatusP->data_cadastro = date('Y-m-d H:i:s');
            $analistaHasPropHasStatusP->updated_at = date('Y-m-d H:i:s');
            $analistaHasPropHasStatusP->save();
        }
    }

    public function persist($Proposta, $ClienteId, $AnaliseDeCredito, $Bens, $emprestimo, $semear, $compraProgramada = NULL) {
        $retorno = array();

        $retorno['hasErrors'] = true;
        $retorno['titulo'] = 'Erros foram encontrados!';
        $retorno['texto'] = 'Erros no processo. Contacte o suporte.';
        $retorno['tipo'] = 'error';

        $Cliente = Cliente::model()->findByPk($ClienteId);

        /*      $filiaisFIDCLecca = [35,36,37,38,39,40,41,42,43,44,45,46,47,48,81,82,83,84,85,86,87,88,89,90,91,
                            92,93,94,95,96,97,98,99,100,101,102,103,104,105,107,108,118,
                            119,123,127,139,203,204,240,273,274];*/

        $filiaisFIDCLecca = [82,83,84,85,86,87,88,99,106,107,274,89,90,92,108,37,41,36,91,35,240,203,42,273,204,98,45,97,103,81,44,43,40,39,37,102,100,101,104,105,94,38,96,95,93];

        $nucleosFIDCLecca = [121,122,125,126,134,135,138,139,140];

        //$nucleosFIDCLeccaSegundaOpcao = [92, 131, 80, 68];

        $valorEntrada = 0;

        if(!isset($Proposta['segurada']))
        {
            $Proposta['segurada'] = 0;
        }

        if (isset($Proposta['valor_entrada']) && $Proposta['valor_entrada'] !== null) {
            $valorEntrada = $Proposta['valor_entrada'];
        }

        $valorProposta = str_replace(array('.', ','), array('', '.'), $Proposta["valor"]);

        if (!$semear) {

            if(Yii::app()->session["usuario"]->id == 713)
            {
                $vmin = 0;
            }
            else
            {
                $vmin = ConfigLN::model()->valorDoParametro('valor_min_emprestimo');
            }

            $vmax = ConfigLN::model()->valorDoParametro('valor_max_emprestimo');
        }
        else {
            if(Yii::app()->session["usuario"]->id == 713)
            {
                $vmin = 0;
            }
            else
            {
                $vmin = 300;
            }
            $vmax = 2000;
        }

        $filial = Yii::app()->session['usuario']->returnFilial();


        if ( $filial !== null && !in_array( $filial->id, $filiaisFIDCLecca ) && !in_array( $filial->NucleoFiliais_id, $nucleosFIDCLecca ) )
        {
            if( ( ( $valorProposta - $valorEntrada ) < 201 ) && Yii::app()->session['usuario']->id != 713 )
            {
                $retorno['hasErrors'] = true;
                $retorno['msgConfig']['pnotify'][] = array(
                    'titulo' => 'Erros foram encontrados!',
                    'texto' => "Valor mínimo de proposta é de R$ 201,00.",
                    'tipo' => 'error',
                );

                return $retorno;
            }
        }

        //ajeitar isso aqui depois
        if ($emprestimo && ($valorProposta < $vmin || $valorProposta > $vmax)) {
            $retorno['hasErrors'] = true;
            $retorno['msgConfig']['pnotify'][] = array(
                'titulo' => 'Erros foram encontrados!',
                'texto' => "Propostas de empréstimo devem ter valor entre R$ " . number_format(ConfigLN::model()->valorDoParametro('valor_min_emprestimo'), 2, ',', '.') . " e R$ " . number_format(ConfigLN::model()->valorDoParametro('valor_max_emprestimo'), 2, ',', '.') . ".",
                'tipo' => 'error',
            );
        }

        else {
            //Cadastro do Cliente localizado
            if ($Cliente != NULL) {
                //Cliente possui propostas abertas. Em analise ou aguardando analise.
                if (count($Cliente->propostasAtivas()) > 0) {
                    $retorno['msgConfig']['pnotify'][] = array(
                        'titulo' => 'Não foi possível completar sua requisição!',
                        'texto' => 'Já existe uma proposta em análise, ou aguardando análise, para este cliente.',
                        'tipo' => 'error',
                    );
                }
                else if(trim(Yii::app()->session["usuario"]->id) !== "713" && ($valorProposta - $valorEntrada) < 100){
                    $retorno['msgConfig']['pnotify'][] = array(
                        'titulo' => 'Não foi possível completar sua requisição!',
                        'texto' => 'A proposta deve ter um valor superior/igual a 100,00. idUsuario: ' . Yii::app()->session["usuario"]->id,
                        'tipo' => 'error',
                    );
                }
                else {
                    //Pega o status do cadastro do cliente
                    $retorno['statusDoCadastro'] = $Cliente->clienteCadastroStatus();

                    //Cadastro irregular
                    if ($retorno['statusDoCadastro']['status'] == 2) {
                        $retorno['msgConfig']['pnotify'][] = array(
                            'titulo' => 'O cliente não possui um cadastro regular.',
                            'texto' => 'Verifique o cadastro do cliente. Ou entre em contato com a central para mais informações!',
                            'tipo' => 'error',
                        );

                        for ($i = 0; $i < count($retorno['statusDoCadastro']['observacoes']); $i++) {
                            $retorno['msgConfig']['alerts'][] = array(
                                'icon' => '<i class="fa fa-exclamation-triangle"></i>',
                                'cssClass' => 'alert alert-warning',
                                'content' => $retorno['statusDoCadastro']['observacoes'][$i]['text'] . $retorno['statusDoCadastro']['observacoes'][$i]['button_modal'],
                                'form_show' => $retorno['statusDoCadastro']['observacoes'][$i]['show_form']
                            );

                            $retorno['msgConfig']['pnotify'][] = array(
                                'titulo' => 'Notificação:',
                                'texto' => $retorno['statusDoCadastro']['observacoes'][$i]['text'],
                                'tipo' => 'error',
                            );
                        }
                    }

                    //Cadastro regular -- Aqui que começa o muído --
                    else {
                        $analise = AnaliseDeCredito::model()->novo($AnaliseDeCredito, $ClienteId, $Proposta['valor'], $valorEntrada, $Bens, $emprestimo);

                        if ($analise[0] != NULL) {
                            $venda = NULL;
                            //cria um novo registro de proposta
                            $proposta = Proposta::model()->novo($Proposta, $analise[0], $emprestimo, $semear);

                            if ($proposta != NULL) {

                                if( $compraProgramada           != null && $compraProgramada == 1){
                                    $cProgramada                = new CompraProgramada;
                                    $cProgramada->Proposta_id   = $proposta->id;
                                    $cProgramada->habilitado    = 1;
                                    $cProgramada->save();
                                }

                                /* INICIO------Registrar venda */
                                $venda = Venda::model()->novo(
                                        substr($proposta->codigo, -5), date('Y-m-d H:i:s'), $proposta->analiseDeCredito->Cliente_id, 1, NULL, $proposta->id
                                );
                                /* FIM---------Registrar venda */
                                /* INICIO adicionando intens da venda (seguro) */

                                if ($Proposta['segurada'] && $venda !== NULL) {
                                    //Capturando o ID do produto selecionado na tela
                                    // $proposta->segurada = 1;
                                    // $proposta->update();
                                    //
                                    // $coberturaSeguro = ConfigLN::model()->find("habilitado AND parametro = 'cobertura_seguro_" . $Proposta['segurada'] . "'");
                                    // $item_da_venda = ItemDaVenda::model()->novo(
                                    //          $Proposta['segurada'], $venda->id, 1, 1, 0, (($proposta->valor - $proposta->valor_entrada) / 100) * intval($coberturaSeguro->valor), 1
                                    // );
                                    //
                                    // $proposta->valor_parcela = $proposta->valor_parcela + $item_da_venda->preco;
                                    // $proposta->update();
                                }
                                /* FIM adicionando intens da venda (seguro) */

                                if ($emprestimo) {
                                    $filialUsuario = Yii::app()->session['usuario']->returnFilial(0);

                                    if ($filialUsuario !== null) {
                                        $idFilial = $filialUsuario->id;
                                    } else {
                                        $idFilial = 184;
                                    }

                                    $emprestimo = new Emprestimo;
                                    $emprestimo->data_cadastro = date("Y-m-d H:i:s");
                                    $emprestimo->habilitado = 1;
                                    $emprestimo->Filial_id = $idFilial;
                                    $emprestimo->Proposta_id = $proposta->id;

                                    $emprestimo->save();
                                }

                                /* Não é empréstimo e é filial credshow */
                                if ( (!$emprestimo) && ($proposta->analiseDeCredito->filial->id == 188) ) {

                                    $retorno['hasErrors'] = false;
                                    $retorno['msgConfig']['pnotify'][] = array(
                                        'titulo' => 'Proposta enviada com sucesso! Código: ' . $proposta->codigo,
                                        'texto' => 'Proposta cadastrada. ',
                                        'tipo' => 'success',
                                    );

                                    $retorno['propostaConfig'] = array(
                                        'id' => $proposta->id,
                                        'codigo' => $proposta->codigo
                                    );
                                }

                                else if ((in_array($proposta->analiseDeCredito->filial->NucleoFiliais_id, [-40]) && $proposta->tabelaCotacao->ModalidadeId == 2))
                                {
                                    $proposta->codigo = "2" . str_pad($proposta->id, 6, 0, STR_PAD_LEFT);
                                    $proposta->Financeira_id = 5;

                                    if ($proposta->update()) {

                                        $retorno['hasErrors'] = false;
                                        $retorno['msgConfig']['pnotify'][] = array(
                                            'titulo' => 'Proposta enviada com sucesso! Código: ' . $proposta->codigo,
                                            'texto' => 'Proposta cadastrada. ',
                                            'tipo' => 'success',
                                        );
                                        $retorno['propostaConfig'] = array(
                                            'id' => $proposta->id,
                                            'codigo' => $proposta->codigo
                                        );
                                    }

                                }

                                else if (in_array($proposta->analiseDeCredito->filial->NucleoFiliais_id, [47,38,139,138,140, 135, 121, 122, 126, 134])) {
                                    $ahsp = new AnalistaHasPropostaHasStatusProposta;
                                    $ahsp->Proposta_id = $proposta->id;
                                    $ahsp->Analista_id = 913;
                                    $ahsp->Status_Proposta_id = 2;
                                    $ahsp->habilitado = 1;
                                    $ahsp->data_cadastro = date('Y-m-d');
                                    $ahsp->updated_at = date('Y-m-d');

                                    if ($ahsp->save()) {
                                        $proposta->codigo = "40" . str_pad($proposta->id, 6, 0, STR_PAD_LEFT);

                                        if ($proposta->analiseDeCredito->filial->NucleoFiliais_id == 123) {
                                            $proposta->Financeira_id = 5;
                                            $proposta->titulos_gerados = 1;
                                        } else {
                                            $proposta->Financeira_id = 5;
                                        }

                                        $proposta->Status_Proposta_id = 2;
                                        $proposta->update();

                                        $retorno['hasErrors'] = false;
                                        $retorno['msgConfig']['pnotify'][] = array(
                                            'titulo' => 'Proposta enviada com sucesso! Código: ' . $proposta->codigo,
                                            'texto' => 'Proposta cadastrada. ',
                                            'tipo' => 'success',
                                        );
                                        $retorno['propostaConfig'] = array(
                                            'id' => $proposta->id,
                                            'codigo' => $proposta->codigo
                                        );
                                    }
                                }

                                /* Aprovação automática temporária, que será realizada neste item */
                                /* Se a filial estiver nas filiais marcadas para serem lecca, ou se o nucleo estiver marcado para ser lecca, entra aqui */
                                else if ( in_array( $proposta->analiseDeCredito->filial->id, $filiaisFIDCLecca ) || in_array( $proposta->analiseDeCredito->filial->NucleoFiliais_id, $nucleosFIDCLecca ) ) {

                                    $proposta->codigo = "40" . str_pad($proposta->id, 6, 0, STR_PAD_LEFT);
                                    $proposta->Financeira_id = 5;
                                    $anexos = $proposta->analiseDeCredito->cliente->listarAnexos(1, 0);

                                    $scores = new ConsultaCliente;
                                    $scores->consultarScore($proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero, 1, $proposta->analiseDeCredito->cliente->id);

                                    //Analise para politica restritiva

                                    //$filialHasPolitica = FilialHasPoliticaCredito::model()->find("habilitado AND Filial_id = " . $proposta->analiseDeCredito->Filial_id);

                                    $filialHasPolitica = $proposta->analiseDeCredito->filial->filialHasPoliticaCredito;

                                    if( isset($filialHasPolitica) && in_array($filialHasPolitica->PoliticaCredito_id, [4,6] ) )
                                    {

                                        if( ($scores->classe == 'D' || $scores->classe == 'E' || $scores->classe == 'F') ){

                                            if( (!$proposta->analiseDeCredito->cliente->temVendaAprovada() && !$proposta->analiseDeCredito->cliente->getCadastro()->cliente_da_casa) || $filialHasPolitica->PoliticaCredito_id == 6 )
                                            {

                                                Mensagem::model()->nova("Negado automaticamente por política de crédito.", $proposta->getDialogo(), 913);

                                                $ahsp                               = new AnalistaHasPropostaHasStatusProposta;
                                                $ahsp->Proposta_id                  = $proposta->id;
                                                $ahsp->Analista_id                  = 913;
                                                $ahsp->Status_Proposta_id           = 3;
                                                $ahsp->habilitado                   = 1;
                                                $ahsp->data_cadastro                = date('Y-m-d');
                                                $ahsp->updated_at                   = date('Y-m-d');

                                                if ($ahsp->save()) {
                                                    $proposta->Status_Proposta_id   = 3;
                                                }
                                            }
                                            else
                                            {
                                                $proposta->Status_Proposta_id       = 4;
                                            }
                                        }

                                        else if( ($scores->classe == 'A' || $scores->classe == 'B' || $scores->classe == 'C') ){

                                            $atrasos = $proposta->analiseDeCredito->cliente->parcelasEmAberto();

                                            if( ($scores->classe == 'A' || $scores->classe == 'B') && $proposta->analiseDeCredito->alerta == 'Verde' && ( ($proposta->valor - $proposta->valor_entrada) <= 1500
                                                && $scores->restricao == 'false' && sizeof($atrasos) < 1 )
                                            ){
                                                $ahsp = new AnalistaHasPropostaHasStatusProposta;
                                                $ahsp->Proposta_id = $proposta->id;
                                                $ahsp->Analista_id = 913;
                                                $ahsp->Status_Proposta_id = 2;
                                                $ahsp->habilitado = 1;
                                                $ahsp->data_cadastro = date('Y-m-d');
                                                $ahsp->updated_at = date('Y-m-d');

                                                if ($ahsp->save()) {
                                                    $proposta->Status_Proposta_id = 2;
                                                }
                                            }
                                            elseif( $proposta->analiseDeCredito->alerta == 'Verde' && $scores->classe == 'C' && ( ($proposta->valor - $proposta->valor_entrada) <= 1200
                                                && $scores->restricao == 'false' && sizeof($atrasos) < 1 )
                                            ){
                                                $ahsp = new AnalistaHasPropostaHasStatusProposta;
                                                $ahsp->Proposta_id = $proposta->id;
                                                $ahsp->Analista_id = 913;
                                                $ahsp->Status_Proposta_id = 2;
                                                $ahsp->habilitado = 1;
                                                $ahsp->data_cadastro = date('Y-m-d');
                                                $ahsp->updated_at = date('Y-m-d');

                                                if ($ahsp->save()) {
                                                    $proposta->Status_Proposta_id   = 2;
                                                }
                                            }
                                            else if ($filialHasPolitica->PoliticaCredito_id == 4)
                                            {
                                                $proposta->Status_Proposta_id       = 4;
                                            }
                                            else
                                            {

                                                Mensagem::model()->nova("Negado automaticamente por política de crédito.", $proposta->getDialogo(), 913);

                                                $ahsp                               = new AnalistaHasPropostaHasStatusProposta;
                                                $ahsp->Proposta_id                  = $proposta->id;
                                                $ahsp->Analista_id                  = 913;
                                                $ahsp->Status_Proposta_id           = 3;
                                                $ahsp->habilitado                   = 1;
                                                $ahsp->data_cadastro                = date('Y-m-d');
                                                $ahsp->updated_at                   = date('Y-m-d');

                                                if ($ahsp->save())
                                                {
                                                    $proposta->Status_Proposta_id = 3;
                                                }

                                            }
                                        }

                                    }

                                    /*Outras políticas*/
                                    /*else{
                                        /* Score F, e o cliente não é cliente da casa 
                                        if (($scores->classe == 'E' || $scores->classe == 'F')) {

                                            if (!$proposta->analiseDeCredito->cliente->getCadastro()->cliente_da_casa && !$proposta->analiseDeCredito->cliente->temVendaAprovada())
                                            {

                                                if ($proposta->analiseDeCredito->cliente->pessoa->getCPF()->vip == 1)
                                                {
                                                    $proposta->Status_Proposta_id = 4;
                                                    $proposta->update();
                                                }
                                                else
                                                {

                                                    Mensagem::model()->nova("Negado automaticamente por política de crédito.", $proposta->getDialogo(), 913);

                                                    $ahsp = new AnalistaHasPropostaHasStatusProposta;
                                                    $ahsp->Proposta_id = $proposta->id;
                                                    $ahsp->Analista_id = 913;
                                                    $ahsp->Status_Proposta_id = 3;
                                                    $ahsp->habilitado = 1;
                                                    $ahsp->data_cadastro = date('Y-m-d');
                                                    $ahsp->updated_at = date('Y-m-d');

                                                    if ($ahsp->save()) {
                                                        $proposta->Status_Proposta_id = 3;
                                                    }
                                                }
                                            }

                                        }
                                        else if
                                        (
                                            (
                                                (
                                                    ($scores->classe == 'A' || $scores->classe == 'B') &&
                                                    ($proposta->valor - $proposta->valor_entrada) <= 2000
                                                ) ||

                                                (
                                                    $scores->classe == 'C' &&
                                                    ($proposta->valor - $proposta->valor_entrada) <= 1500
                                                ) ||

                                                (
                                                    $scores->classe == 'D' &&
                                                    ($proposta->valor - $proposta->valor_entrada) <= 500
                                                )
                                            ) &&
                                                $scores->restricao == 'false' &&
                                                count($proposta->analiseDeCredito->cliente->numeroPassagensDia(date('Y-m-d'), date('Y-m-d'))) <= 2 &&
                                                $proposta->analiseDeCredito->alerta == 'Verde'
                                        )
                                        {
                                            $atrasos = $proposta->analiseDeCredito->cliente->parcelasEmAberto();

                                            if ((sizeof($atrasos) < 1)) {
                                                $ahsp = new AnalistaHasPropostaHasStatusProposta;
                                                $ahsp->Proposta_id = $proposta->id;
                                                $ahsp->Analista_id = 913;
                                                $ahsp->Status_Proposta_id = 2;
                                                $ahsp->habilitado = 1;
                                                $ahsp->data_cadastro = date('Y-m-d');
                                                $ahsp->updated_at = date('Y-m-d');

                                                if ($ahsp->save()) {
                                                    $proposta->Status_Proposta_id = 2;
                                                }
                                            }
                                        }
                                    }*/
                                     /* Atualize a proposta */
                                    if ($proposta->update()) {
                                        $retorno['hasErrors']               = false;
                                        $retorno['msgConfig']['pnotify'][]  = array(
                                            'titulo'                        => 'Proposta enviada com sucesso! Código: ' . $proposta->codigo,
                                            'texto'                         => 'Proposta cadastrada. ',
                                            'tipo'                          => 'success',
                                        );
                                        $retorno['propostaConfig']          = array(
                                            'id'                            => $proposta->id,
                                            'codigo'                        => $proposta->codigo
                                        );
                                    }
                                }

                                /* Propostas enviadas para hosts de terceiros */
                                else
                                {

                                    if  (
                                            ( $proposta->getValorFinanciado() >= 201 && $proposta->tabelaCotacao->id != 32) &&
                                            ( ( $proposta->tabelaCotacao->ModalidadeId == 1 && $proposta->qtd_parcelas > 2) ||
                                            ( $proposta->tabelaCotacao->ModalidadeId == 2 && $proposta->qtd_parcelas > 1 &&
                                            $proposta->qtd_parcelas <= 10 )
                                            )
                                        )
                                    {

                                        if (!$semear) {

                                            /* Chamar o envio da proposta para a Omni aqui... */
                                            $retornoSend = $this->sendToOmni($proposta, $analise[0], $analise[0]->cliente);
                                            $proposta->habilitado = 1;

                                            if ($proposta->Status_Proposta_id == 10) {
                                                $proposta->Status_Proposta_id = 4;
                                            }

                                            if ($proposta->update()) {

                                                $tipoRetorno = $retornoSend['tipo'];
                                                $retornoOmni = $retornoSend['resposta'];
                                                $tabelaHost = $retornoSend['tabelaHost'];
                                                $usuarioHost = $retornoSend['usuarioHost'];

                                                //caso o tipo de retorno seja 1, significa que eh uma resposta a requisicao xml
                                                if ($tipoRetorno == 1) {

                                                    //caso o retorno tenha idProposta, significa que deu certo a criacao no host do WS
                                                    if ($retornoOmni->dados->idProposta != null && !empty((string) $retornoOmni->dados->idProposta)) {

                                                        //atribua o codigo da proposta com o codigo retornado pelo host do WS
                                                        $proposta->codigo = (string) $retornoOmni->dados->idProposta;
                                                        $proposta->Financeira_id = 5;

                                                        if ($proposta->update()) { //salve a proposta
                                                            $retorno['hasErrors'] = false;
                                                            $mensagem = '';

                                                            /*
                                                              chame o omniConfigSave. Funcao que ira atualizar os registros de WS dessa proposta
                                                              junto com os dados retornados pela financeira parceira
                                                             */

                                                            $proposta->omniConfigSave(null, $tabelaHost, $usuarioHost);

                                                            $retorno['msgConfig']['pnotify'][] = array(
                                                                'titulo' => 'Proposta enviada com sucesso! Código: ',
                                                                'texto' => 'Deu tudo certo.',
                                                                'tipo' => 'success',
                                                            );

                                                            $retorno['propostaConfig'] = array(
                                                                'id' => $proposta->id,
                                                                'codigo' => $proposta->codigo
                                                            );

                                                            $sigacLog = new SigacLog;
                                                            $sigacLog->saveLog('Proposta Enviada', 'Proposta', $proposta->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Crediarista " . Yii::app()->session['usuario']->nome_utilizador . " enviou para análise a proposta com código: " . $proposta->codigo, "127.0.0.1", null, Yii::app()->session->getSessionId());
                                                        } else {

                                                            ob_start();
                                                            var_dump($proposta->getErrors());
                                                            $erroPropostaUpdate = ob_get_clean();

                                                            $retorno['hasErrors'] = true;
                                                            $mensagem = "Erro ao alterar código da proposta. "
                                                                    . "Proposta: $erroPropostaUpdate";

                                                            /*
                                                              chame o omniConfigSave. Funcao que ira atualizar os registros de WS dessa proposta
                                                              junto com os dados retornados pela financeira parceira
                                                             */

                                                            $retorno['msgConfig']['pnotify'][] = array(
                                                                'titulo' => 'Erro ao atulizar código da Proposta',
                                                                'texto' => $mensagem,
                                                                'tipo' => 'error',
                                                            );

                                                            $retorno['propostaConfig'] = array(
                                                                'id' => $proposta->id,
                                                                'codigo' => $proposta->codigo
                                                            );

                                                            $sigacLog = new SigacLog;
                                                            $sigacLog->saveLog('Proposta .::. Erro', 'Proposta', $proposta->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, $mensagem, "127.0.0.1", null, Yii::app()->session->getSessionId());

                                                            $message = new YiiMailMessage;
                                                            $message->view = "mandaMailFichamento";
                                                            $message->subject = 'Proposta com erros';

                                                            $parametros = [
                                                                'email' => [
                                                                    'titulo' => "Erro ao atualizar codigo omni da proposta.",
                                                                    'texto' => $mensagem,
                                                                    'link' => ''
                                                                ]
                                                            ];

                                                            $message->setBody($parametros, 'text/html');
                                                            $message->addTo('contato@totorods.com');

                                                            $message->from = 'no-reply@credshow.com.br';

                                                            Yii::app()->mail->send($message);
                                                        }
                                                    } else {

                                                        /*
                                                          A proposta não foi criada no host da Omni, portanto, excluímos
                                                          e retornamos os erros em forma de mensagens na tela da crediarista
                                                         */

                                                        /////////////////////////////////////////////////////////////
                                                        //alteracao//////////////////////////////////////////////////
                                                        /////////////////////////////////////////////////////////////
                                                        //autor : andre//data : 27-05-16 ////////////////////////////
                                                        /////////////////////////////////////////////////////////////
                                                        //descricao : tirando o delete() e desabilitando a proposta//
                                                        /////////////////////////////////////////////////////////////

                                                        $analise = $proposta->analiseDeCredito;
                                                        /*                                                  $proposta->delete();
                                                          $analise->delete(); */

                                                        $retorno['hasErrors'] = true;

                                                        $proposta->habilitado = 0;

                                                        if (!$proposta->update()) {

                                                            ob_start(); //inibe o print do resultado do var_dump
                                                            var_dump($proposta->getErrors()); //extrai as informacoes do erro
                                                            $resultadoErro = ob_get_clean(); //captura o conteudo extraido do var_dump

                                                            $retorno['msgConfig']['pnotify']['texto'] .= "Analise: $resultadoErro";

                                                            $message = new YiiMailMessage;
                                                            $message->view = "mandaMailFichamento";
                                                            $message->subject = 'Proposta com erros';

                                                            $parametros = [
                                                                'email' => [
                                                                    'titulo' => "Erro ao fazer update em proposta quando desabilitava.",
                                                                    'texto' => $retorno['msgConfig']['pnotify']['texto']
                                                                    . "<br>Não foi possível enviar a proposta. Entre em contato com o suporte.",
                                                                    'link' => ''
                                                                ]
                                                            ];

                                                            $message->setBody($parametros, 'text/html');
                                                            $message->addTo('contato@totorods.com');

                                                            $message->from = 'no-reply@credshow.com.br';

                                                            Yii::app()->mail->send($message);
                                                        } else {

                                                            $analise->habilitado = 0;

                                                            if (!$analise->update()) {

                                                                ob_start(); //inibe o print do resultado do var_dump
                                                                var_dump($analise->getErrors()); //extrai as informacoes do erro
                                                                $resultadoErro = ob_get_clean(); //captura o conteudo extraido do var_dump

                                                                $retorno['msgConfig']['pnotify']['texto'] .= "Analise: $resultadoErro";

                                                                $message = new YiiMailMessage;
                                                                $message->view = "mandaMailFichamento";
                                                                $message->subject = 'Proposta com erros';

                                                                $parametros = [
                                                                    'email' => [
                                                                        'titulo' => "Erro ao fazer update em analiseDeCredito quando desabilitava.",
                                                                        'texto' => $retorno['msgConfig']['pnotify']['texto']
                                                                        . "<br>Não foi possível enviar a proposta. Entre em contato com o suporte.",
                                                                        'link' => ''
                                                                    ]
                                                                ];

                                                                $message->setBody($parametros, 'text/html');
                                                                $message->addTo('contato@totorods.com');

                                                                $message->from = 'no-reply@credshow.com.br';

                                                                Yii::app()->mail->send($message);
                                                            } else {

                                                                if (isset($retornoOmni->erros)) {

                                                                    $resultadoErro = "";

                                                                    foreach ($retornoOmni->erros->erro as $er) {

                                                                        $retorno['hasErrors'] = true;
                                                                        $retorno['msgConfig']['pnotify'][] = array(
                                                                            'titulo' => 'Erros foram encontrados!',
                                                                            'texto' => (string) $er->descricao,
                                                                            'tipo' => 'error',
                                                                        );

                                                                        $resultadoErro .= (string) $er->descricao;
                                                                    }

                                                                    $message = new YiiMailMessage;
                                                                    $message->view = "mandaMailFichamento";
                                                                    $message->subject = 'Proposta com erros';

                                                                    $parametros = [
                                                                        'email' => [
                                                                            'titulo' => "Erro ao enviar proposta para Omni.",
                                                                            'texto' => "Erro: $resultadoErro"
                                                                            . "<br>Não foi possível enviar a proposta. Entre em contato com o suporte.",
                                                                            'link' => ''
                                                                        ]
                                                                    ];

                                                                    $message->setBody($parametros, 'text/html');
                                                                    $message->addTo('contato@totorods.com');

                                                                    $message->from = 'no-reply@credshow.com.br';

                                                                    Yii::app()->mail->send($message);
                                                                } else {

                                                                    $retorno['msgConfig']['pnotify'][] = array(
                                                                        'titulo' => 'Erros foram encontrados.!',
                                                                        'texto' => 'Não foi possível enviar a proposta. Entre em contato com o suporte.',
                                                                        'tipo' => 'error',
                                                                    );

                                                                    $message = new YiiMailMessage;
                                                                    $message->view = "mandaMailFichamento";
                                                                    $message->subject = 'Proposta com erros';

                                                                    $parametros = [
                                                                        'email' => [
                                                                            'titulo' => "Erro ao fazer update em analiseDeCredito quando desabilitava.",
                                                                            'texto' => $retorno['msgConfig']['pnotify']['texto']
                                                                            . "<br>Não foi possível enviar a proposta. Entre em contato com o suporte.",
                                                                            'link' => ''
                                                                        ]
                                                                    ];

                                                                    $message->setBody($parametros, 'text/html');
                                                                    $message->addTo('contato@totorods.com');

                                                                    $message->from = 'no-reply@credshow.com.br';

                                                                    Yii::app()->mail->send($message);
                                                                }
                                                            }
                                                        }

                                                        /*                                                  $retorno['hasErrors'] = true;

                                                          $retorno['msgConfig']['pnotify'][] = array(
                                                          'titulo' => 'Erros foram encontrados.!',
                                                          'texto' => 'Não foi possível enviar a proposta. Entre em contato com o suporte.',
                                                          'tipo' => 'error',
                                                          ); */
                                                    }
                                                } else {

                                                    $retorno['hasErrors'] = true;


                                                    $retorno['msgConfig']['pnotify'][] = array(
                                                        'titulo' => 'Erros foram encontrados.',
                                                        'texto' => "Não foi possível criar a proposta. Entre em contato com o suporte."
                                                        . "Erro: $retornoOmni",
                                                        'tipo' => 'error',
                                                    );

                                                    /////////////////////////////////////////////////////////////
                                                    //alteracao//////////////////////////////////////////////////
                                                    /////////////////////////////////////////////////////////////
                                                    //autor : andre//data : 02/01 ///////////////////////////////
                                                    /////////////////////////////////////////////////////////////
                                                    //descricao : tirando o delete() e desabilitando a proposta//
                                                    /////////////////////////////////////////////////////////////

                                                    $analise = $proposta->analiseDeCredito;
                                                    //$proposta->delete();
                                                    //$analise->delete();

                                                    $proposta->habilitado = 0; //desabilitando o registro da proposta
                                                    //
                                                    //caso se consiga atualizar o registro
                                                    if ($proposta->update()) {

                                                        $analise->habilitado = 0; //desabilitando o registro da analise
                                                        //caso consiga atualizar o registro
                                                        if (!$analise->update()) {
                                                            $retorno['hasErrors'] = true;

                                                            ob_start(); //inibe o print do resultado do var_dump
                                                            var_dump($analise->getErrors()); //extrai as informacoes do erro
                                                            $resultado = ob_get_clean(); //captura o conteudo extraido do var_dump

                                                            $retorno['msgConfig']['pnotify']['texto'] .= "Analise: $resultado";

                                                            $message = new YiiMailMessage;
                                                            $message->view = "mandaMailFichamento";
                                                            $message->subject = 'Proposta com erros';

                                                            $parametros = [
                                                                'email' => [
                                                                    'titulo' => "Erro ao fazer update em analiseDeCredito quando desabilitava.",
                                                                    'texto' => $retorno['msgConfig']['pnotify']['texto'],
                                                                    'link' => ''
                                                                ]
                                                            ];

                                                            $message->setBody($parametros, 'text/html');

                                                            $message->addTo('contato@totorods.com');
                                                            $message->from = 'no-reply@credshow.com.br';

                                                            Yii::app()->mail->send($message);
                                                        }
                                                    } else { //caso aconteca algum erro na atualizacao de registro
                                                        $retorno['hasErrors'] = true;

                                                        ob_start(); //inibe o print do resultado do var_dump
                                                        var_dump($proposta->getErrors()); //extrai as informacoes do erro
                                                        $resultado = ob_get_clean(); //captura o conteudo extraido do var_dump

                                                        $retorno['msgConfig']['pnotify']['texto'] .= "Proposta: $resultado";

                                                        ob_start();
                                                        var_dump($proposta->getErrors());
                                                        $resultadoErro = ob_get_clean();

                                                        $message = new YiiMailMessage;
                                                        $message->view = "mandaMailFichamento";
                                                        $message->subject = 'Proposta com erros.';

                                                        $parametros = [
                                                            'email' => [
                                                                'titulo' => "Erro ao fazer update em proposta quando desabilitava.",
                                                                'texto' => $retorno['msgConfig']['pnotify']['texto'],
                                                                'link' => ''
                                                            ]
                                                        ];

                                                        $message->setBody($parametros, 'text/html');

                                                        $message->addTo('contato@totorods.com');
                                                        $message->from = 'no-reply@credshow.com.br';

                                                        Yii::app()->mail->send($message);
                                                    }
                                                }
                                            } else {

                                                ob_start();
                                                var_dump($proposta->getErrors());
                                                $resultadoErro = ob_get_clean();

                                                $message = new YiiMailMessage;
                                                $message->view = "mandaMailFichamento";
                                                $message->subject = 'Modelo Contrato/Proposta';

                                                $parametros = [
                                                    'email' => [
                                                        'titulo' => "Erro ao fazer update em proposta.",
                                                        'texto' => $resultadoErro,
                                                        'link' => ''
                                                    ]
                                                ];

                                                $message->setBody($parametros, 'text/html');

                                                $message->addTo('contato@totorods.com');
                                                $message->from = 'no-reply@credshow.com.br';

                                                Yii::app()->mail->send($message);
                                            }
                                        }
                                        else {

                                            //Envio da proposta de emprestimo para o banco semear
                                            if ($analise[0]->cliente->pessoa->getDadosBancarios() === NULL) {

                                                $analise = $proposta->analiseDeCredito;
                                                $emprestimo->habilitado = 0;
                                                $proposta->habilitado = 0;
                                                $analise->habilitado = 0;

                                                if ($emprestimo->update() && $proposta->update() && $analise->update()) {

                                                    $retorno['hasErrors'] = true;
                                                    $retorno['msgConfig']['pnotify'][] = array(
                                                        'titulo' => 'Erros foram encontrados!',
                                                        'texto' => 'Cadastre os dados bancarios do cliente',
                                                        'tipo' => 'error',
                                                    );
                                                } else {

                                                    $retorno['hasErrors'] = false;
                                                    $retorno['msgConfig']['pnotify'][] = array(
                                                        'titulo' => 'Erros foram encontrados!',
                                                        'texto' => 'Cadastre os dados bancarios do cliente (Não foi possível atualizar o banco de dados)',
                                                        'tipo' => 'error',
                                                    );
                                                }
                                            } else {
                                                $retornoSend = $this->sendToSemear($proposta, $analise[0], $analise[0]->cliente);
                                                $proposta->habilitado = 1;
                                                $proposta->Status_Proposta_id = 1;
                                                $proposta->update();

                                                if ($retornoSend['status'] == 'ERR' || $retornoSend == NULL) {

                                                    $analise = $proposta->analiseDeCredito;
                                                    $emprestimo->habilitado = 0;
                                                    $proposta->habilitado = 0;
                                                    $analise->habilitado = 0;

                                                    if ($emprestimo->update() && $proposta->update() && $analise->update()) {
                                                        if ($retornoSend == NULL) {
                                                            $retorno['hasErrors'] = true;
                                                            $retorno['msgConfig']['pnotify'][] = array(
                                                                'titulo' => 'Erros foram encontrados!',
                                                                'texto' => 'Não enviado para o banco semear',
                                                                'tipo' => 'error',
                                                            );
                                                        } else {
                                                            $retorno['hasErrors'] = true;
                                                            $retorno['msgConfig']['pnotify'][] = array(
                                                                'titulo' => 'Erros foram encontrados!',
                                                                'texto' => (string) $retornoSend['retorno'],
                                                                'tipo' => 'error',
                                                            );
                                                        }
                                                    } else {

                                                        if ($retornoSend == NULL) {
                                                            $retorno['hasErrors'] = true;
                                                            $retorno['msgConfig']['pnotify'][] = array(
                                                                'titulo' => 'Erros foram encontrados!',
                                                                'texto' => 'Não enviado para o banco semear (Não foi possível atualizar o banco de dados)',
                                                                'tipo' => 'error',
                                                            );
                                                        } else {
                                                            $retorno['hasErrors'] = true;
                                                            $retorno['msgConfig']['pnotify'][] = array(
                                                                'titulo' => 'Erros foram encontrados!',
                                                                'texto' => (string) $retornoSend['retorno'] . ' (Não foi possível atualizar o banco de dados)',
                                                                'tipo' => 'error',
                                                            );
                                                        }
                                                    }
                                                } else {
                                                    if ($retornoSend['status'] == 'AGA' || $retornoSend['status'] == 'APR' || $retornoSend['status'] == 'NEG') {

                                                        $proposta->codigo = $retornoSend['numProp'];

                                                        if ($proposta->update()) {
                                                            $ocg = $proposta->omniConfigSemear(null, $retornoSend['numPropLoj']);

                                                            if ($ocg['retorno']) {

                                                                $retorno['hasErrors'] = false;
                                                                $retorno['msgConfig']['pnotify'][] = array(
                                                                    'titulo' => 'Proposta enviada com sucesso! Código: ' . $proposta->codigo,
                                                                    'texto' => (string) $retornoSend['retorno'],
                                                                    'tipo' => 'success',
                                                                );
                                                                $retorno['propostaConfig'] = array(
                                                                    'id' => $proposta->id,
                                                                    'codigo' => $proposta->codigo
                                                                );
                                                            } else {
                                                                $analise = $proposta->analiseDeCredito;
                                                                $emprestimo->habilitado = 0;
                                                                $proposta->habilitado = 0;
                                                                $analise->habilitado = 0;

                                                                if ($emprestimo->update() && $proposta->update() && $analise->update()) {

                                                                    $retorno['hasErrors'] = true;
                                                                    $retorno['msgConfig']['pnotify'][] = array(
                                                                        'titulo' => 'Erros foram encontrados!',
                                                                        'texto' => 'Não foi possivel salvar as configurações da proposta! ' . $ocg['msg'],
                                                                        'tipo' => 'error',
                                                                    );
                                                                } else {

                                                                    $retorno['hasErrors'] = true;
                                                                    $retorno['msgConfig']['pnotify'][] = array(
                                                                        'titulo' => 'Erros foram encontrados!',
                                                                        'texto' => 'Não foi possivel salvar as configurações da proposta! ' . $ocg['msg'] . '( Não foi possível atualizar o banco de dados)',
                                                                        'tipo' => 'error',
                                                                    );
                                                                }
                                                            }
                                                        } else {

                                                            $analise = $proposta->analiseDeCredito;
                                                            $emprestimo->habilitado = 0;
                                                            $proposta->habilitado = 0;
                                                            $analise->habilitado = 0;

                                                            if ($emprestimo->update() && $proposta->update() && $analise->update()) {

                                                                $retorno['hasErrors'] = true;
                                                                $retorno['msgConfig']['pnotify'][] = array(
                                                                    'titulo' => 'Erros foram encontrados!',
                                                                    'texto' => 'Não foi possivel atualizar o codigo da proposta',
                                                                    'tipo' => 'error',
                                                                );
                                                            } else {

                                                                $retorno['hasErrors'] = true;
                                                                $retorno['msgConfig']['pnotify'][] = array(
                                                                    'titulo' => 'Erros foram encontrados!',
                                                                    'texto' => 'Não foi possivel atualizar o codigo da proposta' . ' (Não foi possível atualizar o banco de dados)',
                                                                    'tipo' => 'error',
                                                                );
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else {

                                        $proposta->codigo               = "40" . str_pad($proposta->id, 6, 0, STR_PAD_LEFT);
                                        $proposta->Financeira_id        = 5;
                                        $proposta->Status_Proposta_id   = 4;
                                        $proposta->update();
                                            // Array com segunda opção de financeiras
                                        /*
                                        if (in_array($proposta->analiseDeCredito->filial->id, $filiaisFIDCLecca) || in_array($proposta->analiseDeCredito->filial->NucleoFiliais_id, $nucleosFIDCLecca) || in_array($proposta->analiseDeCredito->filial->NucleoFiliais_id, $this->nucleosFIDCLeccaSegundaOpcao)) {
                                            $proposta->codigo = "4" . str_pad($proposta->id, 6, 0, STR_PAD_LEFT);
                                            $proposta->Financeira_id = 11;

                                            if (!$proposta->update()) {

                                            }
                                        }
                                        */

                                        $retorno['hasErrors'] = false;

                                        $retorno['msgConfig']['pnotify'][] = array(
                                            'titulo' => 'Proposta enviada com sucesso! Código: ' . $proposta->codigo,
                                            'texto' => 'Deu tudo certo.',
                                            'tipo' => 'success',
                                        );

                                        $retorno['propostaConfig'] = array(
                                            'id' => $proposta->id,
                                            'codigo' => $proposta->codigo
                                        );

                                        $sigacLog = new SigacLog;
                                        $sigacLog->saveLog('Proposta Enviada', 'Proposta', $proposta->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Crediarista " . Yii::app()->session['usuario']->nome_utilizador . " enviou para análise a proposta com código: " . $proposta->codigo, "127.0.0.1", null, Yii::app()->session->getSessionId());
                                    }
                                }

                                $proposta->valor_parcela = $proposta->getValorParcela();
                                $proposta->valor_final = $proposta->getValorParcela() * $proposta->qtd_parcelas;
                                $proposta->update();
                            }
                        }

                        else {
                            $retorno['hasErrors'] = true;
                            $retorno['msgConfig']['pnotify'][] = array(
                                'titulo' => 'Erros foram encontrados!',
                                'texto' => "Não foi possível criar a análise. "
                                . "Entre em contato com o Suporte. $analise[1]",
                                'tipo' => 'error',
                            );
                        }
                    }
                }
            }

            //Cadastro do cliente não foi encontrado
            else {
                $retorno['hasErrors'] = true;
                $retorno['msgConfig']['pnotify'][] = array(
                    'titulo' => 'Não foi possível completar sua requisição!',
                    'texto' => 'Não conseguimos encontrar o cadastro do cliente! Verifique os dados novamente.',
                    'tipo' => 'error',
                );
            }
        }
        return $retorno;
    }

    public function valFin() {
        return ($this->valor - $this->valor_entrada);
    }

    public function desistirAnalise() {
        if ($this->hasOmniConfig() == NULL) {
            $this->Status_Proposta_id = 10;

            if ($this->update()) {
                $sigacLog = new SigacLog;
                $sigacLog->saveLog('Desistência de proposta', 'Proposta', $this->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Usuario " . Yii::app()->session['usuario']->username . " informou a desistencia da proposta " . $this->codigo . " por parte do cliente.", "127.0.0.1", NULL, Yii::app()->session->getSessionId());

                $analistaHasPropHasStatusProp = new AnalistaHasPropostaHasStatusProposta;
                $analistaHasPropHasStatusProp->Proposta_id = $this->id;
                $analistaHasPropHasStatusProp->Analista_id = Yii::app()->session['usuario']->id;
                $analistaHasPropHasStatusProp->habilitado = 1;
                $analistaHasPropHasStatusProp->Status_Proposta_id = 10;
                $analistaHasPropHasStatusProp->data_cadastro = date('Y-m-d H:i:s');
                $analistaHasPropHasStatusProp->updated_at = date('Y-m-d H:i:s');
                $analistaHasPropHasStatusProp->save();
            }
            return true;
        } else {
            return false;
        }
    }

    public function updateCodigoContratoOmni() {


        /*        if(isset($this->analiseDeCredito->filial))
          {
          $nucleo = NucleoFiliais::model()->find("habilitado AND id = " . $this->analiseDeCredito->filial->NucleoFiliais_id);
          } else {
          $nucleo = null;
          }

          if (isset($nucleo->GrupoFiliais_id)) {
          $idGrupoFilial = $nucleo->GrupoFiliais_id;
          } else {
          $idGrupoFilial = 0;
          }

          if ($idGrupoFilial == 4 || $idGrupoFilial == 7 //|| $idGrupoFilial == 16
          || $idGrupoFilial == 21) {
          $chave = "01630LHSTMAREMANSA";
          } else {
          $chave = "01630LHSTCREDSHOW";
          } */

        $pOmniConfig = $this->hasOmniConfig();

        if (isset($pOmniConfig->usuarioHost)) {
            $chave = $pOmniConfig->usuarioHost;
        } else {
            /*if (isset($this->analiseDeCredito->filial)) {
                $nucleo = NucleoFiliais::model()->find("habilitado AND id = " . $this->analiseDeCredito->filial->NucleoFiliais_id);
            } else {
                $nucleo = null;
            }

            if (isset($nucleo->GrupoFiliais_id)) {
                $idGrupoFilial = $nucleo->GrupoFiliais_id;
            } else {
                $idGrupoFilial = 0;
            }

            if ($idGrupoFilial == 4 || $idGrupoFilial == 7 //|| $idGrupoFilial == 16
                    || $idGrupoFilial == 21) {
                $chave = "01630LHSTMAREMANSA";
            } else {*/
                $chave = "01630LHSTCREDSHOW";
            //}
        }

        if ($this->hasOmniConfig() != NULL) {
            $xmlString = '<Envelope>
                 <Body>
                    <CM_PROPOSTA>
                       <proposta xmlns:tem="http://tempuri.org/">
                          <identificacao>
                              <chave>' . $chave . '</chave>
                              <senha>' . $this->getSenhaXML() . '</senha>
                          </identificacao>
                          <dados>
                             <operacao>Consultar</operacao>
                             <idProposta>' . $this->codigo . '</idProposta>
                          </dados>
                       </proposta>
                    </CM_PROPOSTA>
                 </Body>
              </Envelope>';


            $apiUrl = $this->getLinkXML();

            $objProposta = new SoapVar($xmlString, XSD_ANYXML);

            $soapCliente = new SoapClient($apiUrl, array("soap_version" => 1, "trace" => 1, "exceptions" => 1, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS));

            $result = $soapCliente->CM_PROPOSTA($objProposta, null);

            $xmlResult = simplexml_load_string($result->propostaResponse);

            return (string) $xmlResult->dados->contrato;
        } else {

            return NULL;
        }
    }

    public function updateSemearStatus() {
        if ($this->hasOmniConfig() != null && $this->Financeira_id == 7 && $this->Status_Proposta_id != 3 && $this->Status_Proposta_id != 8) {
            $headers = array(
                'Content-Type:text/xml;charset=UTF-8',
                'Connection: close'
            );

            $xml = "<soapenv:Envelope
                    xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'
                    xmlns:ws='http://ws.host.banco.venda.jretail/'>
                    <soapenv:Header/>
                    <soapenv:Body>
                        <ws:consultarProposta>
                            <xmlIn>
                                <host>
                                    <autenticacao>
                                        <codUsr>hstcredshow</codUsr>
                                        <pwdUsr>LMt46dc9ezpP1f34DQFpUA==</pwdUsr>
                                    </autenticacao>
                                    <proposta>
                                        <codLoja>51130000</codLoja>
                                        <numPropLoj>" . $this->propostaOmniConfig->numPropLoj . "</numPropLoj>
                                    </proposta>
                                </host>
                            </xmlIn>
                        </ws:consultarProposta>
                    </soapenv:Body>
                </soapenv:Envelope>";

            $ch = curl_init('https://correspondente.bancosemear.com.br:8443/jretail-jretail.ejb/PropostaHostWSSSL?wsdl');
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);

            $retorno = curl_exec($ch);

            $objXML = simplexml_load_string($retorno);
            $objXML->registerXPathNamespace('env', 'http://schemas.xmlsoap.org/soap/envelope/');
            $objXML->registerXPathNamespace('ws', 'http://ws.host.banco.venda.jretail/');

            $resultado = $objXML->xpath("//ws:consultarPropostaResponse");

            $resp = $resultado[0]->xpath('xmlOut');

            $cdata = simplexml_load_string($resp[0]);

            $response = $cdata->xpath('proposta/status')[0];


            if ($response == 'APR' || $response == 'BOR') {
                $this->Status_Proposta_id = 2;
                $this->dialogo_salvo = 0;
            }
            if ($response == 'NEG') {
                $this->Status_Proposta_id = 3;
                $this->dialogo_salvo = 1;
            }
            if ($response == 'CAN') {
                $this->Status_Proposta_id = 8;
                $this->dialogo_salvo = 1;
            }
            if ($response == 'PEN') {
                $this->Status_Proposta_id = 9;
                $this->dialogo_salvo = 1;
            }
            if ($response == 'DEV') {
                $this->Status_Proposta_id = 1;
                $this->dialogo_salvo = 1;
            }
            if ($response == 'AGA') {
                $this->Status_Proposta_id = 1;
                $this->dialogo_salvo = 1;
            }
            if ($this->update()) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function updateOmniStatus()
    {

        $pOmniConfig = $this->hasOmniConfig();

        if (isset($this->analiseDeCredito->filial)) {
            $nucleo = NucleoFiliais::model()->find("habilitado AND id = " . $this->analiseDeCredito->filial->NucleoFiliais_id);
        } else {
            $nucleo = null;
        }

        if (isset($nucleo->GrupoFiliais_id)) {
            $idGrupoFilial = $nucleo->GrupoFiliais_id;
        } else {
            $idGrupoFilial = 0;
        }

        if (isset($pOmniConfig->usuarioHost)) {
            $chave = $pOmniConfig->usuarioHost;
        } else {
            /*
                        if ($idGrupoFilial == 4 || $idGrupoFilial == 7 //|| $idGrupoFilial == 16
                                || $idGrupoFilial == 21) {
                            $chave = "01630LHSTMAREMANSA";
                        } else {*/
                            $chave = "01630LHSTCREDSHOW";
            //            }
        }

        $util = new Util;

        $msg = "";

        if (trim($this->codigo) == "9512697") {

            $msg .= "passo1_";
        }

        if ( in_array( $this->analiseDeCredito->filial->NucleoFiliais_id, array(40) ) ){
             $chave = "01630LHSTMAREMANSA2";
        }


        if ($this->hasOmniConfig() != NULL) {

            if (trim($this->codigo) == "9512697") {

                $msg .= "passo2_";
            }

            $xmlString = '<Envelope>
                                <Body>
                                    <CM_PROPOSTA>
                                        <proposta xmlns:tem="http://tempuri.org/">
                                            <identificacao>
                                                <chave>' . $chave . '</chave>
                                                <senha>' . $this->getSenhaXML() . '</senha>
                                            </identificacao>
                                            <dados>
                                                <operacao>Consultar</operacao>
                                                <idProposta>' . trim($this->codigo) . '</idProposta>
                                            </dados>
                                        </proposta>
                                    </CM_PROPOSTA>
                                </Body>
                             </Envelope>';

            $apiUrl = $this->getLinkXML();

            $objProposta = new SoapVar($xmlString, XSD_ANYXML);

            $soapCliente = new SoapClient($apiUrl, array("soap_version" => 1, "trace" => 1, "exceptions" => 1, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS));

            $linkContrato = NULL;

            $criadoDesde = $util->diffTime($this->data_cadastro, date('Y-m-d H:i:s'));

            if ($this->Status_Proposta_id == 2) {

                if ($this->getEmprestimo() !== null) {
                    $idNatureza = 32;
                } else {
                    $idNatureza = 2;
                }

                $tituloAPagar = Titulo::model()->find("NaturezaTitulo_id = $idNatureza AND Proposta_id = $this->id");

                if ($tituloAPagar == NULL) {
                    Titulo::model()->gerarTituloAPagar(2, NULL, 1, $this->valorRepasse(), $this);
                }

                //$this->gerarRegistroRetornoParceiro('05', TRUE);

                $omniConfig = $this->hasOmniConfig();

                $linkContrato = $omniConfig->urlContrato;
                $linkBoleto = $omniConfig->urlBoleto;

                if ($this->numero_do_contrato == '' OR empty($this->numero_do_contrato)) {
                    $this->numero_do_contrato = $this->updateCodigoContratoOmni();
                    $this->update();
                }
            }

            /*if (($this->Status_Proposta_id == 3 && $this->Financeira_id != 7) && ($criadoDesde > 0 && $criadoDesde < 120)) {

                $this->analise_solicitada = 1;

                if( in_array($this->analiseDeCredito->filial->NucleoFiliais_id, $this->nucleosFIDCLeccaSegundaOpcao) )
                {
                    $this->Financeira_id    = 11;
                }

                if ($this->update()) {

                     $propostasOmniConfig = PropostaOmniConfig::model()->findAll("habilitado AND Proposta = $this->id");

                     foreach ($propostasOmniConfig as $poc)
                     {

                       $poc->habilitado = 0;

                       if($poc->update())
                       {

                       }

                     }
                }
            }*/

            $passos = "";

            /* Se a proposta estiver negada, aprovada, não é necessário consultar o host omni */
            if (
                    (
                    $this->Status_Proposta_id != 2 && $this->Status_Proposta_id != 3
                    ) ||
                    (
                    $this->Status_Proposta_id == 3 && $this->analise_solicitada
                    ) ||
                    (
                    $this->Status_Proposta_id == 2 &&
                    ( $linkContrato == NULL || $linkBoleto == NULL )
                    )
            ) {

                try {

                    //$passos .= "_9";

                    $result = $soapCliente->CM_PROPOSTA($objProposta, null);

                    $xmlResult = simplexml_load_string($result->propostaResponse);

                    //atraves de um de-para, encontre o status (do sigac) retornado pelo WS da financeira
                    $status = StatusProposta::model()->find('statusOmni = "' . strtoupper(trim((string) $xmlResult->dados->situacao)) . '"');

                    $msg .= "idStatus: $status->id _ statusOmni: $status->statusOmni XML: $result->propostaResponse";

                    /*
                      Regra de negócio:  caso a proposta venha negada automaticamente, e se, no cadastro do cliente
                      tiver marcado como "Cliente da casa", devemos solicitar à analista, que analise esta proposta.
                     */

                    $criadoDesde = $util->diffTime($this->data_cadastro, date('Y-m-d H:i:s'));

                    $pOmniCfg = $this->hasOmniConfig();  //busque o registro config relacionado a esta proposta

                    if ($pOmniCfg !== null) {
                        $tabelaHost = $pOmniCfg->tabelaHost;
                        $usuarioHost = $pOmniCfg->usuarioHost;
                    } else {
                        $tabelaHost = "";
                        $usuarioHost = "";
                    }

                    if ($status->id == 3 && ($criadoDesde > 0 && $criadoDesde < 120)) {

                        if (
                                ($usuarioHost == "01630LHSTCREDSHOW") && ($idGrupoFilial == 4 || $idGrupoFilial == 7
                                || $idGrupoFilial == 21
                                )
                        ) {

                            /*
                            if (!($this->enviando)) {

                                $this->enviando = 1;

                                if ($this->update()) {

                                    // Chamar o envio da proposta para a Omni aqui
                                    $retornoSend = $this->sendToOmni($this, $this->analiseDeCredito, $this->analiseDeCredito->cliente);

                                    if ($this->Status_Proposta_id == 10 || $this->Status_Proposta_id == 3) {
                                        $this->Status_Proposta_id = 4;
                                    }

                                    $this->update();

                                    $tipoRetorno = $retornoSend['tipo'];
                                    $retornoOmni = $retornoSend['resposta'];
                                    $tabelaHost = $retornoSend['tabelaHost'];
                                    $usuarioHost = $retornoSend['usuarioHost'];

                                    //caso o tipo de retorno seja 1, significa que eh uma resposta a requisicao xml
                                    if ($tipoRetorno == 1) {

                                        //caso o retorno tenha idProposta, significa que deu certo a criacao no host do WS
                                        if ($retornoOmni->dados->idProposta != null && !empty((string) $retornoOmni->dados->idProposta)) {

                                            //atraves de um de-para, encontre o status (do sigac) retornado pelo WS da financeira
                                            $status = StatusProposta::model()->find('statusOmni = "' . (string) $retornoOmni->dados->situacao . '"');

                                            //atribua o codigo da proposta com o codigo retornado pelo host do WS
                                            $this->codigo = (string) $retornoOmni->dados->idProposta;
                                            $this->update(); //salve a proposta
                                            $retorno['hasErrors'] = false;
                                            $mensagem = '';


                                              //chame o omniConfigSave. Funcao que ira atualizar os registros de WS dessa proposta
                                              //junto com os dados retornados pela financeira parceira


                                            $this->omniConfigSave(null, $tabelaHost, $usuarioHost);

                                            $retorno['msgConfig']['pnotify'][] = array(
                                                'titulo' => 'Proposta enviada com sucesso! Código: ',
                                                'texto' => 'Deu tudo certo.',
                                                'tipo' => 'success',
                                            );

                                            $retorno['propostaConfig'] = array(
                                                'id' => $this->id,
                                                'codigo' => $this->codigo
                                            );

                                            $sigacLog = new SigacLog;
                                            $sigacLog->saveLog('Proposta Enviada', 'Proposta', $this->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Crediarista " . Yii::app()->session['usuario']->nome_utilizador . " enviou para análise a proposta com código: " . $this->codigo, "127.0.0.1", null, Yii::app()->session->getSessionId());
                                        } else {

                                            $passos .= "7";

                                            $erros = "";

                                            if (isset($retornoOmni->erros)) {

                                                foreach ($retornoOmni->erros->erro as $er) {
                                                    $erros .= (string) $er->descricao . chr(13) . chr(10);
                                                }
                                            } else {
                                                $erros = 'Não foi possível enviar a proposta. Entre em contato com o suporte.';
                                            }

                                            //Mensagem::model()->nova($mensagem, $this->getDialogo(), Yii::app()->session['usuario']->id);

                                            $h = fopen("logsOmni/erros_$pOmniCfg->codigoOmni", 'a+');
                                            fwrite($h, $erros . $xmlString);
                                            fclose($h);
                                        }
                                    }
                                    else {

                                        $sigacLog = new SigacLog;
                                        $sigacLog->saveLog('Proposta com erros no reaproveitamento', 'Proposta', $this->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Proposta apresentou erro no WS quando estava tentando virar o codigoLojista. Erro: $retornoOmni", "127.0.0.1", null, Yii::app()->session->getSessionId());
                                    }
                                }
                            }*/
                        }

                        //vieira e flávio
                        elseif ($idGrupoFilial == 5 || $idGrupoFilial == 16 || $idGrupoFilial == 19) {
                            /*$this->Financeira_id = 11;
                            $omniConfigs = PropostaOmniConfig::model()->findAll('Proposta = ' . $this->id);
                            foreach ($omniConfigs as $oc) {
                                $oc->habilitado = 0;
                                $oc->update();
                            }
                            $this->codigo = "4" . str_pad($this->id, 6, 0, STR_PAD_LEFT);
                            $this->Status_Proposta_id = 4;
                            $this->update();
                            */
                        }

                        else {

                            if ($this->Financeira_id != 7) {

                                $this->analise_solicitada = 0;
                            }

                            $this->update();
                        }
                    }

                    /* FIM da regra */

                    if ($status->id == 1) {
                        //                        $passos .= "_10";
                        $this->analise_solicitada = 0;
                    }

                    if ($status->id != 10 && $this->Financeira_id != 11) {

                        if (trim($this->codigo) == "9512697") {

                            $msg .= "passo3_";
                        }

                        //                        $passos .= "_11";
                        $this->Status_Proposta_id = $status->id;  //atribua o novo status da proposta
                        $this->update();                                      //atualize os dados da proposta
                    }

                    if (in_array($status->id, array(1, 2))) { //2 = proposta aprovada; 1 = proposta em analise
                        if ($status->id == 2) {

                            if (trim($this->codigo) == "9512697") {

                                $msg .= "passo4_";
                            }

                            $this->numero_do_contrato = (string) $xmlResult->dados->contrato;
                            $this->update();
                        }

                        ///////////////////////////////////////////////////////////////////////////////////////
                        //chame o omniConfigSave. Funcao que ira atualizar os registros de WS dessa proposta //
                        //junto com os dados retornados pela financeira parceira                             //
                        ///////////////////////////////////////////////////////////////////////////////////////
                        $this->omniConfigSave($pOmniCfg, $tabelaHost, $usuarioHost);
                    }
                }
                catch (SoapFault $e) {
                    //////////////////////////////////////////////////////////////////////////////////////////
                    //caso chegue ate este ponto, significa que houve algum soapFault e retorna uma excessao//
                    //monte uma mensagem de erro e retorne para que o analista possa verifica-lo e trata-lo //
                    //////////////////////////////////////////////////////////////////////////////////////////

                    $mensagem = 'Erros durante a comunicação com o WS' . Chr(13) . Chr(10);
                    $mensagem .= "Data: " . date("d/m/Y");
                    $mensagem .= $e->getMessage() . Chr(13) . Chr(10); //retorna a string da excessao
                    $mensagem .= 'Favor, contacte o setor de Desenvolvimento';

                    //Mensagem::model()->nova($mensagem, $this->getDialogo(), Yii::app()->session['usuario']->id);

                    $h = fopen('logsOmni/' . strtoupper($this->analiseDeCredito->cliente->pessoa->nome) . 'errorUpdateOmniStatus.xml', 'a+');
                    fwrite($h, $mensagem);
                    fclose($h);
                }
            } else {
                $msg = "passo9($this->Status_Proposta_id)_";
            }

        }

    }

    public function omniConfigSemear($omniConfig, $numPropLoj) {
        if ($omniConfig == null) {
            $omniConfig = new PropostaOmniConfig;
            $omniConfig->Proposta = $this->id; //atribua o valor do id desta proposta
            $omniConfig->codigoOmni = $this->codigo; //atribua o codigo da proposta retornado pelo WS
            $omniConfig->codigoSigac = null;
            $omniConfig->habilitado = 1;
            $omniConfig->interno = 0; //nao eh interno pois o banco  esta em poder desta
            $omniConfig->numPropLoj = (string) $numPropLoj;
            if ($omniConfig->save()) {
                return [
                    'retorno' => true,
                    'msg' => 'Funcionou'
                ];
            } else {
                ob_start();
                var_dump($omniConfig->getErrors());
                $resultado = ob_get_clean();

                return [
                    'retorno' => false,
                    'msg' => $resultado
                ];
            }
        }
    }

    public function omniConfigSave($omniCfg, $tabelaHost = null, $usuarioHost = null) {

        ////////////////////////////////////////////////////////////////////////////////////
        //caso o objeto passado esteja nulo, significa que sera uma inclusao. deste modo, //
        //crie um novo registro                                                           //
        ////////////////////////////////////////////////////////////////////////////////////
        if ($omniCfg == null) {

            $pOmniConfigs = PropostaOmniConfig::model()->findAll("habilitado AND Proposta = $this->id");

            foreach ($pOmniConfigs as $poc) {

                if ($poc->usuarioHost == "01630LHSTMAREMANSA") {

                    $message = new YiiMailMessage;
                    $message->view = "mandaMailFichamento";
                    $message->subject = 'Config Save Erro Duplicidade';

                    $parametros = [
                        'email' => [
                            'link' => "",
                            'titulo' => "Teste Update OMNI",
                            'mensagem' => "enviando: $this->enviando",
                            'tipo' => "1"
                        ]
                    ];

                    $message->setBody($parametros, 'text/html');

                    $message->addTo('contato@totorods.com');
                    $message->from = 'no-reply@credshow.com.br';

                    Yii::app()->mail->send($message);
                }

                $poc->habilitado = 0;

                if (!$poc->update()) {
                    //lembrar de por um codigo que reverta aqui
                }
            }

            $omniCfg = new PropostaOmniConfig;
            $omniCfg->Proposta = $this->id; //atribua o valor do id desta proposta
            $omniCfg->codigoOmni = $this->codigo; //atribua o codigo da proposta retornado pelo WS
            $omniCfg->codigoSigac = null;
            $omniCfg->habilitado = 1;
            $omniCfg->interno = 0; //nao eh interno pois a omni esta em poder desta
            $omniCfg->usuarioHost = $usuarioHost;
            $omniCfg->tabelaHost = $tabelaHost;
            $omniCfg->data_cadastro = date("Y-m-d H:i:s");
            //            $omniCfg->enviando      = 0                         ;

            if ($omniCfg->save()) {

                /*                if($enviou)
                  { */

                $this->enviando = 0;

                if (!$this->update()) {

                }

                //                }
            } else {

            }
        }

        if ($this->Status_Proposta_id == 2 && !$this->titulos_gerados && $this->Financeira_id != 7) { //caso a proposta tenha sido aprovada, atribua as url's de impressao
            //
            if ($usuarioHost == null) {

                if (isset($this->analiseDeCredito->filial)) {
                    $nucleo = NucleoFiliais::model()->find("habilitado AND id = " . $this->analiseDeCredito->filial->NucleoFiliais_id);
                } else {
                    $nucleo = null;
                }

                if (isset($nucleo->GrupoFiliais_id)) {
                    $idGrupoFilial = $nucleo->GrupoFiliais_id;
                } else {
                    $idGrupoFilial = 0;
                }

                /*if ($idGrupoFilial == 4 || $idGrupoFilial == 7 //|| $idGrupoFilial == 16
                        || $idGrupoFilial == 21) {
                    $chave = "01630LHSTMAREMANSA";
                } else {*/
                    $chave = "01630LHSTCREDSHOW";
            //                }
            } else {

                $chave = $usuarioHost;
            }

            //monte a string que ira gerar o objeto xml para obter a url de impressao de contrato
            $xmlStringContrato = '<Envelope>
                    <Body>
                       <CM_PROPOSTA>
                          <proposta xmlns:tem="http://tempuri.org/">
                             <identificacao>
                                 <chave>' . $chave . '</chave>
                                 <senha>' . $this->getSenhaXML() . '</senha>
                             </identificacao>
                             <dados>
                                <operacao>imprimir cedula</operacao>
                                <idProposta>' . $this->codigo . '</idProposta>
                             </dados>
                          </proposta>
                       </CM_PROPOSTA>
                    </Body>
                 </Envelope>';

            //monte a string que ira gerar o objeto xml para obter a url de impressao de boletos
            $xmlStringCarne = '<Envelope>
                    <Body>
                       <CM_PROPOSTA>
                          <proposta xmlns:tem="http://tempuri.org/">
                             <identificacao>
                                 <chave>' . $chave . '</chave>
                                 <senha>' . $this->getSenhaXML() . '</senha>
                             </identificacao>
                             <dados>
                                <operacao>imprimir carne</operacao>
                                <idProposta>' . $this->codigo . '</idProposta>
                             </dados>
                          </proposta>
                       </CM_PROPOSTA>
                    </Body>
                 </Envelope>';

            $util = new Util;

            //obtenha o objeto XML com a url de contrato e carne, respectivamente
            $configContrato = $util->callSoapOmni($xmlStringContrato, $this->getLinkXML());
            $configCarne = $util->callSoapOmni($xmlStringCarne, $this->getLinkXML());

            //if ($configContrato != null) { //caso o resultado nao seja nulo
            if (isset($configContrato->impressao->impressao->url)) { //caso o resultado nao seja nulo
                $omniCfg->urlContrato = (string) $configContrato->impressao->impressao->url;
            } else { //caso o resultado seja nulo, informe que houve erro na obtencao da url
                $omniCfg->urlContrato = "Erro ao obter url do contrato.";
            }

            //if ($configCarne != null) {
            if (isset($configCarne->impressao->impressao->url)) {
                $omniCfg->urlBoleto = (string) $configCarne->impressao->impressao->url;
                //$this->titulos_gerados  = 1;
            } else { //caso o resultado seja nulo, informe que houve erro na obtencao da url
                $omniCfg->urlBoleto = "Erro ao obter url dos boletos.";
            }

            if ($usuarioHost !== null) {
                $omniCfg->usuarioHost = $usuarioHost;
            }

            if ($tabelaHost !== null) {
                $omniCfg->tabelaHost = $tabelaHost;
            }

            $omniCfg->update(); //salve o objeto omniCfg
        }
    }

    public function alterarCondicoesOmni($novaMensagem = NULL) {
        $util = new Util;
        $referencias = $this->analiseDeCredito->cliente->listReferencias();

        $conteudoNovaMensagem = '';

        if ($novaMensagem != NULL) {
            $conteudoNovaMensagem = $novaMensagem;
        }

        $tabela = $this->tabelaCotacao;
        $tipoFinanciamento = 0;

        //sem juros
        if ($tabela->id == 14 || $tabela->id == 17 || $tabela->id == 16 || $tabela->id == 33 || $tabela->id == 37 || $tabela->id == 38) {
            $tipoFinanciamento = $this->getTipoFinanciamentoXML(2, null, null, 1);
        }

        else if ($tabela->id == 15)
        {
            $tipoFinanciamento = $this->getTipoFinanciamentoXML(13, null, null, 1);
        }
        else if($tabela->id == 27 || $tabela->id == 28 || $tabela->id == 36 || $tabela->id == 39)
        {
            $tipoFinanciamento = $this->getTipoFinanciamentoXML(1, null, null, 1);
        } else if ($tabela->id == 24) {
            $tipoFinanciamento = $this->getTipoFinanciamentoXML(3, null, null, 1);
        } else if ($tabela->id == 62){
            $tipoFinanciamento = $this->getTipoFinanciamentoXML(12, null, null, 1);
        }

        //flavio
        else if ($tabela->id == 34 || $tabela->id == 35) {
            $tipoFinanciamento = $this->getTipoFinanciamentoXML(7, null, null, 1);
        } else if ($tabela->id == 30 || $tabela->id == 26) {
            $tipoFinanciamento = $this->getTipoFinanciamentoXML(4, null, null, 1);
        } else if ($tabela->id == 44) {
            $tipoFinanciamento = $this->getTipoFinanciamentoXML(11, null, null, 1);
        }

        $nomeCliente = str_replace(["&", "'"], ['e', ''], strtoupper(trim($this->analiseDeCredito->cliente->pessoa->nome)));
        $nomeCidade = str_replace(["&", "'"], ['e', ''], strtoupper(trim($this->analiseDeCredito->cliente->getCidade()->nome)));
        $nomeDaMae = str_replace(["&", "'"], ['e', ''], trim(strtoupper($this->analiseDeCredito->cliente->getCadastro()->nome_da_mae)));
        $nomeDoPai = str_replace(["&", "'"], ['e', ''], trim(strtoupper($this->analiseDeCredito->cliente->getCadastro()->nome_do_pai)));
        $nomeRua = str_replace(["&", "'"], ['e', ''], strtoupper(trim($this->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->logradouro)));
        $nomeBairro = str_replace(["&", "'"], ['e', ''], strtoupper(trim($this->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->bairro)));

        $xmlEnderecoProfissional = '';
        $xmlReferencias = '';
        $telefoneProfissional = $this->analiseDeCredito->cliente->pessoa->getDadosProfissionais()->contato->getLasTelefone();

        if (count($referencias) > 0) {
            foreach ($referencias as $referencia) {
                $xmlReferencias .= $referencia->toXMLOmni();
            }
        }

        if ($telefoneProfissional == NULL) {
            $telefoneProfissional = $this->analiseDeCredito->cliente->pessoa->contato->getLasTelefone();
        }

        if ($this->analiseDeCredito->cliente->pessoa->getDadosProfissionais() != NULL && $this->analiseDeCredito->cliente->pessoa->getDadosProfissionais()->endereco != NULL) {
            $xmlEnderecoProfissional = $this->analiseDeCredito->cliente->pessoa->getDadosProfissionais()->endereco->enderecoToXml('emprego');
        } else {
            $xmlEnderecoProfissional = $this->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->enderecoToXml('emprego');
        }

        //se cliente for da casa, marque-o com vip no xml
        if ($this->analiseDeCredito->cliente->getCadastro()->cliente_da_casa) {
            $clienteVIP = "S";
        } else {
            $clienteVIP = "N";
        }

        $tipoOcupacao = 9;

        /*
         * 15-02-2016
         * Muitos endereços possuem esta propriedade como NULL. Não enviava as alterações.
         * Eric
         */
        if ($this->analiseDeCredito->cliente->pessoa->getDadosProfissionais()->tipoOcupacao != NULL) {
            $tipoOcupacao = $this->analiseDeCredito->cliente->pessoa->getDadosProfissionais()->tipoOcupacao->codOmni;
        }
        /*
          ------
         */

        $pOmniConfig = $this->hasOmniConfig();

        //        $passos = "";

        if (isset($pOmniConfig->usuarioHost)) {

            //            $passos .= "_$pOmniConfig->usuarioHost";

            $chave = $pOmniConfig->usuarioHost;
        } else {

            if ($pOmniConfig != null) {
                ob_start();
                var_dump($pOmniConfig->getAttributes());
                $resultadoOmniCfg = ob_get_clean();
            }

            //            $passos = "_$resultadoOmniCfg";

            if (isset($this->analiseDeCredito->filial)) {
                $nucleo = NucleoFiliais::model()->find("habilitado AND id = " . $this->analiseDeCredito->filial->NucleoFiliais_id);
            } else {
                $nucleo = null;
            }

            if (isset($nucleo->GrupoFiliais_id)) {
                $idGrupoFilial = $nucleo->GrupoFiliais_id;
            } else {
                $idGrupoFilial = 0;
            }

            /*if ($idGrupoFilial == 4 || $idGrupoFilial == 7 //|| $idGrupoFilial == 16
                    || $idGrupoFilial == 21) {

                //                $passos .= "_voti";

                $chave = "01630LHSTMAREMANSA";
            } else {*/
                //                $passos .= "_idGrupoFilial: $idGrupoFilial";
                $chave = "01630LHSTCREDSHOW";
        //            }
        }

        /*        $message            = new YiiMailMessage    ;
          $message->view      = "mandaMailFichamento" ;
          $message->subject   = 'Teste Update Omni'   ;

          $parametros         =   [
          'email' =>  [
          'link'      => ""                   ,
          'titulo'    => "Teste Update OMNI"  ,
          'mensagem'  => $passos              ,
          'tipo'      => "1"
          ]
          ];

          $message->setBody($parametros, 'text/html');

          $message->addTo('contato@totorods.com');
          $message->from = 'no-reply@credshow.com.br';

          Yii::app()->mail->send($message); */

        if ( in_array( $this->analiseDeCredito->filial->NucleoFiliais_id, array(40) ) ){
          $chave = "01630LHSTMAREMANSA2";
        }

        $xmlServicoSeguro = '';
        $valorDoSeguroOmni = 0;

        if( $this->segurada ){
          $valorDoSeguroOmni = Proposta::model()->calcularValorSeguroOmni($this->valor, $this->valor_entrada);
          $xmlServicoSeguro  = '<servicos>
            <servico>
              <idServico>11</idServico>
              <valor>'.number_format($valorDoSeguroOmni, 2, '.', '').'</valor>
              <quantidade>1</quantidade>
            </servico>
          </servicos>';
        }
        else{
          $xmlServicoSeguro = '';
        }
        $xmlString = '<Envelope>
          <Body>
              <CM_PROPOSTA>
                  <proposta xmlns:tem="http://tempuri.org/">
                      <identificacao>
                          <chave>' . $chave . '</chave>
                          <senha>' . $this->getSenhaXML() . '</senha>
                      </identificacao>
                      <dados>
                         <operacao>ALTERAR</operacao>
                         <idProposta>' . $this->codigo . '</idProposta>
                         <numeroProposta/>
                         <codigoLoja>' . $this->getLojaXML(1, $this, 1) . '</codigoLoja>
                         <descricaoBem>' . $this->analiseDeCredito->mercadoria . '</descricaoBem>
                         <tipoFinanciamento>' . $tipoFinanciamento . '</tipoFinanciamento>
                         <taxaMensal>' . $this->tabelaCotacao->taxa . '</taxaMensal>
                         <valorTC>0.0000</valorTC>
                         <valorServico/>
                         <valorBem>' . $this->getValorFinanciadoOmni() . '</valorBem>
                         <valorEntrada>' . $this->valor_entrada . '</valorEntrada>
                         <plano>' . $this->qtd_parcelas . '</plano>
                         <valorParcela>' . $this->getValorParcela() . '</valorParcela>
                         <dataCompra>' . substr($this->data_cadastro, 0, 10) . '</dataCompra>
                         <valorIOF>0.00</valorIOF>
                         <dataVencimento>' . date('Y-m-d', strtotime(substr($this->data_cadastro, 0, 10) . ' +' . $this->carencia . ' days')) . '</dataVencimento>
                         <comentario>' . $conteudoNovaMensagem . '</comentario>
                      </dados>'.
                      $xmlServicoSeguro.'
                      <cliente>
                          <nome>' . $nomeCliente . '</nome>
                          <genero>' . $this->analiseDeCredito->cliente->pessoa->sexo . '</genero>
                          <dataNascimento>' . $this->analiseDeCredito->cliente->pessoa->nascimento . '</dataNascimento>
                          <naturalidade cidade="' . $nomeCidade . '" estado="' . strtoupper($this->analiseDeCredito->cliente->getCidade()->uf) . '" />
                          <nomeMae>' . $nomeDaMae . '</nomeMae>
                          <nomePai>' . $nomeDoPai . '</nomePai>
                          <escolaridade>1</escolaridade>
                          <ocupacao>' . $tipoOcupacao . '</ocupacao>
                          <estadoCivil>' . $this->analiseDeCredito->cliente->pessoa->estadoCivil->codOmni . '</estadoCivil>
                          <clienteVIP>' . $clienteVIP . '</clienteVIP>
                          <dependentes>' . $this->analiseDeCredito->cliente->getCadastro()->numero_de_dependentes . '</dependentes>
                          <documentos>
                              <documento>
                                  <tipo>cpf</tipo>
                                  <numero>' . $this->analiseDeCredito->cliente->pessoa->getCPF()->numero . '</numero>
                              </documento>
                              <documento>
                                   <tipo>rg</tipo>
                                   <numero>' . $this->analiseDeCredito->cliente->pessoa->getRG()->numero . '</numero>
                                   <data>' . $this->analiseDeCredito->cliente->pessoa->getRG()->data_emissao . '</data>
                                   <ufemissor>' . $this->analiseDeCredito->cliente->pessoa->getRG()->uf_emissor . '</ufemissor>
                              </documento>
                          </documentos>
                          <enderecos>
                            <endereco>
                              <tipo>' . strtoupper($this->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->tipoMoradia->tipo) . '</tipo>
                              <rua>' . $nomeRua . '</rua>
                              <numero>0</numero>
                              <complemento>NR:' . $this->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->numero . ' ' . strtoupper($this->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->complemento) . '</complemento>
                              <bairro>' . $nomeBairro . '</bairro>
                              <cidade>' . $nomeCidade . '</cidade>
                              <estado>' . strtoupper($this->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->uf) . '</estado>
                              <cep>' . strtoupper($this->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cep) . '</cep>
                              <condicao>' . $this->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->tipoMoradia->tipoOmni . '</condicao>
                            </endereco>'
                . $xmlEnderecoProfissional .
                '</enderecos>
                          <telefones>
                          <telefone>
                            <tipo>emprego</tipo>
                            <ddd>' . $telefoneProfissional->getDDD() . '</ddd>
                            <numero>' . $telefoneProfissional->getNumero() . '</numero>
                          </telefone>' . $this->analiseDeCredito->cliente->pessoa->contato->telefonesToXml()
                . $xmlReferencias .
                '</telefones>
                          <empregos>
                            <emprego>
                               <empresa>' . strtoupper(str_replace(['-', '&'], ['', 'e'], trim($this->analiseDeCredito->cliente->pessoa->getDadosProfissionais()->empresa))) . '</empresa>
                               <cargo>' . $tipoOcupacao . '</cargo>
                               <salario>' . number_format($this->analiseDeCredito->cliente->pessoa->getDadosProfissionais()->renda_liquida, 2, '.', '') . '</salario>
                               <tempo Anos="' . $this->analiseDeCredito->cliente->pessoa->getDadosProfissionais()->tempo_de_servico_anos . '" Meses="' . $this->analiseDeCredito->cliente->pessoa->getDadosProfissionais()->tempo_de_servico_meses . '"/>
                            </emprego>
                          </empregos>
                      </cliente>
                  </proposta>
              </CM_PROPOSTA>
          </Body>
      </Envelope>';

        $h = fopen('logsOmni/' . $this->codigo . 'alterarCondicoes.xml', 'a+');
        fwrite($h, $xmlString);
        fclose($h);

        return $util->callSoapOmni($xmlString, $this->getLinkXML());
    }

    public function pendenciaSemear($mensagem) {
        $util = new Util;
        $referencias = $this->analiseDeCredito->cliente->listReferencias();
        $telefoneProfissional = $this->analiseDeCredito->cliente->pessoa->getDadosProfissionais()->contato->getLasTelefone();
        $xmlReferencias = '';

        if ($telefoneProfissional == NULL) {
            $telefoneProfissional = $this->analiseDeCredito->cliente->pessoa->contato->getLasTelefone();
        }

        if (count($referencias) > 0) {
            foreach ($referencias as $referencia) {
                $xmlReferencias .= $referencia->toXMLSemear();
            }
        }

        $nomeCliente = str_replace(["&", "'"], ['e', ''], strtoupper(trim($this->analiseDeCredito->cliente->pessoa->nome)));
        $nomeCidade = str_replace(["&", "'"], ['e', ''], strtoupper(trim($this->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cidade)));
        $nomeBairro = str_replace(["&", "'"], ['e', ''], strtoupper(trim($this->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->bairro)));
        $nomeRua = str_replace(["&", "'"], ['e', ''], strtoupper(trim($this->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->logradouro)));

        $ContaBanco = $this->analiseDeCredito->cliente->pessoa->getDadosBancarios();
        if ($ContaBanco != NULL) {

            if ($ContaBanco->Tipo_Conta_Bancaria_id == 1) {
                $flagConta = 'CC';
            } else {
                $flagConta = 'CP';
            }

            $posicaoA = strpos($ContaBanco->agencia, '-');
            if ($posicaoA != 0 || $posicaoA === true) {
                $agenciaBanco = substr($ContaBanco->agencia, 0, $posicaoA);
            } else {
                $agenciaBanco = $ContaBanco->agencia;
            }

            $posicaoN = strpos($ContaBanco->numero, '-');
            if ($posicaoN != 0 || $posicaoN === true) {
                $numeroConta = substr($ContaBanco->numero, 0, $posicaoN);
            } else {
                $numeroConta = $ContaBanco->numero;
            }

            $banco = Banco::model()->find('id = ' . $ContaBanco->Banco_id)->codigo;
        } else {
            $flagConta = '';
            $agenciaBanco = '';
            $numeroConta = '';
            $banco = '';
        }

        if ($this->analiseDeCredito->cliente->pessoa->estadoCivil->codOmni == 1) {
            $estadoCivil = "CA";
        }
        if ($this->analiseDeCredito->cliente->pessoa->estadoCivil->codOmni == 2) {
            $estadoCivil = "SO";
        }
        if ($this->analiseDeCredito->cliente->pessoa->estadoCivil->codOmni == 3) {
            $estadoCivil = "VI";
        }
        if ($this->analiseDeCredito->cliente->pessoa->estadoCivil->codOmni == 5) {
            $estadoCivil = "DI";
        }

        $headers = array(
            'Content-Type:text/xml;charset=UTF-8',
            'Connection: close'
        );

        /* $poc = Yii::app()->db->createCommand("SELECT MAX(numPropLoj) as 'npl' FROM nordeste2.PropostaOmniConfig")->queryRow();
          $numPropLoj = $poc['npl'] + 1; */

        $xml = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ws='http://ws.host.banco.venda.jretail/'>
        <soapenv:Header/>
            <soapenv:Body>
            <ws:enviarPendenciaProposta>
             	<xmlIn>
			    <host>
			    <autenticacao>
			        <codUsr>hstcredshow</codUsr>
			        <pwdUsr>LMt46dc9ezpP1f34DQFpUA==</pwdUsr>
			    </autenticacao>
			    <proposta>
			        <codLoja>51130000</codLoja>
			        <numPropLoj>" . $this->hasOmniConfig()->numPropLoj . "</numPropLoj>
                                <obs>" . $mensagem['conteudo'] . "</obs>
			        <cliente>
			            <cpfCnpj>" . trim($this->analiseDeCredito->cliente->pessoa->getCPF()->numero) . "</cpfCnpj>
			            <nome>" . $nomeCliente . "</nome>
			            <sexo>" . $this->analiseDeCredito->cliente->pessoa->sexo . "</sexo>
			            <dtNasc>" . $util->bd_date_to_view(substr($this->analiseDeCredito->cliente->pessoa->nascimento, 0, 10)) . "</dtNasc>
			            <rgOrgEmi>" . $this->analiseDeCredito->cliente->pessoa->getRG()->orgao_emissor . "</rgOrgEmi>
			            <rgUfEmi>" . $this->analiseDeCredito->cliente->pessoa->getRG()->uf_emissor . "</rgUfEmi>
			            <rgNum>" . $this->analiseDeCredito->cliente->pessoa->getRG()->numero . "</rgNum>
			            <rgDtEmi>" . $util->bd_date_to_view(substr($this->analiseDeCredito->cliente->pessoa->getRG()->data_emissao, 0, 10)) . "</rgDtEmi>
			            <estCivil>" . $estadoCivil . "</estCivil>
			            <nacionalidade>B</nacionalidade>
			            <naturalidade>" . strtoupper(trim($this->analiseDeCredito->cliente->pessoa->cidade->nome)) . "</naturalidade>
			            <naturalidadeUf>" . strtoupper(trim($this->analiseDeCredito->cliente->pessoa->cidade->uf)) . "</naturalidadeUf>
                                    <escolaridade>OU</escolaridade>
			            <nomeMae>" . strtoupper(trim($this->analiseDeCredito->cliente->getCadastro()->nome_da_mae)) . "</nomeMae>
                                    <tipoRes>OU</tipoRes>
                                    <dtRes>01/01/2010</dtRes>
                  <tipoLogradouro>" . strtoupper($this->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->tipoMoradia->tipo) . "</tipoLogradouro>
                  <logradouro>" . $nomeRua . "</logradouro>
                  <numero>" . strtoupper($this->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->numero) . "</numero>
                  <bairro>" . $nomeBairro . "</bairro>
                  <localidade>" . $nomeCidade . "</localidade>
                  <uf>" . strtoupper($this->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->uf) . "</uf>
                  <cep>" . str_replace('-', '', $this->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cep) . "</cep>
                  <ddd>" . $telefoneProfissional->getDDD() . "</ddd>
                  <fone>" . substr($telefoneProfissional->getNumero(), -8) . "</fone>
                  <renda>
                      <tipoRenda>OUT</tipoRenda>
                      <nome>" . strtoupper(trim($this->analiseDeCredito->cliente->pessoa->getDadosProfissionais()->profissao)) . "</nome>
                      <logradouro>" . strtoupper(trim($this->analiseDeCredito->cliente->pessoa->getDadosProfissionais()->endereco->logradouro)) . "</logradouro>
                      <numero>" . strtoupper(trim($this->analiseDeCredito->cliente->pessoa->getDadosProfissionais()->endereco->numero)) . "</numero>
                      <bairro>" . strtoupper(trim($this->analiseDeCredito->cliente->pessoa->getDadosProfissionais()->endereco->bairro)) . "</bairro>
                      <localidade>" . strtoupper(trim($this->analiseDeCredito->cliente->pessoa->getDadosProfissionais()->endereco->cidade)) . "</localidade>
                      <uf>" . strtoupper(trim($this->analiseDeCredito->cliente->pessoa->getDadosProfissionais()->endereco->uf)) . "</uf>
                      <cep>" . str_replace('-', '', trim($this->analiseDeCredito->cliente->pessoa->getDadosProfissionais()->endereco->cep)) . "</cep>
                      <cargo>" . strtoupper(trim($this->analiseDeCredito->cliente->pessoa->getDadosProfissionais()->profissao)) . "</cargo>
                                        <dtAdmissao>01/01/2013</dtAdmissao>
                      <renda>" . number_format($this->analiseDeCredito->cliente->pessoa->getDadosProfissionais()->renda_liquida, 2, '.', '') . "</renda>
                                        <ddd1>" . strtoupper(trim($this->analiseDeCredito->cliente->pessoa->getDadosProfissionais()->contato->contatoHasTelefones[0]->telefone->discagem_direta_distancia)) . "</ddd1>
                                        <fone1>" . substr(trim($this->analiseDeCredito->cliente->pessoa->getDadosProfissionais()->contato->contatoHasTelefones[0]->telefone->numero), -8) . "</fone1>
                  </renda>
                                    <referencias>
                                        " . $xmlReferencias . "
                                    </referencias>
                                    <banco>
                              <flagTipoConta>" . $flagConta . "</flagTipoConta>
                              <banco>" . $banco . "</banco>
                              <agencia>" . $agenciaBanco . "</agencia>
                              <conta>" . $numeroConta . "</conta>
                                    </banco>
              </cliente>
          </proposta>
                            </host>
          </xmlIn>
            </ws:enviarPendenciaProposta>
            </soapenv:Body>
        </soapenv:Envelope>";

        $file = fopen('semear', 'w');
        fwrite($file, $xml);
        fclose($file);

        $ch = curl_init('https://correspondente.bancosemear.com.br:8443/jretail-jretail.ejb/PropostaHostWSSSL');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);

        $retorno = curl_exec($ch);

        $objXML = simplexml_load_string($retorno);
        $objXML->registerXPathNamespace('env', 'http://schemas.xmlsoap.org/soap/envelope/');
        $objXML->registerXPathNamespace('ws', 'http://ws.host.banco.venda.jretail/');

        $resultado = $objXML->xpath("//ws:enviarPendenciaPropostaResponse");

        $resp = $resultado[0]->xpath('xmlOut');

        if ($resp) {
            $file2 = fopen('semear2', 'w');
            fwrite($file2, $resp[0]);
            fclose($file2);

            $cdata = simplexml_load_string($resp[0]);

            $response = $cdata->xpath('proposta/status')[0];

            $arrReturn = array();

            if ($response != 'ERR' && $response != NULL && $response != '') {
                $arrReturn = array(
                    "msg" => "Proposta devolvida com sucesso!",
                    "retorno" => (string) $cdata->xpath('proposta/retorno')[0],
                    "tipo" => "success"
                );
            } else {
                $arrReturn = array(
                    "msg" => "Erro na devolução da proposta!",
                    "retorno" => (string) $cdata->xpath('proposta/retorno')[0],
                    "tipo" => "error"
                );
            }
        } else {
            $arrReturn = array(
                "msg" => "Erro na devolução da proposta!",
                "retorno" => (string) $cdata->xpath('proposta/retorno')[0],
                "tipo" => "error"
            );
        }

        return $arrReturn;
    }

    public function sendToSemear($proposta, $analise, $cliente) {

        $util = new Util;
        $tabela = $proposta->tabelaCotacao;
        $referencias = $cliente->listReferencias();
        $telefoneProfissional = $cliente->pessoa->getDadosProfissionais()->contato->getLasTelefone();
        $xmlReferencias = '';

        if ($tabela->taxa == 13.99) {
            $codTabela = '11E';
        } else {
            $codTabela = '11F';
        }

        if ($telefoneProfissional == NULL) {
            $telefoneProfissional = $cliente->pessoa->contato->getLasTelefone();
        }

        if (count($referencias) > 0) {
            foreach ($referencias as $referencia) {
                $xmlReferencias .= $referencia->toXMLSemear();
            }
        }

        $nomeCliente = str_replace(["&", "'"], ['e', ''], strtoupper(trim($analise->cliente->pessoa->nome)));
        $nomeCidade = str_pad(str_replace(["&", "'"], ['e', ''], strtoupper(trim($cliente->pessoa->getEnderecoCobranca()->cidade))), 15, "", STR_PAD_RIGHT);
        $nomeBairro = str_replace(["&", "'"], ['e', ''], strtoupper(trim($cliente->pessoa->getEnderecoCobranca()->bairro)));
        $nomeRua = str_replace(["&", "'"], ['e', ''], strtoupper(trim($cliente->pessoa->getEnderecoCobranca()->logradouro)));

        $ContaBanco = $cliente->pessoa->getDadosBancarios();
        if ($ContaBanco != NULL) {

            if ($ContaBanco->Tipo_Conta_Bancaria_id == 1) {
                $flagConta = 'CC';
            } else {
                $flagConta = 'CP';
            }

            $posicaoA = strpos($ContaBanco->agencia, '-');
            if ($posicaoA != 0 || $posicaoA === true) {
                $agenciaBanco = substr($ContaBanco->agencia, 0, $posicaoA);
            } else {
                $agenciaBanco = $ContaBanco->agencia;
            }

            $posicaoN = strpos($ContaBanco->numero, '-');
            if ($posicaoN != 0 || $posicaoN === true) {
                $numeroConta = substr($ContaBanco->numero, 0, $posicaoN);
            } else {
                $numeroConta = $ContaBanco->numero;
            }

            $banco = Banco::model()->find('id = ' . $ContaBanco->Banco_id)->codigo;
        } else {
            $flagConta = '';
            $agenciaBanco = '';
            $numeroConta = '';
            $banco = '';
        }

        if ($cliente->pessoa->estadoCivil->codOmni == 1) {
            $estadoCivil = "CA";
        }
        if ($cliente->pessoa->estadoCivil->codOmni == 2) {
            $estadoCivil = "SO";
        }
        if ($cliente->pessoa->estadoCivil->codOmni == 3) {
            $estadoCivil = "VI";
        }
        if ($cliente->pessoa->estadoCivil->codOmni == 5) {
            $estadoCivil = "DI";
        }

        $valorFin = $proposta->valor + $proposta->calcularValorDoSeguro();
        $tac = ($valorFin / 100) * 10;
        if ($tac > 150) {
            $tac = 150;
        }

        $headers = array(
            'Content-Type:text/xml;charset=UTF-8',
            'Connection: close'
        );

        $valorParcela = 0;

        if ($proposta->Tabela_id !== null) {
            $finSemear = (($proposta->getValorFinanciado() / 100) * 10) + $proposta->getValorFinanciado();
            $valores = Fator::model()->getValor($finSemear, $proposta->qtd_parcelas, $proposta->carencia, $proposta->Tabela_id);

            $valorParcela = $valores[0];
        }

        $poc = Yii::app()->db->createCommand("SELECT MAX(numPropLoj) as 'npl' FROM nordeste2.PropostaOmniConfig")->queryRow();
        $numPropLoj = $poc['npl'] + 1;

        $msg = Mensagem::model()->find('Dialogo_id = ' . $proposta->getDialogo()->id);

        $xml = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ws='http://ws.host.banco.venda.jretail/'>
        <soapenv:Header/>
            <soapenv:Body>
            <ws:enviarProposta>
             	<xmlIn>
			    <host>
			    <autenticacao>
			        <codUsr>hstcredshow</codUsr>
			        <pwdUsr>LMt46dc9ezpP1f34DQFpUA==</pwdUsr>
			    </autenticacao>
			    <proposta>
			        <codLoja>51130000</codLoja>
			        <tpOpe>2</tpOpe>
			        <dtCad>" . Date('d/m/Y') . "</dtCad>
			        <dtFin>" . Date('d/m/Y') . "</dtFin>
			        <numPropLoj>" . $numPropLoj . "</numPropLoj>
			        <tpVda>3077</tpVda>
			        <tabFin>" . $codTabela . "</tabFin>
			        <plano>" . $proposta->qtd_parcelas . "</plano>
			        <prestacao>" . $valorParcela . "</prestacao>
			        <vencAniv>N</vencAniv>
			        <carencia>" . $proposta->carencia . "</carencia>
			        <dtVenc1>" . $proposta->getVencimentoParcela(1) . "</dtVenc1>
			        <tc>" . number_format($tac, 2, '.', '') . "</tc>
			        <vrFin>" . number_format(($valorFin + $tac), 2, '.', '') . "</vrFin>
			        <taxa>" . $tabela->taxa . "</taxa>
                                <obs>" . $msg->conteudo . "</obs>
			        <cliente>
			            <cpfCnpj>" . trim($cliente->pessoa->getCPF()->numero) . "</cpfCnpj>
			            <nome>" . $nomeCliente . "</nome>
			            <sexo>" . $analise->cliente->pessoa->sexo . "</sexo>
			            <dtNasc>" . $util->bd_date_to_view(substr($analise->cliente->pessoa->nascimento, 0, 10)) . "</dtNasc>
			            <rgOrgEmi>" . substr($cliente->pessoa->getRG()->orgao_emissor, 0, 5) . "</rgOrgEmi>
			            <rgUfEmi>" . $cliente->pessoa->getRG()->uf_emissor . "</rgUfEmi>
			            <rgNum>" . $cliente->pessoa->getRG()->numero . "</rgNum>
			            <rgDtEmi>" . $util->bd_date_to_view(substr($cliente->pessoa->getRG()->data_emissao, 0, 10)) . "</rgDtEmi>
			            <estCivil>" . $estadoCivil . "</estCivil>
			            <nacionalidade>B</nacionalidade>
			            <naturalidade>" . strtoupper(trim($cliente->pessoa->cidade->nome)) . "</naturalidade>
			            <naturalidadeUf>" . strtoupper(trim($cliente->pessoa->cidade->uf)) . "</naturalidadeUf>
                                    <escolaridade>OU</escolaridade>
			            <nomeMae>" . strtoupper(trim($cliente->getCadastro()->nome_da_mae)) . "</nomeMae>
                                    <tipoRes>OU</tipoRes>
                                    <dtRes>01/01/2010</dtRes>
                  <tipoLogradouro>" . strtoupper($cliente->pessoa->getEnderecoCobranca()->tipoMoradia->tipo) . "</tipoLogradouro>
                  <logradouro>" . $nomeRua . "</logradouro>
                  <numero>" . strtoupper($cliente->pessoa->getEnderecoCobranca()->numero) . "</numero>
                  <bairro>" . $nomeBairro . "</bairro>
                  <localidade>" . strtoupper(trim($cliente->pessoa->getEnderecoCobranca()->cidade)) . "</localidade>
                  <uf>" . strtoupper($cliente->pessoa->getEnderecoCobranca()->uf) . "</uf>
                  <cep>" . str_replace('-', '', ($cliente->pessoa->getEnderecoCobranca()->cep)) . "</cep>
                  <ddd>" . $telefoneProfissional->getDDD() . "</ddd>
                  <fone>" . substr($telefoneProfissional->getNumero(), -8) . "</fone>
                  <renda>
                      <tipoRenda>OUT</tipoRenda>
                      <nome>" . strtoupper(trim($cliente->pessoa->getDadosProfissionais()->profissao)) . "</nome>
                      <logradouro>" . strtoupper(trim($cliente->pessoa->getDadosProfissionais()->endereco->logradouro)) . "</logradouro>
                      <numero>" . strtoupper(trim($cliente->pessoa->getDadosProfissionais()->endereco->numero)) . "</numero>
                      <bairro>" . strtoupper(trim($cliente->pessoa->getDadosProfissionais()->endereco->bairro)) . "</bairro>
                      <localidade>" . strtoupper(trim($cliente->pessoa->getDadosProfissionais()->endereco->cidade)) . "</localidade>
                      <uf>" . strtoupper(trim($cliente->pessoa->getDadosProfissionais()->endereco->uf)) . "</uf>
                      <cep>" . str_replace('-', '', trim($cliente->pessoa->getDadosProfissionais()->endereco->cep)) . "</cep>
                      <cargo>" . strtoupper(trim($cliente->pessoa->getDadosProfissionais()->profissao)) . "</cargo>
                                        <dtAdmissao>01/01/2013</dtAdmissao>
                      <renda>" . number_format($cliente->pessoa->getDadosProfissionais()->renda_liquida, 2, '.', '') . "</renda>
                                        <ddd1>" . strtoupper(trim($cliente->pessoa->getDadosProfissionais()->contato->contatoHasTelefones[0]->telefone->discagem_direta_distancia)) . "</ddd1>
                                        <fone1>" . substr(trim($cliente->pessoa->getDadosProfissionais()->contato->contatoHasTelefones[0]->telefone->numero), -8) . "</fone1>
                  </renda>
                                    <referencias>
                                        " . $xmlReferencias . "
                                    </referencias>
                                    <banco>
                              <flagTipoConta>" . $flagConta . "</flagTipoConta>
                              <banco>" . $banco . "</banco>
                              <agencia>" . $agenciaBanco . "</agencia>
                              <conta>" . $numeroConta . "</conta>
                                    </banco>
              </cliente>
          </proposta>
                            </host>
          </xmlIn>
            </ws:enviarProposta>
            </soapenv:Body>
        </soapenv:Envelope>";

        $file = fopen('semear', 'w');
        fwrite($file, $xml);
        fclose($file);

        $ch = curl_init('https://correspondente.bancosemear.com.br:8443/jretail-jretail.ejb/PropostaHostWSSSL');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);

        $retorno = curl_exec($ch);

        $objXML = simplexml_load_string($retorno);
        $objXML->registerXPathNamespace('env', 'http://schemas.xmlsoap.org/soap/envelope/');
        $objXML->registerXPathNamespace('ws', 'http://ws.host.banco.venda.jretail/');

        $resultado = $objXML->xpath("//ws:enviarPropostaResponse");

        $resp = $resultado[0]->xpath('xmlOut');

        if ($resp) {
            $file2 = fopen('semear2', 'w');
            fwrite($file2, $resp[0]);
            fclose($file2);

            $cdata = simplexml_load_string($resp[0]);

            $response = $cdata->xpath('proposta/status')[0];

            $arrReturn = array();

            if ($response != 'ERR' && $response != NULL && $response != '') {
                $arrReturn = array(
                    "msg" => "Proposta Enviada com Sucesso!",
                    "retorno" => $cdata->xpath('proposta/retorno')[0],
                    "status" => $cdata->xpath('proposta/status')[0],
                    "numProp" => $cdata->xpath('proposta/numProp')[0],
                    "numPropLoj" => $cdata->xpath('proposta/numPropLoj')[0],
                    "xml" => $xml
                );
            } else {
                $arrReturn = array(
                    "msg" => "Erro no envio da proposta!",
                    "retorno" => $cdata->xpath('proposta/retorno')[0],
                    "status" => $cdata->xpath('proposta/status')[0],
                    "xml" => $xml
                );
            }
        } else {
            $arrReturn = array(
                "msg" => "Erro no envio da proposta!",
                "retorno" => $cdata->xpath('proposta/retorno')[0],
                "status" => $cdata->xpath('proposta/status')[0],
                "xml" => $xml
            );
        }

        return $arrReturn;
    }

    public function sendToOmni($proposta, $analise, $cliente) {
        $arrFiliaisOmni = [122,125,124,126, 105, 108];
        $arrReturn = array();
        $result = "";
        $util = new Util;
        $tabela = $proposta->tabelaCotacao;
        $tipoFinanciamento = 0;
        $telefone = $cliente->pessoa->contato->getLasTelefone();
        $referencias = $cliente->listReferencias();

        $xmlServicoSeguro = '';

        //sem juros
        if ($tabela->id == 45 || $tabela->id == 17 || $tabela->id == 16 || $tabela->id == 33 || $tabela->id == 37 || $tabela->id == 38) {
            $tipoFinanciamento = $this->getTipoFinanciamentoXML(2, $proposta);
        }

        //com juros
        else if ($tabela->id == 15)
        {
            $tipoFinanciamento = $this->getTipoFinanciamentoXML(13, $proposta);
        }
        else if($tabela->id == 27 || $tabela->id == 28 || $tabela->id == 36 || $tabela->id == 39)
        {
            $tipoFinanciamento = $this->getTipoFinanciamentoXML(1, $proposta);
        } else if ($tabela->id == 24) {
            $tipoFinanciamento = $this->getTipoFinanciamentoXML(3, $proposta);
        } else if ($tabela->id == 34 || $tabela->id == 35) {
            $tipoFinanciamento = $this->getTipoFinanciamentoXML(7, $proposta);
        } else if ($tabela->id == 30 || $tabela->id == 26) {
            $tipoFinanciamento = $this->getTipoFinanciamentoXML(4, $proposta);
        } else if ($tabela->id == 44) {
            $tipoFinanciamento = $this->getTipoFinanciamentoXML(11, $proposta);
        } else if ($tabela->id == 62) {
            //$tipoFinanciamento = $this->getTipoFinanciamentoXML(12, null, null, 1);
            $tipoFinanciamento = $this->getTipoFinanciamentoXML(12, $proposta);
        }

        $xmlEnderecoProfissional = '';
        $telefoneProfissional = $cliente->pessoa->getDadosProfissionais()->contato->getLasTelefone();
        $xmlReferencias = '';
        $xmlConjuge = '';

        if ($telefoneProfissional == NULL) {
            $telefoneProfissional = $cliente->pessoa->contato->getLasTelefone();
        }

        if ($cliente->pessoa->getDadosProfissionais() != NULL && $cliente->pessoa->getDadosProfissionais()->endereco != NULL) {
            $xmlEnderecoProfissional = $cliente->pessoa->getDadosProfissionais()->endereco->enderecoToXml('emprego');
        } else {
            $xmlEnderecoProfissional = $cliente->pessoa->getEnderecoCobranca()->enderecoToXml('emprego');
        }

        if (count($referencias) > 0) {
            foreach ($referencias as $referencia) {
                $xmlReferencias .= $referencia->toXMLOmni();
            }
        }

        if ($cliente->getCadastro()->conjugue_compoe_renda) {
            $xmlConjuge = $cliente->pessoa->conjugeToXml();
        }

        $pOmniConfig = $proposta->hasOmniConfig();

        $apiUrl = $this->getLinkXML();

        $nomeCliente = str_replace(["&", "'"], ['e', ''], strtoupper(trim($analise->cliente->pessoa->nome)));
        $nomeCidade = str_replace(["&", "'"], ['e', ''], strtoupper(trim($cliente->pessoa->getEnderecoCobranca()->cidade)));
        $nomeBairro = str_replace(["&", "'"], ['e', ''], strtoupper(trim($cliente->pessoa->getEnderecoCobranca()->bairro)));
        $nomeRua = str_replace(["&", "'"], ['e', ''], strtoupper(trim($cliente->pessoa->getEnderecoCobranca()->logradouro)));

        //se cliente for da casa, marque-o com vip no xml
        if ($cliente->getCadastro()->cliente_da_casa) {
            $clienteVIP = "S";
        } else {
            $clienteVIP = "N";
        }

        if (isset($proposta->analiseDeCredito->filial)) {
            $nucleo = NucleoFiliais::model()->find("habilitado AND id = " . $proposta->analiseDeCredito->filial->NucleoFiliais_id);
        } else {
            $nucleo = null;
        }

        if (isset($nucleo->GrupoFiliais_id)) {
            $idGrupoFilial = $nucleo->GrupoFiliais_id;
        } else {
            $idGrupoFilial = 0;
        }

        $chave = "01630LHSTCREDSHOW";

        if (isset($cliente->pessoa->getDadosProfissionais()->tipoOcupacao->codOmni)) {
            $ocupacaoOmni = $cliente->pessoa->getDadosProfissionais()->tipoOcupacao->codOmni;
        } else {
            $ocupacaoOmni = "9";
        }

        if($proposta->analiseDeCredito->cliente->temVendaAprovada()){
            $comentario = '(CLIENTE CREDSHOW) - ' . $proposta->analiseDeCredito->observacao;
        }else{
            $comentario = $proposta->analiseDeCredito->observacao;
        }

        $chave = "01630LHSTCREDSHOW";

        if(in_array($proposta->analiseDeCredito->filial->NucleoFiliais_id, array(40) ))
        {
          $chave = "01630LHSTMAREMANSA2";
        }

        /*
          Logica de iserção do seguro na proposta Omni
        */
        $valorDoSeguroOmni = 0;

        if( $proposta->segurada ){
          $proposta->segurada = 0;
          $proposta->update();
          // $valorDoSeguroOmni = Proposta::model()->calcularValorSeguroOmni($proposta->valor, $proposta->valor_entrada);
          // $xmlServicoSeguro  = '<servicos>
          //   <servico>
          //     <idServico>11</idServico>
          //     <valor>'.number_format($valorDoSeguroOmni, 2, '.', '').'</valor>
          //     <quantidade>1</quantidade>
          //   </servico>
          // </servicos>';
        }

        $xmlString = '<Envelope>
              <Body>
                  <CM_PROPOSTA>
                      <proposta xmlns:tem="http://tempuri.org/">
                          <identificacao>
                              <chave>' . $chave . '</chave>
                              <senha>' . $this->getSenhaXML() . '</senha>
                          </identificacao>
                          <dados>
                             <operacao>Enviar</operacao>
                             <idProposta></idProposta>
                             <numeroProposta/>
                             <codigoLoja>' . $this->getLojaXML(1, $proposta) . '</codigoLoja>
                             <descricaoBem>' . $analise->mercadoria . '</descricaoBem>
                             <tipoFinanciamento>' . $tipoFinanciamento . '</tipoFinanciamento>
                             <taxaMensal>' . $tabela->taxa . '</taxaMensal>
                             <valorTC>0.0000</valorTC>
                             <valorServico/>
                             <valorBem>' . $proposta->getValorFinanciadoOmni() . '</valorBem>
                             <valorEntrada>' . $proposta->valor_entrada . '</valorEntrada>
                             <plano>' . $proposta->qtd_parcelas . '</plano>
                             <valorParcela>' . $proposta->getValorParcelaOmni() . '</valorParcela>
                             <dataCompra>' . date('Y-m-d') . '</dataCompra>
                             <dataVencimento>' . date('Y-m-d', strtotime(date('Y-m-d') . ' + ' . $proposta->carencia . ' days')) . '</dataVencimento>
                             <valorIOF>0.00</valorIOF>
                             <comentario>' . $comentario . '</comentario>
                          </dados>'
                          .$xmlServicoSeguro.
                          '<cliente>
                              <nome>' . $nomeCliente . '</nome>
                              <genero>' . $analise->cliente->pessoa->sexo . '</genero>
                              <dataNascimento>' . $analise->cliente->pessoa->nascimento . '</dataNascimento>
                              <naturalidade cidade="' . strtoupper(trim($cliente->pessoa->cidade->nome)) . '" estado="' . strtoupper($cliente->pessoa->cidade->uf) . '" />
                              <nomeMae>' . strtoupper(trim($cliente->getCadastro()->nome_da_mae)) . '</nomeMae>
                              <nomePai>' . strtoupper(trim($cliente->getCadastro()->nome_do_pai)) . '</nomePai>
                              <escolaridade>1</escolaridade>
                              <ocupacao>' . $ocupacaoOmni . '</ocupacao>
                              <estadoCivil>' . $cliente->pessoa->estadoCivil->codOmni . '</estadoCivil>
                              <clienteVIP>' . $clienteVIP . '</clienteVIP>
                              <dependentes>' . $cliente->getCadastro()->numero_de_dependentes . '</dependentes>
                              <documentos>
                                  <documento>
                                      <tipo>cpf</tipo>
                                      <numero>' . trim($cliente->pessoa->getCPF()->numero) . '</numero>
                                  </documento>
                                  <documento>
                                       <tipo>rg</tipo>
                                       <numero>' . trim($cliente->pessoa->getRG()->numero) . '</numero>
                                       <data>' . $cliente->pessoa->getRG()->data_emissao . '</data>
                                       <ufemissor>' . $cliente->pessoa->getRG()->uf_emissor . '</ufemissor>
                                  </documento>
                              </documentos>
                              <enderecos>
                                  <endereco>
                                      <tipo>' . strtoupper($cliente->pessoa->getEnderecoCobranca()->tipoMoradia->tipo) . '</tipo>
                                      <rua>' . $nomeRua . '</rua>
                                      <numero>0</numero>
                                      <complemento>NR:' . $cliente->pessoa->getEnderecoCobranca()->numero . ' ' . strtoupper($cliente->pessoa->getEnderecoCobranca()->complemento) . '</complemento>
                                      <bairro>' . $nomeBairro . '</bairro>
                                      <cidade>' . $nomeCidade . '</cidade>
                                      <estado>' . strtoupper($cliente->pessoa->getEnderecoCobranca()->uf) . '</estado>
                                      <cep>' . strtoupper($cliente->pessoa->getEnderecoCobranca()->cep) . '</cep>
                                      <condicao>' . $cliente->pessoa->getEnderecoCobranca()->tipoMoradia->tipoOmni . '</condicao>
                                  </endereco>'
                . $xmlEnderecoProfissional .
                '</enderecos>
                              <telefones>
                                  <telefone>
                                      <tipo>emprego</tipo>
                                      <ddd>' . $telefoneProfissional->getDDD() . '</ddd>
                                      <numero>' . $telefoneProfissional->getNumero() . '</numero>
                                  </telefone>' . $cliente->pessoa->contato->telefonesToXml()
                . $xmlReferencias .
                '</telefones>
                              <empregos>
                                <emprego>
                                  <empresa>' . str_replace(['&'], ['e'], strtoupper(trim($cliente->pessoa->getDadosProfissionais()->empresa)) . ' - ' . $cliente->pessoa->getDadosProfissionais()->cnpj_cpf) . '</empresa>
                                  <cargo>' . strtoupper(trim($cliente->pessoa->getDadosProfissionais()->profissao)) . '</cargo>
                                  <salario>' . number_format($cliente->pessoa->getDadosProfissionais()->renda_liquida, 2, '.', '') . '</salario>
                                  <tempo Anos="' . $cliente->pessoa->getDadosProfissionais()->tempo_de_servico_anos . '" Meses="' . $cliente->pessoa->getDadosProfissionais()->tempo_de_servico_meses . '"/>
                                </emprego>
                              </empregos>
                          </cliente>'
                . $xmlConjuge .
                '</proposta>
                  </CM_PROPOSTA>
              </Body>
          </Envelope>';

        $h = fopen('logsOmni/' . strtoupper($analise->cliente->pessoa->nome) . 'ObjProposta.xml', 'a+');
        fwrite($h, $xmlString);
        fclose($h);

        $result = null;

        $objProposta = new SoapVar($xmlString, XSD_ANYXML);

        $soapCliente = new SoapClient($apiUrl, array("soap_version" => 1, "trace" => 1, "exceptions" => 1, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS));
        //$soapCliente = new SoapClient('https://hst4.omni.com.br/homolog/wsProposta/servidor_soap_proposta.wsdl?service=ICM_Credito', array("soap_version" => 1, "trace" => 1, "exceptions" => 1, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS));

        try {
          $result = $soapCliente->CM_PROPOSTA($objProposta, null);
        }
        catch (SoapFault $e) {
          $result = $e;
        }

        ////////////////////
        //tipo            //
        //1 : resposta XML//
        //2 : excessao    //
        ////////////////////
        //caso a resposta seja um xml
        if (isset($result->propostaResponse) && $result->propostaResponse != null) {
            return ['tipo' => 1, 'resposta' => simplexml_load_string($result->propostaResponse), "tabelaHost" => $tipoFinanciamento, "usuarioHost" => $chave, "xml" => $xmlString];
        } else {
            //caso a resposta seja uma excessao
            return ['tipo' => 2, 'resposta' => $e->getMessage(), "tabelaHost" => null, "usuarioHost" => null];
        }
    }

    public function sendEPOmni($proposta, $analise, $cliente) {

    }

    public function getValorParcelaOmni(){

      $valorSeguro = 0;

      if( $this->segurada ){
        $valorSeguro = Proposta::model()->calcularValorSeguroOmni($this->valor, $this->valor_entrada);
      }

      $valores = Fator::model()->getValor(($this->valor - $this->valor_entrada + $valorSeguro), $this->qtd_parcelas, $this->carencia, $this->Tabela_id);

      return $valores[0];
    }

    public function renderBotaoAlterarSeguro() {

        $labelSeguroAtual = 'Selecionar Seguro';
        $htmlSeguros = '';

        /*
         * Verificar se a proposta possui seguro ativado
         * Se for dos moldes antigos, renderiza "Atualizar seguro"
         * Se for nos moldes novos, renderiza o nome do produto
         */
        if ($this->segurada) {
            /* Nos novos moldes, tenta pagar o nome do produto atual selecionado */
            if ($this->venda !== NULL) {
                if ($this->venda->getProdutoSeguro() !== NULL) {
                    $labelSeguroAtual = $this->venda->getProdutoSeguro()->itemDoEstoque->descricao;
                }
            }
        }

        foreach (AnaliseDeCredito::model()->listarSeguros() as $seguro) {
            $selected = '';
            $tipoOperacao = '1';

            if ($this->venda !== NULL) {
                if ($this->venda->getProdutoSeguro() !== NULL) {
                    if (($this->venda->getProdutoSeguro()->habilitado) && ($this->venda->getProdutoSeguro()->itemDoEstoque->id === $seguro->id)) {
                        $selected = ' checked="checked" ';
                        $tipoOperacao = '0';
                    }
                }
            }

            $htmlSeguros .= '<div><input data-produto-seguro="' . $seguro->id . '" data-tipo-alteracao="' . $tipoOperacao . '" data-proposta="' . $this->id . '" class="chekcseguro" ' . $selected . ' name="seguroSelecionado_' . $seguro->id . '"  type="checkbox"> ' . $seguro->descricao . '</div>';
        }

        return '<div class="space10">
            <div class="btn-group">
                <form>
                    <div class="btn-group">
                        <button type="button" class="btn btn-green dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-check"></i> ' . $labelSeguroAtual . ' <span class="caret"></span>
                        </button>
                        <div class="dropdown-menu dropdown-enduring dropdown-checkboxes">
                            ' . $htmlSeguros . '
                        </div>
                    </div>
                </form>
            </div>
        </div>';

        /*
          if ($this->segurada) {
          return '<a data-proposta="' . $this->id . '" data-tipo-alteracao="0" class="btn label btn-danger btn-change-seguro">Remover Seguro <i class="clip-cancel-circle"></i></a>';
          } else {
          return '<a data-proposta="' . $this->id . '" data-tipo-alteracao="1" class="btn label btn-primary btn-change-seguro">Incluir Seguro <i class="fa fa-plus"></i></a>';
          }
         */
    }

    public function atualizarSeguro($tipo, $propostaId) {
        $proposta = Proposta::model()->findByPk($propostaId);
        $mensagem = strtoupper(Yii::app()->session['usuario']->nome_utilizador);
        $retorno = [];
        $hasErroWs = false;
        $transaction = Yii::app()->db->beginTransaction();

        if ($proposta->Status_Proposta_id == 9) {
            $proposta->Status_Proposta_id = 1;

            $apolice = Apolice::model()->find('Proposta_id = ' . $proposta->id);

            /* Incluindo seguro */
            if ($tipo == 1) {
                $proposta->segurada = 1;
                $proposta->update();

                $mensagem .= " incluiu o seguro na proposta.";

                /* Não achou a apolice, então, cria uma */
                if ($apolice == NULL) {
                    Apolice::model()->novo('0000000', $proposta->id);
                }

                /* Achou a apolice e ela estava como desabilitada, se não estivesse deixa como está */ else {
                    if (!$proposta->segurada) {
                        $apolice->habilitado = 1;
                        $apolice->update();
                    }
                }
            }

            /* Removendo seguro */ else {
                /* Se a apolice já existir, desabilita */
                if ($apolice != NULL) {
                    $apolice->habilitado = 0;
                    $apolice->update();
                }

                $proposta->segurada = 0;
                $proposta->update();

                $mensagem .= " removeu o seguro da proposta.";
            }

            if ($proposta->hasOmniConfig() != NULL) {
                $resposta = $proposta->alterarCondicoesOmni();

                if ((isset($resposta->erros->flag)) && ($resposta->erros->flag == 'com erros')) {
                    $hasErroWs = true;

                    foreach ($resposta->erros->erro as $er) {
                        $retorno['msgConfig']['pnotify'][] = array(
                            'titulo' => 'Erros foram encontrados!',
                            'texto' => (string) $er->descricao,
                            'tipo' => 'error',
                        );
                    }
                }
            }

            if (!$hasErroWs) {
                $proposta->Status_Proposta_id = 4;

                foreach (AnalistaHasPropostaHasStatusProposta::model()->findAll('Proposta_id = ' . $proposta->id) as $saa) {
                    $saa->delete();
                }

                /* Criando mensagem para disparar alerta */
                Mensagem::model()->nova($mensagem, $proposta->getDialogo(), Yii::app()->session['usuario']->id);

                if ($proposta->update()) {
                    $retorno['ntfyTitle'] = 'Operação realizada com sucesso!';
                    $retorno['msg'] = 'A proposta foi reenviada para o analista. Aguarde.';
                    $retorno['ntfyTClass'] = 'success';

                    $transaction->commit();
                } else {
                    $transaction->rollBack();
                }
            }
        } else {
            $retorno['valor'] = number_format($proposta->valor, 2, ',', '.');
            $retorno['ntfyTitle'] = 'Não foi possível realizar a operação';
            $retorno['msg'] = 'A proposta precisa estar com o status "Modificar" para realizar alterações.';
            $retorno['ntfyTClass'] = 'error';
        }

        return $retorno;
    }

    public function atualizarValores() {
        $util = new Util;

        $novoValorParcela = Fator::model()->getValor($this->valor - $this->valor_entrada, $this->qtd_parcelas, $this->carencia, $this->tabelaCotacao->id);

        $this->valor_parcela = $novoValorParcela[0];
        $this->valor_parcela_texto = $util->moedaViewToBD($novoValorParcela[1]);
        $this->valor_final = $this->valor_parcela * $this->qtd_parcelas;
        $this->update();
    }

    public function novo($Prop, $Analise, $emprestimo, $semear) {

        $util = new Util;

        if (!isset($Prop['valor_entrada']) || $Prop['valor_entrada'] == null) {
            $valorEntrada = 0;
        } else {
            $valorEntrada = $Prop['valor_entrada'];
        }

        $valor = $util->moedaViewToBD($Prop['valor']) - $util->moedaViewToBD($valorEntrada);
        $valores = Fator::model()->getValor($valor, intval($Prop['qtd_parcelas']), intval($Prop['carencia']), intval($Prop['Tabela_id']));

        $Proposta = new Proposta;
        $Proposta->attributes = $Prop;
        $Proposta->valor = $util->moedaViewToBD($Prop['valor']);
        $Proposta->valor_entrada = $util->moedaViewToBD($valorEntrada);
        $Proposta->Tabela_id = intval($Prop['Tabela_id']);
        if ($emprestimo && $semear) {
            $Proposta->Financeira_id = 7;
        } else {
            if ($emprestimo) {
                $Proposta->Financeira_id = 6;
            } else {
                $Proposta->Financeira_id = 5;
            }
        }
        $Proposta->Analise_de_Credito_id = $Analise->id;
        $Proposta->habilitado = 1;
        $Proposta->data_cadastro = date('Y-m-d H:i:s');
        $Proposta->data_cadastro_br = date('d/m/Y H:i:s');
        $Proposta->valor_texto = $Prop['valor'];
        $Proposta->Status_Proposta_id = 4;
        $Proposta->titulos_gerados = 0;
        $Proposta->codigo = '';//"4" . str_pad($Analise->id, 6, 0, STR_PAD_LEFT);
        $Proposta->valor_parcela = $valores[0];
        $Proposta->valor_parcela_texto = $valores[0];
        $Proposta->valor_final = $valores[0] * intval($Prop['qtd_parcelas']);
        $Proposta->carencia = intval($Prop['carencia']);
        $Proposta->qtd_parcelas = intval($Prop['qtd_parcelas']);

        if ($Proposta->save()){

            if( $Proposta->Financeira_id != 5 ){
                $Proposta->codigo = "40" . str_pad($Proposta->id, 6, 0, STR_PAD_LEFT);
            }

            /*
                As propostas credshow devem, ainda, entrar com codigo grande (forma antiga)
                Solicitação da diretoria da empresa
            */
            else{
                $Proposta->codigo = $Proposta->analiseDeCredito->codigo . '1';
            }

            $Proposta->update();

            /* if ($Proposta->segurada)
              {
              Apolice::model()->novo('000000', $Proposta->id);
              }
             */

            if (Dialogo::model()->novo("Proposta " . $Proposta->codigo, 'Proposta', $Proposta->id, Yii::app()->session['usuario']->id, $Analise['observacao']) != NULL) {
                return $Proposta;
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }
    }

    public function simularCondicoes($Proposta, $semear) {

        /*Estes nucleos nao sao omni*/
        $NucleoFiliais = [139,138,140, 135, 121, 122, 126, 134, 40];

        //in_array($proposta->analiseDeCredito->filial->NucleoFiliais_id, [139,138,140, 135, 121, 122, 126, 134])
        $vs  = 0;
        $util = new Util;
        $condicoes = TabelaCotacao::model()->returnCarenciasXParcelas($Proposta['Tabela_id']);
        $proposta = new Proposta;
        $proposta->attributes = $Proposta;
        $proposta->carencia = $Proposta['carencia'];
        $proposta->qtd_parcelas = $Proposta['qtd_parcelas'];
        $proposta->data_cadastro = date('Y-m-d H:i:s');
        $valorCoberturaSeguro = 0; /* Padrão */
        $valorEntrada = 0;
        $dezPorcentValProposta = 0;

        if (isset($Proposta['valor_entrada']) && $Proposta['valor_entrada'] !== null) {
            $valorEntrada = $Proposta['valor_entrada'];
        }

        if(!isset($Proposta['segurada'])){
            $Proposta['segurada'] = false;
        }


        if ($Proposta['segurada']) {
            /* Configuração do seguro escolhido */

            if( in_array( Yii::app()->session['usuario']->filial->NucleoFiliais_id, $NucleoFiliais ) ){

              $coberturaSeguro = ConfigLN::model()->find("habilitado AND parametro = 'cobertura_seguro_" . $Proposta['segurada'] . "'");

              if ($coberturaSeguro != NULL) {
                  $valorCoberturaSeguro = $coberturaSeguro->valor;
                  $dezPorcentValProposta = ( ($util->moedaViewToBD($Proposta['valor']) - $util->moedaViewToBD($valorEntrada)) / 100 ) * $valorCoberturaSeguro;
              }
            }
            else{
                /*Omni*/
                $dezPorcentValProposta = Proposta::model()->calcularValorSeguroOmni($util->moedaViewToBD($Proposta['valor']), $util->moedaViewToBD($Proposta['valor_entrada']));
            }

            $tc = TabelaCotacao::model()->findByPk($Proposta['Tabela_id']);

            if(Yii::app()->session['usuario']->returnFilial()->nucleoFilial->id == 40 && $tc->ModalidadeId == 1){
                //$tac = ($util->moedaViewToBD($Proposta['valor']) - $util->moedaViewToBD($valorEntrada))*0.05;
                $tac = 0;
            }
            else{
                $tac = 0;
            }

            $Proposta['valor'] = number_format(( $util->moedaViewToBD($Proposta['valor']) - $util->moedaViewToBD($valorEntrada) ) + $dezPorcentValProposta + $tac, 2, ',', '.');
            /*
              Segurada pode vir diferente de 1, pois pega, na view, o id do produto
              Como será um campo salvo no banco, por padrão, vamos usar 0 ou 1
             */
            $Proposta['segurada'] = 1;
        }

        else {
            $tc = TabelaCotacao::model()->findByPk($Proposta['Tabela_id']);

            if(Yii::app()->session['usuario']->returnFilial()->nucleoFilial->id == 40 && $tc->ModalidadeId == 1){
                //$tac = ($util->moedaViewToBD($Proposta['valor']) - $util->moedaViewToBD($valorEntrada))*0.05;
                $tac = 0;
            }else{
                $tac = 0;
            }
            $Proposta['valor'] = number_format(( $util->moedaViewToBD($Proposta['valor']) - $util->moedaViewToBD($valorEntrada) + $tac), 2, ',', '.');
        }

        if ($semear === '1') {
            $dezPorcentTC = ( ($util->moedaViewToBD($Proposta['valor']) - $util->moedaViewToBD($valorEntrada)) / 100 ) * 10;
            if ($dezPorcentTC > 150) {
                $dezPorcentTC = 150;
            }
            $Proposta['valor'] = number_format(( $util->moedaViewToBD($Proposta['valor']) - $util->moedaViewToBD($valorEntrada) ) + $dezPorcentTC, 2, ',', '.');
        }

        $tabela = TabelaCotacao::model()->findByPk($Proposta['Tabela_id']);

        $arrConfig = array('globalConfig' => array(), 'parcelas' => array());

        $valor = $util->moedaViewToBD($Proposta['valor']);

        $arrConfig['globalConfig'] = array(
            'carencia' => intval($Proposta['carencia']),
            'valor' => $Proposta['valor'],
            'entrada' => $valorEntrada,
            'tabelaId' => $Proposta['Tabela_id'],
            'dataPriParc' => $proposta->getDataPrimeiraParcela(),
            'segurada' => $Proposta['segurada'],
            'valFinanciado' => $Proposta['valor'],
            // 'valseguro'    => round($vs, 2),
        );

        for ($i = 0; $i < count($condicoes['parcelas']); $i++) {
            $valores = Fator::model()->getValor($valor, intval($condicoes['parcelas'][$i]['text']), intval($Proposta['carencia']), intval($Proposta['Tabela_id']));

            if ($valores[0] != NULL) {
                $arrConfig['parcelas'][] = array(
                    'valorDoub' => doubleval($valores[0]),
                    'valorView' => $valores[1],
                    'qtdParcelas' => intval($condicoes['parcelas'][$i]['text']),
                    'valorFinal' => number_format($valores[0] * intval($condicoes['parcelas'][$i]['text']), 2, ',', '.'),
                    'dataUltParc' => $this->retornarUltimaParcela(date('Y-m-d H:i:s'), intval($Proposta['carencia']), intval($condicoes['parcelas'][$i]['text'])),
                );
            }
        }

        return $arrConfig;
    }

    public function getValorParcela2($proposta) {
        $valorDoSeguro = 0;

        if ($proposta['segurada'] == 1) {
            $valorDoSeguro = Proposta::model()->calcularValorDoSeguro2($proposta['valor'], $valorEntrada);
        }

        $valorFinanciado = ($proposta['valor'] - $valorEntrada) + $valorDoSeguro;

        $valores = Fator::model()->getValor($valorFinanciado, $proposta['qtd_parcelas'], $proposta['carencia'], $proposta['Tabela_id']);

        if ($proposta['segurada']) {
            return $valores[0];
        } else {
            return $proposta['valor_parcela'];
        }
    }

    public function getValorParcela() {

        $valorParcela = 0;

        if ($this->Tabela_id !== null) {

            $valores = Fator::model()->getValor($this->getValorFinanciado(), $this->qtd_parcelas, $this->carencia, $this->Tabela_id);

            if ($this->segurada) {
                $valorParcela = $valores[0];
            } else {
                if ($this->hasOmniConfig()) {
                    if ($this->hasOmniConfig()->numPropLoj != null) {
                        $valorParcela = $valores[0];
                    } else {
                        $valorParcela = $valores[0];
                        //$valorParcela = ($this->getValorFinanciado()/$this->qtd_parcelas);
                    }
                } else {
                    //$valorParcela = ($this->getValorFinanciado()/$this->qtd_parcelas);
                    $valorParcela = $valores[0];
                }
            }
        }

        return $valorParcela;
    }

    public function getValorFinal() {
        return ( $this->getValorParcela() * $this->qtd_parcelas );
    }

    public function returnAnalistaAprovou() {

        $analistaHasP = AnalistaHasPropostaHasStatusProposta::model()->find('Proposta_id = ' . $this->id);

        return $analistaHasP;
    }

    public function tableName() {
        return 'Proposta';
    }

    public function rules() {
        return array(
            array('Analise_de_Credito_id, valor_entrada, valor_final, Status_Proposta_id', 'required'),
            array('Analise_de_Credito_id, Banco_id, segurada, Financeira_id, Cotacao_id, habilitado, qtd_parcelas, carencia, Status_Proposta_id, titulos_gerados, analise_solicitada, enviando', 'numerical', 'integerOnly' => true),
            array('valor, valor_parcela, valor_entrada, valor_final', 'numerical'),
            array('data_cadastro, data_cadastro_br, entrada, valor', 'safe'),
            array('codigo', 'length', 'max' => 240),
            array('id, codigo, Analise_de_Credito_id, Banco_id, segurada, Tabela_id, Financeira_id, Cotacao_id, habilitado, data_cadastro, data_cadastro_br, valor, valor_texto, qtd_parcelas, carencia, valor_parcela, valor_parcela_texto, valor_entrada, valor_final, Status_Proposta_id, enviando', 'safe', 'on' => 'search'),
        );
    }

    public function propostasCrediaristaCharts($userId) {

        $analises = AnaliseDeCredito::model()->findAll('Usuario_id = ' . $userId . ' AND habilitado');

        $countEmAnalise = 0;
        $countCanceladas = 0;
        $countAguadAnalise = 0;
        $countAprovadas = 0;

        if (count($analises) > 0) {
            foreach ($analises as $analise) {

                $countAguadAnalise += count(Proposta::model()->findAll('Analise_de_Credito_id = ' . $analise->id . ' AND Status_Proposta_id = ' . 4 . ' AND habilitado'));

                $countEmAnalise += count(Proposta::model()->findAll('Analise_de_Credito_id = ' . $analise->id . ' AND Status_Proposta_id = ' . 1 . ' AND habilitado'));

                $countCanceladas += count(Proposta::model()->findAll('Analise_de_Credito_id = ' . $analise->id . ' AND Status_Proposta_id = ' . 3 . ' AND habilitado'));

                $countAprovadas += count(Proposta::model()->findAll('Analise_de_Credito_id = ' . $analise->id . ' AND Status_Proposta_id = ' . 2 . ' AND habilitado'));
            }
        }

        return array(
            'em_analise' => $countEmAnalise,
            'cancelados' => $countCanceladas,
            'aguardando_analise' => $countAguadAnalise,
            'aprovados' => $countAprovadas,
        );
    }

    public function relations() {
        return array(
          'analiseDeCredito'    => array(self::BELONGS_TO, 'AnaliseDeCredito', 'Analise_de_Credito_id'),
          'cotacao'             => array(self::BELONGS_TO, 'Cotacao', 'Cotacao_id'),
          'tabelaCotacao'       => array(self::BELONGS_TO, 'TabelaCotacao', 'Tabela_id'),
          'financeira'          => array(self::BELONGS_TO, 'Financeira', 'Financeira_id'),
          'statusProposta'      => array(self::BELONGS_TO, 'StatusProposta', 'Status_Proposta_id'),
          'soliCancelamento'    => array(self::HAS_ONE, 'SolicitacaoDeCancelamento', 'Proposta_id'),
          'propostaOmniConfig'  => array(self::HAS_ONE, 'PropostaOmniConfig', 'Proposta'),
          'emprestimo'          => array(self::HAS_ONE, 'Emprestimo', 'Proposta_id'),
          'banco'               => array(self::BELONGS_TO, 'Banco', 'Banco_id'),
          'venda'               => array(self::HAS_ONE, 'Venda', 'Proposta_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'codigo' => 'Código',
            'Analise_de_Credito_id' => 'Analise De Credito',
            'Tabela_id' => 'Tabela',
            'Financeira_id' => 'Financeira',
            'Cotacao_id' => 'Cotacao',
            'habilitado' => 'Habilitado',
            'data_cadastro' => 'Data Cadastro',
            'data_cadastro_br' => 'Data /Hora Cadastro',
            'valor' => 'Valor',
            'valor_texto' => 'Valor Txt',
            'qtd_parcelas' => 'Qtd Parcelas',
            'carencia' => 'Carencia',
            'valor_parcela' => 'Valor Parcela',
            'valor_parcela_texto' => 'Valor Parcela Txt',
            'valor_entrada' => 'Valor Entrada',
            'valor_final' => 'Valor Final',
            'Status_Proposta_id' => 'Status Proposta',
            'titulos_gerados' => 'Titulos Gerados',
            'Banco_id' => 'Banco',
            'segurada' => 'Segurada',
            'analise_solicitada' => 'Reanalise',
            'enviando' => 'Enviando',
        );
    }

    public function monetaryFormater($val) {
        $this->util = new Util;
        $this->util->applyMonetaryMask($val, 'echo');
    }

    public function search() {

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('codigo', $this->codigo);
        $criteria->compare('Analise_de_Credito_id', $this->Analise_de_Credito_id);
        $criteria->compare('Tabela_id', $this->Tabela_id);
        $criteria->compare('Financeira_id', $this->Financeira_id);
        $criteria->compare('Cotacao_id', $this->Cotacao_id);
        $criteria->compare('habilitado', $this->habilitado);
        $criteria->compare('data_cadastro', $this->data_cadastro, true);
        $criteria->compare('data_cadastro_br', $this->data_cadastro_br, true);
        $criteria->compare('valor', $this->valor);
        $criteria->compare('valor_texto', $this->valor_texto);
        $criteria->compare('qtd_parcelas', $this->qtd_parcelas);
        $criteria->compare('carencia', $this->carencia);
        $criteria->compare('valor_parcela', $this->valor_parcela);
        $criteria->compare('valor_parcela_texto', $this->valor_parcela_texto);
        $criteria->compare('valor_entrada', $this->valor_entrada);
        $criteria->compare('valor_final', $this->valor_final);
        $criteria->compare('Status_Proposta_id', $this->Status_Proposta_id);
        $criteria->compare('titulos_gerados', $this->titulos_gerados);
        $criteria->compare('enviando', $this->enviando);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getStatuInfo($statusId, $PropostaId) {
        $arrReturn = array(
            'statusAtualId' => '',
            'statusHasChanged' => '',
            'ehCartao' => FALSE,
            'teste' => ''
        );

        if (!empty($statusId) && !empty($PropostaId)) {
            $status = StatusProposta::model()->findByPk($statusId);
            $proposta = Proposta::model()->findByPk($PropostaId);

            if ($proposta->hasOmniConfig() != NULL) {
                $proposta->updateOmniStatus();
            }

            if ($status->id == $proposta->Status_Proposta_id) {
                $arrReturn['statusHasChanged'] = 0;
            } else {
                $statusProposta = StatusProposta::model()->findByPk($proposta->Status_Proposta_id);
                $arrReturn['statusHasChanged'] = 1;
                $arrReturn['statusAttrs'] = $statusProposta->attributes;
            }

            $arrReturn['statusAtualId'] = $proposta->Status_Proposta_id;

            $arrReturn['ehCartao'] = $proposta->ehCartao();

            ob_start();
            var_dump($proposta->ehCartao());
            $resultado = ob_get_clean();

            $arrReturn['teste'] = $resultado;
        }

        return $arrReturn;
    }

    public function getVencimentoParcela($seq) {

        $data_return;
        $data_base = date('d/m/Y', strtotime($this->data_cadastro . '+' . $this->carencia . ' days'));

        if ($seq == 1) {
            return $data_base;
        } else {
            return date('d/m/Y', strtotime("+" . $seq . "months", strtotime($data_base)));
        }
    }

    public function getDialogo() {

        $dialogo = Dialogo::model()->find('Entidade_Relacionamento = "Proposta" ' . ' AND Entidade_Relacionamento_id = ' . $this->id . ' AND habilitado');

        if ($dialogo != NULL)
            return $dialogo;
        else
            return NULL;
    }

    public function atualizarEntrada($propostaId, $entrada) {

        $proposta = Proposta::model()->findByPk($propostaId);
        $mensagem = strtoupper(Yii::app()->session['usuario']->nome_utilizador) . " atualizou o valor da entrada.";

        $retorno = [];
        $hasErroWs = false;

        $transaction = Yii::app()->db->beginTransaction();

        if ($proposta->Status_Proposta_id == 9) {
            $util = new Util;
            $entrada = $util->moedaViewToBD($entrada);
            $proposta->valor_entrada = $entrada;

            //$novoValorParcela               = Fator::model()->getValor($proposta->valor - $proposta->valor_entrada, $proposta->qtd_parcelas, $proposta->carencia, $proposta->tabelaCotacao->id);

            /* ---------------Alterações Novo seguro--------------- */
            /*
             * Mudar o valor do produto seguro que está na venda
             * se for o caso
             */
            $proposta->atualizarValorProdutoSeguro();
            $proposta->valor_parcela = $proposta->getValorFinanciado() / $proposta->qtd_parcelas;
            /* FIM------------Alterações Novo seguro--------------- */

            $proposta->valor_final = $proposta->valor_parcela * $proposta->qtd_parcelas;
            $proposta->Status_Proposta_id = 1;

            if ($proposta->hasOmniConfig() != NULL) {
                $resposta = $proposta->alterarCondicoesOmni();

                if ((isset($resposta->erros->flag)) && ($resposta->erros->flag == 'com erros')) {
                    $hasErroWs = true;

                    foreach ($resposta->erros->erro as $er) {
                        $retorno['msgConfig']['pnotify'][] = [
                            'titulo' => 'Erros foram encontrados!',
                            'texto' => (string) $er->descricao,
                            'tipo' => 'error',
                        ];
                    }
                }
            }

            /*
             * Se for uma proposta Omni, e der erro no WS, nem entra aqui
             * Se for uma proposta interna, vai entrar, pois a variável de verificação é FALSE por padrão
             */
            if (!$hasErroWs) {
                $proposta->Status_Proposta_id = 4;

                if ($proposta->update()) {
                    Mensagem::model()->nova($mensagem, $proposta->getDialogo(), Yii::app()->session['usuario']->id);

                    $retorno['ntfyTitle'] = 'Alteração realizada com sucesso.';
                    $retorno['msg'] = 'A proposta foi reenviada para o analista. Aguarde.';
                    $retorno['ntfyTClass'] = 'success';

                    foreach (AnalistaHasPropostaHasStatusProposta::model()->findAll('Proposta_id = ' . $proposta->id) as $saa) {
                        $saa->delete();
                    }

                    $transaction->commit();
                }

                /* Rollback! */ else {
                    $transaction->rollBack();
                }
            } else {
                $transaction->rollBack();
            }
        } else {
            $retorno['ntfyTitle'] = 'Não foi possível realizar a alteração.';
            $retorno['msg'] = 'A proposta precisa estar com o status "Modificar" para realizar alterações.';
            $retorno['ntfyTClass'] = 'error';
        }

        return $retorno;
    }

    public function atualizarValorInicial($propostaId, $novoValor) {
        $proposta = Proposta::model()->findByPk($propostaId);
        $valorAntigo = number_format($proposta->getValorFinanciado(), 2, ',', '.');
        $mensagem = "";
        $retorno = [];
        $hasErroWs = false;
        $sigacLog = new SigacLog;
        $transaction = Yii::app()->db->beginTransaction();

        if ($proposta->Status_Proposta_id == 9) {
            $sigacLog->saveLog('Tentativa de Alteração de valor incial', 'Proposta', $proposta->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Usuario " . Yii::app()->session['usuario']->username . " tentou alterar valor inicial da proposta " . $this->codigo, "127.0.0.1", NULL, Yii::app()->session->getSessionId());
            $util = new Util;
            $novoValor = $util->moedaViewToBD($novoValor);
            $proposta->valor = $novoValor;

            $novoValorParcela = Fator::model()->getValor($proposta->getValorFinanciado(), $proposta->qtd_parcelas, $proposta->carencia, $proposta->tabelaCotacao->id);

            /* ---------------Alterações Novo seguro--------------- */
            /*
             * Mudar o valor do produto seguro que está na venda
             * se for o caso
             */
            $proposta->atualizarValorProdutoSeguro();
            $proposta->valor_parcela = $proposta->getValorFinanciado() / $proposta->qtd_parcelas;
            /* FIM------------Alterações Novo seguro--------------- */

            $proposta->valor_final = $proposta->valor_parcela * $proposta->qtd_parcelas;


            /* Verifica se é proposta externa, e tenta captar erros */
            if ($proposta->hasOmniConfig() != NULL) {
                $resposta = $proposta->alterarCondicoesOmni();

                if ((isset($resposta->erros->flag)) && ($resposta->erros->flag == 'com erros')) {
                    $hasErroWs = true;

                    foreach ($resposta->erros->erro as $er) {
                        $retorno['msgConfig']['pnotify'][] = array(
                            'titulo' => 'Erros foram encontrados!',
                            'texto' => (string) $er->descricao,
                            'tipo' => 'error',
                        );
                    }
                }
            }

            /*
             * Se for uma proposta Omni, e der erro no WS, nem entra aqui
             * Se for uma proposta interna, vai entrar, pois a variável de verificação é FALSE por padrão
             */
            if ($hasErroWs) {
                $transaction->rollBack();
            } else {
                /* Liberando a proposta dos analistas */
                foreach (AnalistaHasPropostaHasStatusProposta::model()->findAll('Proposta_id = ' . $proposta->id) as $saa) {
                    $saa->delete();
                }

                $proposta->Status_Proposta_id = 4;

                if ($proposta->update()) {
                    $mensagem .= "O valor da proposta foi alterado de R$" . $valorAntigo . " para R$ " . number_format($proposta->getValorFinanciado(), 2, ',', '.');
                    $mensagem .= " por " . strtoupper(Yii::app()->session['usuario']->nome_utilizador);

                    /* Criando mensagem para disparar alerta */
                    Mensagem::model()->nova($mensagem, $proposta->getDialogo(), Yii::app()->session['usuario']->id);

                    $retorno['valor'] = number_format($proposta->valor, 2, ',', '.');
                    $retorno['ntfyTitle'] = 'Alteração realizada com sucesso.';
                    $retorno['msg'] = 'A proposta foi reenviada para o analista. Aguarde.';
                    $retorno['ntfyTClass'] = 'success';

                    $sigacLog->saveLog('Alteração de valor incial', 'Proposta', $proposta->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Usuario " . Yii::app()->session['usuario']->username . " alterou valor inicial da proposta " . $this->codigo, "127.0.0.1", NULL, Yii::app()->session->getSessionId());
                    $transaction->commit();
                } else {
                    $transaction->rollBack();
                }
            }
        } else {
            $retorno['valor'] = number_format($proposta->valor, 2, ',', '.');
            $retorno['ntfyTitle'] = 'Não foi possível realizar a operação';
            $retorno['msg'] = 'A proposta precisa estar com o status "Modificar" para realizar alterações.';
            $retorno['ntfyTClass'] = 'error';
        }

        return $retorno;
    }

    public function alterarCondicoes($propostaId, $cotacao, $carencia, $qtd_parcelas, $valor_parcela) {

        $proposta = Proposta::model()->findByPk($propostaId);
        $mensagem = "As condições da proposta foram alteradas por " . strtoupper(Yii::app()->session['usuario']->nome_utilizador);

        if ($proposta->Status_Proposta_id == 9) {
            $proposta->Tabela_id = $cotacao;
            $proposta->qtd_parcelas = $qtd_parcelas;
            $proposta->carencia = $carencia;
            $proposta->valor_parcela = $valor_parcela;
            $proposta->valor_parcela_texto = $valor_parcela;
            $proposta->valor_final = $valor_parcela * $qtd_parcelas;
            $proposta->update();

            if ($proposta->hasOmniConfig() != NULL) {
                $proposta->alterarCondicoesOmni();
            }

            $proposta->Status_Proposta_id = 4;
            $proposta->update();

            foreach (AnalistaHasPropostaHasStatusProposta::model()->findAll('Proposta_id = ' . $proposta->id) as $saa) {
                $saa->delete();
            }

            Mensagem::model()->nova($mensagem, $proposta->getDialogo(), Yii::app()->session['usuario']->id);

            return array(
                'msg' => 'OK'
            );
        } else {
            return array(
                'msg' => 'Não foi possível atualizar as condições desta proposta'
            );
        }
    }

    public function listarMensagens($propostaId, $draw) {
        $proposta = Proposta::model()->findByPk($propostaId);
        $rows = array();
        $countMensagens = 0;
        $util = new Util;

        if ($proposta != NULL) {

            $dialogo = Dialogo::model()->find('Entidade_Relacionamento = "Proposta" ' . ' AND Entidade_Relacionamento_id = ' . $proposta->id . ' AND habilitado');

            if ($dialogo != NULL) {

                $mensagens = Mensagem::model()->findAll('Dialogo_id = ' . $dialogo->id . ' ORDER BY id DESC');

                if ($mensagens != NULL && count($mensagens) > 0) {

                    foreach ($mensagens as $mensagem) {

                        $btn_more = '';
                        $btn_edit = '';
                        $btn_del = '';
                        $countMensagens++;
                        $remetente = ( $mensagem->Usuario_id == Yii::app()->session['usuario']->id ? "Você" : $mensagem->usuario->nome_utilizador );
                        $lida = ( $mensagem->lida ? "Sim" : "Não" );

                        $btn_edit = ( $mensagem->Usuario_id == Yii::app()->session['usuario']->id ? '<a type="button" class="btn btn-green" data-toggle="modal"  href="#" style="padding:5px;font-size:8px;"><i class="clip-pencil"></i></a>' : '<a type="button" class="btn btn-green" href="#" style="padding:5px;font-size:8px;" disabled="disabled"><i class="clip-pencil"></i></a>' );
                        $btn_del = ( $mensagem->Usuario_id == Yii::app()->session['usuario']->id ? '<a type="button" class="btn btn-red" data-toggle="modal"  href="#" style="padding:5px;font-size:8px;"><i class="clip-close"></i></a>' : '<a type="button" class="btn btn-red" href="#" style="padding:5px;font-size:8px;" disabled="disabled"><i class="clip-close"></i></a>' );

                        $btn_more .= '<a data-mensagem-id="' . $mensagem->id . '" type="button" class="btn btn-primary btn-ler-msg" href="#" style="padding:5px;font-size:8px;"><i class="clip-search"></i></a>';

                        /*
                          $row = array(
                          'assunto' => strtoupper($mensagem->dialogo->assunto),
                          'conteudo' => strtoupper($mensagem->conteudo),
                          'remetente' => strtoupper($remetente),
                          'lida' => strtoupper($lida),
                          'data_envio' => $util->bd_date_to_view($mensagem->data_criacao),
                          'btn_more' => $btn_more,
                          'btn_edit' => $btn_edit,
                          'btn_del' => $btn_del,
                          );
                         */
                        $rows[] = [
                            'conteudo' => strtoupper($mensagem->conteudo),
                            'dataHora' => $util->bd_date_to_view($mensagem->data_criacao),
                        ];

                        $remetente = "";
                        $lida = "";
                        $btn_more = "";
                        $btn_edit = "";
                        $btn_del = "";
                    }
                }
            }
        }

        return [
            "draw" => $draw,
            "recordsTotal" => $countMensagens,
            "recordsFiltered" => $countMensagens,
            "data" => $rows
        ];
    }

    public function getComentariosOmni($propostaId, $draw) {
        $mensagens = [];

        $util = new Util;

        $proposta = Proposta::model()->findByPk($propostaId);

        $pOmniConfig = $proposta->hasOmniConfig();

        if ($proposta->hasOmniConfig() == NULL) {
            if (Yii::app()->session['usuario']->tipo == 'crediarista') {
                foreach (Mensagem::model()->findAll('interno = 0 AND Dialogo_id = ' . $proposta->getDialogo()->id) as $msg) {
                    $mensagens[] = [
                        'conteudo' => strtoupper($msg->usuario->nome_utilizador) . ': ' . $msg->conteudo,
                        'dataHora' => ($msg->data_criacao),
                    ];
                }
            } else {
                foreach (Mensagem::model()->findAll('Dialogo_id = ' . $proposta->getDialogo()->id) as $msg) {
                    $mensagens[] = [
                        'conteudo' => strtoupper($msg->usuario->nome_utilizador) . ': ' . $msg->conteudo,
                        'dataHora' => ($msg->data_criacao),
                    ];
                }
            }
        } else {
            if ($proposta->hasOmniConfig()->numPropLoj == NULL) {
                $xmlString = '<Envelope>
                   <Body>
                      <CM_PROPOSTA>
                         <proposta xmlns:tem="http://tempuri.org/">
                            <identificacao>
                                <chave>' . $pOmniConfig->usuarioHost . '</chave>
                                <senha>' . $this->getSenhaXML() . '</senha>
                            </identificacao>
                            <dados>
                               <operacao>Consultar</operacao>
                               <idProposta>' . $proposta->codigo . '</idProposta>
                            </dados>
                         </proposta>
                      </CM_PROPOSTA>
                   </Body>
                </Envelope>';

                $apiUrl = $this->getLinkXML();

                $objProposta = new SoapVar($xmlString, XSD_ANYXML);

                if ($proposta->Status_Proposta_id == 9) {

                    $soapCliente = new SoapClient($apiUrl, ["soap_version" => 1, "trace" => 1, "exceptions" => 1, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS]);

                    $result = $soapCliente->CM_PROPOSTA($objProposta, null);

                    $xmlObj = simplexml_load_string($result->propostaResponse);

                    foreach ($xmlObj->comentarios->comentario as $com) {
                        $mensagens[] = [
                            'conteudo' => substr((string) $com->comentario, 25),
                            'dataHora' => $util->bd_date_to_view((string) $com->dataHora),
                        ];
                    }
                } else if ($proposta->Status_Proposta_id == 3) {
                    if (!$proposta->dialogo_salvo) {
                        $soapCliente = new SoapClient($apiUrl, ["soap_version" => 1, "trace" => 1, "exceptions" => 1, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS]);

                        $result = $soapCliente->CM_PROPOSTA($objProposta, null);

                        $xmlObj = simplexml_load_string($result->propostaResponse);

                        foreach ($xmlObj->comentarios->comentario as $com) {
                            $Mensagem['conteudo'] = (string) $com->comentario;

                            Mensagem::model()->novo($Mensagem, 422, $proposta->getDialogo()->id,0);

                            $mensagens[] = [
                                'conteudo' => substr((string) $com->comentario, 25),
                                'dataHora' => $util->bd_date_to_view((string) $com->dataHora),
                            ];
                        }

                        foreach (Mensagem::model()->findAll('Dialogo_id = ' . $proposta->getDialogo()->id) as $msg) {
                            $mensagens[] = [
                                'conteudo' => substr(strtoupper($msg->usuario->nome_utilizador) . ': ' . $msg->conteudo, 25),
                                'dataHora' => ($msg->data_criacao),
                            ];
                        }

                        $proposta->dialogo_salvo = 1;
                        $proposta->update();
                    } else {
                        foreach (Mensagem::model()->findAll('Dialogo_id = ' . $proposta->getDialogo()->id) as $msg) {
                            $mensagens[] = [
                                'conteudo' => substr(strtoupper($msg->usuario->nome_utilizador) . ': ' . $msg->conteudo, 25),
                                'dataHora' => ($msg->data_criacao),
                            ];
                        }
                    }
                } else {
                    $mensagens[] = [
                        'conteudo' => 'Sem atualizações',
                        'dataHora' => date('d/m/Y'),
                    ];
                }
            } else {
                if ($proposta->Status_Proposta_id == 9) {
                    $headers = array(
                        'Content-Type:text/xml;charset=UTF-8',
                        'Connection: close'
                    );

                    $xml = "<soapenv:Envelope
                    xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'
                    xmlns:ws='http://ws.host.banco.venda.jretail/'>
                    <soapenv:Header/>
                    <soapenv:Body>
                        <ws:consultarProposta>
                            <xmlIn>
                                <host>
                                    <autenticacao>
                                        <codUsr>hstcredshow</codUsr>
                                        <pwdUsr>LMt46dc9ezpP1f34DQFpUA==</pwdUsr>
                                    </autenticacao>
                                    <proposta>
                                        <codLoja>51130000</codLoja>
                                        <numPropLoj>" . $proposta->propostaOmniConfig->numPropLoj . "</numPropLoj>
                                    </proposta>
                                </host>
                            </xmlIn>
                        </ws:consultarProposta>
                    </soapenv:Body>
                </soapenv:Envelope>";

                    $ch = curl_init('https://correspondente.bancosemear.com.br:8443/jretail-jretail.ejb/PropostaHostWSSSL?wsdl');
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);

                    $retorno = curl_exec($ch);

                    $objXML = simplexml_load_string($retorno);
                    $objXML->registerXPathNamespace('env', 'http://schemas.xmlsoap.org/soap/envelope/');
                    $objXML->registerXPathNamespace('ws', 'http://ws.host.banco.venda.jretail/');

                    $resultado = $objXML->xpath("//ws:consultarPropostaResponse");

                    $resp = $resultado[0]->xpath('xmlOut');

                    $cdata = simplexml_load_string($resp[0]);

                    $response = $cdata->xpath('proposta/pendencias/pendencia/desc')[0];

                    $Mensagem['conteudo'] = (string) $response;

                    //Mensagem::model()->novo($Mensagem, 422, $proposta->getDialogo()->id);

                    $mensagens[] = [
                        'conteudo' => (string) $response,
                        'dataHora' => date('d/m/Y'),
                    ];
                    if ($proposta->dialogo_salvo) {
                        if (Yii::app()->session['usuario']->tipo == 'crediarista') {
                            foreach (Mensagem::model()->findAll('interno = 0 AND Dialogo_id = ' . $proposta->getDialogo()->id) as $msg) {
                                $mensagens[] = [
                                    'conteudo' => strtoupper($msg->usuario->nome_utilizador) . ': ' . $msg->conteudo,
                                    'dataHora' => $util->bd_date_to_view($msg->data_criacao),
                                ];
                            }
                        } else {
                            foreach (Mensagem::model()->findAll('Dialogo_id = ' . $proposta->getDialogo()->id) as $msg) {
                                $mensagens[] = [
                                    'conteudo' => strtoupper($msg->usuario->nome_utilizador) . ': ' . $msg->conteudo,
                                    'dataHora' => ($msg->data_criacao),
                                ];
                            }
                        }
                    }
                } else {
                    $mensagens[] = [
                        'conteudo' => 'Sem atualizações',
                        'dataHora' => date('d/m/Y'),
                    ];
                }
            }
        }


        return [
            "draw" => $draw,
            "recordsTotal" => count($mensagens),
            "recordsFiltered" => count($mensagens),
            "data" => $mensagens
        ];
    }

    public function getDetalhesFinanceiros($propostaId, $draw) {

        $proposta = Proposta::model()->findByPk($propostaId);
        $rows = array();
        $countMensagens = 0;
        $util = new Util;

        $btn_edit = ( ($proposta->Status_Proposta_id == 9 || $proposta->Status_Proposta_id == 4 || $proposta->Status_Proposta_id == 1 && !$proposta->titulos_gerados ) ? '<a type="button" class="btn btn-green" data-toggle="modal"  href="#modal-edit-condicoes-proposta" style="padding:5px;font-size:8px;"><i class="clip-pencil"></i></a>' : '<a type="button" class="btn btn-green" href="#" style="padding:5px;font-size:8px;" disabled="disabled"><i class="clip-pencil"></i></a>' );

        if (Yii::app()->session['usuario']->tipo_id == 4) {
            $btn_edit = '<a type="button" class="btn btn-green btn-update-detalhes-financeiros" href="#" style="padding:5px;font-size:8px;"><i class="clip-refresh"></i></a>';
        }

        if ($proposta->statusProposta->id == 2 || $proposta->statusProposta->id == 3) {
            $btn_edit = '<a type="button" class="btn btn-green" href="#" style="padding:5px;font-size:8px;" disabled="disabled"><i class="clip-pencil"></i></a>';
        }

        $taxaTabela = 0;

        if ($proposta->Tabela_id !== null) {
            $taxaTabela = $proposta->tabelaCotacao->taxa;
        }

        $valorDoSeguro = 0;

        if( $proposta->hasOmniConfig() != Null && $proposta->segurada ){
          $valorDoSeguro = Proposta::model()->calcularValorSeguroOmni($proposta->valor, $proposta->valor_entrada);
        }
        else{
          $valorDoSeguro = $proposta->calcularValorDoSeguro();
        }

        $row = array(
            'btn_seguro' => $proposta->renderBotaoAlterarSeguro(),
            'val_inicial' => "R$ " . number_format($proposta->valor, 2, ',', '.'),
            'val_entrada' => "R$ " . number_format($proposta->valor_entrada, 2, ',', '.'),
            'seguro' => "R$ " . number_format($valorDoSeguro, 2, ',', '.'),
            'carencia' => $proposta->carencia,
            'val_financiado' => "R$ " . number_format($proposta->getValorFinanciado(), 2, ',', '.'),
            'parcelamento' => $proposta->qtd_parcelas . 'x ' . number_format($proposta->getValorParcela(), 2, ',', '.'),
            'val_final' => "R$ " . number_format($proposta->getValorFinal(), 2, ',', '.'),
            'data_primeira_parcela' => $proposta->getDataPrimeiraParcela(),
            'data_ultima_parcela' => $proposta->retornarUltimaParcela($proposta->data_cadastro, $proposta->carencia, $proposta->qtd_parcelas),
            'taxa' => $taxaTabela,
            'btn' => $btn_edit,
        );

        /* Atualização novo seguro */
        $proposta->valor_parcela = $proposta->getValorParcela();
        $proposta->valor_final = $proposta->getValorFinal();
        $proposta->update();
        /* FIM Atualização novo seguro */

        $rows[] = $row;

        return (array(
            "draw" => $draw,
            "recordsTotal" => $countMensagens,
            "recordsFiltered" => $countMensagens,
            "data" => $rows
        ));
    }

    public function findByCode($code, $tg) {

        $proposta = Proposta::model()->find('codigo = ' . $code . ' AND titulos_gerados =' . $tg);

        if (count($proposta) <= 0)
            throw new Exception('Registro não encontrado', 0);

        return $proposta;
    }

    public function getDataPrimeiraParcela() {

        $carencia = $this->carencia + 1;

        return date('d/m/Y', strtotime("+" . $carencia . "days", strtotime($this->data_cadastro)));
    }

    public function getDataUltimaParcela() {

        $carencia = $this->carencia;

        $data_ultima_parcela = date('Y-m-d', strtotime($this->data_cadastro . ' +' . $carencia . ' days'));

        $data_ultima_parcela = date('d/m/Y', strtotime("+" . $this->qtd_parcelas - 1 . "months", strtotime($data_ultima_parcela)));

        return $data_ultima_parcela;
    }

    public function retornarUltimaParcela($data_cadastro, $carencia, $parcelas) {
        $carencia = $carencia + 1;

        $data_ultima_parcela = date('Y-m-d', strtotime($data_cadastro . ' +' . $carencia . ' days'));

        $data_ultima_parcela = date('d/m/Y', strtotime("+" . $parcelas - 1 . "months", strtotime($data_ultima_parcela)));

        return $data_ultima_parcela;
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function propostasDia() {

        $date = date('Y-m-d');
        $start_time = $date . ' 00:00:00';
        $end_time = $date . ' 23:59:59';

        return AnaliseDeCredito::model()->findAll(
                        'data_cadastro BETWEEN :start_time AND :end_time', array(
                    ':start_time' => $start_time,
                    ':end_time' => $end_time,
                        )
        );
    }

    public function cancelarAnalise() {

        $this->Status_Proposta_id = 4;

        if ($this->update()) {
            $sigacLog = new SigacLog;
            $sigacLog->saveLog('Cancelar Análise de proposta', 'Proposta', $this->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Usuario " . Yii::app()->session['usuario']->username . " cancelou a análise da proposta " . $this->codigo, "127.0.0.1", NULL, Yii::app()->session->getSessionId());
        }
    }

    public function recusar($motivoDeNegacao, $obs) {

        if ($this->hasOmniConfig() == NULL) {

            $this->Status_Proposta_id = 3;
            $this->analise_solicitada = 0;

            if ($this->save()) {
                $sigacLog = new SigacLog;
                $sigacLog->saveLog('Proposta Negada', 'Proposta', $this->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Analista " . Yii::app()->session['usuario']->nome_utilizador . " negou a proposta com código: " . $this->codigo, "127.0.0.1", null, Yii::app()->session->getSessionId());

                $analistaHasP = AnalistaHasPropostaHasStatusProposta::model()->find('Proposta_id = ' . $this->id . ' AND Analista_id = ' . Yii::app()->session['usuario']->id . ' AND Status_Proposta_id = 3');

                if ($analistaHasP != NULL) {

                    if (!$analistaHasP->habilitado) {
                        $analistaHasP->habilitado = 1;
                    }

                    $analistaHasP->updated_at = date('Y-m-d H:i:s');
                    $analistaHasP->update();
                } else {
                    $analistaHasPropHasStatusP = new AnalistaHasPropostaHasStatusProposta;
                    $analistaHasPropHasStatusP->Analista_id = Yii::app()->session['usuario']->id;
                    $analistaHasPropHasStatusP->Proposta_id = $this->id;
                    $analistaHasPropHasStatusP->Status_Proposta_id = $this->Status_Proposta_id;
                    $analistaHasPropHasStatusP->habilitado = 1;
                    $analistaHasPropHasStatusP->data_cadastro = date('Y-m-d H:i:s');
                    $analistaHasPropHasStatusP->updated_at = date('Y-m-d H:i:s');
                    $analistaHasPropHasStatusP->save();
                }

                $negacao = new NegacaoDeCredito;
                $negacao->MotivoDeNegacao = $motivoDeNegacao;
                $negacao->observacao = $obs;
                $negacao->Proposta = $this->id;
                $negacao->habilitado = 1;
                $negacao->Analista = Yii::app()->session['usuario']->id;
                $negacao->save();
            }
        }
    }

    public function registrarApolice() {
        $util = new Util;

        $emissao = substr($this->data_cadastro, 0, 10);
        $qtd_parcelas = $this->qtd_parcelas;

        if ($qtd_parcelas < 10) {
            $qtd_parcelas = '0' . strval($this->qtd_parcelas);
        } else {
            $qtd_parcelas = $this->qtd_parcelas;
        }

        $regSeguro = new CnabDetalhe;
        $regSeguro->data_emissao = $emissao;
        $regSeguro->identificacao_do_registro = '1';
        $regSeguro->tipo_do_movimento = 'I';
        $regSeguro->data_da_movimentacao = date('Ymd');
        $regSeguro->nro_chubb = '0008070008';
        $regSeguro->sub_grupo = '000000000000';
        $regSeguro->nro_do_certificado = '0000000001';
        //$regSeguro->nome_do_segurado = strtoupper($util->cleanStr($this->analiseDeCredito->cliente->pessoa->nome, ''));
        $regSeguro->nome_do_segurado = strtoupper($this->analiseDeCredito->cliente->pessoa->nome);
        $regSeguro->cpf_do_segurado = '000000' . substr($this->analiseDeCredito->cliente->pessoa->getCPF()->numero, 0, 9);
        $regSeguro->dv_cpf_do_segurado = substr($this->analiseDeCredito->cliente->pessoa->getCPF()->numero, 9, 2);
        $regSeguro->nascimento_do_segurado = substr($this->analiseDeCredito->cliente->pessoa->nascimento, 0, 4) . substr($this->analiseDeCredito->cliente->pessoa->nascimento, 5, 2) . substr($this->analiseDeCredito->cliente->pessoa->nascimento, 8, 2);
        $regSeguro->sexo_do_segurado = $this->analiseDeCredito->cliente->pessoa->sexo;
        $regSeguro->endereco_do_segurado = strtoupper($util->cleanStr($this->analiseDeCredito->cliente->pessoa->getEndereco()->logradouro, ''));
        $regSeguro->bairro_do_segurado = strtoupper($util->cleanStr($this->analiseDeCredito->cliente->pessoa->getEndereco()->bairro, ''));
        $regSeguro->cidade_do_segurado = strtoupper($util->cleanStr($this->analiseDeCredito->cliente->pessoa->getEndereco()->cidade, ''));
        $regSeguro->estado_do_segurado = strtoupper($this->analiseDeCredito->cliente->pessoa->getEndereco()->uf);
        $regSeguro->cep_do_segurado = str_replace('-', '', $this->analiseDeCredito->cliente->pessoa->getEndereco()->cep);
        $regSeguro->ddd_do_segurado = $this->analiseDeCredito->cliente->pessoa->contato->getLasTelefone()->getDDD();
        $regSeguro->telefone_do_segurado = '0' . $this->analiseDeCredito->cliente->pessoa->contato->getLasTelefone()->getNumero();
        $regSeguro->inicio_de_vigencia = date('Ymd', strtotime($emissao . ' + 1 days'));
        $regSeguro->fim_de_vigencia = date('Ymd', strtotime($emissao . ' + 31 days'));
        $regSeguro->i_s_cobertuda_principal = '0000500000';
        $regSeguro->valor_premio_mensal_contratado = CampoCnabConfig::model()->cnabValorMonetario(number_format($this->calcularValorDoSeguro(), 2, '.', ''), 7);
        $regSeguro->status_da_cobranca = 'P';
        $regSeguro->servicos_chubb = '000000000';
        $regSeguro->numero_do_contrato = substr($this->codigo, 0, 19);
        $regSeguro->chave_de_controle_1 = '000000' . $this->analiseDeCredito->cliente->pessoa->getCPF()->numero . substr($regSeguro->numero_do_contrato, -8);
        $regSeguro->i_s_cobertuda_2 = '0000500000';
        $regSeguro->i_s_cobertuda_3 = '0000090000';
        $regSeguro->saldo_devedor = CampoCnabConfig::model()->cnabValorMonetario(number_format((float) ($this->valor - $this->valor_entrada), 2, '.', ''), 16);
        $regSeguro->parcelas_prestamista = $qtd_parcelas;
        $regSeguro->data_inicio_contrato_prestamista = $regSeguro->inicio_de_vigencia;
        $regSeguro->data_fim_contrato_prestamista = date('Ymd', strtotime($emissao . ' + ' . $qtd_parcelas . ' months'));
        $regSeguro->valor_total_da_compra = CampoCnabConfig::model()->cnabValorMonetario(number_format((float) ($this->valor - $this->valor_entrada), 2, '.', ''), 14);
        $regSeguro->valor_da_parcela = CampoCnabConfig::model()->cnabValorMonetario(number_format((float) $this->getValorParcelaInicial(), 2, '.', ''), 14);
        $regSeguro->Banco_id = 8;
        $regSeguro->transmitido = 0;

        $regSeguro->save();
    }

    public function getValorParcelaInicial() {
        return ($this->valor - $this->valor_entrada) / $this->qtd_parcelas;
    }

    public function gerarRegistroRetornoParceiro($tipo_de_movimentacao_credshow, $repasse = null) {
        /* Gerar registro de remessa para a filial: Tipo 01 = venda confirmada */
        $cnabConfig = new CampoCnabConfig;
        $util = new Util;
        $cnabDetalhe = new CnabDetalhe;
        $valor_do_repasse = '00000000000000000';


        if ($repasse != null) {
            $valor_do_repasse = $cnabConfig->cnabValorMonetario(number_format($this->valorRepasse(), 2, '.', ''), 17);
        }

        $cnabDetalhe->numero_da_proposta_credshow = $cnabConfig->cnabValorMonetario($this->codigo, 30);
        $cnabDetalhe->tipo_de_registro_credshow = '02';
        $cnabDetalhe->cnpj_filial_credshow = $util->cleanStr($this->analiseDeCredito->filial->cnpj, 'upp');
        $cnabDetalhe->cpf_cliente_compra_credshow = '000' . $util->cleanStr($this->analiseDeCredito->cliente->pessoa->getCPF()->numero, 'upp');
        $cnabDetalhe->nome_do_cliente_credshow = $util->nomeSemAcentos($this->analiseDeCredito->cliente->pessoa->nome, 'upp');
        $cnabDetalhe->data_da_venda_credshow = substr($this->data_cadastro, 0, 4) . substr($this->data_cadastro, 5, 2) . substr($this->data_cadastro, 8, 2);
        $cnabDetalhe->valor_da_venda_credshow = $cnabConfig->cnabValorMonetario(number_format($this->valor - $this->valor_entrada, 2, '.', ''), 17);
        $cnabDetalhe->valor_do_repasse_credshow = $valor_do_repasse;
        $cnabDetalhe->data_da_operacao_credshow = date('Y-m-d');
        $cnabDetalhe->data_da_operacao_credshow_2 = date('Ymd');
        $cnabDetalhe->tipo_de_movimentacao_credshow = $tipo_de_movimentacao_credshow;
        $cnabDetalhe->reservado_credshow = '0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000';
        $cnabDetalhe->Banco_id = 10;
        $cnabDetalhe->Filial_id = $this->analiseDeCredito->filial->id;
        $cnabDetalhe->save();
    }

    public function aprovar() {
        $sigacLog = new SigacLog;
        $sigacLog->saveLog('Proposta Aprovada', 'Proposta', $this->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Analista " . Yii::app()->session['usuario']->nome_utilizador . " apravou a proposta com código: " . $this->codigo, "127.0.0.1", null, Yii::app()->session->getSessionId());

        if ($this->hasOmniConfig() == NULL) {

            $this->Status_Proposta_id = 2;

            if ($this->update()) {

                /*
                  $venda                  = new Venda;
                  $venda->data_cadastro   = date('Y-m-d H:i:s');
                  $venda->Cliente_id      = $this->analiseDeCredito->Cliente_id;
                  $venda->habilitado      = 1;
                  $venda->Proposta_id     = $this->id;
                  $venda->save();
                  /*

                  /*
                 * --------Alterações novo seguro--------
                 * A proposta é segurada, então, vamos adicionar itens de uma venda (produto seguro do tipo serviço)
                 * à venda relacionada a esta aprovação de proposta

                  if( $this->segurada )

                  {

                  }
                 */

                $analistaHasP = AnalistaHasPropostaHasStatusProposta::model()->find('Proposta_id = ' . $this->id . ' AND Analista_id = ' . Yii::app()->session['usuario']->id . ' AND Status_Proposta_id = 2');

                //$this->gerarRegistroRetornoParceiro('01');

                if ($analistaHasP != NULL) {

                    if (!$analistaHasP->habilitado) {
                        $analistaHasP->habilitado = 1;
                    }

                    $analistaHasP->updated_at = date('Y-m-d H:i:s');
                    $analistaHasP->update();
                } else {
                    $analistaHasPropHasStatusP = new AnalistaHasPropostaHasStatusProposta;
                    $analistaHasPropHasStatusP->Analista_id = Yii::app()->session['usuario']->id;
                    $analistaHasPropHasStatusP->Proposta_id = $this->id;
                    $analistaHasPropHasStatusP->Status_Proposta_id = $this->Status_Proposta_id;
                    $analistaHasPropHasStatusP->habilitado = 1;
                    $analistaHasPropHasStatusP->data_cadastro = date('Y-m-d H:i:s');
                    $analistaHasPropHasStatusP->updated_at = date('Y-m-d H:i:s');
                    $analistaHasPropHasStatusP->save();
                }
            }
        }
    }

    public function getTitulo() {
        return Titulo::model()->find('Proposta_id = ' . $this->id . ' AND NaturezaTitulo_id = 1 AND habilitado');
    }

    public function getParcelas()
    {

        $retorno = [];

        $titulo = $this->getTitulo();

        if ($titulo != NULL)
        {

            $parcelas = Parcela::model()->findAll('habilitado AND Titulo_id = ' . $titulo->id);

            if ($parcelas != NULL && count($parcelas) > 0)
            {

                foreach ($parcelas as $parcela)
                {

                    $rangeGerado = RangesGerados::model()->find("habilitado AND Parcela_id = $parcela->id");

                    if($rangeGerado !== null)
                    {

                        $retorno[] = $parcela;

                    }

                }

                return $retorno;

            }
            else
            {
                return NULL;
            }

        }
        else
        {
            return NULL;
        }
    }

    public function getByCodigo($codigo, $parceiroId, $itensPgtoAprovados, $totalAtualGrid, $totalAtualGridFinan) {
        $util = new Util;
        $arrReturn = array('encontrado' => true);
        $arrReturn['totalParcial'] = $totalAtualGrid;
        $arrReturn['totalFinanciado'] = $totalAtualGridFinan;

        $itensPgtoAprovadosAux = join(',', $itensPgtoAprovados);
        $parceirosIds[] = $parceiroId;
        $criteria = new CDbCriteria;

        $criteria->with = array('analiseDeCredito' => array('alias' => 'ac'));
        $criteria->condition = 't.codigo = ' . "'$codigo' AND t.id NOT IN($itensPgtoAprovadosAux) AND ac.Filial_id IN($parceiroId) AND t.titulos_gerados AND t.Status_Proposta_id = 2 AND t.habilitado";

        $proposta = Proposta::model()->find($criteria);

        if ($proposta == NULL) {
            $arrReturn['encontrado'] = false;
            $arrReturn['pnotify'] = array(
                'titulo' => 'Notificação',
                'texto' => 'Não foi possível adicionar a proposta ao borderô.',
                'tipo' => 'error',
            );
        } else {

            if (ItemDoBordero::model()->find('Proposta_id = ' . $proposta->id . ' AND habilitado') != NULL || ItemPendente::model()->find('ItemPendente = ' . $proposta->id . ' AND habilitado') != NULL) {

                $arrReturn['encontrado'] = false;
                $arrReturn['pnotify'] = array(
                    'titulo' => 'Notificação',
                    'texto' => 'Esta proposta está pendente ou em outro Borderô.',
                    'tipo' => 'error',
                );
            } else if (SolicitacaoDeCancelamento::model()->find('Proposta_id = ' . $proposta->id) != NULL) {
                $arrReturn['encontrado'] = false;
                $arrReturn['pnotify'] = array(
                    'titulo' => 'Notificação',
                    'texto' => 'Esta proposta possui uma solicitação de cancelamento.',
                    'tipo' => 'error',
                );
            } else {
                $arrReturn['gridAprovado'] = array(
                    'id' => $proposta->id,
                    'codigo' => $proposta->codigo,
                    'cpf' => $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero,
                    'nome' => strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome),
                    'valFin' => "R$ " . number_format($proposta->valor - $proposta->valor_entrada, 2, ',', '.'),
                    'valRep' => "R$ " . number_format($util->arredondar($proposta->valorRepasse()), 2, ',', '.'),
                    'btnRem' => '<a data-id-prop="item_' . $proposta->id . '_id" data-valor-fin="' . ($proposta->valor - $proposta->valor_entrada) . '" data-valor-repasse="' . $util->arredondar($proposta->valorRepasse()) . '" href="#" class="btn btn-xs btn-bricky btn-remover-item"><i class="fa fa-times fa fa-white"></i></a>'
                );

                $arrReturn['valFinDouble'] = $proposta->valor - $proposta->valor_entrada;
                $arrReturn['valRepDouble'] = $util->arredondar($proposta->valorRepasse());

                $arrReturn['totalParcial'] += $util->arredondar($proposta->valorRepasse());
                $arrReturn['totalParcialText'] = number_format($arrReturn['totalParcial'], 2, ',', '.');

                $arrReturn['totalFinanciado'] += ($proposta->valor - $proposta->valor_entrada);
                $arrReturn['totalFinanText'] = number_format($arrReturn['totalFinanciado'], 2, ',', '.');
            }
        }

        return $arrReturn;
    }

    public function desfazerPendencia($codigoProposta) {
        $retorno = false;

        $util = new Util;

        $criteria = new CDbCriteria;

        $codigoContrato = substr($codigoProposta, 1, strlen($codigoProposta) - 3);
        //$criteria->condition = "codigo = '" . $codigoProposta . "' ";
        $criteria->condition = "(t.codigo = '" . $codigoProposta . "' OR t.numero_do_contrato = '" . $codigoContrato . "')  AND t.Status_Proposta_id IN(2,7) ";

        $proposta = Proposta::model()->find($criteria);

        if ($proposta != null) {
            $itemPendente = ItemPendente::model()->find('ItemPendente = ' . $proposta->id . ' AND habilitado');

            if ($itemPendente != null) {
                $itemPendente->habilitado = 0;

                if ($itemPendente->save()) {
                    $proposta->Status_Proposta_id = 2;
                    $retorno = $proposta->save();
                }
            }

            return array(
                'codigo' => $itemPendente->itemPendente->codigo,
                'cpf' => $itemPendente->itemPendente->analiseDeCredito->cliente->pessoa->getCPF()->numero,
                'filial' => $itemPendente->itemPendente->analiseDeCredito->filial->getConcat(),
                'nome' => strtoupper($itemPendente->itemPendente->analiseDeCredito->cliente->pessoa->nome),
                'valFin' => "R$ " . number_format($itemPendente->itemPendente->getValorFinanciado(), 2, ',', '.'),
                'valRep' => "R$ " . number_format($itemPendente->itemPendente->valorRepasse(), 2, ',', '.'),
                'btnRem' => '<a data-id-prop="item_' . $itemPendente->itemPendente->id . '_id" data-valor-fin="' . ($itemPendente->itemPendente->valor - $itemPendente->itemPendente->valor_entrada) . '" data-valor-repasse="' . $util->arredondar($itemPendente->itemPendente->valorRepasse()) . '" href="#" class="btn btn-xs btn-bricky btn-remover-item"><i class="fa fa-times fa fa-white"></i></a>',
            );
        } else {
            return [];
        }
    }

    public function getByCodigoContrato($codigo, $itensPgtoAprovados, $totalAtualGrid, $totalAtualGridFinan, $porImagem) {

        $util = new Util;
        $arrReturn = array('encontrado' => true);
        $arrReturn['totalParcial'] = $totalAtualGrid;
        $arrReturn['totalFinanciado'] = $totalAtualGridFinan;

        $itensPgtoAprovadosAux = join(',', $itensPgtoAprovados);
        $codigoContrato = substr($codigo, 1, strlen($codigo) - 3);

        $criteria = new CDbCriteria;

        $valorParametroIdsFinanceira = ConfigLN::model()->valorDoParametro("financeirasBipAntigo");

        if ($valorParametroIdsFinanceira !== null) {
            eval('$idsFinanceiras = ' . $valorParametroIdsFinanceira . ';');
        } else {
            eval('$idsFinanceiras = [5,6,10,14];');
        }

        $valorParametroUsuariosFinanceiroTodasFinanceiras = ConfigLN::model()->valorDoParametro("usuariosFinanceiroTodasFinanceiras");

        if ($valorParametroUsuariosFinanceiroTodasFinanceiras !== null) {
            eval('$idUsuariosFIDC = ' . $valorParametroUsuariosFinanceiroTodasFinanceiras . ';');
        } else {
            eval('$idUsuariosFIDC = [5,6,10,14];');
        }

        $criteria->with = array('analiseDeCredito' => array('alias' => 'ac'));
        //$criteria->condition            = 't.numero_do_contrato = ' . "'$codigoContrato'  AND t.id NOT IN($itensPgtoAprovadosAux)   AND t.titulos_gerados AND t.Status_Proposta_id = 2 AND t.habilitado";
        $criteria->condition = "(t.codigo = '" . $codigo . "' OR t.numero_do_contrato = '" . $codigoContrato . "')   AND t.id NOT IN($itensPgtoAprovadosAux)   AND t.titulos_gerados AND t.Status_Proposta_id IN(2,7) AND t.habilitado";

        $proposta = Proposta::model()->find($criteria);


        /* Proposta não encontrada */
        if ($proposta == NULL) {
            $p = Proposta::model()->find("t.numero_do_contrato = '" . $codigoContrato . "' OR t.codigo = '" . $codigo . "'");

            if ($p != NULL) {

                if (in_array($p->Financeira_id, $idsFinanceiras) || (in_array(Yii::app()->session["usuario"]->id, $idUsuariosFIDC))) {

                    $emprestimo = Emprestimo::model()->find("habilitado AND Proposta_id = $p->id");

                    if ($emprestimo == null) {

                        if (!$p->titulos_gerados && $p->Status_Proposta_id != 8) {
                            $arrReturn['encontrado'] = false;
                            $arrReturn['pnotify'] = array(
                                'titulo' => 'Notificação',
                                'texto' => 'Os boletos desta proposta não foram impressos. Contate o Parceiro.',
                                'tipo' => 'error',
                            );
                        } else if ($p->Status_Proposta_id == 8) {
                            $arrReturn['encontrado'] = false;
                            $arrReturn['pnotify'] = array(
                                'titulo' => 'Notificação',
                                'texto' => 'A proposta está cancelada. Contate o Parceiro.',
                                'tipo' => 'error',
                            );
                        } else if ($p->Status_Proposta_id == 10) {
                            $arrReturn['encontrado'] = false;
                            $arrReturn['pnotify'] = array(
                                'titulo' => 'Notificação',
                                'texto' => 'A proposta está em desistencia. Contate o Parceiro.',
                                'tipo' => 'error',
                            );
                        }
                    } else {
                        $arrReturn['encontrado'] = false;
                        $arrReturn['pnotify'] = array(
                            'titulo' => 'Notificação',
                            'texto' => 'Proposta de Empréstimo.',
                            'tipo' => 'error'
                        );
                    }
                } else {
                    $arrReturn['encontrado'] = false;
                    $arrReturn['pnotify'] = array
                        (
                        'titulo' => "Notificação",
                        'texto' => "Financeira não permitida nesse modelo de Formalização",
                        'tipo' => 'error'
                    );
                }
            } else {
                $arrReturn['encontrado'] = false;
                $arrReturn['pnotify'] = array(
                    'titulo' => 'Notificação',
                    'texto' => "Proposta não encontrada. Err 00x1 <br>"
                    . "Código: $codigo <br>"
                    . "Contrato: $codigoContrato",
                    'tipo' => 'error',
                );
            }
        } else {


            $emprestimo = Emprestimo::model()->find("habilitado AND Proposta_id = $proposta->id");

            if ($emprestimo == null) {
                $pagoImagem = '';
                $itemDoBordero = ItemDoBordero::model()->find('Proposta_id = ' . $proposta->id . ' AND habilitado');

                if ($itemDoBordero != NULL) {
                    if ($itemDoBordero->pagoImagem !== 1)
                    {

                        if(isset($itemDoBordero->bordero->processo->id))
                        {
                            $idProcesso         = $itemDoBordero->bordero->processo->id;
                            $textoNotificacao   = 'Esta proposta está em outro Borderô. <div class="ui-pnotify-action-bar" style="margin-top: 25px; clear: both; text-align: right;"><form method="POST" target="_blank"  action="/printer/recebimentoDeContratoOmni/"><input type="hidden" value="' . $idProcesso . '" name="codigo"><input type="submit" value="Imprimir" class="ui-pnotify-action-button btn btn-bricky"/></form></div>';
                        }
                        else
                        {

                            if(isset($itemDoBordero->bordero->id))
                            {
                                $idBordero = $itemDoBordero->bordero->id;
                            }
                            else
                            {
                                $idBordero = 0;
                            }

                            $textoNotificacao = "Esta proposta está em outro Borderô. ID do Bordero " . $idBordero;
                        }

                        $arrReturn['encontrado'] = false;
                        $arrReturn['pnotify'] = array(
                            'titulo' => 'Notificação',
                            'texto' => $textoNotificacao,
                            'tipo' => 'error',
                        );
                    } else {
                        $pagoImagem = ' ( Pago por Imagem )';
                    }
                }

                if (ItemPendente::model()->find('ItemPendente = ' . $proposta->id . ' AND habilitado') != NULL) {
                    $arrReturn['encontrado'] = false;
                    $arrReturn['pendente'] = 1;
                    $arrReturn['pnotify'] = array(
                        'titulo' => 'Notificação',
                        'texto' => 'Esta proposta está Pendente.',
                        'tipo' => 'error',
                    );
                } else if (SolicitacaoDeCancelamento::model()->find('Proposta_id = ' . $proposta->id . ' AND aceite AND AceitePor IS NOT NULL') != NULL) {

                    $arrReturn['encontrado'] = false;
                    $arrReturn['pnotify'] = array(
                        'titulo' => 'Notificação',
                        'texto' => 'Esta proposta possui uma solicitação de cancelamento.',
                        'tipo' => 'error',
                    );
                } else {

                    $deuCerto = true;

                    if (!in_array($proposta->Financeira_id, $idsFinanceiras)) {

                        if (!in_array(Yii::app()->session["usuario"]->id, $idUsuariosFIDC)) {

                            $deuCerto = FALSE;

                            $arrReturn['encontrado'] = false;
                            $arrReturn['pnotify'] = array
                                (
                                'titulo' => "Notificação",
                                'texto' => "Financeira não permitida nesse modelo de Formalização",
                                'tipo' => 'error'
                            );
                        } else {
                            $titulo = Titulo::model()->find("habilitado AND Proposta_id = $proposta->id");

                            if ($titulo !== null) {

                                $parcela = Parcela::model()->find("habilitado AND Titulo_id = $titulo->id");

                                if ($parcela !== null) {

                                    $deuCerto = true;
                                    /* $linhaCNAB = LinhaCNAB::model()->find("habilitado AND Parcela_id = $parcela->id ");

                                      if($linhaCNAB == null)
                                      {
                                      $deuCerto = FALSE;

                                      $arrReturn['encontrado' ] = false;
                                      $arrReturn['pnotify'    ] = array
                                      (
                                      'titulo'    =>  "Notificação"                       ,
                                      'texto'     =>  "Proposta ainda não formalizada."   ,
                                      'tipo'      => 'error'
                                      );

                                      } */
                                } else {

                                    $arrReturn['encontrado'] = false;
                                    $arrReturn['pnotify'] = array
                                        (
                                        'titulo' => "Notificação",
                                        'texto' => "Parcela de título a pagar não encontrado.",
                                        'tipo' => 'error'
                                    );
                                }
                            } else {

                                $deuCerto = FALSE;

                                $arrReturn['encontrado'] = false;
                                $arrReturn['pnotify'] = array
                                    (
                                    'titulo' => "Notificação",
                                    'texto' => "Título a pagar não encontrado.",
                                    'tipo' => 'error'
                                );
                            }
                        }
                    }

                    if ($deuCerto) {

                        $arrReturn['gridAprovado'] = array(
                            'id' => $proposta->id,
                            'codigo' => $proposta->codigo . $pagoImagem,
                            'cpf' => $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero,
                            'filial' => $proposta->analiseDeCredito->filial->getConcat(),
                            'nome' => strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome),
                            'valFin' => "R$ " . number_format($proposta->valor - $proposta->valor_entrada, 2, ',', '.'),
                            'valRep' => "R$ " . number_format($util->arredondar($proposta->valorRepasse()), 2, ',', '.'),
                            'btnRem' => '<a data-id-prop="item_' . $proposta->id . '_id" data-valor-fin="' . ($proposta->valor - $proposta->valor_entrada) . '" data-valor-repasse="' . $util->arredondar($proposta->valorRepasse()) . '" href="#" class="btn btn-xs btn-bricky btn-remover-item"><i class="fa fa-times fa fa-white"></i></a>',
                            'porImagem' => $porImagem == 0 ? 0 : 1
                        );

                        $arrReturn['incluido'] = 1;

                        $arrReturn['valFinDouble'] = $proposta->valor - $proposta->valor_entrada;
                        $arrReturn['valRepDouble'] = $util->arredondar($proposta->valorRepasse());

                        $arrReturn['totalParcial'] += $util->arredondar($proposta->valorRepasse());
                        $arrReturn['totalParcialText'] = number_format($arrReturn['totalParcial'], 2, ',', '.');

                        $arrReturn['totalFinanciado'] += ($proposta->valor - $proposta->valor_entrada);
                        $arrReturn['totalFinanText'] = number_format($arrReturn['totalFinanciado'], 2, ',', '.');
                        $arrReturn['idFilial'] = $proposta->analiseDeCredito->filial->id;
                    }
                }
            } else {
                $arrReturn['encontrado'] = false;
                $arrReturn['pnotify'] = array(
                    'titulo' => 'Notificação',
                    'texto' => 'Proposta de Empréstimo.',
                    'tipo' => 'error'
                );
            }
        }

        return $arrReturn;
    }

    public function statusPagamentos() {

        $titulo = $this->getTitulo();
        $campoCnabConfig = new CampoCnabConfig;

        $arrReturn = array(
            'emAberto' => 0,
            'pago' => 0,
            'atrasado' => 0,
            'cancelado' => 0,
            'vencendoHoje' => 0
        );

        if ($titulo != NULL) {
            $parcelas = $titulo->listarParcelas();

            foreach ($parcelas as $parcela) {

                $baixa = Baixa::model()->find('Parcela_id = ' . $parcela->id);

                if ($baixa == NULL) {

                    if ($parcela->atrasada()) {
                        $arrReturn['atrasado'] += $parcela->valor;
                        $arrReturn['emAberto'] += $parcela->valor;
                    } else {
                        $arrReturn['emAberto'] += $parcela->valor;
                    }

                    if ($parcela->diasVencimento() == 0) {
                        $arrReturn['vencendoHoje'] += $parcela->valor;
                    }
                } else {
                    $arrReturn['pago'] += $parcela->valor + $campoCnabConfig->cnabValorMonetarioToDouble($baixa->valor_mora);
                }
            }
        }

        return $arrReturn;
    }

    public function enviarParaFila($Operacao) {

        $arrReturn = [];

        $proposta = Proposta::model()->findByPk($Operacao['Proposta_id']);

        $omniConfig = $proposta->hasOmniConfig();

        $configuracoes = SigacConfig::model()->findByPk(1);

        if ($configuracoes->auth_reanalise != $Operacao['senha']) {
            $arrReturn['titulo'] = 'Erros foram encontrados!';
            $arrReturn['texto'] = 'Senha incorreta.';
            $arrReturn['tipo'] = 'error';
        } else {
            if ($omniConfig != NULL) {

                $omniConfig->habilitado = 0;

                if (!$omniConfig->update()) {

                    ob_start();
                    var_dump($omniConfig->getErrors());
                    $erroOmniConfig = ob_get_clean();

                    $arrReturn['titulo'] = 'Erros foram encontrados!';
                    $arrReturn['texto'] = "Erro ao desabilitar as configurações de proposta. Erro: $erroOmniConfig";
                    $arrReturn['tipo'] = 'error';
                }
            }

            if ($proposta->analiseDeCredito->filial->nucleoFilial->GrupoFiliais_id == 5 || $proposta->analiseDeCredito->filial->nucleoFilial->GrupoFiliais_id == 16 || $proposta->analiseDeCredito->filial->nucleoFilial->GrupoFiliais_id == 19) {

                $proposta->Financeira_id = 11;
                $proposta->codigo = "4" . str_pad($proposta->id, 6, 0, STR_PAD_LEFT);
            } else {
                $proposta->codigo = $proposta->analiseDeCredito->codigo . '1';
            }

            $proposta->Status_Proposta_id = 4;
            $proposta->data_cadastro = date('Y-m-d H:i:s');
            $proposta->data_cadastro_br = date('d/m/Y H:i:s');
            $proposta->analise_solicitada = 0;

            if ($proposta->update()) {

                $analises = AnalistaHasPropostaHasStatusProposta::model()->findAll('Proposta_id = ' . $proposta->id);

                foreach ($analises as $analise) {
                    $analise->delete();
                }

                $arrReturn['titulo'] = 'Sucesso!';
                $arrReturn['texto'] = 'Proposta Enviada para a fila de análise';
                $arrReturn['tipo'] = 'success';
            } else {
                $arrReturn['titulo'] = 'Erros foram encontrados!';
                $arrReturn['texto'] = 'Não foi possível enviar a proposta. Err 00x1';
                $arrReturn['tipo'] = 'error';
            }
        }

        return $arrReturn;
    }

    public function btnStatusAprovado() {

        $pendencia = ItemPendente::model()->find('ItemPendente = ' . $this->id);
        $itemBordero = ItemDoBordero::model()->find('Proposta_id = ' . $this->id);

        if ($pendencia != NULL) {
            return '<button data-original-title="Motivo" data-content="' . strtoupper($pendencia->observacao) . '" data-placement="left" data-trigger="hover" type="buton" style="width:100%!important" class="btn btn-sm btn-warning popovers">Pendente</button>';
        } else if ($itemBordero != NULL) {
            return '<button type="buton" style="width:100%!important" class="btn btn-sm btn-primary">Autorizado</button>';
        } else {
            return '<button type="buton" style="width:100%!important" class="btn btn-sm btn-success">Aprovado</button>';
        }
    }

    public function getFator() {
        $condition = 'habilitado AND Tabela_Cotacao_id = ' . $this->Tabela_id . ' AND carencia = ' . $this->carencia . ' AND parcela = ' . $this->qtd_parcelas;

        return Fator::model()->find($condition);
    }

    public function hasOmniConfig() {
        return PropostaOmniConfig::model()->find('habilitado AND Proposta = ' . $this->id);
    }

    public function hasOmniConfigPrinter() {
        return PropostaOmniConfig::model()->find('habilitado AND Proposta = ' . $this->id);
    }

    /*
      As duas funções abaixo ajudam a renderizar os links ideiais (boleto e contratos), de acordo
      com quem aprovou a proposta. Caso a proposta possuir uma configuração externa, renderizamos
      os links do parceiro. Caso não, nós fornecemos os links internos.
     */

    public function renderImpressaoContrato() {

        $omniConfig = $this->hasOmniConfigPrinter();
        $linkContrato = null;

        $html = "";


        if ($omniConfig != NULL) {
            $omniConfig->atualizarLinks();

            if ($omniConfig->urlContrato == NULL) {

                $this->omniConfigSave($omniConfig);

                $omniConfig = $this->hasOmniConfigPrinter();
                $linkContrato = $omniConfig->urlContrato;
            } else {
                $linkContrato = $omniConfig->urlContrato;
            }

            $html = '<a href="' . $linkContrato . '" target="_blank" class="btn btn-google-plus">
                            <i class="clip-note"></i> |
                            Desejo abrir e imprimir o contrato
                        </a><br />';
        } 
        else 
        {
            $html = '<form target="_blank" action="/contrato/" method="POST">
                            <button type="submit" class="btn btn-google-plus">
                                <i class="clip-note"></i> |
                                Desejo abrir e imprimir o contrato
                            </button>
                            <input id="propId" type="hidden" value="' . $this->id . '" name="propostaId">
                        </form>';
        }

        $emprestimo = Emprestimo::model()->find("habilitado AND Proposta_id = $this->id");

        if ($emprestimo !== null) {
            if ($this->Financeira_id != 7) {
                $html .= '<br>
                         <form target="_blank" action="/contrato/cartaCredito" method="POST">
                            <button type="submit" class="btn btn-google-plus">
                                <i class="clip-note"></i> |
                                Desejo abrir e imprimir a carta de crédito
                            </button>
                            <input id="propostaId" type="hidden" value="' . $this->id . '" name="propostaId">
                         </form>';
            } else {
                $html .= '<br /><a href="' . $omniConfig->urlCG . '" target="_blank" class="btn btn-google-plus">
                                <i class="clip-note"></i> |
                                Desejo abrir o termo de condições gerais
                            </a><br />';
            }
        }

        return $html;
    }

    public function renderImpressaoBoleto() {
        $omniConfig = $this->hasOmniConfigPrinter();

        if ($omniConfig != NULL) {

            $omniConfig->atualizarLinks();

            if ($omniConfig->urlBoleto == NULL) {
                $this->omniConfigSave($omniConfig);
                $omniConfig = $this->hasOmniConfigPrinter();
            }

            return '  <form target="_blank" action="/printer/boleto/" method="POST">
                      <button type="submit" class="btn btn-google-plus">
                          <i class="clip-note"></i> |
                          Imprimir boletos
                      </button>
                      <input type="hidden" value="' . $omniConfig->id . '" name="omniConfig">
                    </form>';
        } else {
            return '<form target="_blank" action="/boleto/" method="POST">
          <button type="submit" class="btn btn-google-plus">
            <i class="clip-note"></i> |
            Imprimir boletos
          </button>
          <input type="hidden" name="c" value="' . $this->codigo . '" />
          <input type="hidden" name="tg" value="' . $this->titulos_gerados . '" />
        </form>';
        }
    }

    ///////////////////////////////////////////////////////////
    //funcao para obter a senha de acesso no WS da financeira//
    ///////////////////////////////////////////////////////////
    public function getSenhaXML($ambiente = 1) { //1 = producao; 2=homologacao
        if ($ambiente == 1) {
            return "OMNI2015";
        } else {
            return "homologacao";
        }
    }

    ////////////////////////////////////////////////////////////////////
    //funcao para obter o codigo da loja referente no WS da financeira//
    ////////////////////////////////////////////////////////////////////
    public function getLojaXML($ambiente = 1, $proposta = null, $alterar = 0) { //1 = producao; 2=homologacao
        //"54192"  ;belem
        //"54183"  ;nova cruz
        //"63983"  ;natal-escritorio
        //"8124585";credshow
        /*$arrFiliaisOmni = [122,125,124,126];
        $retorno = "";

        if ($proposta == null) {
            $proposta = $this;
        }

        $pOmniConfig = PropostaOmniConfig::model()->find("habilitado AND Proposta = $proposta->id");

        if ($ambiente == 1) {

            if (isset($proposta->analiseDeCredito->filial)) {
                $nucleo = NucleoFiliais::model()->find("habilitado AND id = " . $proposta->analiseDeCredito->filial->NucleoFiliais_id);
            } else {
                $nucleo = null;
            }

            if (isset($nucleo->GrupoFiliais_id)) {
                $idGrupoFilial = $nucleo->GrupoFiliais_id;
            } else {
                $idGrupoFilial = 0;
            }

            if (
                    ($idGrupoFilial == 4 || $idGrupoFilial == 7 //|| $idGrupoFilial == 16
                    || $idGrupoFilial == 21) && (isset($pOmniConfig->usuarioHost) && $pOmniConfig->usuarioHost == "01630LHSTCREDSHOW")
            ) {
                if ($alterar == 1) {
                    $retorno = "8124585";
                } else {
                    $retorno = "63983";
                }
            } else {
                if (isset($pOmniConfig->usuarioHost) && $pOmniConfig->usuarioHost == "01630LHSTMAREMANSA") {
                    $retorno = "63983";
                } else {

                    if (($proposta->getValorFinanciadoOmni() > 1500) && ($idGrupoFilial == 4 || $idGrupoFilial == 7 //|| $idGrupoFilial == 16
                            || $idGrupoFilial == 21) && !$alterar) {
                        $retorno = "63983";
                    } else {
                        $retorno = "8124585";
                    }
                }
            }
        } else {

            $retorno = "53580";
        }

        if( $proposta != null && in_array($proposta->analiseDeCredito->filial->id, $arrFiliaisOmni) ){
            $retorno = "8124585";
        }*/

        $retorno = "8124585"; //momentaneamente 09/01/2017 17:02

        if ($proposta != null)
        {
            //if( $proposta->analiseDeCredito->filial->id == 106 ){
            if (in_array($proposta->analiseDeCredito->filial->NucleoFiliais_id, array(40)))
            {
                $retorno = "8124566";
            }
        }

        /*if( $this->analiseDeCredito->filial->id == 106 ){
            $retorno = "8124566";
        }*/

        return $retorno;
    }

    ////////////////////////////////////////////////////////////////////////////
    //funcao para obter o tipo de financiamento para envio ao Ws da financeira//
    ////////////////////////////////////////////////////////////////////////////
    public function getTipoFinanciamentoXML($ambiente = 1, $proposta = null, $tabelasAntigas = null, $alterar = 0) { //1 = producao; 2=homologacao
        /*
          Tabela anterior
         * 5702 1
         * 5817 2
         * 5622 3
         * 5962 4
         * 6013 5
          Nova tabela
         * 6268 1
         * 6269 2
         * 6270 3
         * 6271 4
         * 6272 5
         *
         *
          //sem juros
          if ($tabela->id == 14 || $tabela->id == 17 || $tabela->id == 16 || $tabela->id == 33 || $tabela->id == 37 || $tabela->id == 38) {
          $tipoFinanciamento = $this->getTipoFinanciamentoXML(2,$proposta);
          }

          //com juros
          else if ($tabela->id == 15 || $tabela->id == 27 || $tabela->id == 28 || $tabela->id == 36 || $tabela->id == 39) {
          $tipoFinanciamento = $this->getTipoFinanciamentoXML(1,$proposta);
          } else if ($tabela->id == 24) {
          $tipoFinanciamento = $this->getTipoFinanciamentoXML(3,$proposta);
          } else if ($tabela->id == 34 || $tabela->id == 35) {
          $tipoFinanciamento = $this->getTipoFinanciamentoXML(7,$proposta);
          } else if ($tabela->id == 30 || $tabela->id == 26) {
          $tipoFinanciamento = $this->getTipoFinanciamentoXML(4,$proposta);
          }
         *
         */

        if ($proposta == null) {
            $proposta = $this;
        }

        $pOmniConfig = PropostaOmniConfig::model()->find("habilitado AND Proposta = $proposta->id");

        if (isset($proposta->analiseDeCredito->filial)) {
            $nucleo = NucleoFiliais::model()->find("habilitado AND id = " . $proposta->analiseDeCredito->filial->NucleoFiliais_id);
        } else {
            $nucleo = null;
        }

        if (isset($nucleo->GrupoFiliais_id)) {
            $idGrupoFilial = $nucleo->GrupoFiliais_id;
        } else {
            $idGrupoFilial = 0;
        }

        if (
                ($idGrupoFilial == 4 || $idGrupoFilial == 7 //|| $idGrupoFilial == 16
                || $idGrupoFilial == 21) && (isset($pOmniConfig->usuarioHost) && $pOmniConfig->usuarioHost == "01630LHSTCREDSHOW")
        ) {
            //            if ($alterar == 1) {
                $tabelasAntigas = false;
            /*            } else {
                $tabelasAntigas = true;
            }*/
        } else {
            if (isset($pOmniConfig->usuarioHost) && $pOmniConfig->usuarioHost == "01630LHSTMAREMANSA") {
                $tabelasAntigas = true;
            } else {

                /*if (($proposta->getValorFinanciadoOmni() > 1500) && ($idGrupoFilial == 4 || $idGrupoFilial == 7 //|| $idGrupoFilial == 16
                        || $idGrupoFilial == 21) && !($alterar)) {
                    $tabelasAntigas = true;
                } else {*/
                    $tabelasAntigas = false;
                //}
            }
        }

        ob_start();
        var_dump($proposta->analiseDeCredito->filial->id);
        $vardumpFilialId = ob_get_clean();

        //        if(in_array($proposta->analiseDeCredito->filial->id, array("106") ))
        if(in_array($proposta->analiseDeCredito->filial->NucleoFiliais_id, array(40) ))
        {

            $texto = "Filial: " . $proposta->analiseDeCredito->filial->id . " Oxi";

            /*$message = new YiiMailMessage;
            $message->view = "propostaModeloContrato";
            $message->subject = 'Modelo Contrato/Proposta';

            $parametros = [
                    'email' => [
                            'titulo' => "Filial.",
                            'texto' => $texto
                    ]
            ];

            $message->setBody($parametros, 'text/html');

            $message->addTo('contato@totorods.com');
            $message->from = 'no-reply@credshow.com.br';

            Yii::app()->mail->send($message);*/

            return "6872";

        }
        else
        {


            $texto = "Filial: " . $proposta->analiseDeCredito->filial->id . " "
                    . "vardump: $vardumpFilialId";

            /*$message = new YiiMailMessage;
            $message->view = "propostaModeloContrato";
            $message->subject = 'Modelo Contrato/Proposta';

            $parametros = [
                    'email' => [
                            'titulo' => "Filial.",
                            'texto' => $texto
                    ]
            ];

            $message->setBody($parametros, 'text/html');

            $message->addTo('contato@totorods.com');
            $message->from = 'no-reply@credshow.com.br';

            Yii::app()->mail->send($message);*/

        }

        if ($ambiente == 1) {
            //com juros
            //return "5676";
            if ($tabelasAntigas) {
                return "5622";
            } else {
                return "6270";
            }
        } else if ($ambiente == 3) {
            if ($tabelasAntigas) {
                return "5622";
            } else {
                return "6270";
            }
        } else if ($ambiente == 4) {
            //flavio

            if ($tabelasAntigas) {
                return "5702";
            } else {
                return "6268";
            }
        } else if ($ambiente == 7) {
            //vieira

            if ($tabelasAntigas) {
                return "5817";
            } else {
                return "6269";
            }
        } else if ($ambiente == 11) {
            //vieira

            if ($tabelasAntigas) {
                return "6399";
            } else {
                return "6400";
            }
        } else if($ambiente == 12){
            ///return "6762";
            return "6268";
        } else if($ambiente == 13){
            return "6829";
        } else {
            //return "5687"; //retenção
            if ($tabelasAntigas) {
                return "5687";
            } else {
                if ($proposta->qtd_parcelas >= 6) {
                    return "6321";
                } else {
                    return "6077";
                }
            }
        }
    }

    ////////////////////////////////////////////////
    //funcao para obter o link do WS da financeira//
    ////////////////////////////////////////////////
    public function getLinkXML($ambiente = 1) { //1 = producao; 2=homologacao
        if ($ambiente == 1) {
            //return 'https://www.omnifacil.com.br/wsProposta/servidor_soap_proposta.wsdl?service=ICM_Credito';
            return 'https://hst1.omni.com.br/wsProposta/servidor_soap_proposta.wsdl?service=ICM_Credito';
            //return 'https://www.omnifacil.com.br/wsProposta/servidor_soap_proposta.wsdl?service=ICM_Credito';
        } else {
            //return 'http://omnifacil2.omni.com.br:8080/homolog/wsProposta/servidor_soap_proposta.wsdl?service=ICM_Credito';
            return 'http://omnifacil2.omni.com.br/homolog/wsProposta/servidor_soap_proposta.wsdl?service=ICM_Credito';
        }
    }

    ///////////////////////////////////////////////////////////////
    //funcao para obter as respostas do WS da financeira parceira//
    ///////////////////////////////////////////////////////////////
    public function getMensagemXML($comentarios) {
        $result = ''; //variavel que guardara o retorno
        $enter = Chr(13) . Chr(10); //variavel que serve para dar o salto de linha em uma string
        $listaComentarios = get_object_vars($comentarios); //transforme o objeto comentarios em um array de comentarios
        //percorra todos os comentarios trazido no xml de resposta
        foreach ($listaComentarios as $c) {
            $result .= str_repeat('-', 20) . $enter; //separador de comentarios

            $result .= 'Data:       ' . $c->dataHora . $enter; //data do comentario
            $result .= 'Status:     ' . $c->situacao . $enter; //status na hora que o comentario foi inserido
            $result .= 'Comentário: ' . $c->comentario . $enter; //comentario (observacao)
        }

        return $result;
    }

    public function getEmprestimo() {

        return Emprestimo::model()->find('habilitado AND Proposta_id = ' . $this->id);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //funcao para verificar se ainda esta permitido efetuar o cancelamento do emprestimo. caso   //
    //o emprestimo ja tenha sido pago, não permita que seja criada a solicitacao de cancelamento //
    ///////////////////////////////////////////////////////////////////////////////////////////////
    public function emprestimoAceitaCancelamento() {

        //busque a natureza do titulo a pagar do emprestimo
        $naturezaTitulo = ConfigLN::model()->find("habilitado AND parametro = 'naturezaTituloPagarEmprestimo'");

        if ($naturezaTitulo == null) {
            $valorNaturezaTitulo = 32; //valor padrao
        } else {
            $valorNaturezaTitulo = $naturezaTitulo->valor;
        }

        //ache o registro de emprestimo
        $emprestimo = Emprestimo::model()->find("habilitado AND Proposta_id = $this->id");

        //verifica se a proposta esta relacionada a um emprestimo
        if ($emprestimo !== null) {

            //ache o registro do titulo
            $titulo = Titulo::model()->find("habilitado AND NaturezaTitulo_id = $valorNaturezaTitulo AND Proposta_id = $this->id");

            //se o titulo for encontrado
            if ($titulo !== null) {

                //até esta data e provavelmente para todo o sempre, titulos
                //a pagar de emprestimo possuem uma unica parcela, caso mude
                //favor modificar essas linhas de codigo
                $parcela = Parcela::model()->find("habilitado AND Titulo_id = $titulo->id");

                //se achar a parcela
                if ($parcela !== null) {

                    //tente achar o registro da parcela
                    $baixa = Baixa::model()->find("baixado AND Parcela_id = $parcela->id");

                    //se achar a baixa
                    if ($baixa !== null) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /////////////////////////////////////////////////////////////////////////////
    //funcao para incluir as mensagens da proposta no WS da financeira parceira//
    /////////////////////////////////////////////////////////////////////////////
    public function incluiMensagemXML() {

        $result = ''; //variavel que guardara a string de retorno
        $enter = Chr(13) . Chr(10); //variavel que serve para dar o salto de linha em uma string
        $util = new Util; //utilidades
        $dialogo = $this->getDialogo(); //retorne o dialogo relacionado a esta proposta
        //retorne todas as msg's deste dialogo
        $mensagens = Mensagem::model()->findAll('Dialogo_id = ' . $dialogo->id . ' ORDER BY id DESC');

        foreach ($mensagens as $m) {
            $result .= str_repeat('-', 20) . $enter; //separador de comentarios
            $result .= 'Remetente: ' . strtoupper($m->usuario->nome_utilizador) . $enter; //remetente
            $result .= 'Data:      ' . $util->bd_date_to_view($m->data_criacao) . $enter; //data
            $result .= 'Assunto:   ' . strtoupper($m->dialogo->assunto) . $enter; //assunto
            $result .= 'Mensagem:  ' . strtoupper($m->conteudo) . $enter; //mensagem
        }

        return $result;
    }

    //////////////////////////////////////////////////////////////////////////////////
    //funcao para atualizar todas as propostas que estao em poder de WS de terceiros//
    //////////////////////////////////////////////////////////////////////////////////
    public function atualizaPropostasWS() {

        //trazer todas as Propostas com integracao que nao estao
        $propostasWS = PropostaOmniConfig::model()->findAll('not interno');

        //percorra todas propostas
        foreach ($propostasWS as $p) {

            //busque a proposta corrente
            $proposta = Proposta::model()->findByPk($p->Proposta);

            if ($proposta !== null && in_array($proposta->Status_Proposta_id, array(1, 4))) {
                //busque a atualização no WS da integração
                $proposta->updateOmniStatus();
            }
        }
    }

    public function resgatarCodigoContrato($Importacao) {
        $criteria = new CDbCriteria;

        $propostas = [];

        $dataDe = $Importacao['data_de'] . ' 00:00:00';
        $dataAte = $Importacao['data_ate'] . ' 23:59:59';

        $criteria->condition = 't.Status_Proposta_id = 3 AND t.data_cadastro BETWEEN "' . $dataDe . '" AND "' . $dataAte . '"';

        foreach (Proposta::model()->findAll($criteria) as $proposta) {
            $proposta->numero_do_contrato = $proposta->updateCodigoContratoOmni();

            if ($proposta->update()) {
                $propostas[] = $proposta->numero_do_contrato;
            }
        }

        return [$propostas];
    }

    public function returnInformacoes($id) {
        $proposta = Proposta::model()->findByPk($id);

        if ($proposta != NULL) {

            $recebimentosParcelas = $proposta->statusRecebimentosParcelas();

            return [
                'codigo' => $proposta->codigo,
                'cliente' => strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome),
                'cpf' => $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero,
                'valor_financiado' => $proposta->valor - $proposta->valor_entrada,
                'valor_final' => $proposta->getValorFinal(),
                'iof' => 0,
                'seguro' => $proposta->calcularValorDoSeguro(),
                'recebido' => $recebimentosParcelas['recebido'],
                'vencido' => $recebimentosParcelas['vencido'],
                'areceber' => $recebimentosParcelas['avencer'],
                'data_cadastro' => $proposta->data_cadastro,
                'parceiro' => $proposta->analiseDeCredito->filial->getConcat(),
                'cnpj' => $proposta->analiseDeCredito->filial->cnpj
            ];
        } else {
            return NULL;
        }
    }

    public function ehCartao() {

        $ehCartao = false;

        $cartaoHasProposta = CartaoHasProposta::model()->find("habilitado AND Proposta_id = $this->id");

        if ($cartaoHasProposta !== null) {
            $ehCartao = true;
        }

        return $ehCartao;
    }

    public function cancelarPropostas() {

        $qtdDiasCancelamento = ConfigLN::model()->valorDoParametro('qtd_dias_modificar');

        $dataCancelamento = date("Y-m-d", strtotime("-$qtdDiasCancelamento days"));

        $propostas = Proposta::model()->findAll("habilitado AND Status_Proposta_id = 9 AND LEFT(data_cadastro,10) <= '$dataCancelamento'");

        foreach ($propostas as $proposta) {

            $transaction = Yii::app()->db->beginTransaction();

            $proposta->Status_Proposta_id = ConfigLN::model()->valorDoParametro('status_pos_modificar');

            if ($proposta->update()) {

                $pOmniCfg = PropostaOmniConfig::model()->find('habilitado AND Proposta = ' . $proposta->id);

                $gravouOCfg = true;

                if ($pOmniCfg !== null) {

                    $pOmniCfg->habilitado = 0;

                    if (!$pOmniCfg->update()) {
                        $gravouOCfg = false;
                    }
                }

                if ($gravouOCfg) {

                    $transaction->commit();

                    $observacao = "Proposta atualizada com sucesso. Proposta cancelada devido a falta de interacao.";

                    $sigacLog = new SigacLog;

                    $sigacLog->saveLog(
                            'Atualização de Status 9 para 10', // ação
                            'Proposta', // entidade relacionamento
                            $proposta->id, // id registro entidade
                            date('Y-m-d H:i:s'), // data horario
                            1, // habilitado
                            13, // id usuario (colocar parametro)
                            $observacao, // observacao
                            "127.0.0.1", // ip
                            null, // cookie id
                            Yii::app()->session->getSessionId()       // session id
                    );
                } else {

                    $transaction->rollBack();

                    ob_start();
                    var_dump($pOmniCfg->getErrors());
                    $resultado = ob_get_clean();

                    $observacao = "Proposta estava sendo colocada como desistência devido a falta de interacao. Status Modificar. $resultado";

                    $sigacLog = new SigacLog;

                    $sigacLog->saveLog(
                            'Erro ao atualizar Status 9 para 10', // ação
                            'Proposta', // entidade relacionamento
                            $proposta->id, // id registro entidade
                            date('Y-m-d H:i:s'), // data horario
                            1, // habilitado
                            13, // id usuario (colocar parametro)
                            $observacao, // observacao
                            "127.0.0.1", // ip
                            null, // cookie id
                            Yii::app()->session->getSessionId()       // session id
                    );
                }
            } else {

                $transaction->rollBack();

                ob_start();
                var_dump($proposta->getErrors());
                $resultado = ob_get_clean();

                $observacao = "Proposta estava sendo colocada como desistência devido a falta de interacao. Status Modificar. $resultado";

                $sigacLog = new SigacLog;

                $sigacLog->saveLog(
                        'Erro ao atualizar Status 9 para 10', // ação
                        'Proposta', // entidade relacionamento
                        $proposta->id, // id registro entidade
                        date('Y-m-d H:i:s'), // data horario
                        1, // habilitado
                        13, // id usuario (colocar parametro)
                        $observacao, // observacao
                        "127.0.0.1", // ip
                        null, // cookie id
                        Yii::app()->session->getSessionId()       // session id
                );
            }
        }
    }

    /*
      Retorna o recebido, a vencer e vencido
     */

    public function statusRecebimentosParcelas() {
        $totalRecebido = 0;
        $totalAVencer = 0;
        $totalVencido = 0;

        $parcelas = $this->getParcelas();

        if ($parcelas != NULL) {
            foreach ($this->getParcelas() as $parcela) {
                /* Recebido */
                if ($parcela->valor_atual > 0) {
                    $totalRecebido += $parcela->valor_atual;
                }

                /* Valor em aberto ou é atrasado ou é a vencer */
                if (( $parcela->valor_atual < $parcela->valor)) {
                    /* Vencido */
                    if ($parcela->atrasada()) {
                        $totalVencido += $parcela->valor;
                    }

                    /* A vencer */
                    if (!$parcela->atrasada()) {
                        $totalAVencer += $parcela->valor;
                    }
                }
            }
        }

        return ['recebido' => $totalRecebido, 'avencer' => $totalAVencer, 'vencido' => $totalVencido];
    }

    public function propostasNFE($draw, $start, $filters) {

        $rows = [];

        $btnDetails = "";

        $parametroNaturezaTitulo = ConfigLN::model()->find("habilitado AND parametro = 'idNaturezaTituloReceber'");

        $parametroModeloContrato = ConfigLN::model()->find("habilitado AND parametro = 'modelosXML'             ");

        if ($parametroNaturezaTitulo !== null) {
            $idNaturezaTituloReceber = $parametroNaturezaTitulo->valor;
        } else {
            $idNaturezaTituloReceber = 1;
        }

        if ($parametroModeloContrato !== null) {
            $idsModelos = $parametroModeloContrato->valor;
        } else {
            $idsModelos = "1";
        }

        $sql1 = "";
        $sql2 = "";
        $sql3 = "";

        $sqlFilter = NULL;

        if (isset($filters[0]) && !empty($filters[0])) {
            $sqlFilter = " AND pr.codigo LIKE '%" . $filters[0] . "%' ";
        }

        if (isset($filters[1]) && !empty($filters[1])) {
            $sqlFilter .= " AND pess.nome LIKE '%" . $filters[1] . "%' ";
        }

        $rows = array();
        $sql = "   SELECT pr.data_cadastro as data_de_cadastro, pr.id as Proposta_id, pr.codigo, pr.habilitado, pr.data_cadastro, pess.nome ";
        $sql .= "   FROM        Proposta                    as pr ";
        //        $sql .= "   INNER JOIN  PropostaOmniConfig          as poc  ON   pr.id =  poc.Proposta              AND  poc.habilitado ";
        $sql .= "   INNER JOIN  Analise_de_Credito          as acr  ON  acr.id =   pr.Analise_de_Credito_id AND  acr.habilitado ";
        $sql .= "   INNER JOIN  Cliente                     as clie ON clie.id =  acr.Cliente_id            AND clie.habilitado ";
        $sql .= "   INNER JOIN  Pessoa                      as pess ON pess.id = clie.Pessoa_id             AND pess.habilitado ";
        //        $sql .= "   INNER JOIN  Titulo                      as tit  ON   pr.id =   tit.Proposta_id          AND  tit.habilitado ";
        $sql .= "   LEFT  JOIN  Solicitacao_de_Cancelamento as sc   ON   pr.id =   sc.Proposta_id           AND   sc.habilitado ";
        $sql .= "   LEFT  JOIN  Emprestimo                  as e    ON   pr.id =    e.Proposta_id           AND    e.habilitado ";
        $sql .= " WHERE pr.habilitado AND (sc.id IS NULL OR (sc.data_finalizacao IS NOT NULL AND sc.aceite = 0)) AND pr.titulos_gerados AND pr.Financeira_id = 10 ";
        //        $sql .= "                     AND tit.NaturezaTitulo_id =   $idNaturezaTituloReceber ";
        //        $sql .= "                     AND  pr.ModeloContrato_id IN ($idsModelos) ";

        $sql1 = $sql;

        $recordsTotal = count(Yii::app()->db->createCommand($sql)->queryAll());

        if ($sqlFilter != NULL) {
            $sql .= $sqlFilter;
        }

        $sql2 = $sql;

        $recordsFiltered = count(Yii::app()->db->createCommand($sql)->queryAll());

        if ($recordsFiltered > 0) {

            $sql .= "   ORDER BY data_de_cadastro DESC ";

            $sql .= " LIMIT " . $start . " ,10";

            $sql3 = $sql;

            foreach (Yii::app()->db->createCommand($sql)->queryAll() as $r) {

                $proposta = Proposta::model()->findByPk($r['Proposta_id']);

                /* Já possui xml nf anexado */
                if ($proposta->hasXmlNf()) {

                    $btnDetails = '    <button data-proposta-id="' . $proposta->id . '" data-toggle="modal" data-target="#modalAnexo"  style="font-size:10px;" type="submit" '
                            . '            title="Reenviar XML" class="btn btn-dark-gray btn-modal-xml">'
                            . '        <i class="clip-spinner-6"></i> <i class="clip-file-xml"></i> XML NF'
                            . '    </button>';
                } else {

                    $btnDetails = '    <button data-proposta-id="' . $proposta->id . '" data-toggle="modal" data-target="#modalAnexo"  style="font-size:10px;" type="submit" class="btn btn-primary btn-modal-xml"'
                            . '            title="Enviar XML">'
                            . '        <i class="clip-file-xml"></i> XML NF'
                            . '    </button>';
                }

                $btnStatus = '<span class="' . $proposta->statusProposta->cssClass . '">' . $proposta->statusProposta->status . '</span>';

                //                if ($proposta->titulos_gerados)
                //                {

                $nomeCliente = strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome);
                $nomeCrediarista = strtoupper($proposta->analiseDeCredito->usuario->nome_utilizador);

                $valorProposta = number_format($proposta->valor, 2, ',', '');
                $valorEntrada = number_format($proposta->valor_entrada, 2, ',', '');
                $valorSeguro = number_format($proposta->calcularValorDoSeguro(), 2, ',', '');
                $valorFinanciado = number_format($proposta->getValorFinanciado(), 2, ',', '');
                $valorFinal = number_format($proposta->getValorFinal(), 2, ',', '');

                $qtdParcelas = $proposta->qtd_parcelas . 'x ' . number_format($proposta->getValorParcela(), 2, ',', '.');

                $row = array(
                    'btn' => $btnDetails,
                    'codigo' => $proposta->codigo,
                    'cliente' => $nomeCliente,
                    'crediarista' => $nomeCrediarista,
                    'valor' => $valorProposta,
                    'valor_entrada' => $valorEntrada,
                    'seguro' => $valorSeguro,
                    'val_financiado' => $valorFinanciado,
                    'qtd_parcelas' => $qtdParcelas,
                    'valor_final' => $valorFinal,
                    'btn_status' => $btnStatus
                );

                $rows[] = $row;

                //                }
            }
        }


        return [
            "draw" => $draw,
            "recordsFiltered" => $recordsFiltered,
            "recordsTotal" => $recordsTotal,
            "data" => $rows,
            "sql1" => $sql1,
            "sql2" => $sql2,
            "sql3" => $sql3,
        ];
    }

    /* Esta função é chamada quase sempre que o valor da proposta é alterado */

    public function atualizarValorProdutoSeguro() {
        /* Possui venda? */
        if ($this->venda !== NULL) {
            if (count($this->venda->itemDaVendas) > 0) {
                foreach ($this->venda->itemDaVendas as $item) {
                    if (($item->itemDoEstoque->produto->id == 1) && ($item->habilitado)) {
                        $coberturaSeguro = ConfigLN::model()->find("habilitado AND parametro = 'cobertura_seguro_" . $item->Item_do_Estoque_id . "'");

                        if ($coberturaSeguro !== NULL) {
                            $item->setPreco((($this->valor - $this->valor_entrada) / 100) * intval($coberturaSeguro->valor));
                            $item->update();
                        }
                    }
                }
            }
        }
    }

    public function hasXmlNf() {
        $retorno = false;

        $venda = Venda::model()->find("habilitado AND Proposta_id = $this->id");

        if ($venda !== null) {

            $vendaHasNF = VendaHasNotaFiscal::model()->find("habilitado AND Venda_id = $venda->id");

            if (isset($vendaHasNF->notaFiscal)) {
                $retorno = true;
            }
        }

        return $retorno;
    }

    public function getNFs() {

        $retorno = [];

        $venda = Venda::model()->find("habilitado AND Proposta_id = $this->id");

        if ($venda !== null) {

            $vHasNFs = VendaHasNotaFiscal::model()->findAll("habilitado AND Venda_id = $venda->id");

            foreach ($vHasNFs as $vhnf) {

                $nf = NotaFiscal::model()->find("habilitado AND id = $vhnf->Nota_Fiscal_id");

                if ($nf !== null) {
                    $retorno[] = $nf;
                }
            }
        }

        return $retorno;
    }

    public function nfAnexos() {

        $retorno = [
            "qtdNFe" => 0,
            "qtdNFeAnexo" => 0
        ];

        $notas = $this->getNFs();

        $qtdAnexo = 0;

        foreach ($notas as $nf) {

            $anexo = Anexo::model()->find("habilitado AND entidade_relacionamento = 'NotaFiscal' AND id_entidade_relacionamento = $nf->id");

            if ($anexo !== null) {
                $qtdAnexo++;
            }
        }

        $retorno = [
            "qtdNFe" => count($notas),
            "qtdNFeAnexo" => $qtdAnexo
        ];

        return $retorno;
    }

    public function getCustoEfetivoTotal($periodo = 1) { // periodo (1 = mensal; 2 = anual)
        $cet = 0;

        $arrayCET = [(0 - ($this->valor - $this->valor_entrada))];

        for ($i = 0; $i < $this->qtd_parcelas; $i++) {
            $arrayCET[] = $this->valor_parcela;
        }

        $cetMensal = Math_Finance::internalRateOfReturn($arrayCET, 0.02);

        if ($periodo == 2) {
            $cet = (pow((1 + $cetMensal), 12) - 1);
        } else {
            $cet = $cetMensal;
        }

        return $cet;
    }

    public function negadaAutomaticamente() {
        if (AnalistaHasPropostaHasStatusProposta::model()->find('Status_Proposta_id = 3 AND Analista_id = 913 AND habilitado AND Proposta_id = ' . $this->id) != NULL) {
            return true;
        } else {
            return false;
        }
    }

}
