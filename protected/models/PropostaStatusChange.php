<?php

/**
 * This is the model class for table "Proposta_Status_Change".
 *
 * The followings are the available columns in table 'Proposta_Status_Change':
 * @property integer $id
 * @property string $data_cadastro
 * @property string $de
 * @property string $para
 * @property integer $Usuario_id
 * @property integer $Proposta_id
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property Usuario $usuario
 * @property Proposta $proposta
 */
class PropostaStatusChange extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Proposta_Status_Change';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Usuario_id, Proposta_id', 'required'),
			array('Usuario_id, Proposta_id, habilitado', 'numerical', 'integerOnly'=>true),
			array('de, para', 'length', 'max'=>45),
			array('data_cadastro', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, data_cadastro, de, para, Usuario_id, Proposta_id, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuario' => array(self::BELONGS_TO, 'Usuario', 'Usuario_id'),
			'proposta' => array(self::BELONGS_TO, 'Proposta', 'Proposta_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'data_cadastro' => 'Data Cadastro',
			'de' => 'De',
			'para' => 'Para',
			'Usuario_id' => 'Usuario',
			'Proposta_id' => 'Proposta',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('de',$this->de,true);
		$criteria->compare('para',$this->para,true);
		$criteria->compare('Usuario_id',$this->Usuario_id);
		$criteria->compare('Proposta_id',$this->Proposta_id);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PropostaStatusChange the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
