<?php

class Banco extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function returnSantanderCna400bConfig() {

        return array(
            'camposHeader' => array(48, 49, 50, 51, 52, 107, 58, 77, 60, 61, 101, 109, 110, 111, 112, 113, 114, 115, 67),
            'camposDetalhe' => array(
                1, 2, 3, 116, 117, 118, 9, 119, 120,
                121, 122, 123, 124, 125, 126, 127,
                15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
                25, 26, 27, 28, 29, 30, 31, 32, 33, 35,
                128, 38, 39, 40, 41, 42, 129, 130, 131,
                132, 133, 45, 135, 47
            ),
            'camposTrailer' => array(68, 136, 137, 138, 71)
        );
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Banco';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nome', 'length', 'max' => 45),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, nome, codigo', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'nome' => 'Nome',
            'codigo' => 'Código',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('nome', $this->nome, true);
        $criteria->compare('codigo', $this->codigo);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function configuracoesBanco() {
        return [
            'credshow'                  => [
                'cnpj'                  => '13861348000115',
                'r_s'                   => 'Credshow Operadora de Credito S/A',
                'agencia'               => '45438', //Já com DV
                'conta'                 => '0130029221', //Já com DV
                'psk'                   => '7204175',
                'codigo_de_transmissao' => '45430720417501300292',
                'complemento'           => '21',
                'cod_cedente'           => '7204175',
            ],
            'novacruz'                  => [
                'cnpj'                  => '07221618000120',
                'r_s'                   => 'Nova Cruz Comércio de Móveis Ltda',
                'agencia'               => '32115', //Já com DV
                'conta'                 => '0130026432', //Já com DV
                'psk'                   => '6978738',
                'codigo_de_transmissao' => '32110697873801300264',
                'complemento'           => '32',
                'cod_cedente'           => '0003211',
            ],
            'credshow_bradesco'         => [
                'cnpj'                  => '13861348000115',
                'r_s'                   => 'Credshow Operadora de Credito Ltda',
                'agencia'               => '02632',
                'dvagencia'             => '8',
                'conta'                 => '0010081', // SEM DV
                'dvconta'               => '1',
                'convenio'              => '4735680',
                'carteira'              => '002',
                'idBradesco'            => '00020263200100811',
                'codBradesco'           => '237',
                'ag_cod_boleto'         => '02632-8 / 0010081-1',
                'endereco'              => 'Av Amintas Barros, 3700, 21° andar, CTC, Torre B, Lagoa Nova, Natal, RN'
            ],
            'credshow_bradesco_fidic'   => [
                'cnpj'                  => '23273905000130',
                'r_s'                   => 'Credshow Fundo de Investimentos em Direitos Creditorios LP',
                'agencia'               => '02632',
                'dvagencia'             => '8',
                'conta'                 => '0000073', // SEM DV
                'dvconta'               => '6',
                'convenio'              => '4878797',
                'carteira'              => '009',
                'idBradesco'            => '00090263200000736', // ident_da_empresa_beneficiaria_no_banco
                'codBradesco'           => '237',
                'ag_cod_boleto'         => '02632-8 / 0000073-6',
                'endereco'              => 'Av Brigadeiro Faria de Lima, 1533, 3 and, Jardim Paulistano - São Paulo, SP'
            ],
            'boleto_credshow_nb'        => [
                'cnpj'                  => '23273905000130',
                'r_s'                   => 'Credshow Fundo de Investimentos em Direitos Creditorios LP',
                'agencia'               => '01010',
                'dvagencia'             => '1',
                'conta'                 => '0000010', // SEM DV
                'dvconta'               => '1',
                'convenio'              => '1010101',
                'carteira'              => '010',
                'idBradesco'            => '00010101000000101', // ident_da_empresa_beneficiaria_no_banco
                'codBradesco'           => '010',
                'ag_cod_boleto'         => '01010-1 / 0000010-1',
                'endereco'              => 'Av Brigadeiro Faria de Lima, 1533, 3 and, Jardim Paulistano - São Paulo, SP'
            ],
        ];
    }

    public function listarSelect($idNucleo) {

        $bancos = Banco::model()->findAll();

        $html = '';

        $html .= '<select class="form-control search-select bancoNucleo">';

        $html .= '  <option value="0" ';

        if ($idNucleo == 0) {
            $html .= 'selected';
        }

        $html .= ' >';
        $html .= 'Selecione um Banco...';
        $html .= '</option>';

        foreach ($bancos as $banco) {

            $html .= '<option value="' . $banco->id . '" ';


            $html .= ' >' . $banco->codigo . ' - ' . $banco->nome . '</option>';
        }

        $html .= '</select>';

        return (
                array(
                    "html" => $html
                )
                );
    }

}
