<?php

class Compra extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Compra';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('data, Fornecedor_id, TipoCompra_Id', 'required'),
            array('Fornecedor_id, TipoCompra_Id', 'numerical', 'integerOnly' => true),
            array('id, data, Fornecedor_id, TipoCompra_Id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'fornecedor' => array(self::BELONGS_TO, 'Fornecedor', 'Fornecedor_id'),
            'tipoCompra' => array(self::BELONGS_TO, 'TipoCompra', 'TipoCompra_Id'),
            'compraHasFormaDePagamentoHasCondicaoDePagamentos' => array(self::HAS_MANY, 'CompraHasFormaDePagamentoHasCondicaoDePagamento', 'Compra_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'data' => 'Data',
            'Fornecedor_id' => 'Fornecedor',
            'TipoCompra_Id' => 'Tipo Compra',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('data', $this->data, true);
        $criteria->compare('Fornecedor_id', $this->Fornecedor_id);
        $criteria->compare('TipoCompra_Id', $this->TipoCompra_Id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getValorTotal() {
        $valorTotal = 0;

        foreach (ItemDaCompra::model()->findAll('Compra_id = ' . $this->id) as $i) {
            $valorTotal += $i->valor;
        }

        return $valorTotal;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Compra the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
