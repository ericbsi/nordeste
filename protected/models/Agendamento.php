<?php

/**
 * This is the model class for table "Agendamento".
 *
 * The followings are the available columns in table 'Agendamento':
 * @property integer $id
 * @property string $data_cadastro
 * @property integer $habilitado
 * @property string $data_interacao
 * @property integer $Atendimento_id
 * @property integer $Tipo_Agendamento_id
 * @property integer $Usuario_cobranca
 * @property integer $Usuario_cliente
 *
 * The followings are the available model relations:
 * @property Atendimento $atendimento
 * @property TipoAgendamento $tipoAgendamento
 * @property Usuario $usuarioCobranca
 * @property Usuario $usuarioCliente
 */
class Agendamento extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Agendamento';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('data_cadastro, data_interacao, Atendimento_id, Tipo_Agendamento_id, Usuario_cobranca, Usuario_cliente', 'required'),
			array('habilitado, Atendimento_id, Tipo_Agendamento_id, Usuario_cobranca, Usuario_cliente', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, data_cadastro, habilitado, data_interacao, Atendimento_id, Tipo_Agendamento_id, Usuario_cobranca, Usuario_cliente', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'atendimento' => array(self::BELONGS_TO, 'Atendimento', 'Atendimento_id'),
			'tipoAgendamento' => array(self::BELONGS_TO, 'TipoAgendamento', 'Tipo_Agendamento_id'),
			'usuarioCobranca' => array(self::BELONGS_TO, 'Usuario', 'Usuario_cobranca'),
			'usuarioCliente' => array(self::BELONGS_TO, 'Usuario', 'Usuario_cliente'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'data_cadastro' => 'Data Cadastro',
			'habilitado' => 'Habilitado',
			'data_interacao' => 'Data Interacao',
			'Atendimento_id' => 'Atendimento',
			'Tipo_Agendamento_id' => 'Tipo Agendamento',
			'Usuario_cobranca' => 'Usuario Cobranca',
			'Usuario_cliente' => 'Usuario Cliente',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_interacao',$this->data_interacao,true);
		$criteria->compare('Atendimento_id',$this->Atendimento_id);
		$criteria->compare('Tipo_Agendamento_id',$this->Tipo_Agendamento_id);
		$criteria->compare('Usuario_cobranca',$this->Usuario_cobranca);
		$criteria->compare('Usuario_cliente',$this->Usuario_cliente);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Agendamento the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
