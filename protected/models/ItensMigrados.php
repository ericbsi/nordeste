<?php

/**
 * This is the model class for table "ItensMigrados".
 *
 * The followings are the available columns in table 'ItensMigrados':
 * @property integer $id
 * @property string $tabela
 * @property integer $registro
 * @property string $obs
 * @property string $data_cadastro
 * @property integer $habilitado
 * @property integer $Migracao_id
 *
 * The followings are the available model relations:
 * @property Migracao $migracao
 */
class ItensMigrados extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ItensMigrados';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tabela, registro, obs, data_cadastro, habilitado, Migracao_id', 'required'),
			array('registro, habilitado, Migracao_id', 'numerical', 'integerOnly'=>true),
			array('tabela', 'length', 'max'=>150),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tabela, registro, obs, data_cadastro, habilitado, Migracao_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'migracao' => array(self::BELONGS_TO, 'Migracao', 'Migracao_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tabela' => 'Tabela',
			'registro' => 'Registro',
			'obs' => 'Obs',
			'data_cadastro' => 'Data Cadastro',
			'habilitado' => 'Habilitado',
			'Migracao_id' => 'Migracao',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tabela',$this->tabela,true);
		$criteria->compare('registro',$this->registro);
		$criteria->compare('obs',$this->obs,true);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('Migracao_id',$this->Migracao_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ItensMigrados the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
