<?php

/**
 * This is the model class for table "ItensBuscados".
 *
 * The followings are the available columns in table 'ItensBuscados':
 * @property integer $id
 * @property string $tabela
 * @property string $registro
 * @property string $data_cadastro
 * @property integer $habilitado
 * @property integer $sucesso
 * @property string $obs
 * @property integer $Migracao_id
 *
 * The followings are the available model relations:
 * @property Migracao $migracao
 */
class ItensBuscados extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ItensBuscados';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tabela, registro, data_cadastro, sucesso, obs, Migracao_id', 'required'),
			array('habilitado, sucesso, Migracao_id', 'numerical', 'integerOnly'=>true),
			array('tabela', 'length', 'max'=>45),
			array('registro', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tabela, registro, data_cadastro, habilitado, sucesso, obs, Migracao_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'migracao' => array(self::BELONGS_TO, 'Migracao', 'Migracao_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tabela' => 'Tabela',
			'registro' => 'Registro',
			'data_cadastro' => 'Data Cadastro',
			'habilitado' => 'Habilitado',
			'sucesso' => 'Sucesso',
			'obs' => 'Obs',
			'Migracao_id' => 'Migracao',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tabela',$this->tabela,true);
		$criteria->compare('registro',$this->registro,true);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('sucesso',$this->sucesso);
		$criteria->compare('obs',$this->obs,true);
		$criteria->compare('Migracao_id',$this->Migracao_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function getAllRegOk($origem,$destino)
        {
            $registros = "0";
            
            $query  = "SELECT registro ";
            $query .= "FROM ItensBuscados AS ib ";
            $query .= "INNER JOIN Migracao AS m ON m.habilitado AND m.id = ib.Migracao_id ";
            $query .= "WHERE ib.habilitado and ib.sucesso AND m.origem = " . $origem . " AND m.destino = " . $destino . "";
            
            $dados = Yii::app()->db->createCommand($query)->queryAll();
            
//            var_dump($dados);
            
            foreach ($dados as $d)
            {
                $registros .= $d['registro'] . ",";
            }
            
/*            echo '<br><br>';
            
            echo $registros . '<br><br>';*/
            
            if (strlen($registros) > 1)
            {
                $registros = substr($registros, 0, -1);
            }
            
//            echo $registros . '<br><br>';
            
            return $registros;
        }

        /**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ItensBuscados the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
