<?php

/**
 * This is the model class for table "Item_do_Estoque".
 *
 * The followings are the available columns in table 'Item_do_Estoque':
 * @property integer $id
 * @property integer $Produto_id
 * @property integer $Estoque_id
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property Produto $produto
 * @property Estoque $estoque
 */
class ItemDoEstoque extends CActiveRecord
{	

	public function getFinanceira()
	{
		return FinanceiraHasItemDoEstoque::model()->find('ItemDoEstoque_id = ' . $this->id);
	}

	public function getNumeroApolice()
	{
		return ConfigLN::model()->find("habilitado AND parametro = 'apolice_seguro_".$this->id."'")->valor;
	}

	public function getViewContrato(){
		return ConfigLN::model()->find("habilitado AND parametro = 'contrato_seguro_".$this->id."'")->valor;
	}

	public function loadJson($id){

		$item  = ItemDoEstoque::model()->findByPk($id);
                $valor = ItemTabelaDePrecos::model()->searchMenorPreco($item);

		return array(
			'idItem'		=> $item->id,
			'produto'		=> $item->produto->descricao,
			'filial' 		=> $item->estoque->filial->nome_fantasia,
			'filial_end'	=> $item->estoque->filial->getEndereco()->cidade  .' - ' .$item->estoque->filial->getEndereco()->uf,
			'estoque'		=> $item->estoque->local->sigla,
			'estoqueId'		=> $item->estoque->id,
			'precoVenda'		=> $valor,
		);
	}
        
        public function buscaItemEstoque($estoque,$produto)
        {
            $itemEstoque = ItemDoEstoque::model()->find(
                    array(
                        'condition' => 'Estoque_id = '. $estoque->id . ' and Produto_id = ' . $produto->id
                        )
            );
            
            if ($itemEstoque == null)
            {
                $itemEstoque = new ItemDoEstoque;
                
                $itemEstoque->Estoque_id = $estoque->id;
                $itemEstoque->Produto_id = $produto->id;
                $itemEstoque->saldo      = 0;
                
                $itemEstoque->save();
                
            }
            
            return $itemEstoque;
            
        }


        /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Item_do_Estoque';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Produto_id, Estoque_id, habilitado', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, Produto_id, Estoque_id, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'produto' => array(self::BELONGS_TO, 'Produto', 'Produto_id'),
			'estoque' => array(self::BELONGS_TO, 'Estoque', 'Estoque_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Produto_id' => 'Produto',
			'Estoque_id' => 'Estoque',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Produto_id',$this->Produto_id);
		$criteria->compare('Estoque_id',$this->Estoque_id);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ItemDoEstoque the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
