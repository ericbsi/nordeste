<?php

class Ncm extends CActiveRecord
{	

	public function searchNcmSelec2($q){

		$retorno = null;

		if( !empty( $q ) ){

			$crt 	= new CDbCriteria;
			$crt->addSearchCondition('t.codigo', 	$q, TRUE, 'OR');
			$crt->addSearchCondition('t.descricao', $q, TRUE, 'OR');

			$ncms 	= Ncm::model()->findAll( $crt );

			if( count( $ncms ) > 0 )
			{
				foreach( $ncms as $ncm )
				{
					$row = array(
						'id'	=> $ncm->id,
						'text'	=> $ncm->codigo . ' - ' . $ncm->descricao
					);
					$retorno[] 	= $row;
				}
			}
		}

		return $retorno;
	}

	public function add($NCM){

		$arrReturn = array(
			'hasErrors' => '0',
			'classNtfy' => '',
			'msg' 		=> '',
		);

		if( !empty($NCM['codigo']) )
		{
			$novoNCM				= new Ncm;		
			$novoNCM->attributes 	= $NCM;
			
			if( $novoNCM->save() )
			{
				$arrReturn['classNtfy'] = 'success';
				$arrReturn['msg'] 		= 'NCM cadastrado com sucesso';
			}
			else
			{
				$arrReturn['classNtfy'] = 'error';
				$arrReturn['msg'] 		= 'Erro ao cadastrar.';
			}
		}
		return $arrReturn;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Ncm';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('codigo, descricao', 'required'),
			array('habilitado', 'numerical', 'integerOnly'=>true),
			array('codigo', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, codigo, descricao, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'produtos' => array(self::HAS_MANY, 'Produto', 'Ncm_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'codigo' => 'Codigo',
			'descricao' => 'Descricao',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('codigo',$this->codigo,true);
		$criteria->compare('descricao',$this->descricao,true);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Ncm the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
