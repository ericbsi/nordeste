<?php

class Bordero extends CActiveRecord {

    public function editarItens($itens_pgto_aprovados) {
        for ($i = 0; $i < count($itens_pgto_aprovados); $i++) {
            if ($itens_pgto_aprovados[$i] != 0 && ItemDoBordero::model()->find('Proposta_id = ' . $itens_pgto_aprovados[$i]) == NULL && ItemPendente::model()->find('ItemPendente = ' . $itens_pgto_aprovados[$i]) == NULL) {
                $itemDoBordero = new ItemDoBordero;
                $itemDoBordero->Bordero = $this->id;
                $itemDoBordero->Status = 2;
                $itemDoBordero->Proposta_id = $itens_pgto_aprovados[$i];
                $itemDoBordero->save();
            }
        }
    }

    public function removerItem($itemId) {
        $arrReturn = array('hasErrors' => false);
        $item = ItemDoBordero::model()->find('Proposta_id = ' . $itemId);

        if ($item != NULL) {
            if (!$item->delete()) {
                $arrReturn['hasErrors'] = true;
            }
        } else {
            $arrReturn['hasErrors'] = true;
        }

        return $arrReturn;
    }

    public function novo($itens_pgto_aprovados, $destinatario, $imagens)
    {
        $arrReturn              = array('hasErrors' => false);
        $bordero                = new Bordero;
        $bordero->dataCriacao   = date('Y-m-d H:i:s');
        $bordero->destinatario  = $destinatario;
        $bordero->criadoPor     = Yii::app()->session['usuario']->id;
        $bordero->Status        = 1;

        $itens                  = array();

        if ($bordero->save())
        {
            for ($i = 0; $i < count($itens_pgto_aprovados); $i++)
            {   
                /*Verificando se o item em questão já está em um item de borderô*/
                if ($itens_pgto_aprovados[$i] != 0 && ItemDoBordero::model()->find('habilitado AND Proposta_id = ' . $itens_pgto_aprovados[$i]) == NULL)
                {
                    $proposta                       = Proposta::model()->findByPk($itens_pgto_aprovados[$i]);

                    /*Se a proposta estiver aprovada, com titulos gerados e habilitada*/
                    if ( $proposta->statusProposta->id == 2 && $proposta->titulos_gerados && $proposta->habilitado )
                    {
                        $itemDoBordero              = new ItemDoBordero;
                        $itemDoBordero->Bordero     = $bordero->id;
                        $itemDoBordero->Status      = 2;
                        $itemDoBordero->Proposta_id = $itens_pgto_aprovados[$i];
                        $itemDoBordero->pagoImagem  = $imagens[$i];
                        
                        if (!$itemDoBordero->save())
                        {
                            $arrReturn['hasErrors'] = true;
                            $arrReturn['itensNaoSalvos'][] = $itens_pgto_aprovados[$i];
                        }
                    }
                }
            }

            $arrReturn['borderoId'] = $bordero->id;
        }
        
        else
        {
            $arrReturn['hasErrors'] = true;
        }

        return $arrReturn;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Bordero';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('dataCriacao, destinatario, criadoPor, Status', 'required'),
            array('destinatario, criadoPor, Status, ArquivoCNAB_id', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, dataCriacao, destinatario, criadoPor, Status, ArquivoCNAB_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
                        'destinatario'      => array(self::BELONGS_TO   , 'Filial'                      , 'destinatario'    ),
                        'status'            => array(self::BELONGS_TO   , 'StatusDoBordero'             , 'Status'          ),
                        'criadoPor'         => array(self::BELONGS_TO   , 'Usuario'                     , 'criadoPor'       ),
                        'arquivoCNAB'       => array(self::BELONGS_TO   , 'ArquivoCNAB'                 , 'ArquivoCNAB_id'  ),
                        'itemDoBorderos'    => array(self::HAS_MANY     , 'ItemDoBordero'               , 'Bordero'         ),
                        'processo'          => array(self::HAS_ONE      , 'RecebimentoDeDocumentacao'   , 'Bordero'         ),
                    );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                        'id'                => 'ID'             ,
                        'dataCriacao'       => 'Data Criacao'   ,
                        'destinatario'      => 'Destinatario'   ,
                        'criadoPor'         => 'Criado Por'     ,
                        'Status'            => 'Status'         ,
                        'ArquivoCNAB_id'    => 'Arquivo CNAB'   ,
                    );
    }

    public function totalRepasse() {
        $util = new Util;
        $repasse = 0;

        foreach ($this->itemDoBorderos as $item) 
        {
            if($item->habilitado)
            {
                $repasse += $util->arredondar($item->proposta->valorRepasse());
            }
        }

        return $repasse;
    }

    public function totalFinanciado() {
        $util = new Util;
        $totalFin = 0;

        foreach ($this->itemDoBorderos as $item) 
        {
            
            if($item->habilitado)
            {
                $totalFin += ($item->proposta->valor - $item->proposta->valor_entrada);
            }
            
        }

        return $totalFin;
    }
    
    public function ajustarTitulosPagar()
    {
        
        $resultado = [];
        
        $query  = "SELECT DISTINCT P.id AS idProposta "
                . "FROM         Proposta                    AS P "
                . "LEFT  JOIN   Titulo                      AS T    ON   T.habilitado AND P.id =  T.Proposta_id AND  T.NaturezaTitulo_id = 2    "
                . "LEFT  JOIN   Emprestimo                  AS E    ON   E.habilitado AND P.id =  E.Proposta_id                                 "
                . "LEFT  JOIN   Titulo                      AS eT   ON  eT.habilitado AND P.id = eT.Proposta_id AND eT.NaturezaTitulo_id = 32 "
                . "LEFT  JOIN   Solicitacao_de_Cancelamento AS SC   ON  SC.habilitado AND P.id = SC.Proposta_id AND ((SC.AceitePor IS NULL) OR (SC.AceitePor IS NOT NULL AND SC.aceite)) "
//                . "INNER JOIN   ItemDoBordero   AS IB   ON IB.habilitado AND P.id = IB.Proposta_id  "
//                . "INNER JOIN   Bordero         AS B    ON  B.habilitado AND B.id = IB.Bordero      "
                . "WHERE P.habilitado AND ((T.id IS NULL AND E.id IS NULL) OR (E.id IS NOT NULL AND eT.id IS NULL)) AND P.Status_Proposta_id = 2 AND SC.id IS NULL ";
        
        try
        {
            $resultado = Yii::app()->db->createCommand($query)->queryAll();
        } catch (Exception $exc)
        {
            echo $exc->getTraceAsString();
        }



        foreach ($resultado as $r)
        {
            $proposta = Proposta::model()->find("(habilitado OR Status_Proposta_id = 8) AND id = " . $r["idProposta"]);

            if ($proposta !== null) 
            {
                
                if($proposta->getEmprestimo() !== null)
                {
                    $idNatureza = 32;
                }
                else
                {
                    $idNatureza = 2;
                }
                
                Titulo::model()->gerarTituloAPagar($idNatureza, NULL, 1, $proposta->valorRepasse(), $proposta);
            }
            
        }
        
        /*$borderos = Bordero::model()->findAll('habilitado');

        foreach ($borderos as $bordero) {

            $itensBordero = ItemDoBordero::model()->findAll('habilitado AND Bordero = ' . $bordero->id);

            foreach ($itensBordero as $i) {

                $proposta = Proposta::model()->find('(habilitado OR Status_Proposta_id = 8) AND id = ' . $i->Proposta_id);

                if ($proposta !== null) {

                    $tituloAPagar = Titulo::model()->find('NaturezaTitulo_id = 2 AND Proposta_id = ' . $proposta->id . ' AND habilitado');

                    if ($tituloAPagar == NULL) {
                        Titulo::model()->gerarTituloAPagar(2, NULL, 1, $proposta->valorRepasse(), $proposta);
                    }
                }
            }
        }*/
        
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('dataCriacao', $this->dataCriacao, true);
        $criteria->compare('destinatario', $this->destinatario);
        $criteria->compare('criadoPor', $this->criadoPor);
        $criteria->compare('Status', $this->Status);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
