<?php

class Documento extends CActiveRecord
{	

	public function editarAtributo( $pk, $name, $value )
	{
		$arrReturn 	= array('hasErrors'=>false);
		$documento 	= Documento::model()->findByPk($pk);
		$util 		= new Util;

		if( $documento != NULL )
		{
			if( $name == 'data_emissao' )
			{
				$documento->data_emissao = $util->view_date_to_bd($value);
			}
			else
			{
				$documento->setAttribute($name, $value);
			}

			if( !$documento->update() )
			{
				$arrReturn['hasErrors'] = true;
			}
		}
		
		else
		{
			$arrReturn['hasErrors'] = true;
		}

		return $arrReturn;
	}

	public function atualizarRG($RgNovosDados, $rgId)
    {
        $util               = new Util;
        $RG                 = Documento::model()->findByPk($rgId);

        $RG->numero         = $RgNovosDados['numero'];
        $RG->orgao_emissor  = $RgNovosDados['orgao_emissor'];
        $RG->data_emissao   = $util->view_date_to_bd($RgNovosDados['data_emissao']);
        $RG->uf_emissor     = $RgNovosDados['uf_emissor'];

        if( $RG->update() )
        {
            return $RG;
        }
        else
        {
            return NULL;
        }
    }
	
	public function atualizar($Documento, $DocumentoId){

		$documento = Documento::model()->findByPk($DocumentoId);
		$documento->attributes = $Documento;

		if( !empty( $Documento['data_emissao'] ) )
		{
			$util 						= new Util;
			$documento->data_emissao 	= $util->view_date_to_bd($Documento['data_emissao']);
		}
		else
		{
			$documento->data_emissao	= NULL;
		}

		$documento->update();
	}

	public function loadFormEdit($id){

		$util 						= new Util;
		$documento 					= Documento::model()->findByPk($id);
		$data_emissao 				= ( $documento->data_emissao  	!= NULL ? $util->bd_date_to_view($documento->data_emissao) : "" );
		$orgao_emissor 				= ( $documento->orgao_emissor 	!= NULL ? $documento->orgao_emissor : "" );
		$uf_emissor 				= ( $documento->uf_emissor 		!= NULL ? $documento->uf_emissor : "" );

	    return array(

	    	'fields' 				=> array(
	    		'id'				=> array(
		    		'value' 		=> $documento->id,
		    		'type' 			=> 'text',
		    		'input_bind'	=> 'documento_id',
		    	),
		    	'Tipo_documento_id'	=> array(
		    		'value' 		=> $documento->Tipo_documento_id,
		    		'type' 			=> 'select',
		    		'input_bind'	=> 'Documento_Tipo_documento_id',
		    	),
		    	'numero'			=> array(
		    		'value' 		=> $documento->numero,
		    		'type' 			=> 'text',
		    		'input_bind'	=> 'documento_numero',
		    	),
		    	'data_emissao'		=> array(
		    		'value' 		=> $data_emissao,
		    		'type' 			=> 'text',
		    		'input_bind'	=> 'documento_data_emissao',
		    	),
		    	'orgao_emissor'		=> array(
		    		'value' 		=> $orgao_emissor,
		    		'type' 			=> 'text',
		    		'input_bind'	=> 'documento_orgao_emissor',
		    	),
		    	'uf_emissor'		=> array(
		    		'value' 		=> $uf_emissor,
		    		'type' 			=> 'select',
		    		'input_bind'	=> 'Documento_uf_emissor',
		    	),
	    	),
	    	'form_params'			=> array(
	    		'id'				=> 'form-add-doc',
	    		'action'			=> '/documento/update/',
	    	),
	    	'modal_id'				=> 'modal_form_new_doc'
	    );
	}

	public function novo($Documento, $Cliente_id){

		$arrReturn 										= array('hasErrors' => false);

		if( !empty($Documento['numero']) && !empty($Cliente_id) )
		{	
			$util										= new Util;
			$cliente 									= Cliente::model()->findByPk($Cliente_id);
			$documento 									= new Documento;
			$documento->attributes 						= $Documento;
			$documento->data_cadastro 					= date('Y-m-d H:i:s');
			$documento->data_cadastro_br 				= date('d/m/y');

			if( !empty($documento->data_emissao) && $documento->data_emissao != NULL )
			{
				$documento->data_emissao 				= $util->view_date_to_bd($documento->data_emissao);
			}			

			if( $documento->save() )
			{
				$pessoaHasDocumentos 					= new PessoaHasDocumento;
				$pessoaHasDocumentos->Documento_id 		= $documento->id;
				$pessoaHasDocumentos->Pessoa_id 		= $cliente->Pessoa_id;
				$pessoaHasDocumentos->data_cadastro 	= date('Y-m-d H:i:s');
				$pessoaHasDocumentos->data_cadastro_br 	= date('d/m/y');
				$pessoaHasDocumentos->save();
			}
		}
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Documento';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Tipo_documento_id', 'required'),
			array('habilitado, Tipo_documento_id', 'numerical', 'integerOnly'=>true),
			array('numero', 'length', 'max'=>100),
			array('orgao_emissor, uf_emissor, tipo_documento', 'length', 'max'=>100),
			array('data_emissao, data_cadastro, data_cadastro_br', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, numero, orgao_emissor, data_emissao, habilitado, data_cadastro, data_cadastro_br, uf_emissor, tipo_documento, Tipo_documento_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tipoDocumento' => array(self::BELONGS_TO, 'TipoDocumento', 'Tipo_documento_id'),
			'pessoaHasDocumentos' => array(self::HAS_MANY, 'PessoaHasDocumento', 'Documento_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'numero' => 'Numero',
			'orgao_emissor' => 'Orgao Emissor',
			'data_emissao' => 'Data Emissao',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'data_cadastro_br' => 'Data Hora / Cadastro',
			'uf_emissor' => 'Uf Emissor',
			'tipo_documento' => 'Tipo Documento',
			'Tipo_documento_id' => 'Tipo Documento',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('numero',$this->numero,true);
		$criteria->compare('orgao_emissor',$this->orgao_emissor,true);
		$criteria->compare('data_emissao',$this->data_emissao,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('data_cadastro_br',$this->data_cadastro_br,true);
		$criteria->compare('uf_emissor',$this->uf_emissor,true);
		$criteria->compare('tipo_documento',$this->tipo_documento,true);
		$criteria->compare('Tipo_documento_id',$this->Tipo_documento_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Documento the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function cadastrado($numero){

		$documento = Documento::model()->find("numero = '" . $numero . "'");

		return  ($documento != NULL ? true : false);
	}

	public function docCadastrado( $numero ) {

		$documento = Documento::model()->find( 'numero = ' . "'$numero'" );

		if ( $documento != NULL )
			echo "false";
		else
			echo "true";
	}
}
