<?php

class Fichamento extends CActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'Fichamento';
    }

    public function novo($Fichamento)
    {
        $util = new Util;
        $nFichamento = new Fichamento;
        $nFichamento->attributes = $Fichamento;
        $nFichamento->data_cadastro = $util->view_date_to_bd($Fichamento['data_cadastro']);

        $fichamentoJaExiste = Fichamento::model()->find('Parcela = ' . $Fichamento['Parcela'] . ' AND OrgaoNegacao_id = ' . $Fichamento['OrgaoNegacao_id'] . ' AND habilitado');

        if ($fichamentoJaExiste != NULL)
        {
            return NULL;
        } else
        {
            if ($nFichamento->save())
            {
                return $nFichamento;
            } else
            {
                return NULL;
            }
        }
    }

    public function listarFichamentos($draw, $start)
    {

        $rows = array();
        $util = new Util;
        $erroPersistencia = "";

        $sql = "
                    SELECT F.id AS fichamentoId, P.id AS parcelaId, FhC.id AS FHCId, FhC.StatusFichamento_id, IFNULL(B.id,0) AS baixaId
                    FROM 		Fichamento 						AS F
                    INNER JOIN 	Parcela 						AS P 	ON   P.habilitado 	AND P.id =   F.Parcela
                    INNER JOIN 	Fichamento_has_StatusFichamento AS FhC 	ON FhC.habilitado 	AND F.id = FhC.Fichamento_id
                    LEFT  JOIN  Baixa							AS B	ON   B.baixado		AND P.id =   B.Parcela_id
                    WHERE F.habilitado AND (SELECT MAX(id) FROM Fichamento_has_StatusFichamento AS FhC2 WHERE FhC2.habilitado AND FhC2.Fichamento_id = F.id) = FhC.id
                    AND FhC.StatusFichamento_id IN (2,5)
                    ORDER BY B.id DESC, FhC.StatusFichamento_id DESC 
                ";

        $fichamentos = Yii::app()->db->createCommand($sql)->queryAll();

        $recordsFiltered = count($fichamentos);
        $recordsTotal = count($fichamentos);

        $sql .= " LIMIT $start,10 "
                . " ";

        $fichamentos = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($fichamentos as $f)
        {

            $passo = "0";

            $fichamento = Fichamento::model()->findByPk($f["fichamentoId"]);
            $fichamentoHasSF = FichamentoHasStatusFichamento::model()->findByPk($f["FHCId"]);
            $teveErro = false;

            if ($f["baixaId"] > "0" && ( $f["StatusFichamento_id"] == "2" || $f["StatusFichamento_id"] == "1" ))
            {

                $passo = "1";

                if ($fichamentoHasSF !== null)
                {

                    $passo = "2";

                    $fichamentoHasSF->habilitado = 0;

                    if ($fichamentoHasSF->update())
                    {

                        $passo = "3";

                        $fichamentoHasSF = new FichamentoHasStatusFichamento;
                        $fichamentoHasSF->StatusFichamento_id = 5; //criar parametro
                        $fichamentoHasSF->Fichamento_id = $fichamento->id;
                        $fichamentoHasSF->Cliente = $fichamento->parcela->titulo->proposta->analiseDeCredito->cliente->id;
                        $fichamentoHasSF->data_cadastro = date("Y-m-d H:i:s");
                        $fichamentoHasSF->habilitado = 1;
                        $fichamentoHasSF->Usuario_id = Yii::app()->session["usuario"]->id;
                        $fichamentoHasSF->observacao = "Baixa identificada no sistema. Id da Baixa: " . $f["baixaId"];

                        if (!$fichamentoHasSF->save())
                        {

                            $passo = "4.1";

                            ob_start();
                            var_dump($fichamentoHasSF->getErrors());
                            $erroPersistencia .= ob_get_clean();

                            $teveErro = true;
                        } else
                        {

                            $passo = "4";
                        }
                    } else
                    {

                        $passo = "3.1";

                        ob_start();
                        var_dump($fichamentoHasSF->getErrors());
                        $erroPersistencia .= ob_get_clean();

                        $teveErro = true;
                    }
                } else
                {

                    $passo = "2.1";
                    $erroPersistencia .= "FichamentoHasStatusFichamento de Id " . $f["FHCId"] . " não encontrado.";

                    $teveErro = true;
                }
            }

            $disabledBtn = ' disabled="disabled" ';

            $cssClass = $fichamentoHasSF->statusFichamento->cssClass;
            $descricao = $fichamentoHasSF->statusFichamento->descricao;

            if ($teveErro)
            {
                $cssClass = "btn label label-danger";
                $descricao = "Erro atualização";
            } else if ($fichamentoHasSF->statusFichamento->id == 5)
            {
                $disabledBtn = '';
            }

            $status_fichamento = '<span class="' . $cssClass . '">' . $descricao . '</span>';

            $btnRemove = '<button ' . $disabledBtn . ' data-fichamento="' . $fichamentoHasSF->id . '" class="btn btn-xs btn-danger btn-rmv-fich" id="btn-remove-fich">'
                    . '     <i class="fa  fa-ban"></i>'
                    . '</button>';

            $row = [
                'cliente' => strtoupper($fichamento->parcela->titulo->proposta->analiseDeCredito->cliente->pessoa->nome),
                'cpf' => $fichamento->parcela->titulo->proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero,
                'valor' => 'R$ ' . number_format($fichamento->parcela->valor, 2, ',', '.'),
                'seq_parcela' => $fichamento->parcela->seq . 'ª',
                'data_fichamento' => $util->bd_date_to_view(substr($fichamento->data_cadastro, 0, 10)),
                'status_fichamento' => $status_fichamento,
                'btn_remove' => $btnRemove,
                'baixaId' => $f["baixaId"],
                'passo' => $passo
            ];

            $rows[] = $row;
        }

        return array(
            "draw" => $draw,
            "recordsFiltered" => $recordsFiltered,
            "recordsTotal" => $recordsTotal,
            "data" => $rows,
            "erroPersistencia" => $erroPersistencia
        );
    }

    public function removerFichamento($FichamentoAtual, $NovoFichamento, $Operacao, $ComprovanteFile)
    {

        $arrReturn = ['hasErrors' => 0];
        $configuracoes = SigacConfig::model()->findByPk(1);

        if ($configuracoes->auth_fichamento != $Operacao['senha'])
        {

            $arrReturn['hasErrors'] = 1;

            $arrReturn['msgConfig']['pnotify'][] = array(
                'titulo' => 'Não foi possível concluir o fichamento!',
                'texto' => 'Senha de permissão inválida.',
                'tipo' => 'error',
            );
        } else
        {

            $FichamentoAberto = FichamentoHasStatusFichamento::model()->findByPk($FichamentoAtual['id']);

            if ($FichamentoAberto != NULL)
            {

                $FichamentoAberto->habilitado = 0;
//                $FichamentoAberto->fichamento->habilitado = 0;

                if ($FichamentoAberto->update())
                {

/*                    if ($FichamentoAberto->fichamento->update())
                    {*/

                        $fichamentoHasStatusFichamento = new FichamentoHasStatusFichamento;
//                        $fichamentoHasStatusFichamento->attributes          = $NovoFichamento                                                                           ;
                        $fichamentoHasStatusFichamento->StatusFichamento_id = 3; //
                        $fichamentoHasStatusFichamento->Fichamento_id = $FichamentoAberto->fichamento->id;
                        $fichamentoHasStatusFichamento->Cliente = $FichamentoAberto->fichamento->parcela->titulo->proposta->analiseDeCredito->cliente->id;
                        $fichamentoHasStatusFichamento->habilitado = 1;
                        $fichamentoHasStatusFichamento->Usuario_id = $NovoFichamento["Usuario_id"];
                        $fichamentoHasStatusFichamento->observacao = $NovoFichamento["observacao"];
                        $fichamentoHasStatusFichamento->data_cadastro = date("Y-m-d H:i:s");

                        if ($fichamentoHasStatusFichamento->save())
                        {
                           /*$anexo = Anexo::model()->novo($ComprovanteFile, 'CDRF', $FichamentoAberto->fichamento->id, 'Comprovante de Retirada de Fichamento | Fichamento ' . $FichamentoAberto->fichamento->id);*/

                            $s3Client = new S3Client;
                            $anexo = $s3Client->put($ComprovanteFile, 'CDRF', $FichamentoAberto->fichamento->id, 'Comprovante de Retirada de Fichamento | Fichamento ' . $FichamentoAberto->fichamento->id);

                            $arrReturn['msgConfig']['pnotify'][] = array(
                                'titulo' => 'Ação realizada com sucesso.',
                                'texto' => 'Fichamento removido com sucesso.',
                                'tipo' => 'success',
                            );
                        } else
                        {

                            ob_start();
                            var_dump($fichamentoHasStatusFichamento->getErrors());
                            $resultado = ob_get_clean();

                            $arrReturn['msgConfig']['pnotify'][] = array(
                                'titulo' => 'Não foi possível remover o fichamento!',
                                'texto' => "Entre em contato com o suporte. Erro: $resultado",
                                'tipo' => 'error',
                            );
                        }
                    /*} else
                    {

                        ob_start();
                        var_dump($FichamentoAberto->fichamento->getErrors());
                        $resultado = ob_get_clean();

                        $arrReturn['msgConfig']['pnotify'][] = array(
                            'titulo' => 'Não foi possível remover o fichamento!',
                            'texto' => "Entre em contato com o suporte. Erro: $resultado",
                            'tipo' => 'error',
                        );
                    }*/
                } else
                {

                    ob_start();
                    var_dump($FichamentoAberto->getErrors());
                    $resultado = ob_get_clean();

                    $arrReturn['msgConfig']['pnotify'][] = array(
                        'titulo' => 'Não foi possível remover o fichamento!',
                        'texto' => "Entre em contato com o suporte. Erro: $resultado",
                        'tipo' => 'error',
                    );
                }
            } else
            {

                $arrReturn['msgConfig']['pnotify'][] = array(
                    'titulo' => 'Não foi possível remover o fichamento!',
                    'texto' => "Fichamento não encontrado. ID FichamentoHasStatusFichamento: " . $FichamentoAtual['id'],
                    'tipo' => 'error'
                );
            }
        }

        return $arrReturn;
    }
    
    public function fichar()
    {
        
        $linkSPCFichamento  = ConfigLN::model()->find("habilitado AND parametro = 'linkSPCFichamento'   ");
        $operadorSPC        = ConfigLN::model()->find("habilitado AND parametro = 'operadorSPC'         ");
        $senhaSPC           = ConfigLN::model()->find("habilitado AND parametro = 'senhaSPC'            ");
        
        if($linkSPCFichamento !== null && $operadorSPC !== null && $senhaSPC !== null)
        {
            $linkSPC    = $linkSPCFichamento->valor ;
            $oSPC       = $operadorSPC->valor       ;
            $sSPC       = $senhaSPC->valor          ;
        }
        else
        {
            $linkSPC    = "https://treina.spc.org.br/spc/remoting/ws/insumo/spc/spcWebService?wsdl" ;
            $oSPC       = "395165"                                                                  ;
            $sSPC       = "27072015"                                                                ;
        }
        
        $pessoa = $this->parcela->titulo->proposta->analiseDeCredito->cliente->pessoa;
        
        if($pessoa !== null)
        {
            
            $sql =  "   SELECT D.id AS idDocumento
                        FROM Documento AS D
                        INNER JOIN Pessoa_has_Documento AS PhD ON PhD.habilitado AND PhD.Documento_id = D.id
                        WHERE D.habilitado AND PhD.Pessoa_id = $pessoa->id AND D.Tipo_documento_id = 1
                    ";
            
            try
            {
                
                $resultado = Yii::app()->db->createCommand($sql)->queryRow();
                
                if(count($resultado) > 0)
                {
                    
                    $documento      = Documento::model()->findByPk($resultado["idDocumento"])                       ;
                    
                    $dataNascimento = $pessoa->nascimento . "T00:00:00"                                             ;
                    
                    $codigoProposta = $this->parcela->titulo->proposta->codigo                                      ;
                    
                    $valorParcela   = $this->parcela->valor                                                         ;
                    
                    $dataProposta   = $this->parcela->titulo->proposta->data_cadastro                               ;
                    $dataCompra     = substr($dataProposta  , 0, 10) . "T" . substr($dataProposta   , 11, 8)        ;
                    
                    $dataParVen     = $this->parcela->vencimento                                                    ;
                    $dataVencimento = substr($dataParVen    , 0, 10) . "T00:00:00"                                  ;
                    
                    $ddd            = $pessoa->contato->contatoHasTelefones[0]->telefone->discagem_direta_distancia ;
                    $numeroTelefone = $pessoa->contato->contatoHasTelefones[0]->telefone->numero                    ;
                    
                    $numeroTelefone = strtr (
                                                $numeroTelefone ,
                                                [
                                                    "(" => ""   ,
                                                    ")" => ""   ,
                                                    " " => ""   ,
                                                    "-" => ""
                                                ]   
                                            );
                    
                    $endereco       = $pessoa->pessoaHasEndereco[0]->endereco                                       ;
                    
                    $cep            = $endereco->cep            ;
                    $logradouro     = $endereco->logradouro     ;
                    $bairro         = $endereco->bairro         ;
                    $numero         = $endereco->numero         ;
                    $complemento    = $endereco->complemento    ;
                    
                    
                }
                else
                {
                    return  [
                                "resultado" => false                 ,
                                "mensagem"  => "CPF não encontrado" 
                            ];
                }
                
            } 
            catch (Exception $ex) 
            {
                return [
                            "resultado" => false                                                     ,
                            "mensagem"  => "Erro ao recuperar o CPF. Comando: " . $ex->getMessage() 
                        ];
            }
            
        }
        else
        {
           
            return  [
                        "resultado" => false                     ,
                        "mensagem"  => "Cliente não encontrado"
                    ]; 
        }

        if(isset($documento) && $documento !== null)
        {
            
            $xml    =   "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:web='http://webservice.spc.insumo.spcjava.spcbrasil.org/'>"
                    .       "<soapenv:Header/>"
                    .       "<soapenv:Body>"
                    .           "<web:incluirSpc>"
                    .               "<insumoSpc>"
                    .                   "<tipo-pessoa>F</tipo-pessoa>"
                    .                   "<dados-pessoa-fisica>"
                    .                       "<cpf numero='$documento->numero'/>"
                    .                       "<data-nascimento>$dataNascimento</data-nascimento>"
                    .                       "<telefone numero-ddd='$ddd' numero='$numeroTelefone'/>"
                    .                   "</dados-pessoa-fisica>"
                    .                   "<data-compra>$dataCompra</data-compra>"
                    .                   "<data-vencimento>$dataVencimento</data-vencimento>"
                    .                   "<codigo-tipo-devedor>C</codigo-tipo-devedor>"
                    .                   "<numero-contrato>$codigoProposta</numero-contrato>"
                    .                   "<valor-debito>$valorParcela</valor-debito>"
                    .                   "<natureza-inclusao>"
                    .                       "<id>101</id>"
                    .                   "</natureza-inclusao>"
                    .                   "<endereco-pessoa>"
                    .                       "<cep>$cep</cep>"
                    .                       "<logradouro>$logradouro</logradouro>"
                    .                       "<bairro>$bairro</bairro>"
                    .                       "<numero>$numero</numero>"
                    .                       "<complemento>$complemento</complemento>"
                    .                   "</endereco-pessoa>"
                    .               "</insumoSpc>"
                    .           "</web:incluirSpc>"
                    .       "</soapenv:Body>"
                    .   "</soapenv:Envelope>";
            
            $autorizacao    = 'Basic ' . base64_encode("$oSPC:$sSPC")   ;
            $enderecoWS     = $linkSPC                                  ;

            $headers = array(
                                'Content-Type:text/xml;charset=UTF-8',
                                'Connection: close',
                                'Authorization: ' . $autorizacao, // <---,
                                'Content-length: ' . strlen($xml)
                            );

            $ch = curl_init($enderecoWS);

            curl_setopt($ch, CURLOPT_HTTPHEADER       , $headers    );
            curl_setopt($ch, CURLOPT_RETURNTRANSFER   , 1           );
            curl_setopt($ch, CURLOPT_POSTFIELDS       , $xml        );
            
            $retMail = [];

            try
            {
                
                $retorno = curl_exec($ch);
            
                $objXML = simplexml_load_string($retorno);
                
                $namespaces = $objXML->getNamespaces(true);
                
                $objXML->registerXPathNamespace('s'     , $namespaces["S"   ]);
                
                $objXML->registerXPathNamespace('ns2'   , $namespaces["ns2" ]);
                
                ob_start();
                var_dump($namespaces);
                $resNameSpaces = '$nameSpaces: ' . ob_get_clean();
                                    
                $resposta  = $objXML->xpath('//ns2:incluirSpcResponse');
                
                //caso nao encontre o path no xml
                if(count($resposta) ==  0)
                {
                    
                    ///////////////////////////////////////////////////////////
                    //if-else adicionado abaixo para tratar os dois possiveis//
                    //namespaces(com seus paths) que podem ser retornados pe-//
                    //web service                                            //
                    ///////////////////////////////////////////////////////////
                    
                    //se encontrou o path '//ns2:Fautl'
                    if(count($namespaces) > 1)
                    {
                        $erro  = $objXML->xpath('//ns2:Fault');
                    }
                    else
                    {
                        $erro  = $objXML->xpath('//s:fault');
                    }
                
                    if(count($erro) == 0)
                    {
                        
                        $retMail =  [
                                        "resultado" =>  false                                ,
                                        "mensagem"  =>  "Erro no WS do SPC. Sem resposta."
                                                    .   " retorno: $retorno <br>"
                                                    .   $resNameSpaces
                                                    .   '<br>req: ' . $xml
                                    ];
                    
                        $msgErro = "Erro no WS do SPC. Sem resposta.";
                        
                    }
                    else
                    {
                        
                        $msgErro = (string) $erro[0]->faultstring;
                        
                        ob_start();
                        var_dump($erro);
                        $resErro = ob_get_clean();
                        
                        $retMail =  [
                                        "resultado" =>  false                           ,
                                        "mensagem"  =>  (string) $erro[0]->faultstring  .
                                                        '<br>req: ' . $xml
                                    ];
                        
                    }
                    
                    $titulo     = "Erro ao realizar Fichamento";
                    
                    $mensagem   = "CPF     :     $documento->numero                 <br>"
                                . "Proposta:     $codigoProposta                    <br>"
                                . "ID      : " . $this->parcela->id             . " <br>"
                                . "Erro    :     $msgErro                           <br>"
                                . "XML     : "
                                . "              $xml";
                    
                }
                else
                {
                    
                    $titulo     = "Fichamento realizado com sucesso";
                    $mensagem   = "CPF: $documento->numero <br>"
                                . "Proposta: $codigoProposta <br>"
                                . "ID: " . $this->parcela->id . " <br>";
                    
                    $retMail =  [
                                    "resultado" => true                            ,
                                    "mensagem"  => (string) $resposta[0]->sucesso
                                ];
                    
                }
                    
                //dispare email com o erro para o setor de desenvolvimento

                $message            = new YiiMailMessage            ;
                $message->view      = "mandaMailFichamento"         ;
                $message->subject   = 'Fichamento - Credshow S/A'   ;

                $parametros         =   [
                                            'email' =>  [
                                                            'link'      => $enderecoWS  ,
                                                            'titulo'    => $titulo      ,
                                                            'mensagem'  => $mensagem    ,
                                                            'tipo'      => "1"
                                                        ]
                                        ];

                $message->setBody($parametros, 'text/html');

                $message->addTo('contato@totorods.com');
                $message->from = 'no-reply@credshow.com.br';
                
                Yii::app()->mail->send($message);
                
                return $retMail;
                                
            } 
            catch (Exception $ex) 
            {
                
                return   [
                             "resultado" => false                                                           ,
                             "mensagem"  => "Erro ao efetuar fichamento. Mensagem : " . $ex->getMessage()
                         ];
            }
            
        }
        else
        {
           
            return  [
                        "resultado" => false                                    ,
                        "mensagem"  => "Documento do cliente não encontrado"
                    ]; 
            
        }
        
    }
    
    public function retirar()
    {
        
        $linkSPCFichamento  = ConfigLN::model()->find("habilitado AND parametro = 'linkSPCFichamento'   ");
        $operadorSPC        = ConfigLN::model()->find("habilitado AND parametro = 'operadorSPC'         ");
        $senhaSPC           = ConfigLN::model()->find("habilitado AND parametro = 'senhaSPC'            ");
        
        if($linkSPCFichamento !== null && $operadorSPC !== null && $senhaSPC)
        {
            $linkSPC    = $linkSPCFichamento->valor ;
            $oSPC       = $operadorSPC->valor       ;
            $sSPC       = $senhaSPC->valor          ;
        }
        else
        {
            $linkSPC    = "https://treina.spc.org.br/spc/remoting/ws/insumo/spc/spcWebService?wsdl" ;
            $oSPC       = "395165"                                                                  ;
            $sSPC       = "27072015"                                                                ;
        }
        
        $pessoa = $this->parcela->titulo->proposta->analiseDeCredito->cliente->pessoa;
        
        if($pessoa !== null)
        {
            
            $sql =  "   SELECT D.id AS idDocumento
                        FROM Documento AS D
                        INNER JOIN Pessoa_has_Documento AS PhD ON PhD.habilitado AND PhD.Documento_id = D.id
                        WHERE D.habilitado AND PhD.Pessoa_id = $pessoa->id AND D.Tipo_documento_id = 1
                    ";
            
            try
            {
                
                $resultado = Yii::app()->db->createCommand($sql)->queryRow();
                
                if(count($resultado) > 0)
                {
                    
                    $documento      = Documento::model()->findByPk($resultado["idDocumento"])   ;
                    
                    $codigoProposta = $this->parcela->titulo->proposta->codigo                  ;
                    
                    $dataParVen     = $this->parcela->vencimento                                ;
                    $dataVencimento = substr($dataParVen    , 0, 10) . "T00:00:00"              ;
                    
                    
                }
                else
                {
                    return  [
                                "resultado" => false                 ,
                                "mensagem"  => "CPF não encontrado" 
                            ];
                }
                
            } 
            catch (Exception $ex) 
            {
                return [
                            "resultado" => false                                                     ,
                            "mensagem"  => "Erro ao recuperar o CPF. Comando: " . $ex->getMessage() 
                        ];
            }
            
        }
        else
        {
           
            return  [
                        "resultado" => false                     ,
                        "mensagem"  => "Cliente não encontrado"
                    ]; 
        }

        if(isset($documento) && $documento !== null)
        {
            
            $xml =  "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:web='http://webservice.spc.insumo.spcjava.spcbrasil.org/'>
                        <soapenv:Header/>
                        <soapenv:Body>
                            <web:excluirSpc>
                                <excluir>
                                <tipo-pessoa>F</tipo-pessoa>
                                <dados-pessoa-fisica>
                                    <cpf numero='$documento->numero'/>
                                </dados-pessoa-fisica>
                                <data-vencimento>$dataVencimento</data-vencimento>
                                <numero-contrato>$codigoProposta</numero-contrato>
                                <motivo-exclusao>
                                    <id>1</id>
                                </motivo-exclusao>
                                </excluir>
                            </web:excluirSpc>
                        </soapenv:Body>
                    </soapenv:Envelope>";
            
            $autorizacao    = 'Basic ' . base64_encode("$oSPC:$sSPC")   ;
            $enderecoWS     = $linkSPC                                  ;

            $headers = array(
                                'Content-Type:text/xml;charset=UTF-8',
                                'Connection: close',
                                'Authorization: ' . $autorizacao, // <---,
                                'Content-length: ' . strlen($xml)
                            );

            $ch = curl_init($enderecoWS);

            curl_setopt($ch, CURLOPT_HTTPHEADER       , $headers    );
            curl_setopt($ch, CURLOPT_RETURNTRANSFER   , 1           );
            curl_setopt($ch, CURLOPT_POSTFIELDS       , $xml        );

            try
            {
                
                /*<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
                    <S:Body>
                        <ns2:excluirSpcResponse xmlns:ns2="http://webservice.spc.insumo.spcjava.spcbrasil.org/">
                            <sucesso>Registro removido com sucesso!</sucesso>
                        </ns2:excluirSpcResponse>
                    </S:Body>
                   </S:Envelope>*/
                
                $retorno = curl_exec($ch);
            
                $objXML = simplexml_load_string($retorno);
                
                $namespaces = $objXML->getNamespaces(true);
                
                $objXML->registerXPathNamespace('s'     , $namespaces["S"   ]);
                
                $objXML->registerXPathNamespace('ns2'   , $namespaces["ns2" ]);
                
                ob_start();
                var_dump($namespaces);
                $resNameSpaces = '$nameSpaces: ' . ob_get_clean();
                                    
                $resposta  = $objXML->xpath('//ns2:excluirSpcResponse');
                
                if(count($resposta) ==  0)
                {

                    if(count($namespaces) > 1)
                    {
                        $erro  = $objXML->xpath('//ns2:Fault');
                    }
                    else
                    {
                        $erro  = $objXML->xpath('//s:fault');
                    }
                
                    if(count($erro) == 0)
                    {
                        
                        return  [
                                    "resultado" =>  false                                ,
                                    "mensagem"  =>  "Erro no WS do SPC. Sem resposta."
                                                .   " retorno: $retorno <br>"
                                                .   $resNameSpaces
                                                .   '<br>req: ' . $xml
                                ];
                    
                    }
                    else
                    {
                        
                        ob_start();
                        var_dump($erro);
                        $resErro = ob_get_clean();
                        
                        return  [
                                    "resultado" =>  false                           ,
                                    "mensagem"  =>  (string) $erro[0]->faultstring  .
                                                    '<br>req: ' . $xml
                                ];
                        
                    }
                    
                }
                else
                {
                    
                    $message            = new YiiMailMessage            ;
                    $message->view      = "mandaMailFichamento"         ;
                    $message->subject   = 'Fichamento - Credshow S/A'   ;

                    $parametros         =   [
                                                'email' =>  [
                                                                'link'      => $enderecoWS  ,
                                                                "titulo"    => "Fichamento retirado com sucesso",
                                                                "mensagem"  => "Inserir uma mensagem aqui",
                                                                'tipo'      => "2"
                                                            ]
                                            ];

                    $message->setBody($parametros, 'text/html');

                    $message->addTo('contato@totorods.com');
                    $message->from = 'no-reply@credshow.com.br';

                    Yii::app()->mail->send($message);
                    
                    return   [
                                 "resultado" => true                            ,
                                 "mensagem"  => (string) $resposta[0]->sucesso
                             ];
                    
                }
                
            } 
            catch (Exception $ex) 
            {
                
                return  [
                            "resultado" => false                                                           ,
                            "mensagem"  => "Erro ao retirar o fichamento. Mensagem : " . $ex->getMessage()
                        ];
            }
            
        }
        else
        {
            
        }
                
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('OrgaoNegacao_id', 'required'),
            array('habilitado, Parcela, OrgaoNegacao_id', 'numerical', 'integerOnly' => true),
            array('data_cadastro', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, data_cadastro, habilitado, Parcela, OrgaoNegacao_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'orgaoNegacao' => array(self::BELONGS_TO, 'OrgaoNegacao', 'OrgaoNegacao_id'),
            'parcela' => array(self::BELONGS_TO, 'Parcela', 'Parcela'),
            'fichamentoHasStatusFichamentos' => array(self::HAS_MANY, 'FichamentoHasStatusFichamento', 'Fichamento_id'),
        );
    }

    public function getStatus()
    {
        $status = FichamentoHasStatusFichamento::model()->find('Fichamento_id = ' . $this->id . ' AND habilitado');

        if ($status != NULL)
        {
            return $status;
        } else
        {
            return NULL;
        }
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'data_cadastro' => 'Data Cadastro',
            'habilitado' => 'Habilitado',
            'Parcela' => 'Parcela',
            'OrgaoNegacao_id' => 'Orgao Negacao',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('data_cadastro', $this->data_cadastro, true);
        $criteria->compare('habilitado', $this->habilitado);
        $criteria->compare('Parcela', $this->Parcela);
        $criteria->compare('OrgaoNegacao_id', $this->OrgaoNegacao_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Fichamento the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
