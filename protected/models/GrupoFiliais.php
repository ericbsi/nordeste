<?php

/**
 * This is the model class for table "GrupoFiliais".
 *
 * The followings are the available columns in table 'GrupoFiliais':
 * @property integer $id
 * @property string $nome_fantasia
 * @property string $data_cadastro
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property NucleoFiliais[] $nucleoFiliaises
 */
class GrupoFiliais extends CActiveRecord
{

   /**
    * Returns the static model of the specified AR class.
    * @param string $className active record class name.
    * @return GrupoFiliais the static model class
    */
   public static function model($className = __CLASS__)
   {
      return parent::model($className);
   }

   /**
    * @return string the associated database table name
    */
   public function tableName()
   {
      return 'GrupoFiliais';
   }

   /**
    * @return array validation rules for model attributes.
    */
   public function rules()
   {
      // NOTE: you should only define rules for those attributes that
      // will receive user inputs.
      return array(
          array('nome_fantasia, data_cadastro', 'required'),
          array('habilitado', 'numerical', 'integerOnly' => true),
          array('nome_fantasia', 'length', 'max' => 200),
          // The following rule is used by search().
          // Please remove those attributes that should not be searched.
          array('id, nome_fantasia, data_cadastro, habilitado', 'safe', 'on' => 'search'),
      );
   }

   /**
    * @return array relational rules.
    */
   public function relations()
   {
      // NOTE: you may need to adjust the relation name and the related
      // class name for the relations automatically generated below.
      return array(
          'nucleoFiliaises' => array(self::HAS_MANY, 'NucleoFiliais', 'GrupoFiliais_id'),
      );
   }

   /**
    * @return array customized attribute labels (name=>label)
    */
   public function attributeLabels()
   {
      return array(
          'id' => 'ID',
          'nome_fantasia' => 'Nome Fantasia',
          'data_cadastro' => 'Data Cadastro',
          'habilitado' => 'Habilitado',
      );
   }

   /**
    * Retrieves a list of models based on the current search/filter conditions.
    * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
    */
   public function search()
   {
      // Warning: Please modify the following code to remove attributes that
      // should not be searched.

      $criteria = new CDbCriteria;

      $criteria->compare('id', $this->id);
      $criteria->compare('nome_fantasia', $this->nome_fantasia, true);
      $criteria->compare('data_cadastro', $this->data_cadastro, true);
      $criteria->compare('habilitado', $this->habilitado);

      return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
      ));
   }

   public function listar()
   {

      $rows = array();

      $grupos = GrupoFiliais::model()->findAll();

      foreach ($grupos as $grupo)
      {

         $btn = '  <button '
                 . '         class="btn btn-danger btnRemover" '
                 . '         data-placement="top" '
                 . '         data-original-title="Remover">'
                 . '     <i class="fa fa-trash-o"></i>'
                 . ' </button>';

         $row = array(
             'nucleos' => '',
             'idGrupo' => $grupo->id,
             'nome' => strtoupper($grupo->nome_fantasia),
             'btn' => $btn,
         );

         $rows[] = $row;
      }

      return (
              array(
                  "data" => $rows
              )
      );
   }

   public function listarSelect($idNucleo)
   {

      $nucleo = NucleoFiliais::model()->findByPk($idNucleo);

      $html = '';

      $grupos = GrupoFiliais::model()->findAll();

      $html .= '<select class="form-control search-select grupoFilial">';

      $html .= '  <option value="0" ';

      if ($idNucleo == 0)
      {
         $html .= 'selected';
      }

      $html .= ' >';
      $html .= 'Selecione um Grupo...';
      $html .= '</option>';

      foreach ($grupos as $grupo)
      {

         $html .= '<option value="' . $grupo->id . '" ';

         if ($grupo->id == $nucleo->grupoFiliais->id)
         {
            $html .= 'selected';
         }

         $html .= ' >' . $grupo->nome_fantasia . '</option>';
      }

      $html .= '</select>';

      return (
              array(
                  "html" => $html
              )
              );
   }

   public function listarSelectNucleo($idNucleo)
   {

      $nucleos = NucleoFiliais::model()->findAll();

      $html = '';

      $html .= '<select class="form-control search-select grupoFilial">';

      $html .= '  <option value="0" ';

      if ($idNucleo == 0)
      {
         $html .= 'selected';
      }

      $html .= ' >';
      $html .= 'Selecione um Grupo...';
      $html .= '</option>';

      foreach ($nucleos as $nucleo)
      {

         $html .= '<option value="' . $nucleo->id . '" ';

         if ($nucleo->id == $idNucleo)
         {
            $html .= 'selected';
         }

         $html .= ' >' . $nucleo->grupoFiliais->nome_fantasia . ' '. $nucleo->nome . '</option>';
      }

      $html .= '</select>';

      return (
              array(
                  "html" => $html
              )
              );
   }

   public function listarNucleos()
   {

      $rows = array();

      $nucleos = NucleoFiliais::model()->findAll();

      foreach ($nucleos as $nucleo)
      {
         
         $banco             =  "" ;
         $agencia           =  "" ;
         $conta             =  "" ;
         $operacao          =  "" ;
         $cgcDadosBancarios =  "" ;
         
         $nucleoHasDadosBancarios = NucleoFiliaisHasDadosBancarios::model()->find('NucleoFiliais_id = ' . $nucleo->id);
         
         if ($nucleoHasDadosBancarios !== null)
         {
         
            if($nucleoHasDadosBancarios->dadosBancarios->Banco_id !== null)
            {
               $banco      =  $nucleoHasDadosBancarios->dadosBancarios->banco->codigo . ' - ' . $nucleoHasDadosBancarios->dadosBancarios->banco->nome;
            }
            
            $agencia            =  $nucleoHasDadosBancarios->dadosBancarios->agencia    ;
            $conta              =  $nucleoHasDadosBancarios->dadosBancarios->numero     ;
            $operacao           =  $nucleoHasDadosBancarios->dadosBancarios->operacao   ;
            $cgcDadosBancarios  =  $nucleoHasDadosBancarios->dadosBancarios->cgc        ;
         
         }
         
         $btn = '  <button '
                 . '         class="btn btn-danger btnRemover" '
                 . '         data-placement="top" '
                 . '         data-original-title="Remover">'
                 . '     <i class="fa fa-trash-o"></i>'
                 . ' </button>';

         $row = array(
             'filiais'              => ''                                   ,
             'idNucleo'             => $nucleo->id                          ,
             'nomeGrupo'            => $nucleo->grupoFiliais->nome_fantasia ,
             'nome'                 => strtoupper($nucleo->nome)            ,
             'banco'                => $banco                               ,
             'agencia'              => $agencia                             ,
             'conta'                => $conta                               ,
             'operacao'             => $operacao                            ,
             'cgcDadosBancarios'    => $cgcDadosBancarios                   ,
             'btn'                  => $btn
         );

         $rows[] = $row;
      }

      return (
              array(
                  "data" => $rows
              )
              );
   }

   public function salvar($nomeGrupo)
   {

      $arrayRetorno = array(
          'hasErrors' => 0,
          'msg' => 'Grupo de Filial Cadastrada com Sucesso.',
          'pntfyClass' => 'success'
      );

      $grupo = new GrupoFiliais;
      $grupo->nome_fantasia = $nomeGrupo;
      $grupo->habilitado = 1;
      $grupo->data_cadastro = date('Y-m-d H:i:s');

      if (empty($grupo->nome_fantasia))
      {
         $arrayRetorno['msg'] = 'Digite um nome válido para o Grupo de Filial.';
         $arrayRetorno['pntfyClass'] = 'error';
      } elseif (!$grupo->save())
      {

         ob_start();
         var_dump($grupo->getErrors());
         $result = ob_get_clean();

         $arrayRetorno['msg'] = 'Ocorreu um erro. Tente novamente.' . $result;
         $arrayRetorno['pntfyClass'] = 'error';
      } else
      {

         //salve os logs
         $sigacLog = new SigacLog;
         $sigacLog->saveLog('Criação de Grupo de Filiais', 'Grupo de Filiais', $grupo->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Usuário " . Yii::app()->session['usuario']->username . " criou o grupo.", "127.0.0.1", null, Yii::app()->session->getSessionId());
      }

      return $arrayRetorno;
   }

   public function salvarNucleo($idGrupo, $nomeNucleo, $banco, $agencia, $conta, $operacao)
   {

      $arrayRetorno = array(
          'hasErrors' => 0,
          'msg' => 'Núcleo de Filiais cadastrado com sucesso.',
          'pntfyClass' => 'success'
      );
      
      $transaction = Yii::app()->db->beginTransaction();

      $nucleoFilial                    = new NucleoFiliais     ;
      $nucleoFilial->GrupoFiliais_id   = $idGrupo              ;
      $nucleoFilial->nome              = $nomeNucleo           ;
      $nucleoFilial->habilitado        = 1                     ;
      $nucleoFilial->data_cadastro     = date('Y-m-d H:i:s')   ;
      
      $banco = Banco::model()->findByPk($banco);

      if ($banco == null)
      {
         $arrayRetorno['hasErrors'] = 1;
         $arrayRetorno['msg'] = 'Escolha um banco válido para o Núcleo de Filiais.';
         $arrayRetorno['pntfyClass'] = 'error';

      }
      elseif (empty($nucleoFilial->GrupoFiliais_id))
      {
         $arrayRetorno['hasErrors'] = 1;
         $arrayRetorno['msg'] = 'Escolha um Grupo válido para o Núcleo de Filiais.';
         $arrayRetorno['pntfyClass'] = 'error';
      } 
      elseif (!$nucleoFilial->save())
      {

         ob_start();
         var_dump($nucleoFilial->getErrors());
         $result = ob_get_clean();

         $arrayRetorno['hasErrors'] = 1;
         $arrayRetorno['msg'] = 'Ocorreu um erro. Tente novamente.' . $result;
         $arrayRetorno['pntfyClass'] = 'error';
         
         $transaction->rollBack();
         
      } 
      else
      {
         
         $dadosBancarios = new DadosBancarios;
         
         $dadosBancarios->habilitado      = 1                     ;
         $dadosBancarios->data_cadastro   = date('Y-m-d H:i:s')   ;
         $dadosBancarios->Banco_id        = $banco->id            ;
         $dadosBancarios->agencia         = $agencia              ;
         $dadosBancarios->numero          = $conta                ;
         
         if($operacao!== null && !empty($operacao))
         {
            $dadosBancarios->operacao                 = $operacao ;
            $dadosBancarios->Tipo_Conta_Bancaria_id   = 2         ;
         }
         
         if (!$dadosBancarios->save())
         {

            ob_start();
            var_dump($dadosBancarios->getErrors());
            $result = ob_get_clean();

            $arrayRetorno['hasErrors'] = 1;
            $arrayRetorno['msg'] = 'Ocorreu um erro. Tente novamente.' . $result;
            $arrayRetorno['pntfyClass'] = 'error';

            $transaction->rollBack();

         }
         else
         {
            
            $nucleoHasDB = new NucleoFiliaisHasDadosBancarios;
            
            $nucleoHasDB->Dados_Bancarios_id = $dadosBancarios->id;
            $nucleoHasDB->NucleoFiliais_id   = $nucleoFilial->id  ;
            
            if (!$nucleoHasDB->save())
            {

               ob_start();
               var_dump($nucleoHasDB->getErrors());
               $result = ob_get_clean();

               $arrayRetorno['hasErrors']    = 1                                             ;
               $arrayRetorno['msg']          = 'Ocorreu um erro. Tente novamente.' . $result ;
               $arrayRetorno['pntfyClass']   = 'error'                                       ;

               $transaction->rollBack();

            }
                        
         }

         //salve os logs
         $sigacLog = new SigacLog;
         $sigacLog->saveLog('Criação de Núcleo de Filiais', 'Núcleo de Filiais', $nucleoFilial->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Usuário " . Yii::app()->session['usuario']->username . " criou o núcleo.", "127.0.0.1", null, Yii::app()->session->getSessionId());
         
         $transaction->commit();
         
      }

      return $arrayRetorno;
   }

   public function listarParaProducao($idNucleos = null, $idFiliais = null){
        $retorno = []                               ;

        $query = "  SELECT DISTINCT GF.id, GF.nome_fantasia 
                    FROM        GrupoFiliais        AS GF
                    INNER JOIN  NucleoFiliais       AS NF   ON GF.id    =  NF.GrupoFiliais_id   AND  NF.habilitado
                    INNER JOIN  Filial              AS F    ON NF.id    =   F.NucleoFiliais_id  AND   F.habilitado
                    INNER JOIN  Empresa             AS E    
                    INNER JOIN  Empresa_has_Usuario AS EHU  ON E.id     =  EHU.Empresa_id
                    INNER JOIN  Usuario             AS U    ON U.id     =  EHU.Usuario_id  
                    WHERE GF.habilitado AND U.id = " . Yii::app()->session['usuario']->id;
        
        if(isset($idNucleos))
        {
            $query .= " AND NF.id IN ($idNucleos) ";
        }
        
        if(isset($idFiliais))
        {
            $query .= " AND F.id IN ($idFiliais) ";
        }
        
        $query .= " ORDER BY 2 ";

        $linhas = Yii::app()->db->createCommand($query)->queryAll();

        foreach ($linhas as $linha)
        {

            $retorno[] = GrupoFiliais::model()->findByPk($linha["id"]);

        }

        return $retorno;
   }

   //função para listar os'grupos de filiais, utilizada na tela principal do módulo de cobrança
   public function listarParaCobranca($idNucleos = null, $idFiliais = null)
    {

        $retorno = []                               ;

        $query = "  SELECT DISTINCT GF.id, GF.nome_fantasia 
                    FROM        GrupoFiliais        AS GF
                    INNER JOIN  NucleoFiliais       AS NF   ON GF.id    =  NF.GrupoFiliais_id   AND  NF.habilitado
                    INNER JOIN  Filial              AS F    ON NF.id    =   F.NucleoFiliais_id  AND   F.habilitado  
                    WHERE GF.habilitado ";
        
        if(isset($idNucleos))
        {
            $query .= " AND NF.id IN ($idNucleos) ";
        }
        
        if(isset($idFiliais))
        {
            $query .= " AND F.id IN ($idFiliais) ";
        }
        
        $query .= " ORDER BY 2 ";

        $linhas = Yii::app()->db->createCommand($query)->queryAll();

        foreach ($linhas as $linha)
        {

            $retorno[] = GrupoFiliais::model()->findByPk($linha["id"]);

        }

        return $retorno;

    }

}
