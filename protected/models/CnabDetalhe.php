<?php

class CnabDetalhe extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Cnab_detalhe';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Banco_id', 'required'),
			array('Banco_id, Filial_id, Cnab_header_id, transmitido, habilitado', 'numerical', 'integerOnly'=>true),
			array('valor_do_desconto1, valor_do_desconto2, valor_do_titulo, juros_de_mora, valor_desconto, valor_iof, valor_abatimento', 'numerical'),
			array('cod_registro, cod_inscricao, num_inscricao, zero, agencia_cedente, sub_conta, conta_corrente, brancos, controle_participante, nosso_numero, desconto_data1, desconto_data1_texto, valor_do_desconto1_texto, desconto_data2, desconto_data2_texto, valor_do_desconto2_texto, carteira, cod_ocorrencia, seu_numero, vencimento, vencimento_texto, valor_do_titulo_texto, banco_cobrador, agencia_depositaria, especie, aceite, data_emissao, data_emissao_texto, instrucao01, instrucao02, juros_de_mora_texto, desconto_data, desconto_data_texto, valor_desconto_texto, valor_iof_texto, valor_abatimento_texto, codigo_de_inscricao, numero_de_inscricao, nome_do_sacado, endereco_do_sacado, instrucao_de_nao_recebimento_do_bloqueto, bairro, cep_do_sacado, sufixo_do_cep, cidade_do_sacado, sigla_da_uf, sacador_avalista, tipo_de_bloqueto, prazo_de_protesto, moeda, numero_sequencial, codigo_agencia_beneficiario_sant,conta_movimento_beneficiario_sant,conta_cobranca_beneficiario_sant,nosso_numero_sant,data_do_segundo_desconto_sant,branco1_sant,informacao_de_multa_sant,percentual_multa_por_atraso_sant,unidade_de_valor_moeda_corrente_sant,valor_do_titulo_em_outra_unidade_sant,brancos1_sant,data_para_cobranca_de_multa_sant,endereco_do_pagador_sant,nome_do_sacador_ou_coobrigado_sant,brancos2_sant,identificador_do_complemento_sant,complemento_sant,brancos3_sant,branco2_sant', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, cod_registro, cod_inscricao, num_inscricao, zero, agencia_cedente, sub_conta, conta_corrente, brancos, controle_participante, nosso_numero, desconto_data1, desconto_data1_texto, valor_do_desconto1, valor_do_desconto1_texto, desconto_data2, desconto_data2_texto, valor_do_desconto2, valor_do_desconto2_texto, carteira, cod_ocorrencia, seu_numero, vencimento, vencimento_texto, valor_do_titulo, valor_do_titulo_texto, banco_cobrador, agencia_depositaria, especie, aceite, data_emissao, data_emissao_texto, instrucao01, instrucao02, juros_de_mora_texto, juros_de_mora, desconto_data, desconto_data_texto, valor_desconto, valor_desconto_texto, valor_iof_texto, valor_iof, valor_abatimento_texto, valor_abatimento, codigo_de_inscricao, numero_de_inscricao, nome_do_sacado, endereco_do_sacado, instrucao_de_nao_recebimento_do_bloqueto, bairro, cep_do_sacado, sufixo_do_cep, cidade_do_sacado, sigla_da_uf, sacador_avalista, tipo_de_bloqueto, prazo_de_protesto, moeda, numero_sequencial, Banco_id, Filial_id, Cnab_header_id, codigo_agencia_beneficiario_sant,conta_movimento_beneficiario_sant,conta_cobranca_beneficiario_sant,nosso_numero_sant,data_do_segundo_desconto_sant, branco1_sant, informacao_de_multa_sant, percentual_multa_por_atraso_sant, unidade_de_valor_moeda_corrente_sant, valor_do_titulo_em_outra_unidade_sant, brancos1_sant, data_para_cobranca_de_multa_sant, endereco_do_pagador_sant, nome_do_sacador_ou_coobrigado_sant, brancos2_sant, identificador_do_complemento_sant, complemento_sant, brancos3_sant, branco2_sant', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cnabHeader' => array(self::BELONGS_TO, 'CnabHeader', 'Cnab_header_id'),
			'banco' => array(self::BELONGS_TO, 'Banco', 'Banco_id'),
			'filial' => array(self::BELONGS_TO, 'Filial', 'Filial_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'cod_registro' => 'Cod Registro',
			'cod_inscricao' => 'Cod Inscricao',
			'num_inscricao' => 'Num Inscricao',
			'zero' => 'Zero',
			'agencia_cedente' => 'Agencia Cedente',
			'sub_conta' => 'Sub Conta',
			'conta_corrente' => 'Conta Corrente',
			'brancos' => 'Brancos',
			'controle_participante' => 'Controle Participante',
			'nosso_numero' => 'Nosso Numero',
			'desconto_data1' => 'Desconto Data1',
			'desconto_data1_texto' => 'Desconto Data1 Texto',
			'valor_do_desconto1' => 'Valor Do Desconto1',
			'valor_do_desconto1_texto' => 'Valor Do Desconto1 Texto',
			'desconto_data2' => 'Desconto Data2',
			'desconto_data2_texto' => 'Desconto Data2 Texto',
			'valor_do_desconto2' => 'Valor Do Desconto2',
			'valor_do_desconto2_texto' => 'Valor Do Desconto2 Texto',
			'carteira' => 'Carteira',
			'cod_ocorrencia' => 'Cod Ocorrencia',
			'seu_numero' => 'Seu Numero',
			'vencimento' => 'Vencimento',
			'vencimento_texto' => 'Vencimento Texto',
			'valor_do_titulo' => 'Valor Do Titulo',
			'valor_do_titulo_texto' => 'Valor Do Titulo Texto',
			'banco_cobrador' => 'Banco Cobrador',
			'agencia_depositaria' => 'Agencia Depositaria',
			'especie' => 'Especie',
			'aceite' => 'Aceite',
			'data_emissao' => 'Data Emissao',
			'data_emissao_texto' => 'Data Emissao Texto',
			'instrucao01' => 'Instrucao01',
			'instrucao02' => 'Instrucao02',
			'juros_de_mora_texto' => 'Juros De Mora Texto',
			'juros_de_mora' => 'Juros De Mora',
			'desconto_data' => 'Desconto Data',
			'desconto_data_texto' => 'Desconto Data Texto',
			'valor_desconto' => 'Valor Desconto',
			'valor_desconto_texto' => 'Valor Desconto Texto',
			'valor_iof_texto' => 'Valor Iof Texto',
			'valor_iof' => 'Valor Iof',
			'valor_abatimento_texto' => 'Valor Abatimento Texto',
			'valor_abatimento' => 'Valor Abatimento',
			'codigo_de_inscricao' => 'Codigo De Inscricao',
			'numero_de_inscricao' => 'Numero De Inscricao',
			'nome_do_sacado' => 'Nome Do Sacado',
			'endereco_do_sacado' => 'Endereco Do Sacado',
			'instrucao_de_nao_recebimento_do_bloqueto' => 'Instrucao De Nao Recebimento Do Bloqueto',
			'bairro' => 'Bairro',
			'cep_do_sacado' => 'Cep Do Sacado',
			'sufixo_do_cep' => 'Sufixo Do Cep',
			'cidade_do_sacado' => 'Cidade Do Sacado',
			'sigla_da_uf' => 'Sigla Da Uf',
			'sacador_avalista' => 'Sacador Avalista',
			'tipo_de_bloqueto' => 'Tipo De Bloqueto',
			'prazo_de_protesto' => 'Prazo De Protesto',
			'moeda' => 'Moeda',
			'numero_sequencial' => 'Numero Sequencial',
			'Banco_id' => 'Banco',
			'Filial_id' => 'Filial',
			'Cnab_header_id' => 'Cnab Header',
			'transmitido' => 'Transmitido',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('cod_registro',$this->cod_registro,true);
		$criteria->compare('cod_inscricao',$this->cod_inscricao,true);
		$criteria->compare('num_inscricao',$this->num_inscricao,true);
		$criteria->compare('zero',$this->zero,true);
		$criteria->compare('agencia_cedente',$this->agencia_cedente,true);
		$criteria->compare('sub_conta',$this->sub_conta,true);
		$criteria->compare('conta_corrente',$this->conta_corrente,true);
		$criteria->compare('brancos',$this->brancos,true);
		$criteria->compare('controle_participante',$this->controle_participante,true);
		$criteria->compare('nosso_numero',$this->nosso_numero,true);
		$criteria->compare('desconto_data1',$this->desconto_data1,true);
		$criteria->compare('desconto_data1_texto',$this->desconto_data1_texto,true);
		$criteria->compare('valor_do_desconto1',$this->valor_do_desconto1);
		$criteria->compare('valor_do_desconto1_texto',$this->valor_do_desconto1_texto,true);
		$criteria->compare('desconto_data2',$this->desconto_data2,true);
		$criteria->compare('desconto_data2_texto',$this->desconto_data2_texto,true);
		$criteria->compare('valor_do_desconto2',$this->valor_do_desconto2);
		$criteria->compare('valor_do_desconto2_texto',$this->valor_do_desconto2_texto,true);
		$criteria->compare('carteira',$this->carteira,true);
		$criteria->compare('cod_ocorrencia',$this->cod_ocorrencia,true);
		$criteria->compare('seu_numero',$this->seu_numero,true);
		$criteria->compare('vencimento',$this->vencimento,true);
		$criteria->compare('vencimento_texto',$this->vencimento_texto,true);
		$criteria->compare('valor_do_titulo',$this->valor_do_titulo);
		$criteria->compare('valor_do_titulo_texto',$this->valor_do_titulo_texto,true);
		$criteria->compare('banco_cobrador',$this->banco_cobrador,true);
		$criteria->compare('agencia_depositaria',$this->agencia_depositaria,true);
		$criteria->compare('especie',$this->especie,true);
		$criteria->compare('aceite',$this->aceite,true);
		$criteria->compare('data_emissao',$this->data_emissao,true);
		$criteria->compare('data_emissao_texto',$this->data_emissao_texto,true);
		$criteria->compare('instrucao01',$this->instrucao01,true);
		$criteria->compare('instrucao02',$this->instrucao02,true);
		$criteria->compare('juros_de_mora_texto',$this->juros_de_mora_texto,true);
		$criteria->compare('juros_de_mora',$this->juros_de_mora);
		$criteria->compare('desconto_data',$this->desconto_data,true);
		$criteria->compare('desconto_data_texto',$this->desconto_data_texto,true);
		$criteria->compare('valor_desconto',$this->valor_desconto);
		$criteria->compare('valor_desconto_texto',$this->valor_desconto_texto,true);
		$criteria->compare('valor_iof_texto',$this->valor_iof_texto,true);
		$criteria->compare('valor_iof',$this->valor_iof);
		$criteria->compare('valor_abatimento_texto',$this->valor_abatimento_texto,true);
		$criteria->compare('valor_abatimento',$this->valor_abatimento);
		$criteria->compare('codigo_de_inscricao',$this->codigo_de_inscricao,true);
		$criteria->compare('numero_de_inscricao',$this->numero_de_inscricao,true);
		$criteria->compare('nome_do_sacado',$this->nome_do_sacado,true);
		$criteria->compare('endereco_do_sacado',$this->endereco_do_sacado,true);
		$criteria->compare('instrucao_de_nao_recebimento_do_bloqueto',$this->instrucao_de_nao_recebimento_do_bloqueto,true);
		$criteria->compare('bairro',$this->bairro,true);
		$criteria->compare('cep_do_sacado',$this->cep_do_sacado,true);
		$criteria->compare('sufixo_do_cep',$this->sufixo_do_cep,true);
		$criteria->compare('cidade_do_sacado',$this->cidade_do_sacado,true);
		$criteria->compare('sigla_da_uf',$this->sigla_da_uf,true);
		$criteria->compare('sacador_avalista',$this->sacador_avalista,true);
		$criteria->compare('tipo_de_bloqueto',$this->tipo_de_bloqueto,true);
		$criteria->compare('prazo_de_protesto',$this->prazo_de_protesto,true);
		$criteria->compare('moeda',$this->moeda,true);
		$criteria->compare('numero_sequencial',$this->numero_sequencial,true);
		$criteria->compare('Banco_id',$this->Banco_id);
		$criteria->compare('Filial_id',$this->Filial_id);
		$criteria->compare('Cnab_header_id',$this->Cnab_header_id);
		$criteria->compare('transmitido',$this->transmitido);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getDetalhes( $Banco_id, $dataDe, $dataAte )
	{
		$util 														= new Util;

		$criteriaDetalhes 											= new CDbCriteria;
		$criteriaDetalhes->addInCondition('t.habilitado', 			array(1), ' AND ');
		$criteriaDetalhes->addInCondition('t.Banco_id', 			array($Banco_id), ' AND ');
		$criteriaDetalhes->addBetweenCondition('t.data_emissao', 	$util->view_date_to_bd( $dataDe ), $util->view_date_to_bd( $dataAte ) );

		return CnabDetalhe::model()->findAll( $criteriaDetalhes );
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CnabDetalhe the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
