<?php

class RecebimentoDeDocumentacao extends CActiveRecord {

    public function totalItensBordero() {
        if ($this->bordero == NULL) {
            return 0;
        } else {
            $itensBordero = ItemDoBordero::model()->findAll("habilitado AND Bordero = $this->Bordero");

            return count($itensBordero);
        }
    }

    public function totalItensPendentes() {
        if ($this->registroDePendencias == NULL) {
            return 0;
        } else {
            $itensPendentes = ItemPendente::model()->findAll("habilitado AND RegistroDePendencias_id = $this->RegistroDePendencias");
            return count($itensPendentes);
        }
    }

    public function totalFinanciadoAprovado() {
        if ($this->bordero == NULL) {
            return 0;
        } else {
            return $this->bordero->totalFinanciado();
        }
    }

    public function totalFinanciadoPendente() {
        if ($this->registroDePendencias == NULL) {
            return 0;
        } else {
            return $this->registroDePendencias->totalFinanciado();
        }
    }

    public function totalRepasseAprovado() {
        if ($this->bordero == NULL) {
            return 0;
        } else {
            return $this->bordero->totalRepasse();
        }
    }

    public function totalRepassePendente() {
        if ($this->registroDePendencias == NULL) {
            return 0;
        } else {
            return $this->registroDePendencias->totalRepasse();
        }
    }

    public function returnInformacoes($id) {

        $recebimento = RecebimentoDeDocumentacao::model()->findByPk($id);

        $filialD = Filial::model()->findByPk($recebimento->destinatario);

        $filial = $filialD->getConcat() . ' - ' . $filialD->cnpj;
        $count_aprovadas = 0;
        $count_pendentes = 0;
        $total_aprovado = 0;
        $total_pendente = 0;
        $repasse_aprovado = 0;
        $repasse_pendente = 0;
        $response = 0;


        if ($recebimento->bordero != NULL) {
            $count_aprovadas = count($recebimento->bordero->itemDoBorderos);
            $total_aprovado = number_format($recebimento->bordero->totalFinanciado(), 2, ',', '.');
            $repasse_aprovado = number_format($recebimento->bordero->totalRepasse(), 2, ',', '.');
        }

        if ($recebimento->registroDePendencias != NULL) {
            $count_pendentes = count($recebimento->registroDePendencias->itemPendentes);
            $total_pendente = number_format($recebimento->registroDePendencias->totalFinanciado(), 2, ',', '.');
            $repasse_pendente = number_format($recebimento->registroDePendencias->totalRepasse(), 2, ',', '.');
        }

        return [
            'filial' => $filial,
            'count_aprovadas' => $count_aprovadas,
            'total_aprovado' => $total_aprovado,
            'repasse_aprovado' => $repasse_aprovado,
            'count_pendentes' => $count_pendentes,
            'total_pendente' => $total_pendente,
            'repasse_pendente' => $repasse_pendente
        ];
    }

    public function novo($borderoId, $registroDePendenciasId, $destinatario) {

        $util = new Util;

        $recebimentoDeDocumentacao = new RecebimentoDeDocumentacao;
        $recebimentoDeDocumentacao->codigo = $util->getCodeVenda(date('ymdhis'), 17);
        $recebimentoDeDocumentacao->Bordero = $borderoId;
        $recebimentoDeDocumentacao->RegistroDePendencias = $registroDePendenciasId;
        $recebimentoDeDocumentacao->dataCriacao = date('Y-m-d H:i:s');
        $recebimentoDeDocumentacao->criadoPor = Yii::app()->session['usuario']->id;
        $recebimentoDeDocumentacao->destinatario = $destinatario;

        if ($recebimentoDeDocumentacao->save()) {
            return $recebimentoDeDocumentacao;
        } else {
            return NULL;
        }
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'RecebimentoDeDocumentacao';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('codigo, dataCriacao, destinatario, criadoPor', 'required'),
            array('Bordero, RegistroDePendencias, destinatario, criadoPor, habilitado', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, codigo, Bordero, RegistroDePendencias, dataCriacao, criadoPor, habilitado', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'bordero' => array(self::BELONGS_TO, 'Bordero', 'Bordero'),
            'registroDePendencias' => array(self::BELONGS_TO, 'RegistroDePendencias', 'RegistroDePendencias'),
            'CriadoPor' => array(self::BELONGS_TO, 'Usuario', 'criadoPor'),
            'Destinatario' => array(self::BELONGS_TO, 'Filial', 'destinatario'),
            'recebimentoHasPropostas' => array(self::HAS_MANY, 'RecebimentoDeDocumentacaoHasProposta', 'RecebimentoDeDocumentacao_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'codigo' => 'Codigo',
            'Bordero' => 'Bordero',
            'RegistroDePendencias' => 'Registro De Pendencias',
            'dataCriacao' => 'Data Criacao',
            'criadoPor' => 'Criado Por',
            'destinatario' => 'Parceiro',
            'habilitado' => 'Habilitado',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {

        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('codigo', $this->codigo, true);
        $criteria->compare('Bordero', $this->Bordero);
        $criteria->compare('RegistroDePendencias', $this->RegistroDePendencias);
        $criteria->compare('dataCriacao', $this->dataCriacao, true);
        $criteria->compare('criadoPor', $this->criadoPor);
        $criteria->compare('destinatario', $this->destinatario);
        $criteria->compare('habilitado', $this->habilitado);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return RecebimentoDeDocumentacao the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
}
