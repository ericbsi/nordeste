<?php

class AnalistaHasPropostaHasStatusProposta extends CActiveRecord
{	

	public function buscar( $proposta, $analista, $propostaStatus, $habilitado )
	{
		$criteria 				= new CDbCriteria;
		$criteria->condition 	= 'Proposta_id =:Proposta_id AND Analista_id =:Analista_id AND Status_Proposta_id IN (:Status_Proposta_id) AND habilitado =:habilitado';
		$criteria->params 		= array(
			':Proposta_id'			=>	$proposta->id,
			':Analista_id'			=>	$analista->id,
			':Status_Proposta_id'	=>	'1, 2, 3, 4, 8, 9' ,//$propostaStatus,
			':habilitado'			=>	1,
		);

		return  AnalistaHasPropostaHasStatusProposta::model()->find($criteria);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Analista_has_Proposta_has_Status_Proposta';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Analista_id, Proposta_id, Status_Proposta_id, habilitado, data_cadastro, updated_at', 'required'),
			array('Analista_id, Proposta_id, Status_Proposta_id, habilitado', 'numerical', 'integerOnly'=>true),
			array('id, Analista_id, Proposta_id, Status_Proposta_id, habilitado, data_cadastro, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'analista' 			=> array(self::BELONGS_TO, 'Usuario', 'Analista_id'),
			'proposta' 			=> array(self::BELONGS_TO, 'Proposta', 'Proposta_id'),
			'statusProposta' 	=> array(self::BELONGS_TO, 'StatusProposta', 'Status_Proposta_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Analista_id' => 'Analista',
			'Proposta_id' => 'Proposta',
			'Status_Proposta_id' => 'Status Proposta',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'updated_at' => 'Última atualização',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria =	new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Analista_id',$this->Analista_id);
		$criteria->compare('Proposta_id',$this->Proposta_id);
		$criteria->compare('Status_Proposta_id',$this->Status_Proposta_id);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AnalistaHasPropostaHasStatusProposta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
