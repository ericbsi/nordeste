<?php

/**
 * This is the model class for table "Cartao".
 *
 * The followings are the available columns in table 'Cartao':
 * @property integer $id
 * @property string $numero
 * @property integer $habilitado
 * @property string $data_cadastro
 * @property string $csv
 * @property integer $diaVencimento
 *
 * The followings are the available model relations:
 * @property CartaoHasProposta[] $cartaoHasPropostas
 */
class Cartao extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Cartao';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('numero, habilitado, data_cadastro, csv, diaVencimento', 'required'),
			array('habilitado, diaVencimento', 'numerical', 'integerOnly'=>true),
			array('numero', 'length', 'max'=>16),
			array('csv', 'length', 'max'=>3),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, numero, habilitado, data_cadastro, csv, diaVencimento', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cartaoHasPropostas' => array(self::HAS_MANY, 'CartaoHasProposta', 'Cartao_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'            => 'ID'             ,
			'numero'        => 'Numero'         ,
			'habilitado'    => 'Habilitado'     ,
			'data_cadastro' => 'Data Cadastro'  ,
			'csv'           => 'Csv'            ,
			'diaVencimento' => 'Dia Vencimento' ,
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id'             , $this->id                     );
		$criteria->compare('numero'         , $this->numero         , true  );
		$criteria->compare('habilitado'     , $this->habilitado             );
		$criteria->compare('data_cadastro'  , $this->data_cadastro  , true  );
		$criteria->compare('csv'            , $this->csv            , true  );
		$criteria->compare('diaVencimento'  , $this->diaVencimento          );

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	//pegando o titulo associado a venda realizada com determinado cartão..
	public function getTitulo($cod_venda){
		$natureza_tit = NaturezaTitulo::model()->find("habilitado AND codigo = '10101'");
		$query = "
				SELECT t.id as id_titulo from Venda_has_Forma_de_Pagamento_has_Condicao_de_Pagamento as vhfp
				INNER JOIN Titulo as t on t.Venda_has_FP_has_CP_FP_has_CP_id = vhfp.id and t.NaturezaTitulo_id = " .$natureza_tit->id .
				" INNER JOIN Venda as v on v.id = vhfp.Venda_id and v.codigo = " . $cod_venda;

		$consulta = Yii::app()->db->createCommand($query)->queryRow();

		if($consulta != null){
			return Titulo::model()->find('id = ' . $consulta['id_titulo']);
		}else{
			return null;
		}
	}

	//pegando as parcelas de um determinado titulo
	public function getParcelas($cod_venda, $valor_parc){

		$titulo = $this->getTitulo($cod_venda);

    	if ($titulo != NULL)
    	{

    		$query = "select * from Parcela where Titulo_id = ". $titulo->id ." and valor_atual = " . $valor_parc;
       		$parcelas = Yii::app()->db->createCommand($query)->queryAll();

       		if (count($parcelas) > 0 && $parcelas != NULL)
       		{
       			foreach ($parcelas as $parcela) {
       				$row = array(
       						'id' => $parcela['id'],
       						'seq' => $parcela['seq'],
       						'vencimento' => $parcela['vencimento'],
       						'valor' => $parcela['valor'], 
       					);

       				$rows[] = $row;
       			}
        		return (
        				array(
        					"parcela" => $rows
        				)
        			);
	       	} else
    	   	{
        		return NULL;
	       	}
    		} else
    		{
       			return NULL;
    		}
  	}

	public function listar(){
		$query = 	"
					SELECT v.codigo, p.nome, dc.numero, crt.numero as num_cartao, vhfp.valor, vhfp.qtdParcelas 
					FROM Venda_has_Forma_de_Pagamento_has_Condicao_de_Pagamento as vhfp  
					INNER JOIN Venda as v on vhfp.Venda_id = v.id
					INNER JOIN Cliente as clt on  clt.id = v.Cliente_id
					INNER JOIN Cartao as crt on crt.id = vhfp.Cartao_id
					INNER JOIN Pessoa as p on p.id = clt.Pessoa_id
					INNER JOIN Pessoa_has_Documento as phd on phd.Pessoa_id = clt.Pessoa_id
					INNER JOIN Documento as dc on dc.Tipo_documento_id = 1 AND phd.Documento_id = dc.id";

		$consulta = Yii::app()->db->createCommand($query)->queryAll();

		foreach ($consulta as $dado){

        	$btn = '  <button '
            	    . '         class="btn btn-primary" '
                	. '         data-placement="top" '
                 	. '         data-original-title="Expandir">'
                	. '     <i class="clip-search"></i>'
                	. '</button>';

        	$row = array(
        		'empty' => '',
            	'codigo' => $dado['codigo'],
            	'cliente' => strtoupper($dado['nome']),
            	'cpf' => $dado['numero'],
            	'valor' => $dado['valor'],
            	'qtd_parcelas' => $dado['qtdParcelas'],
            	'btn' => $btn,
        	);

        	$rows[] = $row;
      	}

      	return (
            array(
                "data" => $rows
            )
      	);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Cartao the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
