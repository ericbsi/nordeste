<?php

class CategoriaProduto extends CActiveRecord
{	

	public function listCategorias(){
		
		$categorias = [];

		foreach( CategoriaProduto::model()->findAll() as $categoria ){

			$c = array(
				'id'	=>	$categoria->id,
				'text'	=>	$categoria->getCategoriaDescricao(),
			);

			$categorias[] = $c;
		}

		return $categorias;
	}

	public function add($CategoriaProduto){

		$arrReturn = array(
			'hasErrors' => '0',
			'classNtfy' => '',
			'msg' 		=> '',
		);

		if( !empty($CategoriaProduto['nome']) )
		{
			$novaCategoria 					= new CategoriaProduto;
			
			$novaCategoria->nome 			= $CategoriaProduto['nome'];

			if( $CategoriaProduto['idPai'] != 1 ){
				$catPai 					= CategoriaProduto::model()->findByPk($CategoriaProduto['idPai']);
				$novaCategoria->idPai 		= $CategoriaProduto['idPai'];
				$novaCategoria->codigo 		= $catPai->getNextChildCode();
			}
			else{
				$novaCategoria->idPai 		= null;
				$novaCategoria->codigo 		= CategoriaProduto::model()->getCodeCategoriaSemPai();
			}

			if( $novaCategoria->save() )
			{
				$arrReturn['classNtfy'] = 'success';
				$arrReturn['msg'] 		= 'Categoria cadastrada com sucesso';
			}
			else
			{
				$arrReturn['classNtfy'] = 'error';
				$arrReturn['msg'] 		= 'Erro ao cadastrar.';
			}
		}

		return $arrReturn;
	}

	public function getCodeCategoriaSemPai(){

		$util 				= new Util;
		$zeros 				= '';
		$categoriasSemPai 	= CategoriaProduto::model()->findAll('idPai IS NULL');

		$nZerosAdd 			= 3 - $util->countDigits( count($categoriasSemPai) );

		for ( $i = 0; $i < $nZerosAdd; $i++ )
		{
			$zeros .= '0';
		}

		return $zeros . (count($categoriasSemPai)+1);
	}

	public function searchCategoriasSelec2($q, $showPai){
		
		$retorno	= array();
		$data;
		$draw 		= 1;
		$start 		= 0; 
		$order		= '';

		$columns	= array(
			array(
				'search' => array(
					'value' => '',
				)
			),
			array(
				'search' => array(
					'value' => $q,
				)
			),
		);

		/*$resultado = $this->listarCategorias($draw, $start, $order, $columns);*/

		$data = $this->listarCategorias($draw, $start, $order, $columns);

		for ( $i = 0; $i < count( $data['data'] ); $i++ ){
			
			$row = array(
				'id' 	=> $data['data'][$i]['id'],
				'text' 	=> $data['data'][$i]['nome']
			);

			$retorno[] 	= $row;
		}

		/*Mostrar a'opção 'Sem pai'*/
		if( $showPai )
		{
			$catSemPai 		= CategoriaProduto::model()->findByPk(1);

			$retorno[] 		= array(
				'id' 	=> $catSemPai->id,
				'text' 	=> $catSemPai->nome
			);
		}

		return $retorno;
	}

	public function listarCategorias($draw, $start, $order, $columns){

        $rows                           = array();
        $recordsTotal               	= 0;
        $recordsFiltered               	= 0;
        $btn                            = "";
        $crt                            = new CDbCriteria;
        $countCategorias 				= count(CategoriaProduto::model()->findAll('id <> 1'));

        if( !empty( $columns[0]['search']['value'] ) && !ctype_space($columns[0]['search']['value']) )
        {
            $crt->addSearchCondition( 't.codigo',$columns[0]['search']['value'] );
        }

        $crt->offset                    = $start;
        $crt->limit                     = 10;
        $crt->order                     = "t.nome ASC";

        $categorias 					= CategoriaProduto::model()->findAll($crt);

        if( count( $categorias ) > 0 )
        {

            foreach( $categorias as $cat )
            {
                            
                if( $cat != NULL && $cat->id != 1 )
                {
                    
                	$paiDesc 		= ( $cat->idPai != NULL ? CategoriaProduto::model()->findByPk($cat->idPai)->getCategoriaDescricao():'------------' );

                    //$btn = '<a data-entity-id="'.$cat->id.'"'.'class="btn btn-primary btn-update btn-load-form" style="padding:2px 5px!important; font-size:11px!important;" href="#"><i class="clip-pencil"></i></a>';
                    $btn = '';

                    $row            = array(
                        'id'		=> $cat->id,
                        'codigo'	=> $cat->codigo,
                        'nome'      => $cat->getCategoriaDescricao(),
                        'pai'       => $paiDesc,
                        'btn'       => $btn,
                    );

                    if( !empty( $columns[1]['search']['value'] ) && !ctype_space($columns[1]['search']['value']) )
        			{
        				if (stripos($cat->getCategoriaDescricao(),$columns[1]['search']['value']) !== false) {
    						$rows[] = $row;
						}
        			}
        			else
        			{
                    	$rows[] = $row;
        			}

                    $btn    = "";
                }
            }
        }

        if( count($rows) > 0 ){
        	
	        foreach($rows as $key => $value)
	        {
	        	$nomes[$key] = $value['nome'];
	        }

	        array_multisort($nomes, SORT_ASC, $rows);
        }

        return (array(
            "draw"              => $draw,
            "recordsTotal"      => count($rows),
            "recordsFiltered"   => $countCategorias,
            "data"              => $rows
        ));
    }

	public function getNextChildCode(){

		$util  		= new Util;
		$zeros 		= '';
		$filhos 	= CategoriaProduto::model()->findAll('idPai = ' . $this->id);
		$nZerosAdd 	= 3 - $util->countDigits( count($filhos) );

		for ( $i = 0; $i < $nZerosAdd; $i++ )
		{
			$zeros .= '0';
		}
		
		return $this->codigo.$zeros. (count($filhos)+1);
	}

	public function tableName()
	{
		return 'CategoriaProduto';
	}
	
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('codigo, nome', 'required'),
			array('idPai', 'numerical', 'integerOnly'=>true),
			array('nome', 'length', 'max'=>200),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, codigo, nome, idPai', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations()
	{
		return array(
			'idPai0' => array(self::BELONGS_TO, 'CategoriaProduto', 'idPai'),
			'categoriaProdutos' => array(self::HAS_MANY, 'CategoriaProduto', 'idPai'),
		);
	}

	
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'codigo' => 'Codigo',
			'nome' => 'Nome',
			'idPai' => 'Id Pai',
		);
	}

	
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('codigo',$this->codigo,true);
		$criteria->compare('nome',$this->nome,true);
		$criteria->compare('idPai',$this->idPai);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getCategoriaDescricao(){
		
		$nome 		= $this->nome;
		$pai 		= CategoriaProduto::model()->findByPk( $this->id );


		while ( $pai->idPai != null ){

			$pai 	= CategoriaProduto::model()->findByPk( $pai->idPai );
			$nome 	= $pai->nome . ' ' . $nome;
		}

		return $nome;
	}

	public function getDeepestCategories()
	{	

		$categorias = array();
		$query  	= "SELECT `c1`.`id`, `c1`.`nome`, `c1`.`idPai` ";
		$query 		.= "FROM `CategoriaProduto` AS `c1`";
		$query 		.= "LEFT OUTER JOIN `CategoriaProduto` AS `c2`";
		$query 		.= "ON `c1`.`id` = `c2`.`idPai`";
		$query 		.= "WHERE `c2`.`id` IS NULL and `c1`.`id` <> 1";

		$connection = Yii::app()->db;
		$list		= $connection->createCommand($query)->queryAll();

		foreach ( $list as $lis ) {

			$catId 		= $lis['id'];
			$nome 		= $lis['nome'];
			$pai 		= CategoriaProduto::model()->findByPk( $lis['id'] );

			do {
				
				$pai 	= CategoriaProduto::model()->findByPk( $pai->idPai );

				$nome 	= $pai->nome . ' ' . $nome;

			} while ($pai->idPai != NULL);

			$categorias[] = array('id' => $catId, 'descricao' => $nome);

		}

		return $categorias;

	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
