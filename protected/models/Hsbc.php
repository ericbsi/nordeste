<?php 
	
class Hsbc{

	private $codigobanco 	= "399";
	private $nummoeda 		= "9";
	private $carteira 		= "00";
	private $app 			= "1";
	private $fator_vencimento;
	private $barra;
	private $valor;
	private $numero_documento;
	private $numero_agencia;
	private $codigo_cedente;
	private $digito_verificador;
	private $linha;
	private $codigo_de_barras;
	
	function __construct($vencimento, $valorBoleto, $num_doc, $agencia, $cod_cedente){

		$util 						= new Util;
		$m11 						= new Modulo_11;

		$vencimento 				= $util->bd_date_to_view($vencimento);
		$this->fator_vencimento 	= $this->fator_vencimento($vencimento);
		$this->valor 				= $this->formata_numero($valorBoleto,10,0,"valor");
		$this->numero_documento 	= $num_doc;
		$this->numero_documento 	.= $m11->m11($this->numero_documento,7);
		$this->numero_agencia 		= $agencia;
		$this->codigo_cedente 		= $this->formata_numero($cod_cedente,7,0);
		$this->barra 				= "$this->codigobanco$this->nummoeda$this->fator_vencimento$this->valor$this->numero_documento$this->numero_agencia$this->codigo_cedente$this->carteira$this->app";
		$this->digito_verificador 	= $this->digitoVerificador_barra($this->barra, 9, 0);
		$this->linha 				= substr($this->barra,0,4) . $this->digito_verificador . substr($this->barra,4);
		$this->codigo_de_barras 	= $this->linha;
   	}

   	public function getCodigoDeBarras(){

   		return $this->linha;
   	}

   	public function getNossoNumero(){

   		return $this->numero_documento;
   	}

   	public function getCodigoAgencia(){

   		return $this->formata_numero('0072898',7,0);
   	}

	public function geraNossoNumero( $ndoc,$cedente,$venc,$tipoid ) {

			$ndoc 	= $ndoc.modulo_11_invertido($ndoc).$tipoid;
			$venc 	= substr($venc,0,2).substr($venc,3,2).substr($venc,8,2);
			$res 	= $ndoc + $cedente + $venc;

			return $ndoc.'0';// . modulo_11_invertido($res);
	}

 	public	function dataJuliano($data) {
			$dia = (int)substr($data,0,2);
			$mes = (int)substr($data,3,2);
			$ano = (int)substr($data,6,4);
			$dataf = strtotime("$ano/$mes/$dia");
			$datai = strtotime(($ano-1).'/12/31');
			$dias  = (int)(($dataf - $datai)/(60*60*24));
		    return str_pad($dias,3,'0',STR_PAD_LEFT).substr($data,9,4);
	}

	public function digitoVerificador_nossonumero($numero) {

			$resto2 = modulo_11($numero, 9, 1);
		    $digito = 11 - $resto2;
		    
		    if ($digito == 10 || $digito == 11) {
		        $dv = 0;
		    } 

		    else {
		        $dv = $digito;
		    }

	 		return $dv;
	}

	public function digitoVerificador_barra($numero){

		 $m11 = new Modulo_11;

		 $resto2 = $m11->m11($numero, 9, 1);

	     if ($resto2 == 0 || $resto2 == 1 || $resto2 == 10) {
	        $dv = 1;
	     }

	     else {
		 	$dv = 11 - $resto2;
	     }

		 return $dv;
	}

	public function formata_numero($numero,$loop,$insert,$tipo = "geral") {
			if ($tipo == "geral") {
				$numero = str_replace(",","",$numero);
				while(strlen($numero)<$loop){
					$numero = $insert . $numero;
				}
			}
			if ($tipo == "valor") {
				/*
				retira as virgulas
				formata o numero
				preenche com zeros
				*/
				$numero = str_replace(",","",$numero);
				while(strlen($numero)<$loop){
					$numero = $insert . $numero;
				}
			}
			if ($tipo == "convenio") {
				while(strlen($numero)<$loop){
					$numero = $numero . $insert;
				}
			}
			return $numero;
	}

	public function esquerda( $entra,$comp ){
		
		return substr( $entra,0,$comp );
	}

	public function direita($entra,$comp){

		return substr($entra,strlen($entra)-$comp,$comp);

	}

	public function fator_vencimento($data) {
		
		$data = explode("/",$data);
		$ano = $data[2];
		$mes = $data[1];
		$dia = $data[0];

    	return( abs(($this->_dateToDays("1997","10","07")) - ($this->_dateToDays($ano, $mes, $dia))) );
	}

	public function _dateToDays($year,$month,$day) {

		$century = substr($year, 0, 2);
    	$year = substr($year, 2, 2);

    	if ($month > 2) {
        	$month -= 3;
    	}else {
        	$month += 9;
        	if ($year) {
            	$year--;
        	} else {
	            $year = 99;
	            $century --;
        	}
    	}

    	return ( floor((  146097 * $century)    /  4 ) +
    			floor(( 1461 * $year)        /  4 ) +
    			floor(( 153 * $month +  2) /  5 ) +
    				$day +  1721119);
	}

	public function modulo_10($num){

		$numtotal10 = 0;
        $fator = 2;

        for ($i = strlen($num); $i > 0; $i--) {
        	// pega cada numero isoladamente
            $numeros[$i] = substr($num,$i-1,1);
            // Efetua multiplicacao do numero pelo (falor 10)
            // 2002-07-07 01:33:34 Macete para adequar ao Mod10 do Itaú
            $temp = $numeros[$i] * $fator; 
            $temp0=0;
            
            foreach (preg_split('//',$temp,-1,PREG_SPLIT_NO_EMPTY) as $k=>$v){ 
            	$temp0+=$v; 
           	}

           	$parcial10[$i] = $temp0; //$numeros[$i] * $fator;
            // monta sequencia para soma dos digitos no (modulo 10)
            $numtotal10 += $parcial10[$i];

            if ($fator == 2) {
                $fator = 1;
            } else {
                $fator = 2; // intercala fator de multiplicacao (modulo 10)
            }
        }

        $resto = $numtotal10 % 10;
        $digito = 10 - $resto;
        
        if ($resto == 0) {
            $digito = 0;
        }

        return $digito;
	}

	public function modulo_11_invertido($num){
    
	    $ftini = 2;
		$ftfim = 9;
		$fator = $ftfim;
	    $soma = 0;
		
	    for ($i = strlen($num); $i > 0; $i--) {
			$soma += substr($num,$i-1,1) * $fator;
			if(--$fator < $ftini) $fator = $ftfim;
	    }
		
	    $digito = $soma % 11;
		if($digito > 9) $digito = 0;
			return $digito;
	}

	public function monta_linha_digitavel() {

		global $nossonumero, $numero_agencia, $codigocedente, $carteira;
	
		$nossonumero = $this->numero_documento;
		$numero_agencia = $this->numero_agencia;
		$codigocedente = $this->codigo_cedente;
		$carteira  = $this->carteira;

		// Posição 	Conteúdo
		// 1 a 3    Número do banco
		// 4        Código da Moeda - 9 para Real
		// 5        Digito verificador do Código de Barras
		// 6 a 9    Fator de Vencimento
		// 10 a 19  Valor (8 inteiros e 2 decimais)
		//          Campo Livre definido por cada banco (25 caracteres)
		// 20 a 26  Código do Cedente
		// 27 a 39  Código do Documento
		// 40 a 43  Data de Vencimento em Juliano (mmmy)
		// 44       Código do aplicativo CNR = 2
		
		// 1. Campo - composto pelo código do banco, código da moéda, as cinco primeiras posições
		// do campo livre e DV (modulo10) deste campo

		$campo1 = substr($this->linha,0,4) . substr( $nossonumero, 0 , 1);
		$campo1 = $campo1 . substr( $nossonumero, 1, 4 );
		// $campo1 = $campo1 . '5';
		$campo1 = $campo1 . $this->modulo_10($campo1);
		$campo1 = substr($campo1,0,5) . '.' . substr($campo1,5,5);
		
		// 2. Campo - composto pelas posiçoes 6 a 15 do campo livre
		// e livre e DV (modulo10) deste campo
		$campo2 = substr($nossonumero,5,6) . substr($numero_agencia,0,4);
		$campo2 = $campo2 . $this->modulo_10($campo2);
		$campo2 = substr($campo2,0,5) . '.' . substr($campo2,5,6);

		
		// 3. Campo composto pelas posicoes 16 a 25 do campo livre
		// e livre e DV (modulo10) deste campo
		$campo3 = $codigocedente . $carteira . 1;
		$campo3 = $campo3 . $this->modulo_10($campo3);
		$campo3 = substr($campo3,0,5) . '.' . substr($campo3,5,6);

		// 4. Campo - digito verificador do codigo de barras
		$campo4 = substr($this->linha, 4, 1);
		
		// 5. Campo composto pelo fator vencimento e valor nominal do documento, sem
		// indicacao de zeros a esquerda e sem edicao (sem ponto e virgula). Quando se
		// tratar de valor zerado, a representacao deve ser 000 (tres zeros).
		$campo5 = substr($this->linha, 5, 4) . substr($this->linha, 9, 10);

		return "$campo1 $campo2 $campo3 $campo4 $campo5";
	}

	public function geraCodigoBanco($numero) {

		$m11 = new Modulo_11;

	    $parte1 = substr($numero, 0, 3);

	    $parte2 = $m11->m11($parte1);

	    return $parte1 . "-" . $parte2;
	}

	public function fbarcode(){
	
		$fino 		= 1;
		$largo 		= 3;
		$altura 	= 50;

		$barcodes[0] = "00110" ;
		$barcodes[1] = "10001" ;
		$barcodes[2] = "01001" ;
		$barcodes[3] = "11000" ;
		$barcodes[4] = "00101" ;
		$barcodes[5] = "10100" ;
		$barcodes[6] = "01100" ;
		$barcodes[7] = "00011" ;
		$barcodes[8] = "10010" ;
		$barcodes[9] = "01010" ;

		for ( $f1=9; $f1>=0; $f1-- ) {
			
			for ( $f2=9;$f2>=0;$f2-- ) {

				$f = ( $f1*10 ) + $f2;

				$texto = "" ;

				for ( $i=1; $i<6; $i++ ) {
					$texto .= substr( $barcodes[$f1],($i-1),1 ) . substr($barcodes[$f2],($i-1),1);
				}
				$barcodes[$f] = $texto;
			}
		}
		
		echo '<img src='.Yii::app()->getBaseUrl(true).'/images/p.png width="'.$fino.'" height="'.$altura.'" border=0>'.'<img src='.Yii::app()->getBaseUrl(true).'/images/b.png width="'.$fino.'" height="'.$altura.'" border=0>'.'<img src='.Yii::app()->getBaseUrl(true).'/images/p.png width="'.$fino.'" height="'.$altura.'" border=0>'.'<img src='.Yii::app()->getBaseUrl(true).'/images/b.png width="'.$fino.'" height="'.$altura.'" border=0><img ';

		$texto = $this->linha;

		if( (strlen( $texto ) %2 ) <> 0 ) { 

			$texto = "0".$texto;
		}

		while( strlen( $texto ) > 0 )
		{

			$i = round( $this->esquerda($texto,2) );
			$texto = $this->direita($texto,strlen($texto)-2);
			$f = $barcodes[$i];

			for( $i=1; $i<11; $i+=2 ) {

				if(substr($f,($i-1),1)=="0") { $f1 = $fino; } else { $f1 = $largo; }
				echo 'src='.Yii::app()->getBaseUrl(true).'/images/p.png width="'.$f1.'" height="'.$altura.'" border=0><img ';
				if(substr($f,$i,1)=="0") { $f2 = $fino; } else { $f2 = $largo; }
				echo 'src='.Yii::app()->getBaseUrl(true).'/images/b.png width="'.$f2.'" height="'.$altura.'" border=0><img ';
			}
		}

		$aux = 1;

		echo 'src='.Yii::app()->getBaseUrl(true).'/images/p.png width="'.$largo.'" height="'.$altura.'" border=0><img '.
			 'src='.Yii::app()->getBaseUrl(true).'/images/b.png width="'.$fino.'" height="'.$altura.'" border=0><img '.
			 'src='.Yii::app()->getBaseUrl(true).'/images/p.png width="'.$aux.'" height="'.$altura.'" border=0>';
	}
}

?>