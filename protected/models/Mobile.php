<?php

class Mobile {

    public function login($usuario, $senha) {

        $model = new LoginForm;

        $model->username = $usuario;
        $model->password = $senha;

        return ($model->validate() && $model->login());
    }

    public function getTabelas() {

        $retorno = [];

        $filialHasUsuario = FilialHasUsuario::model()->find('habilitado AND Usuario_id = ' . Yii::app()->session['usuario']->id);

        $filialHasTabela = FilialHasTabelaCotacao::model()->findAll('habilitado AND Filial_id = ' . $filialHasUsuario->Filial_id);

        foreach ($filialHasTabela as $ft) {
            if($ft->tabelaCotacao->habilitado)
            {
                $retorno[] = array('idTabela' => $ft->tabelaCotacao->id, 'descricao' => $ft->tabelaCotacao->descricao);
            }
        }

        return $retorno;
    }

    public function getFatores($idTabela) {

        $rows = [];

        $tabela = TabelaCotacao::model()->findByPk($idTabela);

        if ($tabela != NULL) {

            $fatores = Fator::model()->findAll('habilitado AND Tabela_Cotacao_id = ' . $idTabela);

            foreach ($fatores as $fator) {
                $row = array(
                    'carencia' => $fator->carencia,
                    'fator' => $fator->fator,
//                'retencao' => number_format($fator->porcentagem_retencao, 2, ',', '.'),
                    'parcela' => $fator->parcela . ' °'
                );

                $rows[] = $row;
            }
        }

        return $rows;
    }

}
