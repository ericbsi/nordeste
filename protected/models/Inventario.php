<?php

class Inventario extends CActiveRecord
{


	public function add( $itens_estoque_ids, $itens_estoque_qtds ){

		$inventario 												= new Inventario;
		$inventario->data 											= date('Y-m-d H:i:s');
		$retorno 													= array();
		
		if( $inventario->save() )
		{
			for ( $i = 0; $i < count( $itens_estoque_qtds ); $i++ ) {
				
				$item_do_inventario 								= new ItemDoInventario;
				$item_do_inventario->Inventario_id 					= $inventario->id;
				$item_do_inventario->Item_do_Estoque_id 			= $itens_estoque_ids[$i];
				$item_do_inventario->quantidade 					= $itens_estoque_qtds[$i];
				$item_do_inventario->Status_Item_do_Inventario_id	= 2;
				$item_do_inventario->save();
			}

			return $inventario;
		}

		return null;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Inventario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('data', 'required'),
			array('id, habilitado, Usuario_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, data, Usuario_id, data_validade, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'estoque' 			=> array(self::BELONGS_TO, 'Estoque', 			'Estoque_id'),
			'statusInventario' 	=> array(self::BELONGS_TO, 'StatusInventario', 	'Status_Inventario_id'),
			'usuario' 			=> array(self::BELONGS_TO, 'Usuario', 			'Usuario_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'data' => 'Data',
			'data_validade' => 'Data Validade',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria =	new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('data',$this->data,true);
		$criteria->compare('data_validade',$this->data_validade,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('Usuario_id',$this->Usuario_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Inventario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
