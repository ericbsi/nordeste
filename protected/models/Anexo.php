<?php

class Anexo extends CActiveRecord
{

    public function novo($arquivo, $entidade_relacionamento, $id_entidade_relacionamento, $Descricao_Arquivo) 
    {
        
		/*if(strtoupper($entidade_relacionamento) == "EMPRESTIMO")
        {
        
            $anexo = Anexo::model()->find("habilitado AND entidade_relacionamento = 'Emprestimo' And id_entidade_relacionamento = $this->id");

            if($anexo !== null)
            {
                $anexo->habilitado = 0;

                $anexo->update();
            }
            
        }*/
        
        $UploadDirectory = '/var/www/nordeste/uploads/';

        if ($arquivo["size"] > 5242880) 
        {
            
            return "Arquivo muito grande!";
            
        } 
        else 
        {
            
            $File_Name = strtolower($arquivo['name']);
            $File_Ext = substr($File_Name, strrpos($File_Name, '.')); //pega a extensão do arquivo
            $Random_Number = rand(0, 9999999999); //número randomico que será concatenado ao nome
            $NewFileName = $Random_Number . $File_Ext; //novo nome do arquivo

            if (move_uploaded_file($arquivo['tmp_name'], $UploadDirectory . $NewFileName)) 
            {

                $path = $arquivo['name'];
                $ext = pathinfo($path, PATHINFO_EXTENSION);

                $anexo = new Anexo;
                $anexo->descricao = $Descricao_Arquivo;
                $anexo->relative_path = '/uploads/' . $NewFileName;
                $anexo->absolute_path = '/var/www/nordeste/uploads/' . $NewFileName;
                $anexo->entidade_relacionamento = $entidade_relacionamento;
                $anexo->data_cadastro = date('Y-m-d H:i:s');
                $anexo->id_entidade_relacionamento = $id_entidade_relacionamento;
                $anexo->habilitado = 1;
                $anexo->extensao = $ext;

                if ($anexo->save()) 
                {
                    return $anexo;
                } 
                else 
                {
                    return null;
                }
            }
        }
    }

    public function anexar($arquivo, $entidade_relacionamento, $id_entidade_relacionamento, $Descricao_Arquivo) 
    {
        
        $UploadDirectory = '/var/www/nordeste/uploads/';

        if ($arquivo["size"] > 5242880) 
        {
            
            return  [
                        "hasErrors"     => 1                        ,
                        "msg"           => "Arquivo muito grande!"  ,
                        "pntfyClass"    => "error"                  ,
                    ];
            
        } 
        else 
        {
            
            $File_Name      = strtolower($arquivo['name']                               );
            $File_Ext       = substr    ($File_Name         , strrpos($File_Name, '.')  ); //pega a extensão do arquivo
            $Random_Number  = rand      (0                  , 9999999999                ); //número randomico que será concatenado ao nome
            $NewFileName    = $Random_Number . $File_Ext                                 ; //novo nome do arquivo

            if (move_uploaded_file($arquivo['tmp_name'], $UploadDirectory . $NewFileName)) 
            {

                $path   = $arquivo['name']                      ;
                $ext    = pathinfo($path, PATHINFO_EXTENSION)   ;

                $anexo                              = new Anexo                                     ;
                $anexo->descricao                   = $Descricao_Arquivo                            ;
                $anexo->relative_path               = '/uploads/'                   . $NewFileName  ;
                $anexo->absolute_path               = '/var/www/nordeste/uploads/'  . $NewFileName  ;
                $anexo->entidade_relacionamento     = $entidade_relacionamento                      ;
                $anexo->data_cadastro               = date('Y-m-d H:i:s')                           ;
                $anexo->id_entidade_relacionamento  = $id_entidade_relacionamento                   ;
                $anexo->habilitado                  = 1                                             ;
                $anexo->extensao                    = $ext                                          ;

                if ($anexo->save()) 
                {
                    return  [
                                "hasErrors"     => 0                            ,
                                "msg"           => "Anexo salvo com suceeso!"   ,
                                "pntfyClass"    => "success"                    ,
                            ];
                } 
                else 
                {
                    
                    ob_start();
                    var_dump($anexo->getErrors());
                    $resultado = ob_get_clean();
                    
                    return  [
                                "hasErrors"     => 1                                            ,
                                "msg"           => "Erro ao salvar anexo. Anexo: $resultado"    ,
                                "pntfyClass"    => "error"                                      ,
                            ];
                }
            }
        }
    }

    public function add( $FileInput, $Cliente_id, $Descricao_Arquivo ){

		$UploadDirectory    = '/var/www/nordeste/uploads/';
		$mensagem          	= "";
		
		if ( $FileInput["size"] > 5242880 )
    	{
	        return "Arquivo muito grande!";
    	}

    	else
    	{
	    	switch ( $FileInput["type"] )
	    	{
	    		case 'image/png': 
            	case 'image/gif': 
            	case 'image/jpeg': 
            	case 'image/pjpeg':
            	case 'application/pdf':
                break;

            	default:
                	return 'Tipo de arquivo não suportado!';
	    	}
    	}

    	$File_Name          						= strtolower( $FileInput['name'] );
    	$File_Ext           						= substr( $File_Name, strrpos($File_Name, '.') ); //pega a extensão do arquivo
    	$Random_Number      						= rand(0, 9999999); //número randomico que será concatenado ao nome
    	$NewFileName        						= $Random_Number.$File_Ext; //novo nome do arquivo

    	if( move_uploaded_file( $FileInput['tmp_name'], $UploadDirectory.$NewFileName ) )
       	{
       		$cliente 								= Cliente::model()->findByPk($Cliente_id);
       		$cadastro 								= Cadastro::model()->find('Cliente_id = ' .$Cliente_id);

       		if( $cadastro != NULL )
       		{
       			$path 								= $_FILES['FileInput']['name'];
				$ext 								= pathinfo($path, PATHINFO_EXTENSION);
				
	       		$anexo 								= new Anexo;
	       		$anexo->descricao 					= $Descricao_Arquivo['descricao'];
	       		$anexo->relative_path 				= '/uploads/'.$NewFileName;
	       		$anexo->absolute_path 				= '/var/www/nordeste/uploads/'.$NewFileName;
	       		$anexo->entidade_relacionamento 	= "cadastro";
	       		$anexo->data_cadastro 				= date('Y-m-d H:i:s');
	       		$anexo->id_entidade_relacionamento 	= $cadastro->id;
	       		$anexo->habilitado 					= 1;
	       		$anexo->extensao 					= $ext;
	       		
	       		if( $anexo->save() )
	       		{
	       			if( $cliente->propostasAtivas() != NULL )
	       			{
		       			/*Cria o alerta para a mensagem para disparar o alerta*/
	       				$proposta 					= $cliente->propostasAtivas();
	       				$proposta->alterarCondicoesOmni();
	       				
	       				$mensagem 					= strtoupper( Yii::app()->session['usuario']->nome_utilizador ) . " anexou novos arquivos ao cadastro do cliente " . strtoupper($cliente->pessoa->nome);
		       			Mensagem::model()->nova( $mensagem, $proposta->getDialogo(), Yii::app()->session['usuario']->id );
	       			}

	        		return array(
						'msgReturn' => 'Arquivo enviado com sucesso',
						'classNotify' => 'success'
					);
	       		}
	       		else
	       		{
	       			return array(
						'msgReturn' => 'Não foi possível enviar o arquivo. Contate o suporte',
						'classNotify' => 'error'
					);
	       		}
       		}
    	}

    	else
    	{
        	return array(
				'msgReturn' => 'Não foi possível enviar o arquivo. Contate o suporte',
				'classNotify' => 'error'
			);
    	}
	}

	public function remove( $id ){

		$anexo = Anexo::model()->findByPk($id);

		if( $anexo != NULL )
		{
			if( file_exists( $anexo->absolute_path ) )
			{
				if( $anexo->delete() )
				{	
					unlink( $anexo->absolute_path );

					return array(
						'msgReturn' => 'Arquivo excluído com sucesso',
						'classNotify' => 'success'
					);
				}

				else
				{
					return array(
						'msgReturn' => 'Erro ao excluir. Contate o suporte',
						'classNotify' => 'error'
					);
				}
			}
		}
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Anexo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('id_entidade_relacionamento, habilitado', 'numerical', 'integerOnly'=>true),
			array('entidade_relacionamento', 'length', 'max'=>100),
			array('descricao, relative_path, absolute_path, data_cadastro, url', 'safe'),
			
			array('id, url, descricao, relative_path, absolute_path, entidade_relacionamento, id_entidade_relacionamento, habilitado, data_cadastro, extensao', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'descricao' => 'Descricao',
			'relative_path' => 'Relative Path',
			'absolute_path' => 'Absolute Path',
			'entidade_relacionamento' => 'Entidade Relacionamento',
			'id_entidade_relacionamento' => 'Id Entidade Relacionamento',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'extensao' => 'Extensão',
			'url' => 'Url',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('descricao',$this->descricao,true);
		$criteria->compare('relative_path',$this->relative_path,true);
		$criteria->compare('absolute_path',$this->absolute_path,true);
		$criteria->compare('entidade_relacionamento',$this->entidade_relacionamento,true);
		$criteria->compare('id_entidade_relacionamento',$this->id_entidade_relacionamento);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('extensao',$this->extensao);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Anexo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
