<?php

class ArquivoCnab extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ArquivoCnab the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Arquivo_cnab';
	}


	public function getArquivosRemessa( $dataDe, $dataAte, $Banco_id, $tipo, $draw,$offset )
	{
		$rows 					= array();
		$util 					= new Util;
		$criteria 				= new CDbCriteria;
		$criteriaFilter			= new CDbCriteria;

		if ( !empty($data_de) && $data_de != NULL && !empty($data_ate) && $data_ate != NULL )
		{
            $criteriaFilter->addBetweenCondition('t.data_geracao', $util->view_date_to_bd( $data_de ), $util->view_date_to_bd( $data_ate ), 'AND');
        }

        if ( !empty($Banco_id) && $Banco_id != NULL )
		{
            $criteriaFilter->addInCondition('t.Banco_id', array($Banco_id), 'AND');
        }

        $criteria->addInCondition('t.tipo', 		array($tipo), 'AND');
        $criteriaFilter->addInCondition('t.tipo', 	array($tipo), 'AND');
        $criteriaFilter->offset 	= $offset;
        $criteriaFilter->limit 		= 10;

        foreach( ArquivoCnab::model()->findAll( $criteriaFilter ) as $arquivo )
        {
        	$rows[] 			= array(
        		'arquivo' 		=> $util->bd_date_to_view( substr($arquivo->data_geracao, 0,10) ) . ' às ' . substr($arquivo->data_geracao, 11),
        		'usuario' 		=> strtoupper($arquivo->usuario->nome_utilizador),
        		'banco' 		=> strtoupper($arquivo->banco->nome) . '-'. strtoupper($arquivo->banco->codigo),
        	);
        }

		return array(
			'draw'              => $draw,
            'recordsTotal'      => count($rows),
            'recordsFiltered'   => count( ArquivoCnab::model()->findAll($criteria) ),
            'data'              => $rows,
		);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Usuario_id, Filial_id', 'required'),
			array('tipo, Usuario_id, Filial_id, Banco_id', 'numerical', 'integerOnly'=>true),
			array('data_geracao_texto', 'length', 'max'=>45),
			array('relative_path, absolute_path, data_geracao', 'safe'),
			array('id, relative_path, absolute_path, tipo, data_geracao, data_importacao, data_geracao_texto, Usuario_id, Filial_id, Banco_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'filial' 	=> array(self::BELONGS_TO, 'Filial', 'Filial_id'),
			'usuario' 	=> array(self::BELONGS_TO, 'Usuario', 'Usuario_id'),
			'banco' 	=> array(self::BELONGS_TO, 'Banco', 'Banco_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' 					=> 'ID',
			'relative_path' 		=> 'Relative Path',
			'absolute_path' 		=> 'Absolute Path',
			'tipo' 					=> 'Tipo',
			'data_geracao' 			=> 'Data Geracao',
			'data_importacao' 		=> 'Data Importação',
			'data_geracao_texto'	=> 'Data Geracao Texto',
			'Usuario_id' 			=> 'Usuario',
			'Filial_id' 			=> 'Filial',
			'Banco_id'				=> 'Banco'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('relative_path',$this->relative_path,true);
		$criteria->compare('absolute_path',$this->absolute_path,true);
		$criteria->compare('tipo',$this->tipo);
		$criteria->compare('data_geracao',$this->data_geracao,true);
		$criteria->compare('data_geracao_texto',$this->data_geracao_texto,true);
		$criteria->compare('Usuario_id',$this->Usuario_id);
		$criteria->compare('Filial_id',$this->Filial_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	/*
		* $dataProcessamento do arquivo - determinado pelo banco
	*/

	/*
		Sunday 		(Dom)
		Monday 		(Seg)
		Tuesday 	(Ter)
		Wednesday 	(Qua)
		Thursday 	(Qui)
		Friday 		(Sex)
		Saturday 	(Sab)
	*/

	public function validarImportacao( $dataProcessamento, $Banco_id )
	{
		/*
			* Cria objeto dateTime para ter acesso às informações da data, como nome do dia, etc.
		*/
		$objDate 	= DateTime::createFromFormat('Y-m-d', date('Y').'-'.substr($dataProcessamento, 2, 2).'-'.substr($dataProcessamento, 0, 2));

		/* $objDate->format('d'); Dia, número*/
		/* $objDate->format('D'); Dia, nome abreviado*/
    	/* $objDate->format('M'); Mes, nome abreviado*/
    	/* $objDate->format('Y'); Ano*/

    	/*Padrão, recebe um dia a menos*/
    	$diasAMenos = 1;

    	/*
    		* Se o processamento foi segunda, o sistema deve voltar dois dias para verificar,
    		* pois o ultimo processamento foi sábado
    	*/
    	if( $objDate->format('D') == 'Mon' )
    	{
    		$diasAMenos = 3;
    	}

    	/*Data de processamento*/
    	//$objDate->format('Y-m-d');

    	/*Data de checagem*/
    	$dataChecagem 		= date('Y-m-d', (strtotime ( '-'.$diasAMenos.' day' , strtotime ( $objDate->format('Ymd') ) ) ));
    	    	
		$sql 				= "tipo = 2 AND Banco_id = " . $Banco_id . " AND data_geracao BETWEEN '".$dataChecagem." 00:00:00' AND '".$dataChecagem." 23:59:59' ";

    	/*
    	$h 					= fopen("logsOmni/logdataImport.txt", 'a+');
        fwrite($h, $sql);
		fclose($h);
		
		*/

    	if( ArquivoCnab::model()->find($sql) !== NULL || Feriado::model()->find("data = '".$dataChecagem."'") != NULL )
    	{
    		return true;
    	}

    	else
    	{
    		return false;
    	}
	}

	public function checkRemessaJaGerada($data){

		$criteria = new CDbCriteria;
		$criteria->addCondition('data_texto = ' . $data);
		$cnabGeradoData = CnabGeradosData::model()->find($criteria);

		if ($cnabGeradoData != NULL)
			return 1;
		return 0;
	}
}