<?php

class Cliente extends CActiveRecord {

    public function historicoIntegrado($cpf) {
        $propostas = [];
        $util = new Util;

        $query = "SELECT PR.* FROM Documento             AS DOC ";
        $query .= "INNER JOIN Pessoa_has_Documento        AS PHD  ON PHD.Documento_id = DOC.id AND DOC.numero = '" . $cpf . "' ";
        $query .= "INNER JOIN Pessoa                      AS P    ON P.id = PHD.Pessoa_id ";
        $query .= "INNER JOIN Cliente                     AS C    ON P.id = C.Pessoa_id ";
        $query .= "INNER JOIN Analise_de_Credito          AS AC   ON AC.Cliente_id = C.id AND AC.Filial_id NOT IN(259,258,281,268, 267, 282)";
        $query .= "INNER JOIN Proposta                    AS PR   ON PR.Analise_de_Credito_id = AC.id AND PR.Status_Proposta_id IN(2,7) AND PR.titulos_gerados ";

        $resultadoS1 = Yii::app()->db->createCommand($query)->queryAll();
        $resultadoS2 = Yii::app()->bdSigacOne->createCommand($query)->queryAll();


        for ($i = 0; $i < count($resultadoS1); $i++) {

            $propostas[] = [
                'id' => $resultadoS1[$i]['id'],
                'codigo' => $resultadoS1[$i]['codigo'],
                'data' => $util->bd_date_to_view(substr($resultadoS1[$i]['data_cadastro'], 0, 10)),
                'valor_soli' => "R$ " . number_format($resultadoS1[$i]['valor'], 2, ',', '.'),
                'entrada' => "R$ " . number_format($resultadoS1[$i]['valor_entrada'], 2, ',', '.'),
                'parcelas' => $resultadoS1[$i]['qtd_parcelas'],
                'urlPrint' => 'https://credshow.sigacbr.com.br/public/segundaViaBoletos/',
            ];
        }

        for ($y = 0; $y < count($resultadoS2); $y++) {

            $propostas[] = [
                'id' => $resultadoS2[$y]['id'],
                'codigo' => $resultadoS2[$y]['codigo'],
                'data' => $util->bd_date_to_view(substr($resultadoS2[$y]['data_cadastro'], 0, 10)),
                'valor_soli' => "R$ " . number_format($resultadoS2[$y]['valor'], 2, ',', '.'),
                'entrada' => "R$ " . number_format($resultadoS2[$y]['valor_entrada'], 2, ',', '.'),
                'parcelas' => $resultadoS2[$y]['qtd_parcelas'],
                'urlPrint' => 'https://s1.sigacbr.com.br/public/segundaViaBoletos/',
            ];
        }

        return $propostas;
    }

    public function getCidade() {
        return Cidade::model()->find($this->pessoa->cidade->id);
    }

    public function propostasAtivas() {

        $criteriaCheckPropExists = new CDbCriteria;

        $criteriaCheckPropExists->with = array('analiseDeCredito' => array(
                'alias' => 'ac'
        ));

        //$criteriaCheckPropExists->condition = '( t.Status_Proposta_id = 1 AND  ac.Cliente_id = '.$this->id.' ) OR ( t.Status_Proposta_id = 4 AND  ac.Cliente_id = '.$this->id.' )';
        $criteriaCheckPropExists->condition = 'ac.Cliente_id = ' . $this->id . ' AND ( t.Status_Proposta_id IN(1,4,9) ) AND t.habilitado AND ac.habilitado';

        return Proposta::model()->find($criteriaCheckPropExists);
    }

    public function importarCPF($Importacao) {

        ini_set('max_execution_time', 9600000);

        $Qry = "SELECT DOC.numero  AS CPF, DOC.data_emissao AS EMISSAO ";
        $Qry .= "FROM Documento AS DOC ";
        $Qry .= "WHERE DOC.data_cadastro BETWEEN '" . $Importacao['data_de'] . "' AND '" . $Importacao['data_ate'] . "' AND DOC.numero IS NOT NULL ";
        $Qry .= "AND DOC.Tipo_documento_id = 1";

        $cliente = NULL;
        $total = 0;
        $arrCpfs = [];

        $cpfs = Yii::app()->bdSigacOne->createCommand($Qry)->queryAll();

        foreach ($cpfs as $cpf) {

            $cliente = $this->getClienteByCpf($cpf['CPF']);

            if ($cliente == NULL) {
                $total++;
                $this->importarCadastro($cpf['CPF']);
            }
        }

        return $total;
    }

    public function hasAtraso($NumeroCPF = NULL) {

        $cpf = $NumeroCPF;
        $atrasos = [];

        if ($NumeroCPF == NULL) {
            $cpf = $this->pessoa->getCPF()->numero;
        }

        $query = "SELECT Par.id AS ID FROM Documento   AS CPF ";
        $query .= "INNER JOIN Pessoa_has_Documento     AS PHD  ON PHD.Documento_id         = CPF.id AND PHD.habilitado ";
        $query .= "INNER JOIN Cliente                  AS Cli  ON Cli.Pessoa_id            = PHD.Pessoa_id AND Cli.habilitado ";
        $query .= "INNER JOIN Analise_de_Credito       AS AC   ON AC.Cliente_id            = Cli.id AND AC.habilitado ";
        $query .= "INNER JOIN Proposta                 AS P    ON P.Analise_de_Credito_id  = AC.id AND P.habilitado ";
        $query .= "INNER JOIN Titulo                   AS T    ON P.id                     = T.Proposta_id AND T.NaturezaTitulo_id = 1 AND T.habilitado ";
        $query .= "INNER JOIN Parcela                  AS Par  ON Par.Titulo_id            = T.id AND Par.habilitado ";
        $query .= "WHERE CPF.numero = '" . $cpf . "'       AND Par.vencimento                  < '" . date('Y-m-d') . "' AND Par.valor_atual < Par.valor ";


        $atrasosD1 = Yii::app()->bdSigacOne->createCommand($query)->queryAll();
        $atrasosD2 = Yii::app()->db->createCommand($query)->queryAll();

        foreach ($atrasosD1 as $a1) {
            $atrasos[] = $a1['ID'];
        }

        foreach ($atrasosD2 as $a2) {
            $atrasos[] = $a2['ID'];
        }

        return $atrasos;
    }

    public function parcelasEmAberto( $NumeroCPF = NULL )
    {
        $cpf                         = $NumeroCPF;
        $atrasos                     = [];

        if ($NumeroCPF               == NULL)
        {
            $cpf                     = $this->pessoa->getCPF()->numero;
        }

        $query                       = "SELECT Par.id AS ID FROM Documento   AS CPF ";
        $query                      .= "INNER JOIN Pessoa_has_Documento     AS PHD  ON PHD.Documento_id         = CPF.id ";
        $query                      .= "INNER JOIN Cliente                  AS Cli  ON Cli.Pessoa_id            = PHD.Pessoa_id ";
        $query                      .= "INNER JOIN Analise_de_Credito       AS AC   ON AC.Cliente_id            = Cli.id AND AC.habilitado ";
        $query                      .= "INNER JOIN Proposta                 AS P    ON P.Analise_de_Credito_id  = AC.id AND P.habilitado ";
        $query                      .= "INNER JOIN Titulo                   AS T    ON P.id                     = T.Proposta_id AND T.NaturezaTitulo_id = 1 AND T.habilitado ";
        $query                      .= "INNER JOIN Parcela                  AS Par  ON Par.Titulo_id            = T.id AND Par.habilitado ";
        $query                      .= "WHERE CPF.numero = '" . $cpf . "' AND Par.valor_atual < Par.valor ";

        $atrasosD1                  = Yii::app()->bdSigacOne->createCommand($query)->queryAll();
        $atrasosD2                  = Yii::app()->db->createCommand($query)->queryAll();

        foreach ($atrasosD1         as $a1)
        {
            $atrasos[]              = $a1['ID'];
        }

        foreach ($atrasosD2         as $a2)
        {
            $atrasos[]              = $a2['ID'];
        }

        return $atrasos;
    }

    public function returnSatusCadastro($ClienteId) {
        $Cliente = Cliente::model()->findByPk($ClienteId);

        return $Cliente->clienteCadastroStatus();
    }

    public function persist($Cpf, $Cadastro, $Pessoa, $RG, $DadosBancarios, $action) {

        $Cadastro['titular_do_cpf'] = 1;
        $Cadastro['numero_de_dependentes'] = intval($Cadastro['numero_de_dependentes']);
        $Cadastro['conjugue_compoe_renda'] = intval($Cadastro['conjugue_compoe_renda']);

        $retorno = array();
        $retorno['dadosBancariosReturn'] = 'nada';

        $retorno["msg"] = "";

        if ($action == 'new') {

            if( Yii::app()->db->createCommand("SELECT cpf.id from beta.Documento as cpf where cpf.numero = '".$Cpf['numero']."'" )->queryRow() != NULL )
            {
              $cliente = $this->novo($Pessoa, $Cpf, $RG, $Cadastro);

              if ($cliente != NULL) {

                  if( (isset($DadosBancarios['numero'])) && (isset($DadosBancarios['agencia'])) )
                  {
                      if( $DadosBancarios['numero'] != null && $DadosBancarios['agencia'] != null )
                      {
                          $DadosBancarios['data_abertura'] = null;
                          DadosBancarios::model()->novo($DadosBancarios, $cliente->id);
                      }
                  }

                  $retorno['hasErrors']   = false;

                  $retorno['formConfig']  = array(
                      'fields'            => array(
                          'ControllerAction' => array(
                              'value'     => 'update',
                              'type'      => 'text',
                              'input_bind' => 'ControllerAction',
                          ),
                          'id' => array(
                              'value' => $cliente->id,
                              'type' => 'text',
                              'input_bind' => 'cliente_id',
                          ),
                          'Cliente_idfup' => array(
                              'value' => $cliente->id,
                              'type' => 'text',
                              'input_bind' => 'Cliente_id_fup',
                          ),
                      )
                  );

                  $retorno['msgConfig'][] = array(
                      'icon' => '<i class="fa fa-check-circle"></i>',
                      'cssClass' => 'alert alert-success',
                      'content' => 'Cliente cadastrado com sucesso!'
                  );

                  $retorno['msgConfig']['pnotify'][] = array(
                      'titulo' => 'Cliente cadastrado com sucesso!',
                      'texto' => 'Para continuar, conclua o cadastro!',
                      'tipo' => 'success',
                  );

                  $retorno['abasConfig']['hasError'] = array();
                  $retorno['abasConfig']['hasNoError'] = array('#li-aba-dados-cliente');

                  if ($Cadastro['conjugue_compoe_renda']) {
                    $retorno['abasConfig']['hasError'] = array('#li-aba-dados-conjuge');
                  }

              }
              else {
                  $retorno['hasErrors'] = true;

                  $retorno['formConfig'] = array(
                      'fields' => array(
                          'ControllerAction' => array(
                              'value' => 'new',
                              'type' => 'text',
                              'input_bind' => 'ControllerAction',
                          )
                      )
                  );

                  $retorno['msgConfig'][] = array(
                      'icon' => '<i class="fa fa-exclamation-triangle"></i>',
                      'cssClass' => 'alert alert-warning',
                      'content' => 'Não foi possível cadastrar! Contate o Suporte!'
                  );
              }
            }
            else{

                $cliente = $this->novo($Pessoa, $Cpf, $RG, $Cadastro);
                
                if ($cliente != NULL) {
                
                    if( (isset($DadosBancarios['numero'])) && (isset($DadosBancarios['agencia'])) )
                    {
                        if( $DadosBancarios['numero'] != null && $DadosBancarios['agencia'] != null )
                        {
                            $DadosBancarios['data_abertura'] = null;
                            DadosBancarios::model()->novo($DadosBancarios, $cliente->id);
                        }
                    }
                
                    $retorno['hasErrors']   = false;
                
                    $retorno['formConfig']  = array(
                        'fields'            => array(
                            'ControllerAction' => array(
                                'value'     => 'update',
                                'type'      => 'text',
                                'input_bind' => 'ControllerAction',
                            ),
                            'id' => array(
                                'value' => $cliente->id,
                                'type' => 'text',
                                'input_bind' => 'cliente_id',
                            ),
                            'Cliente_idfup' => array(
                                'value' => $cliente->id,
                                'type' => 'text',
                                'input_bind' => 'Cliente_id_fup',
                            ),
                        )
                    );
                
                    $retorno['msgConfig'][] = array(
                        'icon' => '<i class="fa fa-check-circle"></i>',
                        'cssClass' => 'alert alert-success',
                        'content' => 'Cliente cadastrado com sucesso!'
                    );
                
                    $retorno['msgConfig']['pnotify'][] = array(
                        'titulo' => 'Cliente cadastrado com sucesso!',
                        'texto' => 'Para continuar, conclua o cadastro!',
                        'tipo' => 'success',
                    );
                
                    $retorno['abasConfig']['hasError'] = array();
                    $retorno['abasConfig']['hasNoError'] = array('#li-aba-dados-cliente');
                
                    if ($Cadastro['conjugue_compoe_renda']) {
                      $retorno['abasConfig']['hasError'] = array('#li-aba-dados-conjuge');
                    }
                
                }
                else {
                    $retorno['hasErrors'] = true;
                
                    $retorno['formConfig'] = array(
                        'fields' => array(
                            'ControllerAction' => array(
                                'value' => 'new',
                                'type' => 'text',
                                'input_bind' => 'ControllerAction',
                            )
                        )
                    );
                
                    $retorno['msgConfig'][] = array(
                        'icon' => '<i class="fa fa-exclamation-triangle"></i>',
                        'cssClass' => 'alert alert-warning',
                        'content' => 'Não foi possível cadastrar! Contate o Suporte!'
                    );
                }
            }
            // else{
            //   /*Novos cadastros suspensos - solicitação 04/07/2017*/
            //   $retorno['hasErrors'] = true;

            //   $retorno['formConfig'] = array(
            //     'fields' => array(
            //       'ControllerAction' => array(
            //         'value' => 'nothing',
            //         'type' => 'text',
            //         'input_bind' => 'ControllerAction',
            //       )
            //     )
            //   );

            //   $retorno['msgConfig']['pnotify'][] = array(
            //     'titulo' => 'Erros foram encontrados!',
            //     'texto' => 'Novos cadastros suspensos!',
            //     'tipo' => 'error',
            //   );
            // }

        }
        /* Atualização */
        else if ($action == 'update') {

            $atualizacaoStatus = $this->atualizar($Cpf, $Cadastro, $Pessoa, $RG);
            $retorno['hasErrors'] = $atualizacaoStatus['hasErrors'];
            $retorno['msg'] = $atualizacaoStatus['msg'];

            $clienteAtualizado = Cliente::model()->getClienteByCpf($Cpf['numero']);

            $retorno['abasConfig']['hasError'] = array();
            $retorno['abasConfig']['hasNoError'] = array();

            $retorno['formConfig'] = array(
                'fields' => array(
                    'ControllerAction' => array(
                        'value' => 'update',
                        'type' => 'text',
                        'input_bind' => 'ControllerAction',
                    ),
                    'id' => array(
                        'value' => $clienteAtualizado->id,
                        'type' => 'text',
                        'input_bind' => 'cliente_id',
                    ),
                    'Cliente_idfup' => array(
                        'value' => $clienteAtualizado->id,
                        'type' => 'text',
                        'input_bind' => 'Cliente_id_fup',
                    ),
                )
            );

            if( isset( $DadosBancarios['numero'] ) && isset( $DadosBancarios['agencia'] ) )
            {
                if( $DadosBancarios['numero'] != null && $DadosBancarios['agencia'] != null )
                {
                    if( $clienteAtualizado->pessoa->getDadosBancarios() != NULL )
                    {
                        DadosBancarios::model()->atualizar($DadosBancarios, $clienteAtualizado->pessoa->getDadosBancarios()->id);
                    }
                    else
                    {
                        DadosBancarios::model()->novo($DadosBancarios, $clienteAtualizado->id);
                    }
                }
            }

            if ($retorno['hasErrors'])
            {
                $retorno['msgConfig']['pnotify'][] = array(
                    'titulo' => 'Erros foram encontrados!',
                    'texto' => $retorno["msg"] . ' Não foi possível atualizar os dados!',
                    'tipo' => 'error',
                );
            }

            else
            {
                $retorno['msgConfig']['pnotify'][] = array(
                    'titulo' => 'Ação realizada com sucesso!',
                    'texto' => 'Os dados foram atualizados!',
                    'tipo' => 'success',
                );

                $retorno['abasConfig']['hasNoError'] = array('#li-aba-dados-cliente');
            }
        }

        else
        {
            $retorno['hasErrors'] = true;

            $retorno['formConfig'] = array(
                'fields' => array(
                    'ControllerAction' => array(
                        'value' => 'nothing',
                        'type' => 'text',
                        'input_bind' => 'ControllerAction',
                    )
                )
            );

            $retorno['msgConfig']['pnotify'][] = array(
                'titulo' => 'Erros foram encontrados!',
                'texto' => 'Não foi possível completar sua requisição!',
                'tipo' => 'error',
            );
        }

        return $retorno;
    }

    public function atualizar($Cpf, $Cadastro, $Pessoa, $RG) {
        $arrReturn = array('hasErrors' => false);

        $cliente = Cliente::model()->getClienteByCpf($Cpf['numero']);

        if ($cliente != NULL) {
            if ($cliente->pessoa->atualizar($Pessoa, $cliente->pessoa->id) == NULL) {
                $arrReturn['hasErrors'] = true;
            }

            if ($cliente->pessoa->getRG() != NULL) {
                if ($cliente->pessoa->getRG()->atualizarRG($RG, $cliente->pessoa->getRG()->id) == NULL) {
                    $arrReturn['hasErrors'] = true;
                }
            } else {
                $RG['data_cadastro'] = date('Y-m-d H:i:s');
                $RG['Tipo_documento_id'] = 2;

                $cDoc = Documento::model()->novo($RG, $cliente->id);

                if ($cDoc['hasErrors']) {
                    $arrReturn['hasErrors'] = true;
                }
            }

            $cadastroCliente = Cadastro::model()->find("habilitado AND Cliente_id = $cliente->id");

            if($cadastroCliente == null)
            {
                $cadastroCliente                = new Cadastro;
                $cadastroCliente->Cliente_id = $cliente->id;
                $cadastroCliente->Empresa_id = 5;
                $cadastroCliente->conjugue_compoe_renda = $Cadastro["conjugue_compoe_renda"];
                $cadastroCliente->numero_de_dependentes = $Cadastro["numero_de_dependentes"];
                $cadastroCliente->cliente_da_casa = $Cadastro["cliente_da_casa"];
                $cadastroCliente->nome_da_mae = $Cadastro["nome_da_mae"];
                $cadastroCliente->nome_do_pai = $Cadastro["nome_do_pai"];
                $cadastroCliente->analfabeto = $Cadastro["analfabeto"];

                if(!$cadastroCliente->save())
                {

                    ob_start();
                    var_dump($cadastroCliente->getErrors());
                    $resultadoErro = ob_get_clean();

                    $arrReturn['hasErrors'] = true;
                    $arrReturn["msg"] = $resultadoErro . ". Linha 352";

                    return $arrReturn;

                }

            }
            else
            {
                $retornoAtualizar = $cadastroCliente->atualizarCadastro($Cadastro, $cadastroCliente->id);
            }

            $arrReturn['hasErrors'] = $retornoAtualizar["hasErrors"];
            $arrReturn['msg'] = $retornoAtualizar["msg"];

        }

        return $arrReturn;
    }

    public function getLatestReferencia() {
        return Referencia::model()->find('Cliente_id = ' . $this->id . ' ORDER BY data_cadastro DESC');
    }

    public function pessoaSituacaoCadastral($cpf) {
        $util = new Util;
        $clienteCPF = 0;

        $cliente    = $this->getClienteByCpf($cpf);

        $cpfVip     = Documento::model()->find('vip and numero = ' . $cpf);

        /*
          Cliente não cadastrado
          Vamos tentar encontrá-lo na base de dados do Sigac1
         */

        /*
          if( $cliente == NULL )
          {
          //$cliente                        = $this->importarCadastro($cpf);
          }
         */

        $configuracaoRetorno = array('cadastrado' => false);
        $configuracaoRetorno = array('externalId' => $clienteCPF);

        $configuracaoRetorno['cadastrado']  = Documento::model()->cadastrado($cpf);
        $configuracaoRetorno['vip']         = false;

        if($cpfVip != null){
           $configuracaoRetorno['vip']      = true;
        }
        /*  CPF já cadastrado
          Neste caso, temos três possibilidades:
          #O cadastrao do cliente está OK (Requisitos mínimos para iniciar uma análise)
          #O cliente não possui requisitos mínimos para iniciar uma análise (Ex: falta telefone, endereço)
          #Está tudo ok, porém, o cliente possui restrinções administrativas (Ex: atraso, suspensão)
         */

        /*Verificar se o cpf está no s1*/
        $queryCpfS1 = " SELECT cpf.id from beta.Documento as cpf where cpf.numero = '".$cpf."'";
        //Yii::app()->db->createCommand( $queryCpfS1 )->queryRow() != NULL

        $configuracaoRetorno['otherFormsConfig']['formsToShow'][] = array('form-endereco', 'form-contato', 'form-dados-profissionais', 'form-referencia', 'form-informacoes-basicas', 'form-anexos');

        if ( $cliente != NULL ){
            $anexos = $cliente->listarAnexos(1, 0);
            $configuracaoRetorno['anexos'] = $anexos['data'];

            $configuracaoRetorno['abasConfig']['hasError'] = array();
            $configuracaoRetorno['statusDoCadastro'] = $cliente->clienteCadastroStatus();

            $cadastroCliente = $cliente->getCadastro();
            $actionFormCliente = '';

            if ($cadastroCliente != NULL) { 
                if ($cadastroCliente->atualizar) {
                    $configuracaoRetorno['abasConfig']['hasError'][] = '#li-aba-dados-cliente';
                    $actionFormCliente = 'update';
                    $configuracaoRetorno['statusDoCadastro']['atualizar'] = true;
                } else {
                    $configuracaoRetorno['abasConfig']['hasNoError'][] = '#li-aba-dados-cliente';
                    $actionFormCliente = 'update';
                    $configuracaoRetorno['statusDoCadastro']['atualizar'] = false;
                }
            } else {
                $configuracaoRetorno['abasConfig']['hasError'][] = '#li-aba-dados-cliente';
                $actionFormCliente = 'update';
                $configuracaoRetorno['statusDoCadastro']['atualizar'] = true;
            }

            //libera geral os anexos!
            $configuracaoRetorno['abasConfig']['hasNoError'][] = '#li-aba-anexos';

            //#li-aba-detalhes-proposta','#li-aba-dados-cliente','#li-aba-dados-profissionais','#li-aba-endereco','#li-aba-contato','#li-aba-referencias

            $rgCliente = $cliente->pessoa->getRG();

            $NRgCliente = NULL;
            $OEmissorRG = NULL;
            $UFEmissorRG = NULL;
            $EmissaoRG = NULL;
            $cliNascimento = NULL;

            if ($rgCliente != NULL) {
                $NRgCliente = ( $rgCliente->numero != NULL ? $rgCliente->numero : NULL );
                $OEmissorRG = ( $rgCliente->orgao_emissor != NULL ? $rgCliente->orgao_emissor : NULL );
                $UFEmissorRG = ( $rgCliente->uf_emissor != NULL ? $rgCliente->uf_emissor : NULL );
                $EmissaoRG = ( $rgCliente->data_emissao != NULL ? $util->bd_date_to_view($rgCliente->data_emissao) : NULL );
            }

            if ($cliente->pessoa->nascimento != NULL) {
                $cliNascimento = $util->bd_date_to_view($cliente->pessoa->nascimento);
            }

            $configuracaoRetorno['formConfig'][] = array(
                'fields' => array(
                    'ControllerAction' => array(
                        'value' => $actionFormCliente,
                        'type' => 'text',
                        'input_bind' => 'ControllerAction',
                    ),
                    'id' => array(
                        'value' => $cliente->id,
                        'type' => 'text',
                        'input_bind' => 'cliente_id',
                    ),
                    'Cliente_idfup' => array(
                        'value' => $cliente->id,
                        'type' => 'text',
                        'input_bind' => 'Cliente_id_fup',
                    ),
                    'nome' => array(
                        'value' => $cliente->pessoa->nome,
                        'type' => 'text',
                        'input_bind' => 'cliente_nome',
                    ),
                    'sexo' => array(
                        'value' => $cliente->pessoa->sexo,
                        'type' => 'select',
                        'input_bind' => 'Pessoa_sexo',
                    ),
                    'nascimento' => array(
                        'value' => $cliNascimento,
                        'type' => 'text',
                        'input_bind' => 'nascimento',
                    ),
                    'rg' => array(
                        'value' => $NRgCliente,
                        'type' => 'text',
                        'input_bind' => 'rg',
                    ),
                    'rg_orgao_emissor' => array(
                        'value' => $OEmissorRG,
                        'type' => 'text',
                        'input_bind' => 'RG_orgao_emissor',
                    ),
                    'RG_uf_emissor' => array(
                        'value' => $UFEmissorRG,
                        'type' => 'select',
                        'input_bind' => 'RG_uf_emissor',
                    ),
                    'RG_emissao' => array(
                        'value' => $EmissaoRG,
                        'type' => 'text',
                        'input_bind' => 'RG_data_emissao',
                    ),
                    'select-nacionalidade' => array(
                        'value' => $cliente->pessoa->nacionalidade,
                        'type' => 'select',
                        'input_bind' => 'select-nacionalidade',
                    ),
                    'Pessoa_naturalidade' => array(
                        'value' => $cliente->pessoa->naturalidade,
                        'type' => 'select',
                        'input_bind' => 'Pessoa_naturalidade',
                    ),
                    'Pessoa_Estado_Civil_id' => array(
                        'value' => $cliente->pessoa->Estado_Civil_id,
                        'type' => 'select',
                        'input_bind' => 's2id_Pessoa_Estado_Civil_id',
                    ),
                    'conjugue_compoe_renda' => array(
                        'value' => $cliente->getCadastro() != NULL ? $cliente->getCadastro()->conjugue_compoe_renda : '',
                        'type' => 'select',
                        'input_bind' => 'Cadastro_conjugue_compoe_renda',
                    ),
                    'cliente_da_casa' => array(
                        'value' => $cliente->getCadastro() != NULL ? $cliente->getCadastro()->cliente_da_casa : '',
                        'type' => 'select',
                        'input_bind' => 'Cadastro_cliente_da_casa',
                    ),
                    'cliente_da_casa2' => array(
                        'value' => $cliente->getCadastro() != NULL ? $cliente->getCadastro()->cliente_da_casa : '',
                        'type' => 'select',
                        'input_bind' => 'Cadastro_cliente_da_casa2',
                    ),
                    'nome_da_mae' => array(
                        'value' => $cliente->getCadastro() != NULL ? $cliente->getCadastro()->nome_da_mae : '',
                        'type' => 'text',
                        'input_bind' => 'Cadastro_nome_da_mae',
                    ),
                    'nome_do_pai' => array(
                        'value' => $cliente->getCadastro() != NULL ? $cliente->getCadastro()->nome_do_pai : '',
                        'type' => 'text',
                        'input_bind' => 'Cadastro_nome_do_pai',
                    ),
                    'n_de_dependentes' => array(
                        'value' => $cliente->getCadastro() != NULL ? $cliente->getCadastro()->numero_de_dependentes : '',
                        'type' => 'text',
                        'input_bind' => 'Cadastro_numero_de_dependentes',
                    ),
                    'analfabeto' => array(
                        'value' => $cliente->getCadastro() != NULL ? $cliente->getCadastro()->analfabeto : '',
                        'type' => 'select',
                        'input_bind' => 'select-analfabeto',
                    ),
                    'naturalidade_cidade' => array(
                        'value' => $cliente->pessoa->naturalidade_cidade,
                        'type' => 'text',
                        'input_bind' => 'naturalidade_cidade',
                    ),
                )
            );

            $dadosProfissionais = $cliente->pessoa->getDadosProfissionais();

            $dadosBancarios     = $cliente->pessoa->getDadosBancarios();


            if( $dadosBancarios != NULL )
            {
                $configuracaoRetorno['formConfig'][] = $dadosBancarios->loadFormEdit($dadosBancarios->id);
            }


            if ($dadosProfissionais != NULL)
            {
                $configuracaoRetorno['formConfig'][] = $dadosProfissionais->loadFormEdit($dadosProfissionais->id);
                $configuracaoRetorno['abasConfig']['hasNoError'][] = '#li-aba-dados-profissionais';
            }
            else
            {
                $configuracaoRetorno['abasConfig']['hasError'][] = '#li-aba-dados-profissionais';
            }

            if (count($cliente->pessoa->contato->contatoHasTelefones) > 1) {
                //$configuracaoRetorno['formConfig'][] = $cliente->pessoa->contato->getLasTelefone()->loadFormEdit($cliente->pessoa->contato->getLasTelefone()->id);
                $configuracaoRetorno['formConfig'][] = $cliente->montarFormTelefones();
                $configuracaoRetorno['abasConfig']['hasNoError'][] = '#li-aba-contato';
            } else if (count($cliente->pessoa->contato->contatoHasTelefones) == 1) {
                $configuracaoRetorno['formConfig'][] = $cliente->montarFormTelefones();
                $configuracaoRetorno['abasConfig']['hasError'][] = '#li-aba-contato';
            } else {
                $configuracaoRetorno['abasConfig']['hasError'][] = '#li-aba-contato';
            }

            if ($cliente->pessoa->getEnderecoCobranca() != NULL) {
                $configuracaoRetorno['formConfig'][] = $cliente->pessoa->getEnderecoCobranca()->loadFormEdit($cliente->pessoa->getEnderecoCobranca()->id);
                $configuracaoRetorno['abasConfig']['hasNoError'][] = '#li-aba-endereco';
            } else {
                $configuracaoRetorno['abasConfig']['hasError'][] = '#li-aba-endereco';
            }

            if (count(Referencia::model()->findAll('Cliente_id = ' . $cliente->id)) > 1) {
                $configuracaoRetorno['formConfig'][] = $cliente->montarFormReferencias();
                $configuracaoRetorno['abasConfig']['hasNoError'][] = '#li-aba-referencias';
            } else if (count(Referencia::model()->findAll('Cliente_id = ' . $cliente->id)) == 1) {
                $configuracaoRetorno['formConfig'][] = $cliente->montarFormReferencias();
                $configuracaoRetorno['abasConfig']['hasError'][] = '#li-aba-referencias';
            } else {
                $configuracaoRetorno['abasConfig']['hasError'][] = '#li-aba-referencias';
            }
            
            if($cliente->getCadastro() != NULL){
                if ($cliente->getCadastro()->conjugue_compoe_renda) {
                    $configuracaoRetorno['otherFormsConfig']['formsToShow'][] = array('form-conjuge');
                }
            }

            /* Cadastrado, mas, não regular */
            if ($configuracaoRetorno['statusDoCadastro']['status'] == 2) {
                $configuracaoRetorno['msgConfig'][] = array(
                    'icon' => '<i class="fa fa-check-circle"></i>',
                    'cssClass' => 'alert alert-success',
                    'content' => 'Cliente cadastrado! Por favor, atualize as informações abaixo:',
                    'form_show' => '#',
                    'divId' => 'aviso_ressalvas',
                );

                for ($i = 0; $i < count($configuracaoRetorno['statusDoCadastro']['observacoes']); $i++) {

                    if ($configuracaoRetorno['statusDoCadastro']['observacoes'][$i]['div_id'] == 'aviso_conjuge') {
                        $configuracaoRetorno['abasConfig']['hasError'][8] = '#li-aba-dados-conjuge';
                    } else {
                        $configuracaoRetorno['abasConfig']['hasNoError'][8] = '#li-aba-dados-conjuge';
                    }

                    $configuracaoRetorno['msgConfig'][] = array(
                        'icon' => '<i class="fa fa-exclamation-triangle"></i>',
                        'cssClass' => 'alert alert-warning',
                        'content' => $configuracaoRetorno['statusDoCadastro']['observacoes'][$i]['text'] . $configuracaoRetorno['statusDoCadastro']['observacoes'][$i]['button_modal'],
                        'form_show' => $configuracaoRetorno['statusDoCadastro']['observacoes'][$i]['show_form'],
                        'divId' => $configuracaoRetorno['statusDoCadastro']['observacoes'][$i]['div_id']
                    );
                }
            }

            /* Cadastrado e situação regular */
            else {
                if ($cliente->getCadastro() != NULL){
                    if ($cliente->getCadastro()->conjugue_compoe_renda) {
                        $configuracaoRetorno['abasConfig']['hasNoError'][] = '#li-aba-dados-conjuge';
                        $configuracaoRetorno['formConfig'][] = $cliente->pessoa->getConjuge()->conjugeLoadDadosPessoais();
                        $configuracaoRetorno['formConfig'][] = $cliente->pessoa->getConjuge()->conjugeLoadDadosPorifissionais();
                    }
                }

                /*
                  Implentar .............................
                  Montar os formulários dos dados que o cliente possui, por exemplo, telefones, referencias, dados do conjuge, endereços e dados profissionais.
                 */
                $configuracaoRetorno['msgConfig'][] = array(
                    'icon' => '<i class="fa fa-check-circle"></i>',
                    'cssClass' => 'alert alert-success',
                    'content' => 'O cliente ' . $cliente->pessoa->nome . ' possui cadastro regular! Uma proposta pode ser enviada!'
                );
            }
        }

        /* Cliente não cadastrado */
        else {

            $msgRetorno = 'Cliente não cadastrado!';

            if( Yii::app()->db->createCommand( $queryCpfS1 )->queryRow() != NULL ){
              $configuracaoRetorno['clienteNoS1'] = true;
              $msgRetorno = 'Cliente não cadastrado!';
            }

            $configuracaoRetorno['formConfig'][] = array(
                'fields' => array(
                    'ControllerAction' => array(
                        'value' => 'new',
                        'type' => 'text',
                        'input_bind' => 'ControllerAction',
                    ),
                    'clienteId' => array(
                        'value' => '0',
                        'type' => 'text',
                        'input_bind' => 'cliente_id',
                    ),
                )
            );

            $configuracaoRetorno['abasConfig']['hasError'] = array('#li-aba-detalhes-proposta', '#li-aba-dados-cliente', '#li-aba-dados-profissionais', '#li-aba-endereco', '#li-aba-contato', '#li-aba-referencias', '#li-aba-anexos');
            $configuracaoRetorno['abasConfig']['hasNoError'] = array();

            $configuracaoRetorno['msgConfig'][] = array(
                'icon' => '<i class="fa fa-times-circle"></i>',
                'cssClass' => 'alert alert-danger',
                'content' => $msgRetorno,
                'divId' => 'aviso_nao_cadastrado'
            );
        }

        return $configuracaoRetorno;
    }

    public function pessoaGetCvc($num_cartao, $cvc_dig){
        $retorno = array('hasError' => false);
        $retorno = array('msg' => '');

        $cartao = Cartao::model()->find("numero = " . $num_cartao);
        if($cartao != null){
            if($cartao->csv == $cvc_dig){
                $retorno['msg'] = 'O CVC digitado pertence ao cartão.';
            }else{
                $retorno['hasError'] = true;
                $retorno['msg'] = 'O CVC digitado não pertence ao cartão informado.';
            }
        }else{
            $retorno['hasError'] = true;
            $retorno['msg'] = 'Cartão não encontrado nos registros.';
        }

        return $retorno;
    }

    public function pessoaGetCartao($num_cartao, $cpf){
        $retorno = array('hasError' => false);
        $retorno = array('msg' => '');
        $retorno = array('idClt' => '');
        $retorno = array('idCartao' => '');

        //verificando se o cartão existe no banco de dados
        $cartao = Cartao::model()->find("numero = " . $num_cartao);
        if($cartao != null){
            $retorno['idCartao'] = $cartao->id;
            $cartao_has_prop = CartaoHasProposta::model()->find("Cartao_id = ". $cartao->id);
            //verificando se existe uma proposta associada a este cartão
            if($cartao_has_prop != null){
                $proposta = Proposta::model()->find("id = " . $cartao_has_prop->Proposta_id);
                //consultando a proposta associada ao cartão
                if($proposta != null){
                    $analise = AnaliseDeCredito::model()->find("id = " . $proposta->Analise_de_Credito_id);
                    //verificando se essa proposta foi analisada
                    if($analise != null){
                        //verificando se o cartão informado bate com o cpf do cliente
                        $cliente1 = Cliente::model()->find("id = " . $analise->Cliente_id);
                        $cliente2 = $this->getClienteByCpf($cpf);
                        if($cliente1 == $cliente2){
                            $retorno['hasError'] = false;
                            $retorno['idClt'] = $cliente1->id;
                        }else{
                            $retorno['hasError'] = true;
                            $retorno['msg'] = 'O cartão não está associado ao cliente informado.';
                        }
                    }else{
                        $retorno['hasError'] = true;
                        $retorno['msg'] = 'Não existe uma análise associada a esse cartão.';
                    }
                }else{
                    $retorno['hasError'] = true;
                    $retorno['msg'] = 'A proposta associada a este cartão não pode ser encontrada.';
                }
            }else{
                $retorno['hasError'] = true;
                    $retorno['msg'] = 'Não existe uma proposta associada a esse cartão.';
            }
        }else{
            $retorno['hasError'] = true;
            $retorno['msg'] = 'O cartão informado não existe nos nossos registros.';
        }

        return $retorno;
    }

    //checa se o cliente está apto para realizar uma análise
    public function clienteCadastroStatus() {

        $rendaLiquida = 0;
        $countEndCob = 0;
        $countTelCont = 0;

        $clienteCadastro = $this->getCadastro();

        /* Se o array for retornado desta forma inicia, então o cadastro do cliente está apto */
        $retorno = array(
            'status' => 1,
            'observacoes' => array()
        );
        /* Verificando os dados profissionais */
        $dadosProfissionaisCliente = DadosProfissionais::model()->findAll('Pessoa_id = ' . $this->pessoa->id . ' AND habilitado');

        if (count($dadosProfissionaisCliente) > 0) {
            foreach ($dadosProfissionaisCliente as $dadoProfissional) {
                $rendaLiquida += $dadoProfissional->renda_liquida;
            }
        }

        if ($rendaLiquida <= 0) {
            $retorno['status'] = 2;
            $retorno['observacoes'][] = array(
                'text' => 'O cadastro de dados profissionais está incompleto!',
                'button_modal' => '<a  href="#" data-go-to-tab="#tab4_dados_profissionais" style="padding: 0.3em 1.1em !important;margin-left:150px;" data-form-show="form-dados-profissionais" data-form-input-init="dp_cnpj_cpf" class="btn-show-form label label-success">Cadastrar</a>',
                'show_form' => '#form-dados-profissionais',
                'div_id' => 'aviso_dados_profissionais',
            );
        }
        /* --------------------------------------------------------- */


        /* Verificando endereço de cobrança */
        $clienteHasEnderecos = PessoaHasEndereco::model()->findAll('Pessoa_id = ' . $this->pessoa->id);

        if (count($clienteHasEnderecos) > 0) {
            foreach ($clienteHasEnderecos as $end) {
                $currEnd = Endereco::model()->findByPk($end->Endereco_id);

                if ($currEnd->endereco_de_cobranca)
                    $countEndCob++;
            }
        }

        if ($countEndCob <= 0) {
            $retorno['status'] = 2;

            $retorno['observacoes'][] = array(
                'text' => 'O cliente não possui um endereço de cobrança configurado!',
                'button_modal' => '<a href="#" data-go-to-tab="#tab4_endereco" style="padding: 0.3em 1.1em !important;margin-left:150px;" data-form-show="form-endereco" data-form-input-init="cep_cliente" class="btn-show-form label label-success">Cadastrar</a>',
                'show_form' => '#form-endereco',
                'div_id' => 'aviso_endereco',
            );
        }
        /* ----------------------------------------------------------- */

        /* Verificando se o cliente possui, ao menos, um telefone para contato */
        $contatoCliente = Contato::model()->findByPk($this->pessoa->Contato_id);

        if ($contatoCliente != NULL) {
            $contatoHastelefones = ContatoHasTelefone::model()->findAll('Contato_id = ' . $contatoCliente->id);

            if (count($contatoHastelefones) > 0) {
                foreach ($contatoHastelefones as $cHtelefone) {
                    $currTel = Telefone::model()->findByPk($cHtelefone->Telefone_id);

                    if ($currTel != NULL)
                        $countTelCont++;
                }
            }
        }


        if ($countTelCont < 2) {
            $retorno['status'] = 2;
            $retorno['observacoes'][] = array(
                'text' => 'O cliente precisa possuir, no mínimo, dois telefones para contato!',
                'button_modal' => '<a href="#" data-go-to-tab="#tab4_contato" style="padding: 0.3em 1.1em !important;margin-left:150px;" data-form-show="form-contato" data-form-input-init="telefone_numero" class="btn-show-form label label-success">Cadastrar</a>',
                'show_form' => '#form-contato',
                'div_id' => 'aviso_contato',
            );
        }

        /*
          Verificando se o cliente sinalizou que o conjuge compõe renda, e se sim, verifica se existe um
          laço matrimonial. Se existir, o conjuge precisa possuir dados profissionais cadastrados.
         */
        if ($clienteCadastro != NULL && $clienteCadastro->conjugue_compoe_renda) {
            if ($this->pessoa->casada() != NULL) {
                $papelRelacao = $this->pessoa->casada();
                $relacaoMatrimonio = RelacaoFamiliar::model()->find('Parente_1 = ' . $papelRelacao->id . ' OR Parente_2 = ' . $papelRelacao->id . ' AND habilitado');

                if ($relacaoMatrimonio != NULL) {
                    $idRelacaoConjuge = ( $relacaoMatrimonio->Parente_1 == $papelRelacao->id ? $relacaoMatrimonio->Parente_2 : $relacaoMatrimonio->Parente_1 );
                    $papelRelacaoConj = PapelRelacao::model()->findByPk($idRelacaoConjuge);

                    if ($papelRelacaoConj != NULL) {
                        if ($papelRelacaoConj->pessoa->getDadosProfissionais() == NULL) {
                            $retorno['status'] = 2;

                            $retorno['observacoes'][] = array(
                                'text' => 'O Cônjuge compõe renda, porém, não possui dados profissionais cadastrados no sistema.',
                                'button_modal' => '<a href="#" data-go-to-tab="#tab4_dados_conjuge" style="padding: 0.3em 1.1em !important;margin-left:150px;" data-form-show="form-conjuge" data-form-input-init="dp_conjuge_cnpj_cpf" class="btn-show-form label label-success">Cadastrar</a>',
                                'show_form' => '#form-conjuge',
                                'div_id' => 'aviso_conjuge',
                            );
                        }
                    }
                } else {
                    $retorno['status'] = 2;

                    $retorno['observacoes'][] = array(
                        'text' => 'A renda do cliente é composta pela renda do Cônjuge, porém, não existe uma relação matrimonial cadastrada.',
                        'button_modal' => '<a href="#" data-go-to-tab="#tab4_dados_conjuge" style="padding: 0.3em 1.1em !important;margin-left:150px;" data-form-show="form-conjuge" data-form-input-init="cpf_conjuge" class="btn-show-form label label-success">Cadastrar</a>',
                        'show_form' => '#form-conjuge',
                        'div_id' => 'aviso_conjuge',
                    );
                }
            } else {
                $retorno['status'] = 2;

                $retorno['observacoes'][] = array(
                    'text' => 'A renda do cliente é composta pela renda do Cônjuge, porém, não existe uma relação matrimonial cadastrada.',
                    'button_modal' => '<a href="#" data-go-to-tab="#tab4_dados_conjuge" style="padding: 0.3em 1.1em !important;margin-left:150px;" data-form-show="form-conjuge" data-form-input-init="cpf_conjuge" class="btn-show-form label label-success">Cadastrar</a>',
                    'show_form' => '#form-conjuge',
                    'div_id' => 'aviso_conjuge',
                );
            }
        }

        /* O cliente precisa possuir, ao menos, uma referencia cadastrada */
        $countReferencias = count(Referencia::model()->findAll('Cliente_id = ' . $this->id . ' AND habilitado'));

        if ($countReferencias <= 1) {
            $retorno['status'] = 2;
            $retorno['observacoes'][] = array(
                'text' => 'O cliente precisa possuir duas referencias!',
                'button_modal' => '<a href="#" data-go-to-tab="#tab4_referencias" style="padding: 0.3em 1.1em !important;margin-left:150px;" data-form-show="form-referencia" data-form-input-init="telefone_numero" class="btn-show-form label label-success">Cadastrar</a>',
                'show_form' => '#form-referencia',
                'div_id' => 'aviso_referencia',
            );
        }

        return $retorno;
    }

    public function novo($Pessoa, $CPFCliente, $DocumentoCliente, $Cadastro, $atualizarCadastroCliente = NULL) {

        /*Variavel que guarda true ou false, para cpf cadastrado ou não*/
        $cpfCadastrado = Documento::model()->cadastrado($CPFCliente['numero']);

        $retorno = NULL;

        $util = new Util;

        $pessoa = new Pessoa;
        $cliente = new Cliente;
        $contato = new Contato;
        $cpf = new Documento;
        $rg = new Documento;

        $pessoa->attributes = $Pessoa;
        $cpf->attributes = $CPFCliente;
        $rg->attributes = $DocumentoCliente;
        $rg->data_emissao = $util->view_date_to_bd($rg->data_emissao);

        $pessoa->data_cadastro = date('Y-m-d H:i:s');
        $pessoa->data_cadastro_br = date('d/m/Y');
        $cpf->data_cadastro = date('Y-m-d H:i:s');
        $rg->data_cadastro = date('Y-m-d H:i:s');

        $cpf->Tipo_documento_id = 1;
        $rg->Tipo_documento_id = 2;

        if ($contato->save()) {

            $sigacLog = new SigacLog;
            $sigacLog->saveLog('Cadastro de contato', 'Contato', $contato->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Contato criado.", "127.0.0.1", null, Yii::app()->session->getSessionId());

            $util = new Util;
            $pessoa->Contato_id = $contato->id;

            if (array_key_exists("nascimento", $Pessoa)) {
                $pessoa->nascimento = $util->view_date_to_bd($Pessoa['nascimento']);
            }

            /*Se o cpf já for cadastrado, vamos atribuir ao cpf do novo cliente que está sendo criado*/
            /*O cpf existe mas não está relacionado com nenhum cliente, foi um cpf importado*/
            if( $cpfCadastrado ){
                $cpf = Documento::model()->find("t.numero = '".$CPFCliente['numero']."'");
            }

            /*não existe, vamos salvar*/
            else{
                if( $cpf->save() ){
                    $cpfCadastrado = true;
                }
            }

            if ($cpfCadastrado && $rg->save() && $pessoa->save()) {
                $sigacLogCPF = new SigacLog;
                $sigacLogRG = new SigacLog;
                $sigacLogPessoa = new SigacLog;


                $sigacLogCPF->saveLog('Cadastro de Documento[CPF]', 'Documento', $cpf->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Documento[CPF] " . $cpf->numero . " cadastrado.", "127.0.0.1", null, Yii::app()->session->getSessionId());
                $sigacLogRG->saveLog('Cadastro de Documento[RG]', 'Documento', $rg->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Documento[RG] " . $rg->numero . " cadastrado.", "127.0.0.1", null, Yii::app()->session->getSessionId());
                $sigacLogPessoa->saveLog('Cadastro de Pessoa', 'Pessoa', $pessoa->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Pessoa " . $pessoa->nome . " cadastrada.", "127.0.0.1", null, Yii::app()->session->getSessionId());

                $pessoaHasCPF = new PessoaHasDocumento;
                $pessoaHasRG = new PessoaHasDocumento;

                $pessoaHasCPF->Pessoa_id = $pessoa->id;
                $pessoaHasRG->Pessoa_id = $pessoa->id;
                $pessoaHasCPF->Documento_id = $cpf->id;
                $pessoaHasRG->Documento_id = $rg->id;
                $cliente->Pessoa_id = $pessoa->id;

                $pessoaHasCPF->save();
                $pessoaHasRG->save();

                if ($cliente->save()) {

                    $retorno = $cliente;

                    $cadastro                   = new Cadastro                  ;
                    $cadastro->attributes       = $Cadastro                     ;
                    $cadastro->Cliente_id       = $cliente->id                  ;
                    $cadastro->Empresa_id       = 5                             ;
                    $cadastro->nome_da_mae      = $Cadastro['nome_da_mae'   ]   ;
                    $cadastro->nome_do_pai      = $Cadastro['nome_do_pai'   ]   ;
                    $cadastro->data_cadastro    = date('Y-m-d H:i:s')           ;
                    $cadastro->analfabeto       = $Cadastro["analfabeto"    ]   ;

                    if ($atualizarCadastroCliente != NULL) {
                        $cadastro->atualizar = 1;
                    } else {
                        $cadastro->atualizar = 0;
                    }

                    $cadastro->save();

                    $sigacLogCliente = new SigacLog;
                    $sigacLogCliente->saveLog('Cadastro de Cliente', 'Cliente', $cliente->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Cliente " . $pessoa->nome . " cadastrado(a).", "127.0.0.1", null, Yii::app()->session->getSessionId());

                    $sigacLogCadastro = new SigacLog;
                    $sigacLogCadastro->saveLog('Cadastro de Cadastro de Cliente', 'Cadastro', $cadastro->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Cadastro do Cliente " . $pessoa->nome . " criado.", "127.0.0.1", null, Yii::app()->session->getSessionId());
                }
            }
        }

        return $retorno;
    }

    public function tableName() {

        return 'Cliente';
    }

    public function pagamentosCliente($cpf){
        $campoCnabConfig = new CampoCnabConfig;
        $arrReturn = array(
            'emAberto' => 0,
            'pago' => 0,
            'atrasado' => 0,
            'cancelado' => 0,
            'vencendoHoje' => 0
        );

        $propostas = [];

        $sql = "SELECT Ps.id FROM beta.Analise_de_Credito AS ACs

                INNER JOIN beta.Cliente                         AS Cs		ON Cs.id = ACs.Cliente_id 		AND Cs.habilitado
                INNER JOIN beta.Pessoa				AS Pss		ON Pss.id = Cs.Pessoa_id 		AND Pss.habilitado
                INNER JOIN beta.Pessoa_has_Documento		AS PHDs		ON PHDs.Pessoa_id = Pss.id 		AND PHDs.habilitado
                INNER JOIN beta.Documento			As Ds		ON Ds.Tipo_documento_id = 1 		AND Ds.id = PHDs.Documento_id 	AND Ds.numero = '".$cpf."' AND Ds.habilitado
                INNER JOIN beta.Proposta			AS Ps		ON Ps.Analise_de_credito_id = ACs.id 	AND ACs.habilitado		AND Ps.Status_Proposta_id = 2
                INNER JOIN beta.Status_Proposta                 AS SPs		ON SPs.id = Ps.Status_Proposta_id
                LEFT  JOIN beta.Solicitacao_de_Cancelamento 	AS SCs		ON SCs.Proposta_id = Ps.id 		AND SCs.aceite 			AND SCs.habilitado";

        $sql1 = "SELECT Ps.id
                FROM sigac.Analise_de_Credito AS ACs

                INNER JOIN sigac.Cliente                         AS Cs		ON Cs.id = ACs.Cliente_id 		AND Cs.habilitado
                INNER JOIN sigac.Pessoa				AS Pss		ON Pss.id = Cs.Pessoa_id 		AND Pss.habilitado
                INNER JOIN sigac.Pessoa_has_Documento		AS PHDs		ON PHDs.Pessoa_id = Pss.id 		AND PHDs.habilitado
                INNER JOIN sigac.Documento			As Ds		ON Ds.Tipo_documento_id = 1 		AND Ds.id = PHDs.Documento_id 	AND Ds.numero = '".$cpf."' AND Ds.habilitado
                INNER JOIN sigac.Proposta			AS Ps		ON Ps.Analise_de_credito_id = ACs.id 	AND ACs.habilitado		AND Ps.Status_Proposta_id = 2
                INNER JOIN sigac.Status_Proposta                 AS SPs		ON SPs.id = Ps.Status_Proposta_id
                LEFT  JOIN sigac.Solicitacao_de_Cancelamento 	AS SCs		ON SCs.Proposta_id = Ps.id 		AND SCs.aceite 			AND SCs.habilitado";

        try
        {
            $propostas = Yii::app()->db->createCommand($sql)->queryAll();
        }
        catch (Exception $ex)
        {
            try {
                $propostas = Yii::app()->db->createCommand($sql1)->queryAll();
            } catch (Exception $ex) {

            }

        }

        foreach ($propostas as $prop) {

            $sql2 = "SELECT Pas.*, Bs.id AS 'b_id', Bs.valor_mora FROM beta.Titulo 	AS Ts

                         INNER JOIN beta.Parcela   AS Pas 	ON Pas.Titulo_id = Ts.id 	AND Pas.habilitado
                         LEFT  JOIN beta.Baixa     AS Bs 	ON Bs.Parcela_id = Pas.id	AND Bs.baixado

                         WHERE Ts.Proposta_id = ".$prop['id']." AND Ts.NaturezaTitulo_id = 1 AND Ts.habilitado";

            $sql3 = "SELECT Pas.*, Bs.id AS 'b_id', Bs.valor_mora
                        FROM sigac.Titulo 	AS Ts

                         INNER JOIN sigac.Parcela   AS Pas 	ON Pas.Titulo_id = Ts.id 	AND Pas.habilitado
                         LEFT  JOIN sigac.Baixa     AS Bs 	ON Bs.Parcela_id = Pas.id	AND Bs.baixado

                         WHERE Ts.Proposta_id = ".$prop['id']." AND Ts.NaturezaTitulo_id = 1 AND Ts.habilitado";

            try
            {
                $parcelas = Yii::app()->db->createCommand($sql2)->queryAll();
            }
            catch (Exception $ex)
            {
                $parcelas = Yii::app()->db->createCommand($sql3)->queryAll();
            }


            foreach ($parcelas as $parc) {
                if($parc['b_id'] == null){
                    if($parc['vencimento'] < Date('Y-m-d')){
                        $arrReturn['atrasado'] += $parc['valor'];
                        $arrReturn['emAberto'] += $parc['valor'];
                    }else{
                        $arrReturn['emAberto'] += $parc['valor'];
                    }

                    if($parc['vencimento'] == Date('Y-m-d')){
                        $arrReturn['vencendoHoje'] += $parc['valor'];
                    }
                }else{
                    $arrReturn['pago'] += $parc['valor'] + $campoCnabConfig->cnabValorMonetarioToDouble($parc['valor_mora']);
                }
            }
        }

        $sql3 = "SELECT Ps.id FROM nordeste2.Analise_de_Credito AS ACs

                INNER JOIN nordeste2.Cliente			AS Cs		ON Cs.id = ACs.Cliente_id 		AND Cs.habilitado
                INNER JOIN nordeste2.Pessoa			AS Pss		ON Pss.id = Cs.Pessoa_id 		AND Pss.habilitado
                INNER JOIN nordeste2.Pessoa_has_Documento	AS PHDs		ON PHDs.Pessoa_id = Pss.id 		AND PHDs.habilitado
                INNER JOIN nordeste2.Documento			As Ds		ON Ds.Tipo_documento_id = 1 		AND Ds.id = PHDs.Documento_id 	AND Ds.numero = '".$cpf."' AND Ds.habilitado
                INNER JOIN nordeste2.Proposta			AS Ps		ON Ps.Analise_de_credito_id = ACs.id 	AND ACs.habilitado		AND Ps.Status_Proposta_id = 2
                INNER JOIN nordeste2.Status_Proposta		AS SPs		ON SPs.id = Ps.Status_Proposta_id
                LEFT  JOIN nordeste2.Solicitacao_de_Cancelamento AS SCs		ON SCs.Proposta_id = Ps.id 		AND SCs.aceite 			AND SCs.habilitado";

        $propostas2 = Yii::app()->db->createCommand($sql3)->queryAll();

        foreach ($propostas2 as $prop) {
            $sql4 = "SELECT Pas.*, Bs.id AS 'b_id', Bs.valor_mora FROM nordeste2.Titulo 	AS Ts

                     INNER JOIN nordeste2.Parcela   AS Pas 	ON Pas.Titulo_id = Ts.id 	AND Pas.habilitado
                     LEFT  JOIN nordeste2.Baixa     AS Bs 	ON Bs.Parcela_id = Pas.id	AND Bs.baixado

                     WHERE Ts.Proposta_id = ".$prop['id']." AND Ts.NaturezaTitulo_id = 1 AND Ts.habilitado";

            $parcelas2 = Yii::app()->db->createCommand($sql4)->queryAll();

            foreach ($parcelas2 as $parc) {
                if($parc['b_id'] == null){
                    if($parc['vencimento'] < Date('Y-m-d')){
                        $arrReturn['atrasado'] += $parc['valor'];
                        $arrReturn['emAberto'] += $parc['valor'];
                    }else{
                        $arrReturn['emAberto'] += $parc['valor'];
                    }

                    if($parc['vencimento'] == Date('Y-m-d')){
                        $arrReturn['vencendoHoje'] += $parc['valor'];
                    }
                }else{
                    $arrReturn['pago'] += $parc['valor'] + $campoCnabConfig->cnabValorMonetarioToDouble($parc['valor_mora']);
                }
            }
        }

        return $arrReturn;
    }

    public function numerosDePagamentos() {

        $arrReturn = array(
            'emAberto' => 0,
            'pago' => 0,
            'atrasado' => 0,
            'cancelado' => 0,
            'vencendoHoje' => 0
        );

        $analises = $this->getAnalisesCliente();

        foreach ($analises as $analise) {

            $propostas = Proposta::model()->findAll('Analise_de_Credito_id = ' . $analise->id . ' AND Status_Proposta_id = ' . 2 . ' AND habilitado');

            foreach ($propostas as $proposta) {

                $arrStatusPgtos = $proposta->statusPagamentos();
                $arrReturn['emAberto'] += $arrStatusPgtos['emAberto'];
                $arrReturn['pago'] += $arrStatusPgtos['pago'];
                $arrReturn['atrasado'] += $arrStatusPgtos['atrasado'];
                $arrReturn['cancelado'] += $arrStatusPgtos['cancelado'];
                $arrReturn['vencendoHoje'] += $arrStatusPgtos['vencendoHoje'];
            }
        }

        return $arrReturn;
    }

    public function listarEnderecos($draw) {

        $pessoa = Pessoa::model()->findByPk($this->Pessoa_id);

        return $pessoa->listarEnderecos($draw);
    }

    public function listarContatos(){
        $contatos = [];

        $contato                        = Contato::model()->findByPk($this->pessoa->Contato_id);
        $contatoHasTelefone             = ContatoHasTelefone::model()->findAll('habilitado AND Contato_id = ' . $contato->id);
        if(count($contatoHasTelefone) > 0){
            foreach ($contatoHasTelefone as $cht) {
                $telefone               = Telefone::model()->findByPk($cht->Telefone_id);

                if ($telefone != NULL) {
                    $t = array(
                        "id"    => $cht->Contato_id,
                        "text"  => $telefone->discagem_direta_distancia . " " .$telefone->numero . " (CLIENTE)"
                    );

                    $contatos[] = $t;
                }
            }
        }

        $referencias                    = Referencia::model()->findAll('habilitado AND Cliente_id = ' . $this->id);
        if (count($referencias) > 0) {
            foreach ($referencias as $ref) {
                $CHT     = ContatoHasTelefone::model()->find('habilitado AND Contato_id = ' . $ref->Contato_id);
                if($CHT != NULL){
                    $telefone           = Telefone::model()->findByPk($CHT->Telefone_id);
                    if ($telefone != NULL) {
                        $t = array(
                            "id"    => $CHT->Contato_id,
                            "text"  => $telefone->discagem_direta_distancia . " " . $telefone->numero . " (". strtoupper($ref->parentesco) .": " . $ref->nome . ")"
                        );

                        $contatos[] = $t;
                    }
                }
            }
        }

        return $contatos;
    }

    public function listarReferencias($draw) {

        $rows = array();
        $countReferencias = 0;
        $btn = "";
        $numero_telefone = "";

        $referencias = Referencia::model()->findAll('Cliente_id = ' . $this->id);

        if (count($referencias) > 0) {

            foreach ($referencias as $ref) {

                $contatoHasTelefone = ContatoHasTelefone::model()->find('Contato_id = ' . $ref->Contato_id);

                if ($contatoHasTelefone != NULL) {
                    $telefone = Telefone::model()->findByPk($contatoHasTelefone->Telefone_id);

                    if ($telefone != NULL) {
                        $numero_telefone = $telefone->numero;
                    }
                }

                if ($ref != NULL) {

                    $countReferencias++;

                    $btn = '<a data-input-controller-action="controller_referencia_action" data-controller-action="update" data-url-request="/referencia/load/" data-entity-id="' . $ref->id . '"' . 'class="btn btn-primary btn-update btn-load-form" style="padding:2px 5px!important; font-size:11px!important;" href="#"><i class="clip-pencil"></i></a>';

                    $row = array(
                        'parentesco' => strtoupper($ref->parentesco),
                        'nome' => strtoupper($ref->nome),
                        'numero' => $numero_telefone,
                        'tipo' => strtoupper($ref->tipoReferencia->tipo),
                        'obs' => '<input type="hidden" value="'. $ref->id .'">' . $ref->observacao,
                        'btn' => $btn,
                    );

                    $rows[] = $row;
                    $btn = "";
                }
            }
        }

        return (array(
            "draw" => $draw,
            "recordsTotal" => $countReferencias,
            "recordsFiltered" => $countReferencias,
            "data" => $rows
        ));
    }

    public function listarFamiliares($draw) {

        $rows = array();
        $countFamiliares = 0;
        $btn = "";
        $parenteNome = "";
        $parenteParentesco = "";

        $papeisRelacoes = PapelRelacao::model()->findAll('Pessoa_id = ' . $this->pessoa->id);

        if (count($papeisRelacoes) > 0) {

            foreach ($papeisRelacoes as $pr) {

                $relacoesFamiliares = RelacaoFamiliar::model()->findAll('Parente_1 = ' . $pr->id . ' OR Parente_2 = ' . $pr->id);

                if (count($relacoesFamiliares) > 0) {

                    foreach ($relacoesFamiliares as $rf) {
                        if ($rf->Parente_1 == $pr->id) {
                            $familiarPapelRelacao = PapelRelacao::model()->findByPk($rf->Parente_2);
                            $pessoaParente = Pessoa::model()->findByPk($familiarPapelRelacao->Pessoa_id);
                            $parenteNome = $pessoaParente->nome;
                            $parenteParentesco = $familiarPapelRelacao->parentesco->nome;
                            $btn = '<a data-entity-id="' . $pessoaParente->id . '"' . 'class="btn btn-primary btn-load-form" style="padding:2px 5px!important; font-size:11px!important;" href="#"><i class="clip-pencil"></i></a>';
                        } else if ($rf->Parente_2 == $pr->id) {
                            $familiarPapelRelacao = PapelRelacao::model()->findByPk($rf->Parente_1);
                            $pessoaParente = Pessoa::model()->findByPk($familiarPapelRelacao->Pessoa_id);
                            $parenteNome = $pessoaParente->nome;
                            $parenteParentesco = $familiarPapelRelacao->parentesco->nome;
                            $btn = '<a data-entity-id="' . $pessoaParente->id . '"' . 'class="btn btn-primary btn-load-form" style="padding:2px 5px!important; font-size:11px!important;" href="#"><i class="clip-pencil"></i></a>';
                        }

                        $row = array(
                            'nome' => $parenteNome,
                            'relacao' => $parenteParentesco,
                            'btn' => $btn,
                        );

                        $rows[] = $row;

                        $parenteNome = "";
                        $parenteParentesco = "";
                        $btn = "";
                    }
                }

                /* if( $ref != NULL )
                  {

                  $countFamiliares++;

                  $btn = '<a data-input-controller-action="controller_referencia_action" data-controller-action="update" data-url-request="/referencia/load/" data-entity-id="'.$ref->id.'"'.'class="btn btn-primary btn-update btn-load-form" style="padding:2px 5px!important; font-size:11px!important;" href="#"><i class="clip-pencil"></i></a>';

                  $row                = array(
                  'nome'          => $ref->nome,
                  'relacao'       => $numero_telefone,
                  );

                  $rows[] = $row;
                  } */
            }
        }

        return (array(
            "draw" => $draw,
            "recordsTotal" => $countFamiliares,
            "recordsFiltered" => $countFamiliares,
            "data" => $rows
        ));
    }

    public function listarDadosBancarios($draw) {

        $pessoa = Pessoa::model()->findByPk($this->Pessoa_id);

        return $pessoa->listarDadosBancarios($draw);
    }

    public function listarDadosProfissionais($draw) {

        return $this->pessoa->listarDadosProfissionais($draw);
    }

    public function listarTelefones($draw) {

        return $this->pessoa->listarTelefones($draw);
    }

    public function listarEmails($draw) {

        return $this->pessoa->listarEmails($draw);
    }

    public function listarAnexos($draw, $canDelete) {

        $rows = array();
        $countAnexos = 0;
        $btn = "";

        $cadastro = Cadastro::model()->find('Cliente_id = ' . $this->id);
        $btn = "";

        $url = null;

        if ($cadastro != NULL) {

            $anexos = Anexo::model()->findAll('entidade_relacionamento = "cadastro" AND id_entidade_relacionamento = ' . $cadastro->id);
            $util = new Util;

            if ($anexos != NULL) {
                foreach ($anexos as $anexo) {

                    if( $anexo->url != NULL ){
                        $url = $anexo->url;
                    }
                    else{
                        $url = $anexo->relative_path;
                    }

                    $btn = '<a target="_blank" href="' . $url . '"' . 'class="btn btn-primary" style="padding:2px 5px!important; font-size:11px!important;" ><i class=" clip-search"></i></a>';

                    if ($canDelete != 0) {
                        $btn .= ' <a data-toggle="modal" href="#modal_confirm_delete_anexo" data-anexo-id="' . $anexo->id . '" class="btn btn-red btn-remove-anexo" style="padding:2px 5px!important; font-size:11px!important;" ><i class=" clip-close"></i></a>';
                    }

                    $row = array(
                        'descricao' => $anexo->descricao,
                        'ext' => $anexo->extensao,
                        'data_envio' => $util->bd_date_to_view(substr($anexo->data_cadastro, 0, 10)) . ' às ' . substr($anexo->data_cadastro, 10),
                        'btn' => $btn,
                        'path' => $anexo->relative_path
                    );

                    $rows[] = $row;
                    $btn = "";
                }
            }
        }

        return (array(
            "draw" => $draw,
            "recordsTotal" => $countAnexos,
            "recordsFiltered" => $countAnexos,
            "data" => $rows
        ));
    }

    public function listarDocumentos($draw) {

        return $this->pessoa->listarDocumentos($draw);
    }

    //em espera
    public function propostasS1eS2($cpf){
        $sql = "SELECT Ps.id, Ps.codigo, Ps.data_cadastro_br, Ps.valor, Ps.valor_final, Ps.qtd_parcelas, SCs.id AS 'sc_id', Ps.Status_Proposta_id, SPs.status FROM sigac.Analise_de_Credito AS ACs

                INNER JOIN sigac.Cliente			AS Cs		ON Cs.id = ACs.Cliente_id 		AND Cs.habilitado
                INNER JOIN sigac.Pessoa				AS Pss		ON Pss.id = Cs.Pessoa_id 		AND Pss.habilitado
                INNER JOIN sigac.Pessoa_has_Documento		AS PHDs		ON PHDs.Pessoa_id = Pss.id 		AND PHDs.habilitado
                INNER JOIN sigac.Documento			As Ds		ON Ds.Tipo_documento_id = 1             AND Ds.id = PHDs.Documento_id 	AND Ds.numero = '".$cpf."' AND Ds.habilitado
                INNER JOIN sigac.Proposta			AS Ps		ON Ps.Analise_de_credito_id = ACs.id 	AND ACs.habilitado		AND Ps.Status_Proposta_id = 2
                INNER JOIN sigac.Status_Proposta		AS SPs		ON SPs.id = Ps.Status_Proposta_id
                LEFT  JOIN sigac.Solicitacao_de_Cancelamento 	AS SCs		ON SCs.Proposta_id = Ps.id 		AND SCs.aceite 			AND SCs.habilitado

                UNION ALL

                SELECT Pn.id, Pn.codigo, Pn.data_cadastro_br, Pn.valor, Pn.valor_final, Pn.qtd_parcelas, SCn.id AS 'sc_id', Pn.Status_Proposta_id, SPn.status FROM nordeste2.Analise_de_Credito AS ACn

                INNER JOIN nordeste2.Cliente                     AS Cn		ON Cn.id = ACn.Cliente_id 		AND Cn.habilitado
                INNER JOIN nordeste2.Pessoa			AS Psn		ON Psn.id = Cn.Pessoa_id 		AND Psn.habilitado
                INNER JOIN nordeste2.Pessoa_has_Documento	AS PHDn		ON PHDn.Pessoa_id = Psn.id 		AND PHDn.habilitado
                INNER JOIN nordeste2.Documento			As Dn		ON Dn.Tipo_documento_id = 1 		AND Dn.id = PHDn.Documento_id 	AND Dn.numero = '".$cpf."' AND Dn.habilitado
                INNER JOIN nordeste2.Proposta			AS Pn		ON Pn.Analise_de_credito_id = ACn.id 	AND ACn.habilitado		AND Pn.Status_Proposta_id = 2
                INNER JOIN nordeste2.Status_Proposta		AS SPn		ON SPn.id = Pn.Status_Proposta_id
                LEFT  JOIN nordeste2.Solicitacao_de_Cancelamento AS SCn		ON SCn.Proposta_id = Pn.id 		AND SCn.aceite 			AND SCn.habilitado";
    }

    public function getAnalisesCliente() {

        return AnaliseDeCredito::model()->findAll('habilitado AND Cliente_id = ' . $this->id . ' AND habilitado');
    }

    public function getDadosSociais($draw) {

        $rows = array();
        $countTelefones = 0;
        $btn = "";

        $cadastro = Cadastro::model()->find('Cliente_id = ' . $this->id . ' AND habilitado');

        if (count($cadastro) != NULL) {
            $btn = '<a data-url-request="/cadastro/load/" data-entity-id="' . $cadastro->id . '"' . 'class="btn btn-primary btn-load-form" style="padding:2px 5px!important; font-size:11px!important;" href="#"><i class="clip-pencil"></i></a>';
            $titularCpf = ( $cadastro->titular_do_cpf ? "Sim" : "Não" );
            $conjCompRenda = ( $cadastro->conjugue_compoe_renda ? "Sim" : "Não" );

            $row = array(
                'nome_da_mae' => strtoupper($cadastro->nome_da_mae),
                'nome_do_pai' => strtoupper($cadastro->nome_do_pai),
                'numero_de_dependentes' => $cadastro->numero_de_dependentes,
                'titular_cpf' => $titularCpf,
                'estado_civil' => strtoupper($this->pessoa->estadoCivil->estado),
                'conjCompRenda' => $conjCompRenda,
                'btn' => $btn,
            );
            $btn = "";
            $rows[] = $row;
        }

        return (array(
            "draw" => $draw,
            "recordsTotal" => 1,
            "recordsFiltered" => 1,
            "data" => $rows
        ));
    }

    public function rules() {

        return array(
            array('Pessoa_id', 'required'),
            array('Pessoa_id, habilitado', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, Pessoa_id, habilitado', 'safe', 'on' => 'search'),
        );
    }

    public function listReferencias() {
        return Referencia::model()->findAll('Cliente_id = ' . $this->id . ' AND habilitado');
    }

    public function montarFormTelefones() {
        $arrReturn = array();

        $countTele = 0;

        foreach ($this->pessoa->contato->contatoHasTelefones as $cht) {
            $countTele++;

            if ($countTele <= 3) {
                $id = 'telefone' . $countTele . '_id';
                $numero = 'telefone' . $countTele . '_numero';
                $tipo = 'Tipo_Telefone' . $countTele . '_id';
                $ramal = 'telefone' . $countTele . '_ramal';

                $arrReturn['fields']['ControllerAction'] = array('value' => 'update', 'type' => 'text', 'input_bind' => 'ControllerTelefoneAction');
                $arrReturn['fields'][$id] = array('value' => $cht->telefone->id, 'type' => 'text', 'input_bind' => $id);
                $arrReturn['fields'][$numero] = array('value' => $cht->telefone->discagem_direta_distancia . $cht->telefone->numero, 'type' => 'text', 'input_bind' => $numero);
                $arrReturn['fields'][$tipo] = array('value' => $cht->telefone->Tipo_Telefone_id, 'type' => 'select', 'input_bind' => $tipo);
                $arrReturn['fields'][$ramal] = array('value' => $cht->telefone->ramal, 'type' => 'text', 'input_bind' => $ramal);
            }
        }

        return $arrReturn;
    }

    public function montarFormReferencias() {
        $arrReturn = array();
        $criteria = new CDbCriteria;
        $criteria->compare('Cliente_id', $this->id);
        $criteria->compare('habilitado', 1, true);
        $criteria->offset = 0;
        $criteria->limit = 3;
        $criteria->order = 'id DESC';

        if (count(Referencia::model()->findAll($criteria)) > 0) {
            $countRefs = 0;

            foreach (Referencia::model()->findAll($criteria) as $referencia) {

                $telefone_obj_numero = "";
                $telefone_obj_id = "";

                $contato = Contato::model()->findByPk($referencia->Contato_id);

                if ($contato != NULL) {
                    $contatoHasTelefone = ContatoHasTelefone::model()->find('Contato_id = ' . $contato->id);

                    if ($contatoHasTelefone != NULL) {
                        $telefone = Telefone::model()->findByPk($contatoHasTelefone->Telefone_id);

                        if ($telefone != NULL) {
                            $telefone_obj_id = $telefone->id;
                            $telefone_obj_numero = $telefone->discagem_direta_distancia . $telefone->numero;
                        }
                    }
                }

                $countRefs++;

                $id = 'ref' . $countRefs . 'Id';
                $parentesco = 'referencia' . $countRefs . '_parentesco';
                $nome = 'referencia' . $countRefs . '_nome';
                $tipo = 'Referencia' . $countRefs . '_Tipo_Referencia_id';
                $telefone_id = 't' . $countRefs . '_id';
                $telefone_numero = 'telefone_referencia' . $countRefs . '_numero';

                $arrReturn['fields']['ControllerAction'] = array('value' => 'update', 'type' => 'text', 'input_bind' => 'ControllerReferenciaAction');
                $arrReturn['fields'][$id] = array('value' => $referencia->id, 'type' => 'text', 'input_bind' => $id);
                $arrReturn['fields'][$parentesco] = array('value' => $referencia->parentesco, 'type' => 'text', 'input_bind' => $parentesco);
                $arrReturn['fields'][$nome] = array('value' => $referencia->nome, 'type' => 'text', 'input_bind' => $nome);
                $arrReturn['fields'][$tipo] = array('value' => $referencia->Tipo_Referencia_id, 'type' => 'select', 'input_bind' => $tipo);
                $arrReturn['fields'][$telefone_id] = array('value' => $telefone_obj_id, 'type' => 'text', 'input_bind' => $telefone_id);
                $arrReturn['fields'][$telefone_numero] = array('value' => $telefone_obj_numero, 'type' => 'text', 'input_bind' => $telefone_numero);
            }
        }

        return $arrReturn;
    }

    public function getConjuge() {

        $conjuge = Familiar::model()->find('Cliente_id = ' . $this->id . ' AND Tipo_Familiar_id = ' . 2);

        return $conjuge;
    }

    public function hasFichamento() {
        $fichamento = FichamentoHasStatusFichamento::model()->find('Cliente = ' . $this->id . ' AND StatusFichamento_id = 2 AND habilitado');

        if ($fichamento != NULL) {
            return true;
        } else {
            return false;
        }
    }

    public function relations() {

        return array(
            'pessoa' => array(self::BELONGS_TO, 'Pessoa', 'Pessoa_id', 'order' => 'nome ASC'),
        );
    }

    public function attributeLabels() {

        return array(
            'id' => 'ID',
            'Pessoa_id' => 'Pessoa',
            'habilitado' => 'Habilitado',
        );
    }

    public function search() {

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('Pessoa_id', $this->Pessoa_id);
        $criteria->compare('habilitado', $this->habilitado);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function clearFamiliares() {

        $familiares = Familiar::model()->findAll('Cliente_id = ' . $this->id);

        if (count($familiares) > 0) {

            foreach ($familiares as $familiar) {

                $pessoaFamiliar = Pessoa::model()->findByPk($familiar->id);
                $pessoaFamiliar->clearDocumentos();
                $pessoaFamiliar->clearEnderecos();
                $pessoaFamiliar->clearDadosProfissionais();
            }
        }
    }

    public function clearReferencias() {

        $referencias = Referencia::model()->findAll('Cliente_id = ' . $this->id);

        if (count($referencias) > 0) {

            foreach ($referencias as $referencia) {

                $referencia->delete();
                $referencia->clearContato();
            }
        }
    }

    public function listar($draw, $offset, $order, $columns) {

        $orderDir = $order[0]['dir'];
        $orderString = "";
        $num_cpf = "";

        if ($order[0]['column'] == 0) {
            $orderString = "pessoa.nome $orderDir";
        }

        $countClientes = Cliente::model()->findAll('habilitado');

        $util = new Util;

        $rows = array();

        $crt = new CDbCriteria;

        if (!empty($columns[0]['search']['value']) && !ctype_space($columns[0]['search']['value'])) {
            $crt->addSearchCondition('pessoa.nome', $columns[0]['search']['value'], 'AND');
        }

        $crt->offset = $offset;
        $crt->limit = 10;
        $crt->with = array('pessoa');
        $crt->order = "pessoa.nome $orderDir";

        foreach (Cliente::model()->findAll($crt) as $cliente) {

            $btn = '<form method="post" action="' . Yii::app()->getBaseUrl(true) . '/cliente/editar">';
            $btn .= '  <button class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Editar"><i class="fa fa-edit"></i></button>';
            $btn .= '  <input type="hidden" name="idCliente" value="' . $cliente->id . '">';
            $btn .= '</form>';

            if ($cliente->pessoa->getCPF() != NULL) {
                $num_cpf = $cliente->pessoa->getCPF()->numero;
            }

            $nascimento = ($cliente->pessoa->nascimento != NULL) ? $util->bd_date_to_view($cliente->pessoa->nascimento) : '--/--/----';

            $row = array(
                'nome' => strtoupper($cliente->pessoa->nome),
                'cpf' => $num_cpf,
                'sexo' => $cliente->pessoa->sexo,
                'nascimento' => $nascimento,
                'btn' => $btn,
            );
            $num_cpf = "";
            $rows[] = $row;
        }

        return (array(
            "draw" => $draw,
            "recordsTotal" => count($rows),
            "recordsFiltered" => count($countClientes),
            "data" => $rows
        ));
    }

    public function getClienteByCpf($cpf) {
        $Qry = " SELECT cli.id as clienteId from Cliente as cli ";
        $Qry .= " INNER JOIN Pessoa as p ON cli.Pessoa_id = p.id ";
        $Qry .= " INNER JOIN Pessoa_has_Documento as phd on phd.Pessoa_id = p.id ";
        $Qry .= " INNER JOIN Documento as doc on phd.Documento_id = doc.id ";
        $Qry .= " WHERE doc.Tipo_documento_id = 1 AND doc.numero = '" . $cpf . "'";

        $r = Yii::app()->db->createCommand($Qry)->queryAll();

        if ($r != NULL) {
            return Cliente::model()->findByPk($r[0]['clienteId']);
        } else {
            return NULL;
        }
    }

    public function importarCadastro($cpf) {

        $Pessoa = [];
        $CPFCliente = [];
        $DocumentoCliente = [];
        $Cadastro = [];
        $DadosProfissionais = [];
        $DadosReferencia = [];
        $TelefoneReferencia = [];
        $util = new Util;

        $Qry = " SELECT cli.id as clienteId from Cliente as cli ";
        $Qry .= " INNER JOIN Pessoa as p ON cli.Pessoa_id = p.id ";
        $Qry .= " INNER JOIN Pessoa_has_Documento as phd on phd.Pessoa_id = p.id ";
        $Qry .= " INNER JOIN Documento as doc on phd.Documento_id = doc.id ";
        $Qry .= " WHERE doc.Tipo_documento_id = 1 AND doc.numero = '" . $cpf . "'";

        /* $file = fopen('queryImportar.sql', 'w');
          fwrite($file, $Qry);
          fclose($file); */

        $r = Yii::app()->bdSigacOne->createCommand($Qry)->queryAll();

        $QryDados = "";

        if ($r != NULL) {
            $QryDados = "SELECT C.id AS ClienteId, P.nome AS Nome, P.Contato_id AS Contato, P.nascimento AS Nascimento, P.sexo AS Sexo, ";
            $QryDados .= "P.naturalidade AS NAT, P.naturalidade_cidade AS NCidade,P.nacionalidade AS NAC, P.Estado_Civil_id AS ES, ";
            $QryDados .= "Cad.conjugue_compoe_renda AS CompoeRenda, Cad.nome_do_pai AS Pai, Cad.nome_da_mae AS Mae, Cad.numero_de_dependentes AS Dependentes, ";
            $QryDados .= "RG.numero AS RGnum, RG.orgao_emissor AS OE, RG.data_emissao AS DE, RG.uf_emissor AS UFE, ";
            $QryDados .= "DP.empresa AS Empresa, DP.data_admissao AS Data_admissao, DP.Tipo_Ocupacao_id AS TipoOcupacao, ";
            $QryDados .= "DP.Classe_Profissional_id AS ClasseProfissional, DP.renda_liquida as Renda, DP.tipo_de_comprovante AS TipoComprovante, DP.profissao AS Profissao, ";
            $QryDados .= "DP.mes_ano_renda AS MesAnoRenda, DP.orgao_pagador AS OrgaoPagador, DP.numero_do_beneficio AS NumeroBeneficio, DP.cnpj_cpf AS CnpjCpf, ";
            $QryDados .= "EndP.logradouro AS EPLogradouro, EndP.numero AS EPNumero, EndP.complemento AS EPComplemento, EndP.cidade AS EPCidade, EndP.bairro AS EPBairro, EndP.uf AS EPUf, ";
            $QryDados .= "EndP.cep AS EPCep, EndP.Tipo_Endereco_id AS EPTipo, ";
            $QryDados .= "TelefoneP.numero AS TelPNum, TelefoneP.Tipo_Telefone_id AS TelPTipo, ";
            $QryDados .= "EndPe.logradouro AS EPeLogradouro, EndPe.numero AS EPeNumero, EndPe.complemento AS EPeComplemento, EndPe.cidade AS EPeCidade, ";
            $QryDados .= "EndPe.bairro AS EPeBairro, EndPe.uf AS EPeUf, EndPe.cep AS EPeCep, EndPe.Tipo_Endereco_id AS EPeTipo ";
            $QryDados .= "FROM Cliente AS C ";
            $QryDados .= "INNER JOIN Pessoa AS P ON P.id = C.Pessoa_id ";
            $QryDados .= "INNER JOIN Cadastro AS Cad ON Cad.Cliente_id = C.id ";
            $QryDados .= "INNER JOIN Pessoa_has_Documento AS PHD1 ON PHD1.Pessoa_id = P.id ";
            $QryDados .= "INNER JOIN Documento AS RG ON RG.id = PHD1.Documento_id AND RG.Tipo_documento_id = 2 ";
            $QryDados .= "INNER JOIN Dados_Profissionais AS DP ON DP.Pessoa_id = P.id ";
            $QryDados .= "INNER JOIN Endereco AS EndP ON EndP.id = DP.Endereco_id ";
            $QryDados .= "INNER JOIN Contato_has_Telefone AS ContatoPHT ON ContatoPHT.Contato_id = DP.Contato_id ";
            $QryDados .= "INNER JOIN Telefone AS TelefoneP ON TelefoneP.id = ContatoPHT.Telefone_id ";
            $QryDados .= "INNER JOIN Pessoa_has_Endereco AS PHE ON PHE.Pessoa_id = P.id ";
            $QryDados .= "INNER JOIN Endereco AS EndPe ON EndPe.id = PHE.Endereco_id ";
            $QryDados .= "WHERE C.id = '" . $r[0]['clienteId'] . "' AND P.nascimento IS NOT NULL AND RG.data_emissao IS NOT NULL";

            /* $file = fopen('queryDados.sql', 'w');
              fwrite($file, $QryDados);
              fclose($file); */

            $dadosCliente = Yii::app()->bdSigacOne->createCommand($QryDados)->queryRow();

            if ($dadosCliente != NULL) {
                if ($dadosCliente['Nascimento'] == NULL) {
                    $dadosCliente['Nascimento'] = date('1980-01-01');
                }

                if ($dadosCliente['DE'] == NULL) {
                    $dadosCliente['DE'] = date('2014-01-01');
                }

                $Pessoa['nome'] = $dadosCliente['Nome'];
                $Pessoa['sexo'] = $dadosCliente['Sexo'];
                $Pessoa['naturalidade'] = $dadosCliente['NAT'];
                $Pessoa['naturalidade_cidade'] = $dadosCliente['NCidade'];
                $Pessoa['nacionalidade'] = $dadosCliente['NAC'];
                $Pessoa['Estado_Civil_id'] = $dadosCliente['ES'];
                $Pessoa['nascimento'] = $util->bd_date_to_view($dadosCliente['Nascimento']);

                $CPFCliente['numero'] = $cpf;
                $CPFCliente['Tipo_documento_id'] = 1;
                $CPFCliente['data_cadastro'] = date('Y-m-d H:i:s');

                $DocumentoCliente['numero'] = $dadosCliente['RGnum'];
                $DocumentoCliente['orgao_emissor'] = $dadosCliente['OE'];
                $DocumentoCliente['uf_emissor'] = $dadosCliente['UFE'];
                $DocumentoCliente['Tipo_documento_id'] = 2;
                $DocumentoCliente['data_cadastro'] = date('Y-m-d H:i:s');
                $DocumentoCliente['data_emissao'] = $util->bd_date_to_view($dadosCliente['DE']);

                $Cadastro['nome_do_pai'] = $dadosCliente['Pai'];
                $Cadastro['nome_da_mae'] = $dadosCliente['Mae'];
                $Cadastro['numero_de_dependentes'] = $dadosCliente['Dependentes'];
                $Cadastro['conjugue_compoe_renda'] = $dadosCliente['CompoeRenda'];
                $Cadastro['titular_do_cpf'] = 1;
            }

            $clienteImportado = $this->novo($Pessoa, $CPFCliente, $DocumentoCliente, $Cadastro, 1);

            if ($clienteImportado != NULL) {
                $admissao = new DateTime($dadosCliente['Data_admissao']);
                $hoje = new DateTime(date('Y-m-d'));
                $diferencaPerido = $admissao->diff($hoje);

                /*
                  $DadosProfissionais['empresa'                   ] = $dadosCliente['Empresa'             ];
                  $DadosProfissionais['Tipo_Ocupacao_id'          ] = 13;
                  $DadosProfissionais['Classe_Profissional_id'    ] = $dadosCliente['ClasseProfissional'  ];
                  $DadosProfissionais['renda_liquida'             ] = $dadosCliente['Renda'               ];
                  $DadosProfissionais['tipo_de_comprovante'       ] = $dadosCliente['TipoComprovante'     ];
                  $DadosProfissionais['profissao'                 ] = $dadosCliente['Profissao'           ];
                  $DadosProfissionais['mes_ano_renda'             ] = $dadosCliente['MesAnoRenda'         ];
                  $DadosProfissionais['orgao_pagador'             ] = $dadosCliente['OrgaoPagador'        ];
                  $DadosProfissionais['numero_do_beneficio'       ] = $dadosCliente['NumeroBeneficio'     ];
                  $DadosProfissionais['cnpj_cpf'                  ] = $dadosCliente['CnpjCpf'             ];
                  $DadosProfissionais['tempo_de_servico_meses'    ] = $diferencaPerido->m;
                  $DadosProfissionais['tempo_de_servico_anos'     ] = $diferencaPerido->y;
                  $DadosProfissionais['data_admissao'             ] = $util->bd_date_to_view($dadosCliente['Data_admissao']);

                  $EnderecoProfiss['logradouro'                   ] = $dadosCliente['EPLogradouro'        ];
                  $EnderecoProfiss['numero'                       ] = $dadosCliente['EPNumero'            ];
                  $EnderecoProfiss['complemento'                  ] = $dadosCliente['EPComplemento'       ];
                  $EnderecoProfiss['cidade'                       ] = $dadosCliente['EPCidade'            ];
                  $EnderecoProfiss['bairro'                       ] = $dadosCliente['EPBairro'            ];
                  $EnderecoProfiss['uf'                           ] = $dadosCliente['EPUf'                ];
                  $EnderecoProfiss['cep'                          ] = $dadosCliente['EPCep'               ];
                  $EnderecoProfiss['Tipo_Endereco_id'             ] = $dadosCliente['EPTipo'              ];

                  $TelefoneProfiss['numero'                       ] = str_replace(['-','(',')',' '], ['','','',''], $dadosCliente['TelPNum']);
                  $TelefoneProfiss['Tipo_Telefone_id'             ] = $dadosCliente['TelPTipo'            ];

                  if( trim( $DadosProfissionais['empresa'] ) == '' || $DadosProfissionais['empresa'] == NULL && empty( $DadosProfissionais['empresa'] ) ){
                  $DadosProfissionais['empresa']                = strtoupper( $clienteImportado->pessoa->nome );
                  }

                  DadosProfissionais::model()->persist( $DadosProfissionais, $EnderecoProfiss, $TelefoneProfiss, $clienteImportado->id, null, 'new' );
                 */

                $EnderecoPessoal['logradouro'] = $dadosCliente['EPeLogradouro'];
                $EnderecoPessoal['numero'] = $dadosCliente['EPeNumero'];
                $EnderecoPessoal['complemento'] = $dadosCliente['EPeComplemento'];
                $EnderecoPessoal['cidade'] = $dadosCliente['EPeCidade'];
                $EnderecoPessoal['bairro'] = $dadosCliente['EPeBairro'];
                $EnderecoPessoal['uf'] = $dadosCliente['EPeUf'];
                $EnderecoPessoal['cep'] = $dadosCliente['EPeCep'];
                $EnderecoPessoal['Tipo_Endereco_id'] = $dadosCliente['EPeTipo'];
                $EnderecoPessoal['endereco_de_cobranca'] = 1;
                $EnderecoPessoal['Tipo_Moradia_id'] = 3;

                Endereco::model()->novo($EnderecoPessoal, $clienteImportado->id);

                /*
                  $QryTelefones        = "SELECT * FROM Telefone AS T ";
                  $QryTelefones       .= "INNER JOIN Contato_has_Telefone AS CHT ON CHT.Telefone_id = T.id ";
                  $QryTelefones       .= "WHERE CHT.Contato_id = ".$dadosCliente['Contato'];

                  $telefonesPessoais   = Yii::app()->bdSigacOne->createCommand($QryTelefones)->queryAll();

                  foreach($telefonesPessoais as $telefone)
                  {
                  Telefone::model()->novo($telefone, $clienteImportado->id);
                  }
                 */

                $QryReferencias = "SELECT R.nome as RNome, R.parentesco as RParentesco, T.numero as TNumero FROM Referencia AS R ";
                $QryReferencias .= "INNER JOIN Contato_has_Telefone AS CHT ON CHT.Contato_id = R.Contato_id ";
                $QryReferencias .= "INNER JOIN Telefone AS T ON T.id = CHT.Telefone_id ";
                $QryReferencias .= "WHERE R.Cliente_id = " . $dadosCliente['ClienteId'];

                $referencias = Yii::app()->bdSigacOne->createCommand($QryReferencias)->queryAll();

                foreach ($referencias as $referencia) {
                    $DadosReferencia['parentesco'] = $referencia['RParentesco'];
                    $DadosReferencia['nome'] = $referencia['RNome'];
                    $DadosReferencia['Tipo_Referencia_id'] = 1;
                    $TelefoneReferencia['numero'] = str_replace(['(', ')', '-', ' '], ['', '', '', ''], $referencia['TNumero']);
                    $TelefoneReferencia['Tipo_Telefone_id'] = 3;

                    Referencia::model()->novo($DadosReferencia, $TelefoneReferencia, $clienteImportado->id);
                }
            }

            return $clienteImportado;
        } else {
            return NULL;
        }
    }

    public function getCadastro() {
        return Cadastro::model()->find('habilitado AND Cliente_id = ' . $this->id);
    }

    public function ficharCliente($Fichamento, $FichamentoHasStatusFichamento, $Operacao, $ComprovanteFile) {

        $arrReturn                                  = ['hasErrors' => 0];

        $configuracoes                              = SigacConfig::model()->findByPk(1);

        if ($configuracoes->auth_fichamento != $Operacao['senha'])
        {
            $arrReturn['hasErrors'] = 1;

            $arrReturn['msgConfig']['pnotify'][]    = array(
                'titulo'                            => 'Não foi possível concluir o fichamento!',
                'texto'                             => 'Senha de permissão inválida.',
                'tipo'                              => 'error',
            );
        }
        //f1ch@m3nt0
        else
        {
            $nFichamento                            = Fichamento::model()->novo($Fichamento);

            if ($nFichamento                        != NULL)
            {
                $nfichamentoHasStatusFichamento     = FichamentoHasStatusFichamento::model()->novo($nFichamento, $FichamentoHasStatusFichamento, 2);

                /*$anexo = Anexo::model()->novo($ComprovanteFile, 'CDF', $nFichamento->id, 'Comprovante de Fichamento | Fichamento ' . $nFichamento->id);*/
                $s3Client   = new S3Client;

                $anexo      = $s3Client->put($ComprovanteFile, 'CDF', $nFichamento->id, 'Comprovante de Fichamento | Fichamento ' . $nFichamento->id);

                if ($nfichamentoHasStatusFichamento != NULL && $anexo != null)
                {
                    $arrReturn['msgConfig']['pnotify'][]    = array(
                        'titulo'                            => 'Fichamento realizado com sucesso!',
                        'texto'                             => 'Operação realizada com sucesso.',
                        'tipo'                              => 'success',
                    );
                }

                else
                {
                    $arrReturn['hasErrors']                 = 1;

                    $arrReturn['msgConfig']['pnotify'][]    = array(
                        'titulo'                            => 'Não foi possível realizar a operação!1',
                        'texto'                             => 'Entre em contato com o suporte.',
                        'tipo'                              => 'error',
                    );
                }
            }

            else
            {
                $arrReturn['hasErrors']                     = 1;

                $arrReturn['msgConfig']['pnotify'][]        = array(
                    'titulo'                                => 'Não foi possível realizar a operação!2',
                    'texto'                                 => 'Entre em contato com o suporte.',
                    'tipo'                                  => 'error',
                );
            }
        }

        return $arrReturn;
    }

    public static function model($className = __CLASS__) {

        return parent::model($className);
    }

    public function getSaldoDevedor($idCartao)
    {

        //lembrar de ver a regra por fatura abertas e compras em aberto

        $codigoNaturezaAReceber = "10101";

        $valorRetorno = 0;

        $vendas = VendaHasFormaDePagamentoHasCondicaoDePagamento::model()->findAll("habilitado AND Cartao_id = $idCartao");

        $naturezaReceber = NaturezaTitulo::model()->find("habilitado AND codigo = '$codigoNaturezaAReceber' ");

        foreach ($vendas as $venda)
        {

            if($naturezaReceber !== null)
            {

                $titulos = Titulo::model()->findAll("habilitado AND NaturezaTitulo_id = $naturezaReceber->id");

                foreach ($titulos as $titulo)
                {
                    $parcelas = Parcela::model()->findAll("habilitado AND Titulo_id = $titulo");

                    foreach ($parcelas as $parcela)
                    {

                        $baixas = Baixa::model()->findAll("habilitado AND Parcela_id = $parcela->id");

                        $valorAbatimento = 0;

                        foreach ($baixas as $baixa)
                        {
                            $valorAbatimento += $baixa->valor_abatimento;
                        }

                        $valorRetorno += ($parcela->valor - $valorAbatimento);

                    }

                }

            }

        }

        return $valorRetorno;

    }

    public function numeroPassagensDia($data_de, $data_ate)
    {

        $criteria               = new CDbCriteria;

        $criteria->addBetweenCondition('t.data_cadastro',  $data_de.' 00:00:00', $data_ate.' 23:59:59', 'AND');
        $criteria->addInCondition('t.Cliente_id', [$this->id], 'AND');
        $criteria->addInCondition('t.habilitado', [1], 'AND');

        return AnaliseDeCredito::model()->findAll($criteria);
    }

    /////////////////////////////////
    // funcao   : temVendaAprovada //
    ///////////////////////////////
    // autor    :
    public function temVendaAprovada()
    {

        $retorno    = false ;
        $cpf        = ""    ;

        if($this->pessoa->getCPF() !== null)
        {
            $cpf = $this->pessoa->getCPF()->numero;
        }

        if($cpf !== "")
        {

            $sql    = " SELECT SUM(QTD) AS QTDTOT
                        FROM (
                                SELECT COUNT(*)                 AS QTD
                                FROM         Proposta           AS P
                                INNER JOIN   Analise_de_Credito AS AC   ON AC.habilitado AND P.Analise_de_Credito_id = AC.id
                                WHERE P.habilitado AND AC.Cliente_id = $this->id AND P.Status_Proposta_id = 2

                                UNION

                                SELECT COUNT(*) AS QTD
                                FROM beta.Proposta 						AS P2
                                INNER JOIN beta.Analise_de_Credito 		AS AC2 ON AC2.habilitado AND 	 P2.Analise_de_Credito_id 	= AC2.id
                                INNER JOIN beta.Cliente					AS  C2 ON  C2.habilitado AND 	AC2.Cliente_id				=  C2.id
                                INNER JOIN beta.Pessoa					AS Pe2 ON Pe2.habilitado AND 	 C2.Pessoa_id				= Pe2.id
                                INNER JOIN beta.Pessoa_has_Documento 	AS PD2 ON PD2.habilitado AND 	PD2.Pessoa_id				= Pe2.id
                                INNER JOIN beta.Documento				AS  D2 ON  D2.habilitado AND	PD2.Documento_id			=  D2.id
                                WHERE P2.habilitado AND D2.numero = '" . $cpf . "' AND P2.Status_Proposta_id = 2
                        ) AS TMP";

            $resultado = Yii::app()->db->createCommand($sql)->queryRow();

            if($resultado["QTDTOT"] > 0)
            {
                $retorno = true;
            }

        }

        return $retorno;

    }

}
