<?php

/**
 * This is the model class for table "NegacaoDeCredito".
 *
 * The followings are the available columns in table 'NegacaoDeCredito':
 * @property integer $id
 * @property string $observacao
 * @property integer $MotivoDeNegacao
 * @property integer $Proposta
 * @property integer $Analista
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property MotivoDeNegacao $motivoDeNegacao
 * @property Proposta $proposta
 * @property Usuario $analista
 */
class NegacaoDeCredito extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'NegacaoDeCredito';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('observacao, MotivoDeNegacao, Proposta, Analista', 'required'),
			array('MotivoDeNegacao, Proposta, Analista, habilitado', 'numerical', 'integerOnly'=>true),
			array('observacao', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, observacao, MotivoDeNegacao, Proposta, Analista, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'motivoDeNegacao' => array(self::BELONGS_TO, 'MotivoDeNegacao', 'MotivoDeNegacao'),
			'proposta' => array(self::BELONGS_TO, 'Proposta', 'Proposta'),
			'analista' => array(self::BELONGS_TO, 'Usuario', 'Analista'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'observacao' => 'Observacao',
			'MotivoDeNegacao' => 'Motivo De Negacao',
			'Proposta' => 'Proposta',
			'Analista' => 'Analista',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('observacao',$this->observacao,true);
		$criteria->compare('MotivoDeNegacao',$this->MotivoDeNegacao);
		$criteria->compare('Proposta',$this->Proposta);
		$criteria->compare('Analista',$this->Analista);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return NegacaoDeCredito the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
