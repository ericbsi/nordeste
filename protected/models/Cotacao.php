<?php

/**
 * This is the model class for table "Cotacao".
 *
 * The followings are the available columns in table 'Cotacao':
 * @property integer $id
 * @property double $taxa
 * @property integer $dias_carencia
 * @property string $descricao
 * @property string $data_cadastro
 * @property integer $habilitado
 * @property double $valor_de
 * @property double $valor_ate
 *
 * The followings are the available model relations:
 * @property Filial $filial
 */
class Cotacao extends CActiveRecord{

	private $connection;


	private function setConn(){

		$this->connection = new CDbConnection(
			Yii::app()->params['dbconf']['dns'],
			Yii::app()->params['dbconf']['user'],
			Yii::app()->params['dbconf']['pass']
		);

	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName(){
		
		return 'Cotacao';

	}

	public function rules(){
		
		return array(
			array('habilitado, Empresa_id, parcela_inicio, parcela_fim', 'numerical', 'integerOnly'=>true),
			array('taxa', 'numerical'),
			array('descricao', 'length', 'max'=>100),
			array('data_cadastro, data_cadastro_br, Empresa_id', 'safe'),
			
			array('id,parcela_fim, Empresa_id, parcela_inicio, taxa, descricao, data_cadastro, data_cadastro_br, habilitado', 'safe', 'on'=>'search'),
		);

	}

	public function taxaToView(){

		return number_format($this->taxa, 2, ",", ".");

	}

	public function relations(){
		
		return array(
			'empresaId' => array(self::BELONGS_TO, 'Empresa', 'Empresa_id'),
		);

	}

	public function attributeLabels(){

		return array(
			'id' => 'ID',
			'taxa' => 'Taxa',
			'descricao' => 'Descricao',
			'data_cadastro' => 'Data Cadastro',
			'data_cadastro_br' => 'Data / Hora Cadastro',
			'habilitado' => 'Habilitado',
			'parcela_fim' => 'Parcela Fim',
			'parcela_inicio' => 'Parcela Início',
			'Empresa_id' => 'Empresa',
		);

	}

	public function cotacoesPGD(){

		$arrCotacoesIds     = array()           ;
		$criteria           = new CDbCriteria   ;

		$criteria->addInCondition("id", [77]);

		return $criteria;
	}

	public function filialCotacoes( $filial )
	{
		$arrCotacoesIds     = array()           ;
		$criteria           = new CDbCriteria   ;
                
                if(isset($filial) && $filial !== null)
                {
                
                    $filialHasCotacao   = FilialHasTabelaCotacao::model()->findAll(
                                            'Filial_id = ' . $filial->id . ' AND habilitado'
                            );

                    foreach ( $filialHasCotacao as $fhc ) {

                            if ( !in_array($fhc->Tabela_Cotacao_id, $arrCotacoesIds) )
                              array_push($arrCotacoesIds, $fhc->Tabela_Cotacao_id );
                    }

                    $criteria->addInCondition("id", $arrCotacoesIds);
                }
                else
                {
                    $criteria->condition = "0";
                }
                
		return $criteria;
	}

	//atualizar
	public function findByFilial( $filial ){

		$arrCotacoesIds = array();

		$criteria = new CDbCriteria;

		$filialHasCotacao = FilialHasCotacao::model()->findAll('Filial_id = ' . $filial->id . ' AND habilitado');

		foreach ( $filialHasCotacao as $fhc ) {
			
			if ( !in_array($fhc->Cotacao_id, $arrCotacoesIds) )
			  array_push($arrCotacoesIds, $fhc->Cotacao_id );
		}
		
		$criteria->addInCondition("id", $arrCotacoesIds);
		
		return $criteria;
	}

	public function search(){

		$criteria = new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('taxa',$this->taxa);
		$criteria->compare('descricao',$this->descricao,true);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('data_cadastro_br',$this->data_cadastro_br,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('parcela_inicio',$this->parcela_inicio);
		$criteria->compare('parcela_fim',$this->parcela_fim);
		$criteria->compare('Empresa_id',$this->Empresa_id);

		/*$criteria->with[]='filial';
		$criteria->addSearchCondition("filial.nome_fantasia",$this->id);*/
	
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function valorPosCarencia( $valor, $prazo, $taxa ){

		$taxaAux2 = $taxa/100;

		return $valor * pow( (1 + $taxaAux2), $prazo );
		 
	}

	public function returnValParcela( $valor, $seqParcela, $carencia ){

		if( $seqParcela > 5 ){

			$carenciaAux = ($carencia-30)/30;

			$valorAux = $this->valorPosCarencia($valor,$carenciaAux, $this->taxa);

			$taxaAux = $this->taxa / 100;

			$fator = pow(1 + $taxaAux, $seqParcela);		
			$fator = 1 - ( 1 / $fator );
			$fator = $taxaAux / $fator;
			
			return array(
				number_format($valorAux * $fator, 2, ".", ""),
				number_format($valorAux * $fator, 2, ",", ".")
			);
		}
		
		else
		{
			return array(
				number_format($valor/$seqParcela, 2, ".", ""),
				number_format($valor/$seqParcela, 2, ",", ".")
			);
		}
	}

	public function financeirasAssociadas( $type ){

		//$this->setConn();
		$finNaoAssociadas = array();
		$sql = "";

		if ( $type == 'in' ) {	

			$sql = "select f.id, f.nome, f.cnpj from `Financeira` as f\n"
			    . "	inner join `Cotacao_has_Financeira` as cf on f.id = cf.Financeira_id\n"
			    . "	inner join `Cotacao` as c on cf.Cotacao_id = c.id\n"
			    . " where c.id = " . $this->id;
		}

		else{

			$sql = "select f.id, f.nome, f.cnpj from `Financeira` as f\n"
			    . "	left outer join `Cotacao_has_Financeira` as cf on f.id = cf.Financeira_id and cf.Cotacao_id = " .$this->id."\n"
			    . "	where cf.id is null";
		}

		$finAssociadas = Yii::app()->db->createCommand($sql)->queryAll();
		return $finAssociadas;
	}


	public function listFinanceiras(){

		return CotacaoHasFinanceira::model()->findAll('Cotacao_id = ' . $this->id);

	}

	public static function model($className=__CLASS__){

		return parent::model($className);

	}
}
