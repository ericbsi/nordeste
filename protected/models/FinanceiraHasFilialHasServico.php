<?php

/**
 * This is the model class for table "Financeira_has_Filial_has_Servico".
 *
 * The followings are the available columns in table 'Financeira_has_Filial_has_Servico':
 * @property integer $Filial_has_Servico_id
 * @property integer $id
 * @property integer $habilitado
 * @property string $data_cadastro
 * @property integer $ordem
 * @property integer $Financeira_id
 *
 * The followings are the available model relations:
 * @property FilialHasServico $filialHasServico
 * @property Financeira $financeira
 */
class FinanceiraHasFilialHasServico extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Financeira_has_Filial_has_Servico';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Filial_has_Servico_id, habilitado, data_cadastro, ordem, Financeira_id', 'required'),
			array('Filial_has_Servico_id, habilitado, ordem, Financeira_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Filial_has_Servico_id, id, habilitado, data_cadastro, ordem, Financeira_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'filialHasServico' => array(self::BELONGS_TO, 'FilialHasServico', 'Filial_has_Servico_id'),
			'financeira' => array(self::BELONGS_TO, 'Financeira', 'Financeira_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Filial_has_Servico_id' => 'Filial Has Servico',
			'id' => 'ID',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'ordem' => 'Ordem',
			'Financeira_id' => 'Financeira',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Filial_has_Servico_id',$this->Filial_has_Servico_id);
		$criteria->compare('id',$this->id);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('ordem',$this->ordem);
		$criteria->compare('Financeira_id',$this->Financeira_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FinanceiraHasFilialHasServico the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
