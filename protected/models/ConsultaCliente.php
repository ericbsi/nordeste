<?php

class ConsultaCliente {

    public function testeWS($cgc, $ambiente = 1) {

        $this->comunicaWS($cgc, $ambiente);

        if ($this->retorno != null) {

            $objXML = simplexml_load_string($this->retorno);

            $objXML->registerXPathNamespace('S', 'http://schemas.xmlsoap.org/soap/envelope/');
            $objXML->registerXPathNamespace('ns2', 'http://webservice.consulta.spcjava.spcbrasil.org/');

            $resultado = $objXML->xpath("//ns2:resultado");

            return $resultado;
        }
    }

    public function listarProdutos() {
        $this->comunicaWS("1", 2, "141", "78");


        if ($this->retorno !== null) {

            $objXML = simplexml_load_string($this->retorno);

            $objXML->registerXPathNamespace('S', 'http://schemas.xmlsoap.org/soap/envelope/');
            $objXML->registerXPathNamespace('ns2', 'http://webservice.consulta.spcjava.spcbrasil.org/');

            $resultado = $objXML->xpath("//ns2:resultado");

            $this->resultado = $resultado;
        }
    }

    public function consultarScore($cgc, $ambiente = 1, $id_cli) {

        $cliente                                = Cliente::model()->find('habilitado AND id = ' . $id_cli);

        if ($cliente != null)
        {   
            /*Verifica se o cliente já possui ficha*/
            $ficha                              = Ficha::model()->find('habilitado AND Pessoa_id = ' . $cliente->pessoa->id);

            /*Se possuir ficha...*/
            if ($ficha != null)
            {
                /*A ficha existente ainda não expirou*/
                if ($ficha->data_expiracao > Date('Y-m-d'))
                {
                    $this->restricao            = (string) $ficha->restricao;
                    $this->probabilidade        = (float) $ficha->risco;
                    $this->classe               = (string) $ficha->classe;
                    $this->score                = (int) $ficha->score;
                    $this->procedencia          = "FICHA";
                }

                /*A ficha existente está com data expirada*/
                else
                {

                    $transaction                = Yii::app()->db->beginTransaction();
                    
                    /*Desabilitando a ficha vencida*/
                    $ficha->habilitado          = 0;
                    
                    if ($ficha->update())
                    {   
                        /*Ficha com nova consulta de score*/
                        $nficha                 = new Ficha;
                        $this->comunicaWS($cgc, $ambiente, "141", "78");

                        if ($this->retorno      !== null)
                        {
                            $objXML             = simplexml_load_string($this->retorno);

                            $objXML->registerXPathNamespace('S', 'http://schemas.xmlsoap.org/soap/envelope/');
                            $objXML->registerXPathNamespace('ns2', 'http://webservice.consulta.spcjava.spcbrasil.org/');

                            $resultado                  = $objXML->xpath("//ns2:resultado");

                            if(isset($resultado[0]))
                            {
                                $score                  = $resultado[0]->xpath("spc-score-12-meses/detalhe-spc-score-12-meses");

                                $this->resultado        = $resultado;

                                $this->restricao        = (string) $resultado[0]["restricao"];

                                $this->probabilidade    = (float) $score[0]["probabilidade"];
                                $this->classe           = (string) $score[0]["classe"];
                                $this->score            = (int) $score[0]["score"];
                                $this->procedencia      = "CONSULTA";
                                $nficha->Pessoa_id      = $cliente->pessoa->id;
                                $nficha->score          = $this->score;
                                $nficha->restricao      = $this->restricao;
                                $nficha->classe         = $this->classe;
                                $nficha->risco          = $this->probabilidade;
                                $nficha->habilitado     = 1;
                                $nficha->data_cadastro  = Date('Y-m-d H:i:s');
                                $nficha->data_expiracao = Date('Y-m-d', strtotime('+10 days')); //pegar esse valor por parametro

                                if ($nficha->save())
                                {
                                    $transaction->commit();
                                }
                                else
                                {
                                    $transaction->rollBack();
                                }

                            }
                            else
                            {
                                $this->probabilidade    = "OFF";
                                $this->classe           = "OFF";
                                $this->score            = "OFF";
                                $this->procedencia      = "OFF";
                            }

                        }
                        else 
                        {
                            $transaction->rollBack();
                        }

                    }
                    else
                    {
                        $transaction->rollBack();
                    }
                }
            }
            else
            {
                $transaction                        = Yii::app()->db->beginTransaction();
                $nficha                             = new Ficha;
                $this->comunicaWS($cgc, $ambiente, "141", "78");

                if ($this->retorno                  !== null) {

                    $objXML                         = simplexml_load_string($this->retorno);

                    $objXML->registerXPathNamespace('S', 'http://schemas.xmlsoap.org/soap/envelope/');
                    $objXML->registerXPathNamespace('ns2', 'http://webservice.consulta.spcjava.spcbrasil.org/');

                    $resultado                      = $objXML->xpath("//ns2:resultado");

                    if(isset($resultado[0])) {
                        $score                      = $resultado[0]->xpath("spc-score-12-meses/detalhe-spc-score-12-meses");

                        $this->resultado            = $resultado;

                        $this->restricao            = (string) $resultado[0]["restricao"];

                        $this->probabilidade        = (float) $score[0]["probabilidade"];
                        $this->classe               = (string) $score[0]["classe"];
                        $this->score                = (int) $score[0]["score"];
                        $this->procedencia          = "CONSULTA";
                        $nficha->Pessoa_id          = $cliente->pessoa->id;
                        $nficha->score              = $this->score;
                        $nficha->restricao          = $this->restricao;
                        $nficha->classe             = $this->classe;
                        $nficha->risco              = $this->probabilidade;
                        $nficha->habilitado         = 1;
                        $nficha->data_cadastro      = Date('Y-m-d H:i:s');
                        $nficha->data_expiracao     = Date('Y-m-d', strtotime('+10 days')); //pegar esse valor por parametro

                        if ($nficha->save())
                        {
                            $transaction->commit();
                        }

                        else
                        {
                            $transaction->rollBack();
                        }
                    }
                    else
                    {
                        $this->probabilidade        = "OFF";
                        $this->classe               = "OFF";
                        $this->score                = "OFF";
                        $this->restricao            = "OFF";
                        $this->procedencia          = "OFF";
                    }
                }
            }
        }
    }

    /*
    public function consultar($cgc, $ambiente = 1) {

        $this->comunicaWS($cgc, $ambiente, "308", "55");

        $this->HTMLSPC = '';
        $this->HTMLSerasa = '';
        $this->HTMLConsulta = '';
        $this->consultaDetalhes = '';

        if ($this->retorno !== null) {

            $objXML = simplexml_load_string($this->retorno);

            $namespaces = $objXML->getNamespaces(true);

            $objXML->registerXPathNamespace('S', 'http://schemas.xmlsoap.org/soap/envelope/');
            $objXML->registerXPathNamespace('ns2', 'http://webservice.consulta.spcjava.spcbrasil.org/');

            $resultado = $objXML->xpath("//ns2:resultado");
            $consumidor = $resultado[0]->xpath('consumidor/consumidor-pessoa-fisica');

            $telefones = $resultado[0]->xpath('ultimo-telefone-informado');
            $telefonesDetalhes = $resultado[0]->xpath('ultimo-telefone-informado/detalhe-ultimo-telefone-informado');

            $enderecosDetalhes = $resultado[0]->xpath('ultimo-endereco-informado/detalhe-ultimo-endereco-informado');

            $spcDetalhes = $resultado[0]->xpath('spc/detalhe-spc');

            $serasa = $resultado[0]->xpath('pendencia-financeira');
            $serasaDetalhes = $resultado[0]->xpath('pendencia-financeira/detalhe-pendencia-financeira');

            $poderJudiciario = $resultado[0]->xpath('informacao-poder-judiciario');

            $consultas = $resultado[0]->xpath('consulta-realizada');
            $consultasDetalhes = $resultado[0]->xpath('consulta-realizada/detalhe-consulta-realizada');

            $alertas = $resultado[0]->xpath('alerta-documento');
            $alertasDetalhes = $resultado[0]->xpath('alerta-documento/detalhe-alerta-documento');

            $dateTime = new DateTime(substr($consumidor[0]['data-nascimento'], 0, 10));
            $dataNascimento = $dateTime->format('d/m/Y');

            $this->restricao = ( $resultado [0] ['restricao'] == "true") ? "Sim" : "Não";
            $this->protocolo = $resultado [0]->protocolo ['numero'] . '-' .
                $resultado [0]->protocolo ['digito'];
            $this->nomeConsumidor = (string) $consumidor [0] ['nome'];
            $this->maeConsumidor = (string) $consumidor [0] ['nome-mae'];
            $this->cgcConsumidor = (string) $consumidor [0]->cpf ['numero'];
            $this->nomeOperador = (string) $resultado [0]->operador ['nome'];
            $this->nascimentoConsumidor = $dataNascimento;

            if (sizeof($enderecosDetalhes)) {
                $this->logradouroConsumidor = (string) $enderecosDetalhes [0]->endereco ['logradouro'];
                $this->numEndConsumidor = (string) $enderecosDetalhes [0]->endereco ['numero'];
                $this->bairroConsumidor = (string) $enderecosDetalhes [0]->endereco ['bairro'];
                $this->comEndConsumidor = (string) $enderecosDetalhes [0]->endereco ['complemento'];
                $this->cidadeConsumidor = (string) $enderecosDetalhes [0]->endereco->cidade ['nome'];
                $this->ufConsumidor = (string) $enderecosDetalhes [0]->endereco->cidade->estado ['sigla-uf'];
                $this->cepConsumidor = (string) $enderecosDetalhes [0]->endereco ['cep'];
            } else {
                $this->logradouroConsumidor = '';
                $this->numEndConsumidor = '';
                $this->bairroConsumidor = '';
                $this->comEndConsumidor = '';
                $this->cidadeConsumidor = '';
                $this->ufConsumidor = '';
                $this->cepConsumidor = '';
            }

             $this->qtdDadosTelefonicos    = 0;
              $this->ultDadosTelefonicos    = '';
              $this->vlrDadosTelefonicos    = ''; 

            $this->qtdTelefoneVinculado = sizeof($telefones[0]);

            if (sizeof($telefones[0]) > 0) {
                $dateTime = new DateTime(substr($telefonesDetalhes[0]['data-ultima-consulta'], 0, 10));
                $dataUltTelefone = $dateTime->format('d/m/Y');

                $this->ultTelefoneVinculado = $dataUltTelefone;
                $this->vlrTelefoneVinculado = '(' . $telefonesDetalhes[0]->telefone['numero-ddd'] . ') ' . $telefonesDetalhes[0]->telefone['numero'];
            } else {
                $this->ultTelefoneVinculado = '';
                $this->vlrTelefoneVinculado = '';
            }

            $this->qtdRegistroSPC = (string) $resultado[0]->spc->resumo['quantidade-total'];

            if (sizeof($spcDetalhes) > 0) {
                $dateTime = new DateTime(substr($resultado[0]->spc->resumo['data-ultima-ocorrencia'], 0, 10));
                $dataUltRegSPC = $dateTime->format('d/m/Y');

                $this->ultRegistroSPC = $dataUltRegSPC;
                $this->vlrRegistroSPC = (string) $resultado[0]->spc->resumo['valor-ultima-ocorrencia'];

                $this->setHTMLSPC($spcDetalhes);
            } else {
                $this->ultRegistroSPC = '';
                $this->vlrRegistroSPC = '';
            }

            $this->qtdRegistroSerasa = (string) $serasa[0]->resumo['quantidade-total'];

            if (sizeof($serasaDetalhes) > 0) {
                $dateTime = new DateTime(substr($serasa[0]->resumo['data-ultima-ocorrencia'], 0, 10));
                $dataUltRegSerasa = $dateTime->format('d/m/Y');

                $this->ultRegistroSerasa = $dataUltRegSerasa;
                $this->vlrRegistroSerasa = (string) $serasa[0]->resumo['valor-ultima-ocorrencia'];

                $this->setHTMLSerasa($serasaDetalhes);
            } else {
                $this->ultRegistroSerasa = '';
                $this->vlrRegistroSerasa = '';
            }

            $this->qtdPoderJudiciario = (string) $poderJudiciario[0]->resumo['quantidade-total'];

            if ((int) $this->qtdPoderJudiciario > 0) {
                        $dateTime    = new DateTime(substr($resultado[0]->spc->resumo['data-ultima-ocorrencia'],0,10));
                  $dataUltPJud = $dateTime->format('d/m/Y'); 

                $this->ultPoderJudiciario = 'Indefinido';
                $this->vlrPoderJudiciario = 'Indefinido';
            } else {
                $this->ultPoderJudiciario = '';
                $this->vlrPoderJudiciario = '';
            }

            $this->qtdConsultaRealizada = (string) $consultas[0]->resumo['quantidade-total'];

            if (sizeof($consultasDetalhes) > 0) {
                $dateTime = new DateTime(substr($consultas[0]->resumo['data-ultima-ocorrencia'], 0, 10));
                $dataUltConsulta = $dateTime->format('d/m/Y');

                $this->ultConsultaRealizada = $dataUltConsulta;
                $this->vlrConsultaRealizada = 'Nos últimos ' . $consultas[0]['quantidade-dias-consultados'] . ' dias';

                $this->setHTMLConsulta($consultasDetalhes);
                $this->consultaDetalhes = $consultasDetalhes;
            } else {
                $this->ultConsultaRealizada = '';
                $this->vlrConsultaRealizada = '';
            }

            $this->qtdAlertaDocumentos = (string) $alertas[0]->resumo['quantidade-total'];

            if (sizeof($alertasDetalhes) > 0) {
                $dateTime = new DateTime(substr($alertas[0]->resumo['data-ultima-ocorrencia'], 0, 10));
                $dataUltAlerta = $dateTime->format('d/m/Y');

                $this->ultAlertaDocumentos = $dataUltAlerta;
            } else {

                $this->ultAlertaDocumentos = '';
            }

            $this->vlrAlertaDocumentos = '';
            
              $this->qtdCreditoConcedido
              $this->ultCreditoConcedido
              $this->vlrCreditoConcedido
             
              $this->qtdContraOrdem
              $this->ultContraOrdem
              $this->vlrContraOrdem

              $this->qtdCODocDif
              $this->ultCODocDif
              $this->vlrCODocDif 
        } else {
            
        }
    }
    */

    public function comunicaWS($cgc, $ambiente, $codigoProduto, $insumoOpicional) {

        $this->montaXML($cgc, $codigoProduto, $insumoOpicional, $ambiente);
        //$this->montaXML2();

        $this->setUsuarioWS($ambiente);

        $autorizacao = 'Basic ' . base64_encode($this->usuarioSenha);

        $this->setHeadersWS($autorizacao, strlen($this->xml));

        if ($ambiente == 1) {
            $ch = curl_init('https://servicos.spc.org.br/spc/remoting/ws/consulta/consultaWebService?WSDL');
        } else {
            $ch = curl_init('https://treina.spc.org.br/spc/remoting/ws/consulta/consultaWebService?WSDL');
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->xml);

        try {
            $this->retorno = curl_exec($ch);
        } catch (Exception $ex) {
            $this->retorno = null;
        }


        //      $file = fopen('testeResposta', 'w');
        //      fwrite($file, $this->retorno);
        //      fclose($file);
    }

    public function montaXML($cgc, $codigoProduto, $insumoOpicional, $ambiente = 1) {

        if ($ambiente !== 1 && $codigoProduto == "141") {
            $codigoProduto = "175";
        }

        $this->xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://webservice.consulta.spcjava.spcbrasil.org/">
                <soapenv:Header/>
                <soapenv:Body>
                  <web:filtro>
                    <codigo-produto>' . $codigoProduto . '</codigo-produto>
                    <tipo-consumidor>F</tipo-consumidor>
                    <documento-consumidor>' . trim($cgc) . '</documento-consumidor>
                    <codigo-insumo-opcional>' . $insumoOpicional . '</codigo-insumo-opcional>
                  </web:filtro>
                </soapenv:Body>
              </soapenv:Envelope>';

        //$file = fopen('testeRequisicao', 'w');
        //fwrite($file, $this->xml);
        //fclose($file);
    }

    public function montaXML2() {
        $this->xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://webservice.consulta.spcjava.spcbrasil.org/">
               <soapenv:Header/>
               <soapenv:Body>
               </soapenv:Body>
             </soapenv:Envelope>';

        //$file = fopen('testeRequisicao', 'w');
        //fwrite($file, $this->xml);
        //fclose($file);
    }

    public function setUsuarioWS($ambiente) {

        if ($ambiente == 1) {
            $this->usuarioSenha = "1628476:32084269";
        } else {
            $this->usuarioSenha = "395165:27072015";
        }
    }

    public function setHeadersWS($autorizacao, $tamanho) {
        $this->headers = array(
                'Content-Type:text/xml;charset=UTF-8',
                'Connection: close',
                'Authorization: ' . $autorizacao, // <---,
                'Content-length: ' . $tamanho
        );
    }

    public function setHTMLSPC($detalhesSPC) {

        $this->HTMLSPC = '';

        foreach ($detalhesSPC as $dSPC) {
            $dateTime = new DateTime($dSPC['data-inclusao']);
            $dataInclusao = $dateTime->format('d/m/Y');

            $dateTime = new DateTime(substr($dSPC['data-vencimento'], 0, 10));
            $dataVencimento = $dateTime->format('d/m/Y');

            $cidadeAssociado = $dSPC->xpath('cidade-associado');

            $this->HTMLSPC .= '<tr>';
            $this->HTMLSPC .= '  <td>';
            $this->HTMLSPC .= $dataInclusao;
            $this->HTMLSPC .= '  </td>';
            $this->HTMLSPC .= '  <td>';
            $this->HTMLSPC .= $dataVencimento;
            $this->HTMLSPC .= '  </td>';
            $this->HTMLSPC .= '  <td>';
            $this->HTMLSPC .= $dSPC['contrato'];
            $this->HTMLSPC .= '  </td>';
            $this->HTMLSPC .= '  <td>';
            $this->HTMLSPC .= $dSPC['comprador-fiador-avalista'];
            $this->HTMLSPC .= '  </td>';
            $this->HTMLSPC .= '  <td>';
            $this->HTMLSPC .= $dSPC['valor'];
            $this->HTMLSPC .= '  </td>';
            $this->HTMLSPC .= '  <td>';
            $this->HTMLSPC .= $dSPC['nome-associado'];
            $this->HTMLSPC .= '  </td>';
            $this->HTMLSPC .= '  <td>';
            $this->HTMLSPC .= (sizeof($cidadeAssociado) > 0) ? $cidadeAssociado[0]['nome'] . '/' . $cidadeAssociado[0]->estado['sigla-uf'] : '';
            $this->HTMLSPC .= '  </td>';
            $this->HTMLSPC .= '  <td>';
            $this->HTMLSPC .= $dSPC['nome-entidade'];
            $this->HTMLSPC .= '  </td>';
            $this->HTMLSPC .= '</tr>';
        }
    }

    public function setHTMLSerasa($detalhesSerasa) {

        //echo '<br><br>';
        //var_dump($detalhesSerasa);

        $this->HTMLSerasa = '';

        foreach ($detalhesSerasa as $dSerasa) {
            $dateTime = new DateTime(substr($dSerasa['data-ocorrencia'], 0, 10));
            $dataInclusao = $dateTime->format('d/m/Y');

            /* $dateTime         = new DateTime(substr($dSerasa['data-vencimento'],0,10));
              $dataVencimento   = $dateTime->format('d/m/Y'); */

            $cidadeAssociado = $dSerasa->xpath('cidade');

            $this->HTMLSerasa .= '<tr>';
            $this->HTMLSerasa .= '  <td>';
            $this->HTMLSerasa .= $dataInclusao;
            $this->HTMLSerasa .= '  </td>';
            $this->HTMLSerasa .= '  <td>';
            $this->HTMLSerasa .= ' - ';
            $this->HTMLSerasa .= '  </td>';
            $this->HTMLSerasa .= '  <td>';
            $this->HTMLSerasa .= $dSerasa['contrato'];
            $this->HTMLSerasa .= '  </td>';
            $this->HTMLSerasa .= '  <td>';
            $this->HTMLSerasa .= (($dSerasa['avalista'] == "true") ? "Sim" : "Não");
            $this->HTMLSerasa .= '  </td>';
            $this->HTMLSerasa .= '  <td>';
            $this->HTMLSerasa .= $dSerasa['valor-pendencia'];
            $this->HTMLSerasa .= '  </td>';
            $this->HTMLSerasa .= '  <td>';
            $this->HTMLSerasa .= $dSerasa['origem'];
            $this->HTMLSerasa .= '  </td>';
            $this->HTMLSerasa .= '  <td>';
            $this->HTMLSerasa .= $dSerasa['titulo-ocorrencia'];
            $this->HTMLSerasa .= '  </td>';
            $this->HTMLSerasa .= '  <td>';
            $this->HTMLSerasa .= (sizeof($cidadeAssociado) > 0) ? $cidadeAssociado[0]['nome'] . '/' . $cidadeAssociado[0]->estado['sigla-uf'] : '';
            $this->HTMLSerasa .= '  </td>';
            $this->HTMLSerasa .= '</tr>';
        }
    }

    public function setHTMLConsulta($consultaDetalhes) {

        $this->HTMLConsulta = '';

        foreach ($consultaDetalhes as $dConsulta) {
            $dateTime = new DateTime(substr($dConsulta->resumo['data-consulta'], 0, 10));
            $dataConsulta = $dateTime->format('d/m/Y');

            $cidadeAssociado = $dConsulta->xpath('origem-associado');

            $this->HTMLConsulta .= '<tr>';
            $this->HTMLConsulta .= '  <td>';
            $this->HTMLConsulta .= $dataConsulta;
            $this->HTMLConsulta .= '  </td>';
            $this->HTMLConsulta .= '  <td>';
            $this->HTMLConsulta .= $dConsulta['nome-associado'];
            $this->HTMLConsulta .= '  </td>';
            $this->HTMLConsulta .= '  <td>';
            $this->HTMLConsulta .= (sizeof($cidadeAssociado) > 0) ? $cidadeAssociado[0]['nome'] . '/' . $cidadeAssociado[0]->estado['sigla-uf'] : '';
            $this->HTMLConsulta .= '  </td>';
            $this->HTMLConsulta .= '  <td>';
            $this->HTMLConsulta .= $dConsulta['nome-entidade-origem'];
            $this->HTMLConsulta .= '  </td>';
            $this->HTMLConsulta .= '</tr>';
        }
    }

    public function quantidadePassagensDia( $data )
    {
        $quantidadePassagens    = 0;

        foreach ($this->consultaDetalhes as $cDetalhe)
        {
            if( $data == substr($cDetalhe['data-consulta'], 0, 10) )
            {
                $quantidadePassagens++;
            }
        }

        return $quantidadePassagens;
    }
}
