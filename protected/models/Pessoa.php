<?php

class Pessoa extends CActiveRecord {

    private $connection;

    public function editarAtributo( $pessoaId, $atributo, $valor )
    {
        $arrReturn = array('hasErrors'=>false);

        $pessoa = Pessoa::model()->findByPk($pessoaId);
        $pessoa->setAttribute($atributo, $valor);

        if( $pessoa != NULL )
        {
            if( !$pessoa->update() )
            {
                $arrReturn['hasErrors'] = true;
            }
        }

        return $arrReturn;
    }

    public function conjugeToXml()
    {
        $conjuge = $this->getConjuge();

        return '<conjuge>'.
                    '<nome>'.strtoupper($conjuge->nome).'</nome>'.
                    '<dataNascimento>'.$conjuge->nascimento.'</dataNascimento>'.
                    '<naturalidade cidade="'.strtoupper($conjuge->cidade->nome).'" estado="' . strtoupper($conjuge->cidade->uf) .'" />'.
                    '<documentos>'.
                        '<documento>'.
                            '<tipo>cpf</tipo>'.
                            '<numero>'.$conjuge->getCPF()->numero.'</numero>'
                        .'</documento>'.
                        '<documento>'.
                            '<tipo>rg</tipo>'.
                            '<numero>'.$conjuge->getRG()->numero.'</numero>'.
                            '<data>'.$conjuge->getRG()->data_emissao.'</data>'.
                            '<ufemissor>'.$conjuge->getRG()->uf_emissor.'</ufemissor>'
                        .'</documento>'
                    .'</documentos>'.
                    '<empregos>'.
                        '<emprego>'.
                            '<empresa>'.strtoupper($conjuge->getDadosProfissionais()->empresa).'</empresa>'.
                            '<cargo>' . strtoupper($conjuge->getDadosProfissionais()->profissao) . '</cargo>'.
                            '<salario>' . number_format($conjuge->getDadosProfissionais()->renda_liquida, 2, '.', '') . '</salario>'.
                            '<tempo Anos="'.$conjuge->getDadosProfissionais()->tempo_de_servico_anos.'" Meses="'.$conjuge->getDadosProfissionais()->tempo_de_servico_meses.'"/>'.
                        '</emprego>'.
                    '</empregos>'
                .'</conjuge>';
    }

    /*Retorna objeto tipo pessoa*/
    public function getConjuge()
    {
        $papelRelacao = $this->casada();

        if( $papelRelacao != NULL )
        {
            $relacaoFamiliar            = RelacaoFamiliar::model()->find('Parente_1 = ' .$papelRelacao->id. ' OR Parente_2 = '. $papelRelacao->id . ' AND habilitado');
            
            if( $relacaoFamiliar       != NULL )
            {
                $papelRelacaoConjuge    = ( $relacaoFamiliar->Parente_1 == $papelRelacao->id ? $relacaoFamiliar->Parente_2 : $relacaoFamiliar->Parente_1 );

                $papelDoConjuge         = PapelRelacao::model()->findByPk($papelRelacaoConjuge);

                if( $papelDoConjuge != NULL )
                {
                    return $papelDoConjuge->pessoa;
                }
                else
                {
                    return NULL;
                }
            }

            else
            {
                return NULL;
            }
        }

        else
        {
            return NULL;
        }
    }

    public function atualizar($PessoaNovosDados, $Pessoa_id)
    {
        $util                           = new Util;
        $Pessoa                         = Pessoa::model()->findByPk($Pessoa_id);

        $Pessoa->nome                   = $PessoaNovosDados['nome'];
        $Pessoa->nascimento             = $util->view_date_to_bd($PessoaNovosDados['nascimento']);
        $Pessoa->sexo                   = $PessoaNovosDados['sexo'];
        $Pessoa->naturalidade           = $PessoaNovosDados['naturalidade'];
        $Pessoa->naturalidade_cidade    = $PessoaNovosDados['naturalidade_cidade'];
        $Pessoa->nacionalidade          = $PessoaNovosDados['nacionalidade'];
        $Pessoa->Estado_Civil_id        = $PessoaNovosDados['Estado_Civil_id'];

        if( $Pessoa->update() )
        {
            return $Pessoa;
        }

        else
        {
            return NULL;
        }
    }

    public function getProfissao(){

    }

    public function tableName()
    {
        return 'Pessoa';
    }

    public function listarEnderecos($draw){

        $rows                           = array();
        $countEnderecos                 = 0;
        $btn                            = "";

        $pessoaHasEndereco = PessoaHasEndereco::model()->findAll('Pessoa_id = ' . $this->id);

        if( count($pessoaHasEndereco) > 0 )
        {

            foreach( $pessoaHasEndereco as $phe )
            {

                $endereco   = Endereco::model()->findByPk( $phe->Endereco_id );

                if( $endereco != NULL )
                {
                    $enderecoDeCobranca = ( $endereco->endereco_de_cobranca ? "Sim" : "Não" );
                    $countEnderecos++;

                    $btn = '<a data-input-controller-action="controller_endereco_action" data-controller-action="update" data-url-request="/endereco/load/" data-entity-id="'.$endereco->id.'"'.'class="btn btn-primary btn-update btn-load-form" style="padding:2px 5px!important; font-size:11px!important;" href="#"><i class="clip-pencil"></i></a>';

                    $row                = array(
                        'logradouro'    => $endereco->logradouro,
                        'bairro'        => $endereco->bairro,
                        'numero'        => $endereco->numero,
                        'cidade'        => $endereco->cidade,
                        'cep'           => $endereco->cep,
                        'uf'            => $endereco->uf,
                        'complemento'   => $endereco->complemento,
                        'cob'           => $enderecoDeCobranca,
                        'btn'           => $btn,
                    );

                    $rows[] = $row;
                    $btn    = "";
                }
            }
        }

        return (array(
            "draw"              => $draw,
            "recordsTotal"      => $countEnderecos,
            "recordsFiltered"   => $countEnderecos,
            "data"              => $rows
        ));
    }

    public function listarDadosBancarios($draw){

        $rows                           = array();
        $countDadosBancarios            = 0;
        $btn                            = "";

        $pessoaHasDadosBancarios        = PessoaHasDadosBancarios::model()->findAll('Pessoa_id = ' . $this->id);

        if( count($pessoaHasDadosBancarios) > 0 )
        {

            foreach( $pessoaHasDadosBancarios as $phdb )
            {

                $dadosBancarios         = DadosBancarios::model()->findByPk( $phdb->Dados_Bancarios_id );

                if( $dadosBancarios != NULL )
                {
                    
                    $countDadosBancarios++;

                    $btn = '<a data-input-controller-action="controller_db_action" data-controller-action="update" data-input-controller-action="controller_db_action" data-controller-action="update" data-url-request="/dadosBancarios/load/" data-entity-id="'.$dadosBancarios->id.'"'.'class="btn btn-update btn-primary btn-load-form" style="padding:2px 5px!important; font-size:11px!important;" href="#"><i class="clip-pencil"></i></a>';

                    $row                = array(
                        'banco'         => $dadosBancarios->banco->nome,
                        'agencia'       => $dadosBancarios->agencia,
                        'conta'         => $dadosBancarios->numero,
                        'tipo'          => $dadosBancarios->tipoContaBancaria->tipo,
                        'operacao'      => $dadosBancarios->operacao,
                        'btn'           => $btn,
                    );

                    $rows[] = $row;
                    $btn    = "";
                }
            }
        }

        return (array(
            "draw"              => $draw,
            "recordsTotal"      => $countDadosBancarios,
            "recordsFiltered"   => $countDadosBancarios,
            "data"              => $rows
        ));
    }

    public function listarDadosProfissionais($draw){

        $rows                           = array();
        $countDP                        = 0;
        $util                           = new Util;
        $btn                            = "";

        $dadosProfissionais             = DadosProfissionais::model()->findAll('Pessoa_id = ' . $this->id);

        if( count( $dadosProfissionais ) > 0 )
        {
            foreach ( $dadosProfissionais as $dadosP )
            {
                $countDP++;
                $contato                                = Contato::model()->findByPk($dadosP->Contato_id);
                $aposentado                             = ( $dadosP->aposentado  ? "Sim" : "Não" );
                $pensionista                            = ( $dadosP->pensionista ? "Sim" : "Não" );
                $pensionista                            = ( $dadosP->pensionista ? "Sim" : "Não" );
                $data_admissao                          = ( $dadosP->data_admissao == NULL ? "" : $util->bd_date_to_view($dadosP->data_admissao) );
                $telefone                               = "";
                $ramal                                  = "";
                $email                                  = "";

                $contatoProfissionalHasTelefone         = ContatoHasTelefone::model()->find('Contato_id = ' . $dadosP->contato->id);
                $contatoProfissionalHasEmail            = ContatoHasEmail::model()->find('Contato_id = ' . $dadosP->contato->id);

                if( $contatoProfissionalHasTelefone     != null )
                {
                    $telefoneProfissional               = Telefone::model()->findByPk($contatoProfissionalHasTelefone->Telefone_id);
                    
                    if( $telefoneProfissional != NULL )
                    {
                        $telefone                        = $telefoneProfissional->numero;
                        $ramal                           = $telefoneProfissional->ramal;
                    }
                }

                if( $contatoProfissionalHasEmail        != null )
                {
                    $emailProfissional                  = Email::model()->findByPk($contatoProfissionalHasEmail->Email_id);
                    if( $emailProfissional              != NULL )
                    {
                        $email                          = $emailProfissional->email;
                    }
                }

                $btn .= '<a data-input-controller-action="controller_dp_action" data-controller-action="update" data-url-request="/dadosProfissionais/load/" data-entity-id="'.$dadosP->id.'"'.'class="btn-update btn btn-primary btn-load-form" style="padding:2px 5px!important; font-size:11px!important;" href="#"><i class="clip-pencil"></i></a>';

                $princi                                 = ($dadosP->principal ? "Sim" : "Não");

                $ocupacao                               = ( $dadosP->tipoOcupacao         != NULL ? $dadosP->tipoOcupacao->tipo : "" );
                $classeP                                = ( $dadosP->classeProfissional   != NULL ? $dadosP->classeProfissional->classe : "" );

                $row                                    = array(
                    'principal'                         => $princi,
                    'empresa'                           => $dadosP->empresa,
                    'cpf_cnpj'                          => $dadosP->cnpj_cpf,
                    'data_de_admissao'                  => $data_admissao,
                    'ocupacao'                          => $ocupacao,
                    'classe_profissional'               => $classeP,
                    'renda_liquida'                     => number_format($dadosP->renda_liquida, 2, ',','.'),
                    'mes_ano_renda'                     => $dadosP->mes_ano_renda,
                    'profissao'                         => $dadosP->profissao,
                    'aposentado'                        => $aposentado,
                    'pensionista'                       => $pensionista,
                    'tipo_de_comprovante'               => $dadosP->tipo_de_comprovante,
                    'numero_do_beneficio'               => $dadosP->numero_do_beneficio,
                    'orgao_pagador'                     => $dadosP->orgao_pagador,
                    'cep'                               => $dadosP->endereco->cep,
                    'logradouro'                        => $dadosP->endereco->logradouro,
                    'bairro'                            => $dadosP->endereco->bairro,
                    'cidade'                            => $dadosP->endereco->cidade,
                    'estado'                            => $dadosP->endereco->uf,
                    'telefone'                          => $telefone,
                    'ramal'                             => $ramal,
                    'email'                             => $email,
                    'btn'                               => $btn,
                );
                                
                $rows[]                                 = $row;
                $btn                                    = "";
                $email                                  = "";
                $telefone                               = "";
                $ramal                                  = "";
                $princi                                 = "";
            }
        }

        return (array(
            "draw"                                      => $draw,
            "recordsTotal"                              => $countDP,
            "recordsFiltered"                           => $countDP,
            "data"                                      => $rows
        ));
    }
    
    public function getTelefone() {
        $contato                        = Contato::model()->findByPk($this->Contato_id);
        $contatoHasTelefone = null;
        $telefone = null;
        
        if($contato != null){
            $contatoHasTelefone             = ContatoHasTelefone::model()->find('Contato_id = ' . $contato->id);
        }
        
        if($contatoHasTelefone != null){
            $telefone                       = Telefone::model()->findByPk($contatoHasTelefone->Telefone_id);
        }
        
        if($telefone != null){
            return "(" . $telefone->discagem_direta_distancia . ")" . $telefone->numero;
        }else{
            return "Não cadastrado";
        }
    }
    
    public function listarTelefones($draw){

        $rows                           = array();
        $countTelefones                 = 0;
        $btn                            = "";

        $contato                        = Contato::model()->findByPk($this->Contato_id);

        $contatoHasTelefone             = ContatoHasTelefone::model()->findAll('Contato_id = ' . $contato->id);

        if( count($contatoHasTelefone) > 0 )
        {
            foreach ($contatoHasTelefone as $cht)
            {
                $telefone               = Telefone::model()->findByPk($cht->Telefone_id);

                if( $telefone != NULL )
                {
                    $countTelefones++;

                    $btn .= '<a data-input-controller-action="controller_telefone_action" data-controller-action="update" data-url-request="/telefone/load/" data-entity-id="'.$telefone->id.'"'.'class="btn btn-update btn-primary btn-load-form" style="padding:2px 5px!important; font-size:11px!important;" href="#"><i class="clip-pencil"></i></a>';
                    
                    $row            = array(
                        'numero'    => $telefone->numero,
                        'tipo'      => $telefone->tipoTelefone->tipo,
                        'ramal'     => $telefone->ramal,
                        'btn'       => $btn,
                    );
                                
                    $rows[] = $row;
                    $btn                            = "";
                }
            }
        }

        return (array(
            "draw"              => $draw,
            "recordsTotal"      => $countTelefones,
            "recordsFiltered"   => $countTelefones,
            "data"              => $rows
        ));
    }
    
    public function getEmail(){
        $contato                        = Contato::model()->findByPk($this->Contato_id);
        $email = null;
        $contatoHasEmail = null;
        
        if($contato != null){
            $contatoHasEmail                = ContatoHasEmail::model()->find('Contato_id = ' . $contato->id);     
        }
        
        if($contatoHasEmail != null){
            $email                          = Email::model()->findByPk($contatoHasEmail->Email_id);
        }
        
        if($email != null){
            return $email->email;
        }else{
            return "Não cadastrado";
        }
    }
    
    public function listarEmails($draw){

        $rows                           = array();
        $countEmails                    = 0;

        $contato                        = Contato::model()->findByPk($this->Contato_id);
        $btn                            = "";

        $contatoHasEmail                = ContatoHasEmail::model()->findAll('Contato_id = ' . $contato->id);

        if( count($contatoHasEmail) > 0 )
        {
            foreach ($contatoHasEmail as $che)
            {
                $email                  = Email::model()->findByPk($che->Email_id);

                if( $email != NULL )
                {
                    $btn = '<a data-input-controller-action="controller_email_action" data-controller-action="update" data-url-request="/email/load/" data-entity-id="'.$email->id.'"'.'class="btn btn-primary btn-load-form btn-update" style="padding:2px 5px!important; font-size:11px!important;" href="#"><i class="clip-pencil"></i></a>';
                    $countEmails++;

                    $row                = array(
                        'email'         => $email->email,
                        'btn'           => $btn,
                    );
                                
                    $rows[] = $row;
                    $btn = '';
                }
            }
        }

        return (array(
            "draw"                      => $draw,
            "recordsTotal"              => $countEmails,
            "recordsFiltered"           => $countEmails,
            "data"                      => $rows
        ));
    }

    public function listarDocumentos($draw){

        $util                               = new Util;
        $rows                               = array();
        $countDocs                          = 0;
        $btn                                = "";

        $pessoaHasDocumento                 = PessoaHasDocumento::model()->findAll('Pessoa_id = ' . $this->id . ' AND habilitado');

        if( count( $pessoaHasDocumento ) > 0 )
        {
            foreach ( $pessoaHasDocumento as $phd )
            {
                $doc                        = Documento::model()->findByPk($phd->Documento_id);

                $data_emissao               = ( ( !empty( $doc->data_emissao ) && $doc->data_emissao != NULL ) ? $util->bd_date_to_view( $doc->data_emissao ) : $doc->data_emissao );

                if( $doc != NULL && $doc->habilitado )
                {   
                    if( $doc->Tipo_documento_id != 1 && $doc->Tipo_documento_id != 2 )
                    {
                        $btn .= '<a data-input-controller-action="controller_documento_action" data-controller-action="update" data-url-request="/documento/load/" data-entity-id="'.$doc->id.'"'.'class="btn-update btn btn-primary btn-load-form" style="padding:2px 5px!important; font-size:11px!important;" href="#modal_form_new_doc"><i class="clip-pencil"></i></a>';
                    }
                    
                    $countDocs++;

                    $row                    = array(
                        'numero'            => $doc->numero,
                        'tipo'              => strtoupper($doc->tipoDocumento->tipo),
                        'orgao_emissor'     => strtoupper($doc->orgao_emissor),
                        'uf_emissor'        => $doc->uf_emissor,
                        'data_emissao'      => $data_emissao,
                        'btn'               => $btn,
                    );
                                
                    $rows[]                 = $row;
                    $btn                    = "";
                }
            }
        }

        return (array(
            "draw"                      => $draw,
            "recordsTotal"              => $countDocs,
            "recordsFiltered"           => $countDocs,
            "data"                      => $rows
        ));
    }

    public function getEndereco() {

        $pessoaHasEndereco = PessoaHasEndereco::model()->find('Pessoa_id = ' . $this->id);

        return Endereco::model()->findByPk($pessoaHasEndereco->Endereco_id);
    }

    public function getEnderecoCobranca(){

        $pessoaHasEndereco              = PessoaHasEndereco::model()->find('Pessoa_id = ' . $this->id . ' AND habilitado');

        if( $pessoaHasEndereco != NULL )
        {
            return Endereco::model()->findByPk($pessoaHasEndereco->Endereco_id);
        }

        else
        {
            return NULL;
        }

    }

    public function isClient($cpf) {

        $doc = Documento::model()->find('numero = ' . $cpf . ' AND Tipo_Documento_id = 1 AND habilitado');

        if ($doc != NULL) {
            $phd = PessoaHasDocumento::model()->find('Documento_id = ' . $doc->id);
            return Pessoa::model()->findByPk($phd->Pessoa_id);
        } else {
            return NULL;
        }
    }

    private function setConn() {

        $this->connection = new CDbConnection(
                Yii::app()->params['dbconf']['dns'], Yii::app()->params['dbconf']['user'], Yii::app()->params['dbconf']['pass']
        );
    }

    public function getCPF() {

        $pessoaHasDocumento = PessoaHasDocumento::model()->findAll('Pessoa_id = ' . $this->id);

        if( count( $pessoaHasDocumento ) > 0 )
        {
            foreach ($pessoaHasDocumento as $phd) {

                $doc = Documento::model()->findByPk($phd->Documento_id);

                if( $doc != NULL )
                {
                    if ($doc->tipoDocumento->id == 1)
                        return $doc;
                }
            }            
        }
        
        return null;
    }
    
    public function getRG() {

        $pessoaHasDocumento = PessoaHasDocumento::model()->findAll('Pessoa_id = ' . $this->id);

        if( count( $pessoaHasDocumento ) > 0 )
        {
            foreach ($pessoaHasDocumento as $phd) {

                $doc = Documento::model()->findByPk($phd->Documento_id);

                if( $doc != NULL )
                {
                    if ($doc->tipoDocumento->id == 2){
                        return $doc;
                    }
                }
            }            
        }

        else
        {
            return NULL;
        }
    }

    public function rules() {

        return array(
            array('Contato_id, nome, Estado_Civil_id', 'required'),
            array('Contato_id, Estado_Civil_id, naturalidade_cidade, habilitado', 'numerical', 'integerOnly' => true),
            array('nome', 'length', 'max' => 150),
            array('sexo', 'length', 'max' => 5),
            array('naturalidade', 'length', 'max' => 4),
            array('estado_civil', 'length', 'max' => 45),
            array('data_cadastro, nascimento, data_cadastro_br', 'safe'),
            array('id, Contato_id, nome, razao_social, nascimento, habilitado, data_cadastro, data_cadastro_br, sexo, naturalidade, naturalidade_cidade, estado_civil, Estado_Civil_id', 'safe', 'on' => 'search'),
        );
    }
   
    public function getCidadeNaturalidade(){

        return Cidade::model()->findByPk( $this->naturalidade_cidade );
    }

    public function relations() {
        
        return array(
            'contato'               => array(self::BELONGS_TO   ,   'Contato'               ,   'Contato_id'                                                            ),
            'estadoCivil'           => array(self::BELONGS_TO   ,   'EstadoCivil'           ,   'Estado_Civil_id'                                                       ),
            'pessoaHasDocumentos'   => array(self::HAS_MANY     ,   'PessoaHasDocumento'    ,   'Pessoa_id'                                                             ),
            'pessoaHasEndereco'     => array(self::HAS_MANY     ,   'PessoaHasEndereco'     ,   'Pessoa_id'             , 'condition'=>'pessoaHasEndereco.habilitado'   ),
            'cidade'                => array(self::BELONGS_TO   ,   'Cidade'                ,   'naturalidade_cidade'                                                   ),
        );
    }
  
    public function attributeLabels() {
        return array(
            'id'                    => 'ID',
            'Contato_id'            => 'Contato',
            'nome'                  => 'Nome',
            'razao_social'          => 'Razão Social',
            'nascimento'            => 'Nascimento',
            'habilitado'            => 'Habilitado',
            'data_cadastro'         => 'Data Cadastro',
            'data_cadastro_br'      => 'Data / Hora Cadastro',
            'sexo'                  => 'Sexo',
            'naturalidade'          => 'Naturalidade',
            'naturalidade_cidade'   => 'Naturalidade cidade',
            'nacionalidade'         => 'Nacionalidade',
            'estado_civil'          => 'Estado Civil',
            'Estado_Civil_id'       => 'Estado Civil',            
        );
    }

    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('Contato_id', $this->Contato_id);
        $criteria->compare('nome', $this->nome, true);
        $criteria->compare('nascimento', $this->nascimento, true);
        $criteria->compare('habilitado', $this->habilitado);
        $criteria->compare('data_cadastro', $this->data_cadastro, true);
        $criteria->compare('data_cadastro_br', $this->data_cadastro_br, true);
        $criteria->compare('sexo', $this->sexo, true);
        $criteria->compare('naturalidade', $this->naturalidade, true);
        $criteria->compare('naturalidade_cidade', $this->naturalidade_cidade, true);
        $criteria->compare('nacionalidade', $this->nacionalidade, true);
        $criteria->compare('estado_civil', $this->estado_civil, true);
        $criteria->compare('Estado_Civil_id', $this->Estado_Civil_id, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getDadosBancarios() {

        $pessoaHasDadosBancarios = PessoaHasDadosBancarios::model()->find('Pessoa_id =' . $this->id);

        if( $pessoaHasDadosBancarios != NULL )
        {
            return DadosBancarios::model()->findByPk($pessoaHasDadosBancarios->Dados_Bancarios_id);
        }
        
        else
        {
            return null;
        }

    }

    public function getDadosProfissionais() {
        return DadosProfissionais::model()->find('Pessoa_id = ' . $this->id .' AND habilitado AND principal');
    }

    public static function model($className = __CLASS__) {

        return parent::model($className);
    }
    
    public static function nova()
    {
        $pessoa = new Pessoa;
                    
        return $pessoa;        
    }

    public function clearDocumentos() {

        $pessoaHasDocumento = PessoaHasDocumento::model()->findAll('Pessoa_id = ' . $this->id);

        if (count($pessoaHasDocumento) > 0) {
            
            foreach ($pessoaHasDocumento as $phd) {
                $documento = Documento::model()->findByPk($phd->Documento_id);
                $phd->delete();
                $documento->delete();
            }
        }
    }

    public function clearDadosBancarios() {

        $pessoaHasDadosBancarios = PessoaHasDadosBancarios::model()->findAll('Pessoa_id = ' . $this->id);
        
        if (count($pessoaHasDadosBancarios) > 0) {
            
            foreach ($pessoaHasDadosBancarios as $phdb) {
                $dadoBancario = DadosBancarios::model()->findByPk($phdb->Dados_Bancarios_id);
                $phdb->delete();
                $dadoBancario->delete();
            }
        }   
    }

    public function clearEnderecos() {

        $pessoaHasEndereco = PessoaHasEndereco::model()->findAll('Pessoa_id = ' . $this->id);
        
        if ( count($pessoaHasEndereco) > 0 ) {
            
           foreach ($pessoaHasEndereco as $phe) {
               
                $endereco = Endereco::model()->findByPk($phe->Endereco_id);
                $phe->delete();
                $endereco->delete();
            } 
        }
    }
        
    public function clearDadosProfissionais() {

        $dadosProfissionais = DadosProfissionais::model()->findAll('Pessoa_id = ' . $this->id);

        if (count($dadosProfissionais) > 0) {

            foreach ($dadosProfissionais as $dpf) {

                $dpf->delete();
                $dpf->clearEndereco();
                $dpf->clearContato();
            }
        }
    }

    public function conjugeLoadDadosPessoais()
    {
        $util                               = new Util;
        $cliente                            = Cliente::model()->find('Pessoa_id = ' . $this->id);
        $dataDeEmissao                      = NULL;
        $nascimentoCli                      = NULL;

        if( $cliente != NULL )
        {   
            $dataDeEmissao                  = ( $cliente->pessoa->getRG()->data_emissao     != NULL  ? $util->bd_date_to_view($cliente->pessoa->getRG()->data_emissao   )  : NULL );
            $nascimentoCli                  = ( $cliente->pessoa->nascimento                != NULL  ? $util->bd_date_to_view($cliente->pessoa->nascimento              )  : NULL );
            
            return array(
                'fields'                    => array(
                    'id'                    => array(
                        'value'             => $cliente->id,
                        'type'              => 'text',
                        'input_bind'        => 'Conjuge_id',
                    ),
                    'nome'                  => array(
                        'value'             => $cliente->pessoa->nome,
                        'type'              => 'text',
                        'input_bind'        => 'conjuge_nome',
                    ),
                    'cpfConjuge'            => array(
                        'value'             => $cliente->pessoa->getCPF()->numero,
                        'type'              => 'text',
                        'input_bind'        => 'cpf_conjuge',
                    ),
                    'cpfConjugeHidden'      => array(
                        'value'             => $cliente->pessoa->getCPF()->numero,
                        'type'              => 'text',
                        'input_bind'        => 'cpf_conjuge_hidden',
                    ),
                    'sexo'                  => array(
                        'value'             => $cliente->pessoa->sexo,
                        'type'              => 'select',
                        'input_bind'        => 'Conjuge_sexo',
                    ),
                    'nascimento'            => array(
                        'value'             => $nascimentoCli,
                        'type'              => 'text',
                        'input_bind'        => 'conjuge_nascimento',
                    ),
                    'rg'                    => array(
                        'value'             => $cliente->pessoa->getRG()->numero,
                        'type'              => 'text',
                        'input_bind'        => 'rg_conjuge',
                    ),
                    'rg_orgao_emissor'      => array(
                        'value'             => $cliente->pessoa->getRG()->orgao_emissor,
                        'type'              => 'text',
                        'input_bind'        => 'RG_conjuge_orgao_emissor',
                    ),
                    'RG_uf_emissor'         => array(
                        'value'             => $cliente->pessoa->getRG()->uf_emissor,
                        'type'              => 'select',
                        'input_bind'        => 'RGConjuge_uf_emissor',
                    ),
                    'RG_emissao'            => array(
                        'value'             => $dataDeEmissao,
                        'type'              => 'text',
                        'input_bind'        => 'RG_conjuge_data_emissao',
                    ),
                    'select-nacionalidade'  => array(
                        'value'             => $cliente->pessoa->nacionalidade,
                        'type'              => 'select',
                        'input_bind'        => 'select-nacionalidade-conjuge',
                    ),
                    'Pessoa_naturalidade'   => array(
                        'value'             => $cliente->pessoa->naturalidade,
                        'type'              => 'select',
                        'input_bind'        => 'Conjuge_naturalidade',
                    ),
                    'Pessoa_Estado_Civil_id'=> array(
                        'value'             => $cliente->pessoa->Estado_Civil_id,
                        'type'              => 'select',
                        'input_bind'        => 'Conjuge_Estado_Civil_id',
                    ),
                    'nome_da_mae'           => array(
                        'value'             => $cliente->getCadastro()->nome_da_mae,
                        'type'              => 'text',
                        'input_bind'        => 'Cadastro_conjuge_nome_da_mae',
                    ),                              
                )
            );
        }
        
        else
        {
            return NULL;
        }
    }

    public function conjugeLoadDadosPorifissionais()
    {
        $util                               = new Util;
        $cliente                            = Cliente::model()->find('Pessoa_id = ' . $this->id);
        $tel_numero                         = NULL;
        $tel_ramal                          = NULL;
        $tel_tipo                           = NULL;
        $dataAdmissao                       = NULL;

        if( $cliente != NULL )
        {
            $dPConj                         = $cliente->pessoa->getDadosProfissionais();

            $contatoProfissionalHasTelefone = ContatoHasTelefone::model()->find('Contato_id = ' . $dPConj->contato->id);

            if( $contatoProfissionalHasTelefone != NULL )
            {
                $telefoneProfissional           = Telefone::model()->findByPk($contatoProfissionalHasTelefone->Telefone_id);
                
                if( $telefoneProfissional       != NULL )
                {
                    $tel_numero                 = $telefoneProfissional->numero;
                    $tel_ramal                  = $telefoneProfissional->ramal;
                    $tel_tipo                   = $telefoneProfissional->Tipo_Telefone_id;
                }
            }

            if( $dPConj != NULL )
            {   

                $dataAdmissao                  = ( $dPConj->data_admissao   != NULL  ? $util->bd_date_to_view($dPConj->data_admissao) : NULL );

                return array(
                    'fields'                => array(
                        'ControllerAction'  => array(
                            'value'     => 'join',
                            'type'      => 'text',
                            'input_bind'=> 'ControllerConjugeAction',
                        ),
                        'dp_conjuge_cnpj_cpf'   => array(
                            'value'             => $dPConj->cnpj_cpf,
                            'type'              => 'text',
                            'input_bind'        => 'dp_conjuge_cnpj_cpf',
                        ),
                        'dp_con_classe_profis'  => array(
                            'value'             => $dPConj->Classe_Profissional_id,
                            'type'              => 'select',
                            'input_bind'        => 'DadosProfissionaisConjuge_Classe_Profissional_id',
                        ),
                        'dp_conjuge_empresa'    => array(
                            'value'             => $dPConj->empresa,
                            'type'              => 'text',
                            'input_bind'        => 'dp_conj_empresa',
                        ),
                        'dp_conj_meses_de_servico'  => array(
                            'value'                 => $dPConj->tempo_de_servico_meses,
                            'type'                  => 'text',
                            'input_bind'            => 'dp_conj_meses_de_servico',
                        ),
                        'dp_conj_anos_de_servico'   => array(
                            'value'                 => $dPConj->tempo_de_servico_anos,
                            'type'                  => 'text',
                            'input_bind'            => 'dp_conj_anos_de_servico',
                        ),
                        'dp_con_ocupacao_id'    => array(
                            'value'             => $dPConj->Tipo_Ocupacao_id,
                            'type'              => 'select',
                            'input_bind'        => 'DadosProfissionaisConjuge_Tipo_Ocupacao_id',
                        ),
                        'dp_con_data_emissao'   => array(
                            'value'             => $dataAdmissao,
                            'type'              => 'text',
                            'input_bind'        => 'dp_con_data_admissao',
                        ),
                        'dp_con_profissao'      => array(
                            'value'             => $dPConj->profissao,
                            'type'              => 'select',
                            'input_bind'        => 'DadosProfissionaisConjuge_profissao',
                        ),
                        'dp_con_renda_liq'      => array(
                            'value'             => number_format($dPConj->renda_liquida,2,',','.'),
                            'type'              => 'text',
                            'input_bind'        => 'dp_conj_renda_liquida',
                        ),
                        'dp_con_mes_ano_renda'  => array(
                            'value'             => $dPConj->mes_ano_renda,
                            'type'              => 'text',
                            'input_bind'        => 'dp_conj_mes_ano_renda',
                        ),
                        'dp_con_orgao-pg'       => array(
                            'value'             => $dPConj->orgao_pagador,
                            'type'              => 'select',
                            'input_bind'        => 'DadosProfissionaisConjuge_orgao_pagador',
                        ),
                        'dp_con_n_beneficio'    => array(
                            'value'             => $dPConj->numero_do_beneficio,
                            'type'              => 'text',
                            'input_bind'        => 'dp_conj_numero_do_beneficio',
                        ),
                        'dp_con_tipo_compro'    => array(
                            'value'             => $dPConj->tipo_de_comprovante,
                            'type'              => 'select',
                            'input_bind'        => 'DadosProfissionaisConjuge_TipoDeComprovante',
                        ),
                        'dp_end_cep'            => array(
                            'value'             => $dPConj->endereco->cep,
                            'type'              => 'text',
                            'input_bind'        => 'cep_dados_profissionais_conjuge',
                        ),
                        'dp_end_logradouro'     => array(
                            'value'             => $dPConj->endereco->logradouro,
                            'type'              => 'text',
                            'input_bind'        => 'logradouro_dados_profissionais_conjuge',
                        ),
                        'dp_end_numero'         => array(
                            'value'             => $dPConj->endereco->numero,
                            'type'              => 'text',
                            'input_bind'        => 'numero_endereco_dados_profissionais_conjuge',
                        ),
                        'dp_end_cidade'         => array(
                            'value'             => $dPConj->endereco->cidade,
                            'type'              => 'text',
                            'input_bind'        => 'cidade_dados_profissionais_conjuge',
                        ),
                        'dp_end_bairro'         => array(
                            'value'             => $dPConj->endereco->bairro,
                            'type'              => 'text',
                            'input_bind'        => 'bairro_dados_profissionais_conjuge',
                        ),
                        'dp_con_end_uf'         => array(
                            'value'             => $dPConj->endereco->uf,
                            'type'              => 'select',
                            'input_bind'        => 'EnderecoProfissConjuge_uf',
                        ),
                        'dp_end_complemento'    => array(
                            'value'             => $dPConj->endereco->complemento,
                            'type'              => 'text',
                            'input_bind'        => 'dp_conj_endereco_complemento',
                        ),
                        'dpConjTelefoneBumero'  => array(
                            'value'             => $tel_numero,
                            'type'              => 'text',
                            'input_bind'        => 'dp_conj_telefone_numero',
                        ),
                        'dpConjTelefoneRamal'   => array(
                            'value'             => $tel_ramal,
                            'type'              => 'text',
                            'input_bind'        => 'dp_conj_telefone_ramal',
                        ),
                        'dpConjTelefoneTipo'    => array(
                            'value'             => $tel_tipo,
                            'type'              => 'select',
                            'input_bind'        => 's2id_TelefoneProfissConjuge_Tipo_Telefone_id',
                        ),
                    )
                );
            }

            else
            {
                return NULL;
            }
        }

        else
        {
            return NULL;
        }
    }

    public function casada()
    {

        return PapelRelacao::model()->find('Pessoa_id = ' . $this->id . ' AND habilitado');
    }

}
