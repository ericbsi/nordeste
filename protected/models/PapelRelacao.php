<?php

class PapelRelacao extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Papel_Relacao';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Parentesco_id, Pessoa_id', 'required'),
			array('Parentesco_id, habilitado, Pessoa_id', 'numerical', 'integerOnly'=>true),
			array('id, Parentesco_id, habilitado, Pessoa_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'pessoa' => array(self::BELONGS_TO, 'Pessoa', 'Pessoa_id'),
			'parentesco' => array(self::BELONGS_TO, 'Parentesco', 'Parentesco_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' 			=> 'ID',
			'Parentesco_id' => 'Parentesco',
			'Pessoa_id' 	=> 'Pessoa',
			'habilitado' 	=> 'Habilitado'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Parentesco_id',$this->Parentesco_id);
		$criteria->compare('Pessoa_id',$this->Pessoa_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PapelRelacao the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}