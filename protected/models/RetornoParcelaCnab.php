<?php

class RetornoParcelaCnab extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Retorno_parcela_cnab';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Ocorrencia_retorno_cnab_id', 'required'),
			array('origem_do_pagamento_id, Ocorrencia_retorno_cnab_id, Motivo_rejeicao_titulo_id, Range_gerado_id', 'numerical', 'integerOnly'=>true),
			array('valor_pago, juros_de_mora', 'numerical'),
			array('nosso_numero', 'length', 'max'=>100),
			array('data_ocorrencia, data_importacao', 'safe'),
			array('id, origem_do_pagamento_id, nosso_numero, valor_pago, juros_de_mora, Ocorrencia_retorno_cnab_id, Motivo_rejeicao_titulo_id, data_ocorrencia, data_importacao, Range_gerado_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'motivoRejeicaoTitulo' => array(self::BELONGS_TO, 'MotivoRejeicaoTitulo', 'Motivo_rejeicao_titulo_id'),
			'rangeGerado' => array(self::BELONGS_TO, 'RangesGerados', 'Range_gerado_id'),
			'ocorrenciaRetornoCnab' => array(self::BELONGS_TO, 'OcorrenciaRetornoCnab', 'Ocorrencia_retorno_cnab_id'),
			'origemDoPagamento' => array(self::BELONGS_TO, 'OrigemDoPagamento', 'origem_do_pagamento_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'	 						=> 'ID',
			'origem_do_pagamento_id' 		=> 'Origem Do Pagamento',
			'nosso_numero' 					=> 'Nosso Numero',
			'valor_pago' 					=> 'Valor Pago',
			'juros_de_mora' 				=> 'Juros De Mora',
			'Ocorrencia_retorno_cnab_id' 	=> 'Ocorrencia Retorno Cnab',
			'Motivo_rejeicao_titulo_id' 	=> 'Motivo Rejeicao Titulo',
			'data_ocorrencia' 				=> 'Data Ocorrencia',
			'Range_gerado_id' 				=> 'Range Gerado',
			'data_importacao' 				=> 'Data importação',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('origem_do_pagamento_id',$this->origem_do_pagamento_id);
		$criteria->compare('nosso_numero',$this->nosso_numero,true);
		$criteria->compare('valor_pago',$this->valor_pago);
		$criteria->compare('juros_de_mora',$this->juros_de_mora);
		$criteria->compare('Ocorrencia_retorno_cnab_id',$this->Ocorrencia_retorno_cnab_id);
		$criteria->compare('Motivo_rejeicao_titulo_id',$this->Motivo_rejeicao_titulo_id);
		$criteria->compare('data_ocorrencia',$this->data_ocorrencia,true);
		$criteria->compare('data_importacao',$this->data_importacao,true);
		$criteria->compare('Range_gerado_id',$this->Range_gerado_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public function novo( $origem_do_pagamento_id = NULL, $nosso_numero = NULL, $valor_pago = NULL, $juros_de_mora = NULL, $Ocorrencia_retorno_cnab_id = NULL, $Motivo_rejeicao_titulo_id = NULL, $data_ocorrencia = NULL, $Range_gerado_id = NULL, $data_importacao = NULL )
	{
		$retornoParcelaCnab									= new RetornoParcelaCnab;
		$retornoParcelaCnab->origem_do_pagamento_id 		= $origem_do_pagamento_id;
		$retornoParcelaCnab->nosso_numero					= $nosso_numero;
		$retornoParcelaCnab->valor_pago						= $valor_pago;
		$retornoParcelaCnab->juros_de_mora					= $juros_de_mora;
		$retornoParcelaCnab->Ocorrencia_retorno_cnab_id		= $Ocorrencia_retorno_cnab_id;
		$retornoParcelaCnab->Motivo_rejeicao_titulo_id		= $Motivo_rejeicao_titulo_id;
		$retornoParcelaCnab->data_ocorrencia				= $data_ocorrencia;
		$retornoParcelaCnab->Range_gerado_id				= $Range_gerado_id;
		$retornoParcelaCnab->data_importacao				= $data_importacao;
		$retornoParcelaCnab->save();

		return $retornoParcelaCnab;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RetornoParcelaCnab the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
