<?php


class Produto extends CActiveRecord {

    public function novo($Produto, $Cores, $Tamanhos){
        
        $retorno        = array(
            'hasError'  => 0,
            'ntfyClass' => 'success',
            'msg'       => 'Produto cadastrado com sucesso!',
        );

        if( !empty( $Produto['descricao'] ) )
        {
            
            for ( $i = 0; $i < count($Cores); $i++ )
            {   
                if( $Cores[$i] != 0 )
                {
                    for ( $y = 0; $y < count($Tamanhos); $y++ )
                    {   
                        if( $Tamanhos[$y] != 0 )
                        {
                            $produto                                            = new Produto;
                            $produto->attributes                                = $Produto;

                            if( !$produto->save() )
                            {
                                $retorno['hasError']                            = 1;
                                $retorno['ntfyClass']                           = 'error';
                                $retorno['msg']                                 = 'Erro ao cadastrar produto! Contate o suporte!';
                            }

                            else
                            {
                                $produtoHasCor                                  = new ProdutoHasValoresCaracteristica;
                                $produtoHasCor->Produto_id                      = $produto->id;
                                $produtoHasCor->ValoresCaracteristica_id        = $Cores[$i];
                                
                                $produtoHasTamanho                              = new ProdutoHasValoresCaracteristica;
                                $produtoHasTamanho->Produto_id                  = $produto->id;
                                $produtoHasTamanho->ValoresCaracteristica_id    = $Tamanhos[$y];
                                

                                if( $produtoHasCor->save() && $produtoHasTamanho->save() )
                                {
                                    $produto->descricao = $produto->descricao . ' ' . $produtoHasCor->valoresCaracteristica->valor . ' ' . $produtoHasTamanho->valoresCaracteristica->valor;
                                    $produto->update();
                                }
                            }
                        }
                    }
                }
            }
        }
    
        return $retorno;

        /*return array(
            'cores_correct'     => print_r($cores_correct),
            'tamanhos_correct'  => print_r($tamanhos_correct)
        );*/
    }

    public function tableName() {

        return 'Produto';
    }
   
    public function rules() {
        return array(
            array('descricao, TipoProduto_id, Categoria_id, Modelo_id, Ncm_id', 'required'),
            array('habilitado, TipoProduto_id, Categoria_id, Modelo_id, Ncm_id', 'numerical', 'integerOnly' => true),
            array('descricao', 'length', 'max' => 100),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, descricao, habilitado, TipoProduto_id, Categoria_id, Modelo_id, Ncm_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {

        return array(
            'itemDoEstoques' => array(self::HAS_MANY, 'ItemDoEstoque', 'Produto_id'),
            'modelo' => array(self::BELONGS_TO, 'Modelo', 'Modelo_id'),
            'categoria' => array(self::BELONGS_TO, 'CategoriaProduto', 'Categoria_id'),
            'tipoProduto' => array(self::BELONGS_TO, 'TipoProduto', 'TipoProduto_id'),
            'ncm' => array(self::BELONGS_TO, 'Ncm', 'nCM_id'),
        );
    }
    
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'descricao' => 'Descricao',
            'habilitado' => 'Habilitado',
            'TipoProduto_id' => 'Tipo Produto',
            'Categoria_id' => 'Categoria',
            'Modelo_id' => 'Modelo',
            'Ncm_id' => 'Ncm',
        );
    }
    
    public function search() {
        

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('descricao', $this->descricao, true);
        $criteria->compare('habilitado', $this->habilitado);
        $criteria->compare('TipoProduto_id', $this->TipoProduto_id);
        $criteria->compare('Categoria_id', $this->Categoria_id);
        $criteria->compare('Modelo_id', $this->Modelo_id);
        $criteria->compare('Ncm_id', $this->Ncm_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function listar($draw, $offset, $order, $columns) {

        $orderDir = $order[0]['dir'];
        $orderString = "";

        if ($order[0]['column'] == 0) {
            $orderString = "produto.descricao $orderDir";
        }

        $countProdutos = Produto::model()->findAll();

        $util = new Util;

        $rows = array();

        $crt = new CDbCriteria();
        $crt->offset = $offset;
        $crt->limit = 10;
        //$crt->with = array('produto');
        $crt->order = "descricao $orderDir";


        if (!empty($columns[0]['search']['value']) && !ctype_space($columns[0]['search']['value'])) {
            $crt->addSearchCondition('descricao', $columns[0]['search']['value']);
        }

        $produtos = Produto::model()->findAll($crt);

        foreach ($produtos as $produto) {

            $btn = '<form method="post" action="' . Yii::app()->getBaseUrl(true) . '/produto/editar">';
            $btn .= '  <button class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Editar"><i class="fa fa-edit"></i></button>';
            $btn .= '  <input type="hidden" name="idProduto" value="' . $produto->id . '">';
            $btn .= '</form>';
            /*
            { "data": "descricao" },
            { "data": "tipo" },
            { "data": "categoria" },
            { "data": "modelo" },*/
            
            $tipo      = TipoProduto::model()->findByPk($produto->TipoProduto_id);
            $categoria = CategoriaProduto::model()->findByPk($produto->Categoria_id);
            $modelo    = Modelo::model()->findByPk($produto->Modelo_id);

            $row = array(
                'descricao' => $produto->descricao,
                'tipo'      => $tipo->descricao,
                /*'categoria' => $categoria->nome,
                'modelo'    => $modelo->descricao,*/
                'btn'       => $btn,
            );
            
            $rows[] = $row;
        }

        return (array(
            "draw" => $draw,
            "recordsTotal" => count($countProdutos),
            "recordsFiltered" => count($countProdutos),
            "data" => $rows
        ));
    }

    public static function model($className = __CLASS__) {

        return parent::model($className);
    }
    
    public function searchProdutosSelec2($q, $idsProd){

        $retorno = null;

        if( !empty( $q ) ){

            $crt        = new CDbCriteria;
            $crt->addSearchCondition('t.descricao', $q, TRUE, 'OR');
            
            if( !empty( $idsProd ) )
            {
                $crt->addNotInCondition('id', $idsProd);
            }


            $produtos   = Produto::model()->findAll( $crt );

            

            if( count( $produtos ) > 0 )
            {
                foreach( $produtos as $produto )
                {
                    $row = array(
                        'id'    => $produto->id,
                        'text'  => $produto->descricao
                    );
                    $retorno[]  = $row;
                }
            }
        }

        return $retorno;
    }
}
