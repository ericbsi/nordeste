<?php

/**
 * This is the model class for table "FilialOrigem".
 *
 * The followings are the available columns in table 'FilialOrigem':
 * @property integer $id
 * @property string $nome
 * @property string $data_cadastro
 * @property integer $habilitado
 * @property integer $EmpresaOrigem_id
 *
 * The followings are the available model relations:
 * @property EmpresaOrigem $empresaOrigem
 * @property FilialOrigemHasSistemaOrigem[] $filialOrigemHasSistemaOrigems
 */
class FilialOrigem extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'FilialOrigem';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nome, data_cadastro, EmpresaOrigem_id', 'required'),
			array('habilitado, EmpresaOrigem_id', 'numerical', 'integerOnly'=>true),
			array('nome', 'length', 'max'=>150),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nome, data_cadastro, habilitado, EmpresaOrigem_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'empresaOrigem' => array(self::BELONGS_TO, 'EmpresaOrigem', 'EmpresaOrigem_id'),
			'filialOrigemHasSistemaOrigems' => array(self::HAS_MANY, 'FilialOrigemHasSistemaOrigem', 'FilialOrigem_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nome' => 'Nome',
			'data_cadastro' => 'Data Cadastro',
			'habilitado' => 'Habilitado',
			'EmpresaOrigem_id' => 'Empresa Origem',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nome',$this->nome,true);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('EmpresaOrigem_id',$this->EmpresaOrigem_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FilialOrigem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
