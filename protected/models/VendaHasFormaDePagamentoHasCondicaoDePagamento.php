<?php

/**
 * This is the model class for table "Venda_has_Forma_de_Pagamento_has_Condicao_de_Pagamento".
 *
 * The followings are the available columns in table 'Venda_has_Forma_de_Pagamento_has_Condicao_de_Pagamento':
 * @property integer $id
 * @property integer $Venda_id
 * @property integer $Forma_de_Pagamento_has_Condicao_de_Pagamento_id
 * @property double $valor
 * @property integer $qtdParcelas
 *
 * The followings are the available model relations:
 * @property VendaHasFPHasCPHasAdministradoraFinanceira[] $vendaHasFPHasCPHasAdministradoraFinanceiras
 * @property FormaDePagamentoHasCondicaoDePagamento $formaDePagamentoHasCondicaoDePagamento
 * @property Venda $venda
 */
class VendaHasFormaDePagamentoHasCondicaoDePagamento extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'Venda_has_Forma_de_Pagamento_has_Condicao_de_Pagamento';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Venda_id, Forma_de_Pagamento_has_Condicao_de_Pagamento_id, valor, qtdParcelas', 'required'),
            array('Venda_id, Forma_de_Pagamento_has_Condicao_de_Pagamento_id, qtdParcelas, Cartao_id', 'numerical', 'integerOnly'=>true),
            array('valor', 'numerical'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, Venda_id, Forma_de_Pagamento_has_Condicao_de_Pagamento_id, valor, qtdParcelas, Cartao_id', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'vendaHasFPHasCPHasAdministradoraFinanceiras'   => array(self::HAS_MANY     , 'VendaHasFPHasCPHasAdministradoraFinanceira'  , 'Venda_has_FP_has_CP_id'                          ),
            'formaDePagamentoHasCondicaoDePagamento'        => array(self::BELONGS_TO   , 'FormaDePagamentoHasCondicaoDePagamento'      , 'Forma_de_Pagamento_has_Condicao_de_Pagamento_id' ),
            'venda'                                         => array(self::BELONGS_TO   , 'Venda'                                       , 'Venda_id'                                        ),
            'cartao'                                        => array(self::BELONGS_TO   , 'Cartao'                                      , 'Cartao_id'                                       ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'                                                => 'ID'                                             ,
            'Venda_id'                                          => 'Venda'                                          ,
            'Cartao_id'                                         => 'Cartao'                                         ,
            'Forma_de_Pagamento_has_Condicao_de_Pagamento_id'   => 'Forma De Pagamento Has Condicao De Pagamento'   ,
            'valor'                                             => 'Valor'                                          ,
            'qtdParcelas'                                       => 'Qtd Parcelas'                                   ,
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id'                                                 , $this->id                                                 );
        $criteria->compare('Venda_id'                                           , $this->Venda_id                                           );
        $criteria->compare('Cartao_id'                                          , $this->Cartao_id                                          );
        $criteria->compare('Forma_de_Pagamento_has_Condicao_de_Pagamento_id'    , $this->Forma_de_Pagamento_has_Condicao_de_Pagamento_id    );
        $criteria->compare('valor'                                              , $this->valor                                              );
        $criteria->compare('qtdParcelas'                                        , $this->qtdParcelas                                        );

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return VendaHasFormaDePagamentoHasCondicaoDePagamento the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}