<?php

class Titulo extends CActiveRecord {

    public function gerarRepasse($Proposta) {
        $Titulo = new Titulo;
        $Compra = new Compra;
        $Fornecedor = Fornecedor::model()->find('Filial_id = ' . $Proposta->analiseDeCredito->Filial_id . ' AND habilitado');

        if ($Fornecedor != NULL) {
            $Compra->data = date('Y-m-d');
            $Compra->Fornecedor_id = $Fornecedor->id;
            $Compra->TipoCompra_Id = 1;

            try {
                $Compra->save();
                CompraHasFormaDePagamentoHasCondicaoDePagamento::model()->novo($Compra->id, 2, $Proposta, 1);
            } catch (Exception $e) {
                throw new Exception($e->getMessage(), 1);
            }
        }
    }

    public function gerarTituloAPagar($natureza, $CFDePgtoCDePgto_id, $qtdParcelas, $valorParcelas, $Proposta) {
        $arrReturn = array(
                'title' => 'Sucesso',
                'text' => 'Títulos gerados com sucesso',
                'type' => 'success',
                'hasErrors' => false
        );

        $Titulo = new Titulo;
        $Titulo->NaturezaTitulo_id = $natureza;
        $Titulo->Compra_has_FP_has_CP_FP_has_CP_id = $CFDePgtoCDePgto_id;
        $Titulo->habilitado = 1;
        $Titulo->Proposta_id = $Proposta->id;

        if ($Titulo->save()) {
            for ($i = 1; $i <= $qtdParcelas; $i++) {
                $Parcela = new Parcela;
                $Parcela->seq = $i;
                $Parcela->valor = $valorParcelas;
                $Parcela->valor_atual = 0;
                $Parcela->Titulo_id = $Titulo->id;
                $Parcela->habilitado = 1;
                $Parcela->vencimento = NULL;

                if (!$Parcela->save()) {

                    ob_start();
                    var_dump($Parcela->getErrors());
                    $retorno = ob_get_clean();

                    $arrReturn['title'] = "Erro";
                    $arrReturn['text'] = "Erro ao gerar títulos. Parcela: $retorno";
                    $arrReturn['type'] = "error";
                    $arrReturn['hasErrors'] = true;
                }
            }
        } else {

            ob_start();
            var_dump($Titulo->getErrors());
            $retorno = ob_get_clean();

            $arrReturn['title'] = "Erro";
            $arrReturn['text'] = "Erro ao gerar títulos. Titulo: $retorno";
            $arrReturn['type'] = "error";
            $arrReturn['hasErrors'] = true;
        }

        return $arrReturn;
    }

    public function tableName() {
        return 'Titulo';
    }

    public function rules() {
        return array(
                array('Proposta_id, NaturezaTitulo_id, VendaW_id, Tributo_id, Venda_has_FP_has_CP_FP_has_CP_id', 'numerical', 'integerOnly' => true),
                array('prefixo', 'length', 'max' => 45),
                array('id, emissao, prefixo, Proposta_id, NaturezaTitulo_id, Venda_has_FP_has_CP_FP_has_CP_id, VendaW_id, Tributo_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
                'parcelas'                => array(self::HAS_MANY,    'Parcela', 'Titulo_id'),
                'proposta'                => array(self::BELONGS_TO,  'Proposta', 'Proposta_id'),
                'natureza'                => array(self::BELONGS_TO,  'NaturezaTitulo', 'NaturezaTitulo_id'),
                'tributo'                 => array(self::BELONGS_TO,  'Tributo', 'Tributo_id'),
                'tituloHasParcelas'       => array(self::HAS_MANY,    'TituloHasParcela', 'Titulo_id'),
                'vendaW'                  => array(self::BELONGS_TO,  'VendaW', 'VendaW_id'),
                'vendaHasFPHasCPFPhasCP'  => array(self::BELONGS_TO,  'VendaHasFormaDePagamentoHasCondicaoDePagamento', 'Venda_has_FP_has_CP_FP_has_CP_id'),
                'compraHasFPHasCPFPhasCP' => array(self::BELONGS_TO,  'CompraHasFormaDePagamentoHasCondicaoDePagamento', 'Compra_has_FP_has_CP_FP_has_CP_id'),
                'recebimentoInterno'      => array(self::HAS_ONE,     'RecebimentoInterno', 'Titulo_id'),
        );
    }

    public function attributeLabels() {
        return array(
                'id' => 'ID',
                'prefixo' => 'Prefixo',
                'Proposta_id' => 'Proposta',
                'Tributo_id' => 'Tributo',
                'VendaW_id' => 'Venda Web',
                'emissao' => 'Data de Emissão',
                'Venda_has_FP_has_CP_FP_has_CP_id' => 'VendahasFPhasCPFPhasCP_id',
                'Compra_has_FP_has_CP_FP_has_CP_id' => 'ComprahasFPhasCPFPhasCP_id',
                'NaturezaTitulo_id' => 'Natureza'
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('prefixo', $this->prefixo, true);
        $criteria->compare('Proposta_id', $this->Proposta_id);
        $criteria->compare('Tributo_id', $this->Tributo_id);
        $criteria->compare('VendaW_id', $this->VendaW_id);

        return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
        ));
    }

    public function listarParcelas() {

        return Parcela::model()->findAll('Titulo_id = ' . $this->id . ' AND habilitado');
    }

    public function listarPagar($draw, $search, $start) {

        $linhas = [];

        $valores = Titulo::model()->getValoresTotais($start);

        foreach ($valores[0] as $v) {

            $linhas[] = array(
                    'grupo' => $v['Grupo'],
                    'valorAberto' => 'R$ ' . number_format($v['ValorAberto'], 2, ",", "."),
                    'valorBordero' => 'R$ ' . number_format($v['ValorBordero'], 2, ",", "."),
                    'valorLote' => 'R$ ' . number_format($v['ValorLote'], 2, ",", "."),
                    'valorTotal' => 'R$ ' . number_format($v['ValorTotal'], 2, ",", ".")
            );
        }

        return array(
                "draw" => $draw,
                "recordsTotal" => $valores[1],
                "recordsFiltered" => $valores[1],
                "data" => $linhas
        );
    }

    public function listarPagamentos($draw, $search, $start) {

        $linhas = [];

        $valores = Titulo::model()->getValoresTotaisPagos($start);

        foreach ($valores[0] as $v) {

            $linhas[] = array(
                    'idGrupo' => $v['idGrupo'],
                    'grupo' => $v['Grupo'],
                    'valorTotal' => 'R$ ' . number_format($v['ValorTotal'], 2, ",", ".")
            );
        }

        return array(
                "draw" => $draw,
                "recordsTotal" => $valores[1],
                "recordsFiltered" => $valores[1],
                "data" => $linhas
        );
    }

    public function listarPagamentosNucleo($idGrupo) {

        $linhas = [];

        $valores = Titulo::model()->getValoresTotaisPagosNucleo($idGrupo);

        foreach ($valores as $v) {

            $linhas[] = array(
                    'idNucleo' => $v['idNucleo'],
                    'nucleo' => $v['Nucleo'],
                    'valorTotal' => 'R$' . number_format($v['ValorTotal'], 2, ",", ".")
            );
        }

        return array(
                "dados" => $linhas
        );
    }

    public function listarBorderos($draw, $search, $start, $borderosSelecionados, $buscaParceiro, $buscaData, $buscaCodigo) {

        $linhas = [];
        $borderos = Titulo::model()->getBorderos($start, $buscaParceiro, $buscaData, $buscaCodigo);

        foreach ($borderos[0] as $b) {

            $checked = "";

            $filial = Filial::model()->findByPk($b['destinatario']);

            if (array_search($b['id'], $borderosSelecionados)) {
                $checked = "checked";
            }

            $checkBox = '<input value="' . $b['id'] . '" ' . $checked . ' type="checkbox" class="check_bordero" />';

            $dateTime = new DateTime($b['dataCriacao']);
            $dataCriacao = $dateTime->format('d/m/Y H:i:s');

            $codigoBordero = str_pad($b["id"], 8, "0", STR_PAD_LEFT);

            $linhas[] = array(
                    'checkbox' => $checkBox,
                    'parceiro' => $filial->getConcat(),
                    'bordero' => $codigoBordero,
                    'documentacao' => $b['documentacao'],
                    'dataCriacao' => $dataCriacao,
                    'responsavel' => $b['nome_utilizador'],
                    'valor' => 'R$ ' . number_format($b['valor'], 2, ",", ".")
            );
        }

        return array(
                "draw" => $draw,
                "recordsTotal" => $borderos[1],
                "recordsFiltered" => $borderos[1],
                "data" => $linhas,
                "sql" => $borderos[2]
        );
    }

    public function listarBorderosPagos(
    $draw, $search, $start, $buscaParceiro, $buscaData, $buscaCodigo, $idGrupo, $idLote, $nucleosMarcados
    ) {

        $linhas = [];
        $borderos = Titulo::model()->getBorderosPagos(
            $start, $buscaParceiro, $buscaData, $buscaCodigo, $idGrupo, $idLote, $nucleosMarcados
        );

        foreach ($borderos[0] as $b) {

            $filial = Filial::model()->findByPk($b['destinatario']);

            $dateTime = new DateTime($b['dataCriacao']);
            $dataCriacao = $dateTime->format('d/m/Y H:i:s');

            $codigoBordero = str_pad($b["id"], 8, "0", STR_PAD_LEFT);

            $linhas[] = array(
                    'idBordero' => $b['id'],
                    'parceiro' => $filial->getConcat(),
                    'bordero' => $codigoBordero,
                    'documentacao' => $b['codigo'],
                    'dataCriacao' => $dataCriacao,
                    'responsavel' => $b['nome_utilizador'],
                    'valor' => 'R$ ' . number_format($b['valor'], 2, ",", ".")
            );
        }

        return array(
                "draw" => $draw,
                "recordsTotal" => $borderos[1],
                "recordsFiltered" => $borderos[1],
                "data" => $linhas,
                "query" => $borderos[2]
        );
    }

    public function listarLotesPagamento($draw, $search, $start) {

        $linhas = [];
        $lotes = Titulo::model()->getLotesPagamento($start);

        $dBancarios = "";
        $cgcBancarios = "";
        $total = 0;

        foreach ($lotes[0] as $l) {

            $dBancarios = "";
            $cgcBancarios = "";

            $nucleoHasDB = NucleoFiliaisHasDadosBancarios::model()->find('NucleoFiliais_id = ' . $l['idNucleo']);

            if ($nucleoHasDB !== null) {
                $dadosBancarios = DadosBancarios::model()->find('habilitado AND id = ' . $nucleoHasDB->Dados_Bancarios_id);

                if (
                    (!$dadosBancarios !== null && $dadosBancarios->Banco_id !== null ) &&
                    (!$dadosBancarios->agencia !== null && !empty($dadosBancarios->agencia) ) &&
                    (!$dadosBancarios->numero !== null && !empty($dadosBancarios->numero) )
                ) {

                    if (strpos($dadosBancarios->agencia, '-')) {
                        $agencia = str_pad(trim(str_replace(".", "", $dadosBancarios->agencia)), 8, "0", STR_PAD_LEFT);
                    } else {
                        $agencia = str_pad(trim(str_replace(".", "", $dadosBancarios->agencia)), 6, "0", STR_PAD_LEFT) . '- ';
                    }

                    if (strpos($dadosBancarios->numero, '-')) {
                        $numero = str_pad(trim(str_replace(".", "", $dadosBancarios->numero)), 12, "0", STR_PAD_LEFT);
                    } else {
                        $numero = str_pad(trim(str_replace(".", "", $dadosBancarios->numero)), 10, "0", STR_PAD_LEFT) . '- ';
                    }

                    $dBancarios = 'Banco: ' . $dadosBancarios->banco->codigo . ' - '
                        . $dadosBancarios->banco->nome . ' | ';
                    $dBancarios .= 'Ag.: ' . $agencia . ' | ';
                    $dBancarios .= 'CC: ' . $numero;

                    if (isset($dadosBancarios->cgc) && !empty($dadosBancarios->cgc)) {
                        $cgcBancarios = $dadosBancarios->cgc;
                    }
                }
            }

            $dateTime = new DateTime($l['data_cadastro']);
            $data_cadastro = $dateTime->format('d/m/Y H:i:s');

            if (Yii::app()->session['usuario']->tipo_id !== 11) {
                $btn = '<button class="btn btn-xs btn-bricky"><i class="fa fa-trash-o"></i></button>';
            } else {
                $btn = '';
            }

            $valor = $l['Valor'];
            
            $vlote = 0;
            $lhbs = LoteHasBordero::model()->findAll('habilitado AND Lote_id = ' . $l['id']);
            foreach ($lhbs as $lhb) {
                $bord = Bordero::model()->find('habilitado AND id = ' . $lhb->Bordero_id);
                foreach ($bord->itemDoBorderos as $ib) {
                    if($ib->habilitado){
                        $vlote += $ib->proposta->valorRepasse();
                    }
                }
            }
            
            $linhas[] = array(
                    'idLote' => $l['id'],
                    'codigo' => str_pad($l['id'], 10, '0', STR_PAD_LEFT),
                    'nucleo' => strtoupper($l['Nucleo']),
                    'dataCadastro' => $data_cadastro,
                    'dBancarios' => $dBancarios,
                    'cgc' => $cgcBancarios,
                    'valor' => 'R$ ' . number_format($vlote, 2, ",", "."),
                    'valorNumero' => number_format($vlote, 2, ".", ""),
                    'btnRemoveLote' => $btn
            );

            $total += floatval($l['Valor']);
        }

        return array(
                "draw" => $draw,
                "recordsTotal" => $lotes[1],
                "recordsFiltered" => $lotes[1],
                "data" => $linhas,
                "customReturn" => [
                        'total' => number_format($lotes[2], 2, ',', '.')
                ]
        );
    }

    public function listarLotesPagamentoPagos($draw, $search, $start, $dataBaixa, $nucleosMarcados, $idGrupo, $imagem, $fisico, $dataFiltro) {

        $linhas = [];
        $lotes = Titulo::model()->getLotesPagamentoPagos($start, $search, $dataBaixa, $nucleosMarcados, $idGrupo, $imagem, $fisico, $dataFiltro);

        foreach ($lotes[0] as $l) {

            $dateTime = new DateTime($l['data_cadastro']);
            $data_cadastro = $dateTime->format('d/m/Y H:i:s');

            $btn = '<button class="btn btn-xs btn-bricky"><i class="fa fa-trash-o"></i></button>';

            if ($l['pagoImagem'] == 1) {
                $pg_img = 'Pago por Imagem';
            } else {
                if ($l['pagoImagem'] == NULL) {
                    $pg_img = 'Indefinido';
                } else {
                    $pg_img = 'Contrato Físico';
                }
            }

            $valor = $l['Valor'];
            
            $vlote = 0;
            $lhbs = LoteHasBordero::model()->findAll('habilitado AND Lote_id = ' . $l['id']);
            foreach ($lhbs as $lhb) {
                $bord = Bordero::model()->find('habilitado AND id = ' . $lhb->Bordero_id);
                foreach ($bord->itemDoBorderos as $ib) {
                    if($ib->habilitado){
                        $vlote += $ib->proposta->valorRepasse();
                    }
                }
            }

            $linhas[] = array(
                    'idLote' => $l['id'],
                    'codigo' => str_pad($l['id'], 10, '0', STR_PAD_LEFT),
                    'nucleo' => strtoupper($l['Nucleo']),
                    'dataCadastro' => $data_cadastro,
                    'responsavel' => $l['Usuario'],
                    'qtdBordero' => $l['QTDLB'],
                    'valor' => 'R$ ' . number_format($vlote, 2, ",", "."),
                    'porImagem' => $pg_img
            );
        }

        return array(
                "draw"              => $draw,
                "recordsTotal"      => $lotes[1],
                "recordsFiltered"   => $lotes[2],
                "data"              => $linhas,
                "query"             => $lotes[3],
                "queryTotal"        => $lotes[4],
                "queryFiltr"        => $lotes[5],
                "dataFiltro"        => $dataFiltro
        );
    }

    public function getValoresTotais($start) {

        $filiais = [];
        $filiaisString = "";

        if (Yii::app()->session['usuario']->tipo_id == 11) {

            foreach (Yii::app()->session['usuario']->adminParceiros as $f) {
                if ($f->habilitado) {
                    $filiais[] = $f->parceiro->id;
                }
            }

            if (!empty($filiais)) {
                $filiaisString = join(',', $filiais);
            }
        }

        $query = "  SELECT  COALESCE(GF.nome_fantasia  , 'Sem Grupo'  ) AS Grupo   ,
                            COALESCE(GF.id             , 0            ) AS idGrupo ,
                            SUM(CASE WHEN  B.id IS     NULL AND  L.id IS NULL THEN P.valor ELSE 0 END) AS ValorAberto	,
                            SUM(CASE WHEN  B.id IS NOT NULL AND  L.id IS NULL THEN P.valor ELSE 0 END) AS ValorBordero	,
                            SUM(CASE WHEN  L.id IS NOT NULL                   THEN P.valor ELSE 0 END) AS ValorLote     ,
                            SUM(P.valor)                                                               AS ValorTotal

                    FROM 			Parcela             AS P
                    INNER 	JOIN 	Titulo              AS T  ON  T.habilitado  AND  P.Titulo_id              =  T.id
                    INNER 	JOIN 	Proposta            AS Pr ON Pr.habilitado  AND  T.Proposta_id            = Pr.id
                    INNER 	JOIN 	Analise_de_Credito  AS AC ON AC.habilitado  AND Pr.Analise_de_Credito_id  = AC.id
                    INNER 	JOIN 	Filial              AS F  ON  F.habilitado  AND AC.Filial_id              =  F.id
                    LEFT OUTER 	JOIN 	NucleoFiliais       AS NF ON NF.habilitado  AND  F.NucleoFiliais_id       = NF.id
                    LEFT OUTER 	JOIN 	GrupoFiliais        AS GF ON GF.habilitado  AND NF.GrupoFiliais_id        = GF.id
                    LEFT OUTER 	JOIN  	ItemDoBordero       AS IB ON IB.habilitado  AND IB.Proposta_id            = Pr.id
                    LEFT OUTER 	JOIN  	Bordero             AS  B ON  B.habilitado  AND	IB.Bordero                =  B.id
                    LEFT OUTER	JOIN	Lote_has_Bordero    AS LB ON LB.habilitado  AND LB.Bordero_id             =  B.id
                    LEFT OUTER	JOIN	LotePagamento       AS  L ON  L.habilitado  AND	LB.Lote_id                =  L.id
                    LEFT OUTER 	JOIN	DadosPagamento      AS DB ON DB.habilitado  AND  L.DadosPagamento_id      = DB.id

                    WHERE P.habilitado AND T.NaturezaTitulo_id = 2 AND Pr.titulos_gerados AND Pr.Status_Proposta_id IN (2,7) AND
                            (CASE WHEN L.id IS  NOT NULL THEN
                        (           SELECT MAX(LS.StatusLotePagamento_id)
                                                    FROM LotePagamento_has_StatusLotePagamento AS LS
                                    WHERE LS.habilitado AND LS.LotePagamento_id = LB.Lote_id
                                    HAVING MAX(LS.StatusLotePagamento_id) = 1    )
								  WHEN B.id IS NOT NULL AND L.id IS NULL THEN B.Status = 2
                                  ELSE B.id IS NULL AND L.id IS NULL
                                     END)  ";

        if (!empty($filiaisString)) {
            $query .= " AND F.id IN ($filiaisString) ";
        }

        $filiaisString .= "GROUP BY GF.nome_fantasia, GF.id ";


        /*      $file = fopen('queryTituloPagar', 'w');
          fwrite($file, $query);
          fclose($file); */

        $qtdTotal = count(Yii::app()->db->createCommand($query)->queryAll());

        $query .= "LIMIT $start, 10";

        $resultado = Yii::app()->db->createCommand($query)->queryAll();

        return [$resultado, $qtdTotal];
    }

    public function getValoresTotaisPagos($start) {

        $filiais = [];
        $filiaisString = "";

        if (Yii::app()->session['usuario']->tipo_id == 11) {

            foreach (Yii::app()->session['usuario']->adminParceiros as $f) {
                if ($f->habilitado) {
                    $filiais[] = $f->parceiro->id;
                }
            }

            if (!empty($filiais)) {
                $filiaisString = join(',', $filiais);
            }
        }

        $query = "SELECT	COALESCE(GF.nome_fantasia  , 'Sem Grupo'  ) AS Grupo   ,
                              COALESCE(GF.id             , 0            ) AS idGrupo ,
                              SUM(P.valor)                                AS ValorTotal

                   FROM                Parcela              AS P
                   INNER         JOIN  Baixa                AS Ba ON Ba.baixado		AND Ba.Parcela_id			  =  P.id
                   INNER         JOIN  Titulo               AS T  ON  T.habilitado  AND  P.Titulo_id              =  T.id
                   INNER         JOIN  Proposta             AS Pr ON Pr.habilitado  AND  T.Proposta_id            = Pr.id
                   INNER         JOIN  Analise_de_Credito   AS AC ON AC.habilitado  AND Pr.Analise_de_Credito_id  = AC.id
                   INNER         JOIN  Filial               AS F  ON  F.habilitado  AND AC.Filial_id              =  F.id
                   LEFT    OUTER JOIN  NucleoFiliais        AS NF ON NF.habilitado  AND  F.NucleoFiliais_id       = NF.id
                   LEFT    OUTER JOIN  GrupoFiliais         AS GF ON GF.habilitado  AND NF.GrupoFiliais_id        = GF.id

                   WHERE P.habilitado AND T.NaturezaTitulo_id = 2 ";


        if (!empty($filiaisString)) {
            $query .= " AND F.id IN ($filiaisString) ";
        }

        $query .= "GROUP BY GF.nome_fantasia, GF.id ";


        /*      $file = fopen('queryTituloPagar', 'w');
          fwrite($file, $query);
          fclose($file); */

        $qtdTotal = count(Yii::app()->db->createCommand($query)->queryAll());

        $query .= "LIMIT $start, 10";

        $resultado = Yii::app()->db->createCommand($query)->queryAll();

        return [$resultado, $qtdTotal];
    }

    public function getValoresTotaisPagosNucleo($idGrupo) {

        $query = "SELECT   COALESCE(NF.nome  , 'Sem Núcleo' )  AS Nucleo      ,
                           COALESCE(NF.id    , 0            )  AS idNucleo    ,
                           SUM(P.valor)                        AS ValorTotal

                   FROM                Parcela              AS P
                   INNER         JOIN  Baixa                AS Ba ON Ba.baixado		AND Ba.Parcela_id			  =  P.id
                   INNER         JOIN  Titulo               AS T  ON  T.habilitado  AND  P.Titulo_id              =  T.id
                   INNER         JOIN  Proposta             AS Pr ON Pr.habilitado  AND  T.Proposta_id            = Pr.id
                   INNER         JOIN  Analise_de_Credito   AS AC ON AC.habilitado  AND Pr.Analise_de_Credito_id  = AC.id
                   INNER         JOIN  Filial               AS F  ON  F.habilitado  AND AC.Filial_id              =  F.id
                   LEFT    OUTER JOIN  NucleoFiliais        AS NF ON NF.habilitado  AND  F.NucleoFiliais_id       = NF.id

                   WHERE P.habilitado AND T.NaturezaTitulo_id = 2 AND NF.GrupoFiliais_id = $idGrupo

                   GROUP BY NF.nome, NF.id ";


        /*      $file = fopen('queryTituloPagar', 'w');
          fwrite($file, $query);
          fclose($file);

          $qtdTotal = count(Yii::app()->db->createCommand($query)->queryAll());

          $query .= "LIMIT $start, 10"; */

        $resultado = Yii::app()->db->createCommand($query)->queryAll();

        return $resultado;
    }

    public function getBorderos($start, $buscaParceiro, $buscaData, $buscaCodigo) {
        //      $criteria   = new CDbCriteria ;

        $query = "SELECT   B.id                       ,  B.dataCriacao                      ,   "
            . "         B.destinatario                ,  SB.status                          ,   "
            . "         B.criadoPor    AS responsavel ,  U.nome_utilizador                  ,   "
            . "         ROUND(SUM((Pr.valor - Pr.valor_entrada) - ((Pr.valor - Pr.valor_entrada) * (FA.porcentagem_retencao/100))),2) AS valor, "
            . "         RD.codigo          AS documentacao      "
            . "         "
            . "FROM 		Bordero                   AS B                             "
            . "INNER 	JOIN	StatusDoBordero           AS SB  ON   B.Status       = SB.id "
            . "LEFT 	JOIN	RecebimentoDeDocumentacao AS RD  ON  RD.Bordero      =  B.id AND  RD.habilitado "
            . "INNER 	JOIN 	ItemDoBordero             AS IB  ON  IB.Bordero      =  B.id AND  IB.habilitado "
            . "INNER    JOIN	StatusItemDoBordero       AS SI  ON  IB.Status       = SI.id  "
            . "INNER	JOIN	Filial                    AS F   ON   B.destinatario =  F.id AND   F.habilitado "
            . "INNER    JOIN	Filial_has_Endereco       AS FhE ON FhE.Filial_id    =  F.id AND FhE.habilitado "
            . "INNER    JOIN	Endereco                  AS E   ON FhE.Endereco_id  =  E.id AND   E.habilitado "
            . "LEFT	JOIN	Usuario                   AS U   ON  B.criadoPor     =  U.id  "
            . "LEFT     JOIN	Proposta                  AS Pr  ON IB.Proposta_id   = Pr.id AND  Pr.habilitado "
            . "LEFT     JOIN 	Titulo                    AS T   ON  T.Proposta_id   = Pr.id AND   T.habilitado AND T.NaturezaTitulo_id = 2 "
            . "LEFT     JOIN	Parcela                   AS P   ON  P.Titulo_id     =  T.id AND   P.habilitado "
            . "INNER JOIN Tabela_Cotacao                  AS TC ON TC.id             = Pr.Tabela_id                 "
            . "INNER JOIN Fator                           AS FA ON FA.Tabela_Cotacao_id = TC.id AND FA.carencia = Pr.carencia AND FA.parcela = Pr.qtd_parcelas AND FA.habilitado "
            . "WHERE B.habilitado AND B.Status = 1 ";

        if ($buscaParceiro !== null && !empty($buscaParceiro)) {
            $query .= "AND UPPER(CONCAT(F.nome_fantasia, ' ', E.cidade, ' ', E.uf)) LIKE '%" . strtoupper(trim($buscaParceiro)) . "%' ";
        }

        if ($buscaData !== null && !empty($buscaData)) {
            //         echo 'eita';

            $query .= "AND LEFT(B.dataCriacao,10) = '" . trim($buscaData) . "' ";
        }

        if ($buscaCodigo !== null && !empty($buscaCodigo)) {
            //         echo 'eita';

            $query .= "AND RD.codigo LIKE '%" . strtoupper(trim($buscaCodigo)) . "%' ";
        }

        $query .= "GROUP BY B.dataCriacao  , B.destinatario, SB.status, B.criadoPor "
            . "ORDER BY B.id DESC ";

        $qtdTotal = count(Yii::app()->db->createCommand($query)->queryAll());

        $query .= "LIMIT $start, 10";

        $resultado = Yii::app()->db->createCommand($query)->queryAll();

        return [$resultado, $qtdTotal, $query];
    }

    public function getBorderosPagos($start, $buscaParceiro, $buscaData, $buscaCodigo, $idGrupo, $idLote, $nucleosMarcados) {

        $filiais = [];
        $filiaisString = "";

        if (Yii::app()->session['usuario']->tipo_id == 11) {

            foreach (Yii::app()->session['usuario']->adminParceiros as $f) {
                if ($f->habilitado) {
                    $filiais[] = $f->parceiro->id;
                }
            }

            if (!empty($filiais)) {
                $filiaisString = join(',', $filiais);
            }
        }

        if (isset($nucleosMarcados) && !empty($nucleosMarcados)) {
            $nucleos = join(",", $nucleosMarcados);
        }

        $query = "SELECT   B.id                         ,  B.dataCriacao                 ,  "
            . "         B.destinatario                  ,  SB.status                     ,  "
            . "         B.criadoPor    AS responsavel   ,  U.nome_utilizador             ,  "
            . "         ROUND(SUM((Pr.valor - Pr.valor_entrada) - ((Pr.valor - Pr.valor_entrada) * (FA.porcentagem_retencao/100))),2) AS valor, "
            . "RD.codigo                         "
            . "FROM 		Bordero                   AS B                             "
            . "INNER 	JOIN	StatusDoBordero           AS SB  ON   B.Status              = SB.id "
            . "INNER 	JOIN	Lote_has_Bordero          AS LhB ON LhB.Bordero_id          =  B.id AND LhB.habilitado "
            . "LEFT 	JOIN	RecebimentoDeDocumentacao AS RD  ON  RD.Bordero             =  B.id AND  RD.habilitado "
            . "INNER 	JOIN 	ItemDoBordero             AS IB  ON  IB.Bordero             =  B.id AND  IB.habilitado "
            . "INNER	JOIN	StatusItemDoBordero       AS SI  ON  IB.Status              = SI.id "
            . "INNER	JOIN	Filial                    AS F   ON   B.destinatario        =  F.id AND   F.habilitado "
            . "INNER	JOIN	NucleoFiliais             AS NF  ON   F.NucleoFiliais_id    = NF.id "
            . "INNER    JOIN	Filial_has_Endereco       AS FhE ON FhE.Filial_id           =  F.id AND FhE.habilitado "
            . "INNER    JOIN	Endereco                  AS E   ON FhE.Endereco_id         =  E.id AND   E.habilitado "
            . "INNER	JOIN	Usuario                   AS U   ON  B.criadoPor            =  U.id "
            . "INNER 	JOIN	Proposta                  AS Pr  ON IB.Proposta_id          = Pr.id AND  Pr.habilitado "
            . "INNER	JOIN 	Titulo                    AS T   ON  T.Proposta_id          = Pr.id AND   T.habilitado AND T.NaturezaTitulo_id = 2 "
            . "INNER 	JOIN	Parcela                   AS P   ON  P.Titulo_id            =  T.id AND   P.habilitado "
//            . "INNER 	JOIN	Baixa                     AS Ba  ON Ba.Parcela_id           =  P.id AND  Ba.baixado "
            . "INNER JOIN Tabela_Cotacao                  AS TC ON TC.id             = Pr.Tabela_id                 "
            . "INNER JOIN Fator                           AS FA ON FA.Tabela_Cotacao_id = TC.id AND FA.carencia = Pr.carencia AND FA.parcela = Pr.qtd_parcelas AND FA.habilitado "
            . "WHERE B.habilitado AND B.Status = 4 AND B.habilitado AND "
            .   "             EXISTS(SELECT * FROM Baixa AS Ba WHERE Ba.baixado AND Ba.Parcela_id =  P.id ) ";

        if ($buscaParceiro !== null && !empty($buscaParceiro)) {
            $query .= "AND UPPER(CONCAT(F.nome_fantasia, ' ', E.cidade, ' ', E.uf)) LIKE '%" . strtoupper(trim($buscaParceiro)) . "%' ";
        }

        if ($buscaData !== null && !empty($buscaData)) {

            $query .= "AND LEFT(B.dataCriacao,10) = '" . trim($buscaData) . "' ";
        }

        if ($buscaCodigo !== null && !empty($buscaCodigo)) {

            $query .= "AND RD.codigo LIKE '%" . strtoupper(trim($buscaCodigo)) . "%' ";
        }

        if ($idLote > "0") {
            $query .= "AND LhB.Lote_id = $idLote ";
        } else {
            if ($idGrupo > "0") {

                $query .= " AND NF.GrupoFiliais_id = $idGrupo ";

                if (!empty($nucleos)) {
                    $query .= " AND NF.id in ($nucleos) ";
                }
            }
        }

        if (!empty($filiaisString)) {
            $query .= " AND F.id IN ($filiaisString) ";
        }

        $query .= "GROUP BY RD.codigo, B.id, B.dataCriacao  , B.destinatario, SB.status, B.criadoPor, U.nome_utilizador "
            . "ORDER BY B.id DESC ";

        //        echo $query;

        $qtdTotal = count(Yii::app()->db->createCommand($query)->queryAll());

        $query .= "LIMIT $start, 10";

        $resultado = Yii::app()->db->createCommand($query)->queryAll();

        return [$resultado, $qtdTotal,$query];
    }

    public function getLotesPagamento($start) 
    {

        $filiais = [];
        $filiaisString = "";
        $valorTotal = 0;
        $qtdTotal = 0;

        if (Yii::app()->session['usuario']->tipo_id == 11) {

            foreach (Yii::app()->session['usuario']->adminParceiros as $f) {
                if ($f->habilitado) {
                    $filiais[] = $f->parceiro->id;
                }
            }

            if (!empty($filiais)) {
                $filiaisString = join(',', $filiais);
            }
        }

        $queryTotal = "SELECT COUNT(DISTINCT LP.id) AS QTD "
            . "FROM        LotePagamento                          AS LP "
            . "INNER JOIN  Usuario                                AS  U ON  U.id          = LP.Usuario_id                "
//            . "INNER JOIN  LotePagamento_has_StatusLotePagamento  AS LS ON LP.id          = LS.LotePagamento_id         AND LS.habilitado "
            . "INNER JOIN  Lote_has_Bordero                       AS LB ON LP.id          = LB.Lote_id                  AND LB.habilitado "
            . "INNER JOIN  ItemDoBordero                          AS IB ON LB.Bordero_id  = IB.Bordero                  AND IB.habilitado "
            . "INNER JOIN  Proposta                               AS Pr ON Pr.id          = IB.Proposta_id AND Pr.habilitado "
            . "INNER JOIN  Analise_de_Credito                     AS AC ON AC.id          = Pr.Analise_de_Credito_id "
            . "WHERE LP.habilitado ";

        if (!empty($filiaisString)) {
            $queryTotal .= " AND AC.Filial_id IN ($filiaisString) ";
        }

        $queryTotal .=  " AND ( SELECT MAX(LS.StatusLotePagamento_id)                       "
                    .   "       FROM LotePagamento_has_StatusLotePagamento  AS LS           "
                    .   "       WHERE LP.id = LS.LotePagamento_id AND LS.habilitado ) = 1   ";

        $resultado = Yii::app()->db->createCommand($queryTotal)->queryRow();

        $qtdTotal = $resultado["QTD"];

        $queryVlrTotal = "SELECT ROUND(SUM((Pr.valor - Pr.valor_entrada) - ((Pr.valor - Pr.valor_entrada) * (FA.porcentagem_retencao/100))),2) AS VlrTotal  "
            . "FROM        LotePagamento                          AS LP "
            . "INNER JOIN  Usuario                                AS  U ON  U.id          = LP.Usuario_id                "
//            . "INNER JOIN  LotePagamento_has_StatusLotePagamento  AS LS ON LP.id          = LS.LotePagamento_id         AND LS.habilitado "
            . "INNER JOIN  Lote_has_Bordero                       AS LB ON LP.id          = LB.Lote_id                  AND LB.habilitado "
            . "INNER JOIN  ItemDoBordero                          AS IB ON LB.Bordero_id  = IB.Bordero                  AND IB.habilitado "
            . "INNER JOIN  Proposta                               AS Pr ON Pr.id          = IB.Proposta_id AND Pr.habilitado "
            . "INNER JOIN  Analise_de_Credito                     AS AC ON AC.id          = Pr.Analise_de_Credito_id "
            . "INNER JOIN Tabela_Cotacao                          AS TC ON TC.id          = Pr.Tabela_id                 "
            . "INNER JOIN Fator                                   AS FA ON FA.Tabela_Cotacao_id = TC.id AND FA.carencia = Pr.carencia AND FA.parcela = Pr.qtd_parcelas AND FA.habilitado "
            . "WHERE LP.habilitado ";

        if (!empty($filiaisString)) {
            $queryVlrTotal .= " AND AC.Filial_id IN ($filiaisString) ";
        }

        $queryVlrTotal .=  " AND ( SELECT MAX(LS.StatusLotePagamento_id)                       "
                    .   "       FROM LotePagamento_has_StatusLotePagamento  AS LS           "
                    .   "       WHERE LP.id = LS.LotePagamento_id AND LS.habilitado ) = 1   ";

        $resultado = Yii::app()->db->createCommand($queryVlrTotal)->queryRow();

        $valorTotal = $resultado["VlrTotal"];

        $query = "SELECT "
            . "LP.id, LP.data_cadastro, NC.nome AS Nucleo, U.nome_utilizador AS Usuario, "
            . "COUNT(DISTINCT LB.id) AS QTDLB, NC.id AS idNucleo, "
            . "ROUND(SUM((Pr.valor - Pr.valor_entrada) - ((Pr.valor - Pr.valor_entrada) * (FA.porcentagem_retencao/100))),2) AS Valor "
            . "FROM        LotePagamento                          AS LP "
            . "INNER JOIN  Usuario                                AS  U ON  U.id          = LP.Usuario_id                "
            . "INNER JOIN  NucleoFiliais                          AS NC ON NC.id          = LP.NucleoFiliais_id         AND NC.habilitado "
            . "INNER JOIN  Lote_has_Bordero                       AS LB ON LP.id          = LB.Lote_id                  AND LB.habilitado "
            . "INNER JOIN  ItemDoBordero                          AS IB ON LB.Bordero_id  = IB.Bordero                  AND IB.habilitado "
            //. "INNER JOIN  Titulo                                 AS  T ON  T.Proposta_id = IB.Proposta_id              AND  T.habilitado "
            //. "INNER JOIN  Parcela                                AS  P ON  T.id          =  P.Titulo_id                AND  P.habilitado "
            . "INNER JOIN  Proposta                               AS Pr ON Pr.id          = IB.Proposta_id AND Pr.habilitado "
            . "INNER JOIN  Analise_de_Credito                     AS AC ON AC.id          = Pr.Analise_de_Credito_id "
            . "INNER JOIN Tabela_Cotacao                          AS TC ON TC.id          = Pr.Tabela_id                 "
            . "INNER JOIN Fator                                   AS FA ON FA.Tabela_Cotacao_id = TC.id AND FA.carencia = Pr.carencia AND FA.parcela = Pr.qtd_parcelas AND FA.habilitado "
            . "WHERE LP.habilitado ";

        $query  .=  " AND ( SELECT MAX(LS.StatusLotePagamento_id)                       "
                .   "       FROM LotePagamento_has_StatusLotePagamento  AS LS           "
                .   "       WHERE LP.id = LS.LotePagamento_id AND LS.habilitado ) = 1   ";

        if (!empty($filiaisString)) {
            $query .= " AND AC.Filial_id IN ($filiaisString) ";
        }

        $query .= "GROUP BY LP.id, NC.nome, U.nome_utilizador ";


/*        $file = fopen('queryLotes', 'w');
          fwrite($file, $query . chr(13) . chr(10) . chr(13) . chr(10) . $queryTotal . chr(13) . chr(10) . chr(13) . chr(10) . $queryVlrTotal);
          fclose($file);*/

        /*$qtdTotal = count(Yii::app()->db->createCommand($query)->queryAll());

        foreach (Yii::app()->db->createCommand($query)->queryAll() as $r) {
            $valorTotal += $r['Valor'];
        }*/


        $query .= "LIMIT $start, 10";

        $resultado = Yii::app()->db->createCommand($query)->queryAll();

        return [$resultado, $qtdTotal, $valorTotal];
    }

    public function getLotesPagamentoPagos($start, $search, $dataBaixa, $nucleosMarcados, $idGrupo, $imagem, $fisico, $dataFiltro) 
    {
        
        //      $criteria   = new CDbCriteria ;

        $busca = "";
        //$dataFiltro = "";
        $nucleos = "";

        $filiais = [];
        $filiaisString = "";

        if (Yii::app()->session['usuario']->tipo_id == 11) {

            foreach (Yii::app()->session['usuario']->adminParceiros as $f) {
                if ($f->habilitado) {
                    $filiais[] = $f->parceiro->id;
                }
            }

            if (!empty($filiais)) {
                $filiaisString = join(',', $filiais);
            }
        }

        if (isset($search) && isset($search['value']) && !empty($search['value'])) {
            $busca = $search['value'];
        }

        if (isset($nucleosMarcados) && !empty($nucleosMarcados)) {
            $nucleos = join(",", $nucleosMarcados);
        }

//        $query = "SELECT
//
//                    SUM(SQ.Valor) AS Valor, SQ.pagoImagem, SQ.id, SQ.data_cadastro, SQ.Nucleo, SQ.Usuario, COUNT(DISTINCT SQ.QTD) AS QTDLB FROM
//                    (
//                        SELECT
//                        DISTINCT Ba.Parcela_id, IB.pagoImagem, LP.id, LP.data_cadastro, NC.nome AS Nucleo, U.nome_utilizador AS Usuario, LB.id AS QTD,
//                        ROUND(((Pr.valor - Pr.valor_entrada) - ((Pr.valor - Pr.valor_entrada) * (FA.porcentagem_retencao/100))),2) AS Valor
//                        FROM        LotePagamento                          AS LP
//                        INNER JOIN  LotePagamento_has_StatusLotePagamento  AS LS ON LP.id          = LS.LotePagamento_id         AND LS.habilitado
//                        INNER JOIN  Usuario                                AS U  ON  U.id          = LP.Usuario_id               AND  U.habilitado
//                        INNER JOIN  NucleoFiliais                          AS NC ON NC.id          = LP.NucleoFiliais_id         AND NC.habilitado
//                        INNER JOIN  Lote_has_Bordero                       AS LB ON LP.id          = LB.Lote_id                  AND LB.habilitado
//                        INNER JOIN  ItemDoBordero                          AS IB ON LB.Bordero_id  = IB.Bordero                  AND IB.habilitado
//                        INNER JOIN  Titulo                                 AS  T ON  T.Proposta_id = IB.Proposta_id              AND  T.habilitado
//                        INNER JOIN  Parcela                                AS  P ON  T.id          =  P.Titulo_id                AND  P.habilitado
//                        INNER JOIN  Baixa                                  AS Ba ON  P.id          = Ba.Parcela_id               AND Ba.baixado
//                        INNER JOIN  Proposta                               AS Pr ON Pr.id          =  T.Proposta_id              AND Pr.habilitado
//                        INNER JOIN  Analise_de_Credito                     AS AC ON AC.id          = Pr.Analise_de_Credito_id    AND AC.habilitado
//                        INNER JOIN  Tabela_Cotacao                         AS TC ON TC.id          = Pr.Tabela_id
//                        INNER JOIN  Fator                                  AS FA ON FA.Tabela_Cotacao_id = TC.id AND FA.carencia = Pr.carencia AND FA.parcela = Pr.qtd_parcelas AND FA.habilitado
//                        WHERE LP.habilitado AND T.NaturezaTitulo_id = 2 AND LS.StatusLotePagamento_id = 2
//                    ) SQ GROUP BY SQ.pagoImagem, SQ.id, SQ.Nucleo, SQ.Usuario";

        $queryTotal = "SELECT       COUNT(DISTINCT LP.id)   AS QTD                                                                                                                                  "
                    . "FROM         LotePagamento           AS LP                                                                                                                                   "
                    . "INNER JOIN   NucleoFiliais           AS NC ON NC.id          = LP.NucleoFiliais_id       AND NC.habilitado                                                                   "
                    . "INNER JOIN   Usuario                 AS  U ON  U.id          = LP.Usuario_id                                                                                "
                    . "INNER JOIN   Lote_has_Bordero        AS LB ON LP.id          = LB.Lote_id                AND LB.habilitado                                                                   "
                    . "INNER JOIN   ItemDoBordero           AS IB ON LB.Bordero_id  = IB.Bordero                AND IB.habilitado                                                                   "
                    . "INNER JOIN   Proposta                AS Pr ON Pr.id          = IB.Proposta_id            AND Pr.habilitado                                                                   "
                    . "INNER JOIN   Titulo                  AS  T ON  T.Proposta_id = IB.Proposta_id            AND  T.habilitado AND T.NaturezaTitulo_id = 2                                       "
                    . "INNER JOIN   Parcela                 AS  P ON  T.id          =  P.Titulo_id              AND  P.habilitado                                                                   "
                    . "INNER JOIN   Baixa                   AS Ba ON  P.id          = Ba.Parcela_id             AND Ba.baixado                                                                      "
                    . "INNER JOIN   Analise_de_Credito      AS AC ON AC.id          = Pr.Analise_de_Credito_id                                                                                      "
//                    . "INNER JOIN   Tabela_Cotacao          AS TC ON TC.id          = Pr.Tabela_id                                                                                                  "
//                    . "INNER JOIN   Fator                   AS FA ON TC.id          = FA.Tabela_Cotacao_id      AND FA.carencia = Pr.carencia AND FA.parcela = Pr.qtd_parcelas AND FA.habilitado    "
                    . "WHERE LP.habilitado                                                                                          ";

        if ($idGrupo > 0) 
        {

            $queryTotal .= " AND NC.GrupoFiliais_id = $idGrupo ";

            if (!empty($nucleos)) 
            {
                $queryTotal .= " AND NC.id in ($nucleos) ";
            }
            
        }

        $queryTotal .=  " AND ( SELECT MAX(LS.StatusLotePagamento_id)                       "
                    .   "       FROM LotePagamento_has_StatusLotePagamento  AS LS           "
                    .   "       WHERE LP.id = LS.LotePagamento_id AND LS.habilitado ) = 2   ";

        if (!empty($filiaisString)) {
            $queryTotal .= " AND AC.Filial_id IN ($filiaisString) ";
        }

        $resultado = Yii::app()->db->createCommand($queryTotal)->queryRow();

        $qtdTotal = $resultado["QTD"];
        
        //qtdFiltrado
        $queryFiltr = "SELECT       COUNT(DISTINCT LP.id)   AS QTD                                                                                                                                  "
                    . "FROM         LotePagamento           AS LP                                                                                                                                   "
                    . "INNER JOIN   NucleoFiliais           AS NC ON NC.id          = LP.NucleoFiliais_id       AND NC.habilitado                                                                 "
                    . "INNER JOIN   Usuario                 AS  U ON  U.id          = LP.Usuario_id                                                                                "
                    . "INNER JOIN   Lote_has_Bordero        AS LB ON LP.id          = LB.Lote_id                AND LB.habilitado                                                                   "
                    . "INNER JOIN   ItemDoBordero           AS IB ON LB.Bordero_id  = IB.Bordero                AND IB.habilitado                                                                   "
                    . "INNER JOIN   Proposta                AS Pr ON Pr.id          = IB.Proposta_id            AND Pr.habilitado                                                                   "
                    . "INNER JOIN   Titulo                  AS  T ON  T.Proposta_id = IB.Proposta_id            AND  T.habilitado AND T.NaturezaTitulo_id = 2                                       "
                    . "INNER JOIN   Parcela                 AS  P ON  T.id          =  P.Titulo_id              AND  P.habilitado                                                                   "
                    . "INNER JOIN   Baixa                   AS Ba ON  P.id          = Ba.Parcela_id             AND Ba.baixado                                                                      "
                    . "INNER JOIN   Analise_de_Credito      AS AC ON AC.id          = Pr.Analise_de_Credito_id                                                                                      "
                    . "WHERE LP.habilitado                                                                                          ";

        if (!empty($busca)) {
            $queryFiltr .= " AND ( ((UPPER(NC.nome) LIKE '%" . strtoupper($busca) . "%') AND (LPAD(TRIM(CAST(LP.id AS CHAR(50))),10,'0') LIKE '%$busca%') )  "
                . "  OR   ((UPPER(NC.nome) LIKE '%" . strtoupper($busca) . "%')  OR (LPAD(TRIM(CAST(LP.id AS CHAR(50))),10,'0') LIKE '%$busca%') ) )";
        }

        if (!empty($dataFiltro)) {
            $queryFiltr .= " AND LEFT(LP.data_cadastro,10) = '$dataFiltro' ";
        }
        else
        {
            $queryFiltr .= " AND 'porra' = 'porra' ";
        }

        if ($idGrupo > 0) {

            $queryFiltr .= " AND NC.GrupoFiliais_id = $idGrupo ";

            if (!empty($nucleos)) {
                $queryFiltr .= " AND NC.id in ($nucleos) ";
            }
        }

        if ($imagem == 1 && $fisico == 1) {
            $queryFiltr .= " AND (IB.pagoImagem = 0 OR IB.pagoImagem = 1 OR IB.pagoImagem is null) ";
        } else {
            if ($imagem == 1) {
                $queryFiltr .= " AND IB.pagoImagem = 1 ";
            }

            if ($fisico == 1) {
                $queryFiltr .= " AND (IB.pagoImagem = 0 OR IB.pagoImagem is null) ";
            }
        }

        $queryFiltr .=  " AND ( SELECT MAX(LS.StatusLotePagamento_id)                       "
                    .   "       FROM LotePagamento_has_StatusLotePagamento  AS LS           "
                    .   "       WHERE LP.id = LS.LotePagamento_id AND LS.habilitado ) = 2   ";

        if (!empty($filiaisString)) {
            $queryFiltr .= " AND AC.Filial_id IN ($filiaisString) ";
        }

        $resultado = Yii::app()->db->createCommand($queryFiltr)->queryRow();

        $qtdFiltrada = $resultado["QTD"];

        //registros

        $query  = "SELECT   LP.id                           , LP.data_cadastro                  , NC.nome   AS Nucleo   , "
                . "         U.nome_utilizador   AS Usuario  , COUNT(DISTINCT LB.id) AS QTDLB    , NC.id     AS idNucleo , "
                
                . "ROUND(SUM((Pr.valor - Pr.valor_entrada) - ((Pr.valor - Pr.valor_entrada) * (FA.porcentagem_retencao/100))),2) AS Valor, "
                . "COUNT(DISTINCT LB.id) AS QTDLB, "
                
                . "IB.pagoImagem AS pagoImagem "
                
                . "FROM         LotePagamento       AS LP "
                . "INNER JOIN   Usuario             AS  U ON  U.id          = LP.Usuario_id                "
                . "INNER JOIN   NucleoFiliais       AS NC ON NC.id          = LP.NucleoFiliais_id       AND NC.habilitado   "
                . "INNER JOIN   Lote_has_Bordero    AS LB ON LP.id          = LB.Lote_id                AND LB.habilitado   "
                . "INNER JOIN   ItemDoBordero       AS IB ON LB.Bordero_id  = IB.Bordero                AND IB.habilitado   "
                . "INNER JOIN   Titulo              AS  T ON  T.Proposta_id = IB.Proposta_id            AND  T.habilitado AND T.NaturezaTitulo_id = 2                                       "
                . "INNER JOIN   Parcela             AS  P ON  T.id          =  P.Titulo_id              AND  P.habilitado   "
//                . "INNER JOIN   Baixa               AS Ba ON  P.id          = Ba.Parcela_id             AND Ba.baixado                                                                      "
                . "INNER JOIN   Proposta            AS Pr ON Pr.id          = IB.Proposta_id            AND Pr.habilitado   "
                . "INNER JOIN   Analise_de_Credito  AS AC ON AC.id          = Pr.Analise_de_Credito_id                      "
                . "INNER JOIN   Tabela_Cotacao      AS TC ON TC.id          = Pr.Tabela_id                                  "
                . "INNER JOIN   Fator               AS FA ON TC.id          = FA.Tabela_Cotacao_id AND FA.carencia = Pr.carencia AND FA.parcela = Pr.qtd_parcelas AND FA.habilitado "
                . "WHERE LP.habilitado ";

        $query  .=  " AND ( SELECT MAX(LS.StatusLotePagamento_id)                                               "
                .   "       FROM LotePagamento_has_StatusLotePagamento  AS LS                                   "
                .   "       WHERE LP.id = LS.LotePagamento_id AND LS.habilitado ) = 2 AND                       "
                .   "             EXISTS(SELECT * FROM Baixa AS Ba WHERE Ba.baixado AND Ba.Parcela_id =  P.id ) ";
        
//        $query = "SELECT
//
//                    SUM(SQ.Valor) AS Valor, SQ.pagoImagem, SQ.id, SQ.data_cadastro, SQ.Nucleo, SQ.Usuario, COUNT(DISTINCT SQ.QTD) AS QTDLB FROM
//                    (
//                        SELECT
//                        DISTINCT Ba.Parcela_id, IB.pagoImagem, LP.id, LP.data_cadastro, NC.nome AS Nucleo, U.nome_utilizador AS Usuario, LB.id AS QTD,
//                        ROUND(((Pr.valor - Pr.valor_entrada) - ((Pr.valor - Pr.valor_entrada) * (FA.porcentagem_retencao/100))),2) AS Valor
//                        FROM        LotePagamento                          AS LP
//                        INNER JOIN  LotePagamento_has_StatusLotePagamento  AS LS ON LP.id          = LS.LotePagamento_id         AND LS.habilitado
//                        INNER JOIN  Usuario                                AS U  ON  U.id          = LP.Usuario_id               AND  U.habilitado
//                        INNER JOIN  NucleoFiliais                          AS NC ON NC.id          = LP.NucleoFiliais_id         AND NC.habilitado
//                        INNER JOIN  Lote_has_Bordero                       AS LB ON LP.id          = LB.Lote_id                  AND LB.habilitado
//                        INNER JOIN  ItemDoBordero                          AS IB ON LB.Bordero_id  = IB.Bordero                  AND IB.habilitado
//                        INNER JOIN  Titulo                                 AS  T ON  T.Proposta_id = IB.Proposta_id              AND  T.habilitado
//                        INNER JOIN  Parcela                                AS  P ON  T.id          =  P.Titulo_id                AND  P.habilitado
//                        INNER JOIN  Baixa                                  AS Ba ON  P.id          = Ba.Parcela_id               AND Ba.baixado
//                        INNER JOIN  Proposta                               AS Pr ON Pr.id          =  T.Proposta_id              AND Pr.habilitado
//                        INNER JOIN  Analise_de_Credito                     AS AC ON AC.id          = Pr.Analise_de_Credito_id    AND AC.habilitado
//                        INNER JOIN  Tabela_Cotacao                         AS TC ON TC.id          = Pr.Tabela_id
//                        INNER JOIN  Fator                                  AS FA ON FA.Tabela_Cotacao_id = TC.id AND FA.carencia = Pr.carencia AND FA.parcela = Pr.qtd_parcelas AND FA.habilitado
//                        WHERE LP.habilitado AND T.NaturezaTitulo_id = 2 AND LS.StatusLotePagamento_id = 2 ";

        if (!empty($busca)) {
            $query .= " AND ( ((UPPER(NC.nome) LIKE '%" . strtoupper($busca) . "%') AND (LPAD(TRIM(CAST(LP.id AS CHAR(50))),10,'0') LIKE '%$busca%') )  "
                . "  OR   ((UPPER(NC.nome) LIKE '%" . strtoupper($busca) . "%')  OR (LPAD(TRIM(CAST(LP.id AS CHAR(50))),10,'0') LIKE '%$busca%') ) )";
        }

        if (!empty($dataFiltro)) {
            $query .= " AND LEFT(LP.data_cadastro,10) = '$dataFiltro' ";
        }

        if ($idGrupo > 0) {

            $query .= " AND NC.GrupoFiliais_id = $idGrupo ";

            if (!empty($nucleos)) {
                $query .= " AND NC.id in ($nucleos) ";
            }
        }

        if (!empty($filiaisString)) {
            $query .= " AND AC.Filial_id IN ($filiaisString) ";
        }

        if ($imagem == 1 && $fisico == 1) {
            $query .= " AND (IB.pagoImagem = 0 OR IB.pagoImagem = 1 OR IB.pagoImagem is null) ";
        } else {
            if ($imagem == 1) {
                $query .= " AND IB.pagoImagem = 1 ";
            }

            if ($fisico == 1) {
                $query .= " AND (IB.pagoImagem = 0 OR IB.pagoImagem is null) ";
            }
        }

//        $query .= ") SQ GROUP BY SQ.pagoImagem, SQ.id, SQ.Nucleo, SQ.Usuario ";

        //$qtdFiltrada = count(Yii::app()->db->createCommand($query)->queryAll());
        
        $query .= "GROUP BY LP.id, LP.data_cadastro, NC.nome, U.nome_utilizador, NC.id, IB.pagoImagem ";

        $query .= "LIMIT $start, 10";

        //        var_dump($query);

        $resultado = Yii::app()->db->createCommand($query)->queryAll();

        return [$resultado, $qtdTotal, $qtdFiltrada,$query,$queryTotal,$queryFiltr];
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function novo($objHasFPHasCP) {
        if ($this->save()) {

            $carencia = $objHasFPHasCP->formaDePagamentoHasCondicaoDePagamento->carencia;
            $intervalo = $objHasFPHasCP->formaDePagamentoHasCondicaoDePagamento->intervalo;

            for ($i = 0; $i < $objHasFPHasCP->qtdParcelas; $i++) {

                $diasSoma = $carencia + (($i + 1) * $intervalo);

                $vencimento = date('Y-m-d', strtotime("+" . $diasSoma . " days")); //strtotime("+" . $diasSoma . " days",date("d-m-y"));

                $parcela = new Parcela;

                $parcela->Titulo_id = $this->id;
                $parcela->seq = ($i + 1);
                $parcela->valor = ($objHasFPHasCP->valor / $objHasFPHasCP->qtdParcelas);
                $parcela->valor_atual = ($objHasFPHasCP->valor / $objHasFPHasCP->qtdParcelas);
                $parcela->vencimento = $vencimento;

                if (!$parcela->save()) {
                    var_dump($parcela->getErrors());
                }
            }
        }
    }

    public function listar($draw, $offset, $order, $columns) {

        //setlocale(LC_MONETARY, 'it_IT');

        $orderDir = $order[0]['dir'];
        $orderString = "";

        if ($order[0]['column'] == 0) {
            $orderString = "titulo.descricao $orderDir";
        }

        $countTitulos = Titulo::model()->findAll();

        $rows = array();

        $crt = new CDbCriteria();
        $crt->offset = $offset;
        $crt->limit = 10;
        //$crt->with = array('titulo');
        $crt->order = "prefixo $orderDir";


        if (!empty($columns[0]['search']['value']) && !ctype_space($columns[0]['search']['value'])) {
            $crt->addSearchCondition('prefixo', $columns[0]['search']['value']);
        }

        $titulos = Titulo::model()->findAll($crt);

        foreach ($titulos as $titulo) {

            $btn = 'Mostrar';

            $body = '';

            $tableDetalhe = '';

            if ($titulo->compraHasFPHasCPFPhasCP !== null) {
                $objHasFPHasCP = $titulo->compraHasFPHasCPFPhasCP;

                $objComVen = $objHasFPHasCP->compra;

                $natureza = 'Despesa';

                $desComVen = 'Compra';

                $pessoa = $titulo->compraHasFPHasCPFPhasCP->compra->fornecedor;
            } else {
                $objHasFPHasCP = $titulo->vendaHasFPHasCPFPhasCP;

                $objComVen = $objHasFPHasCP->compra;

                $natureza = 'Receita';

                $desComVen = 'Venda';

                $pessoa = $titulo->vendaHasFPHasCPFPhasCP->venda->cliente->pessoa;
            }

            $tableDetalhe = '<table class="table table-striped table-bordered table-hover table-full-width">';
            $tableDetalhe .= '<tr>';

            if ($natureza == "Despesa") {
                $tableDetalhe .= '<th>Fornecedor</th>';
            } else {
                $tableDetalhe .= '<th>Cliente</th>';
            }

            $tableDetalhe .= '<td>';
            $tableDetalhe .= '<table  class="table table-striped table-bordered table-hover table-full-width">';
            $tableDetalhe .= '<tr>';
            $tableDetalhe .= '<th>Nome</th>';
            $tableDetalhe .= '<th>CGC</th>';
            $tableDetalhe .= '</tr>';
            $tableDetalhe .= '<tr>';
            $tableDetalhe .= '<td>' . $pessoa->pessoa->nome . '</td>';
            $tableDetalhe .= '<td>123.456.789-01</td>';
            $tableDetalhe .= '</tr>';
            $tableDetalhe .= '</table>';
            $tableDetalhe .= '</td>';
            $tableDetalhe .= '</tr>';
            $tableDetalhe .= '<tr>';
            $tableDetalhe .= '<th>' . $desComVen . '</th>';
            $tableDetalhe .= '<td>';
            $tableDetalhe .= '<table  class="table table-striped table-bordered table-hover table-full-width">';
            $tableDetalhe .= '<tr>';
            $tableDetalhe .= '<th>Codigo</th>';
            $tableDetalhe .= '<th>Data</th>';
            $tableDetalhe .= '<th>Valor Total</th>';
            $tableDetalhe .= '</tr>';
            $tableDetalhe .= '<tr>';
            $tableDetalhe .= '<td>' . $objComVen->id . '</td>';
            $tableDetalhe .= '<td>' . $objComVen->data . '</td>';
            $tableDetalhe .= '<td>R$ ' . number_format($objComVen->getValorTotal(), 2, ',', '.') . '</td>';
            $tableDetalhe .= '</tr>';
            $tableDetalhe .= '</table>';
            $tableDetalhe .= '</td>';
            $tableDetalhe .= '</tr>';
            $tableDetalhe .= '</table>';

            $formaPagamento = $objHasFPHasCP->formaDePagamentoHasCondicaoDePagamento->formaDePagamento;

            $parcelas = $titulo->listarParcelas();

            $cabec = '<tr>';
            $cabec .= '<th>Parcela</th>';
            $cabec .= '<th>Vencimento</th>';
            $cabec .= '<th>Valor Original</th>';
            $cabec .= '<th>Valor a Pagar</th>';
            $cabec .= '<th>Valor Restante</th>';
            $cabec .= '<th></th>';
            $cabec .= '</tr>';

            foreach ($parcelas as $p) {
                $body .= '<tr>';
                $body .= '<td>' . $p->seq . '</td>';
                $body .= '<td>' . date('d/m/Y', strtotime($p->vencimento)) . '</td>';
                $body .= '<td>R$ ' . number_format($p->valor, 2, ',', '.') . '</td>';
                $body .= '<td>0</td>';
                $body .= '<td>R$ ' . number_format($p->valor, 2, ',', '.') . '</td>';
                $body .= '<td></td>';
                $body .= '</tr>';
            }

            $row = array(
                    'natureza' => $natureza,
                    'cliFor' => $pessoa->pessoa->nome,
                    'prefixo' => $titulo->prefixo,
                    'numero' => str_pad($titulo->id, 9, '0', STR_PAD_LEFT),
                    'emissao' => date('d/m/Y', strtotime($titulo->emissao)),
                    'formaPagamento' => $formaPagamento->descricao,
                    'qtdParcelas' => count($parcelas),
                    'valorTotal' => 'R$ ' . number_format($titulo->getValorTotal(), 2, ',', '.'),
                    'detalhes' => array('<b>Detalhes</b>' . $tableDetalhe . '<b>Parcelas</b>' .
                            '<table class="table table-striped table-bordered table-hover table-full-width">'
                            . '<thead>' . $cabec . '</thead>'
                            . '<tbody>' . $body . '</tbody>'
                            . '</table>',
                    ),
            );

            $rows[] = $row;
        }

        return (array(
                "draw" => $draw,
                "recordsTotal" => count($countTitulos),
                "recordsFiltered" => count($countTitulos),
                "data" => $rows
        ));
    }

    /*   public function listar() {

      $rows = array();
      $titulos = Titulo::model()->findAll();

      foreach ($titulos as $titulo) {

      $btn = 'Mostrar';

      if ($titulo->compraHasFPHasCPFPhasCP !== null) {
      $objHasFPHasCP = $titulo->compraHasFPHasCPFPhasCP;
      //$cliFor = 'Fornecedor: ' . $titulo->compraHasFPHasCPFPhasCP->compra->fornecedor->pessoa->nome;
      $cliFor = 'Fornecedor: ' . $titulo->compraHasFPHasCPFPhasCP->compra->fornecedor->nome;
      } else {
      $objHasFPHasCP = $titulo->vendaHasFPHasCPFPhasCP;
      $cliFor = 'Cliente: ' . $titulo->vendaHasFPHasCPFPhasCP->venda->cliente->pessoa->nome;
      }

      $formaPagamento = $objHasFPHasCP->formaDePagamentoHasCondicaoDePagamento->formaDePagamento;

      $parcelas = $titulo->listarParcelas();

      $cabec  =   '<tr>';
      $cabec .=        '<th>Parcela</th>';
      $cabec .=        '<th>Vencimento</th>';
      $cabec .=        '<th>Valor</th>';
      $cabec .=   '</tr>';

      $body =    '';

      foreach ($parcelas as $p)
      {
      $body .=    '<tr>';
      $body .=        '<td>' . $p->seq        . '</td>';
      $body .=        '<td>' . date('d/m/Y',  strtotime($p->vencimento)) . '</td>';
      $body .=        '<td>' . $p->valor      . '</td>';
      $body .=    '</tr>';
      }

      $row = array(
      //                'btn' => $btn,
      'cliFor' => $cliFor,
      'prefixo' => $titulo->prefixo,
      'numero' => str_pad($titulo->id, 9, '0', STR_PAD_LEFT),
      'emissao' => date('d/m/Y',  strtotime($titulo->emissao)),
      'formaPagamento' => $formaPagamento->descricao,
      'qtdParcelas' => count($parcelas),
      'valorTotal'=> $titulo->getValorTotal(),
      'parcelas' => array(
      '<table class="table table-striped table-bordered table-hover table-full-width">'
      . '<thead>' . $cabec . '</thead>'
      . '<tbody>' . $body  . '</tbody>'
      . '</table>',
      ),
      );

      $rows[] = $row;
      }

      var_dump($rows);
      } */

    public function getValorTotal() {
        $valorTotal = 0;

        foreach ($this->listarParcelas() as $parcela) {
            $valorTotal += $parcela->valor;
        }

        return $valorTotal;
    }

}
