<?php

/**
 * This is the model class for table "Titulo_has_Parcela".
 *
 * The followings are the available columns in table 'Titulo_has_Parcela':
 * @property integer $id
 * @property integer $Titulo_id
 * @property integer $Parcela_id
 * @property integer $habilitado
 * @property string $data_cadastro
 *
 * The followings are the available model relations:
 * @property Titulo $titulo
 * @property Parcela $parcela
 */
class TituloHasParcela extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Titulo_has_Parcela';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Titulo_id, Parcela_id', 'required'),
			array('Titulo_id, Parcela_id, habilitado', 'numerical', 'integerOnly'=>true),
			array('data_cadastro', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, Titulo_id, Parcela_id, habilitado, data_cadastro', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'titulo' => array(self::BELONGS_TO, 'Titulo', 'Titulo_id'),
			'parcela' => array(self::BELONGS_TO, 'Parcela', 'Parcela_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Titulo_id' => 'Titulo',
			'Parcela_id' => 'Parcela',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Titulo_id',$this->Titulo_id);
		$criteria->compare('Parcela_id',$this->Parcela_id);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TituloHasParcela the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
