<?php

/**
 * This is the model class for table "Conciliacao".
 *
 * The followings are the available columns in table 'Conciliacao':
 * @property integer $id
 * @property string $data_cadastro
 * @property integer $habilitado
 * @property integer $Usuario_id
 *
 * The followings are the available model relations:
 * @property Usuario $usuario
 * @property ItemConciliacao[] $itemConciliacaos
 */
class Conciliacao extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Conciliacao';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('data_cadastro, habilitado, Usuario_id', 'required'),
            array('habilitado, Usuario_id', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, data_cadastro, habilitado, Usuario_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'usuario' => array(self::BELONGS_TO, 'Usuario', 'Usuario_id'),
            'itemConciliacaos' => array(self::HAS_MANY, 'ItemConciliacao', 'Conciliacao_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'data_cadastro' => 'Data Cadastro',
            'habilitado' => 'Habilitado',
            'Usuario_id' => 'Usuario',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('data_cadastro', $this->data_cadastro, true);
        $criteria->compare('habilitado', $this->habilitado);
        $criteria->compare('Usuario_id', $this->Usuario_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Conciliacao the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function importarArquivo($arquivo) 
    {

        $arrReturn = array(
            'hasErrors' =>  false                           ,
            'msgs'      =>  'Arquivo importado com sucesso' ,
            'type'      =>  'success'
        );

        $_UPCONFIG['dir'        ] = '/var/www/nordeste/importarConciliacaoOMNI/';
        $_UPCONFIG['finalFile'  ] = $_UPCONFIG['dir'] . $arquivo['name']        ;
        
        $nomeArquivo = $arquivo['name'];

        $arquivoTemporario = file($arquivo['tmp_name']);
        
        /* Tenta salvar apenas se o arquivo não existir... */
        if (!file_exists($_UPCONFIG['finalFile'])) {
            /* Tentando salvar o arquivo em disco. */
            if (move_uploaded_file($arquivo['tmp_name'], $_UPCONFIG['finalFile'])) {

                $oldUmask = umask(0);
                chmod($_UPCONFIG['finalFile'], 0755);
                umask($oldUmask);

                /* Carregando o arquivo final */
//                $arquivoRetorno = file($_UPCONFIG['finalFile']);
                
                $retornoSalva = Conciliacao::model()->salvarConciliacao($_UPCONFIG['finalFile']);
                
                $arrReturn['msgs'] .= $retornoSalva['msgs'];
                
            } else {
                
                ob_start();
                var_dump(error_get_last());
                $result = ob_get_clean();
                
                $arrReturn['hasErrors'  ] = true                                                        ;
                $arrReturn['msgs'       ] = "Não foi possível ler o arquivo $nomeArquivo, por favor, "
                                            . "entre em contato com o suporte.". $result                ;
                $arrReturn['type'       ] = 'error'                                                     ;
            }
        } else {
            $arrReturn['hasErrors'  ] = true                    ;
            $arrReturn['msgs'       ] = 'O arquivo já existe.'  ;
            $arrReturn['type'       ] = 'error'                 ;
        }

        return $arrReturn;
    }
    
    public function salvarConciliacao($arquivo)
    {
        
        $posicaoData        = 0;
        $posicaoContrato    = 1;
        $posicaoCodigo      = 2;
        $posicaoNome        = 3;
        $posicaoCGC         = 4;
        $posicaoValor       = 5;
        
        $retorno = "";
        
        $file = fopen($arquivo, 'r');

        if ($file) {
            
            $transaction = Yii::app()->db->beginTransaction();
            
            $conciliacao                = new Conciliacao                       ;
            $conciliacao->Usuario_id    = Yii::app()->session['usuario']->id    ;
            $conciliacao->data_cadastro = date('Y-m-d H:i:s')                   ;
            $conciliacao->habilitado    = 1                                     ;
            
            if(!$conciliacao->save())
            {
                ob_start();
                var_dump($conciliacao->getErrors());
                $result = ob_get_clean();
                
                $retorno = "Erro ao salvar a conciliação. $result"  ;
                $tipoMsg = "error"                                  ;
                
                $transaction->rollBack();
            }
            else
            {
                
                $qtdImportada = 0;

                while (!feof($file)) {

                    // Ler uma linha do arquivo
                    $linha = fgetcsv($file, 0, ";");

                    if (!$linha) {
                        continue;
                    }

                    if (!isset($linha[$posicaoCodigo]) || $linha[$posicaoCodigo] == null || empty($linha[$posicaoCodigo])) 
                    {
                        $retorno .= "Proposta sem código. Contrato: $linha[$posicaoContrato] <br>";
                        $tipoMsg  = 'error'                                                       ;
                        continue;
                    }

                    if(ItemConciliacao::model()->find("habilitado AND codigoProposta = $linha[$posicaoCodigo]") !== null)
                    {
                        
                        $retorno .= "Proposta já importada: $linha[$posicaoCodigo] <br>"    ;
                        $tipoMsg  = 'error'                                                 ;
                        continue;
                    
                    }

                    $date           = str_replace('/', '', trim($linha[$posicaoData]));
                    $dateFormato    = '20' . substr($date, 4, 2) . '-' . substr($date, 2, 2) . '-' . substr($date, 0, 2);
                    $dataProposta   = date('Y-m-d', strtotime($dateFormato));

                    $valorP = str_replace('.', '' , $linha[$posicaoValor] );
                    $valorP = str_replace(',', '.', $valorP             );

                    $cgcCliente = str_replace('.', ''   , $linha[$posicaoCGC]   );
                    $cgcCliente = str_replace('-', ''   , $cgcCliente           );
                    $cgcCliente = str_replace('/', ''   , $cgcCliente           );
                    
                    $proposta = Proposta::model()->find("habilitado AND codigo LIKE '$linha[$posicaoCodigo]'");

                    $itemConciliacao                    = new ItemConciliacao               ;
                    $itemConciliacao->habilitado        = 1                                 ;
                    $itemConciliacao->data_cadastro     = date('Y-m-d H:i:s')               ;
                    $itemConciliacao->codigoContrato    = trim($linha[$posicaoContrato  ])  ;
                    $itemConciliacao->codigoProposta    = trim($linha[$posicaoCodigo    ])  ;
                    $itemConciliacao->nomeCliente       = trim($linha[$posicaoNome      ])  ;
                    $itemConciliacao->cgcCliente        = $cgcCliente                       ;
                    $itemConciliacao->dataProposta      = $dataProposta                     ;
                    $itemConciliacao->valorProposta     = $valorP                           ;
                    $itemConciliacao->Conciliacao_id    = $conciliacao->id                  ;
                    
                    if($proposta !== null)
                    {
                        $itemConciliacao->Proposta_id = $proposta->id;
                    }

                    if(!$itemConciliacao->save())
                    {
                        ob_start();
                        var_dump($itemConciliacao->getErrors());
                        $result = ob_get_clean();
                        
                        $transaction->rollBack();
                        $retorno = "Erro ao importar Proposta: $linha[$posicaoCodigo] " . $result   ;
                        $tipoMsg = 'error'                                                          ;
                        break;
                    }
                    else
                    {
                        $qtdImportada++;
                    }

                }
                
                if($qtdImportada > 0)
                {
                    $transaction->commit()                                      ;
                    
                    $retorno .= "Arquivo $arquivo importado com suceso. <br>"   ;
                    $retorno .= "Quantidade importada: $qtdImportada. <br>"     ;
                    
                    $tipoMsg = 'success';
                }
                
            }

            fclose($file);
        }
        
        $arrayRetorno = [
                            'msgs'  => $retorno ,
                            'type'  => $tipoMsg
                        ];
        
        return  $arrayRetorno;
        
    }

}
