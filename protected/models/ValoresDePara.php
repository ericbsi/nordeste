<?php

/**
 * This is the model class for table "ValoresDePara".
 *
 * The followings are the available columns in table 'ValoresDePara':
 * @property integer $TipoValorDePara_id
 * @property integer $id
 * @property string $conteudoDe
 * @property string $conteudoPara
 * @property string $data_cadastro
 * @property integer $habilitado
 * @property integer $ItensDePara_id
 *
 * The followings are the available model relations:
 * @property ItensDePara $itensDePara
 * @property TipoValorDePara $tipoValorDePara
 */
class ValoresDePara extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ValoresDePara';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TipoValorDePara_id, conteudoDe, conteudoPara, data_cadastro, ItensDePara_id', 'required'),
			array('TipoValorDePara_id, habilitado, ItensDePara_id', 'numerical', 'integerOnly'=>true),
			array('conteudoDe', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('TipoValorDePara_id, id, conteudoDe, conteudoPara, data_cadastro, habilitado, ItensDePara_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'itensDePara' => array(self::BELONGS_TO, 'ItensDePara', 'ItensDePara_id'),
			'tipoValorDePara' => array(self::BELONGS_TO, 'TipoValorDePara', 'TipoValorDePara_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'TipoValorDePara_id' => 'Tipo Valor De Para',
			'id' => 'ID',
			'conteudoDe' => 'Conteudo De',
			'conteudoPara' => 'Conteudo Para',
			'data_cadastro' => 'Data Cadastro',
			'habilitado' => 'Habilitado',
			'ItensDePara_id' => 'Itens De Para',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('TipoValorDePara_id',$this->TipoValorDePara_id);
		$criteria->compare('id',$this->id);
		$criteria->compare('conteudoDe',$this->conteudoDe,true);
		$criteria->compare('conteudoPara',$this->conteudoPara,true);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('ItensDePara_id',$this->ItensDePara_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ValoresDePara the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
