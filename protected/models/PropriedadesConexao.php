<?php

/**
 * This is the model class for table "PropriedadesConexao".
 *
 * The followings are the available columns in table 'PropriedadesConexao':
 * @property integer $id
 * @property string $nomeSGBD
 * @property string $nomeConexaoSGBD
 * @property string $portaBD
 * @property string $nomeBD
 * @property string $query
 * @property string $hospedeiro
 * @property string $usuarioBD
 * @property string $senha
 * @property string $data_cadastro
 * @property integer $habilitado
 * @property integer $FilialOrigem_has_SistemaOrigem_id
 *
 * The followings are the available model relations:
 * @property FilialOrigemHasSistemaOrigem $filialOrigemHasSistemaOrigem
 */
class PropriedadesConexao extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'PropriedadesConexao';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nomeSGBD, nomeConexaoSGBD, portaBD, nomeBD, query, hospedeiro, usuarioBD, senha, data_cadastro, FilialOrigem_has_SistemaOrigem_id', 'required'),
			array('habilitado, FilialOrigem_has_SistemaOrigem_id', 'numerical', 'integerOnly'=>true),
			array('nomeSGBD', 'length', 'max'=>45),
			array('nomeConexaoSGBD', 'length', 'max'=>20),
			array('portaBD', 'length', 'max'=>8),
			array('nomeBD, usuarioBD', 'length', 'max'=>100),
			array('hospedeiro', 'length', 'max'=>200),
			array('senha', 'length', 'max'=>150),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nomeSGBD, nomeConexaoSGBD, portaBD, nomeBD, query, hospedeiro, usuarioBD, senha, data_cadastro, habilitado, FilialOrigem_has_SistemaOrigem_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'filialOrigemHasSistemaOrigem' => array(self::BELONGS_TO, 'FilialOrigemHasSistemaOrigem', 'FilialOrigem_has_SistemaOrigem_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nomeSGBD' => 'Nome Sgbd',
			'nomeConexaoSGBD' => 'Nome Conexao Sgbd',
			'portaBD' => 'Porta Bd',
			'nomeBD' => 'Nome Bd',
			'query' => 'Query',
			'hospedeiro' => 'Hospedeiro',
			'usuarioBD' => 'Usuario Bd',
			'senha' => 'Senha',
			'data_cadastro' => 'Data Cadastro',
			'habilitado' => 'Habilitado',
			'FilialOrigem_has_SistemaOrigem_id' => 'Filial Origem Has Sistema Origem',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nomeSGBD',$this->nomeSGBD,true);
		$criteria->compare('nomeConexaoSGBD',$this->nomeConexaoSGBD,true);
		$criteria->compare('portaBD',$this->portaBD,true);
		$criteria->compare('nomeBD',$this->nomeBD,true);
		$criteria->compare('query',$this->query,true);
		$criteria->compare('hospedeiro',$this->hospedeiro,true);
		$criteria->compare('usuarioBD',$this->usuarioBD,true);
		$criteria->compare('senha',$this->senha,true);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('FilialOrigem_has_SistemaOrigem_id',$this->FilialOrigem_has_SistemaOrigem_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PropriedadesConexao the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
