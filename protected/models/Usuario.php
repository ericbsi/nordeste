<?php

class Usuario extends CActiveRecord {

    private $connection;

    public function getAdmins($draw, $start) {

        $rows = array();

        $parceiros = array();

        $criteria = new CDbCriteria;

        $criteria->offset = $start;
        $criteria->limit = 10;
        $criteria->condition = 'habilitado AND tipo_id = 11';
        $criteria->order = 't.nome_utilizador ASC';

        foreach (Usuario::model()->findAll($criteria) as $admin) {

            $parceiros = array();

            foreach ($admin->adminParceiros as $p) {
                if ($p->habilitado) {
                    $parceiros[] = $p;
                }
            }

            $row[] = array(
                'idUsuario' => $admin->id,
                'utilizador' => strtoupper($admin->nome_utilizador),
                'username' => $admin->username,
                'senha' => '*****',
                'qtd_parceiros' => count($parceiros)
            );

            $rows = $row;
        }


        return array(
            "draw" => $draw,
            "recordsTotal" => count($rows),
            "recordsFiltered" => count(Usuario::model()->findAll('habilitado AND tipo_id = 11')),
            "data" => $rows
        );
    }

    public function novoAdminEmpresas($Usuario, $Parceiros) {

        $arrReturn = array('hasErrors' => false);

        $Usuario['habilitado'] = 1;
        $Usuario['data_cadastro'] = date('Y-m-d H:i:s');
        $Usuario['tipo'] = 'parceiros_admin';
        $Usuario['role'] = 'parceiros_admin';
        $Usuario['tipo_id'] = 11;
        $Usuario['primeira_senha'] = 1;
        $Usuario['data_ultima_acao'] = date('Y-m-d H:i:s');

        $newUserResponse = $this->novo($Usuario);

        if ($newUserResponse['hasErrors']) {
            $arrReturn['hasErrors'] = true;
        } else {
            for ($i = 0; $i < sizeof($_POST['Parceiros']); $i++) {
                $adminHasParceiro = new AdminHasParceiro;
                $adminHasParceiro->Parceiro = $Parceiros[$i];
                $adminHasParceiro->Administrador = $newUserResponse['Usuario']->id;
                $adminHasParceiro->habilitado = 1;

                if (!$adminHasParceiro->save()) {
                    $arrReturn['hasErrors'] = true;
                }
            }
        }

        return $arrReturn;
    }

    public function salvarAdmin($nomeUtilizador, $username, $senha, $parceiros) {

        $usuario = new Usuario;

        $transaction = Yii::app()->db->beginTransaction();

        $arrayReturn = array('hasErrors' => false,
            'msg' => 'Admin salvo com sucesso',
            'type' => 'success'
        );

        $usuario->habilitado = 1;
        $usuario->data_cadastro = date('Y-m-d H:i:s');
        $usuario->tipo = 'parceiros_admin';
        $usuario->role = 'parceiros_admin';
        $usuario->tipo_id = 11;
        $usuario->primeira_senha = 1;
        $usuario->data_ultima_acao = date('Y-m-d H:i:s');
        $usuario->nome_utilizador = $nomeUtilizador;
        $usuario->username = $username;
        $usuario->password = Yii::app()->hasher->hashPassword($senha);

        if (!$usuario->save()) {

            ob_start();
            var_dump($usuario->getErrors());
            $result = ob_get_clean();

            $arrayReturn = array(
                'hasErrors' => 1,
                'title' => 'Erro',
                'msg' => 'Erro ao salvar Usuário. ' . $result,
                'type' => 'error'
            );

            $transaction->rollBack();

            return $arrayReturn;
        } else {

            $role = Role::model()->findByPk($usuario->tipo_id);

            if ($role !== null) {
                $usuarioRole = new UsuarioRole;
                $usuarioRole->Usuario_id = $usuario->id;
                $usuarioRole->Role_id = $role->id;
                $usuarioRole->role = $role->papel;
                $usuarioRole->data_cadastro = date('Y-m-d H:i:s');
                $usuarioRole->habilitado = 1;

                if (!$usuarioRole->save()) {

                    ob_start();
                    var_dump($usuarioRole->getErrors());
                    $result = ob_get_clean();

                    $arrayReturn = array(
                        'hasErrors' => 1,
                        'title' => 'Erro',
                        'msg' => 'Erro ao salvar Usuário. ' . $result,
                        'type' => 'error'
                    );

                    $transaction->rollBack();

                    return $arrayReturn;
                } else {

                    for ($i = 0; $i < sizeof($parceiros); $i++) {

                        $adminHasParceiro = new AdminHasParceiro;
                        $adminHasParceiro->Parceiro = $parceiros[$i];
                        $adminHasParceiro->Administrador = $usuario->id;
                        $adminHasParceiro->habilitado = 1;

                        if (!$adminHasParceiro->save()) {

                            ob_start();
                            var_dump($adminHasParceiro->getErrors());
                            $result = ob_get_clean();

                            $arrayReturn = array(
                                'hasErrors' => 1,
                                'title' => 'Erro',
                                'msg' => 'Erro ao salvar Usuário. ' . $result,
                                'type' => 'error'
                            );

                            $transaction->rollBack();

                            return $arrayReturn;
                        }
                    }
                }
            } else {

                $arrayReturn = array(
                    'hasErrors' => 1,
                    'title' => 'Erro',
                    'msg' => 'Role não encontrada. ',
                    'type' => 'error'
                );

                $transaction->rollBack();

                return $arrayReturn;
            }
        }

        $transaction->commit();

        return $arrayReturn;
    }

    public function novo($Usuario) {

        $arrReturn = array('hasErrors' => false);

        $usuario = new Usuario;
        $usuario->attributes = $Usuario;
        $role = Role::model()->findByPk($Usuario['tipo_id']);
        $usuario->password = Yii::app()->hasher->hashPassword($usuario->password);

        if (!$usuario->save()) {
            $arrReturn['hasErrors'] = true;
            $arrReturn['errorsDesc'] = $usuario->getErrors();
        } else {
            $usuarioRole = new UsuarioRole;
            $usuarioRole->Usuario_id = $usuario->id;
            $usuarioRole->Role_id = $role->id;
            $usuarioRole->role = $role->papel;
            $usuarioRole->data_cadastro = date('Y-m-d H:i:s');
            $usuarioRole->habilitado = 1;

            if (!$usuarioRole->save()) {
                $arrReturn['hasErrors'] = true;
                $arrReturn['errorsDesc'] = $usuarioRole->getErrors();
            } else {
                $arrReturn['Usuario'] = $usuario;
            }
        }

        return $arrReturn;
    }

    public function analisando($proposta) {
        $analistaEstaAnalisandoProposta = AnalistaHasPropostaHasStatusProposta::model()->buscar($proposta, $this, 1, 1);

        if ($analistaEstaAnalisandoProposta != NULL) {
            return $analistaEstaAnalisandoProposta;
        } else {
            return NULL;
        }
    }

    public function performanceComparativo($tipo, $analistas, $ano) {
        $arrRetorno = array();
        $retorno = array();
        $util = new Util;

        $scoreAnoMesesCabec = array(
            array('Mes')
        );

        for ($i = 0; $i < count($analistas); $i++) {
            if ($analistas[$i] != 0) {
                $analista = Usuario::model()->findByPk($analistas[$i]);
                $scoreAnoMesesCabec[0][] = $analista->nome_utilizador;
            }
        }

        foreach ($util->getMeses() as $mes) {
            $mesScoreAnalistas = array($mes[2]);

            for ($y = 0; $y < count($analistas); $y++) {
                if ($analistas[$y] != 0) {
                    $Qry = " SELECT ScoreAnalistaMesAno($analistas[$y], $mes[0], $ano, $tipo, 1) as Total";
                    $QryR = Yii::app()->db->createCommand($Qry)->queryAll();
                    $mesScoreAnalistas[] = intval($QryR[0]['Total']);
                }
            }

            $scoreAnoMesesCabec[] = $mesScoreAnalistas;
        }

        return $scoreAnoMesesCabec;
    }

    public function performance($analistaId, $ano) {
        $util = new Util;
        $scoreAnoMeses = array(array('Mes', 'Aprovadas', 'Recusadas'));

        foreach ($util->getMeses() as $mes):

            $QryAprovadas = " SELECT ScoreAnalistaMesAno($analistaId, $mes[0], $ano, 2, 1) as Total ";
            $QryCanceladas = " SELECT ScoreAnalistaMesAno($analistaId, $mes[0], $ano, 3, 1) as Total ";

            $QryAprovadasR = Yii::app()->db->createCommand($QryAprovadas)->queryAll();
            $QryCanceladasR = Yii::app()->db->createCommand($QryCanceladas)->queryAll();

            $scoreAnoMeses[] = array($mes[2], intval($QryAprovadasR[0]['Total']), intval($QryCanceladasR[0]['Total']));

        endforeach;

        return $scoreAnoMeses;
    }

    public function score($analistaId) {
        $arrReturn = array(
            'Aprovadas' => array(
                'Total' => 0,
                'Valor' => 0,
            ),
            'Recusadas' => array(
                'Total' => 0,
                'Valor' => 0,
            ),
        );

        $QryScore = " SELECT ";
        $QryScore .= " SUM( IF(ap.Status_Proposta_id = 2 AND p.habilitado ,1,0) ) AS `Aprovadas`, ";
        $QryScore .= " SUM( IF(ap.Status_Proposta_id = 3,1,0) ) AS `Canceladas`, ";
        $QryScore .= " ROUND(SUM( IF(ap.Status_Proposta_id = 2 AND p.habilitado ,p.valor,0) ),1) AS `TotAprovado`, ";
        $QryScore .= " ROUND(SUM( IF(ap.Status_Proposta_id = 3 ,p.valor,0) ),1) AS `TotRecusado`  ";
        $QryScore .= " FROM `Analista_has_Proposta_has_Status_Proposta` as ap ";
        $QryScore .= " INNER JOIN `Proposta` as p ON ap.Proposta_id = p.id ";
        $QryScore .= " WHERE ap.Analista_id = " . $analistaId;

        $QryScoreR = Yii::app()->db->createCommand($QryScore)->queryAll();

        if (count($QryScoreR) > 0) {
            foreach ($QryScoreR as $r) {
                $arrReturn['Aprovadas']['Total'] = $r['Aprovadas'];
                $arrReturn['Aprovadas']['Valor'] = number_format($r['TotAprovado'], 2, ',', '.');

                $arrReturn['Recusadas']['Total'] = $r['Canceladas'];
                $arrReturn['Recusadas']['Valor'] = number_format($r['TotRecusado'], 2, ',', '.');
            }
        }

        return $arrReturn;
    }

    public function meuHistoricoAnalista($aprneg, $draw, $offset, $dataDe, $dataAte, $filiais, $qtd_parc_de, $qtd_parc_ate)
    {

        $rows = array();
        $filiaisGrahp = array();
        $util = new Util;
        $recordsTotal = 0;
        $recordsFiltered = 0;
        $totalInicial = 0;
        $totalFinanciado = 0;
        $totalEntradas = 0;
        $totalFut = 0;
        $totalAprovado = 0;
        $totalReprovado = 0;
        $util = new Util;
        /* Query para montar chart */
        $QryChart = " SELECT ";
        $QryChart .= " SUM( IF(ap.Status_Proposta_id = 2 AND p.habilitado ,1,0) ) AS `Aprovadas`, ";
        $QryChart .= " SUM( IF(ap.Status_Proposta_id = 3,1,0) ) AS `Canceladas`, ";
        $QryChart .= " ROUND(SUM( IF(ap.Status_Proposta_id = 2 AND p.habilitado ,p.valor,0) ),1) AS `TotAprovado`, ";
        $QryChart .= " ROUND(SUM( IF(ap.Status_Proposta_id = 3 ,p.valor,0) ),1) AS `TotRecusado`  ";
        $QryChart .= " FROM `Analista_has_Proposta_has_Status_Proposta` as ap ";
        $QryChart .= " INNER JOIN `Proposta` as p ON ap.Proposta_id = p.id ";
        $QryChart .= " WHERE ap.Analista_id = " . Yii::app()->session['usuario']->id;

        if(isset($dataDe) && $dataDe != ''){
            $QryChart .= " AND p.data_cadastro > '" . $util->view_date_to_bd($dataDe) . " 00:00:00'";
        }
        if(isset($dataAte) && $dataAte != ''){
            $QryChart .= " AND p.data_cadastro < '" . $util->view_date_to_bd($dataAte) . " 23:59:59'";
        }

        $QryChartR = Yii::app()->db->createCommand($QryChart)->queryAll();
        $graphOne = array();

        foreach ($QryChartR as $r) {
            $graphOne[] = array('Aprovadas', intval($r['Aprovadas']));
            $graphOne[] = array('Rejeitadas', intval($r['Canceladas']));
        }

        /* $totalAprovado = $r['TotAprovado'];
          $totalReprovado = $r['TotRecusado']; */
        /* ------------------------ */

        $QryValores = " SELECT ";
        $QryValores .= " ap.Proposta_id as PropostaId ";
        $QryValores .= " FROM `Analista_has_Proposta_has_Status_Proposta` as ap  ";
        $QryValores .= " INNER JOIN `Proposta` as p ON ap.Proposta_id = p.id ";
        $QryValores .= " WHERE ap.Analista_id = " . Yii::app()->session['usuario']->id;
        if(isset($aprneg)){
            if($aprneg == "0"){
                $QryValores .= " AND ((ap.Status_Proposta_id = 2 AND p.Titulos_gerados) OR ap.Status_Proposta_id = 3) ";
            }
            if($aprneg == "1"){
                $QryValores .= " AND ((ap.Status_Proposta_id = 2 AND p.Titulos_gerados)) ";
            }
            if($aprneg == "2"){
                $QryValores .= " AND ap.Status_Proposta_id = 3 ";
            }
        }

        if(isset($dataDe) && $dataDe != ''){
            $QryValores .= " AND p.data_cadastro > '" . $util->view_date_to_bd($dataDe) . " 00:00:00'";
        }
        if(isset($dataAte) && $dataAte != ''){
            $QryValores .= " AND p.data_cadastro < '" . $util->view_date_to_bd($dataAte) . " 23:59:59'";
        }

        $QryValoresR = Yii::app()->db->createCommand($QryValores)->queryAll();
        $recordsFiltered = count($QryValoresR);


        foreach (Yii::app()->db->createCommand($QryValores)->queryAll() as $p) {
            $proposta = Proposta::model()->findByPk($p['PropostaId']);

            if ($proposta->Status_Proposta_id == 2 && $proposta->titulos_gerados) {
                $totalAprovado += $proposta->getValorFinanciado();
            } else if ($proposta->Status_Proposta_id == 3) {
                $totalReprovado += $proposta->getValorFinanciado();
            }
        }

        if (count($QryValoresR) > 0) {

            $QryValores .= " ORDER BY ap.data_cadastro DESC ";
            $QryValores .= " LIMIT " . $offset . ",10 ";

            foreach (Yii::app()->db->createCommand($QryValores)->queryAll() as $rs) {

                $proposta = Proposta::model()->findByPk($rs['PropostaId']);

                if ($proposta != NULL) {
                    $row = array(
                        'codigo' => $proposta->codigo,
                        'emissao' => $util->bd_date_to_view(substr($proposta->data_cadastro, 0, 10)),
                        'filial' => strtoupper($proposta->analiseDeCredito->filial->nome_fantasia . ' - ' . $proposta->analiseDeCredito->filial->getEndereco()->cidade . '/' . $proposta->analiseDeCredito->filial->getEndereco()->uf),
                        'cliente' => strtoupper(substr($proposta->analiseDeCredito->cliente->pessoa->nome, 0, 23) . '...'),
                        'cpf' => $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero,
                        'valor' => "R$ " . number_format($proposta->valor, 2, ',', ''),
                        'entrada' => "R$ " . number_format($proposta->valor_entrada, 2, ',', ''),
                        'seguro' => "R$ " . number_format($proposta->calcularValorDoSeguro(), 2, ',', ''),
                        'val_fin' => "R$ " . number_format($proposta->getValorFinanciado(), 2, ',', ''),
                        'qtd_par' => $proposta->qtd_parcelas . ' x ' . number_format($proposta->getValorParcela(), 2, ',', '.'),
                        'val_fut' => "R$ " . number_format($proposta->getValorFinal(), 2, ',', ''),
                        'btn_status' => '<span class="' . $proposta->statusProposta->cssClass . '">' . $proposta->statusProposta->status . '</span>',
                    );

                    $rows[] = $row;
                }
            }
        }


        return (array(
            "draw" => $draw,
            "recordsTotal" => count($rows),
            "recordsFiltered" => $recordsFiltered,
            "data" => $rows,
            "customReturn" => array(
                'totalInicial' => "R$ " . number_format($totalInicial, 2, ',', '.'),
                'totalFin' => "R$ " . number_format($totalFinanciado, 2, ',', '.'),
                'totalEntradas' => "R$ " . number_format($totalEntradas, 2, ',', '.'),
                'totalFut' => "R$ " . number_format($totalFut, 2, ',', '.'),
                'totalAprovado' => "R$ " . number_format($totalAprovado, 2, ',', '.'),
                'totalRecusado' => "R$ " . number_format($totalReprovado, 2, ',', '.'),
                'graphs' => array(
                    'porFiliais' => $filiaisGrahp,
                    'graphOne' => $graphOne,
                )
            )
        ));
    }

    public function checkUserNameAvailability($username) {

        $usuario = Usuario::model()->find('username = ' . "'$username'");

        if ($usuario != NULL)
            echo "false";
        else
            echo "true";
    }

    public function updateVistoPorUltimoupdate() {

        $this->data_ultima_acao = date('Y-m-d H:i:s');
        $this->update();
    }

    public function changePassword($old_pass, $new_pass) {

        $retorno = array(
            'empty' => false,
            'hasErrors' => false,
            'messages' => array()
        );

        // !Yii::app()->hasher->checkPassword( $this->password, $usuario->password )

        /* Senha atual incorreta */
        if (!Yii::app()->hasher->checkPassword($old_pass, Yii::app()->session['usuario']->password)) {
            $retorno['hasErrors'] = true;
            $retorno['messages'][] = 'Senha atual incorreta.';
            $retorno['alert_class'] = 'alert-danger';
        }

        /* Nova senha igual a senha atual */ else if (Yii::app()->hasher->checkPassword($new_pass, Yii::app()->session['usuario']->password)) {
            $retorno['hasErrors'] = true;
            $retorno['messages'][] = 'A nova senha deve ser diferente da atual.';
            $retorno['alert_class'] = 'alert-danger';
        } else {
            $usuario = Usuario::model()->findByPk(Yii::app()->session['usuario']->id);
            $usuario->password = Yii::app()->hasher->hashPassword($new_pass);

            if ($usuario->primeira_senha) {
                $usuario->primeira_senha = 0;
            }

            if ($usuario->update()) {

                $usuarioS1 = Yii::app()->bdSigacOne->createCommand()->update('Usuario', ['password' => $usuario->password], 'username=:username', [':username' => $usuario->username]);

                $retorno['hasErrors'] = false;
                $retorno['messages'][] = 'Sua senha foi alterada com sucesso.';
                $retorno['alert_class'] = 'alert-success';

                $sigacLog = new SigacLog;
                $sigacLog->saveLog('Alteração de senha de usuário', 'usuario', $usuario->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Usuário " . Yii::app()->session['usuario']->username . " alterou a senha.", "127.0.0.1", null, Yii::app()->session->getSessionId());
            }

            Yii::app()->session['usuario'] = $usuario;
        }

        return $retorno;
    }

    public function clearRascunhos() {

        $rascunhos = AnaliseRascunho::model()->findAll('Usuario_id = ' . $this->id);

        if (count($rascunhos) > 0) {
            foreach ($rascunhos as $rascunho) {
                $rascunho->delete();
            }
        }
    }

    public function cleanCaptchas() {

        $captchas = glob('images/captchas/captchas_' . Yii::app()->session['usuario']->id . '/*');

        foreach ($captchas as $c) {

            if (is_file($c)) {
                unlink($c);
            }
        }
    }

    public function listFiliais() {

        $q = $this->empresasId('query');

        $filiaisArr = array();

        for ($i = 0; $i < sizeof($q); $i++) {

            $filiais = Filial::model()->findAll('Empresa_id = ' . $q[$i]['id']);

            foreach ($filiais as $f) {

                if (!in_array($f->id, $filiaisArr)) {
                    array_push($filiaisArr, $f->id);
                }
            }
        }

        $criteria = new CDbCriteria;
        $criteria->addInCondition('id', $filiaisArr);

        return Filial::model()->findAll($criteria);
    }

    public function listCotacoesEmpresa() {

        $criteria = new CDbCriteria;

        $empresa = $this->getEmpresa();

        $cotacoes = Cotacao::model()->findAll('Empresa_id = ' . $empresa->id);

        $arrIdsCotacoes = array();

        foreach ($cotacoes as $c) {

            if (!in_array($c->id, $arrIdsCotacoes)) {

                array_push($arrIdsCotacoes, $c->id);
            }
        }

        $criteria->addInCondition("id", $arrIdsCotacoes);

        return new CActiveDataProvider(Cotacao::model(), array(
            'criteria' => $criteria,
        ));
    }

    public function listCotacoesFiliaisEmpresa() {

        $criteria = new CDbCriteria;

        $empresa = Empresa::model()->find("nome_fantasia = " . "'" . $this->nomeEmpresa() . "'");

        $arrIdsCotacoes = array();

        $filiais = $empresa->listFiliais();

        foreach ($filiais as $f) {

            $cotacoes = $f->listCotacoes();

            foreach ($cotacoes as $c) {

                if (!in_array($c->id, $arrIdsCotacoes)) {

                    array_push($arrIdsCotacoes, $c->id);
                }
            }
        }

        $criteria->addInCondition("id", $arrIdsCotacoes);

        return new CActiveDataProvider(Cotacao::model(), array(
            'criteria' => $criteria,
        ));
    }

    public function tableName() {

        return 'Usuario';
    }

    public function nomeEmpresa() {

        $empresaUsuario = EmpresaHasUsuario::model()->find('Usuario_id = ' . $this->id);
        $empresa = Empresa::model()->findByPk($empresaUsuario->Empresa_id);

        return $empresa->nome_fantasia;
    }

    public function getEmpresa() {

        $empresaUsuario = EmpresaHasUsuario::model()->find('Usuario_id = ' . $this->id);

        if ($empresaUsuario != NULL) {
            return Empresa::model()->findByPk($empresaUsuario->Empresa_id);
        } else {
            return NULL;
        }
    }

    /* Retorna um objeto do tipo filial */

    public function getFilial() {
        $filialUsuario = FilialHasUsuario::model()->find('Usuario_id = ' . $this->id);

        if ($filialUsuario != NULL) {
            return Filial::model()->findByPk($filialUsuario->Filial_id);
        } else {
            return NULL;
        }
    }

    public function returnFilial($emprestimo = null) {

        if ($emprestimo !== null && $emprestimo == "1") {
            return Filial::model()->findByPk(184); //credshow
        } else {

            $filialUsuario = FilialHasUsuario::model()->find('Usuario_id = ' . $this->id);

            if ($filialUsuario != NULL) {
                return Filial::model()->findByPk($filialUsuario->Filial_id);
            } else {
                return NULL;
            }
        }
    }

    public function nomeFilial() {

        $filialUsuario = FilialHasUsuario::model()->find('Usuario_id = ' . $this->id);
        $filial = Filial::model()->findByPk($filialUsuario->Filial_id);

        return $filial->nome_fantasia;
    }

    public function roleLabel() {

        $usuarioRole = UsuarioRole::model()->find('Usuario_id = ' . $this->id);
        $role = Role::model()->findByPk($usuarioRole->Role_id);

        return $role->label;
    }

    public function getRole() {

        $usuarioRole = UsuarioRole::model()->find('Usuario_id = ' . $this->id);

        if ($usuarioRole != NULL) {
            return Role::model()->findByPk($usuarioRole->Role_id);
        } else {
            return NULL;
        }
    }

    private function setConn() {

        $this->connection = new CDbConnection(
                Yii::app()->params['dbconf']['dns'], Yii::app()->params['dbconf']['user'], Yii::app()->params['dbconf']['pass']
        );
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {

        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('username, role, nome_utilizador', 'length', 'max' => 100),
            array('password', 'length', 'max' => 230),
            array('username', 'required', 'message' => 'Informe o nome de usuario.'),
            array('password', 'required', 'message' => 'Informe a senha.'),
            array('id, tipo_id, habilitado, limite_analise', 'numerical', 'integerOnly' => true),
            array('data_cadastro, role, data_cadastro_br, tipo, tipo_id', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, username, primeira_senha, nome_utilizador, tipo_id, limite_analise, password, data_cadastro, data_cadastro_br, habilitado, data_ultima_acao, role', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {

        return array(
            'usuarioRoles' => array(self::HAS_MANY, 'UsuarioRole', 'Usuario_id'),
            'adminParceiros' => array(self::HAS_MANY, 'AdminHasParceiro', 'Administrador'),
            'tipoId' => array(self::BELONGS_TO, 'Role', 'tipo_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {

        return array(
            'id' => 'ID',
            'username' => 'Nome de usuário',
            'role' => 'Papel',
            'password' => 'Senha',
            'data_cadastro' => 'Data Cadastro',
            'data_cadastro_br' => 'Data / Hora Cadastro',
            'habilitado' => 'Habilitado',
            'tipo' => 'Tipo',
            'tipo_id' => 'Tipo de Usuário',
            'limite_analise' => 'Limite análise de Crédito',
            'nome_utilizador' => 'Nome do utilizador',
            'primeira_senha' => 'Primeira Senha',
            'data_ultima_acao' => 'Visto por último',
            'analise_solicitada' => 'Analise solicitada',
        );
    }

    public function search() {

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('username', $this->username, true);
        $criteria->compare('nome_utilizador', $this->nome_utilizador, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('data_cadastro', $this->data_cadastro, true);
        $criteria->compare('data_cadastro_br', $this->data_cadastro_br, true);
        $criteria->compare('habilitado', $this->habilitado);
        $criteria->compare('tipo', $this->tipo, true);
        $criteria->compare('tipo_id', $this->tipo_id, true);
        $criteria->compare('primeira_senha', $this->primeira_senha);
        $criteria->compare('data_ultima_acao', $this->data_ultima_acao);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__) {

        return parent::model($className);
    }

    public function fetch_menus() {

        //$this->setConn();

        $sql = "select r.menu_padrao\n"
                . "from Usuario as u\n"
                . "inner join Usuario_Role as ur on u.id = ur.Usuario_id\n"
                . "inner join Role as r on r.id = ur.Role_id\n"
                . "where u.id =" . $this->id;

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    public function findAdminsEmpresas() {

        $arrIdsUsuarios = array();
        $criteria = new CDbCriteria;

        foreach ($this->empresasId('query') as $key) {

            $empresa = Empresa::model()->findByPk($key['id']);

            foreach ($empresa->listAdminsEmpresa() as $admin) {

                if (!in_array($admin['id'], $arrIdsUsuarios)) {

                    array_push($arrIdsUsuarios, $admin['id']);
                }
            }
        }

        $criteria->addInCondition("id", $arrIdsUsuarios);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function findAnalistas() {

        $arrIdsAnalistas = array();
        $criteria = new CDbCriteria;

        foreach ($this->empresasId('query') as $id) {

            $empresa = Empresa::model()->findByPk($id['id']);

            foreach ($empresa->listAnalistasEmpresa() as $analista) {

                if (!in_array($analista['id'], $arrIdsAnalistas)) {

                    array_push($arrIdsAnalistas, $analista['id']);
                }
            }
        }

        $criteria->addInCondition("id", $arrIdsAnalistas);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchUserFilialUsers() {

        $arrIdsUsuarios = array();
        $criteria = new CDbCriteria;

        foreach ($this->empresasId('query') as $id) {

            $empresa = Empresa::model()->findByPk($id['id']);

            foreach ($empresa->usuariosEmpresa($this) as $idUsuario) {

                /* Evita duplicações */
                if (!in_array($idUsuario['id'], $arrIdsUsuarios)) {

                    array_push($arrIdsUsuarios, $idUsuario['id']);
                }
            }
        }

        $criteria->addInCondition("id", $arrIdsUsuarios);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function gruposId() {

        $arrIdsGrupos = array();

        $grupoDeAnalistasHasUsuario = GrupoDeAnalistasHasUsuario::model()->findAll(
                'Usuario_id = ' . $this->id . ' AND ' .
                'habilitado'
        );

        foreach ($grupoDeAnalistasHasUsuario as $gahu) {

            $grupoDeAnalista = GrupoDeAnalistas::model()->find(
                    'id = ' . $gahu->Grupo_de_Analistas_id .
                    ' AND habilitado'
            );

            if (!in_array($grupoDeAnalista->id, $arrIdsGrupos)) {
                array_push($arrIdsGrupos, $grupoDeAnalista->id);
            }
        }
        return $arrIdsGrupos;
    }

    public function empresasId($returnType) {

        //$this->setConn();

        $sql = "select e.id \n"
                . "	FROM `Empresa` as e\n"
                . "	 inner join `Empresa_has_Usuario` as eu on e.id = eu.Empresa_id\n"
                . "	 inner join `Usuario` as u on eu.Usuario_id = u.id\n"
                . "	 WHERE u.id =" . $this->id;

        $queryReturn = Yii::app()->db->createCommand($sql)->queryAll();

        $ids = array();

        foreach ($queryReturn as $id) {
            array_push($ids, $id['id']);
        }

        $criteria = new CDbCriteria();
        $criteria->addInCondition("id", $ids);

        switch ($returnType) {
            case 'criteria':
                return $criteria;
                break;

            default:
            case 'query':
                return $queryReturn;
                break;
        }
    }

    //retorna as filiais de um usuario

    public function filiaisId($returnType) {

        $filiaisIds = array();

        foreach ($this->empresasId('query') as $empresaId) {

            $sql = "Select f.id\n"
                    . "	from Filial as f\n"
                    . "	 where f.Empresa_id = " . $empresaId['id'];

            $idsFiliaisReturn = Yii::app()->db->createCommand($sql)->queryAll();

            foreach ($idsFiliaisReturn as $id) {
                array_push($filiaisIds, $id['id']);
            }
        }

        $criteria = new CDbCriteria();
        $criteria->addInCondition("id", $filiaisIds);

        switch ($returnType) {
            case 'criteria':
                return $criteria;
                break;

            default:
            case 'query':
                return $queryReturn;
                break;
        }
    }

    /* Tipos de usuários que um usuário pode criar */

    public function userTypeCreate() {


        $criteria = new CDbCriteria;

        switch ($this->tipo) {
            case 'super':
                $criteria->addInCondition("id", array(3, 15));
                return $criteria;
                break;

            case 'empresa_admin':
                $criteria->addInCondition("id", array(4, 5, 6, 7, 8, 9, 10, 12, 15));
                return $criteria;
                break;
            default:
                //do nothing
                break;
        }
    }

    public function listPropostas() {

        $criteria = new CDbCriteria;
        $arrFiliais = array();
        $arrPropostas = array();

        $arrGrupos = $this->gruposId();

        for ($i = 0; $i < sizeof($arrGrupos); $i++) {

            $objGrupo = GrupoDeAnalistas::model()->findByPk($arrGrupos[$i]);

            $arrIdsFiliais = $objGrupo->listFiliais();

            for ($i = 0; $i < sizeof($arrIdsFiliais); $i++) {

                if (!in_array($arrIdsFiliais[$i], $arrFiliais)) {
                    array_push($arrFiliais, $arrIdsFiliais[$i]);
                }
            }
        }

        for ($i = 0; $i < sizeof($arrFiliais); $i++) {

            $objFilial = Filial::model()->findByPk($arrFiliais[$i]);

            $idsPropostas = $objFilial->listPropostas($this);

            for ($i = 0; $i < sizeof($idsPropostas); $i++) {

                if (!in_array($idsPropostas[$i], $arrPropostas)) {

                    array_push($arrPropostas, $idsPropostas[$i]);
                }
            }
        }

        $proposta = new Proposta;

        $criteria->addInCondition("id", $arrPropostas);

        return new CActiveDataProvider($proposta, array(
            'criteria' => $criteria,
        ));
    }

    /* Deprecated */

    public function listCrediaristaAnalises($start = null) {

        $criteria = new CDbCriteria;
        $criteria->addInCondition('Usuario_id', array(Yii::app()->session['usuario']->id), 'AND');
        $criteria->addInCondition('habilitado', array(1), 'AND');
        $criteria->order = 'data_cadastro DESC';

        if ($start != null) {
            $criteria->offset = $start;
            $criteria->limit = 10;
        }

        return AnaliseDeCredito::model()->findAll($criteria);
    }

    /* Retorna as últimas dez analises do cliente */

    public function analisesCrediarista($start) {
        $criteria = new CDbCriteria;
        $criteria->addInCondition('Usuario_id', array($this->id), 'AND');
        $criteria->addInCondition('habilitado', array(1), 'AND');
        $criteria->offset = $start;
        $criteria->limit = 10;
        $criteria->order = "data_cadastro DESC";

        return AnaliseDeCredito::model()->findAll($criteria);
    }

    public function getQtdMsgNaoLida($proposta) {
        $query = "";

        $query .= "SELECT count(*) AS QTD ";
        $query .= "FROM Mensagem as M ";
        $query .= "INNER JOIN Dialogo AS D ON D.habilitado AND M.Dialogo_id = D.id ";
        $query .= "Where M.habilitado AND Entidade_relacionamento = 'Proposta' AND Entidade_Relacionamento_id = " . $proposta->id . " ";
        $query .= "		AND Usuario_id <> " . $this->id . " AND NOT lida ";

        $queryReturn = Yii::app()->db->createCommand($query)->queryRow();

        return $queryReturn['QTD'];
    }

    /* Tela inicial das crediaristas */

    public function listCrediaristaPropostas($arrStatus, $draw, $start) {
        $rows = [];
        $crt = new CDbCriteria;
        $arrStatusP = join(',', $arrStatus);

        $crt->with = [
            'analiseDeCredito' => [
                'alias' => 'ac'
            ]
        ];
        $crt->condition = 'ac.Usuario_id = ' . $this->id . ' AND NOT t.titulos_gerados AND t.habilitado AND ( (t.Status_Proposta_id IN (4, 1, 2, 9)) OR (t.Status_Proposta_id = 3 AND t.data_cadastro BETWEEN "' . date('Y-m-d 00:00:00') . '" AND "' . date('Y-m-d 23:59:59') . '") )';
        $crt->order = 't.data_cadastro DESC';


        $propostas = Proposta::model()->findAll($crt);

        foreach ($propostas as $proposta) {

            $solicitacaoCancelamento = SolicitacaoDeCancelamento::model()->find("habilitado AND Proposta_id = $proposta->id AND aceite");

            if ($solicitacaoCancelamento !== null) {
                continue;
            }

            $propostaOmniConfig = $proposta->hasOmniConfig();

            if ($propostaOmniConfig != NULL && ( ($proposta->Status_Proposta_id != 2 && $proposta->Status_Proposta_id != 3) || ( $proposta->Status_Proposta_id == 3 && $proposta->analise_solicitada ) )) {
                if ($proposta->Financeira_id == 7) {
                    $proposta->updateSemearStatus();
                } else {
                    $proposta->updateOmniStatus();
                }
            }

            $btnAtualizacoes = '';
            $btnDetails = '';
            $btn = '';

            $btnDetails .= '<form method="post" action="' . Yii::app()->getBaseUrl(true) . '/proposta/more">';
            $btnDetails .= '	<input type="hidden" name="id" value="' . $proposta->id . '">';
            $btnDetails .= '	<button style="padding:6px;font-size:8px;" type="submit" class="btn btn-primary"><i class="clip-search"></i></button>';
            $btnDetails .= '</form>';

            if ($proposta->statusProposta->id == 2 && !$proposta->titulos_gerados) {
                $btn .= '<form method="post" action="' . Yii::app()->getBaseUrl(true) . '/proposta/concluir">';
                $btn .= '	<input type="hidden" name="c" value="' . $proposta->codigo . '">';
                $btn .= '	<button type="submit" class="disabled ' . $proposta->statusProposta->cssClass . '">' . $proposta->statusProposta->status . '</button>';
                $btn .= '</form>';
            } else if ($proposta->statusProposta->id == 2 && $proposta->titulos_gerados) {
                $btn = '<span class="' . $proposta->statusProposta->cssClass . '">Finalizada</span>';
            } else {
                $btn = '<span class="' . $proposta->statusProposta->cssClass . '">' . $proposta->statusProposta->status . '</span>';
            }

            if ($proposta->statusProposta->id == 9) {
                $btnAtualizacoes = '<span class="label label-danger buttonpulsate">Atualizações</span>';
            } else {
                $btnAtualizacoes = '';
            }


            if ($proposta->Status_Proposta_id == 3 && $proposta->analise_solicitada) {
                $btn = '<span class="btn label label-warning">Aguardando Analise</span>';
            }

            if ($proposta->statusProposta->id == 2 && $proposta->Financeira_id != 7) {
                $proposta->numero_do_contrato = $proposta->updateCodigoContratoOmni();
                $proposta->update();
            }

            $valorDoSeguro = 0;

            if( $proposta->hasOmniConfig() != Null && $proposta->segurada ){
              $valorDoSeguro = Proposta::model()->calcularValorSeguroOmni($proposta->valor, $proposta->valor_entrada);
            }
            else{
              $valorDoSeguro = $proposta->calcularValorDoSeguro();
            }

            $row = array(
                'btn_det' => $btnDetails,
                'codigo' => $proposta->codigo,
                'cliente' => strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome),
                'financeira' => strtoupper("Credshow S/A"),
                'val_inicial' => number_format($proposta->valor, 2, ',', '.'),
                'val_entrada' => number_format($proposta->valor_entrada, 2, ',', '.'),
                'val_seguro' => number_format($valorDoSeguro, 2, ',', '.'),
                'val_fin' => number_format($proposta->getValorFinanciado(), 2, ',', '.'),
                'parcelamento' => $proposta->qtd_parcelas . 'x ' . "R$ " . number_format($proposta->getValorParcela(), 2, ',', '.'),
                'val_final' => number_format(( $proposta->getValorFinal()), 2, ',', '.'),
                'msg_novas' => $btnAtualizacoes,
                'btn' => $btn
            );

            $rows[] = $row;
        }

        return (array(
            "draw" => $draw,
            "recordsTotal" => count($rows),
            "recordsFiltered" => count($rows),
            "data" => $rows
        ));
    }

    public function listarReanalise($draw, $start, $codigo_filter) {
        $rows = array();
        $crt = new CDbCriteria;
        $condicao = null;

        if (isset($codigo_filter) && trim($codigo_filter) != '' && $codigo_filter != null) {
            $condicao = 't.codigo LIKE CONCAT("%", "' . $codigo_filter . '", "%" ) AND t.Status_Proposta_id = 3 ';
        } else {
            $condicao = 't.Status_Proposta_id = 3  AND ( t.data_cadastro BETWEEN "' . date('Y-m-d 00:00:00') . '" AND "' . date('Y-m-d 23:59:59') . '" ) ';
        }


        $crt->condition = $condicao;
        $crt->order = 't.data_cadastro DESC';

        $propostasReanalise = Proposta::model()->findAll($crt);

        foreach ($propostasReanalise as $propA) {
            if ($propA->hasOmniConfig() != NULL) {

                $btn = '<a data-propid="' . $propA->id . '" href="#modal-enviar" class="label-warning show-modal" data-toggle="modal"><span class="btn label label-danger">NEGADO</span></a>';

                $row = array(
                    'codigo' => $propA->codigo,
                    'cliente' => strtoupper($propA->analiseDeCredito->cliente->pessoa->nome),
                    'filial' => strtoupper($propA->analiseDeCredito->filial->getConcat()),
                    'valor' => number_format($propA->valor - $propA->valor_entrada, 2, ',', '.'),
                    'valorSeguro' => number_format($propA->calcularValorDoSeguro(), 2, ',', '.'),
                    'qtd_parcelas' => $propA->qtd_parcelas . 'x ' . "R$ " . number_format($propA->getValorParcela(), 2, ',', '.'),
                    'btn' => $btn,
                    'data_cadastro' => $propA->data_cadastro_br,
                );

                $rows[] = $row;
            }
        }

        return array(
            "draw" => $draw,
            "recordsTotal" => count($rows),
            "recordsFiltered" => count($rows),
            "data" => $rows,
        );
    }

    public function listarLiberar($draw, $start, $codigo_filter) {
        $rows = array();
        $crt = new CDbCriteria;
        $condicao = null;

        if (isset($codigo_filter) && trim($codigo_filter) != '' && $codigo_filter != null) {
            $condicao = 't.codigo LIKE CONCAT("%", "' . $codigo_filter . '", "%" ) AND t.Status_Proposta_id IN(1,4) ';
        } else {
            $condicao = 't.Status_Proposta_id IN(1,4)';
        }


        $crt->condition = $condicao;
        $crt->order = 't.data_cadastro DESC';

        $propostasReanalise = Proposta::model()->findAll($crt);

        foreach ($propostasReanalise as $propA) {
            $btn = '<a data-propid="' . $propA->id . '" href="#modal-enviar" class="label-warning show-modal" data-toggle="modal"><span class="' . $propA->statusProposta->cssClass . '">' . $propA->statusProposta->status . '</span></a>';

            $row = array(
                'codigo' => $propA->codigo,
                'cliente' => strtoupper($propA->analiseDeCredito->cliente->pessoa->nome),
                'filial' => strtoupper($propA->analiseDeCredito->filial->getConcat()),
                'valor' => number_format($propA->valor - $propA->valor_entrada, 2, ',', '.'),
                'valorSeguro' => number_format($propA->calcularValorDoSeguro(), 2, ',', '.'),
                'qtd_parcelas' => $propA->qtd_parcelas . 'x ' . "R$ " . number_format($propA->getValorParcela(), 2, ',', '.'),
                'btn' => $btn,
                'data_cadastro' => $propA->data_cadastro_br,
            );

            $rows[] = $row;
        }

        return array(
            "draw" => $draw,
            "recordsTotal" => count($rows),
            "recordsFiltered" => count($rows),
            "data" => $rows,
        );
    }

    /* Tela inicial das analistas */

    public function listAnalistaPropostas($draw, $start, $codigo_filter, $nome_filter, $negadas = FALSE, $semear = false, $prestige = false) {
        $maior = '9000';
        $condicao = NULL;
        $crt = new CDbCriteria;
        $corCpfVip = "";
        $labelVip  = "";

        if (isset($codigo_filter) && trim($codigo_filter) != '' && $codigo_filter != null) {
            if (($prestige == "true") && ($semear == "false")) {
                $condicao = '(t.habilitado AND t.valor >= 9000 AND (t.Status_Proposta_id = 1 OR t.Status_Proposta_id = 4)) AND t.codigo LIKE CONCAT("%", "' . $codigo_filter . '", "%" )';
            } else {
                $condicao = 't.habilitado AND t.codigo LIKE CONCAT("%", "' . $codigo_filter . '", "%" )';
            }

            if ($semear == "true") {
                $condicao = 't.Financeira_id = 7 AND t.habilitado AND t.codigo LIKE CONCAT("%", "' . $codigo_filter . '", "%" )';
            } else {
                if ($prestige == "false") {
                    $condicao = 't.habilitado AND t.codigo LIKE CONCAT("%", "' . $codigo_filter . '", "%" )';
                }
            }
        } else

        if (isset($nome_filter) && trim($nome_filter) != '' && $nome_filter != null) {
            $crt->join = 'JOIN Analise_de_Credito ac ON t.Analise_de_Credito_id = ac.id JOIN Cliente cli ON cli.id = ac.Cliente_id JOIN Pessoa pess ON pess.id = cli.Pessoa_id';
            $crt->addSearchCondition('t.habilitado', 1, 'AND');
            $crt->addSearchCondition('pess.nome', $nome_filter, 'AND');
            if ($semear == "true") {
                $crt->addSearchCondition('t.Financeira_id', 7, 'AND');
            } else {
                if ($prestige == "true") {
                    $crt->addCondition('t.valor > "' . $maior . '" ');
                }
            }
        } else {
            if ($negadas == TRUE) {
                //$condicao = 't.habilitado AND (t.Status_Proposta_id = 4 OR t.Status_Proposta_id = 1) OR (t.Status_Proposta_id = 3 AND ( t.data_cadastro BETWEEN "' . date('Y-m-d 00:00:00') . '" AND "' . date('Y-m-d 23:59:59') . '" ) )';
                //$condicao = 't.Status_Proposta_id = 3 AND ( t.data_cadastro BETWEEN "' . date('Y-m-d 00:00:00') . '" AND "' . date('Y-m-d 23:59:59') . '" ) ';
                if ($semear == "true") {
                    $condicao = 't.habilitado AND t.Financeira_id = 7 AND t.Status_Proposta_id = 3 AND ( t.data_cadastro > DATE_SUB( NOW(), INTERVAL 5 HOUR ) ) ';
                } else {
                    $condicao = 't.habilitado AND t.Status_Proposta_id = 3 AND ( t.data_cadastro > DATE_SUB( NOW(), INTERVAL 5 HOUR ) ) ';
                }
            } else {
                if ($semear == "true") {
                    $condicao = 't.habilitado AND (t.Status_Proposta_id = 4 OR t.Status_Proposta_id = 1) AND t.Financeira_id = 7 ';
                } else {
                    $condicao = 't.habilitado AND (t.Status_Proposta_id = 4 OR t.Status_Proposta_id = 1) AND t.Financeira_id != 7 AND t.valor < 9000';
                }
                if ($prestige == "true") {

                    $user = Yii::app()->session['usuario'];

                    if (($user->id == 51) || ($user->id == 663)) { // se usuário logado for Thiago ou Elismar
                        $condicao = 't.habilitado AND t.valor >= 9000 AND (t.Status_Proposta_id = 1 OR t.Status_Proposta_id = 4 OR t.Status_Proposta_id = 9)'; // exibir propostas prestige em análise
                    } else {
                        $condicao = 't.habilitado AND (t.Status_Proposta_id = 4 OR t.Status_Proposta_id = 1) AND t.valor < 9000';
                        $retorno['hasErrors'] = true;
                        $retorno['messages'][] = 'Usuário não autorizado para esta operação.';
                        $retorno['type'] = 'error';
                    }
                }
            }
        }

        if ($condicao != NULL) {
            $crt->condition = $condicao;
        }
        $crt->order = 't.data_cadastro DESC, FIELD(t.Status_Proposta_id,4,3,1)';


        /*ALTERAÇÃO 191016 - ERIC*/

        if( in_array(Yii::app()->session['usuario']->id, [1009, 974]) )
        {
          $crt->with = ['analiseDeCredito' => ['alias' => 'ac']];
          $crt->addInCondition('ac.Filial_id', [108], 'AND');
        }

        if( in_array(Yii::app()->session['usuario']->id, [973, 1007]) )
        {
          $crt->with = ['analiseDeCredito' => ['alias' => 'ac']];
          $crt->addInCondition('ac.Filial_id', [105], 'AND');
        }
        //MM Acari
        if( in_array(Yii::app()->session['usuario']->id, [1036]) )
        {
          $crt->with = ['analiseDeCredito' => ['alias' => 'ac']];
          $crt->addInCondition('ac.Filial_id', [98], 'AND');
        }
        //MM Jardim do serido
        if( in_array(Yii::app()->session['usuario']->id, [1037]) )
        {
          $crt->with = ['analiseDeCredito' => ['alias' => 'ac']];
          $crt->addInCondition('ac.Filial_id', [100], 'AND');
        }
        //MM Lagoa nova
        if( in_array(Yii::app()->session['usuario']->id, [1038]) )
        {
          $crt->with = ['analiseDeCredito' => ['alias' => 'ac']];
          $crt->addInCondition('ac.Filial_id', [102], 'AND');
        }
        //MM Parelhas
        if( in_array(Yii::app()->session['usuario']->id, [1039]) )
        {
          $crt->with = ['analiseDeCredito' => ['alias' => 'ac']];
          $crt->addInCondition('ac.Filial_id', [101], 'AND');
        }
        if( in_array(Yii::app()->session['usuario']->id, [1040]) )
        {
          $crt->with = ['analiseDeCredito' => ['alias' => 'ac']];
          $crt->addInCondition('ac.Filial_id', [37], 'AND');
        }
        /*FIM ALTERAÇÃO 191016 - ERIC*/

        /*ANALISTA DANIELA VIDAL*/
        if( in_array(Yii::app()->session['usuario']->id, [334]) )
        {
          $crt->with = ['analiseDeCredito' => ['alias' => 'ac']];
          $crt->addInCondition('ac.Filial_id', [35,36,37,38,39,40,41,42,43,44,45,46,47,48,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,118,119,120,121,122,123,124,125,126,127,139,203,204,240,273,274,268], 'AND');
        }


        $propostasAnalista = Proposta::model()->findAll($crt);
        $rows = array();

        if (count($propostasAnalista) > 0) {

            $crt->offset = $start;
            $crt->limit = 20;

            foreach (Proposta::model()->findAll($crt) as $propA) {
                /* Gatilho para atualizar o status da proposta de acordo com o host da omni */
                $propostaOmniConfig = $propA->hasOmniConfig();

                if ($propostaOmniConfig != NULL && !($propostaOmniConfig->interno)) {
                    if ($propA->Financeira_id == 7) {
                        $propA->updateSemearStatus();
                        $financeira = "Semear";
                    } else {
                        $propA->updateOmniStatus();
                        $financeira = "Omni";
                    }

                    $disabled = true;
                } else {
                    $financeira = $propA->financeira->nome; //"Credshow S/A";
                    $disabled = false;
                }
                /* Fim gatilho */

                $msgNaoLidas = $this->getQtdMsgNaoLida($propA);

                if ($msgNaoLidas > 0) {
                    $btnAtualizacoes = '<span class="label label-danger buttonpulsate">Atualizações</span>';
                } else {
                    $btnAtualizacoes = '';
                }

                $cssClass = $propA->statusProposta->cssClass; //busca classe do css
                //-----inicia montagem do botão de analise----
                $btn = '<a   ';

                if (!$disabled) { //caso deva habilitar a ação do click do botão
                    $btn .= 'href="#responsive"';
                    $btn .= ' class="modal_toggle label-warning" ';
                } else { //caso deva desabilitar a ação do click do botão
                    $btn .= ' class="modal_toggle" ';
                    $cssClass .= " label label-inverse ";
                }

                //$btn .=     ' class="modal_toggle label-warning" '                                   ;
                $btn .= ' subid="' . $propA->id . '" ';
                $btn .= ' data-toggle="' . $propA->statusProposta->data_toggle . '" ';
                $btn .= '>';
                $btn .= '   <span class="' . $cssClass . '">';
                $btn .= strtoupper($propA->statusProposta->status);
                $btn .= '   </span>';
                $btn .= '</a>';
                //-----finaliza montagem do botão de analise----


                $tempoEspera = '';
                $tempoEspera = $this->getTempoEspera($propA->data_cadastro);

                //-----------inicio alteracao-----------//
                $btnPC = '';

                //if ($propA->analiseDeCredito !== null) {
                if (isset($propA->analiseDeCredito->filial->filialHasPoliticaCredito->politicaCredito)) {
                    $politicaCredito = $propA->analiseDeCredito->filial->filialHasPoliticaCredito->politicaCredito;
                } else {
                    $politicaCredito = null;
                }


                if ($politicaCredito !== null) {
                    $btnPC .= '<button title="Política de Crédito" '
                            . '        class="btn btn-default" '
                            . '        style="font-size:78%!important;background-color:' . $politicaCredito->cor . '" > '
                            . '           <b>' . $politicaCredito->descricao . '</b>'
                            . '</button>';
                } else {
                    $btnPC .= '<span style="font-size:78%!important;" disabled="disabled">'
                            . '           <b>Sem Política de Crédito</b>'
                            . '</span>';
                }
                //-------------fim alteracao------------//
                //if ($propA->Status_Proposta_id == 3 && $propA->analise_solicitada != NULL && $propA->analise_solicitada ) {
                if ($propA->Status_Proposta_id == 3 && $propA->analise_solicitada) {
                    $btn = '<a data-propid="' . $propA->id . '" href="#modal-enviar" class="label-warning show-modal" data-toggle="modal">
                                <span class="btn label label-warning">Reanálise</span>
                            </a>';
                }

                $date = new DateTime($propA->data_cadastro);
                $dataProposta = $date->format('d/m/Y H:i:s');

                if ($propA->ehCartao()) {
                    $qtdParcelas = "-";
                    $valor = "R$ " . number_format($propA->valor_final, 2, ',', '.');
                } else {
                    $qtdParcelas = $propA->qtd_parcelas . 'x ' . "R$ " . number_format($propA->getValorParcela(), 2, ',', '.');
                    $valor = "R$ " . number_format($propA->valor - $propA->valor_entrada, 2, ',', '.');
                }

                //verificar se trata-se de uma proposta de empréstimo, assim é possível pegar o nome da filial corretamente..
                $emprestimo = Emprestimo::model()->find("habilitado AND Proposta_id = $propA->id");

                if (isset($propA->analiseDeCredito) && isset($propA->analiseDeCredito->filial)) {
                    $filial = $propA->analiseDeCredito->filial;
                } else {
                    $filial = null;
                }

                //pegando o nome da filial..
                if ($emprestimo !== NULL) {
                    $nomeFilial = $emprestimo->filial->getConcat();
                    $modalidade = "<button title='Modalidade' style='font-size:78%!important;' class='btn btn-sm btn-green'>CCP</button>";
                    if ($propA->Financeira_id == 7) {
                        $modalidade = "<button title='Modalidade' style='font-size:78%!important;' class='btn btn-sm btn-purple'>EP</button>";
                    }
                } else {
                    if ($filial !== null) {
                        $nomeFilial = $filial->getConcat();
                    } else {
                        $nomeFilial = "";
                    }
                    if ($propA->Financeira_id == 10) {
                        $modalidade = "<button title='Modalidade' style='font-size:78%!important;' class='btn btn-sm btn-info'>CSC</button>";
                    } elseif ($propA->Financeira_id == 11) {
                        $modalidade = "<button title='Modalidade' style='font-size:78%!important;' class='btn btn-sm btn-info'>CDC</button>";
                    } else {
                        $modalidade = "<button title='Modalidade' style='font-size:78%!important;' class='btn btn-sm btn-blue'>CDC</button>";
                    }
                }

                $cProgramada = CompraProgramada::model()->find('habilitado AND Proposta_id = ' . $propA->id);

                if( $cProgramada != null ){
                    $modalidade = "<button title='Modalidade' style='font-size:78%!important;' class='btn btn-sm btn-purple'>PGD</button>";
                }

                $btnDados = '<form target="_blank" method="post" action="' . Yii::app()->baseUrl . '/proposta/analise/">
                                <input type="hidden" name="id" value="' . $propA->id . '">
                                <button title="Dados do Cliente" style="padding:6px;font-size:8px;" type="submit" class="btn btn-primary">
                                    <i class="clip-search"></i></button>
                            </form>';


                /* Quando o sistema Omni cair, roda esse if
                  if( $propA->Status_Proposta_id == 4 && $propA->hasOmniConfig() != NULL )
                  {
                  $btn = '<a data-propid="'.$propA->id.'" href="#modal-enviar" class="label-warning show-modal" data-toggle="modal">
                  <span class="btn label label-warning">Reanálise</span>
                  </a>';
                  }
                 */

                $nomeCliente = "";

                if (isset($propA->analiseDeCredito->cliente->pessoa->nome)) {
                    $nomeCliente = $propA->analiseDeCredito->cliente->pessoa->nome;
                }

                if( $propA->analiseDeCredito->cliente->pessoa->getCPF()->vip ){

                    $labelVip = " - VIP ";
                    $corCpfVip = "color:#e6674a;";

                }
                else
                {
                    $labelVip = "";
                    $corCpfVip = "";
                }

                $row = array(
                    'modalidade' => $modalidade,
                    'codigo' => "<span style='".$corCpfVip."'>".$propA->codigo.$labelVip."</span>",
                    'cliente' => "<span style='".$corCpfVip."'>".strtoupper($nomeCliente)."</span>",
                    'dadosCli' => $btnDados,
                    'financeira' => "<span style='".$corCpfVip."'>".($financeira)."</span>",
                    'filial' => "<span style='".$corCpfVip."'>".strtoupper($nomeFilial)."</span>",
                    'politicaCredito' => $btnPC,
                    'valor' => $valor,
                    'valorSeguro' => number_format($propA->calcularValorDoSeguro(), 2, ',', '.'),
                    'qtd_parcelas' => $qtdParcelas,
                    'btn' => $btn,
                    'data_cadastro' => $dataProposta,
                    'tempoEspera' => $tempoEspera,
                );

                $rows[] = $row;
            }
        }

        if (!isset($retorno)) {
            $retorno = null;
        }

        if (!isset($analistaValido)) {
            $analistaValido = null;
        }

        return [
            "draw" => $draw,
            "recordsTotal" => count($rows),
            "recordsFiltered" => count($propostasAnalista),
            "data" => $rows,
            "retorno" => $retorno,
            "user" => $analistaValido,
        ];
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    // Funcao que retorna o tempo cronometrado desde que a proposta foi inserida no sistema //
    //////////////////////////////////////////////////////////////////////////////////////////
    // Data : 24-03-2015 // Autor : Andre ////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////
    public function getTempoEspera($dtCadastro) {

        $retorno = '';
        $tag = '';

        //////////////////////////////////////////////
        //define a cor em que sera mostrado o tempo //
        //caso nenhuma das setencas abaixo sejam sa-//
        //tisfeitas, permaneca a cor padrao definida//
        //abaixo                                    //
        //////////////////////////////////////////////
        //verde    - ate  5 minutos                 //
        //amarelo  - ate 10 minutos                 //
        //vermelho - ate 20 minutos                 //
        //preto    - + q 20 minutos                 //
        //////////////////////////////////////////////

        $class = 'label label-success';

        //transforma a data de hoje em um formato de data que permite operacoes matematicas
        $dataHoje = new DateTime(Date('Y-m-d H:i:s'));

        //transforma a data da proposta em um formato de data que permite operacoes matematicas
        $dataProp = new DateTime($dtCadastro);

        //obtem a subtracao entre a data de hoje e a data da proposta
        $dataDiff = $dataProp->diff($dataHoje);

        //caso tenha um ano ou mais
        if ($dataDiff->y > 0) {
            //retorno recebe o valor
            $retorno .= substr($dataDiff->y,0) . ' ano';

            //caso seja maior que 1, retorne o plural
            if ($dataDiff->y > 1) {
                $retorno .= 's';
            }

            //crescente um espaco
            $retorno .= ' ';

            //maior que vinte minutos, mude a cor para preto
            //$cor = 'black';
        }

        //caso tenha um mês ou mais
        if ($dataDiff->m > 0) {

            //caso seja maior que 1, retorne o plural
            if ($dataDiff->m > 1) {
                //retorno recebe o valor
                $retorno .= $dataDiff->m . ' meses';
            } else { // caso seja apenas 1 mês
                //retorno recebe o valor
                $retorno .= $dataDiff->m . ' mês ';
            }

            //maior que vinte minutos, mude a cor para preto
            //         $cor = 'black';
        }

        //caso tenha um dia ou mais
        if ($dataDiff->d > 0) {

            //recebe o valor
            $retorno .= $dataDiff->d . ' dia';


            //caso seja maior que 1, retorne o plural
            if ($dataDiff->d > 1) {
                $retorno .= 's';
            }

            //acrescente um espaço
            $retorno .= ' ';

            //maior que vinte minutos, mude a cor para preto
        }

        //caso possua 1 hora ou mais
        if ($dataDiff->h > 0) {
            //retorno recebe a quantidade
            $retorno .= $dataDiff->h . 'h ';

            //maior que vinte minutos, mude a cor para preto
        }

        //maior que vinte minutos ou tem retorno diferente de vazio, mude a cor para preto
        if ($dataDiff->i > 20 || !empty($retorno)) {


            $class = 'label buttonpulsate';

            //maior que dez minutos
        } elseif ($dataDiff->i > 10) {

            ///////////////////////////////////////////////////////////////////////////////////////////
            // se nao tiver retorno, significa que nao se encaixa em nenhuma das setencas anteriores //
            // entao mude a cor para vermelho                                                        //
            ///////////////////////////////////////////////////////////////////////////////////////////
            if (empty($retorno)) {


                $class = 'label label-danger';
            }

            //maior que 5 minutos
        } elseif ($dataDiff->i > 5) {

            ///////////////////////////////////////////////////////////////////////////////////////////
            // se nao tiver retorno, significa que nao se encaixa em nenhuma das setencas anteriores //
            // entao mude a cor para amarelo                                                         //
            ///////////////////////////////////////////////////////////////////////////////////////////
            if (empty($retorno)) {
                $class = 'label label-warning';
            }
        }

        //independentemente do resultado das setencas anteriores, adicione a qtd de minutos
        $retorno .= $dataDiff->i . 'm';

        //monte a tag de retorno


        $tag = '<span class="label ' . $class . '">' . $retorno . '</span>';

        return $tag;
    }

    public function listEmpresasAdmins($draw) {

        $rows = array();
        $countUsuarios = 0;
        $util = new Util;
        $usuarios = Usuario::model()->findAll('tipo_id = ' . 3);

        if (count($usuarios) > 0) {

            foreach ($usuarios as $usuario) {

                $last_view = ($usuario->data_ultima_acao == NULL ? '-----------' : $util->bd_date_to_view(substr($usuario->data_ultima_acao, 0, 10)) . ' às ' . substr($usuario->data_ultima_acao, 11));
                $countUsuarios++;

                $row = array(
                    'nome_utilizador' => $usuario->nome_utilizador,
                    'username' => $usuario->username,
                    'tipo' => $usuario->tipoId->label,
                    'empresa_filial' => $usuario->getEmpresa()->nome_fantasia,
                    'last_view' => $last_view,
                );

                $rows[] = $row;
            }
        }

        return (array(
            "draw" => $draw,
            "recordsTotal" => $countUsuarios,
            "recordsFiltered" => $countUsuarios,
            "data" => $rows
        ));
    }

    public function listUsuariosEmpresa($draw, $start, $columns) {

        $empresaHasUsuarioLogado = EmpresaHasUsuario::model()->find('Usuario_id = ' . $this->id);
        $rows = array();
        $countUsuarios = 0;
        $usuariosIds = array();
        $util = new Util;
        $btn_habdesab = "";

        $usersSearchCriteria = new CDbCriteria;
        $usersSearchCriteria->addNotInCondition('t.id', [$this->id], 'AND');

        /* FILTROS */
        if (!empty($columns[1]['search']['value']) && !ctype_space($columns[1]['search']['value'])) {
            $usersSearchCriteria->addSearchCondition('t.nome_utilizador', $columns[1]['search']['value'], 'AND');
        }

        if (!empty($columns[2]['search']['value']) && !ctype_space($columns[2]['search']['value'])) {
            $usersSearchCriteria->addSearchCondition('t.username', $columns[2]['search']['value'], 'AND');
        }
        /* FIMFILTROS */

        $recordsTotal = count(Usuario::model()->findAll($usersSearchCriteria));

        $usersSearchCriteria->offset = $start;
        $usersSearchCriteria->limit = 10;

        foreach (Usuario::model()->findAll($usersSearchCriteria) as $usuario) {

            if ($usuario->habilitado) {
                $btn_habdesab = '<button data-username="' . $usuario->username . '" data-hab="0" class="btn btn-xs btn-red btn-hab"><i class="fa fa-exclamation-circle"></i></button>';
            } else {
                $btn_habdesab = '<button data-username="' . $usuario->username . '" data-hab="1" class="btn btn-xs btn-green btn-hab" ><i class="fa fa-edit"></i></button>';
            }


            /* Saldo de pontos, se houver */
            $saldo = 0;
            /* Nome da empresa à qual o usuário está associado */
            $empresa_filial = "";

            /* Retornando o saldo dos pontos fidelidade */
            $saldo = $usuario->getSaldoPontos();

            if ($usuario->getRole() != NULL) {
                if ($usuario->getRole()->id == 5) {
                    $saldo = $usuario->getSaldoPontos();
                }
            }

            if ($usuario->returnFilial() != NULL) {
                $empresa_filial = $usuario->returnFilial()->getConcat();
            } else {
                if ($usuario->getEmpresa() != NULL) {
                    $empresa_filial = $usuario->getEmpresa()->nome_fantasia;
                }
            }

            $last_view = ($usuario->data_ultima_acao == NULL ? '-----------' : $util->bd_date_to_view(substr($usuario->data_ultima_acao, 0, 10)) . ' às ' . substr($usuario->data_ultima_acao, 11));

            $row = array(
                'btn_habdesab' => $btn_habdesab,
                'idUsuario' => $usuario->id,
                'nome_utilizador' => strtoupper($usuario->nome_utilizador),
                'username' => strtoupper($usuario->username),
                'tipo' => strtoupper($usuario->tipoId->label),
                'empresa_filial' => strtoupper($empresa_filial),
                'senha' => 'Mudar...',
                'last_view' => $last_view,
                'saldoPontos' => $saldo,
                'usuarioId' => $usuario->id
            );

            $rows[] = $row;
        }

        return (array(
            "draw" => $draw,
            "recordsTotal" => count($rows),
            "recordsFiltered" => $recordsTotal,
            "data" => $rows
        ));
    }

    public function marcarTodasFiliaisAdmin($idUsuario, $classeCheck) {

        $arrayReturn = array(
            'hasErrors' => 0,
            'title' => 'Sucesso',
            'msg' => '',
            'pntfyClass' => 'success',
            'qtdAdmin' => 0
        );

        $strPosPartial = strpos($classeCheck, "clip-checkbox-partial");
        $strPosChecked = strpos($classeCheck, "clip-checkbox-checked-2");

        if ($strPosPartial !== false || $strPosChecked !== false) {
            $arrayReturn['msg'] = 'Todas as filiais foram desmarcadas';
        } else {
            $arrayReturn['msg'] = 'Todas as filiais foram marcadas';
        }

        $filiais = Filial::model()->findAll('habilitado');

        $transaction = Yii::app()->db->beginTransaction();

        foreach ($filiais as $filial) {

            $parceiroAdmin = AdminHasParceiro::model()->find("habilitado AND Parceiro = $filial->id AND Administrador = $idUsuario");

            if ($parceiroAdmin == null) {

                $parceiroAdmin = new AdminHasParceiro;
                $parceiroAdmin->habilitado = 1;
                $parceiroAdmin->Parceiro = $filial->id;
                $parceiroAdmin->Administrador = $idUsuario;

                if (!$parceiroAdmin->save()) {

                    ob_start();
                    var_dump($parceiroAdmin);
                    $result = ob_get_clean();

                    $arrayReturn = array(
                        'hasErrors' => 1,
                        'title' => 'Erro',
                        'msg' => 'Erro ao salvar parceiroHasAdmin' .
                        $result,
                        'pntfyClass' => 'error'
                    );

                    $transaction->rollBack();

                    return $arrayReturn;
                }
            }

            if ($strPosPartial !== false || $strPosChecked !== false) {
                $parceiroAdmin->habilitado = 0;
            } else {
                $arrayReturn['qtdAdmin'] ++;
                $parceiroAdmin->habilitado = 1;
            }

            if (!$parceiroAdmin->update()) {

                ob_start();
                var_dump($parceiroAdmin);
                $result = ob_get_clean();

                $arrayReturn = array(
                    'hasErrors' => 1,
                    'title' => 'Erro',
                    'msg' => 'Erro ao salvar parceiroHasAdmin' .
                    $result,
                    'pntfyClass' => 'error'
                );

                $transaction->rollBack();

                return $arrayReturn;
            }
        }

        $arrayReturn['msg'] .= "<br> $classeCheck <br>StrPosPartial: $strPosPartial <br>StrPosChecked: $strPosChecked ";

        $transaction->commit();

        return $arrayReturn;
    }

    public function marcarFilialAdmin($idUsuario, $idFilial, $classeCheck) {

        $arrayReturn = array(
            'hasErrors' => 0,
            'title' => 'Sucesso',
            'msg' => '',
            'pntfyClass' => 'success',
            'checked' => '',
            'qtdAdmin' => 0,
            'qtdGeral' => 0
        );

        $strPos = strpos($classeCheck, "clip-checkbox-checked-2");

        $arrayReturn['checked'] = $strPos;

        if ($strPos) {
            $arrayReturn['msg'] = 'Filial desmarcada com sucesso';
        } else {
            $arrayReturn['msg'] = 'Filial marcada com sucesso';
        }

        $transaction = Yii::app()->db->beginTransaction();

        $parceiroAdmin = AdminHasParceiro::model()->find("habilitado AND Parceiro = $idFilial AND Administrador = $idUsuario");

        if ($parceiroAdmin == null) {

            $parceiroAdmin = new AdminHasParceiro;
            $parceiroAdmin->habilitado = 1;
            $parceiroAdmin->Parceiro = $idFilial;
            $parceiroAdmin->Administrador = $idUsuario;

            if (!$parceiroAdmin->save()) {

                ob_start();
                var_dump($parceiroAdmin);
                $result = ob_get_clean();

                $arrayReturn = array(
                    'hasErrors' => 1,
                    'title' => 'Erro',
                    'msg' => 'Erro ao salvar parceiroHasAdmin' .
                    $result,
                    'pntfyClass' => 'error'
                );

                $transaction->rollBack();

                return $arrayReturn;
            }
        }

        if ($strPos) {
            $parceiroAdmin->habilitado = 0;
        } else {
            $parceiroAdmin->habilitado = 1;
        }

        if (!$parceiroAdmin->update()) {

            ob_start();
            var_dump($parceiroAdmin);
            $result = ob_get_clean();

            $arrayReturn = array(
                'hasErrors' => 1,
                'title' => 'Erro',
                'msg' => 'Erro ao salvar parceiroHasAdmin' .
                $result,
                'pntfyClass' => 'error'
            );

            $transaction->rollBack();

            return $arrayReturn;
        }

        $qtdAdminFilial = Usuario::model()->qtdFiliaisAdmin($idUsuario);

        $arrayReturn['qtdAdmin'] = $qtdAdminFilial['qtdSim'];
        $arrayReturn['qtdGeral'] = $qtdAdminFilial['qtdSim'] + $qtdAdminFilial['qtdNao'];

        $transaction->commit();

        return $arrayReturn;
    }

    public function qtdFiliaisAdmin($idUsuario) {

        $retorno = ['qtdSim' => 0, 'qtdNao' => 0];

        $filiais = Filial::model()->findAll('habilitado');

        foreach ($filiais as $filial) {

            $filAdmin = AdminHasParceiro::model()->find("habilitado AND Parceiro=$filial->id AND Administrador=$idUsuario");

            if ($filAdmin == null) {
                $retorno['qtdNao'] ++;
            } else {
                $retorno['qtdSim'] ++;
            }
        }

        return $retorno;
    }

    public function doGridAnalises($draw, $start) {

        $rows = array();

        $minhasAnalises = $this->listCrediaristaAnalises();
        $countAnalises = 0;

        if (count($minhasAnalises) > 0) {
            $countAnalises = count($minhasAnalises);
            $util = new Util;

            foreach ($this->listCrediaristaAnalises($start) as $analise) {

                $entrada = ( $analise->entrada != NULL ? number_format($analise->entrada, 2, ',', '.') : "0,00" );
                $propostas = Proposta::model()->findAll('Analise_de_Credito_id = ' . $analise->id);
                $htmlTableProps = "";


                if (count($propostas) > 0) {
                    foreach ($propostas as $proposta) {

                        if ($proposta->Status_Proposta_id == 3 || !$proposta->habilitado) {
                            $btnDetails = '';
                            $btnDetails .= '<form>';
                            $btnDetails .= '	<button disabled="disabled" style="padding:6px;font-size:8px;" type="submit" class="btn btn-primary"><i class="clip-search"></i></button>';
                            $btnDetails .= '</form>';
                        } else {
                            $btnDetails = '';
                            $btnDetails .= '<form method="post" action="' . Yii::app()->getBaseUrl(true) . '/proposta/more">';
                            $btnDetails .= '	<input type="hidden" name="id" value="' . $proposta->id . '">';
                            $btnDetails .= '	<button style="padding:6px;font-size:8px;" type="submit" class="btn btn-primary"><i class="clip-search"></i></button>';
                            $btnDetails .= '</form>';
                        }

                        $htmlTableProps = '<tr>'
                                . '<td>' . $btnDetails . '</td>'
                                . '<td>' . $proposta->codigo . '</td>'
                                . '<td>' . $proposta->financeira->nome . '</td>'
                                . '<td>' . number_format($proposta->valor, 2, ',', '.') . '</td>'
                                . '<td>' . number_format($proposta->valor_entrada, 2, ',', '.') . '</td>'
                                . '<td>' . number_format($proposta->getValorFinanciadoOmni(), 2, ',', '.') . '</td>'
                                . '<td>' . $proposta->qtd_parcelas . ' x ' . number_format($proposta->getValorParcela(), 2, ',', '.') . '</td>'
                                . '<td>' . number_format($proposta->getValorParcela() * $proposta->qtd_parcelas, 2, ',', '.') . '</td>'
                                . '<td>' . '<span class="' . $proposta->statusProposta->cssClass . '">' . $proposta->statusProposta->status . '</span>' . '</td>'
                                . '</tr>';
                    }
                }

                $row = array(
                    'btn-more' => '',
                    'codigo' => $analise->codigo,
                    'cliente' => strtoupper($analise->cliente->pessoa->nome),
                    'valor' => number_format($analise->valor, 2, ',', '.'),
                    'entrada' => $entrada,
                    'seguro' => $entrada,
                    'data' => $util->bd_date_to_view(substr($analise->data_cadastro, 0, 10)) . ' às ' . substr($analise->data_cadastro, 11),
                    'analises' => array(
                        '<table class="table table-striped table-bordered table-hover table-full-width">'
                        . '<thead>'
                        . '<tr>'
                        . '<th></th>'
                        . '<th>Cód</th>'
                        . '<th>Financeira</th>'
                        . '<th>R$ Solicitado</th>'
                        . '<th>R$ Entrada</th>'
                        . '<th>R$ Financiado</th>'
                        . '<th>Parcelamento</th>'
                        . '<th>R$ Final</th>'
                        . '<th>Status</th>'
                        . '</tr>'
                        . '</thead>'
                        . '<tbody>' . $htmlTableProps . '</tbody>'
                        . '</table>',
                    ),
                );

                $rows[] = $row;
                $htmlTableProps = "";
            }
        }

        return (array(
            "draw" => $draw,
            "recordsTotal" => count($rows),
            "recordsFiltered" => $countAnalises,
            "data" => $rows
        ));
    }

    public function listarFiliaisSelect($idUsuario) {

        $html = '';

        $filiais = Filial::model()->findAll('habilitado');

        $filial = FilialHasUsuario::model()->find('Usuario_id = ' . $idUsuario);

        $html .= '<select class="form-control search-select filial">';

        $html .= '  <option value="0" ';

        $html .= ' >';
        $html .= 'Selecione uma Filial...';
        $html .= '</option>';

        foreach ($filiais as $f) {

            $html .= '<option value="' . $f->id . '" ';

            if ($f->id == $filial->Filial_id) {
                $html .= 'selected';
            }

            $html .= ' >' . $f->getConcat() . '</option>';
        }

        $html .= '</select>';

        return (
                array(
                    "html" => $html
                )
                );
    }

    public function valorTotalResgates() {
        $criteria = new CDbCriteria;
        $criteria->addInCondition('t.Usuario_id', [$this->id], 'AND');

        $valorTotal = 0;

        foreach (ResgatePontos::model()->findAll($criteria) as $resgate) {
            $valorTotal += $resgate->valor;
        }

        return $valorTotal;
    }

    public function valorTotalPropostasCrediarista() {
        $criteria = new CDbCriteria;
        $criteria->with = ['analiseDeCredito' => ['alias' => 'ac']];
        $criteria->addInCondition('ac.Usuario_id', [$this->id], 'AND');
        $criteria->addInCondition('t.Status_Proposta_id', [2], 'AND');
        $criteria->addInCondition('t.habilitado', [1], 'AND');
        $criteria->addInCondition('t.titulos_gerados', [1], 'AND');

        $valorTotal = 0;

        foreach (Proposta::model()->findAll($criteria) as $proposta) {
            $valorTotal += ($proposta->valor - $proposta->valor_entrada); //$proposta->getValorFinal();
        }
        return $valorTotal;
    }

    public function getSaldoPontos() {
        return $this->valorTotalPropostasCrediarista() - $this->valorTotalResgates();
    }

    public function gridPontosCrediaristas($draw, $start, $nome_filter) {

        $criteria = new CDbCriteria;
        $criteria->addInCondition('t.tipo_id', [5], 'AND');
        $criteria->addInCondition('t.habilitado', [1], 'AND');
        $criteria->order = 't.nome_utilizador ASC';

        if (isset($nome_filter) && $nome_filter != NULL && trim($nome_filter) != '') {
            $criteria->addSearchCondition('t.nome_utilizador', $nome_filter);
        }

        $total = count(Usuario::model()->findAll($criteria));

        if ($total > 0) {
            $criteria->offset = $start;
            $criteria->limit = 10;
        }


        $rows = [];

        foreach (Usuario::model()->findAll($criteria) as $crediarista) {
            $rows[] = [
                'crediarista' => strtoupper($crediarista->nome_utilizador),
                'filial' => strtoupper($crediarista->returnFilial()->getConcat()),
                'pts_total' => $crediarista->valorTotalPropostasCrediarista(),
                'pts_resgate' => $crediarista->valorTotalResgates(),
                'pts_saldo' => $crediarista->getSaldoPontos(),
                'btn_down' => '<a data-crediarista-id="' . $crediarista->id . '" data-toggle="modal"  href="#modal_form_resgatar_premio" class="btn_down btn btn-info btn-sm"><i class="clip-download"></i></>'
            ];
        }

        return [
            "draw" => $draw,
            "recordsFiltered" => $total,
            "recordsTotal" => count($rows),
            "data" => $rows,
        ];
    }

    public function temEmprestimo() {

        $idEmprestimo = ConfigLN::model()->find("habilitado AND parametro = 'idServicoEmprestimo'")->valor;

        $filialHasUsuario = FilialHasUsuario::model()->find("habilitado AND Usuario_id = $this->id");

        $filialHasServico = FilialHasServico::model()->find("habilitado AND Filial_id = $filialHasUsuario->Filial_id AND Servico_id = $idEmprestimo");

        $Financeira = null;
        if ($filialHasServico !== null) {
            $Financeira = FinanceiraHasFilialHasServico::model()->find('habilitado AND Filial_has_Servico_id = ' . $filialHasServico->id);
        }

        $url = "/analiseDeCredito/iniciarEmprestimo/";
        if ($Financeira !== null) {
            if ($Financeira->Financeira_id == 7) {
                $url = "/analiseDeCredito/iniciarEmprestimoSemear/";
            }
        }

        return [
            'temEmprestimo' => $filialHasServico !== null,
            'urlServico' => $url
        ];
    }

    public function temCDC() {

        $idCDC = ConfigLN::model()->find("habilitado AND parametro = 'idServicoCDC'")->valor;

        $filialHasUsuario = FilialHasUsuario::model()->find("habilitado AND Usuario_id = $this->id");

        $filialHasServico = FilialHasServico::model()->find("habilitado AND Filial_id = $filialHasUsuario->Filial_id AND Servico_id = $idCDC");

        return ($filialHasServico !== null);
    }

    public function temRecebimentoInterno() {

        $filialHasUsuario = FilialHasUsuario::model()->find("habilitado AND Usuario_id = $this->id");

        $filialHasServico = FilialHasServico::model()->find("habilitado AND Filial_id = $filialHasUsuario->Filial_id AND Servico_id = 3");

        return ($filialHasServico !== null);
    }

    public function temPGD() {
        
        $filialHasUsuario = FilialHasUsuario::model()->find("habilitado AND Usuario_id = $this->id");
        
        $filialHasServico = FilialHasServico::model()->find("habilitado AND Filial_id = $filialHasUsuario->Filial_id AND Servico_id = 5");
        
        return ($filialHasServico !== null);
    }

    public function listaPremios() {
        //$sql = "SELECT * FROM sigac.PremioFidelidade as PF WHERE PF.ativo = 1 AND DATEDIFF('" . date('Y-m-d') . "', PF.data_cadastro) <= 5"; //para testar nas maquinas locais
        $sql = "SELECT * FROM beta.PremioFidelidade as PF WHERE PF.ativo = 1 AND DATEDIFF('" . date('Y-m-d') . "', PF.data_cadastro) <= 5";
        $rows = [];
        $novos = 0;

        try {
            if (sizeof(Yii::app()->db->createCommand($sql)->queryAll()) > 0) {

                foreach (Yii::app()->db->createCommand($sql)->queryAll() as $premio) {
                    $rows[] = [
                        $premio['descricao'],
                        $premio['urlimg'],
                        $premio['valor']
                    ];

                    $novos = 1;
                }
            }
        } catch (Exception $e) {
            $sql = "SELECT * FROM sigac.PremioFidelidade as PF WHERE PF.ativo = 1 AND DATEDIFF('" . date('Y-m-d') . "', PF.data_cadastro) <= 5";

            if (sizeof(Yii::app()->db->createCommand($sql)->queryAll()) > 0) {

                foreach (Yii::app()->db->createCommand($sql)->queryAll() as $premio) {
                    $rows[] = [
                        $premio['descricao'],
                        $premio['urlimg'],
                        $premio['valor']
                    ];

                    $novos = 1;
                }
            }
        }


        return [
            "data" => $rows,
            "listar" => $novos
        ];
    }

    public function mostrarPremios() {
        /*        if (substr($this->getDataUlt(), 0, 10) < Date("Y-m-d") AND $this->listaPremios()['listar'] == 1) {
          return 1;
          } else { */
        return 0;
        //        }
    }

    public function getDataUlt() {
        $user = Usuario::model()->findByPk($this->id);
        return $user->data_ultima_acao;
    }

    public function listFiliaisAdmin($idUsuario, $draw, $start, $search) {

        $rows = [];

        $filiais = Filial::model()->findAll('habilitado');

        $recordsTotal = count($filiais);

        //$recordsFiltered    = count($filiais);
        $recordsFiltered = 0;

        foreach ($filiais as $filial) {

            if (!empty($search['value'])) {
                $strPos = strpos($filial->getConcat(), strtoupper($search['value']));

                if ($strPos <= 0) {
                    continue;
                }
            }

            $recordsFiltered++;
        }

        $criteria = new CDbCriteria;
        $criteria->offset = $start;
        $criteria->limit = 10;
        $criteria->order = 't.nome_fantasia asc';

        $filiais = Filial::model()->findAll($criteria);

        foreach ($filiais as $filial) {

            if (!empty($search['value'])) {
                $strPos = strpos($filial->getConcat(), strtoupper($search['value']));

                if ($strPos <= 0) {
                    continue;
                }
            }

            $filAdmin = AdminHasParceiro::model()->find("habilitado AND Parceiro=$filial->id AND Administrador=$idUsuario");

            if ($filAdmin == null) {
                $checkBox = '<i class="iCheckFilial clip-checkbox-unchecked-2"></i>';
            } else {
                $checkBox = '<i class="iCheckFilial clip-checkbox-checked-2"></i>';
            }

            $row = array(
                'checkFilial' => $checkBox,
                'filial' => $filial->getConcat(),
                'filialID' => $filial->id
            );

            $rows[] = $row;
        }

        return (
                array(
                    "draw" => $draw,
                    "recordsTotal" => $recordsTotal,
                    "recordsFiltered" => $recordsFiltered,
                    "data" => $rows
                )
                );
    }

}
