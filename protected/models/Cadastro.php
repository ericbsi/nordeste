<?php

class Cadastro extends CActiveRecord{
	
	public function atualizarCadastro($CadastroNovosDados, $CadastroId)
	{
            
            $retorno["hasErrors"] = false           ;
            $retorno["msg"      ] = "Atualizado"    ;

            $Cadastro 				= Cadastro::model()->findByPk($CadastroId);
            $Cadastro->attributes 	= $CadastroNovosDados;
            $Cadastro->analfabeto   = $CadastroNovosDados['analfabeto'];
            $Cadastro->atualizar 	= 0;

            if( !$Cadastro->update() )
            {
                    
                ob_start();
                var_dump($Cadastro->getErrors());
                $resultadoErro = ob_get_clean();
            
                $retorno["hasErrors"] = true;
                $retorno["msg"      ] = $resultadoErro . ". Linha 24";
            
            }
            else{
            	$sigacLogCadastro = new SigacLog;
                $sigacLogCadastro->saveLog('Atualização de Cadastro de Cliente', 'Cadastro', $Cadastro->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Cadastro do Cliente " . $Cadastro->id . " atualizado.", "127.0.0.1", null, Yii::app()->session->getSessionId());
            }
            
            return $retorno;
            
	}

	/*Deprecated*/
	public function atualizar($Cadastro, $cadastro_id, $Estado_Civil){

		if( $cadastro_id != NULL )
		{
			$CadastroCliente 										= Cadastro::model()->findByPk($cadastro_id);
			$CadastroCliente->attributes 							= $Cadastro;
			$CadastroCliente->cliente->pessoa->Estado_Civil_id 		= $Estado_Civil;

			$CadastroCliente->update();
			$CadastroCliente->cliente->pessoa->update();

			$sigacLog                               = new SigacLog;
			$sigacLog->saveLog('Atualização de cadastro','Cadastro', $CadastroCliente->id, date('Y-m-d H:i:s'),1, Yii::app()->session['usuario']->id, "Usuario ". Yii::app()->session['usuario']->username . " atualizou o cadastro do cliente " . $CadastroCliente->cliente->pessoa->nome, "127.0.0.1",null, Yii::app()->session->getSessionId() );
		}
	}

	public function loadFormEdit( $id ){

		$cadastro 						= Cadastro::model()->findByPk( $id );

	    return array(

	    	'fields' 					=> array(

		    	'id' 					=> array(
		    		'value' 			=> $cadastro->id,
		    		'type' 				=> 'text',
		    		'input_bind'		=> 'cadastro_id',
		    	),
		    	'nome_do_pai' 			=> array(
		    		'value' 			=> $cadastro->nome_do_pai,
		    		'type' 				=> 'text',
		    		'input_bind'		=> 'nome_do_pai',
		    	),
		    	'nome_da_mae' 			=> array(
		    		'value' 			=> $cadastro->nome_da_mae,
		    		'type' 				=> 'text',
		    		'input_bind'		=> 'nome_da_mae',
		    	),
		    	'numero_de_dependentes'	=> array(
		    		'value' 			=> $cadastro->numero_de_dependentes,
		    		'type' 				=> 'text',
		    		'input_bind'		=> 'numero_de_dependentes',
		    	),
		    	'conjugue_compoe_renda'	=> array(
		    		'value' 			=> $cadastro->conjugue_compoe_renda,
		    		'type' 				=> 'select',
		    		'input_bind'		=> 'conjugue_compoe_renda',
		    	),
		    	'cliente_da_casa'		=> array(
		    		'value' 			=> $cadastro->cliente_da_casa,
		    		'type' 				=> 'select',
		    		'input_bind'		=> 'Cadastro_cliente_da_casa',
		    	),
		    	'titular_do_cpf'		=> array(
		    		'value' 			=> $cadastro->titular_do_cpf,
		    		'type' 				=> 'select',
		    		'input_bind'		=> 'titular_do_cpf',
		    	),
		    	'estado_civil'			=> array(
		    		'value' 			=> $cadastro->cliente->pessoa->Estado_Civil_id,
		    		'type' 				=> 'select',
		    		'input_bind'		=> 'Estado_Civil_id',
		    	),
                        'analfabeto'                    => array(
                                'value'                         => $cadastro->analfabeto,
                                'type'                          => 'select',
                                'input_bind'                    => 'select-analfabeto',
                        )
	    	),
	    	'form_params'				=> array(
	    		'id'					=> 'form-add-cadastro',
	    		'action'				=> '/cadastro/update/',
	    	),
	    	'modal_id'					=> 'modal_form_new_cadastro'
	    );
	}

	public function tableName(){

		return 'Cadastro';
	}

	
	public function rules(){
		
		return array(
			array('Cliente_id, Empresa_id', 'required'),
			array('numero_de_dependentes, conjugue_compoe_renda, cliente_da_casa, titular_do_cpf, Cliente_id, Empresa_id, habilitado, atualizar', 'numerical', 'integerOnly'=>true),
			array('nome_do_pai, nome_da_mae', 'length', 'max'=>150),
			array('data_cadastro, data_cadastro_br', 'safe'),
			array('id, numero_de_dependentes, data_cadastro, data_cadastro_br, conjugue_compoe_renda, nome_do_pai, nome_da_mae, titular_do_cpf, Cliente_id, Empresa_id, habilitado, atualizar', 'safe', 'on'=>'search'),
		);
	}

	public function relations(){

		return array(
			'cliente' => array(self::BELONGS_TO, 'Cliente', 'Cliente_id'),
			'empresa' => array(self::BELONGS_TO, 'Empresa', 'Empresa_id'),
			'filials' => array(self::MANY_MANY, 'Filial', 'Cadastro_has_Filial(Cadastro_id, Filial_id)'),
		);
	}


	public function attributeLabels(){

		return array(
			'id' 						=> 'ID',
			'conjugue_compoe_renda' 	=> 'Conjugue Compoe Renda',
			'nome_do_pai' 				=> 'Nome Do Pai',
			'nome_da_mae' 				=> 'Nome Da Mae',
			'titular_do_cpf' 			=> 'Titular Do Cpf',
			'Cliente_id' 				=> 'Cliente',
			'Empresa_id' 				=> 'Empresa',
			'habilitado' 				=> 'Habilitado',
			'data_cadastro' 			=> 'Data Cadastro',
			'data_cadastro_br' 			=> 'Data / Hora Cadastro',
			'numero_de_dependentes' 	=> 'Número de dependentes',
			'atualizar' 				=> 'Atualizar',
			'cliente_da_casa'			=> 'É Cliente da casa?',
		);
	}

	public function search(){

		$criteria = new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('conjugue_compoe_renda',$this->conjugue_compoe_renda);
		$criteria->compare('nome_do_pai',$this->nome_do_pai,true);
		$criteria->compare('nome_da_mae',$this->nome_da_mae,true);
		$criteria->compare('titular_do_cpf',$this->titular_do_cpf);
		$criteria->compare('Cliente_id',$this->Cliente_id);
		$criteria->compare('Empresa_id',$this->Empresa_id);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro);
		$criteria->compare('data_cadastro_br',$this->data_cadastro_br);
		$criteria->compare('numero_de_dependentes',$this->numero_de_dependentes);
		$criteria->compare('atualizar',$this->atualizar);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__){
		
		return parent::model($className);
	}
}
