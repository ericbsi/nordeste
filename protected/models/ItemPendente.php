<?php

class ItemPendente extends CActiveRecord
{	

	public function buscarItem( $propostaId )
	{
		return ItemPendente::model()->find('ItemPendente = ' . $propostaId . ' AND habilitado');
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ItemPendente';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ItemPendente, RegistroDePendencias_id, observacao', 'required'),
			array('ItemPendente, habilitado, RegistroDePendencias_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, ItemPendente, habilitado, RegistroDePendencias_id, observacao', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'itemPendente' 			=> array(self::BELONGS_TO, 'Proposta', 				'ItemPendente'),
			'registroDePendencias' 	=> array(self::BELONGS_TO, 'RegistroDePendencias', 	'RegistroDePendencias_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'ItemPendente' => 'Item Pendente',
			'habilitado' => 'Habilitado',
			'RegistroDePendencias_id' => 'Registro De Pendencias',
			'observacao' => 'Observacao',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('ItemPendente',$this->ItemPendente);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('RegistroDePendencias_id',$this->RegistroDePendencias_id);
		$criteria->compare('observacao',$this->observacao,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ItemPendente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
