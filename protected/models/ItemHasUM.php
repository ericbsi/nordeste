<?php

/**
 * This is the model class for table "Item_has_UM".
 *
 * The followings are the available columns in table 'Item_has_UM':
 * @property integer $id
 * @property integer $Item_do_Estoque_id
 * @property integer $Unidade_de_Medida_id
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property ItemDoEstoque $itemDoEstoque
 * @property UnidadeDeMedida $unidadeDeMedida
 */
class ItemHasUM extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Item_has_UM';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Item_do_Estoque_id, Unidade_de_Medida_id', 'required'),
			array('Item_do_Estoque_id, Unidade_de_Medida_id, habilitado', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, Item_do_Estoque_id, Unidade_de_Medida_id, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'itemDoEstoque' => array(self::BELONGS_TO, 'ItemDoEstoque', 'Item_do_Estoque_id'),
			'unidadeDeMedida' => array(self::BELONGS_TO, 'UnidadeDeMedida', 'Unidade_de_Medida_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Item_do_Estoque_id' => 'Item Do Estoque',
			'Unidade_de_Medida_id' => 'Unidade De Medida',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Item_do_Estoque_id',$this->Item_do_Estoque_id);
		$criteria->compare('Unidade_de_Medida_id',$this->Unidade_de_Medida_id);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ItemHasUM the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
