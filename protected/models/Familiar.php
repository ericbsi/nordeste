<?php


class Familiar extends CActiveRecord{

	public function criarLacoFamiliar( $pessoa1, $pessoa2, $lacoId )
	{
		$arrReturn 		= array(
			'hasErrors' => true
		);

		$cliente1       				= Cliente::model()->findByPk($pessoa1);
		$cliente2       				= Cliente::model()->findByPk($pessoa2);

		$PapelRelacao1 					= new PapelRelacao;
		$PapelRelacao2					= new PapelRelacao;

		$PapelRelacao1->Parentesco_id	= $lacoId;
		$PapelRelacao1->Pessoa_id 		= $cliente1->pessoa->id;
		$PapelRelacao1->habilitado 		= 1;

		$PapelRelacao2->Parentesco_id	= $lacoId;
		$PapelRelacao2->Pessoa_id 		= $cliente2->pessoa->id;
		$PapelRelacao2->habilitado 		= 1;

		if( $PapelRelacao1->save() && $PapelRelacao2->save() )
		{
			$relacaoFamiliar 				= new RelacaoFamiliar;
			$relacaoFamiliar->Parente_1 	= $PapelRelacao1->id;
			$relacaoFamiliar->Parente_2 	= $PapelRelacao2->id;
			$relacaoFamiliar->habilitado 	= 1;

			if( $relacaoFamiliar->save() )
			{
				
				$arrReturn['hasErrors']   	= false;
				
				$arrReturn['msgConfig']['pnotify'][] = array(
				    'titulo'    => 'Ação realizada com sucesso!',
				    'texto'     => 'Relação familiar entre ' . strtoupper($cliente1->pessoa->nome) . ' e ' . strtoupper($cliente2->pessoa->nome) . ' cadastrada com sucesso!',
					'tipo'      => 'success',
			    );
			}
			else
			{
				$arrReturn['msgConfig']['pnotify'][] = array(
				    'titulo'    => 'Erros foram encontrados!',
				    'texto'     => 'Não foi possível criar esta relação familiar!',
					'tipo'      => 'error',
		    	);
			}
		}
		else
		{
			$arrReturn['msgConfig']['pnotify'][] = array(
			    'titulo'    => 'Erros foram encontrados!',
			    'texto'     => 'Não foi possível criar esta relação familiar!',
				'tipo'      => 'error',
		    );
		}
		return $arrReturn;
	}

	public function persist( $CPFConjuge, $Conjuge_id, $Conjuge, $DadosProfissionaisConjuge, $EnderecoProfissConjuge, $RGConjuge, $TelefoneProfissConjuge, $Cliente_id, $CadastroConjuge, $Action )
	{
		$arrReturn = array(
			'hasErrors' => false
		);

		/*$cliente 								= Cliente::model()->findByPk($Cliente_id);

		$statusDoCadastro                       = $cliente->clienteCadastroStatus();
        $arrReturn['statusDoCadastro']			= $statusDoCadastro['status'];*/

		if( $Action == 'join' )
		{
			return $this->criarLacoFamiliar($Cliente_id, $Conjuge_id, 7);
		}

		else if( $Action == 'conjugeNewDp' )
		{
			if( DadosProfissionais::model()->persist( $DadosProfissionaisConjuge, $EnderecoProfissConjuge, $TelefoneProfissConjuge, $Conjuge_id, NULL, 'new' ) != NULL )
			{
				return $this->criarLacoFamiliar($Cliente_id, $Conjuge_id, 7);
			}
		}

		else if( $Action == 'new' )
		{
			$CadastroConjuge['nome_do_pai']  			= NULL;
			$CadastroConjuge['conjugue_compoe_renda']  	= 1;

			$ConjAsCliente   = Cliente::model()->novo($Conjuge, $CPFConjuge, $RGConjuge, $CadastroConjuge);

			if( $ConjAsCliente != NULL )
			{

				if( DadosProfissionais::model()->persist( $DadosProfissionaisConjuge, $EnderecoProfissConjuge, $TelefoneProfissConjuge, $ConjAsCliente->id, NULL,  'new' ) != NULL )
				{
					return $this->criarLacoFamiliar($Cliente_id, $ConjAsCliente->id, 7);
				}
			}
		}

		else
		{
			return NULL;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Familiar the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Familiar';
	}


	public function novo( $Pessoa, $Parentesco_id, $Cliente_id ){

		if( !empty( $Pessoa['nome'] ) )
		{
			$cliente 									= Cliente::model()->findByPk( $Cliente_id );
			$util 										= new Util;
			$contato									= new Contato;
			$pessoaParente 								= new Pessoa;
			$parenteCadastro 							= new Cadastro;
			$pessoaParente->attributes 					= $Pessoa;
			$parentesco 								= Parentesco::model()->findByPk( $Parentesco_id );
			$parentesco_familiar 						= $parentesco->und;

			if( !empty($Pessoa['nascimento']) )
			{
				$pessoaParente->nascimento 				= $util->view_date_to_bd($Pessoa['nascimento']);
			}	

			$pessoaParente->data_cadastro 				= date('Y-m-d H:i:s');
			$pessoaParente->data_cadastro_br 			= date('d/m/Y');
			$contato->data_cadastro 					= date('Y-m-d H:i:s');
			$contato->data_cadastro_br 					= date('d/m/Y');

			if( $parentesco->id == 7 )
			{
				$pessoaParente->Estado_Civil_id 		= 2;
			}

			if( $contato->save() )
			{
				$pessoaParente->Contato_id 				= $contato->id;
			}

			if( $pessoaParente->save() )
			{	
				/*Tornando o parente um cliente do sistema*/
				$parenteCliente 						= new Cliente;
				$parenteCliente->Pessoa_id 				= $pessoaParente->id;

				if( $parenteCliente->save() )
				{
					$parenteCadastro->Cliente_id 		= $parenteCliente->id;
					$parenteCadastro->Empresa_id 		= Yii::app()->session['usuario']->getEmpresa()->id;
					$contato->data_cadastro 			= date('Y-m-d H:i:s');
					$contato->data_cadastro_br 			= date('d/m/Y');
					$parenteCadastro->save();
				}

				/*Criando relação*/
				$PapelParenteRelacao 					= new PapelRelacao;
				$PapelParenteRelacao->Parentesco_id 	= $parentesco_familiar;
				$PapelParenteRelacao->Pessoa_id 		= $pessoaParente->id;
				$PapelParenteRelacao->habilitado 		= 1;


				$PapelClienteRelacao 					= new PapelRelacao;
				$PapelClienteRelacao->Parentesco_id 	= $parentesco->id;
				$PapelClienteRelacao->Pessoa_id 		= $cliente->pessoa->id;
				$PapelClienteRelacao->habilitado 		= 1;

				if( $PapelParenteRelacao->save() && $PapelClienteRelacao->save() )
				{
					$relacaoFamiliar 					= new RelacaoFamiliar;
					$relacaoFamiliar->Parente_1 		= $PapelParenteRelacao->id;
					$relacaoFamiliar->Parente_2 		= $PapelClienteRelacao->id;
					$relacaoFamiliar->habilitado 		= 1;
					$relacaoFamiliar->save();
				}
			}
		}
	}

	public function getDocs(){

		$arrDocumentos = array();

		$pessoaHasDocumento = PessoaHasDocumento::model()->findAll( 'Pessoa_id =' . $this->pessoa->id );

		foreach ( $pessoaHasDocumento as $phd ) {

			$doc = Documento::model()->findByPk( $phd->Documento_id );

			array_push($arrDocumentos, $doc);

		}

		return $arrDocumentos;
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Pessoa_id, Tipo_Familiar_id, Cliente_id', 'required'),
			array('Pessoa_id, Tipo_Familiar_id, Cliente_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, Pessoa_id, Tipo_Familiar_id, Cliente_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pessoa' => array(self::BELONGS_TO, 'Pessoa', 'Pessoa_id'),
			'tipoFamiliar' => array(self::BELONGS_TO, 'TipoFamiliar', 'Tipo_Familiar_id'),
			'cliente' => array(self::BELONGS_TO, 'Cliente', 'Cliente_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Pessoa_id' => 'Pessoa',
			'Tipo_Familiar_id' => 'Tipo Familiar',
			'Cliente_id' => 'Cliente',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Pessoa_id',$this->Pessoa_id);
		$criteria->compare('Tipo_Familiar_id',$this->Tipo_Familiar_id);
		$criteria->compare('Cliente_id',$this->Cliente_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function checkConjugeStatus( $CpfConjuge, $Cliente_id )
	{
		$util 				= new Util;
		$arrReturn 			= array('hasErrors' => true, 'enableSubmit' => false);

		$cliente 			= Cliente::model()->findByPk($Cliente_id);

		if( $cliente != NULL )
		{
			/*Verifica se o cliente já é casado*/
			if( $cliente->pessoa->casada() != NULL )
			{
				$arrReturn['msgConfig']['pnotify'][] = array(
				    'titulo'    => 'Erros foram encontrados!',
				    'texto'     => 'O Cliente já possui relação matrimonial no sistema!',
					'tipo'      => 'error',
		        );

		        $arrReturn['formConfig'][] 	= array(
					'fields' 			 	=> array(
						'ControllerAction'	=> array(
					    	'value'     => 'nothing',
					        'type'      => 'text',
					        'input_bind'=> 'ControllerConjugeAction',
						),
					)
				);
			}

			/*Cliente não é casado. decorrerm outras validações*/
			else
			{
				/*1° Verifica se o cpf do conjuge é o mesmo do cliente*/
				if( $CpfConjuge == $cliente->pessoa->getCPF()->numero )
				{
					$arrReturn['msgConfig']['pnotify'][] = array(
			            'titulo'    => 'Erros foram encontrados!',
			            'texto'     => 'O CPF do Cônjuge não pode ser igual ao CPF do cliente!',
			            'tipo'      => 'error',
			        );

			        $arrReturn['formConfig'][] 	= array(
						'fields' 			 	=> array(
							'ControllerAction'	=> array(
						        'value'     => 'nothing',
						        'type'      => 'text',
						    	'input_bind'=> 'ControllerConjugeAction',
						    ),
						)
					);
				}

				/*CPF é diferente, decorrerm outras validações*/
				else
				{
					$conjuge  	= Cliente::model()->getClienteByCpf($CpfConjuge);

					/*Conjuge já está cadastrado no sistema*/
					if( $conjuge != NULL )
					{	
						$conjugeCad	= Cadastro::model()->find('Cliente_id = ' . $conjuge->id);
						$arrReturn['hasErrors'] 		= false;

						/*VERIFICAR se o conjuge já possui relação matrimonial*/
						if( $conjuge->pessoa->casada() != NULL )
						{
							$arrReturn['msgConfig']['pnotify'][] = array(
					            'titulo'    => 'Erros foram encontrados!',
					            'texto'     => 'Titular do CPF já possui relação matrimonial no sistema!',
					            'tipo'      => 'error',
			        		);

			        		$arrReturn['formConfig'][] 	= array(
								'fields' 			 	=> array(
									'ControllerAction'	=> array(
						                'value'     => 'nothing',
						                'type'      => 'text',
						            	'input_bind'=> 'ControllerConjugeAction',
						            ),
								)
							);
						}				
											
						else
						{	

							$dataEmissaoRgConjuge 					= NULL;

							if( $conjuge->pessoa->getRG()->data_emissao != NULL )
							{
								$dataEmissaoRgConjuge 				= $util->bd_date_to_view($conjuge->pessoa->getRG()->data_emissao);
							}

							$arrReturn['enableSubmit'] 				= true;

							$arrReturn['msgConfig']['pnotify'][] 	= array(
					            'titulo'    						=> 'Conjuge já cadastrado!',
					            'texto'     						=> 'O CPF do Cônjuge está cadastrado em nossos sistemas!',
					            'tipo'      						=> 'success',
				        	);

							$arrReturn['formConfig'][] = array(

				                'fields'                    => array(
				                    'id'                    => array(
				                        'value'             => $conjuge->id,
				                        'type'              => 'text',
				                        'input_bind'        => 'Conjuge_id',
				                    ),
				                    'nome'                  => array(
				                        'value'             => $conjuge->pessoa->nome,
				                        'type'              => 'text',
				                        'input_bind'        => 'conjuge_nome',
				                    ),
				                    'sexo'                  => array(
				                        'value'             => $conjuge->pessoa->sexo,
				                        'type'              => 'select',
				                        'input_bind'        => 'Conjuge_sexo',
				                    ),
				                    'nascimento'            => array(
				                        'value'             => $util->bd_date_to_view($conjuge->pessoa->nascimento),
				                        'type'              => 'text',
				                        'input_bind'        => 'conjuge_nascimento',
				                    ),
				                    'rg'                    => array(
				                        'value'             => $conjuge->pessoa->getRG()->numero,
				                        'type'              => 'text',
				                        'input_bind'        => 'rg_conjuge',
				                    ),
				                    'rg_orgao_emissor'      => array(
				                        'value'             => $conjuge->pessoa->getRG()->orgao_emissor,
				                        'type'              => 'text',
				                        'input_bind'        => 'RG_conjuge_orgao_emissor',
				                    ),
				                    'RG_uf_emissor'         => array(
				                        'value'             => $conjuge->pessoa->getRG()->uf_emissor,
				                        'type'              => 'select',
				                        'input_bind'        => 'RGConjuge_uf_emissor',
				                    ),
				                    'RG_emissao'            => array(
				                        'value'             => $dataEmissaoRgConjuge,
				                        'type'              => 'text',
				                        'input_bind'        => 'RG_conjuge_data_emissao',
				                    ),
				                    'select-nacionalidade'  => array(
				                        'value'             => $conjuge->pessoa->nacionalidade,
				                        'type'              => 'select',
				                        'input_bind'        => 'select-nacionalidade-conjuge',
				                    ),
				                    'Pessoa_naturalidade'   => array(
				                        'value'             => $conjuge->pessoa->naturalidade,
				                        'type'              => 'select',
				                        'input_bind'        => 'Conjuge_naturalidade',
				                    ),
				                    'Pessoa_Estado_Civil_id'=> array(
				                        'value'             => $conjuge->pessoa->Estado_Civil_id,
				                        'type'              => 'select',
				                        'input_bind'        => 'Conjuge_Estado_Civil_id',
				                    ),
				                    /*'nome_da_mae'           => array(
				                        'value'             => $conjuge->getCadastro()->nome_da_mae,
				                        'type'              => 'text',
				                        'input_bind'        => 'Cadastro_nome_da_mae',
				                    ),
				                    'naturalidade_cidade'   => array(
				                        'value'             => $conjuge->pessoa->naturalidade_cidade,
				                        'type'              => 'text',
				                        'input_bind'        => 'naturalidade_cidade',
				                    ),*/
				                )
			            	);
							
							/*Dados Profissionais do conjuge está OK*/
							if( $conjuge->pessoa->getDadosProfissionais() != NULL )
							{	
								$dataAdmissaoConjuge 	= NULL;

								$dPConj  				= $conjuge->pessoa->getDadosProfissionais();
								$contatoDadosP			= Contato::model()->findByPk($dPConj->Contato_id);

								if( $dPConj->data_admissao != NULL )
								{
									$dataAdmissaoConjuge = $util->bd_date_to_view($dPConj->data_admissao);
								}
								
								$cht 					= ContatoHasTelefone::model()->find('Contato_id = ' . $contatoDadosP->id);

								if( $cht != NULL )
								{
									$telefone 			= Telefone::model()->findByPk($cht->Telefone_id);

									if( $telefone != NULL )
									{
										$arrReturn['formConfig'][] 	= array(
											'fields' 				=> array(
												'dp_con_tel_num'	=> array(
							                        'value'     	=> $telefone->numero,
							                        'type'      	=> 'text',
							                        'input_bind'	=> 'dp_conj_telefone_numero',
						                		),
						                		'dp_con_tel_ramal'	=> array(
							                        'value'     	=> $telefone->ramal,
							                        'type'      	=> 'text',
							                        'input_bind'	=> 'dp_conj_telefone_ramal',
						                		),
						                		'dp_con_tel_tipo'	=> array(
							                        'value'     	=> $telefone->Tipo_Telefone_id,
							                        'type'      	=> 'select',
							                        'input_bind'	=> 'TelefoneProfissConjuge_Tipo_Telefone_id',
						                		),
											)
										);
									}

								}


								$arrReturn['msgConfig']['pnotify'][] 	= array(
						            'titulo'    => 'Ação realizada com sucesso!',
						            'texto'     => 'O Cônjuge já possui dados profissionais cadastrados!',
						            'tipo'      => 'success',
				        		);

								$arrReturn['formConfig'][] 		= array(
									'fields'                	=> array(
										'Cad_conj_nome_da_mae'  => array(
					                        'value'             => $conjugeCad->nome_da_mae,
					                        'type'              => 'text',
					                        'input_bind'        => 'Cadastro_conjuge_nome_da_mae',
					                    ),
									)								
								);

								$arrReturn['formConfig'][] = array(

					                'fields'                => array(
					                	'ControllerAction'	=> array(
						                        'value'     => 'join',
						                        'type'      => 'text',
						                        'input_bind'=> 'ControllerConjugeAction',
						                ),
					                    'dp_conjuge_cnpj_cpf'   => array(
					                        'value'             => $dPConj->cnpj_cpf,
					                        'type'              => 'text',
					                        'input_bind'        => 'dp_conjuge_cnpj_cpf',
					                    ),
					                    'dp_con_classe_profis'	=> array(
					                        'value'             => $dPConj->Classe_Profissional_id,
					                        'type'              => 'select',
					                        'input_bind'        => 'DadosProfissionaisConjuge_Classe_Profissional_id',
					                    ),
					                    'dp_conjuge_empresa'   	=> array(
					                        'value'             => $dPConj->empresa,
					                        'type'              => 'text',
					                        'input_bind'        => 'dp_conj_empresa',
					                    ),
					                    'dp_con_ocupacao_id'	=> array(
					                        'value'             => $dPConj->Tipo_Ocupacao_id,
					                        'type'              => 'select',
					                        'input_bind'        => 'DadosProfissionaisConjuge_Tipo_Ocupacao_id',
					                    ),
					                    'dp_con_data_emissao'	=> array(
					                        'value'             => $dataAdmissaoConjuge,
					                        'type'              => 'text',
					                        'input_bind'        => 'dp_con_data_admissao',
					                    ),
					                    'dp_con_profissao'		=> array(
					                        'value'             => $dPConj->profissao,
					                        'type'              => 'select',
					                        'input_bind'        => 'DadosProfissionaisConjuge_profissao',
					                    ),
					                    'dp_con_renda_liq'		=> array(
					                        'value'             => number_format($dPConj->renda_liquida,2,',','.'),
					                        'type'              => 'text',
					                        'input_bind'        => 'dp_conj_renda_liquida',
					                    ),
					                    'dp_con_mes_ano_renda'	=> array(
					                        'value'             => $dPConj->mes_ano_renda,
					                        'type'              => 'text',
					                        'input_bind'        => 'dp_conj_mes_ano_renda',
					                    ),
					                    'dp_con_orgao-pg'		=> array(
					                        'value'             => $dPConj->orgao_pagador,
					                        'type'              => 'select',
					                        'input_bind'        => 'DadosProfissionaisConjuge_orgao_pagador',
					                    ),
					                    'dp_con_n_beneficio'	=> array(
					                        'value'             => $dPConj->numero_do_beneficio,
					                        'type'              => 'text',
					                        'input_bind'        => 'dp_conj_numero_do_beneficio',
					                    ),
					                    'dp_con_tipo_compro'	=> array(
					                        'value'             => $dPConj->tipo_de_comprovante,
					                        'type'              => 'select',
					                        'input_bind'        => 'DadosProfissionaisConjuge_TipoDeComprovante',
					                    ),
					                    'dp_end_cep'			=> array(
					                        'value'             => $dPConj->endereco->cep,
					                        'type'              => 'text',
					                        'input_bind'        => 'cep_dados_profissionais_conjuge',
					                    ),
					                    'dp_end_logradouro'		=> array(
					                        'value'             => $dPConj->endereco->logradouro,
					                        'type'              => 'text',
					                        'input_bind'        => 'logradouro_dados_profissionais_conjuge',
					                    ),
					                    'dp_end_numero'			=> array(
					                        'value'             => $dPConj->endereco->numero,
					                        'type'              => 'text',
					                        'input_bind'        => 'numero_endereco_dados_profissionais_conjuge',
					                    ),
					                    'dp_end_cidade'			=> array(
					                        'value'             => $dPConj->endereco->cidade,
					                        'type'              => 'text',
					                        'input_bind'        => 'cidade_dados_profissionais_conjuge',
					                    ),
					                    'dp_end_bairro'			=> array(
					                        'value'             => $dPConj->endereco->bairro,
					                        'type'              => 'text',
					                        'input_bind'        => 'bairro_dados_profissionais_conjuge',
					                    ),
					                    'dp_con_end_uf'			=> array(
					                        'value'             => $dPConj->endereco->uf,
					                        'type'              => 'select',
					                        'input_bind'        => 'EnderecoProfissConjuge_uf',
					                    ),
					                    'dp_end_complemento'	=> array(
					                        'value'             => $dPConj->endereco->complemento,
					                        'type'              => 'text',
					                        'input_bind'        => 'dp_conj_endereco_complemento',
					                    ),
					                )
			            		);
							}
							else
							{
								$arrReturn['formConfig'][] 	= array(
									'fields' 			 	=> array(
										'ControllerAction'	=> array(
						                        'value'     => 'conjugeNewDp',
						                        'type'      => 'text',
						                        'input_bind'=> 'ControllerConjugeAction',
						                ),
									)
								);


								$arrReturn['msgConfig']['pnotify'][] = array(
						            'titulo'    => 'Erros foram encontrados!',
						            'texto'     => 'O Cônjuge não possui dados profissionais cadastrados!',
						            'tipo'      => 'error',
				        		);
							}
						}
					}
					
					else
					{
						$arrReturn['hasErrors'] 	= false;
						$arrReturn['enableSubmit'] 	= true;

						$arrReturn['msgConfig']['pnotify'][] = array(
				            'titulo'    => 'Prossiga o cadastro do Cônjuge.',
				            'texto'     => 'Informe os dados do Cônjuge',
				            'tipo'      => 'success',
			        	);

						$arrReturn['formConfig'][] = array(
				        	'fields'                    => array(
				                'ControllerAction'      => array(
				                	'value'             => 'new',
				                    'type'              => 'text',
				                    'input_bind'        => 'ControllerConjugeAction',
				            	),
				            )
			            );
					}
				}
			}
		}

		else
		{
			$arrReturn['msgConfig']['pnotify'][] = array(
			    'titulo'    => 'Erros foram encontrados!',
			    'texto'     => 'O CPF do Cliente não foi encontrado.',
				'tipo'      => 'error',
			);

			$arrReturn['formConfig'][] 	= array(
				'fields' 			 	=> array(
					'ControllerAction'	=> array(
						'value'     => 'nothing',
						'type'      => 'text',
						'input_bind'=> 'ControllerConjugeAction',
					),
				)
			);
		}

		return $arrReturn;
	}
}