<?php

/**
 * This is the model class for table "Financeira_has_ItemDoEstoque".
 *
 * The followings are the available columns in table 'Financeira_has_ItemDoEstoque':
 * @property integer $id
 * @property integer $Financeira_id
 * @property integer $ItemDoEstoque_id
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property Financeira $financeira
 * @property ItemDoEstoque $itemDoEstoque
 */
class FinanceiraHasItemDoEstoque extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return FinanceiraHasItemDoEstoque the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Financeira_has_ItemDoEstoque';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Financeira_id, ItemDoEstoque_id', 'required'),
			array('Financeira_id, ItemDoEstoque_id, habilitado', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, Financeira_id, ItemDoEstoque_id, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'financeira' => array(self::BELONGS_TO, 'Financeira', 'Financeira_id'),
			'itemDoEstoque' => array(self::BELONGS_TO, 'ItemDoEstoque', 'ItemDoEstoque_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Financeira_id' => 'Financeira',
			'ItemDoEstoque_id' => 'Item Do Estoque',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Financeira_id',$this->Financeira_id);
		$criteria->compare('ItemDoEstoque_id',$this->ItemDoEstoque_id);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}