<?php

class Parcela extends CActiveRecord{

	public function valorMoraDia(){

		$util 	= new Util;

		return $util->round_up( ( $this->valor/100) * ConfigLN::model()->valorDoParametro("taxa_mora") , 2 );
	}

	public function calcMora(){

		$util 	= new Util;

		if( !$util->checkDiaFinalDeSemana( $this->vencimento ) )
		{
			return $this->valorMoraDia() * $this->diasVencimento();
		}

		else
		{
			if( $this->diasVencimento() > 2 )
			{
				return ( $this->valorMoraDia() * $this->diasVencimento() ) - $this->valorMoraDia() * 2;
			}
			else
			{
				return 0;
			}
		}
	}

	public function hasFichamento()
	{
		$fichamento = Fichamento::model()->find('Parcela = ' . $this->id . ' AND habilitado');

		if($fichamento != NULL)
        {
        	$fichamentoAtivo = FichamentoHasStatusFichamento::model()->find('Fichamento_id = ' . $fichamento->id . ' AND StatusFichamento_id = 2 AND habilitado');

            if($fichamentoAtivo != NULL)
            {
            	return true;
            }
            else
            {
            	return false;
            }
        }
        else
        {
            return false;
        }
	}

	public function diasUteisEmAtraso()
	{
		$util 			= new Util;
		$hoje 			= date('Y-m-d');
		$vencimento 	= $this->vencimento;
		$diasEmAtraso 	= abs( floor( ( strtotime( $vencimento ) - strtotime( $hoje ) ) / ( 60 * 60 * 24 ) ) );

		if( !$util->checkDiaFinalDeSemana( $vencimento ) )
		{
			return $diasEmAtraso;
		}
		else
		{
			return $diasEmAtraso - $util->checkDiaFinalDeSemana( $vencimento );
		}
	}

	public function diasVencimento(){

		$util 			= new Util;
		$hoje 			= date('Y-m-d');
		$vencimento 	= $this->vencimento;
		if($vencimento < $hoje){
                    return abs( floor( ( strtotime( $vencimento ) - strtotime( $hoje ) ) / ( 60 * 60 * 24 ) ) );
                }else{
                    return 0;
                }
	}

	public function atrasada(){

		$hoje = date('Y-m-d');
		$vencimento = $this->vencimento;

		if ( strtotime( $hoje ) > strtotime( $vencimento ) )
			return true;

		return false;

	}

	public function statusPagamento(){

		$hoje = date('Y-m-d');
		$vencimento = $this->vencimento;

		$arrReturn = array();
		$baixa = Baixa::model()->find( 'Parcela_id = ' . $this->id );

		/*Não existe baixa*/
		if ( $baixa == NULL )
		{

			$diffDias = $this->diasVencimento();
			$arrReturn['numDias'] = $diffDias;

			/*Não está vencida, e não está no dia do pagamento*/
			if ( strtotime( $vencimento ) > strtotime( $hoje ) )
			{
				$arrReturn['status'] 		= 'Em dia';
				$arrReturn['span_class'] 	= 'btn label label-success';
				$arrReturn['mensagem'] 		= "A parcela está em dia. Restando " . $arrReturn['numDias'] . " dias para o vencimento.";
			}

			/*Está no dia do pagamento*/
			else if ( strtotime( $vencimento ) == strtotime( $hoje ) )
			{
				$arrReturn['status'] 		= 'Em dia';
				$arrReturn['span_class'] 	= 'btn label label-warning';
				$arrReturn['mensagem'] 		= 'Estamos no dia do vencimento da parcela';
			}

			/*Está vencido*/
			else
			{
				$arrReturn['status'] 		= 'Atrasado';
				$arrReturn['span_class'] 	= 'btn label label-danger';
				$arrReturn['mensagem'] 		= "Atrasada há " . $arrReturn['numDias']  . " dia(s)";
			}
		}

		/*Existe baixa*/
		else
		{
			$diasPagamento	= floor( ( strtotime( $this->vencimento ) - strtotime( $baixa->data_da_ocorrencia ) ) / ( 60 * 60 * 24 ) );

			$arrReturn['span_class'] 		= 'btn label label-success';
			$arrReturn['status'] 			= 'Paga em dia';

			if ( $diasPagamento < 0 )
			{
				$arrReturn['status'] 		= 'Paga em atraso';
				$arrReturn['mensagem'] 		= "A parcela foi paga " . abs($diasPagamento)  . " dia(s) após o vencimento. ";
			}

			else if( $diasPagamento == 0 )
			{
				$arrReturn['mensagem'] 		= "A parcela foi paga no dia do vencimento";
			}

			else
			{
				$arrReturn['mensagem'] 		= "A parcela foi paga " . abs($diasPagamento) . " dia(s) antes do vencimento.";
			}
		}

		return $arrReturn;
	}

	public function getBaixas(){

		return Baixa::model()->findAll( 'Parcela_id = ' . $this->id );
	}

	public function baixar( $valor ){

		$this->valor_atual = $valor;
		$this->update();
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Parcela';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules(){
		return array(
			array('seq, valor, valor_atual', 'required'),
			array('seq, Titulo_id, habilitado', 'numerical', 'integerOnly'=>true),
			array('valor, valor_atual', 'numerical'),
			array('id, seq, vencimento, habilitado, valor, valor_texto, valor_atual, Titulo_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations(){

		return array(
			'titulo'        		=> array(self::BELONGS_TO  	, 'Titulo'          	, 'Titulo_id'     ),
			'rangesGerados' 		=> array(self::HAS_ONE     	, 'RangesGerados'   	, 'Parcela_id'    ),
		);

	}

	public function attributeLabels(){

		return array(
			'id' 			=> 'ID',
			'seq' 			=> 'Seq',
			'vencimento' 	=> 'Vencimento',
			'valor' 		=> 'Valor',
			'valor_texto' 	=> 'Valor Txt',
			'valor_atual' 	=> 'Valor Atual',
			'Titulo_id' 	=> 'Titulo',
			'habilitado' 	=> 'Habilitado',
		);

	}

	public function search(){

		$criteria = new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('seq',$this->seq);
		$criteria->compare('vencimento',$this->vencimento,true);
		$criteria->compare('valor',$this->valor);
		$criteria->compare('valor_texto',$this->valor_texto);
		$criteria->compare('valor_atual',$this->valor_atual);
		$criteria->compare('Titulo_id',$this->Titulo_id);
		$criteria->compare('habilitado',$this->habilitado, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Parcela the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getVencimento($seq, $proposta){

		$util = new Util;

		$data_primeira_parcela = $util->view_date_to_bd($proposta->getDataPrimeiraParcela());

		$numero_de_dias = $seq * 30;

		return date('Y-m-d', strtotime($data_primeira_parcela. ' + ' .$numero_de_dias. ' days'));
	}
}
