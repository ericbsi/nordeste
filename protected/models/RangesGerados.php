<?php

/**
 * This is the model class for table "Ranges_gerados".
 *
 * The followings are the available columns in table 'Ranges_gerados':
 * @property integer $id
 * @property integer $numero
 * @property string $data_geracao
 * @property integer $Range_id
 * @property integer $Parcela_id
 *
 * The followings are the available model relations:
 * @property Range $range
 * @property Parcela $parcela
 */
class RangesGerados extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Ranges_gerados';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Range_id, Parcela_id, label', 'required'),
			array('numero, Range_id, Parcela_id, habilitado', 'numerical', 'integerOnly'=>true),
			array('data_geracao', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, label, numero, data_geracao, Range_id, Parcela_id, data_cadastro, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'range' => array(self::BELONGS_TO, 'Range', 'Range_id'),
			'parcela' => array(self::BELONGS_TO, 'Parcela', 'Parcela_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'numero' => 'Numero',
			'data_geracao' => 'Data Geracao',
			'Range_id' => 'Range',
			'Parcela_id' => 'Parcela',
			'label' => 'Label',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('numero',$this->numero);
		$criteria->compare('data_geracao',$this->data_geracao,true);
		$criteria->compare('Range_id',$this->Range_id);
		$criteria->compare('Parcela_id',$this->Parcela_id);
		$criteria->compare('label',$this->label);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RangesGerados the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
