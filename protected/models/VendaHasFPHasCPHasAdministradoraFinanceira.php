<?php

/**
 * This is the model class for table "Venda_has_FP_has_CP_has_AdministradoraFinanceira".
 *
 * The followings are the available columns in table 'Venda_has_FP_has_CP_has_AdministradoraFinanceira':
 * @property integer $Venda_has_FP_has_CP_id
 * @property integer $AdministradoraFinanceira_id
 * @property integer $id
 *
 * The followings are the available model relations:
 * @property VendaHasFormaDePagamentoHasCondicaoDePagamento $vendaHasFPHasCP
 * @property AdministradoraFinanceira $administradoraFinanceira
 */
class VendaHasFPHasCPHasAdministradoraFinanceira extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Venda_has_FP_has_CP_has_AdministradoraFinanceira';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Venda_has_FP_has_CP_id, AdministradoraFinanceira_id', 'required'),
			array('Venda_has_FP_has_CP_id, AdministradoraFinanceira_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Venda_has_FP_has_CP_id, AdministradoraFinanceira_id, id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'vendaHasFPHasCP' => array(self::BELONGS_TO, 'VendaHasFormaDePagamentoHasCondicaoDePagamento', 'Venda_has_FP_has_CP_id'),
			'administradoraFinanceira' => array(self::BELONGS_TO, 'AdministradoraFinanceira', 'AdministradoraFinanceira_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Venda_has_FP_has_CP_id' => 'Venda Has Fp Has Cp',
			'AdministradoraFinanceira_id' => 'Administradora Financeira',
			'id' => 'ID',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Venda_has_FP_has_CP_id',$this->Venda_has_FP_has_CP_id);
		$criteria->compare('AdministradoraFinanceira_id',$this->AdministradoraFinanceira_id);
		$criteria->compare('id',$this->id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VendaHasFPHasCPHasAdministradoraFinanceira the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
