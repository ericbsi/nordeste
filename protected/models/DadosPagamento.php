<?php

class DadosPagamento extends CActiveRecord
{

   /**
    * Returns the static model of the specified AR class.
    * @param string $className active record class name.
    * @return DadosPagamento the static model class
    */
   public static function model($className = __CLASS__)
   {
      return parent::model($className);
   }

    public function anexarComprovante($DadosPagamentoId, $ComprovanteFile, $ComprovanteFileDescricao, $funcao = 1)
    {   
        $s3 = new S3Client;

        if($funcao == 1)
        {
          //return Anexo::model()->novo($ComprovanteFile, 'DadosPagamento', $DadosPagamentoId, $ComprovanteFileDescricao);
          return $s3->put($ComprovanteFile, 'DadosPagamento', $DadosPagamentoId, $ComprovanteFileDescricao);
        }
        else
        {
          return $s3->put($ComprovanteFile, 'DadosPagamento', $DadosPagamentoId, $ComprovanteFileDescricao);
          //return Anexo::model()->anexar($ComprovanteFile, 'DadosPagamento', $DadosPagamentoId, $ComprovanteFileDescricao);
        }
    }

   /**
    * @return string the associated database table name
    */
   public function tableName()
   {
      return 'DadosPagamento';
   }

   /**
    * @return array validation rules for model attributes.
    */
   public function rules()
   {
      // NOTE: you should only define rules for those attributes that
      // will receive user inputs.
      return array(
          array('Dados_Bancarios_Debito, Dados_Bancarios_Credito, data_cadastro', 'required'),
          array('Dados_Bancarios_Debito, Dados_Bancarios_Credito, habilitado', 'numerical', 'integerOnly' => true),
          array('valorPago', 'numerical'),
          array('comprovante', 'length', 'max' => 200),
          array('dataCompensacao', 'safe'),
          // The following rule is used by search().
          // Please remove those attributes that should not be searched.
          array('id, Dados_Bancarios_Debito, Dados_Bancarios_Credito, dataCompensacao, valorPago, comprovante, habilitado, data_cadastro', 'safe', 'on' => 'search'),
      );
   }

   /**
    * @return array relational rules.
    */
   public function relations()
   {
      // NOTE: you may need to adjust the relation name and the related
      // class name for the relations automatically generated below.
      return array(
          'dadosBancariosDebito' => array(self::BELONGS_TO, 'DadosBancarios', 'Dados_Bancarios_Debito'),
          'dadosBancariosCredito' => array(self::BELONGS_TO, 'DadosBancarios', 'Dados_Bancarios_Credito'),
      );
   }

   /**
    * @return array customized attribute labels (name=>label)
    */
   public function attributeLabels()
   {
      return array(
          'id' => 'ID',
          'Dados_Bancarios_Debito' => 'Dados Bancarios Debito',
          'Dados_Bancarios_Credito' => 'Dados Bancarios Credito',
          'dataCompensacao' => 'Data Compensacao',
          'valorPago' => 'Valor Pago',
          'comprovante' => 'Comprovante',
          'habilitado' => 'Habilitado',
          'data_cadastro' => 'Data Cadastro',
      );
   }

   /**
    * Retrieves a list of models based on the current search/filter conditions.
    * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
    */
   public function search()
   {
      // Warning: Please modify the following code to remove attributes that
      // should not be searched.

      $criteria = new CDbCriteria;

      $criteria->compare('id', $this->id);
      $criteria->compare('Dados_Bancarios_Debito', $this->Dados_Bancarios_Debito);
      $criteria->compare('Dados_Bancarios_Credito', $this->Dados_Bancarios_Credito);
      $criteria->compare('dataCompensacao', $this->dataCompensacao, true);
      $criteria->compare('valorPago', $this->valorPago);
      $criteria->compare('comprovante', $this->comprovante, true);
      $criteria->compare('habilitado', $this->habilitado);
      $criteria->compare('data_cadastro', $this->data_cadastro, true);

      return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
      ));
   }

}
