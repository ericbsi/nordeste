<?php

/**
 * This is the model class for table "DePara".
 *
 * The followings are the available columns in table 'DePara':
 * @property integer $id
 * @property string $dataDe
 * @property string $dataAte
 * @property integer $ativo
 * @property string $descricao
 * @property string $nome
 * @property string $data_cadastro
 * @property integer $habilitado
 * @property integer $FilialOrigem_has_SistemaOrigem_id
 *
 * The followings are the available model relations:
 * @property FilialOrigemHasSistemaOrigem $filialOrigemHasSistemaOrigem
 * @property ItensDePara[] $itensDeParas
 */
class DePara extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'DePara';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dataDe, dataAte, descricao, nome, data_cadastro, habilitado, FilialOrigem_has_SistemaOrigem_id', 'required'),
			array('ativo, habilitado, FilialOrigem_has_SistemaOrigem_id', 'numerical', 'integerOnly'=>true),
			array('nome', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, dataDe, dataAte, ativo, descricao, nome, data_cadastro, habilitado, FilialOrigem_has_SistemaOrigem_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'filialOrigemHasSistemaOrigem' => array(self::BELONGS_TO, 'FilialOrigemHasSistemaOrigem', 'FilialOrigem_has_SistemaOrigem_id'),
			'itensDeParas' => array(self::HAS_MANY, 'ItensDePara', 'dePara_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'dataDe' => 'Data De',
			'dataAte' => 'Data Ate',
			'ativo' => 'Ativo',
			'descricao' => 'Descricao',
			'nome' => 'Nome',
			'data_cadastro' => 'Data Cadastro',
			'habilitado' => 'Habilitado',
			'FilialOrigem_has_SistemaOrigem_id' => 'Filial Origem Has Sistema Origem',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('dataDe',$this->dataDe,true);
		$criteria->compare('dataAte',$this->dataAte,true);
		$criteria->compare('ativo',$this->ativo);
		$criteria->compare('descricao',$this->descricao,true);
		$criteria->compare('nome',$this->nome,true);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('FilialOrigem_has_SistemaOrigem_id',$this->FilialOrigem_has_SistemaOrigem_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DePara the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
