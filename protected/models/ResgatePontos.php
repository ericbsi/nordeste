<?php

class ResgatePontos extends CActiveRecord
{

   /**
    * Returns the static model of the specified AR class.
    * @param string $className active record class name.
    * @return ResgatePontos the static model class
    */
   public static function model($className = __CLASS__)
   {
      return parent::model($className);
   }

   /**
    * @return string the associated database table name
    */
   public function tableName()
   {
      return 'ResgatePontos';
   }

   /**
    * @return array validation rules for model attributes.
    */
   public function rules()
   {
      // NOTE: you should only define rules for those attributes that
      // will receive user inputs.
      return array(
          array('Usuario_id', 'required'),
          array('Usuario_id', 'numerical', 'integerOnly' => true),
          array('valor', 'numerical'),
          array('data', 'safe'),
          // The following rule is used by search().
          // Please remove those attributes that should not be searched.
          array('id, valor, data, Usuario_id', 'safe', 'on' => 'search'),
      );
   }

   /**
    * @return array relational rules.
    */
   public function relations()
   {
      // NOTE: you may need to adjust the relation name and the related
      // class name for the relations automatically generated below.
      return array(
          'usuario' => array(self::BELONGS_TO, 'Usuario', 'Usuario_id'),
      );
   }

   public function resgatarPremio($ResgatePontos, $Operacao)
   {

      $arrReturn = [];

      $configuracoes = SigacConfig::model()->findByPk(1);

      if ($configuracoes->auth_fichamento != $Operacao['senha'])
      {

         $arrReturn['msgConfig']['pnotify'][] = array(
             'titulo' => 'Não foi possível concluir o resgate!',
             'texto' => 'Senha de permissão inválida.',
             'tipo' => 'error',
         );
      } else
      {
         $novoResgate = new ResgatePontos;
         $novoResgate->attributes = $ResgatePontos;

         $crediarista = Usuario::model()->findByPk($ResgatePontos['Usuario_id']);

         if ($crediarista->getSaldoPontos() >= $novoResgate->valor)
         {
            if ($novoResgate->save())
            {
               $arrReturn['msgConfig']['pnotify'][] = array(
                   'titulo' => 'Resgate realizado com sucesso!',
                   'texto' => 'O saldo do crediarista foi atualizado.',
                   'tipo' => 'success',
               );
            } else
            {
               $arrReturn['msgConfig']['pnotify'][] = array(
                   'titulo' => 'Não foi possível concluir o resgate!',
                   'texto' => 'Erros foram encontrados. Contacte o suporte.',
                   'tipo' => 'error',
               );
            }
         } else
         {
            $arrReturn['msgConfig']['pnotify'][] = array(
                'titulo' => 'Não foi possível concluir o resgate!',
                'texto' => 'Saldo insuficiente para o resgate.',
                'tipo' => 'error',
            );
         }
      }

      return $arrReturn;
   }

   /**
    * @return array customized attribute labels (name=>label)
    */
   public function attributeLabels()
   {
      return array(
          'id' => 'ID',
          'valor' => 'Valor',
          'data' => 'Data',
          'Usuario_id' => 'Usuario',
      );
   }

   /**
    * Retrieves a list of models based on the current search/filter conditions.
    * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
    */
   public function search()
   {
      // Warning: Please modify the following code to remove attributes that
      // should not be searched.

      $criteria = new CDbCriteria;

      $criteria->compare('id', $this->id);
      $criteria->compare('valor', $this->valor);
      $criteria->compare('data', $this->data, true);
      $criteria->compare('Usuario_id', $this->Usuario_id);

      return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
      ));
   }

}
