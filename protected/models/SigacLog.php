<?php

class SigacLog extends CActiveRecord{

	
	public function tableName(){

		return 'Sigac_Log';
	}

	
	public function rules(){
		
		return array(
			array('entidade_id, habilitado, Usuario_id', 'numerical', 'integerOnly'=>true),
			array('acao, entidade', 'length', 'max'=>100),
			array('ip', 'length', 'max'=>45),
			array('cookie_id, session_id', 'length', 'max'=>200),
			array('data, observacao', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, acao, entidade, entidade_id, data, habilitado, Usuario_id, observacao, ip, cookie_id, session_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations(){
		
		return array(
			'usuario' => array(self::BELONGS_TO, 'Usuario', 'Usuario_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'acao' => 'Acao',
			'entidade' => 'Entidade',
			'entidade_id' => 'Entidade',
			'data' => 'Data',
			'habilitado' => 'Habilitado',
			'Usuario_id' => 'Usuario',
			'observacao' => 'Observacao',
			'ip' => 'Ip',
			'cookie_id' => 'Cookie',
			'session_id' => 'Session',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('acao',$this->acao,true);
		$criteria->compare('entidade',$this->entidade,true);
		$criteria->compare('entidade_id',$this->entidade_id);
		$criteria->compare('data',$this->data,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('Usuario_id',$this->Usuario_id);
		$criteria->compare('observacao',$this->observacao,true);
		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('cookie_id',$this->cookie_id,true);
		$criteria->compare('session_id',$this->session_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SigacLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function saveLog( $acao, $entidade, $entidade_id, $data, $habilitado, $Usuario_id, $observacao, $ip, $cookie_id, $session_id ){

		$this->acao 		= $acao;
		$this->entidade 	= $entidade;
		$this->entidade_id 	= $entidade_id;
		$this->data 		= $data;
		$this->habilitado 	= $habilitado;
		$this->Usuario_id 	= $Usuario_id;
		$this->observacao 	= $observacao;
		$this->ip 			= $ip;
		$this->cookie_id 	= $cookie_id;
		$this->session_id 	= $session_id;		
		$this->save();

		/*

		$sigacLog = new SigacLog;
		$sigacLog->saveLog('Cadastro de usuario','usuario', $model->id, date('Y-m-d H:i:s'),1, Yii::app()->session['usuario']->id, "Usuário ".Yii::app()->session['usuario']->username . " cadastrou o usuário " . $model->username, "127.0.0.1",null, Yii::app()->session->getSessionId() );

		*/

	}
}
