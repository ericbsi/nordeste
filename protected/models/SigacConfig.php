<?php

class SigacConfig extends CActiveRecord
{

	public function getYearsAlive()
	{	
		$yearsArr 	=	array();
		$appConfig  = 	SigacConfig::model()->find('habilitado');
		$years 		= 	explode(' ', $appConfig->anos_ativo);
				           
        foreach($years as $year)
        {
        	if( strlen($year) > 0 )
            {
            	$yearsArr[] = $year;
            }
        }

        return $yearsArr;
	}

	public function tableName()
	{
		return 'Sigac_config';
	}


	public function rules()
	{
		return array(
			array('id, habilitado, ativo_deste, anos_ativo', 'safe', 'on'=>'search'),
		);
	}


	public function relations()
	{
		return array(
		);
	}


	public function attributeLabels()
	{
		return array(
			'id' 			=> 'ID',
			'habilitado'	=> 'Habilitado',
			'ativo_deste'	=> 'Online deste',
			'anos_ativo'	=> 'Anos ativo'
		);
	}


	public function search()
	{
		$criteria = new CDbCriteria;
		$criteria->compare('id',$this->id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
