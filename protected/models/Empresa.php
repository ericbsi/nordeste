<?php

class Empresa extends CActiveRecord {

    private $connection;

    public function listarFichamentos($draw, $start)
    {
      $rows = array();
      $util = new Util;

      $criteria = new CDbCriteria;
      $criteria->with = array('fichamento' => ['alias' => 'fi']);
      $criteria->addInCondition('t.StatusFichamento_id', [2, 5], ' AND ');
      $criteria->addInCondition('t.habilitado', [1], ' AND ');
      $criteria->order = "t.StatusFichamento_id DESC, fi.data_cadastro DESC";

      $statusFichamentos = FichamentoHasStatusFichamento::model()->findAll($criteria);


      $criteria->offset = $start;
      $criteria->limit = 10;

      foreach (FichamentoHasStatusFichamento::model()->findAll($criteria) as $fichamento)
      {

         $disabledBtn = ' disabled="disabled" ';

         if ($fichamento->statusFichamento->id == 5)
         {
            $disabledBtn = '';
         }

         $row = [
             'cliente' => strtoupper($fichamento->fichamento->parcela->titulo->proposta->analiseDeCredito->cliente->pessoa->nome),
             'cpf' => $fichamento->fichamento->parcela->titulo->proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero,
             'valor' => 'R$ ' . number_format($fichamento->fichamento->parcela->valor, 2, ',', '.'),
             'seq_parcela' => $fichamento->fichamento->parcela->seq . '°',
             'data_fichamento' => $util->bd_date_to_view(substr($fichamento->fichamento->data_cadastro, 0, 10)),
             'status_fichamento' => '<span class="' . $fichamento->statusFichamento->cssClass . '">' . $fichamento->statusFichamento->descricao . '</span>',
             'btn_remove' => '<button ' . $disabledBtn . ' data-fichamento="' . $fichamento->id . '" class="btn btn-xs btn-danger btn-rmv-fich"><i class="fa  fa-ban"></i></button>',
         ];

         $rows[] = $row;
      }

      return array(
          "draw" => $draw,
          "recordsFiltered" => count($statusFichamentos),
          "recordsTotal" => count($rows),
          "data" => $rows,
      );
    }

    public function removerFichamento($FichamentoAtual, $NovoFichamento, $Operacao)
    {
      $arrReturn = ['hasErrors' => 0];

      $configuracoes = SigacConfig::model()->findByPk(1);

      if ($configuracoes->auth_fichamento != $Operacao['senha'])
      {
         $arrReturn['hasErrors'] = 1;

         $arrReturn['msgConfig']['pnotify'][] = array(
             'titulo' => 'Não foi possível concluir o fichamento!',
             'texto' => 'Senha de permissão inválida.',
             'tipo' => 'error',
         );
      }
      else
      {
         $FichamentoAberto = FichamentoHasStatusFichamento::model()->findByPk($FichamentoAtual['id']);

         if ($FichamentoAberto != NULL)
         {
            $FichamentoAberto->habilitado = 0;
            $FichamentoAberto->fichamento->habilitado = 0;

            if ($FichamentoAberto->update() && $FichamentoAberto->fichamento->update())
            {
               $novoFichamento = FichamentoHasStatusFichamento::model()->novo($FichamentoAberto->fichamento, $NovoFichamento, 3);

               if ($novoFichamento != NULL)
               {
                  $arrReturn['msgConfig']['pnotify'][] = array(
                      'titulo' => 'Ação realizada com sucesso.',
                      'texto' => 'Fichamento removido com sucesso.',
                      'tipo' => 'success',
                  );
               } else
               {
                  $arrReturn['msgConfig']['pnotify'][] = array(
                      'titulo' => 'Não foi possível remover o fichamento!',
                      'texto' => 'Entre em contato com o suporte.',
                      'tipo' => 'error',
                  );
               }
            } else
            {
               $arrReturn['msgConfig']['pnotify'][] = array(
                   'titulo' => 'Não foi possível remover o fichamento!',
                   'texto' => 'Entre em contato com o suporte.',
                   'tipo' => 'error',
               );
            }
         } else
         {
            $arrReturn['msgConfig']['pnotify'][] = array(
                'titulo' => 'Não foi possível remover o fichamento!',
                'texto' => 'Entre em contato com o suporte.',
                'tipo' => 'error',
            );
         }
      }

      return $arrReturn;
    }

    public function cancelamentosPagos($start, $draw, $filtroGrupoFilial)
    {
      $util = new Util;
      $totalFinanciado = 0;
      $totalRepasse = 0;
      $seguroColor = '';

      $rows       = [];
      $resultado  = [];

      $Query = "SELECT P.id AS `PROPOSTAID`, SC.id AS `SOLICITACAOID`, IB.id AS `ITEMBORDEROID` ";
      $Query .= " FROM Solicitacao_de_Cancelamento AS SC ";
      $Query .= " INNER JOIN Proposta       AS P  ON SC.Proposta_id = P.id ";
      $Query .= " INNER JOIN ItemDoBordero  AS IB ON P.id = IB.Proposta_id ";

      $Query .= " WHERE SC.aceite ";
      $Query .= " ORDER BY SC.data_solicitacao DESC; ";

      $resultado = Yii::app()->db->createCommand($Query)->queryAll();

      $Query = "SELECT P.id AS `PROPOSTAID`, SC.id AS `SOLICITACAOID`, IB.id AS `ITEMBORDEROID` ";
      $Query .= " FROM Solicitacao_de_Cancelamento AS SC ";
      $Query .= " INNER JOIN Proposta       AS P  ON SC.Proposta_id = P.id ";
      $Query .= " INNER JOIN ItemDoBordero  AS IB ON P.id = IB.Proposta_id ";

      if( ((int)$filtroGrupoFilial) > 0)
      {
         $Query .= " INNER JOIN Analise_de_Credito AS AC ON AC.id =  P.Analise_de_Credito_id                                                       ";
         $Query .= " INNER JOIN Filial             AS F  ON  F.id = AC.Filial_id                                                                   ";
         $Query .= " INNER JOIN NucleoFiliais      AS NF ON NF.id =  F.NucleoFiliais_id      AND NF.GrupoFiliais_id  = " . $filtroGrupoFilial . "  ";
      }

      $Query .= " WHERE SC.aceite ";
      $Query .= " ORDER BY SC.data_solicitacao DESC; ";

      if (count($resultado) > 0)
      {
         foreach (Yii::app()->db->createCommand($Query)->queryAll() as $r)
         {
            $proposta = Proposta::model()->findByPk($r['PROPOSTAID']);
            $itemBordero = ItemDoBordero::model()->findByPk($r['ITEMBORDEROID']);

            if ($proposta->segurada)
            {
               $seguroColor = ' style="color:#3D9400" ';
            }

            if ($proposta->titulos_gerados)
            {
               $row = array(
                   'codigo' => '<span' . $seguroColor . '>' . $proposta->codigo . '</span>',
                   'data' => $util->bd_date_to_view(substr($proposta->data_cadastro, 0, 10)),
                   'cliente' => strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome) . ' - ' . $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero,
                   'bordero' => $itemBordero->bordero->processo->codigo . ' - ' . '<span style="font-weight: bold;">' . $util->bd_date_to_view(substr($itemBordero->bordero->processo->dataCriacao, 0, 10)) . '</span>',
                   'repasse' => '<span style="color:#3D9400!important;">' . number_format($proposta->getValorFinanciado(), 2, ',', '.') . '</span><span style="font-weight: bold;font-size:13px;"> -> </span><span style="color:#DA5251">' . number_format($proposta->valorRepasse(), 2, ',', '.') . '</span>',
                   'parceiro' => strtoupper($proposta->analiseDeCredito->filial->getConcat())
               );

               $rows[] = $row;

               $seguroColor = '';
            }

            $totalFinanciado += $proposta->getValorFinanciado();
            $totalRepasse += $proposta->valorRepasse();
         }

         return[
             "draw" => $draw,
             "recordsFiltered" => count($resultado),
             "recordsTotal" => count($rows),
             "data" => $rows,
             "customReturn" => [
                 "total" => '<span style="color:#3D9400!important;">' . number_format($totalFinanciado, 2, ',', '.') . '</span><span style="font-weight: bold;font-size:15px;"> -> </span><span style="color:#d9534f!important;">' . number_format($totalRepasse, 2, ',', '.') . '</span>',
             ]
         ];
      } else
      {
         return null;
      }
    }

    public function listarCrediaristasGrupo( $filiais )
    {
        $criteria                   = new CDbCriteria;

        $arrReturn                  = [];

        if ( empty( $filiais ) )
        {
            foreach ( Yii::app()->session['usuario']->adminParceiros as $f )
            {
                $filiais[]          = $f->parceiro->id;
            }
        }

        $criteria->addInCondition('t.Filial_id', $filiais,  'AND' );
        $criteria->addInCondition('t.habilitado', array(1), 'AND' );

        foreach( FilialHasUsuario::model()->findAll($criteria) as $fhu )
        {
            $arrReturn[]            = [
                'label'             => $fhu->usuario->nome_utilizador,
                'value'             => $fhu->usuario->id,
                'criteria'          => $criteria
            ];
        }

        return $arrReturn;
    }

    public function propostasMes($confDate, $status, $titulosGerados, $habilitado, $parceiro, $analista){

        $status         = join(',', $status);
        $titulosGerados = join(',', $titulosGerados);

        $Qry            = " SELECT P.id as PropostaId, P.qtd_parcelas as QuantidadeDeParcelasMaxima ";
        $Qry            .= " FROM Proposta AS P ";

        if (( is_array($parceiro) && count($parceiro) == 1 && $parceiro[0] != 0 ) || count($parceiro) > 1) {
            $parceiro = join(',', $parceiro);
            $Qry .= " INNER JOIN Analise_de_Credito AS AC ON P.Analise_de_Credito_id = AC.id AND AC.Filial_id IN( $parceiro ) ";
        }

        if ($analista != 0) {
            $Qry .= " INNER JOIN Analista_has_Proposta_has_Status_Proposta AP ON P.id = AP.Proposta_id AND AP.Status_Proposta_id = 2 AND AP.habilitado AND AP.Analista_id = $analista ";
        }

        $Qry .= " WHERE YEAR(P.data_cadastro) = " . $confDate['ano'] . " AND MONTH(P.data_cadastro) = " . $confDate['mes'];
        $Qry .= " AND P.habilitado = $habilitado AND P.Status_Proposta_id IN(2,7) AND P.titulos_gerados IN($titulosGerados) ";
        $Qry .= " ORDER BY QuantidadeDeParcelasMaxima DESC ";

        return Yii::app()->db->createCommand($Qry)->queryAll();
    }

    public function filiaisProducaoSeguro( $dataDe, $dataAte, $filiais, $draw, $start )
    {
        $totalSolicitado            = 0;
        $totalSeguro                = 0;
        $totalFinanciado            = 0;

        $util                       = new Util;
        $criteriaAll                = new CDbCriteria;
        $criteriaFilter             = new CDbCriteria;
        $rows                       = array();

        /*Os dois critérios devem filtrar apenas propostas seguradas*/
        $criteriaAll->addInCondition('t.segurada',              array(1),'OR');
        $criteriaFilter->addInCondition('t.segurada',           array(1),'OR');

        $criteriaAll->addInCondition('t.Status_Proposta_id',    array(2,7),'AND');
        $criteriaFilter->addInCondition('t.Status_Proposta_id', array(2,7),'AND');

        /*Aplica as datas apenas se forem informadas*/
        if ( !empty( $dataDe ) && !empty( $dataAte ) )
        {
            $criteriaAll->addBetweenCondition('t.data_cadastro', $util->view_date_to_bd($dataDe) . ' 00:00:00', $util->view_date_to_bd($dataAte) . ' 23:59:59', 'AND');
            $criteriaFilter->addBetweenCondition('t.data_cadastro', $util->view_date_to_bd($dataDe) . ' 00:00:00', $util->view_date_to_bd($dataAte) . ' 23:59:59', 'AND');
        }

        /*Se não foi aplicado ao filtro alguma filial (ou mais de uma), busca todas geridas pelo Empresa Admin atual*/
        if ( empty( $filiais ) )
        {
            foreach ( Yii::app()->session['usuario']->adminParceiros as $f )
            {
                $filiais[]  = $f->parceiro->id;
            }
        }

        /*Inner join AnaliseDeCredito*/
        $criteriaAll->with      = array('analiseDeCredito' => array(
            'alias' => 'ac'
        ));

        $criteriaFilter->with   = array('analiseDeCredito' => array(
            'alias' => 'ac'
        ));
        /*---------------------------------------------*/

        /*Aplicando o filtro das filiais*/
        $criteriaAll->addInCondition('ac.Filial_id',    $filiais, ' AND ');
        $criteriaFilter->addInCondition('ac.Filial_id', $filiais, ' AND ');

        foreach( Proposta::model()->findAll( $criteriaAll ) as $p )
        {
            $totalSolicitado        += $p->valor-$p->valor_entrada;
            $totalSeguro            += $p->calcularValorDoSeguro();
            $totalFinanciado        += $p->getValorFinanciado();
        }

        /*Critério filter nos possibilita a paginação*/
        $criteriaFilter->offset     = $start;
        $criteriaFilter->limit      = 10;

        foreach( Proposta::model()->findAll( $criteriaFilter ) as $proposta )
        {
            $rows[]                 = [
                'codigo'            => $proposta->codigo,
                'filial'            => strtoupper($proposta->analiseDeCredito->filial->nome_fantasia).'/'.strtoupper($proposta->analiseDeCredito->filial->getConcat()),
                'data'              => $util->bd_date_to_view( substr( $proposta->data_cadastro, 0,10 ) ),
                'crediarista'       => strtoupper($proposta->analiseDeCredito->usuario->nome_utilizador),
                'valor_da_proposta' => number_format($proposta->valor-$proposta->valor_entrada,2,',','.'),
                'valor_do_seguro'   => number_format($proposta->calcularValorDoSeguro(),2,',','.'),
                'valor_financiado'  => number_format($proposta->getValorFinanciado(),2,',','.'),
            ];
        }

        return array(
            'draw'                  => $draw,
            'recordsTotal'          => count($rows),
            'recordsFiltered'       => count(Proposta::model()->findAll( $criteriaAll ) ),
            'data'                  => $rows,
            "customReturn"          => array(
                'totalSolicitado'   => number_format($totalSolicitado,  2, ',', '.'),
                'totalSeguro'       => number_format($totalSeguro,      2, ',', '.'),
                'totalFinanciado'   => number_format($totalFinanciado,  2, ',', '.'),
                'filiais'           => $filiais
            )
        );
    }

    public function getRankingBackofficeIndex($draw) {

        $parceiros = array();
        $rows = array();
        $totalGeralDia = 0;
        $totalGeralMes = 0;

        foreach (Yii::app()->session['usuario']->adminParceiros as $parceiro) {
            $parceiros[] = $parceiro->parceiro->id;
        }

        $parceiros = join(',', $parceiros);

        $Query = " SELECT F.id AS 'FILIAL', TRUNCATE(COALESCE(SUM(P.valor-P.valor_entrada), 0),2) AS 'VALOR' ";
        $Query .= " FROM Filial AS F ";
        $Query .= " LEFT OUTER JOIN( ";
        $Query .= " Analise_de_Credito AS AC ";
        $Query .= " JOIN Proposta AS P ON (P.Analise_de_Credito_id = AC.id AND P.habilitado and P.Status_Proposta_id IN(2,7) and P.titulos_gerados) ";
        $Query .= " ) ON (F.id = AC.Filial_id AND AC.data_cadastro BETWEEN '" . date('Y-m-d') . " 00:00:00' AND '" . date('Y-m-d') . " 23:59:59') ";
        $Query .= " WHERE F.id IN($parceiros) ";
        $Query .= " GROUP BY F.id  ";
        $Query .= " ORDER BY TRUNCATE(COALESCE(SUM(P.valor), 0),2) DESC ";

        $resultQuery = Yii::app()->db->createCommand($Query)->queryAll();

        foreach ($resultQuery as $rQ) {
            $parceiro = Filial::model()->findByPk($rQ['FILIAL']);

            $producaoMes = 0;

            $parceiroPM = $this->producaoResultRows(array($parceiro->id), array(2, 7), date('01/m/y'), date('d/m/y'), TRUE);

            foreach ($parceiroPM as $p) {
                $proposta = Proposta::model()->findByPk($p['id']);

                if ($proposta->habilitado && ( $proposta->Status_Proposta_id == 2 || $proposta->Status_Proposta_id == 7 ) && $proposta->titulos_gerados) {
                    $producaoMes += ($proposta->valor - $proposta->valor_entrada);
                }
            }

            $row = array(
                'filial' => strtoupper($parceiro->getConcat()),
                'totalDia' => number_format($rQ['VALOR'], 2, ',', '.'),
                'totalMes' => number_format($producaoMes, 2, ',', '.')
            );

            $totalGeralDia += $rQ['VALOR'];
            $totalGeralMes += $producaoMes;

            $rows[] = $row;
        }

        return array(
            'draw' => $draw,
            'recordsTotal' => count($rows),
            'recordsFiltered' => count($rows),
            'data' => $rows,
            "customReturn" => array(
                'totalGeralDia' => number_format($totalGeralDia, 2, ',', '.'),
                'totalGeralMes' => number_format($totalGeralMes, 2, ',', '.')
            )
        );
    }

    public function producaoResultRows($filiais, $status, $dataDe, $dataAte, $aprovadas = FALSE) {

        $util = new Util;
        $filiais = join(',', $filiais);
        $status = join(',', $status);

        $Qry = " SELECT P.id ";
        $Qry .= " FROM Proposta as P ";
        $Qry .= " INNER JOIN Analise_de_Credito as AC ON AC.id = P.Analise_de_Credito_id AND P.Status_Proposta_id IN($status) ";
        $Qry .= ' AND P.data_cadastro BETWEEN "' . $util->view_date_to_bd($dataDe) . ' 00:00:00" AND "' . $util->view_date_to_bd($dataAte) . ' 23:59:59" ';

        if ($aprovadas) {
            $Qry .= " AND P.habilitado AND P.titulos_gerados ";
        }

        $Qry .= " WHERE AC.Filial_id IN($filiais) ";

        $resultQuery = Yii::app()->db->createCommand($Qry)->queryAll();

        return $resultQuery;
    }

    public function producaoAprovadasDocNaoRecebido($filiais, $dataDe, $dataAte) {

        $util = new Util;
        $filiais = join(',', $filiais);

        $Qry = " SELECT P.id ";
        $Qry .= " FROM Proposta as P ";
        $Qry .= " INNER JOIN Analise_de_Credito as AC ON AC.id = P.Analise_de_Credito_id AND P.Status_Proposta_id IN(2) AND AC.Filial_id IN($filiais) ";
        $Qry .= " LEFT  JOIN ItemDoBordero as IB ON P.id = IB.Proposta_id AND IB.Proposta_id IS NULL ";
        $Qry .= ' AND P.data_cadastro BETWEEN "' . $util->view_date_to_bd($dataDe) . ' 00:00:00" AND "' . $util->view_date_to_bd($dataAte) . ' 23:59:59" ';
        $Qry .= " AND P.habilitado AND P.titulos_gerados ";

        $resultQuery = Yii::app()->db->createCommand($Qry)->queryAll();

        return $resultQuery;
    }

    public function producaoResultRowsPgtoAprovados($filiais, $status, $dataDe, $dataAte, $aprovadas = FALSE) {
        $util = new Util;
        $filiais = join(',', $filiais);
        $status = join(',', $status);

        $Qry = " SELECT P.id ";
        $Qry .= " FROM Proposta as P ";
        $Qry .= " INNER JOIN Analise_de_Credito as AC ON AC.id = P.Analise_de_Credito_id AND P.Status_Proposta_id IN($status) ";
        $Qry .= " INNER JOIN ItemDoBordero as IB ON IB.Proposta_id = P.id ";
        $Qry .= ' AND P.data_cadastro BETWEEN "' . $util->view_date_to_bd($dataDe) . ' 00:00:00" AND "' . $util->view_date_to_bd($dataAte) . ' 23:59:59" ';

        if ($aprovadas) {
            $Qry .= " AND P.habilitado AND P.titulos_gerados ";
        }

        $Qry .= " WHERE AC.Filial_id IN($filiais) ";

        $resultQuery = Yii::app()->db->createCommand($Qry)->queryAll();

        return $resultQuery;
    }

    public function producaoResult($filiais, $status, $dataDe, $dataAte, $aprovadas = FALSE) {

        $util = new Util;
        $filiais = join(',', $filiais);
        $status = join(',', $status);

        $Qry = " SELECT TRUNCATE(SUM(P.valor-P.valor_entrada), 2) as 'Valor' ";
        $Qry .= " FROM Proposta as P ";
        $Qry .= " INNER JOIN Analise_de_Credito as AC ON AC.id = P.Analise_de_Credito_id AND P.Status_Proposta_id IN($status) ";
        $Qry .= ' AND P.data_cadastro BETWEEN "' . $util->view_date_to_bd($dataDe) . ' 00:00:00" AND "' . $util->view_date_to_bd($dataAte) . ' 23:59:59" ';

        if ($aprovadas) {
            $Qry .= " AND P.habilitado AND P.titulos_gerados ";
        }

        $Qry .= " WHERE AC.Filial_id IN($filiais) ";

        $resultQuery = Yii::app()->db->createCommand($Qry)->queryAll();

        return $resultQuery;
    }

    public function producaoParceiros($draw, $parceiros, $dataDe, $dataAte) {

        $rows = array();
        $porcentagemTotalAprovado = 0;
        $porcentagemTotalNegado = 0;

        $htmlInputsParceiros = "";
        $htmlInputDataDe = '<input type="hidden" name="dataDe"  value="' . $dataDe . '">';
        $htmlInputDataAte = '<input type="hidden" name="dataAte" value="' . $dataAte . '">';

        $htmlInputStaAprovados = '<input type="hidden" name="status" value="2">';
        $htmlInputStaRecusados = '<input type="hidden" name="status" value="3">';
        $htmlInputStaCancelados = '<input type="hidden" name="status" value="8">';

        if (empty($parceiros)) {
            $parceiros = array();

            foreach (Yii::app()->session['usuario']->adminParceiros as $f) {
                $parceiros[] = $f->parceiro->id;
                $htmlInputsParceiros .= '<input type="hidden" name="Parceiros[]" value="' . $f->parceiro->id . '">';
            }
        } else {
            for ($i = 0; $i < sizeof($parceiros); $i++) {
                $htmlInputsParceiros .= '<input type="hidden" name="Parceiros[]" value="' . $parceiros[$i] . '">';
            }
        }

        $rAprovadas = $this->producaoResult($parceiros, array(2, 7), $dataDe, $dataAte, TRUE);
        $rNegadas = $this->producaoResult($parceiros, array(3), $dataDe, $dataAte);
        $rCanceladas = $this->producaoResult($parceiros, array(8), $dataDe, $dataAte);

        $totalAnalisado = $rAprovadas[0]['Valor'] + $rNegadas[0]['Valor'];

        if ($rAprovadas[0]['Valor'] > 0) {
            $porcentagemTotalAprovado = ($rAprovadas[0]['Valor'] * 100) / $totalAnalisado;
        }

        $htmlInputPercentApro = '<input type="hidden" name="porcentagemTotalAprovado" value="' . number_format($porcentagemTotalAprovado, 2, ',', '.') . ' %' . '">';

        if ($rNegadas[0]['Valor'] > 0) {
            $porcentagemTotalNegado = ( $rNegadas[0]['Valor'] * 100 ) / $totalAnalisado;
        }

        $htmlInputPercentNega = '<input type="hidden" name="porcentagemTotalNegado" value="' . number_format($porcentagemTotalNegado, 2, ',', '.') . ' %' . '">';

        $rows[] = array(
            'btn_status' => '<button style="width:100%" type="button" class="btn btn-sm btn-success">Aprovadas</button>',
            'valor' => "R$ " . number_format($rAprovadas[0]['Valor'], 2, ',', '.'),
            'porcentagem' => '<div class="progress"><div class="progress-bar progress-bar-success" style="width: ' . number_format($porcentagemTotalAprovado, 2) . '%">' . number_format($porcentagemTotalAprovado, 2) . '%</div></div>',
            'btn_analitico' => '<form method="POST" action="/administradorDeParceiros/producaoPorStatus/">' . $htmlInputsParceiros . $htmlInputDataDe . $htmlInputDataAte . $htmlInputStaAprovados . $htmlInputPercentApro . '<input type="hidden" name="view" value="producaoAprovadas"><button type="submit" class="btn btn-sm btn-primary">Mais detalhes</i></button></form>',
            'detalhe' => array($this->getDetalheProposta()),
        );

        $rows[] = array(
            'btn_status' => '<button style="width:100%" type="button" class="btn btn-sm btn-danger">Negadas</button>',
            'valor' => "R$ " . number_format($rNegadas[0]['Valor'], 2, ',', '.'),
            'porcentagem' => '<div class="progress"><div class="progress-bar progress-bar-danger" style="width: ' . number_format($porcentagemTotalNegado, 2) . '%">' . number_format($porcentagemTotalNegado, 2) . '%</div></div>',
            'btn_analitico' => '<form method="POST" action="/administradorDeParceiros/producaoPorStatus/">' . $htmlInputsParceiros . $htmlInputDataDe . $htmlInputDataAte . $htmlInputStaRecusados . $htmlInputPercentNega . '<input type="hidden" name="view" value="producaoNegadas"><button type="submit" class="btn btn-sm btn-primary">Mais detalhes</i></button></form>',
            'detalhe' => array('<table><tr><td>Teste</td></tr></table>'),
        );

        $rows[] = array(
            'btn_status' => '<button style="width:100%" type="button" class="btn btn-sm btn-warning">Canceladas</button>',
            'valor' => "R$ " . number_format($rCanceladas[0]['Valor'], 2, ',', '.'),
            'porcentagem' => '<div class="progress"><div class="progress-bar progress-bar-danger" style="width:0%">0.00%</div></div>',
            'btn_analitico' => '<form method="POST" action="/administradorDeParceiros/producaoPorStatus/">' . $htmlInputsParceiros . $htmlInputDataDe . $htmlInputDataAte . $htmlInputStaCancelados . '<input type="hidden" name="view" value="producaoCanceladas"><button type="submit" class="btn btn-sm btn-primary">Mais detalhes</i></button></form>',
            'detalhe' => array('<table><tr><td>Teste</td></tr></table>'),
        );

        return array(
            "draw" => $draw,
            "recordsFiltered" => 3,
            "recordsTotal" => 3,
            "data" => $rows,
        );
    }

    public function getDetalheProposta() {
        $tableDetalhe = '';

        $tableDetalhe .= '  <table class="table table-striped table-bordered table-hover table-full-width">';
        $tableDetalhe .= '      <tr>';
        $tableDetalhe .= '          <td>';
        $tableDetalhe .= '              <button style="width:100%" type="button" class="btn btn-sm btn-warning">Aguardando documentação</button>';
        $tableDetalhe .= '          </td>';
        $tableDetalhe .= '          <td>';
        $tableDetalhe .= '              R$ 1.234,56';
        $tableDetalhe .= '          </td>';
        $tableDetalhe .= '          <td>';
        $tableDetalhe .= '              <div class="progress">';
        $tableDetalhe .= '                  <div class="progress-bar progress-bar-danger" style="width: 35%">';
        $tableDetalhe .= '                      35%';
        $tableDetalhe .= '                  </div>';
        $tableDetalhe .= '              </div>';
        $tableDetalhe .= '          </td>';
        $tableDetalhe .= '      </tr>';
        $tableDetalhe .= '  </table>';

        return $tableDetalhe;
    }

    public function producaoGeral($parceiros, $dataDe, $dataAte) {

        $util = new Util;

        $vlrTotal = 0;

        $rAprovadas = $this->producaoResult($parceiros, array(2, 7), $dataDe, $dataAte, TRUE);
        $rNegadas = $this->producaoResult($parceiros, array(3), $dataDe, $dataAte);
        $rCanceladas = $this->producaoResult($parceiros, array(8), $dataDe, $dataAte);

        $vlrTotal += $rAprovadas[0]['Valor'] + $rNegadas[0]['Valor'] + $rCanceladas[0]['Valor'];

        $rFinal[] = [
            'status' => 'Aprovadas',
            'valor' => $rAprovadas[0]['Valor'],
            'porcentagem' => round((($ret[1] / $vlrTotal) * 100), 2),
            'cor' => 'green'
        ];
        $rFinal[] = [
            'status' => 'Negadas',
            'valor' => $rNegadas[0]['Valor'],
            'porcentagem' => round((($rCanceladas[0]['Valor'] / $vlrTotal) * 100), 2),
            'cor' => 'red'
        ];
        $rFinal[] = [
            'status' => 'Canceladas',
            'valor' => $rCanceladas[0]['Valor'],
            'porcentagem' => round((($rCanceladas[0]['Valor'] / $vlrTotal) * 100), 2),
            'cor' => 'black'];

        return $rFinal;
    }

    public function listarRecebimentosDeContratos($draw, $filial, $start, $dataDe, $dataAte)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 300);
        error_reporting(E_ALL ^ E_DEPRECATED);
        ini_set('display_errors', 1);
        error_reporting(E_ALL);

        $util = new Util;

        $criteria = new CDbCriteria;
        $totalRepasseApp = 0;

        $criteria->addInCondition('t.habilitado', [1], ' AND ');

        if (!empty($filial))
        {
           $criteria->addInCondition('t.destinatario', $filial, ' AND ');
        }

        if (!empty($dataDe) && !empty($dataAte))
        {
           $criteria->addBetweenCondition('t.dataCriacao', $util->view_date_to_bd($dataDe) . ' 00:00:00', $util->view_date_to_bd($dataAte) . ' 23:59:59', 'AND');
        }

        $criteria->order = 't.dataCriacao DESC, t.destinatario ASC';

        $recebimentos = RecebimentoDeDocumentacao::model()->findAll($criteria);

        foreach ($recebimentos as $r)
        {
           $totalRepasseApp += $r->totalRepasseAprovado();
        }

        $criteria->limit = 1000;

        $rows = array();

        if (count($recebimentos) > 0)
        {
            $criteria->offset = $start;
            $criteria->limit = 10;

            foreach (RecebimentoDeDocumentacao::model()->findAll($criteria) as $recebimento)
            {

                $row = array(
                                'codigo' => $recebimento->codigo,
                                'data_criacao' => $util->bd_date_to_view(substr($recebimento->dataCriacao, 0, 10)) . ' às ' . substr($recebimento->dataCriacao, 11),
                                'operador' => strtoupper($recebimento->CriadoPor->nome_utilizador),
                                'parceiro' => $recebimento->Destinatario->getConcat(),
                                'count_aprovadas' => $recebimento->totalItensBordero(),
                                'count_pendentes' => $recebimento->totalItensPendentes(),
                                'total_aprovado' => "R$ " . number_format($recebimento->totalFinanciadoAprovado(), 2, ',', '.'),
                                'total_pendente' => "R$ " . number_format($recebimento->totalFinanciadoPendente(), 2, ',', '.'),
                                'repasse_aprovado' => "R$ " . number_format($recebimento->totalRepasseAprovado(), 2, ',', '.'),
                                'repasse_pendente' => "R$ " . number_format($recebimento->totalRepassePendente(), 2, ',', '.'),
                                'btn_print' => '<form method="POST" target="_blank" action="/printer/recebimentoDeContratoOmni/"><input type="hidden" value="' . $recebimento->id . '" name="codigo"><button class="submit"><i class="fa fa-print"></i></button></form>',
                            );

                $rows[] = $row;
            }

       }

       return array(
           'draw' => $draw,
           'recordsTotal' => count($rows),
           'recordsFiltered' => count($recebimentos),
           'data' => $rows,
           'customReturn' => array(
               'totalRepasse' => number_format($totalRepasseApp, 2, ',', '.')
           )
       );
    }

    public function repasse($dataDe, $dataAte, $filiais, $draw) {
        $util = new Util;
        $rows = array();
        $criteria = new CDbCriteria;
        $criteria->addInCondition('isMatriz', array(0), 'AND');
        $criteria->addInCondition('habilitado', array(1), 'AND');

        /* if( !empty( $dataDe ) && !empty( $dataAte ) )
          {
          $criteria->addBetweenCondition('t.data_cadastro',  $util->view_date_to_bd($dataDe).' 00:00:00', $util->view_date_to_bd($dataAte).' 23:59:59', 'AND');
          }
         */

        if (!empty($filiais)) {
            $idsFiliais = array();

            for ($i = 0; $i < count($filiais); $i ++) {
                $idsFiliais[] = $filiais[$i];
            }

            $criteria->addInCondition('id', $idsFiliais, ' AND ');
        }

        $parceiros = Filial::model()->findAll($criteria);


        if ($parceiros != NULL && count($parceiros) > 0) {
            foreach ($parceiros as $parceiro) {
                $row = array(
                    'nome' => $util->cleanStr(strtoupper($parceiro->nome_fantasia . ' - ' . $parceiro->getEndereco()->cidade . '/' . $parceiro->getEndereco()->uf), 'upp'),
                    'valor' => number_format(0, 2, ',', '.')
                );
                $rows[] = $row;
            }
        }

        return array(
            'draw' => $draw,
            'recordsTotal' => count($rows),
            'recordsFiltered' => count($rows),
            'data' => $rows,
        );
    }

    public function reportProducaoParceiro($parceiroId, $dataDe, $dataAte) {

        $criteria = new CDbCriteria;
        $util = new Util;

        if (!empty($dataDe) && !empty($dataAte)) {
            $criteria->addBetweenCondition('t.data_cadastro', $util->view_date_to_bd($dataDe) . ' 00:00:00', $util->view_date_to_bd($dataAte) . ' 23:59:59', 'AND');
        }

        $criteria->with = array('analiseDeCredito' => array(
                'alias' => 'ac'
        ));

        $criteria->addInCondition('ac.Filial_id', array($parceiroId), ' AND ');
        $criteria->addInCondition('t.habilitado', array(1), ' AND ');
        $criteria->addInCondition('t.Titulos_gerados', array(1), ' AND ');
        $criteria->addInCondition('t.Status_Proposta_id', array(2, 7), ' AND ');
        $criteria->order = 't.data_cadastro DESC';

        return Proposta::model()->findAll($criteria);
    }

    public function reportProducao($draw, $start, $dataDe, $dataAte, $filiais, $valorDe, $valorAte, /*$ambiente,*/ $tipoFin, $servicos){

        $util               = new Util;
        $rows               = array();
        $criteria           = new CDbCriteria;
        $countValTotalIni   = 0;
        $countValPagIni     = 0;
        $countValTotal      = 0;
        $countValFin        = 0;
        $countValPag        = 0;
        $countEntTot        = 0;
        $countSegTot        = 0;
        $countEntPag        = 0;

        $modalidades = [];

        if (!empty($dataDe) && !empty($dataAte)) {
            $criteria->addBetweenCondition('t.data_cadastro', $util->view_date_to_bd($dataDe) . ' 00:00:00', $util->view_date_to_bd($dataAte) . ' 23:59:59', 'AND');
        }else{
            $criteria->addBetweenCondition('t.data_cadastro', date('Y-m-d') . ' 00:00:00', date('Y-m-d') . ' 23:59:59', 'AND');
        }

        if(!empty($servicos))
        {
            if(in_array(5, $servicos)){
                $servicos[] = 6;
            }
            //$servicosStr = join(",", $servicos);

            $criteria->addInCondition('t.Financeira_id', $servicos, ' AND ');

        }

        /*$criteria->with = array('analiseDeCredito' => array(
            'alias' => 'ac'
        ));*/


        if (!empty($filiais)) {
            $idsFiliais = array();

            for ($i = 0; $i < count($filiais); $i ++) {
                $idsFiliais[] = $filiais[$i];
            }

            $criteria->addInCondition('ac.Filial_id', $idsFiliais, ' AND ');
        }

        if( !empty($valorDe) && !empty($valorAte) ){
            $criteria->addBetweenCondition('t.valor-t.valor_entrada', $util->moedaViewToBD($valorDe), $util->moedaViewToBD($valorAte), 'AND');
        }

        $criteria->join = "INNER JOIN Analise_de_Credito    AS ac ON ac.id = t.Analise_de_Credito_id    ";


        /*if( $ambiente == 1 )
        {
            $criteria->join .= "LEFT JOIN PropostaOmniConfig AS poc ON poc.Proposta = t.id AND poc.habilitado ";
            $criteria->addInCondition('poc.id', array(null), ' AND ');
            $criteria->addInCondition('t.Financeira_id',           array(5),   ' AND ');
        }

        if( $ambiente == 2 )
        {
            $criteria->join .= "INNER JOIN PropostaOmniConfig AS poc ON poc.Proposta = t.id AND poc.habilitado ";
        }

        if( $ambiente == 3 )
        {
            $criteria->addInCondition('t.Financeira_id',           array(10),   ' AND ');
        }*/

        if( !empty($tipoFin) )
        {
            for( $y = 0; $y < sizeof($tipoFin); $y++ ){

                if ($tipoFin[$y] != 'multiselect-all') {
                    $modalidades[] = $tipoFin[$y];
                }
            }

            $modalidades = join($modalidades,',');

            $criteria->join .= "INNER JOIN Tabela_Cotacao AS tc ON tc.habilitado AND tc.id = t.Tabela_id AND tc.ModalidadeId IN (". $modalidades .") ";
        }

        $criteria->addInCondition('t.habilitado',           array(1),   ' AND ');
        $criteria->addInCondition('t.Titulos_gerados',      array(1),   ' AND ');
        $criteria->addInCondition('t.Status_Proposta_id',   array(2, 7),' AND ');

        /*
        $criteria->condition = 'email=:email AND pass=:pass';
        $criteria->params = array(':email'=>$email, ':pass'=>$pass);
        */

        $criteria->order = 't.data_cadastro DESC';

        $resultado = Proposta::model()->findAll($criteria);

        if (count($resultado) > 0) {


            foreach ($resultado as $proposta)
            {

                $countValTotalIni   += $proposta->valor;
                $countEntTot        += $proposta->valor_entrada;
                $countSegTot        += $proposta->calcularValorDoSeguro();
                $countValFin        += $proposta->getValorFinanciado();
                $countValTotal      += $proposta->getValorFinal();

            }


            $criteria->offset = $start;
            $criteria->limit = 10;

            foreach ( Proposta::model()->findAll( $criteria ) as $r )
            {

                $financeira = ""    ;
                $servico    = "CDC" ;

                if($r->Financeira_id == 5)
                {

                    $financeira = "CredShow"    ;

                    $pOmniConfig = PropostaOmniConfig::model()->find("habilitado AND Proposta = $r->id");

                    if($pOmniConfig !== null && count($pOmniConfig) > 0)
                    {
                        $financeira = "Omni";
                    }

                }
                else
                {

                    $financeira = $r->financeira->nome;

                    if($r->Financeira_id == 10)
                    {
                        $servico = "CSC";
                    }

                }

                $row = array
                (
                    "codigo"            => $r->codigo,
                    "financeira"        => $financeira,
                    "servico"           => $servico,
                    "emissao"           => $util->bd_date_to_view(substr($r->data_cadastro, 0, 10)),
                    "filial"            => strtoupper($r->analiseDeCredito->filial->nome_fantasia . ' - ' . $r->analiseDeCredito->filial->getEndereco()->cidade . ' / ' . $r->analiseDeCredito->filial->getEndereco()->uf),
                    "cliente"           => strtoupper($r->analiseDeCredito->cliente->pessoa->nome),
                    "cpf"               => $r->analiseDeCredito->cliente->pessoa->getCPF()->numero,
                    "val_inicial"       => number_format($r->valor, 2, ',', '.'),
                    "val_entrada"       => number_format($r->valor_entrada, 2, ',', '.'),
                    "seguro"            => number_format($r->calcularValorDoSeguro(), 2, ',', '.'),
                    "carencia"          => $r->carencia,
                    "val_financiado"    => number_format($r->getValorFinanciado(), 2, ',', '.'),
                    "condi_parce"       => $r->qtd_parcelas . ' x ' . number_format($r->getValorParcela(), 2, ',', '.'),
                    "val_total"         => number_format($r->qtd_parcelas * $r->getValorParcela(), 2, ',', '.')
                );

                $rows[]             = $row;
            }
        }

        return array(
            'draw'                  => $draw,
            'recordsTotal'          => count($rows),
            'recordsFiltered'       => count($resultado),
            'data'                  => $rows,
            "customReturn"          => array(
                'countValTotalIni'  => "R$ " . number_format($countValTotalIni, 2, ',', '.'),
                'countEntTot'       => "R$ " . number_format($countEntTot, 2, ',', '.'),
                'countSegTot'       => "R$ " . number_format($countSegTot, 2, ',', '.'),
                'totalFin'          => "R$ " . number_format($countValFin, 2, ',', '.'),
                'totalGeral'        => "R$ " . number_format($countValTotal, 2, ',', '.'),
                'retorno'           => $modalidades,
            ),
            'criteria'              => $criteria
        );
    }

    public function listarAtrasos($draw, $start, $idsFiliais /* ,$analistas */) {
        $idsF = array();
        $util = new Util;

        if (!empty($idsFiliais)) {
            for ($i = 0; $i < count($idsFiliais); $i ++) {
                $idsF[] = $idsFiliais[$i];
            }
        }

        $rows = array();
        $sql = "SELECT `p`.`id`, `p`.`seq`, `p`.`vencimento`, `p`.`valor`, `p`.`valor_atual`, `pr`.`id` as `proposta_id`, `rg`.`id`, `cd`.`nosso_numero` as `numero_titulo` FROM `Parcela` as `p` ";
        $sql .= " INNER JOIN Ranges_gerados as `rg` ON `rg`.`Parcela_id` = `p`.`id` ";
        $sql .= " INNER JOIN Cnab_detalhe as `cd` ON `cd`.`nosso_numero` LIKE CONCAT( '%',`rg`.`label`, '%' ) ";
        $sql .= " INNER JOIN `Titulo` as `t` ON `p`.`Titulo_id` = `t`.`id` ";
        $sql .= " INNER JOIN `Proposta` as `pr` ON `t`.`Proposta_id` = `pr`.`id` ";
        $sql .= " INNER JOIN `Analise_de_Credito` as `a` ON `pr`.`Analise_de_Credito_id` = `a`.`id` ";
        $sql .= " INNER JOIN `Filial` as `f` ON `a`.`Filial_id` = `f`.`id`";
        $sql .= " INNER JOIN `Empresa` as `e` ON `f`.`Empresa_id` = `e`.`id` ";
        $sql .= " WHERE `e`.`id` = " . Yii::app()->session['usuario']->getEmpresa()->id . " and `p`.`vencimento` < '" . date('Y-m-d') . "'  and `p`.`valor_atual` < `p`.`valor`  ";

        if (!empty($idsFiliais)) {
            $sql .= " AND f.id IN (" . implode("', '", $idsFiliais) . ") ";
        }

        $sql .= " and `p`.`habilitado` ORDER BY `p`.`vencimento` ASC ";
        $resultado = Yii::app()->db->createCommand($sql)->queryAll();

        if (count($resultado) > 0) {
            $sql .= " LIMIT " . $start . " ,10";

            foreach (Yii::app()->db->createCommand($sql)->queryAll() as $r) {
                $btn_cob = '<form method="post" action="' . Yii::app()->getBaseUrl(true) . '/cobranca/cobrancasParcela">';
                $btn_cob .= '  <button class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Editar"><i class="fa fa-edit"></i></button>';
                $btn_cob .= '  <input type="hidden" name="idParcela" value="' . $r['id'] . '">';
                $btn_cob .= '</form>';
                $htmlTableProps = "";
                $proposta = Proposta::model()->findByPk($r['proposta_id']);
                $parcela = Parcela::model()->findByPk($r['id']);

                $statusParcela = $parcela->statusPagamento();
                $telefones = $proposta->analiseDeCredito->cliente->pessoa->listarTelefones(1);
                $referencias = Referencia::model()->findAll('Cliente_id = ' . $proposta->analiseDeCredito->cliente->id);

                $numerosTel = '';
                $numerosRef = '';


                if (count($referencias) > 0) {

                    foreach ($referencias as $referencia) {

                        if (count($referencia->contato->listarTelefones()) > 0) {
                            foreach ($referencia->contato->listarTelefones() as $telRef) {
                                $numerosRef .= $numerosRef . $telRef->numero . ' - ' . strtoupper($referencia->nome) . ' - Referencia ' . strtoupper($referencia->tipoReferencia->tipo);
                            }
                        }
                    }
                }

                for ($i = 0; $i < count($telefones['data']); $i++) {
                    $v = '';

                    if ($i != 0) {
                        $v = ', ';
                    }

                    $numerosTel .= $numerosTel . $v . $telefones['data'][$i]['numero'] . ' - ' . $telefones['data'][$i]['tipo'] . $v;
                }

                $htmlTableProps = '<tr>'
                        . '<td>' . $numerosTel . '</td>'
                        . '</tr>'
                        . '<tr>'
                        . '<td>' . $numerosRef . '</td>'
                        . '</tr>';


                $nomeAnalista = ( $proposta->returnAnalistaAprovou() != NULL ? $proposta->returnAnalistaAprovou()->analista->nome_utilizador : "----------" );

                $row = array(
                    'btn-more' => '',
                    'nosso_numero' => $r['numero_titulo'],
                    'proposta' => $proposta->codigo,
                    'filial' => strtoupper($proposta->analiseDeCredito->filial->nome_fantasia . ' - ' . $proposta->analiseDeCredito->filial->getEndereco()->cidade . ' / ' . $proposta->analiseDeCredito->filial->getEndereco()->uf),
                    'cliente' => strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome),
                    'cpf' => $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero,
                    'nasc' => $util->bd_date_to_view($proposta->analiseDeCredito->cliente->pessoa->nascimento),
                    'parcela' => $r['seq'] . '°',
                    'valor' => number_format($r['valor'], 2, ',', '.'),
                    'dias_em_atraso' => '<span class="' . $statusParcela['span_class'] . '">' . $statusParcela['mensagem'] . '</span>',
                    'analista' => $nomeAnalista,
                    'btn_reg_cob' => $btn_cob,
                    'detalhesCliente' => array(
                        '<table class="table table-striped table-bordered table-hover table-full-width">'
                        . '<thead>'
                        . '<tr>'
                        . '<th>Telefones</th>'
                        . '</tr>'
                        . '</thead>'
                        . '<tbody>' . $htmlTableProps . '</tbody>'
                        . '</table>',
                    ),
                );

                $rows[] = $row;

                $btn_cob = '';
            }
        }

        return (array(
            "draw" => $draw,
            "recordsFiltered" => count($resultado),
            "recordsTotal" => count($rows),
            "data" => $rows
        ));
    }

    public function listarInadimplencias($draw, $start, $filiais, $analistas, $cpf) {

        $rows = array();
        $valorTotal = 0;
        $htmlTableProps = "";
        $cpfPesquisa = 0;
        $util = new Util;

        if (!empty($filiais)) {
            $filiais = join(',', $filiais);
        }

        if (!empty($analistas)) {
            $analistas = join(',', $analistas);
        }

        $sql = "SELECT P.id as 'ParcelaId', P.valor as 'ValorParcela', U.id as AnalistaId, Aps.Analista_id as Analista, Pr.id AS Proposta, P.* ";
        $sql .= " FROM Parcela AS P ";
        $sql .= " INNER JOIN Titulo AS T ON T.id = P.Titulo_id AND T.NaturezaTitulo_id = 1 AND T.VendaW_id IS NULL";
        $sql .= " INNER JOIN Proposta AS Pr ON T.Proposta_id = Pr.id AND Pr.Status_Proposta_id = 2";
        $sql .= " INNER JOIN Analise_de_Credito AS AC ON AC.id = Pr.Analise_de_Credito_id ";

        if (!empty($cpf) && $cpf != NULL && trim($cpf) != '') {
            $cpfPesquisa = $cpf;
            $sql .= " INNER JOIN Cliente AS CLI ON AC.Cliente_id = CLI.id ";
            $sql .= " INNER JOIN Pessoa AS PES ON CLI.Pessoa_id = PES.id ";
            $sql .= " INNER JOIN Pessoa_has_Documento PHD ON PHD.Pessoa_id = PES.id ";
            $sql .= " INNER JOIN Documento DOC ON DOC.id = PHD.Documento_id AND DOC.numero = $cpf ";
        }

        $sql .= " INNER JOIN Analista_has_Proposta_has_Status_Proposta AS Aps ON Aps.Proposta_id = Pr.id AND Aps.Status_Proposta_id = 2";
        $sql .= " INNER JOIN Usuario AS U ON Aps.Analista_id = U.id ";
        $sql .= " INNER JOIN Ranges_gerados  AS Rg ON Rg.Parcela_id = P.id ";

        if (!empty($filiais)) {
            $sql .= " INNER JOIN Filial as FI ON FI.id = AC.Filial_id ";
        }

        $sql .= " WHERE ";
        $sql .= " DATEDIFF('" . date('Y-m-d') . "', P.vencimento) >= 20 ";
        $sql .= " AND P.valor_atual < P.valor AND P.habilitado ";

        if (!empty($filiais)) {
            $sql .= " AND FI.id IN($filiais) ";
        }

        if (!empty($analistas)) {
            $sql .= " AND U.id IN($analistas) ";
        }


        foreach (Yii::app()->db->createCommand($sql)->queryAll() as $r) {
            $valorTotal += $r['ValorParcela'];
        }

        $sql .= " ORDER BY P.vencimento ASC ";

        $countTotal = count(Yii::app()->db->createCommand($sql)->queryAll());

        $sql .= " LIMIT $start,10 ";

        foreach (Yii::app()->db->createCommand($sql)->queryAll() as $r) {
            $parcela = Parcela::model()->findByPk($r['ParcelaId']);
            $range = RangesGerados::model()->find('Parcela_id = ' . $parcela->id);
            $analista = Usuario::model()->findByPk($r['AnalistaId']);

            /* Contato do cliente */
            $telefones = $parcela->titulo->proposta->analiseDeCredito->cliente->pessoa->listarTelefones(1);
            $referencias = Referencia::model()->findAll('Cliente_id = ' . $parcela->titulo->proposta->analiseDeCredito->cliente->id);

            $numerosTel = '';
            $numerosRef = '';


            if (count($referencias) > 0) {
                foreach ($referencias as $referencia) {
                    if (count($referencia->contato->listarTelefones()) > 0) {
                        foreach ($referencia->contato->listarTelefones() as $telRef) {
                            $numerosRef .= $numerosRef . $telRef->numero . ' - ' . strtoupper($referencia->nome) . ' - Referencia ' . strtoupper($referencia->tipoReferencia->tipo);
                        }
                    }
                }
            }

            for ($i = 0; $i < count($telefones['data']); $i++) {
                $v = '';

                if ($i != 0) {
                    $v = ', ';
                }

                $numerosTel .= $numerosTel . $v . $telefones['data'][$i]['numero'] . ' - ' . $telefones['data'][$i]['tipo'] . $v;
            }

            $htmlTableProps = '<tr>'
                    . '<td>' . $numerosTel . '</td>'
                    . '</tr>'
                    . '<tr>'
                    . '<td>' . $numerosRef . '</td>'
                    . '</tr>';

            $statusParcela  = $parcela->statusPagamento();

            $classBtnFich   = "btn-danger btn-add-fich ";
            $classBtnAudit  = "";

            $stylecpf       = '';
            $clienteFichado = $parcela->titulo->proposta->analiseDeCredito->cliente->hasFichamento();

            if ($clienteFichado) //cliente esta fichado?
            {
              $stylecpf     = ' style="color:#d9534f; font-weight: bold;"';
            }

            $parcelaFichada = $parcela->hasFichamento();

            if ($parcelaFichada)
            {
                $classBtnFich = "btn-dark-grey";
            }

            /* Verifica se a parcela está em auditoria */
            if (Auditoria::model()->hasAuditoria("Parcela", $parcela->id))
            {
                $classBtnAudit = " disabled ";
            }

            $btn_cob = '<form method="post" action="' . Yii::app()->getBaseUrl(true) . '/cobranca/cobrancasParcela">';
            $btn_cob .= '  <button class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Editar"><i class="fa fa-edit"></i></button>';
            $btn_cob .= '  <input type="hidden" name="idParcela" value="' . $r['id'] . '">';
            $btn_cob .= '  <input type="hidden" name="cpfPesquisa" value="' . $cpfPesquisa . '">';
            $btn_cob .= '</form>';


            $btn_add_fich = '<button data-parcela="' . $parcela->id . '" class="btn btn-xs ' . $classBtnFich . ' "><i class="fa fa-exclamation-circle"></i></button>';
            $btn_add_audit = '<button data-parcela="' . $parcela->id . '" class="' . $classBtnAudit . ' btn btn-xs btn-warning btn-add-audit"><i class="fa fa-exclamation-triangle"></i></button>';

            $row = array(
                "filial"            => $parcela->titulo->proposta->analiseDeCredito->filial->getConcat(),
                "cliente"           => substr(strtoupper($parcela->titulo->proposta->analiseDeCredito->cliente->pessoa->nome), 0, 30),
                "cpf"               => '<a' . $stylecpf . '>' . $parcela->titulo->proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero . '</a>',
                "nascimento"        => $util->bd_date_to_view($parcela->titulo->proposta->analiseDeCredito->cliente->pessoa->nascimento),
                "parcela"           => $parcela->seq . '°',
                "valor"             => "R$ " . number_format($parcela->valor, 2, ',', '.'),
                "data_da_compra"    => $util->bd_date_to_view(substr($parcela->titulo->proposta->data_cadastro, 0,10)),
                "vencimento"        => $util->bd_date_to_view($parcela->vencimento),
                "dias_em_atraso"    => '<span class="' . $statusParcela['span_class'] . '">' . $statusParcela['mensagem'] . '</span>',
                "analista"          => strtoupper($analista->username),
                "btn_reg_cob"       => $btn_cob,
                "btn_add_fich"      => $btn_add_fich,
                "btn_add_audit"     => $btn_add_audit,
                "detalhesCliente"   => array(
                    '<table class="table table-striped table-bordered table-hover table-full-width">'
                    . '<thead>'
                    . '<tr>'
                    . '<th>Telefones</th>'
                    . '</tr>'
                    . '</thead>'
                    . '<tbody>' . $htmlTableProps . '</tbody>'
                    . '</table>',
                ),
            );

            $rows[] = $row;
        }

        return (array(
            "draw" => $draw,
            "recordsFiltered" => $countTotal,
            "recordsTotal" => count($rows),
            "data" => $rows,
            "customReturn" => array(
                "valorTotal" => "R$ " . number_format($valorTotal, 2, ',', '.'),
                "cpfPesquisa" => $cpfPesquisa
            ),
            "query"=> $sql
        ));
    }

    public function getRetornos($draw, $data_de, $data_ate, $offset) {

        $recordsFiltered = 0;
        $rows = array();
        $util = new Util;

        $criteriaFilter = new CDbCriteria;

        $retornos = RetornoParcelaCnab::model()->findAll();

        $recordsFiltered = count($retornos);

        if (!empty($data_de) && $data_de != NULL && !empty($data_ate) && $data_ate != NULL) {
            $criteriaFilter->addBetweenCondition('t.data_importacao', $util->view_date_to_bd($data_de), $util->view_date_to_bd($data_ate), 'AND');
            $recordsFiltered = count(RetornoParcelaCnab::model()->findAll($criteriaFilter));
        }

        $criteriaFilter->offset = $offset;
        $criteriaFilter->limit = 10;
        $criteriaFilter->order = "t.id DESC";
        $retornos = RetornoParcelaCnab::model()->findAll($criteriaFilter);

        if (count($retornos) > 0) {

            foreach ($retornos as $retorno) {

                $row = array(
                    'titulo' => $retorno->nosso_numero,
                    'ocorrencia' => $retorno->ocorrenciaRetornoCnab->descricao,
                    'data_importacao' => $util->bd_date_to_view($retorno->data_importacao),
                    'data_ocorrencia' => ($retorno->data_ocorrencia != null ? $util->bd_date_to_view($retorno->data_ocorrencia) : '---'),
                    'valor_pago' => number_format($retorno->valor_pago, 2, ',', '.'),
                    'valor_mora' => number_format($retorno->juros_de_mora, 2, ',', '.'),
                    //'origem_do_pagamento' => ($retorno->valor_pago > 0 ? $retorno->origemDoPagamento->descricao : '----' ),
                    'motivo_da_rejeicao' => ($retorno->Motivo_rejeicao_titulo_id != NULL ? $retorno->motivoRejeicaoTitulo->descricao : '----'),
                );

                $rows[] = $row;
            }
        }

        return (array(
            "draw" => $draw,
            "recordsFiltered" => $recordsFiltered,
            "recordsTotal" => count($rows),
            "data" => $rows,
        ));
    }

    public function clearRetornosRegistrados() {

        $retornosRegistrados = RetornoParcelaCnab::model()->findAll('Ocorrencia_retorno_cnab_id = ' . 2);

        if (count($retornosRegistrados) > 0) {
            foreach ($retornosRegistrados as $r) {
                $r->delete();
            }
        }
    }

    public function getFinanciamentos($draw, $offset, $dataDe, $dataAte, $filiais, $qtd_parc_de, $qtd_parc_ate) {

        /**/
        $filiaisGrahp = array();
        $maxQtdParcelas = 39;
        $carencias = array();
        $carencias[15] = array('15 DIAS', 0);
        $carencias[30] = array('30 DIAS', 0);
        $carencias[35] = array('35 DIAS', 0);
        $carencias[40] = array('40 DIAS', 0);
        $carencias[45] = array('45 DIAS', 0);
        $carencias[50] = array('50 DIAS', 0);
        $carencias[55] = array('55 DIAS', 0);
        $carencias[60] = array('60 DIAS', 0);
        $carencias[90] = array('90 DIAS', 0);
        $carencias[120] = array('120 DIAS', 0);

        $arrCountParcelas = array();

        for ($i = 1; $i <= $maxQtdParcelas; $i++) {

            $stringParceLabel = ' PARCELAS';

            if ($i == 0) {
                $stringParceLabel = ' PARCELA';
            }

            $arrCountParcelas[$i] = array(
                strval($i) . $stringParceLabel, 0
            );
        }

        /**/

        $idsFiliais = array();
        $crt = new CDbCriteria;
        $util = new Util;
        $recordsTotal = 0;
        $recordsFiltered = 0;
        $rows = array();

        $totalInicial = 0;
        $totalFinanciado = 0;
        $totalFinaPag = 0;

        $totalEntradaPag = 0;
        $totalEntradas = 0;
        $totalSeguro = 0;

        $totalFutPag = 0;
        $totalFut = 0;

        if (count($this->listFiliais()) > 0) {

            foreach ($this->listFiliais() as $filial) {
                $idsFiliais[] = $filial->id;
            }

            if (!empty($dataDe) && !empty($dataAte)) {
                $crt->addBetweenCondition('t.data_cadastro', $util->view_date_to_bd($dataDe) . ' 00:00:00', $util->view_date_to_bd($dataAte) . ' 23:59:59', 'AND');
            }

            if (!empty($qtd_parc_de) && !empty($qtd_parc_ate)) {
                $crt->addBetweenCondition('t.qtd_parcelas', $qtd_parc_de, $qtd_parc_ate, 'AND');
            }

            if (!empty($filiais)) {
                $idsFiliais = array();

                for ($i = 0; $i < count($filiais); $i ++) {
                    $idsFiliais[] = $filiais[$i];
                }
            }

            for ($i = 0; $i < count($idsFiliais); $i++) {

                $args = array();
                $args['dataDe'] = NULL;
                $args['dataAte'] = NULL;
                $args['parc_de'] = NULL;
                $args['parc_ate'] = NULL;

                if (!empty($dataDe) && !empty($dataAte)) {
                    $args['dataDe'] = $dataDe;
                    $args['dataAte'] = $dataAte;
                }

                if (!empty($qtd_parc_de) && !empty($qtd_parc_ate)) {
                    $args['parc_de'] = $qtd_parc_de;
                    $args['parc_ate'] = $qtd_parc_ate;
                }

                $filialGp = Filial::model()->findByPk($idsFiliais[$i]);
                $filiaisGrahp[] = $filialGp->propostasFilialGraph($args);
            }

            $crt->with = array('analiseDeCredito' => array('alias' => 'ac'));

            $crt->addInCondition('ac.Filial_id', $idsFiliais, ' AND ');
            $crt->addInCondition('t.Status_Proposta_id', array(2), ' AND ');
            $crt->addInCondition('t.titulos_gerados', array(1), ' AND ');
            $crt->addInCondition('t.habilitado', array(1), ' AND ');
            $crt->order = "t.data_cadastro DESC";

            $recordsTotal = count(Proposta::model()->findAll($crt));

            foreach (Proposta::model()->findAll($crt) as $p) {

                $totalInicial += $p->valor;
                $totalFinanciado += ( $p->getValorFinanciado() );
                $totalEntradas += $p->valor_entrada;
                $totalSeguro += $p->calcularValorDoSeguro();
                $totalFut += $p->getValorFinal();

                $arrCountParcelas[$p->qtd_parcelas][1] += 1;
                $carencias[$p->carencia][1] += 1;
            }

            $crt->offset = $offset;
            $crt->limit = 10;

            $propostas = Proposta::model()->findAll($crt);
            $recordsFiltered = count($propostas);

            if (count($propostas) > 0) {
                foreach ($propostas as $proposta) {

                    if ($proposta->Status_Proposta_id == 2 && $proposta->titulos_gerados) {

                        $row = array(
                            'codigo' => $proposta->codigo,
                            'emissao' => $util->bd_date_to_view(substr($proposta->data_cadastro, 0, 10)),
                            'filial' => strtoupper($proposta->analiseDeCredito->filial->nome_fantasia) . ' - ' . strtoupper($proposta->analiseDeCredito->filial->getEndereco()->cidade . '/' . $proposta->analiseDeCredito->filial->getEndereco()->uf),
                            'cliente' => strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome),
                            'cpf' => $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero,
                            'valor' => "R$ " . number_format($proposta->valor, 2, ',', ''),
                            'entrada' => "R$ " . number_format($proposta->valor_entrada, 2, ',', ''),
                            'seguro' => "R$ " . number_format($proposta->calcularValorDoSeguro(), 2, ',', ''),
                            'val_fin' => "R$ " . number_format($proposta->getValorFinanciado(), 2, ',', ''),
                            'qtd_par' => $proposta->qtd_parcelas . ' x ' . number_format($proposta->getValorParcela(), 2, ',', '.'),
                            'val_fut' => "R$ " . number_format($proposta->getValorFinal(), 2, ',', ''),
                        );

                        $rows[] = $row;
                    }
                }
            }
        }

        return (array(
            "draw" => $draw,
            "recordsTotal" => $recordsFiltered,
            "recordsFiltered" => $recordsTotal,
            "data" => $rows,
            "customReturn" => array(
                'totalInicial' => "R$ " . number_format($totalInicial, 2, ',', '.'),
                'totalFin' => "R$ " . number_format($totalFinanciado, 2, ',', '.'),
                'totalEntradas' => "R$ " . number_format($totalEntradas, 2, ',', '.'),
                'totalSeguro' => "R$ " . number_format($totalSeguro, 2, ',', '.'),
                'totalFut' => "R$ " . number_format($totalFut, 2, ',', '.'),
                'graphs' => array(
                    'porFiliais' => $filiaisGrahp,
                    'porQtdParc' => $arrCountParcelas,
                    'porQtdCare' => $carencias
                )
            )
        ));
    }

    public function contasAReceber() {

        $empresaId = Yii::app()->session['usuario']->empresasId('query');

        $empresa = Empresa::model()->findByPk($empresaId[0]['id']);

        $filiais = $empresa->listFiliais();

        $arrParcelas = array();

        foreach ($filiais as $filial) {

            $propostas = $filial->listPropostasFilial();

            /* for ( $i = 0; $i < count( $propostas ); $i++ ) {

              $titulo = Titulo::model()->find( 'Proposta_id = ' . $propostas[ $i ][0]['id'] );

              if ( $titulo != NULL )
              {
              $parcelaCriteria = new CDbCriteria;
              $parcelaCriteria->condition = 'Titulo_id=:titulo_id';
              $parcelaCriteria->params = array(':titulo_id' => $titulo->id);
              $parcelaCriteria->order = 'vencimento DESC';

              $parcelas = Parcela::model()->findAll($parcelaCriteria);

              foreach ( $parcelas as $p ) {

              if ( $p->valor > $p->valor_atual )
              array_push($arrParcelas, $p->attributes);

              }
              }

              } */

            //$vendas = $filial->listVendas();

            /* foreach ( $vendas as $venda ) {

              $titulo = Titulo::model()->find( 'Venda_id = ' . $venda->id );
              $parcelas = Parcela::model()->findAll( 'Titulo_id = ' . $titulo->id );

              foreach ( $parcelas as $p ) {

              if ( $p->valor > $p->valor_atual )
              array_push($arrParcelas, $p->attributes);

              }
              } */
        }

        $aaData = array();

        $output = array(
            "sEcho" => 1,
            "iTotalRecords" => count($aaData),
            "iTotalDisplayRecords" => 5,
            "aaData" => array()
        );

        for ($i = 0; $i < count($arrParcelas); $i++) {

            $row = array();

            $titulo = Titulo::model()->findByPk($arrParcelas[$i]['Titulo_id']);

            if ($titulo->Proposta_id != NULL) {

                $proposta = Proposta::model()->findByPk($titulo->Proposta_id);
                array_push($row, 'Financiamento');
                array_push($row, $proposta->codigo);
                array_push($row, $proposta->analiseDeCredito->cliente->pessoa->nome);
                array_push($row, $proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero);
            } else {

                $titulo = Titulo::model()->findByPk($arrParcelas[$i]['Titulo_id']);
                array_push($row, 'Loja Virtual');
                array_push($row, $titulo->venda->codigo_externo);
                array_push($row, $titulo->venda->cliente->pessoa->nome);
                array_push($row, $titulo->venda->cliente->pessoa->getCPF()->numero);
            }

            foreach ($arrParcelas[$i] as $key => $value) {
                array_push($row, $value);
            }

            array_push($aaData, $row);
        }

        $output['aaData'] = $aaData;

        return $output;
    }

    public function tableName() {

        return 'Empresa';
    }

    private function setConn() {

        $this->connection = new CDbConnection(
                Yii::app()->params['dbconf']['dns'], Yii::app()->params['dbconf']['user'], Yii::app()->params['dbconf']['pass']
        );
    }

    public function rules() {


        return array(
            array('habilitado, Unidade_de_negocio_id', 'numerical', 'integerOnly' => true),
            array('razao_social', 'length', 'max' => 120),
            array('nome_fantasia', 'length', 'max' => 100),
            array('cnpj, inscricao_estadual', 'length', 'max' => 45),
            array('data_de_abertura, data_cadastro, data_cadastro_br', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, razao_social, nome_fantasia, cnpj, habilitado, data_cadastro, data_cadastro_br, inscricao_estadual, Unidade_de_negocio_id, data_de_abertura', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'unidadeDeNegocio' => array(self::BELONGS_TO, 'UnidadeDeNegocio', 'Unidade_de_negocio_id'),
            'empresaHasUsuarios' => array(self::HAS_MANY, 'EmpresaHasUsuario', 'Empresa_id'),
        );
    }

    public function attributeLabels() {

        return array(
            'id' => 'ID',
            'razao_social' => 'Razao Social',
            'nome_fantasia' => 'Nome Fantasia',
            'cnpj' => 'Cnpj',
            'habilitado' => 'Habilitado',
            'data_cadastro' => 'Data Cadastro',
            'data_cadastro_br' => 'Data / Hora Cadastro',
            'inscricao_estadual' => 'Inscricao Estadual',
            'Unidade_de_negocio_id' => 'Atividade Econômica Principal',
            'data_de_abertura' => 'Data de Abertura',
        );
    }

    public function search() {

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('razao_social', $this->razao_social, true);
        $criteria->compare('nome_fantasia', $this->nome_fantasia, true);
        $criteria->compare('cnpj', $this->cnpj, true);
        $criteria->compare('habilitado', $this->habilitado);
        $criteria->compare('data_cadastro', $this->data_cadastro, true);
        $criteria->compare('data_cadastro_br', $this->data_cadastro_br, true);
        $criteria->compare('inscricao_estadual', $this->inscricao_estadual, true);
        $criteria->compare('Unidade_de_negocio_id', $this->Unidade_de_negocio_id);
        $criteria->compare('data_de_abertura', $this->data_de_abertura);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /* Função que retorna um array contendo os ids dos usuários (do sistema) da empresa */

    public function usuariosEmpresa($user) {

        //$this->setConn();

        $sql = "select u.id, u.username\n"
                . " from `Usuario` as u\n"
                . " inner join `Empresa_has_Usuario` as eu on u.id = eu.Usuario_id\n"
                . " inner join `Empresa` as e on eu.Empresa_id = e.id\n"
                . " where e.id = " . $this->id . " and  u.id != " . $user->id . " and u.tipo != " . "'$user->tipo'";

        if ($user->tipo != 'super') {
            $sql .= " and u.tipo != 'super' ";
        }


        /* $sql . = " and u.tipo != 'empresa_admin' and u.tipo != 'super' "; */
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    public function listAdminsEmpresa() {

        //$this->setConn();

        $sql = "select u.id, u.username\n"
                . " from `Usuario` as u\n"
                . " inner join `Empresa_has_Usuario` as fu on u.id = fu.Usuario_id\n"
                . " inner join `Empresa` as e on fu.Empresa_id = e.id\n"
                . " where u.tipo = 'empresa_admin' and e.id = " . $this->id;

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    public function getAnalistas() {
        $idsAnalistas = array();
        $criteria = new CDbCriteria;
        $QryAnalistas = " SELECT u.id as UsuarioId FROM Usuario as u ";
        $QryAnalistas .= " INNER JOIN Empresa_has_Usuario   as ehu ON ehu.Usuario_id = u.id ";
        $QryAnalistas .= " INNER JOIN Empresa               as emp ON ehu.Empresa_id = emp.id ";
        $QryAnalistas .= " WHERE emp.id = 5 AND u.tipo_id = 4 AND u.habilitado ";

        foreach (Yii::app()->db->createCommand($QryAnalistas)->queryAll() as $r) {
            $idsAnalistas[] = $r['UsuarioId'];
        }

        $criteria->addInCondition('id', $idsAnalistas);

        return Usuario::model()->findAll($criteria);
    }

    public function listAnalistasEmpresa(/* $user */) {

        //$this->setConn();


        $sql = "select u.id, u.username\n"
                . "from `Usuario` as u\n"
                . "inner join `Empresa_has_Usuario` as eu on u.id = eu.Usuario_id\n"
                . "inner join `Empresa` as e on eu.Empresa_id = e.id\n"
                . "where u.tipo = 'analista_de_credito' or u.tipo = 'financeiro' and e.id =" . $this->id . "\n"
                . "\n"
                . "union\n"
                . "\n"
                . "select u.id, u.username\n"
                . "from `Usuario` as u\n"
                . "inner join `Filial_has_Usuario` as fu on u.id = fu.Usuario_id\n"
                . "inner join `Filial` as f on fu.Filial_id = f.id\n"
                . "inner join `Empresa` as e on f.Empresa_id = e.id\n"
                . "where u.tipo ='crediarista' or u.tipo ='financeiro' and e.id = " . $this->id;

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    public function listFiliais() {

        return Filial::model()->findAll("Empresa_id = " . $this->id . ' AND habilitado');
    }

    public function listGruposDeAnalistas() {

        return GrupoDeAnalistas::model()->findAll('Empresa_id = ' . $this->id);
    }

    public function totaisRecebimentos() {

        $filiais = $this->listFiliais();

        $arrPropostas = array();

        $arrTotais = array(
            'totalAReceber' => 0,
            'totalAtrasado' => 0,
            'totalAReceberHoje' => 0,
            'totalAReceberAmanha' => 0
        );

        foreach ($filiais as $filial) {

            $propostas = $filial->listPropostasFilial();

            for ($i = 0; $i < count($propostas); $i++) {

                /* if ( $propostas[$i][0]['attributes']['Status_Proposta_id'] == 2 )
                  {


                  $titulo = Titulo::model()->find( 'Proposta_id = ' . $propostas[$i][0]['attributes']['id'] );

                  if ( $titulo != NULL )
                  {

                  $parcelas = $titulo->listarParcelas();

                  foreach ( $parcelas as $parcela )
                  {

                  if ( $parcela->valor_atual < $parcela->valor )
                  {

                  $arrTotais['totalAReceber'] += ( $parcela->valor - $parcela->valor_atual );
                  $vencimento = strtotime( $parcela->vencimento );
                  $hoje         = strtotime( date('Y-m-d') );
                  $amanha       = strtotime( date( 'Y-m-d', strtotime( date('Y-m-d') . ' + 1 day') ) );

                  if ( $parcela->atrasada() )
                  {
                  $arrTotais['totalAtrasado'] += ( $parcela->valor - $parcela->valor_atual );
                  }

                  if ( $vencimento == $hoje )
                  {
                  $arrTotais['totalAReceberHoje'] += ( $parcela->valor - $parcela->valor_atual );
                  }

                  else if( $vencimento == $amanha )
                  {
                  $arrTotais['totalAReceberAmanha'] += ( $parcela->valor - $parcela->valor_atual );
                  }
                  }
                  }
                  }
                  } */
            }
        }

        return $arrTotais;
    }

    public function listEstoques($draw, $filiaisPost, $offset, $locais, $vende) {

        $recordsTotal = 0;
        $recordsFiltered = 0;
        $idsFiliais = array();
        $rows = array();
        $util = new Util;

        if ($filiaisPost !== "") {
            $criteriaFiliais = new CDbCriteria;
            $criteriaFiliais->addInCondition('id', $filiaisPost);

            $filiais = Filial::model()->findAll($criteriaFiliais);
        } else {
            $filiais = $this->listFiliais();
        }

        foreach ($filiais as $filial) {
            $idsFiliais[] = $filial->id;
        }

        $criteriaNoFilter = new CDbCriteria;
        $criteriaFilter = new CDbCriteria;

        $criteriaNoFilter->addInCondition('Filial_id', $idsFiliais);
        $criteriaNoFilter->addInCondition('habilitado', array(1));

        $recordsTotal = count(Estoque::model()->findAll($criteriaNoFilter));

        $criteriaFilter->addInCondition('t.Filial_id', $idsFiliais, 'AND');
        $criteriaFilter->addInCondition('t.habilitado', array(1), 'AND');

        if ($locais !== "") {
            $criteriaFilter->addInCondition('t.Local_id', $locais, 'AND');
        }

        if ($vende !== "") {
            $criteriaFilter->addInCondition('t.vende', $vende, 'AND');
        }


        $criteriaFilter->offset = $offset;
        $criteriaFilter->limit = 10;
        $criteriaFilter->with = array('local');
        $criteriaFilter->order = "local.nome ASC";

        $estoquesFiltro = Estoque::model()->findAll($criteriaFilter);

        if (count($estoquesFiltro) > 0) {

            foreach ($estoquesFiltro as $estoque) {
                $row = array(
                    'filial' => $estoque->filial->getConcat(),
                    'local' => $estoque->local->nome,
                    'vende' => ($estoque->vende ? 'Sim' : 'Não' ),
                );

                $rows[] = $row;
            }
        }

        return (array(
            "draw" => $draw,
            "recordsTotal" => count($estoquesFiltro),
            "recordsFiltered" => $recordsTotal,
            "data" => $rows,
            "idsFiliais" => $idsFiliais
        ));
    }

    public function analisarBadFinanceira($mes, $ano, $draw, $parceiro, $analista) {

        $util           = new Util;
        $confBads       = array();
        $rows           = array();
        $totalProducao  = 0;

        $nomesMeses     = $util->getMeses();

        $mesDePartida   = mktime(0, 0, 0, date($mes), 1, date($ano));

        /* Recuperado as propostas produzidas no mês e ano informados pelo usuário */
        $r = $this->propostasMes(
            array('mes' => $mes, 'ano' => $ano), array(2, 7), array(1), 1, $parceiro, $analista
        );

        /*
          $r[0]['QuantidadeDeParcelasMaxima'] Mês máximo de safra para o mês informado.
          Montando as configurações das Bads
         */
        if ( isset( $r[0]['QuantidadeDeParcelasMaxima'] ) ){

            for ( $i = 1; $i <= $r[0]['QuantidadeDeParcelasMaxima']; $i++ ){

                $confDataSafra = explode('/', date("m/Y", strtotime("+$i month", $mesDePartida)));

                $cb = array(
                    'periodo'       => array(
                        'mes'       => $confDataSafra[0],
                        'nomeMes'   => $nomesMeses[intval($confDataSafra[0]) - 1][1],
                        'ano'       => $confDataSafra[1]
                    ),
                    'indicativos'   => array(
                        'total'     => 0,
                        'pago'      => 0,
                        'inadim'    => 0
                    )
                );

                $confBads[] = $cb;
            }

            foreach ($r as $p){

                $proposta       = Proposta::model()->findByPk($p['PropostaId']);

                $totalProducao += $proposta->getValorFinal();

                for ($i = 0; $i < count($confBads); $i++){

                    $bad = $proposta->parcelaMes(array('ano' => $confBads[$i]['periodo']['ano'], 'mes' => $confBads[$i]['periodo']['mes']));

                    if ( !empty( $bad ) ){

                        foreach ($bad as $b){

                            $parcelaB1 = Parcela::model()->findByPk($b['ParcelaId']);

                            if ($parcelaB1 != NULL){

                                $confBads[$i]['indicativos']['total'] += $parcelaB1->valor;

                                if (( $parcelaB1->valor_atual >= $parcelaB1->valor)) {
                                    $confBads[$i]['indicativos']['pago'] += $parcelaB1->valor;
                                } else if ($parcelaB1->valor_atual == 0) {
                                    $confBads[$i]['indicativos']['inadim'] += $parcelaB1->valor;
                                }
                            }
                        }
                    }
                }
            }

            for ($y = 0; $y < count($confBads); $y++){

                $percentualPgto     = ( $confBads[$y]['indicativos']['pago'] * 100 ) / $confBads[$y]['indicativos']['total'];
                $percentualInadim   = ( $confBads[$y]['indicativos']['inadim'] * 100 ) / $confBads[$y]['indicativos']['total'];

                $row                = array(
                    'bad'           => $y + 1 . '°',
                    'periodo'       => $confBads[$y]['periodo']['nomeMes'] . ' de ' . $confBads[$y]['periodo']['ano'],
                    'total'         => number_format($confBads[$y]['indicativos']['total'], 2, ',', '.'),
                    'pago'          => '<div class="progress"><div class="progress-bar progress-bar-success" style="color:#000;width: ' . number_format($percentualPgto, 2) . '%">' . number_format($percentualPgto, 2, ',', '.') . '%</div></div>',
                    'inadim'        => '<div class="progress"><div class="progress-bar progress-bar-danger" style="color:#000;width: ' . number_format($percentualInadim, 2) . '%">' . number_format($percentualInadim, 2, ',', '.') . '%</div></div>',
                    'porcenPgto'    => number_format($percentualPgto, 2) . '%',
                    'porcenInadim'  => number_format($percentualInadim, 2) . '%',
                );

                $rows[] = $row;
                //$totalProducao += $confBads[$y]['indicativos']['total'];
            }
        }

        return array(
            "draw" => $draw,
            "recordsTotal" => count($rows),
            "recordsFiltered" => count($rows),
            "data" => $rows,
            "customReturn" => array(
                "labelSafra" => ": Safra de " . $nomesMeses[intval($mes) - 1][1] . " de " . $ano,
                "totalProducao" => number_format($totalProducao, 2, ',', '.'),
                "printerConfig" => array(
                    'mes' => $mes,
                    'ano' => $ano,
                    'parceiros' => $parceiro,
                    'analista' => $analista,
                    "labelSafra" => ": Safra de " . $nomesMeses[intval($mes) - 1][1] . " de " . $ano,
                )
            )
        );
    }

   public function getDadosBancarios()
   {
      $query = "SELECT   E.nome_fantasia AS empresa  , B.codigo AS codBanco  , B.nome AS nomeBanco   , "
              . "         DB.agencia                  , DB.numero             , DB.operacao           , "
              . "         DB.id                                                                         "
              . "FROM 		Dados_Bancarios             AS DB "
              . "INNER JOIN 	Empresa_has_Dados_Bancarios AS ED ON ED.habilitado AND ED.Dados_Bancarios_id   = DB.id "
              . "INNER JOIN 	Empresa                     AS E  ON  E.habilitado AND ED.Empresa_id           =  E.id "
              . "INNER JOIN 	Banco                       AS B  ON                   DB.Banco_id             =  B.id "
              . "WHERE DB.habilitado ";

      $resultado = Yii::app()->db->createCommand($query)->queryAll();

      return $resultado;
   }

    public static function model($className = __CLASS__) {

        return parent::model($className);
    }

    public function producaoCont( $de, $ate, $filiais )
    {
      $producaoFinanciado   = 0;
      $producaoFinFinal     = 0;
      $producaoSegFinal     = 0;
      $criteria             = new CDbCriteria;
      $criteria->with       = [
        'analiseDeCredito'  => [
          'alias'           => 'ac'
        ]
      ];

      $criteria->addBetweenCondition( 't.data_cadastro',        $de. ' 00:00:00', $ate. ' 23:59:59',                                                            'AND');
      $criteria->addInCondition(      't.Status_Proposta_id', [2,7],                                                                                          'AND');

      if( $filiais != NULL )
      {
        $criteria->addInCondition(      'ac.Filial_id',         $filiais,                                                                                     'AND');
      }

      foreach( Proposta::model()->findAll($criteria) as $proposta )
      {
          $producaoFinanciado += ($proposta->valor - $proposta->valor_entrada);
          $producaoFinFinal   += $proposta->getValorFinal();
          $producaoSegFinal   += $proposta->calcularValorDoSeguro();
      }

      return [
        'producaoFinanciado'  => $producaoFinanciado,
        'producaoFinFinal'    => $producaoFinFinal,
        'producaoSegFinal'    => $producaoSegFinal,
      ];
    }
}
