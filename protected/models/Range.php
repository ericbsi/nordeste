<?php

class Range extends CActiveRecord {

    public function tableName() {
        return 'Range';
    }

    public function rules() {

        return array(
                array('prefixo, de, ate, ultimo_gerado, habilitado', 'numerical', 'integerOnly' => true),
                array('data_ultimo', 'safe'),
                array('id, habilitado, prefixo, de, ate, ultimo_gerado, data_ultimo', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {

        return array(
                'rangesGeradoses' => array(self::HAS_MANY, 'RangesGerados', 'Range_id'),
        );
    }

    public function attributeLabels() {
        return array(
                'id' => 'ID',
                'prefixo' => 'Prefixo',
                'de' => 'De',
                'ate' => 'Ate',
                'ultimo_gerado' => 'Ultimo Gerado',
                'data_ultimo' => 'Data Ultimo',
                'habilitado' => 'Habilitado',
        );
    }

    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('prefixo', $this->prefixo);
        $criteria->compare('de', $this->de);
        $criteria->compare('ate', $this->ate);
        $criteria->compare('ultimo_gerado', $this->ultimo_gerado);
        $criteria->compare('data_ultimo', $this->data_ultimo, true);
        $criteria->compare('habilitado', $this->habilitado);

        return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__) {

        return parent::model($className);
    }

    //Loja virtual
    public function boletoECommerce($venda) {

        $util = new Util;
        $arrRetorno = array();
        $cnabConfig = new CampoCnabConfig;
        $financeira = Financeira::model()->findByPk(5);
        $venda->titulos_gerados = 1;
        $titulo = Titulo::model()->find('VendaW_id 		= ' . $venda->id);
        $parcelas = Parcela::model()->findAll('Titulo_id 	= ' . $titulo->id);

        foreach ($parcelas as $parcela) {

            $duplicado = 1;

            do {
                $zeros = "";
                $range = Range::model()->find('habilitado');
                $range->ultimo_gerado = $range->ultimo_gerado + 1;

                for ($y = 1; $y <= 5 - strlen((string) $range->ultimo_gerado); $y ++) {
                    $zeros .= "0";
                }

                $rangeSeq = strval($range->ultimo_gerado);
                $rangeBoleto = $zeros . $rangeSeq;

                $rangesGerados = new RangesGerados;
                $rangesGerados->numero = $rangeBoleto;
                $rangesGerados->data_geracao = date('Y-m-d H:i:s');
                $rangesGerados->Range_id = $range->id;
                $rangesGerados->Parcela_id = $parcela->id;
                $rangesGerados->label = strval($range->prefixo) . $zeros . $rangeSeq;

                $rangesGerados->data_cadastro   = date('Y-m-d H:i:s');
                $rangesGerados->habilitado      = 1;

                try {

                    if ($rangesGerados->save()) {

                        $r = array(
                                'codigobanco' => $financeira->getDadosBancarios()->banco->codigo,
                                'cedente' => $financeira->nome,
                                'cpf_cnpj' => $financeira->cnpj,
                                'agencia' => $financeira->getDadosBancarios()->agencia,
                                'valor_boleto' => number_format($parcela->valor, 2, ',', ''),
                                'sacado' => $titulo->vendaW->cliente->pessoa->nome,
                                'cpfsacado' => $titulo->vendaW->cliente->pessoa->getCpf()->numero,
                                'endereco1' => $titulo->vendaW->cliente->pessoa->getEnderecoCobranca()->logradouro . ', ' . $titulo->vendaW->cliente->pessoa->getEnderecoCobranca()->numero . ' ' . $titulo->vendaW->cliente->pessoa->getEnderecoCobranca()->complemento,
                                'bairro' => $titulo->vendaW->cliente->pessoa->getEnderecoCobranca()->bairro,
                                'uf' => $titulo->vendaW->cliente->pessoa->getEnderecoCobranca()->uf,
                                'endereco2' => $titulo->vendaW->cliente->pessoa->getEnderecoCobranca()->cep . ' ' . $titulo->vendaW->cliente->pessoa->getEnderecoCobranca()->cidade,
                                'vencimento' => $parcela->vencimento,
                                'range' => strval($range->prefixo) . $zeros . $rangeSeq,
                                'especie_doc' => 'PD',
                                'especie' => 'REAL',
                                'aceite' => 'NÃO',
                                'quantidade' => '1',
                                'instrucoes1' => '- Após o vencimento, multa de 0,29% ao dia.',
                                'instrucoes2' => '',
                                'instrucoes3' => '- Em caso de dúvidas entre em contato conosco: contato@amaremansa.com.br',
                                'instrucoes4' => '- Boleto relativo ao pagamento da ' . $parcela->seq . '° parcela',
                                'instrucoes5' => $parcela->seq . '/' . count($parcelas),
                                'valor_corri' => $parcela->valor
                        );

                        array_push($arrRetorno, $r);

                        $hsbc = new Hsbc('2014-05-05', "0.00", strval($range->prefixo) . $zeros . $rangeSeq, "0738", substr("07380072898", 5));

                        $cnabDetalhe = new CnabDetalhe;
                        $cnabDetalhe->cod_registro = '1'; //OK
                        $cnabDetalhe->cod_inscricao = '02'; //OK
                        $cnabDetalhe->num_inscricao = '07221618000120'; //OK
                        $cnabDetalhe->zero = '0'; //OK
                        $cnabDetalhe->agencia_cedente = $financeira->getDadosBancarios()->agencia; //OK
                        $cnabDetalhe->sub_conta = '55'; //OK
                        $cnabDetalhe->conta_corrente = $financeira->getDadosBancarios()->numero; //OK
                        $cnabDetalhe->controle_participante = $cnabConfig->cnabValorMonetario(strval($range->prefixo) . $zeros . $rangeSeq, 25); //OK
                        $cnabDetalhe->nosso_numero = $hsbc->getNossoNumero(); //OK
                        $cnabDetalhe->carteira = '1'; //OK
                        $cnabDetalhe->cod_ocorrencia = '01'; //OK
                        $cnabDetalhe->vencimento = $parcela->vencimento; //OK
                        $cnabDetalhe->seu_numero = $cnabConfig->cnabValorMonetario(strval($range->prefixo) . $zeros . $rangeSeq, 10); //OK
                        $cnabDetalhe->vencimento_texto = $cnabConfig->cnabDate($parcela->vencimento); //OK
                        $cnabDetalhe->valor_do_titulo_texto = $cnabConfig->cnabValorMonetario($parcela->valor_texto, 13);
                        $cnabDetalhe->valor_do_titulo = $parcela->valor_texto;
                        $cnabDetalhe->banco_cobrador = '399'; //OK
                        $cnabDetalhe->agencia_depositaria = $cnabConfig->cnabValorMonetario("", 5); //OK
                        $cnabDetalhe->especie = '98'; //OK
                        $cnabDetalhe->aceite = 'N'; //OK
                        $cnabDetalhe->data_emissao_texto = date('dmy');
                        $cnabDetalhe->instrucao01 = '00'; //OK
                        $cnabDetalhe->instrucao02 = '23'; //OK
                        $cnabDetalhe->juros_de_mora_texto = $cnabConfig->cnabValorMonetario("", 13);
                        $cnabDetalhe->desconto_data_texto = $cnabConfig->cnabValorMonetario("", 6);
                        $cnabDetalhe->valor_desconto_texto = $cnabConfig->cnabValorMonetario("", 13);
                        $cnabDetalhe->valor_iof_texto = $cnabConfig->cnabValorMonetario("", 13); //parei aqui
                        $cnabDetalhe->valor_abatimento_texto = $cnabConfig->getValorAbatimento($parcela->vencimento, "2.00"); //OK
                        $cnabDetalhe->codigo_de_inscricao = '01';
                        $cnabDetalhe->numero_de_inscricao = '000' . $venda->cliente->pessoa->getCPF()->numero;
                        $cnabDetalhe->nome_do_sacado = substr($util->cleanStr($venda->cliente->pessoa->nome, 'upp'), 0, 35);
                        $cnabDetalhe->endereco_do_sacado = substr($util->cleanStr($venda->cliente->pessoa->getEnderecoCobranca()->logradouro, 'upp'), 0, 30) . ' ' . $venda->cliente->pessoa->getEnderecoCobranca()->numero;
                        $cnabDetalhe->instrucao_de_nao_recebimento_do_bloqueto = '';
                        $cnabDetalhe->bairro = $util->cleanStr($venda->cliente->pessoa->getEnderecoCobranca()->bairro, 'upp');
                        $cnabDetalhe->cep_do_sacado = substr($venda->cliente->pessoa->getEnderecoCobranca()->cep, 0, 5);
                        $cnabDetalhe->sufixo_do_cep = substr($venda->cliente->pessoa->getEnderecoCobranca()->cep, 5);
                        $cnabDetalhe->cidade_do_sacado = $util->cleanStr($venda->cliente->pessoa->getEnderecoCobranca()->cidade, 'upp');
                        $cnabDetalhe->sigla_da_uf = $venda->cliente->pessoa->getEnderecoCobranca()->uf;
                        $cnabDetalhe->moeda = '9';
                        $cnabDetalhe->numero_sequencial = $cnabConfig->getCodigoSequencial('detalhe', date('dmy'), null);
                        $cnabDetalhe->Banco_id = $financeira->getDadosBancarios()->Banco_id;
                        $cnabDetalhe->Filial_id = 19;
                        $cnabDetalhe->juros_de_mora = $cnabConfig->cnabValorMonetario("", 13);
                        $cnabDetalhe->data_emissao = date('Y-m-d');
                        $cnabDetalhe->transmitido = 0;
                        $cnabDetalhe->habilitado = 1;
                        $cnabDetalhe->save();

                        $venda->update();
                        $duplicado = 0;
                    }
                } catch (CDbException $e) {

                }

                $range->update();
            } while ($duplicado);
        }

        return $arrRetorno;
    }

    /* Função que retorna os ranges a serem usados em uma analise aprovada. */

    public function getRanges($objProposta) {

        $util = new Util;
        $arrRetorno = array();

        $titulo = new Titulo;
        $titulo->prefixo = '001';
        $titulo->NaturezaTitulo_id = 1;
        $titulo->Proposta_id = $objProposta->id;
        $mora = 0.29 / 100;

        $zeros = "";

        if ($titulo->save()) {

            $objProposta->titulos_gerados = 1;
            $objProposta->Banco_id = 5;

            if ($objProposta->update()) {
                Titulo::model()->gerarRepasse($objProposta);
            }

            for ($i = 0; $i < $objProposta->qtd_parcelas; $i++) {

                $range = Range::model()->find('habilitado');
                $range->ultimo_gerado = $range->ultimo_gerado + 1;

                for ($y = 1; $y <= 5 - strlen((string) $range->ultimo_gerado); $y ++) {
                    $zeros .= "0";
                }

                $parcela = new Parcela;
                $parcela->seq = $i + 1;
                $parcela->valor = $objProposta->getValorParcela();
                //$parcela->valor 				= $objProposta->valor_parcela;
                $parcela->valor_texto = $objProposta->valor_parcela_texto;
                $parcela->valor_atual = 0;
                $parcela->Titulo_id = $titulo->id;

                if ($i == 0) {
                    $parcela->vencimento = $util->view_date_to_bd($objProposta->getDataPrimeiraParcela());
                } else {
                    $parcela->vencimento = $util->adicionarMesMantendoDia($util->view_date_to_bd($objProposta->getDataPrimeiraParcela()), $i)->format('Y-m-d');
                }

                $parcela->save();

                $rangeSeq = strval($range->ultimo_gerado);
                $rangeBoleto = $zeros . $rangeSeq;

                $rangesGerados = new RangesGerados;
                $rangesGerados->numero = $rangeBoleto;
                $rangesGerados->data_geracao = date('Y-m-d H:i:s');
                $rangesGerados->Range_id = $range->id;
                $rangesGerados->Parcela_id = $parcela->id;
                $rangesGerados->label = strval($range->prefixo) . $zeros . $rangeSeq;

                $rangesGerados->data_cadastro   = date('Y-m-d H:i:s');
                $rangesGerados->habilitado      = 1;

                if ($rangesGerados->save()) {
                    $r = array(
                            'codigobanco' => $objProposta->financeira->getDadosBancarios()->banco->codigo,
                            'cedente' => $objProposta->financeira->nome,
                            'cpf_cnpj' => $objProposta->financeira->cnpj,
                            'agencia' => $objProposta->financeira->getDadosBancarios()->agencia,
                            //'valor_boleto' 	=> number_format($objProposta->valor_parcela, 2, ',', ''),
                            'valor_boleto' => number_format($objProposta->getValorParcela(), 2, ',', ''),
                            'sacado' => $objProposta->analiseDeCredito->cliente->pessoa->nome,
                            'cpfsacado' => $objProposta->analiseDeCredito->cliente->pessoa->getCpf()->numero,
                            'endereco1' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->logradouro . ', ' . $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->numero . ' ' . $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->complemento,
                            'bairro' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->bairro,
                            'uf' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->uf,
                            'endereco2' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cep . ' ' . $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cidade,
                            'vencimento' => $parcela->vencimento,
                            'range' => strval($range->prefixo) . $zeros . $rangeSeq,
                            'especie_doc' => 'PD',
                            'especie' => 'REAL',
                            'aceite' => 'NÃO',
                            'quantidade' => '1',
                            'instrucoes1' => '- Após o vencimento, mora de 0,29% ao dia.',
                            'instrucoes2' => '',
                            'instrucoes3' => '- Em caso de dúvidas entre em contato conosco: contato@amaremansa.com.br',
                            'instrucoes4' => '- Boleto relativo ao pagamento da ' . $parcela->seq . '° parcela da proposta ' . $objProposta->codigo,
                            'instrucoes5' => $parcela->seq . '/' . $objProposta->qtd_parcelas,
                            'seq_parc' => $parcela->seq,
                            //'valor_corri'   => $objProposta->valor_parcela
                            'valor_corri' => $objProposta->getValorParcela()
                    );

                    array_push($arrRetorno, $r);

                    $hsbc = new Hsbc('2014-05-05', "0.00", strval($range->prefixo) . $zeros . $rangeSeq, "0738", substr("07380072898", 5));
                    $cnabConfig = new CampoCnabConfig;

                    $cnabDetalhe = new CnabDetalhe;
                    $cnabDetalhe->cod_registro = '1'; //OK
                    $cnabDetalhe->cod_inscricao = '02'; //OK
                    $cnabDetalhe->num_inscricao = str_replace(array(',', '.', '/', '-'), array('', '', '', ''), $objProposta->financeira->cnpj); //OK
                    $cnabDetalhe->zero = '0'; //OK
                    $cnabDetalhe->agencia_cedente = $objProposta->financeira->getDadosBancarios()->agencia; //OK
                    $cnabDetalhe->sub_conta = '55'; //OK
                    $cnabDetalhe->conta_corrente = $objProposta->financeira->getDadosBancarios()->numero; //OK
                    $cnabDetalhe->controle_participante = $cnabConfig->cnabValorMonetario(strval($range->prefixo) . $zeros . $rangeSeq, 25); //OK
                    $cnabDetalhe->nosso_numero = $hsbc->getNossoNumero(); //OK
                    $cnabDetalhe->carteira = '1'; //OK
                    $cnabDetalhe->cod_ocorrencia = '01'; //OK
                    $cnabDetalhe->seu_numero = $cnabConfig->cnabValorMonetario(strval($range->prefixo) . $zeros . $rangeSeq, 10); //OK
                    $cnabDetalhe->vencimento = $parcela->vencimento; //OK
                    $cnabDetalhe->vencimento_texto = $cnabConfig->cnabDate($parcela->vencimento); //OK
                    //$cnabDetalhe->valor_do_titulo_texto 					= $cnabConfig->cnabValorMonetario($objProposta->valor_parcela_texto,13);
                    $cnabDetalhe->valor_do_titulo_texto = $cnabConfig->cnabValorMonetario($objProposta->getValorParcela(), 13);
                    $cnabDetalhe->valor_do_titulo = $objProposta->getValorParcela();
                    $cnabDetalhe->banco_cobrador = '399'; //OK
                    $cnabDetalhe->agencia_depositaria = '00000'; //OK
                    $cnabDetalhe->especie = '98'; //OK
                    $cnabDetalhe->aceite = 'N'; //OK
                    $cnabDetalhe->data_emissao_texto = date('dmy');
                    $cnabDetalhe->instrucao01 = '00'; //OK
                    $cnabDetalhe->instrucao02 = '23'; //OK
                    //$cnabDetalhe->juros_de_mora_texto 					= $cnabConfig->cnabValorMonetario( $util->round_up( ( $parcela->valor * $mora ), 2 ) ,13 );
                    $cnabDetalhe->juros_de_mora_texto = $cnabConfig->cnabValorMonetario($util->round_up(( $objProposta->getValorParcela() * $mora), 2), 13);
                    $cnabDetalhe->desconto_data_texto = '000000';
                    $cnabDetalhe->valor_desconto_texto = $cnabConfig->cnabValorMonetario("", 13);
                    $cnabDetalhe->valor_iof_texto = $cnabConfig->cnabValorMonetario("", 13);
                    $cnabDetalhe->valor_abatimento_texto = $cnabConfig->getValorAbatimento($parcela->vencimento, "0.00"); //OK
                    $cnabDetalhe->codigo_de_inscricao = '01';
                    $cnabDetalhe->numero_de_inscricao = '000' . $objProposta->analiseDeCredito->cliente->pessoa->getCPF()->numero;
                    $cnabDetalhe->nome_do_sacado = substr($util->cleanStr($objProposta->analiseDeCredito->cliente->pessoa->nome, 'upp'), 0, 35);
                    $cnabDetalhe->endereco_do_sacado = strtoupper(substr($util->cleanStr($objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->logradouro, 'upp'), 0, 30) . ' ' . $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->numero);
                    $cnabDetalhe->instrucao_de_nao_recebimento_do_bloqueto = '';
                    $cnabDetalhe->bairro = $util->cleanStr($objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->bairro, 'upp');
                    $cnabDetalhe->cep_do_sacado = substr($objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cep, 0, 5);
                    $cnabDetalhe->sufixo_do_cep = substr($objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cep, 5);
                    $cnabDetalhe->cidade_do_sacado = $util->cleanStr($objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cidade, 'upp');
                    $cnabDetalhe->sigla_da_uf = $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->uf;
                    $cnabDetalhe->moeda = '9';
                    $cnabDetalhe->numero_sequencial = $cnabConfig->getCodigoSequencial('detalhe', date('dmy'), null);
                    $cnabDetalhe->Banco_id = $objProposta->financeira->getDadosBancarios()->Banco_id;
                    $cnabDetalhe->Filial_id = $objProposta->analiseDeCredito->Filial_id;
                    $cnabDetalhe->juros_de_mora = $cnabConfig->cnabValorMonetario("", 13);
                    $cnabDetalhe->data_emissao = date('Y-m-d');
                    $cnabDetalhe->transmitido = 0;
                    $cnabDetalhe->habilitado = 1;
                    $cnabDetalhe->save();

                    $range->update();
                    $zeros = "";
                }
            }
        }

        return $arrRetorno;
    }

    public function gerarTitulosSantander($objProposta) {

        $util = new Util;
        $arrRetorno = array();

        $titulo = new Titulo;
        $titulo->prefixo = '001';
        $titulo->NaturezaTitulo_id = 1;
        $titulo->Proposta_id = $objProposta->id;
        $mora = 0.29 / 100;

        $zeros = "";

        if ($titulo->save()) {

            $objProposta->titulos_gerados = 1;
            $objProposta->Banco_id = 7;

            if ($objProposta->update()) {
                Titulo::model()->gerarRepasse($objProposta);
            }

            for ($i = 0; $i < $objProposta->qtd_parcelas; $i++) {

                $range = Range::model()->findByPk(3);
                $range->ultimo_gerado = $range->ultimo_gerado + 1;

                for ($y = 1; $y <= 5 - strlen((string) $range->ultimo_gerado); $y ++) {
                    $zeros .= "0";
                }

                $parcela = new Parcela;
                $parcela->seq = $i + 1;
                $parcela->valor = $objProposta->getValorParcela();
                $parcela->valor_texto = $objProposta->valor_parcela_texto;
                $parcela->valor_atual = 0;
                $parcela->Titulo_id = $titulo->id;

                if ($i == 0) {
                    $parcela->vencimento = $util->view_date_to_bd($objProposta->getDataPrimeiraParcela());
                } else {
                    $parcela->vencimento = $util->adicionarMesMantendoDia($util->view_date_to_bd($objProposta->getDataPrimeiraParcela()), $i)->format('Y-m-d');
                }

                $parcela->save();

                $rangeSeq = strval($range->ultimo_gerado);
                $rangeBoleto = $zeros . $rangeSeq;

                $rangesGerados = new RangesGerados;
                $rangesGerados->numero = $rangeBoleto;
                $rangesGerados->data_geracao = date('Y-m-d H:i:s');
                $rangesGerados->Range_id = $range->id;
                $rangesGerados->Parcela_id = $parcela->id;
                $rangesGerados->label = strval($range->prefixo) . $zeros . $rangeSeq;

                $rangesGerados->data_cadastro   = date('Y-m-d H:i:s');
                $rangesGerados->habilitado      = 1;

                if ($rangesGerados->save()) {
                    $r = array(
                            'codigobanco' => '033',
                            'cedente' => 'Nova Cruz Comércio de M. Ltda',
                            'cpf_cnpj' => $objProposta->financeira->cnpj,
                            'agencia' => '3211',
                            'valor_boleto' => number_format($objProposta->getValorParcela(), 2, ',', ''),
                            'sacado' => $objProposta->analiseDeCredito->cliente->pessoa->nome,
                            'cpfsacado' => $objProposta->analiseDeCredito->cliente->pessoa->getCpf()->numero,
                            'endereco1' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->logradouro . ', ' . $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->numero . ' ' . $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->complemento,
                            'bairro' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->bairro,
                            'uf' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->uf,
                            'endereco2' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cep . ' ' . $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cidade,
                            'vencimento' => $parcela->vencimento,
                            'range' => strval($range->prefixo) . $zeros . $rangeSeq,
                            'especie_doc' => 'PD',
                            'especie' => 'REAL',
                            'aceite' => 'NÃO',
                            'quantidade' => '1',
                            'instrucoes1' => '- Após o vencimento, mora de 0,29% ao dia.',
                            'instrucoes2' => '',
                            'instrucoes3' => '- Em caso de dúvidas entre em contato conosco: contato@amaremansa.com.br',
                            'instrucoes4' => '- Boleto relativo ao pagamento da ' . $parcela->seq . '° parcela da proposta ' . $objProposta->codigo,
                            'instrucoes5' => $parcela->seq . '/' . $objProposta->qtd_parcelas,
                            'seq_parc' => $parcela->seq,
                            'valor_corri' => $objProposta->getValorParcela()
                    );

                    array_push($arrRetorno, $r);

                    $santander = new Santander('2014-05-05', "0.00", strval($range->prefixo) . $zeros . $rangeSeq, "3211", substr("0130026432", 5));
                    $cnabConfig = new CampoCnabConfig;

                    $cnabDetalhe = new CnabDetalhe;
                    $cnabDetalhe->cod_registro = '1'; //OK
                    $cnabDetalhe->cod_inscricao = '02'; //OK
                    $cnabDetalhe->num_inscricao = '07221618000120'; //OK
                    $cnabDetalhe->codigo_agencia_beneficiario_sant = '3211'; //OK
                    $cnabDetalhe->conta_movimento_beneficiario_sant = '06978738'; //OK
                    $cnabDetalhe->conta_cobranca_beneficiario_sant = '01300264'; //OK
                    $cnabDetalhe->controle_participante = $cnabConfig->cnabValorMonetario(strval($range->prefixo) . $zeros . $rangeSeq, 25); //OK
                    $cnabDetalhe->nosso_numero_sant = $santander->getNossoNumero();
                    $cnabDetalhe->data_do_segundo_desconto_sant = '000000';
                    $cnabDetalhe->informacao_de_multa_sant = '0';
                    $cnabDetalhe->percentual_multa_por_atraso_sant = '0000';
                    $cnabDetalhe->unidade_de_valor_moeda_corrente_sant = '00';
                    $cnabDetalhe->valor_do_titulo_em_outra_unidade_sant = '0000000000000';
                    $cnabDetalhe->data_para_cobranca_de_multa_sant = '000000';
                    $cnabDetalhe->carteira = '5'; //OK
                    $cnabDetalhe->cod_ocorrencia = '01'; //OK
                    $cnabDetalhe->seu_numero = $cnabConfig->cnabValorMonetario(strval($range->prefixo) . $zeros . $rangeSeq, 10); //OK
                    $cnabDetalhe->vencimento = $parcela->vencimento; //OK
                    $cnabDetalhe->vencimento_texto = $cnabConfig->cnabDate($parcela->vencimento); //OK
                    $cnabDetalhe->valor_do_titulo_texto = $cnabConfig->cnabValorMonetario($parcela->valor_texto, 13);
                    //$cnabDetalhe->valor_do_titulo_texto 					= $cnabConfig->cnabValorMonetario($objProposta->getValorParcela(),13);
                    $cnabDetalhe->valor_do_titulo = $objProposta->getValorParcela();
                    $cnabDetalhe->banco_cobrador = '033'; //OK
                    $cnabDetalhe->agencia_depositaria = '32115';
                    $cnabDetalhe->especie = '01'; //OK
                    $cnabDetalhe->aceite = 'N'; //OK
                    $cnabDetalhe->data_emissao = date('Y-m-d');
                    $cnabDetalhe->data_emissao_texto = date('dmy');
                    $cnabDetalhe->instrucao01 = '00'; //OK
                    $cnabDetalhe->instrucao02 = '00'; //OK
                    $cnabDetalhe->juros_de_mora_texto = $cnabConfig->cnabValorMonetario($util->round_up(( $parcela->valor * $mora), 2), 13);
                    $cnabDetalhe->desconto_data_texto = $cnabConfig->cnabValorMonetario("", 6);
                    $cnabDetalhe->valor_desconto_texto = $cnabConfig->cnabValorMonetario("", 13);
                    $cnabDetalhe->valor_iof_texto = $cnabConfig->cnabValorMonetario("", 13);
                    $cnabDetalhe->valor_abatimento_texto = '0000000000000';
                    $cnabDetalhe->codigo_de_inscricao = '01';
                    $cnabDetalhe->numero_de_inscricao = '000' . $objProposta->analiseDeCredito->cliente->pessoa->getCPF()->numero;
                    $cnabDetalhe->nome_do_sacado = substr($util->cleanStr($objProposta->analiseDeCredito->cliente->pessoa->nome, 'upp'), 0, 35);
                    $cnabDetalhe->endereco_do_pagador_sant = substr($util->cleanStr($objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->logradouro, 'upp'), 0, 30) . ' ' . $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->numero;
                    $cnabDetalhe->bairro = $util->cleanStr($objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->bairro, 'upp');
                    $cnabDetalhe->cep_do_sacado = substr($objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cep, 0, 5);
                    $cnabDetalhe->sufixo_do_cep = substr($objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cep, 5);
                    $cnabDetalhe->cidade_do_sacado = $util->cleanStr($objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cidade, 'upp');
                    $cnabDetalhe->sigla_da_uf = $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->uf;
                    $cnabDetalhe->nome_do_sacador_ou_coobrigado_sant = '';
                    $cnabDetalhe->identificador_do_complemento_sant = 'I';
                    $cnabDetalhe->complemento_sant = '32';
                    $cnabDetalhe->prazo_de_protesto = '00';
                    //$cnabDetalhe->numero_sequencial 						= $cnabConfig->getCodigoSequencial('detalhe',date('dmy'), null);
                    $cnabDetalhe->numero_sequencial = '000000';
                    $cnabDetalhe->Banco_id = 7;
                    $cnabDetalhe->save();

                    $range->update();
                    $zeros = "";
                }
            }
        }

        return $arrRetorno;
    }

    /* Gerar arquivo cnab400 remessa */

    public function gerarRemessa($financeira) {

        ini_set('max_execution_time', 1600000);

        $util = new Util;
        $dataGeracaoCnab = date('260215');
        $nSpacesAdd = 0;

        $dirRemessa = 'remessas/' . $dataGeracaoCnab;
        $arquivoRemessa = $dirRemessa . '/' . $util->getCodeVenda("0123456789", 8) . '.txt';
        $dadosBancarios = $financeira->getDadosBancarios();

        /* Carrega o Header base do banco */
        $cnabHeaderBase = CnabHeader::model()->find('Banco_id = ' . $dadosBancarios->Banco_id . ' AND base');

        /* Cria o arquivo */
        if (!is_dir($dirRemessa)) {

            $oldUmask = umask(0);
            mkdir($dirRemessa, 0777, true);
            umask($oldUmask);

            if (!file_exists($arquivoRemessa)) {
                $handle = fopen($arquivoRemessa, 'a+');

                $arquivoCnab = new ArquivoCnab;
                $arquivoCnab->relative_path = $arquivoRemessa;
                $arquivoCnab->absolute_path = '/var/www/sigac/' . $arquivoRemessa;
                $arquivoCnab->tipo = 1;
                $arquivoCnab->data_geracao = date('Y-m-d');
                $arquivoCnab->data_geracao_texto = date('dmY');
                $arquivoCnab->Usuario_id = Yii::app()->session['usuario']->id;
                $arquivoCnab->Filial_id = 19;
                $arquivoCnab->save();

                $cnabGeradosData = new CnabGeradosData;
                $cnabGeradosData->data_geracao = date('Y-m-d');
                $cnabGeradosData->data_texto = date('dmy');
                $cnabGeradosData->save();
            }
        }

        /* Cria novo registro header */
        $cnabHeader = new CnabHeader;
        $cnabHeader->setAttributes($cnabHeaderBase->attributes);
        $cnabHeader->setAttribute('agencia_cedente', $dadosBancarios->agencia);
        $cnabHeader->setAttribute('conta_corrente', $dadosBancarios->numero);
        $cnabHeader->setAttribute('nome_do_cliente', 'NOVA CRUZ COMERCIO DE MOVEIS L');
        $cnabHeader->setAttribute('data_gravacao', date('Y-m-d'));
        $cnabHeader->setAttribute('data_gravacao_texto', $dataGeracaoCnab);

        if ($cnabHeader->save()) {

            $linhaHeader = "";

            foreach ($cnabHeader->attributes as $attr => $value) {

                if (in_array($attr, CampoCnabConfig::model()->getCampos('header', 1))) {

                    $campoCnabConfig = CampoCnabConfig::model()->find('tipo != 1 AND setor = "header" AND sigla =' . "'$attr'");

                    $nSpacesAdd = ( $campoCnabConfig->posi_fim - $campoCnabConfig->posi_ini ) + 1;

                    for ($i = 0; $i < $nSpacesAdd; $i++) {
                        $linhaHeader .= str_replace("\xc2\xa0", ' ', html_entity_decode("&nbsp;"));
                    }

                    $linhaHeader = substr_replace($linhaHeader, $value, $campoCnabConfig->posi_ini - 1, 1);
                }
            }
        }

        $nSpacesAdd = 0;

        fwrite($handle, substr($linhaHeader, 0, 400) . PHP_EOL);

        /* Encontrando os detalhes do dia.... */
        $criteriaDetalhes = new CDbCriteria;
        $criteriaDetalhes->addInCondition('transmitido', array(0), ' AND ');
        $criteriaDetalhes->addInCondition('habilitado', array(1), ' AND ');
        $criteriaDetalhes->addBetweenCondition('data_emissao', date('2015-02-26'), date('2015-02-26'));

        $cnabDetalheDia = CnabDetalhe::model()->findAll($criteriaDetalhes);

        $countDetalhes = 1;

        foreach ($cnabDetalheDia as $detalhe) {

            $countDetalhes += 1;

            $linhaDetalhe = "";
            $detalhe->Cnab_header_id = $cnabHeader->id;
            $detalhe->transmitido = 1;
            $detalhe->numero_sequencial = CampoCnabConfig::model()->getCodigoSequencial('detalhe', $dataGeracaoCnab, $countDetalhes);

            $detalhe->update();

            foreach ($detalhe->attributes as $cattr => $valor) {

                if (in_array($cattr, CampoCnabConfig::model()->getCampos('detalhe', 1))) {

                    $campoCnabConfig = CampoCnabConfig::model()->find('tipo != 1 AND setor = "detalhe" AND sigla =' . "'$cattr'");

                    $nSpacesAdd = ( $campoCnabConfig->posi_fim - $campoCnabConfig->posi_ini ) + 1;

                    for ($i = 0; $i < $nSpacesAdd; $i++) {
                        $linhaDetalhe .= str_replace("\xc2\xa0", ' ', html_entity_decode("&nbsp;"));
                    }

                    $linhaDetalhe = substr_replace($linhaDetalhe, $valor, $campoCnabConfig->posi_ini - 1, 1);
                }
            }

            fwrite($handle, substr($linhaDetalhe, 0, 400) . PHP_EOL);
        }

        /* Gravando o trailer */
        $linhaTrailer = "";
        $cnabTrailer = new CnabTrailer;
        $nSpacesAdd = 0;

        $cnabTrailer->cod_registro = '9';
        $cnabTrailer->banco = '';
        $cnabTrailer->numero_sequencial = CampoCnabConfig::model()->getCodigoSequencial('detalhe', $dataGeracaoCnab, null);
        $cnabTrailer->Banco_id = $dadosBancarios->Banco_id;
        $cnabTrailer->Cnab_header_id = $cnabHeader->id;

        if ($cnabTrailer->save()) {

            foreach ($cnabTrailer->attributes as $atributo => $v) {

                if (in_array($atributo, CampoCnabConfig::model()->getCampos('trailer', 1))) {

                    $campoCnabConfig = CampoCnabConfig::model()->find('tipo != 1 AND setor = "trailer" AND sigla =' . "'$atributo'");
                    $nSpacesAdd = ( $campoCnabConfig->posi_fim - $campoCnabConfig->posi_ini ) + 1;

                    for ($i = 0; $i < $nSpacesAdd; $i++) {
                        $linhaTrailer .= str_replace("\xc2\xa0", ' ', html_entity_decode("&nbsp;"));
                    }

                    $linhaTrailer = substr_replace($linhaTrailer, $v, $campoCnabConfig->posi_ini - 1, 1);
                }
            }

            fwrite($handle, substr($linhaTrailer, 0, 400) . PHP_EOL);
        }

        fclose($handle);

        $cnabGeradosData->data_geracao = date('Y-m-d');
        $cnabGeradosData->data_texto = date('dmy');
        $cnabGeradosData->save();
    }

    /* Gerando segunda via do título */

    public function getSegundaVia($ParcelasIds, $objProposta) {

        $arrRetorno = array();
        $hoje = date('Y-m-d');

        if (count($ParcelasIds) > 0) {
            for ($i = 0; $i < count($ParcelasIds); $i++) {

                $mora = null;
                $parcela = Parcela::model()->findByPk($ParcelasIds[$i]);
                $rangeGerado = RangesGerados::model()->find('Parcela_id = ' . $ParcelasIds[$i]);

                $r = array(
                        'codigobanco' => $objProposta->financeira->getDadosBancarios()->banco->codigo,
                        'cedente' => $objProposta->financeira->nome,
                        'cpf_cnpj' => $objProposta->financeira->cnpj,
                        'agencia' => $objProposta->financeira->getDadosBancarios()->agencia,
                        //'valor_boleto' 	=> number_format($objProposta->valor_parcela, 2, ',', ''),
                        'valor_boleto' => number_format($objProposta->getValorParcela(), 2, ',', ''),
                        'sacado' => $objProposta->analiseDeCredito->cliente->pessoa->nome,
                        'cpfsacado' => $objProposta->analiseDeCredito->cliente->pessoa->getCpf()->numero,
                        'endereco1' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->logradouro . ', ' . $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->numero . ' ' . $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->complemento,
                        'bairro' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->bairro,
                        'uf' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->uf,
                        'endereco2' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cep . ' ' . $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cidade,
                        'vencimento' => $parcela->vencimento,
                        'range' => $rangeGerado->label,
                        'especie_doc' => 'PD',
                        'especie' => 'REAL',
                        'aceite' => 'NÃO',
                        'quantidade' => '1',
                        'instrucoes1' => '- Após o vencimento, mora de 0,29% ao dia.',
                        'instrucoes2' => '',
                        'instrucoes3' => '- Em caso de dúvidas entre em contato conosco: contato@amaremansa.com.br.',
                        'instrucoes4' => '- Boleto relativo ao pagamento da ' . $parcela->seq . '° parcela da proposta ' . $objProposta->codigo,
                        'instrucoes5' => $parcela->seq . '/' . $objProposta->qtd_parcelas,
                        'seq_parc' => $parcela->seq,
                        'mora' => $mora,
                        //'valor_corri'   => $objProposta->valor_parcela
                        'valor_corri' => $objProposta->getValorParcela()
                );

                if (strtotime($parcela->vencimento) < strtotime($hoje)) {
                    $mora = $parcela->calcMora();
                    $r['mora'] = $mora;
                    $r['vencimento'] = date('Y-m-d');
                    $r['valor_boleto'] = number_format(($objProposta->getValorParcela() + $parcela->calcMora()), 2, ',', '');
                    $r['valor_corri'] = $objProposta->getValorParcela() + $parcela->calcMora();
                }

                $arrRetorno[] = $r;
            }
        }

        return $arrRetorno;
    }

    public function getSegundaViaSantander($ParcelasIds, $objProposta) {
        $arrRetorno = array();
        $hoje = date('Y-m-d');

        if (count($ParcelasIds) > 0) {
            for ($i = 0; $i < count($ParcelasIds); $i++) {
                $mora = null;
                $parcela = Parcela::model()->findByPk($ParcelasIds[$i]);
                $rangeGerado = RangesGerados::model()->find('Parcela_id = ' . $ParcelasIds[$i]);

                $r = array(
                        'codigobanco' => '033',
                        'cedente' => 'Nova Cruz Comércio de M. Ltda',
                        'cpf_cnpj' => $objProposta->financeira->cnpj,
                        'agencia' => '3211',
                        'valor_boleto' => number_format($objProposta->getValorParcela(), 2, ',', ''),
                        'sacado' => $objProposta->analiseDeCredito->cliente->pessoa->nome,
                        'cpfsacado' => $objProposta->analiseDeCredito->cliente->pessoa->getCpf()->numero,
                        'endereco1' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->logradouro . ', ' . $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->numero . ' ' . $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->complemento,
                        'bairro' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->bairro,
                        'uf' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->uf,
                        'endereco2' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cep . ' ' . $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cidade,
                        'vencimento' => $parcela->vencimento,
                        'range' => $rangeGerado->label,
                        'especie_doc' => 'PD',
                        'especie' => 'REAL',
                        'aceite' => 'NÃO',
                        'quantidade' => '1',
                        'instrucoes1' => '- Após o vencimento, mora de 0,29% ao dia.',
                        'instrucoes2' => '',
                        'instrucoes3' => '- Em caso de dúvidas entre em contato conosco: contato@amaremansa.com.br',
                        'instrucoes4' => '- Boleto relativo ao pagamento da ' . $parcela->seq . '° parcela da proposta ' . $objProposta->codigo,
                        'instrucoes5' => $parcela->seq . '/' . $objProposta->qtd_parcelas,
                        'mora' => $mora,
                        'seq_parc' => $parcela->seq,
                        'valor_corri' => $objProposta->getValorParcela()
                );

                if (strtotime($parcela->vencimento) < strtotime($hoje)) {
                    $mora = $parcela->calcMora();
                    $r['mora'] = $mora;
                    $r['vencimento'] = date('Y-m-d');
                    $r['valor_boleto'] = number_format(( $objProposta->getValorParcela() + $parcela->calcMora()), 2, ',', '');
                    $r['valor_corri'] = $objProposta->getValorParcela() + $parcela->calcMora();
                }

                $arrRetorno[] = $r;
            }
        }

        return $arrRetorno;
    }

    public function getBoletoEcommerce($ParcelasSeq, $codVenda, $accessToken) {

        $arrRetorno = array();
        $hoje = date('Y-m-d');
        $financeira = Financeira::model()->findByPk(5);

        $venda = VendaW::model()->find('codigo = ' . $codVenda . ' AND external_access_token = ' . "'$accessToken' AND titulos_gerados = 1");

        if ($venda != NULL) {

            $titulo = Titulo::model()->find('VendaW_id = ' . $venda->id);

            if ($titulo != NULL) {

                $qtd_parcelas = count(Parcela::model()->findAll('Titulo_id = ' . $titulo->id));
                $criteria = new CDbCriteria;
                $criteria->addInCondition('Titulo_id', array($titulo->id), 'AND');
                $criteria->addInCondition('seq', $ParcelasSeq, 'AND');
                $parcelas = Parcela::model()->findAll($criteria);

                if (count($parcelas)) {
                    foreach ($parcelas as $parcela) {
                        $mora = null;
                        $rangeGerado = RangesGerados::model()->find('Parcela_id = ' . $parcela->id);

                        $r = array(
                                'codigobanco' => $financeira->getDadosBancarios()->banco->codigo,
                                'cedente' => $financeira->nome,
                                'cpf_cnpj' => $financeira->cnpj,
                                'agencia' => $financeira->getDadosBancarios()->agencia,
                                'valor_boleto' => number_format($parcela->valor, 2, ',', ''),
                                'sacado' => $parcela->titulo->vendaW->cliente->pessoa->nome,
                                'cpfsacado' => $parcela->titulo->vendaW->cliente->pessoa->getCpf()->numero,
                                'endereco1' => $parcela->titulo->vendaW->cliente->pessoa->getEnderecoCobranca()->logradouro . ', ' . $parcela->titulo->vendaW->cliente->pessoa->getEnderecoCobranca()->numero . ' ' . $parcela->titulo->vendaW->cliente->pessoa->getEnderecoCobranca()->complemento,
                                'bairro' => $parcela->titulo->vendaW->cliente->pessoa->getEnderecoCobranca()->bairro,
                                'uf' => $parcela->titulo->vendaW->cliente->pessoa->getEnderecoCobranca()->uf,
                                'endereco2' => $parcela->titulo->vendaW->cliente->pessoa->getEnderecoCobranca()->cep . ' ' . $parcela->titulo->vendaW->cliente->pessoa->getEnderecoCobranca()->cidade,
                                'vencimento' => $parcela->vencimento,
                                'range' => $rangeGerado->label,
                                'especie_doc' => 'PD',
                                'especie' => 'REAL',
                                'aceite' => 'NÃO',
                                'quantidade' => '1',
                                'instrucoes1' => '- Após o vencimento, mora de 0,29% ao dia.',
                                'instrucoes2' => '',
                                'instrucoes3' => '- Em caso de dúvidas entre em contato conosco: contato@amaremansa.com.br.',
                                'instrucoes4' => '- Boleto relativo ao pagamento da ' . $parcela->seq . '° parcela da compra ' . $venda->codigo,
                                'instrucoes5' => $parcela->seq . '/' . $qtd_parcelas,
                                'mora' => $mora,
                                'valor_corri' => $parcela->valor
                        );

                        if (strtotime($parcela->vencimento) < strtotime($hoje)) {
                            $mora = $parcela->calcMora();
                            $r['mora'] = $mora;
                            $r['vencimento'] = date('Y-m-d');
                            $r['valor_boleto'] = number_format(($parcela->valor + $parcela->calcMora()), 2, ',', '');
                            $r['valor_corri'] = $parcela->valor + $parcela->calcMora();
                        }

                        $arrRetorno[] = $r;
                    }
                }
            }
        }

        return $arrRetorno;
    }

    public function gerarTitulosCredshow($objProposta) {

        $util           = new Util;
        $arrRetorno     = array();
        $configBradesco = Banco::model()->configuracoesBanco();
        $config         = 'boleto_credshow_nb';
        $inst7          = '';

        $retornoEmprestimo = null;

        $ehEmprestimo = false;

        $titulo = new Titulo;
        $titulo->prefixo = '001';
        $titulo->NaturezaTitulo_id = 1;
        $titulo->Proposta_id = $objProposta->id;
        $mora = ConfigLN::model()->valorDoParametro("taxa_mora") / 100;

        $zeros = "";

        if ($titulo->save()) {

            $objProposta->titulos_gerados = 1;
            $objProposta->Banco_id = 10;

            if ($objProposta->update()){
              Titulo::model()->gerarRepasse($objProposta);
            }

            for ($i = 0; $i < $objProposta->qtd_parcelas; $i++) {

                $range = Range::model()->findByPk(4);
                $range->ultimo_gerado = $range->ultimo_gerado + 1;

                for ($y = 1; $y <= 5 - strlen((string) $range->ultimo_gerado); $y ++){
                    $zeros .= "0";
                }

                $parcela = new Parcela;
                $parcela->seq = $i + 1;
                $parcela->valor = $objProposta->getValorParcela();
                $parcela->valor_texto = $objProposta->valor_parcela_texto;
                $parcela->valor_atual = 0;
                $parcela->Titulo_id = $titulo->id;

                if ($i == 0) {
                    $parcela->vencimento = $util->view_date_to_bd($objProposta->getDataPrimeiraParcela());
                } else {
                    $parcela->vencimento = $util->adicionarMesMantendoDia($util->view_date_to_bd($objProposta->getDataPrimeiraParcela()), $i)->format('Y-m-d');
                }

                $parcela->save();

                $rangeSeq = strval($range->ultimo_gerado);
                $rangeBoleto = $zeros . $rangeSeq;

                $rangesGerados = new RangesGerados;
                $rangesGerados->numero = $rangeBoleto;
                $rangesGerados->data_geracao = date('Y-m-d H:i:s');
                $rangesGerados->Range_id = $range->id;
                $rangesGerados->Parcela_id = $parcela->id;
                $rangesGerados->label = strval($range->prefixo) . $zeros . $rangeSeq;

                $rangesGerados->data_cadastro   = date('Y-m-d H:i:s');
                $rangesGerados->habilitado      = 1;

                if ($rangesGerados->save())
                {

                    $r = array  (
                                    'codigobanco' => '237',
                                    'cedente' => $configBradesco[$config]['r_s'],
                                    'cpf_cnpj' => $configBradesco[$config]['cnpj'],
                                    'agencia' => substr($configBradesco[$config]['agencia'], 1, 4),
                                    'valor_boleto' => number_format($objProposta->getValorParcela(), 2, ',', ''),
                                    'sacado' => $objProposta->analiseDeCredito->cliente->pessoa->nome,
                                    'cpfsacado' => $objProposta->analiseDeCredito->cliente->pessoa->getCpf()->numero,
                                    'endereco1' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->logradouro . ', ' . $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->numero . ' ' . $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->complemento,
                                    'bairro' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->bairro,
                                    'uf' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->uf,
                                    'endereco2' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cep . ' ' . $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cidade,
                                    'vencimento' => $parcela->vencimento,
                                    'range' => strval($range->prefixo) . $zeros . $rangeSeq,
                                    'especie_doc' => 'PD',
                                    'especie' => 'REAL',
                                    'aceite' => 'NÃO',
                                    'quantidade' => '1',
                                    'instrucoes1' => 'Após o vencimento, mora de ' . ConfigLN::model()->valorDoParametro("taxa_mora") . '% ao dia.',
                                    'instrucoes2' => 'Atendimento ao cliente: 084 2040-0800',
                                    'instrucoes4' => 'Boleto relativo ao pagamento da ' . $parcela->seq . '° parcela da proposta ' . $objProposta->codigo,
                                    'instrucoes5' => $parcela->seq . '/' . $objProposta->qtd_parcelas,
                                    'instrucoes6' => 'Atenção: após vencido, o título estará sujeito a inclusão no SPC e SERASA',
                                    'instrucoes7' => $inst7,
                                    'seq_parc' => $parcela->seq,
                                    'valor_corri' => $objProposta->getValorParcela()
                                );

                    array_push($arrRetorno, $r);
                    $range->update();
                    $zeros = "";
                }
            }
        }

        $arrRetorno["retornoEmprestimo"] = $retornoEmprestimo;
        $arrRetorno["ehEmprestimo"] = $ehEmprestimo;

        $sigacLog = new SigacLog;
        $sigacLog->saveLog('Impressão de Boletos', 'Proposta', $objProposta->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Crediarista " . Yii::app()->session['usuario']->nome_utilizador . " imprimiu os boletos referentes a proposta com código: " . $objProposta->codigo, "127.0.0.1", null, Yii::app()->session->getSessionId());

        return $arrRetorno;
    }

    public function gerarTitulosBradesco($objProposta) {

        $util = new Util;
        $arrRetorno = array();
        $configBradesco = Banco::model()->configuracoesBanco();

        $retornoEmprestimo = null;

        $ehEmprestimo = false;

        $titulo = new Titulo;
        $titulo->prefixo = '001';
        $titulo->NaturezaTitulo_id = 1;
        $titulo->Proposta_id = $objProposta->id;
        $mora = ConfigLN::model()->valorDoParametro("taxa_mora") / 100;

        $zeros = "";

        if ($titulo->save()) {

            $objProposta->titulos_gerados = 1;
            $objProposta->Banco_id = 3;

            if ($objProposta->update()) {

                $emprestimo = Emprestimo::model()->find("habilitado AND Proposta_id = $objProposta->id");

                if ($emprestimo !== null) {
                    $retornoEmprestimo = $emprestimo->gerarTituloPagar();
                    $ehEmprestimo = true;
                } else {
                    Titulo::model()->gerarRepasse($objProposta);
                }
            }

            for ($i = 0; $i < $objProposta->qtd_parcelas; $i++) {

                $range = Range::model()->findByPk(4);
                $range->ultimo_gerado = $range->ultimo_gerado + 1;

                for ($y = 1; $y <= 5 - strlen((string) $range->ultimo_gerado); $y ++){
                    $zeros .= "0";
                }

                $parcela = new Parcela;
                $parcela->seq = $i + 1;
                $parcela->valor = $objProposta->getValorParcela();
                $parcela->valor_texto = $objProposta->valor_parcela_texto;
                $parcela->valor_atual = 0;
                $parcela->Titulo_id = $titulo->id;

                if ($i == 0) {
                    $parcela->vencimento = $util->view_date_to_bd($objProposta->getDataPrimeiraParcela());
                } else {
                    $parcela->vencimento = $util->adicionarMesMantendoDia($util->view_date_to_bd($objProposta->getDataPrimeiraParcela()), $i)->format('Y-m-d');
                }

                $parcela->save();

                $rangeSeq = strval($range->ultimo_gerado);
                $rangeBoleto = $zeros . $rangeSeq;

                $rangesGerados = new RangesGerados;
                $rangesGerados->numero = $rangeBoleto;
                $rangesGerados->data_geracao = date('Y-m-d H:i:s');
                $rangesGerados->Range_id = $range->id;
                $rangesGerados->Parcela_id = $parcela->id;
                $rangesGerados->label = strval($range->prefixo) . $zeros . $rangeSeq;

                $rangesGerados->data_cadastro   = date('Y-m-d H:i:s');
                $rangesGerados->habilitado      = 1;

                if (in_array($objProposta->Financeira_id, [10,11]) )
                {

                    $config = 'credshow_bradesco_fidic';
                    $inst7 = '';

                }
                else
                {
                    $config = 'credshow_bradesco'   ;
                    $inst7  = ''                    ;
                }

                if ($rangesGerados->save())
                {

                    $r = array  (
                                    'codigobanco' => '237',
                                    'cedente' => $configBradesco[$config]['r_s'],
                                    'cpf_cnpj' => $configBradesco[$config]['cnpj'],
                                    'agencia' => substr($configBradesco[$config]['agencia'], 1, 4),
                                    'valor_boleto' => number_format($objProposta->getValorParcela(), 2, ',', ''),
                                    'sacado' => $objProposta->analiseDeCredito->cliente->pessoa->nome,
                                    'cpfsacado' => $objProposta->analiseDeCredito->cliente->pessoa->getCpf()->numero,
                                    'endereco1' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->logradouro . ', ' . $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->numero . ' ' . $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->complemento,
                                    'bairro' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->bairro,
                                    'uf' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->uf,
                                    'endereco2' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cep . ' ' . $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cidade,
                                    'vencimento' => $parcela->vencimento,
                                    'range' => strval($range->prefixo) . $zeros . $rangeSeq,
                                    'especie_doc' => 'PD',
                                    'especie' => 'REAL',
                                    'aceite' => 'NÃO',
                                    'quantidade' => '1',
                                    'instrucoes1' => 'Após o vencimento, mora de ' . ConfigLN::model()->valorDoParametro("taxa_mora") . '% ao dia.',
                                    'instrucoes2' => 'Atendimento ao cliente: 084 2040-0800',
                                    'instrucoes4' => 'Boleto relativo ao pagamento da ' . $parcela->seq . '° parcela da proposta ' . $objProposta->codigo,
                                    'instrucoes5' => $parcela->seq . '/' . $objProposta->qtd_parcelas,
                                    'instrucoes6' => 'Atenção: após vencido, o título estará sujeito a inclusão no SPC e SERASA',
                                    'instrucoes7' => $inst7,
                                    'seq_parc' => $parcela->seq,
                                    'valor_corri' => $objProposta->getValorParcela()
                                );

                    array_push($arrRetorno, $r);

                    $bradesco = new Bradesco('2014-05-05', "0.00", strval($range->prefixo) . $zeros . $rangeSeq, $objProposta);
                    $cnabConfig = new CampoCnabConfig;
                    $cnabDetalhe = new CnabDetalhe;

                    $cnabDetalhe->cod_registro = '1';
                    $cnabDetalhe->agencia_de_debito_bradesco = '00000';
                    $cnabDetalhe->digito_da_agencia_de_debito_bradesco = '0';
                    $cnabDetalhe->razao_da_conta_corrente_bradesco = '00000';
                    $cnabDetalhe->conta_corrente_bradesco = '0000000';
                    $cnabDetalhe->digito_da_conta_corrente_bradesco = '0';
                    $cnabDetalhe->ident_da_empresa_beneficiaria_no_banco = $configBradesco[$config]['idBradesco'];
                    $cnabDetalhe->controle_participante = $cnabConfig->cnabValorMonetario(strval($range->prefixo) . $zeros . $rangeSeq, 25);
                    $cnabDetalhe->codigo_do_banco_debitado_bradesco = '000';
                    $cnabDetalhe->campo_de_multa = '0';
                    $cnabDetalhe->percentual_de_multa_bradesco = '0000';
                    $cnabDetalhe->ident_do_titulo_no_banco_bradesco = '0' . strval($range->prefixo) . $zeros . $rangeSeq;
                    $cnabDetalhe->digito_de_auto_conf_numero_bancario_bradesco = $bradesco->getDigitoVerificador_nossonumero();
                    $cnabDetalhe->desconto_bonificacao_por_dia_bradesco = '0000000000';
                    $cnabDetalhe->emissao_da_papelete_bradesco = '2';
                    $cnabDetalhe->emite_para_debito_automatico_bradesco = 'N';
                    $cnabDetalhe->identificacao_da_operacao_do_banco_bradesco = '';
                    $cnabDetalhe->indicador_rateio_credito_bradesco = 'R';
                    $cnabDetalhe->enderecamento_para_aviso_do_debito_aut_em_conta_corrente = '0';
                    $cnabDetalhe->cod_ocorrencia = '01';
                    $cnabDetalhe->seu_numero = strval($range->prefixo) . $zeros . $rangeSeq;
                    $cnabDetalhe->vencimento_texto = $cnabConfig->cnabDate($parcela->vencimento); //OK;
                    $cnabDetalhe->valor_do_titulo_texto = $cnabConfig->cnabValorMonetario(number_format((float) $objProposta->getValorParcela(), 2, '.', ''), 13);
                    $cnabDetalhe->banco_cobrador = '000';
                    $cnabDetalhe->agencia_depositaria = '00000';
                    $cnabDetalhe->especie = '01';
                    $cnabDetalhe->aceite = 'N';
                    $cnabDetalhe->data_emissao_texto = date('dmy');
                    $cnabDetalhe->data_emissao = date('Y-m-d');
                    $cnabDetalhe->instrucao01 = '00';
                    $cnabDetalhe->instrucao02 = '00';
                    $cnabDetalhe->juros_de_mora_texto = $cnabConfig->cnabValorMonetario($util->round_up(( $parcela->valor * $mora), 2), 13);
                    $cnabDetalhe->desconto_data_texto = '000000';
                    $cnabDetalhe->valor_desconto_texto = '0000000000000';
                    $cnabDetalhe->valor_iof_texto = '0000000000000';
                    $cnabDetalhe->valor_abatimento_texto = '0000000000000';
                    $cnabDetalhe->codigo_de_inscricao = '01';
                    $cnabDetalhe->numero_de_inscricao = '000' . $objProposta->analiseDeCredito->cliente->pessoa->getCPF()->numero;
                    $cnabDetalhe->nome_do_sacado = substr($util->cleanStr($objProposta->analiseDeCredito->cliente->pessoa->nome, 'upp'), 0, 35);
                    $cnabDetalhe->endereco_do_pagador_sant = substr($util->cleanStr($objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->logradouro, 'upp'), 0, 30) . ' ' . $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->numero;
                    $cnabDetalhe->cep_do_sacado = substr($objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cep, 0, 5);
                    $cnabDetalhe->sufixo_do_cep = substr($objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cep, 5);
                    $cnabDetalhe->numero_sequencial = '000000';
                    $cnabDetalhe->mensagem_bradesco_um = '';
                    $cnabDetalhe->sacador_avalista_ou_2_mensagem_bradesco = '';
                    $cnabDetalhe->Banco_id = 3;
                    $cnabDetalhe->save();

                    $range->update();
                    $zeros = "";
                }
            }
        }

        $arrRetorno["retornoEmprestimo"] = $retornoEmprestimo;
        $arrRetorno["ehEmprestimo"] = $ehEmprestimo;

        $sigacLog = new SigacLog;
        $sigacLog->saveLog('Impressão de Boletos', 'Proposta', $objProposta->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Crediarista " . Yii::app()->session['usuario']->nome_utilizador . " imprimiu os boletos referentes a proposta com código: " . $objProposta->codigo, "127.0.0.1", null, Yii::app()->session->getSessionId());

        return $arrRetorno;
    }

    public function getSegundaViaBradesco($ParcelasIds, $objProposta, $todasAsParcelas = null) {

        /*$arquivo = fopen("teste12.txt", "w+");
        fwrite($arquivo, "12");
        fclose($arquivo);*/

        $arrRetorno = array();
        $hoje = date('Y-m-d');
        $configBradesco = Banco::model()->configuracoesBanco();

        if ($todasAsParcelas != NULL) {
            $parcelas = $objProposta->getParcelas();

            foreach ($parcelas as $parcela) {
                if ($parcela->valor_atual < $parcela->valor) {
                    $ParcelasIds[] = $parcela->id;
                }
            }
        }

        ob_start();
        var_dump($ParcelasIds);
        $varDump = ob_get_clean();

        /*$arquivo = fopen("testeParcelasId", "w+");
        fwrite($arquivo, $varDump);
        fclose($arquivo);*/

        if( in_array($objProposta->Financeira_id, [10,11]) )
        {
            $config = 'credshow_bradesco_fidic';
            $inst7 = '';
        }
        else
        {
            $config = 'credshow_bradesco';
            $inst7 = '';
        }

        for ($i = 0; $i < count($ParcelasIds); $i++)
        {
            $mora = null;
            $parcela = Parcela::model()->findByPk($ParcelasIds[$i]);
            $rangeGerado = RangesGerados::model()->find('Parcela_id = ' . $ParcelasIds[$i]);

            if( $parcela->valor_atual == 0 )
            {
                $r = array(
                        'codigobanco' => '237',
                        'cedente' => $configBradesco[$config]['r_s'],
                        'cpf_cnpj' => $configBradesco[$config]['cnpj'],
                        'agencia' => substr($configBradesco[$config]['agencia'], 1, 4),
                        'valor_boleto' => number_format($objProposta->getValorParcela(), 2, ',', ''),
                        'sacado' => $objProposta->analiseDeCredito->cliente->pessoa->nome,
                        'cpfsacado' => $objProposta->analiseDeCredito->cliente->pessoa->getCpf()->numero,
                        'endereco1' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->logradouro . ', ' . $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->numero . ' ' . $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->complemento,
                        'bairro' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->bairro,
                        'uf' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->uf,
                        'endereco2' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cep . ' ' . $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cidade,
                        'vencimento' => $parcela->vencimento,
                        'range' => $rangeGerado->label,
                        'especie_doc' => 'PD',
                        'especie' => 'REAL',
                        'aceite' => 'NÃO',
                        'quantidade' => '1',
                        'instrucoes1' => 'Após o vencimento, mora de ' . number_format(ConfigLN::model()->valorDoParametro("taxa_mora"), 2, ',', '.') . '% ao dia.',
                        'instrucoes2' => 'Atendimento ao cliente: 084 2040-0800',
                        'instrucoes4' => 'Boleto relativo ao pagamento da ' . $parcela->seq . '° parcela da proposta ' . $objProposta->codigo,
                        'instrucoes5' => $parcela->seq . '/' . $objProposta->qtd_parcelas,
                        'instrucoes6' => 'Atenção: após vencido, o título estará sujeito a inclusão no SPC e SERASA',
                        'instrucoes7' => $inst7,
                        'mora' => $mora,
                        'seq_parc' => $parcela->seq,
                        'valor_corri' => $objProposta->getValorParcela()
                );

                if (strtotime($parcela->vencimento) < strtotime($hoje)) {
                    $mora = $parcela->calcMora();
                    $r['mora'] = $mora;
                    $r['vencimento'] = date('Y-m-d');
                    $r['valor_boleto'] = number_format(( $objProposta->getValorParcela() + $parcela->calcMora()), 2, ',', '');
                    $r['valor_corri'] = $objProposta->getValorParcela() + $parcela->calcMora();
                }

                $arrRetorno[] = $r;
            }
        }

        return $arrRetorno;
    }

    public function getSegundaViaCredshow($ParcelasIds, $objProposta, $todasAsParcelas = null) {

        $arrRetorno     = array();
        $hoje           = date('Y-m-d');
        $configBradesco = Banco::model()->configuracoesBanco();
        $config         = 'boleto_credshow_nb';

        if ($todasAsParcelas != NULL) {

            $parcelas = $objProposta->getParcelas();

            foreach ($parcelas as $parcela) {
                if ($parcela->valor_atual < $parcela->valor) {
                    $ParcelasIds[] = $parcela->id;
                }
            }
        }

        for ($i = 0; $i < count($ParcelasIds); $i++)
        {
            $mora         = null;
            $parcela      = Parcela::model()->findByPk($ParcelasIds[$i]);
            $rangeGerado  = RangesGerados::model()->find('Parcela_id = ' . $ParcelasIds[$i]);

            if( $parcela->valor_atual == 0 )
            {
                $r = array(
                        'codigobanco' => '237',
                        'cedente' => $configBradesco[$config]['r_s'],
                        'cpf_cnpj' => $configBradesco[$config]['cnpj'],
                        'agencia' => substr($configBradesco[$config]['agencia'], 1, 4),
                        'valor_boleto' => number_format($objProposta->getValorParcela(), 2, ',', ''),
                        'sacado' => $objProposta->analiseDeCredito->cliente->pessoa->nome,
                        'cpfsacado' => $objProposta->analiseDeCredito->cliente->pessoa->getCpf()->numero,
                        'endereco1' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->logradouro . ', ' . $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->numero . ' ' . $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->complemento,
                        'bairro' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->bairro,
                        'uf' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->uf,
                        'endereco2' => $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cep . ' ' . $objProposta->analiseDeCredito->cliente->pessoa->getEnderecoCobranca()->cidade,
                        'vencimento' => $parcela->vencimento,
                        'range' => $rangeGerado->label,
                        'especie_doc' => 'PD',
                        'especie' => 'REAL',
                        'aceite' => 'NÃO',
                        'quantidade' => '1',
                        'instrucoes1' => 'Após o vencimento, mora de ' . number_format(ConfigLN::model()->valorDoParametro("taxa_mora"), 2, ',', '.') . '% ao dia.',
                        'instrucoes2' => 'Atendimento ao cliente: 084 2040-0800',
                        'instrucoes4' => 'Boleto relativo ao pagamento da ' . $parcela->seq . '° parcela da proposta ' . $objProposta->codigo,
                        'instrucoes5' => $parcela->seq . '/' . $objProposta->qtd_parcelas,
                        'instrucoes6' => 'Atenção: após vencido, o título estará sujeito a inclusão no SPC e SERASA',
                        // 'instrucoes7' => $inst7,
                        'mora' => $mora,
                        'seq_parc' => $parcela->seq,
                        'valor_corri' => $objProposta->getValorParcela()
                );

                if (strtotime($parcela->vencimento) < strtotime($hoje)) {
                    $mora = $parcela->calcMora();
                    $r['mora'] = $mora;
                    $r['vencimento'] = date('Y-m-d');
                    $r['valor_boleto'] = number_format(( $objProposta->getValorParcela() + $parcela->calcMora()), 2, ',', '');
                    $r['valor_corri'] = $objProposta->getValorParcela() + $parcela->calcMora();
                }

                $arrRetorno[] = $r;
            }
        }

        return $arrRetorno;
    }
}
