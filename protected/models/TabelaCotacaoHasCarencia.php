<?php

class TabelaCotacaoHasCarencia extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'TabelaCotacaoHasCarencia';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('CarenciaId, TabelaCotacaoId', 'required'),
			array('CarenciaId, TabelaCotacaoId, habilitado', 'numerical', 'integerOnly'=>true),
			array('id, CarenciaId, TabelaCotacaoId, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'carencia' 	=> array(self::BELONGS_TO, 'Carencia', 		'CarenciaId'),
			'tabela' 	=> array(self::BELONGS_TO, 'TabelaCotacao', 'TabelaCotacaoId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' 				=> 'ID',
			'CarenciaId' 		=> 'Contato',
			'TabelaCotacaoId'	=> 'Email',
			'habilitado' 		=> 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id',				$this->id);
		$criteria->compare('CarenciaId',		$this->CarenciaId);
		$criteria->compare('TabelaCotacaoId',	$this->TabelaCotacaoId);
		$criteria->compare('habilitado',		$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ContatoHasEmail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
