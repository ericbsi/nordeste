<?php

/**
 * This is the model class for table "Nota_Fiscal".
 *
 * The followings are the available columns in table 'Nota_Fiscal':
 * @property integer $id
 * @property string $data
 * @property string $serie
 * @property string $documento
 * @property integer $Status_Nota_Fiscal_id
 *
 * The followings are the available model relations:
 * @property ItemNotaFiscal[] $itemNotaFiscals
 * @property StatusNotaFiscal $statusNotaFiscal
 */
class NotaFiscal extends CActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'Nota_Fiscal';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('data_cadastro'                                                                                               , 'required'                                ),
            array('Status_Nota_Fiscal_id, habilitado'                                                                           , 'numerical'   , 'integerOnly' => true     ),
            array('serie'                                                                                                       , 'length'      , 'max'         => 3        ),
            array('documento'                                                                                                   , 'length'      , 'max'         => 9        ),
            array('chaveNFe'                                                                                                    , 'length'      , 'max'         => 255      ),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, data_cadastro, serie, documento, Status_Nota_Fiscal_id, habilitado, data_cadastro, chaveNFe, observacao' , 'safe'        , 'on'          => 'search' ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
                        'itemNotaFiscals'   => array(self::HAS_MANY     , 'ItemNotaFiscal'      , 'Nota_Fiscal_id'          ),
                        'statusNotaFiscal'  => array(self::BELONGS_TO   , 'StatusNotaFiscal'    , 'Status_Nota_Fiscal_id'   ),
                    );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
                        'id'                    => 'ID'                 ,
                        'data_cadastro'         => 'Data'               ,
                        'serie'                 => 'Serie'              ,
                        'documento'             => 'Documento'          ,
                        'Status_Nota_Fiscal_id' => 'Status Nota Fiscal' ,
                        'habilitado'            => 'Habilitado'         ,
                        'data_cadastro'         => 'Data Cadastro'      ,
                        'chaveNFe'              => 'Chave NFe'          ,
                        'observacao'            => 'Observacao'         ,
                    );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id'                     , $this->id                             );
        $criteria->compare('data_cadastro'          , $this->data_cadastro          , true  );
        $criteria->compare('serie'                  , $this->serie                  , true  );
        $criteria->compare('documento'              , $this->documento              , true  );
        $criteria->compare('Status_Nota_Fiscal_id'  , $this->Status_Nota_Fiscal_id          );
        $criteria->compare('habilitado'             , $this->habilitado                     );
        $criteria->compare('chaveNFe'               , $this->chaveNFe                       );

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return NotaFiscal the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    /*public function exportarLinhasCNAB()
    {
        
        $linhas = "";
        
        $vendasHasNF = VendaHasNotaFiscal::model()->find("habilitado AND Nota_Fiscal_id = $this->id");
        
        foreach ($vendasHasNF as $vendaHasNF)
        {

            $venda = Venda::model()->find("habilitado AND Venda_id = $vendaHasNF->id");

            if($venda !== null)
            {

                $idNaturezaTitulo = ConfigLN::model()->valorDoParametro("idNaturezaTituloReceber");

                if(!isset($idNaturezaTitulo))
                {
                    $idNaturezaTitulo = 1;
                }

                $titulo = Titulo::model()->find("habilitado AND NaturezaTitulo_id = $idNaturezaTitulo AND Proposta_id = $venda->Proposta_id");

                $cont = 1;

                if($titulo !== null)
                {

                    $parcelas = Parcela::model()->findAll("habilitado AND Titulo_id = $titulo->id");

                    foreach ($parcelas as $parcela)
                    {

                        $valorParcelaRepasse    = ($parcela->titulo->proposta->valorRepasse()/$parcela->titulo->proposta->qtd_parcelas) ;
                        $valorParcela           =  $parcela->valor                                                                      ;

                        $cont++;

                        $linha = "";

                        $valorFormatadoRepasse  = number_format($valorParcelaRepasse    ,2,'','');
                        $valorFormatado         = number_format($valorParcela           ,2,'','');

                        $cpf            = $parcela->titulo->proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero   ;
                        $nomeCliente    = $parcela->titulo->proposta->analiseDeCredito->cliente->pessoa->nome               ;
                        $endereco       = $parcela->titulo->proposta->analiseDeCredito->cliente->pessoa->getEndereco()      ;

    //                  $cedente        = "CREDSHOW FUNDO DE INVESTIMENTOS EM DIREITOS CR23273905000130"                    ;
                        $cedente        = "A MARE MANSA COMERCIO DE MOVEIS E ELETRODOMEST08106783000102"                    ;

                        if($endereco == null)
                        {
                            str_repeat(" ", 40);
                        }
                        else
                        {
                            $enderecoCompleto    = trim($endereco->logradouro ) . " "   ; 
                            $enderecoCompleto   .= trim($endereco->numero     ) . " "   ;
                            $enderecoCompleto   .= trim($endereco->bairro     ) . " "   ;
                            $enderecoCompleto   .= trim($endereco->cidade     ) . " "   ;
                            $enderecoCompleto   .= trim($endereco->uf         ) . " "   ;
                            $enderecoCompleto   .= trim($endereco->complemento) . " "   ;

                            $enderecoCEP         = trim($endereco->cep        )         ;

                            $enderecoCompleto   = strtoupper(substr(strtr( utf8_decode($enderecoCompleto    )   , utf8_decode("áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ"), "aaaaeeiooouucAAAAEEIOOOUUC"), 0, 40)   );
                            $nomeCliente        = strtoupper(       strtr( utf8_decode($nomeCliente         )   , utf8_decode("áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ"), "aaaaeeiooouucAAAAEEIOOOUUC")           );
                        }

                        $linha .= "1"                                                               ; //de:   1 ate:   1
                        $linha .= str_repeat(" ", 19)                                               ; //de:   2 ate:  20
                        $linha .= "02"                                                              ; //de:  21 ate:  22 
                        $linha .= "00"                                                              ; //de:  23 ate:  24 
                        $linha .= str_repeat("0", 04)                                               ; //de:  25 ate:  28 
                        $linha .= "00"                                                              ; //de:  29 ate:  30 
                        $linha .= "0199"                                                            ; //de:  31 ate:  34 
                        $linha .= "  "                                                              ; //de:  35 ate:  36
                        $linha .= "0"                                                               ; //de:  37 ate:  37
                        $linha .= str_pad($parcela->id              , 25, "0", STR_PAD_LEFT)        ; //de:  38 ate:  62 /** nosso numero?*/ //verificar isso depois
                        /*$linha .= str_repeat("0", 03)                                               ; //de:  63 ate:  65
                        $linha .= str_repeat("0", 05)                                               ; //de:  66 ate:  70
                        $linha .= str_repeat(" ", 11)                                               ; //de:  71 ate:  81
                        $linha .= str_repeat(" ", 01)                                               ; //de:  82 ate:  82
                        $linha .= str_repeat("0", 10)                                               ; //de:  83 ate:  92
                        $linha .= str_repeat(" ", 01)                                               ; //de:  93 ate:  93
                        $linha .= str_repeat(" ", 01)                                               ; //de:  94 ate:  94
                        $linha .= str_repeat("0", 06)                                               ; //de:  95 ate: 100
                        $linha .= str_repeat(" ", 04)                                               ; //de: 101 ate: 104
                        $linha .= str_repeat(" ", 01)                                               ; //de: 105 ate: 105
                        $linha .= str_repeat(" ", 01)                                               ; //de: 106 ate: 106
                        $linha .= str_repeat(" ", 02)                                               ; //de: 107 ate: 108
    //                    $linha .= str_pad($parcela->seq         ,  2, "0", STR_PAD_LEFT)          ; //de: 109 ate: 110
                        $linha .= "01"                                                              ; //de: 109 ate: 110
                        $linha .= str_pad($parcela->titulo->id      , 10, "0", STR_PAD_LEFT)        ; //de: 111 ate: 120
                        $linha .= date("dmy",  strtotime($parcela->vencimento))                     ; //de: 121 ate: 126
                        $linha .= str_pad($valorFormatado           , 13, "0", STR_PAD_LEFT)        ; //de: 127 ate: 139
                        $linha .= str_repeat("0", 03)                                               ; //de: 140 ate: 142
                        $linha .= str_repeat("0", 05)                                               ; //de: 143 ate: 147
                        $linha .= "01"                                                              ; //de: 148 ate: 149
                        $linha .= " "                                                               ; //de: 150 ate: 150
                        $linha .= date("dmy", strtotime($parcela->titulo->proposta->data_cadastro)) ; //de: 151 ate: 156
                        $linha .= "00"                                                              ; //de: 157 ate: 158
                        $linha .= "0"                                                               ; //de: 159 ate: 159
                        $linha .= "02"                                                              ; //de: 160 ate: 161
                        $linha .= str_repeat("0", 12)                                               ; //de: 162 ate: 173
                        $linha .= str_repeat("0", 19)                                               ; //de: 174 ate: 192
                        $linha .= str_pad($valorFormatadoRepasse    , 13, "0", STR_PAD_LEFT     )   ; //de: 193 ate: 205
                        $linha .= str_repeat("0", 13)                                               ; //de: 206 ate: 218
                        $linha .= "01"                                                              ; //de: 219 ate: 220
                        $linha .= str_pad($cpf                      , 14, " ", STR_PAD_LEFT     )   ; //de: 221 ate: 234
                        $linha .= str_pad($nomeCliente              , 40, " ", STR_PAD_RIGHT    )   ; //de: 235 ate: 274
                        $linha .= str_pad($enderecoCompleto         , 40, " ", STR_PAD_RIGHT    )   ; //de: 275 ate: 314
                        $linha .= str_pad($this->documento          ,  9, "0", STR_PAD_LEFT     )   ; //de: 315 ate: 323
                        $linha .= str_pad($this->serie              , 03, "0", STR_PAD_LEFT     )   ; //de: 324 ate: 326
                        $linha .= str_pad($enderecoCEP              , 08, "0", STR_PAD_LEFT     )   ; //de: 327 ate: 334
                        $linha .= $cedente                                                          ; //de: 335 ate: 394
                        $linha .= $this->chaveNFe                                                   ; //de: 395 ate: 438
                        $linha .= str_pad($cont                     , 06, "0", STR_PAD_LEFT     )   ; //de: 439 ate: 444

                        $linha .= chr(13) . chr(10);

                        //         1         2         3         4         5         6         7         8         9        10        11        12        13        14        15        16        17        18        19        20        21        22        23        24        25        26        27        28        29        30        31        32        33        34        35        36        37        38        39        40        41        42        43        44                 
                        //123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234
                        //1                   02000000000199  0000000000000000000008697200000000            0000000000  000000        01000004890110061600000000039600000000001 1005160000200000000000000000000000000000000000000003445000000000000001   04524059474LUCIANA DA SILVA ALENCAR                SITIO BUJARI SN MUNICIPIO CATOLE DO ROCH00002472800158884000A MARE MANSA COMERCIO DE MOVEIS E ELETRODOMEST0810678300010225160508106783003462550010000247281004299275000002


                        $linhas .= $linha;

                    }

                }

            }
            
        }

        return $linhas;
            
    }*/
    

}
