<?php

class AnaliseDeCredito extends CActiveRecord {

    public function novo($AnaliseDeCredito, $ClienteId, $Valor, $Entrada, $Bens, $emprestimo) {
        $filial = Yii::app()->session['usuario']->returnFilial();

        $codigo = str_replace("-", "", substr($filial->cnpj, 11));
        $codigo .= Yii::app()->session['usuario']->id;
        $codigo .= date('dmyhis');
        
        $mercadoria = "OUTROS";
        
        if(isset($_POST['mercadoria']) && $_POST['mercadoria'] !== null)
        {
            $mercadoria = $_POST['mercadoria'];
        }

        $util = new Util;
        $ac = new AnaliseDeCredito;
        $ac->attributes = $AnaliseDeCredito;
        
        //concertar isso aqui depois{
        $ac->Vendedor_id = $ac->vendedor;
        $ac->vendedor = Vendedor::model()->findByPk($ac->vendedor)->nome;
        //}concertar isso aqui depois..
        
        if(!isset($ac->Procedencia_compra_id) || $ac->Procedencia_compra_id == null)
        {
            $ac->Procedencia_compra_id = 9;
        }
        
        $ac->Cliente_id = $ClienteId;
        $ac->valor = $util->moedaViewToBD($Valor);
        $ac->entrada = $util->moedaViewToBD($Entrada);
        
        if($emprestimo)
        {
            $ac->Filial_id = 184; //credshow
        }
        else
        {
            $ac->Filial_id = $filial->id;
        }
        
        $ac->Usuario_id             = Yii::app()->session['usuario']->id;
        $ac->habilitado             = 1;
        $ac->data_cadastro          = date('Y-m-d H:i:s');
        $ac->data_cadastro_br       = date('d/m/Y H:i:s');
        $ac->codigo                 = $codigo;
        $ac->mercadoria             = $mercadoria;

        $configValorMaximo          = ConfigLN::model()->find("habilitado AND parametro = 'valor_total_proposta_permitido' ");
        $valorMaximoPermitido       = 1500;

        if( $configValorMaximo      != NULL )
        {
            $valorMaximoPermitido   = doubleval($configValorMaximo->valor);
        }

        if ( (( $ac->valor - $ac->entrada ) <=  $valorMaximoPermitido) || $filial->NucleoFiliais_id == 120 )
        {
            if ($ac->save())
            {
                for ($i = 0; $i < sizeof($Bens); $i++)
                {
                    AnaliseHasSubgrupoDeProduto::model()->novo($ac->id, $Bens[$i]);
                }

                return [$ac,null];
            }
            else
            {
                ob_start();
                var_dump($ac->getErrors());
                $resultado = ob_get_clean();
                
                return [NULL,$resultado];
            }
        }
        
        else
        {
            return [NULL,"Valor maior que ". number_format($valorMaximoPermitido, 2, ',', '.') ." não é possível."];
        }
    }

    public function listarSeguros()
    {
        $criteria       = new CDbCriteria;

        $criteria->with = ['produto' => [ 'alias' => 'p' ] ];

        $criteria->addInCondition('p.id',           [1], ' AND ');
        $criteria->addInCondition('p.habilitado',   [1], ' AND ');
        $criteria->addInCondition('t.habilitado',   [1], ' AND ');

        return ItemDoEstoque::model()->findAll( $criteria );    
    }
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'Analise_de_Credito';
    }

    public function rules() {

        return array(
            array('Vendedor_id, Cliente_id, Filial_id, Procedencia_compra_id, Usuario_id', 'required'),
            array('Vendedor_id, Cliente_id, Filial_id, habilitado, Procedencia_compra_id, mercadoria_retirada_na_hora, celular_pre_pago, Usuario_id', 'numerical', 'integerOnly' => true),
            array('valor, entrada', 'numerical'),
            array('numero_do_pedido, vendedor, telefone_loja, promotor, alerta', 'length', 'max' => 45),
            array('codigo', 'length', 'max' => 240),
            array('data_cadastro, data_cadastro_br, descricao_do_bem, observacao, mercadoria', 'safe'),
            array('id, codigo, Vendedor_id, Cliente_id, Filial_id, habilitado, mercadoria, data_cadastro, data_cadastro_br, numero_do_pedido, descricao_do_bem, vendedor, telefone_loja, promotor, Procedencia_compra_id, mercadoria_retirada_na_hora, celular_pre_pago, observacao, alerta, valor, entrada, Usuario_id', 'safe', 'on' => 'search'),
        );
    }

    public function getPropostasAnalise($statusProposta, $titulosGerados) {

        $crt = new CDbCriteria();
        $crt->select = '*';
        $crt->addInCondition('Status_Proposta_id', $statusProposta);
        $crt->condition = 'Analise_de_Credito_id=:ac AND titulos_gerados=:tg';
        $crt->order = 'id ASC';
        $crt->params = array(':ac' => $this->id, ':tg' => $titulosGerados);

        return Proposta::model()->findAll($crt);
    }

    public function listItensDaAnalise() {

        $arrItens = array();

        $analiseHasItens = ItemDaAnalise::model()->findAll('Analise_de_Credito_id = ' . $this->id);

        foreach ($analiseHasItens as $ahi) {
            $item = GrupoDeProduto::model()->findByPk($ahi->Grupo_de_Produto_id);
            array_push($arrItens, $item);
        }

        return $arrItens;
    }

    public function listSubgruposDaAnalise() {

        $arrItens = array();

        $analiseHasItens = AnaliseHasSubgrupoDeProduto::model()->findAll('Analise_de_Credito_id = ' . $this->id);

        foreach ($analiseHasItens as $ahi) {
            $item = SubgrupoDeProduto::model()->findByPk($ahi->SubgrupoDeProtudo_id);
            array_push($arrItens, $item);
        }

        return $arrItens;
    }

    public function getProdutoDaAnalise()
    {
        $analiseHasItem = AnaliseHasSubgrupoDeProduto::model()->find('Analise_de_Credito_id = ' . $this->id);

        if( $analiseHasItem !== NULL )
        {
            $item = SubgrupoDeProduto::model()->findByPk($analiseHasItem->SubgrupoDeProtudo_id);
            return $item->descricao;
        }

        else
        {
            return "Outros";
        }
    }

    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'filial' => array(self::BELONGS_TO, 'Filial', 'Filial_id'),
            'cliente' => array(self::BELONGS_TO, 'Cliente', 'Cliente_id'),
            'vendedor' => array(self::BELONGS_TO, 'Vendedor', 'Vendedor_id'),
            'procedenciaCompra' => array(self::BELONGS_TO, 'ProcedenciaCompra', 'Procedencia_compra_id'),
            'usuario' => array(self::BELONGS_TO, 'Usuario', 'Usuario_id'),
            'analiseDeCreditoHasCotacaos' => array(self::HAS_MANY, 'AnaliseDeCreditoHasCotacao', 'Analise_de_Credito_id'),
            'itemDaAnalises' => array(self::HAS_MANY, 'ItemDaAnalise', 'Analise_de_Credito_id'),
            'propostas' => array(self::HAS_MANY, 'Proposta', 'Analise_de_Credito_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'codigo' => 'Código',
            'Cliente_id' => 'Cliente',
            'Filial_id' => 'Filial',
            'habilitado' => 'Habilitado',
            'data_cadastro' => 'Data Cadastro',
            'data_cadastro_br' => 'Data / Hora Cadastro',
            'numero_do_pedido' => 'Numero Do Pedido',
            'descricao_do_bem' => 'Descricao Do Bem',
            'vendedor' => 'Vendedor',
            'Vendedor_id' => "Vendedor_id",
            'telefone_loja' => 'Telefone Loja',
            'mercadoria' => 'Mercadoria',
            'promotor' => 'Promotor',
            'Procedencia_compra_id' => 'Procedencia Compra',
            'mercadoria_retirada_na_hora' => 'Mercadoria Retirada Na Hora',
            'celular_pre_pago' => 'Celular Pre Pago',
            'observacao' => 'Observacao',
            'alerta' => 'Alerta',
            'valor' => 'Valor',
            'entrada' => 'Entrada',
            'Usuario_id' => 'Usuario',
        );
    }

    public function search($userId = NULL) {

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('codigo', $this->codigo);
        $criteria->compare('Cliente_id', $this->Cliente_id);
        $criteria->compare('Filial_id', $this->Filial_id);
        $criteria->compare('habilitado', $this->habilitado);
        $criteria->compare('data_cadastro', $this->data_cadastro, true);
        $criteria->addSearchCondition('data_cadastro_br', $this->data_cadastro_br, true);
        //$criteria->compare('data_cadastro_br',$this->data_cadastro_br,true);
        $criteria->compare('numero_do_pedido', $this->numero_do_pedido, true);
        $criteria->compare('descricao_do_bem', $this->descricao_do_bem, true);
        $criteria->compare('vendedor', $this->vendedor, true);
        $criteria->compare('Vendedor_id', $this->Vendedor_id, true);
        $criteria->compare('telefone_loja', $this->telefone_loja, true);
        $criteria->compare('promotor', $this->promotor, true);
        $criteria->compare('Procedencia_compra_id', $this->Procedencia_compra_id);
        $criteria->compare('mercadoria_retirada_na_hora', $this->mercadoria_retirada_na_hora);
        $criteria->compare('celular_pre_pago', $this->celular_pre_pago);
        $criteria->compare('observacao', $this->observacao, true);
        $criteria->compare('alerta', $this->alerta, true);
        $criteria->compare('valor', $this->valor);
        $criteria->compare('mercadoria', $this->mercadoria);
        $criteria->compare('entrada', $this->entrada);

        if ($userId != NULL)
            $criteria->compare('Usuario_id', $userId);
        else
            $criteria->compare('Usuario_id', $this->Usuario_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function codeAnaliseGenerate() {
        return '000xx';
    }

    public function qtdAnalisesCliente($id_cliente){
        $date = date('Y-m-d'); // the date
        $start_time = $date . ' 00:00:00';
        $end_time = $date . ' 23:59:59';

        return count(AnaliseDeCredito::model()->findAll(
            'habilitado AND Cliente_id = :id_cliente AND data_cadastro BETWEEN :start_time AND :end_time', array(
            ':start_time' => $start_time,
            ':end_time' => $end_time,
            ':id_cliente' => $id_cliente,
            )
        ));
    }

    public function analisesDia() {

        $date = date('Y-m-d'); // the date
        $start_time = $date . ' 00:00:00';
        $end_time = $date . ' 23:59:59';

        return AnaliseDeCredito::model()->findAll(
                    'data_cadastro BETWEEN :start_time AND :end_time', array(
                    ':start_time' => $start_time,
                    ':end_time' => $end_time,
                        )
        );
    }

    public function analisesCrediarista($credId) {

        return AnaliseDeCredito::model()->findAll(array(
                    'order' => 'data_cadastro DESC',
                    'condition' => 'Usuario_id=:uid',
                    'params' => array(':uid' => $credId)
        ));
    }

    public function clearItensDaAnalise() {

        $itensDaAnalise = ItemDaAnalise::model()->findAll('Analise_de_Credito_id = ' . $this->id);

        if (count($itensDaAnalise) > 0) {

            foreach ($itensDaAnalise as $item) {
                $item->delete();
            }
        }
    }

}
