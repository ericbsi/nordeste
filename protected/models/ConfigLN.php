<?php

/**
 * This is the model class for table "ConfigLN".
 *
 * The followings are the available columns in table 'ConfigLN':
 * @property integer $id
 * @property string $parametro
 * @property integer $valor
 * @property string $tipo
 * @property integer $habilitado
 * @property string $data_cadastro
 *
 * The followings are the available model relations:
 * @property ListaNegra[] $listaNegras
 */
class ConfigLN extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ConfigLN';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('parametro, valor, tipo, habilitado, data_cadastro', 'required'),
			array('habilitado', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, parametro, valor, tipo, habilitado, data_cadastro', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'parametro' => 'Parametro',
			'valor' => 'Valor',
			'tipo' => 'Tipo',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
		);
	}

	public function valorDoParametro($parametro){
		$config = ConfigLN::model()->find("habilitado AND parametro = '" . $parametro . "'");
		if($config != null){
			return $config->valor;
		}else{
			return null;
		}
	}

	public function listarParametros(){
		$query = 	"
					SELECT * FROM ConfigLN WHERE habilitado = 1";

		$consulta = Yii::app()->db->createCommand($query)->queryAll();
		$rows = [];
		
		foreach ($consulta as $dado){

        		$row 					= array(
        			'empty' 			=> '<button class="btn_remove_par btn btn-red" style="margin-right:-45px"><i class="fa fa-trash-o"></i></button>'	,
            		'parametro' 		=> $dado['parametro'] 																								,
            		'valor' 			=> $dado['valor']																									,
            		'tipo' 				=> $dado['tipo']																									,
            		'data_cadastro' 	=> $dado['data_cadastro']																							,
            		'descricao' 		=> $dado['descricao']																								,
            		'id'				=> $dado['id']																										,
        		);

        		$rows[] = $row;
      	}

      	return (
            array(
                "data" => $rows
            )
      	);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('parametro',$this->parametro,true);
		$criteria->compare('valor',$this->valor);
		$criteria->compare('tipo',$this->tipo,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ConfigLN the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
