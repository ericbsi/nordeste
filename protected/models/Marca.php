<?php

/**
 * This is the model class for table "Marca".
 *
 * The followings are the available columns in table 'Marca':
 * @property integer $id
 * @property string $descricao
 * @property integer $habilitado
 * @property integer $Fabricante_id
 *
 * The followings are the available model relations:
 * @property Fabricante $fabricante
 * @property Modelo[] $modelos
 */
class Marca extends CActiveRecord
{	

	public function add($Marca){

		$arrReturn = array(
			'hasErrors' => '1',
			'classNtfy' => '',
			'msg' 		=> '',
		);

		if( !empty($Marca['descricao']) )
		{
			$novaMarca				= new Marca;		
			$novaMarca->attributes 	= $Marca;
			$novaMarca->habilitado 	= 1;
			
			if( $novaMarca->save() )
			{
				$arrReturn['classNtfy'] = 'success';
				$arrReturn['msg'] 		= 'Marca cadastrada com sucesso';
				$arrReturn['hasErrors']	= '0';
			}
			else
			{	
				$novaMarca->save();
				$arrReturn['classNtfy'] = 'error';
				$arrReturn['msg'] 		= 'Erro ao cadastrar.';
				$arrReturn['err'] 		= json_encode($novaMarca->getErrors());
			}
		}
		return $arrReturn;
	}

	public function searchMarcaSelec2($q){

		$retorno = null;

		if( !empty( $q ) ){

			$crt 	= new CDbCriteria;
			$crt->addSearchCondition('t.descricao', $q, TRUE, 'OR');

			$marcas 	= Marca::model()->findAll( $crt );

			if( count( $marcas ) > 0 )
			{
				foreach( $marcas as $marca )
				{
					$row = array(
						'id'	=> $marca->id,
						'text'	=> $marca->descricao
					);
					$retorno[] 	= $row;
				}
			}
		}
		return $retorno;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Marca';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('descricao, habilitado, Fabricante_id', 'required'),
			array('habilitado, Fabricante_id', 'numerical', 'integerOnly'=>true),
			array('descricao', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, descricao, habilitado, Fabricante_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'fabricante' => array(self::BELONGS_TO, 'Fabricante', 'Fabricante_id'),
			'modelos' => array(self::HAS_MANY, 'Modelo', 'Marca_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'descricao' => 'Descricao',
			'habilitado' => 'Habilitado',
			'Fabricante_id' => 'Fabricante',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('descricao',$this->descricao,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('Fabricante_id',$this->Fabricante_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Marca the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
