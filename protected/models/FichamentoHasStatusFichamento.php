
<?php

class FichamentoHasStatusFichamento extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Fichamento_has_StatusFichamento';
	}

	public function novo($Fichamento, $FichamentoHasStatusFichamento, $StatusFichamento)
	{

		$nFichamentoHasStatusFichamento 						= new FichamentoHasStatusFichamento;
		$nFichamentoHasStatusFichamento->attributes				= $FichamentoHasStatusFichamento;
		$nFichamentoHasStatusFichamento->StatusFichamento_id	= $StatusFichamento;
		$nFichamentoHasStatusFichamento->Fichamento_id 			= $Fichamento->id;
		$nFichamentoHasStatusFichamento->Cliente 				= $Fichamento->parcela->titulo->proposta->analiseDeCredito->cliente->id;

		$condicoesExistentes 									= FichamentoHasStatusFichamento::model()->find('StatusFichamento_id = ' . $StatusFichamento . ' AND Fichamento_id = ' . $Fichamento->id . ' AND habilitado ');

		if( $condicoesExistentes == NULL )
		{
			if( $nFichamentoHasStatusFichamento->save() )
			{
				return $nFichamentoHasStatusFichamento;
			}
			else
			{
				return NULL;
			}
		}
		else
		{
			return NULL;
		}
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Fichamento_id, StatusFichamento_id, Usuario_id, observacao', 'required'),
			array('Fichamento_id, StatusFichamento_id, Usuario_id, habilitado', 'numerical', 'integerOnly'=>true),
			array('data_cadastro', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, Fichamento_id, StatusFichamento_id, Usuario_id, Cliente, data_cadastro, habilitado, observacao', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'fichamento' 		=> array(self::BELONGS_TO, 'Fichamento', 		'Fichamento_id'),
			'statusFichamento' 	=> array(self::BELONGS_TO, 'StatusFichamento', 	'StatusFichamento_id'),
			'usuario' 			=> array(self::BELONGS_TO, 'Usuario', 			'Usuario_id'),
			'cliente' 			=> array(self::BELONGS_TO, 'Cliente', 			'Cliente'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' 					=> 'ID',
			'Fichamento_id' 		=> 'Fichamento',
			'StatusFichamento_id' 	=> 'Status Fichamento',
			'Usuario_id' 			=> 'Usuario',
			'Cliente' 				=> 'Cliente',
			'data_cadastro' 		=> 'Data Cadastro',
			'habilitado'		 	=> 'Habilitado',
			'observacao' 			=> 'Observacao',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Fichamento_id',$this->Fichamento_id);
		$criteria->compare('StatusFichamento_id',$this->StatusFichamento_id);
		$criteria->compare('Usuario_id',$this->Usuario_id);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('observacao',$this->observacao,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function desabilitar($fichamento, $status)
	{
		$criteria 	= new CDbCriteria;
		$criteria->addInCondition('t.Fichamento_id', 		[$fichamento->id], 	' AND ');
		$criteria->addInCondition('t.StatusFichamento_id', 	[$status], 			' AND ');

		$fhsf 		= FichamentoHasStatusFichamento::model()->find($criteria);

		if( $fhsf != NULL )
		{
			$fhsf->habilitado = 0;
			
			if( $fhsf->update() )
			{
				return TRUE;
			}
			else
			{
				return NULL;
			}
		}

		else
		{
			return NULL;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FichamentoHasStatusFichamento the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
