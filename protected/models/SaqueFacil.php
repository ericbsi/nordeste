<?php

class SaqueFacil extends CActiveRecord
{
	public function enviar($SaqueFacil)
	{
		$util 						= new Util;
		$retorno 					= ['hasErrors' => 0];
		$novoSaque 					= new SaqueFacil;
		$novoSaque->attributes 		= $SaqueFacil;
		$novoSaque->valorSolicitado	= $util->moedaViewToBD($novoSaque->valorSolicitado);
		$novoSaque->nascimento		= $util->view_date_to_bd($novoSaque->nascimento);
		$novoSaque->data_cadastro	= date('Y-m-d');
		$novoSaque->crediarista		= Yii::app()->session['usuario']->id;
		$fatores 					= $this->fatores();

		$retencao 					= ($novoSaque->valorSolicitado/100) * $fatores[$novoSaque->qtdParcelas];

		$novoSaque->valorSaque		= $novoSaque->valorSolicitado - $retencao;

		if( $novoSaque->save() )
		{
			$retorno['saqueId'] 	= $novoSaque->id;

			/*Enviando email*/
			$message            	= new YiiMailMessage;

		    $message->view       	= "notificarIntencaoDeSaque";
		    $message->subject    	= 'Intenção de Saque - Credshow S/A';

		    $parametros            	= [
		    	'email'=>[
		    		'cpf'			=> $novoSaque->cpf,
		    		'nome'			=> $novoSaque->nomeCliente,
		    	]
		    ];

		    $message->setBody($parametros, 'text/html');
		    
		    //$message->addTo('eu@ericvinicius.com.br');
		    $message->addTo('kamila@grupomaremansa.com.br');
		    $message->from 			= 'no-reply@credshow.com.br';
		    
		    Yii::app()->mail->send($message);
		}

		else
		{
			$retorno['errors'] 		= $novoSaque->getErrors();
			$retorno['hasErrors'] 	= 1;
		}

		return $retorno;
	}

	public function fatores(){
		return [0,4.98,6.97,8.96,10.95,12.94,14.93,16.92,18.91,20.90,22.89,24.88,26.87];
	}

	public function simularValorFinal($valor, $parcela){

		$util 			= new Util;
		$fatores 		= $this->fatores();

		$retencao		= ($util->moedaViewToBD($valor)/100) * $fatores[$parcela];
		
		return ['valor'=>'R$ '.number_format($util->moedaViewToBD($valor)-$retencao, 2, ',','.')];
	}

	public function getParcelas($valor){

		$util 			=	new Util;
		$parcelas 		= 	[];
		$qtdParcelas 	= 	12;

		$valor 			= $util->moedaViewToBD($valor);

		for( $i = 1; $i <= $qtdParcelas; $i++ )
		{	
			$valorParcela 	= $valor/$i;

			$parcelas[]		= [
				'id' 		=>	$i,
				'text' 		=>	$i.'x de R$ '. number_format($valorParcela, 2,',','.')
			];
		}

		return ['parcelas'=>$parcelas];
	}

	public function tableName()
	{
		return 'SaqueFacil';
	}

	public function rules()
	{
		return array(
			array('valorSolicitado, banco, agencia, conta, valorSaque, qtdParcelas, cpf, nomeCliente, nascimento, numeroCartao, bandeiraCartao, codSeguranca, vencimentoCartao, nomeTitular', 'required'),
			array('id, valorSolicitado, data_cadastro, crediarista, valorSaque, qtdParcelas, cpf, nomeCliente, nascimento, numeroCartao, bandeiraCartao, codSeguranca, vencimentoCartao, nomeTitular', 'safe', 'on'=>'search'),
		);
	}


	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' 				=> 'Id',
			'valorSolicitado' 	=> 'Valor',
			'valorSaque' 		=> 'Valor Parcela',
			'qtdParcelas' 		=> 'Quantidades Parcela',
			'cpf' 				=> 'CPF',
			'nomeCliente' 		=> 'Nome',
			'nascimento' 		=> 'Nascimento',
			'numeroCartao' 		=> 'Numero do cartao',
			'bandeiraCartao' 	=> 'Bandeira Cartão',
			'codSeguranca' 		=> 'Codigo de segurança',
			'vencimentoCartao' 	=> 'Vencimento',
			'nomeTitular' 		=> 'Nome do titular',
			'crediarista' 		=> 'Crediarista',
			'data_cadastro'		=> 'Data de Cadastro',
			'banco' 			=> 'Banco',
			'agencia' 			=> 'Agencia',
			'conta'				=> 'Conta'
		);
	}

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria 	= 	new CDbCriteria;

		/*
		$criteria->compare('id_estado',$this->id_estado);
		$criteria->compare('codigo_ibge',$this->codigo_ibge,true);
		$criteria->compare('sigla',$this->sigla,true);
		$criteria->compare('nome',$this->nome,true);
		$criteria->compare('dtm_lcto',$this->dtm_lcto,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
		*/
	}

	public function historicoCrediarista($draw, $start)
	{

		$util 						= new Util;
		$rows 						= [];

		$criteria 					= new CDbCriteria;
		$criteria->addInCondition('t.crediarista', [ Yii::app()->session['usuario']->id ], 'AND');

		foreach( SaqueFacil::model()->findAll( $criteria ) as $saque )
		{	
			$btnPrinter 			= '<form method="POST" target="_blank" action="/saqueFacil/comprovante/"><input type="hidden" value="'.$saque->id.'" name="id"><button class="submit"><i class="fa fa-print"></i></button></form>';

			$rows[] 				= [
				'cliente'			=> strtoupper($saque->nomeCliente),
				'cpf'				=> strtoupper($saque->cpf),
				'valorInicial'		=> number_format($saque->valorSolicitado, 2, ',','.'),
				'parcelamento'		=> $saque->getParcelamento(),
				'valorSaque'		=> $saque->getValorSaque(),
				'dataCadastro'		=> $util->bd_date_to_view($saque->data_cadastro),
				'btnPrinter'		=> $btnPrinter
			];
		}

		return [
            "draw"              	=> $draw,
            "recordsTotal"      	=> count($rows),
            "recordsFiltered"   	=> count($rows),
            "data"              	=> $rows
        ];

	}

	public function getValorSaque(){
		
		$util 			= new Util;
		$fatores 		= $this->fatores();

		$retencao		= ($this->valorSolicitado/100) * $fatores[$this->qtdParcelas];
		
		return number_format($this->valorSolicitado - $retencao, 2, ',', '.');
	}

	public function getParcelamento()
	{	
		$valorParcela 			= 	$this->valorSolicitado / $this->qtdParcelas;

		return $this->qtdParcelas." x " . number_format($valorParcela, 2, ',', '.');
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
