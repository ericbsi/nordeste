<?php

/**
 * This is the model class for table "NucleoFiliais".
 *
 * The followings are the available columns in table 'NucleoFiliais':
 * @property integer $id
 * @property string $data_cadastro
 * @property integer $habilitado
 * @property integer $GrupoFiliais_id
 *
 * The followings are the available model relations:
 * @property Filial[] $filials
 * @property GrupoFiliais $grupoFiliais
 */
class NucleoFiliais extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return NucleoFiliais the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'NucleoFiliais';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nome, data_cadastro, GrupoFiliais_id', 'required'),
			array('habilitado, GrupoFiliais_id', 'numerical', 'integerOnly'=>true),
                        array('nome', 'length', 'max' => 200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nome, data_cadastro, habilitado, GrupoFiliais_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'filials'                  => array(self::HAS_MANY,   'Filial'                         , 'NucleoFiliais_id', 'on'=>'filials.habilitado = 1'),
			'grupoFiliais'             => array(self::BELONGS_TO, 'GrupoFiliais'                   , 'GrupoFiliais_id'),
            'nucleoHasDadosBancarios'  => array(self::HAS_MANY,   'NucleoFiliaisHasDadosBancarios' , 'NucleoFiliais_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nome' => 'Nome',
			'data_cadastro' => 'Data Cadastro',
			'habilitado' => 'Habilitado',
			'GrupoFiliais_id' => 'Grupo Filiais',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nome',$this->nome);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('GrupoFiliais_id',$this->GrupoFiliais_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function listarParaProducao($idGrupo = null, $idFiliais = null){
		$retorno = []                               ;
            
            $query = "  SELECT DISTINCT NF.id, NF.nome 
                        FROM        NucleoFiliais 		AS NF	
                        INNER JOIN  Filial 				AS F 	ON NF.id    =  F.NucleoFiliais_id  AND   F.habilitado	
                        INNER JOIN  Empresa             AS E    ON E.id 	=  F.Empresa_id
                        INNER JOIN  Empresa_has_Usuario AS EHU  ON E.id     =  EHU.Empresa_id
                        INNER JOIN  Usuario             AS U    ON U.id     =  EHU.Usuario_id
                        WHERE NF.habilitado AND U.id = " . Yii::app()->session['usuario']->id;
                         
            if(isset($idGrupo))
            {
                $query .= " AND NF.GrupoFiliais_id =  $idGrupo ";
            }
                         
            if(isset($idFiliais))
            {
                $query .= " AND F.id IN ($idFiliais) ";
            }
            
            $query .= " ORDER BY 2";
            
            $linhas = Yii::app()->db->createCommand($query)->queryAll();
            
            foreach ($linhas as $linha)
            {
                
                $retorno[] = NucleoFiliais::model()->findByPk($linha["id"]);
                
            }
            
            return $retorno;
	}

	//função utilizada para listar os núcleos, utilizada na tela principal do módulo de cobrança
	public function listarParaCobranca($idGrupo = null, $idFiliais = null)
        {
            
            $retorno = []                               ;
            
            $query = "  SELECT DISTINCT NF.id, NF.nome 
                        FROM        NucleoFiliais 	AS NF	
                        INNER JOIN  Filial 		AS F 	ON NF.id    =   F.NucleoFiliais_id  AND   F.habilitado	
                        WHERE NF.habilitado ";
                         
            if(isset($idGrupo))
            {
                $query .= " AND NF.GrupoFiliais_id =  $idGrupo ";
            }
                         
            if(isset($idFiliais))
            {
                $query .= " AND F.id IN ($idFiliais) ";
            }
            
            $query .= "ORDER BY 2";
            
            $linhas = Yii::app()->db->createCommand($query)->queryAll();
            
            foreach ($linhas as $linha)
            {
                
                $retorno[] = NucleoFiliais::model()->findByPk($linha["id"]);
                
            }
            
            return $retorno;
            
        }
}