<?php

class Dialogo extends CActiveRecord
{	

	public function getMensagens()
	{
		return Mensagem::model()->findAll('Dialogo_id = ' . $this->id);
	}
	
	public function getProposta()
	{
		return Proposta::model()->findByPk($this->Entidade_Relacionamento_id);
	}

	public function novo($assunto, $EntidadeRelacionamento, $EntidadeRelacionamentoId, $CriadoPor, $Mensagem = NULL)
	{
		$dialogo                                = new Dialogo;
        $dialogo->assunto                       = $assunto;
        $dialogo->data_criacao                  = date('Y-m-d H:i:s');
        $dialogo->habilitado                    = 1;
        $dialogo->Entidade_Relacionamento       = $EntidadeRelacionamento;
        $dialogo->Entidade_Relacionamento_id    = $EntidadeRelacionamentoId;
        $dialogo->criado_por                    = $CriadoPor;

        if( $dialogo->save() )
        {
        	if( $Mensagem != NULL )
        	{
        		if( Mensagem::model()->nova( $Mensagem, $dialogo, Yii::app()->session['usuario']->id ) != NULL )
        		{
        			return $dialogo;
        		}
        		else
        		{
        			return NULL;
        		}
        	}
        	else
        	{
        		return $dialogo;
        	}

        }
        else
        {
        	return NULL;
        }
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Dialogo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('habilitado, Entidade_Relacionamento_id, criado_por', 'numerical', 'integerOnly'=>true),
			array('Entidade_Relacionamento', 'length', 'max'=>100),
			array('assunto, data_criacao', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, assunto, data_criacao, habilitado, Entidade_Relacionamento, Entidade_Relacionamento_id, criado_por', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'criadoPor' => array(self::BELONGS_TO, 'Usuario', 'criado_por'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'assunto' => 'Assunto',
			'data_criacao' => 'Data Criacao',
			'habilitado' => 'Habilitado',
			'Entidade_Relacionamento' => 'Entidade Relacionamento',
			'Entidade_Relacionamento_id' => 'Entidade Relacionamento',
			'criado_por' => 'Criado Por',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('assunto',$this->assunto,true);
		$criteria->compare('data_criacao',$this->data_criacao,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('Entidade_Relacionamento',$this->Entidade_Relacionamento,true);
		$criteria->compare('Entidade_Relacionamento_id',$this->Entidade_Relacionamento_id);
		$criteria->compare('criado_por',$this->criado_por);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Dialogo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
