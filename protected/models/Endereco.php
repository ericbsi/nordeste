<?php

class Endereco extends CActiveRecord
{

    public function enderecoToXml($tipo)
    {
        return '
					<endereco>
	                    <tipo>EMPREGO</tipo>
	                    <rua>' . strtoupper($this->logradouro) . '</rua>
	                    <numero/>
	                	<complemento>NR:' . $this->numero . ' ' . strtoupper($this->complemento) . '</complemento>
	                    <bairro>' . strtoupper($this->bairro) . '</bairro>
	                    <cidade>' . strtoupper($this->cidade) . '</cidade>
	                    <estado>' . strtoupper($this->uf) . '</estado>
	                    <cep>' . strtoupper($this->cep) . '</cep>
	                </endereco>';
    }

    public function persist($EnderecoCliente, $clienteId, $EnderecoId, $action)
    {
        if ($action == 'new')
        {
            $EnderecoCliente['endereco_de_cobranca'] = 1;

            return $this->novo($EnderecoCliente, $clienteId);
        } else if ($action == 'update')
        {
            return $this->atualizar($EnderecoCliente, $EnderecoId, $clienteId);
        }
    }

    public function atualizar($Endereco, $EnderecoId, $Cliente_id)
    {

        $arrReturn = array('hasErrors' => false);

        $endereco = Endereco::model()->findByPk($EnderecoId);
        $endereco->attributes = $Endereco;
        $endereco->endereco_de_cobranca = 1;

        if ($endereco->update())
        {
            $arrReturn['msgConfig']['pnotify'][] = array(
                'titulo' => 'Ação realizada com sucesso!',
                'texto' => 'O endereço de cobrança foi autalizado!',
                'tipo' => 'success',
            );

            $cliente = Cliente::model()->findByPk($Cliente_id);

            if ($endereco->endereco_de_cobranca)
            {
                
                if(isset($cliente->pessoa->id))
                {
                
                    $pheAtuais = PessoaHasEndereco::model()->findAll('Endereco_id <> ' . $endereco->id . ' AND Pessoa_id = ' . $cliente->pessoa->id);

                    foreach ($pheAtuais as $pheA)
                    {

                        $end = Endereco::model()->findByPk($pheA->Endereco_id);

                        if ($end->endereco_de_cobranca)
                        {
                            
                            $end->endereco_de_cobranca = 0;
                            
                            if(!$end->update())
                            {
                                
                                ob_start()                      ;
                                var_dump($end->getErrors())     ;
                                $erroEndereco = ob_get_clean()  ;

                                $arrReturn['hasErrors'  ] = true            ;
                                $arrReturn['texto'      ] = $erroEndereco   ;
                                $arrReturn['tipo'       ] = "error"         ;
                                
                            }
                            
                        }
                    }
                    
                }
                else
                {
                    
                    $arrReturn['hasErrors'  ] = true                                                                        ;
                    $arrReturn['texto'      ] = "Problema com o cadastro do cliente. Favor, informar ao desenvolvimento."   ;
                    $arrReturn['tipo'       ] = "error"                                                                     ;
                    
                }
                
            }
        } 
        else
        {
            
            ob_start()                          ;
            var_dump($endereco->getErrors())    ;
            $erroEndereco = ob_get_clean()      ;
            
            $arrReturn['hasErrors'  ] = true            ;
            $arrReturn['texto'      ] = $erroEndereco   ;
            $arrReturn['tipo'       ] = "error"         ;
        }

        return $arrReturn;
    }

    public function loadFormEdit($id)
    {

        $endereco = Endereco::model()->findByPk($id);

        return array(
            'fields' => array(
                'id' => array(
                    'value' => $endereco->id,
                    'type' => 'text',
                    'input_bind' => 'endereco_id',
                ),
                'ControllerAction' => array(
                    'value' => 'update',
                    'type' => 'text',
                    'input_bind' => 'ControllerEnderecoAction',
                ),
                'cep' => array(
                    'value' => $endereco->cep,
                    'type' => 'text',
                    'input_bind' => 'cep_cliente',
                ),
                'logradouro' => array(
                    'value' => $endereco->logradouro,
                    'type' => 'text',
                    'input_bind' => 'logradouro_cliente',
                ),
                'cidade' => array(
                    'value' => $endereco->cidade,
                    'type' => 'text',
                    'input_bind' => 'cidade_cliente',
                ),
                'bairro' => array(
                    'value' => $endereco->bairro,
                    'type' => 'text',
                    'input_bind' => 'bairro_cliente',
                ),
                'tempo_de_residencia' => array(
                    'value' => $endereco->tempo_de_residencia,
                    'type' => 'text',
                    'input_bind' => 'tempo_de_residencia',
                ),
                'Tipo_Moradia_id' => array(
                    'value' => $endereco->Tipo_Moradia_id,
                    'type' => 'select',
                    'input_bind' => 'EnderecoCliente_Tipo_Moradia_id',
                ),
                'numero' => array(
                    'value' => $endereco->numero,
                    'type' => 'text',
                    'input_bind' => 'numero_end_cliente',
                ),
                'complemento' => array(
                    'value' => $endereco->complemento,
                    'type' => 'text',
                    'input_bind' => 'complemento_end_cliente',
                ),
                'complemento' => array(
                    'value' => $endereco->uf,
                    'type' => 'select',
                    'input_bind' => 'EnderecoCliente_uf',
                ),
                'Tipo_Endereco_id' => array(
                    'value' => $endereco->Tipo_Endereco_id,
                    'type' => 'select',
                    'input_bind' => 'EnderecoCliente_Tipo_Endereco_id',
                ),
                'Endereco_eh_entrega' => array(
                    'value' => $endereco->endereco_de_cobranca,
                    'type' => 'select',
                    'input_bind' => 'EnderecoCliente_endereco_de_cobranca',
                )
            ),
            'form_params' => array(
                'id' => 'form-add-endereco-client',
                'action' => '/endereco/update/',
            ),
            'modal_id' => 'modal_form_new_endereco'
        );
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'Endereco';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('Tipo_Endereco_id', 'required'),
            array('habilitado, endereco_de_cobranca, Tipo_Endereco_id, Tipo_Moradia_id, entrega', 'numerical', 'integerOnly' => true),
            array('logradouro', 'length', 'max' => 145),
            array('numero, cep', 'length', 'max' => 45),
            array('tempo_de_residencia', 'length', 'max' => 20),
            array('complemento, cidade, bairro', 'length', 'max' => 100),
            array('uf', 'length', 'max' => 2),
            array('data_cadastro, data_cadastro_br', 'safe'),
            array('id, logradouro, numero, tempo_de_residencia, complemento, cidade, bairro, uf, cep, habilitado, data_cadastro, data_cadastro_br, Tipo_Endereco_id, endereco_de_cobranca, Tipo_Moradia_id, entrega', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'tipoEndereco' => array(self::BELONGS_TO, 'TipoEndereco', 'Tipo_Endereco_id'),
            'tipoMoradia' => array(self::BELONGS_TO, 'TipoMoradia', 'Tipo_Moradia_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'logradouro' => 'Logradouro',
            'numero' => 'Numero',
            'complemento' => 'Complemento',
            'cidade' => 'Cidade',
            'bairro' => 'Bairro',
            'uf' => 'Uf',
            'cep' => 'Cep',
            'habilitado' => 'Habilitado',
            'data_cadastro' => 'Data Cadastro',
            'data_cadastro_br' => 'Data / Hora Cadastro',
            'tempo_de_residencia' => 'Tempo de residência',
            'Tipo_Endereco_id' => 'Tipo Endereco',
            'endereco_de_cobranca' => 'Endereço de cobrança',
            'Tipo_Moradia_id' => 'Tipo de Moradia',
            'entrega' => 'Entrega'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('logradouro', $this->logradouro, true);
        $criteria->compare('numero', $this->numero, true);
        $criteria->compare('complemento', $this->complemento, true);
        $criteria->compare('cidade', $this->cidade, true);
        $criteria->compare('bairro', $this->bairro, true);
        $criteria->compare('uf', $this->uf, true);
        $criteria->compare('cep', $this->cep, true);
        $criteria->compare('habilitado', $this->habilitado);
        $criteria->compare('endereco_de_cobranca', $this->endereco_de_cobranca);
        $criteria->compare('tempo_de_residencia', $this->tempo_de_residencia);
        $criteria->compare('data_cadastro', $this->data_cadastro, true);
        $criteria->compare('data_cadastro_br', $this->data_cadastro_br, true);
        $criteria->compare('Tipo_Endereco_id', $this->Tipo_Endereco_id);
        $criteria->compare('Tipo_Moradia_id', $this->Tipo_Endereco_id);
        $criteria->compare('endereco_de_cobranca', $this->endereco_de_cobranca);
        $criteria->compare('entrega', $this->entrega);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /* public function atualizar($attributes){

      $endereco = Endereco::model()->findByPk( $attributes['id'] );
      $endereco->attributes = $attributes;
      $endereco->update();
      } */

    public function novo($enderecoAttributes, $cliente_id)
    {

        $arrReturn = array('hasErrors' => true);

        $cliente = Cliente::model()->findByPk($cliente_id);

        $endereco = new Endereco;
        $endereco->attributes = $enderecoAttributes;
        $endereco->endereco_de_cobranca = 1;

        if ($endereco->save())
        {

            $arrReturn['hasErrors'] = false;

            $arrReturn['msgConfig']['pnotify'][] = array(
                'titulo' => 'Ação realizada com sucesso!',
                'texto' => 'O endereço foi cadastrado, e faz parte dos endereços do cliente ' . strtoupper($cliente->pessoa->nome),
                'tipo' => 'success',
            );

            $pessoaHasEndereco = new PessoaHasEndereco;
            $pessoaHasEndereco->Pessoa_id = $cliente->pessoa->id;
            $pessoaHasEndereco->Endereco_id = $endereco->id;

            $pessoaHasEndereco->save();

            if ($enderecoAttributes['endereco_de_cobranca'] == 1)
            {
                $pheAtuais = PessoaHasEndereco::model()->findAll('Pessoa_id = ' . $cliente->pessoa->id);

                foreach ($pheAtuais as $pheA)
                {

                    $end = Endereco::model()->findByPk($pheA->Endereco_id);

                    if ($end->endereco_de_cobranca && $end->id != $endereco->id)
                    {
                        $end->endereco_de_cobranca = 0;
                        $end->save();
                    }
                }
            }

            $sigacLog = new SigacLog;
            $sigacLog->saveLog('Cadastro de Endereço', 'Endereço', $endereco->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Usuario " . Yii::app()->session['usuario']->username . " cadastrou um endereço para o cliente " . $cliente->pessoa->nome, "127.0.0.1", null, Yii::app()->session->getSessionId());
        } else
        {
            $arrReturn['msgConfig']['pnotify'][] = array(
                'titulo' => 'Erros foram encontrados!',
                'texto' => 'Não foi possível cadastrar o endereço. ',
                'tipo' => 'error',
            );
        }


        $statusDoCadastro = $cliente->clienteCadastroStatus();
        $arrReturn['statusDoCadastro'] = $statusDoCadastro['status'];

        return $arrReturn;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Endereco the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
