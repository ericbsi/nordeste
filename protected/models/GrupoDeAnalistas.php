<?php

class GrupoDeAnalistas extends CActiveRecord{

	private $connection;

	
	private function setConn(){

		$this->connection = new CDbConnection(
			Yii::app()->params['dbconf']['dns'],
			Yii::app()->params['dbconf']['user'],
			Yii::app()->params['dbconf']['pass']
		);
	}
	
	public function filialRelationChange( $filialId, $checked ){

		$grupoDeAnalistasHasFilial = GrupoDeAnalistasHasFilial::model()->find('Grupo_de_Analistas_id = ' . $this->id .' AND Filial_id = ' . $filialId);
	
		if ($checked == 'true')
			$checked = true;
		else
			$checked = false;

		if ($grupoDeAnalistasHasFilial == NULL) {

			$gahf = new GrupoDeAnalistasHasFilial;
			$gahf->Filial_id = $filialId;
			$gahf->Grupo_de_Analistas_id = $this->id;
			$gahf->habilitado = 1;
			$gahf->data_cadastro = date('Y-m-d H:i:s');
			$gahf->data_cadastro_br = date('d/m/Y H:i:s');
			
			if ($gahf->save())
				return "Sucesso";
			else
				return "Erro";
		}

		else{
			
			if ( $checked )
				$grupoDeAnalistasHasFilial->habilitado = 1;
			else
				$grupoDeAnalistasHasFilial->habilitado = 0;

			if ($grupoDeAnalistasHasFilial->update())
				return "Sucesso";
			else
				return "Erro";
		}
	}

	public function listFiliais(){

		$arrIdsFiliais = array();

		$grupoDeAnalistasHasFilial = GrupoDeAnalistasHasFilial::model()->findAll(
			'Grupo_de_Analistas_id = ' . $this->id . ' AND habilitado '
		);

		foreach ( $grupoDeAnalistasHasFilial as $gahf ) {
						
			$filial = Filial::model()->find( 
				'id = ' . $gahf->Filial_id
				. ' AND habilitado = 1'
		    );

		    if ( !in_array($filial->id, $arrIdsFiliais) ) {
		    	array_push($arrIdsFiliais, $filial->id);
		    }
		}

		return $arrIdsFiliais;
	}

	public function grupoHasFilial($filialId){

		$grupoDeAnalistasHasFilial = GrupoDeAnalistasHasFilial::model()->find(
			'Grupo_de_Analistas_id = ' . 
			$this->id . ' AND Filial_id = ' . 
			$filialId . ' AND habilitado'
		);

		if ($grupoDeAnalistasHasFilial != NULL)
			return true;
		return false;
	}

	public function tableName()
	{
		return 'Grupo_de_Analistas';
	}

	public function rules(){
		
		return array(
			array('Empresa_id', 'required'),
			array('habilitado, Empresa_id', 'numerical', 'integerOnly'=>true),
			array('nome', 'length', 'max'=>45),
			array('descricao', 'length', 'max'=>150),
			array('data_cadastro, data_cadastro_br', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, Empresa_id, nome, descricao, habilitado, data_cadastro, data_cadastro_br', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations(){

		return array(

			'EmpresaId' => array(self::BELONGS_TO, 'Empresa', 'Empresa_id'),

		);

	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Empresa_id' => 'Empresa',
			'nome' => 'Nome',
			'descricao' => 'Descricao',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'data_cadastro_br' => 'Data / Hora Cadastro',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */

	public function search( $idsFilter ){


		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Empresa_id',$this->Empresa_id);
		$criteria->compare('nome',$this->nome,true);
		$criteria->compare('descricao',$this->descricao,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('data_cadastro_br',$this->data_cadastro_br,true);

		$criteria->addInCondition("id", $idsFilter);
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GrupoDeAnalistas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
