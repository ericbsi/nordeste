<?php

/**
 * This is the model class for table "Malote_has_RecebimentoDeDocumentacao".
 *
 * The followings are the available columns in table 'Malote_has_RecebimentoDeDocumentacao':
 * @property integer $RecebimentoDeDocumentacao_id
 * @property integer $Malote_id
 * @property integer $id
 * @property string $data_cadastro
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property RecebimentoDeDocumentacao $recebimentoDeDocumentacao
 * @property Malote $malote
 */
class MaloteHasRecebimentoDeDocumentacao extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Malote_has_RecebimentoDeDocumentacao';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('RecebimentoDeDocumentacao_id, Malote_id', 'required'),
			array('RecebimentoDeDocumentacao_id, Malote_id, habilitado', 'numerical', 'integerOnly'=>true),
			array('data_cadastro', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('RecebimentoDeDocumentacao_id, Malote_id, id, data_cadastro, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'recebimentoDeDocumentacao' => array(self::BELONGS_TO, 'RecebimentoDeDocumentacao', 'RecebimentoDeDocumentacao_id'),
			'malote' => array(self::BELONGS_TO, 'Malote', 'Malote_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'RecebimentoDeDocumentacao_id' => 'Recebimento De Documentacao',
			'Malote_id' => 'Malote',
			'id' => 'ID',
			'data_cadastro' => 'Data Cadastro',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('RecebimentoDeDocumentacao_id',$this->RecebimentoDeDocumentacao_id);
		$criteria->compare('Malote_id',$this->Malote_id);
		$criteria->compare('id',$this->id);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MaloteHasRecebimentoDeDocumentacao the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
