<?php

/**
 * This is the model class for table "ItemFicha".
 *
 * The followings are the available columns in table 'ItemFicha':
 * @property integer $id
 * @property integer $Ficha_id
 * @property integer $Ficha_Pessoa_id
 * @property integer $OrgaoNegacao_id
 * @property string $data_cadastro
 * @property integer $habilitado
 * @property string $atributo
 * @property integer $quantidade
 * @property string $ocorrencia
 * @property string $valor
 * @property integer $TipoItemFicha_id
 *
 * The followings are the available model relations:
 * @property Ficha $ficha
 * @property Ficha $fichaPessoa
 * @property OrgaoNegacao $orgaoNegacao
 * @property TipoItemFicha $tipoItemFicha
 */
class ItemFicha extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'ItemFicha';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                            array('Ficha_id, data_cadastro, habilitado, TipoItemFicha_id'                                                               , 'required'                                ),
                            array('Ficha_id, OrgaoNegacao_id, habilitado, quantidade, TipoItemFicha_id'                                                 , 'numerical'   , 'integerOnly' => true     ),
                            array('atributo'                                                                                                            , 'length'      , 'max'         =>   45     ),
                            array('valor'                                                                                                               , 'length'      , 'max'         =>  512     ),
                            array('ocorrencia'                                                                                                          , 'safe'                                    ),
                            // The following rule is used by search().
                            // @todo Please remove those attributes that should not be searched.
                            array('id, Ficha_id, OrgaoNegacao_id, data_cadastro, habilitado, atributo, quantidade, ocorrencia, valor, TipoItemFicha_id' , 'safe'        , 'on'          => 'search' ),
                        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                            'ficha'         => array(self::BELONGS_TO   , 'Ficha'           , 'Ficha_id'            ),
                            'orgaoNegacao'  => array(self::BELONGS_TO   , 'OrgaoNegacao'    , 'OrgaoNegacao_id'     ),
                            'tipoItemFicha' => array(self::BELONGS_TO   , 'TipoItemFicha'   , 'TipoItemFicha_id'    ),
                        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        
        return array(
                        'id'                => 'ID'                 ,
                        'Ficha_id'          => 'Ficha'              ,
                        'OrgaoNegacao_id'   => 'Orgao Negacao'      ,
                        'data_cadastro'     => 'Data Cadastro'      ,
                        'habilitado'        => 'Habilitado'         ,
                        'atributo'          => 'Atributo'           ,
                        'quantidade'        => 'Quantidade'         ,
                        'ocorrencia'        => 'Ocorrencia'         ,
                        'valor'             => 'Valor'              ,
                        'TipoItemFicha_id'  => 'Tipo Item Ficha'    ,
                    );
            
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
            // @todo Please modify the following code to remove attributes that should not be searched.

            $criteria=new CDbCriteria;

            $criteria->compare('id'                 , $this->id                         );
            $criteria->compare('Ficha_id'           , $this->Ficha_id                   );
            $criteria->compare('OrgaoNegacao_id'    , $this->OrgaoNegacao_id            );
            $criteria->compare('data_cadastro'      , $this->data_cadastro      , true  );
            $criteria->compare('habilitado'         , $this->habilitado                 );
            $criteria->compare('atributo'           , $this->atributo           , true  );
            $criteria->compare('quantidade'         , $this->quantidade                 );
            $criteria->compare('ocorrencia'         , $this->ocorrencia         , true  );
            $criteria->compare('valor'              , $this->valor              , true  );
            $criteria->compare('TipoItemFicha_id'   , $this->TipoItemFicha_id           );

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ItemFicha the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }
        
}
