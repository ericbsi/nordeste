<?php

class Mensagem extends CActiveRecord
{

	public function tableName()
	{
		return 'Mensagem';
	}


	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Dialogo_id, conteudo', 'required'),
			array('Dialogo_id, lida, habilitado, editada, Usuario_id', 'numerical', 'integerOnly'=>true),
			array('data_criacao, data_ultima_edicao', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, Dialogo_id, conteudo, lida, habilitado, editada, data_criacao, data_ultima_edicao, Usuario_id', 'safe', 'on'=>'search'),
		);
	}

	public function markAsRead( $mensagemId, $usuarioId ){

		if( !empty( $mensagemId ) && !empty( $usuarioId ) )
		{
			$mensagem = Mensagem::model()->findByPk( $mensagemId );

			if( $mensagem != NULL )
			{
				if( $usuarioId != $mensagem->Usuario_id )
				{	

					$mensagem->lida 								= 1;
					$mensagem->update();

					$usuarioLeuMensagem  							= UsuarioLeuMensagem::model()->find('Mensagem_id = '.$mensagem->id. ' AND Usuario_id = ' .$usuarioId . ' AND habilitado');

					if( $usuarioLeuMensagem != NULL )
					{
						$usuarioLeuMensagem->data_ultima_leitura = date('Y-m-d H:i:s');
						$usuarioLeuMensagem->update();
					}
					else
					{
						$usuarioLeuMensagem 						= new UsuarioLeuMensagem;
						$usuarioLeuMensagem->Mensagem_id 			= $mensagem->id;
						$usuarioLeuMensagem->Usuario_id 			= $usuarioId;
						$usuarioLeuMensagem->habilitado 			= 1;
						$usuarioLeuMensagem->data_ultima_leitura 	= date('Y-m-d H:i:s');
						$usuarioLeuMensagem->save();
					}

				}
			}

		}
	}

	public function read($mensagemId, $usuarioId)
	{

		if( !empty( $mensagemId ) && !empty( $usuarioId ) )
		{
			$arrayLoadMensagem = $this->loadFormEdit($mensagemId);
			$this->markAsRead($mensagemId, $usuarioId);
			return $arrayLoadMensagem['fields'];
		}
	}

	public function loadFormEdit($id){

		if( !empty( $id ) )
		{

			$mensagem 	= Mensagem::model()->findByPk($id);

			if( $mensagem != NULL )
			{
			    return array(
			    	'fields' 						=> array(
				    	'id' 						=> array(
				    		'value' 				=> $mensagem->id,
				    		'type' 					=> 'text',
				    		'input_bind'			=> 'mensagem_id',
				    	),
				    	'Dialogo_id'				=> array(
				    		'value' 				=> $mensagem->Dialogo_id,
				    		'type' 					=> 'text',
				    		'input_bind'			=> 'mensagem_dialogo_id',
				    	),
				    	'conteudo'					=> array(
				    		'value' 				=> strtoupper($mensagem->conteudo),
				    		'type' 					=> 'text',
				    		'input_bind'			=> 'mensagem_conteudo',	
				    	),
				    	'lida'						=> array(
				    		'value' 				=> $mensagem->lida,
				    		'type' 					=> 'select',
				    		'input_bind'			=> 'mensagem_lida',	
				    	),
				    	'editada'					=> array(
				    		'value' 				=> $mensagem->editada,
				    		'type' 					=> 'text',
				    		'input_bind'			=> 'mensagem_editada',	
				    	),
				    	'data_criacao'				=> array(
				    		'value' 				=> $mensagem->data_criacao,
				    		'type' 					=> 'text',
				    		'input_bind'			=> 'mensagem_data_criacao',	
				    	),
				    	'data_ultima_edicao'		=> array(
				    		'data_ultima_edicao'	=> $mensagem->data_ultima_edicao,
				    		'type' 					=> 'text',
				    		'input_bind'			=> 'mensagem_data_ultima_edicao',	
				    	)
			    	),
			    	'form_params'			=> array(
			    		'id'				=> 'form-add-msg',
			    		'action'			=> '/mensagem/update/',
			    	),
			    	'modal_id'				=> 'modal_form_new_email'
			    );
			}

			else
			{
				return NULL;
			}

		}

		else
		{
			return NULL;
		}
	}

	public function nova($Conteudo, $Dialogo, $UsuarioId)
   	{
            
      $mensagem = new Mensagem;
      $mensagem->Dialogo_id = $Dialogo->id;
      $mensagem->conteudo = $Conteudo;
      $mensagem->lida = 0;
      $mensagem->habilitado = 1;
      $mensagem->editada = 0;
      $mensagem->data_criacao = date('Y-m-d H:i:s');
      $mensagem->data_ultima_edicao = date('Y-m-d H:i:s');
      $mensagem->Usuario_id = $UsuarioId;
      if ($mensagem->save())
      {
         return $mensagem;
      } else
      {
         return NULL;
      }
   	}

	public function novo( $Mensagem, $Usuario_id, $Dialogo_id, $interno){

		$arrReturn 			= array(
			'msgReturn'		=>'',
			'classNotify'	=>''
		);
		
		if( !empty( $Mensagem['conteudo'] ) )
		{
			$dialogo 							= Dialogo::model()->findByPk( $Dialogo_id );

			$proposta 							= $dialogo->getProposta();

			$novaMensagem 						= new Mensagem;
			$novaMensagem->Dialogo_id 			= $dialogo->id;
			$novaMensagem->conteudo 			= $Mensagem['conteudo'];
			$novaMensagem->lida 				= 0;
			$novaMensagem->habilitado 			= 1;
			$novaMensagem->editada 				= 0;
			$novaMensagem->data_criacao                     = date('Y-m-d H:i:s');
			$novaMensagem->data_ultima_edicao               = date('Y-m-d H:i:s');
			$novaMensagem->Usuario_id 			= $Usuario_id;
            $novaMensagem->interno                          = $interno;

			if( $novaMensagem->save() )
			{
				/*Caso o usuario que esteja enviando a mensagem seja um analista, marcamos a proposta como pendencia*/
				/*Caso o usuario que esteja enviando a mensagem seja um crediarista, marcamos como em analise, para o analista poder pegar*/
				
				if( $proposta->hasOmniConfig() == NULL )
				{
					$arrReturn['msgReturn'] 	= 'Mensagem enviada com sucesso!';
					$arrReturn['classNotify'] 	= 'success';
					
					if( Yii::app()->session['usuario']->tipo_id == 4 && $proposta->Status_Proposta_id  == 1 && $novaMensagem->interno == 0)
					{
						$proposta->Status_Proposta_id  = 9;
						$proposta->update();
					}

					if( Yii::app()->session['usuario']->tipo_id == 5 && $proposta->Status_Proposta_id  == 9 )
					{
						$proposta->Status_Proposta_id  = 4;
						$proposta->update();

						foreach ( AnalistaHasPropostaHasStatusProposta::model()->findAll('Proposta_id = ' . $proposta->id ) as $saa )
						{
							$saa->delete();
						}
					}
				}

				else if( $proposta->hasOmniConfig() != NULL && $proposta->Status_Proposta_id == 9 && !$novaMensagem->interno)
				{
					$arrReturn['msgReturn'] 	= 'Mensagem enviada com sucesso!';
					$arrReturn['classNotify'] 	= 'success';

					$proposta->alterarCondicoesOmni($novaMensagem->conteudo);

					if( Yii::app()->session['usuario']->tipo_id == 5 && $proposta->Status_Proposta_id  == 9 )
					{
						$proposta->Status_Proposta_id  = 4;
						$proposta->update();
					}
				}

				else
				{
					//$novaMensagem->save();
					$arrReturn['msgReturn'] 	= 'A mensagem só pode ser enviada com a proposta com Satutus "Modificar"!';
					$arrReturn['classNotify'] 	= 'error';	
				}
			}

			else
			{
				$novaMensagem->save();
				$arrReturn['msgReturn'] 	= 'Não foi possível enviar a mensagem!';
				//$arrReturn['msgReturn'] 	= json_encode($novaMensagem->getErrors());
				$arrReturn['classNotify'] 	= 'error';
			}
		}

		return $arrReturn;
	}


	public function relations()
	{
		return array(
			'usuario' => array(self::BELONGS_TO, 'Usuario', 'Usuario_id'),
			'dialogo' => array(self::BELONGS_TO, 'Dialogo', 'Dialogo_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Dialogo_id' => 'Dialogo',
			'conteudo' => 'Conteudo',
			'lida' => 'Lida',
			'habilitado' => 'Habilitado',
			'editada' => 'Editada',
			'data_criacao' => 'Data Criacao',
			'data_ultima_edicao' => 'Data Ultima Edicao',
			'Usuario_id' => 'Usuario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Dialogo_id',$this->Dialogo_id);
		$criteria->compare('conteudo',$this->conteudo,true);
		$criteria->compare('lida',$this->lida);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('editada',$this->editada);
		$criteria->compare('data_criacao',$this->data_criacao,true);
		$criteria->compare('data_ultima_edicao',$this->data_ultima_edicao,true);
		$criteria->compare('Usuario_id',$this->Usuario_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mensagem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
