<?php

/**
 * This is the model class for table "Item_do_Inventario".
 *
 * The followings are the available columns in table 'Item_do_Inventario':
 * @property integer $id
 * @property integer $Inventario_id
 * @property integer $Item_do_Estoque_id
 * @property integer $quantidade
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property ItemDoEstoque $itemDoEstoque
 * @property Inventario $inventario
 */
class ItemDoInventario extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Item_do_Inventario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('Inventario_id, Item_do_Estoque_id, quantidade, Status_Item_do_Inventario_id', 'required'),
			array('Inventario_id, Item_do_Estoque_id, quantidade, habilitado, Status_Item_do_Inventario_id', 'numerical', 'integerOnly'=>true),

			array('id, Inventario_id, Item_do_Estoque_id, quantidade, habilitado, Status_Item_do_Inventario_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{

		return array(
			'itemDoEstoque' 			=> array(self::BELONGS_TO, 'ItemDoEstoque', 'Item_do_Estoque_id'),
			'inventario' 				=> array(self::BELONGS_TO, 'Inventario', 'Inventario_id'),
			'statusItemDoInventario' 	=> array(self::BELONGS_TO, 'StatusItemDoInventario', 'Status_Item_do_Inventario_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' 							=> 'ID',
			'Inventario_id' 				=> 'Inventario',
			'Item_do_Estoque_id' 			=> 'Item Do Estoque',
			'quantidade' 					=> 'Quantidade',
			'habilitado' 					=> 'Habilitado',
			'Status_Item_do_Inventario_id' 	=> 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Inventario_id',$this->Inventario_id);
		$criteria->compare('Item_do_Estoque_id',$this->Item_do_Estoque_id);
		$criteria->compare('quantidade',$this->quantidade);
		$criteria->compare('Status_Item_do_Inventario_id',$this->Status_Item_do_Inventario_id);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ItemDoInventario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
