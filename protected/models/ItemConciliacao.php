<?php

/**
 * This is the model class for table "ItemConciliacao".
 *
 * The followings are the available columns in table 'ItemConciliacao':
 * @property integer $id
 * @property string $cnpj
 * @property string $codigoProposta
 * @property string $nomeCliente
 * @property string $cgcCliente
 * @property double $valorProposta
 * @property integer $Conciliacao_id
 * @property string $data_cadastro
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property Conciliacao $conciliacao
 */
class ItemConciliacao extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ItemConciliacao';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('codigoContrato, codigoProposta, nomeCliente, cgcCliente, dataProposta, valorProposta, Conciliacao_id, data_cadastro, habilitado', 'required'),
			array('Conciliacao_id, habilitado, Proposta_id', 'numerical', 'integerOnly'=>true),
			array('valorProposta', 'numerical'),
			array('codigoContrato, codigoProposta, cgcCliente', 'length', 'max'=>45),
			array('nomeCliente', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, codigoContrato, codigoProposta, nomeCliente, cgcCliente, dataProposta, valorProposta, Conciliacao_id, data_cadastro, habilitado, Proposta_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'conciliacao'   => array(self::BELONGS_TO, 'Conciliacao'  , 'Conciliacao_id'),
			'proposta'      => array(self::BELONGS_TO, 'Proposta'     , 'Proposta_id'   ),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'                => 'ID'                 ,
			'codigoContrato'    => 'Contrato Proposta'  ,
			'codigoProposta'    => 'Codigo Proposta'    ,
			'nomeCliente'       => 'Nome Cliente'       ,
			'cgcCliente'        => 'Cgc Cliente'        ,
			'dataProposta'      => 'Data da Proposta'   ,
			'valorProposta'     => 'Valor Proposta'     ,
			'Conciliacao_id'    => 'Conciliacao'        ,
			'data_cadastro'     => 'Data Cadastro'      ,
			'habilitado'        => 'Habilitado'         ,
			'Proposta_id'       => 'Proposta'           ,
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id'                 , $this->id                      );
		$criteria->compare('codigoContrato'     , $this->cnpj            , true  );
		$criteria->compare('codigoProposta'     , $this->codigoContrato  , true  );
		$criteria->compare('nomeCliente'        , $this->nomeCliente     , true  );
		$criteria->compare('cgcCliente'         , $this->cgcCliente      , true  );
		$criteria->compare('dataProposta'       , $this->dataProposta    , true  );
		$criteria->compare('valorProposta'      , $this->valorProposta           );
		$criteria->compare('Conciliacao_id'     , $this->Conciliacao_id          );
		$criteria->compare('data_cadastro'      , $this->data_cadastro   , true  );
		$criteria->compare('habilitado'         , $this->habilitado              );
		$criteria->compare('Proposta_id'        , $this->Proposta_id             );

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ItemConciliacao the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
