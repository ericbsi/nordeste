<?php

class Cobranca extends CActiveRecord {

    public function persist($Cobranca, $action_type, $Cobranca_id) {

        if ($action_type == "save") {
            return $this->novo($Cobranca);
        } else {
            return $this->atualizar($Cobranca, $Cobranca_id);
        }
    }

    public function loadFormEdit($id) {

        $cobranca = Cobranca::model()->findByPk($id);
        $util = new Util;

        return array(
                'fields' => array(
                        'id' => array(
                                'value' => $cobranca->id,
                                'type' => 'text',
                                'input_bind' => 'Cobranca_id',
                        ),
                        'agente' => array(
                                'value' => $cobranca->usuario->nome_utilizador,
                                'type' => 'text',
                                'input_bind' => 'ipt_Usuario_id',
                        ),
                        'data_cobranca' => array(
                                'value' => $util->bd_date_to_view($cobranca->data_cobranca),
                                'type' => 'text',
                                'input_bind' => 'ipt_data_cobranca',
                        ),
                        'Tipo_Conta' => array(
                                'value' => $cobranca->Metodo_cobranca_id,
                                'type' => 'select',
                                'input_bind' => 'select_Metodo_cobranca_id',
                        ),
                        'observacao' => array(
                                'value' => $cobranca->observacao,
                                'type' => 'text',
                                'input_bind' => 'textarea_observacao',
                        ),
                ),
                'form_params' => array(
                        'id' => 'form-add-cobranca',
                        'action' => '/cobranca/add/',
                        'action_type' => 'update',
                ),
                'modal_id' => 'modal_form_new_cobranca'
        );
    }

    public function cobrancasParcela($draw, $Parcela_id, $start) {
        $util = new Util;
        $rows = array();
        $criteria = new CDbCriteria;
        $criteria->addInCondition('Parcela_id', array($Parcela_id), 'AND');
        $criteria->order = 't.data_cobranca asc';

        $recordsTotal = count(Cobranca::model()->findAll($criteria));

        $criteria->offset = $start;
        $criteria->limit = 10;

        foreach (Cobranca::model()->findAll($criteria) as $c) {

            $btn = '<button id="btn_load_model_' . $c->id . '" class="btn btn-xs btn-teal btn-load-cob" data-model-id="' . $c->id . '"><i class="fa fa-edit"></i></button>';

            $row = array(
                    'data_da_cobranca' => $util->bd_date_to_view($c->data_cobranca),
                    'usuario' => $c->usuario->nome_utilizador,
                    'observacao' => $c->observacao,
                    'metodo' => $c->metodoCobranca->descricao,
                    "btn" => $btn
            );

            $rows[] = $row;
        }

        return array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $recordsTotal,
                "recordsFiltered" => count($rows),
                "data" => $rows,
        );
    }

    public function atualizar($Cobranca, $Cobranca_id) {
        $cob = Cobranca::model()->findByPk($Cobranca_id);

        $util = new Util;

        $aReturn = array(
                'hasErrors' => 0,
                'msg' => 'Cobrança atualizada com Sucesso.',
                'pntfyClass' => 'success'
        );

        if ($cob != null) {
            $cob->attributes = $Cobranca;

            if (!$cob->update()) {
                $aReturn['hasErrors'] = 1;
                $aReturn['msg'] = 'Ocorreu um erro.';
                $aReturn['errors'] = $cob->getErrors();
            }
        }

        if ($aReturn['hasErrors']) {
            $aReturn['pntfyClass'] = 'error';
        }

        return $aReturn;
    }

    public function novo($Cobranca) {
        $util = new Util;

        $aReturn = array(
                'hasErrors' => 0,
                'msg' => 'Cobrança registrada com Sucesso.',
                'pntfyClass' => 'success'
        );

        $cobranca = new Cobranca;
        $cobranca->attributes = $Cobranca;
        $cobranca->data_cobranca = $util->view_date_to_bd($Cobranca['data_cobranca']);
        $cobranca->data_registro = date('Y-m-d H:i:s');

        if (!$cobranca->save()) {
            //$cobranca->save();
            $aReturn['hasErrors'] = 1;
            $aReturn['msg'] = 'Ocorreu um erro.';
            $aReturn['errors'] = $cobranca->getErrors();
        }

        if ($aReturn['hasErrors']) {
            $aReturn['pntfyClass'] = 'error';
        }

        return $aReturn;
    }

    public function listarParcelas($cpf) {

        $util = new Util;

        //consultando todos os atrasos existentes no banco
        $sql = " SELECT P.id as 'ParcelaId', P.valor as 'ValorParcela', P.seq as 'Sequencia', P.vencimento as Vencimento, CLI.id as id_cliente ";
        $sql .= " FROM Parcela AS P ";
        $sql .= " INNER JOIN Titulo AS T ON T.id = P.Titulo_id AND T.NaturezaTitulo_id = 1 AND T.VendaW_id IS NULL";
        $sql .= " INNER JOIN Proposta AS Pr ON T.Proposta_id = Pr.id AND Pr.Status_Proposta_id = 2";
        $sql .= " INNER JOIN Analise_de_Credito AS AC ON AC.id = Pr.Analise_de_Credito_id ";
        $sql .= " INNER JOIN Cliente AS CLI ON AC.Cliente_id = CLI.id ";
        $sql .= " INNER JOIN Pessoa AS PES ON CLI.Pessoa_id = PES.id ";
        $sql .= " INNER JOIN Pessoa_has_Documento PHD ON PHD.Pessoa_id = PES.id ";
        $sql .= " INNER JOIN Documento DOC ON DOC.id = PHD.Documento_id AND DOC.Tipo_documento_id = 1 AND DOC.numero = " . "'" . $cpf . "'";
        $sql .= " INNER JOIN Analista_has_Proposta_has_Status_Proposta AS Aps ON Aps.Proposta_id = Pr.id AND Aps.Status_Proposta_id = 2";
        $sql .= " INNER JOIN Usuario AS U ON Aps.Analista_id = U.id ";
        $sql .= " INNER JOIN Ranges_gerados  AS Rg ON Rg.Parcela_id = P.id ";
        $sql .= " LEFT JOIN Atendimento_has_Parcela AS AHP ON AHP.Parcela_id = P.id AND AHP.habilitado = 1";

        $sql .= " WHERE ";
        $sql .= " DATEDIFF('" . date('Y-m-d') . "', P.vencimento) >= 5 "; //Pegar essa diferença por parâmetro
        $sql .= " AND P.valor_atual < P.valor AND P.habilitado AND AHP.id is null";
        $sql .= " ORDER BY P.vencimento ASC ";

        $atrasos = Yii::app()->db->createCommand($sql)->queryAll();
        $row = [];
        $rows = [];

        $button = '';

        foreach ($atrasos as $atraso) {
            $check = '  <label class="checkbox-inline" style="float:left"> ' .
                '       <input class="check_parc" type="checkbox" name="parc_id[]" value="' . $atraso['ParcelaId'] . '" />' .
                '  </label>';

            $row = array(
                    "check" => $check,
                    "id" => $atraso['ParcelaId'],
                    "seq" => strtoupper($atraso['Sequencia']),
                    "valor" => $atraso['ValorParcela'],
                    "vencimento" => $util->bd_date_to_view($atraso['Vencimento']),
                    "button" => $button,
                    "id_cliente" => $atraso['id_cliente']
            );

            $rows[] = $row;
        }

        return (array(
                "data" => $rows
        ));
    }

    public function listarCobrancas($draw, $start, $filiais, $dataDe, $dataAte, $mtodos, $aghoje, $agen, $fich, $cpf, $csc, $fdic, $qtd_atrasos_de, $qtd_atrasos_ate) {
        //verificando se o campo de filiais contém alguma filial marcada e colocando numa formatação que se encaixe na query
        if (!empty($filiais)) {
            $filiais = join(',', $filiais);
        }

        $util = new Util;

        //consultando todos os atrasos existentes no banco
        $sql = "SELECT CLI.id as 'cli_id', PES.nome as 'nome_cliente', Count(DISTINCT P.id) as 'qtd_atrasos', DOC.numero as 'cpf_cliente',P.id as 'ParcelaId', P.valor as 'ValorParcela', U.id as AnalistaId, Aps.Analista_id as Analista, Pr.id AS Proposta, P.* ";
        $sql .= " FROM Parcela AS P ";
        $sql .= " INNER JOIN Titulo AS T ON T.id = P.Titulo_id AND T.NaturezaTitulo_id = 1 AND T.VendaW_id IS NULL";
        $sql .= " INNER JOIN Proposta AS Pr ON T.Proposta_id = Pr.id AND Pr.Status_Proposta_id = 2 ";
        $sql .= " INNER JOIN Analise_de_Credito AS AC ON AC.id = Pr.Analise_de_Credito_id ";
        $sql .= " INNER JOIN Cliente AS CLI ON AC.Cliente_id = CLI.id ";
        $sql .= " INNER JOIN Pessoa AS PES ON CLI.Pessoa_id = PES.id ";
        $sql .= " INNER JOIN Pessoa_has_Documento PHD ON PHD.Pessoa_id = PES.id ";
        $sql .= " INNER JOIN Documento DOC ON DOC.id = PHD.Documento_id AND DOC.Tipo_documento_id = 1";
        if (!empty($cpf)) {
            $sql .= " AND DOC.numero = " . $cpf;
        }
        $sql .= " INNER JOIN Analista_has_Proposta_has_Status_Proposta AS Aps ON Aps.Proposta_id = Pr.id AND Aps.Status_Proposta_id = 2 ";
        $sql .= " INNER JOIN Usuario AS U ON Aps.Analista_id = U.id ";
        $sql .= " INNER JOIN Ranges_gerados  AS Rg ON Rg.Parcela_id = P.id ";
        $sql .= " LEFT JOIN Fichamento AS F ON F.habilitado AND F.Parcela = P.id";
        $sql .= " LEFT JOIN Fichamento_has_StatusFichamento AS FHS ON FHS.Fichamento_id = F.id AND FHS.habilitado";
        $sql .= " LEFT JOIN Atendimento_has_Parcela AS AHP ON AHP.Parcela_id = P.id AND AHP.habilitado = 1";
        $sql .= " LEFT JOIN Agendamento AS Agen ON Agen.Atendimento_id = AHP.Atendimento_id";
        if (!empty($filiais)) {
            $sql .= " INNER JOIN Filial as FI ON FI.id = AC.Filial_id ";
        }
        $sql .= " WHERE ";
        if ($mtodos == 1) {
            $sql .= " (Agen.id is not null OR Agen.id is null) AND ";
        } else {
            if ($agen == 1) {
                $sql .= " Agen.id is not null AND DATEDIFF('" . date('Y-m-d') . "', Agen.data_interacao) <= 0 AND ";
            } else {
                if ($aghoje == 1) {
                    $sql .= " Agen.data_interacao = '" . date('Y-m-d') . "' AND Agen.id is not null AND ";
                } else {
                    $sql .= " (Agen.data_interacao = '" . date('Y-m-d') . "' OR Agen.id is null) AND ";
                }
            }
            if ($fich == 1) {
                $sql .= " FHS.id is not null AND ";
            } else {
                $sql .= " FHS.id is null AND ";
            }
        }
        $sql .= " DATEDIFF('" . date('Y-m-d') . "', P.vencimento) >= 5 "; //Pegar essa diferença por parâmetro
        if (!empty($dataDe)) {
            $sql .= " AND LEFT(Pr.data_cadastro,10) >= '" . $dataDe . "'";
        }
        if (!empty($dataAte)) {
            $sql .= " AND LEFT(Pr.data_cadastro,10) <= '" . $dataAte . "'";
        }
        $sql .= " AND P.valor_atual < P.valor AND P.habilitado ";
        if (!empty($filiais)) {
            $sql .= " AND FI.id IN($filiais) ";
        }

        if ($csc == 1) {
            $sql .= " AND  Pr.Financeira_id = 10 ";
        }

        if( $fdic == 1 ){
            $sql .= " AND Pr.Financeira_id = 11 ";
        }

        $sql .= " GROUP BY PES.id "; //Agrupando os resultados por cliente, para que seja possível verificar quantas parcelas em atraso cada um tem

        if( $qtd_atrasos_de != '0' && $qtd_atrasos_ate != '0'){

            $sql .= " HAVING Count(DISTINCT P.id) BETWEEN " .$qtd_atrasos_de. " AND ". $qtd_atrasos_ate . " ";
        }

        $sql .= " ORDER BY CASE WHEN Agen.data_interacao IS NULL THEN 1 ELSE 0 END, Agen.data_interacao ASC, P.vencimento ASC ";

        $atrasos = Yii::app()->db->createCommand($sql)->queryAll();
        $recordsTotal = count($atrasos);
        $recordsFiltered = count($atrasos);

        // if( isset($pagina) && trim($pagina) != '' && !empty($pagina) && $pagina != Null ){
        //   $start = (($pagina-1) * 10);
        // }

        $sql .= " LIMIT $start,10 ";
        $atrasos = Yii::app()->db->createCommand($sql)->queryAll();
        $row = [];
        $rows = [];
        $agenda = "";
        $sql2 = "";
        foreach ($atrasos as $atraso) {
            $sql2 = "SELECT Agen.id as 'Age', PES.nome as 'nome_cliente', Count(DISTINCT P.id) as 'qtd_atrasos', DOC.numero as 'cpf_cliente',P.id as 'ParcelaId', P.valor as 'ValorParcela'
                        FROM Parcela AS P
                        INNER JOIN Titulo AS T ON T.id = P.Titulo_id AND T.NaturezaTitulo_id = 1 AND T.VendaW_id IS NULL
                        INNER JOIN Proposta AS Pr ON T.Proposta_id = Pr.id AND Pr.Status_Proposta_id = 2
                        INNER JOIN Analise_de_Credito AS AC ON AC.id = Pr.Analise_de_Credito_id
                        INNER JOIN Cliente AS CLI ON AC.Cliente_id = CLI.id
                        INNER JOIN Pessoa AS PES ON CLI.Pessoa_id = PES.id
                        INNER JOIN Pessoa_has_Documento PHD ON PHD.Pessoa_id = PES.id
                        INNER JOIN Documento DOC ON DOC.id = PHD.Documento_id AND DOC.Tipo_documento_id = 1 AND DOC.numero = '" . $atraso['cpf_cliente'] . "'
                        INNER JOIN Analista_has_Proposta_has_Status_Proposta AS Aps ON Aps.Proposta_id = Pr.id AND Aps.Status_Proposta_id = 2
                        INNER JOIN Usuario AS U ON Aps.Analista_id = U.id
                        INNER JOIN Ranges_gerados  AS Rg ON Rg.Parcela_id = P.id
                        INNER JOIN Atendimento AS Atend
                        INNER JOIN Atendimento_has_Parcela AS AHP ON AHP.Parcela_id = P.id
                        INNER JOIN Agendamento AS Agen ON Agen.Atendimento_id = AHP.Atendimento_id
                        WHERE
                        DATEDIFF('" . date('Y-m-d') . "', P.vencimento) >= 5 AND
                        DATEDIFF('" . date('Y-m-d') . "', Agen.data_interacao) <= 0
                        AND P.valor_atual < P.valor AND P.habilitado
                        GROUP BY PES.id
                        ORDER BY P.vencimento ASC";
            $qtd_agd = count(Yii::app()->db->createCommand($sql2)->query());
            if ($qtd_agd > 0) {
                $agenda = "<span class='label buttonpulsate'>Possui Agendamento</span>";
            } else {
                $agenda = "";
            }
            $cpf = '<span style="color: #000000;">' . $atraso['cpf_cliente'] . '<span/>';


            $sql_maxid = "SELECT MAX(FHS.id) as id_max
                        FROM Parcela AS P
                        INNER JOIN Titulo AS T ON T.id = P.Titulo_id AND T.NaturezaTitulo_id = 1 AND T.VendaW_id IS NULL
                        INNER JOIN Proposta AS Pr ON T.Proposta_id = Pr.id AND Pr.Status_Proposta_id = 2
                        INNER JOIN Analise_de_Credito AS AC ON AC.id = Pr.Analise_de_Credito_id
                        INNER JOIN Cliente AS CLI ON AC.Cliente_id = CLI.id
                        INNER JOIN Pessoa AS PES ON CLI.Pessoa_id = PES.id
                        INNER JOIN Pessoa_has_Documento PHD ON PHD.Pessoa_id = PES.id
                        INNER JOIN Documento DOC ON DOC.id = PHD.Documento_id AND DOC.Tipo_documento_id = 1 AND DOC.numero = '" . $atraso['cpf_cliente'] . "'
                        INNER JOIN Fichamento AS F ON F.habilitado AND F.Parcela = P.id
                        INNER JOIN Fichamento_has_StatusFichamento AS FHS ON FHS.habilitado AND FHS.Fichamento_id = F.id";

            $max = Yii::app()->db->createCommand($sql_maxid)->queryRow();
            if ($max != null) {
                $fichamento = FichamentoHasStatusFichamento::model()->findByPk($max['id_max']);
                if ($fichamento != null) {
                    $fch = Fichamento::model()->findByPk($fichamento->Fichamento_id);
                    if ($fch != null) {
                        $baixa = Baixa::model()->find('baixado AND Parcela_id = ' . $fch->Parcela);
                        if ($baixa != null && $fichamento->StatusFichamento_id == 2) {
                            $fichamento->habilitado = 0;
                            if ($fichamento->update()) {
                                $fichamentoHasSF = new FichamentoHasStatusFichamento;
                                $fichamentoHasSF->StatusFichamento_id = 5; //criar parametro
                                $fichamentoHasSF->Fichamento_id = $fch->id;
                                $fichamentoHasSF->Cliente = $fch->parcela->titulo->proposta->analiseDeCredito->cliente->id;
                                $fichamentoHasSF->data_cadastro = date("Y-m-d H:i:s");
                                $fichamentoHasSF->habilitado = 1;
                                $fichamentoHasSF->Usuario_id = Yii::app()->session["usuario"]->id;
                                $fichamentoHasSF->observacao = "Baixa identificada no sistema. Id da Baixa: " . $baixa->id;

                                $fichamentoHasSF->save();
                            }
                        }
                    }
                }
            } else {
                $fichamento = null;
            }
            if ($fichamento == null) {
                $fichamento = '<span class="label label-default"> NADA CONSTA </span>';
            } else {
                $fichs = FichamentoHasStatusFichamento::model()->findAll('habilitado AND Cliente = ' . $atraso['cli_id']);
                if (count($fichs) > 0) {
                    foreach ($fichs as $f) {
                        $atend_parc = AtendimentoHasParcela::model()->find('habilitado AND Parcela_id = ' . $f->fichamento->Parcela);
                        if ($atend_parc == null) {
                            $transaction = Yii::app()->db->beginTransaction();

                            $atendimento = new Atendimento;
                            $atendimento->data_cadastro = Date('Y-m-d');
                            $atendimento->habilitado = 1;
                            if ($atendimento->save()) {
                                $chamado = new Chamado;
                                $chamado->habilitado = 1;
                                $chamado->data_cadastro = Date('Y-m-d');
                                $chamado->Atendimento_id = $atendimento->id;

                                if ($chamado->save()) {
                                    $sequencia = new Sequencia;

                                    $sequencia->inicio = Date('Y-m-d');
                                    $sequencia->fim = Date('Y-m-d');
                                    $sequencia->data_cadastro = Date('Y-m-d');
                                    $sequencia->Usuario_id = Yii::app()->session['usuario']->id;
                                    $sequencia->Chamado_id = $chamado->id;
                                    $sequencia->observacao = 'Atendimento criado automaticamente pelo sistema para suprir os fichamentos retroativos advindos da módulo anterior.';
                                    $sequencia->habilitado = 1;

                                    if ($sequencia->save()) {
                                        $ahp = new AtendimentoHasParcela;
                                        $ahp->Atendimento_id = $atendimento->id;
                                        $ahp->Parcela_id = $f->fichamento->Parcela;
                                        $ahp->data_cadastro = Date('Y-m-d');
                                        $ahp->habilitado = 1;

                                        if ($ahp->save()) {
                                            $transaction->commit();
                                        } else {
                                            $transaction->rollBack();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if ($fichamento->statusFichamento->descricao == 'FICHADO') {
                    $cpf = '<span style="color: #d9534f;">' . $atraso['cpf_cliente'] . '<span/>';
                }
                $fichamento = '<span class="' . $fichamento->statusFichamento->cssClass . '">' . $fichamento->statusFichamento->descricao . '</span>';
            }
            $row = array(
                    "cpf" => $cpf,
                    "numeroCPF" => $atraso['cpf_cliente'],
                    "nome_cliente" => strtoupper($atraso['nome_cliente']),
                    "qtd_atrasos" => $atraso['qtd_atrasos'],
                    "maior_atraso" => $util->bd_date_to_view($atraso['vencimento']),
                    "agenda" => $agenda,
                    "fichamento" => $fichamento
            );

            $rows[] = $row;
        }

        return (array(
                "draw" => $draw,
                "start" => $start,
                "recordsFiltered" => $recordsFiltered,
                "recordsTotal" => $recordsTotal,
                "data" => $rows,
                // "sql" => $sql,
                // "sql2" => $sql2
        ));
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Cobranca';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
                array('Usuario_id, Metodo_cobranca_id, Parcela_id, data_cobranca, data_registro, observacao', 'required'),
                array('Usuario_id, Metodo_cobranca_id, Parcela_id', 'numerical', 'integerOnly' => true),
                // The following rule is used by search().
                // @todo Please remove those attributes that should not be searched.
                array('id, Usuario_id, Metodo_cobranca_id, Parcela_id, data_cobranca, data_registro, observacao', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
                'parcela' => array(self::BELONGS_TO, 'Parcela', 'Parcela_id'),
                'usuario' => array(self::BELONGS_TO, 'Usuario', 'Usuario_id'),
                'metodoCobranca' => array(self::BELONGS_TO, 'MetodoCobranca', 'Metodo_cobranca_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'id' => 'ID',
                'Usuario_id' => 'Usuario',
                'Metodo_cobranca_id' => 'Metodo Cobranca',
                'Parcela_id' => 'Parcela',
                'data_cobranca' => 'Data Cobranca',
                'data_registro' => 'Data Registro',
                'observacao' => 'Observacao',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('Usuario_id', $this->Usuario_id);
        $criteria->compare('Metodo_cobranca_id', $this->Metodo_cobranca_id);
        $criteria->compare('Parcela_id', $this->Parcela_id);
        $criteria->compare('data_cobranca', $this->data_cobranca, true);
        $criteria->compare('data_registro', $this->data_registro, true);
        $criteria->compare('observacao', $this->observacao, true);

        return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Cobranca the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
