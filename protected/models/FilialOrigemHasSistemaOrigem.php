<?php

/**
 * This is the model class for table "FilialOrigem_has_SistemaOrigem".
 *
 * The followings are the available columns in table 'FilialOrigem_has_SistemaOrigem':
 * @property integer $FilialOrigem_id
 * @property integer $SistemaOrigem_id
 * @property integer $id
 * @property string $data_cadastro
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property DePara[] $deParas
 * @property FilialOrigem $filialOrigem
 * @property SistemaOrigem $sistemaOrigem
 * @property Migracao[] $migracaos
 * @property PropriedadesConexao[] $propriedadesConexaos
 */
class FilialOrigemHasSistemaOrigem extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'FilialOrigem_has_SistemaOrigem';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('FilialOrigem_id, SistemaOrigem_id, data_cadastro, habilitado', 'required'),
			array('FilialOrigem_id, SistemaOrigem_id, habilitado', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('FilialOrigem_id, SistemaOrigem_id, id, data_cadastro, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'deParas' => array(self::HAS_MANY, 'DePara', 'FilialOrigem_has_SistemaOrigem_id'),
			'filialOrigem' => array(self::BELONGS_TO, 'FilialOrigem', 'FilialOrigem_id'),
			'sistemaOrigem' => array(self::BELONGS_TO, 'SistemaOrigem', 'SistemaOrigem_id'),
			'migracaos' => array(self::HAS_MANY, 'Migracao', 'origem'),
			'propriedadesConexaos' => array(self::HAS_MANY, 'PropriedadesConexao', 'FilialOrigem_has_SistemaOrigem_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'FilialOrigem_id' => 'Filial Origem',
			'SistemaOrigem_id' => 'Sistema Origem',
			'id' => 'ID',
			'data_cadastro' => 'Data Cadastro',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('FilialOrigem_id',$this->FilialOrigem_id);
		$criteria->compare('SistemaOrigem_id',$this->SistemaOrigem_id);
		$criteria->compare('id',$this->id);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FilialOrigemHasSistemaOrigem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
