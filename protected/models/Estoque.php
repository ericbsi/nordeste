<?php


class Estoque extends CActiveRecord
{

	public function novo($Estoque)
	{	
		$aReturn = array(
            'hasErrors'     => 0,
            'msg'           => 'Estoque cadastrado com Sucesso.',
            'pntfyClass'    => 'success'
        );

		if( !empty( $Estoque['Filial_id'] ) && !empty( $Estoque['Local_id'] ) )
        {
        	$estoqueExistente 			= Estoque::model()->find('Filial_id = ' . $Estoque['Filial_id'] . ' AND Local_id = ' . $Estoque['Local_id']);

        	if( $estoqueExistente !== NULL )
        	{
        		$aReturn['hasErrors']	= 1;
				$aReturn['msg']			= 'O estoque deste local já está cadastrado nesta filial.';
        	}

        	else
        	{

        		$estoque 				= new Estoque;
        		$estoque->attributes    = $Estoque;

        		if( !$estoque->save() )
        		{
        			$aReturn['hasErrors']	= 1;
					$aReturn['msg']			= 'Ocorreu um erro.';
        		}
        	}

        	if( $aReturn['hasErrors'] )
			{
				$aReturn['pntfyClass'] 	= 'error';
			}

        }

		return $aReturn;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Estoque';
	}

        public function buscaPorFilial($filialId)
        {
            
            $retorno = [];
            
            $estoques = Estoque::model()->findAll(array('condition' => 'Filial_id = '.$filialId));
            
            foreach ($estoques as $e)
            {
                $retorno[] = array('id'=>$e->id,'text'=>$e->local->nome);
            }
            
            return $retorno;
            
        }
        
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('vende, Local_id, Filial_id', 'required'),
			array('id, vende, Local_id, Filial_id, habilitado', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, vende, Local_id, Filial_id, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'local' => array(self::BELONGS_TO, 'Local', 'Local_id'),
			'filial' => array(self::BELONGS_TO, 'Filial', 'Filial_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'vende' => 'Vende',
			'Local_id' => 'Local',
			'Filial_id' => 'Filial',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('vende',$this->vende);
		$criteria->compare('Local_id',$this->Local_id);
		$criteria->compare('Filial_id',$this->Filial_id);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function listItensEstoque()
	{
		return ItemDoEstoque::model()->findAll('Estoque_id = ' . $this->id . ' AND habilitado');
	}

	public function listItensEstoqueDataTable($draw){

        $rows                           = array();
        $countItens                    	= 0;
        
        $btn                            = "";
        $itens = $this->listItensEstoque();

        if( count($itens) > 0 )
        {
            foreach ( $itens as $item )
            {
                if( $item != NULL )
                {
                    $btn = '<a data-estoque-id="'.$item->Estoque_id.'" data-item-id="'.$item->id.'" class="btn btn-primary btn-add-item-ao-inventario" style="padding:2px 5px!important; font-size:11px!important;"><i class="clip-plus-circle"></i></a>';
                    $countItens++;

                    $row                = array(
                        'produto'       => $item->produto->descricao,
                        'saldo'       	=> $item->saldo,
                        'local'       	=> $item->estoque->local->sigla,
                        'btn'           => $btn,
                    );
                                
                    $rows[] = $row;
                    $btn = '';
                }
            }
        }

        return (array(
            "draw"                      => $draw,
            "recordsTotal"              => $countItens,
            "recordsFiltered"           => $countItens,
            "data"                      => $rows
        ));
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Estoque the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


}
