<?php

class Financeira extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function tableName()
	{
		return 'Financeira';
	}


	public function rules()
	{

		return array(
			array('Contato_id', 'required'),
			array('habilitado, Contato_id', 'numerical', 'integerOnly'=>true),
			array('cnpj', 'length', 'max'=>45),
			array('nome', 'length', 'max'=>100),
			array('data_cadastro, data_cadastro_br', 'safe'),
			array('id, cnpj, nome, habilitado, data_cadastro, data_cadastro_br, Contato_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'contato' => array(self::BELONGS_TO, 'Contato', 'Contato_id'),
		);
	}
	
	public function attributeLabels()
	{
		return array(
			'id' 				=> 'ID',
			'cnpj' 				=> 'Cnpj',
			'nome' 				=> 'Nome',
			'habilitado' 		=> 'Habilitado',
			'data_cadastro' 	=> 'Data Cadastro',
			'data_cadastro_br' 	=> 'Data / Hora Cadastro',
			'Contato_id' 		=> 'Contato',
		);
	}

	public function search()
	{
		$criteria 	= new CDbCriteria;

		$criteria->compare('id',				$this->id);
		$criteria->compare('cnpj',				$this->cnpj,true);
		$criteria->compare('nome',				$this->nome,true);
		$criteria->compare('habilitado',		$this->habilitado);
		$criteria->compare('data_cadastro',		$this->data_cadastro,true);
		$criteria->compare('data_cadastro_br',	$this->data_cadastro_br,true);
		$criteria->compare('Contato_id',		$this->Contato_id);

		return new CActiveDataProvider($this, array(
			'criteria' =>	$criteria,
		));
	}

	/*Funções Maré mansa*/

	public function getDadosBancarios(){

		$financeiraHasDadosBancarios 	= FinanceiraHasDadosBancarios::model()->find('Financeira_id = ' . $this->id);
	
		return DadosBancarios::model()->findByPk( $financeiraHasDadosBancarios->Dados_Bancarios_id );

	}

	public function getCodigoCedente(){

		$contaBancaria 					= $this->getDadosBancarios()->numero;

		return substr($contaBancaria,5);
	}
}