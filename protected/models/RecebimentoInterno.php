<?php

class RecebimentoInterno extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'RecebimentoInterno';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('data_cadastro, habilitado, Filial_id, StatusRI_id, Financeira_id', 'required'),
			array('habilitado, Filial_id, Titulo_id, StatusRI_id, Financeira_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, data_cadastro, habilitado, Filial_id, Titulo_id, StatusRI_id, Financeira_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'filial' => array(self::BELONGS_TO, 'Filial', 'Filial_id'),
			'financeira' => array(self::BELONGS_TO, 'Financeira', 'Financeira_id'),
			'statusRI' => array(self::BELONGS_TO, 'StatusRI', 'StatusRI_id'),
			'titulo' => array(self::BELONGS_TO, 'Titulo', 'Titulo_id'),
			'parcelas' => array(self::MANY_MANY, 'Parcela', 'RecebimentoInterno_has_Parcela(RecebimentoInterno_id, Parcela_id)', 'on'=>'parcelas_parcelas.habilitado = 1'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'data_cadastro' => 'Data Cadastro',
			'habilitado' => 'Habilitado',
			'Filial_id' => 'Filial',
			'Titulo_id' => 'Titulo',
			'StatusRI_id' => 'Status Ri',
			'Financeira_id' => 'Financeira',
		);
	}

	public function segundaViaBoleto()
	{
		$vencimento 								= '';
		$boletoconfig 							= 'credshow_bradesco';
		$valorBoleto 								= 0;
		$arrRetorno 								= [];
		$configBradesco 						= Banco::model()->configuracoesBanco();

		$parcela 										= Parcela::model()->find('Titulo_id = ' . $this->titulo->id);

		foreach ( $this->parcelas as $parc ) {
			$valorBoleto += $parc->valor_atual;
		}

		// if( in_array($this->Financeira_id, [10,11]) ){
		// 	$boletoconfig = 'credshow_bradesco_fidic';
		// }
		// else{
		// 	$boletoconfig = 'credshow_bradesco';
		// }
		if( $parcela != Null ){

			if( $parcela->atrasada() )
			{
				$vencimento 		= date('Y-m-d');
			}

			else
			{
				$vencimento	 			= $parcela->vencimento;
			}

			$r 								= [
				'codigobanco' 	=> '237',
				'cedente' 			=> $configBradesco[$boletoconfig]['r_s'],
				'cpf_cnpj' 			=> $configBradesco[$boletoconfig]['cnpj'],
				'agencia' 			=> substr($configBradesco[$boletoconfig]['agencia'], 1, 4),
				'valor_boleto' 	=> number_format($valorBoleto, 2, ',', ''),
				'sacado' 				=> 'CREDSHOW OPERADORA DE CREDITO ',
				'cpfsacado' 		=> $this->filial->cnpj,
				'endereco1' 		=> $this->filial->getEndereco()->logradouro . ', ' . $this->filial->getEndereco()->numero . ' ' . $this->filial->getEndereco()->complemento,
				'bairro' 				=> $this->filial->getEndereco()->bairro,
				'uf' 						=> $this->filial->getEndereco()->uf,
				'endereco2' 		=> $this->filial->getEndereco()->cep . ' ' . $this->filial->getEndereco()->cidade,
				'vencimento' 		=> $vencimento,
				'range' 				=> $parcela->rangesGerados->label,
				'especie_doc' 	=> 'PD',
				'especie' 			=> 'REAL',
				'aceite' 				=> 'NÃO',
				'quantidade' 		=> '1',
				'instrucoes1' 	=> 'Após o vencimento, mora de ' . ConfigLN::model()->valorDoParametro("taxa_mora") . '% ao dia.',
				'instrucoes2' 	=> 'Atendimento ao cliente: 084 2040-0800',
				'instrucoes4'	 	=> 'Boleto relativo ao pagamento da ' . $parcela->seq,
				'instrucoes5'	 	=> $parcela->seq . '/1',
				'instrucoes6' 	=> 'Atenção: após vencido, o título estará sujeito a inclusão no SPC e SERASA',
				'instrucoes7' 	=> '',
				'seq_parc' 			=> $parcela->seq,
				'valor_corri' 	=> $valorBoleto
			];
			array_push($arrRetorno, $r);
		}
		return $arrRetorno;
	}

	public function gerarBoleto($recebimento)
	{
		$arrRetorno 								= [];
		$configBradesco 						= Banco::model()->configuracoesBanco();
		$transaction 								= Yii::app()->db->beginTransaction();
		$boletoconfig 							= 'credshow_bradesco';
		$zeros 											= "";
		$valorBoleto 								= 0;
		$titulo 										= new Titulo;
		$titulo->prefixo 						= '001';
		$titulo->NaturezaTitulo_id 	= 1;
		$hasError 									= false;

		foreach ( $recebimento->parcelas as $parcela )
		{
			$valorBoleto += $parcela->valor_atual;
		}

		if( $titulo->save() )
		{
			$arrRetorno['titulo_id'] 	= $titulo->id;

			$parcela 								= new Parcela;
			$parcela->seq 					= 1;
			$parcela->valor 				= $valorBoleto;
			$parcela->valor_texto 	= '0';
			$parcela->valor_atual 	= 0;
			$parcela->Titulo_id 		= $titulo->id;
			$parcela->vencimento 		= date( 'Y-m-d', strtotime( date('Y-m-d'). '+ 1 days' ) );

			if( $parcela->save() )
			{
				$range 								= Range::model()->findByPk(4);
				$range->ultimo_gerado = $range->ultimo_gerado + 1;

				for ($y = 1; $y <= 5 - strlen((string) $range->ultimo_gerado); $y ++){
					$zeros .= "0";
				}

				$rangeSeq 											= strval($range->ultimo_gerado);
				$rangeBoleto 										= $zeros . $rangeSeq;

				$rangesGerados 									= new RangesGerados;
				$rangesGerados->numero 					= $rangeBoleto;
				$rangesGerados->data_geracao 		= date('Y-m-d H:i:s');
				$rangesGerados->Range_id 				= $range->id;
				$rangesGerados->Parcela_id 			= $parcela->id;
				$rangesGerados->label 					= strval($range->prefixo) . $zeros . $rangeSeq;
				$rangesGerados->data_cadastro   = date('Y-m-d H:i:s');
				$rangesGerados->habilitado      = 1;

				// if( in_array($recebimento->Financeira_id, [10,11]) )
				// {
				// 	$boletoconfig = 'credshow_bradesco_fidic';
				// }
				// else
				// {
				// 	$boletoconfig = 'credshow_bradesco';
				// }

				if( $rangesGerados->save() )
				{
					$r 								= [
						'codigobanco' 	=> '237',
						'cedente' 			=> $configBradesco[$boletoconfig]['r_s'],
						'cpf_cnpj' 			=> $configBradesco[$boletoconfig]['cnpj'],
						'agencia' 			=> substr($configBradesco[$boletoconfig]['agencia'], 1, 4),
						'valor_boleto' 	=> number_format($valorBoleto, 2, ',', ''),
						'sacado' 				=> 'CREDSHOW OPERADORA DE CREDITO ',
						'cpfsacado' 		=> $recebimento->filial->cnpj,
						'endereco1' 		=> $recebimento->filial->getEndereco()->logradouro . ', ' . $recebimento->filial->getEndereco()->numero . ' ' . $recebimento->filial->getEndereco()->complemento,
						'bairro' 				=> $recebimento->filial->getEndereco()->bairro,
						'uf' 						=> $recebimento->filial->getEndereco()->uf,
						'endereco2' 		=> $recebimento->filial->getEndereco()->cep . ' ' . $recebimento->filial->getEndereco()->cidade,
						'vencimento' 		=> $parcela->vencimento,
						'range' 				=> strval($range->prefixo) . $zeros . $rangeSeq,
						'especie_doc' 	=> 'PD',
						'especie' 			=> 'REAL',
						'aceite' 				=> 'NÃO',
						'quantidade' 		=> '1',
						'instrucoes1' 	=> 'Após o vencimento, mora de ' . ConfigLN::model()->valorDoParametro("taxa_mora") . '% ao dia.',
						'instrucoes2' 	=> 'Atendimento ao cliente: 084 2040-0800',
						'instrucoes4'	 	=> 'Boleto relativo ao pagamento da ' . $parcela->seq,
						'instrucoes5'	 	=> $parcela->seq . '/1',
						'instrucoes6' 	=> 'Atenção: após vencido, o título estará sujeito a inclusão no SPC e SERASA',
						'instrucoes7' 	=> '',
						'seq_parc' 			=> $parcela->seq,
						'valor_corri' 	=> $valorBoleto
					];

					$range->update();
					array_push($arrRetorno, $r);
				}
				else
				{
					$hasError = true;
					$transaction->rollBack();
				}

			}

			else
			{
				$hasError = true;
				$transaction->rollBack();
			}

		}
		else
		{
			$hasError = true;
			$transaction->rollBack();
		}

		if(!$hasError){
			$transaction->commit();
			return $arrRetorno;
		}
		else{
			return null;
		}

	}

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('Filial_id',$this->Filial_id);
		$criteria->compare('Titulo_id',$this->Titulo_id);
		$criteria->compare('StatusRI_id',$this->StatusRI_id);
		$criteria->compare('Financeira_id',$this->Financeira_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
