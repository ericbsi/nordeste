<?php

class ValoresCaracteristica extends CActiveRecord
{	

	public function novo( $ValoresCaracteristica )
	{
		$aReturn = array(
            'hasErrors'     => 0,
            'msg'           => 'Item da Característica cadastrado com Sucesso.',
            'pntfyClass'    => 'success'
        );

		if( !empty( $ValoresCaracteristica['valor'] ) )
        {

        	$valorCaracteristica				= new ValoresCaracteristica;
        	$valorCaracteristica->attributes 	= $ValoresCaracteristica;

        	if( !$valorCaracteristica->save() )
        	{
        		$aReturn['hasErrors']	= 1;
				$aReturn['msg']			= 'Ocorreu um erro.';
        	}
        	
        	if( $aReturn['hasErrors'] )
			{
				$aReturn['pntfyClass'] 	= 'error';
			}
        }

		return $aReturn;
	}

	public function listItensCaracteristica( $draw, $caracteristicaId, $offset ){

		$valoresCaracteristica 			= ValoresCaracteristica::model()->findAll('Caracteristica_id = ' . $caracteristicaId);
		$rows							= arraY();
		
		$criteriaFilter					= new CDbCriteria;
		
		$criteriaFilter->addInCondition('Caracteristica_id', array($caracteristicaId),'AND');
		$criteriaFilter->order 			= 'valor ASC';
		$criteriaFilter->offset 		= $offset;
		$criteriaFilter->limit 			= 10;	



		if( count( $valoresCaracteristica ) > 0 )
		{
			foreach( ValoresCaracteristica::model()->findAll( $criteriaFilter ) as $valor )
			{
				$row 					=  array(
					'valor'				=> $valor->valor,
				);

				$rows[]					= $row;
			}
		}
	
		return (array(
            "draw"                      => $draw,
            "recordsTotal"              => count( $rows ),
            "recordsFiltered"           => count( $valoresCaracteristica ),
            "data"                      => $rows
        ));
	}

	public function actionGetValoresCaracteristica(){

		echo json_encode( CaracteristicaProduto::model()->listCaracteristicas( $_POST['draw'] ) );
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ValoresCaracteristica';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Caracteristica_id, valor', 'required'),
			array('Caracteristica_id, habilitado', 'numerical', 'integerOnly'=>true),
			array('valor', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, Caracteristica_id, valor, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'produtoHasValoresCaracteristicas'	=> array(self::HAS_MANY, 'ProdutoHasValoresCaracteristica', 'ValoresCaracteristica_id'),
			'caracteristica' 					=> array(self::BELONGS_TO, 'CaracteristicaProduto', 'Caracteristica_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Caracteristica_id' => 'Caracteristica',
			'valor' => 'Valor',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Caracteristica_id',$this->Caracteristica_id);
		$criteria->compare('valor',$this->valor,true);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ValoresCaracteristica the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
