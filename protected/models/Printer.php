<?php 

class Printer{

	public function printCapaProducao($dataDe, $dataAte)
	{
		$util = new Util;

		$sql  = "SELECT CONCAT(f.nome_fantasia, ' ',ende.cidade, ' / ', ende.uf) as Filial, COUNT(*) as numero_de_proposta, SUM(p.valor-p.valor_entrada) as valor_total from Proposta as p ";
		$sql .= " INNER JOIN Analise_de_Credito as ac ON ac.id = p.Analise_de_Credito_id ";
		$sql .= " INNER JOIN Filial as f ON f.id = ac.Filial_id";
		$sql .= " INNER JOIN Filial_has_Endereco as fhe ON ac.Filial_id = fhe.Filial_id ";
		$sql .= " INNER JOIN Endereco as ende ON ende.id = fhe.Endereco_id ";
		$sql .= " WHERE p.titulos_gerados AND p.habilitado AND p.Status_Proposta_id IN(2,7) AND p.data_cadastro between '".$util->view_date_to_bd($dataDe) . " 00:00:00' AND ' ".$util->view_date_to_bd($dataAte)." 23:59:59'";
		$sql .= " GROUP BY ac.Filial_id ";
		$sql .= " ORDER BY valor_total DESC";

		return array(
			'resultado' => Yii::app()->db->createCommand($sql)->queryAll(),
			'dataDe'	=> $dataDe,
			'dataAte'	=> $dataAte
		);
	}

	public function printAllProducao( $dataDe, $dataAte, $filiais )
	{
		$util 				= new Util;
		$criteria 			= new CDbCriteria;

		$criteria->with  	= array('analiseDeCredito' => array(
		    'alias'  	=> 'ac'
		));

		if( !empty( $filiais ) )
		{	
			$idsFiliais = array();

			for ($i = 0; $i < count($filiais); $i ++ )
			{
				$idsFiliais[] = $filiais[$i];
			}

			$criteria->addInCondition('ac.Filial_id',	$idsFiliais, ' AND ');
		}

		if( !empty( $dataDe ) && !empty( $dataAte ) )
		{
			$criteria->addBetweenCondition('t.data_cadastro',  	$util->view_date_to_bd($dataDe).' 00:00:00', $util->view_date_to_bd($dataAte).' 23:59:59', 'AND');	
		}

		$criteria->addInCondition('t.habilitado',			array(1), ' AND ');
		$criteria->addInCondition('t.titulos_gerados', 		array(1), ' AND ');		
		$criteria->addInCondition('t.Status_Proposta_id', 	array(2), ' AND ');
		$criteria->order 		  = 't.data_cadastro DESC';
				
		return Proposta::model()->findAll($criteria);
	}

	public function printAllProducaoParceiro($idsPropostas){

		$ids					= join(',',$idsPropostas);
		
		$criteria 				= new CDbCriteria;

		$criteria->condition 	= "t.id IN($ids)";

		return Proposta::model()->findAll($criteria);
	}
}