<?php

/**
 * This is the model class for table "Financeira_has_Endereco".
 *
 * The followings are the available columns in table 'Financeira_has_Endereco':
 * @property integer $id
 * @property integer $Financeira_id
 * @property integer $Endereco_id
 * @property string $data_cadastro
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property Financeira $financeira
 * @property Endereco $endereco
 */
class FinanceiraHasEndereco extends CActiveRecord{
	
public static function model($className=__CLASS__){

		return parent::model($className);
	}

	public function tableName(){

		return 'Financeira_has_Endereco';
	}


	public function rules(){

		return array(
			array('Financeira_id, Endereco_id', 'required'),
			array('Financeira_id, Endereco_id, habilitado', 'numerical', 'integerOnly'=>true),
			array('data_cadastro, data_cadastro_br', 'safe'),
			array('id, Financeira_id, Endereco_id, data_cadastro, data_cadastro_br, habilitado', 'safe', 'on'=>'search'),
		);
	}

	public function relations(){
		
		return array(
			'financeira' => array(self::BELONGS_TO, 'Financeira', 'Financeira_id'),
			'endereco' => array(self::BELONGS_TO, 'Endereco', 'Endereco_id'),
		);
	}

	public function attributeLabels(){

		return array(
			'id' => 'ID',
			'Financeira_id' => 'Financeira',
			'Endereco_id' => 'Endereco',
			'data_cadastro' => 'Data Cadastro',
			'data_cadastro_br' => 'Data / Hora Cadastro',
			'habilitado' => 'Habilitado',
		);
	}

	
	public function search(){
		
		$criteria = new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Financeira_id',$this->Financeira_id);
		$criteria->compare('Endereco_id',$this->Endereco_id);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('data_cadastro_br',$this->data_cadastro_br,true);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}