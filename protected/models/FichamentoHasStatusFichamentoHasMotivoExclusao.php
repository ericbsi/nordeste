<?php

/**
 * This is the model class for table "Fichamento_has_StatusFichamento_has_MotivoExclusao".
 *
 * The followings are the available columns in table 'Fichamento_has_StatusFichamento_has_MotivoExclusao':
 * @property integer $Fichamento_has_StatusFichamento_id
 * @property integer $MotivoExclusao_id
 * @property integer $id
 * @property integer $habilitado
 * @property string $data_cadastro
 *
 * The followings are the available model relations:
 * @property FichamentoHasStatusFichamento $fichamentoHasStatusFichamento
 * @property MotivoExclusao $motivoExclusao
 */
class FichamentoHasStatusFichamentoHasMotivoExclusao extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Fichamento_has_StatusFichamento_has_MotivoExclusao';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Fichamento_has_StatusFichamento_id, MotivoExclusao_id, habilitado, data_cadastro', 'required'),
			array('Fichamento_has_StatusFichamento_id, MotivoExclusao_id, habilitado', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Fichamento_has_StatusFichamento_id, MotivoExclusao_id, id, habilitado, data_cadastro', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'fichamentoHasStatusFichamento' => array(self::BELONGS_TO, 'FichamentoHasStatusFichamento', 'Fichamento_has_StatusFichamento_id'),
			'motivoExclusao' => array(self::BELONGS_TO, 'MotivoExclusao', 'MotivoExclusao_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Fichamento_has_StatusFichamento_id' => 'Fichamento Has Status Fichamento',
			'MotivoExclusao_id' => 'Motivo Exclusao',
			'id' => 'ID',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Fichamento_has_StatusFichamento_id',$this->Fichamento_has_StatusFichamento_id);
		$criteria->compare('MotivoExclusao_id',$this->MotivoExclusao_id);
		$criteria->compare('id',$this->id);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FichamentoHasStatusFichamentoHasMotivoExclusao the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
