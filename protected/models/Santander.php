<?php 
	
class Santander{
	
	private $codigobanco          	= "033";
	private $nummoeda             	= "9";
	private $fixo                 	= "9";
	private $ios	             	= "0";   
	private $carteira 				= "101";
	private $fator_vencimento;
	private $valor;
	private $codigocliente;
	private $codigo_banco_com_dv;
	private $nosso_numero;
	private $barra;
	private $linha;
	private $digito_verificador;

	function __construct( $vencimento, $valorBoleto, $nossoNumero, $codigoDoCliente ){

		$m11 						= new Modulo_11;
		$util 						= new Util;

		$nnum 						= $this->formata_numero($nossoNumero,7,0);
		$dv_nosso_numero      		= $m11->m11($nnum,9,0);

		$vencimento 				= $util->bd_date_to_view($vencimento);
		$this->fator_vencimento    	= $this->fator_vencimento($vencimento);
		$this->valor   	 			= $this->formata_numero($valorBoleto,10,0,"valor");
		$this->codigocliente 		= $this->formata_numero($codigoDoCliente,7,0);
		$this->codigo_banco_com_dv  = $this->geraCodigoBanco($this->codigobanco);
		//$this->nosso_numero 		= "00000".$nnum.$dv_nosso_numero;
		$this->nosso_numero 		= $nnum.$dv_nosso_numero;
		$this->barra               	= "$this->codigobanco$this->nummoeda$this->fator_vencimento$this->valor$this->fixo$this->codigocliente"."00000"."$this->nosso_numero$this->ios$this->carteira";

		$dv                   		= $this->digitoVerificador_barra($this->barra);
		$this->linha                = substr($this->barra,0,4) . $dv . substr($this->barra,4);
   	}

   	public function getCodigoDeBarras(){

   		return $this->linha;
   	}

   	public function getCodigoDoBanco()
   	{
   		return $this->codigo_banco_com_dv;
   	}

   	public function getNossoNumero(){

   		return $this->nosso_numero;
   	}
   	  
   	public function getCodigoAgencia(){

   		return $this->formata_numero('3211',7,0);
   	}

   	public function digitoVerificador_barra( $numero ){
  		
   		$m11 		= new Modulo_11;

  		$resto2 	= $m11->m11($numero, 9, 1);
  
	  	if ($resto2 == 0 || $resto2 == 1 || $resto2 == 10)
	  	{
	        $dv = 1;
	  	}

	  	else{
			$dv = 11 - $resto2;
	  	}

		return $dv;
	}

   	public function formata_numero($numero,$loop,$insert,$tipo = "geral"){
	
	  	if ($tipo == "geral")
	  	{
			$numero = str_replace(",","",$numero);
			while(strlen($numero)<$loop){
				$numero = $insert . $numero;
			}
		}

		if ($tipo == "valor")
		{
			/*
			retira as virgulas
			formata o numero
			preenche com zeros
			*/
			$numero = str_replace(",","",$numero);
			while(strlen($numero)<$loop){
				$numero = $insert . $numero;
			}
		}
		if ($tipo == "convenio")
		{
			while(strlen($numero)<$loop){
				$numero = $numero . $insert;
			}
		}

		return $numero;
	}

   	public function fator_vencimento( $data ){

		$data 	= explode("/",$data);
		$ano 	= $data[2];
		$mes 	= $data[1];
		$dia 	= $data[0];

	    return( abs ( ($this->_dateToDays("1997","10","07") ) - ($this->_dateToDays($ano, $mes, $dia) )  ) );
	}

	public function _dateToDays( $year, $month, $day ){

	    $century 	= substr($year, 0, 2);
	    $year 		= substr($year, 2, 2);

	    if ($month > 2)
	    {
	        $month -= 3;
	    }
	    else 
	    {
	        $month += 9;
	        if ($year) {
	            $year--;
	        } else {
	            $year = 99;
	            $century --;
	        }
	    }

	    return ( floor((  146097 * $century)    /  4 ) +
	            floor(( 1461 * $year)        /  4 ) +
	            floor(( 153 * $month +  2) /  5 ) +
	                $day +  1721119);
	}

   	public function geraCodigoBanco($numero){

   		$m11 		= new Modulo_11;
	    
	    $parte1   	= substr($numero, 0, 3);
	    
	    $parte2   	= $m11->m11($parte1);
	    
	    return $parte1 . "-" . $parte2;
	}

	public function esquerda($entra,$comp){
  		return substr($entra,0,$comp);
	}

	public function direita($entra,$comp){
		return substr($entra,strlen($entra)-$comp,$comp);
	}

	public function modulo_10($num){

		$numtotal10 	= 0;
        $fator 			= 2;

        // Separacao dos numeros
        for ($i = strlen($num); $i > 0; $i--) {
            // pega cada numero isoladamente
            $numeros[$i] = substr($num,$i-1,1);
            // Efetua multiplicacao do numero pelo (falor 10)
            // 2002-07-07 01:33:34 Macete para adequar ao Mod10 do Itaú
            $temp = $numeros[$i] * $fator; 
            $temp0=0;
            foreach (preg_split('//',$temp,-1,PREG_SPLIT_NO_EMPTY) as $k=>$v){ $temp0+=$v; }
            $parcial10[$i] = $temp0; //$numeros[$i] * $fator;
            // monta sequencia para soma dos digitos no (modulo 10)
            $numtotal10 += $parcial10[$i];
            if ($fator == 2) {
                $fator = 1;
            } else {
                $fator = 2; // intercala fator de multiplicacao (modulo 10)
            }
        }
		
        // várias linhas removidas, vide função original
        // Calculo do modulo 10
        $resto = $numtotal10 % 10;
        $digito = 10 - $resto;
        if ($resto == 0) {
            $digito = 0;
        }
		
        return $digito;
	}

	public function monta_linha_digitavel(){
		
		$codigo = $this->linha;

		$campo1 = substr($codigo,0,3) . substr($codigo,3,1) . substr($codigo,19,1) . substr($codigo,20,4);
		$campo1 = $campo1 . $this->modulo_10($campo1);
  		$campo1 = substr($campo1, 0, 5).'.'.substr($campo1, 5);


  		$campo2 = substr($codigo,24,10);
		$campo2 = $campo2 . $this->modulo_10($campo2);
	  	$campo2 = substr($campo2, 0, 5).'.'.substr($campo2, 5);

	  	$campo3 = substr($codigo,34,10);
		$campo3 = $campo3 . $this->modulo_10($campo3);
	  	$campo3 = substr($campo3, 0, 5).'.'.substr($campo3, 5);
	  	$campo4 = substr($codigo, 4, 1);

  		$campo5 = substr($codigo, 5, 4) . substr($codigo, 9, 10);
	
		return "$campo1 $campo2 $campo3 $campo4 $campo5";
	}

	public function fbarcode(){
	
		$fino 		= 1;
		$largo 		= 3;
		$altura 	= 50;

		$barcodes[0] = "00110" ;
		$barcodes[1] = "10001" ;
		$barcodes[2] = "01001" ;
		$barcodes[3] = "11000" ;
		$barcodes[4] = "00101" ;
		$barcodes[5] = "10100" ;
		$barcodes[6] = "01100" ;
		$barcodes[7] = "00011" ;
		$barcodes[8] = "10010" ;
		$barcodes[9] = "01010" ;

		for ( $f1=9; $f1>=0; $f1-- ) {
			
			for ( $f2=9;$f2>=0;$f2-- ) {

				$f = ( $f1*10 ) + $f2;

				$texto = "" ;

				for ( $i=1; $i<6; $i++ ) {
					$texto .= substr( $barcodes[$f1],($i-1),1 ) . substr($barcodes[$f2],($i-1),1);
				}
				$barcodes[$f] = $texto;
			}
		}
		
		echo '<img src='.Yii::app()->getBaseUrl(true).'/images/p.png width="'.$fino.'" height="'.$altura.'" border=0>'.'<img src='.Yii::app()->getBaseUrl(true).'/images/b.png width="'.$fino.'" height="'.$altura.'" border=0>'.'<img src='.Yii::app()->getBaseUrl(true).'/images/p.png width="'.$fino.'" height="'.$altura.'" border=0>'.'<img src='.Yii::app()->getBaseUrl(true).'/images/b.png width="'.$fino.'" height="'.$altura.'" border=0><img ';

		$texto = $this->linha;

		if( (strlen( $texto ) %2 ) <> 0 ) { 

			$texto = "0".$texto;
		}

		while( strlen( $texto ) > 0 )
		{

			$i = round( $this->esquerda($texto,2) );
			$texto = $this->direita($texto,strlen($texto)-2);
			$f = $barcodes[$i];

			for( $i=1; $i<11; $i+=2 ) {

				if(substr($f,($i-1),1)=="0") { $f1 = $fino; } else { $f1 = $largo; }
				echo 'src='.Yii::app()->getBaseUrl(true).'/images/p.png width="'.$f1.'" height="'.$altura.'" border=0><img ';
				if(substr($f,$i,1)=="0") { $f2 = $fino; } else { $f2 = $largo; }
				echo 'src='.Yii::app()->getBaseUrl(true).'/images/b.png width="'.$f2.'" height="'.$altura.'" border=0><img ';
			}
		}

		$aux = 1;

		echo 'src='.Yii::app()->getBaseUrl(true).'/images/p.png width="'.$largo.'" height="'.$altura.'" border=0><img '.
			 'src='.Yii::app()->getBaseUrl(true).'/images/b.png width="'.$fino.'" height="'.$altura.'" border=0><img '.
			 'src='.Yii::app()->getBaseUrl(true).'/images/p.png width="'.$aux.'" height="'.$altura.'" border=0>';
	}
}

?>