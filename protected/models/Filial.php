<?php

class Filial extends CActiveRecord
{

    private $connection;

    public function producaoDia($filiais, $status, $dataDe, $dataAte)
    {

        $arrReturn = array(
            'propostas' => array(),
            'valorTotal' => 0,
        );

        $filiais = join(',', $filiais);

        $status = join(',', $status);

        $criteria = new CDbCriteria;

        $criteria->with = array('analiseDeCredito' => array('alias' => 'ac'));
        $criteria->condition = "ac.Filial_id IN($filiais) AND t.titulos_gerados AND t.Status_Proposta_id = 2 AND t.habilitado AND t.data_cadastro BETWEEN '$dataDe' AND '$dataAte'";

        foreach (Proposta::model()->findAll($criteria) as $proposta)
        {
            $arrReturn['propostas'][] = $proposta;
            $arrReturn['valorTotal'] += $proposta->valor - $proposta->valor_entrada;
        }

        return $arrReturn;
    }

    public function propostasFilial($draw, $start, $filters)
    {

        $sql1 = "";
        $sql2 = "";
        $sql3 = "";

        if (Yii::app()->session['usuario']->tipo_id == 5)
        {

            $sqlFilter = NULL;

            if (isset($filters[0]) && !empty($filters[0]))
            {
                $sqlFilter = " AND codigo LIKE '%" . $filters[0] . "%' ";
            }

            if (isset($filters[1]) && !empty($filters[1]))
            {
                $sqlFilter .= " AND nome LIKE '%" . $filters[1] . "%' ";
            }


            $rows = array();

            $sql = " SELECT * FROM( ";
            $sql .= " 	SELECT pr.data_cadastro as data_de_cadastro, pr.id as Proposta_id, pr.codigo, pr.habilitado, pr.data_cadastro, pess.nome ";
            $sql .= "   FROM        Proposta                    as pr ";
            $sql .= " 	INNER JOIN  Solicitacao_de_Cancelamento as sc   ON sc.Proposta_id   = pr.id ";
            $sql .= " 	INNER JOIN  Analise_de_Credito          as acr  ON acr.id           = pr.Analise_de_Credito_id ";
            $sql .= " 	INNER JOIN  Cliente                     as clie ON clie.id          = acr.Cliente_id ";
            $sql .= " 	INNER JOIN  Pessoa                      as pess ON clie.Pessoa_id   = pess.id ";
            $sql .= " 	WHERE acr.Filial_id = " . Yii::app()->session['usuario']->returnFilial()->id;

            $sql .= " 	UNION ";

            $sql .= " 	SELECT p.data_cadastro as data_de_cadastro, p.id as Proposta_id, p.codigo, p.habilitado, p.data_cadastro, pes.nome ";
            $sql .= "   FROM        Proposta            as p ";
            $sql .= " 	INNER JOIN  Analise_de_Credito  as ac   ON ac.id            = p.Analise_de_Credito_id ";
            $sql .= " 	INNER JOIN  Cliente             as cli  ON cli.id           = ac.Cliente_id ";
            $sql .= " 	INNER JOIN  Pessoa              as pes  ON cli.Pessoa_id    = pes.id ";
            $sql .= " 	WHERE ac.Filial_id = " . Yii::app()->session['usuario']->returnFilial()->id;
            $sql .= "  AND   (p.Status_Proposta_id = 2 OR p.Status_Proposta_id = 7) ";

            $sql .= " 	UNION ";

            $sql .= " 	SELECT eP.data_cadastro as data_de_cadastro, eP.id as Proposta_id, eP.codigo, eP.habilitado, eP.data_cadastro, ePe.nome ";
            $sql .= "   FROM        Proposta            AS eP ";
            $sql .= " 	INNER JOIN  Emprestimo          AS E    ON  eP.id =   E.Proposta_id             AND   E.habilitado ";
            $sql .= " 	INNER JOIN  Analise_de_Credito  AS eAC  ON eAC.id =  eP.Analise_de_Credito_id   AND eAC.habilitado ";
            $sql .= " 	INNER JOIN  Cliente             AS eC   ON  eC.id = eAC.Cliente_id              AND  eC.habilitado ";
            $sql .= " 	INNER JOIN  Pessoa              AS ePe  ON ePe.id =  eC.Pessoa_id               AND ePe.habilitado ";
            $sql .= " 	WHERE E.Filial_id = " . Yii::app()->session['usuario']->returnFilial()->id;
            $sql .= "           AND eP.Status_Proposta_id = 2 ";

            $sql .= " ) as allQry  WHERE 1 ";

            $sql1 = $sql;

            $recordsTotal = count(Yii::app()->db->createCommand($sql)->queryAll());

            if ($sqlFilter != NULL)
            {
                $sql .= $sqlFilter;
            }

            $sql2 = $sql;

            $recordsFiltered = count(Yii::app()->db->createCommand($sql)->queryAll());

            if ($recordsFiltered > 0)
            {

                $sql .= " 	ORDER BY data_de_cadastro DESC ";

                $sql .= " LIMIT " . $start . " ,10";

                $sql3 = $sql;

                foreach (Yii::app()->db->createCommand($sql)->queryAll() as $r)
                {
                    $proposta = Proposta::model()->findByPk($r['Proposta_id']);

                    $soliCancel = SolicitacaoDeCancelamento::model()->find('Proposta_id = ' . $proposta->id . ' AND habilitado AND aceite AND AceitePor IS NOT NULL ');

                    if ($soliCancel != NULL && $proposta->Status_Proposta_id != 8)
                    {
                        $proposta->Status_Proposta_id = 8;
                        $proposta->update();
                    }

                    $btnDetails = '';

                    if (!$proposta->habilitado || $proposta->Status_Proposta_id == 3)
                    {
                        $btnDetails .= '<form>';
                        $btnDetails .= '	<button style="padding:6px;font-size:8px;" type="submit" disabled="disabled" class="btn btn-primary">'
                                . '        <i class="clip-search"></i>'
                                . '    </button>';
                        $btnDetails .= '</form>';
                    } else
                    {
                        $btnDetails .= '<form method="post" action="' . Yii::app()->getBaseUrl(true) . '/proposta/more">';
                        $btnDetails .= '	<input type="hidden" name="id" value="' . $proposta->id . '">';
                        $btnDetails .= '	<button style="padding:6px;font-size:8px;" type="submit" class="btn btn-primary">'
                                . '        <i class="clip-search"></i>'
                                . '    </button>';
                        $btnDetails .= '</form>';
                    }

                    $btnStatus = '<span class="' . $proposta->statusProposta->cssClass . '">' . $proposta->statusProposta->status . '</span>';

                    if ($proposta->titulos_gerados)
                    {
                        $row = array(
                            'btn' => $btnDetails,
                            'codigo' => $proposta->codigo,
                            'cliente' => strtoupper($proposta->analiseDeCredito->cliente->pessoa->nome),
                            'crediarista' => strtoupper($proposta->analiseDeCredito->usuario->nome_utilizador),
                            'valor' => number_format($proposta->valor, 2, ',', ''),
                            'valor_entrada' => number_format($proposta->valor_entrada, 2, ',', ''),
                            'seguro' => number_format($proposta->calcularValorDoSeguro(), 2, ',', ''),
                            'val_financiado' => number_format($proposta->getValorFinanciado(), 2, ',', ''),
                            'qtd_parcelas' => $proposta->qtd_parcelas . 'x ' . number_format($proposta->getValorParcela(), 2, ',', '.'),
                            'valor_final' => number_format($proposta->getValorFinal(), 2, ',', ''),
                            'btn_status' => $btnStatus
                        );

                        $rows[] = $row;
                    }
                }
            }


            return [
                "draw" => $draw,
                "recordsFiltered" => $recordsFiltered,
                "recordsTotal" => $recordsTotal,
                "data" => $rows,
                "sql1" => $sql1,
                "sql2" => $sql2,
                "sql3" => $sql3,
            ];
        } else
        {
            return null;
        }
    }

    public function getRecebimentos($draw, $Filiais_ids, $data_de, $data_ate, $offset)
    {

        $recordsTotal = 0;
        $recordsFiltered = 0;
        $rows = array();
        $util = new Util;
        $criteria = new CDbCriteria;
        $criteriaFilter = new CDbCriteria;
        $totalAtrasoFilter = 0;
        $totalAtraso = 0;

        /* Aplicando filtro por filial, se informado */
        if (!empty($Filiais_ids))
        {
            $criteria->addInCondition('Filial_id', $Filiais_ids, 'AND');
            $criteriaFilter->addInCondition('Filial_id', $Filiais_ids, 'AND');
        }

        /* Aplicando filtro por data, se informado */
        if (!empty($data_de) && $data_de != NULL && !empty($data_ate) && $data_ate != NULL)
        {
            $criteria->addBetweenCondition('t.data_da_baixa', $util->view_date_to_bd($data_de), $util->view_date_to_bd($data_ate), 'AND');
            $criteriaFilter->addBetweenCondition('t.data_da_baixa', $util->view_date_to_bd($data_de), $util->view_date_to_bd($data_ate), 'AND');
        }

        $criteria->order = 't.data_da_baixa DESC';
        $baixas = Baixa::model()->findAll($criteria);
        $recordsTotal = count($baixas);


        if (count($baixas) > 0)
        {
            foreach ($baixas as $b)
            {
                $totalAtraso += $b->parcela->valor_atual;
            }
        }

        $criteriaFilter->offset = $offset;
        $criteriaFilter->limit = 10;
        $criteriaFilter->order = 't.data_da_baixa DESC';

        if (count($baixas) > 0)
        {

            foreach (Baixa::model()->findAll($criteriaFilter) as $baixa)
            {
                $row = array(
                    'cliente' => strtoupper($baixa->cliente->pessoa->nome),
                    'cpf' => $baixa->cliente->pessoa->getCPF()->numero,
                    'filial' => strtoupper($baixa->filial->getConcat()),
                    'seq_parcela' => $baixa->parcela->seq . '°',
                    'vencimento_da_parcela' => $util->bd_date_to_view($baixa->parcela->vencimento),
                    'data_do_pgto' => $util->bd_date_to_view($baixa->data_da_ocorrencia),
                    'data_da_baixa' => $util->bd_date_to_view($baixa->data_da_baixa),
                    'valor_parcela' => number_format($baixa->parcela->valor, 2, ',', '.'),
                    'valor_pago' => number_format($baixa->valor_pago, 2, ',', '.'),
                    'proposta' => $baixa->parcela->titulo->proposta->codigo,
                );

                $rows[] = $row;

                $totalAtrasoFilter += $baixa->parcela->valor_atual;
            }
        }

        return (array(
            "draw" => $draw,
            "recordsFiltered" => count($baixas),
            "recordsTotal" => count($rows),
            "data" => $rows,
            "customReturn" => array(
                'totalAtrasoFilter' => "R$ " . number_format($totalAtrasoFilter, 2, ',', '.'),
                'totalAtraso' => "R$ " . number_format($totalAtraso, 2, ',', '.'),
            )
        ));
    }

    public function listarClientes()
    {

        $idsCadastros = array();
        $idsClientes = array();


        /* Relação cadastro-filial... */
        $criteriaCHF = new CDbCriteria;
        $criteriaCHF->condition = 'Filial_id=:filial_id';
        $criteriaCHF->params = array(':filial_id' => $this->id);
        $cadastroHasFilial = CadastroHasFilial::model()->findAll($criteriaCHF);

        foreach ($cadastroHasFilial as $chf)
        {

            if (!in_array($chf->Cadastro_id, $idsCadastros))
                array_push($idsCadastros, $chf->Cadastro_id);
        }

        /* Listando cadastros */
        $crt = new CDbCriteria();
        $crt->addInCondition('id', $idsCadastros);
        $cadastros = Cadastro::model()->findAll($crt);



        foreach ($cadastros as $cadastro)
        {
            if (!in_array($cadastro->Cliente_id, $idsClientes))
                ;
            array_push($idsClientes, $cadastro->Cliente_id);
        }

        $crtCliente = new CDbCriteria();
        $crtCliente->addInCondition('id', $idsClientes);

        $clientes = Cliente::model()->findAll($crtCliente);

        return $clientes;
    }

    public function mudarAtividadePrimaria($atividadeId)
    {

        $this->Unidade_de_negocio_id = $atividadeId;

        if ($this->update())
            return "Sucesso";
        return "Erro";
    }

    public function mudarRelacaoAtividadeSecundaria($atividadeId, $checked)
    {

        $filiaHasAtividadeSecundaria = FilialHasAtividadeEconomicaSecundaria::model()->find('Unidade_de_negocio_id = ' . $atividadeId . ' AND Filial_id = ' . $this->id);

        if ($checked == 'true')
            $checked = true;
        else
            $checked = false;

        if ($filiaHasAtividadeSecundaria == NULL)
        {

            $fhas = new FilialHasAtividadeEconomicaSecundaria;
            $fhas->Filial_id = $this->id;
            $fhas->Unidade_de_negocio_id = $atividadeId;
            $fhas->habilitado = 1;
            $fhas->data_cadastro = date('Y-m-d H:i:s');

            if ($fhas->save())
                return "Sucesso";
            else
                return "Erro";
        }

        else
        {

            if ($checked)
                $filiaHasAtividadeSecundaria->habilitado = 1;
            else
                $filiaHasAtividadeSecundaria->habilitado = 0;

            if ($filiaHasAtividadeSecundaria->update())
                return "Sucesso";
            else
                return "Erro";
        }
    }

    public function hasAtividadeSecundaria($atividadeId)
    {

        $filiaHasAtividadeSecundaria = FilialHasAtividadeEconomicaSecundaria::model()->find('Unidade_de_negocio_id = ' . $atividadeId . ' AND Filial_id = ' . $this->id . ' AND habilitado');

        if ($filiaHasAtividadeSecundaria != NULL)
            return true;
        return false;
    }

    public function tableName()
    {

        return 'Filial';
    }

    public function getEndereco()
    {

        $filialHasEndereco = FilialHasEndereco::model()->find('Filial_id = ' . $this->id);

        return Endereco::model()->findByPk($filialHasEndereco->Endereco_id);
    }

    public function getConcat()
    {

        return strtoupper($this->nome_fantasia . ' / ' . $this->getEndereco()->cidade . '-' . $this->getEndereco()->uf);
    }

    public function listCotacoes()
    {

        $arrIdsCotacoes = array();

        $criteria = new CDbCriteria();

        $filialHasCotacao = FilialHasCotacao::model()->findAll('Filial_id = ' . $this->id);

        foreach ($filialHasCotacao as $fhc)
        {

            if (!in_array($fhc->Cotacao_id, $arrIdsCotacoes))
                array_push($arrIdsCotacoes, $fhc->Cotacao_id);
        }

        $criteria->addInCondition('id', $arrIdsCotacoes);

        return Cotacao::model()->findAll($criteria);
    }

    private function setConn()
    {

        $this->connection = new CDbConnection(
                Yii::app()->params['dbconf']['dns'], Yii::app()->params['dbconf']['user'], Yii::app()->params['dbconf']['pass']
        );
    }

    public function propostasFilialGraph($args)
    {
        $criteria = new CDbCriteria;
        $util = new Util;

        $criteria->with = array('analiseDeCredito' => array(
                'alias' => 'ac'
        ));

        $criteria->addInCondition('ac.Filial_id', array($this->id));

        if ($args['dataDe'] != NULL && $args['dataAte'] != NULL)
        {
            $criteria->addBetweenCondition('t.data_cadastro', $util->view_date_to_bd($args['dataDe']) . ' 00:00:00', $util->view_date_to_bd($args['dataAte']) . ' 23:59:59', 'AND');
        }

        if ($args['parc_de'] != NULL && $args['parc_ate'] != NULL)
        {
            $criteria->addBetweenCondition('t.qtd_parcelas', $args['parc_de'], $args['parc_ate'], 'AND');
        }

        return array(
            strtoupper($this->nome_fantasia . '' . $this->getEndereco()->cidade) . '/' . strtoupper($this->getEndereco()->uf), count(Proposta::model()->findAll($criteria))
        );
    }

    public function listPropostasFilial()
    {

        $analises = AnaliseDeCredito::model()->findAll('Filial_id = ' . $this->id);
        $arrPropostas = array();

        foreach ($analises as $analise)
        {

            $propostas = Proposta::model()->findAll('Analise_de_Credito_id = ' . $analise->id);
            array_push($arrPropostas, $propostas);
        }

        return $arrPropostas;
    }

    public function listVendas()
    {

        return Venda::model()->findAll('Filial_id = ' . $this->id);
    }

    public function listPropostas($analista, $start = null, $limit = null)
    {
        $arrIdPropostas = array();

        $analises = AnaliseDeCredito::model()->findAll(
                'Filial_id = ' . $this->id .
                ' AND habilitado'
        );

        foreach ($analises as $analise)
        {
            $crt = new CDbCriteria;
            $crt->condition = 't.habilitado AND t.Analise_de_Credito_id = ' . $analise->id . ' AND (t.Status_Proposta_id = 4 OR t.Status_Proposta_id = 1) OR (t.Status_Proposta_id = 3 AND ( t.data_cadastro BETWEEN "' . date('Y-m-d 00:00:00') . '" AND "' . date('Y-m-d 23:59:59') . '" ) )';
            $crt->order = 't.data_cadastro DESC';

            $criteria = new CDbCriteria;
            $criteria->addInCondition('Analise_de_Credito_id', array($analise->id), ' AND ');
            $criteria->addInCondition('Status_Proposta_id', array(4, 3), ' AND ');
            $criteria->addInCondition('habilitado', array(1), ' AND ');
            $criteria->order = 'data_cadastro DESC';

            $propostas = Proposta::model()->findAll($crt);

            foreach ($propostas as $prop)
            {

                if ($prop->valor <= $analista->limite_analise)
                {
                    if (!in_array($prop->id, $arrIdPropostas))
                    {
                        array_push($arrIdPropostas, $prop->id);
                    }
                }
            }
        }

        return $arrIdPropostas;
    }

    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Unidade_de_negocio_id,Contato_id, Empresa_id, PoliticaCredito_id', 'required'),
            array('isMatriz, habilitado, Contato_id, Empresa_id, PoliticaCredito_id, NucleoFiliais_id', 'numerical', 'integerOnly' => true),
            array('nome_fantasia', 'length', 'max' => 100),
            array('cnpj, inscricao_estadual', 'length', 'max' => 45),
            array('data_cadastro, data_de_abertura, data_cadastro_br, isMatriz', 'safe'),
            array('id, codigo, nome_fantasia, cnpj, inscricao_estadual, data_cadastro, data_cadastro_br, habilitado, Contato_id, Empresa_id, data_de_abertura, Unidade_de_negocio_id, PoliticaCredito_id, NucleoFiliais_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {

        return array(
            'unidadeDeNegocio'          =>  array
                                            (
                                                self::BELONGS_TO        ,
                                                'UnidadeDeNegocio'      ,
                                                'Unidade_de_negocio_id'
                                            ),
            'contato'                   =>  array
                                            (
                                                self::BELONGS_TO    ,
                                                'Contato'           ,
                                                'Contato_id'
                                            ),
            'empresa'                   =>  array
                                            (
                                                self::BELONGS_TO    ,
                                                'Empresa'           ,
                                                'Empresa_id'
                                            ),
            'politicaCredito'           =>  array
                                            (
                                                self::BELONGS_TO        ,
                                                'PoliticaCredito'       ,
                                                'PoliticaCredito_id'
                                            ),
            'nucleoFilial'              =>  array
                                            (
                                                self::BELONGS_TO    ,
                                                'NucleoFiliais'     ,
                                                'NucleoFiliais_id'
                                            ),
            'filialHasPoliticaCredito'  =>  array
                                            (
                                                self::HAS_ONE                                                                                                                                                   ,
                                                'FilialHasPoliticaCredito'                                                                                                                                      ,
                                                'Filial_id'                                                                                                                                                     ,
                                                'on'                        => "habilitado AND '" . date('Y-m-d H:i:s') . "' >= dataInicio AND ('"  . date('Y-m-d H:i:s') . "' <= dataFim OR dataFim IS NULL)"),
            'hasServicos'         => array(self::HAS_MANY, 'FilialHasServico', 'Filial_id'),
        );
    }

    public function boletoNaoBancario(){
      $servicoAtivo = FilialHasServico::model()->find('habilitado AND Filial_id = '.$this->id .' AND Servico_id = 4');
      return ($servicoAtivo != Null ? True : False);
    }

    public function attributeLabels()
    {

        return array(
            'id' => 'ID',
            'codigo' => 'Código',
            'nome_fantasia' => 'Nome Fantasia',
            'cnpj' => 'Cnpj',
            'inscricao_estadual' => 'Inscrição Estadual',
            'data_cadastro' => 'Data Cadastro',
            'data_cadastro_br' => 'Data / Hora Cadastro',
            'habilitado' => 'Habilitado',
            'Contato_id' => 'Contato',
            'Empresa_id' => 'Matriz',
            'data_de_abertura' => 'Data de abertura',
            'isMatriz' => 'É matriz',
            'Unidade_de_negocio_id' => 'Atividade Econômica Principal',
            'PoliticaCredito_id' => 'Política de Crédito', //--alteracao politicaCredito
            'NucleoFiliais_id' => 'Núcleo Filial',
        );
    }

    public function search()
    {

        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('codigo', $this->codigo);
        $criteria->compare('nome_fantasia', $this->nome_fantasia, true);
        $criteria->compare('cnpj', $this->cnpj, true);
        $criteria->compare('data_cadastro', $this->data_cadastro, true);
        $criteria->compare('data_cadastro_br', $this->data_cadastro_br, true);
        $criteria->compare('habilitado', $this->habilitado);
        $criteria->compare('Contato_id', $this->Contato_id);
        $criteria->compare('Empresa_id', $this->Empresa_id);
        $criteria->compare('data_de_abertura', $this->data_de_abertura);
        $criteria->compare('isMatriz', $this->isMatriz);
        $criteria->compare('Unidade_de_negocio_id', $this->Unidade_de_negocio_id);
        $criteria->compare('PoliticaCredito_id', $this->PoliticaCredito_id); //-----alteracao politica de credito
        $criteria->compare('NucleoFiliais_id', $this->NucleoFiliais_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function filialUserFilterSearch($user_id)
    {

        $connection = new CDbConnection(
                Yii::app()->params['dbconf']['dns'], Yii::app()->params['dbconf']['user'], Yii::app()->params['dbconf']['pass']
        );

        $usuario = Usuario::model()->find("id = " . $user_id);

        $filiais_ids = array();

        foreach ($usuario->empresasId('query') as $id)
        {

            $sql_filiais = "select f.id \n"
                    . "	from `Filial` as f\n"
                    . "	where f.Empresa_id = " . $id['id'];

            $queryReturnFiliais = Yii::app()->db->createCommand($sql_filiais)->queryAll();

            foreach ($queryReturnFiliais as $id_filial)
            {
                array_push($filiais_ids, $id_filial['id']);
            }
        }

        $criteria = new CDbCriteria;
        $criteria->addInCondition("id", $filiais_ids);
        $criteria->compare('habilitado', 1);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function returnCotacoes()
    {

        return Cotacao::model()->findAll('Filial_id = ' . $this->id);
    }

    public function returnFornecedor()
    {
        return Fornecedor::model()->find('Filial_id = ' . $this->id);
    }

    public function listEstoques()
    {

        return Estoque::model()->findAll('Filial_id = ' . $this->id);
    }

    public function salvarUrlLogin($idFilial, $url)
    {
        $arrayRetorno = array(
            'hasErrors' => 0,
            'msg' => 'Url salva com sucesso.',
            'pntfyClass' => 'success'
        );

        $filialUrl = FilialLoginConfig::model()->find('Filial_id = ' . $idFilial);

        if ($filialUrl == null)
        {
            $filialUrl = new FilialLoginConfig;
            $filialUrl->Filial_id = $idFilial;
            $filialUrl->url = $url;

            if (!$filialUrl->save())
            {
                ob_start();
                var_dump($filialUrl->getErrors());
                $result = ob_get_clean();

                $arrayRetorno['msg'] = 'Ocorreu um erro. Tente novamente.' . $result;
                $arrayRetorno['pntfyClass'] = 'error';
            }
        } else
        {

            $filialUrl->url = $url;

            if (!$filialUrl->update())
            {
                ob_start();
                var_dump($filialUrl->getErrors());
                $result = ob_get_clean();

                $arrayRetorno['msg'] = 'Ocorreu um erro. Tente novamente.' . $result;
                $arrayRetorno['pntfyClass'] = 'error';
            }
        }

        /* else
          {

          //salve os logs
          $sigacLog = new SigacLog;
          $sigacLog->saveLog('Alteração de url de Filial', 'Filiais', $idFilial, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Usuário " . Yii::app()->session['usuario']->username . " alterou a url da filial.", "127.0.0.1", null, Yii::app()->session->getSessionId());
          }
         */

        return $arrayRetorno;
    }

    public function salvarNucleo($idFilial, $idNucleo)
    {

        $arrayRetorno = array(
            'hasErrors' => 0,
            'msg' => 'Núcleo de Filiais escolhido com sucesso.',
            'pntfyClass' => 'success'
        );

        $filial = Filial::model()->findByPk($idFilial);
        $filial->NucleoFiliais_id = $idNucleo;

        if (!$filial->save())
        {

            ob_start();
            var_dump($filial->getErrors());
            $result = ob_get_clean();

            $arrayRetorno['msg'] = 'Ocorreu um erro. Tente novamente.' . $result;
            $arrayRetorno['pntfyClass'] = 'error';
        } else
        {

            //salve os logs
            $sigacLog = new SigacLog;
            $sigacLog->saveLog('Alteração de Núcleo de Filiais', 'Filiais', $filial->id, date('Y-m-d H:i:s'), 1, Yii::app()->session['usuario']->id, "Usuário " . Yii::app()->session['usuario']->username . " alterou o núcleo.", "127.0.0.1", null, Yii::app()->session->getSessionId());
        }

        return $arrayRetorno;
    }

    public function getNomeNucleo()
    {

        $nomeNucleo = 'Sem Núcleo';

        $nucleo = NucleoFiliais::model()->findByPk($this->NucleoFiliais_id);

        if ($nucleo !== null)
        {
            $nomeNucleo = $nucleo->grupoFiliais->nome_fantasia . ' ' . $nucleo->nome;
        }

        return $nomeNucleo;
    }

    public static function model($className = __CLASS__)
    {

        return parent::model($className);
    }

    public function temEmprestimo()
    {

        $filialHasServico = FilialHasServico::model()->find("Filial_id = $this->id AND Servico_id = 1");

        if ($filialHasServico !== null)
        {
            return true;
        }

        return false;
    }

    public function listarParaProducao($idGrupo = null, $idFiliais = null)
    {
        $retorno = [];

        $query = "  SELECT DISTINCT F.id, F.nome_fantasia
                        FROM  Filial    AS F
                        INNER JOIN  Empresa             AS E    ON E.id     =  F.Empresa_id
                        INNER JOIN  Empresa_has_Usuario AS EHU  ON E.id     =  EHU.Empresa_id
                        INNER JOIN  Usuario             AS U    ON U.id     =  EHU.Usuario_id
                        WHERE F.habilitado AND U.id = " . Yii::app()->session['usuario']->id;

        if (isset($idGrupo))
        {
            $query .= " AND NF.GrupoFiliais_id =  $idGrupo ";
        }

        if (isset($idFiliais))
        {
            $query .= " AND F.id IN ($idFiliais) ";
        }

        $query .= " ORDER BY 2";

        $linhas = Yii::app()->db->createCommand($query)->queryAll();

        foreach ($linhas as $linha)
        {

            $retorno[] = Filial::model()->findByPk($linha["id"]);
        }

        return $retorno;
    }

    //Função para listar as filiais, utilizada na tela principal do módulo de cobrança
    public function listarParaCobranca($idGrupo = null, $idFiliais = null)
    {

        $retorno = [];

        $query = "  SELECT DISTINCT F.id, F.nome_fantasia
                        FROM  Filial    AS F
                        WHERE F.habilitado ";

        if (isset($idGrupo))
        {
            $query .= " AND NF.GrupoFiliais_id =  $idGrupo ";
        }

        if (isset($idFiliais))
        {
            $query .= " AND F.id IN ($idFiliais) ";
        }

        $query .= "ORDER BY 2";

        $linhas = Yii::app()->db->createCommand($query)->queryAll();

        foreach ($linhas as $linha)
        {

            $retorno[] = Filial::model()->findByPk($linha["id"]);
        }

        return $retorno;
    }

    public function getPoliticaCredito()
    {

        //$politicaCredito = null;

        $sql    =   "SELECT MAX(FhPC.id) AS idMAX "
                .   "FROM Filial_has_PoliticaCredito AS FhPC "
                .   "WHERE FhPC.habilitado AND FhPC.Filial_id = $this->id "
                .   "GROUP BY FhPC.Filial_id ";

        $retorno = Yii::app()->db->createCommand($sql)->queryRow();

        if($this->id == 37)
        {

            ob_start();
            var_dump($retorno);
            $testeRetorno = ob_get_clean() . " " . $sql;

            $file = fopen("testeRetorno.txt", "w");
            fwrite($file, $testeRetorno);
            fclose($file);
        }

        if(isset($retorno["idMAX"]))
        {

            $filialHasPoliticaCredito = FilialHasPoliticaCredito::model()->findByPk($retorno["idMAX"]);

            if(isset($filialHasPoliticaCredito))
            {
                return $filialHasPoliticaCredito;
            }

        }

        return null; //$politicaCredito;

    }

}
