<?php

/**
 * This is the model class for table "LotePagamento_has_StatusLotePagamento".
 *
 * The followings are the available columns in table 'LotePagamento_has_StatusLotePagamento':
 * @property integer $LotePagamento_id
 * @property integer $StatusLotePagamento_id
 * @property integer $habilitado
 * @property string $data_cadastro
 * @property integer $id
 * @property integer $Usuario_id
 *
 * The followings are the available model relations:
 * @property LotePagamento $lotePagamento
 * @property StatusLotePagamento $statusLotePagamento
 * @property Usuario $usuario
 */
class LotePagamentoHasStatusLotePagamento extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LotePagamentoHasStatusLotePagamento the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'LotePagamento_has_StatusLotePagamento';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('LotePagamento_id, StatusLotePagamento_id, data_cadastro, Usuario_id', 'required'),
			array('LotePagamento_id, StatusLotePagamento_id, habilitado, Usuario_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('LotePagamento_id, StatusLotePagamento_id, habilitado, data_cadastro, id, Usuario_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'lotePagamento' => array(self::BELONGS_TO, 'LotePagamento', 'LotePagamento_id'),
			'statusLotePagamento' => array(self::BELONGS_TO, 'StatusLotePagamento', 'StatusLotePagamento_id'),
			'usuario' => array(self::BELONGS_TO, 'Usuario', 'Usuario_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'LotePagamento_id' => 'Lote Pagamento',
			'StatusLotePagamento_id' => 'Status Lote Pagamento',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'id' => 'ID',
			'Usuario_id' => 'Usuario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('LotePagamento_id',$this->LotePagamento_id);
		$criteria->compare('StatusLotePagamento_id',$this->StatusLotePagamento_id);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('id',$this->id);
		$criteria->compare('Usuario_id',$this->Usuario_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}