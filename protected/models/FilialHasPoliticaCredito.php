<?php

/**
 * This is the model class for table "Filial_has_PoliticaCredito".
 *
 * The followings are the available columns in table 'Filial_has_PoliticaCredito':
 * @property integer $Filial_id
 * @property integer $PoliticaCredito_id
 * @property integer $id
 * @property integer $habilitado
 * @property string $data_cadastro
 * @property string $dataInicio
 * @property string $dataFim
 *
 * The followings are the available model relations:
 * @property Filial $filial
 * @property PoliticaCredito $politicaCredito
 */
class FilialHasPoliticaCredito extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Filial_has_PoliticaCredito';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Filial_id, PoliticaCredito_id, habilitado, data_cadastro, dataInicio', 'required'),
			array('Filial_id, PoliticaCredito_id, habilitado', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Filial_id, PoliticaCredito_id, id, habilitado, data_cadastro, dataInicio, dataFim', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'filial'            => array(self::BELONGS_TO, 'Filial'             , 'Filial_id'           ),
			'politicaCredito'   => array(self::BELONGS_TO, 'PoliticaCredito'    , 'PoliticaCredito_id'  ),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Filial_id' => 'Filial',
			'PoliticaCredito_id' => 'Politica Credito',
			'id' => 'ID',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'dataInicio' => 'Data Inicio',
			'dataFim' => 'Data Fim',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Filial_id',$this->Filial_id);
		$criteria->compare('PoliticaCredito_id',$this->PoliticaCredito_id);
		$criteria->compare('id',$this->id);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('dataInicio',$this->dataInicio,true);
		$criteria->compare('dataFim',$this->dataFim,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FilialHasPoliticaCredito the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
