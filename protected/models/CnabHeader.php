<?php

/**
 * This is the model class for table "Cnab_header".
 *
 * The followings are the available columns in table 'Cnab_header':
 * @property integer $id
 * @property string $cod_registro
 * @property string $cod_arquivo
 * @property string $literal_arquivo
 * @property string $cod_servico
 * @property string $literal_servico
 * @property string $zero
 * @property string $agencia_cedente
 * @property string $sub_conta
 * @property string $conta_corrente
 * @property string $banco
 * @property string $nome_do_cliente
 * @property string $codigo_do_banco
 * @property string $nome_do_banco
 * @property string $data_gravacao
 * @property string $densidade
 * @property string $literal_densidade
 * @property string $banco1
 * @property string $sigla_layout
 * @property string $banco2
 * @property string $numero_sequencial
 * @property integer $Banco_id
 *
 * The followings are the available model relations:
 * @property Banco $banco0
 */
class CnabHeader extends CActiveRecord{

	public function salvarHeader( $conteudo, $tipo ){

		$campos = CampoCnabConfig::model()->getCampos('header', $tipo);
		$header = new CnabHeader;

		for ( $i = 0; $i < count($campos) ; $i++ ) {

			$campo = CampoCnabConfig::model()->find( 'sigla = "'.$campos[$i].'"' );			
			$header->setAttribute( $campos[$i] , substr($conteudo, $campo->posi_ini-1, $campo->significativo+$campo->decimal) );
		}

		$header->Banco_id = 5;
		$header->save();
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Cnab_header';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Banco_id', 'required'),
			array('Banco_id', 'numerical', 'integerOnly'=>true),
			array('numero_sequencial, data_gravacao_texto', 'length', 'max'=>45),
			array('cod_registro, cod_arquivo, literal_arquivo, cod_servico, literal_servico, zero, agencia_cedente, sub_conta, conta_corrente, banco, nome_do_cliente, codigo_do_banco, nome_do_banco, data_gravacao, densidade, literal_densidade, banco1, sigla_layout, banco2', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, cod_registro, cod_arquivo, literal_arquivo, cod_servico, literal_servico, zero, agencia_cedente, sub_conta, conta_corrente, banco, nome_do_cliente, codigo_do_banco, nome_do_banco, data_gravacao, densidade, literal_densidade, banco1, sigla_layout, banco2, numero_sequencial, Banco_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'banco0' => array(self::BELONGS_TO, 'Banco', 'Banco_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'cod_registro' => 'Cod Registro',
			'cod_arquivo' => 'Cod Arquivo',
			'literal_arquivo' => 'Literal Arquivo',
			'cod_servico' => 'Cod Servico',
			'literal_servico' => 'Literal Servico',
			'zero' => 'Zero',
			'agencia_cedente' => 'Agencia Cedente',
			'sub_conta' => 'Sub Conta',
			'conta_corrente' => 'Conta Corrente',
			'banco' => 'Banco',
			'nome_do_cliente' => 'Nome Do Cliente',
			'codigo_do_banco' => 'Codigo Do Banco',
			'nome_do_banco' => 'Nome Do Banco',
			'data_gravacao' => 'Data Gravacao',
			'data_gravacao_texto' => 'Data Gravacao Texto',
			'densidade' => 'Densidade',
			'literal_densidade' => 'Literal Densidade',
			'banco1' => 'Banco1',
			'sigla_layout' => 'Sigla Layout',
			'banco2' => 'Banco2',
			'numero_sequencial' => 'Numero Sequencial',
			'Banco_id' => 'Banco',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('cod_registro',$this->cod_registro,true);
		$criteria->compare('cod_arquivo',$this->cod_arquivo,true);
		$criteria->compare('literal_arquivo',$this->literal_arquivo,true);
		$criteria->compare('cod_servico',$this->cod_servico,true);
		$criteria->compare('literal_servico',$this->literal_servico,true);
		$criteria->compare('zero',$this->zero,true);
		$criteria->compare('agencia_cedente',$this->agencia_cedente,true);
		$criteria->compare('sub_conta',$this->sub_conta,true);
		$criteria->compare('conta_corrente',$this->conta_corrente,true);
		$criteria->compare('banco',$this->banco,true);
		$criteria->compare('nome_do_cliente',$this->nome_do_cliente,true);
		$criteria->compare('codigo_do_banco',$this->codigo_do_banco,true);
		$criteria->compare('nome_do_banco',$this->nome_do_banco,true);
		$criteria->compare('data_gravacao',$this->data_gravacao,true);
		$criteria->compare('data_gravacao_texto',$this->data_gravacao_texto,true);
		$criteria->compare('densidade',$this->densidade,true);
		$criteria->compare('literal_densidade',$this->literal_densidade,true);
		$criteria->compare('banco1',$this->banco1,true);
		$criteria->compare('sigla_layout',$this->sigla_layout,true);
		$criteria->compare('banco2',$this->banco2,true);
		$criteria->compare('numero_sequencial',$this->numero_sequencial,true);
		$criteria->compare('Banco_id',$this->Banco_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CnabHeader the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}	
}
