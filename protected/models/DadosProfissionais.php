<?php

class DadosProfissionais extends CActiveRecord {

    public function persist( $DadosProfissionais, $EnderecoProfiss, $TelefoneProfiss, $clienteId, $dadosProfissionaisId, $action )
    {
        $DadosProfissionais['principal'] = 1;

        if( $action == 'new' )
        {
            return $this->novo( $DadosProfissionais, $EnderecoProfiss, $TelefoneProfiss, NULL, $clienteId );
        }

        else if( $action == 'update' )
        {
            return $this->atualizar($DadosProfissionais, $EnderecoProfiss, $TelefoneProfiss, NULL, NULL, $dadosProfissionaisId);
        }
    }

    public function novo($DadosProfissionais, $EnderecoProfiss, $TelefoneProfiss, $EmailProfiss = NULL, $Cliente_id)
    {
        $arrReturn = array('hasErrors' => true);

        $cliente                                                = Cliente::model()->findByPk($Cliente_id);
        
        if( !empty( $DadosProfissionais['renda_liquida'] ) || $DadosProfissionais['renda_liquida'] == '0' )
        {

            $util                                                   = new Util;
            $dadosProfissionais                                     = new DadosProfissionais;
            $contato                                                = new Contato;
            $endereco                                               = new Endereco;

            $DadosProfissionais['renda_liquida']                    = $util->moedaViewToBD($DadosProfissionais['renda_liquida']);
            
            
            if ($DadosProfissionais['renda_liquida'] < 957) {
                

                    $arrReturn['msgConfig']['pnotify'][]    = array(
                        'titulo'                            => 'Ação realizada com sucesso!',
                        'texto'                             => 'Dados profissionais de cadastrados com sucesso',
                        'tipo'                              => 'erro',
                    );
            }

            if( $DadosProfissionais['principal'] )
            {

                $dadosPrincipais                                    = DadosProfissionais::model()->findAll('Pessoa_id = ' . $cliente->pessoa->id .' AND principal AND habilitado');

                foreach( $dadosPrincipais as $dp )
                {
                    $dp->principal = 0;
                    $dp->update();
                }
            }
            else
            {
                $dadosPrincipais                                    = DadosProfissionais::model()->findAll('Pessoa_id = ' . $cliente->pessoa->id .' AND principal AND habilitado');

                if( count( $dadosPrincipais ) < 1 )
                {
                    $DadosProfissionais['principal']                = 1;
                }

            }

            $dadosProfissionais->attributes                         = $DadosProfissionais;
            $dadosProfissionais->Pessoa_id                          = $cliente->pessoa->id;
            $dadosProfissionais->data_cadastro                      = date('Y-m-d H:i:s');
            $dadosProfissionais->data_cadastro_br                   = date('d/m/Y');

            if( !empty( $DadosProfissionais['data_admissao'] ) )
            {
                $dadosProfissionais->data_admissao                  = $util->view_date_to_bd($dadosProfissionais->data_admissao);
            }
            

            $contato->data_cadastro                                 = date('Y-m-d H:i:s');
            $contato->data_cadastro_br                              = date('d/m/Y');

            if( $contato->save() )
            {

                if( !empty( $TelefoneProfiss['numero'] ) )
                {   
                    $telefone                                       = new Telefone;
                    $telefone->attributes                           = $TelefoneProfiss;
                    $telefone->data_cadastro                        = date('Y-m-d H:i:s');
                    $telefone->data_cadastro_br                     = date('d/m/Y');

                    $telefone->discagem_direta_distancia            = substr($telefone->numero, 0,2);
                    $telefone->numero                               = substr($telefone->numero, 2);

                    if( $telefone->save() )
                    {
                        $contatoHasTelefone                         = new ContatoHasTelefone;
                        $contatoHasTelefone->Contato_id             = $contato->id;
                        $contatoHasTelefone->Telefone_id            = $telefone->id;
                        $contatoHasTelefone->data_cadastro          = date('Y-m-d H:i:s');
                        $contatoHasTelefone->data_cadastro_br       = date('d/m/Y');
                        $contatoHasTelefone->save();
                    }
                }
                
                if( $EmailProfiss != NULL )
                {
                    if( !empty( $EmailProfiss['email'] ) )
                    {
                        $email                                          = new Email;
                        $email->attributes                              = $EmailProfiss;
                        $email->data_cadastro                           = date('Y-m-d H:i:s');
                        $email->data_cadastro_br                        = date('d/m/Y');


                        if( $email->save() )
                        {
                            $contatoHasEmail                            = new ContatoHasEmail;
                            $contatoHasEmail->Contato_id                = $contato->id;
                            $contatoHasEmail->Email_id                  = $email->id;
                            $contatoHasEmail->data_cadastro             = date('Y-m-d H:i:s');
                            $contatoHasEmail->data_cadastro_br          = date('d/m/Y');
                            $contatoHasEmail->save();
                        }
                    }
                }


                $dadosProfissionais->Contato_id                     = $contato->id;

            }

            if( !empty( $EnderecoProfiss['cep'] ) && !empty( $EnderecoProfiss['logradouro'] ) && !empty( $EnderecoProfiss['numero'] ) && !empty( $EnderecoProfiss['cidade'] ) )
            {
                $endereco->attributes                               = $EnderecoProfiss;
                $endereco->Tipo_Endereco_id                         = 1;
                $endereco->data_cadastro                            = date('Y-m-d H:i:s');
                $endereco->data_cadastro_br                         = date('d/m/Y');

                if( $endereco->save() )
                {
                    $dadosProfissionais->Endereco_id                = $endereco->id;
                }
        
                if( $dadosProfissionais->save() )
                {
                    $arrReturn['hasErrors']                 = false;

                    $arrReturn['msgConfig']['pnotify'][]    = array(
                        'titulo'                            => 'Ação realizada com sucesso!',
                        'texto'                             => 'Dados profissionais de ' . strtoupper($cliente->pessoa->nome) . ' cadastrados com sucesso',
                        'tipo'                              => 'success',
                    );
                }
                else
                {
                    $arrReturn['msgConfig']['pnotify'][]    = array(
                        'titulo'                            => 'Erros foram encontrados',
                        'texto'                             => 'Erro ao cadastrar!',
                        'tipo'                              => 'error',
                    );                    
                }

            }
        }

        $statusDoCadastro                       = $cliente->clienteCadastroStatus();
        $arrReturn['statusDoCadastro']          = $statusDoCadastro['status'];

        return $arrReturn;
    }
    
    public function atualizar($DadosProfissionais, $EnderecoProfiss, $TelefoneProfiss, $EmailProfiss = NULL, $email_id = NULL, $dados_profissionais_id){

        $arrReturn                              = array('hasErrors'=>false);
        $util                                   = new Util;
        $dadosProfissionais                     = DadosProfissionais::model()->findByPk($dados_profissionais_id);

        /*Tornando os outros dados profissionais não principais*/
        if( $DadosProfissionais['principal'] )
        {
            $dadosPrincipais                = DadosProfissionais::model()->findAll('Pessoa_id = ' . $dadosProfissionais->Pessoa_id .' AND principal AND habilitado');

            foreach( $dadosPrincipais as $dp )
            {   
                if( $dp->id !=  $dados_profissionais_id )
                {
                    $dp->principal = 0;
                    $dp->update();
                }
            }
        }

        if( !empty( $TelefoneProfiss['id'] ) )
        {
            $telPro                             = Telefone::model()->findByPk($TelefoneProfiss['id']);
            $telPro->attributes                 = $TelefoneProfiss;
            
            if( $telPro->update() )
            {
                $arrReturn['telProId']          = $telPro->id;
            }
            else
            {
                $arrReturn['hasErrors']         = true;
            }
        }

        if( !empty( $email_id ) && $email_id != NULL )
        {
            $emailPro                           = Email::model()->findByPk($email_id);
            $emailPro->attributes               = $EmailProfiss;
            $emailPro->update();
        }

        $endPro                                 = Endereco::model()->findByPk($dadosProfissionais->Endereco_id);

        if( $endPro != NULL )
        {
            $endPro->attributes                 = $EnderecoProfiss;
            
            if( $endPro->update() )
            {
                $arrReturn['endPro']            = $endPro->id;
            }
            else
            {
                $arrReturn['hasErrors']         = true;
            }
        }

        if( !empty($DadosProfissionais['renda_liquida']) )
        {
            $DadosProfissionais['renda_liquida']                    = $util->moedaViewToBD($DadosProfissionais['renda_liquida']);
        }

        if( !empty( $DadosProfissionais['data_admissao'] ) )
        {
            $DadosProfissionais['data_admissao'] = $util->view_date_to_bd($DadosProfissionais['data_admissao']);
        }

        $dadosProfissionais->attributes = $DadosProfissionais;

        if( $dadosProfissionais->update() )
        {
            $arrReturn['dadosProfissionais']    = $dadosProfissionais->id;

            $arrReturn['formConfig'][]          = array(
                'fields'                        => array(
                    'ControllerAction'          => array(
                        'value'                 => 'update',
                        'type'                  => 'text',
                        'input_bind'            => 'ControllerDPAction',
                    ),
                ),
            );

        }
        else
        {
            $arrReturn['hasErrors']             = true;
        }

        if( !$arrReturn['hasErrors'] )
        {
            $arrReturn['msgConfig']['pnotify'][] = array(
                'titulo'    => 'Atualização de dados profissionais!',
                'texto'     => 'Os dados profissionais foram atualizados com sucesso!',
                'tipo'      => 'success',
            );
        }

        else
        {
            $arrReturn['msgConfig']['pnotify'][] = array(
                'titulo'    => 'Erros foram encontrados!',
                'texto'     => 'Não foi possível atualizar os dados profissionais!',
                'tipo'      => 'error',
            );   
        }

        return $arrReturn;
    }

    public function loadFormEdit($id){

        $util                               = new Util;
        $dadosProfissionais                 = DadosProfissionais::model()->findByPk($id);

        $contatoProfissionalHasTelefone     = ContatoHasTelefone::model()->find('Contato_id = ' . $dadosProfissionais->contato->id);
        $contatoProfissionalHasEmail        = ContatoHasEmail::model()->find('Contato_id = ' . $dadosProfissionais->contato->id);

        $tel_id                             = NULL;
        $tel_numero                         = NULL;
        $tel_ramal                          = NULL;
        $tel_tipo                           = NULL;
        $email_id                           = NULL;
        $email                              = NULL;
        $data_admissao                      = ( $dadosProfissionais->data_admissao != NULL ? $util->bd_date_to_view($dadosProfissionais->data_admissao) : $dadosProfissionais->data_admissao );

        if( $contatoProfissionalHasTelefone != NULL )
        {
            $telefoneProfissional           = Telefone::model()->findByPk($contatoProfissionalHasTelefone->Telefone_id);
            
            if( $telefoneProfissional       != NULL )
            {
                $tel_id                     = $telefoneProfissional->id;
                $tel_numero                 = $telefoneProfissional->numero;
                $tel_ramal                  = $telefoneProfissional->ramal;
                $tel_tipo                   = $telefoneProfissional->Tipo_Telefone_id;
            }
        }

        if( $contatoProfissionalHasEmail    != NULL )
        {
            $emailProfissional              = Email::model()->findByPk($contatoProfissionalHasEmail->Email_id);
            if( $emailProfissional          != NULL )
            {
                $email_id                   = $emailProfissional->id;
                $email                      = $emailProfissional->email;
            }
        }

        return array(

            'fields'                            => array(
                'id'                            => array(
                    'value'                     => $dadosProfissionais->id,
                    'type'                      => 'text',
                    'input_bind'                => 'dados_profissionais_id',
                ),
                'Classe_Profissional_id'        => array(
                    'value'                     => $dadosProfissionais->Classe_Profissional_id,
                    'type'                      => 'select',
                    'input_bind'                => 'DadosProfissionais_Classe_Profissional_id',
                ),
                'ControllerDPAction'            => array(
                    'value'                     => 'update',
                    'type'                      => 'text',
                    'input_bind'                => 'ControllerDPAction',
                ),
                'cnpj_cpf'                      => array(
                    'value'                     => $dadosProfissionais->cnpj_cpf,
                    'type'                      => 'text',
                    'input_bind'                => 'dp_cnpj_cpf',
                ),
                'dp_meses_de_servico'           => array(
                    'value'                     => $dadosProfissionais->tempo_de_servico_meses,
                    'type'                      => 'text',
                    'input_bind'                => 'dp_meses_de_servico',
                ),
                'dp_anos_de_servico'            => array(
                    'value'                     => $dadosProfissionais->tempo_de_servico_anos,
                    'type'                      => 'text',
                    'input_bind'                => 'dp_anos_de_servico',
                ),
                'empresa'                       => array(
                    'value'                     => $dadosProfissionais->empresa,
                    'type'                      => 'text',
                    'input_bind'                => 'dp_empresa',
                ),
                'Tipo_Ocupacao_id'              => array(
                    'value'                     => $dadosProfissionais->Tipo_Ocupacao_id,
                    'type'                      => 'select',
                    'input_bind'                => 'DadosProfissionais_Tipo_Ocupacao_id',
                ),
                'data_admissao'                 => array(
                    'value'                     => $data_admissao,
                    'type'                      => 'text',
                    'input_bind'                => 'dp_data_admissao',
                ),
                'profissao'                     => array(
                    'value'                     => $dadosProfissionais->profissao,
                    'type'                      => 'select',
                    'input_bind'                => 'DadosProfissionais_profissao',
                ),
                'renda_liquida'                 => array(
                    'value'                     => number_format($dadosProfissionais->renda_liquida,2,',','.'),
                    'type'                      => 'text',
                    'input_bind'                => 'dp_renda_liquida',
                ),
                'mes_ano_renda'                 => array(
                    'value'                     => $dadosProfissionais->mes_ano_renda,
                    'type'                      => 'text',
                    'input_bind'                => 'dp_mes_ano_renda',
                ),
                'orgao_pagador'                 => array(
                    'value'                     => $dadosProfissionais->orgao_pagador,
                    'type'                      => 'select',
                    'input_bind'                => 'DadosProfissionais_orgao_pagador',
                ),
                'numero_do_beneficio'           => array(
                    'value'                     => $dadosProfissionais->numero_do_beneficio,
                    'type'                      => 'text',
                    'input_bind'                => 'dp_numero_do_beneficio',
                ),
                'tipo_de_comprovante'           => array(
                    'value'                     => $dadosProfissionais->tipo_de_comprovante,
                    'type'                      => 'select',
                    'input_bind'                => 'DadosProfissionais_TipoDeComprovante',
                ),
                'dp_endereco_cep'               => array(
                    'value'                     => $dadosProfissionais->endereco->cep,
                    'type'                      => 'text',
                    'input_bind'                => 'cep_dados_profissionais',
                ),
                'dp_endereco_logradouro'        => array(
                    'value'                     => $dadosProfissionais->endereco->logradouro,
                    'type'                      => 'text',
                    'input_bind'                => 'logradouro_dados_profissionais',
                ),
                'dp_endereco_numero'            => array(
                    'value'                     => $dadosProfissionais->endereco->numero,
                    'type'                      => 'text',
                    'input_bind'                => 'numero_endereco_dados_profissionais',
                ),
                'dp_endereco_cidade'            => array(
                    'value'                     => $dadosProfissionais->endereco->cidade,
                    'type'                      => 'text',
                    'input_bind'                => 'cidade_dados_profissionais',
                ),
                'dp_endereco_bairro'            => array(
                    'value'                     => $dadosProfissionais->endereco->bairro,
                    'type'                      => 'text',
                    'input_bind'                => 'bairro_dados_profissionais',
                ),
                'dp_endereco_uf'                => array(
                    'value'                     => $dadosProfissionais->endereco->uf,
                    'type'                      => 'select',
                    'input_bind'                => 'EnderecoProfiss_uf',
                ),
                'dp_endereco_complemento'       => array(
                    'value'                     => $dadosProfissionais->endereco->complemento,
                    'type'                      => 'text',
                    'input_bind'                => 'dp_endereco_complemento',
                ),
                'dp_telefone_id'                => array(
                    'value'                     => $tel_id,
                    'type'                      => 'text',
                    'input_bind'                => 'dp_telefone_id',
                ),
                'dp_telefone_numero'            => array(
                    'value'                     => $tel_numero,
                    'type'                      => 'text',
                    'input_bind'                => 'dp_telefone_numero',
                ),
                'dp_telefone_ramal'             => array(
                    'value'                     => $tel_ramal,
                    'type'                      => 'text',
                    'input_bind'                => 'dp_telefone_ramal',
                ),
                'dp_telefone_tipo'              => array(
                    'value'                     => $tel_tipo,
                    'type'                      => 'select',
                    'input_bind'                => 'TelefoneProfiss_Tipo_Telefone_id',
                ),
                'dp_email_id'                   => array(
                    'value'                     => $email,
                    'type'                      => 'text',
                    'input_bind'                => 'dp_email_id',
                ),
                'dp_email'                      => array(
                    'value'                     => $email,
                    'type'                      => 'text',
                    'input_bind'                => 'dp_email',
                ),
                'DadosProfissionais_principal'  => array(
                    'value'                     => $dadosProfissionais->principal,
                    'type'                      => 'select',
                    'input_bind'                => 'DadosProfissionais_principal',
                ),
            ),
            'form_params'                       => array(
                'id'                            => 'form-add-dado-profissional-client',
                'action'                        => '/dadosProfissionais/update/',
            ),
            'modal_id'                          => 'modal_form_new_dados_profissionais'
        );
    }
    
    public function tableName() {

        return 'Dados_Profissionais';        
    }

    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Pessoa_id', 'required'),
            array('Pessoa_id, Classe_Profissional_id, tempo_de_servico_meses, tempo_de_servico_anos, Endereco_id, Contato_id, Tipo_Ocupacao_id, aposentado, pensionista, habilitado, principal', 'numerical', 'integerOnly' => true),
            array('renda_liquida', 'numerical'),
            array('empresa, cnpj_cpf, profissao, numero_do_beneficio', 'length', 'max' => 100),
            array('tipo_de_comprovante', 'length', 'max' => 45),
            array('orgao_pagador', 'length', 'max' => 150),
            array('data_admissao, mes_ano_renda, data_cadastro, data_cadastro_br, cnpj_cpf', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, empresa, cnpj_cpf, tempo_de_servico_anos, tempo_de_servico_meses, data_admissao, Pessoa_id, Classe_Profissional_id, Endereco_id, Contato_id, Tipo_Ocupacao_id, renda_liquida, aposentado, pensionista, tipo_de_comprovante, profissao, mes_ano_renda, numero_do_beneficio, orgao_pagador, habilitado, data_cadastro, data_cadastro_br, principal', 'safe', 'on' => 'search'),
        );
    }

    public function relations(){

        return array(
            'pessoa'                => array(self::BELONGS_TO, 'Pessoa', 'Pessoa_id'),
            'endereco'              => array(self::BELONGS_TO, 'Endereco', 'Endereco_id'),
            'contato'               => array(self::BELONGS_TO, 'Contato', 'Contato_id'),
            'tipoOcupacao'          => array(self::BELONGS_TO, 'TipoOcupacao', 'Tipo_Ocupacao_id'),
            'classeProfissional'    => array(self::BELONGS_TO, 'ClasseProfissional', 'Classe_Profissional_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'id'                        => 'ID',
            'empresa'                   => 'Empresa',
            'data_admissao'             => 'Data Admissao',
            'Pessoa_id'                 => 'Pessoa',
            'Endereco_id'               => 'Endereco',
            'Contato_id'                => 'Contato',
            'Tipo_Ocupacao_id'          => 'Tipo Ocupacao',
            'Classe_Profissional_id'    => 'Classe Profissional',
            'renda_liquida'             => 'Renda Liquida',
            'aposentado'                => 'Aposentado',
            'pensionista'               => 'Pensionista',
            'tipo_de_comprovante'       => 'Tipo De Comprovante',
            'profissao'                 => 'Profissao',
            'mes_ano_renda'             => 'Mes Ano Renda',
            'numero_do_beneficio'       => 'Numero Do Beneficio',
            'orgao_pagador'             => 'Orgao Pagador',
            'habilitado'                => 'Habilitado',
            'data_cadastro'             => 'Data Cadastro',
            'data_cadastro_br'          => 'Data / Hora Cadastro',
            'cnpj_cpf'                  => 'Cpf / CNPJ',
            'principal'                 => 'Usar como Principal?',
            'tempo_de_servico_anos'     => 'Anos de serviço',
            'tempo_de_servico_meses'    => 'Meses de serviço'
        );
    }

    public function search() {

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('empresa', $this->empresa, true);
        $criteria->compare('data_admissao', $this->data_admissao, true);
        $criteria->compare('Pessoa_id', $this->Pessoa_id);
        $criteria->compare('Endereco_id', $this->Endereco_id);
        $criteria->compare('Contato_id', $this->Contato_id);
        $criteria->compare('Tipo_Ocupacao_id', $this->Tipo_Ocupacao_id);
        $criteria->compare('Classe_Profissional_id', $this->Classe_Profissional_id);
        $criteria->compare('renda_liquida', $this->renda_liquida);
        $criteria->compare('aposentado', $this->aposentado);
        $criteria->compare('pensionista', $this->pensionista);
        $criteria->compare('cnpj_cpf', $this->cnpj_cpf);
        $criteria->compare('tipo_de_comprovante', $this->tipo_de_comprovante, true);
        $criteria->compare('profissao', $this->profissao, true);
        $criteria->compare('mes_ano_renda', $this->mes_ano_renda, true);
        $criteria->compare('numero_do_beneficio', $this->numero_do_beneficio, true);
        $criteria->compare('orgao_pagador', $this->orgao_pagador, true);
        $criteria->compare('habilitado', $this->habilitado);
        $criteria->compare('data_cadastro', $this->data_cadastro, true);
        $criteria->compare('data_cadastro_br', $this->data_cadastro_br, true);
        $criteria->compare('principal', $this->principal);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__) {

        return parent::model($className);
    }

    public function clearEndereco() {
        
        $endereco = Endereco::model()->findByPk($this->Endereco_id);
        $endereco->delete();      
    }

    public function clearContato() {
        
        $contato = Contato::model()->findByPk($this->Contato_id);
        $contato->delete();      
    }

}
