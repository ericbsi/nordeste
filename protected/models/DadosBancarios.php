<?php


class DadosBancarios extends CActiveRecord{

	public function atualizar($DadosBancarios, $DadosBancariosId){

		$db 			= DadosBancarios::model()->findByPk($DadosBancariosId);
		$db->attributes = $DadosBancarios;
		$db->update();
                $sigacLog = new SigacLog;
                $sigacLog->saveLog('Dados Bancarios alterados','DadosBancarios', $db->id, date('Y-m-d H:i:s'),1, Yii::app()->session['usuario']->id, "Usuario ". Yii::app()->session['usuario']->username . " alterou dados bancarios.", "127.0.0.1",NULL, Yii::app()->session->getSessionId() );
	}

	public function loadFormEdit($id){
		
		$dadosBancarios 			= DadosBancarios::model()->findByPk($id);
		$data_abertura 				= ($dadosBancarios->data_abertura != NULL && !empty($dadosBancarios->data_abertura) ? $dadosBancarios->data_abertura : "");

	    return array(

	    	'fields' 				=> array(

		    	'id' 				=> array(
		    		'value' 		=> $dadosBancarios->id,
		    		'type' 			=> 'text',
		    		'input_bind'	=> 'dados_bancarios_id',
		    	),
		    	'numero'			=> array(
		    		'value' 		=> $dadosBancarios->numero,
		    		'type' 			=> 'text',
		    		'input_bind'	=> 'conta_numero',
		    	),
		    	'Tipo_Conta'		=> array(
		    		'value' 		=> $dadosBancarios->Tipo_Conta_Bancaria_id,
		    		'type' 			=> 'select',
		    		'input_bind'	=> 'DadosBancarios_Tipo_Conta_Bancaria_id',
		    	),
		    	'Banco_Conta'		=> array(
		    		'value' 		=> $dadosBancarios->Banco_id,
		    		'type' 			=> 'select',
		    		'input_bind'	=> 'DadosBancarios_Banco_id',
		    	),
		    	'agencia'			=> array(
		    		'value' 		=> $dadosBancarios->agencia,
		    		'type' 			=> 'text',
		    		'input_bind'	=> 'conta_agencia',
		    	),
		    	'operacao'			=> array(
		    		'value' 		=> $dadosBancarios->operacao,
		    		'type' 			=> 'text',
		    		'input_bind'	=> 'conta_operacao',
		    	),
		    	'data_abertura'		=> array(
		    		'value' 		=> $data_abertura,
		    		'type' 			=> 'text',
		    		'input_bind'	=> 'conta_data_abertura',
		    	),
	    	),
	    	'form_params'			=> array(
	    		'id'				=> 'form-add-dado-bancario-client',
	    		'action'			=> '/dadosBancarios/update/',
	    	),
	    	'modal_id'				=> 'modal_form_new_conta_bancaria'
	    );
	}

	public function novo($dadosBancariosArr,$cliente_id){

		$cliente 								= Cliente::model()->findByPk($cliente_id);
		$dadosBancarios 						= new DadosBancarios;
		$dadosBancarios->attributes 			= $dadosBancariosArr;
		$dadosBancarios->data_cadastro 			= date('Y-m-d H:i:s');
		$dadosBancarios->data_cadastro_br 		= date('d/m/Y H:i:s');
		
		/*
		$dadosBancarios->numero 				= $dadosBancarios['numero'];
		$dadosBancarios->Tipo_Conta_Bancaria_id = $dadosBancarios['Tipo_Conta_Bancaria_id'];
		$dadosBancarios->agencia           		= $dadosBancarios['agencia'];
		$dadosBancarios->Banco_id 				= $dadosBancarios['Banco_id'];
		$dadosBancarios->operacao 				= $dadosBancarios['operacao'];
		$dadosBancarios->data_abertura 			= $dadosBancarios['data_abertura'];
		*/
		

		if( $dadosBancarios->save() )
		{
			$pessoaHasDadosBancarios 			= new PessoaHasDadosBancarios   ;
			$pessoaHasDadosBancarios->Pessoa_id             = $cliente->pessoa->id          ;
			$pessoaHasDadosBancarios->Dados_Bancarios_id 	= $dadosBancarios->id           ;
			$pessoaHasDadosBancarios->data_cadastro         = date("Y-m-d H:i:s")           ;
			$pessoaHasDadosBancarios->habilitado            = 1                             ;
			$pessoaHasDadosBancarios->save()                                                ;
		}
	}

	public function tableName()
	{
		return 'Dados_Bancarios';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('Tipo_Conta_Bancaria_id, Banco_id', 'required'),
			array('habilitado, Tipo_Conta_Bancaria_id, Banco_id', 'numerical', 'integerOnly'=>true),
			array('agencia, operacao, numero, data_abertura', 'length', 'max'=>45),
			array('data_cadastro, data_cadastro_br', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, agencia, operacao, numero, data_abertura, habilitado, data_cadastro, data_cadastro_br, Tipo_Conta_Bancaria_id, Banco_id, cgc', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tipoContaBancaria' => array(self::BELONGS_TO, 'TipoContaBancaria', 'Tipo_Conta_Bancaria_id'),
			'banco' => array(self::BELONGS_TO, 'Banco', 'Banco_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'agencia' => 'Agencia',
			'operacao' => 'Operacao',
			'numero' => 'Numero da conta',
			'data_abertura' => 'Data de Abertura',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'data_cadastro_br' => 'Data / Hora Cadastro',
			'Tipo_Conta_Bancaria_id' => 'Tipo Conta Bancaria',
			'Banco_id' => 'Banco',
			'cgc' => 'CGC',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('agencia',$this->agencia,true);
		$criteria->compare('operacao',$this->operacao,true);
		$criteria->compare('numero',$this->numero,true);
		$criteria->compare('data_abertura',$this->data_abertura,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('data_cadastro_br',$this->data_cadastro_br,true);
		$criteria->compare('Tipo_Conta_Bancaria_id',$this->Tipo_Conta_Bancaria_id);
		$criteria->compare('Banco_id',$this->Banco_id);
		$criteria->compare('cgc',$this->cgc);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DadosBancarios the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
