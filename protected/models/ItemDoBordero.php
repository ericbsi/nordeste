<?php

/**
 * This is the model class for table "ItemDoBordero".
 *
 * The followings are the available columns in table 'ItemDoBordero':
 * @property integer $id
 * @property integer $Bordero
 * @property integer $Status
 * @property integer $Proposta_id
 * @property integer $habilitado
 * @property integer $Parcela_id
 * @property integer $pagoImagem
 *
 * The followings are the available model relations:
 * @property Parcela $parcela
 * @property Bordero $bordero
 * @property Proposta $proposta
 * @property StatusItemDoBordero $status
 */
class ItemDoBordero extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ItemDoBordero';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Bordero, Status, Proposta_id', 'required'),
			array('Bordero, Status, Proposta_id, habilitado, Parcela_id, pagoImagem', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, Bordero, Status, Proposta_id, habilitado, Parcela_id, pagoImagem', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'parcela' => array(self::BELONGS_TO, 'Parcela', 'Parcela_id'),
			'bordero' => array(self::BELONGS_TO, 'Bordero', 'Bordero'),
			'proposta' => array(self::BELONGS_TO, 'Proposta', 'Proposta_id'),
			'status' => array(self::BELONGS_TO, 'StatusItemDoBordero', 'Status'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Bordero' => 'Bordero',
			'Status' => 'Status',
			'Proposta_id' => 'Proposta',
			'habilitado' => 'Habilitado',
			'Parcela_id' => 'Parcela',
			'pagoImagem' => 'Pago Imagem',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Bordero',$this->Bordero);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('Proposta_id',$this->Proposta_id);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('Parcela_id',$this->Parcela_id);
		$criteria->compare('pagoImagem',$this->pagoImagem);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ItemDoBordero the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
