<?php

class Referencia extends CActiveRecord{
	
	public function toXMLOmni()
	{
		return '<telefone>
				<tipo>'.$this->tipoReferencia->tipoOmni.'</tipo>
				<ddd>'.$this->getTelefone()->getDDD().'</ddd>
				<numero>'.$this->getTelefone()->getNumero().'</numero>
				<contato>'.strtoupper($this->nome).'</contato>
				<grau>'.strtoupper($this->parentesco).'</grau>
		</telefone>';
	}

        
        public function toXMLSemear(){
            $tipoRef = '';
            
            if($this->tipoReferencia->id == 1){
                $tipoRef = 'RP';
            }
            
            if($this->tipoReferencia->id == 2){
                $tipoRef = 'RC';
            }
            
            return '<referencia>
			<tipoRef>'.$tipoRef.'</tipoRef>
                        <nome>'.$this->nome.'</nome>
                        <ddd>'.$this->getTelefone()->getDDD().'</ddd>
                        <fone>'.substr($this->getTelefone()->getNumero(), -8).'</fone>
                    </referencia>';
        }
        
	public function persist( $R1, $TR1, $R2, $TR2, $R3, $TR3, $clienteId, $ref1Id, $ref2Id, $ref3Id, $t1_id, $t2_id, $t3_id, $ControllerAction )
	{
		if( $ControllerAction == 'new' )
		{
			return $this->novasReferencias( $R1, $TR1, $R2, $TR2, $R3, $TR3, $clienteId );
		}

		else if( $ControllerAction == 'update' )
		{
			return $this->atualizarReferencias( $ref1Id, $ref2Id, $ref3Id, $t1_id, $t2_id, $t3_id, $R1, $TR1, $R2, $TR2, $R3, $TR3, $clienteId );
		}
	}

	public function atualizarReferencias( $idR1, $idR2, $idR3, $idTR1, $idTR2, $idTR3, $R1, $TR1, $R2, $TR2, $R3, $TR3, $Cliente_id )
	{
		$arrReturn 	= array('hasErrors' => false);

		if( $idR1 	!= 0 )
		{	
			if( !empty( $R1['parentesco'] ) && $R1['parentesco'] != '' && $R1['parentesco'] != NULL )
			{
				$ra 					= $this->atualizar( $R1, $TR1, $idR1, $idTR1 );
				$arrReturn['hasErrors']	= $ra['hasErrors'];
			}
		}
		else
		{
			if( !empty( $R1['parentesco'] ) && $R1['parentesco'] != '' && $R1['parentesco'] != NULL )
			{
				$rn = $this->novo($R1, $TR1, $Cliente_id);

				if( !$rn['hasErrors'] )
				{
					$arrReturn['formConfig'][] 			= array(
						'fields' 						=> array(
							'ControllerAction' 			=> array(
								'value' 				=> 'update',
								'type' 					=> 'text',
								'input_bind'			=> 'ControllerReferenciaAction',
							),
							'id' 						=> array(
								'value' 				=> $rn['referenciaId'],
								'type' 					=> 'text',
								'input_bind'			=> 'ref1Id',
							),
							't1id' 						=> array(
								'value' 				=> $rn['telefoneId'],
								'type' 					=> 'text',
								'input_bind'			=> 't1_id',
							),
						),
					);
				}
				else
				{
					$arrReturn['hasErrors'] = true;
				}
			}		
		}

		if( $idR2 != 0 )
		{	
			if( !empty( $R2['parentesco'] ) && $R2['parentesco'] != '' && $R2['parentesco'] != NULL )
			{
				$ra 					= $this->atualizar( $R2, $TR2, $idR2, $idTR2 );
			}
		}
		else
		{
			if( !empty( $R2['parentesco'] ) && $R2['parentesco'] != '' && $R2['parentesco'] != NULL )
			{
				$rn = $this->novo($R2, $TR2, $Cliente_id);

				if( !$rn['hasErrors'] )
				{
					$arrReturn['formConfig'][] 			= array(
						'fields' 						=> array(
							'id2' 						=> array(
								'value' 				=> $rn['referenciaId'],
								'type' 					=> 'text',
								'input_bind'			=> 'ref2Id',
							),
							't2id' 						=> array(
								'value' 				=> $rn['telefoneId'],
								'type' 					=> 'text',
								'input_bind'			=> 't2_id',
							),
						),
					);
				}
				else
				{
					$arrReturn['hasErrors'] = true;
				}
			}
		}

		if( $idR3 != 0 )
		{
			if( !empty( $R3['parentesco'] ) && $R3['parentesco'] != '' && $R3['parentesco'] != NULL )
			{
				$ra 					= $this->atualizar( $R3, $TR3, $idR3, $idTR3 );
				$arrReturn['hasErrors']	= $ra['hasErrors'];
			}
		}
		else
		{
			if( !empty( $R3['parentesco'] ) && $R3['parentesco'] != '' && $R3['parentesco'] != NULL )
			{
				$rn = $this->novo($R3, $TR3, $Cliente_id);

				if( !$rn['hasErrors'] )
				{
					$arrReturn['formConfig'][] 			= array(
						'fields' 						=> array(
							'id3' 						=> array(
								'value' 				=> $rn['referenciaId'],
								'type' 					=> 'text',
								'input_bind'			=> 'ref3Id',
							),
							't3id' 						=> array(
								'value' 				=> $rn['telefoneId'],
								'type' 					=> 'text',
								'input_bind'			=> 't3_id',
							),
						),
					);
				}
				else
				{
					$arrReturn['hasErrors'] = true;
				}
			}
		}

		if( !$arrReturn['hasErrors'] )
		{
			$arrReturn['msgConfig']['pnotify'][] = array(
				'titulo'    => 'Ação realizada com sucesso!',
				'texto'     => 'Os telefones de contato foram atualizados com sucesso!',
				'tipo'      => 'success',
			);
		}

		else
		{
			$arrReturn['msgConfig']['pnotify'][] = array(
				'titulo'    => 'Erros foram encontrados!',
				'texto'     => 'Não foi possível atualizar os contatos. Entre em contato com o suporte!',
				'tipo'      => 'error',
			);
		}

		return $arrReturn;
	}

	public function novasReferencias($R1, $TR1, $R2, $TR2, $R3, $TR3, $Cliente_id)
	{
		$cliente 	= Cliente::model()->findByPk($Cliente_id);

		$arrReturn 	= array('hasErrors' => false);

		if( !empty( $R1['parentesco'] ) && $R1['parentesco'] != '' && $R1['parentesco'] != NULL )
		{
			$r = $this->novo($R1, $TR1, $Cliente_id);

			$ref1 									= Referencia::model()->findByPk($r['referenciaId']);
			$telRef1								= Telefone::model()->findByPk($r['telefoneId']);

			if( $ref1 != NULL )
			{
				if( !$r['hasErrors'] )
				{
					$arrReturn['formConfig'][] 			= array(
						'fields' 						=> array(
							'ControllerAction' 			=> array(
								'value' 				=> 'update',
								'type' 					=> 'text',
								'input_bind'			=> 'ControllerReferenciaAction',
							),
							'id' 						=> array(
								'value' 				=> $ref1->id,
								'type' 					=> 'text',
								'input_bind'			=> 'ref1Id',
							),
							't1id' 						=> array(
								'value' 				=> $r['telefoneId'],
								'type' 					=> 'text',
								'input_bind'			=> 't1_id',
							),
							'referenciaRarentesco'		=> array(
								'value' 				=> $ref1->parentesco,
								'type' 					=> 'text',
								'input_bind'			=> 'referencia1_parentesco',
							),
							'referenciaNome'			=> array(
								'value' 				=> $ref1->nome,
								'type' 					=> 'text',
								'input_bind'			=> 'referencia1_nome',
							),
							'telefoneReferenciaNumero'	=> array(
								'value' 				=> $telRef1->numero,
								'type' 					=> 'text',
								'input_bind'			=> 'telefone_referencia1_numero',
							),
							'RefTipoReferencia_id'		=> array(
								'value' 				=> $ref1->Tipo_Referencia_id,
								'type' 					=> 'select',
								'input_bind'			=> 'Referencia1_Tipo_Referencia_id',
							),
						),
					);				
				}
				else
				{
					$arrReturn['hasErrors'] = true;
				}
			}
		}

		if( !empty( $R2['parentesco'] ) && $R2['parentesco'] != '' && $R2['parentesco'] != NULL )
		{
			$r = $this->novo($R2, $TR2, $Cliente_id);

			$ref2 									= Referencia::model()->findByPk($r['referenciaId']);
			$telRef2								= Telefone::model()->findByPk($r['telefoneId']);

			if( $ref2 != NULL )
			{
				if( !$r['hasErrors'] )
				{
					$arrReturn['formConfig'][] 			= array(
						'fields' 						=> array(
							'id2' 						=> array(
								'value' 				=> $ref2->id,
								'type' 					=> 'text',
								'input_bind'			=> 'ref2Id',
							),
							't2id' 						=> array(
								'value' 				=> $r['telefoneId'],
								'type' 					=> 'text',
								'input_bind'			=> 't2_id',
							),
							'referencia2Rarentesco'		=> array(
								'value' 				=> $ref2->parentesco,
								'type' 					=> 'text',
								'input_bind'			=> 'referencia2_parentesco',
							),
							'referencia2Nome'			=> array(
								'value' 				=> $ref2->nome,
								'type' 					=> 'text',
								'input_bind'			=> 'referencia2_nome',
							),
							'telefoneReferencia2Numero'	=> array(
								'value' 				=> $telRef2->numero,
								'type' 					=> 'text',
								'input_bind'			=> 'telefone_referencia2_numero',
							),
							'RefTipoReferencia2_id'		=> array(
								'value' 				=> $ref2->Tipo_Referencia_id,
								'type' 					=> 'select',
								'input_bind'			=> 'Referencia2_Tipo_Referencia_id',
							),
						),
					);				
				}
				else
				{
					$arrReturn['hasErrors'] = true;
				}
			}
		}

		if( !empty( $R3['parentesco'] ) && $R3['parentesco'] != '' && $R3['parentesco'] != NULL )
		{
			$r = $this->novo($R3, $TR3, $Cliente_id);

			$ref3 									= Referencia::model()->findByPk($r['referenciaId']);
			$telRef2								= Telefone::model()->findByPk($r['telefoneId']);

			if( $ref3 != NULL )
			{
				if( !$r['hasErrors'] )
				{
					$arrReturn['formConfig'][] 			= array(
						'fields' 						=> array(
							'id3' 						=> array(
								'value' 				=> $ref3->id,
								'type' 					=> 'text',
								'input_bind'			=> 'ref3Id',
							),
							't3id' 						=> array(
								'value' 				=> $r['telefoneId'],
								'type' 					=> 'text',
								'input_bind'			=> 't3_id',
							),
							'referencia3Rarentesco'		=> array(
								'value' 				=> $ref3->parentesco,
								'type' 					=> 'text',
								'input_bind'			=> 'referencia3_parentesco',
							),
							'referencia3Nome'			=> array(
								'value' 				=> $ref3->nome,
								'type' 					=> 'text',
								'input_bind'			=> 'referencia3_nome',
							),
							'telefoneReferencia3Numero'	=> array(
								'value' 				=> $telRef2->numero,
								'type' 					=> 'text',
								'input_bind'			=> 'telefone_referencia3_numero',
							),
							'RefTipoReferencia3_id'		=> array(
								'value' 				=> $ref3->Tipo_Referencia_id,
								'type' 					=> 'select',
								'input_bind'			=> 'Referencia3_Tipo_Referencia_id',
							),
						),
					);				
				}
				else
				{
					$arrReturn['hasErrors'] = true;
				}
			}
		}

		if( !$arrReturn['hasErrors'] )
		{
			$arrReturn['msgConfig']['pnotify'][] = array(
				'titulo'    => 'Ação realizada com sucesso!',
				'texto'     => 'A referência foi cadastrada, e faz parte dos contatos do cliente ' . strtoupper($cliente->pessoa->nome),
				'tipo'      => 'success',
			);
		}
		else
		{
			$arrReturn['msgConfig']['pnotify'][] = array(
			    'titulo'    => 'Erros foram encontrados!',
			    'texto'     => 'Não foi possível cadastrar a referência!',
				'tipo'      => 'error',
			);
		}

		return $arrReturn;
	}

	public function novo($Referencia, $TelefoneReferencia, $Cliente_id){

		$arrReturn 	= array('hasErrors' => true);
		$cliente 	= Cliente::model()->findByPk( $Cliente_id );
		
		if( !empty( $Referencia['nome'] ) )
		{
			$referencia 								= new Referencia;
			$referencia->attributes						= $Referencia;
			$referencia->data_cadastro 					= date('Y-m-d H:i:s');
			$referencia->data_cadastro_br 				= date('d/m/Y');
			$referencia->habilitado 					= 1;
			$referencia->Cliente_id 					= $Cliente_id;
			
			if( !empty( $TelefoneReferencia['numero'] ) )
			{
				$telRef 								= new Telefone;				
				$telRef->attributes 					= $TelefoneReferencia;
				$telRef->Tipo_Telefone_id 				= 3;
				$telRef->data_cadastro 					= date('Y-m-d H:i:s');
				$telRef->data_cadastro_br 				= date('d/m/Y');

				$telRef->discagem_direta_distancia  	= substr($telRef->numero, 0,2);
				$telRef->numero  						= substr($telRef->numero, 2);
			}

			if( $telRef->save() )
			{
				$contatoHasTelefones 					= new ContatoHasTelefone;
				$contatoHasTelefones->data_cadastro 	= date('Y-m-d H:i:s');
				$contatoHasTelefones->data_cadastro_br	= date('d/m/Y');
				$contatoReferencia 						= new Contato;
				$contatoReferencia->data_cadastro 		= date('Y-m-d H:i:s');
				$contatoReferencia->data_cadastro_br	= date('d/m/Y');
				$arrReturn['telefoneId'] 				= $telRef->id;

				if( $contatoReferencia->save() )
				{
					$referencia->Contato_id 			= $contatoReferencia->id;
					$contatoHasTelefones->Contato_id 	= $contatoReferencia->id;
					$contatoHasTelefones->Telefone_id 	= $telRef->id;
					$contatoHasTelefones->save();
					
					if( $referencia->save() )					
					{
						$arrReturn['hasErrors'] 			= false;
						$arrReturn['referenciaId'] 			= $referencia->id;
				
						if($cliente != NULL)
						{
							$arrReturn['msgConfig']['pnotify'][] = array(
				                'titulo'    => 'Ação realizada com sucesso!',
				                'texto'     => 'A referência foi cadastrada, e faz parte dos contatos do cliente ' . strtoupper($cliente->pessoa->nome),
				                'tipo'      => 'success',
			            	);
						}
					}
					else
					{
						$arrReturn['msgConfig']['pnotify'][] = array(
			                'titulo'    => 'Erros foram encontrados!',
			                'texto'     => 'Não foi possível cadastrar a referência!',
			                'tipo'      => 'error',
			            );
			            $arrReturn['hasErrors'] 			= true;
					}
				}
			}
		}

		$statusDoCadastro                       = $cliente->clienteCadastroStatus();
        $arrReturn['statusDoCadastro']			= $statusDoCadastro['status'];
		
		return $arrReturn;
	}

	public function atualizar($Referencia, $TelefoneReferencia, $referencia_cliente_id, $telefone_referencia_id){

		$arrReturn = array('hasErrors' => false);

		if( !empty($referencia_cliente_id) && !empty($telefone_referencia_id) )
		{
			$referencia 	= Referencia::model()->findByPk($referencia_cliente_id);
			$telefone 		= Telefone::model()->findByPk($telefone_referencia_id);

			if( $referencia != NULL )
			{
				if( !empty($Referencia['nome']) )
				{
					$referencia->attributes = $Referencia;
					
					if( !$referencia->update() )
					{
						$arrReturn['hasErrors'] = true;
					}
				}
			}

			if( $telefone != NULL )
			{
				if( !empty($TelefoneReferencia['numero']) )
				{	
					$telefone->attributes = $TelefoneReferencia;
					
					if( !$telefone->update() )
					{
						$arrReturn['hasErrors'] = true;
					}
				}
			}
		}

		return $arrReturn;
	}

	public function loadFormEdit($id){

		$referencia 				= Referencia::model()->findByPk($id);
		$contato 					= Contato::model()->findByPk($referencia->Contato_id);

		$telefone_numero 			=	"";
		$telefone_id 				=	"";

		if( $contato != NULL )
		{
			$contatoHasTelefone 	= ContatoHasTelefone::model()->find('Contato_id = ' . $contato->id);

			if( $contatoHasTelefone != NULL )
			{
				$telefone 				= Telefone::model()->findByPk($contatoHasTelefone->Telefone_id);

				if( $telefone != NULL )
				{
					$telefone_id 				=	$telefone->id;
					$telefone_numero 			=	$telefone->numero;
				}
			}
		}
		
	    return array(
	    	'fields' 							=> array(
		    	'id' 							=> array(
		    		'value' 					=> $referencia->id,
		    		'type' 						=> 'text',
		    		'input_bind'				=> 'referencia_cliente_id',
		    	),
		    	'parentesco'					=> array(
		    		'value' 					=> $referencia->parentesco,
		    		'type' 						=> 'text',
		    		'input_bind'				=> 'referencia_parentesco',
		    	),
		    	'nome'							=> array(
		    		'value' 					=> $referencia->nome,
		    		'type' 						=> 'text',
		    		'input_bind'				=> 'referencia_nome',
		    	),
		    	'tipo'							=> array(
		    		'value' 					=> $referencia->Tipo_Referencia_id,
		    		'type' 						=> 'select',
		    		'input_bind'				=> 's2id_Referencia_Tipo_Referencia_id',
		    	),
		    	'telefone_referencia_id'		=> array(
		    		'value' 					=> $telefone_id,
		    		'type' 						=> 'text',
		    		'input_bind'				=> 'telefone_referencia_id',
		    	),
		    	'telefone_referencia_numero'	=> array(
		    		'value' 					=> $telefone_numero,
		    		'type' 						=> 'text',
		    		'input_bind'				=> 'telefone_referencia_numero',
		    	)
	    	),
	    	'form_params'						=> array(
	    		'id'							=> 'form-add-referencia',
	    		'action'						=> '/referencia/update/',
	    	),
	    	'modal_id'							=> 'modal_form_new_referencia'
	    );
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName(){

		return 'Referencia';
	}

	public function getTelefone(){

		$contato = Contato::model()->findByPk($this->Contato_id);
		$contatoHasTelefone = ContatoHasTelefone::model()->find('Contato_id = ' . $contato->id);
		
		return Telefone::model()->findByPk($contatoHasTelefone->Telefone_id);
	}


	public function rules(){

		return array(
			array('Cliente_id, Contato_id, Tipo_Referencia_id, habilitado', 'numerical', 'integerOnly'=>true),
			array('nome, parentesco', 'length', 'max'=>145),
			array('data_cadastro, data_cadastro_br', 'safe'),
			array('id, nome, parentesco, Cliente_id, Contato_id, Tipo_Referencia_id, data_cadastro, data_cadastro_br, habilitado', 'safe', 'on'=>'search'),
		);
	}

	public function relations(){

		
		return array(
			'cliente' 			=> array(self::BELONGS_TO, 		'Cliente', 'Cliente_id'),
			'contato' 			=> array(self::BELONGS_TO, 		'Contato', 'Contato_id'),
			'tipoReferencia' 	=> array(self::BELONGS_TO, 		'TipoReferencia', 'Tipo_Referencia_id'),
		);
	}

	public function attributeLabels(){

		return array(
			'id' 					=> 'ID',
			'parentesco' 			=> 'Parentesco',
			'nome' 					=> 'Nome',
			'Cliente_id' 			=> 'Cliente',
			'Contato_id' 			=> 'Contato',
			'Tipo_Referencia_id' 	=> 'Tipo Referencia',
			'data_cadastro' 		=> 'Data Cadastro',
			'data_cadastro_br' 		=> 'Data / Hora Cadastro',
			'habilitado' 			=> 'Habilitado',
		);
	}

	public function search(){

		$criteria = new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('parentesco',$this->parentesco,true);
		$criteria->compare('nome',$this->nome,true);
		$criteria->compare('Cliente_id',$this->Cliente_id);
		$criteria->compare('Contato_id',$this->Contato_id);
		$criteria->compare('Tipo_Referencia_id',$this->Tipo_Referencia_id);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('data_cadastro_br',$this->data_cadastro_br,true);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__){

		return parent::model($className);
	}
        
        public function clearContato(){
            
            $contato = Contato::model()->findByPk($this->Contato_id);
            $contato->delete();
        }
        
}
