<?php

class Migracao extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Migracao';
	}

	public function getBancosCamaleao()
	{
		return array(
          	"firebird:dbname=187.12.141.11/3050:C:\automacao\dados\MARE MANSA\ESCRITORIO\camaleao.gdb", 	//OK
          	"firebird:dbname=187.12.141.11/3050:C:\automacao\dados\MARE MANSA\LOJA 02\camaleao.gdb",	 	//OK
          	"firebird:dbname=187.12.141.11/3050:C:\automacao\dados\MARE MANSA\LOJA 03\camaleao.gdb",	 	//OK
          	"firebird:dbname=187.12.141.11/3050:C:\automacao\dados\MARE MANSA\LOJA 09\camaleao.gdb",   		//OK
          	"firebird:dbname=187.12.141.11/3050:C:\automacao\dados\MARE MANSA\LOJA 12\camaleao.gdb",		//OK
          	"firebird:dbname=187.12.141.11/3050:C:\automacao\dados\MARE MANSA\LOJA 13\camaleao.gdb",		//OK
          	"firebird:dbname=187.12.141.11/3050:C:\automacao\dados\MARE MANSA\LOJA 20\camaleao.gdb",		//OK
          	"firebird:dbname=187.12.141.11/3050:C:\automacao\dados\MARE MANSA\LOJA 22\camaleao.gdb",		//OK
          	"firebird:dbname=187.12.141.11/3050:C:\automacao\dados\MARE MANSA\LOJA 25\camaleao.gdb",		//OK
          	"firebird:dbname=187.12.141.11/3050:C:\automacao\dados\MARE MANSA\LOJA 05\camaleao.gdb",		//OK
          	"firebird:dbname=187.12.141.11/3050:C:\automacao\dados\MARE MANSA\LOJA 29\camaleao.gdb",		//OK
          	"firebird:dbname=187.12.141.11/3050:C:\automacao\dados\MARE MANSA\LOJA 30\camaleao.gdb",		//OK
          	"firebird:dbname=187.12.141.11/3050:C:\automacao\dados\MARE MANSA\LOJA 31\camaleao.gdb",		//OK
          	"firebird:dbname=187.12.141.11/3050:C:\automacao\dados\MARE MANSA\LOJA 50\camaleao.gdb",		//OK
        	"firebird:dbname=187.12.141.11/3050:C:\automacao\dados\MARE MANSA\LOJA 32\camaleao.gdb"			//OK
        );
	}

	/**
		* @return array validation rules for model attributes.
	*/
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('data, usuarioId, data_cadastro, habilitado, destino, origem', 'required'),
			array('usuarioId, habilitado, destino, origem', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, data, usuarioId, data_cadastro, habilitado, destino, origem', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'itensBuscadoses' => array(self::HAS_MANY, 'ItensBuscados', 'Migracao_id'),
			'itensMigradoses' => array(self::HAS_MANY, 'ItensMigrados', 'Migracao_id'),
			'destino0' => array(self::BELONGS_TO, 'Filial', 'destino'),
			'origem0' => array(self::BELONGS_TO, 'FilialOrigemHasSistemaOrigem', 'origem'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' 			=> 'ID',
			'data' 			=> 'Data',
			'usuarioId' 	=> 'Usuario',
			'data_cadastro' => 'Data Cadastro',
			'habilitado' 	=> 'Habilitado',
			'destino' 		=> 'Destino',
			'origem' 		=> 'Origem',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('data',$this->data,true);
		$criteria->compare('usuarioId',$this->usuarioId);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('destino',$this->destino);
		$criteria->compare('origem',$this->origem);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Migracao the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}