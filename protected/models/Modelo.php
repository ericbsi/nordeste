<?php


class Modelo extends CActiveRecord
{	

	public function add($Modelo){

		$arrReturn = array(
			'hasErrors' => '1',
			'classNtfy' => '',
			'msg' 		=> '',
		);

		if( !empty($Modelo['descricao']) )
		{
			$novoModelo				= new Modelo;		
			$novoModelo->attributes = $Modelo;
			$novoModelo->habilitado = 1;
			
			if( $novoModelo->save() )
			{
				$arrReturn['classNtfy'] = 'success';
				$arrReturn['msg'] 		= 'Modelo cadastrado com sucesso';
				$arrReturn['hasErrors']	= '0';
			}
			else
			{	
				$novoModelo->save();
				$arrReturn['classNtfy'] = 'error';
				$arrReturn['msg'] 		= 'Erro ao cadastrar.';
				$arrReturn['err'] 		= json_encode($novoModelo->getErrors());
			}
		}
		return $arrReturn;
	}

	public function searchModeloSelec2($q){

		$retorno = null;

		if( !empty( $q ) ){

			$crt 	= new CDbCriteria;
			
			$crt->with          = array('marca' => array(
		    	'alias'         => 'm'
		    ));

			$crt->addSearchCondition('t.descricao', $q, TRUE, 'OR');
			$crt->addSearchCondition('m.descricao', $q, TRUE, 'OR');

			$modelos 	= Modelo::model()->findAll( $crt );

			if( count( $modelos ) > 0 )
			{
				foreach( $modelos as $modelo )
				{
					$row = array(
						'id'	=> $modelo->id,
						'text'	=> $modelo->marca->descricao . ' - ' . $modelo->descricao
					);
					$retorno[] 	= $row;
				}
			}
		}

		return $retorno;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Modelo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('descricao, habilitado, Marca_id', 'required'),
			array('habilitado, Marca_id', 'numerical', 'integerOnly'=>true),
			array('descricao', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, descricao, habilitado, Marca_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{	
		return array(
			'marca' => array(self::BELONGS_TO, 'Marca', 'Marca_id'),
			'produtos' => array(self::HAS_MANY, 'Produto', 'Modelo_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'descricao' => 'Descricao',
			'habilitado' => 'Habilitado',
			'Marca_id' => 'Marca',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('descricao',$this->descricao,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('Marca_id',$this->Marca_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Modelo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
