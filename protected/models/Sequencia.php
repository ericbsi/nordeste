<?php

/**
 * This is the model class for table "Sequencia".
 *
 * The followings are the available columns in table 'Sequencia':
 * @property integer $id
 * @property integer $habilitado
 * @property string $data_cadastro
 * @property integer $Chamado_id
 * @property integer $Usuario_id
 * @property string $inicio
 * @property string $fim
 * @property string $observacao
 *
 * The followings are the available model relations:
 * @property Chamado $chamado
 * @property Usuario $usuario
 */
class Sequencia extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Sequencia';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('data_cadastro, Chamado_id, Usuario_id, inicio, fim, observacao', 'required'),
			array('habilitado, Chamado_id, Usuario_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, habilitado, data_cadastro, Chamado_id, Usuario_id, inicio, fim, observacao', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'chamado' => array(self::BELONGS_TO, 'Chamado', 'Chamado_id'),
			'usuario' => array(self::BELONGS_TO, 'Usuario', 'Usuario_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'Chamado_id' => 'Chamado',
			'Usuario_id' => 'Usuario',
			'inicio' => 'Inicio',
			'fim' => 'Fim',
			'observacao' => 'Observacao',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('Chamado_id',$this->Chamado_id);
		$criteria->compare('Usuario_id',$this->Usuario_id);
		$criteria->compare('inicio',$this->inicio,true);
		$criteria->compare('fim',$this->fim,true);
		$criteria->compare('observacao',$this->observacao,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Sequencia the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
