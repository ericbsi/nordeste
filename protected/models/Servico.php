<?php

/**
 * This is the model class for table "Servico".
 *
 * The followings are the available columns in table 'Servico':
 * @property integer $id
 * @property string $descricao
 * @property integer $habilitado
 * @property string $data_cadastro
 */
class Servico extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'Servico';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                    array('descricao, habilitado, data_cadastro', 'required'),
                    array('habilitado', 'numerical', 'integerOnly'=>true),
                    array('descricao', 'length', 'max'=>200),
                    // The following rule is used by search().
                    // @todo Please remove those attributes that should not be searched.
                    array('id, descricao, habilitado, data_cadastro', 'safe', 'on'=>'search'),
            );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
            );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                    'id' => 'ID',
                    'descricao' => 'Descricao',
                    'habilitado' => 'Habilitado',
                    'data_cadastro' => 'Data Cadastro',
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
            // @todo Please modify the following code to remove attributes that should not be searched.

            $criteria=new CDbCriteria;

            $criteria->compare('id',$this->id);
            $criteria->compare('descricao',$this->descricao,true);
            $criteria->compare('habilitado',$this->habilitado);
            $criteria->compare('data_cadastro',$this->data_cadastro,true);

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Servico the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    public function marcarTodasFiliaisAdmin($idServico,$classeCheck)
    {
        
        $arrayReturn =  array   (
                                    'hasErrors'         => 0            ,
                                    'title'             => 'Sucesso'    ,
                                    'msg'               => ''           ,
                                    'pntfyClass'        => 'success'    ,
                                    'qtdAdmin'          => 0                        
                                );
        
        $strPosPartial = strpos($classeCheck,"clip-checkbox-partial"    );
        $strPosChecked = strpos($classeCheck,"clip-checkbox-checked-2"  );
        
        if($strPosPartial !== false || $strPosChecked !== false)
        {
            $arrayReturn['msg'] = 'Todas as filiais foram desmarcadas';
        }
        else
        {
            $arrayReturn['msg'] = 'Todas as filiais foram marcadas';
        }
        
        $filiais = Filial::model()->findAll('habilitado');
        
        $transaction = Yii::app()->db->beginTransaction();
        
        foreach ($filiais as $filial)
        {
            
            $filialHasServico = FilialHasServico::model()->find("habilitado AND Filial_id = $filial->id AND Servico_id = $idServico");
            
            if($filialHasServico == null)
            {
                
                $filialHasServico                   = new FilialHasServico  ;
                $filialHasServico->habilitado       = 1                     ;
                $filialHasServico->data_cadastro    = date('Y-m-d H:i:s')   ;
                $filialHasServico->Filial_id        = $filial->id           ;
                $filialHasServico->Servico_id       = $idServico            ;
                
                if(!$filialHasServico->save())
                {
                    
                    ob_start();
                    var_dump($filialHasServico->getErrors());
                    $result = ob_get_clean();
        
                    $arrayReturn =  array   (
                                                'hasErrors'         =>  1                                   ,
                                                'title'             =>  'Erro'                              ,
                                                'msg'               =>  'Erro ao salvar parceiroHasAdmin' .
                                                                        $result                             ,
                                                'pntfyClass'        =>  'error'                        
                                            );
        
                    $transaction->rollBack();
                    
                    return $arrayReturn;
                }
                
            }
                
            if($strPosPartial !== false || $strPosChecked !== false)
            {
                $filialHasServico->habilitado = 0   ;
            }
            else
            {
                $arrayReturn['qtdAdmin']++          ;
                $filialHasServico->habilitado  = 1  ;
            }
            
            if(!$filialHasServico->update())
            {
                    
                ob_start();
                var_dump($filialHasServico->getErrors());
                $result = ob_get_clean();

                $arrayReturn =  array   (
                                            'hasErrors'         =>  1                                   ,
                                            'title'             =>  'Erro'                              ,
                                            'msg'               =>  'Erro ao salvar parceiroHasAdmin' .
                                                                    $result                             ,
                                            'pntfyClass'        =>  'error'                        
                                        );

                $transaction->rollBack();

                return $arrayReturn;
                
            }
            
        }
        
        $transaction->commit();
        
        return $arrayReturn;
        
    }
    
    public function marcarFilialAdmin($idServico,$idFilial,$classeCheck)
    {
        
        $arrayReturn =  array   (
                                    'hasErrors'     => 0            ,
                                    'title'         => 'Sucesso'    ,
                                    'msg'           => ''           ,
                                    'pntfyClass'    => 'success'    ,
                                    'checked'       => ''           ,
                                    'qtdAdmin'      => 0            ,
                                    'qtdGeral'      => 0
                                );
        
        $strPos = strpos($classeCheck,"clip-checkbox-checked-2");
        
        $arrayReturn['checked'] = $strPos;
        
        if($strPos)
        {
            $arrayReturn['msg'] = 'Filial desmarcada com sucesso';
        }
        else
        {
            $arrayReturn['msg'] = 'Filial marcada com sucesso';
        }
        
        $transaction = Yii::app()->db->beginTransaction();
            
        $filialHasServico = FilialHasServico::model()->find("habilitado AND Filial_id = $idFilial AND Servico_id = $idServico");

        if($filialHasServico == null)
        {

            $filialHasServico                   = new FilialHasServico  ;
            $filialHasServico->habilitado       = 1                     ;
            $filialHasServico->data_cadastro    = date('Y-m-d H:i:s')   ;
            $filialHasServico->Filial_id        = $idFilial             ;
            $filialHasServico->Servico_id       = $idServico            ;

            if(!$filialHasServico->save())
            {

                ob_start();
                var_dump($filialHasServico->getErrors());
                $result = ob_get_clean();

                $arrayReturn =  array   (
                                            'hasErrors'         =>  1                                   ,
                                            'title'             =>  'Erro'                              ,
                                            'msg'               =>  'Erro ao salvar parceiroHasAdmin' .
                                                                    $result                             ,
                                            'pntfyClass'        =>  'error'                        
                                        );

                $transaction->rollBack();

                return $arrayReturn;
            }

        }

        if($strPos)
        {
            $filialHasServico->habilitado = 0;
        }
        else
        {
            $filialHasServico->habilitado = 1;
        }

        if(!$filialHasServico->update())
        {

            ob_start();
            var_dump($filialHasServico->getErrors());
            $result = ob_get_clean();

            $arrayReturn =  array   (
                                        'hasErrors'         =>  1                                   ,
                                        'title'             =>  'Erro'                              ,
                                        'msg'               =>  'Erro ao salvar parceiroHasAdmin' .
                                                                $result                             ,
                                        'pntfyClass'        =>  'error'                        
                                    );

            $transaction->rollBack();

            return $arrayReturn;

        }
        
        $arrayReturn['qtdAdmin'  ] = count(FilialHasServico::model()->findAll("habilitado AND Servico_id = $idServico"));
        $arrayReturn['qtdGeral'  ] = count(Filial::model()->findAll("habilitado"));
        
        /*$arrayReturn['qtdAdmin' ]   =   $qtdAdminFilial['qtdSim']                               ;
        $arrayReturn['qtdGeral' ]   =   $qtdAdminFilial['qtdSim'] + $qtdAdminFilial['qtdNao']   ;*/
        
        $transaction->commit();
        
        return $arrayReturn;
        
    }           

    public function listFiliaisAdmin($idServico, $draw, $start, $search) 
    {
        
        $rows = [];
        $erro = "";
        
        $sql    =   "SELECT UPPER(CONCAT(F.nome_fantasia, ' / ', E.cidade, '-', E.uf)) AS nome, F.id
                     FROM       Filial              AS F
                     INNER JOIN Filial_has_Endereco AS FhE  ON FhE.habilitado AND FhE.Filial_id     = F.id
                     INNER JOIN Endereco            AS E    ON   E.habilitado AND FhE.Endereco_id   = E.id
                     WHERE F.habilitado
                    ";
        
        try
        {
            $filiais = Yii::app()->db->createCommand($sql)->queryAll(); //Filial::model()->findAll('habilitado');

            $recordsTotal       = count($filiais);

            $sql .= " AND UPPER(CONCAT(F.nome_fantasia, ' / ', E.cidade, '-', E.uf)) LIKE '%" . strtoupper($search['value']) . "%'";

            $filiais = Yii::app()->db->createCommand($sql)->queryAll();

            //$recordsFiltered    = count($filiais);
            $recordsFiltered    = count($filiais);

            $sql .= " LIMIT  $start, 10 ";

            $filiais = Yii::app()->db->createCommand($sql)->queryAll();

            foreach ($filiais as $filial) 
            {

                $filAdmin = FilialHasServico::model()->find("habilitado AND Filial_id = " . $filial['id'] . " AND Servico_id = $idServico");

                if($filAdmin == null)
                {
                    $checkBox = '<i class="iCheckFilial clip-checkbox-unchecked-2"></i>';
                    $financeira = 'Selecione:';
                    $valor = '';
                }
                else
                {
                    $checkBox = '<i class="iCheckFilial clip-checkbox-checked-2"></i>';
                    $financeiraServ = FinanceiraHasFilialHasServico::model()->find('habilitado AND Filial_has_Servico_id = '. $filAdmin->id);
                    if($financeiraServ !== null){
                        $financeira = 'Utilizando: ' . Financeira::model()->find('habilitado AND id = '. $financeiraServ->Financeira_id)->nome;
                        $valor = Financeira::model()->find('habilitado AND id = '. $financeiraServ->Financeira_id)->id;
                    }else{
                        $financeira = 'Selecione:';
                        $valor = '';
                    }
                }

                $selectFin = CHtml::dropDownList('Financeira','Financeira', CHtml::listData( Financeira::model()->findAll('habilitado AND id NOT IN (8,9)'),
                                        'id','nome' ), array( 'class'=>'form-control search-select select2', 'prompt'=>$financeira, 'id'=>'selectFin'));

                $row    =   array   (
                                        'checkFilial'   => $checkBox            ,
                                        'filial'        => $filial['nome'   ]   ,
                                        'filialID'      => $filial['id'     ]   ,
                                        'selectFin'     => $selectFin
                                    );

                $rows[] = $row;

            }

        }
        catch (Exception $ex)
        {
            
            $erro = $e->getMessage();
            
        }
            
        return (
                    array   (
                                "draw"              => $draw            ,
                                "recordsTotal"      => $recordsTotal    ,
                                "recordsFiltered"   => $recordsFiltered ,
                                "data"              => $rows            ,
                                "erro"              => $erro            ,
                                "sql"               => $sql
                            )
                );
    }
}
