<?php

class ClientesCamaleao extends CActiveRecord
{

	public function efetivar()
	{
		set_time_limit(900);

		header('Content-Type: text/html; charset=iso-8859-1');

		$arrStatus							= array( 'hasErrors' => false, 'errors' => array() );
		$Util 								= new Util;
		$Criteria 							= new CDbCriteria;
		//$Criteria->addBetweenCondition('INCLUSAO', '2005-01-01 00:00:00', '2005-12-31 23:59:59', 'AND');
		$Criteria->addInCondition('EFETIVADO', array(0), 'AND');
		$Criteria->order 					= 'INCLUSAO ASC';
		$Criteria->limit 					= 1000000;

		$ClientesCamaleao  					= ClientesCamaleao::model()->findAll($Criteria);

		$data_cadastro						= NULL;

		foreach ( $ClientesCamaleao as $CC )
		{
			if( !Documento::model()->cadastrado( $CC->CPF ) )
			{
				$CPF 							= new Documento;
				$RG 							= new Documento;
				$Pessoa 						= new Pessoa;
				$Endereco 						= new Endereco;
				$Contato 						= new Contato;
				$Contato->habilitado 			= 1;
				
				if( $Contato->save() )
				{
					$Pessoa->Contato_id  		= $Contato->id;
					$Pessoa->Estado_Civil_id	= 1;
					$Pessoa->nome	 			= $CC->RAZAO_SOCIAL;
					$Pessoa->razao_social		= $CC->RAZAO_SOCIAL;
					$Pessoa->habilitado			= 1;

					if( !empty( $CC->NASCIMENTO )  && $CC->NASCIMENTO != NULL && $CC->NASCIMENTO != '' )
					{
						$Pessoa->nascimento 	= substr($CC->NASCIMENTO, 0, 10);
					}

					if( !empty( $CC->INCLUSAO )  && $CC->INCLUSAO != NULL && $CC->INCLUSAO != '' )
					{
						$Pessoa->data_cadastro 				= $CC->INCLUSAO;
						$data_cadastro 						= $CC->INCLUSAO;
					}

					if( $Pessoa->save() )
					{
						$Cliente 							= new Cliente;
						$Cliente->Pessoa_id					= $Pessoa->id;
						$Cliente->habilitado				= 1;

						if( $Cliente->save() )
						{
							$Cadastro 							= new Cadastro;
							$Cadastro->Cliente_id				= $Cliente->id;
							$Cadastro->data_cadastro			= $data_cadastro;
							$Cadastro->titular_do_cpf			= 1;
							$Cadastro->habilitado				= 1;
							$Cadastro->conjugue_compoe_renda	= 0;
							$Cadastro->Empresa_id				= 5;
							$Cadastro->numero_de_dependentes	= 0;
							$Cadastro->atualizar				= 1;

							if( !empty( $CC->MAE ) && $CC->MAE != NULL && $CC->MAE != '')
							{
								$Cadastro->nome_da_mae          = $CC->MAE;
							}

							if( !empty( $CC->PAI ) && $CC->PAI != NULL && $CC->PAI != '')
							{
								$Cadastro->nome_do_pai          = $CC->PAI;
							}

							if( !$Cadastro->save() )
							{
								$arrStatus['hasErrors'] = true;
								$arrStatus['errors'][] 	= $Cadastro->getErrors();
							}

							//INICIO CPF
							$CPF->numero 				= $CC->CPF;
							$CPF->Tipo_documento_id		= 1;
							$CPF->data_cadastro			= $data_cadastro;

							if( $CPF->save() )
							{
								$pessoaHasCPF 					= new PessoaHasDocumento;
								$pessoaHasCPF->Documento_id 	= $CPF->id;
								$pessoaHasCPF->Pessoa_id 		= $Pessoa->id;
								$pessoaHasCPF->data_cadastro	= $data_cadastro;

								if( !$pessoaHasCPF->save() )
								{
									$arrStatus['hasErrors'] 	= true;
									$arrStatus['errors'][] 		= 'pessoaHasCPF';
								}
							}
							
							if( !empty( $CC->RG )  && $CC->RG != NULL && $CC->RG != '' )
							{
								$RG->numero					= $Util->cleanStr($CC->RG,'upp');
								$RG->Tipo_documento_id		= 2;
								$RG->data_cadastro			= $data_cadastro;

								if( !empty( $CC->EXPEDICAO )  && $CC->EXPEDICAO != NULL && $CC->EXPEDICAO != '' )
								{
									$RG->data_emissao 		= substr($CC->EXPEDICAO, 0, 10);
								}

								if( !empty( $CC->ORGAO )  && $CC->ORGAO != NULL && $CC->ORGAO != '' )
								{
									$RG->orgao_emissor 		= $CC->ORGAO;
								}
								
								if( $RG->save() )
								{
									$pessoaHasRG 					= new PessoaHasDocumento;
									$pessoaHasRG->Documento_id 		= $RG->id;
									$pessoaHasRG->Pessoa_id 		= $Pessoa->id;
									$pessoaHasRG->data_cadastro		= $data_cadastro;

									if( !$pessoaHasRG->save() )
									{
										$arrStatus['hasErrors'] 	= true;
										$arrStatus['errors'][] 		= 'pessoaHasRG';
									}
								}
								else
								{
									$arrStatus['hasErrors'] 		= true;
									$arrStatus['errors'][] 			= $RG->getErrors();
								}
							}
							
							if( !empty( $CC->RUA )  && $CC->RUA != NULL && $CC->RUA != '' )
							{
								$Endereco->logradouro 		= $CC->RUA;

								if( !empty( $CC->CEP )  && $CC->CEP != NULL && $CC->CEP != '' )
								{
									$Endereco->cep 			= $CC->CEP;
								}

								if( !empty( $CC->BAIRRO )  && $CC->BAIRRO != NULL && $CC->BAIRRO != '' )
								{
									$Endereco->bairro 		= $CC->BAIRRO;
								}

								if( !empty( $CC->UF )  && $CC->UF != NULL && $CC->UF != '' )
								{
									$Endereco->uf 			= $CC->UF;
								}

								if( !empty( $CC->CIDADE )  && $CC->CIDADE != NULL && $CC->CIDADE != '' )
								{
									$Endereco->cidade 		= $CC->CIDADE;
								}

								$Endereco->data_cadastro 		= $data_cadastro;
								$Endereco->Tipo_Endereco_id		= 2;
								$Endereco->endereco_de_cobranca	= 1;

								if( $Endereco->save() )
								{
									$pHasEndereco  				= new PessoaHasEndereco;
									$pHasEndereco->Pessoa_id 	= $Pessoa->id;
									$pHasEndereco->Endereco_id 	= $Endereco->id;
									$pHasEndereco->data_cadastro= $data_cadastro;
									$pHasEndereco->habilitado 	= 1;
									
									if(!$pHasEndereco->save())
									{
										$arrStatus['hasErrors'] = true;
										$arrStatus['errors'][] 	= 'pHasEndereco';
									}
								}
								else
								{
									$arrStatus['hasErrors'] 	= true;
									$arrStatus['errors'][]		= $Endereco->getErrors();
								}

							}
						}
					
						$CC->delete();
					}
				}
			}
			else
			{
				$CC->delete();	
			}
		}

		if( $arrStatus['hasErrors'] )
		{
			var_dump( $arrStatus['errors'] );
		}
		else
		{
			echo "<h1>Importacao realizada com sucesso</h1>";
		}
	}

	public function importar()
	{
		set_time_limit(90000);

		header('Content-Type: text/html; charset=iso-8859-1');

		$util 				= new Util;
		$bancosCamaleao 	= Migracao::model()->getBancosCamaleao();

		//$str_conn   		= "firebird:dbname=10.0.254.4/3050:C:\automacao\dados\MARE MANSA\LOJA 20\camaleao.gdb";
		//$str_conn   		= "firebird:dbname=187.12.141.11/3050:C:\automacao\dados\MARE MANSA\LOJA 32\camaleao.gdb";
		$str_conn   		= "";
		
		$countSalvos        = 0;

		for ( $i = 0; $i < count($bancosCamaleao); $i++)
		{
			$str_conn 		= $bancosCamaleao[$i];
			
			try{

	        	$dbh            = new PDO($str_conn, "sysdba", "masterkey");
	                
	            $sql  = "SELECT CODIGO, CPF, RG, ORGAO, EXPEDICAO, RAZAO_SOCIAL, RUA, BAIRRO, CIDADE, UF, CEP, NASCIMENTO, FONE, CELULAR, INCLUSAO, MAE, PAI FROM CLIENTES ";
	            $sql .= " WHERE CPF IS NOT NULL AND CPF <>'' AND RAZAO_SOCIAL IS NOT NULL AND RAZAO_SOCIAL <>'' AND INCLUSAO BETWEEN '2014-12-01' AND '2015-01-06' ";
	                
	            foreach ($dbh->query($sql) as $row)
	            {
	            	if( strlen( $util->limparCPFPJ( $row['CPF'] ) ) == 11 )
	            	{	
	            		if( $util->validarCPF( $util->limparCPFPJ( $row['CPF'] ) ) )
	            		{
	            			$this->novo( $row );
	            			//echo $row['CPF'] . '<br>';
	            		}
	            	}
	            }
	        }

	        catch(PDOException $e)
	        {
	        	echo $e->getMessage();
	        }
		}
	}

	public function novo( $ClienteCamaleao )
	{

		$util 				= new Util;
		$CliCam 			= new ClientesCamaleao;
		
		if( isset( $ClienteCamaleao['CPF'] ) )
		{
			$CliCam->CPF = $util->limparCPFPJ( $ClienteCamaleao['CPF'] );
		}

		if( isset( $ClienteCamaleao['CODIGO'] ) )
		{
			$CliCam->CODIGO = $ClienteCamaleao['CODIGO'];
		}
		
		if( isset( $ClienteCamaleao['RG'] ) )
		{
			$CliCam->RG = $ClienteCamaleao['RG'];
		}
	
		if( isset( $ClienteCamaleao['RAZAO_SOCIAL'] ) )
		{
			$CliCam->RAZAO_SOCIAL = $ClienteCamaleao['RAZAO_SOCIAL'];
		}

		if( isset( $ClienteCamaleao['ORGAO'] ) )
		{
			$CliCam->ORGAO = $ClienteCamaleao['ORGAO'];
		}

		if( isset( $ClienteCamaleao['EXPEDICAO'] ) )
		{
			$CliCam->EXPEDICAO = $ClienteCamaleao['EXPEDICAO'];
		}
		
		if( isset( $ClienteCamaleao['RUA'] ) )
		{
			$CliCam->RUA = $ClienteCamaleao['RUA'];
		}

		if( isset( $ClienteCamaleao['BAIRRO'] ) )
		{
			$CliCam->BAIRRO = $ClienteCamaleao['BAIRRO'];
		}

		if( isset( $ClienteCamaleao['CIDADE'] ) )
		{
			$CliCam->CIDADE = $ClienteCamaleao['CIDADE'];
		}

		if( isset( $ClienteCamaleao['UF'] ) )
		{
			$CliCam->UF = $ClienteCamaleao['UF'];
		}

		if( isset( $ClienteCamaleao['CEP'] ) )
		{
			$CliCam->CEP = $ClienteCamaleao['CEP'];
		}

		if( isset( $ClienteCamaleao['NASCIMENTO'] ) )
		{
			$CliCam->NASCIMENTO = $ClienteCamaleao['NASCIMENTO'];
		}

		if( isset( $ClienteCamaleao['FONE'] ) )
		{
			$CliCam->FONE = $ClienteCamaleao['FONE'];
		}
		
		if( isset( $ClienteCamaleao['CELULAR'] ) )
		{
			$CliCam->CELULAR = $ClienteCamaleao['CELULAR'];
		}

		if( isset( $ClienteCamaleao['INCLUSAO'] ) )
		{
			$CliCam->INCLUSAO = $ClienteCamaleao['INCLUSAO'];
		}

		if( isset( $ClienteCamaleao['PAI'] ) )
		{
			$CliCam->PAI = $ClienteCamaleao['PAI'];
		}

		if( isset( $ClienteCamaleao['MAE'] ) )
		{
			$CliCam->MAE = $ClienteCamaleao['MAE'];
		}

		$CliCam->DATA_IMPORTACAO = date('Y-m-d H:i:s');
		$CliCam->EFETIVADO = 0;
		$CliCam->save();
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ClientesCamaleao';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('EFETIVADO', 'numerical', 'integerOnly'=>true),
			array('CPF, RG, CODIGO, ORGAO, EXPEDICAO, NOME_FANTASIA, RAZAO_SOCIAL, RUA, BAIRRO, CIDADE, UF, CEP, NASCIMENTO, FONE, CELULAR, INCLUSAO, MAE, PAI, DATA_IMPORTACAO, DATA_EFETIVACAO, EFETIVADO', 'safe'),
			array('id, CPF, RG, ORGAO, EXPEDICAO, NOME_FANTASIA, RAZAO_SOCIAL, RUA, BAIRRO, CIDADE, UF, CEP, NASCIMENTO, FONE, CELULAR, INCLUSAO, MAE, PAI', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' 				=> 'ID',
			'CPF' 				=> 'Cpf',
			'CODIGO' 			=> 'CODIGO',
			'RG' 				=> 'Rg',
			'ORGAO' 			=> 'Orgao',
			'EXPEDICAO' 		=> 'Expedicao',
			'NOME_FANTASIA' 	=> 'Nome Fantasia',
			'RAZAO_SOCIAL' 		=> 'Razao Social',
			'RUA' 				=> 'Rua',
			'BAIRRO' 			=> 'Bairro',
			'CIDADE' 			=> 'Cidade',
			'UF' 				=> 'Uf',
			'CEP' 				=> 'Cep',
			'NASCIMENTO' 		=> 'Nascimento',
			'FONE'	 			=> 'Fone',
			'CELULAR' 			=> 'Celular',
			'INCLUSAO' 			=> 'Inclusao',
			'MAE' 				=> 'Mae',
			'PAI' 				=> 'Pai',
			'DATA_IMPORTACAO'	=> 'DATA DA IMPORTAÇÃO',
			'DATA_EFETIVACAO'	=> 'DATA DA EFETIVAÇÃO',
			'EFETIVADO'			=> 'EFETIVADO',
		);
	}

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('CPF',$this->CPF,true);
		$criteria->compare('RG',$this->RG,true);
		$criteria->compare('ORGAO',$this->ORGAO,true);
		$criteria->compare('EXPEDICAO',$this->EXPEDICAO,true);
		$criteria->compare('NOME_FANTASIA',$this->NOME_FANTASIA,true);
		$criteria->compare('RAZAO_SOCIAL',$this->RAZAO_SOCIAL,true);
		$criteria->compare('RUA',$this->RUA,true);
		$criteria->compare('BAIRRO',$this->BAIRRO,true);
		$criteria->compare('CIDADE',$this->CIDADE,true);
		$criteria->compare('UF',$this->UF,true);
		$criteria->compare('CEP',$this->CEP,true);
		$criteria->compare('NASCIMENTO',$this->NASCIMENTO,true);
		$criteria->compare('FONE',$this->FONE,true);
		$criteria->compare('CELULAR',$this->CELULAR,true);
		$criteria->compare('INCLUSAO',$this->INCLUSAO,true);
		$criteria->compare('MAE',$this->MAE,true);
		$criteria->compare('PAI',$this->PAI,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ClientesCamaleao the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
