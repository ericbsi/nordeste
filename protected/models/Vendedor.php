<?php

/**
 * This is the model class for table "Vendedor".
 *
 * The followings are the available columns in table 'Vendedor':
 * @property integer $id
 * @property integer $nome
 * @property string $cpf
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property FilialHasVendedor[] $filialHasVendedors
 */
class Vendedor extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Vendedor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nome, cpf, habilitado', 'required'),
			array('habilitado', 'numerical', 'integerOnly'=>true),
			array('cpf', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nome, cpf, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'filialHasVendedors' => array(self::HAS_MANY, 'FilialHasVendedor', 'Vendedor_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nome' => 'Nome',
			'cpf' => 'Cpf',
			'habilitado' => 'Habilitado',
		);
	}

	public function listar($id_filial){
            
            $return = [];
            
		$query = "SELECT V.* FROM Vendedor as V ";
		$query .= "INNER JOIN Filial_has_Vendedor as FHV ";
		$query .= "WHERE V.habilitado = 1 AND FHV.habilitado = 1 AND FHV.Filial_id = " . $id_filial . " AND FHV.Vendedor_id = V.id";

		$vendedores = Yii::app()->db->createCommand($query)->queryAll();

		foreach ($vendedores as $vendedor) {
			$v = array(
				'id'	=>	$vendedor['id'],
				'text'	=>	$vendedor['nome'],
			);

			$return[] = $v;
		}

		return $return;
	}

	public function cadastrado($cpf, $id_filial){
		$query = "SELECT V.* FROM Vendedor as V ";
		$query .= "INNER JOIN Filial_has_Vendedor as FHV ";
		$query .= "WHERE V.habilitado = 1 AND FHV.habilitado = 1 AND FHV.Filial_id = " . $id_filial . " AND FHV.Vendedor_id = V.id ";
		$query .= "AND V.cpf = '" . $cpf . "'";

		$retorno ['cadastrado'] = false;

		$vendedor = Yii::app()->db->createCommand($query)->queryAll();

		if(count($vendedor) > 0){
			$retorno ['cadastrado'] = true;
		}

		return $retorno;
	}

	public function adicionar($nome, $cpf, $id_filial){
		$v = new Vendedor;
		$v->nome = $nome;
		$v->cpf = $cpf;
		$v->habilitado = 1;

		$retorno ['hasError'] = false;
		$retorno ['msg'] = "Vendedor cadastrado com sucesso";
		
		if($v->save()){
			$fhv = new FilialHasVendedor;
			$fhv->Filial_id = $id_filial;
			$fhv->Vendedor_id = $v->id;
			$fhv->data_cadastro = date('Y-m-d H:i:s');
			$fhv->habilitado = 1;

			if ($fhv->save()) {
				$retorno ['hasError'] = false;
				$retorno ['msg'] = "Vendedor cadastrado com sucesso";
			}else{

				ob_start();
				var_dump($fhv->getErrors());
				$resultado = ob_get_clean();
				$retorno ['hasError'] = true;
				$retorno ['msg'] = "Erro ao salvar FHV. $resultado";
			}
		}else{

			ob_start();
			var_dump($v->getErrors());
			$resultado = ob_get_clean();

			$retorno ['hasError'] = true;
			$retorno ['msg'] = "Erro ao salvar vendedor. $resultado";
		}

		return $retorno;
	}

	public function listVendedores($id_filial){
		$query = "SELECT V.* FROM Vendedor as V ";
		$query .= "INNER JOIN Filial_has_Vendedor as FHV ";
		$query .= "WHERE FHV.Filial_id = " . $id_filial . " AND FHV.Vendedor_id = V.id";

		return Yii::app()->db->createCommand($query)->queryAll();
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nome',$this->nome, true);
		$criteria->compare('cpf',$this->cpf,true);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Vendedor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
