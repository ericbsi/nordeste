<?php

class PropostaOmniConfig extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'PropostaOmniConfig';
    }

    public function rules() {
        return array(
                array('Proposta', 'required'),
                array('Proposta, interno, habilitado', 'numerical', 'integerOnly' => true),
                array('urlContrato, urlBoleto, codigoOmni, codigoSigac, interno, numPropLoj, urlCG, usuarioHost, tabelaHost, data_cadastro', 'safe'),
                array('id, urlContrato, urlBoleto, codigoOmni, codigoSigac, habilitado, Proposta, interno, numPropLoj, urlCG, usuarioHost, tabelaHost, data_cadastro', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
        );
    }

    /**
     * Consulta o webservice da omni para pegar os links de boleto e contrato.
     *
     * Uso típico:
     * - Função que a princípio é executada em background, sem o cliente "saber"
     *
     *
     * @return sem retorno.
     */
    public function getUsuarioHost() {

        if (isset($this->usuarioHost)) {
            return $this->usuarioHost;
        } else {
            return "01630LHSTCREDSHOW";
        }
    }

    public function atualizarLinks() {

        $proposta = Proposta::model()->findByPk($this->Proposta);
        $configContrato = NULL;
        $configCarne = NULL;

        if ($proposta != NULL && $proposta->Financeira_id != 7) {

            $util = new Util;

            //monte a string que ira gerar o objeto xml para obter a url de impressao de boletos
            $xmlStringContrato = '<Envelope>
                      <Body>
                         <CM_PROPOSTA>
                            <proposta xmlns:tem="https://hst1.omni.com.br/wsProposta/servidor_soap_proposta.wsdl?service=ICM_Credito">
                               <identificacao>
                                   <chave>' . $this->getUsuarioHost() . '</chave>
                                   <senha>' . $proposta->getSenhaXML() . '</senha>
                               </identificacao>
                               <dados>
                                  <operacao>imprimir cedula</operacao>
                                  <idProposta>' . $proposta->codigo . '</idProposta>
                               </dados>
                            </proposta>
                         </CM_PROPOSTA>
                      </Body>
                   </Envelope>';

            $xmlStringCarne = '<Envelope>
                      <Body>
                         <CM_PROPOSTA>
                            <proposta xmlns:tem="https://hst1.omni.com.br/wsProposta/servidor_soap_proposta.wsdl?service=ICM_Credito">
                               <identificacao>
                                   <chave>' . $this->getUsuarioHost() . '</chave>
                                   <senha>' . $proposta->getSenhaXML() . '</senha>
                               </identificacao>
                               <dados>
                                  <operacao>imprimir carne</operacao>
                                  <idProposta>' . $proposta->codigo . '</idProposta>
                               </dados>
                            </proposta>
                         </CM_PROPOSTA>
                      </Body>
                   </Envelope>';

            $configContrato = $util->callSoapOmni($xmlStringContrato, $proposta->getLinkXML());

            if ($configContrato != null && isset($configContrato->impressao->impressao->url)) { //caso o resultado nao seja nulo
                $this->urlContrato = (string) $configContrato->impressao->impressao->url;
            } else { //caso o resultado seja nulo, informe que houve erro na obtencao da url
                $this->urlContrato = null;

                ob_start();
                var_dump($configContrato);
                $respConfigContrato = ob_get_clean();
                
                if (!is_dir("errosOmni")) {
                    mkdir("errosOmni", 0700);
                }

                $file = fopen("errosOmni/erroContrato.xml", 'w+');
                fwrite($file, $xmlStringContrato);
                fclose($file);
                
                /*                $message            = new YiiMailMessage    ;
                  $message->view      = "mandaMailFichamento" ;
                  $message->subject   = 'Links Contrato'      ;

                  $parametros         =   [
                  'email' =>  [
                  'link'      => ""                                                                   ,
                  'titulo'    => "Links Contrato .::. Erro 2"                                         ,
                  'mensagem'  => "Erro ao obter url dos boletos. configCarne: $respConfigContrato"    ,
                  'tipo'      => "1"
                  ]
                  ];

                  $message->setBody($parametros, 'text/html');

                  $message->addTo('andre@credshow.com.br');
                  $message->from = 'no-reply@credshow.com.br';

                  Yii::app()->mail->send($message); */
            }

            $configCarne = $util->callSoapOmni($xmlStringCarne, $proposta->getLinkXML());

            if ($configCarne != null && isset($configCarne->impressao->impressao->url)) {

                $this->urlBoleto = (string) $configCarne->impressao->impressao->url;
            } else { //caso o resultado seja nulo, informe que houve erro na obtencao da url
                $this->urlBoleto = null;

                ob_start();
                var_dump($configCarne);
                $respConfigCarne = ob_get_clean();

                if (!is_dir("errosOmni")) {
                    mkdir("errosOmni", 0700);
                }

                $file = fopen("errosOmni/erroBoleto.xml", 'w+');
                fwrite($file, $xmlStringCarne);
                fclose($file);

                /*                $message            = new YiiMailMessage    ;
                  $message->view      = "mandaMailFichamento" ;
                  $message->subject   = 'Links Contrato'      ;

                  $parametros         =   [
                  'email' =>  [
                  'link'      =>  ""                                                                  ,
                  'titulo'    =>  "Links Contrato .::. Erro 2"                                        ,
                  'mensagem'  =>  "Erro ao obter url dos boletos. configCarne: $respConfigCarne <br>"
                  .   $xmlStringCarne                                                     ,
                  'tipo'      =>  "1"
                  ]
                  ];

                  $message->setBody($parametros, 'text/html');

                  $message->addTo('andre@credshow.com.br');
                  $message->from = 'no-reply@credshow.com.br';

                  Yii::app()->mail->send($message); */
            }

            $this->update();
        } else {
            if ($proposta != NULL && $proposta->Financeira_id == 7 && $proposta->Status_Proposta_id == 2) {
                $headers = array(
                        'Content-Type:text/xml;charset=UTF-8',
                        'Connection: close'
                );

                $xml = "<soapenv:Envelope
                    xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'
                    xmlns:ws='http://ws.host.banco.venda.jretail/'>
                    <soapenv:Header/>
                    <soapenv:Body>
                        <ws:imprimirProposta>
                            <xmlIn>
                                <host>
                                    <autenticacao>
                                        <codUsr>hstcredshow</codUsr>
                                        <pwdUsr>LMt46dc9ezpP1f34DQFpUA==</pwdUsr>
                                    </autenticacao>
                                    <proposta>
                                        <codLoja>51130000</codLoja>
                                        <numProp>" . $proposta->codigo . "</numProp>
                                        <numPropLoj>" . $proposta->propostaOmniConfig->numPropLoj . "</numPropLoj>
                                    </proposta>
                                </host>
                            </xmlIn>
                        </ws:imprimirProposta>
                    </soapenv:Body>
                </soapenv:Envelope>";

                $file = fopen('imprimir_semear', 'w+');
                fwrite($file, $xml);
                fclose($file);

                $ch = curl_init('https://correspondente.bancosemear.com.br:8443/jretail-jretail.ejb/PropostaHostWSSSL?wsdl');
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);

                $retorno = curl_exec($ch);

                $objXML = simplexml_load_string($retorno);
                $objXML->registerXPathNamespace('env', 'http://schemas.xmlsoap.org/soap/envelope/');
                $objXML->registerXPathNamespace('ws', 'http://ws.host.banco.venda.jretail/');

                $resultado = $objXML->xpath("//ws:imprimirPropostaResponse");

                $resp = $resultado[0]->xpath('xmlOut');

                $file2 = fopen('imprimir_result', 'w+');
                fwrite($file2, $resp[0]);
                fclose($file2);

                $cdata = simplexml_load_string($resp[0]);

                $response = $cdata->xpath('proposta/status')[0];

                if ($response != 'ERR' && $response != NULL && $response != '') {
                    if (($this->urlContrato == NULL) || (trim($this->urlContrato) == '') || ($this->urlContrato == '') || (empty($this->urlContrato))) {
                        $this->urlContrato = $cdata->xpath('proposta/documentos/documento')[0]->url;
                    }

                    if (($this->urlBoleto == NULL) || (trim($this->urlBoleto) == '') || ($this->urlBoleto == '') || (empty($this->urlBoleto))) {
                        $this->urlBoleto = $cdata->xpath('proposta/documentos/documento')[3]->url;
                    }

                    if (($this->urlCG == NULL) || (trim($this->urlCG) == '') || ($this->urlBoleto == '') || (empty($this->urlCG))) {
                        $this->urlCG = $cdata->xpath('proposta/documentos/documento')[1]->url;
                    }
                }

                $this->update();
            }
        }
    }

    public function attributeLabels() {
        return array(
                'id' => 'ID',
                'urlContrato' => 'Url Contrato',
                'urlBoleto' => 'Url Boleto',
                'codigoOmni' => 'Código Omni',
                'codigoSigac' => 'Código Sigac',
                'habilitado' => 'Habilitado',
                'Proposta' => 'Proposta',
                'interno' => 'Interno',
                'numPropLoj' => 'numPropLoj',
                'urlCG' => 'urlCG',
                'usuarioHost' => 'Usuario Host',
                'tabelaHost' => 'Tabela Host',
                'data_cadstro' => 'Data Cadastro'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('urlContrato', $this->urlContrato, true);
        $criteria->compare('urlBoleto', $this->urlBoleto, true);
        $criteria->compare('Proposta', $this->Proposta);
        $criteria->compare('codigoOmni', $this->codigoOmni, true);
        $criteria->compare('codigoSigac', $this->codigoSigac, true);
        $criteria->compare('habilitado', $this->habilitado);
        $criteria->compare('interno', $this->interno);
        $criteria->compare('numPropLoj', $this->numPropLoj, true);
        $criteria->compare('urlCG', $this->urlCG, true);
        $criteria->compare('usuarioHost', $this->usuarioHost, true);
        $criteria->compare('tabelaHost', $this->tabelaHost, true);
        $criteria->compare('data_cadastro', $this->data_cadastro);

        return new CActiveDataProvider(
            $this, array(
                'criteria' => $criteria,
            )
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PropostaOmniConfig the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
