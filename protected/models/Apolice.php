<?php

class Apolice extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Apolice';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('valor, Proposta_id', 'required'),
			array('Proposta_id, habilitado', 'numerical', 'integerOnly'=>true),
			array('valor', 'numerical'),
			array('numero, vencimento', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, numero, valor, vencimento, Proposta_id, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'proposta' => array(self::BELONGS_TO, 'Proposta', 'Proposta_id'),
		);
	}

	public function novo( $numero, $Proposta_id )
	{
		$Proposta 					= Proposta::model()->findByPk( $Proposta_id );
		$util 						= new Util;

		$apolice 					= new Apolice;
		$apolice->numero 			= $numero;
		$apolice->valor 			= 0;
		//$apolice->valor 			= ( ( $Proposta->valor - $Proposta->valor_entrada ) / 100 ) * 10;
		$apolice->vencimento		= $util->view_date_to_bd( $Proposta->getDataUltimaParcela() );
		$apolice->Proposta_id		= $Proposta->id;
		$apolice->save();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' 			=> 'ID',
			'numero' 		=> 'Numero',
			'valor' 		=> 'Valor',
			'vencimento' 	=> 'Vencimento',
			'Proposta_id' 	=> 'Proposta',
			'habilitado' 	=> 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('numero',$this->numero,true);
		$criteria->compare('valor',$this->valor);
		$criteria->compare('vencimento',$this->vencimento,true);
		$criteria->compare('Proposta_id',$this->Proposta_id);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Apolice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
