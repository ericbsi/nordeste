<?php

class Bradesco{

  private $codigobanco            = "237";
  private $nummoeda               = "9";
  private $carteira               = "02";
  private $fator_vencimento;
  private $codigo_banco_com_dv;
  private $valor;
  private $agencia;
  private $nosso_numero;
  private $conta_cedente;
  private $configuracoesBanco;
  private $conta_cedente_dv;
  private $agenciaCodigo;
  private $barra;
  private $linha;
  private $dv_nosso_numero;

  function __construct( $vencimento, $valorBoleto, $nossoNumero, $proposta = NULL, $recebimento = NULL )
  {

    $configuracaoBoleto = 'credshow_bradesco';

    if( $proposta !== NULL )
    {
      if( in_array($proposta->Financeira_id, [10,11]) ) //lembrar de ajeitar isso aqui
      {
        $configuracaoBoleto = 'credshow_bradesco_fidic';
      }
    }

    if( $recebimento != Null )
    {
      if( in_array($recebimento->Financeira_id, [10,11]) ) //lembrar de ajeitar isso aqui
      {
        //$configuracaoBoleto = 'credshow_bradesco_fidic';
      }
    }

    $this->configuracoesBanco     = Banco::model()->configuracoesBanco();
    $m11                          = new Modulo_11;
    $util                         = new Util;
    $this->carteira               = substr($this->configuracoesBanco[$configuracaoBoleto]['carteira'], 1,2);

    $vencimento                   = $util->bd_date_to_view( $vencimento );
    $this->fator_vencimento       = $this->fator_vencimentoF( $vencimento );
    $this->codigo_banco_com_dv    = $this->geraCodigoBanco( $this->codigobanco );
    $this->valor                  = $this->formata_numero($valorBoleto,10,0,"valor");
    $this->agencia                = substr($this->configuracoesBanco[$configuracaoBoleto]['agencia'],               1, 4);

    $nnum                         = $this->formata_numero($this->carteira,2,0).$this->formata_numero($nossoNumero,  11,0);

    $this->nosso_numero           = substr($nnum,0,2).'/'.substr($nnum,2).$this->digitoVerificador_nossonumero($nnum);

    $this->dv_nosso_numero        = $this->digitoVerificador_nossonumero($nnum);

    $this->conta_cedente          = $this->formata_numero($this->configuracoesBanco[$configuracaoBoleto]['conta'],  7,0);
    $this->conta_cedente_dv       = $this->formata_numero($this->configuracoesBanco[$configuracaoBoleto]['dvconta'],1,0);
    $this->agenciaCodigo          = $this->agencia."-".substr($this->agencia, 4)." / ". $this->conta_cedente ."-". $this->conta_cedente_dv;

    $this->barra                  = "$this->codigobanco$this->nummoeda$this->fator_vencimento$this->valor$this->agencia$nnum$this->conta_cedente".'0';
    $dv                           = $this->digitoVerificador_barra( $this->barra );

    $this->linha                  = "$this->codigobanco$this->nummoeda$dv$this->fator_vencimento$this->valor$this->agencia$nnum$this->conta_cedente"."0";
  }

  public function digitoVerificador_nossonumero($numero)
  {
    $m11            = new Modulo_11;

    $resto2         = $m11->m11($numero, 7, 1);
    $digito         = 11 - $resto2;

    if ($digito == 10)
    {
        $dv = "P";
    }
    elseif($digito == 11)
    {
      $dv = 0;
    }
    else
    {
      $dv = $digito;
    }

    return $dv;
  }

  public function getDigitoVerificador_nossonumero()
  {
    return $this->dv_nosso_numero;
  }

  public function getNossoNumero(){
    return $this->nosso_numero;
  }

  public function digitoVerificador_barra( $numero ){

      $m11      = new Modulo_11;

      $resto2   = $m11->m11($numero, 9, 1);

      if ($resto2 == 0 || $resto2 == 1 || $resto2 == 10)
      {
          $dv = 1;
      }

      else{
      $dv = 11 - $resto2;
      }

      return $dv;
  }

  public function formata_numero($numero, $loop, $insert, $tipo = "geral")
  {
    if ($tipo == "geral")
    {
      $numero = str_replace(",","",$numero);

      while( strlen( $numero ) < $loop )
      {
        $numero = $insert . $numero;
      }
    }
    if ($tipo == "valor")
    {
      $numero = str_replace(",","",$numero);
      while( strlen( $numero ) < $loop )
      {
        $numero = $insert . $numero;
      }
    }
    if ($tipo == "convenio")
    {
      while( strlen( $numero ) < $loop )
      {
        $numero = $numero . $insert;
      }
    }
    return $numero;
  }

  public function esquerda($entra,$comp)
  {

    return substr($entra,0,$comp);
  }

  public function direita($entra,$comp)
  {

    return substr($entra,strlen($entra)-$comp,$comp);
  }

  public function fator_vencimentoF($data)
  {
    $data   = explode("/",$data);
    $ano    = $data[2];
    $mes    = $data[1];
    $dia    = $data[0];
    return( abs( ( $this->_dateToDays("1997","10","07") ) - ( $this->_dateToDays( $ano, $mes, $dia ) ) ) );
  }

  public function _dateToDays($year,$month,$day)
  {
      $century  = substr($year, 0, 2);
      $year     = substr($year, 2, 2);

      if ($month > 2)
      {
        $month -= 3;
      }
      else
      {
        $month += 9;
        if ($year)
        {
          $year--;
        }

        else
        {
          $year   = 99;
          $century --;
        }
      }

      return ( floor((  146097 * $century)    /  4 ) + floor(( 1461 * $year)        /  4 ) + floor(( 153 * $month +  2) /  5 ) +  $day +  1721119);
  }

  public function modulo_10($num)
  {
    $numtotal10 = 0;
    $fator      = 2;

    for ($i = strlen($num); $i > 0; $i--)
    {

      $numeros[$i] = substr($num,$i-1,1);

      $temp   = $numeros[$i] * $fator;
      $temp0  = 0;

      foreach (preg_split('//',$temp,-1,PREG_SPLIT_NO_EMPTY) as $k=>$v)
      {
        $temp0  += $v;
      }

      $parcial10[$i] = $temp0;

      $numtotal10 += $parcial10[$i];
      if ($fator == 2)
      {
        $fator = 1;
      }
      else
      {
        $fator = 2;
      }
    }

    $resto      = $numtotal10 % 10;
    $digito     = 10 - $resto;

    if ($resto == 0)
    {
      $digito = 0;
    }

    return $digito;
  }

  public function modulo_11( $num, $base = 9, $r = 0 ){
    $soma     = 0;
    $fator    = 2;

    for ($i = strlen($num); $i > 0; $i--)
    {
      $numeros[$i] = substr($num,$i-1,1);

      $parcial[$i] = $numeros[$i] * $fator;

      $soma += $parcial[$i];

      if ($fator == $base)
      {
        $fator = 1;
      }
      $fator++;
    }

    if ($r == 0)
    {
      $soma   *= 10;
      $digito = $soma % 11;

      if ($digito == 10)
      {
        $digito = 0;
      }

      return $digito;
    }
    elseif ($r == 1)
    {
      $resto = $soma % 11;
      return $resto;
    }
  }

  public function monta_linha_digitavel()
  {
    $campo1       = '';
    $campo2       = '';
    $campo3       = '';
    $campo4       = '';
    $campo5       = '';

    $codigo       = $this->linha;

    $p1           = substr($codigo, 0, 4);
    $p2           = substr($codigo, 19, 5);
    $p3           = $this->modulo_10("$p1$p2");
    $p4           = "$p1$p2$p3";
    $campo1       = substr($p4, 0, 5).'.'.substr($p4, 5);

    $p1           = substr($codigo, 24, 10);
    $p2           = $this->modulo_10($p1);
    $p3           = "$p1$p2";
    $campo2       = substr($p3, 0, 5).'.'.substr($p3, 5);

    $p1           = substr($codigo, 34, 10);
    $p2           = $this->modulo_10($p1);
    $p3           = "$p1$p2";
    $campo3       = substr($p3, 0, 5).'.'.substr($p3, 5);

    $campo4       = substr($codigo, 4, 1);

  	$p1           = substr($codigo, 5, 4);
  	$p2           = substr($codigo, 9, 10);
  	$campo5       = "$p1$p2";

    return "$campo1 $campo2 $campo3 $campo4 $campo5";
  }

  public function geraCodigoBanco($numero)
  {
    $m11        = new Modulo_11;

    $parte1     = substr($numero, 0, 3);

    $parte2     = $m11->m11($parte1);

    return $parte1 . "-" . $parte2;
  }

  public function fbarcodeCredshow(){

    $fino     = 1;
    $largo    = 3;
    $altura   = 50;

    $barcodes[0] = "00000" ;
    $barcodes[1] = "00001" ;
    $barcodes[2] = "00011" ;
    $barcodes[3] = "00111" ;
    $barcodes[4] = "01111" ;
    $barcodes[5] = "11111" ;
    $barcodes[6] = "10000" ;
    $barcodes[7] = "11000" ;
    $barcodes[8] = "11100" ;
    $barcodes[9] = "11110" ;

    // $barcodes[0] = "00110" ;
    // $barcodes[1] = "10001" ;
    // $barcodes[2] = "01001" ;
    // $barcodes[3] = "11000" ;
    // $barcodes[4] = "00101" ;
    // $barcodes[5] = "10100" ;
    // $barcodes[6] = "01100" ;
    // $barcodes[7] = "00011" ;
    // $barcodes[8] = "10010" ;
    // $barcodes[9] = "01010" ;


    for ( $f1=9; $f1>=0; $f1-- )
    {
      for ( $f2=9;$f2>=0;$f2-- )
      {

        $f = ( $f1*10 ) + $f2;

        $texto = $this->nosso_numero;

        for ( $i=1; $i<6; $i++ )
        {
          $texto .= substr( $barcodes[$f1],($i-1),1 ) . substr($barcodes[$f2],($i-1),1);
        }

        $texto = $this->nosso_numero;

        $barcodes[$f] = $texto;
      }
    }

    echo '<img src='.Yii::app()->getBaseUrl(true).'/images/p.png width="'.$fino.'" height="'.$altura.'" border=0>'.'<img src='.Yii::app()->getBaseUrl(true).'/images/b.png width="'.$fino.'" height="'.$altura.'" border=0>'.'<img src='.Yii::app()->getBaseUrl(true).'/images/p.png width="'.$fino.'" height="'.$altura.'" border=0>'.'<img src='.Yii::app()->getBaseUrl(true).'/images/b.png width="'.$fino.'" height="'.$altura.'" border=0><img ';

    // $texto = $this->linha;
    $texto = $this->nosso_numero;

    if( (strlen( $texto ) %2 ) <> 0 )
    {
      $texto = "0".$this->nosso_numero;
    }

    while( strlen( $texto ) > 0 )
    {
      $i      = round( $this->esquerda($texto,2) );
      $texto  = $this->direita($texto,strlen($texto)-2);
      $f      = $barcodes[$i];

      for( $i = 1; $i < 11; $i += 2 )
      {
        if(substr($f,($i-1),1)=="0") { $f1 = $fino; } else { $f1 = $largo; }
        echo 'src='.Yii::app()->getBaseUrl(true).'/images/p.png width="'.$f1.'" height="'.$altura.'" border=0><img ';
        if(substr($f,$i,1)=="0") { $f2 = $fino; } else { $f2 = $largo; }
        echo 'src='.Yii::app()->getBaseUrl(true).'/images/b.png width="'.$f2.'" height="'.$altura.'" border=0><img ';
      }
    }

    $aux = 1;

    echo 'src='.Yii::app()->getBaseUrl(true).'/images/p.png width="'.$largo.'" height="'.$altura.'" border=0><img '.
       'src='.Yii::app()->getBaseUrl(true).'/images/b.png width="'.$fino.'" height="'.$altura.'" border=0><img '.
       'src='.Yii::app()->getBaseUrl(true).'/images/p.png width="'.$aux.'" height="'.$altura.'" border=0>';
  }

  public function fbarcode(){

    $fino     = 1;
    $largo    = 3;
    $altura   = 50;

    $barcodes[0] = "00110" ;
    $barcodes[1] = "10001" ;
    $barcodes[2] = "01001" ;
    $barcodes[3] = "11000" ;
    $barcodes[4] = "00101" ;
    $barcodes[5] = "10100" ;
    $barcodes[6] = "01100" ;
    $barcodes[7] = "00011" ;
    $barcodes[8] = "10010" ;
    $barcodes[9] = "01010" ;

    for ( $f1=9; $f1>=0; $f1-- )
    {
      for ( $f2=9;$f2>=0;$f2-- )
      {

        $f = ( $f1*10 ) + $f2;

        $texto = "" ;

        for ( $i=1; $i<6; $i++ )
        {
          $texto .= substr( $barcodes[$f1],($i-1),1 ) . substr($barcodes[$f2],($i-1),1);
        }

        $barcodes[$f] = $texto;
      }
    }

    echo '<img src='.Yii::app()->getBaseUrl(true).'/images/p.png width="'.$fino.'" height="'.$altura.'" border=0>'.'<img src='.Yii::app()->getBaseUrl(true).'/images/b.png width="'.$fino.'" height="'.$altura.'" border=0>'.'<img src='.Yii::app()->getBaseUrl(true).'/images/p.png width="'.$fino.'" height="'.$altura.'" border=0>'.'<img src='.Yii::app()->getBaseUrl(true).'/images/b.png width="'.$fino.'" height="'.$altura.'" border=0><img ';

    $texto = $this->linha;

    if( (strlen( $texto ) %2 ) <> 0 )
    {
      $texto = "0".$texto;
    }

    while( strlen( $texto ) > 0 )
    {
      $i      = round( $this->esquerda($texto,2) );
      $texto  = $this->direita($texto,strlen($texto)-2);
      $f      = $barcodes[$i];

      for( $i = 1; $i < 11; $i += 2 )
      {
        if(substr($f,($i-1),1)=="0") { $f1 = $fino; } else { $f1 = $largo; }
        echo 'src='.Yii::app()->getBaseUrl(true).'/images/p.png width="'.$f1.'" height="'.$altura.'" border=0><img ';
        if(substr($f,$i,1)=="0") { $f2 = $fino; } else { $f2 = $largo; }
        echo 'src='.Yii::app()->getBaseUrl(true).'/images/b.png width="'.$f2.'" height="'.$altura.'" border=0><img ';
      }
    }

    $aux = 1;

    echo 'src='.Yii::app()->getBaseUrl(true).'/images/p.png width="'.$largo.'" height="'.$altura.'" border=0><img '.
       'src='.Yii::app()->getBaseUrl(true).'/images/b.png width="'.$fino.'" height="'.$altura.'" border=0><img '.
       'src='.Yii::app()->getBaseUrl(true).'/images/p.png width="'.$aux.'" height="'.$altura.'" border=0>';
  }

}

?>
