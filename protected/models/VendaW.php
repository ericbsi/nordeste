<?php

class VendaW extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'VendaW';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('valor_total_texto, Origem_venda_id, titulos_gerados, Cliente_id, Filial_id, external_access_token', 'required'),
			array('numero_parcelas, Origem_venda_id, Cliente_id, Filial_id', 'numerical', 'integerOnly'=>true),
			array('valor_total, titulos_gerados', 'numerical'),
			array('codigo_externo', 'length', 'max'=>100),
			array('codigo, data_compra', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, codigo, codigo_externo, numero_parcelas, data_compra, valor_total, valor_total_texto, Origem_venda_id, titulos_gerados, Cliente_id, Filial_id, external_access_token', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'filial' 		=> array(self::BELONGS_TO, 'Filial', 'Filial_id'),
			'origemVenda' 	=> array(self::BELONGS_TO, 'OrigemVenda', 'Origem_venda_id'),
			'cliente' 		=> array(self::BELONGS_TO, 'Cliente', 'Cliente_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'codigo' => 'Codigo',
			'codigo_externo' => 'Codigo Externo',
			'numero_parcelas' => 'Numero Parcelas',
			'data_compra' => 'Data Compra',
			'valor_total' => 'Valor Total',
			'valor_total_texto' => 'Valor Total Texto',
			'Origem_venda_id' => 'Origem Venda',
			'titulos_gerados' => 'Titulos Gerados',
			'Cliente_id' => 'Cliente',
			'Filial_id' => 'Filial',
			'external_access_token' => 'External Access Token',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('codigo',$this->codigo,true);
		$criteria->compare('codigo_externo',$this->codigo_externo,true);
		$criteria->compare('numero_parcelas',$this->numero_parcelas);
		$criteria->compare('data_compra',$this->data_compra,true);
		$criteria->compare('valor_total',$this->valor_total);
		$criteria->compare('valor_total_texto',$this->valor_total_texto,true);
		$criteria->compare('Origem_venda_id',$this->Origem_venda_id);
		$criteria->compare('titulos_gerados',$this->titulos_gerados);
		$criteria->compare('Cliente_id',$this->Cliente_id);
		$criteria->compare('Filial_id',$this->Filial_id);
		$criteria->compare('external_access_token',$this->external_access_token,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VendaW the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
