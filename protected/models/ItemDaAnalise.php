<?php

/**
 * This is the model class for table "Item_da_Analise".
 *
 * The followings are the available columns in table 'Item_da_Analise':
 * @property integer $id
 * @property integer $Analise_de_Credito_id
 * @property integer $Grupo_de_Produto_id
 * @property string $data_cadastro
 * @property integer $habilitado
 * @property integer $quantidade
 *
 * The followings are the available model relations:
 * @property AnaliseDeCredito $analiseDeCredito
 * @property GrupoDeProduto $grupoDeProduto
 */
class ItemDaAnalise extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Item_da_Analise';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Analise_de_Credito_id, Grupo_de_Produto_id', 'required'),
			array('Analise_de_Credito_id, Grupo_de_Produto_id, habilitado, quantidade', 'numerical', 'integerOnly'=>true),
			array('data_cadastro, data_cadastro_br', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, Analise_de_Credito_id, Grupo_de_Produto_id, data_cadastro, data_cadastro_br, habilitado, quantidade', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'analiseDeCredito' => array(self::BELONGS_TO, 'AnaliseDeCredito', 'Analise_de_Credito_id'),
			'grupoDeProduto' => array(self::BELONGS_TO, 'GrupoDeProduto', 'Grupo_de_Produto_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Analise_de_Credito_id' => 'Analise De Credito',
			'Grupo_de_Produto_id' => 'Grupo De Produto',
			'data_cadastro' => 'Data Cadastro',
			'data_cadastro_br' => 'Data / Hora Cadastro',
			'habilitado' => 'Habilitado',
			'quantidade' => 'Quantidade',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Analise_de_Credito_id',$this->Analise_de_Credito_id);
		$criteria->compare('Grupo_de_Produto_id',$this->Grupo_de_Produto_id);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('data_cadastro_br',$this->data_cadastro_br,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('quantidade',$this->quantidade);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ItemDaAnalise the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
