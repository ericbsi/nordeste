<?php

class CampoCnabConfig extends CActiveRecord{

	public function gerarRemessa( $Banco_id, $dataDe, $dataAte, $nomeDoArquivo )
	{
		if( $Banco_id == 5 )
		{
			return $this->geracaoCnab400HSBC( $Banco_id, $dataDe, $dataAte, $nomeDoArquivo );
		}

		else if( $Banco_id == 7 )
		{
			return $this->geracaoCnab400Santander( $Banco_id, $dataDe, $dataAte, $nomeDoArquivo );
		}

		else if( $Banco_id == 10 )
		{
			return $this->geracaoRetornoCredshow();
		}

		else
		{
			return array();
		}
	}

	public function gerarRemessaChubb( $Banco_id, $dataDe, $dataAte, $nomeDoArquivo )
	{
		$mY 										= substr($dataDe, 3,2).substr($dataDe, 6);

		$nomeArquivo 								= '8070008_0000_'.$mY.'_'.str_replace(['/'], [''], $dataDe);

		$arrReturn 									= array(
			'hasErrors' 							=> 0, 
			'errors' 								=> array(
			)
		);

		$valorTotalDeRegistros						= 0;

		$arquivoRemessa             				= 'remessas/chubb/'.$nomeArquivo.'.txt';

		if ( !file_exists( $arquivoRemessa ) )
		{
			/*
				-Primeiro, verificar se existem detalhes na data informada
				-O processo só seguirá caso existam detalhes.
			*/
			$registros                				= CnabDetalhe::model()->getDetalhes($Banco_id, $dataDe, $dataAte);

			if( count( $registros ) > 0 )
			{	
				$handle                     		= fopen($arquivoRemessa, 'w+'); //Cria o arquivo
				$header 						 	= new CnabHeader;
				$trailer 						 	= new CnabTrailer;

				$confgCamposChubb       			= $this->camposRemessaChubb(); //Carrega as configurações do modelo Chubb de arquivo

				$camposHeader             			= $this->returnCampos( $confgCamposChubb['header'] );
		        $camposDetalhe             			= $this->returnCampos( $confgCamposChubb['detalhe'] );
		        $camposTrailer             			= $this->returnCampos( $confgCamposChubb['trailer'] );


		        /*INICIO HEADER*/
		        $linhaHeader 						= '';

		        $header->identificacao_do_registro 	= '0';
		        $header->data_de_geracao 			= date('Ymd');
		        $header->hora_de_geracao 			= date('His');
		        $header->nome_do_representante		= 'CREDSHOW OPERADORA DE CREDITO SA';
		        $header->cnpj_do_representante		= '013861348000115';
		        $header->competencia_do_seguro		= date('Ym');
		        $header->sequencial_do_arquivo		= '000001';
		        $header->filler						= '';

		        for ( $i = 0; $i < count( $camposHeader ); $i++ )
		        {
		        	$campoHeader 					= CampoCnabConfig::model()->findByPk( $camposHeader[$i] );
		        	$nSpacesAdd         			= ( $campoHeader->posi_fim - $campoHeader->posi_ini ) + 1;

		        	for ( $y = 0; $y < $nSpacesAdd; $y++ )
		            {
		            	$linhaHeader          		.= str_replace( "\xc2\xa0",' ', html_entity_decode("&nbsp;") );
		            }

		            $linhaHeader               		= substr_replace($linhaHeader, $header->getAttribute($campoHeader->sigla), $campoHeader->posi_ini-1,1);
		        }

		        fwrite($handle, substr($linhaHeader,0,1359).PHP_EOL);
		        /*FIM HEADER*/

		        /*REGISTROS*/
		        foreach( $registros as $r )
				{
					$nSpacesAdd						= 0;
        			$linhaDetalhe          			= '';

        			for ( $i = 0; $i < sizeof($camposDetalhe); $i++ )
        			{
        				$campoDetalhe              	= CampoCnabConfig::model()->findByPk( $camposDetalhe[$i] );
                		$nSpacesAdd         		= ( $campoDetalhe->posi_fim - $campoDetalhe->posi_ini ) + 1;

                		for ( $y = 0; $y < $nSpacesAdd; $y++ )
		                {
		                	$linhaDetalhe          	.= str_replace( "\xc2\xa0",' ', html_entity_decode("&nbsp;") );
		                }

                		$linhaDetalhe               = substr_replace($linhaDetalhe, $r->getAttribute($campoDetalhe->sigla), $campoDetalhe->posi_ini-1,1);
        			}

        			fwrite($handle, substr($linhaDetalhe,0,1359).PHP_EOL);
				}
		        /*FIM REGISTROS*/

		        /*TRAILER*/
		     	$trailer->identificacao_do_registro = '9';
		     	$trailer->total_de_registros		= CampoCnabConfig::model()->cnabValorMonetario(count($registros)+2,6);

		     	$linhaTrailer 						= "";

		   		for( $b = 0; $b < count( $camposTrailer ); $b ++ )
		        {
		        	
			    	$campoTrailer 			    					= CampoCnabConfig::model()->findByPk( $camposTrailer[$b] );
			    	$nSpacesAdd         							= ( $campoTrailer->posi_fim - $campoTrailer->posi_ini ) + 1;

		        	for ( $y = 0; $y < $nSpacesAdd; $y++ )
		            {
		            	$linhaTrailer          						.= str_replace( "\xc2\xa0",' ', html_entity_decode("&nbsp;") );
		            }

		            $linhaTrailer               			 		= substr_replace($linhaTrailer, $trailer->getAttribute($campoTrailer->sigla), $campoTrailer->posi_ini-1,1);
		        }

		        fwrite($handle, substr($linhaTrailer,0,1359).PHP_EOL);
		        fclose($handle);
		        /*FIM TRAILER*/
		        
		        $arrReturn['hasErrors'] = 0;
				$arrReturn['errors'][] 	= 'Tudo ok';
			}

			/*Não existem registros para o critério informado*/
			else
			{
				$arrReturn['hasErrors'] = 1;
				$arrReturn['errors'][] 	= 'Nenhum registro encontrado';
			}
		}

		/*Usuário informou um nome de arquivo que já existe*/
	    else
		{
			$arrReturn['hasErrors'] = 1;
			$arrReturn['errors'][] 	= 'Arquivo já existente.';
		}

		return $arrReturn;
	}
	
	public function geracaoCnab400HSBC( $Banco_id, $dataDe, $dataAte, $nomeDoArquivo )
	{
		$arrReturn 		= array(
			'hasErrors' => 0, 
			'errors' 	=> array(
			)
		);

		$arquivoRemessa             				= 'remessas/hsbc/'.$nomeDoArquivo.'.txt';

		/*Verifica se o arquivo já existe. Se não existir, prossegue...*/
		if ( !file_exists( $arquivoRemessa ) )
		{
			$registros                				= CnabDetalhe::model()->getDetalhes($Banco_id, $dataDe, $dataAte);

			if( count( $registros ) > 0 )
			{
				$confgCamposHSBC       				= $this->cnab400HSBCConfig();
				$headerBaseHSBC    					= CnabHeader::model()->findByPk(1);
				$dataGeracaoCnab 					= date('dmy');
				$nSpacesAdd 						= 0;
        		$trailer    						= new CnabTrailer;
        		$handle                     		= fopen($arquivoRemessa, 'w+');
        		$numSequencial 						= 1;
		        
		        $camposHeader             			= $this->returnCampos( $confgCamposHSBC['header'] );
		        $camposDetalhe             			= $this->returnCampos( $confgCamposHSBC['detalhe'] );
		        $camposTrailer             			= $this->returnCampos( $confgCamposHSBC['trailer'] );
				
				/*INICIO HEADER*/
				$headerBaseHSBC->setAttribute('agencia_cedente',	'0738');
				$headerBaseHSBC->setAttribute('conta_corrente',		'07380072898');
				$headerBaseHSBC->setAttribute('nome_do_cliente',	'NOVA CRUZ COMERCIO DE MOVEIS L');
				$headerBaseHSBC->setAttribute('data_gravacao',		date('Y-m-d'));
				$headerBaseHSBC->setAttribute('data_gravacao_texto',date('dmy'));

				$linhaHeader 						= '';

				for ( $i = 0; $i < count( $camposHeader ); $i++ )
		        {
		        	$campoHeader 					= CampoCnabConfig::model()->findByPk( $camposHeader[$i] );
		        	$nSpacesAdd         			= ( $campoHeader->posi_fim - $campoHeader->posi_ini ) + 1;

		        	for ( $y = 0; $y < $nSpacesAdd; $y++ )
		            {
		            	$linhaHeader          		.= str_replace( "\xc2\xa0",' ', html_entity_decode("&nbsp;") );
		            }

		            $linhaHeader               		= substr_replace($linhaHeader, $headerBaseHSBC->getAttribute($campoHeader->sigla), $campoHeader->posi_ini-1,1);
		            
		        }

		        fwrite($handle, substr($linhaHeader,0,400).PHP_EOL);
				/*FIM HEADER*/

				/*INICIO DETALHES*/
				foreach( $registros as $r )
				{
					$nSpacesAdd						= 0;
        			$linhaDetalhe          			= '';
        			$numSequencial++;
        			$r->numero_sequencial 			= $this->getCodigoSequencial('detalhe',date('dmy'), $numSequencial);

        			for ( $i = 0; $i < sizeof($camposDetalhe); $i++ )
        			{
        				$campoDetalhe              	= CampoCnabConfig::model()->findByPk( $camposDetalhe[$i] );
                		$nSpacesAdd         		= ( $campoDetalhe->posi_fim - $campoDetalhe->posi_ini ) + 1;

                		for ( $y = 0; $y < $nSpacesAdd; $y++ )
		                {
		                	$linhaDetalhe          	.= str_replace( "\xc2\xa0",' ', html_entity_decode("&nbsp;") );
		                }

                		$linhaDetalhe               = substr_replace($linhaDetalhe, $r->getAttribute($campoDetalhe->sigla), $campoDetalhe->posi_ini-1,1);
        			}

        			fwrite($handle, substr($linhaDetalhe,0,400).PHP_EOL);
				}
				/*FIM DETALHES*/
				
				/*INICIO TRAILER*/
				$cnabTrailer 											= new CnabTrailer;
				$nSpacesAdd 											= 0;
				$linhaTrailer 											= '';
				$cnabTrailer->cod_registro 								= '9';
				$cnabTrailer->banco 									= '';
				$cnabTrailer->numero_sequencial				 			= $this->getCodigoSequencial('detalhe', date('dmy'), count($registros)+2);

				for( $x = 0; $x < count( $camposTrailer ); $x ++ )
		        {
			    	$campoTrailer 			    			= CampoCnabConfig::model()->findByPk( $camposTrailer[$x] );
			    	$nSpacesAdd         					= ( $campoTrailer->posi_fim - $campoTrailer->posi_ini ) + 1;

		        	for ( $y = 0; $y < $nSpacesAdd; $y++ )
		            {
		            	$linhaTrailer          				.= str_replace( "\xc2\xa0",' ', html_entity_decode("&nbsp;") );
		            }

		            $linhaTrailer               			 = substr_replace($linhaTrailer, $cnabTrailer->getAttribute($campoTrailer->sigla), $campoTrailer->posi_ini-1,1);
		        }

        		fwrite($handle, substr($linhaTrailer,0,400).PHP_EOL);
				/*FIM TRAILER*/

				fclose($handle);
			}
			else
			{
				$arrReturn['hasErrors'] = 1;
				$arrReturn['errors'][] 	= 'Não foram encontrados registros.';
			}
		}

		else
		{
			$arrReturn['hasErrors'] = 1;
			$arrReturn['errors'][] 	= 'Arquivo já existente.';
		}
	} 

	public function geracaoCnab400Santander( $Banco_id, $dataDe, $dataAte, $nomeDoArquivo )
	{
		$arrReturn 		= array(
			'hasErrors' => 0, 
			'errors' 	=> array(
			)
		);

		$valorTotalDeRegistros	= 0;

		$arquivoRemessa             				= 'remessas/santander/'.$nomeDoArquivo.'.txt';

		/*Verifica se o arquivo já existe. Se não existir, prossegue...*/
		if ( !file_exists( $arquivoRemessa ) )
		{
			/*
				-Primeiro, verificar se existem detalhes na data informada
				-O processo só seguirá caso existam detalhes.
			*/
			$registros                						= CnabDetalhe::model()->getDetalhes($Banco_id, $dataDe, $dataAte);

			if( count( $registros ) > 0 )
			{
				$valorTotalDeRegistros						= 0;
				$confgCamposSantander       				= $this->cnab400SantanderConfig(); //Carrega as configurações do CNAB400 Santander
				$numSequencial 								= 1;
				$nSpacesAdd 								= 0;
		        $handle                     				= fopen($arquivoRemessa, 'w+'); //Cria o arquivo
		        $headerBaseSantander    					= CnabHeader::model()->findByPk(3); //Header Base do Santander
		        $trailer    								= new CnabTrailer;
		        $headerBaseSantander->data_gravacao_texto 	= date('dmy');
		       
		        $camposHeader             					= $this->returnCampos( $confgCamposSantander['header'] );
		        $camposDetalhe             					= $this->returnCampos( $confgCamposSantander['detalhe'] );
		        $camposTrailer             					= $this->returnCampos( $confgCamposSantander['trailer'] );

		        /*INICIO HEADER*/
		        $linhaHeader 								= '';

		        for ( $i = 0; $i < count( $camposHeader ); $i++ )
		        {
		        	$campoHeader 							= CampoCnabConfig::model()->findByPk( $camposHeader[$i] );
		        	$nSpacesAdd         					= ( $campoHeader->posi_fim - $campoHeader->posi_ini ) + 1;

		        	for ( $y = 0; $y < $nSpacesAdd; $y++ )
		            {
		            	$linhaHeader          				.= str_replace( "\xc2\xa0",' ', html_entity_decode("&nbsp;") );
		            }

		            $linhaHeader               			 	= substr_replace($linhaHeader, $headerBaseSantander->getAttribute($campoHeader->sigla), $campoHeader->posi_ini-1,1);
		        }

		        fwrite($handle, substr($linhaHeader,0,400).PHP_EOL);
		        /*FIM HEADER*/
		        	
		        /*INICIO REGISTROS*/
		        foreach( $registros as $r )
		        {
		        	$numSequencial++;
		        	$nSpacesAdd 							= 0;
		        	$linhaDetalhe                  			= '';
		        	$valorTotalDeRegistros					+= $r->valor_do_titulo;

		        	$r->numero_sequencial 					= $this->getCodigoSequencial('detalhe',date('dmy'), $numSequencial);

		        	for ( $i = 0; $i < sizeof( $camposDetalhe ); $i++ )
		        	{
		        		$campoDetalhe              			= CampoCnabConfig::model()->findByPk( $camposDetalhe[$i] );

		                $nSpacesAdd         				= ( $campoDetalhe->posi_fim - $campoDetalhe->posi_ini ) + 1;

		                for ( $y = 0; $y < $nSpacesAdd; $y++ )
		                {
		                	$linhaDetalhe          			.= str_replace( "\xc2\xa0",' ', html_entity_decode("&nbsp;") );
		                }

		                $linhaDetalhe               		= substr_replace($linhaDetalhe, $r->getAttribute($campoDetalhe->sigla), $campoDetalhe->posi_ini-1,1);
		        	}

		        	fwrite($handle, substr($linhaDetalhe,0,400).PHP_EOL);
		        }
		        /*FIM REGISTROS*/

		       	/*INICIO TRAILER*/
		        $nSpacesAdd 										= 0;
		        $linhaTrailer 										= '';

		        $trailer->cod_registro				 				= '9';
		        $trailer->quantidade_de_documentos_no_arquivo_sant	= $this->getCodigoSequencial('detalhe', date('dmy'), count($registros)+2);
		        //$trailer->valor_total_dos_titulos_sant				= '0000000000300';
		        $trailer->valor_total_dos_titulos_sant				= $this->cnabValorMonetario($valorTotalDeRegistros,13);
		        $trailer->numero_sequencial				 			= $this->getCodigoSequencial('detalhe', date('dmy'), count($registros)+2);
		        $trailer->zeros1_sant				 				= '00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000';

		        for( $x = 0; $x < count( $camposTrailer ); $x ++ )
		        {
			    	$campoTrailer 			    					= CampoCnabConfig::model()->findByPk( $camposTrailer[$x] );
			    	$nSpacesAdd         							= ( $campoTrailer->posi_fim - $campoTrailer->posi_ini ) + 1;

		        	for ( $y = 0; $y < $nSpacesAdd; $y++ )
		            {
		            	$linhaTrailer          						.= str_replace( "\xc2\xa0",' ', html_entity_decode("&nbsp;") );
		            }

		            $linhaTrailer               			 		= substr_replace($linhaTrailer, $trailer->getAttribute($campoTrailer->sigla), $campoTrailer->posi_ini-1,1);
		        }
		        
		        fwrite($handle, substr($linhaTrailer,0,400).PHP_EOL);
		        /*FIM TRAILER*/

		        fclose($handle);

		        $arrReturn['hasErrors'] = 0;
				$arrReturn['errors'][] 	= 'Tudo ok.';
			}

			/*Não existem registros para o critério informado*/
			else
			{
				$arrReturn['hasErrors'] = 1;
				$arrReturn['errors'][] 	= 'Nenhum registro encontrado';
			}
	    }

	    /*Usuário informou um nome de arquivo que já existe*/
	    else
		{
			$arrReturn['hasErrors'] = 1;
			$arrReturn['errors'][] 	= 'Arquivo já existente.';
		}

		return $arrReturn;
	}

	public function cnab400SantanderConfig(){

		return array(
			'header' 	=> array(48,49,50,51,52,59,107,58,60,61,109,108,110,111,112,113,114,115,67),
			'detalhe' 	=> array(1,2,3,116,117,118,9,119,120,121,122,123,
								124,125,126,127,16,15,17,18,19,20,21,22,23,24,25,26,
								27,28,29,30,31,32,33,35,128,38,39,40,41,42,129,130,
								131,132,133,45,135,47),
			'trailer' 	=> array(68,136,137,138,71),
		);
	}

	public function cnabItauConfig()
	{
		return array(
			'header' 	=> [48,49,50,51,52,276,212,277,278,279,58,59,60,61,280, 67],
			'detalhe' 	=> [1,2,3,116,281,282,283,284,285,9,119,286,287,288,105,16,
							17,18,19,20,21,22,23,24,25,26,28,29,30,31,32,33,289,290,128,38,39,40,
							41,42,291,292,133,45,135,47],
			'trailer' 	=> [68,69,71],
		);
	}

	public function returnCampos($ids){
		
		$idsCampos 						= array();
		$criteria 						= new CDbCriteria;
		$criteria->addInCondition('id',	$ids,' AND ');
		$criteria->order 				= ' t.posi_ini ASC ';

		$camposCfg 						= CampoCnabConfig::model()->findAll( $criteria );

		foreach( $camposCfg as $cCfg )
		{
			$idsCampos[] 				= $cCfg->id;
		}

		return $idsCampos;
	}

	public function cnab400HSBCConfig()
	{
		return array(
			'header' 	=> array(48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67),
			'detalhe' 	=> array(
				1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,
				19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,
				35,36,37,38,39,40,41,42,43,44,45,46,47
			),
			'trailer' 	=> array(68,69,71),
		);
	}

	public function cnabCredshowConfig()
	{
		return array(
			'header_a' 	=> [223,224,225,226,227,228,229,230,231,232		],
			'header_e' 	=> [223,224,233,234,235,236,237					],
			'detalhe' 	=> [211,212,213,214,215,216,217,218,222,220,221	],
			'trailer' 	=> [223,224,238,239,240,241,242,243,244,245		],
		);
	}

	public function camposRemessaChubb()
	{
		return array(
			'header' 	=> array(139,140,141,142,143,144,145,146),
			'detalhe' 	=> array(
				147,148,149,150,151,152,153,154,155,
				156,157,158,159,160,161,162,163,164,
				165,166,167,168,169,170,171,172,173,
				174,175,176,177,178,179,180,181,182,
				183,184,185
			),
			'trailer' 	=> array(186,187,188),
		);
	}

	public function geracaoRetornoCredshow()
	{
		$arrReturn 		= array(
			'hasErrors' => 0, 
			'errors' 	=> []
		);

		$nSpacesAdd 	= 0;

		$arquivoRemessa             									= 'remessas/credshow/arquivovalidacao.TXT';

		if ( !file_exists( $arquivoRemessa ) )
		{
			$util 														= new Util;

			$criteriaDetalhes 											= new CDbCriteria;
			$criteriaDetalhes->addInCondition('t.habilitado', 			array(1)	, 	' AND ');
			$criteriaDetalhes->addInCondition('t.Banco_id', 			array(10)	, 	' AND ');
			//$criteriaDetalhes->addBetweenCondition('t.data_da_operacao_credshow', 	$util->view_date_to_bd( '21-10-2015' ), $util->view_date_to_bd( '21-10-2015' ) );

			$registros                									= CnabDetalhe::model()->findAll( $criteriaDetalhes );

			if( count( $registros ) > 0 )
			{
				$handle                     							= fopen($arquivoRemessa, 'w+'); //Cria o arquivo
				$confgCamposCredshow       								= $this->cnabCredshowConfig();
				$nSpacesAdd 											= 0;

				$camposHeader_a           								= $this->returnCampos( $confgCamposCredshow['header_a'	] 	);
				$camposHeader_e           								= $this->returnCampos( $confgCamposCredshow['header_e'	] 	);
				$camposDetalhe             								= $this->returnCampos( $confgCamposCredshow['detalhe'	]	);
				$camposTrailer             								= $this->returnCampos( $confgCamposCredshow['trailer'	]	);

				/*Headers*/

				$headerA 												= new CnabHeader;
				$headerE 												= new CnabHeader;

				$headerA->numero_sequencial_credshow					= '000001';
				$headerA->tipo_de_registro_credshow						= '0';
				$headerA->tipo_de_transmissao_credshow					= 'BC';
				$headerA->data_do_movimento_credshow					= date('Ymd');
				$headerA->codigo_do_lojista_credshow					= '000000';
				$headerA->filial_do_lojista_credshow					= '000';
				$headerA->numero_da_transmissao_credshow				= '000000';
				$headerA->data_do_pagamento_credshow					= '00000000';
				$headerA->hora_da_transmissao_credshow					= '000000';
				$headerA->reserva_credshow								= '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000';

				$headerE->numero_sequencial_credshow					= '000000';
				$headerE->tipo_de_registro_credshow						= '1';
				$headerE->tipo_de_empresa_credshow						= '0';
				$headerE->codigo_do_lojista_cpg_credshow				= '000000';
				$headerE->filial_do_lojista_cpg_credshow				= '000';
				$headerE->bordero_de_pagamento_credshow					= '00000';
				$headerE->reserva_2_credshow							= '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000';

				//header de arquivo e de empresa
				$linhaHeader_a 											= '';
				$linhaHeader_e 											= '';

				for ( $y = 0; $y < sizeof($camposHeader_a); $y++ )
				{
					$campoHeader_a         			= CampoCnabConfig::model()->findByPk( $camposHeader_a[$y] );

					$nSpacesAdd         			= ( $campoHeader_a->posi_fim - $campoHeader_a->posi_ini ) + 1;

		        	for ( $z = 0; $z < $nSpacesAdd; $z++ )
		            {
		            	$linhaHeader_a         		.= str_replace( "\xc2\xa0",' ', html_entity_decode("&nbsp;") );
		            }

		            $linhaHeader_a             		= substr_replace($linhaHeader_a, $headerA->getAttribute($campoHeader_a->sigla), $campoHeader_a->posi_ini-1,1);
				}

				for ( $z = 0; $z < sizeof($camposHeader_e); $z++ )
				{
					$campoHeader_e         			= CampoCnabConfig::model()->findByPk( $camposHeader_e[$z] );

					$nSpacesAdd         			= ( $campoHeader_e->posi_fim - $campoHeader_e->posi_ini ) + 1;

		        	for ( $a = 0; $a < $nSpacesAdd; $a++ )
		            {
		            	$linhaHeader_e         		.= str_replace( "\xc2\xa0",' ', html_entity_decode("&nbsp;") );
		            }

		            $linhaHeader_e             		= substr_replace($linhaHeader_e, $headerE->getAttribute($campoHeader_e->sigla), $campoHeader_e->posi_ini-1,1);
				}
				
				fwrite($handle, substr($linhaHeader_a,0,250)."\r\n");
				fwrite($handle, substr($linhaHeader_e,0,250)."\r\n");
				//fim headers

				//DETALHE
		        foreach( $registros as $r )
		        {
		        	$nSpacesAdd 							= 0;
		        	$linhaDetalhe                  			= '';
		        
		        	for ( $i = 0; $i < sizeof( $camposDetalhe ); $i++ )
		        	{
		        		$campoDetalhe              			= CampoCnabConfig::model()->findByPk( $camposDetalhe[$i] );

		                $nSpacesAdd         				= ( $campoDetalhe->posi_fim - $campoDetalhe->posi_ini ) + 1;

		                for ( $b = 0; $b < $nSpacesAdd; $b++ )
		                {
		                	$linhaDetalhe          			.= str_replace( "\xc2\xa0",' ', html_entity_decode("&nbsp;") );
		                }

		                $linhaDetalhe               		= substr_replace($linhaDetalhe, $r->getAttribute($campoDetalhe->sigla), $campoDetalhe->posi_ini-1,1);
		        	}

		        	fwrite($handle, substr($linhaDetalhe,0,250)."\r\n");
		        }
		        
		        //FIM DETALHE



		        //trailer

		        $trailer 												= new CnabTrailer;
		        $trailer->numero_sequencial_credshow 					= '000000';
		        $trailer->tipo_de_registro_credshow 					= '9';
		        $trailer->quantidade_de_registros_credshow 				= '000000';
		        $trailer->valor_total_de_venda_credshow 				= '000000000000000000';
		        $trailer->valor_total_da_retencao_credshow 				= '000000000000000000';
		        $trailer->valor_total_do_servico_credshow 				= '000000000000000000';
		        $trailer->valor_total_do_iss_credshow 					= '000000000000000000';
		        $trailer->reservado_credshow_credshow 					= '000000000000000000000000000000000000';
		        $trailer->valor_total_do_liquido_credshow 				= '000000000000000000';
		        $trailer->reservado_credshow_2_credshow 				= '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000';

		        $linhaTrailer 											= '';

		        for ( $o = 0; $o < sizeof($camposTrailer); $o++ )
				{
					$campoTrailer         			= CampoCnabConfig::model()->findByPk( $camposTrailer[$o] );

					$nSpacesAdd         			= ( $campoTrailer->posi_fim - $campoTrailer->posi_ini ) + 1;

		        	for ( $l = 0; $l < $nSpacesAdd; $l++ )
		            {
		            	$linhaTrailer         		.= str_replace( "\xc2\xa0",' ', html_entity_decode("&nbsp;") );
		            }

		            $linhaTrailer             		= substr_replace($linhaTrailer, $trailer->getAttribute($campoTrailer->sigla), $campoTrailer->posi_ini-1,1);
				}

				fwrite($handle, substr($linhaTrailer,0,250)."\r\n");
		        fclose($handle);
			}
		}

		return $arrReturn;

	}

	public function gerarRetorno ( $arrIdsDetalhes, $dadosBancarios, $financeira, $cnabTrailer ){
		
		$util = new Util;
		$nSpacesAdd = 0;
		
		/*Criando o arquivo*/
		$dirRetorno = 'retornos/'.date('dmy');
		$arquivoRetorno = $dirRetorno.'/'. $util->getCodeVenda("0123456789",8).'.txt';

		if ( !is_dir( $dirRetorno ) ) {

			$oldUmask = umask(0);
			mkdir($dirRetorno,0777,true);
			umask($oldUmask);
				
			if ( !file_exists( $arquivoRetorno ) ) {

				$handle = fopen($arquivoRetorno, 'a+');
			}
		}
		
		/*header...*/
		$headerRetorno = new CnabHeader;

		$headerRetorno->setAttribute('cod_registro','0');
		$headerRetorno->setAttribute('cod_arquivo','2');
		$headerRetorno->setAttribute('literal_arquivo','RETORNO');
		$headerRetorno->setAttribute('cod_servico','01');
		$headerRetorno->setAttribute('literal_servico','COBRANCA');
		$headerRetorno->setAttribute('zero','0');
		$headerRetorno->setAttribute('agencia_cedente','0738');
		$headerRetorno->setAttribute('sub_conta','55');
		$headerRetorno->setAttribute('conta_corrente','07380072898');
		$headerRetorno->setAttribute('banco','00');
		$headerRetorno->setAttribute('nome_do_cliente','NOVA CRUZ COMERCIO DE MOVEIS L');
		$headerRetorno->setAttribute('codigo_do_banco','399');
		$headerRetorno->setAttribute('nome_do_banco','HSBC');
		$headerRetorno->setAttribute('data_gravacao_texto',date('dmy'));
		$headerRetorno->setAttribute('densidade','01600');
		$headerRetorno->setAttribute('literal_densidade','BPI');
		$headerRetorno->setAttribute('banco3','00000000000');
		$headerRetorno->setAttribute('data_credito',date('dmy'));
		$headerRetorno->setAttribute('banco4','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
		$headerRetorno->setAttribute('sequencial_arquivo','00000');
		$headerRetorno->setAttribute('banco5','0');
		$headerRetorno->setAttribute('numero_sequencial','000001');
		$headerRetorno->setAttribute('Banco_id',5);

		$linhaHeader = "";

		foreach ( $headerRetorno->attributes as $attr => $value ) {
			
			if ( in_array( $attr, $this->getCampos('header',0) ) ) {
				
				$campoCnabConfig = CampoCnabConfig::model()->find('tipo != 0 AND setor = "header" AND sigla =' . "'$attr'");
				$nSpacesAdd = ( $campoCnabConfig->posi_fim - $campoCnabConfig->posi_ini ) + 1;

				for ( $i = 0; $i < $nSpacesAdd; $i++ ) {
					$linhaHeader .= str_replace( "\xc2\xa0",' ', html_entity_decode("&nbsp;") );
				}

				$linhaHeader = substr_replace( $linhaHeader, $value, $campoCnabConfig->posi_ini-1,1 );
			}
		}

		$nSpacesAdd = 0;

		fwrite($handle, substr($linhaHeader,0,400).PHP_EOL);

		/*Detalhes*/
		$criteriaDetalhes = new CDbCriteria;
		$criteriaDetalhes->addInCondition( 'id', $arrIdsDetalhes );
		$cnabDetalhes = CnabDetalhe::model()->findAll( $criteriaDetalhes );


		foreach ( $cnabDetalhes as $detalhe ) {
			
			$detalheRetorno = new CnabDetalhe;

			$detalheRetorno->setAttribute('cod_registro', '1');
			$detalheRetorno->setAttribute('cod_inscricao', '02');
			$detalheRetorno->setAttribute('num_inscricao', $detalhe->num_inscricao);
			$detalheRetorno->setAttribute('zero', '0');
			$detalheRetorno->setAttribute('agencia_cedente', $detalhe->agencia_cedente);
			$detalheRetorno->setAttribute('sub_conta', $detalhe->sub_conta);
			$detalheRetorno->setAttribute('conta_corrente', $detalhe->conta_corrente);
			$detalheRetorno->setAttribute('origem_pagamento', '0');
			$detalheRetorno->setAttribute('banco8', '0');
			$detalheRetorno->setAttribute('controle_participante', $detalhe->controle_participante);
			$detalheRetorno->setAttribute('nosso_numero', $detalhe->nosso_numero);
			$detalheRetorno->setAttribute('desconto_data1_texto', $detalhe->desconto_data1_texto);
			$detalheRetorno->setAttribute('valor_do_desconto1_texto', $detalhe->valor_do_desconto1_texto);
			$detalheRetorno->setAttribute('desconto_data2_texto', $detalhe->desconto_data2_texto);
			$detalheRetorno->setAttribute('valor_do_desconto2_texto', $detalhe->valor_do_desconto2_texto);
			$detalheRetorno->setAttribute('carteira', $detalhe->carteira);
			$detalheRetorno->setAttribute('cod_ocorrencia', $detalhe->cod_ocorrencia);
			$detalheRetorno->setAttribute('data_ocorrencia', date('dmy'));
			$detalheRetorno->setAttribute('seu_numero2', $detalhe->seu_numero);
			$detalheRetorno->setAttribute('nosso_numero1', '00000000000');
			$detalheRetorno->setAttribute('banco9', '000000000');
			$detalheRetorno->setAttribute('vencimento2', $detalhe->vencimento_texto);
			$detalheRetorno->setAttribute('valor_titulo', $detalhe->valor_do_titulo_texto);
			$detalheRetorno->setAttribute('banco_cobrador2', '123');
			$detalheRetorno->setAttribute('agencia_cobradora', '12345');
			$detalheRetorno->setAttribute('especie2', $detalhe->especie);
			$detalheRetorno->setAttribute('tarifas_custas', '0000000000000');
			$detalheRetorno->setAttribute('banco10', '000000000000000000000000000000000000000');
			$detalheRetorno->setAttribute('valor_abatimento2_texto', '0000000000000');
			$detalheRetorno->setAttribute('valor_do_desconto4_texto', '0000000000000');
			$detalheRetorno->setAttribute('valor_pago', $detalhe->valor_do_titulo_texto);
			$detalheRetorno->setAttribute('juros_de_mora2_texto', $detalhe->juros_de_mora_texto);
			$detalheRetorno->setAttribute('banco11', '0000000000000000000000');
			$detalheRetorno->setAttribute('complemento_da_ocorrencia', '11');
			$detalheRetorno->setAttribute('indicativo_de_credito', '1');
			$detalheRetorno->setAttribute('banco12', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
			$detalheRetorno->setAttribute('numero_do_aviso', '00000');
			$detalheRetorno->setAttribute('moeda', $detalhe->moeda);
			$detalheRetorno->setAttribute('numero_sequencial', $detalhe->numero_sequencial);

			$linhaDetalhe = "";

			foreach ( $detalheRetorno->attributes as $cattr => $valor ) 
			{
				
				if ( in_array( $cattr, CampoCnabConfig::model()->getCampos('detalhe',0) ) ) 
				{
					$campoCnabConfig = CampoCnabConfig::model()->find('tipo != 0 AND setor = "detalhe" AND sigla =' . "'$cattr'");
					$nSpacesAdd = ( $campoCnabConfig->posi_fim - $campoCnabConfig->posi_ini ) + 1;

					for ( $i = 0; $i < $nSpacesAdd; $i++ ) {
						$linhaDetalhe .= str_replace( "\xc2\xa0",' ', html_entity_decode("&nbsp;") );
					}

					$linhaDetalhe = substr_replace($linhaDetalhe, $valor, $campoCnabConfig->posi_ini-1,1);
				}
			}

			fwrite($handle, substr($linhaDetalhe,0,400).PHP_EOL);
		}

		/*Trailer*/

		$trailerRetorno = new CnabTrailer;
		$trailerRetorno->setAttribute('cod_registro', '9');
		$trailerRetorno->setAttribute('codigo_arquivo', '2');
		$trailerRetorno->setAttribute('codigo_do_servico', '01');
		$trailerRetorno->setAttribute('codigo_banco', '399');
		$trailerRetorno->setAttribute('banco6', '0000000000');
		$trailerRetorno->setAttribute('quantidade_ser', '00000000');
		$trailerRetorno->setAttribute('valor_ser', '00000000000000');
		$trailerRetorno->setAttribute('banco7', '');
		$trailerRetorno->setAttribute('numero_sequencial', $cnabTrailer->numero_sequencial);

		$linhaTrailer = "";

		foreach ( $trailerRetorno->attributes as $atributo => $v ) {

			if ( in_array( $atributo, CampoCnabConfig::model()->getCampos('trailer',0) ) ) {

				$campoCnabConfig = CampoCnabConfig::model()->find('tipo != 0 AND setor = "trailer" AND sigla =' . "'$atributo'");
				$nSpacesAdd = ( $campoCnabConfig->posi_fim - $campoCnabConfig->posi_ini ) + 1;

				for ( $i = 0; $i < $nSpacesAdd; $i++ ) {
					$linhaTrailer .= str_replace( "\xc2\xa0",' ', html_entity_decode("&nbsp;") );
				}

				$linhaTrailer = substr_replace($linhaTrailer, $v, $campoCnabConfig->posi_ini-1,1);
			}
		}

		fwrite($handle, substr($linhaTrailer,0,400).PHP_EOL);
		fclose( $handle );
	}

	public function tableName(){
		return 'Campo_cnab_config';
	}

	public function rules(){
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tipo, posi_ini, posi_fim, significativo, decimal', 'numerical', 'integerOnly'=>true),
			array('campo, significado, conteudo, sigla, setor', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, campo, tipo, significado, posi_ini, posi_fim, significativo, decimal, conteudo, sigla, setor', 'safe', 'on'=>'search'),
		);
	}

	public function relations(){

		
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tipo' => 'Tipo',
			'campo' => 'Campo',
			'significado' => 'Significado',
			'posi_ini' => 'Posi Ini',
			'posi_fim' => 'Posi Fim',
			'significativo' => 'Significativo',
			'decimal' => 'Decimal',
			'conteudo' => 'Conteudo',
			'sigla' => 'Sigla',
			'setor' => 'Setor',
		);
	}

	
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tipo',$this->tipo);
		$criteria->compare('campo',$this->campo,true);
		$criteria->compare('significado',$this->significado,true);
		$criteria->compare('posi_ini',$this->posi_ini);
		$criteria->compare('posi_fim',$this->posi_fim);
		$criteria->compare('significativo',$this->significativo);
		$criteria->compare('decimal',$this->decimal);
		$criteria->compare('conteudo',$this->conteudo,true);
		$criteria->compare('sigla',$this->sigla,true);
		$criteria->compare('setor',$this->setor,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/*Para datas yyyy-mm-dd*/
	/*retorna : ddmmyy*/
	public function cnabDate($date){

		/*
			[0] => yyyy
			[1] => mm
			[2] => dd
		*/

		$dataSep = explode('-', $date);

		return $dataSep[2].$dataSep[1].substr($dataSep[0], 2);;
	}

	public function cnabValorMonetario($valor, $sizeReturn){

		$numZerosAdd = $sizeReturn - strlen(str_replace(array('.',','), array('',''), $valor));
		$zeros = "";

		for ($i = 0; $i < $numZerosAdd; $i++) { 
			$zeros .= "0";
		}

		return $zeros.str_replace(array('.',','), array('',''), $valor);
	}

	public function cnabValorMonetarioToDouble( $valor ) {

		$inteiro = floatval( substr( $valor, 0, 11 ) );
		$decimal = substr( $valor, 11, 2 );
		return floatval($inteiro.'.'.$decimal);
	}

	public function jurosDeMora( $valor ){
		$porcent = 0.29;
		return $this->cnabValorMonetario(($valor/100) * $porcent, 13);
	}

	public function getValorAbatimento($vencimento, $taxa){

		/*
		* vencimento : yyyy-mm-dd
		* taxa : 
		*/

		//$vencimento = date('Y-m-d', strtotime($vencimento. ' + 1 days'));
		//return $this->cnabDate($vencimento).$this->cnabValorMonetario($taxa,4);
		
		return "0000000000";
	}

	public function gerarHeaderHSBC(){

	}
	
	public function gerarDetalheHSBC(){

	}

	public function getCodigoSequencial( $setor, $dataGeracao, $seq = NULL ){

		switch ($setor) {
			
			case 'header':
				return '000001';
				break;
			case 'detalhe':

				$zerosAddNumSequencial = "";
				$detalhes = CnabDetalhe::model()->findAll('data_emissao_texto = ' . $dataGeracao . ' AND habilitado');
				
				$numDetalhesGeradosData = sizeof($detalhes);
				
				if( $seq == NULL )
				{
					if ( $numDetalhesGeradosData == 0 ) {
						$numDetalhesGeradosData = 1;
					}
					
					else{
						$numDetalhesGeradosData += 1;
					}

					++$numDetalhesGeradosData;

					for ( $z = 0; $z < 6-strlen((string)$numDetalhesGeradosData) ; $z++ ) {
						$zerosAddNumSequencial .= "0";
					}

					return $zerosAddNumSequencial.strval($numDetalhesGeradosData);
					break;
				}

				else
				{	
					for ( $z = 0; $z < 6-strlen((string)$seq) ; $z++ ) {
						$zerosAddNumSequencial .= "0";
					}
					
					return $zerosAddNumSequencial.strval($seq);
					break;
				}

			default:
				return "000000";
				break;
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CampoCnabConfig the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	* @return um array com os campos que fazem parte do header/detalhe/trailer de 
	* @var int $tipo:  retorno ou remessa
	* @var setor: setor que aplica a diferença na query	
	*/

	public function getCampos( $setor, $tipo ){

		$arrCampos;

		$crt 				= new CDbCriteria();
		$crt->select 		= 'sigla, posi_ini, posi_fim';
		$crt->condition 	= 'setor=:setor AND tipo<>:tipo';
		$crt->params 		= array(':setor' => $setor, ':tipo' => $tipo);
		$crt->order 		= 'posi_ini ASC';
		$camposCnabConfig	= CampoCnabConfig::model()->findAll( $crt );

		foreach ( $camposCnabConfig as $campo ) {
			$arrCampos[] = $campo->sigla;
		}

		return $arrCampos;
	}
}
