<?php

class LoginForm extends CFormModel{

	//public vars
	public $username;
	public $password;

	//private vars
	private $sigacLog;
	private $_identity;
	
	public function rules(){

		return array(
			// username and password are required
			array('username, password', 'required', 'message'=>'Informe o nome de usuário.'),
			// password needs to be authenticated
			array('password', 'message'=>'Informe a senha.', 'authenticate'),
		);
	}

	public function attributeLabels(){

		return array(

		);
	}

	public function authenticate($attribute,$params){
		
		$this->sigacLog = new SigacLog;

		$usuario = Usuario::model()->findByAttributes([ 'username'=>$this->username, 'habilitado'=>1 ]);

		if( !$this->hasErrors() ){

			$this->_identity = new UserIdentity($this->username,$this->password);

			$this->_identity->authenticate();

			switch ( $this->_identity->errorCode ) {

				case UserIdentity::ERROR_NONE:
					$this->sigacLog->saveLog('login; autenticacao','usuario', $usuario->id, date('Y-m-d H:i:s'),1, $usuario->id, "Usuário logado | Usuário: ".$usuario->username, "127.0.0.1",null, Yii::app()->session->getSessionId() );
					Yii::app()->session['usuario'] = $usuario;
					Yii::app()->session->setGCProbability(0);
					//0 indica que, ao fechar o navegador, a sessão do usuário será encerrada
        			Yii::app()->user->login( $this->_identity,0 );
					break;

				case UserIdentity::ERROR_USERNAME_INVALID:
					$this->sigacLog->saveLog('login_de_usuario','usuario', 0, date('Y-m-d H:i:s'),1, null, "Usuário:". $this->username .", não encontrado", "127.0.0.1", null, Yii::app()->session->getSessionId() );
					$this->addError('password','Erro ao entrar: Usuário e/ou senha incorretos.');
			        break;

				default:
					$this->sigacLog->saveLog('login_senha_incorreta','usuario', 0, date('Y-m-d H:i:s'),1, null, "Senha incorreta | Usuário: não encontrado", "127.0.0.1", null, null);
					$this->addError('password','Erro ao entrar: Usuário e/ou senha incorretos.');
			        break;
			}
		}
	}

	/**
	 * Logs in the user using the given username and password in the model.
	 * @return boolean whether login is successful
	 */
	public function login(){
		
		if( $this->_identity === null ) {
			$this->_identity = new UserIdentity($this->username,$this->password);
			$this->_identity->authenticate();
			
		}

		if( $this->_identity->errorCode === UserIdentity::ERROR_NONE ){

			Yii::app()->user->login($this->_identity,0); 
			return true;
		}

		else
			return false;
	}
}
