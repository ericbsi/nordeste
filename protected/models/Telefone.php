<?php

class Telefone extends CActiveRecord
{	

	public function getNumero()
	{
		/*$util 		= new Util;
		$telefone 	= $util->cleanStr($this->numero,'');
		return substr($telefone, 2);*/

		return trim($this->numero);
	}

	public function getDDD()
	{
		/*$util 		= new Util;
		$telefone 	= $util->cleanStr($this->numero,'');
		return substr($telefone, 0,2);*/

		return $this->discagem_direta_distancia;
	}

	public function persist( $Telefone1, $Telefone2, $Telefone3, $Cliente_id, $telefone1_id, $telefone2_id, $telefone3_id, $action )
	{
		if( $action == 'new' )
		{
			return $this->novosTelefones($Telefone1, $Telefone2, $Telefone3, $Cliente_id);
		}
		else if( $action == 'update' )
		{
			$arrReturn = array( 'hasErrors'	=> false );

			$arrReturn['formConfig'][] 			= array(
				'fields' 						=> array(
					'ControllerAction' 			=> array(
						'value' 				=> 'update',
						'type' 					=> 'text',
						'input_bind'			=> 'ControllerTelefoneAction',
					),
				),
			);

			$arrReturn['msgConfig']['pnotify'][] = array(
				'titulo'    => 'Atualização de telefones de contato!',
				'texto'     => 'A ação que você solicitou estará disponível em breve!',
				'tipo'      => 'success',
			);

			$this->atualizarTelefones($telefone1_id, $telefone2_id, $telefone3_id,$Telefone1, $Telefone2, $Telefone3, $Cliente_id);

			return $arrReturn;
		}
	}

	public function atualizarTelefones($telefone1_id, $telefone2_id, $telefone3_id, $Telefone1, $Telefone2, $Telefone3, $Cliente_id)
	{
		$arrReturn = array('hasErrors' => false);

		$arrReturn['formConfig'][] 			= array(
			'fields' 						=> array(
				'ControllerAction' 			=> array(
					'value' 				=> 'update',
					'type' 					=> 'text',
					'input_bind'			=> 'ControllerTelefoneAction',
				),
			),
		);

		if( $telefone1_id != 0 && !empty( $Telefone1['numero'] ) && $Telefone1['numero'] != '' && $Telefone1['numero'] != NULL )
		{
			if( !$this->atualizar($Telefone1,$telefone1_id) )
			{
				$arrReturn['hasErrors'] = true;
			}
		}

		else
		{
			if( !empty( $Telefone1['numero'] ) && $Telefone1['numero'] != '' && $Telefone1['numero'] != NULL )
			{
				$r = $this->novo($Telefone1, $Cliente_id);

				if( $r['hasErrors'] )
				{
					$arrReturn['hasErrors'] = true;
				}
				else
				{
					$arrReturn['formConfig']['fields'][] = array(
						'telefone1id'		=> array(
							'value' 		=> $r['telId'],
							'type' 			=> 'text',
							'input_bind'	=> 'telefone1_id',
						),
					);
				}
			}
		}

		if( $telefone2_id != 0 && !empty( $Telefone2['numero'] ) && $Telefone2['numero'] != '' && $Telefone2['numero'] != NULL )
		{
			if( !$this->atualizar($Telefone2,$telefone2_id) )
			{
				$arrReturn['hasErrors'] = true;
			}
		}
		else
		{
			if( !empty( $Telefone2['numero'] ) && $Telefone2['numero'] != '' && $Telefone2['numero'] != NULL )
			{
				$r = $this->novo($Telefone2, $Cliente_id);

				if( $r['hasErrors'] )
				{
					$arrReturn['hasErrors'] = true;
				}
				else
				{
					$arrReturn['formConfig']['fields'][] = array(
						'telefone2id'		=> array(
							'value' 		=> $r['telId'],
							'type' 			=> 'text',
							'input_bind'	=> 'telefone2_id',
						),
					);
				}
			}
		}

		if( $telefone3_id != 0 && !empty( $Telefone3['numero'] ) && $Telefone3['numero'] != '' && $Telefone3['numero'] != NULL )
		{
			if( !$this->atualizar($Telefone3,$telefone3_id) )
			{
				$arrReturn['hasErrors'] = true;
			}
		}
		else
		{
			if( !empty( $Telefone3['numero'] ) && $Telefone3['numero'] != '' && $Telefone3['numero'] != NULL )
			{
				$r = $this->novo($Telefone3, $Cliente_id);

				if( $r['hasErrors'] )
				{
					$arrReturn['hasErrors'] = true;
				}
				else
				{
					$arrReturn['formConfig']['fields'][] = array(
						'telefone3id'		=> array(
							'value' 		=> $r['telId'],
							'type' 			=> 'text',
							'input_bind'	=> 'telefone3_id',
						),
					);
				}
			}
		}
		
		if( !$arrReturn['hasErrors'] )
		{
			$arrReturn['msgConfig']['pnotify'][] = array(
				'titulo'    => 'Atualização de telefones de contato!',
				'texto'     => 'Telefones de contato atualizados com sucesso!',
				'tipo'      => 'success',
			);
		}
		else
		{
			$arrReturn['msgConfig']['pnotify'][] = array(
				'titulo'    => 'Erros foram encontrados!',
				'texto'     => 'Não foi possível atualizar os telefones de contato!',
				'tipo'      => 'error',
			);
		}

		return $arrReturn;
	}

	public function novosTelefones($T1, $T2, $T3, $Cliente_id)
	{

		$arrReturn = array('hasErrors' => false);

		if( !empty( $T1['numero'] ) && $T1['numero'] != '' && $T1['numero'] != NULL )
		{
			$r = $this->novo($T1, $Cliente_id);

			if( !$r['hasErrors'] )
			{
				$tel1 = Telefone::model()->findByPk($r['telId']);

				if( $tel1 != NULL )
				{
					$arrReturn['formConfig'][] = array(
						'fields' 				=> array(
							'ControllerAction' 	=> array(
								'value' 		=> 'update',
								'type' 			=> 'text',
								'input_bind'	=> 'ControllerTelefoneAction',
							),
							'id' 				=> array(
								'value' 		=> $tel1->id,
								'type' 			=> 'text',
								'input_bind'	=> 'telefone_id',
							),
							'numero'			=> array(
								'value' 		=> $tel1->numero,
								'type' 			=> 'text',
								'input_bind'	=> 'telefone_numero',
							),
							'ramal'				=> array(
								'value' 		=> $tel1->ramal,
								'type' 			=> 'text',
								'input_bind'	=> 'telefone_ramal',
							),
							'Tipo_Telefone_id'	=> array(
								'value' 		=> $tel1->Tipo_Telefone_id,
								'type' 			=> 'select',
								'input_bind'	=> 'TelefoneCliente_Tipo_Telefone_id',
							),
						),
					);
				}
			}
			else
			{
				$arrReturn['hasErrors'] = true;
			}
		}

		if( !empty( $T2['numero'] ) && $T2['numero'] != '' && $T2['numero'] != NULL )
		{	
			$r = $this->novo($T2, $Cliente_id);

			if( !$r['hasErrors'] )
			{
				$tel2 = Telefone::model()->findByPk($r['telId']);

				if( $tel2 != NULL )
				{
					$arrReturn['formConfig'][] = array(
						'fields' 				=> array(
							'telefone2id'		=> array(
								'value' 		=> $tel2->id,
								'type' 			=> 'text',
								'input_bind'	=> 'telefone2_id',
							),
							'numerot2'			=> array(
								'value' 		=> $tel2->numero,
								'type' 			=> 'text',
								'input_bind'	=> 'telefone2_numero',
							),
							'ramalt2'			=> array(
								'value' 		=> $tel2->ramal,
								'type' 			=> 'text',
								'input_bind'	=> 'telefone2_ramal',
							),
							'Tipo_Telefone2_id'	=> array(
								'value' 		=> $tel2->Tipo_Telefone_id,
								'type' 			=> 'select',
								'input_bind'	=> 'TelefoneCliente2_Tipo_Telefone_id',
							),
						),
					);
				}
			}
			else
			{
				$arrReturn['hasErrors'] = true;
			}
		}

		if( !empty( $T3['numero'] ) && $T3['numero'] != '' && $T3['numero'] != NULL )
		{
			$r = $this->novo($T3, $Cliente_id);

			if( !$r['hasErrors'] )
			{
				$tel3 = Telefone::model()->findByPk($r['telId']);

				if( $tel3 != NULL )
				{
					$arrReturn['formConfig'][] = array(
						'fields' 				=> array(
							'telefone3id'		=> array(
								'value' 		=> $tel3->id,
								'type' 			=> 'text',
								'input_bind'	=> 'telefone3_id',
							),
							'numerot3'			=> array(
								'value' 		=> $tel3->numero,
								'type' 			=> 'text',
								'input_bind'	=> 'telefone3_numero',
							),
							'ramalt3'			=> array(
								'value' 		=> $tel3->ramal,
								'type' 			=> 'text',
								'input_bind'	=> 'telefone3_ramal',
							),
							'Tipo_Telefone3_id'	=> array(
								'value' 		=> $tel3->Tipo_Telefone_id,
								'type' 			=> 'select',
								'input_bind'	=> 'TelefoneCliente3_Tipo_Telefone_id',
							),
						),
					);
				}
			}
			else
			{
				$arrReturn['hasErrors'] = true;
			}
		}

		if( !$arrReturn['hasErrors'] )
		{
			$arrReturn['msgConfig']['pnotify'][] = array(
	          'titulo'    	=> 'Ação realizada com sucesso!',
	          'texto'     	=> 'Os telefones foram cadastrados com êxito!',
	          'tipo'      	=> 'success',
	        );
		}
		else
		{
			$arrReturn['msgConfig']['pnotify'][] = array(
	          'titulo'    	=> 'Erros foram encontrados!',
	          'texto'     	=> 'Não foi possível salvar as informações. Contate o suporte!',
	          'tipo'      	=> 'error',
	        );
		}

		return $arrReturn;
	}

	public function atualizar($TelefoneCliente, $TelefoneClienteId){

		$telefoneCliente = Telefone::model()->findByPk($TelefoneClienteId);
		
		$telefoneCliente->attributes = $TelefoneCliente;
		
		if( $telefoneCliente->update() )
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function loadFormEdit($id){

		$telefone 	= Telefone::model()->findByPk($id);

	    return array(

	    	'fields' 				=> array(

		    	'id' 				=> array(
		    		'value' 		=> $telefone->id,
		    		'type' 			=> 'text',
		    		'input_bind'	=> 'telefone_id',
		    	),

		    	'numero'			=> array(
		    		'value' 		=> $telefone->discagem_direta_distancia.$telefone->numero,
		    		'type' 			=> 'text',
		    		'input_bind'	=> 'telefone_numero',
		    	),
		    	'ramal'				=> array(
		    		'value' 		=> $telefone->ramal,
		    		'type' 			=> 'text',
		    		'input_bind'	=> 'telefone_ramal',
		    	),
		    	'Tipo_Telefone_id'	=> array(
		    		'value' 		=> $telefone->Tipo_Telefone_id,
		    		'type' 			=> 'select',
		    		'input_bind'	=> 'TelefoneCliente_Tipo_Telefone_id',
		    	),
	    	),
	    	'form_params'			=> array(
	    		'id'				=> 'form-add-telefone',
	    		'action'			=> '/telefone/update/',
	    	),
	    	'modal_id'				=> 'modal_form_new_telefone'
	    );
	}

	public function novo($TelefoneCliente, $Cliente_id){

		$arrReturn = array('hasErrors' => true);

		$cliente 									= Cliente::model()->findByPk($Cliente_id);

		if( !empty( $TelefoneCliente['numero'] ) )
		{
			$telefone 								= new Telefone;
			$telefone->attributes 					= $TelefoneCliente;
			$telefone->data_cadastro 				= date('Y-m-d H:i:s');
			$telefone->data_cadastro_br 			= date('d/m/Y');
			$telefone->numero 						= str_replace(['(',')','-',' '], ['','','',''], $telefone->numero);
			$telefone->discagem_direta_distancia  	= substr($telefone->numero, 0,2);
			$telefone->numero  						= substr($telefone->numero, 2);

						
			if( $telefone->save() )
			{					
				$contatoHasTelefones 				= new ContatoHasTelefone;
				$contatoHasTelefones->Contato_id 	= $cliente->pessoa->Contato_id;
				$contatoHasTelefones->Telefone_id 	= $telefone->id;
				$contatoHasTelefones->save();

				$sigacLogCliente            		= new SigacLog;
                $sigacLogCliente->saveLog('Cadastro de telefone','Telefone', $telefone->id, date('Y-m-d H:i:s'),1, Yii::app()->session['usuario']->id, "Usuario ". Yii::app()->session['usuario']->username . " cadastrou o telefone ". $telefone->numero ." para o cliente " . $cliente->pessoa->nome, "127.0.0.1",null, Yii::app()->session->getSessionId() );

                $arrReturn['hasErrors'] 			= false;
				
				$arrReturn['msgConfig']['pnotify'][] = array(
	                'titulo'    	=> 'Ação realizada com sucesso!',
	                'texto'     	=> 'O telefone foi cadastrado, e faz parte dos contatos do cliente ' . strtoupper($cliente->pessoa->nome),
	                'tipo'      	=> 'success',
	            );

	            $arrReturn['telId']	= $telefone->id;
			}

			else
			{
	            $arrReturn['msgConfig']['pnotify'][] = array(
	                'titulo'    => 'Erros foram encontrados!',
	                'texto'     => 'Não foi possível salvar o telefone.',
	                'tipo'      => 'error',
	            );
			}
		}

		$statusDoCadastro                       = $cliente->clienteCadastroStatus();
        $arrReturn['statusDoCadastro']			= $statusDoCadastro['status'];

		return $arrReturn;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Telefone';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Tipo_Telefone_id', 'required'),
			array('habilitado, Tipo_Telefone_id, telefone_proprio', 'numerical', 'integerOnly'=>true),
			array('numero, ramal', 'length', 'max'=>45),
			array('discagem_direta_distancia', 'length', 'max'=>4),
			array('data_cadastro, data_cadastro_br', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, numero, habilitado, data_cadastro, data_cadastro_br, discagem_direta_distancia, ramal, Tipo_Telefone_id, telefone_proprio', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contatoHasTelefones' => array(self::HAS_MANY, 'ContatoHasTelefone', 'Telefone_id'),
			'tipoTelefone' => array(self::BELONGS_TO, 'TipoTelefone', 'Tipo_Telefone_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels(){
		
		return array(
			'id' => 'ID',
			'numero' => 'Telefone',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'data_cadastro_br' => 'Data / Hora Cadastro',
			'discagem_direta_distancia' => 'Discagem Direta Distancia',
			'ramal' => 'Ramal',
			'Tipo_Telefone_id' => 'Tipo Telefone',
			'telefone_proprio' => 'Telefone Proprio',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('numero',$this->numero,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('data_cadastro_br',$this->data_cadastro_br,true);
		$criteria->compare('discagem_direta_distancia',$this->discagem_direta_distancia,true);
		$criteria->compare('ramal',$this->ramal,true);
		$criteria->compare('Tipo_Telefone_id',$this->Tipo_Telefone_id);
		$criteria->compare('telefone_proprio',$this->telefone_proprio);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Telefone the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
