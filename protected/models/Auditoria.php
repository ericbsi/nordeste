<?php

class Auditoria extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Auditoria';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('data_criacao, Agente, Status_Auditoria_id', 'required'),
			array('Agente, Finalizado_por, Entidade_Relacionamento_id, Status_Auditoria_id', 'numerical', 'integerOnly'=>true),
			array('Entidade_Relacionamento', 'length', 'max'=>200),
			array('Observacao', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, data_criacao, Agente, Finalizado_por, Entidade_Relacionamento, Entidade_Relacionamento_id, Status_Auditoria_id, Observacao', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'statusAuditoria' 	=> array(self::BELONGS_TO, 'StatusAuditoria', 	'Status_Auditoria_id'),
			'agente' 			=> array(self::BELONGS_TO, 'Usuario',	 		'Agente'),
			'finalizadoPor'		=> array(self::BELONGS_TO, 'Usuario',	 		'Finalizado_por'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'data_criacao' => 'Data Criacao',
			'Agente' => 'Agente',
			'Entidade_Relacionamento' => 'Entidade Relacionamento',
			'Entidade_Relacionamento_id' => 'Entidade Relacionamento',
			'Status_Auditoria_id' => 'Status Auditoria',
			'Observacao' => 'Observacao',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('data_criacao',$this->data_criacao,true);
		$criteria->compare('Agente',$this->Agente);
		$criteria->compare('Entidade_Relacionamento',$this->Entidade_Relacionamento,true);
		$criteria->compare('Entidade_Relacionamento_id',$this->Entidade_Relacionamento_id);
		$criteria->compare('Status_Auditoria_id',$this->Status_Auditoria_id);
		$criteria->compare('Observacao',$this->Observacao,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	/*Métodos criados - Equipe Credshow*/

	public function novo($Auditoria, $ComprovanteFile)
	{
		$novaAuditoria 							= new Auditoria;
		$s3Client 								= new S3Client;
		$novaAuditoria->attributes 				= $Auditoria;
		$novaAuditoria->Status_Auditoria_id 	= 1;

		if( $novaAuditoria->save() )
		{
			if( $ComprovanteFile != NULL )
			{
				//Anexo::model()->novo($ComprovanteFile, 'Auditoria', $novaAuditoria->id, 'Anexo de auditoria');
				$s3Client->put( $ComprovanteFile, 'Auditoria', $novaAuditoria->id, 'Anexo de auditoria' );
			}

			return ['pntfyClass'=> 'success', 'msg' => 'Operação realizada com sucesso.'];

		}

		return ['pntfyClass'	=> 'error', 'msg' 	=> 'Não foi possível concluir sua solicitação.'];
	}

	public function finalizar($Auditoria)
	{
		$aud 						= Auditoria::model()->findByPk( $Auditoria['id'] );

		$aud->Finalizado_por 		= $Auditoria['Finalizado_por'];
		$aud->Status_Auditoria_id 	= 2;
		$aud->update();
	}

	public function anexar( $Auditoria_id, $ComprovanteFile, $ComprovanteFileDescricao )
	{
		Anexo::model()->novo($ComprovanteFile, 'Auditoria', $Auditoria_id, $ComprovanteFileDescricao);
	}

	/*Verifica se uma instância de uma entidade tem auditoria em aberto*/
	public function hasAuditoria($Entidade_Relacionamento, $Entidade_Relacionamento_id)
	{
		return Auditoria::model()->find( 
			'Entidade_Relacionamento 			= "' . $Entidade_Relacionamento . '"'.
			' AND Entidade_Relacionamento_id 	= '  . $Entidade_Relacionamento_id . 
			' AND Status_Auditoria_id 			= 1' 
		);
	}

	public function listar($Entidade_Relacionamento, $Status_Auditoria_id, $draw = NULL, $start = NULL)
	{
		$rows 					= [];
		$util 					= new Util;

		$criteria 				= new CDbCriteria;
		$criteria->compare('Entidade_Relacionamento', 	$Entidade_Relacionamento);
		$criteria->compare('Status_Auditoria_id', 		$Status_Auditoria_id);
		$criteria->order        = 'data_criacao DESC';

		$countTotal 			= count( Auditoria::model()->findAll( $criteria ) );

		$criteria->offset     	= $start;
      	$criteria->limit      	= 10;

      	foreach ( Auditoria::model()->findAll( $criteria ) as $auditoria )
      	{
      		$parcela 			= Parcela::model()->findByPk( $auditoria->Entidade_Relacionamento_id );

      		if( $parcela != NULL )
      		{
      			if( count( $parcela->getBaixas() ) > 0 )
      			{
      				$auditoria->Status_Auditoria_id = 2;
      				$auditoria->update();
      			}
      		}
      		
      		$rows[]				= [
      			'data_criacao'	=> $util->bd_date_to_view( substr( $auditoria->data_criacao, 0, 10) ),
      			'criado_por'	=> strtoupper( $auditoria->agente->nome_utilizador ),
      			'cod_proposta'	=> $parcela->titulo->proposta->codigo,
      			'cliente'		=> strtoupper( $parcela->titulo->proposta->analiseDeCredito->cliente->pessoa->nome ),
      			'cpf'			=> $parcela->titulo->proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero,
      			'status'		=> '<span class="' . $auditoria->statusAuditoria->cssClass . '">' . $auditoria->statusAuditoria->Status . '</span>',
      			'btn_more'		=> '<form method="POST" action="/auditoria/more/"><input type="hidden" name="AuditoriaId" value="'.$auditoria->id.'"><button type="submit" class="btn btn-info btn-squared btn-sm"><i class="fa fa-plus"></i></button></form>'
      		];


      	}

		return [
            "draw"              => $draw,
            "recordsTotal"      => count($rows),
            "recordsFiltered"   => $countTotal,
            "data"              => $rows
        ];
	}

	public function getObservacoes($AuditoriaId, $draw = NULL, $start = NULL)
	{
		$rows  							= [];
		$util 							= new Util;
		$countTotal 					= 0;

		$dialogo 						= Dialogo::model()->find('Entidade_Relacionamento = "Auditoria" AND Entidade_Relacionamento_id = ' . $AuditoriaId . ' AND habilitado ');

		if( $dialogo 					!= NULL )
		{	
			$mensagens 					= $dialogo->getMensagens();
			$countTotal 				= count( $mensagens );

			if( $countTotal > 0 )
			{
				foreach ( $mensagens as $mensagem )
				{
					$rows[]				= [
						'escrita_por' 	=> strtoupper( $mensagem->usuario->nome_utilizador 	),
						'data_criacao'	=> $util->bd_date_to_view($mensagem->data_criacao	),
						'conteudo'		=> strtoupper( $mensagem->conteudo 					)
					];
				}
			}

		}

		return [
            "draw"              => $draw,
            "recordsTotal"      => count($rows),
            "recordsFiltered"   => $countTotal,
            "data"              => $rows
        ];		
	}

	public function addObs( $AuditoriaId, $Observacao )
	{
		$dialogo 						= Dialogo::model()->find('Entidade_Relacionamento = "Auditoria" AND Entidade_Relacionamento_id = ' . $AuditoriaId . ' AND habilitado ');

		if( $dialogo == NULL )
		{
			$dialogo 					= Dialogo::model()->novo( 'Auditoria', 'Auditoria', $AuditoriaId, $Observacao['Usuario_id'], NULL );

			if( $dialogo != NULL )
			{
				Mensagem::model()->nova( $Observacao['conteudo'], $dialogo, Yii::app()->session['usuario']->id );
			}
		}
		else
		{
			Mensagem::model()->nova( $Observacao['conteudo'], $dialogo, Yii::app()->session['usuario']->id );
		}
	}

	public function getAnexos( $AuditoriaId, $draw )
	{
		$rows  							= [];
		$util 							= new Util;
		$countTotal 					= 0;
		$url 							= '';

		$criteria 						= new CDbCriteria;

		$criteria->compare('entidade_relacionamento',	"Auditoria");
		$criteria->compare('id_entidade_relacionamento',$AuditoriaId);

		$anexos 						= Anexo::model()->findAll($criteria);

		if( count( $anexos ) > 0 )
		{	
			$countTotal 				= count( $anexos );

			foreach( $anexos 			as $anexo )
			{	

				if($anexo->url != NULL){
					$url = $anexo->url;
				}
				else{
					$url = $anexo->relative_path;
				}

				$rows[] 				= [
					'descricao'			=> $anexo->descricao ,
					'visualizar'		=> '<a target="_blank" href="' . $url . '"' . 'class="btn btn-blue" style="padding:2px 5px!important; font-size:11px!important;" ><i class="clip-download-3"></i></a>',
				];
			}
		}

		return [
            "draw"              		=> $draw,
            "recordsTotal"      		=> count($rows),
            "recordsFiltered"   		=> $countTotal,
            "data"              		=> $rows
        ];
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Auditoria the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
