<?php

/**
 * This is the model class for table "Condicao_de_Pagamento".
 *
 * The followings are the available columns in table 'Condicao_de_Pagamento':
 * @property integer $id
 * @property string $nome
 * @property string $descricao
 * @property integer $habilitado
 * @property string $dataInicial
 * @property string $dataFinal
 *
 * The followings are the available model relations:
 * @property FormaDePagamentoHasCondicaoDePagamento[] $formaDePagamentoHasCondicaoDePagamentos
 */
class CondicaoDePagamento extends CActiveRecord
{	

	public function novo($CondicaoDePagamento){

		$cp 	= new CondicaoDePagamento;
		$cp->attributes 	=  $CondicaoDePagamento;
		$cp->save();
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Condicao_de_Pagamento';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nome, descricao, dataInicial, dataFinal', 'required'),
			array('habilitado', 'numerical', 'integerOnly'=>true),
			array('nome', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nome, descricao, habilitado, dataInicial, dataFinal', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'formaDePagamentoHasCondicaoDePagamentos' => array(self::HAS_MANY, 'FormaDePagamentoHasCondicaoDePagamento', 'Condicao_de_Pagamento_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nome' => 'Nome',
			'descricao' => 'Descricao',
			'habilitado' => 'Habilitado',
			'dataInicial' => 'Data Inicial',
			'dataFinal' => 'Data Final',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nome',$this->nome,true);
		$criteria->compare('descricao',$this->descricao,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('dataInicial',$this->dataInicial,true);
		$criteria->compare('dataFinal',$this->dataFinal,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CondicaoDePagamento the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
