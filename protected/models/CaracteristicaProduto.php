<?php

class CaracteristicaProduto extends CActiveRecord
{	

	public function listCaracteristicas( $draw ){

		$caracteristicas				= CaracteristicaProduto::model()->findAll('habilitado');
		$rows							= arraY();

		if( count( $caracteristicas ) > 0 )
		{
			foreach( $caracteristicas as $c )
			{	

				$btn                     = '<form method="post" action="'.Yii::app()->getBaseUrl(true).'/caracteristicaProduto/editar/">';
            	$btn                    .= '  <button class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Editar"><i class="fa fa-edit"></i></button>';
            	$btn                    .= '  <input type="hidden" name="caracteristicaId" value="'.$c->id.'">';
            	$btn                    .= '</form>';

				$row 					=  array(
					'descricao'			=> $c->descricao,
					'qtd_itens'			=> count( $c->listItensCaracteristica() ),
					'btn'				=> $btn
				);

				$rows[]					= $row;
				$btn 					= "";
			}
		}

		return (array(
            "draw"                      => $draw,
            "recordsTotal"              => 0,
            "recordsFiltered"           => 0,
            "data"                      => $rows
        ));
	}

	public function listItensCaracteristica(){

		return ValoresCaracteristica::model()->findAll( 'Caracteristica_id = ' . $this->id );
	}

	public function novo( $CaracteristicaProduto )
	{
		$aReturn = array(
            'hasErrors'     => 0,
            'msg'           => 'Característica cadastrada com Sucesso.',
            'pntfyClass'    => 'success'
        );

		if( !empty(  $CaracteristicaProduto['descricao'] ) )
        {

        	$caracteristica				= new CaracteristicaProduto;
        	$caracteristica->attributes = $CaracteristicaProduto;

        	if( !$caracteristica->save() )
        	{
        		$aReturn['hasErrors']	= 1;
				$aReturn['msg']			= 'Ocorreu um erro.';
        	}
        	
        	if( $aReturn['hasErrors'] )
			{
				$aReturn['pntfyClass'] 	= 'error';
			}
        }

		return $aReturn;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'CaracteristicaProduto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('descricao', 'required'),
			array('habilitado', 'numerical', 'integerOnly'=>true),
			array('descricao', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, descricao, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'valoresCaracteristicas' => array(self::HAS_MANY, 'ValoresCaracteristica', 'Caracteristica_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'descricao' => 'Descricao',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('descricao',$this->descricao,true);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CaracteristicaProduto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
