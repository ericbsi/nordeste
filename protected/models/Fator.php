<?php

class Fator extends CActiveRecord
{

    public function getValorRetido($valor)
    {
        return ($valor / 100) * $this->porcentagem_retencao;
    }

    public function getValor($valor, $seqParcela, $carencia, $tabelaId)
    {
        $aReturn = array();

        $fator = Fator::model()->find('habilitado AND parcela = ' . $seqParcela . ' AND carencia = ' . $carencia . ' AND Tabela_Cotacao_id = ' . $tabelaId);

        if ($fator != NULL)
        {

            $valorFinal = 0;

            if ($fator->fator == 0)
            {
                $aReturn[] = number_format($valor / $seqParcela, 2, ".", "");
                $aReturn[] = number_format($valor / $seqParcela, 2, ",", ".");
            } else
            {
                $aReturn[] = number_format($valor * $fator->fator, 2, ".", "");
                $aReturn[] = number_format($valor * $fator->fator, 2, ",", ".");
            }
        } else
        {
            return 0;
            // $aReturn[] = number_format(110, 2, ".", "");
            // $aReturn[] = number_format(110, 2, ",", ".");
        }

        return $aReturn;
    }

    public function novo($Fator, $TabelaId)
    {
        $aReturn = array(
            'hasErrors' => 0,
            'msg' => 'Tabela Cadastrada com Sucesso.',
            'pntfyClass' => 'success'
        );

        if ($Fator['fator'] != '' && $Fator['fator'] != NULL && !empty($Fator['carencia']) && !empty($Fator['parcela']) && !empty($TabelaId))
        {
            $tchc = TabelaCotacaoHasCarencia::model()->find('CarenciaId = ' . $Fator['carencia'] . ' AND TabelaCotacaoId = ' . $TabelaId . ' AND habilitado');

            if( $tchc == NULL )
            {
                $thc                        = new TabelaCotacaoHasCarencia;
                $thc->CarenciaId            = $Fator['carencia'];
                $thc->TabelaCotacaoId       = $TabelaId;
                $thc->habilitado            = 1;
                $thc->save();
            }

            $Fator['fator']     = trim($Fator['fator']);
            $Fator['carencia']  = trim($Fator['carencia']);
            $Fator['parcela']   = trim($Fator['parcela']);
            $Fator['fatorIOF']  = trim($Fator['fatorIOF']);

            $fatorExistente     = Fator::model()->find('Tabela_Cotacao_id = ' . $TabelaId . ' AND carencia = ' . $Fator['carencia'] . ' AND parcela = ' . $Fator['parcela']);

            if ($fatorExistente != NULL)
            {
                $aReturn['hasErrors'] = 1;
                $aReturn['msg'] = 'Condições já cadastradas para esta tabela.';
            }
            else
            {
                $fator = new Fator;
                $fator->attributes = $Fator;
                $fator->habilitado = 1;
                $fator->Tabela_Cotacao_id = $TabelaId;

                if (!$fator->save())
                {
                    $aReturn['hasErrors'] = 1;
                    $aReturn['msg'] = 'Ocorreu um erro. Por favor, tente novamente.';
                    $aReturn['err'] = $fator->getErrors();
                }
            }

            if ($aReturn['hasErrors'])
            {
                $aReturn['pntfyClass'] = 'error';
            }
        }

        return $aReturn;
    }

    public function tableName()
    {
        return 'Fator';
    }

    public function rules()
    {
        return array
        (
            array('carencia, parcela, habilitado, fator, Tabela_Cotacao_id'                                     , 'required'                                ),
            array('carencia, parcela, habilitado, Tabela_Cotacao_id'                                            , 'numerical'   , 'integerOnly' => true     ),
            array('fator, porcentagem_retencao, fatorIOF'                                                       , 'numerical'                               ),
            array('id, carencia, parcela, habilitado, fator, Tabela_Cotacao_id, porcentagem_retencao, fatorIOF' , 'safe'        , 'on'          => 'search' ),
        );
    }

    public function relations()
    {

        return array(
            'tabelaCotacao' => array(self::BELONGS_TO, 'TabelaCotacao', 'Tabela_Cotacao_id'),
        );
    }

    public function attributeLabels()
    {
        return array
        (
            'id'                    => 'ID'             ,
            'carencia'              => 'Carencia'       ,
            'parcela'               => 'Parcela'        ,
            'habilitado'            => 'Habilitado'     ,
            'fator'                 => 'Fator'          ,
            'Tabela_Cotacao_id'     => 'Tabela Cotacao' ,
            'porcentagem_retencao'  => 'Retenção'       ,
            'fatorIOF'              => 'Fator IOF'      ,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id'                     , $this->id                     );
        $criteria->compare('carencia'               , $this->carencia               );
        $criteria->compare('parcela'                , $this->parcela                );
        $criteria->compare('habilitado'             , $this->habilitado             );
        $criteria->compare('fator'                  , $this->fator                  );
        $criteria->compare('Tabela_Cotacao_id'      , $this->Tabela_Cotacao_id      );
        $criteria->compare('porcentagem_retencao'   , $this->porcentagem_retencao   );
        $criteria->compare('fatorIOF'               , $this->fatorIOF               );

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
