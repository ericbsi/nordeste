<?php

/**
 * Calculador do CET.
 *
 * O calculo abaixo esta de acordo com a formula da Resolucaoo CMN 3.517, de 2007
 * A formula e uma generalizacao e seu calculo por aproximacao deixa o programa
 * com entendimento mais simples
 *
 * Ha uma margem de erro insignificante no calculo.
 *
 */    
    
define('CET_MAXVALUE'   , 10000.00              );
define('CET_PRECISION'  , 0.00001               );
define('DAY'            , 1000 * 60 * 60 * 24   );
    
class Cet 
{

    /**
     * Calculo do custo efetivo total mensal.
     *
     * @param $fc0 FC0, valor financiado.
     * @param $fcj FCj, valor da parcela fixa
     * @param $n N, numero de parcelas mensais
     * @param $d0 D0 data do contrato (liberacao de recursos)
     * @param $dj0 DJ0 data da liberacao da primeira parcela
     * @return Custo Efetivo Total (CET)
     */
    
    public function mensal($fc0, $fcj, $n, $d0, $dj0) 
    {

        $cet = 0.0;

        while(true) 
        {

            $total = 0.0;

            for($j = 0; $j < $n; $j++) 
            {
                $total += $fcj / pow(1.0 + $cet, $j+1);
            }

            $cet += CET_PRECISION;

            if($cet >= CET_MAXVALUE) {
                return -1.0;
            }
            if($total - $fc0 <= 0) {
                break;
            }
            else {
                $cet *= $total / $fc0;
            }
        }
        return $cet * 100.0;
    }

    /**
     * Calculo do custo efetivo total anual.
     *
     * @param $fc0 FC0, valor financiado.
     * @param $fcj FCj, valor da parcela fixa
     * @param $n N, numero de parcelas mensais
     * @param $d0 D0 data do contrato (liberacao de recursos)
     * @param $dj0 DJ0 data da liberacao da primeira parcela
     * @return Custo Efetivo Total (CET)
     */
    public function anual($fc0, $fcj, $n, $d0, $dj0) 
    {
    /*    
        $hoje = date('Y-m-d');

        $cet = 0.0;

        while(true) 
        {

            $total = 0.0;

            for($j = 0; $j < $n; $j++) 
            {
                
                $dj = $dj0;
                
                if($j != 0) 
                {
                    $hoje = $dj0;
                    $hoje = date('Y-m-d', strtotime("+$j months", strtotime($hoje)));
//                    c.add(Calendar.MONTH, j);
                    $dj = $hoje;
                }
                
                $days =  / DAY;
                $total += $fcj / pow(1.0 + cet, days/365.0);
            }

            cet += CET_PRECISION;

            if(cet >= CET_MAXVALUE) {
                return -1.0;
            }
            if(total - fc0 <= 0) {
                break;
            }
            else {
                cet *= total / fc0;
            }
        }
        return $cet * 100.0;*/
    }

}
