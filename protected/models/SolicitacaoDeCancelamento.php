<?php

class SolicitacaoDeCancelamento extends CActiveRecord
{

   public function change($params)
   {
      if ($params['action'] == 'accept')
      {
         return $this->accept($params['solicitacaoId'], $params['usuarioId']);
      } else
      {
         return $this->decline($params['solicitacaoId'], $params['usuarioId'], $params['resposta_analista']);
      }
   }

   public function accept($solicitacaoId, $usuarioId)
   {
      $aReturn = array(
          'hasErrors' => 0,
          'msg' => 'Cancelamento efetivado com Sucesso.',
          'pntfyClass' => 'success'
      );
      
      $erroComplemento = "";

      $solicitacao = SolicitacaoDeCancelamento::model()->findByPk($solicitacaoId);

      if ($solicitacao != NULL)
      {
         $proposta = Proposta::model()->findByPk($solicitacao->Proposta_id);
         $proposta->Status_Proposta_id = 8;
          //         $proposta->habilitado = 0;

         if (!$proposta->update())
         {
            $aReturn['hasErrors'] = 1;
                
            ob_start();
            var_dump($proposta->getErrors());
            $erroComplemento = ob_get_clean();
                
         } else
         {

      

            $solicitacao->AceitePor = $usuarioId;
            $solicitacao->aceite = 1;
            $solicitacao->data_finalizacao = date('Y-m-d H:i:s');

            if (!$solicitacao->update())
            {
                
                ob_start();
                var_dump($solicitacao->getErrors());
                $erroComplemento = ob_get_clean();
                                
                $aReturn['hasErrors'] = 1;               
            }
            else
            {
                
                $pOmniConfig = PropostaOmniConfig::model()->find("habilitado AND Proposta = $proposta->id");
                
                if($pOmniConfig !== null)
                {
                    
                    $pOmniConfig->habilitado = 0;
                    
                    if(!$pOmniConfig->save())
                    {
                        ob_start();
                        var_dump($pOmniConfig->getErrors());
                        $erroComplemento = ob_get_clean();

                        $aReturn['hasErrors'] = 1;  
                        
                    }
                    
                }

                $tReceber = new Titulo;

                $tReceber->prefixo = '002';
                $tReceber->emissao = Date('Y-m-d');
                $tReceber->Proposta_id = $proposta->id;
                $tReceber->NaturezaTitulo_id = 41;

                if($tReceber->save()){
                  $nParcela = new Parcela;
                  $nParcela->seq = 1;
                  $nParcela->valor = $proposta->valorRepasse();
                  $nParcela->Titulo_id = $tReceber->id;
                  $nParcela->valor_atual = 0;

                if(!$nParcela->save()){
                  ob_start();
                  var_dump($nParcela->getErrors());
                  $erroComplemento = ob_get_clean();

                  $aReturn['hasErrors'] = 1;
                }
              }
                
            }
            
         }

         if ($aReturn['hasErrors'])
         {
            $aReturn['msg'] = "Não foi possível efetivar a Solicitação de Cancelamento. $erroComplemento";
            $aReturn['pntfyClass'] = 'error';
         }
      }

      return $aReturn;
   }

   public function decline($solicitacaoId, $usuarioId, $resposta)
   {
      $aReturn          = array(
          'hasErrors'   => 0,
          'msg'         => 'Solicitação rejeitada com Sucesso.',
          'pntfyClass'  => 'success'
      );

      $solicitacao = SolicitacaoDeCancelamento::model()->findByPk($solicitacaoId);

      if ($solicitacao != NULL)
      {
         $solicitacao->aceite = 0;
         $solicitacao->AceitePor = $usuarioId;
         $solicitacao->data_finalizacao = date('Y-m-d H:i:s');
         $solicitacao->motivo = $solicitacao->motivo . " | Motivo da Recusa: " . $resposta;
         if (!$solicitacao->update())
         {
            $aReturn['hasErrors'] = 1;
            $aReturn['msg'] = 'Não foi possível rejeitar a solicitação.';
            $aReturn['pntfyClass'] = 'error';
         }
      }

      return $aReturn;
   }

   public function listar($draw)
   {
      $criteria             = new CDbCriteria;
      $criteria->with       = array('proposta' => array('alias' => 'p'));
      $criteria->condition  = 'NOT t.aceite AND AceitePor IS NULL';
      $criteria->order      = 't.data_solicitacao DESC';

      $rows                 = array();
      $util                 = new Util;
      $solicitacoes         = SolicitacaoDeCancelamento::model()->findAll($criteria);

      foreach ($solicitacoes as $solicitacao)
      {
         $htmlMotivo        = '';
         $htmlTitulos       = '';
         $htmlComprovante   = '';
         $banco             = 'SEM BANCO';

         $cDSrelative_path  = '#';
         $cDSdescricao      = 'SEM COMPROVANTE';

         if ($solicitacao->getComprovanteDeSolicitacao() != NULL)
         {
            if( $solicitacao->getComprovanteDeSolicitacao()->url != NULL ){
              $cDSrelative_path = $solicitacao->getComprovanteDeSolicitacao()->url;
            }
            else{
              $cDSrelative_path = $solicitacao->getComprovanteDeSolicitacao()->relative_path;
            }

            $cDSdescricao     = $solicitacao->getComprovanteDeSolicitacao()->descricao;
         }

         /* Banco */
         if ($solicitacao->proposta->banco != NULL)
         {
            $banco = $solicitacao->proposta->banco->nome;
         }


         $sqlNossosNumeros = " SELECT cd.vencimento as vencimento, cd.vencimento_texto as vencimento_txt, cd.nosso_numero as nosso_numero, cd.seu_numero as seu_numero_bradesco, cd.nosso_numero_sant as nosso_numero_sant, cd.nome_do_sacado as cliente_nome, cd.numero_de_inscricao as cpf from Ranges_gerados as rg ";
         $sqlNossosNumeros .= " INNER JOIN Parcela as parc ON parc.id = rg.Parcela_id ";
         $sqlNossosNumeros .= " INNER JOIN Titulo as tit ON tit.id = parc.Titulo_id AND tit.NaturezaTitulo_id = 1";
         $sqlNossosNumeros .= " INNER JOIN Proposta as p ON tit.Proposta_id = p.id ";
         $sqlNossosNumeros .= " INNER JOIN Solicitacao_de_Cancelamento as sc ON sc.Proposta_id = p.id ";
         $sqlNossosNumeros .= " INNER JOIN Cnab_detalhe as cd ON cd.nosso_numero LIKE CONCAT('%',rg.label,'%') OR cd.nosso_numero_sant LIKE CONCAT('%',rg.label,'%') OR cd.seu_numero LIKE CONCAT('%',rg.label,'%') ";
         $sqlNossosNumeros .= " WHERE sc.id = " . $solicitacao->id;

         $titulos = Yii::app()->db->createCommand($sqlNossosNumeros)->queryAll();

         foreach ($titulos as $titulo)
         {            
            if( ($titulo['nosso_numero'] == NULL OR empty($titulo['nosso_numero'])) AND ( $titulo['nosso_numero_sant'] == NULL OR empty($titulo['nosso_numero_sant']) ) )
            {
                $htmlTitulos .= '<tr><td>' . $titulo['seu_numero_bradesco'] . '</td>';
                $htmlTitulos .= '<td>' . substr($titulo['vencimento_txt'], 0, 2) .'/'.substr($titulo['vencimento_txt'], 2, 2).'/20'.substr($titulo['vencimento_txt'], 4, 2).'</td><tr>';
            }
            else if( ($titulo['nosso_numero'] == NULL OR empty($titulo['nosso_numero'])) AND ( $titulo['seu_numero_bradesco'] == NULL OR empty($titulo['seu_numero_bradesco']) ) )
            {
                $htmlTitulos .= '<tr><td>' . substr($titulo['nosso_numero_sant'], 0, 8) . '</td>';
                $htmlTitulos .= '<td>' . $util->bd_date_to_view($titulo['vencimento']) . '</td><tr>';
            }
            else if( ($titulo['nosso_numero_sant'] == NULL OR empty($titulo['nosso_numero_sant'])) AND ( $titulo['seu_numero_bradesco'] == NULL OR empty($titulo['seu_numero_bradesco']) ) )
            {
                $htmlTitulos .= '<tr><td>' . $titulo['nosso_numero'] . '</td>';
                $htmlTitulos .= '<td>' . $util->bd_date_to_view($titulo['vencimento']) . '</td><tr>';
            }
            
            /*
            if ($titulo['nosso_numero'] == NULL OR empty($titulo['nosso_numero']))
            {
                $htmlTitulos .= '<tr><td>' . substr($titulo['nosso_numero_sant'], 0, 8) . '</td>';
            }

            else
            {
                
            }
            */
         }

         $htmlMotivo = '<tr><td>' . strtoupper($solicitacao->motivo) . '</tr></td>';
         $htmlComprovante = '<tr><td>' . '<a target="_blank" href="' . $cDSrelative_path . '">' . $cDSdescricao . '</a>' . '</tr></td>';
         
         $dataProposta = $util->bd_date_to_view(substr($solicitacao->proposta->data_cadastro, 0, 10));
         $financeira = ''; 
         
         if($solicitacao->proposta->Financeira_id == 10){
             $financeira = 'FIDC CSC';
         }
         if($solicitacao->proposta->Financeira_id == 11){
             $financeira = 'FIDC LECCA';
         }
         if($solicitacao->proposta->Financeira_id == 7){
             $financeira = 'SEMEAR';
         }
         if($solicitacao->proposta->Financeira_id == 5 || $solicitacao->proposta->Financeira_id == 6){
             if($solicitacao->proposta->hasOmniConfig()){
                 $financeira = 'OMNI';
             }else{
                 $financeira = 'CREDSHOW';
             }
         }
        
         $accept_decline = '<a href="#confirm-modal" data-toggle="modal" data-soli-id="' . $solicitacao->id . '" style="padding:1px 4px; font-size:12px;" class="btn btn-green btn-accept-soli" href="#"><i class="fa fa-thumbs-o-up"></i></a> <a href="#confirm-modal" data-toggle="modal" data-soli-id="' . $solicitacao->id . '" style="padding:1px 4px; font-size:12px;" class="btn btn-red btn-deny-soli" href="#"><i class="fa fa-thumbs-o-down"></i></a>';
         if(Yii::app()->session['usuario']->role == 'analista_de_credito'){
             $accept_decline = '';
         }
         $itemBordero = ItemDoBordero::model()->find('habilitado AND Proposta_id = ' . $solicitacao->proposta->id);
         if($itemBordero != null){
             $bordero = Bordero::model()->findByPk($itemBordero->Bordero);
             if($bordero->habilitado == 1 && $bordero->Status != 6){
                 $loteHB = LoteHasBordero::model()->find('habilitado AND Bordero_id = ' . $bordero->id);
                 if($loteHB != null){
                     $info = "Lote: " . $loteHB->Lote_id . " <br> Borderô: " . $loteHB->Bordero_id;
                 }else{
                     $info = "Em Borderô ($bordero->id)";
                 }
             }else{
                 $info = "Não paga";
             }
         }else{
             $info = "Não paga";
         }
         $row = array(
             'btn_more' => '',
             'financeira' => $financeira,
             'proposta' => $solicitacao->proposta->codigo,
             'dataProposta' => $dataProposta,
             'cliente' => strtoupper($solicitacao->proposta->analiseDeCredito->cliente->pessoa->nome),
             'cpf_cliente' => $solicitacao->proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero,
             'solicitante' => strtoupper($solicitacao->solicitante->nome_utilizador),
             'filial' => strtoupper($solicitacao->proposta->analiseDeCredito->filial->nome_fantasia . ' - ' . $solicitacao->proposta->analiseDeCredito->filial->getEndereco()->cidade . ' / ' . $solicitacao->proposta->analiseDeCredito->filial->getEndereco()->uf),
             'parcelamento' => $solicitacao->proposta->qtd_parcelas . ' x ' . number_format($solicitacao->proposta->getValorParcela(), 2, ',', '.'),
             'valorTotal' => number_format($solicitacao->proposta->getValorFinal(), 2, ',', '.'),
             'banco' => $banco,
             'btn' => $accept_decline,
             'info' => $info,
             'moreDetails' => [
                 '<table class="table table-striped table-bordered table-hover table-full-width">'
                 . '<thead>'
                 . '<tr>'
                 . '<th>Motivo</th>'
                 . '</tr>'
                 . '</thead>'
                 . '<tbody>' . $htmlMotivo . '</tbody>'
                 . '</table>' .
                 '<table class="table table-striped table-bordered table-hover table-full-width">'
                 . '<thead>'
                 . '<tr>'
                 . '<th>Comprovante</th>'
                 . '</tr>'
                 . '</thead>'
                 . '<tbody>' . $htmlComprovante . '</tbody>'
                 . '</table>' .
                 '<table class="table table-striped table-bordered table-hover table-full-width">'
                 . '<thead>'
                 . '<tr>'
                 . '<th>Nosso Número</th>'
                 . '<th>Vencimento</th>'
                 . '</tr>'
                 . '</thead>'
                 . '<tbody>' . $htmlTitulos . '</tbody>'
                 . '</table>',
             ]
         );

         $rows[] = $row;
      }

      return [
          'draw' => $draw,
          "recordsTotal" => count($rows),
          "recordsFiltered" => count($rows),
          "data" => $rows
      ];
   }

   public function getComprovanteDeSolicitacao()
   {
      return Anexo::model()->find('entidade_relacionamento = "SCP" AND id_entidade_relacionamento = ' . $this->id);
   }

   public function historico($draw, $search, $start, $data_de, $data_ate, $mostraTitulo, $filiais)
   {

      $criteria = new CDbCriteria;

      $util = new Util;

      $rows = array();

      $filiaisFiltradas = '';

      $countLoop = 0;
      $qtdReg = 0;

      if (!empty($data_de))
      {
         $criteria->compare("data_solicitacao", ">= " . $data_de . " 00:00:00");
      }

      if (!empty($data_ate))
      {
         $criteria->compare("data_solicitacao", "<= " . $data_ate . " 23:59:59");
      }

      if (is_array($filiais))
      {
         $filiaisFiltradas = join(',', $filiais);
      }

      /*        var_dump($data_ate);

        var_dump($data_ate); */


      $recordsTotal = count(SolicitacaoDeCancelamento::model()->findAll());

      $criteria->join .= "INNER JOIN Proposta               AS p    ON                    p.id          =   t.Proposta_id            ";
      $criteria->join .= "INNER JOIN Analise_de_Credito     AS ac   ON                   ac.id          =   p.Analise_de_Credito_id  ";
      $criteria->join .= "INNER JOIN Cliente                AS C    ON                    C.id          =  ac.Cliente_id             ";
      $criteria->join .= "INNER JOIN Pessoa_has_Documento   AS PhD  ON                  PhD.Pessoa_id   =   C.Pessoa_id              ";
      $criteria->join .= "INNER JOIN Pessoa                 AS Pe   ON                   Pe.id          =   C.Pessoa_id              ";
      $criteria->join .= "INNER JOIN Documento              AS D    ON D.habilitado AND   D.id          = PhD.Documento_id           AND Tipo_documento_id = 1";
         
      if(!empty($data_de) || !empty($data_ate))
      {
         $criteria->condition .= " AND ";
      }
      else
      {
         $criteria->condition .= "";
      }

      if (!empty($filiaisFiltradas))
      {
         
         $criteria->condition .= " ac.Filial_id            IN (" . $filiaisFiltradas . ") AND ";
      }

      $criteria->condition .= "t.AceitePor IS NOT NULL AND t.habilitado ";
      
      if(!empty($search['value']))
      {
         $criteria->condition   .=  " AND   (   (   (p.codigo LIKE '%" . $search['value'] . "%')  AND   "
                                .   "               (Pe.nome  LIKE '%" . $search['value'] . "%')  AND   "
                                .   "               (D.numero LIKE '%" . $search['value'] . "%')        "
                                .   "           )                                                 OR    "
                                .   "           (                                                       "
                                .   "              ((p.codigo LIKE '%" . $search['value'] . "%')  AND   "
                                .   "               (Pe.nome  LIKE '%" . $search['value'] . "%')) OR    "
                                .   "               (D.numero LIKE '%" . $search['value'] . "%')        "
                                .   "           )                                                 OR    "
                                .   "           (                                                       "
                                .   "              ((p.codigo LIKE '%" . $search['value'] . "%')  AND   "
                                .   "               (D.numero LIKE '%" . $search['value'] . "%')) OR    "
                                .   "               (Pe.nome  LIKE '%" . $search['value'] . "%')        "
                                .   "           )                                                 OR    "
                                .   "           (                                                       "
                                .   "              ((Pe.nome  LIKE '%" . $search['value'] . "%')  AND   "
                                .   "               (D.numero LIKE '%" . $search['value'] . "%')) OR    "
                                .   "               (p.codigo LIKE '%" . $search['value'] . "%')        "
                                .   "           )                                                 OR    "
                                .   "           (                                                       "
                                .   "               (p.codigo LIKE '%" . $search['value'] . "%')  OR    "
                                .   "               (Pe.nome  LIKE '%" . $search['value'] . "%')  OR    "
                                .   "               (D.numero LIKE '%" . $search['value'] . "%')        "
                                .   "           )"
                                .   "       )";
      }

      $criteria->order = "t.data_solicitacao DESC";

      $recordsFiltered = count(SolicitacaoDeCancelamento::model()->findAll($criteria));

      if ($mostraTitulo == "A")
      {
         $criteria->offset = $start;
         $criteria->limit = 10;
      }

      $solicitacoes = SolicitacaoDeCancelamento::model()->findAll($criteria);

      foreach ($solicitacoes as $solicitacao)
      {

         $htmlTitulos = '';
         $moreDetails = '';
         $util = new Util;
         $btnStatus = '';
         $finalizadoPor = Usuario::model()->findByPk($solicitacao->AceitePor);

         if ($solicitacao->AceitePor != NULL && $solicitacao->aceite == 1)
         {
            $btnStatus = '<span class="badge badge-danger">Cancelado</span>';
         } else if ($solicitacao->AceitePor != NULL && $solicitacao->aceite == 0)
         {
            $btnStatus = '<span class="badge badge-warning">Cancelamento recusado</span>';
         }


         $sqlNossosNumeros = " SELECT cd.nosso_numero as nosso_numero, cd.nome_do_sacado as cliente_nome, cd.numero_de_inscricao as cpf from "
                 . "Ranges_gerados as rg ";
         $sqlNossosNumeros .= " INNER JOIN Parcela as parc ON parc.id = rg.Parcela_id ";
         $sqlNossosNumeros .= " INNER JOIN Titulo as tit ON tit.id = parc.Titulo_id ";
         $sqlNossosNumeros .= " INNER JOIN Proposta as p ON tit.Proposta_id = p.id ";
         $sqlNossosNumeros .= " INNER JOIN Solicitacao_de_Cancelamento as sc ON sc.Proposta_id = p.id ";
         $sqlNossosNumeros .= " INNER JOIN Cnab_detalhe as cd ON cd.nosso_numero LIKE CONCAT('%',rg.label,'%') ";
         $sqlNossosNumeros .= " INNER JOIN Analise_de_Credito as ac ON ac.id = p.Analise_de_Credito_id ";
         $sqlNossosNumeros .= " WHERE sc.id = " . $solicitacao->id;

         $titulos = Yii::app()->db->createCommand($sqlNossosNumeros)->queryAll();
         
         $htmlMotivo  = '<table class="table table-striped table-bordered table-hover table-full-width">';
         $htmlMotivo .= ' <thead>';
         $htmlMotivo .= '  <tr>';
         $htmlMotivo .= '   <th>Motivo</th>';
         $htmlMotivo .= '  </tr>';
         $htmlMotivo .= ' </thead>';
         $htmlMotivo .= ' <tbody>';
         $htmlMotivo .= '  <tr>';
         $htmlMotivo .= '   <th>' . $solicitacao->motivo . '</th>';
         $htmlMotivo .= '  </tr>';
         $htmlMotivo .= ' </tbody>';
         $htmlMotivo .= '</table>';

         foreach ($titulos as $titulo)
         {
            $htmlTitulos .= '<tr><td>' . $titulo['nosso_numero'] . '</td>';
            $htmlTitulos .= '<td>' . $titulo['cliente_nome'] . '</td>';
            $htmlTitulos .= '<td>' . $titulo['cpf'] . '</td><tr>';
         }
            
         $moreDetails .= $htmlMotivo;

         if (!empty($htmlTitulos))
         {

            if ($mostraTitulo == "N")
            {
               continue;
            }

            $moreDetails .=
                    '<table class="table table-striped table-bordered table-hover table-full-width">'
                    . '<thead>'
                    . '<tr>'
                    . '<th>Titulo</th>'
                    . '<th>Cliente</th>'
                    . '<th>CPF</th>'
                    . '</tr>'
                    . '</thead>'
                    . '<tbody>' . $htmlTitulos . '</tbody>'
                    . '</table>';
         } else if ($mostraTitulo == "S")
         {
            continue;
         }

         if ($mostraTitulo <> "A")
         {

            $countLoop++;

            if ($countLoop < $start)
            {
               continue;
            }

            $qtdReg++;

            if ($qtdReg > 10)
            {
               continue;
            }
         }

         if ($solicitacao->data_finalizacao !== null)
         {
            $data_finalizacao = $util->bd_date_to_view(substr($solicitacao->data_finalizacao, 0, 10));
         } else
         {
            $data_finalizacao = '';
         }

         $emissao = $util->bd_date_to_view(substr($solicitacao->data_solicitacao, 0, 10));

         $dtVenda = $util->bd_date_to_view(substr($solicitacao->proposta->data_cadastro, 0, 10));

         $nomeFilial = strtoupper($solicitacao->proposta->analiseDeCredito->filial->nome_fantasia . ' - ' . $solicitacao->proposta->analiseDeCredito->filial->getEndereco()->cidade . ' / ' . $solicitacao->proposta->analiseDeCredito->filial->getEndereco()->uf);

         $parcelamento = $solicitacao->proposta->qtd_parcelas . ' x ' . number_format($solicitacao->proposta->getValorParcela(), 2, ',', '.');


         $row = array(
             'btn_more' => '',
             'proposta' => $solicitacao->proposta->codigo,
             'cpf' => $solicitacao->proposta->analiseDeCredito->cliente->pessoa->getCPF()->numero,
             'dtVenda' => $dtVenda,
             'emissao' => $emissao,
             'dtAceite' => $data_finalizacao,
             'solicitante' => strtoupper($solicitacao->solicitante->nome_utilizador),
             'filial' => $nomeFilial,
             'finalizadoPor' => strtoupper($finalizadoPor->nome_utilizador),
             'parcelamento' => $parcelamento,
             'valorTotal' => number_format($solicitacao->proposta->valor_final, 2, ',', '.'),
             'status' => $btnStatus,
             'moreDetails' => array
                 (
                 $moreDetails,
             )
         );

         $rows[] = $row;
      }

      if ($mostraTitulo <> "A")
      {
         $recordsFiltered = $countLoop;
      }

      return (array(
          'draw' => $draw,
          "recordsTotal" => $recordsTotal,
          "recordsFiltered" => $recordsFiltered, //count($solicitacoes) ,
          "data" => $rows
      ));
   }

   public function persist($Solicitacao, $ComprovanteFile)
   {
      $util = new Util;

      $aReturn = array(
          'hasErrors' => 0,
          'msg' => 'Solicitação registrada com Sucesso.',
          'pntfyClass' => 'success'
      );

      if (!$this->solicitacaoExistente($Solicitacao['Proposta_id']))
      {
         $soli = new SolicitacaoDeCancelamento;
         $soli->attributes = $Solicitacao;
         $soli->data_solicitacao = date('Y-m-d H:i:s');
         $soli->aceite = 0;
         $soli->habilitado = 1;

         if (!$soli->save())
         {
            $aReturn['hasErrors'] = 1;
            $aReturn['msg'] = 'Ocorreu um erro.';
            $aReturn['errors'] = $cobranca->getErrors();
         } else
         {
            $proposta = Proposta::model()->findByPk($Solicitacao['Proposta_id']);

            /*Anexo::model()->novo($ComprovanteFile, 'SCP', $soli->id, 'Comprovante de Solicitação de Cancelamento | Proposta ' . $proposta->codigo);*/

            $S3Client = new S3Client;
            $S3Client->put( $ComprovanteFile, 'SCP', $soli->id, 'Comprovante de Solicitação de Cancelamento | Proposta '.$proposta->codigo);
         }

         if ($aReturn['hasErrors'])
         {
            $aReturn['pntfyClass'] = 'error';
         }
      } else
      {
         $aReturn['hasErrors'] = 1;
         $aReturn['msg'] = 'Esta proposta já possui uma solicitação de cancelamento.';
         $aReturn['pntfyClass'] = 'error';
      }

      return $aReturn;
   }

   public function solicitacaoExistente($Proposta_id)
   {
      $solicitacao      = SolicitacaoDeCancelamento::model()->find('Proposta_id = ' . $Proposta_id . ' AND habilitado AND AceitePor IS NULL');

      if ($solicitacao != NULL)
      {
         return $solicitacao;
      } else
      {
         return null;
      }
   }

   public function existeCancelamento($Proposta_id)
   {
      $solicitacao = SolicitacaoDeCancelamento::model()->find('Proposta_id = ' . $Proposta_id . ' AND habilitado AND aceite');

      if ($solicitacao != NULL)
      {
         return true;
      } else
      {
         return false;
      }
   }

   /**
    * @return string the associated database table name
    */
   public function tableName()
   {
      return 'Solicitacao_de_Cancelamento';
   }

   /**
    * @return array validation rules for model attributes.
    */
   public function rules()
   {
      return array(
          array('Proposta_id, Solicitante, aceite', 'required'),
          array('Proposta_id, habilitado, AceitePor, Solicitante', 'numerical', 'integerOnly' => true),
          array('motivo, data_solicitacao', 'safe'),
          array('id, motivo, data_solicitacao, Proposta_id, Solicitante, habilitado', 'safe', 'on' => 'search'),
      );
   }

   public function relations()
   {
      return array(
          'proposta'      =>  array( self::BELONGS_TO,  'Proposta', 'Proposta_id'),
          'solicitante'   =>  array( self::BELONGS_TO,  'Usuario',  'Solicitante'),
          'aceite_por'    =>  array( self::BELONGS_TO,  'Usuario',  'AceitePor' ),
      );
   }

   public function attributeLabels()
   {
      return array(
          'id' => 'ID',
          'motivo' => 'Motivo',
          'Proposta_id' => 'Proposta',
          'habilitado' => 'Habilitado',
          'Solicitante' => 'Solicitante',
          'aceite' => 'Pedido Aceito',
          'aceite_por' => 'Pedido Aceito Por',
          'data_solicitacao' => 'Data da solicitação',
      );
   }

   public function search()
   {
      $criteria = new CDbCriteria;

      $criteria->compare('id', $this->id);
      $criteria->compare('motivo', $this->motivo, true);
      $criteria->compare('Proposta_id', $this->Proposta_id);
      $criteria->compare('habilitado', $this->habilitado);
      $criteria->compare('aceite', $this->aceite);

      return new CActiveDataProvider($this, array(
          'criteria' => $criteria,
      ));
   }

   /**
    * Returns the static model of the specified AR class.
    * Please note that you should have this exact method in all your CActiveRecord descendants!
    * @param string $className active record class name.
    * @return SolicitacaoDeCancelamento the static model class
    */
   public static function model($className = __CLASS__)
   {
      return parent::model($className);
   }

}
