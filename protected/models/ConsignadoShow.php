<?php

class ConsignadoShow extends CActiveRecord
{	

	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ConsignadoShow';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('StatusConsignado', 'required'),
			array('qtd_parcelas, solicitante, StatusConsignado', 'numerical', 'integerOnly'=>true),
			array('limiteConsignavel, valorParcelas', 'numerical'),
			array('nomeCliente', 'length', 'max'=>200),
			array('cpf', 'length', 'max'=>11),
			array('numero_do_beneficio, telefone', 'length', 'max'=>45),
			array('data_nascimento, linkExterno', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nomeCliente, cpf, data_nascimento, numero_do_beneficio, telefone, linkExterno, limiteConsignavel, qtd_parcelas, valorParcelas, StatusConsignado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'statusConsignado' => array(self::BELONGS_TO, 'StatusConsignado', 'StatusConsignado'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nomeCliente' => 'Nome Cliente',
			'cpf' => 'Cpf',
			'data_nascimento' => 'Data Nascimento',
			'numero_do_beneficio' => 'Numero Do Beneficio',
			'telefone' => 'Telefone',
			'linkExterno' => 'Link Externo',
			'limiteConsignavel' => 'Limite Consignavel',
			'qtd_parcelas' => 'Qtd Parcelas',
			'valorParcelas' => 'Valor Parcelas',
			'StatusConsignado' => 'Status Consignado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nomeCliente',$this->nomeCliente,true);
		$criteria->compare('cpf',$this->cpf,true);
		$criteria->compare('data_nascimento',$this->data_nascimento,true);
		$criteria->compare('numero_do_beneficio',$this->numero_do_beneficio,true);
		$criteria->compare('telefone',$this->telefone,true);
		$criteria->compare('linkExterno',$this->linkExterno,true);
		$criteria->compare('limiteConsignavel',$this->limiteConsignavel);
		$criteria->compare('qtd_parcelas',$this->qtd_parcelas);
		$criteria->compare('valorParcelas',$this->valorParcelas);
		$criteria->compare('StatusConsignado',$this->StatusConsignado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


	public function novo($ConsignadoShow)
	{
		$retorno 							= ['hasErrors' => 0];
		$util 								= new Util;

		$novoConsignado 					= new ConsignadoShow;
		$novoConsignado->attributes 		= $ConsignadoShow;
		
		$novoConsignado->data_nascimento	= $util->view_date_to_bd( $ConsignadoShow['data_nascimento'] );

		if( !$novoConsignado->save() )
		{
			$retorno['hasErrors'] 			= 1;
		}
		
		else
		{	
			$mail 				= Yii::app()->Smtpmail;

			$mensagem 			= '<!DOCTYPE html>
									<html>
										<head>
											<title></title>
											<meta charset="utf-8" />
										</head>
										<body>

											<h3>Nova solicitação</h3>
											<p>Nome:'.$novoConsignado->nomeCliente.'</p>
											<p>Cpf:'.$novoConsignado->cpf.'</p>
											<p>Nascimento:'.$ConsignadoShow['data_nascimento'].'</p>
											<p>Numero do Beneficio:'.$novoConsignado->numero_do_beneficio.'</p>
											<p>Telefone para contato:'.$novoConsignado->telefone.'</p>

										</body>
									</html>';

			$mail->SetFrom('consignado@credshow.com.br', 'De - ConsignadoShow Credshow');
		    $mail->SMTPSecure 	= 'ssl';
		    $mail->SMTPAuth 	= true;
		    $mail->Subject    	= "Solicitação";
		    $mail->MsgHTML($mensagem);
		    $mail->AddAddress("eu@ericvinicius.com.br", "");
		    //atendimento@mgcredito.com.br

		    $mail->Send();
		}

		return $retorno;
	}
}