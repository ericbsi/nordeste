<?php

/**
 * This is the model class for table "Situacao_cadastral".
 *
 * The followings are the available columns in table 'Situacao_cadastral':
 * @property integer $id
 * @property string $situacao
 * @property string $detalhe
 *
 * The followings are the available model relations:
 * @property DocumentoHasSituacaoCadastral[] $documentoHasSituacaoCadastrals
 */
class SituacaoCadastral extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Situacao_cadastral';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('situacao', 'length', 'max'=>45),
			array('detalhe', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, situacao, detalhe', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'documentoHasSituacaoCadastrals' => array(self::HAS_MANY, 'DocumentoHasSituacaoCadastral', 'Situacao_cadastral_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'situacao' => 'Situacao',
			'detalhe' => 'Detalhe',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('situacao',$this->situacao,true);
		$criteria->compare('detalhe',$this->detalhe,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SituacaoCadastral the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
