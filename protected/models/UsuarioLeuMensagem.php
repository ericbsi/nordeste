<?php

class UsuarioLeuMensagem extends CActiveRecord
{

	public function tableName()
	{
		return 'Usuario_leu_Mensagem';
	}


	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Usuario_id, Mensagem_id, data_ultima_leitura', 'required'),
			array('Usuario_id, Mensagem_id, habilitado', 'numerical', 'integerOnly'=>true),
			array('data_ultima_leitura', 'safe'),
			array('id, Usuario_id, Mensagem_id, habilitado, data_ultima_leitura', 'safe', 'on'=>'search'),
		);
	}


	public function novo(){

	}


	public function relations()
	{
		return array(
			'usuario' => array(self::BELONGS_TO, 'Usuario', 'Usuario_id'),
			'mensagem' => array(self::BELONGS_TO, 'Mensagem', 'Mensagem_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'	 				=> 'ID',
			'habilitado' 			=> 'Habilitado',
			'Usuario_id' 			=> 'Usuario',
			'Mensagem_id' 			=> 'Mensagem',
			'data_ultima_leitura' 	=> 'Ultima leitura',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{


		$criteria = new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Mensagem_id',$this->Mensagem_id);
		$criteria->compare('habilitado',$this->habilitado);				
		$criteria->compare('Usuario_id',$this->Usuario_id);
		$criteria->compare('data_ultima_leitura',$this->data_ultima_leitura);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mensagem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
