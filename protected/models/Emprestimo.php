<?php

/**
 * This is the model class for table "Emprestimo".
 *
 * The followings are the available columns in table 'Emprestimo':
 * @property integer $id
 * @property integer $habilitado
 * @property string $data_cadastro
 * @property integer $Proposta_id
 * @property integer $Filial_id
 *
 * The followings are the available model relations:
 * @property Proposta $proposta
 * @property Filial $filial
 */
class Emprestimo extends CActiveRecord
{
    /**
    * @return string the associated database table name
    */
    public function tableName()
    {
        return 'Emprestimo';
    }

   /**
    * @return array validation rules for model attributes.
    */
   public function rules()
   {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
                array('data_cadastro, Proposta_id, Filial_id', 'required'),
                array('habilitado, Proposta_id, Filial_id', 'numerical', 'integerOnly'=>true),
                // The following rule is used by search().
                // @todo Please remove those attributes that should not be searched.
                array('id, habilitado, data_cadastro, Proposta_id, Filial_id', 'safe', 'on'=>'search'),
        );
   }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
                'proposta' => array(self::BELONGS_TO, 'Proposta', 'Proposta_id'),
                'filial' => array(self::BELONGS_TO, 'Filial', 'Filial_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
                'id' => 'ID',
                'habilitado' => 'Habilitado',
                'data_cadastro' => 'Data Cadastro',
                'Proposta_id' => 'Proposta',
                'Filial_id' => 'Filial',
        );
    }

    /**
    * Retrieves a list of models based on the current search/filter conditions.
    *
    * Typical usecase:
    * - Initialize the model fields with values from filter form.
    * - Execute this method to get CActiveDataProvider instance which will filter
    * models according to data in model fields.
    * - Pass data provider to CGridView, CListView or any similar widget.
    *
    * @return CActiveDataProvider the data provider that can return the models
    * based on the search/filter conditions.
    */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id'             , $this->id                     );
        $criteria->compare('habilitado'     , $this->habilitado             );
        $criteria->compare('data_cadastro'  , $this->data_cadastro  , true  );
        $criteria->compare('Proposta_id'    , $this->Proposta_id            );
        $criteria->compare('Filial_id'      , $this->Filial_id              );

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
        ));
    }

    /**
    * Returns the static model of the specified AR class.
    * Please note that you should have this exact method in all your CActiveRecord descendants!
    * @param string $className active record class name.
    * @return Emprestimo the static model class
    */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    public function getAnexoContrato()
    {
        
        $anexos = Anexo::model()->findAll("habilitado AND entidade_relacionamento = 'Emprestimo' And id_entidade_relacionamento = $this->id");
        
        return $anexos;

    }

    public function gerarTituloPagar()
    {
        
        $codigoNatureza = "2999999";
        
        $transaction = Yii::app()->db->beginTransaction();
        
        $proposta = Proposta::model()->find("habilitado AND id = $this->Proposta_id");
        
        $aRetorno = [
                        'title'     => "Sucesso"                        ,
                        'text'      => "Títulos gerados com sucesso"    ,
                        'title'     => "success"                        ,
                        'hasErrors' => false
                    ];
        
        if($proposta !== null)
        {
            $fornecedor = Fornecedor::model()->find("habilitado AND Pessoa_id = " . $proposta->analiseDeCredito->cliente->Pessoa_id);
            
            if($fornecedor == null)
            {
                
                $fornecedor = new Fornecedor;
                
                $fornecedor->habilitado         = 1                                                 ;
                $fornecedor->TipoFornecedor_id  = 3                                                 ;
                $fornecedor->Pessoa_id          = $proposta->analiseDeCredito->cliente->Pessoa_id   ;
                
                if(!$fornecedor->save())
                {
                    
                    ob_start();
                    var_dump($fornecedor->getErrors());
                    $resultado = ob_get_clean();
        
                    $aRetorno = [
                                    'title'     => "Erro"                                                           ,
                                    'text'      => "Erro ao atribuir títulos ao cliente. Fornecedor: $resultado"    ,
                                    'title'     => "error"                                                          ,
                                    'hasErrors' => true
                                ];
                    
                    $transaction->rollBack();
                    
                    return $aRetorno;
                    
                }
                
            }
                
            $natureza = NaturezaTitulo::model()->find("habilitado AND codigo = '$codigoNatureza'");

            if($natureza !== null)
            {
                $idNatureza = $natureza->id;
            }
            else
            {
                $idNatureza = 2;
            }
            
            $titulo = Titulo::model()->find("habilitado AND Proposta_id = $proposta->id AND NaturezaTitulo_id = $idNatureza");
            
            if($titulo == null)
            {
            
                $compra                 = new Compra            ;

                $compra->data           = date("Y-m-d H:i:s")   ;
                $compra->data_cadastro  = date("Y-m-d H:i:s")   ;
                $compra->habilitado     = 1                     ;
                $compra->TipoCompra_Id  = 3                     ;
                $compra->Fornecedor_id  = $fornecedor->id       ;

                if($compra->save())
                {
                    /*$compraHasFPHasCP               = new CompraHasFormaDePagamentoHasCondicaoDePagamento;
                    $compraHasFPHasCP->Compra_id    = 1 ;*/


                    $aRetorno = CompraHasFormaDePagamentoHasCondicaoDePagamento::model()->novo($compra->id, 2, $proposta, 1, $idNatureza);

                    if($aRetorno["hasErrors"])
                    {
                        $transaction->rollBack();

                        return $aRetorno;
                    }
                    else
                    {
                        $transaction->commit();
                    }

                }
                else
                {

                    ob_start();
                    var_dump($compra->getErrors());
                    $resultado = ob_get_clean();

                    $aRetorno = [
                                    'title'     => "Erro"                                                       ,
                                    'text'      => "Erro ao atribuir títulos ao cliente. Compra: $resultado"    ,
                                    'title'     => "error"                                                      ,
                                    'hasErrors' => true
                                ];

                    $transaction->rollBack();

                    return $aRetorno;

                }
                
            }
            
        }
        else
        {

            $aRetorno = [
                            'title'     => "Erro"                                                                                   ,
                            'text'      => "Erro ao atribuir títulos ao cliente. Proposta não encontrada. ID: $this->Proposta_id"   ,
                            'title'     => "error"                                                                                  ,
                            'hasErrors' => true
                        ];

            $transaction->rollBack();

            return $aRetorno;
            
        }
        
    }
    
}
