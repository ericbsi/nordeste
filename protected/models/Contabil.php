<?php 

class Contabil {

	public function todasAsPropostas( $nucleos )
	{
		$arrFiliaisS1 			= [];
		$arrFiliaisNordeste 	= [];
		$queryFiliaisNordeste 	= NULL;

		if( array_filter( $nucleos ) )
		{
			for ( $i 		= 0; $i < sizeof( $nucleos ); $i++ )
			{
				$nucleo 	= NucleoFiliais::model()->findByPk( $nucleos[ $i ] );

				if ( $nucleo != NULL )
				{
					foreach( $nucleo->filials as $filial )
					{
						$arrFiliaisS1[] 	= $filial->id;
					}	
				}
			}
		}

		if( array_filter( $arrFiliaisS1 ) )
		{
			$queryFiliaisNordeste 	= Integracao::model()->getFiliaisNordesteID( $arrFiliaisS1 );
		}

		return $queryFiliaisNordeste;
	}
}