<?php

class RegistroDePendencias extends CActiveRecord
{	
	public function removerItem( $itemId )
	{
		$arrReturn 	= array('hasErrors' => false);
		$item 		= ItemPendente::model()->find('ItemPendente = ' . $itemId . ' AND RegistroDePendencias_id = ' . $this->id);

		if( $item != NULL )
		{	
			$item->itemPendente->Status_Proposta_id = 2;
			$item->itemPendente->update();

			if( !$item->delete() )
			{
				$arrReturn['hasErrors'] = true;
			}
		}
		else
		{
			$arrReturn['hasErrors']	 	= true;
		}

		return $arrReturn;
	}

	public function novo( $itens_pgto_aprovados, $itens_pgto_aprovados_obs, $destinatario )
	{
		$arrReturn 										= array('hasErrors' => false);
		$RegDePendencias 								= new RegistroDePendencias;
		$RegDePendencias->dataCriacao 					= date('Y-m-d H:i:s');
		$RegDePendencias->criadoPor 					= Yii::app()->session['usuario']->id;
		$RegDePendencias->destinatario 					= $destinatario;
		
		if( $RegDePendencias->save() )
		{
			for ($i = 0; $i < count($itens_pgto_aprovados); $i++){

				if( $itens_pgto_aprovados[$i] != 0 )
				{
					$proposta 							 	= Proposta::model()->findByPk( $itens_pgto_aprovados[$i] );
					
					if( $proposta != NULL )
					{	
						$proposta->Status_Proposta_id = 7;
						$proposta->update();

						if( $proposta->statusProposta->id == 7 && $proposta->titulos_gerados && ItemPendente::model()->buscarItem($proposta->id) == NULL )
						{
							$itemPendente 								= new ItemPendente;
							$itemPendente->ItemPendente					= $itens_pgto_aprovados[$i];
							$itemPendente->habilitado					= 1;
							$itemPendente->RegistroDePendencias_id		= $RegDePendencias->id;
							$itemPendente->observacao					= $itens_pgto_aprovados_obs[$i];

							if(!$itemPendente->save())
							{
								$arrReturn['hasErrors'] = true;
							}
							else
							{
								//$proposta->gerarRegistroRetornoParceiro('03');
							}
						}
					}
				}
			}

			$arrReturn['registroId'] = $RegDePendencias->id;
		}
		else
		{
			$arrReturn['hasErrors'] = true;
		}

		return $arrReturn;
	}

	public function editarItens( $itens_pgto_aprovados, $itens_pgto_aprovados_obs )
	{
		for ( $i = 0; $i < count( $itens_pgto_aprovados ); $i++ )
		{
			if( $itens_pgto_aprovados[$i] != 0 && ItemDoBordero::model()->find('Proposta_id = ' . $itens_pgto_aprovados[$i]) == NULL && ItemPendente::model()->find('ItemPendente = ' . $itens_pgto_aprovados[$i] ) == NULL )
			{	

				$proposta 							 	= Proposta::model()->findByPk( $itens_pgto_aprovados[$i] );

				if( $proposta != NULL )
				{
					$proposta->Status_Proposta_id = 7;
					$proposta->update();

					if( $proposta->statusProposta->id == 7 && $proposta->titulos_gerados)
					{
						$itemPendente 								= new ItemPendente;
						$itemPendente->ItemPendente					= $itens_pgto_aprovados[$i];
						$itemPendente->habilitado					= 1;
						$itemPendente->RegistroDePendencias_id		= $this->id;
						$itemPendente->observacao					= $itens_pgto_aprovados_obs[$i];
						$itemPendente->save();
					}
				}
			}
		}
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'RegistroDePendencias';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('criadoPor, destinatario', 'required'),
			array('criadoPor, destinatario', 'numerical', 'integerOnly'=>true),
			array('dataCriacao', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, dataCriacao, criadoPor, destinatario', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'itemPendentes' => array(self::HAS_MANY, 	'ItemPendente', 'RegistroDePendencias_id'),
			'destinatario' 	=> array(self::BELONGS_TO, 	'Filial', 		'destinatario'),
			'criadoPor' 	=> array(self::BELONGS_TO, 	'Usuario', 		'criadoPor'),
		);
	}

    public function totalRepasse()
    {
        $util		= new Util;
        $repasse 	= 0;

        foreach( $this->itemPendentes as $item )
        {
            if($item->habilitado)
            {
                $repasse += $util->arredondar( $item->itemPendente->valorRepasse() );
            }
        }

        return $repasse;
    }

    public function totalFinanciado()
    {
        $util		= new Util;
        $totalFin 	= 0;

        foreach( $this->itemPendentes as $item )
        {
            if($item->habilitado)
            {
                $totalFin += ($item->itemPendente->valor - $item->itemPendente->valor_entrada);
            }
        }

        return $totalFin;
    }
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' 				=> 'ID',
			'dataCriacao' 		=> 'Data Criacao',
			'criadoPor' 		=> 'Criado Por',
			'destinatario' 		=> 'Destinatario',
		);
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('dataCriacao',$this->dataCriacao,true);
		$criteria->compare('criadoPor',$this->criadoPor);
		$criteria->compare('destinatario',$this->destinatario);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RegistroDePendencias the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
