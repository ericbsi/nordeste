<?php

/**
 * This is the model class for table "Cartao_has_Limite".
 *
 * The followings are the available columns in table 'Cartao_has_Limite':
 * @property integer $Cartao_id
 * @property integer $Limite_id
 * @property integer $id
 * @property integer $habilitado
 * @property string $data_cadastro
 *
 * The followings are the available model relations:
 * @property Cartao $cartao
 * @property Limite $limite
 */
class CartaoHasLimite extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Cartao_has_Limite';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Cartao_has_Proposta_id, Limite_id, ativo, habilitado, data_cadastro', 'required'),
			array('Cartao_has_Proposta_id, Limite_id, ativo, habilitado', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Cartao_has_Proposta_id, Limite_id, id, habilitado, ativo, data_cadastro', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cartaoHasProposta' => array(self::BELONGS_TO, 'CartaoHasProposta'  , 'Cartao_has_Proposta_id'  ),
			'limite'            => array(self::BELONGS_TO, 'Limite'             , 'Limite_id'               ),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Cartao_has_Proposta_id'    => 'Cartao Proposta'    ,
			'Limite_id'                 => 'Limite'             ,
			'id'                        => 'ID'                 ,
			'habilitado'                => 'Habilitado'         ,
			'ativo'                     => 'Ativo'              ,
			'data_cadastro'             => 'Data Cadastro'      ,
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Cartao_has_Proposta_id' , $this->Cartao_has_Proposta_id         );
		$criteria->compare('Limite_id'              , $this->Limite_id                      );
		$criteria->compare('id'                     , $this->id                             );
		$criteria->compare('habilitado'             , $this->habilitado                     );
		$criteria->compare('ativo'                  , $this->ativo                          );
		$criteria->compare('data_cadastro'          , $this->data_cadastro          , true  );

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CartaoHasLimite the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
