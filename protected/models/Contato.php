<?php

class Contato extends CActiveRecord
{

	public function telefonesToXml()
	{	
		$contatoHasTelefones 	= ContatoHasTelefone::model()->findAll('Contato_id = ' . $this->id . ' AND habilitado');
		$xml 					= '';

		$tipos					= ['celular', 'proprio'];
		$inc 					= 0;
		$tipo 				 	= '';

		foreach( $contatoHasTelefones as $cht )
		{
			if( $inc % 2 == 0 )
			{
				$tipo 			= $tipos[0];
			}
			else
			{
				$tipo 			= $tipos[1];
			}

			$xml .= '<telefone>
                	  <tipo>'.$tipo.'</tipo>
                      <ddd>' . $cht->telefone->getDDD() . '</ddd>
                      <numero>' . $cht->telefone->getNumero() . '</numero>
					</telefone>';
			$inc++;
		}

		return $xml;
	}

	public function getLasTelefone()
	{
		$tel = NULL;

		$contatoHasTelefone = ContatoHasTelefone::model()->find('Contato_id = ' . $this->id . ' ORDER BY data_cadastro DESC');

		if( $contatoHasTelefone != NULL )
		{
			$telefone = Telefone::model()->find('id = '.$contatoHasTelefone->Telefone_id . ' AND habilitado' );
			
			if( $telefone != NULL )
			{
				$tel = $telefone;
			}
		}

		return $tel;
	}

	public function listarTelefones()
	{
		$arrIdsTelefones 		= array();

		$contatoHasTelefones 	= ContatoHasTelefone::model()->findAll('Contato_id = ' . $this->id . ' AND habilitado');

		if( count( $contatoHasTelefones ) > 0 )
		{	
			foreach($contatoHasTelefones as $cHT)
			{
				$arrIdsTelefones[] 	= $cHT->Telefone_id;
			}

			$criteria 					= new CDbCriteria;
			$criteria->addInCondition('t.id', 			$arrIdsTelefones, 	' AND ');
			$criteria->addInCondition('t.habilitado', 	array(1), 			' AND ');

			$telefones 					= Telefone::model()->findAll($criteria);

			if( count( $telefones ) > 0 )
			{
				return $telefones;
			}
			else
			{
				return NULL;
			}
		}

		else
		{
			return NULL;
		}
	}

	/*ToDo*/
	public function listarEmails()
	{
		
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Contato';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('habilitado', 'numerical', 'integerOnly'=>true),
			array('facebook, skype, twitter', 'length', 'max'=>45),
			array('data_cadastro, data_cadastro_br', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, facebook, skype, twitter, data_cadastro, data_cadastro_br, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'contatoHasEmails' 		=> array(self::HAS_MANY, 'ContatoHasEmail', 	'Contato_id'),
			'contatoHasTelefones' 	=> array(self::HAS_MANY, 'ContatoHasTelefone', 	'Contato_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'facebook' => 'Facebook',
			'skype' => 'Skype',
			'twitter' => 'Twitter',
			'data_cadastro' => 'Data Cadastro',
			'data_cadastro_br' => 'Data / Hora Cadastro',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('facebook',$this->facebook,true);
		$criteria->compare('skype',$this->skype,true);
		$criteria->compare('twitter',$this->twitter,true);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('data_cadastro_br',$this->data_cadastro_br,true);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Contato the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
