<?php

/**
 * This is the model class for table "Filial_Has_Tabela_Cotacao".
 *
 * The followings are the available columns in table 'Filial_Has_Tabela_Cotacao':
 * @property integer $id
 * @property integer $Tabela_Cotacao_id
 * @property integer $Filial_id
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property Filial $filial
 * @property TabelaCotacao $tabelaCotacao
 */
class FilialHasTabelaCotacao extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Filial_Has_Tabela_Cotacao';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Tabela_Cotacao_id, Filial_id, habilitado', 'required'),
			array('Tabela_Cotacao_id, Filial_id, habilitado', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, Tabela_Cotacao_id, Filial_id, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'filial' => array(self::BELONGS_TO, 'Filial', 'Filial_id'),
			'tabelaCotacao' => array(self::BELONGS_TO, 'TabelaCotacao', 'Tabela_Cotacao_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Tabela_Cotacao_id' => 'Tabela Cotacao',
			'Filial_id' => 'Filial',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Tabela_Cotacao_id',$this->Tabela_Cotacao_id);
		$criteria->compare('Filial_id',$this->Filial_id);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FilialHasTabelaCotacao the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
