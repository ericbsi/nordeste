<?php

/**
 * This is the model class for table "Email".
 *
 * The followings are the available columns in table 'Email':
 * @property integer $id
 * @property string $email
 * @property integer $habilitado
 * @property string $data_cadastro
 *
 * The followings are the available model relations:
 * @property ContatoHasEmail[] $contatoHasEmails
 */
class Email extends CActiveRecord
{	

	public function atualizar($EmailCliente, $EmailClienteId){

		$email = Email::model()->findByPk($EmailClienteId);
		$email->attributes = $EmailCliente;
		$email->update();
	}

	public function loadFormEdit($id){

		$email 	= Email::model()->findByPk($id);

	    return array(
	    	'fields' 				=> array(
		    	'id' 				=> array(
		    		'value' 		=> $email->id,
		    		'type' 			=> 'text',
		    		'input_bind'	=> 'email_cliente_id',
		    	),
		    	'email'				=> array(
		    		'value' 		=> $email->email,
		    		'type' 			=> 'text',
		    		'input_bind'	=> 'email_email',
		    	)
	    	),
	    	'form_params'			=> array(
	    		'id'				=> 'form-add-email',
	    		'action'			=> '/email/update/',
	    	),
	    	'modal_id'				=> 'modal_form_new_email'
	    );
	}

	public function novo($EmailCliente,$Cliente_id){

		if( !empty( $EmailCliente['email'] ) )
		{
			$cliente 							= Cliente::model()->findByPk($Cliente_id);
			$email 								= new Email;
			$email->attributes 					= $EmailCliente;
			$email->data_cadastro 				= date('Y-m-d H:i:s');
			$email->data_cadastro_br 			= date('d/m/Y');

			if( $email->save() )
			{
				$contatoHasEmail 				= new ContatoHasEmail;
				$contatoHasEmail->Email_id 		= $email->id;
				$contatoHasEmail->Contato_id 	= $cliente->pessoa->Contato_id;
				$contatoHasEmail->save();
			}
		}
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Email';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('habilitado', 'numerical', 'integerOnly'=>true),
			array('email', 'length', 'max'=>45),
			array('data_cadastro, data_cadastro_br', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, email, habilitado, data_cadastro, data_cadastro_br', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contatoHasEmails' => array(self::HAS_MANY, 'ContatoHasEmail', 'Email_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email' => 'Email',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'data_cadastro_br' => 'Data / Hora Cadastro',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('data_cadastro_br',$this->data_cadastro_br,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Email the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
