<?php

/**
 * This is the model class for table "RecebimentoDeDocumentacao_has_Proposta".
 *
 * The followings are the available columns in table 'RecebimentoDeDocumentacao_has_Proposta':
 * @property integer $Proposta_id
 * @property integer $RecebimentoDeDocumentacao_id
 * @property integer $id
 * @property string $data_cadastro
 * @property integer $habilitado
 *
 * The followings are the available model relations:
 * @property Proposta $proposta
 * @property RecebimentoDeDocumentacao $recebimentoDeDocumentacao
 */
class RecebimentoDeDocumentacaoHasProposta extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'RecebimentoDeDocumentacao_has_Proposta';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Proposta_id, RecebimentoDeDocumentacao_id', 'required'),
			array('Proposta_id, RecebimentoDeDocumentacao_id, id, habilitado', 'numerical', 'integerOnly'=>true),
			array('data_cadastro', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Proposta_id, RecebimentoDeDocumentacao_id, id, data_cadastro, habilitado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'proposta' => array(self::BELONGS_TO, 'Proposta', 'Proposta_id'),
			'recebimentoDeDocumentacao' => array(self::BELONGS_TO, 'RecebimentoDeDocumentacao', 'RecebimentoDeDocumentacao_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Proposta_id' => 'Proposta',
			'RecebimentoDeDocumentacao_id' => 'Recebimento De Documentacao',
			'id' => 'ID',
			'data_cadastro' => 'Data Cadastro',
			'habilitado' => 'Habilitado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Proposta_id',$this->Proposta_id);
		$criteria->compare('RecebimentoDeDocumentacao_id',$this->RecebimentoDeDocumentacao_id);
		$criteria->compare('id',$this->id);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('habilitado',$this->habilitado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RecebimentoDeDocumentacaoHasProposta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
