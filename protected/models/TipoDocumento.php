<?php

class TipoDocumento extends CActiveRecord{
	
	public function tableName(){

		return 'Tipo_documento';
	}

	
	public function rules(){
		
		return array(
			array('tipo', 'length', 'max'=>100),
			array('id, tipo', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations(){
		
		return array(
			'documentos' => array(self::HAS_MANY, 'Documento', 'Tipo_documento_id'),
		);
	}

	
	public function attributeLabels(){

		return array(
			'id' => 'ID',
			'tipo' => 'Tipo',
		);
	}

	
	public function search(){

		$criteria = new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tipo',$this->tipo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TipoDocumento the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
