<?php

class TabelaCotacao extends CActiveRecord
{

    public function parceiroRelationChange($parceiroId, $checked, $tabelaId)
    {

        $filialHasTabelaCotacao = FilialHasTabelaCotacao::model()->find('Tabela_Cotacao_id = ' . $tabelaId . ' AND Filial_id = ' . $parceiroId);

        if ($checked == 'true')
            $checked = true;
        else
            $checked = false;

        if ($filialHasTabelaCotacao == NULL)
        {

            $fhtc = new FilialHasTabelaCotacao;
            $fhtc->Filial_id = $parceiroId;
            $fhtc->Tabela_Cotacao_id = $tabelaId;

            if ($checked)
            {
                $fhtc->habilitado = 1;
            } else
            {
                $fhtc->habilitado = 0;
            }

            if ($fhtc->save())
            {
                return "Sucesso";
            } else
            {
                return $fhtc->getErrors();
            }
        } else
        {

            if ($checked)
            {
                $filialHasTabelaCotacao->habilitado = 1;
            } else
            {
                $filialHasTabelaCotacao->habilitado = 0;
            }

            if ($filialHasTabelaCotacao->update())
            {
                return "Sucesso";
            } else
            {
                return $filialHasTabelaCotacao->getErrors();
            }
        }
    }

    public function hasFilial($filial)
    {
        $reg = FilialHasTabelaCotacao::model()->findAll('Tabela_Cotacao_id = ' . $this->id . ' AND Filial_id = ' . $filial->id . ' AND habilitado ');

        if ($reg != NULL)
        {
            return TRUE;
        } else
        {
            return FALSE;
        }
    }

    public function listFinanceiras()
    {
        return FinanceiraHasTabelaCotacao::model()->findAll('Tabela_Cotacao_id = ' . $this->id);
    }

    public function listParceiros()
    {
        return FilialHasTabelaCotacao::model()->findAll('Tabela_Cotacao_id = ' . $this->id . ' AND habilitado');
    }

    public function listParceirosAusentes()
    {
        $arrParceiros = array();
        $parceirosDesabilitados = FilialHasTabelaCotacao::model()->findAll('Tabela_Cotacao_id = ' . $this->id . ' AND NOT habilitado');

        $Qry = "SELECT f.id as Filial_id FROM `Filial` as f ";
        $Qry .= " LEFT JOIN Filial_Has_Tabela_Cotacao as fhtc ";
        $Qry .= " ON f.id = fhtc.Filial_id ";
        $Qry .= " WHERE fhtc.id IS NULL AND f.habilitado ";

        foreach ($parceirosDesabilitados as $pD):
            $arrParceiros[] = array(
                'id' => $pD->filial->id,
                'nome' => strtoupper($pD->filial->getConcat()),
            );
        endforeach;

        foreach (Yii::app()->db->createCommand($Qry)->queryAll() as $r):
            $filial = Filial::model()->findByPk($r['Filial_id']);

            $arrParceiros[] = array(
                'id' => $filial->id,
                'nome' => strtoupper($filial->getConcat()),
            );

        endforeach;

        return $arrParceiros;
    }

    public function getFatores($criteria = null)
    {

        return Fator::model()->findAll('Tabela_Cotacao_id = ' . $this->id . ' AND habilitado');
    }

    public function listFatores($draw, $TabelaId, $start)
    {

        $rows = array();

        if (!empty($TabelaId))
        {

            $tabela = TabelaCotacao::model()->findByPk($TabelaId);

            if ($tabela != NULL)
            {

                if (count($tabela->getFatores()) > 0)
                {

                    $crt            = new CDbCriteria()                                     ;
                    $crt->condition = 'Tabela_Cotacao_id=:TCotacao_id AND habilitado=:hab'  ;
                    $crt->params    = array(':TCotacao_id' => $TabelaId, ':hab' => 1)       ;
                    $crt->offset    = $start                                                ;
                    $crt->limit     = 10                                                    ;
                    $crt->order     = 'carencia ASC, parcela ASC'                           ;

                    Fator::model()->findAll($crt);

                    foreach (Fator::model()->findAll($crt) as $fator)
                    {
                        $row = array
                        (
                            'idFator'   => $fator->id                                               ,
                            'carencia'  => $fator->carencia                                         ,
                            'fator'     => $fator->fator                                            ,
                            'fatorIOF'  => $fator->fatorIOF                                         ,
                            'retencao'  => number_format($fator->porcentagem_retencao, 2, ',', '.') ,
                            'parcela'   => $fator->parcela . ' °'
                        );

                        $rows[] = $row;

                    }
                }
            }
        }

        return (array(
            "draw" => $draw,
            "recordsTotal" => count($tabela->getFatores()),
            "recordsFiltered" => count($tabela->getFatores()),
            "data" => $rows
        ));
    }

    public function novo($TabelaCotacao, $Filiais, $Financeiras)
    {
        $arrayReturn = array(
            'hasErrors' => 0,
            'msg' => 'Tabela Cadastrada com Sucesso.',
            'pntfyClass' => 'success'
        );

        if (count($Filiais) > 0 && !empty($TabelaCotacao['descricao']) && count($Financeiras) > 0)
        {

            $tabela = new TabelaCotacao;
            $tabela->attributes = $TabelaCotacao;
            $tabela->habilitado = 1;
            $tabela->data_cadastro = date('Y-m-d H:i:S');

            if ($tabela->save())
            {
                for ($i = 0; $i < count($Filiais); $i++)
                {
                    $filialHasTabelaCotacao = new FilialHasTabelaCotacao;
                    $filialHasTabelaCotacao->Filial_id = $Filiais[$i];
                    $filialHasTabelaCotacao->Tabela_Cotacao_id = $tabela->id;
                    $filialHasTabelaCotacao->habilitado = 1;

                    if (!$filialHasTabelaCotacao->save())
                    {
                        $arrayReturn['hasErrors'] = 1;
                    }
                }

                for ($i = 0; $i < count($Financeiras); $i++)
                {
                    $financeiraHasTabelaCotacao = new FinanceiraHasTabelaCotacao;
                    $financeiraHasTabelaCotacao->Financeira_id = $Financeiras[$i];
                    $financeiraHasTabelaCotacao->Tabela_Cotacao_id = $tabela->id;
                    $financeiraHasTabelaCotacao->habilitado = 1;


                    if (!$financeiraHasTabelaCotacao->save())
                    {
                        $arrayReturn['hasErrors'] = 1;
                    }
                }
            } else
            {
                $arrayReturn['hasErrors'] = 1;
            }
        }

        if ($arrayReturn['hasErrors'])
        {
            $arrayReturn['msg'] = 'Ocorreu um erro. Tente novamente.';
            $arrayReturn['pntfyClass'] = 'error';
        }

        return $arrayReturn;
    }

    public function listar($draw, $offset, $order, $columns)
    {

        $countTabelas = TabelaCotacao::model()->findAll('habilitado');

        $util = new Util;

        $rows = array();

        $crt = new CDbCriteria();
        $crt->condition = "habilitado";
        $crt->offset = $offset;
        $crt->limit = 10;

        $tabelas = TabelaCotacao::model()->findAll($crt);

        foreach ($tabelas as $tabela)
        {

            $btn = '<form method="post" action="' . Yii::app()->getBaseUrl(true) . '/tabelaCotacao/editar">';
            $btn .= '  <button class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Editar"><i class="fa fa-edit"></i></button>';
            $btn .= '  <input type="hidden" name="tabelaId" value="' . $tabela->id . '">';
            $btn .= '</form>';

            $row = array(
                'idTabela' => $tabela->id,
                'descricao' => '<a data-query-string="coluna=descricao&objId=' . $tabela->id . '" data-url="/tabelaCotacao/atualizarAtributo/">' . strtoupper($tabela->descricao) . '</a>',
                'modalidade' => strtoupper($tabela->modalidade->descricao),
                'parcela_inicio' => '<a data-query-string="coluna=parcela_inicio&objId=' . $tabela->id . '" data-url="/tabelaCotacao/atualizarAtributo/">' . $tabela->parcela_inicio . '</a>',
                'parcela_fim' => '<a data-query-string="coluna=parcela_fim&objId=' . $tabela->id . '" data-url="/tabelaCotacao/atualizarAtributo/">' . $tabela->parcela_fim . '</a>',
                'taxa' => '<a data-query-string="coluna=taxa&objId=' . $tabela->id . '" data-url="/tabelaCotacao/atualizarAtributo/">' . $tabela->taxa . '</a>',
                'taxa_anual' => '<a data-query-string="coluna=taxa_anual&objId=' . $tabela->id . '" data-url="/tabelaCotacao/atualizarAtributo/">' . $tabela->taxa_anual . '</a>',
                'btn' => $btn,
            );

            $rows[] = $row;
        }

        return (array(
            "draw" => $draw,
            "recordsTotal" => count($countTabelas),
            "recordsFiltered" => count($countTabelas),
            "data" => $rows
        ));
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'Tabela_Cotacao';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('descricao, habilitado, ModalidadeId, data_cadastro', 'required'),
            array('taxa, taxa_anual', 'numerical'),
            array('habilitado, ModalidadeId, parcela_inicio, parcela_fim', 'numerical', 'integerOnly' => true),
            array('descricao', 'length', 'max' => 45),
            array('id, descricao, taxa, taxa_anual, habilitado, ModalidadeId, data_cadastro, parcela_inicio, parcela_fim', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'fators' => array(self::HAS_MANY, 'Fator', 'Tabela_Cotacao_id'),
            'modalidade' => array(self::BELONGS_TO, 'ModalidadeTabela', 'ModalidadeId'),
            'tabelaCotacaoHasCarencia' => array(self::HAS_MANY, 'TabelaCotacaoHasCarencia', 'TabelaCotacaoId', 'on' => 'tabelaCotacaoHasCarencia.habilitado = 1', 'order'=>'tabelaCotacaoHasCarencia.CarenciaId ASC'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'descricao' => 'Descricao',
            'habilitado' => 'Habilitado',
            'taxa' => 'Taxa',
            'taxa_anual' => 'Taxa Anual',
            'data_cadastro' => 'Data Cadastro',
            'parcela_inicio' => 'Parcela Inicio',
            'parcela_fim' => 'Parcela Fim',
            'ModalidadeId' => 'Modalidade',
        );
    }

    public function taxaToView()
    {

        return number_format($this->taxa, 2, ",", ".");
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('taxa', $this->taxa);
        $criteria->compare('descricao', $this->descricao, true);
        $criteria->compare('habilitado', $this->habilitado);
        $criteria->compare('data_cadastro', $this->data_cadastro, true);
        $criteria->compare('parcela_inicio', $this->parcela_inicio);
        $criteria->compare('parcela_fim', $this->parcela_fim);
        $criteria->compare('ModalidadeId', $this->ModalidadeId);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function returnCarenciasXParcelas($tabelaId, $idFilial = null)
    {
        $tabela = TabelaCotacao::model()->findByPk($tabelaId);

        $carencias = [];
        $parcelas = [];
        $bloquearSeg = 0;

        $filial = Filial::model()->findByPk($idFilial);

        if (($filial !== NULL) && (in_array($filial->NucleoFiliais_id, [40])) && $tabela->ModalidadeId == 2)
        {
            $bloquearSeg = 1;
        }

        foreach ($tabela->tabelaCotacaoHasCarencia as $tchc):
            $c = array(
                'id' => $tchc->carencia->valor,
                'text' => $tchc->carencia->valor
            );
            $carencias[] = $c;
        endforeach;

        for ($i = $tabela->parcela_inicio; $i <= $tabela->parcela_fim; $i++)
        {
            // if( strval($i) > 4 || $tabela->id == 40){
            //
            // }
            $p = array(
                'id' => strval($i),
                'text' => strval($i)
            );
            $parcelas[] = $p;
        }

        return array(
            'carencias' => $carencias,
            'parcelas' => $parcelas,
            'bloqSeg' => $bloquearSeg
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TabelaCotacao the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
