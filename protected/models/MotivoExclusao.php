<?php

/**
 * This is the model class for table "MotivoExclusao".
 *
 * The followings are the available columns in table 'MotivoExclusao':
 * @property integer $id
 * @property integer $OrgaoNegacao_id
 * @property integer $habilitado
 * @property string $data_cadastro
 * @property string $codigo
 * @property string $descricao
 *
 * The followings are the available model relations:
 * @property FichamentoHasStatusFichamentoHasMotivoExclusao[] $fichamentoHasStatusFichamentoHasMotivoExclusaos
 * @property OrgaoNegacao $orgaoNegacao
 */
class MotivoExclusao extends CActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'MotivoExclusao';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('OrgaoNegacao_id, habilitado, data_cadastro, descricao', 'required'),
            array('OrgaoNegacao_id, habilitado', 'numerical', 'integerOnly' => true),
            array('codigo', 'length', 'max' => 45),
            array('descricao', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, OrgaoNegacao_id, habilitado, data_cadastro, codigo, descricao', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'fichamentoHasStatusFichamentoHasMotivoExclusaos' => array(self::HAS_MANY, 'FichamentoHasStatusFichamentoHasMotivoExclusao', 'MotivoExclusao_id'),
            'orgaoNegacao' => array(self::BELONGS_TO, 'OrgaoNegacao', 'OrgaoNegacao_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'OrgaoNegacao_id' => 'Orgao Negacao',
            'habilitado' => 'Habilitado',
            'data_cadastro' => 'Data Cadastro',
            'codigo' => 'Codigo',
            'descricao' => 'Descricao',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('OrgaoNegacao_id', $this->OrgaoNegacao_id);
        $criteria->compare('habilitado', $this->habilitado);
        $criteria->compare('data_cadastro', $this->data_cadastro, true);
        $criteria->compare('codigo', $this->codigo, true);
        $criteria->compare('descricao', $this->descricao, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return MotivoExclusao the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    public function gerarMotivos()
    {
        
        $xml =  "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:web='http://webservice.spc.insumo.spcjava.spcbrasil.org/'>
                    <soapenv:Header/>
                    <soapenv:Body>
                       <web:listarMotivoExclusao/>
                    </soapenv:Body>
                 </soapenv:Envelope>";
        
        
        $linkSPC    = "https://treina.spc.org.br/spc/remoting/ws/insumo/spc/spcWebService?wsdl" ;
        $oSPC       = "395165"                                                                  ;
        $sSPC       = "27072015"                                                                ;
        
        $autorizacao    = 'Basic ' . base64_encode("$oSPC:$sSPC")   ;

        $headers = array(
                            'Content-Type:text/xml;charset=UTF-8',
                            'Connection: close',
                            'Authorization: ' . $autorizacao, // <---,
                            'Content-length: ' . strlen($xml)
                        );

        $ch = curl_init($linkSPC);

        curl_setopt($ch, CURLOPT_HTTPHEADER       , $headers    );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER   , 1           );
        curl_setopt($ch, CURLOPT_POSTFIELDS       , $xml        );

        $retorno    = curl_exec($ch)                    ;
        $objXML     = simplexml_load_string($retorno)   ;
        $namespaces = $objXML->getNamespaces(true)      ;

        $objXML->registerXPathNamespace('s'     , $namespaces["S"   ]);
        $objXML->registerXPathNamespace('ns2'   , $namespaces["ns2" ]);
                                    
        $resposta  = $objXML->xpath('//ns2:listarMotivoExclusaoResponse');
        
        foreach ($resposta[0] as $motivo)
        {
            
            $motivoExclusao  = MotivoExclusao::model()->find("codigo = '$motivo->id'");
            
            if($motivoExclusao !== null)
            {
                
                $motivoExclusao->habilitado = 1                                     ;
                $motivoExclusao->descricao  = $motivo->xpath("descricao-motivo")[0] ;
                
                if(!$motivoExclusao->update())
                {
                    
                    $texto  = "id:          $motivoExclusao->id  "
                            . "codigo:      $motivo->id          "
                            . "descricao:  " . $motivo->xpath("descricao-motivo")[0] ;
                    
                    ob_start()                              ;
                    var_dump($motivoExclusao->getErrors())  ;
                    $resErro = ob_get_clean()               ;
                    
                    $message            = new YiiMailMessage            ;
                    $message->view      = "erroGeraMotivoExclusao"      ;
                    $message->subject   = 'Fichamento - Credshow S/A'   ;

                    $parametros =   [
                                        'email' =>  [
                                                        'erro'  => $resErro ,
                                                        'texto' => $texto
                                                    ]
                                    ];

                    $message->setBody($parametros, 'text/html');

                    $message->addTo('contato@totorods.com');
                    $message->from = 'no-reply@credshow.com.br';

                    Yii::app()->mail->send($message);
                    
                }
                
            }
            else
            {
                
                $configON = ConfigLN::model()->find("habilitado AND parametro = 'idOrgaoNegacaoSPC'");
                
                if($configON !== null)
                {
                    $idOrgaoSPC = $configON->valor;
                }
                else
                {
                    $idOrgaoSPC = 1;
                    
                }

                $motivoExclusao                     = new MotivoExclusao                    ;
                $motivoExclusao->habilitado         = 1                                     ;
                $motivoExclusao->data_cadastro      = date("Y-m-d H:i:s")                   ;
                $motivoExclusao->codigo             = $motivo->id                           ;
                $motivoExclusao->descricao          = $motivo->xpath("descricao-motivo")[0] ;
                $motivoExclusao->OrgaoNegacao_id    = $idOrgaoSPC                           ;
                
                if(!$motivoExclusao->save())
                {
                    
                    $texto  = "codigo:      $motivo->id          "
                            . "descricao:  " . $motivo->xpath("descricao-motivo")[0] ;
                    
                    ob_start()                              ;
                    var_dump($motivoExclusao->getErrors())  ;
                    $resErro = ob_get_clean()               ;
                    
                    $message            = new YiiMailMessage            ;
                    $message->view      = "erroGeraMotivoExclusao"      ;
                    $message->subject   = 'Fichamento - Credshow S/A'   ;

                    $parametros =   [
                                        'email' =>  [
                                                        'erro'  => $resErro ,
                                                        'texto' => $texto
                                                    ]
                                    ];

                    $message->setBody($parametros, 'text/html');

                    $message->addTo('contato@totorods.com');
                    $message->from = 'no-reply@credshow.com.br';

                    Yii::app()->mail->send($message);
                    
                }
                
            }
            
        }
        
    }

}
