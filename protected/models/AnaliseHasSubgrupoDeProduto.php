<?php

class AnaliseHasSubgrupoDeProduto extends CActiveRecord
{	

	public function novo($AnaliseId, $Bem)
	{
		$analiseHasSubgrupoDeProduto                        = new AnaliseHasSubgrupoDeProduto;
        $analiseHasSubgrupoDeProduto->Analise_de_Credito_id = $AnaliseId;
        $analiseHasSubgrupoDeProduto->SubgrupoDeProtudo_id  = $Bem;
        $analiseHasSubgrupoDeProduto->save();
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'AnaliseHasSubgrupoDeProduto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('SubgrupoDeProtudo_id, Analise_de_Credito_id', 'required'),
			array('SubgrupoDeProtudo_id, Analise_de_Credito_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, SubgrupoDeProtudo_id, Analise_de_Credito_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'analiseDeCredito' => array(self::BELONGS_TO, 'AnaliseDeCredito', 'Analise_de_Credito_id'),
			'subgrupoDeProtudo' => array(self::BELONGS_TO, 'SubgrupoDeProduto', 'SubgrupoDeProtudo_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'SubgrupoDeProtudo_id' => 'Subgrupo De Protudo',
			'Analise_de_Credito_id' => 'Analise De Credito',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('SubgrupoDeProtudo_id',$this->SubgrupoDeProtudo_id);
		$criteria->compare('Analise_de_Credito_id',$this->Analise_de_Credito_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AnaliseHasSubgrupoDeProduto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
