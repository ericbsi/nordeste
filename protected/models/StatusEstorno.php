<?php

/**
 * This is the model class for table "StatusEstorno".
 *
 * The followings are the available columns in table 'StatusEstorno':
 * @property integer $id
 * @property integer $habilitado
 * @property string $data_cadastro
 * @property string $descricao
 *
 * The followings are the available model relations:
 * @property EstornoBaixaHasStatusEstorno[] $estornoBaixaHasStatusEstornos
 */
class StatusEstorno extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'StatusEstorno';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('data_cadastro, descricao', 'required'),
			array('habilitado', 'numerical', 'integerOnly'=>true),
			array('descricao', 'length', 'max'=>200),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, habilitado, data_cadastro, descricao', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'estornoBaixaHasStatusEstornos' => array(self::HAS_MANY, 'EstornoBaixaHasStatusEstorno', 'StatusEstorno_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'descricao' => 'Descricao',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('descricao',$this->descricao,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StatusEstorno the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
