<?php

/**
 * This is the model class for table "Chamado".
 *
 * The followings are the available columns in table 'Chamado':
 * @property integer $id
 * @property integer $habilitado
 * @property string $data_cadastro
 * @property integer $Atendimento_id
 *
 * The followings are the available model relations:
 * @property Atendimento $atendimento
 * @property Sequencia[] $sequencias
 */
class Chamado extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Chamado';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('data_cadastro, Atendimento_id', 'required'),
			array('habilitado, Atendimento_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, habilitado, data_cadastro, Atendimento_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'atendimento' => array(self::BELONGS_TO, 'Atendimento', 'Atendimento_id'),
			'sequencias' => array(self::HAS_MANY, 'Sequencia', 'Chamado_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'habilitado' => 'Habilitado',
			'data_cadastro' => 'Data Cadastro',
			'Atendimento_id' => 'Atendimento',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('data_cadastro',$this->data_cadastro,true);
		$criteria->compare('Atendimento_id',$this->Atendimento_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Chamado the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
