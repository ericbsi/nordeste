<?php

class Fornecedor extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Fornecedor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		
		return array(
			array('Pessoa_id, TipoFornecedor_id', 'required'),
			array('Pessoa_id, habilitado, TipoFornecedor_id, Filial_id', 'numerical', 'integerOnly'=>true),
			array('id, Pessoa_id, habilitado, TipoFornecedor_id, Filial_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'filial' 					=> array(self::BELONGS_TO, 	'Filial', 				'Filial_id'),
			'pessoa' 					=> array(self::BELONGS_TO, 	'Pessoa', 				'Pessoa_id'),
			'tipoFornecedor' 			=> array(self::BELONGS_TO, 	'TipoFornecedor', 		'TipoFornecedor_id'),
			'fornecedorHasContato' 		=> array(self::HAS_MANY, 	'FornecedorHasContato',	'Contato_id'),
			'fornecedorHasEndereco' 	=> array(self::HAS_MANY, 	'FornecedorHasEndereco','Endereco_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'Pessoa_id' => 'Pessoa',
			'habilitado' => 'Habilitado',
			'TipoFornecedor_id' => 'Tipo Fornecedor',
			'Filial_id' => 'Filial',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Pessoa_id',$this->Pessoa_id);
		$criteria->compare('habilitado',$this->habilitado);
		$criteria->compare('TipoFornecedor_id',$this->TipoFornecedor_id);
		$criteria->compare('Filial_id',$this->Filial_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Fornecedor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}